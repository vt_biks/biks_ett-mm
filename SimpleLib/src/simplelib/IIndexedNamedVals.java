/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author pmielanczuk
 */
public interface IIndexedNamedVals {

    public int getValCnt();

    public String getDstName(int idx);

    public Object getVal(int idx);
}
