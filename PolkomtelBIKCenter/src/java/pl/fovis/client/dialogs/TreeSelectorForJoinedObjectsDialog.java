/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class TreeSelectorForJoinedObjectsDialog extends TreeSelectorDialog/*TreeSelectorDialogBase*/ {

    //protected Set<Integer> alreadyJoinedObjIds;
    protected Set<String> treeKinds;
    protected Integer nodeKindId;
    protected int treeId;
    protected String relationType;
    protected boolean isNodeFromDynamicTree;

    @Override

    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        if (useLazyLoading) {
            getService().getBikAllJoinableRootTreeNodes(treeKinds, mustHideLinkedNodes(), getNodeKindId(), relationType, getTreeId(), callback);
        } else {
            getService().getBikAllJoinableTreeNodes(callback);
        }
    }

    @Override
    protected String getMessageForUnselectableNode(TreeNodeBean tnb) {
        if (alreadyJoinedManager != null && alreadyJoinedManager.getInfoForJoinedNodeById(tnb.id) != null) {
            return I18n.toPowiazanieJuzIstnieje.get() /* I18N:  */;
        }
        return super.getMessageForUnselectableNode(tnb);
    }

    public void buildAndShowDialog(int thisNodeId, Set<String> treeKinds,
            IParametrizedContinuation<TreeNodeBean> saveCont,
            IJoinedObjSelectionManager<Integer> alreadyJoinedManager) {
        this.treeKinds = treeKinds;
        //this.alreadyJoinedObjIds = alreadyJoinedObjIds;
        super.buildAndShowDialog(thisNodeId, saveCont, alreadyJoinedManager);
    }

    public void buildAndShowDialog(int thisNodeId, Set<String> treeKinds, Integer nodeKindId, Integer treeId, String relationType, boolean isNodeFromDynamicTree,
            IParametrizedContinuation<TreeNodeBean> saveCont,
            IJoinedObjSelectionManager<Integer> alreadyJoinedManager) {

        this.treeKinds = treeKinds;
        this.nodeKindId = nodeKindId; //this.alreadyJoinedObjIds = alreadyJoinedObjIds;
        this.relationType = relationType;
        this.isNodeFromDynamicTree = isNodeFromDynamicTree;
        this.treeId = treeId;
        super.buildAndShowDialog(thisNodeId, saveCont, alreadyJoinedManager);
    }

    @Override
    protected boolean isInheritToDescendantsCheckBoxVisible() {
        return true;
    }

    @Override
    protected boolean mustHideLinkedNodes() {
        return treeKinds != null ? BIKClientSingletons.isTreeKindDynamic(treeKinds) : false;
//        return BaseUtils.safeEquals(treeKind, BIKConstants.TREE_KIND_TAXONOMY);
    }

    @Override
    protected Collection<Integer> getTreeIdsForFiltering() {
        return BIKClientSingletons.getProperTreeIdsWithTreeKinds(treeKinds);
        //return " tree_id in (select id from bik_tree where tree_kind in ('" + treeKind + "'))";
    }

    @Override
    protected boolean isNodeKindIdInLinkedKind(TreeNodeBean tb) {
        return getNodeKindId() == null ? false : !tb.isFixed;
    }

    @Override
    protected Integer getNodeKindId() {
        return isNodeFromDynamicTree ? nodeKindId : null;
    }

    protected String getRelationType() {
        return BIKConstants.ASSOCIATION;
    }

    protected Integer getTreeId() {
        return treeId;
    }

    protected Pair<String, String> getInnerJoinRelationType(Integer nodeKindId) {
        return new Pair<String, String>(nodeKindId != -1 ? BIKConstants.CHILD : null, BIKConstants.ASSOCIATION);
    }

    @Override
    protected void addAdditionalButtons(HorizontalPanel hp) {
        if (BIKClientSingletons.isShowAddNodeButtonOnHyperlinkDialog()) {
            hp.add(addNodeBtn());
        }
    }

}
