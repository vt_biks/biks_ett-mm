/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.util.List;
import pl.fovis.common.ArticleBean;
import pl.fovis.common.AttachmentBean;
import pl.fovis.common.BlogBean;
import pl.fovis.common.FrequentlyAskedQuestionsBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.dqm.DataQualityGroupBean;

/**
 *
 * @author wezyr
 */
public interface IBOXIServiceForEDDBPropsReader extends IBOXIServiceForTabWidgetDP {

//    public UserBean getLoggedUserBean();
    public SystemUserBean getLoggedUserBean();

//    public List<BizDefBean> getBizDefVersInDepartments(int nodeId);
    public boolean isInFvs(int nodeId);

//    public List<JoinedObjBean> getBikArticle(int nodeId);
    public ArticleBean getBikBizDefForNodeId(int nodeId);

    public BlogBean getBikEntryByNodeId(int nodeId);

    public AttachmentBean getBikDocumentForSiId(int nodeId);

//    public TreeNodeBean getNodeBeanById(int nodeId, boolean allowNull);
    public FrequentlyAskedQuestionsBean getFrequentlyAskedQuestions(int nodeId);

//    public String getAvatarFieldForAd();
    public List<DataQualityGroupBean> getDqcTestGroupExtradata(int nodeId);

    public BOXIServiceImplData getBOXIServiceImplDataForCurrentRequest();

//    public Map<Integer, Float> getOptSimilarNodes(int nodeId);
//    public int getSuggestSimilarMaxResults();
//    public Set<String> getDynamicTreeKindCodes();
//
//    public UserBean bikUserByNodeId(int nodeId);
}
