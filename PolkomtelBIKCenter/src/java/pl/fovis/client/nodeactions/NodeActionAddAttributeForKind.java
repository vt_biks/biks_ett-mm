/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author Eugeniusz
 */
public class NodeActionAddAttributeForKind extends NodeActionAddNodeBaseOnTreeKindBase {

    public String actionCode = "addOrEditAttribute";

    @Override
    public boolean isEnabledByDesign() {
        return !data.attributes.isEmpty() && BIKClientSingletons.hideAddAttrBtnsOnRight()
                /*&& (BIKClientSingletons.isEffectiveExpertLoggedIn() || BIKClientSingletons.isAppAdminLoggedIn()
                || BIKClientSingletons.isBranchAuthorLoggedIn(data.node.branchIds) || BIKClientSingletons.isLoggedUserAuthorOfTree(data.node.treeId)
                || BIKClientSingletons.isLoggedUserCreatorOfTree(data.node.treeId) || BIKClientSingletons.isBranchCreatorLoggedIn(data.node.branchIds))*/
                && !(BIKConstants.NODE_KIND_META_PRINT_ATTRIBUT.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(data.node.nodeKindId))
                || BIKConstants.NODE_KIND_META_PRINT_KIND.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(data.node.nodeKindId))
                || BIKConstants.NODE_KIND_META_PRINTS_TEMPLATES.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(data.node.nodeKindId))
                || BIKConstants.NODE_KIND_META_PRINT.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(data.node.nodeKindId)));
    }

    @Override
    public String getCaption() {
        return I18n.dodajLubEdytAtrybut.get();
    }

    @Override
    public void execute() {
        BIKClientSingletons.getEDPForms().showAttributeForm(node.id, node, null, node.treeId, BIKClientSingletons.filtrPublicAttributes(node, data.attributes), node.nodeKindId,
                (BIKClientSingletons.isTreeKindDynamic(node.treeKind) && !BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(node.treeKind)), null);
    }
}
