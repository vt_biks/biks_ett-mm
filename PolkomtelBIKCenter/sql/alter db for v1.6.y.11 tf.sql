﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- Poprawki do konektora MSSQL
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_check_version '1.6.y.11';
go

if not exists(select 1 from bik_node_kind where code = 'MSSQLTableFunction')
begin
	update bik_node_kind
	set caption = 'Funkcja skalarna MSSQL'
	where code = 'MSSQLFunction'

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('MSSQLTableFunction', 'Funkcja tabelaryczna MSSQL', 'mssqlTableFunction', 'Metadata', 0, 8, 0)

	insert into bik_translation(code, txt, lang, kind)
	values ('MSSQLTableFunction', 'MSSQL table-valued function', 'en', 'kind')
	
	update bik_translation
	set txt = 'MSSQL scalar-valued function'
	where code = 'MSSQLFunction'
	and kind = 'kind'
	and lang = 'en'

	declare @treeOfTreesId int = dbo.fn_tree_id_by_code('TreeOfTrees');

	exec sp_add_menu_node '&MSSQLDatabase', '@', '&MSSQLTableFunction'
	    
	exec sp_node_init_branch_id @treeOfTreesId, null
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_mssql_extended_properties') and type in (N'U'))
begin
	create table aaa_mssql_extended_properties (
		id int not null identity(1,1) primary key,
		server varchar(500) not null,
		database_name sysname not null,
		type varchar(10) not null,
		owner sysname null,
		table_name sysname null,
		column_name sysname null,
		name varchar(512) not null,
		value varchar(max) null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_mssql_extended_properties') and type in (N'U'))
begin
	create table bik_mssql_extended_properties (
		id int not null identity(1,1) primary key,
		server varchar(500) not null,
		node_id int not null references bik_node(id) unique,
		name varchar(512) not null,
		value varchar(max) null
	);
end;
go

exec sp_update_version '1.6.y.11', '1.6.y2';
go