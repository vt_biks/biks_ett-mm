/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.javasdk;

import com.crystaldecisions.sdk.framework.CrystalEnterprise;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.framework.ISessionMgr;
import com.crystaldecisions.sdk.occa.infostore.CeSecurityID;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import com.crystaldecisions.sdk.occa.infostore.ISecurityInfo2;
import com.crystaldecisions.sdk.occa.infostore.RightDescriptor;
import com.crystaldecisions.sdk.occa.security.CeSecurityOptions;
import commonlib.LameUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class ReportAccessChecker {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    // metoda do sprawdzania uprawnien usera do raportu. Sprawdzane jest tylko podstawowe prawo: View report
    public static boolean canOpenReport(String si_id, int userID, String server, String user, String password) {
        IEnterpriseSession enterpriseSession = null;
        try {
            ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
            enterpriseSession = sessionMgr.logon(user, password, server, SapBOPump.AUTHENTICATION_METHOD);
            IInfoStore iStore = (IInfoStore) enterpriseSession.getService("InfoStore");
            boolean checkRight = checkRight(iStore, si_id, userID);
            enterpriseSession.logoff();
            return checkRight;
        } catch (Exception ex) {
            if (enterpriseSession != null) {
                enterpriseSession.logoff();
            }
            if (logger.isErrorEnabled()) {
                logger.error("Error in Report Access Checker", ex);
            }
            return true;
        }
    }

    // metoda nie loguje się i nie wylogowuje, działa już na gotowym "połączeniu"
    public static boolean checkRight(IInfoStore iStore, String si_id, int userID) throws Exception {
        IInfoObjects results = iStore.query("select * from ci_infoobjects where si_kind='Webi' and si_instance=0 and si_id=" + si_id);
        IInfoObject report = (IInfoObject) results.get(0);
        ISecurityInfo2 securityInfo2 = report.getSecurityInfo2();
        RightDescriptor rightToCheck = new RightDescriptor(CeSecurityID.Right.VIEW, "", false, CeSecurityOptions.RightScope.CURRENT_OBJECT, CeSecurityOptions.ANY_OBJTYPE);
        return securityInfo2.checkRight(rightToCheck, userID, false);
    }

    // for tests
//    public static void main(String[] args) {
//        try {
//            ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
//            IEnterpriseSession enterpriseSession;
//            enterpriseSession = sessionMgr.logon("Administrator", "1qaz2wsx", "192.168.167.57", "secEnterprise");
//            IInfoStore iStore = (IInfoStore) enterpriseSession.getService("InfoStore");
//            IInfoObjects results = iStore.query("select si_name from ci_infoobjects where si_kind='Webi' and si_instance=0 and si_name='efashiontest2'");
//            IInfoObject report = (IInfoObject) results.get(0);
//            ISecurityInfo2 securityInfo2 = report.getSecurityInfo2();
//            IEffectivePrincipals effectivePrincipals = securityInfo2.getEffectivePrincipals();
//            System.out.println("effectivePrincipals.size: " + effectivePrincipals.size());
//            Iterator iterator = effectivePrincipals.iterator();
//            while (iterator.hasNext()) {
//                IEffectivePrincipal effectivePrincipal = (IEffectivePrincipal) iterator.next();
//                System.out.println("effectivePrincipal = " + effectivePrincipal + ", name: " + effectivePrincipal.getName() + ", id:" + effectivePrincipal.getID());
////                IEffectiveLimits limits = effectivePrincipal.getLimits();
////                IEffectiveRights rights = effectivePrincipal.getRights();
//                IEffectiveRoles roles = effectivePrincipal.getRoles();
////                Iterator iteratorlimits = limits.iterator();
////                Iterator iteratorrights = rights.iterator();
//                Iterator iteratorroles = roles.iterator();
//
////                while (iteratorlimits.hasNext()) {
////                     IEffectiveLimit effectiveLimit = (IEffectiveLimit)iteratorlimits.next();
////                     System.out.println("effectiveLimit.getDescription(java.util.Locale.ENGLISH): " + effectiveLimit.getDescription(java.util.Locale.ENGLISH) + ", effectiveLimit.getValue(): " + effectiveLimit.getValue());
////                }
////                while (iteratorrights.hasNext()) {
////                    IEffectiveRight effectiveRight = (IEffectiveRight)iteratorrights.next();
////                    System.out.println("effectiveRight.getDescription(java.util.Locale.ENGLISH): " + effectiveRight.getDescription(java.util.Locale.ENGLISH) + ", effectiveRight.isInherited(): " + effectiveRight.isInherited());
////                }
//                while (iteratorroles.hasNext()) {
//                    IEffectiveRole effectiveRole = (IEffectiveRole) iteratorroles.next();
//                    System.out.println("effectiveRole.getTitle(): " + effectiveRole.getTitle() + ", effectiveRole.isInherited(): " + effectiveRole.isInherited() + ", effectiveRole.toString(): " + effectiveRole.toString() + ", effectiveRole.getID():" + effectiveRole.getID() + ", effectiveRole.getSources().size(): " + effectiveRole.getSources().size());
//                }
//            }
//            IEffectivePrincipal anyPrincipal = securityInfo2.getAnyPrincipal(106010);
//            System.out.println("result: " + anyPrincipal);
//            IEffectiveRights rights = anyPrincipal.getRights();
//            for (Iterator it = rights.iterator(); it.hasNext();) {
//                IEffectiveRight effectiveRight = (IEffectiveRight) it.next();
//                System.out.println("effectiveRight.toString(): " + effectiveRight.toString() + ", effectiveRight.getBaseID(): " + effectiveRight.getBaseID() + ", effectiveRight.getRightPluginType(): " + effectiveRight.getRightPluginType() + ", effectiveRight.getApplicableKind(): " + effectiveRight.getApplicableKind() + ", effectiveRight.getRightPluginKind():" + effectiveRight.getRightPluginKind() + ", effectiveRight.getScope(): " + effectiveRight.getScope() + ", effectiveRight.isGranted(): " + effectiveRight.isGranted() + "effectiveRight.isOwner(): " + effectiveRight.isOwner());
//            }
//
//            IPluginBasedRightIDs pluginBasedRightIDs = securityInfo2.getKnownRightsByPlugin();
//
//            java.util.Map pluginRights = pluginBasedRightIDs.getPluginRights();
//            java.util.Iterator it = pluginRights.keySet().iterator();
//
//            while (it.hasNext()) {
//                Object progID = it.next();
////                System.out.println("                      Plugin Type: " + progID.toString());
//
//                java.util.Set rightIDs = (java.util.Set) pluginRights.get(progID);
//                java.util.Iterator rIt = rightIDs.iterator();
//
//                while (rIt.hasNext()) {
//                    IRightID key = (IRightID) rIt.next();
////                    System.out.println("Right: " + key.toString() + ", id: " + key.getBaseID());
////                    RightDescriptor rightToCheck = new RightDescriptor(key.getBaseID(), key.getRightPluginKind(), key.isOwner(), CeSecurityOptions.RightScope.CURRENT_OBJECT, progID);
//                    RightDescriptor rightToCheck = new RightDescriptor(CeSecurityID.Right.VIEW, "", false, CeSecurityOptions.RightScope.CURRENT_OBJECT, CeSecurityOptions.ANY_OBJTYPE);
//                    System.out.println("" + securityInfo2.checkRight(rightToCheck, 106010, false));
////                    CeSecurityID.Right.VIEW
//                }
//            }
//            enterpriseSession.logoff();
//            System.out.println("Koniec");
//        } catch (Exception ex) {
//            System.out.println("Błąd: " + ex.getMessage());
//            throw new LameRuntimeException(ex.getMessage());
//        }
//    }
}
