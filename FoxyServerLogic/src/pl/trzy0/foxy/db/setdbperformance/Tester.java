/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.db.setdbperformance;

import commonlib.GlobalThreadStopWatch;
import commonlib.LameUtils;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author pmielanczuk
 */
public class Tester {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    public static void performTestingSingle() {
        performTestingSingle(true);
    }

    public static void performTestingSingle(boolean enableGlobalStopWatch) {
        Scenario ss = new Scenario(
                //                new SetDbStep("MasterBIKS8"),
                //                new QueryStep(null, "\r\n    select name, val from mltx_app_prop where name in (\'multiXDbVer\')\r\n"),
                //                new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n"),
                //                new QueryStep(null, "\n    select * from mltx_database\n"),
                //                new SetDbStep("biks_en_8X7CSJ5B"),
                //                new QueryStep(null, "\r\n    select name, val from bik_app_prop where name in (\'bik_ver\',\'load_user_login\',\'load_user_pwd\')\r\n"),
                //                new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n"),
                //                new SetDbStep("biks_en_A3SXM621"),
                //                new QueryStep(null, "\r\n    select name, val from bik_app_prop where name in (\'bik_ver\',\'load_user_login\',\'load_user_pwd\')\r\n"),
                //                new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n"),
                //                new SetDbStep("biks_en_X6R6VR51"),
                //                new QueryStep(null, "\r\n    select name, val from bik_app_prop where name in (\'bik_ver\',\'load_user_login\',\'load_user_pwd\')\r\n"),
                //                new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n"),
                //                new SetDbStep("biks_pl_MM4Z1D2Z"),
                //                new QueryStep(null, "\r\n    select name, val from bik_app_prop where name in (\'bik_ver\',\'load_user_login\',\'load_user_pwd\')\r\n"),
                //                new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n"),
                //                new SetDbStep("biks_pl_UZURGSXX"),
                //                new QueryStep(null, "\r\n    select name, val from bik_app_prop where name in (\'bik_ver\',\'load_user_login\',\'load_user_pwd\')\r\n"),
                //                new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n"),
                //                new SetDbStep("biks_pl_WFSKVR75"),
                //                new QueryStep(null, "\r\n    select name, val from bik_app_prop where name in (\'bik_ver\',\'load_user_login\',\'load_user_pwd\')\r\n"),
                //                new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n")
                /* 18:34:36.316 */new SetDbStep("MasterBIKS8"),
                /* 18:34:36.318 */ new ExecCmdStep("\r\nif object_id(\'dbau_process\') is null\r\ncreate table dbau_process (\r\n  id int identity not null primary key, \r\n  date_added datetime not null default getdate(),\r\n  db_ver varchar(100) not null,\r\n  app_ver varchar(100) not null,\r\n  spid int default @@spid,\r\n  status int not null default 0 -- 0:in progress, -1:fail, 1:success, 2:fixed\r\n)\r\n\r\nif object_id(\'dbau_log\') is null\r\ncreate table dbau_log (\r\n  id int identity not null primary key,\r\n  date_added datetime not null default getdate(),\r\n  proc_id int not null references dbau_process (id),\r\n  msg varchar(max) not null,\r\n  spid int default @@spid\r\n)\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'final_db_ver\')\r\n  alter table dbau_process add final_db_ver varchar(100) null\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'init_db_ver\')\r\n  exec sp_rename \'dbau_process.db_ver\', \'init_db_ver\'\r\n"),
                /* 18:34:36.412 */ new QueryStep(null, "\r\n    select name, val from mltx_app_prop where name in (\'multiXDbVer\')\r\n"),
                //                /* 18:34:36.413 */ new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n"),
                //                /* 18:34:36.414 */ new QueryStep(null, "\n    select * from mltx_database\n"),
                /* 18:34:36.415 */ new SetDbStep("biks_en_8X7CSJ5B"),
                /* 18:34:37.363 */ new ExecCmdStep("\r\nif object_id(\'dbau_process\') is null\r\ncreate table dbau_process (\r\n  id int identity not null primary key, \r\n  date_added datetime not null default getdate(),\r\n  db_ver varchar(100) not null,\r\n  app_ver varchar(100) not null,\r\n  spid int default @@spid,\r\n  status int not null default 0 -- 0:in progress, -1:fail, 1:success, 2:fixed\r\n)\r\n\r\nif object_id(\'dbau_log\') is null\r\ncreate table dbau_log (\r\n  id int identity not null primary key,\r\n  date_added datetime not null default getdate(),\r\n  proc_id int not null references dbau_process (id),\r\n  msg varchar(max) not null,\r\n  spid int default @@spid\r\n)\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'final_db_ver\')\r\n  alter table dbau_process add final_db_ver varchar(100) null\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'init_db_ver\')\r\n  exec sp_rename \'dbau_process.db_ver\', \'init_db_ver\'\r\n"),
                /* 18:34:37.740 */ new QueryStep(null, "\r\n    select name, val from bik_app_prop where name in (\'bik_ver\',\'load_user_login\',\'load_user_pwd\')\r\n"),
                //                /* 18:34:37.827 */ new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n"),
                /* 18:34:37.870 */ new SetDbStep("biks_en_A3SXM621"),
                /* 18:34:38.661 */ new ExecCmdStep("\r\nif object_id(\'dbau_process\') is null\r\ncreate table dbau_process (\r\n  id int identity not null primary key, \r\n  date_added datetime not null default getdate(),\r\n  db_ver varchar(100) not null,\r\n  app_ver varchar(100) not null,\r\n  spid int default @@spid,\r\n  status int not null default 0 -- 0:in progress, -1:fail, 1:success, 2:fixed\r\n)\r\n\r\nif object_id(\'dbau_log\') is null\r\ncreate table dbau_log (\r\n  id int identity not null primary key,\r\n  date_added datetime not null default getdate(),\r\n  proc_id int not null references dbau_process (id),\r\n  msg varchar(max) not null,\r\n  spid int default @@spid\r\n)\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'final_db_ver\')\r\n  alter table dbau_process add final_db_ver varchar(100) null\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'init_db_ver\')\r\n  exec sp_rename \'dbau_process.db_ver\', \'init_db_ver\'\r\n"),
                /* 18:34:39.118 */ new QueryStep(null, "\r\n    select name, val from bik_app_prop where name in (\'bik_ver\',\'load_user_login\',\'load_user_pwd\')\r\n"),
                //                /* 18:34:39.296 */ new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n"),
                /* 18:34:39.364 */ new SetDbStep("biks_en_X6R6VR51"),
                /* 18:34:41.327 */ new ExecCmdStep("\r\nif object_id(\'dbau_process\') is null\r\ncreate table dbau_process (\r\n  id int identity not null primary key, \r\n  date_added datetime not null default getdate(),\r\n  db_ver varchar(100) not null,\r\n  app_ver varchar(100) not null,\r\n  spid int default @@spid,\r\n  status int not null default 0 -- 0:in progress, -1:fail, 1:success, 2:fixed\r\n)\r\n\r\nif object_id(\'dbau_log\') is null\r\ncreate table dbau_log (\r\n  id int identity not null primary key,\r\n  date_added datetime not null default getdate(),\r\n  proc_id int not null references dbau_process (id),\r\n  msg varchar(max) not null,\r\n  spid int default @@spid\r\n)\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'final_db_ver\')\r\n  alter table dbau_process add final_db_ver varchar(100) null\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'init_db_ver\')\r\n  exec sp_rename \'dbau_process.db_ver\', \'init_db_ver\'\r\n"),
                /* 18:34:41.801 */ new QueryStep(null, "\r\n    select name, val from bik_app_prop where name in (\'bik_ver\',\'load_user_login\',\'load_user_pwd\')\r\n"),
                //                /* 18:34:41.939 */ new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n"),
                /* 18:34:42.006 */ new SetDbStep("biks_pl_MM4Z1D2Z"),
                /* 18:34:42.368 */ new ExecCmdStep("\r\nif object_id(\'dbau_process\') is null\r\ncreate table dbau_process (\r\n  id int identity not null primary key, \r\n  date_added datetime not null default getdate(),\r\n  db_ver varchar(100) not null,\r\n  app_ver varchar(100) not null,\r\n  spid int default @@spid,\r\n  status int not null default 0 -- 0:in progress, -1:fail, 1:success, 2:fixed\r\n)\r\n\r\nif object_id(\'dbau_log\') is null\r\ncreate table dbau_log (\r\n  id int identity not null primary key,\r\n  date_added datetime not null default getdate(),\r\n  proc_id int not null references dbau_process (id),\r\n  msg varchar(max) not null,\r\n  spid int default @@spid\r\n)\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'final_db_ver\')\r\n  alter table dbau_process add final_db_ver varchar(100) null\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'init_db_ver\')\r\n  exec sp_rename \'dbau_process.db_ver\', \'init_db_ver\'\r\n"),
                /* 18:34:42.535 */ new QueryStep(null, "\r\n    select name, val from bik_app_prop where name in (\'bik_ver\',\'load_user_login\',\'load_user_pwd\')\r\n"),
                //                /* 18:34:42.599 */ new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n"),
                /* 18:34:42.612 */ new SetDbStep("biks_pl_UZURGSXX"),
                /* 18:34:42.924 */ new ExecCmdStep("\r\nif object_id(\'dbau_process\') is null\r\ncreate table dbau_process (\r\n  id int identity not null primary key, \r\n  date_added datetime not null default getdate(),\r\n  db_ver varchar(100) not null,\r\n  app_ver varchar(100) not null,\r\n  spid int default @@spid,\r\n  status int not null default 0 -- 0:in progress, -1:fail, 1:success, 2:fixed\r\n)\r\n\r\nif object_id(\'dbau_log\') is null\r\ncreate table dbau_log (\r\n  id int identity not null primary key,\r\n  date_added datetime not null default getdate(),\r\n  proc_id int not null references dbau_process (id),\r\n  msg varchar(max) not null,\r\n  spid int default @@spid\r\n)\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'final_db_ver\')\r\n  alter table dbau_process add final_db_ver varchar(100) null\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'init_db_ver\')\r\n  exec sp_rename \'dbau_process.db_ver\', \'init_db_ver\'\r\n"),
                /* 18:34:43.148 */ new QueryStep(null, "\r\n    select name, val from bik_app_prop where name in (\'bik_ver\',\'load_user_login\',\'load_user_pwd\')\r\n"),
                //                /* 18:34:43.220 */ new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n"),
                /* 18:34:43.238 */ new SetDbStep("biks_pl_WFSKVR75"),
                /* 18:34:43.520 */ new ExecCmdStep("\r\nif object_id(\'dbau_process\') is null\r\ncreate table dbau_process (\r\n  id int identity not null primary key, \r\n  date_added datetime not null default getdate(),\r\n  db_ver varchar(100) not null,\r\n  app_ver varchar(100) not null,\r\n  spid int default @@spid,\r\n  status int not null default 0 -- 0:in progress, -1:fail, 1:success, 2:fixed\r\n)\r\n\r\nif object_id(\'dbau_log\') is null\r\ncreate table dbau_log (\r\n  id int identity not null primary key,\r\n  date_added datetime not null default getdate(),\r\n  proc_id int not null references dbau_process (id),\r\n  msg varchar(max) not null,\r\n  spid int default @@spid\r\n)\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'final_db_ver\')\r\n  alter table dbau_process add final_db_ver varchar(100) null\r\n\r\nif not exists (select 1 from sys.columns where object_id = object_id(\'dbau_process\') and name = \'init_db_ver\')\r\n  exec sp_rename \'dbau_process.db_ver\', \'init_db_ver\'\r\n"),
                /* 18:34:43.747 */ new QueryStep(null, "\r\n    select name, val from bik_app_prop where name in (\'bik_ver\',\'load_user_login\',\'load_user_pwd\')\r\n")//,
                //                /* 18:34:43.816 */ new QueryStep(null, "\r\nselect top 1 *, (select max(date_added) from dbau_log where proc_id = dbau_process.id) as last_log_date\r\nfrom dbau_process\r\norder by id desc\r\n") //....
                /* ... */);
        boolean wasGlobalThreadStopWatchEnabled =
                GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled;
        GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled = enableGlobalStopWatch;
        try {
            DaoSingleton.setDatabase("MasterBIKS8");
            ss.run();
            DaoSingleton.finalizeTran(true);
        } finally {
            GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled = wasGlobalThreadStopWatchEnabled;
        }
    }

    public static void performTesting() {
        SetDbStep.fakeMode = false;

        logger.info("warming up...");
        DaoSingleton.setDatabase("MasterBIKS8");
        performTestingSingle(false);
        logger.info("proper run...");
        performTestingSingle(true);
        logger.info("proper run 2...");
        performTestingSingle(true);
        logger.info("done! totalRowCnt=" + QueryStep.totalRowCnt);
    }

    public static void main(String[] args) {
        LameLoggerFactory.setOneConfigEntry(GlobalThreadStopWatch.class, ILameLogger.LameLogLevel.Info);
        LameLoggerFactory.setOneConfigEntry(logger, ILameLogger.LameLogLevel.Info);
//        final QueryStep s1 = new QueryStep(null, "select top 20 * from sys.objects");
//        final QueryStep s2 = new QueryStep("MasterBIKS8", "select top 10 * from sys.objects");
//        final QueryStep s3 = new QueryStep("master", "select top 15 * from sys.objects");
//
//        Scenario s = new Scenario(s1, s1, s2, s2, s1, s1, s3, s3, s1, s1, s2, s1, s3, s1, s2, s1);
//        s.run();

        performTesting();
    }
}
