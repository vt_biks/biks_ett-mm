/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.glass;

import simplelib.BaseUtils;

/**
 *
 * @author witzar
 */

/* Generic Language-Agnostic Status String */
public class Glass {

    private static final String FIELD_PREFIX = "field.";
    private Object key;
    private Object[] args;
    private Boolean isForError;

    public Glass(String key) {
        this.key = key;
    }

    public Glass(Object key, Object... args) {
        this.key = key;
        this.args = args;
    }

    public Glass(String key, Boolean isForError) {
        this.key = key;
        this.isForError = isForError;
    }

    public Glass(Object key, Boolean isForError, Object... args) {
        this.key = key;
        this.isForError = isForError;
        this.args = args;
    }

    public Object getKey() {
        return key;
    }

    public int getArgCount() {
        return BaseUtils.arrLengthFix(args);
    }

    public Object getArg(int idx) {
        return args[idx];
    }

    public Object getArgOrDefVal(int idx, Object defVal) {
        return getArgCount() <= idx ? defVal : BaseUtils.nullToDef(args[idx], defVal);
    }

    public boolean isForError(boolean defVal) {
        return isForError == null ? defVal : isForError;
    }

    public boolean isForError() {
        return isForError(false);
    }

    public static Glass makeGlassEx(String key, Boolean isForError) {
        return new Glass(key, isForError);
    }

    public static Glass makeGlassEx(String key, Boolean isForError, Object... args) {
        return new Glass(key, isForError, args);
    }

    public static Glass makeSimpleGlass(String key, String subKey) {
        return new Glass(key, new Glass(subKey));
    }

    public static Glass makeSimpleGlassForField(String key, String fieldName) {
        return new Glass(key, new Glass(FIELD_PREFIX + fieldName));
    }

    public static Glass emptyField(String fieldKey) {
        return makeSimpleGlassForField("fldEmpty", fieldKey);
    }

    public static Glass tooShortField(String fieldKey, int length) {
        return makeGlassEx("fldTooShort", true, new Glass(FIELD_PREFIX + fieldKey), length);
    }

    public static Glass tooLongField(String fieldKey, int length) {
        return makeGlassEx("fldTooLong", true,  new Glass(FIELD_PREFIX + fieldKey), length);
    }

    public static Glass invalidField(String fieldKey) {
        return makeSimpleGlassForField("fldInvalid", fieldKey);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{{glass key=<");
        sb.append(key).append(">");

        if (args != null) {
            for (Object arg : args) {
                sb.append(" <").append(arg).append(">");
            }
        }

        sb.append("}}");
        return sb.toString();
    }
}
