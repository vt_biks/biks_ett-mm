/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons.caches;

import java.util.Collection;
import java.util.List;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import simplelib.BaseUtils;
import simplelib.INamedPropsBean;

/**
 *
 * @author wezyr
 */
public class FoxyBeanSupplier<K, V extends INamedPropsBean> 
        extends FoxyNamedPropsBeanObjectSupplierByManyBase<K, V>
        implements IFoxyObjectSupplier<K, V> {

    private IBeanConnectionEx<? super V> conn;
    private Class<V> beanClass;
    private String tableName;

    public FoxyBeanSupplier(IBeanConnectionEx<? super V> conn,
            Class<V> beanClass, String tableName, String primaryKeyName,
            String altKeyNames) {
        super(primaryKeyName, altKeyNames, null);
        this.conn = conn;
        this.beanClass = beanClass;
        this.tableName = tableName;
    }

    public static String standardPropNameToFieldName(String propName) {
        List<String> split = BaseUtils.splitByCapitals(propName);
        String res = BaseUtils.mergeWithSepEx(split, "_");
        return res;
    }

    protected String keyNameToFieldName(String keyName) {
        return standardPropNameToFieldName(keyName);
    }

    protected void postProcessOneBean(V bean) {
        // no-op
    }
    
    protected void postProcessBeansOneByOne(Collection<V> beans) {
        for (V bean : beans) {
            postProcessOneBean(bean);
        }
    }
    
    protected void postProcessBeans(Collection<V> beans) {
        // no-op
    }

    protected Collection<V> getMany(String keyName, Collection<?> keyVals) {
        String keysMerged = BeanConnectionUtils.mergeAsSqlStrings(conn, keyVals);
        String keyFieldName = keyNameToFieldName(keyName);

        Collection<V> beans = conn.createBeansFromQry(
                "select * from " + tableName +
                " where " + keyFieldName + " in (" +
                keysMerged + ")", beanClass);

        postProcessBeans(beans);
        
        return beans;
    }

    @Override
    protected Collection<V> internalGetManyByKeys(Collection<K> keys) {
        return getMany(primaryKeyName, keys);
    }

    @Override
    public Collection<V> getManyByAltKeyVals(String keyName, Collection<Object> vals) {
        return getMany(keyName, vals);
    }
}
