﻿exec sp_check_version '1.3.0.6';
go

alter table bik_schedule
add instance_name varchar(200) null;
go

create table bik_sapbo_server (
	id int not null identity(1,1) primary key,
	source_id int not null references bik_data_source_def(id),
	name varchar(200) null,
	server varchar(200) null,
	user_name varchar(200) null,
	password varchar(200) null,
	with_users int not null default(1),
	open_report_template varchar(200) null,
	check_rights int not null default(1),
	report_tree_id int not null references bik_tree(id),
	universe_tree_id int not null references bik_tree(id),
	connection_tree_id int not null references bik_tree(id),
	is_active int not null default(0),
	is_deleted int not null default(0)
);
go

declare @sourceId int, @server varchar(200), @user_name varchar(200), @password varchar(200), @withUsers int, @openReportTemplate varchar(200), @checkRights int, @reportId int, @universeId int, @connId int, @isActive int;
declare @name varchar(200) = 'SAP BO';
select @sourceId = id from bik_data_source_def where description = 'Business Objects'
select @server = value from bik_admin where param = 'sapbo.server'
select @user_name = value from bik_admin where param = 'sapbo.user'
select @password = value from bik_admin where param = 'sapbo.password'
select @withUsers = value from bik_admin where param = 'sapbo.withUsers'
select @openReportTemplate = value from bik_admin where param = 'sapbo.openReportTemplate'
select @checkRights = value from bik_admin where param = 'sapbo.checkRights'
select @reportId = id from bik_tree where code = 'Reports'
select @universeId = id from bik_tree where code = 'ObjectUniverses'
select @connId = id from bik_tree where code = 'Connections'
select @isActive = value from bik_admin where param = 'sapbo.isActive'

insert into bik_sapbo_server(source_id, name, server, user_name, password, with_users, open_report_template, check_rights, report_tree_id, universe_tree_id, connection_tree_id, is_active)
values (@sourceId, @name, @server, @user_name, @password, @withUsers, @openReportTemplate, @checkRights, @reportId, @universeId, @connId, @isActive)

update bik_schedule
set instance_name = @name
where source_id = @sourceId


alter table bik_sapbo_schedule
add constraint FK_bik_sapbo_schedule_node_id
foreign key (node_id)
references bik_node(id);
go

alter table bik_sapbo_query
add constraint FK_bik_sapbo_query_node_id
foreign key (node_id)
references bik_node(id);
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_create_bo_actual_instance_function]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_create_bo_actual_instance_function]
go

create procedure [dbo].[sp_create_bo_actual_instance_function](@serverId int)
as
begin

	declare @reportTreeId int, @universeTreeId int, @connectionTreeId int;
	select @reportTreeId = report_tree_id, @universeTreeId = universe_tree_id, @connectionTreeId = connection_tree_id from bik_sapbo_server where id = @serverId
    
    -- drop if exists
    exec('if exists (select * from sys.objects where object_id = OBJECT_ID(N''[dbo].[fn_get_bo_actual_report_tree_id]'') and type in (N''FN''))
			drop function [dbo].[fn_get_bo_actual_report_tree_id]');
	exec('if exists (select * from sys.objects where object_id = OBJECT_ID(N''[dbo].[fn_get_bo_actual_universe_tree_id]'') and type in (N''FN''))
			drop function [dbo].[fn_get_bo_actual_universe_tree_id]');
    exec('if exists (select * from sys.objects where object_id = OBJECT_ID(N''[dbo].[fn_get_bo_actual_connection_tree_id]'') and type in (N''FN''))
			drop function [dbo].[fn_get_bo_actual_connection_tree_id]');
    exec('if exists (select * from sys.objects where object_id = OBJECT_ID(N''[dbo].[fn_get_bo_actual_server_id]'') and type in (N''FN''))
			drop function [dbo].[fn_get_bo_actual_server_id]');
	-- create
	exec('create function fn_get_bo_actual_report_tree_id()
		returns int
		as
		begin
			return ' + @reportTreeId + ';
		end')
	exec('create function fn_get_bo_actual_universe_tree_id()
		returns int
		as
		begin
			return ' + @universeTreeId + ';
		end')
	exec('create function fn_get_bo_actual_connection_tree_id()
		returns int
		as
		begin
			return ' + @connectionTreeId + ';
		end')
	exec('create function fn_get_bo_actual_server_id()
		returns int
		as
		begin
			return ' + @serverId + ';
		end')
end;
go


alter table bik_sapbo_user
add server_instance_id int null;
go

declare @BoServer int;
select @BoServer = a.id from bik_sapbo_server a 
inner join bik_data_source_def def on a.source_id = def.id where def.description = 'Business Objects'

update bik_sapbo_user
set server_instance_id = @BoServer

exec sp_create_bo_actual_instance_function @BoServer

alter table bik_sapbo_user
alter column server_instance_id int not null; 
go

alter table bik_sapbo_user
add constraint FK_bik_sapbo_user_server_id
foreign key (server_instance_id)
references bik_sapbo_server(id);
go

-- poprzednia wersja w: alter db for v1.2.3.3 tf.sql
-- dodanie instancyjności BO
if exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
as
begin
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();

	delete from bik_sapbo_extradata
	from bik_sapbo_extradata
	inner join bik_node bn on bn.id = bik_sapbo_extradata.node_id
	where bn.tree_id in (@reportTreeId , @universeTreeId, @connectionTreeId)
	
	
	declare @tempTable table (
		id int identity(1,1) not null,
		column_name varchar(50) not null,
		table_name varchar(50) not null
	);
	
	-- ważna jest kolejnosc wstawiania
	insert into @tempTable(column_name,table_name)
	values
		('SI_AUTHOR','INFO_WEBI'),
		('SI_OWNER','aaa_global_props'),
		('SI_KEYWORD','aaa_global_props'),
		('SI_FILES__SI_PATH','aaa_global_props'),
		('SI_FILES__SI_FILE1','aaa_global_props'),
		('SI_CREATION_TIME','aaa_global_props'),
		('SI_UPDATE_TS','aaa_global_props'),
		('SI_CUID','aaa_global_props'),
		('SI_FILES__SI_VALUE1','aaa_global_props'),
		('SI_SIZE','aaa_global_props'),
		('SI_HAS_CHILDREN','aaa_global_props'),
		('SI_GUID','aaa_global_props'),
		('SI_INSTANCE','aaa_global_props'),
		('SI_OWNERID','aaa_global_props'),
		('SI_PROGID','aaa_global_props'),
		('SI_OBTYPE','aaa_global_props'),
		('SI_FLAGS','aaa_global_props'),
		('SI_CHILDREN','aaa_global_props'),
		('SI_RUID','aaa_global_props'),
		('SI_RUNNABLE_OBJECT','aaa_global_props'),
		('SI_CONTENT_LOCALE','aaa_global_props'),
		('SI_IS_SCHEDULABLE','aaa_global_props'),
		('SI_WEBI_DOC_PROPERTIES','aaa_global_props'),
		('SI_READ_ONLY','aaa_global_props'),
		('SI_LAST_SUCCESSFUL_INSTANCE_ID','aaa_global_props'),
		('SI_LAST_RUN_TIME','aaa_global_props'),
		('SI_TIMESTAMP','INFO_WEBI'),
		('SI_PROGID_MACHINE','INFO_WEBI'),
		('SI_ENDTIME','aaa_global_props'),
		('SI_STARTTIME','aaa_global_props'),
		('SI_SCHEDULE_STATUS','aaa_global_props'),
		('SI_RECURRING','aaa_global_props'),
		('SI_NEXTRUNTIME','aaa_global_props'),
		('SI_DOC_SENDER','aaa_global_props'),
		('SI_REVISIONNUM','APP_UNIVERSE'),
		('SI_APPLICATION_OBJECT','APP_UNIVERSE'),
		('SI_SHORTNAME','APP_UNIVERSE'),
		('SI_DATACONNECTION__SI_TOTAL','APP_UNIVERSE'),
		('SI_UNIVERSE__SI_TOTAL','INFO_WEBI')
		
	declare @sqlText varchar(max);
	declare @count int;
	select @count = count(*) from @tempTable
	
	set @sqlText = 'insert into bik_sapbo_extradata (node_id,author,owner,keyword,file_path,file_name,created,modified,cuid,files_value,size,has_children,guid,instance,owner_id,progid,obtype,flags,children,ruid,runnable_object,content_locale,is_schedulable,webi_doc_properties,read_only,last_successful_instance_id,last_run_time,timestamp,progid_machine,endtime,starttime,schedule_status,recurring,nextruntime,doc_sender,revisionnum,application_object,shortname,dataconnection_total,universe_total )
    select bik.id as node_id, '
		
	declare kurs cursor for 
	select column_name, table_name from @tempTable order by id 

	open kurs;
	declare @column varchar(50);
	declare @table varchar(50);
	declare @i int = 1;

	fetch next from kurs into @column, @table;
	while @@fetch_status = 0
	begin
		if exists(select * from sys.columns where name = @column and Object_ID = Object_ID(@table))    
		begin
			set @sqlText = @sqlText + @table + '.' + @column
		end
		else
		begin
			set @sqlText = @sqlText + 'null'
		end
		if(@i <> @count)
		begin
			set @sqlText = @sqlText + ', '
		end
		--
		set @i = @i + 1;
		fetch next from kurs into @column, @table;
	end
	close kurs;
	deallocate kurs;	

	set @sqlText = @sqlText + ' from aaa_global_props
    left join INFO_WEBI on aaa_global_props.si_id = INFO_WEBI.si_id
    left join APP_UNIVERSE on aaa_global_props.si_id = APP_UNIVERSE.si_id
    inner join bik_node bik on convert(varchar(30),aaa_global_props.si_id) = bik.obj_id
    where bik.tree_id in (' + convert(varchar(10),@reportTreeId) + ', ' + convert(varchar(10),@universeTreeId) + ', ' + convert(varchar(10),@connectionTreeId) + ') 
    and bik.is_deleted = 0
    and bik.linked_node_id is null
    order by bik.id'
    
    exec(@sqlText);
	
	/*insert into bik_sapbo_extradata (node_id,author,owner,keyword,file_path,file_name,created,modified,cuid,files_value,size,has_children,guid,instance,owner_id,progid,obtype,flags,children,ruid,runnable_object,content_locale,is_schedulable,webi_doc_properties,read_only,last_successful_instance_id,last_run_time,timestamp,progid_machine,endtime,starttime,schedule_status,recurring,nextruntime,doc_sender,revisionnum,application_object,shortname,dataconnection_total,universe_total )
    select bik.id as node_id,
    nullif(ltrim(rtrim(iw.SI_AUTHOR)), '') as author,
    nullif(ltrim(rtrim(p.SI_OWNER)), '') as owner,
    nullif(ltrim(rtrim(p.SI_KEYWORD)), '') as keyword,
    nullif(ltrim(rtrim(p.SI_FILES__SI_PATH)), '') as file_path,
    nullif(ltrim(rtrim(p.SI_FILES__SI_FILE1)), '') as file_name,
    nullif(ltrim(rtrim(p.SI_CREATION_TIME)), '') as created,
    nullif(ltrim(rtrim(p.SI_UPDATE_TS)), '') as modified,
    nullif(ltrim(rtrim(p.SI_CUID)), '') as cuid,
    nullif(ltrim(rtrim(p.SI_FILES__SI_VALUE1)), '') as files_value,
    nullif(ltrim(rtrim(p.SI_SIZE)), '') as size,
    p.SI_HAS_CHILDREN as has_children,
    nullif(ltrim(rtrim(p.SI_GUID)), '') as guid,
    p.SI_INSTANCE as instance,
    p.SI_OWNERID as owner_id,
    nullif(ltrim(rtrim(p.SI_PROGID)), '') as progid,
    p.SI_OBTYPE as obtype,
    p.SI_FLAGS as flags,
    p.SI_CHILDREN as children,
    nullif(ltrim(rtrim(p.SI_RUID)), '') as ruid,
    p.SI_RUNNABLE_OBJECT as runnable_object,
    nullif(ltrim(rtrim(p.SI_CONTENT_LOCALE)), '') as content_locale,
    p.SI_IS_SCHEDULABLE as is_schedulable,
    nullif(ltrim(rtrim(p.SI_WEBI_DOC_PROPERTIES)), '') as webi_doc_properties,
    p.SI_READ_ONLY as read_only,
    nullif(ltrim(rtrim(p.SI_LAST_SUCCESSFUL_INSTANCE_ID)), '') as last_successful_instance_id,
    nullif(ltrim(rtrim(p.SI_LAST_RUN_TIME)), '') as last_run_time,
    nullif(ltrim(rtrim(iw.SI_TIMESTAMP)), '')  as timestamp,
    nullif(ltrim(rtrim(iw.SI_PROGID_MACHINE)), '') as progid_machine,
    nullif(ltrim(rtrim(p.SI_ENDTIME)), '') as endtime,
    nullif(ltrim(rtrim(p.SI_STARTTIME)), '') as starttime,
    nullif(ltrim(rtrim(p.SI_SCHEDULE_STATUS)), '') as schedule_status,
    nullif(ltrim(rtrim(p.SI_RECURRING)), '') as recurring,
    nullif(ltrim(rtrim(p.SI_NEXTRUNTIME)), '') as nextruntime,
    nullif(ltrim(rtrim(p.SI_DOC_SENDER)), '') as doc_sender,
    au.SI_REVISIONNUM as revisionnum,
    au.SI_APPLICATION_OBJECT as application_object,
    nullif(ltrim(rtrim(au.SI_SHORTNAME)), '') as shortname,
    au.SI_DATACONNECTION__SI_TOTAL as dataconnection_total,
    iw.SI_UNIVERSE__SI_TOTAL as universe_total
    from aaa_global_props p
    left join INFO_WEBI iw on p.si_id = iw.si_id
    left join APP_UNIVERSE au on p.si_id = au.si_id
    inner join bik_node bik on convert(varchar(30),p.si_id) = bik.obj_id
    where bik.tree_id in (@reportTreeId, @universeTreeId, @connectionTreeId)
    and bik.is_deleted = 0
    and bik.linked_node_id is null 
    order by bik.id*/
    
    delete from bik_sapbo_schedule
    from bik_sapbo_schedule
	inner join bik_node bn on bn.id = bik_sapbo_schedule.node_id
	where bn.tree_id = @reportTreeId
    
    insert into bik_sapbo_schedule(node_id,destination,owner,creation_time,nextruntime,expire,type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters)
    select bn.id,destination,owner,creation_time,nextruntime,expire,type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters 
    from aaa_report_schedule sch
    inner join bik_node bn on convert(varchar(30),sch.id) = bn.obj_id
    --inner join bik_tree bt on bt.id = bn.tree_id
    where bn.tree_id = @reportTreeId 
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    --and bt.code in ('Reports')
    
end;
go


-- poprzednia wersja w: alter db for v1.1.8.6 tf.sql
-- fix pod instacje BO
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_delete_bik_joined_objs_by_kinds_fast]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_delete_bik_joined_objs_by_kinds_fast
go

create procedure sp_delete_bik_joined_objs_by_kinds_fast @kinds_one_str varchar(max), @kinds_two_str varchar(max) as
begin
  set nocount on

  declare @timestamp_start_total datetime = current_timestamp
  declare @diags_level int = 0 -- wartość 0 oznacza brak logowania, 1 i więcej - jest logowanie
  declare @rc int
  declare @timestamp_start datetime 
  declare @timestamp_end datetime
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: start' +
      ', @kinds_one_str = ' + @kinds_one_str + 
      ', @kinds_two_str = ' + @kinds_two_str
  end

  -- declare @kinds_one varchar(max) = 'Universe', @kinds_two varchar(max) = 'TeradataSchema'
  declare @codes_one table (code varchar(255) not null primary key)
  declare @codes_two table (code varchar(255) not null primary key)

  insert into @codes_one (code)
  select distinct str
  from dbo.fn_split_by_sep(@kinds_one_str, ',', 7)

  insert into @codes_two (code)
  select distinct str
  from dbo.fn_split_by_sep(@kinds_two_str, ',', 7)

  declare @kind_one_id table (id int not null primary key)
  declare @kind_two_id table (id int not null primary key)

  insert into @kind_one_id (id)
  select distinct k.id
  from @codes_one x inner join bik_node_kind k on x.code = k.code
  where substring(x.code, 1, 1) <> '@'
  
  insert into @kind_two_id (id)
  select distinct k.id
  from @codes_two x inner join bik_node_kind k on x.code = k.code
  where substring(x.code, 1, 1) <> '@'

  declare @tree_one_id table (id int not null primary key)
  declare @tree_two_id table (id int not null primary key)

  delete from @codes_one where substring(code, 1, 1) <> '@'
  delete from @codes_two where substring(code, 1, 1) <> '@'

  update @codes_one set code = substring(code, 2, len(code))
  update @codes_two set code = substring(code, 2, len(code))

  -- fix tree codes
  declare @reportTreeCode varchar(255), @universeTreeCode varchar(255), @connectionTreeCode varchar(255);
  select @reportTreeCode = code from bik_tree where id = dbo.fn_get_bo_actual_report_tree_id()
  select @universeTreeCode = code from bik_tree where id = dbo.fn_get_bo_actual_universe_tree_id()  
  select @connectionTreeCode = code from bik_tree where id = dbo.fn_get_bo_actual_connection_tree_id()
  
  update @codes_one
  set code = @reportTreeCode
  where code = 'Reports'
  
  update @codes_one
  set code = @universeTreeCode
  where code = 'ObjectUniverses'
  
  update @codes_one
  set code = @connectionTreeCode
  where code = 'Connections'
  
  update @codes_two
  set code = @reportTreeCode
  where code = 'Reports'
  
  update @codes_two
  set code = @universeTreeCode
  where code = 'ObjectUniverses'
  
  update @codes_two
  set code = @connectionTreeCode
  where code = 'Connections'  
  -- end fix


  insert into @tree_one_id (id)
  select distinct t.id
  from @codes_one x inner join bik_tree t on x.code = t.code

  insert into @tree_two_id (id)
  select distinct t.id
  from @codes_two x inner join bik_tree t on x.code = t.code

  declare @tree_id_to_kind_id table (tree_id int not null, node_kind_id int not null)
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before insert into @tree_id_to_kind_id'
  end

  set @timestamp_start = current_timestamp
  
  insert into @tree_id_to_kind_id (tree_id, node_kind_id)
  select distinct tree_id, node_kind_id
  from bik_node where is_deleted = 0 and linked_node_id is null

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after insert into @tree_id_to_kind_id, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end

  insert into @kind_one_id (id)
  select t2k.node_kind_id
  from @tree_one_id t inner join @tree_id_to_kind_id t2k on t.id = t2k.tree_id
  left join @kind_one_id k on k.id = t2k.node_kind_id
  where k.id is null

  insert into @kind_two_id (id)
  select t2k.node_kind_id
  from @tree_two_id t inner join @tree_id_to_kind_id t2k on t.id = t2k.tree_id
  left join @kind_two_id k on k.id = t2k.node_kind_id
  where k.id is null

  declare @joined_ids_to_del table (id int not null primary key)

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before insert into @joined_ids_to_del'
  end

  set @timestamp_start = current_timestamp
  
  declare @actualServerId int = dbo.fn_get_bo_actual_server_id();
  declare @actualSourceType int;
  select @actualSourceType = source_id from bik_sapbo_server where id = @actualServerId
  declare @otherInstanceBOTree table (
	 tree_id int not null
  );
  
  insert into @otherInstanceBOTree(tree_id)
  select report_tree_id from bik_sapbo_server
  where id <> @actualServerId
  and source_id = @actualSourceType
  union all
  select universe_tree_id from bik_sapbo_server
  where id <> @actualServerId
  and source_id = @actualSourceType
  union all
  select connection_tree_id from bik_sapbo_server
  where id <> @actualServerId
  and source_id = @actualSourceType  

  insert into @joined_ids_to_del (id)
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.src_node_id = n_one.id
  inner join bik_node n_two on jo.dst_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1
  and n_one.tree_id not in (select tree_id from @otherInstanceBOTree)
  and n_two.tree_id not in (select tree_id from @otherInstanceBOTree)
  union -- bez all, bo mogą być duplikaty, a chcemy je właśnie wyeliminować
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.dst_node_id = n_one.id
  inner join bik_node n_two on jo.src_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1
  and n_one.tree_id not in (select tree_id from @otherInstanceBOTree)
  and n_two.tree_id not in (select tree_id from @otherInstanceBOTree)

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after insert into @joined_ids_to_del, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end
  
  --select count(*) from @joined_ids_to_del

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before final delete'
  end

  set @timestamp_start = current_timestamp
  
  delete from bik_joined_objs where id in (select id from @joined_ids_to_del)
  
  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  declare @timestamp_end_total datetime = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after final delete, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
      + ', TOTAL TIME TAKEN: ' + cast(datediff(ms, @timestamp_start_total, @timestamp_end_total) / 1000.0 as varchar(20)) + ' s'
  end
end
go

-- poprzednia wersja w: alter db for v1.2.3.3 tf.sql
-- dostosowanie procedury do instancyjności BO
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node]
go

create procedure [dbo].[sp_insert_into_bik_node](@tree_code varchar(255))
as
	declare @table_folders varchar(255);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	declare @diag_level int = 0;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'

	create table #primaryIds(si_id varchar(5000) primary key);

	if(@tree_code!='Reports' and @tree_code!='Teradata')
	begin
		set @table_folders = ' APP_FOLDER ';
		if(@tree_code = 'Connections')
		begin
			set @kinds = '''MetaData.DataConnection''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_METADATA__DATACONNECTION
					where SI_KIND in (' + @kinds + ') and SI_OBJECT_IS_CONTAINER=0');			
		end;
		if(@tree_code = 'ObjectUniverses')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_UNIVERSE
					where SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_code='Reports')
	begin
		-- wybranie folderw raportów bez raportów użytkowników
		set @table_folders = ' INFO_FOLDER where si_name != ''~Webintelligence''';
		--set @table_folders = ' INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 is null or SI_PATH__SI_FOLDER_NAME2 != ''User Folders''';
		set @kinds = '''Webi'', ''Flash'', ''CrystalReport'',''Excel'',''FullClient'',''Pdf'',''Hyperlink''
		,''Rtf'',''Txt'',''Powerpoint'',''Word''';
	end;
	
    declare @sql nvarchar(max);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		--print cast(sysdatetime() as varchar(22)) + ': faza 4.1 - teradata przed sp_teradata_init_branch_names'
		--exec sp_teradata_init_branch_names;
		if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': tworzenie pierwszego @sql'
		set @sql = N'select branch_names as SI_ID, parent_branch_names	as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata';
	end;
	else
	begin
		if(@tree_code = 'Reports')
		-- sztuczny podzial na root folder i user folders dla raportów
		set @sql = N'select distinct SI_PARENT_FOLDER as SI_ID, (select si_id from INFO_FOLDER where SI_NAME=''User Folders'') as SI_PARENTID, SI_KIND, SI_PATH__SI_FOLDER_NAME1 as SI_NAME, NULL as DESCR
				 from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' 
				 and SI_NAME!=''~Webintelligence'' 

				 union all
				
				 select si_id, case
									when SI_PARENTID=0 and si_name!=''User Folders''
										then (select si_id from INFO_FOLDER where SI_NAME=''Root Folder'')
									when si_id in(select si_id from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 = ''User Folders'')
                                        then SI_PARENTID
									when SI_PARENTID in(select si_id from INFO_FOLDER)
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case
																			when SI_NAME=''Root Folder''
																				then ''Foldery korporacyjne''
																			when SI_NAME=''User Folders''
																				then ''Foldery użytkowników''
																			else SI_NAME end, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 								
				' union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') and SI_INSTANCE = 0
				union all
				
				select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR
				from aaa_global_props where si_parentid in (select si_parentid from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' and SI_NAME!=''~Webintelligence'') and si_kind!=''Folder''
				
				union all
				
				select id, parent_id, ''ReportSchedule'', name, null
				from aaa_report_schedule sch
				inner join aaa_global_props gp on gp.si_id=sch.parent_id
				';
		if(@tree_code = 'Connections')
			set @sql = N'select 1 as si_id, 0 as si_parentid, ''ConnectionFolder'' as si_kind, ''Połączenia'' as si_name, '''' as descr
				
				 union all

				 select SI_ID, 1, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from APP_METADATA__DATACONNECTION
				 where SI_KIND in ('+ @kinds +') and SI_OBJECT_IS_CONTAINER=0';

		if(@tree_code = 'ObjectUniverses')
			set @sql = N'select si_id, case 
									when SI_NAME=''Universes''
										then 0
									when SI_PARENTID in(select si_id from ' + @table_folders +') 
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case when SI_NAME=''Universes'' then ''Światy obiektów'' else SI_NAME end as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 
													
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +')';
	end;					

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed zapytaniem'
	print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(1000) primary key, si_parentid varchar(1000), si_kind varchar(max), si_name varchar(max), descr varchar(max));
	if(@tree_code = 'Reports')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ReportFolder'' 
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
		set @tree_id = dbo.fn_get_bo_actual_report_tree_id();
	end;
	if(@tree_code = 'Connections')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ConnectionFolder''
										when si_kind=''MetaData.DataConnection'' then ''DataConnection''
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
		set @tree_id = dbo.fn_get_bo_actual_connection_tree_id();
	end;
	if(@tree_code = 'ObjectUniverses')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''UniversesFolder'' else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
		set @tree_id = dbo.fn_get_bo_actual_universe_tree_id();
	end;
	if(@tree_code = 'Teradata')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, si_kind, si_name, DESCR
					from (' + @sql + ') xxx';
		select @tree_id = id from bik_tree where code = @tree_code;
	end;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po stworzeniu drugiego @sql'
			   
	EXECUTE(@sql);

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/

	declare @rc int = 1

	while @rc > 0 begin
	
	delete from #tmpNodes
	where (si_kind = 'UniversesFolder' or si_kind = 'ConnectionFolder' or si_kind = 'ReportFolder') and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/
	
	declare @report_tree_id int = dbo.fn_get_bo_actual_report_tree_id();

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updatetami'

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.descr, tree_id = @tree_id
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.tree_id = @tree_id and bik_node.obj_id = #tmpNodes.si_id and bik_node.linked_node_id is null
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed update podlinkowanych'
	--update podlinkowanych
	update bik_node 
	set /*parent_node_id = null, */node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		 descr = #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and (tree_id = @report_tree_id or tree_id in (select id from bik_tree where tree_kind in (select code from bik_tree_kind where allow_linking = 1))) and bik_node.linked_node_id is not null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed insertami'
	
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id
	where bik_node.id is null;

	set nocount on;

	drop table #primaryIds;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updateami parentow'
	

	update bik_node 
	set parent_node_id = bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id
	inner join bik_node bk on bk.tree_id = @tree_id and bk.obj_id = pt.si_parentid where bik_node.linked_node_id is null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed usuwaniem'
	
	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in 
	('Measure', 'Dimension', 'Detail', 'UniverseClass', 'ReportQuery', 'Filter', 'UniverseColumn', 'UniverseTable', 'UniverseAliasTable', 'UniverseDerivedTable', 'UniverseTablesFolder', 'ConnectionEngineFolder', 'ConnectionNetworkFolder')
				
	drop table #tmpNodes
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po usuwaniu i po dropie'

	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': koniec'
end;
go

create table ami_app_universe (
	server_instance_id int not null references bik_sapbo_server(id),
	si_id int not null,
	si_name varchar(max) null,
	si_parent_folder_cuid varchar(max) null,
	si_update_ts datetime null,
	si_cuid varchar(max) null
);
go

create table ami_universe (
	server_instance_id int not null references bik_sapbo_server(id),
	id int not null,
	unique_id varchar(300) not null,
	name varchar(255) not null,
	description varchar(max) null,
	file_name varchar(255) not null,
	author varchar(120) null,
	universe_connetion_id int not null,
	path varchar(500) not null,
	global_props_si_id int null,
	statistic varchar(500) null,
	update_ts datetime null
);
go

create table ami_universe_class (
	server_instance_id int not null references bik_sapbo_server(id),
	id int not null,
	si_id int not null,
	parent_id int null,
	name varchar(255) not null,
	description varchar(max) null,
	universe_id int not null,
	si_id_designer varchar(1000) null,
	si_parentid varchar(1000) null
);
go

create table ami_universe_column (
	server_instance_id int not null references bik_sapbo_server(id),
	id int not null,
	universe_table_id int not null,
	name varchar(255) not null,
	type varchar(255) not null
);
go

create table ami_universe_connection (
	server_instance_id int not null references bik_sapbo_server(id),
	id int not null,
	connetion_name varchar(1000) not null,
	server varchar(255) null,
	user_name varchar(255) null,
	password varchar(255) null,
	database_source varchar(500) null,
	connetion_networklayer_id int not null,
	type varchar(255) null,
	database_enigme varchar(255) null,
	client_number int null,
	language varchar(10) null,
	system_number varchar(255) null,
	system_id varchar(255) null
);
go

create table ami_universe_connection_networklayer (
	server_instance_id int not null references bik_sapbo_server(id),
	id int not null,
	name varchar(255) not null
);
go

create table ami_universe_delta (
	server_instance_id int not null references bik_sapbo_server(id),
	si_id int null,
	name varchar(max) null,
	folderCUID varchar(max) null,
	to_delete int null,
	to_load int null
);
go

create table ami_universe_filter (
	server_instance_id int not null references bik_sapbo_server(id),
	id int not null,
	si_id int not null,
	name varchar(255) not null,
	description varchar(max) null,
	where_clause varchar(max) null,
	type varchar(255) not null,
	universe_class_id int null,
	filtr_branch varchar(1000) null
);
go

create table ami_universe_obj (
	server_instance_id int not null references bik_sapbo_server(id),
	id int not null,
	name varchar(255) not null,
	description varchar(max) null,
	text_of_select varchar(max) null,
	type varchar(255) not null,
	universe_class_id int null,
	si_id int not null,
	parent_id int null,
	qualification varchar(50) null,
	aggregate_function varchar(50) null,
	text_of_where varchar(max) null,
	obj_branch varchar(1000) null,
	si_parentid varchar(1000) null
);
go

create table ami_universe_obj_tables (
	server_instance_id int not null references bik_sapbo_server(id),
	id int not null,
	universe_table_id int not null,
	universe_obj_id int not null,
	obj_type int null
);
go

create table ami_universe_table (
	server_instance_id int not null references bik_sapbo_server(id),
	id int not null,
	their_id int not null,
	name varchar(255) not null,
	is_alias int not null,
	is_derived int not null,
	original_table int null,
	sql_of_derived_table varchar(max) null,
	sql_of_derived_table_with_alias varchar(max) null,
	universe_id int null,
	universe_branch varchar(1000) null
);
go

declare @BoServer int;
select @BoServer = a.id from bik_sapbo_server a 
inner join bik_data_source_def def on a.source_id = def.id where def.description = 'Business Objects'

-- napełnienie tabel obecnymi danymi
insert into ami_app_universe(server_instance_id, si_id, si_name, si_parent_folder_cuid, si_update_ts, si_cuid)
select @BoServer, si_id, si_name, si_parent_folder_cuid, si_update_ts, si_cuid from APP_UNIVERSE

insert into ami_universe(server_instance_id,id,unique_id,name,description,file_name,author,universe_connetion_id,path,global_props_si_id,statistic,update_ts)
select @BoServer, id,unique_id,name,description,file_name,author,universe_connetion_id,path,global_props_si_id,statistic,update_ts from aaa_universe

insert into ami_universe_class(server_instance_id,id,si_id,parent_id,name,description,universe_id,si_id_designer,si_parentid)
select @BoServer, id,si_id,parent_id,name,description,universe_id,si_id_designer,si_parentid from aaa_universe_class

insert into ami_universe_column(server_instance_id, id, universe_table_id, name, type)
select @BoServer, id, universe_table_id, name, type from aaa_universe_column

insert into ami_universe_connection(server_instance_id,id,connetion_name,server,user_name,password,database_source,connetion_networklayer_id,type,database_enigme,client_number,language,system_number,system_id)
select @BoServer, id,connetion_name,server,user_name,password,database_source,connetion_networklayer_id,type,database_enigme,client_number,language,system_number,system_id from aaa_universe_connection

insert into ami_universe_connection_networklayer(server_instance_id, id, name)
select @BoServer, id, name from aaa_universe_connection_networklayer

insert into ami_universe_delta(server_instance_id,si_id,name,folderCUID,to_delete,to_load)
select @BoServer, si_id,name,folderCUID,to_delete,to_load from aaa_universe_delta

insert into ami_universe_filter(server_instance_id,id,si_id,name,description,where_clause,type,universe_class_id,filtr_branch)
select @BoServer, id,si_id,name,description,where_clause,type,universe_class_id,filtr_branch from aaa_universe_filter

insert into ami_universe_obj(server_instance_id,id,name,description,text_of_select,type,universe_class_id,si_id,parent_id,qualification,aggregate_function,text_of_where,obj_branch,si_parentid)
select @BoServer,id,name,description,text_of_select,type,universe_class_id,si_id,parent_id,qualification,aggregate_function,text_of_where,obj_branch,si_parentid from aaa_universe_obj

insert into ami_universe_obj_tables(server_instance_id,id,universe_table_id,universe_obj_id,obj_type)
select @BoServer,id,universe_table_id,universe_obj_id,obj_type from aaa_universe_obj_tables

insert into ami_universe_table(server_instance_id,id,their_id,name,is_alias,is_derived,original_table,sql_of_derived_table,sql_of_derived_table_with_alias,universe_id,universe_branch)
select @BoServer,id,their_id,name,is_alias,is_derived,original_table,sql_of_derived_table,sql_of_derived_table_with_alias,universe_id,universe_branch from aaa_universe_table

-- /

-- poprzednia wersja w: alter db for v1.2.4.1 tf.sql
-- fix dla wieloinstancyjności
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_objects_from_Designer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_objects_from_Designer]
GO

create procedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]
as
begin--begin procedure

	declare @diag_level int = 0;

	----wybranie id odpowiedniego drzewa
	declare @tree_id int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	
	----wybranie id odpowiedniego kind'a
	declare @class_kind_id int = dbo.fn_node_kind_id_by_code('UniverseClass');
	declare @folder_universe_node_kind_id int = dbo.fn_node_kind_id_by_code('UniversesFolder');
	declare @detail_nki int = dbo.fn_node_kind_id_by_code('Detail');
	declare @dimension_nki int = dbo.fn_node_kind_id_by_code('Dimension');
	declare @measure_nki int = dbo.fn_node_kind_id_by_code('Measure');
	declare @filtr_nki int = dbo.fn_node_kind_id_by_code('Filter');
	declare @folder_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseTablesFolder');
	declare @table_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseTable');
	declare @table_alias_node_kind_id int =  dbo.fn_node_kind_id_by_code('UniverseAliasTable');
	declare @table_derived_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseDerivedTable');
	declare @universe_kind_id int = dbo.fn_node_kind_id_by_code('Universe');
	declare @universe_column_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseColumn');
		
	create table #tmpNodes (si_id varchar(1000), si_parentid varchar(1000), si_kind_id int, si_name varchar(max), si_description varchar(max));

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Wrzucanie do tableli tymczasowej #tmpNodes'
	
	----wrzucam do tymczasowej tabeli wszystkie dane z Universe_Class
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select si_id_designer, si_parentid, @class_kind_id, name, description
	from aaa_universe_class  
	
	
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Obj
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select obj_branch, si_parentid, 
			case
				when qualification = 'dsDetailObject' then @detail_nki
				when qualification = 'dsMeasureObject' then @measure_nki
				when qualification = 'dsDimensionObject' then @dimension_nki
				end as node_kind_id,
		 name, description
	from aaa_universe_obj
							 						 
	--wrzucam do tabeli tymczasowej wszystkie dane z Universe_filrt
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select uf.filtr_branch,	uc.si_id_designer, @filtr_nki, uf.name, nullif(ltrim(rtrim(uf.description)), '') 
	from aaa_universe_filter uf join aaa_universe_class uc on uf.universe_class_id = uc.id

	--wrzucam katalogi do których są wrzucane tabele
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)
	select convert(varchar(10),global_props_si_id) + '|tableCatalog', convert(varchar(10),global_props_si_id), @folder_node_kind_id, 'Tabele'
	from aaa_universe 

							 
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Table	
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)		
	select convert(varchar(10), their_id) + '|' + convert(varchar(1000), universe_branch), convert(varchar(1000), universe_branch) + '|tableCatalog', 
	case
				when is_alias = 1 then @table_alias_node_kind_id
				when is_derived = 1 then @table_derived_node_kind_id
				else @table_node_kind_id
				end as node_kind_id, name 
	from aaa_universe_table 
	
	
	----wrzucam do tymczasowej tabeli wszystkie dane z Universe_Column
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch) + '|' + uc.name, 
		convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch), @universe_column_node_kind_id, uc.name, REPLACE(REPLACE(uc.type,'ds',''),'Column','')
	from aaa_universe_table ut join aaa_universe_column uc on ut.id = uc.universe_table_id
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Update bik_node'
	
	--aktualizacja istniejących węzłów w drzewie							
	update bik_node 
	set parent_node_id = null, node_kind_id = #tmpNodes.si_kind_id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.si_description
	from #tmpNodes 
	where bik_node.tree_id = @tree_id and bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id and bik_node.linked_node_id is null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Insert into bik_node'
	
	--dorzucanie nowych węzłów w drzewie
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select #tmpNodes.si_kind_id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.si_description
	from #tmpNodes left join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where bik_node.id is null;	
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Update parentów'
	
	--uaktualnianie parentów w nodach
	update bik_node 
	set parent_node_id = bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.tree_id = @tree_id and bik_node.obj_id = pt.si_id and bik_node.node_kind_id = pt.si_kind_id
		inner join bik_node bk on bk.tree_id = @tree_id and bk.obj_id = pt.si_parentid
		and  (
		 (pt.si_kind_id=@detail_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @class_kind_id)) or
		 (pt.si_kind_id=@dimension_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @class_kind_id)) or
		 (pt.si_kind_id=@measure_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @class_kind_id)) or
		 (pt.si_kind_id=@filtr_nki and bk.node_kind_id=@class_kind_id) or
		 (pt.si_kind_id=@universe_column_node_kind_id and (bk.node_kind_id=@table_node_kind_id or bk.node_kind_id=@table_alias_node_kind_id or bk.node_kind_id=@table_derived_node_kind_id)) or
		 (pt.si_kind_id=@class_kind_id and (bk.node_kind_id = @class_kind_id or bk.node_kind_id = @universe_kind_id))or
		 ((pt.si_kind_id=@table_node_kind_id or pt.si_kind_id=@table_alias_node_kind_id or pt.si_kind_id=@table_derived_node_kind_id) and bk.node_kind_id=@folder_node_kind_id)or
		 (pt.si_kind_id=@folder_node_kind_id and bk.node_kind_id=@universe_kind_id)
		 )
	where bk.linked_node_id is null and bik_node.parent_node_id is null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Usuwanie nodów'
		
	--usuwanie nodów
	update bik_node
	set is_deleted = 1
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and bik_node.node_kind_id in (@class_kind_id, @detail_nki, @dimension_nki, @measure_nki, -- do usuniecia tabele i kolumny i folder tabel
	  @filtr_nki);
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Usuwanie błędnych danych w Universum'
	
	-- usuwanie obiektow, wrzuconych ze zlym parentem --> blad w Universum  
	update bik_node
	set is_deleted = 1
	where parent_node_id is null and tree_id = @tree_id and node_kind_id not in (@universe_kind_id, @folder_universe_node_kind_id)
	
	drop table #tmpNodes;
	--uzupełnianie danych w tabelach extra 
	--dla object
	create table #tmpObjExtra(text_of_select varchar(max),text_of_where varchar(max), type varchar(155), aggregate_function varchar(50), node_id int)
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Wrzucanie do tableli tymczasowej #tmpObjExtra'
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select univ_obj.text_of_select, univ_obj.text_of_where, univ_obj.type, univ_obj.aggregate_function, bik_node.id
	from aaa_universe_obj univ_obj join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = univ_obj.obj_branch 
		and bik_node.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)-- = @obj_node_kind_id;
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select null, uf.where_clause, uf.type, null, bik_node.id
	from aaa_universe_filter uf join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = uf.filtr_branch and bik_node.node_kind_id = @filtr_nki
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Wrzucanie do tabel bik_sapbo...'
	
	update bik_sapbo_universe_object
	set text_of_select = #tmpObjExtra.text_of_select, text_of_where = #tmpObjExtra.text_of_where, type = #tmpObjExtra.type,  
		           aggregate_function = #tmpObjExtra.aggregate_function 
	from #tmpObjExtra join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id;
	
	insert into bik_sapbo_universe_object(text_of_select, text_of_where, type, aggregate_function, node_id)
	select #tmpObjExtra.text_of_select,#tmpObjExtra.text_of_where, #tmpObjExtra.type, #tmpObjExtra.aggregate_function, #tmpObjExtra.node_id 
	from #tmpObjExtra left join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id
	where bik_sapbo_universe_object.id is null;

	drop table #tmpObjExtra;
	
	--dla tabeli
	
	update bik_sapbo_universe_table
	set is_alias = ut.is_alias, is_derived = ut.is_derived, original_table = ut.original_table, sql_of_derived_table = ut.sql_of_derived_table, 
		sql_of_derived_table_with_alias = ut.sql_of_derived_table_with_alias, their_id = ut.their_id, node_id = bik_node.id, type = ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
		/*left*/ join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
		join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
	where bik_node.linked_node_id is null and bik_node.is_deleted = 0 and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	
	insert into bik_sapbo_universe_table(is_alias, is_derived, original_table, sql_of_derived_table, sql_of_derived_table_with_alias, their_id, node_id, type)
	select ut.is_alias, ut.is_derived, ut.original_table, ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, ut.their_id, 
					bik_node.id as node_id, ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			/*left*/ join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
	where  bik_node.linked_node_id is null and bsut.id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)

	--uzupełnianie name_for_teradata: Teradata i Oracle
	update bik_sapbo_universe_table
	set name_for_teradata = replace(bik_node.name,'.', '|') + '|'
	from bik_sapbo_universe_table 
	inner join bik_node on bik_sapbo_universe_table.node_id = bik_node.id
	where bik_sapbo_universe_table.type in ('Teradata', 'ODBC', 'Oracle OCI') and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 0

	update bik_sapbo_universe_table
	set name_for_teradata = tabo.name_for_teradata
	from bik_sapbo_universe_table 
	inner join bik_sapbo_universe_table tabo on bik_sapbo_universe_table.original_table = tabo.their_id 
	inner join bik_node bn on bn.id = bik_sapbo_universe_table.node_id
	inner join bik_node bn2 on bn2.id = tabo.node_id
	where bik_sapbo_universe_table.type in ('Teradata', 'ODBC', 'Oracle OCI') and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 1 
	and bn2.parent_node_id = bn.parent_node_id
	
	update bik_sapbo_universe_table
	set schema_name = left(name_for_teradata, charindex('|',name_for_teradata))
	where type in ('Teradata', 'ODBC', 'Oracle OCI') and is_derived = 0
	
	--dla kolumn
	update bik_sapbo_universe_column
	set name = uc.name, type = uc.type, table_id = bik_node.id--bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			--join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
			join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bik_node.id--bsut.id
	where linked_node_id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	
	insert into bik_sapbo_universe_column(name, type, table_id)
	select uc.name, uc.type, bik_node.id--bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			--join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id =bik_node.id--bsut.id
	where linked_node_id is null and bsuc.id is null  and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)

	--delete from bik_sapbo_universe_table i bik_sapbo_universe_column nie jest potrzebne bo w bik_node jest ustawiont isDeleted na 1:)
	
	delete from bik_sapbo_universe_connection 
	from bik_sapbo_universe_connection conn
	inner join bik_node bn on bn.id = conn.node_id
	where bn.tree_id = @connectionTreeId
	
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id, type, database_engine, client_number, language, system_number, system_id)
	select aaa_universe_connection.server, aaa_universe_connection.user_name, aaa_universe_connection.password, aaa_universe_connection.database_source, 
		aaa_universe_connection_networklayer.name, bik_node.id,
		aaa_universe_connection.type, aaa_universe_connection.database_enigme, aaa_universe_connection.client_number, aaa_universe_connection.language,
		aaa_universe_connection.system_number, aaa_universe_connection.system_id
	from aaa_universe_connection inner join aaa_universe_connection_networklayer on aaa_universe_connection.connetion_networklayer_id = aaa_universe_connection_networklayer.id
		inner join bik_node on bik_node.tree_id = @connectionTreeId and aaa_universe_connection.connetion_name = bik_node.name
		left join bik_sapbo_universe_connection on bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_sapbo_universe_connection.id is null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Update extradaty'
	
	-- dodanie statystyk dla światów obiektów	
	update bik_sapbo_extradata
	set statistic = u.statistic
	from bik_sapbo_extradata join bik_node bn on bn.id = bik_sapbo_extradata.node_id
	inner join aaa_universe u on convert(varchar(30),u.global_props_si_id) = bn.obj_id
	where bn.node_kind_id = @universe_kind_id
	and bn.tree_id = @tree_id
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Ustawianie visual_order'
	
	-- ustawianie visual order dla folderów tabel
	update bik_node set visual_order = -1 where node_kind_id = @folder_node_kind_id
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Wykonywanie procedur: sp_delete_linked i sp_node_init_branch'
		
	exec sp_delete_linked_nodes_where_orignal_is_deleted;	
	exec sp_node_init_branch_id @tree_id, null;
end;
go


-- poprzednia wersja w pliku: alter db for v1.2.4.1 tf.sql
-- fix dla wieloinstancyjności
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbo_connections]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbo_connections]
as
begin

	create table #tmp_network (name varchar(max));
	create table #tmp_engine (db_engine varchar(max),network_layer varchar(max));
	create table #tmp_conn (conn_node_id int,parent_node_id int);

	declare @connTree int = dbo.fn_get_bo_actual_connection_tree_id();
	declare @connFolderKind int = dbo.fn_node_kind_id_by_code('ConnectionFolder');
	declare @connEngineFolderKind int = dbo.fn_node_kind_id_by_code('ConnectionEngineFolder');
	declare @connNetworkFolderKind int = dbo.fn_node_kind_id_by_code('ConnectionNetworkFolder');
	declare @parentNodeId int;
	select @parentNodeId = id from bik_node where obj_id = '1'/*name='Połączenia'*/ and tree_id = @connTree and node_kind_id = @connFolderKind and is_deleted = 0 and linked_node_id is null and parent_node_id is null

	-- wrzucenie network layers folders
	insert into #tmp_network(name)
	select distinct connetion_networklayer_name from bik_sapbo_universe_connection conn
	inner join bik_node bn on bn.id = conn.node_id
	where bn.tree_id = @connTree

	-- przywrocenie dobrych folderkow, usunietych po 1 fazie zasilania
	update bik_node
	set is_deleted = 0
	where tree_id = @connTree
	and is_deleted = 1
	and linked_node_id is null
	and node_kind_id = @connNetworkFolderKind
	and parent_node_id = @parentNodeId
	and name in (select name from #tmp_network)

	-- dodanie nowych, jeśli są nowe
	insert into bik_node (parent_node_id, node_kind_id, name, tree_id)
	select @parentNodeId,@connNetworkFolderKind, case when #tmp_network.name is null then 'Inne' else #tmp_network.name end, @connTree
	from #tmp_network 
	left join bik_node bn on bn.tree_id = @connTree and bn.is_deleted = 0 and bn.name = #tmp_network.name
	and bn.linked_node_id is null and bn.node_kind_id = @connNetworkFolderKind
	where bn.id is null;

	-- usuniecie starych/niepotrzebnych
	update bik_node
	set is_deleted = 1
	where tree_id = @connTree 
	and is_deleted = 0
	and node_kind_id = @connNetworkFolderKind
	and name not in (select name from #tmp_network)
	and linked_node_id is null

	-- wrzucenie DB engine folders
	insert into #tmp_engine(db_engine, network_layer)
	select distinct case when database_engine is null then 'Inne' else database_engine end,connetion_networklayer_name from bik_sapbo_universe_connection conn
	inner join bik_node bn on bn.id = conn.node_id
	where bn.tree_id = @connTree

	-- przywrocenie dobrych folderkow, usunietych po 1 fazie zasilania
	update bik_node
	set is_deleted = 0
	where tree_id = @connTree
	and is_deleted = 1
	and linked_node_id is null
	and node_kind_id = @connEngineFolderKind
	and parent_node_id in (select id from bik_node where tree_id = @connTree and is_deleted = 0 and linked_node_id is null and node_kind_id = @connNetworkFolderKind)
	and name in (select db_engine from #tmp_engine)

	-- dodanie nowych, jeśli są nowe
	insert into bik_node (parent_node_id, node_kind_id, name, tree_id)
	select (select id from bik_node where tree_id = @connTree and is_deleted = 0 and name = #tmp_engine.network_layer and linked_node_id is null and node_kind_id = @connNetworkFolderKind),@connEngineFolderKind, #tmp_engine.db_engine, @connTree
	from #tmp_engine 
	left join bik_node bn on bn.tree_id = @connTree and bn.is_deleted = 0 and bn.name = #tmp_engine.db_engine
	and bn.linked_node_id is null and bn.node_kind_id = @connEngineFolderKind
	and bn.parent_node_id = (select id from bik_node where tree_id = @connTree and is_deleted = 0 and name = #tmp_engine.network_layer and linked_node_id is null and node_kind_id = @connNetworkFolderKind)
	where bn.id is null;

	-- usuniecie starych/niepotrzebnych	
	update bik_node
	set is_deleted = 1
	where tree_id = @connTree 
	and is_deleted = 0 
	and node_kind_id = @connEngineFolderKind
	and name not in (select db_engine from #tmp_engine)
	and linked_node_id is null

	-- podłączenie połączeń pod odpowiednie foldery
	insert into #tmp_conn(conn_node_id,parent_node_id)
	select bsuc.node_id, bn.id 
	from bik_sapbo_universe_connection bsuc
	inner join bik_node nod on nod.id = bsuc.node_id and nod.tree_id = @connTree
	inner join bik_node bn on bn.tree_id = @connTree 
	and bn.is_deleted = 0
	and bn.name = case when bsuc.database_engine is null then 'Inne' else bsuc.database_engine end
	and bn.linked_node_id is null 
	and bn.node_kind_id = @connEngineFolderKind
	inner join bik_node bn2 on bn.parent_node_id = bn2.id
	and bn2.name = bsuc.connetion_networklayer_name

	update bik_node
	set parent_node_id = #tmp_conn.parent_node_id
	from #tmp_conn
	where bik_node.id = #tmp_conn.conn_node_id

	drop table #tmp_engine;
	drop table #tmp_network;
	drop table #tmp_conn;

	exec sp_node_init_branch_id @connTree, null;

end;
go

-- poprzednia wersja w pliku: alter db for v1.0.96.1.5 am.sql
-- fix dla wieloinstancyjności
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_object_table]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbo_object_table]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbo_object_table]
as
begin

	declare @tree_id int = dbo.fn_get_bo_actual_universe_tree_id();

	declare @universe_table_kind int = dbo.fn_node_kind_id_by_code('UniverseTable');
	declare @universe_alias_table_kind int = dbo.fn_node_kind_id_by_code('UniverseAliasTable');
	declare @universe_derived_table_kind int = dbo.fn_node_kind_id_by_code('UniverseDerivedTable');
	declare @filtr_kind int = dbo.fn_node_kind_id_by_code('Filter');
	declare @detail_nki int = dbo.fn_node_kind_id_by_code('Detail');
	declare @dimension_nki int = dbo.fn_node_kind_id_by_code('Dimension');
	declare @measure_nki int = dbo.fn_node_kind_id_by_code('Measure');

	delete from bik_sapbo_object_table
	from bik_sapbo_object_table bo
	inner join bik_node obj on obj.id = bo.object_node_id
	inner join bik_node tab on tab.id = bo.table_node_id
	where obj.tree_id = @tree_id
	or tab.tree_id = @tree_id

	insert into bik_sapbo_object_table(object_node_id,table_node_id)
	select *
	from (
	select case when uot.obj_type = 1 then bn.id else bn3.id end as obj_node_ide, bn2.id 
	from aaa_universe_obj_tables uot
	left join aaa_universe_obj uo on uot.universe_obj_id = uo.id
	left join bik_node bn on bn.tree_id = @tree_id
	and bn.obj_id = uo.obj_branch
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bn.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)
	inner join aaa_universe_table ut on uot.universe_table_id = ut.id
	inner join bik_node bn2 on bn2.tree_id = @tree_id and bn2.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
	and bn2.is_deleted = 0
	and bn2.linked_node_id is null
	and bn2.node_kind_id in (@universe_table_kind, @universe_alias_table_kind, @universe_derived_table_kind)--=@universe_table_kind
	left join aaa_universe_filter uf on uot.universe_obj_id = uf.id
	left join bik_node bn3 on bn3.tree_id = @tree_id 
	and bn3.obj_id = uf.filtr_branch
	and bn3.is_deleted = 0
	and bn3.linked_node_id is null
	and bn3.node_kind_id = @filtr_kind
	) x
	where obj_node_ide is not null

	insert into bik_joined_objs(src_node_id,dst_node_id,type)
	select object_node_id, table_node_id, 1 
	from bik_sapbo_object_table a 
	inner join bik_node obj on obj.id = a.object_node_id and obj.tree_id = @tree_id
	inner join bik_node tab on tab.id = a.table_node_id and tab.tree_id = @tree_id
	left join bik_joined_objs b on a.object_node_id = b.src_node_id and a.table_node_id = b.dst_node_id
	where b.id is null

	insert into bik_joined_objs(src_node_id,dst_node_id,type)
	select table_node_id, object_node_id, 1 
	from bik_sapbo_object_table a
	inner join bik_node obj on obj.id = a.object_node_id and obj.tree_id = @tree_id
	inner join bik_node tab on tab.id = a.table_node_id and tab.tree_id = @tree_id
	left join bik_joined_objs b on a.table_node_id = b.src_node_id and a.object_node_id = b.dst_node_id
	where b.id is null

end;--procedure
go

-- poprzednia wersja w pliku: alter db for v1.1.8.6 tf.sql
-- fix dla wieloinstancyjności
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_universe_objects]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_joined_objs_universe_objects
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_universe_objects]
as
begin
	
	declare @uniTree int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connTree int = dbo.fn_get_bo_actual_connection_tree_id();
    declare @uniKind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @dcKind int = dbo.fn_node_kind_id_by_code('DataConnection');

    create table #tmpBranch (branch_id varchar(max), node_id int);

    insert into #tmpBranch(branch_id,node_id)
    select bn.branch_ids, bn2.id from bik_node bn
    inner join bik_joined_objs jo on jo.src_node_id = bn.id
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    where bn.tree_id = @uniTree
    and bn.node_kind_id = @uniKind
    and bn.linked_node_id is null
    and bn.is_deleted = 0
    and bn2.tree_id = @connTree
    and bn2.node_kind_id = @dcKind
    and bn2.linked_node_id is null
    and bn2.is_deleted = 0

    exec sp_prepare_bik_joined_objs_tmp

		insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
		select bn.id, a.node_id, 1 from #tmpBranch a 
		inner join bik_node bn on bn.branch_ids like a.branch_id + '%'
		and bn.is_deleted = 0
		and bn.linked_node_id is null
		where node_kind_id <> @uniKind
		
		insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
		select bn2.id, bn.id, 1 from bik_node bn 
		inner join bik_node bn2 on bn2.branch_ids like bn.branch_ids + '%'
		and bn2.is_deleted = 0
		and bn2.linked_node_id is null
		and bn2.node_kind_id <> @uniKind
		and bn.tree_id = @uniTree
		and bn.is_deleted = 0
		and bn.linked_node_id is null
		and bn.node_kind_id = @uniKind

    exec sp_move_bik_joined_objs_tmp 'Universe,DataConnection', 'UniverseClass,Measure,Dimension,Detail,Filter,UniverseAliasTable,UniverseDerivedTable,UniverseTable,UniverseTablesFolder'
	
	drop table #tmpBranch
end;
go

create table ami_query (
	server_instance_id int not null references bik_sapbo_server(id),
	id int not null,
	name varchar(max) null,
	id_text varchar(max) null,
	filtr_text varchar(max) null,
	sql_text varchar(max) null,
	universe_cuid varchar(max) null,
	report_node_id int null,
	universe_name varchar(max) null,
	wrong_universe_cuid varchar(100) null,
	report_si_id int not null,
	report_update datetime null
);
go

create table ami_report_object (
	server_instance_id int not null references bik_sapbo_server(id),
	id int not null,
	query_id int null,
	universe_CUID varchar(300) null,
	object_name varchar(255) null,
	class_name varchar(255) null,
	universe_name varchar(max) null,
	wrong_universe_cuid varchar(100) null,
	report_si_id int not null,
	report_update datetime null
);
go

declare @BoServer int;
select @BoServer = a.id from bik_sapbo_server a 
inner join bik_data_source_def def on a.source_id = def.id where def.description = 'Business Objects'

-- napełnienie tabel obecnymi danymi
insert into ami_query(server_instance_id, id, name, id_text, filtr_text, sql_text, universe_cuid, report_node_id, universe_name, wrong_universe_cuid, report_si_id, report_update)
select @BoServer, id, name, id_text, filtr_text, sql_text, universe_cuid, report_node_id, universe_name, wrong_universe_cuid, report_si_id, report_update from aaa_query

insert into ami_report_object(server_instance_id,id, query_id, universe_CUID, object_name, class_name, universe_name, wrong_universe_cuid, report_si_id, report_update)
select @BoServer, id, query_id, universe_CUID, object_name, class_name, universe_name, wrong_universe_cuid, report_si_id, report_update from aaa_report_object

-- poprzednia wersja w pliku: alter db for v1.2.4.1 tf.sql
-- fix dla wieloinstancyjności BO
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_metadata_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
	
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();

    declare @teradataKind int = dbo.fn_tree_id_by_code('Teradata');
    declare @schemaKind int = dbo.fn_node_kind_id_by_code('TeradataSchema');
    declare @tabKind int = dbo.fn_node_kind_id_by_code('TeradataTable');
    declare @viewKind int = dbo.fn_node_kind_id_by_code('TeradataView');
    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @universeNodeKind int = dbo.fn_node_kind_id_by_code('Universe');

    ----------   zapytanie  -->  universe    ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
    select bn.id as src_node_id, bnu.id as dst_node_id, 1 as type, 1 as inherit_to_descendants
    from bik_sapbo_query sbq
    inner join bik_node bn on sbq.node_id = bn.id
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id = @universeNodeKind
	where bn.tree_id = @reportTreeId
	and bn.is_deleted = 0

    ----------   zapytanie  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id as src_node_id, bnc.id as dst_node_id, 1 as type
    from bik_sapbo_query sbq
    inner join bik_node bn on sbq.node_id = bn.id
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id = @universeNodeKind
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
    and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 and bnc.node_kind_id = @connection_kind
    --inner join bik_node bnc on bnc.obj_id = convert(varchar(30),uni.SI_DATACONNECTION__1) and
    --bnc.is_deleted = 0 and bnc.node_kind_id = @connection_kind
	where bn.tree_id = @reportTreeId
	and bn.is_deleted = 0 

    ----------   universe  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnu.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
    and bnu.obj_id = convert(varchar(30),uni.si_id) 
    and bnu.is_deleted = 0 and bnu.node_kind_id = @universeNodeKind

    ----------   raport  -->  universe   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnu.id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id = @universeNodeKind

    ----------   raport  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnc.id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id = @universeNodeKind
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
    and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 and bnc.node_kind_id = @connection_kind
    --inner join bik_node bnc on bnc.obj_id=convert(varchar(30),uni.SI_DATACONNECTION__1) and
    --bnc.is_deleted=0 and bnc.node_kind_id = @connection_kind

	----------   connection  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnc.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id = @universeNodeKind
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
    and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 and bnc.node_kind_id = @connection_kind
    --inner join bik_node bnc on bnc.obj_id = convert(varchar(30),uni.SI_DATACONNECTION__1) and
    --bnc.is_deleted=0 and bnc.node_kind_id = @connection_kind

    ----------   obiekt  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_node obj on a.object_node_id = obj.id
    and obj.tree_id = @universeTreeId and obj.is_deleted = 0
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)

    ----------   obiekt  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_node obj on a.object_node_id = obj.id
    and obj.tree_id = @universeTreeId and obj.is_deleted = 0
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind

    ----------   universum  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type = 'Teradata' and b.is_derived = 0 
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Table/View  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type = 'Teradata' and b.is_derived = 0 
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   universum  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Schema  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   connection  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id = @connection_kind
    where b.type = 'Teradata' and b.is_derived = 0 and bn2.id is not null
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Schema  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id = @connection_kind
    where b.type = 'Teradata' and b.is_derived = 0 and bn2.id is not null
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Table/View  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id = @connection_kind
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   connection  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id = @connection_kind
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Table/View  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   raport  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   teradata Schema  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   raport  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   zapytanie  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   zapytanie  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

	----------   obiekt  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct ids.object_node_id, bn.parent_node_id, 1 
    from aaa_ids_for_report ids 
    inner join bik_node bn on ids.query_node_id = bn.id
    and bn.tree_id = @reportTreeId
    and bn.is_deleted = 0
    inner join bik_node bno on bno.id = ids.object_node_id
    and bno.tree_id = @universeTreeId
    and bno.is_deleted = 0
	
	----------   UniverseTable  -->  teradata Table/View   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.name_for_teradata = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'
	inner join bik_node tab on tab.id = bsut.node_id
	and tab.tree_id = @universeTreeId
	and tab.is_deleted = 0

	----------   teradata Table/View  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bsut.node_id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.name_for_teradata = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'
	inner join bik_node tab on tab.id = bsut.node_id
	and tab.tree_id = @universeTreeId
	and tab.is_deleted = 0

	----------   UniverseTable  -->  teradata Schema   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.schema_name = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'
	inner join bik_node tab on tab.id = bsut.node_id
	and tab.tree_id = @universeTreeId
	and tab.is_deleted = 0
	
	----------   UniverseAliasTable  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bsut1.node_id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_sapbo_universe_table bsut1 on bsut.original_table = bsut1.their_id
	inner join bik_node bn on bn.id = bsut.node_id
	inner join bik_node bn2 on bn2.id = bsut1.node_id
	where bsut.is_alias = 1 and bn2.parent_node_id = bn.parent_node_id
	and bn.tree_id = @universeTreeId
	and bn2.tree_id = @universeTreeId

	-- usuniecie alias table - univ table
	exec sp_delete_bik_joined_objs_by_kinds_fast 'UniverseTable', 'UniverseAliasTable'
	
    exec sp_move_bik_joined_objs_tmp 'TeradataView,TeradataTable,TeradataSchema,Webi,ReportQuery', 'Universe,DataConnection,Measure,Dimension,Detail,Filter,Webi,ReportQuery,UniverseAliasTable,UniverseDerivedTable,UniverseTable'
	
end;
go


-- poprzednia wersja w pliku: alter db for v1.2.4.1 tf.sql
-- fix dla wieloinstancyjności BO
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_mssql_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
    
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	
    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');    
    declare @universe_kind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @mssqlTree int = dbo.fn_tree_id_by_code('MS SQL');

	---------- universe table --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ut.node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- universe table --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ut.node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- object --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- object --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- query --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- query --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- report --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- report --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId

	---------- universe --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- universe --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null


	
	---------- mssql table --> universe table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, ut.node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted=0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- mssql table --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bno on bno.id = ot.object_node_id
	and bno.tree_id = @universeTreeId
	and bno.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- mssql table --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql table --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null

	---------- mssql DB --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bno on bno.id = ot.object_node_id
	and bno.tree_id = @universeTreeId
	and bno.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null		
	
    exec sp_move_bik_joined_objs_tmp '@MSSQL', '@Reports, @ObjectUniverses, @Connections'
    
end;
go

-- poprzednia wersja w pliku: alter db for v1.2.4.1 tf.sql
-- fix dla wieloinstancyjności BO
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_oracle_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_oracle_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_oracle_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
    
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	
    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @universe_kind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @oracleTree int = dbo.fn_tree_id_by_code('Oracle');

	---------- universe table --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- universe table --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- object --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- object --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- query --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- query --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null	

	---------- universe --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- universe --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null


	
	---------- oracle table --> universe table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, ut.node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- oracle table --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- oracle table --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- oracle table --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- oracle DB --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null

	---------- oracle DB --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- oracle DB --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null		
	
    exec sp_move_bik_joined_objs_tmp '@Oracle', '@Reports, @ObjectUniverses, @Connections'
    
end;
go


delete from bik_admin
where param like 'sapbo.%' and param <> 'sapbo.path'


declare @constrainName varchar(100);

select @constrainName = name
from sys.key_constraints
where type = 'PK' AND OBJECT_NAME(parent_object_id) = N'bik_sapbo_user';

exec('alter table bik_sapbo_user drop constraint ' + @constrainName)
go

alter table bik_sapbo_user add constraint uk_si_id_instance unique (si_id,server_instance_id)
go

exec sp_update_version '1.3.0.6', '1.3.0.7';
go