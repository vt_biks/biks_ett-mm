/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.ftp;

/**
 *
 * @author pmielanczuk
 */
public class FoxyFTPException extends RuntimeException {

    public FoxyFTPException(Throwable cause) {
        super(cause);
    }

    public FoxyFTPException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
