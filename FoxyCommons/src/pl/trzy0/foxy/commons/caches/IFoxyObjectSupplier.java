/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.caches;

import java.util.Collection;
import java.util.Set;

/**
 *
 * @author wezyr
 */
public interface IFoxyObjectSupplier<K, V> {

    public K getKeyValue(V obj);

    public V getByKey(K key);

    public boolean isNullUniqueForKey(String keyName);

    public Set<String> getAlternativeKeyNames();

    public Object getObjectKeyValue(V obj, String keyName);

    public Collection<V> getManyByKeys(Collection<K> keys);

    public V getByAltKeyVal(String keyName, Object val);

    public Collection<V> getManyByAltKeyVals(String keyName, Collection<Object> vals);
}
