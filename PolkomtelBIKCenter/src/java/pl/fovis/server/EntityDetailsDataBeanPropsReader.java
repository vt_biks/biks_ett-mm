/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import edu.emory.mathcs.backport.java.util.Collections;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;
import pl.bssg.metadatapump.common.ProfileBean;
import pl.bssg.metadatapump.common.ProfileFileBean;
import pl.bssg.metadatapump.filesystem.PumpFileUtils;
import pl.fovis.common.ArticleBean;
import pl.fovis.common.AttachmentBean;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.AuthorBean;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BlogAuthorBean;
import pl.fovis.common.BlogBean;
import pl.fovis.common.ChildrenBean;
import pl.fovis.common.DBColumnBean;
import pl.fovis.common.DBDependencyBean;
import pl.fovis.common.DBDependentObjectBean;
import pl.fovis.common.DBForeignKeyBean;
import pl.fovis.common.DBIndexBean;
import pl.fovis.common.DBPartitionBean;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.ErwinDataModelProcessObjectsRel;
import pl.fovis.common.ErwinRelationshipBean;
import pl.fovis.common.ErwinShapeBean;
import pl.fovis.common.ErwinSubjectAreaBean;
import pl.fovis.common.ErwinSubjectAreaObjectRefBean;
import pl.fovis.common.ErwinTableBean;
import pl.fovis.common.ErwinTableColumnBean;
import pl.fovis.common.FrequentlyAskedQuestionsBean;
import pl.fovis.common.LinkedAlsoBean;
import pl.fovis.common.MssqlExPropBean;
import pl.fovis.common.NodeHistoryChangeBean;
import pl.fovis.common.SapBWBean;
import pl.fovis.common.SapBoConnectionExtraDataBean;
import pl.fovis.common.SapBoExtraDataBean;
import pl.fovis.common.SapBoOLAPConnectionExtraDataBean;
import pl.fovis.common.SapBoQueryExtraDataBean;
import pl.fovis.common.SapBoScheduleBean;
import pl.fovis.common.SapBoUniverseColumnBean;
import pl.fovis.common.SapBoUniverseExtraDataBean;
import pl.fovis.common.SapBoUniverseTableBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.UserExtradataBean;
import pl.fovis.common.UserRoleInNodeBean;
import pl.fovis.common.VoteBean;
import pl.fovis.common.dqm.DQMCoordinatorDataBean;
import pl.fovis.common.dqm.DQMDataErrorIssueBean;
import pl.fovis.common.dqm.DQMEvaluationBean;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.fovis.common.dqm.DQMReportBean;
import pl.fovis.common.dqm.DataQualityGroupBean;
import pl.fovis.common.dqm.DataQualityRequestBean;
import pl.fovis.common.dqm.DataQualityTestBean;
import pl.fovis.common.dqm.DqmGrupaDanychPotomkowieBean;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class EntityDetailsDataBeanPropsReader {

    protected INamedSqlsDAO<Object> adhocDao;
    protected EntityDetailsDataBean ret;
    protected int nodeId;
    //protected Integer parentNodeId;
    //protected boolean isFromLinkedSubtree;
    protected Integer linkingParentId;
    protected IBOXIServiceForEDDBPropsReader service;
    protected String avatarFieldForAd;
//    protected Map<Integer, Float> similarNodes;
//    protected Map<String, JoinedObjsTabKindEx> treeKindToJoinedObjsTabKindMap;
    protected Integer maxChildrenExtradataLevel;
    protected boolean isHistoryEnabled;

    public EntityDetailsDataBeanPropsReader(IBOXIServiceForEDDBPropsReader service, INamedSqlsDAO<Object> adhocDao,
            EntityDetailsDataBean ret, Integer linkingParentId) {
        this.adhocDao = adhocDao;
        this.ret = ret;
        this.nodeId = ret.node.id;
        this.linkingParentId = linkingParentId;
        //this.parentNodeId = ret.node.parentNodeId;
        //this.isFromLinkedSubtree = ret.node.isFromLinkedSubtree;
        this.service = service;

        maxChildrenExtradataLevel = BaseUtils.tryParseInteger((String) execNamedQuerySingleVal("getAppPropValue", true, "val", "maxChildrenExtradataLevel"));
        if (maxChildrenExtradataLevel == null) {
            maxChildrenExtradataLevel = 2;
        }
        isHistoryEnabled = "true".equalsIgnoreCase(BaseUtils.safeToString(execNamedQuerySingleVal("getAppPropValue", true, "val", "enableNodeHistory")));
//        similarNodes = service.getOptSimilarNodes(nodeId);
//        if (similarNodes != null) {
//            treeKindToJoinedObjsTabKindMap = BIKCenterUtils.makeTreeKindToJoinedObjsTabKindMap(service.getDynamicTreeKindCodes());
//        }
    }

    protected String getAvatarFieldForAd() {
//        if (true) {
//            return service.getAvatarFieldForAd();
//        }

        if (avatarFieldForAd == null) {
            avatarFieldForAd = service.getAvatarFieldForAd();
        }

        return avatarFieldForAd;
    }

//    protected UserBean getLoggedUserBean() {
//        return service.getLoggedUserBean();
//    }
    protected SystemUserBean getLoggedUserBean() {
        return service.getLoggedUserBean();
    }

    protected boolean isUserLoggedIn() {
        return getLoggedUserBean() != null;
    }

    public boolean getIsInFav() {
        return isUserLoggedIn() && service.isInFvs(nodeId);
    }

    public FrequentlyAskedQuestionsBean getFrequentlyAskedQuestions() {
        return service.getFrequentlyAskedQuestions(nodeId);
    }

    public List<ArticleBean> getVersions() {
        List<ArticleBean> bizDefs = adhocDao.createBeansFromNamedQry("getBikBizDefForNodeId", ArticleBean.class, nodeId);
        return bizDefs;
    }

//TF: przeniesione do BOXIServiceImpl
//    public Pair<List<UserInNodeBean>, Map<Integer, String>> getUsersInNode() {
//
//        String avatarField = getAvatarFieldForAd();
//        List<Integer> res = getAncestorIdsByBranchIds(ret.node.branchIds);
//        List<UserInNodeBean> usersInNodesList = adhocDao.createBeansFromNamedQry("getUsersInNode", UserInNodeBean.class, nodeId, avatarField);
//        Map<String, UserInNodeBean> userMainRolesInNodes = new HashMap<String, UserInNodeBean>();
//        Map<String, UserInNodeBean> userAuxiliaryRolesInNodes = new HashMap<String, UserInNodeBean>();
//        for (UserInNodeBean ub : usersInNodesList) {
//            if (ub.isAuxiliary) {
//                userAuxiliaryRolesInNodes.put(ub.roleForNodeId + "_" + ub.userName, ub);
//            } else {
//                userMainRolesInNodes.put(ub.roleForNodeId + "_" + ub.isAuxiliary, ub);
//            }
//        }
//        for (int i = res.size() - 1; i >= 0; i--) {
//            usersInNodesList = adhocDao.createBeansFromNamedQry("getUsersInParentNode", UserInNodeBean.class, res.get(i), avatarField);
//            for (UserInNodeBean ub : usersInNodesList) {
//                if (ub.isAuxiliary) {
//                    userAuxiliaryRolesInNodes.put(ub.roleForNodeId + "_" + ub.userName, ub);
//                } else {
//                    if (!userMainRolesInNodes.containsKey(ub.roleForNodeId + "_" + ub.isAuxiliary)) {
//                        userMainRolesInNodes.put(ub.roleForNodeId + "_" + ub.isAuxiliary, ub);
//                    }
//                }
//            }
//        }
//        for (UserInNodeBean ub : userMainRolesInNodes.values()) {
//            userAuxiliaryRolesInNodes.put(ub.roleForNodeId + "_" + ub.userName, ub);
//        }
//
//        Set<Integer> nodeInUsersBranch = new HashSet<Integer>();
//        for (Entry<String, UserInNodeBean> e : userAuxiliaryRolesInNodes.entrySet()) {
//            UserInNodeBean uinb = e.getValue();
//            if (uinb != null) {
//                nodeInUsersBranch.addAll(BIKCenterUtils.splitBranchIdsEx(uinb.userInRoleBranchIds, false));
//            }
//        }
//        Map<Integer, String> mapOfNodeNamesByIdsFromUsersBranch = getIdsAndNamesFromBranches(nodeInUsersBranch);
//
//        return new Pair<List<UserInNodeBean>, Map<Integer, String>>(new ArrayList<UserInNodeBean>(userAuxiliaryRolesInNodes.values()), mapOfNodeNamesByIdsFromUsersBranch);
//    }
    protected List<ChildrenBean> getChildrenExtradataNoWhere() {
        return adhocDao.createBeansFromNamedQry("getChildrenExtradata", ChildrenBean.class, nodeId, null);
    }

    private List<ChildrenBean> getChildrenExtradata4Node(Integer nodeId) {
        List<ChildrenBean> childs = null;
        Map<String, Object> m = adhocDao.execNamedQuerySingleRowCFN("getNodeKindChildrenKindsAndAttrsByNodeId", true, null, nodeId);
        //
        if (!BaseUtils.isMapEmpty(m)) {
            String childrenKinds = (String) m.get("children_kinds");
            String childrenAttrs = (String) m.get("children_attr_names");
            if (!BaseUtils.isStrEmptyOrWhiteSpace(childrenKinds)) {
                List<String> nodeKinds = BaseUtils.splitBySep(childrenKinds, "|", true);
                childs = adhocDao.createBeansFromNamedQry("getChildrenExtradata", ChildrenBean.class, nodeId, nodeKinds);
                if (!BaseUtils.isStrEmptyOrWhiteSpace(childrenAttrs)) {
                    List<String> splittedAttrs = BaseUtils.splitBySep(childrenAttrs, "|", true);
                    for (ChildrenBean child : childs) {
                        child.attributes = getAttributesForNode(child.nodeId, splittedAttrs);
                    }
                }
                int l = ret.node.nodePath.length() + 2;
                for (ChildrenBean child : childs) {
                    child.nodePath = child.nodePath.substring(l);
                }
            }
        }
        return childs;
    }

    public List<ChildrenBean> getChildrenExtradata() {
        List<ChildrenBean> childs = getChildrenExtradata4Node(nodeId);
        int lastCheckedId = 0;

        if (!BaseUtils.isCollectionEmpty(childs)) {
            for (int depth = 1; depth < maxChildrenExtradataLevel; depth++) {
                int size = childs.size();
                for (int i = lastCheckedId; i < size; i++) {
                    List<ChildrenBean> cc = getChildrenExtradata4Node(childs.get(i).nodeId);
                    if (!BaseUtils.isCollectionEmpty(cc)) {
                        childs.addAll(cc);
                    }
                }
                lastCheckedId = size;
            }

            Collections.sort(childs, new Comparator<ChildrenBean>() {

                @Override
                public int compare(ChildrenBean o1, ChildrenBean o2) {
                    return o1.nodePath.compareTo(o2.nodePath);
                }
            });
        }

        return childs;
    }

    public int getCountAttributeForKind() {
        Map<String, Object> id = adhocDao.execNamedQuerySingleRowCFN("getcountAttributeForKind", true, FieldNameConversion.ToLowerCase, ret.node.nodeKindId);
        return (id.get("count" /* I18N: no */) == null) ? 0 : BaseUtils.tryParseInt(id.get("count" /* I18N: no */).toString());
    }

    public List<NodeHistoryChangeBean> getChangeHistory() {
//        return adhocDao.createBeansFromNamedQry("getChangeHistory", NodeHistoryChangeBean.class, nodeId);
        if (!isHistoryEnabled) {
            return new ArrayList<NodeHistoryChangeBean>();
        }
        List<NodeHistoryChangeBean> history = adhocDao.createBeansFromNamedQry("getAttributeChangeHistory", NodeHistoryChangeBean.class, nodeId);
        for (NodeHistoryChangeBean bean : history) {
            bean.newValue = BaseUtils.safeToStringNotNull(bean.newValue);
            bean.oldValue = BaseUtils.safeToStringNotNull(bean.oldValue);
        }

//        for (int i = 0; i < history.size(); i++) {
//            if (i < history.size() - 1) {
//                if (history.get(i).attrId.equals(history.get(i + 1).attrId)) {
//                  //  history.get(i).oldValue = history.get(i + 1).newValue;
//                    continue;
//                }
//            }
        // String oldValue = execNamedQuerySingleVal("getLastAttributeValueBeforeDate", true, null, history.get(i).attrId, history.get(i).dateAdded);
        // history.get(i).oldValue = BaseUtils.safeToStringNotNull(oldValue);
        //   }
        List<NodeHistoryChangeBean> res = new ArrayList<NodeHistoryChangeBean>();
        for (NodeHistoryChangeBean bean : history) {
            if (!bean.oldValue.equals(bean.newValue)) {
                res.add(bean);
            }
        }
        Collections.sort(res, new Comparator<NodeHistoryChangeBean>() {

            @Override
            public int compare(NodeHistoryChangeBean o1, NodeHistoryChangeBean o2) {
                return -o1.dateAdded.compareTo(o2.dateAdded);
            }
        });
        return res;
    }

    public List<BlogAuthorBean> getBlogAuthors() {
        String avatarField = getAvatarFieldForAd();
        return adhocDao.createBeansFromNamedQry("getBikBlogAuthors", BlogAuthorBean.class,
                ret.node.nodeKindCode.equals("BlogEntry") ? ret.node.parentNodeId : nodeId, avatarField);
    }

//    protected List<Integer> getAncestorIdsByNodeId(int nodeId) {
//        return getAncestorIdsByNodeBean(service.getNodeBeanById(nodeId));
//    }
    protected List<Integer> getAncestorIdsByNodeBean(TreeNodeBean n) {
        return getAncestorIdsByBranchIds(n.branchIds);
    }

    protected List<Integer> getAncestorIdsByBranchIds(String branchIds) {
        return BIKCenterUtils.splitBranchIdsEx(branchIds, true);
    }

//    protected <T> void addBeansWithDstNodeIdWithoutDuplicates(
//            Map<Integer, T> res, Iterable<T> beansToAdd,
//            IDstNodeIdExtractor<T> extractor) {
//        //long startTime = System.currentTimeMillis();
//        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
//            GlobalThreadStopWatch.startProcess("addBeansWithDstNodeIdWithoutDuplicates");
//        }
//        try {
//
//            int suggestSimilarMaxResults = service.getSuggestSimilarMaxResults();
//
////            int currentSimilarResults = 0;
//            Map<JoinedObjsTabKindEx, Integer> currentSimilarResults = new EnumMap<JoinedObjsTabKindEx, Integer>(JoinedObjsTabKindEx.class);
//            for (JoinedObjsTabKindEx jotk : JoinedObjsTabKindEx.values()) {
//                currentSimilarResults.put(jotk, 0);
//            }
//
//            for (T bean : res.values()) {
//                if (bean instanceof IHasSimilarityScore) {
//                    IHasSimilarityScore hss = (IHasSimilarityScore) bean;
//                    if (hss.getSimilarityScore() != null) {
//                        JoinedObjsTabKindEx jotk = treeKindToJoinedObjsTabKindMap.get(extractor.getTreeKind(bean));
//                        if (jotk != null) {
////                        currentSimilarResults++;
//                            currentSimilarResults.put(jotk, currentSimilarResults.get(jotk) + 1);
//                        }
//                    }
//                }
//            }
//
//            for (T beanToAdd : beansToAdd) {
//
//                boolean hasSimilarityScore = false;
//
//                if (beanToAdd instanceof IHasSimilarityScore) {
//                    IHasSimilarityScore hss = (IHasSimilarityScore) beanToAdd;
//                    hasSimilarityScore = hss.getSimilarityScore() != null;
//                }
//
//                Integer dstNodeId = //beanToAdd.getDstNodeId();
//                        extractor.getDstNodeId(beanToAdd);
//
//                T prevAddedBean = res.get(dstNodeId);
//                boolean prevHasScore = false;
//
//                if (prevAddedBean instanceof IHasSimilarityScore) {
//                    IHasSimilarityScore phss = (IHasSimilarityScore) prevAddedBean;
//                    prevHasScore = phss.getSimilarityScore() != null;
//                }
//
//                if (prevAddedBean == null || prevHasScore) {
//
//                    if (hasSimilarityScore) {
//
//                        JoinedObjsTabKindEx jotk = treeKindToJoinedObjsTabKindMap.get(extractor.getTreeKind(beanToAdd));
//                        int csr = jotk == null ? suggestSimilarMaxResults : currentSimilarResults.get(jotk);
//
//                        if (csr >= suggestSimilarMaxResults) {
//                            continue;
//                        }
//                        if (!prevHasScore) {
////                            currentSimilarResults++;
//                            currentSimilarResults.put(jotk, csr + 1);
//                        }
//                    }
//
//                    res.put(dstNodeId, beanToAdd);
//                }
//            }
//        } finally {
//            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
//                GlobalThreadStopWatch.endProcess();
//            }
//            //long endTime = System.currentTimeMillis();
//            //System.out.println("addBeansWithDstNodeIdWithoutDuplicates: elapsed time: " + (endTime - startTime) + " millis");
//        }
//    }
//    protected <T> void addBeansWithDstNodeIdAndInherited(
//            Map<Integer, T> res, TreeNodeBean n, boolean includeInheritedFromThisNode,
//            String queryName, Class<T> beanClass,
//            IDstNodeIdExtractor<T> extractor, String avatarField) {
//        //long startTime = System.currentTimeMillis();
//        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
//            GlobalThreadStopWatch.startProcess("addBeansWithDstNodeIdAndInherited");
//        }
//        try {
//            List<Integer> ancIds = getAncestorIdsByNodeBean(n);
////            if (includeInheritedFromThisNode) {
//            ancIds.add(n.id);
////            } else {
////                addBeansWithDstNodeIdWithoutDuplicates(res,
////                        avatarField == null ? adhocDao.createBeansFromNamedQry(queryName, beanClass, nodeId, false, similarNodes == null ? null : similarNodes, nodeId)
////                                : adhocDao.createBeansFromNamedQry(queryName, beanClass, nodeId, false, avatarField, similarNodes == null ? null : similarNodes, nodeId),
////                        extractor);
////            }
//
//            if (!ancIds.isEmpty()) {
//                List<T> inherited = (avatarField == null ? adhocDao.createBeansFromNamedQry(queryName, beanClass, ancIds, true, similarNodes == null ? null : similarNodes, nodeId)
//                        : adhocDao.createBeansFromNamedQry(queryName, beanClass, ancIds, true, avatarField, similarNodes == null ? null : similarNodes, nodeId));
//                addBeansWithDstNodeIdWithoutDuplicates(res, inherited, extractor);
//            }
//
//            //long endTime = System.currentTimeMillis();
//            //System.out.println("addBeansWithDstNodeIdAndInherited: elapsed time: " + (endTime - startTime) + " millis");
//        } finally {
//            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
//                GlobalThreadStopWatch.endProcess();
//            }
//        }
//    }
//    protected <T> List<T> getRelatedBeansWithDstNodeId(String queryName,
//            Class<T> beanClass, IDstNodeIdExtractor<T> extractor, String avatarField) {
//        Map<Integer, T> res = new LinkedHashMap<Integer, T>();
//        addBeansWithDstNodeIdAndInherited(res, ret.node, false, queryName, beanClass,
//                extractor, avatarField);
//
//        if (linkingParentId != null) {
//            TreeNodeBean linkingParentNode = service.getNodeBeanById(linkingParentId, false);
//            addBeansWithDstNodeIdAndInherited(res, linkingParentNode, true, queryName, beanClass,
//                    extractor, avatarField);
//        }
//        return new ArrayList<T>(res.values());
//    }
    // domyślnie nie robimy private, tylko protected, private tylko gdy wiadomo dlaczego ma
    // to być konkretnie private!
//    protected List<JoinedObjBean> getRelated(String queryName) {
//        return getRelatedBeansWithDstNodeId(queryName, JoinedObjBean.class,
//                //DstNodeIdExtractorForBeanWithDstNodeId.<JoinedObjBean>getInstance()
//                //new DstNodeIdExtractorForBeanWithDstNodeId<JoinedObjBean>()
//                new IDstNodeIdExtractor<JoinedObjBean>() {
//                    @Override
//                    public Integer getDstNodeId(JoinedObjBean bean) {
//
////                        System.out.println("getDstNodeId: bean class=" + BaseUtils.safeGetClassName(bean));
////                        Object dstIdAsObj = bean.dstId;
////                        System.out.println("getDstNodeId: dstId class=" + BaseUtils.safeGetClassName(dstIdAsObj));
//                        return bean.dstId;
//                    }
//
//                    @Override
//                    public String getTreeKind(JoinedObjBean bean) {
//                        return bean.treeKind;
//                    }
//                }, null);
//    }
    // domyślnie nie robimy private, tylko protected, private tylko gdy wiadomo dlaczego ma
    // to być konkretnie private!
//    protected List<BlogBean> getRelatedBlogs() {
//        String avatarField = getAvatarFieldForAd();
//        return getRelatedBeansWithDstNodeId("bikEntityBlog", BlogBean.class,
//                new IDstNodeIdExtractor<BlogBean>() {
//                    @Override
//                    public Integer getDstNodeId(BlogBean bean) {
//                        return bean.nodeId;
//                    }
//
//                    @Override
//                    public String getTreeKind(BlogBean bean) {
//                        return BIKGWTConstants.TREE_KIND_BLOGS;
//                    }
//                }, avatarField == null ? BIKConstants.DEFAULT_AD_FIELD_NAME : avatarField);
//    }
//    protected List<AttachmentBean> getRelatedAttachment() {
//        //long startTime = System.currentTimeMillis();
//        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
//            GlobalThreadStopWatch.startProcess("addBeansWithDstNodeIdAndInherited");
//        }
//        try {
//            return getRelatedBeansWithDstNodeId("bikEntityAttachments", AttachmentBean.class,
//                    new IDstNodeIdExtractor<AttachmentBean>() {
//                        @Override
//                        public Integer getDstNodeId(AttachmentBean bean) {
//                            return bean.nodeId;
//                        }
//
//                        @Override
//                        public String getTreeKind(AttachmentBean bean) {
//                            return BIKGWTConstants.TREE_KIND_DOCUMENTS;
//                        }
//                    }, null);
//        } finally {
////            long endTime = System.currentTimeMillis();
////            System.out.println("getRelatedAttachment: elapsed time: " + (endTime - startTime) + " millis");
//            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
//                GlobalThreadStopWatch.endProcess();
//            }
//        }
//    }
//    public List<JoinedObjBean> getArts() {
//        return getRelated("getBikArtsBySiId");
//    }
//
//    public List<JoinedObjBean> getDictionaries() {
//        return getRelated("getBikDictionariesBySiId");
//    }
//    public List<JoinedObjBean> getJoinedObjs() {
////        return getRelated("getBikJoinedObjsInner");
//
//        TabWidgetsDataProvider dp = new TabWidgetsDataProvider(adhocDao, service);
//        return dp.getJoinedObjs(nodeId, linkingParentId);
//    }
//    public List<AttachmentBean> getAttachments() {
//        return getRelatedAttachment();
////        return adhocDao.createBeansFromNamedQry("bikEntityAttachments", AttachmentBean.class, nodeId);
//    }
    public List<SapBoUniverseColumnBean> getColumns() {
        return adhocDao.createBeansFromNamedQry("getAllAboutTable", SapBoUniverseColumnBean.class, nodeId);
    }

    public List<DBIndexBean> getTeradataIndices() {
        if (ret.node.nodeKindCode.equals(BIKConstants.NODE_KIND_TERADATA_TABLE)) {
            return adhocDao.createBeansFromNamedQry("getTeradataIndexForTable", DBIndexBean.class, ret.node.objId);
        } else { // for column
            return adhocDao.createBeansFromNamedQry("getTeradataIndexForColumn", DBIndexBean.class, ret.node.objId);
        }
    }

    public String getTeratadaExtradata() {
        return (String) execNamedQuerySingleVal("getTeradataExtradata", true, "txt" /* I18N: no */, ret.node.objId);
    }

    public String getMssqlExtradata() {
        return (String) execNamedQuerySingleVal("getMssqlExtradata", true, "txt" /* I18N: no */, nodeId);
    }

    public String getOracleExtradata() {
        return (String) execNamedQuerySingleVal("getOracleExtradata", true, "txt" /* I18N: no */, nodeId);
    }

    public List<DBColumnBean> getMssqlColumnExtradata() {
        return adhocDao.createBeansFromNamedQry("getMssqlColumnExtradata", DBColumnBean.class, nodeId, ret.node.nodeKindCode.equals(BIKConstants.NODE_KIND_MSSQL_TABLE) || ret.node.nodeKindCode.equals(BIKConstants.NODE_KIND_MSSQL_VIEW));
    }

    public List<DBForeignKeyBean> getMssqlForeignKeyExtradata() {
        return adhocDao.createBeansFromNamedQry("getMssqlForeignKeyExtradata", DBForeignKeyBean.class, nodeId, ret.node.nodeKindCode.equals(BIKConstants.NODE_KIND_MSSQL_TABLE));
    }

    public List<DBPartitionBean> getMssqlPartitionExtradata() {
        return adhocDao.createBeansFromNamedQry("getMssqlPartitionExtradata", DBPartitionBean.class, nodeId, ret.node.nodeKindCode.equals(BIKConstants.NODE_KIND_MSSQL_TABLE));
    }

    public List<DBDependencyBean> getMssqlDependencyExtradata() {
        return adhocDao.createBeansFromNamedQry("getMssqlDependencyExtradata", DBDependencyBean.class, nodeId);
    }

    public List<DBDependentObjectBean> getMssqlDependentObjectsExtradata() {
        return adhocDao.createBeansFromNamedQry("getMssqlDependentObjectsExtradata", DBDependentObjectBean.class, nodeId);
    }

    public List<DBIndexBean> getMssqlIndices() {
        return adhocDao.createBeansFromNamedQry("getMssqlIndexExtradata", DBIndexBean.class, nodeId, ret.node.nodeKindCode.equals(BIKConstants.NODE_KIND_MSSQL_TABLE));
    }

    public List<DBColumnBean> getDbColumnExtradata() {
        return adhocDao.createBeansFromNamedQry("getDbColumnExtradata", DBColumnBean.class, nodeId);
    }

    public List<DBForeignKeyBean> getDbForeignKeyExtradata() {
        return adhocDao.createBeansFromNamedQry("getDbForeignKeyExtradata", DBForeignKeyBean.class, nodeId);
    }

    public List<DBPartitionBean> getDbPartitionExtradata() {
        return adhocDao.createBeansFromNamedQry("getDbPartitionExtradata", DBPartitionBean.class, nodeId);
    }

    public List<DBDependencyBean> getDbDependencyExtradata() {
        return adhocDao.createBeansFromNamedQry("getDbDependencyExtradata", DBDependencyBean.class, nodeId);
    }

    public List<DBDependentObjectBean> getDbDependentObjectsExtradata() {
        return adhocDao.createBeansFromNamedQry("getDbDependentObjectsExtradata", DBDependentObjectBean.class, nodeId);
    }

    public List<DBIndexBean> getDbIndices() {
        List<DBIndexBean> tmp = adhocDao.createBeansFromNamedQry("getDbIndices", DBIndexBean.class, nodeId);
        return tmp;
    }

    public List<MssqlExPropBean> getMssqlExProps() {
        return adhocDao.createBeansFromNamedQry("getMssqlExProps", MssqlExPropBean.class, nodeId);
    }

    public List<ChildrenBean> getMssqlChildrenExtradata() {
        return getChildrenExtradataNoWhere();
    }

    public ConfluenceObjectBean getConfluenceExtradata() {
        ConfluenceObjectBean bean = adhocDao.createBeanFromNamedQry("getConfluenceExtradata", ConfluenceObjectBean.class, true, nodeId);
//        bean.setAttachments(adhocDao.createBeansFromNamedQry("getConfluenceAttachments", ConfluenceObjectBean.class, nodeId));
        return bean;
    }

    public SapBoUniverseTableBean getTables() {
        return adhocDao.createBeanFromNamedQry("getBikTables", SapBoUniverseTableBean.class, true, nodeId);
    }

//    public List<NoteBean> getNotes() {
//        List<NoteBean> notes;
//        String avatarField = getAvatarFieldForAd();
//        if (ret.user == null) { // normalne komentarze
//            notes = adhocDao.createBeansFromNamedQry("bikEntityNotes", NoteBean.class, nodeId, avatarField);
//        } else { // moje komentarza na MYBIKS
//            notes = adhocDao.createBeansFromNamedQry("bikUserEntityNotes", NoteBean.class, ret.user.id, avatarField);
//        }
//        return notes;
//    }
    public List<AttributeBean> getAttributes() {
        List<AttributeBean> res = getAttributesForNode(nodeId, null);
        List<AttributeBean> metaAttrs = adhocDao.createBeansFromNamedQry("getLoadedAttributes", AttributeBean.class, nodeId);
        res.addAll(metaAttrs);
        return res;
    }

    protected List<AttributeBean> getAttributesForNode(int nodeId, List<String> attrToFIlter) {
        return adhocDao.createBeansFromNamedQry("bikEntityAttribute", AttributeBean.class, nodeId, attrToFIlter);
    }

    public List<TreeNodeBean> getAuthorBlogs() {
        if (ret.user != null) {
            return adhocDao.createBeansFromNamedQry("getBikAuthorBlogs", TreeNodeBean.class, ret.user.id);
        } else {
            return null;
        }
    }

//    public List<BlogBean> getBlogs() {
//        List<BlogBean> blogs;
//        if (ret.user == null) {
//            blogs = getRelatedBlogs();
//        } else {
//            blogs = adhocDao.createBeansFromNamedQry("getBikUserBlogs", BlogBean.class, ret.user.id);
//        }
//
//        return blogs;
//    }
    public ArticleBean getThisBizDef() {
        return service.getBikBizDefForNodeId(nodeId);
    }

    public BlogBean getBlogEntry() {
        return service.getBikEntryByNodeId(nodeId);
    }

    public AttachmentBean getThisDocument() {
        return service.getBikDocumentForSiId(nodeId);
    }

    public VoteBean getVote() {
//        UserBean user = getLoggedUserBean();
        SystemUserBean user = getLoggedUserBean();
        return adhocDao.createBeanFromNamedQry("getVote", VoteBean.class, true, nodeId, user != null ? user.id : null);
    }

    //ww->pane: kto to zakomentował i dlaczego?
    //ww: nie pamiętam czemu się do tego czepiałem, widocznie to coś ważnego
    // pytanie czy było ważne, czy nadal jest...? nie wiem teraz...
//    public UserExtBean getUser() {
//
//        return bikUserByNodeId(nodeId);
//    }
    // nieuzywane
//    public String getAddDescr() {
//        return ret.node.descr;
//    }
    public String getMetadataEditedDescription() {
        return execNamedQuerySingleVal("getMetadataEditedDescription", true, "descr" /* I18N: no */, nodeId);
    }

    public String getDbDefinition() {
        return execNamedQuerySingleVal("getDbDefinition", true, "definition" /* I18N: no */, nodeId);
    }

    public UserExtradataBean getUserExtradata() {
        String avatarField = getAvatarFieldForAd();
        return adhocDao.createBeanFromNamedQry("getUserExtradata", UserExtradataBean.class, true, nodeId, avatarField != null ? BaseUtils.beanFieldNameToTableColName(avatarField) : null);
    }

    public DataQualityTestBean getDataQualityTestExtradata() {//DqcTestExtradata() {
        if (ret.node.treeCode.equals(BIKConstants.TREE_CODE_DQM)) {
            DataQualityTestBean bean = adhocDao.createBeanFromNamedQry("getDQMTestExtradata", DataQualityTestBean.class, true, nodeId);
            bean.multiSources = adhocDao.createBeansFromNamedQry("getDQMTestMultiSources", DQMMultiSourceBean.class, nodeId, null);
            return bean;
        } else {
            return adhocDao.createBeanFromNamedQry("getDQCTestExtradata", DataQualityTestBean.class, true, ret.node.objId);
        }
    }

    public DQMEvaluationBean getDqmEvaluationBean() {
        return adhocDao.createBeanFromNamedQry("getDQMEvaluation", DQMEvaluationBean.class, true, nodeId, true);
    }

    public Pair<DQMReportBean, List<DQMEvaluationBean>> getDqmReportEvaluations() {
        DQMReportBean report = adhocDao.createBeanFromNamedQry("getDQMReport", DQMReportBean.class, true, nodeId);
        if (report == null) {
            return null;
        }
        List<DQMEvaluationBean> objects = adhocDao.createBeansFromNamedQry("getDQMReportEvaluation", DQMEvaluationBean.class, nodeId, true);
        return new Pair<DQMReportBean, List<DQMEvaluationBean>>(report, objects);
    }

    public Map<String, DQMCoordinatorDataBean> getDqmCoordinatorData() {
        if (ret.node.parentNodeId != null) {
            return null;
        }
        Map<String, DQMCoordinatorDataBean> groupMap = new LinkedHashMap<String, DQMCoordinatorDataBean>();
        List<TreeNodeBean> groups = adhocDao.createBeansFromNamedQry("getDQMGroupsForCoordinatorReport", TreeNodeBean.class);
        for (TreeNodeBean group : groups) {
            DQMCoordinatorDataBean data = new DQMCoordinatorDataBean();
            data.reportData = adhocDao.createBeansFromNamedQry("getDQMCoordinatorReport", DQMEvaluationBean.class, group.id);
            data.numerAllDataErrorIssues = execNamedQuerySingleVal("getNumberDataErrorIssues", false, "cnt", group.id, null);
            data.numerOpenedDataErrorIssues = execNamedQuerySingleVal("getNumberDataErrorIssues", false, "cnt", group.id, 0);
            data.errorRegisterNodeId = execNamedQuerySingleVal("getErrorRegisterNodeId", true, "id", group.id);
            groupMap.put(group.name, data);
        }
        return groupMap;
    }

    public List<DataQualityGroupBean> getDataQualityTestGroupExtradata() {
        if (ret.node.treeCode.equals(BIKConstants.TREE_CODE_DQM)) {

            // dla "Grupa danych" tego nie pokazujemy (w PB)
            if (BaseUtils.countSubstrings(ret.node.branchIds, "|") == 1) {
                return null;
            }

            /* List<DataQualityGroupBean> children = adhocDao.createBeansFromNamedQry("getDataQualityChildren", DataQualityGroupBean.class, nodeId);
             List<DataQualityGroupBean> listToReturn = new ArrayList<DataQualityGroupBean>();
             for (DataQualityGroupBean child : children) {
             List<DataQualityGroupBean> childResult = getDataQualityTestGroupInner(child);
             for (int i = childResult.size(); i > 0; i--) {
             DataQualityGroupBean cbgb = childResult.get(i - 1);
             cbgb.isError = child.isError;
             listToReturn.add(cbgb);
             }
             if (childResult.isEmpty()) {
             listToReturn.add(child);
             }
             }
             return listToReturn;*/
            return adhocDao.createBeansFromNamedQry("getDataQualityLastRequestsEx", DataQualityGroupBean.class, nodeId);
        } else {
            return service.getDqcTestGroupExtradata(nodeId);
        }
    }

    // zwraca do 7 rekordow posortowanych po dacie malejaco
    /*   protected List<DataQualityGroupBean> getDataQualityTestGroupInner(DataQualityGroupBean bean) {
     if (bean.type.equals(BIKConstants.NODE_KIND_DQM_GROUP)) {
     List<DataQualityGroupBean> listToReturn = new ArrayList<DataQualityGroupBean>();
     Map<Integer, DataQualityGroupBean> map = new LinkedHashMap<Integer, DataQualityGroupBean>();
     List<DataQualityGroupBean> children = adhocDao.createBeansFromNamedQry("getDataQualityChildren", DataQualityGroupBean.class, bean.nodeId);
     for (DataQualityGroupBean child : children) {
     List<DataQualityGroupBean> childResults = getDataQualityTestGroupInner(child);
     int i = 0;
     for (DataQualityGroupBean chR : childResults) {
     if (map.get(i) == null || !chR.isSuccess) {
     chR.nodeId = bean.nodeId;
     chR.name = bean.name;
     chR.requestHint = null;
     chR.testHint = null;
     map.put(i, chR);
     }
     i++;
     }
     }
     for (Entry<Integer, DataQualityGroupBean> entrySet : map.entrySet()) {
     listToReturn.add(entrySet.getKey(), entrySet.getValue());
     }
     return listToReturn;
     } else {
     List<DataQualityGroupBean> res = adhocDao.createBeansFromNamedQry("getDataQualityLastRequests", DataQualityGroupBean.class, bean.nodeId);
     return res;
     }
     }*/
    public SapBWBean getSapbwExtradata() {
        SapBWBean bean = null;
        if (ret.node.nodeKindCode.equals(BIKConstants.NODE_KIND_SAPBW_BEX)) {
            bean = adhocDao.createBeanFromNamedQry("getSapBWExtradata", SapBWBean.class, true, ret.node.objId);
        }
        if (bean == null) {
            bean = new SapBWBean();
        }
        bean.technicalName = ret.node.objId;
        return bean;
    }

    public List<DataQualityRequestBean> getDataQualityTestRequests() { //getDqcTestLastRequests() {
        if (ret.node.treeCode.equals(BIKConstants.TREE_CODE_DQM)) {
            return adhocDao.createBeansFromNamedQry("getDqmTestRequests", DataQualityRequestBean.class, nodeId);
        } else {
            return adhocDao.createBeansFromNamedQry("getDQCTestRequests", DataQualityRequestBean.class, ret.node.objId);
        }
    }

    public List<LinkedAlsoBean> getAlsoLinked() {
        List<LinkedAlsoBean> lns;

//        String treeCode = ret.node.nodeKindCode.equals(BIKConstants.NODE_KIND_USER) ? BIKConstants.TREE_CODE_USERS : BIKConstants.TREE_CODE_DQC;
        lns = adhocDao.createBeansFromNamedQry("getOtherLinekedNodes", LinkedAlsoBean.class, nodeId);
//        if (ret.node.nodeKindCode.equals(BIKConstants.NODE_KIND_USER)) {
////            return adhocDao.createBeansFromNamedQry("getOtherLinekedNodes", LinkedAlsoBean.class, nodeId, BIKConstants.TREE_CODE_USERS);
//        } else { // for dqc tests: possitive, negative and unactive
////            return adhocDao.createBeansFromNamedQry("getOtherLinekedNodes", LinkedAlsoBean.class, nodeId, BIKConstants.TREE_CODE_DQC);
//        }
        for (LinkedAlsoBean ln : lns) {
            ln.ancestorNodePathByNodeId = execNamedQuerySingleColAsList("getNodeNamesByIds", "name" /* I18N: no */, BIKCenterUtils.splitBranchIdsEx(ln.branchIds, false));
        }
        return lns;
    }

    public List<LinkedAlsoBean> getOtherBOTheSameObject() {
        return adhocDao.createBeansFromNamedQry("getOtherBOTheSameObject", LinkedAlsoBean.class, nodeId);
    }

    public String getSapbwOriginalProvidersArea() {
        return execNamedQuerySingleVal("getSapbwOriginalProvidersArea", true, null, nodeId);
    }

    public SapBoExtraDataBean getSapBoExtraData() {
        return adhocDao.createBeanFromNamedQry("getBikSapBoExtraData", SapBoExtraDataBean.class, true, nodeId);
    }

    public SapBoExtraDataBean getSapBoFolderExtraData() {
        return adhocDao.createBeanFromNamedQry("getBikSapBoFolderExtraData", SapBoExtraDataBean.class, true, nodeId);
    }

    public SapBoConnectionExtraDataBean getSapBoConnectionExtraData() {
        return adhocDao.createBeanFromNamedQry("getConnectionExtraData", SapBoConnectionExtraDataBean.class, true, nodeId);
    }

    public SapBoOLAPConnectionExtraDataBean getSapBoOLAPConnectionExtraData() {
        return adhocDao.createBeanFromNamedQry("getSapBoOLAPConnectionExtraData", SapBoOLAPConnectionExtraDataBean.class, true, nodeId);
    }

    public SapBoUniverseExtraDataBean getSapBoUniverseExtraData() {
        SapBoUniverseExtraDataBean bean = adhocDao.createBeanFromNamedQry("getUniverseExtraData", SapBoUniverseExtraDataBean.class, true, nodeId);
        if (bean != null) {
            bean.tables = execNamedQuerySingleColAsList("getUniverseObjectTables", "name" /* I18N: no */, nodeId);
        }
        return bean;
    }

    public SapBoQueryExtraDataBean getSapBoQueryDataBean() {
        return adhocDao.createBeanFromNamedQry("getSapBoQueryDataBean", SapBoQueryExtraDataBean.class, true, nodeId);
    }

    public SapBoScheduleBean getSapBoScheduleBean() {
        return adhocDao.createBeanFromNamedQry("getSapBoSchedule", SapBoScheduleBean.class, true, nodeId);
    }

    public boolean getHasChildren() {
        Object flagObj = execNamedQuerySingleVal("nodeHasChildren", false, "flag" /* I18N: no */,
                ret.node.id, ret.node.treeId);

//        System.out.println("#### getHasChildren: node=" + ret.node.id + ", treeId=" + ret.node.treeId
//                + ", flagObj=" + flagObj);
        return ((Integer) flagObj) != 0;
    }

    public <V> List<V> execNamedQuerySingleColAsList(String qryName, String optColumnName, Object... args) {
        return BeanConnectionUtils.execNamedQuerySingleColAsList(adhocDao, qryName,
                optColumnName, args);
    }

    public <V> V execNamedQuerySingleVal(String qryName, boolean allowNoResult,
            String optColumnName, Object... args) {
        return BeanConnectionUtils.<V>execNamedQuerySingleVal(adhocDao, qryName, allowNoResult,
                optColumnName, args);
    }

    public Pair<List<UserRoleInNodeBean>, Map<Integer, String>> getUserRolesInNodes() {
        String avatarField = getAvatarFieldForAd();
        List<UserRoleInNodeBean> listOfUserRolesInNodes = adhocDao.createBeansFromNamedQry("getBikUserRolesInNodes", UserRoleInNodeBean.class, nodeId, avatarField);
        Set<Integer> nodeInUsersBranch = new HashSet<Integer>();
        for (UserRoleInNodeBean ur : listOfUserRolesInNodes) {
            nodeInUsersBranch.addAll(BIKCenterUtils.splitBranchIdsEx(ur.roleBranchIds, false));
        }
        TabWidgetsDataProvider dp = new TabWidgetsDataProvider(adhocDao, service);
        Map<Integer, String> mapOfNodeNamesByIdsFromRoleBranch = dp.getIdsAndNamesFromBranches(nodeInUsersBranch);
        return new Pair<List<UserRoleInNodeBean>, Map<Integer, String>>(listOfUserRolesInNodes, mapOfNodeNamesByIdsFromRoleBranch);
    }

//    protected Map<Integer, String> getIdsAndNamesFromBranches(Set<Integer> idsFromBranches) {
//        Map<Integer, String> mapOfNodeNamesByIdsFromUsersBranch = new LinkedHashMap<Integer, String>();
//        List<Map<String, Object>> nodeIdsAndNamesFromUsersBranch;
//        idsFromBranches.toArray();
//        if (!idsFromBranches.isEmpty()) {
//            nodeIdsAndNamesFromUsersBranch = adhocDao.execNamedQueryCFN("getIdsAndNames",
//                    FieldNameConversion.ToJavaPropName, idsFromBranches);
//            for (Map<String, Object> m : nodeIdsAndNamesFromUsersBranch) {
//                mapOfNodeNamesByIdsFromUsersBranch.put((Integer) m.get("id" /* I18N: no */), (String) m.get("name" /* I18N: no */));
//            }
//        }
//        return mapOfNodeNamesByIdsFromUsersBranch;
//    }
    public boolean getIsAuthor() {
        SystemUserBean user = getLoggedUserBean();
        if (user == null || user.userId == null || ret.node == null || ret.node.branchIds == null) {
            return false;
        }

        List<Integer> list = getIdsListFrombranchIds();
        return ((Integer) execNamedQuerySingleVal("haveProperRoleInNodeOrAboveInTree", true, "" + "result" /* I18N: no */, user.userId, "Author" /* I18N: no */, list)) > 0;
    }

    public boolean getIsEditor() {
        SystemUserBean user = getLoggedUserBean();
        if (user == null || user.userId == null || ret.node == null || ret.node.branchIds == null) {
            return false;
        }

        List<Integer> list = getIdsListFrombranchIds();
        return ((Integer) execNamedQuerySingleVal("haveProperRoleInNodeOrAboveInTree", true, "" + "result" /* I18N: no */, user.userId, "Editor" /* I18N: no */, list)) > 0;
    }

    public AuthorBean getAuthor() {
        AuthorBean bean = adhocDao.createBeanFromNamedQry("getAuthor", AuthorBean.class, true, nodeId);
        if (bean == null) {
            Date dateFromHistory = execNamedQuerySingleVal("getCreationDateFromHistory", true, null, nodeId);
            if (dateFromHistory != null) {
                bean = new AuthorBean();
                bean.authorId = null;
                bean.authorName = "";
                bean.authorSystemId = null;
                bean.dateAdded = dateFromHistory;
                adhocDao.execNamedCommand("addObjectAuthorAsConnector", nodeId, dateFromHistory);
            }
        }
        return bean;
    }

    protected List<Integer> getIdsListFrombranchIds() {
        List<Integer> list = new ArrayList<Integer>();
        for (String id : ret.node.branchIds.substring(0, ret.node.branchIds.length() - 1).split("\\|")) {
            list.add(BaseUtils.tryParseInteger(id, 0));
        }
        return list;
    }

//    public String getRoleCode() {
//        return execNamedQuerySingleVal("getRoleCode", true, "code" /* I18N: no */, nodeId);
//    }
    public Map<Integer, String> getRolesNodeIdAndCode() {

        Map<Integer, String> roleNodeIdsAndCodes = new HashMap<Integer, String>();

        List<Map<String, Object>> roles = adhocDao.execNamedQueryCFN("getRolesNodeIdAndCode", FieldNameConversion.ToLowerCase);
        for (Map<String, Object> m : roles) {
            roleNodeIdsAndCodes.put((Integer) m.get("node_id" /* I18N: no */), (String) m.get("code" /* I18N: no */));
        }
        return roleNodeIdsAndCodes;
    }

    public List<ErwinTableBean> getErwinListTables() {
        return null;
        //return adhocDao.createBeansFromNamedQry("getErwinTables", ErwinTableBean.class, nodeId);
    }

    public List<ErwinShapeBean> getErwinShapes() {
        return null;
        //return adhocDao.createBeansFromNamedQry("getErwinShapes", ErwinShapeBean.class, nodeId);
    }

    public List<ErwinTableColumnBean> getErwinTableColumns() {
        return null;
        //return adhocDao.createBeansFromNamedQry("getErwinTableColumns", ErwinTableColumnBean.class, nodeId);
    }

    public List<ErwinRelationshipBean> getErwinRelationships() {
        return null;
        //return adhocDao.createBeansFromNamedQry("getErwinRelationships", ErwinRelationshipBean.class, nodeId);
    }

    public List<ErwinSubjectAreaBean> getErwinSubjectAreas() {
        return adhocDao.createBeansFromNamedQry("getErwinSubjectAreas", ErwinSubjectAreaBean.class, nodeId);
    }

    public List<ErwinSubjectAreaObjectRefBean> getErwinErwinSubjectAreaObjectRefs() {
        return null;
        //return adhocDao.createBeansFromNamedQry("getErwinSubjectAreaObjectRef", ErwinSubjectAreaObjectRefBean.class, nodeId);
    }

    public List<ErwinDataModelProcessObjectsRel> getErwinDataModelProcessObjectsRel() {
        return null;
        //return adhocDao.createBeansFromNamedQry("getDataModelProcessObjectsRel", ErwinDataModelProcessObjectsRel.class, nodeId);
    }

    public List<String> getErwinDataModelProcessAreas() {
        return execNamedQuerySingleColAsList("getErwinDataModelProcessAreas", "area" /* I18N: no */, nodeId);
    }

    public ProfileBean getProfileItemExtradata() {
        return adhocDao.createBeanFromNamedQry("getProfileItemExtradata", ProfileBean.class, true, nodeId);
    }

    public ProfileFileBean getProfileFileExtradata() {
        return adhocDao.createBeanFromNamedQry("getProfileFileExtradata", ProfileFileBean.class, true, nodeId);
    }

    public List<Map<String, String>> getGenericNodeObjects() {
        List<Map<String, String>> toReturn = new ArrayList<Map<String, String>>();
        String command = execNamedQuerySingleVal("getGenericNodeObjects", true, "generic_node_select", nodeId);
        if (BaseUtils.isStrEmptyOrWhiteSpace(command)) {
            return null;
        }
        List<Map<String, Object>> result = adhocDao.execNamedQueryCFN("execAnySql", null, command);
        if (result == null) {
            return null;
        }
        for (Map<String, Object> row : result) {
            Map<String, String> rowMap = new LinkedHashMap<String, String>();
            for (Entry<String, Object> entrySet : row.entrySet()) {
                rowMap.put(entrySet.getKey(), entrySet.getValue().toString());
            }
            toReturn.add(rowMap);
        }
        return toReturn;
    }

    public DQMDataErrorIssueBean getDataErrorIssueBean() {
        return adhocDao.createBeanFromNamedQry("getDQMDataErrorIssue", DQMDataErrorIssueBean.class, false, nodeId, true);
    }

    public List<DQMDataErrorIssueBean> getAllDataErrorIssuesInNode() {
        return adhocDao.createBeansFromNamedQry("getAllDQMDataErrorIssuesInNode", DQMDataErrorIssueBean.class, nodeId, true);
    }

    protected Map<Integer, String> getDqmAtrybutyJakosciDanych() {
        if (BaseUtils.countSubstrings(ret.node.branchIds, "|") != 1) {
            return null;
        }

        Map<Integer, String> res = new LinkedHashMap<Integer, String>();

        List<Map<String, Object>> roles = adhocDao.execNamedQueryCFN("getChildNodesIdAndName",
                FieldNameConversion.ToLowerCase, nodeId);
        for (Map<String, Object> m : roles) {
            res.put((Integer) m.get("id" /* I18N: no */), (String) m.get("name" /* I18N: no */));
        }

        return res;
    }

    public DqmGrupaDanychPotomkowieBean getDqmGrupaDanychPotomkowie() {

        Map<Integer, String> attrs = getDqmAtrybutyJakosciDanych();

        if (attrs == null) {
            return null;
        }

        DqmGrupaDanychPotomkowieBean res = new DqmGrupaDanychPotomkowieBean();
        res.atrybutyJakosciDanych = attrs;
        res.testyDlaAtrybutow = new LinkedHashMap<Integer, List<DataQualityGroupBean>>();

        for (Integer attrId : attrs.keySet()) {
            res.testyDlaAtrybutow.put(attrId, adhocDao.createBeansFromNamedQry("getDataQualityLastRequestsEx", DataQualityGroupBean.class, attrId));
        }

        return res;
    }

    public String getCrrLabels() {

//        System.out.println("getCrrLabels: start");
        boolean hasLabels = service.getBOXIServiceImplDataForCurrentRequest().hasCrrLabels;

//        System.out.println("getCrrLabels: hasCrrLabels=" + hasLabels);
        String res;

        if (hasLabels) {
            res = this.<String>execNamedQuerySingleVal("getCrrLabelsForNode", false, null, nodeId);
        } else {
            res = null;
        }

//        System.out.println("getCrrLabels: res=" + res);
        return res;
    }

    public Long getLastModified() {
        return this.<Long>execNamedQuerySingleVal("getRawFileLastModificationDate", false, null, nodeId);
    }

    public String getRawFileDownloadLink() {
        return this.<String>execNamedQuerySingleVal("getRawFileDownloadLink", true, null, nodeId);
    }

    public String getFilePath() {
        return PumpFileUtils.getOriginalPath(this.<String>execNamedQuerySingleVal("getRawTargetFilePath", true, null, nodeId));
    }

    public String getFolderPath() {
        String path = PumpFileUtils.getOriginalPath(this.<String>execNamedQuerySingleVal("getRawTargetFolderPath", true, null, nodeId));
        return path != null ? path.replace("|", "\\") : path;
    }

    public LisaInstanceInfoBean getLisaInstanceInfo() {
        Integer parentId = nodeId;
        LisaInstanceInfoBean bean = null;
        while (parentId != null && bean == null) {
            bean = adhocDao.createBeanFromNamedQry("getLisaInstanceInfoByNodeId", LisaInstanceInfoBean.class, true, parentId);
            parentId = execNamedQuerySingleVal("getParentNodeId", false, null, parentId);
        }
        if (bean == null && ret.node.objId.indexOf(".") > 0) {
            Integer instanceId = BaseUtils.tryParseInteger(ret.node.objId.substring(0, ret.node.objId.indexOf(".")));
            bean = adhocDao.createBeanFromNamedQry("getLisaInstanceInfoById", LisaInstanceInfoBean.class, true, instanceId);
        }
        if (bean != null) {
            adhocDao.execNamedCommand("addDict2Instance", nodeId, bean.id);
        }
        return bean;
    }

    public String getPrintAttribute() {
        String name = ret.node.name;
        String branchNodeId = ret.node.branchIds;
        Integer i = BaseUtils.countSubstrings(ret.node.branchIds, "|");
        Integer parentNodeId = ret.node.parentNodeId;
        int isParent = 0;
        if (i == 5) {
            Integer a = BaseUtils.findNthPos(branchNodeId, "|", 1) + 1;
            Integer b = BaseUtils.findNthPos(branchNodeId, "|", 2);
            parentNodeId = Integer.parseInt(branchNodeId.substring(a, b));
            isParent = 1;
        }

        Map<String, String> m = new HashMap<String, String>();
        List<Map<String, Object>> roles = adhocDao.execNamedQueryCFN("getPatterAndValueAttrPrint", null, name, parentNodeId, isParent);
        if (BaseUtils.isCollectionEmpty(roles)) {
            return "";
        }
        for (Map<String, Object> mm : roles) {
            m.put((String) mm.get("code" /* I18N: no */), (String) mm.get("value" /* I18N: no */));
        }

        if (m.isEmpty()) {
            return "";
        }
        String baPattern = m.get("baWzorzec");
        if (!BaseUtils.isStrEmpty(baPattern)) {
            if (baPattern.equals("hyperlinkInMulti") || baPattern.equals("hyperlinkInMono")) {
                String baLisa = m.get("baLista");
                if (!BaseUtils.isStrEmptyOrWhiteSpace(baLisa)) {
                    return baLisa;
                }
                baPattern = m.get("badWzorzec");
                if (baPattern.equals("hyperlinkInMulti") || baPattern.equals("hyperlinkInMono")) {
                    baLisa = m.get("badLista");
                    return BaseUtils.isStrEmptyOrWhiteSpace(baLisa) ? "" : baLisa;
                }
            }
        } else {
            baPattern = m.get("badWzorzec");
            if (baPattern.equals("hyperlinkInMulti") || baPattern.equals("hyperlinkInMono")) {
                String badLisa = m.get("badLista");
                String baLisa = m.get("baLista");
                return BaseUtils.isStrEmptyOrWhiteSpace(baLisa) ? (BaseUtils.isStrEmptyOrWhiteSpace(badLisa) ? "" : badLisa) : baLisa;
            }
        }
        return "";
    }

//    public Map<String, PrintTemplateBean> getPrintTemplates() {
//
//        List<PrintTemplateBean> printTmp = adhocDao.createBeansFromNamedQry("getPrintTemplates", PrintTemplateBean.class, ret.node.nodeKindCode);
//
//        Map<String, PrintTemplateBean> ptbMap = new HashMap<String, PrintTemplateBean>();
//
//        for (PrintTemplateBean pt : printTmp) {
//            ptbMap.put(pt.template, pt);
//        }
//
//        PrintTemplateBean basic = new PrintTemplateBean();
//        basic.maxLevel = null;
//        basic.showAuthor = true;
//        basic.showNewLine = true;
//        basic.showPageNumber = true;
//        basic.template = "Podstawowy";
//        ptbMap.put(basic.template, basic);
//
////        Map<String, String> templateAndMaxLevel = new HashMap<String, String>();
////
////        List<Map<String, Object>> roles = adhocDao.execNamedQueryCFN("getPrintTemplates", FieldNameConversion.ToLowerCase, ret.node.nodeKindCode);
////        templateAndMaxLevel.put("Podstawowy", null);
////        for (Map<String, Object> m : roles) {
////            String templ = (String) m.get("template" /* I18N: no */);
////            Integer level = (Integer) m.get("maxlevel" /* I18N: no */);
////            templateAndMaxLevel.put(templ, level.toString());
////        }
//        return ptbMap;
//    }
    public boolean getIsTemplatesFolderAdd() {
        Integer i = (Integer) execNamedQuerySingleVal("isTemplatesFolderAdd", true, null /* I18N: no */, nodeId);

        return i == null;
    }
}
