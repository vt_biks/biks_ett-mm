/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.BitSet;
import java.util.Map;
import java.util.Map.Entry;
import simplelib.BaseUtils;
import simplelib.IPreMap;
import simplelib.LameRuntimeException;
import simplelib.PreMapForMap;

/**
 *
 * @author wezyr
 */
public class UrlMakingUtils {

    private static final BitSet dontNeedEncoding = new BitSet(128);

    static {
        int i;
        for (i = 'a'; i <= 'z'; i++) {
            dontNeedEncoding.set(i);
        }
        for (i = 'A'; i <= 'Z'; i++) {
            dontNeedEncoding.set(i);
        }
        for (i = '0'; i <= '9'; i++) {
            dontNeedEncoding.set(i);
        }
        dontNeedEncoding.set(' '); /* encoding a space to a + is done
         * in the encode() method */
        dontNeedEncoding.set('-');
        dontNeedEncoding.set('_');
        dontNeedEncoding.set('.');
        dontNeedEncoding.set('*');
    }

    public static String encodeParamsForUrlWW(Map<String, ?> params) {
        if (params == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        //System.out.println("dupadupa");

        encodeParamsForUrlWW(sb, params, null);

        return sb.toString();
    }

    public static void encodeParamsForUrlWW(StringBuilder sb, IPreMap<String, ?> params,
            String sepBeforeFirst) {
        boolean first = true;

        //for (Entry<String, ?> param : params.entrySet()) {
        for (String paramName : params.keySet()) {
            Object paramVal = //param.getValue();
                    params.get(paramName);
            if (paramVal == null) {
                continue;
            }

            Iterable paramVals = LameUtils.makeIterable(paramVal, true);

            for (Object obj : paramVals) {
                String objStr = BaseUtils.safeToString(obj);
                if (first) {
                    if (sepBeforeFirst != null) {
                        sb.append(sepBeforeFirst);
                    }
                    first = false;
                } else {
                    sb.append("&");
                }
                encodeStringForUrlWW(sb, paramName);
                sb.append("=");
                encodeStringForUrlWW(sb, objStr);
            }
        }
    }

    public static <T> void encodeParamsForUrlWW(StringBuilder sb, Map<String, T> params,
            String sepBeforeFirst) {
        encodeParamsForUrlWW(sb, new PreMapForMap<String, T>(params), sepBeforeFirst);
    }

    public static void appendPercentHexHex(StringBuilder sb, int v) {
        sb.append('%').append(LameUtils.hexDigits[(v >> 4) & 0xf]).append(LameUtils.hexDigits[v & 0xf]);
    }

    public static void encodeCharForUrlWW(char c, StringBuilder sb) {
        if (c <= 0x7f) {
            if (dontNeedEncoding.get(c)) {
                sb.append(c == ' ' ? '+' : c);
            } else {
                appendPercentHexHex(sb, c);
            }
        } else if (c <= 0x7ff) {
            appendPercentHexHex(sb, 0xc0 | (c >> 6));
            appendPercentHexHex(sb, 0x80 | (c & 0x3f));
        } else {
            appendPercentHexHex(sb, 0xe0 | (c >> 12));
            appendPercentHexHex(sb, 0x80 | ((c >> 6) & 0x3f));
            appendPercentHexHex(sb, 0x80 | (c & 0x3f));
        }
    }

    public static void encodeStringForUrlWW(StringBuilder sb, String s) {
        for (int i = 0; i < s.length(); i++) {
            encodeCharForUrlWW(s.charAt(i), sb);
        }
    }

    public static String encodeStringForUrlWW(String s) {
        if (s == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder(s.length() * 3);

        encodeStringForUrlWW(sb, s);

        return sb.toString();
    }

    @Deprecated
    public static String encodeForURLSlow(String s) throws UnsupportedEncodingException {
        if (s == null) {
            return null;
        }
        return URLEncoder.encode(s, "utf-8");
    }

    @Deprecated
    public static String encodeForURLNoExcSlow(String s) {
        try {
            return encodeForURLSlow(s);
        } catch (UnsupportedEncodingException ex) {
            throw new LameRuntimeException("error when encoding string: " + s, ex);
        }
    }

    @Deprecated
    public static String encodeForURLNoExcWWOld2(String s) {
        if (s == null) {
            return null;
        }
        return encodeStringForUrlWW(s);
//        com.ziesemer.utils.codec.impl.URLEncoder ue = new com.ziesemer.utils.codec.impl.URLEncoder();
//        try {
//            ByteBuffer in = ByteBuffer.wrap(s.getBytes("utf-8"));
//            CharBuffer cb = ue.code(in);
//            return cb.toString();
//        } catch (Exception ex) {
//            throw new LameRuntimeException("encode failed", ex);
//        }
    }

    @Deprecated
    public static String encodeParamsForUrlOldWW(Map<String, Object> params) {
        if (params == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        boolean first = true;

        for (Entry<String, Object> param : params.entrySet()) {
            String paramName = param.getKey();
            Object paramVal = param.getValue();
            if (paramVal == null) {
                continue;
            }
            String paramNameEncoded = encodeForURLNoExcWWOld2(paramName);

            Iterable paramVals = LameUtils.makeIterable(paramVal, true);

            for (Object obj : paramVals) {
                String objStr = BaseUtils.safeToStringNotNull(obj);
                if (first) {
                    first = false;
                } else {
                    sb.append("&");
                }
                sb.append(paramNameEncoded).
                        append("=").append(encodeForURLNoExcWWOld2(objStr));
            }
        }

        return sb.toString();
    }

    @Deprecated
    public static void encodeForURLNoExcWWOldLame(String s, StringBuilder sb) {
        if (s == null) {
            return;
        }

        try {
            byte[] bb = s.getBytes("utf-8");

            for (byte b : bb) {
                if (b > 0 && dontNeedEncoding.get(b)) {
                    if (b == ' ') {
                        b = '+';
                    }
                    sb.append((char) b);
                } else {
                    sb.append('%').append(LameUtils.hexDigits[(b >> 4) & 0xf]).append(LameUtils.hexDigits[b & 0xf]);
                }
            }
        } catch (Exception ex) {
            throw new LameRuntimeException("encoding?", ex);
        }
    }

    @Deprecated
    public static String encodeForURLNoExcWWOld(String s) {
        StringBuilder sb = new StringBuilder(s.length() * 3);
        encodeForURLNoExcWWOldLame(s, sb);

        return sb.toString();
    }
}
