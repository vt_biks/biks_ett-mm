/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.IPageDataFetchBroker;
import pl.fovis.client.bikpages.PageDataFetchBroker;
import pl.fovis.client.bikpages.TreeDataFetchAsyncCallback;
import pl.fovis.client.treeandlist.TreeNodeBeanAsMapStorage;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.DisabledUserApplyForAccountMsgsBean;
import pl.fovis.common.MenuExtender;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.basewidgets.IsWidgetVp;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public class ApplyForExtendedAccessFormPanel extends IsWidgetVp {

    protected DisabledUserApplyForAccountMsgsBean duafamb = BIKClientSingletons.getDisabledUserApplyForAccountMsgs();
    protected HTML headerHtml;
    protected HTML footerHtml;
    protected final String NODE_ACTION_APPLY_FOR_EXTENDED_ACCESS = "#ApplyForExtendedAccess"; // + przerobić wyświetlanie drzewka!
    protected final Integer NODE_ACTION_ID_SHOW_IN_TREE = BIKClientSingletons.getNodeActionIdByCode(BIKConstants.NODE_ACTION_CODE_SHOW_IN_TREE);
    protected final Integer NODE_ACTION_ID_NEW_NESTABLE_PARENT = BIKClientSingletons.getNodeActionIdByCode("NewNestableParent");
    protected final boolean isEffectiveExpertLoggedIn = BIKClientSingletons.isEffectiveExpertLoggedInEx(NODE_ACTION_APPLY_FOR_EXTENDED_ACCESS);
//    protected ApplyForExtendedAccessTreeOfTrees tot = new ApplyForExtendedAccessTreeOfTrees(new IContinuation() {
//
//        @Override
//        public void doIt() {
//            setProperMailToPart();
//        }
//    });
    public IPageDataFetchBroker dataFetchBroker = new PageDataFetchBroker();

    protected Widget totWidget;

    ApplyForExtendedAccessTreeAndRootTreeWidget bikTree = new ApplyForExtendedAccessTreeAndRootTreeWidget(0, "",
            dataFetchBroker,
            new IContinuation() {

                @Override
                public void doIt() {
                    setProperMailToPart();
                }
            },
            TreeNodeBeanAsMapStorage.CHECKBOX + "=10:",
            TreeNodeBeanAsMapStorage.NAME_FIELD_NAME + "=300:Nazwa",
            TreeNodeBeanAsMapStorage.RADIOBUTTONO + "=100:",
            TreeNodeBeanAsMapStorage.RADIOBUTTONW + "=100:");

    // brzydki hak dla integracji z java scriptem
    protected static ApplyForExtendedAccessFormPanel inst;
    protected String link;

    public ApplyForExtendedAccessFormPanel(String link) {
        this.link = link;
        inst = this;
    }

    protected String encodeForMailToUrl(String part) {
        if (!GWTUtils.isBrowserIE()) {
            return URL.encodeQueryString(part).replace("+", "%20") /*+ " Zażółć gęślą jaźń"*/;
        }

        String res = part; //+ " - Zażółć gęślą jaźń!";

        StringBuilder sb = new StringBuilder();

        int len = res.length();

        for (int i = 0; i < len; i++) {
            char c = res.charAt(i);
            if (c != '\n' && c != '\r') {

                String sss = BaseUtils.encodePolishCharsAsHtmlEntities(c + "");
//                String sss = BaseUtils.encodeCharsForHtmlAsIso8859_2(c + "");

                if (sss.length() > 1) {
                    sb.append(sss);
                } else {
                    sb.append("&#").append(Integer.toString(c)).append(";");
                }
            } else {
                sb.append(c);
            }
        }

        res = sb.toString();

        res = BaseUtils.replaceNewLinesToRowSep(res, "%0D%0A") /*+ " Zażółć gęślą jaźń"*/;

//        Window.alert(res);
        //res = URL.encodeQueryString(res).replace("+", "%20");
        return res;
    }

    protected Pair<String, String> generateSubjectAndBody() {
        String subject = duafamb.mailTitle;
        String body = duafamb.mailBody;

        List<String> selectedOptions = generateSelectedOptionsTextForMailTo();

        if (selectedOptions.isEmpty()) {
            //return "javascript: alert('Nie wybrano żadnych opcji');";
            return null;
        }

        Collections.sort(selectedOptions, new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                return String.CASE_INSENSITIVE_ORDER.compare(o1, o2);
            }
        });

        String options1 = BaseUtils.mergeWithSepEx(selectedOptions, ", ");
        String optionsN = BaseUtils.mergeWithSepEx(selectedOptions, "\r\n\r\n");

//        String ssoUser = BIKClientSingletons.getSsoUserName();
        String ssoUser = BaseUtils.nullToDef(BIKClientSingletons.getLoggedUserLoginName(), BIKClientSingletons.getSsoUserName());

        SystemUserBean sysUsr = BIKClientSingletons.getLoggedUserBean();

        String userName = sysUsr == null || BaseUtils.isStrEmptyOrWhiteSpace(sysUsr.userName) ? "" /*"???"*/ : sysUsr.userName;
        String userNameP = sysUsr == null || BaseUtils.isStrEmptyOrWhiteSpace(sysUsr.userName) ? "" : " (" + sysUsr.userName + ")";

        String ssoUserP;
        // drut dla Bżemga...
        String systemBIKS = BIKClientSingletons.getBiksDatabaseName().contains("TEST") ? "BIKS (Testowy)" : "BIKS (Produkcyjny)";

        if (BaseUtils.isStrEmptyOrWhiteSpace(ssoUser)) {
            ssoUser = I18n.uzytkownikNierozpoznany.get();
            ssoUserP = "";
        } else {
            if (BaseUtils.isStrEmptyOrWhiteSpace(userName)) {
                ssoUserP = ssoUser;
            } else {
                ssoUserP = " (" + ssoUser + ")";
            }
        }

        body = body.replace("%{options1}%", options1).replace("%{optionsN}%", optionsN).replace("%{ssoUser}%", ssoUser).replace("%{userName}%", userName).replace("%{ssoUserP}%", ssoUserP).replace("%{userNameP}%", userNameP).replace("systemu BIKS", "systemu " + systemBIKS);
        subject = subject.replace("%{ssoUser}%", ssoUser).replace("%{userName}%", userName).replace("%{ssoUserP}%", ssoUserP).replace("%{userNameP}%", userNameP).replace("BIKS", systemBIKS);

        return new Pair<String, String>(subject, body);
    }

    protected String createLinkInEmail(String treeCode, int nodeId, String name) {
        StringBuilder url = new StringBuilder();
        url.append("<" + "a" /* I18N: no */).append(" " + "href=" /* I18N: no */)
                .append(link)
                //                .append("http://192.168.56.1:8888")
                .append("/#")
                .append(treeCode)
                .append("/").append(nodeId > 0 ? nodeId : "")
                .append(" style=\"text-decoration: none ;color: #094f8b\"") //#094f8b
                .append(">&nbsp;")
                .append(name)
                .append("</a>");
        return url.toString();
    }

    protected String generateMailTo() {

        if (BIKClientSingletons.canSendMailViaServer) {
            return "javascript: afeafpSendMailViaServer();";
        }

        Pair<String, String> p = generateSubjectAndBody();

        if (p == null) {
            return "javascript: alert('Nie wybrano żadnych opcji');";
        }

        String subject = p.v1;
        String body = p.v2;

        //body = BaseUtils.replaceNewLinesToRowSep(body, "%0D%0A")/*.replace(" ", "%20")*/;
        String mailToText = "mailto:" + duafamb.mailTo + "?subject=" + encodeForMailToUrl(subject) + "&body=" + encodeForMailToUrl(body);

        if (!GWTUtils.isBrowserIE()) {
            return mailToText;
        }
        setMailToTextForOpenInNewWindow(mailToText);
        String res = "javascript: openMailToInNewWindow();";

        return res;
    }

    protected void setProperMailToPart() {
        final String mailToTxt = generateMailTo();
        headerHtml.setHTML(duafamb.headerMsg.replace("mailto:Admin@Of@BIKS", mailToTxt));
        footerHtml.setHTML(duafamb.footerMsg.replace("mailto:Admin@Of@BIKS", mailToTxt));
    }

    protected void generateWidgetsForOptionsSelection() {

//        totWidget = tot.buildWidgets();
//        totWidget.setWidth("550px");
//        mainWidget.add(totWidget);
        Widget treeWidget = bikTree.getTreeGridWidget();
        mainWidget.add(treeWidget);
        treeWidget.setWidth("900px");
        fetchData();
    }

    protected List<String> generateSelectedOptionsTextForMailTo() {
        List<String> res = new ArrayList<String>();

        for (int i = 0; i < bikTree.getOptionCount(); i++) {
            if (bikTree.isOptionSelected(i)) {

                TreeNodeBean mnb = bikTree.getOptionMenuNodeBean(i);

                boolean isRegularUserTree = true;

                if (mnb != null /*&& mnb.treeId != null*/) {
                    isRegularUserTree = BIKClientSingletons.isRegularUserTree(BIKClientSingletons.getTreeBeanById(Math.abs(mnb.treeId)));
                }

//                final boolean isReadOnlyAccessForOption = true;// bikTree.isReadOnlyAccessForOption(i);
//                String roleTxt;
//
//                if (isRegularUserTree) {
//                    roleTxt = isReadOnlyAccessForOption ? I18n.zwyklyUzytkownik.get() : I18n.autor.get();
//                } else {
//                    roleTxt = isReadOnlyAccessForOption ? "Użytkownik DQM" : "Redaktor DQM";
//                }
                res.add(createLinkInEmail(
                        mnb.treeCode,
                       /* "DQM",*/ bikTree.getOptionMenuNodeBean(i).id, bikTree.getOptionName(i)));

//                res.add(bikTree.getOptionName(i) /*+ " (rola: " + roleTxt + ")"*/);
            }
        }

        return res;
    }

    public static native void setMailToTextForOpenInNewWindow(String mailToHref) /*-{
     $wnd.mailToTextForOpenInNewWindow = mailToHref;
     }-*/;

//    public static native void openMailToInNewWindow(String mailToHref) /*-{
//
//     }-*/;
    public static native void exportOpenMailToInNewWindow() /*-{
     //$wnd.alert('before export');
     //     $wnd.openMailToInNewWindow = $entry(@pl.fovis.client.bikwidgets.ApplyForExtendedAccessFormPanel::openMailToInNewWindow());
     $wnd.openMailToInNewWindow = function () {
     //alert('mailToTextForOpenInNewWindow=' + $wnd.mailToTextForOpenInNewWindow);
     if (false) {
     var jswin = $wnd.open($wnd.mailToTextForOpenInNewWindow, "jswin", "");
     return;
     }
     var jswin = $wnd.open("about:blank", "jswin", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=100, height=50");
     jswin.document.write('<html><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1250" />');
     jswin.document.write('<scr'+'ipt>');
     jswin.document.write('(function() {location.href="' + $wnd.mailToTextForOpenInNewWindow + '";setTimeout(function() { window.close(); }, 5000);})();</scr'+'ipt>');
     jswin.document.write('</head>');
     jswin.document.write('Otwieram program pocztowy...<br/>Po kilku sekundach to okienko zamknie się automatycznie.');
     jswin.document.write('</body></html>');
     };
     //$wnd.alert('exported: ' + $wnd.openMailToInNewWindow);
     }-*/;

    public static native void exportSendMailViaServer() /*-{
     $wnd.afeafpSendMailViaServer = $entry(@pl.fovis.client.bikwidgets.ApplyForExtendedAccessFormPanel::sendMailViaServer());
     }-*/;

    static {
        exportOpenMailToInNewWindow();
        exportSendMailViaServer();
    }

    protected void sendMailViaServerProper() {
        Pair<String, String> p = generateSubjectAndBody();

        if (p == null) {
            Window.alert("Nie wybrano żadnych opcji");
            return;
        }

        BIKClientSingletons.showInfo("Wysyłam wniosek");

        BIKClientSingletons.getService().sendMail(p.v1, BaseUtils.replaceNewLinesToRowSep(p.v2, "<br/>"), new String[]{duafamb.mailTo}, new StandardAsyncCallback<Void>() {

            @Override
            public void onSuccess(Void result) {
                BIKClientSingletons.showInfo("Wniosek wysłany");
            }
        });
    }

    public static void sendMailViaServer() {
        //Window.alert("sendMailViaServer: ok!");
        inst.sendMailViaServerProper();
    }

    protected void generateApplyForAccountPart() {

        headerHtml = new HTML(duafamb.headerMsg);
        footerHtml = new HTML(duafamb.footerMsg);

//        footerHtml.addClickHandler(new ClickHandler() {
//
//            @Override
//            public void onClick(ClickEvent event) {
//                //Window.alert("will open new window");
//                //Window.open("http://onet.pl", null, null);
//                openInNewWindow(generateMailTo());
//            }
//        });
        mainWidget.add(headerHtml);

        generateWidgetsForOptionsSelection();

        setProperMailToPart();

        mainWidget.add(footerHtml);
    }

    @Override
    protected void initCreatedMainWidget() {
        mainWidget.setWidth("100%");
    }

    @Override
    protected void fillMainWidget() {
        generateApplyForAccountPart();
//        mainWidget.setVisible(tot.getOptionCount() > 0);
        mainWidget.setVisible(!BaseUtils.isCollectionEmpty(getTrees()));
    }

    protected List<MenuNodeBean> extendMenuNodes() {
        List<MenuNodeBean> lmn = BIKClientSingletons.getExtendedMenuNodes(
                BIKClientSingletons.getMenuNodes(), getTrees(), BIKClientSingletons.getNodeKindsByCodeMap(),
                true, MenuExtender.MenuBottomLevel.NodeTrees,
                false, true);
        List<MenuNodeBean> newExtendMenuNodes = new ArrayList<MenuNodeBean>();
        for (MenuNodeBean mn : lmn) {
  /* dla PB zakomentowane*/
//            if (mn.code.equals("dqm") || mn.code.equals("$DQM")) {
            newExtendMenuNodes.add(mn);
//            }
        }
        return newExtendMenuNodes;
//        
//        return BIKClientSingletons.getExtendedMenuNodes(
//                BIKClientSingletons.getMenuNodes(), getTrees(), BIKClientSingletons.getNodeKindsByCodeMap(),
//                true, MenuExtender.MenuBottomLevel.NodeTrees,
//                false, true);
    }

    protected void fetchData() {
        callServiceToGetData(new TreeDataFetchAsyncCallback<List<TreeNodeBean>>(dataFetchBroker) {
            protected void doOnSuccess(List<TreeNodeBean> result) {
                processAndShowNodes(result);
            }
        });
    }

    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        Set<String> treeCodes = new HashSet<String>();
        for (TreeBean tb : getTrees()) {
            treeCodes.add(tb.code);

        }
        BIKClientSingletons.getService().getRootTreeNodes4ApplyForExtendedAccess(treeCodes, callback);
    }

    protected Collection<TreeBean> getTrees() {

        List<TreeBean> res = new ArrayList<TreeBean>();

        if (BIKClientSingletons.isAppAdminLoggedIn(BIKConstants.ACTION_OTHER_ACTIONS_APP_ADMIN)) {
            return res;
        }

        Collection<TreeBean> allTrees = BIKClientSingletons.getAllTrees();

        for (TreeBean tb : allTrees) {

            boolean isRegularUserTree = BIKClientSingletons.isRegularUserTree(tb);
            final int treeId = tb.id;

            if (isRegularUserTree) {

                if (isEffectiveExpertLoggedIn || BIKClientSingletons.isLoggedUserAuthorOfTree(treeId) || BIKClientSingletons.isLoggedUserCreatorOfTree(treeId)
                        || BIKClientSingletons.isLoggedUserIsNonPublicUserInTree(treeId)) {
                    continue;
                }

                if (tb.nodeKindId == null && BIKClientSingletons.isNodeActionGrantedInTreeRoot(treeId, NODE_ACTION_ID_SHOW_IN_TREE)) {
                    continue;
                }

            } else {
                if (BIKClientSingletons.isNodeActionGrantedInTreeRoot(treeId, NODE_ACTION_ID_NEW_NESTABLE_PARENT)) {
                    continue;
                }
            }
            
            /* dla PB*/
            if (tb.code.equals("DQM") || tb.treeKind.startsWith("RODO")) {
                res.add(tb);
            }
        }

        return res;
    }

    protected void processAndShowNodes(List<TreeNodeBean> result) {
        groupResultsByMenu(result);
        bikTree.initWithFullData(result, true, null, null);
        List<Integer> test = new ArrayList<Integer>();
        for (TreeNodeBean t : result) {
            if (t.treeId == -1) {
                test.add(t.id);
            }
        }
        bikTree.expandNodesByIds(test);
    }

    protected void groupResultsByMenu(List<TreeNodeBean> result) {
        List<MenuNodeBean> menuItems = extendMenuNodes();

        Map<Integer, TreeNodeBean> menuItemsWithoutTrees = new HashMap<Integer, TreeNodeBean>();

        List<TreeNodeBean> newRootNodesAsList = new ArrayList<TreeNodeBean>();
        Map<Integer, TreeNodeBean> newTreeRootNodes = new HashMap<Integer, TreeNodeBean>();
        for (MenuNodeBean mnb : menuItems) {

            if (mnb.nodeKindId == null) {

                TreeNodeBean newRoot = new TreeNodeBean();
                newRoot.id = -mnb.id; //newRootId--;
                newRoot.name = mnb.name;
                //mm-> poprawka na szybko, żeby było po polsku - do poprawienia, jak będzie koncepcja
                newRoot.nodeKindCode = I18n.kategoria.get() /* I18N:  *//*"Category"*/;
                newRoot.nodeKindCaption = I18n.kategoria.get() /* I18N:  */;
                newRoot.nodeKindId = -1;
                newRoot.parentNodeId = mnb.parentNodeId == null ? null : -mnb.parentNodeId;
                newRoot.branchIds = mnb.parentNodeId == null ? (-mnb.id) + "|" : ((-mnb.parentNodeId) + "|" + (-mnb.id) + "|");
                newRoot.treeId = mnb.treeId == null ? -1 : -mnb.treeId; //-1;
                newRoot.treeCode = mnb.code;
                newRoot.treeName = mnb.name;
                newRoot.visualOrder = menuItems.indexOf(mnb); // do sortowania
                if (mnb.treeId != null) {
                    newTreeRootNodes.put(mnb.treeId, newRoot);
                    newRootNodesAsList.add(newRoot);
                } else {
                    menuItemsWithoutTrees.put(newRoot.id, newRoot);
                }
            }
        }

        List<TreeNodeBean> rootDataNodes
                = BIKCenterUtils.getRootNodes(result);
        if (newTreeRootNodes.size() >= 1) {

            for (Map.Entry<Integer, TreeNodeBean> tnb : newTreeRootNodes.entrySet()) {
                getAndAddAncestors(tnb.getValue(), newRootNodesAsList, menuItemsWithoutTrees);
            }

            Collections.sort(newRootNodesAsList, new Comparator<TreeNodeBean>() {

                @Override
                public int compare(TreeNodeBean o1, TreeNodeBean o2) {
                    if (o1.visualOrder > o2.visualOrder) {
                        return 1;
                    }
                    if (o1.visualOrder < o2.visualOrder) {
                        return -1;
                    }
                    return 0;
                }
            });
            result.addAll(newRootNodesAsList);

            for (TreeNodeBean tnb : rootDataNodes) {
                tnb.parentNodeId = newTreeRootNodes.get(tnb.treeId) != null ? newTreeRootNodes.get(tnb.treeId).id : null;
            }

            for (TreeNodeBean tnb : result) {
                if (tnb.treeId > 0) { // normalne obiekty, bez sztucznych
                    tnb.branchIds = (tnb.parentTreeId == null ? (-tnb.treeId) + "|" : (-tnb.parentTreeId) + "|" + ((-tnb.treeId) + "|")) + tnb.branchIds;
                }
            }

        }
    }

    protected void getAndAddAncestors(TreeNodeBean tnb, List<TreeNodeBean> newRootNodesAsList, Map<Integer, TreeNodeBean> menuItemsWithoutTrees) {
        TreeNodeBean newTnb = menuItemsWithoutTrees.get(tnb.parentNodeId);
        if (newTnb != null && !newRootNodesAsList.contains(newTnb)) {
            newRootNodesAsList.add(newTnb);
            getAndAddAncestors(newTnb, newRootNodesAsList, menuItemsWithoutTrees);
        }
    }
}
