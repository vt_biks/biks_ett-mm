/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.dev.gwt;

import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JField;
import com.google.gwt.core.ext.typeinfo.JGenericType;
import com.google.gwt.core.ext.typeinfo.JParameterizedType;
import com.google.gwt.core.ext.typeinfo.JType;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import simplelib.i18nsupport.I18nMessage;
import simplelib.i18nsupport.IDynamicI18nMessages;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public class FoxyDynamicI18nMessagesGenerator extends FoxyGeneratorBase {

    @Override
    protected void addImports() {
        super.addImports();
//        addImport(LameRuntimeException.class);
//        addImport(IObjectCreator.class);
//        addImport(GWT.class);
        addImport(Set.class);
        addImport(Map.class);
        addImport(LinkedHashMap.class);
        addImport(I18nMessage.class);
        addImport(IDynamicI18nMessages.class);
    }

    @Override
    protected void generateClassBody() throws UnableToCompleteException {

        JGenericType intfType = getClassType(IDynamicI18nMessages.class).isGenericType();
        if (intfType == null) {
            throwUnableToComplete("cannot get dynamic class creator as generic type");
        }

        JParameterizedType parametrization = genClassType.asParameterizationOf(intfType);
        if (parametrization == null) {
            throwUnableToComplete("cannot get parametrization of dynamic class creator for class: \"" + genClassName + "\"");
        }

        JClassType[] typeArgs = parametrization.getTypeArgs();

        if (typeArgs.length != 1) {
            throwUnableToComplete("parametrization for class \"" + genClassName
                    + "\" has " + typeArgs.length + " " + "typeArgs, but should have just 1");
        }

        JClassType oneTypeArg = typeArgs[0];
        JGenericType oneArgGenericType = oneTypeArg.isGenericType();
        if (oneArgGenericType != null) {
            throwUnableToComplete("type arg of IDynamicI18nMessages for class \"" + genClassName
                    + "\" is generic type \"" + oneTypeArg.getName() + "\"");
        }

        String classOfCreator = oneTypeArg.getParameterizedQualifiedSourceName();
        log("IDynamicI18nMessages \"" + genClassName
                + "\" gives access to i18n message variables in class \"" + classOfCreator + "\"");

        addImport(classOfCreator);

        JClassType clz = typeOracle.findType(classOfCreator);

        Set<JField> flds = getAllFields(clz);

        String clzSimpleName = clz.getName();

        JClassType i18nClassType = getClassType(I18nMessage.class);

        println("private final Map<String, I18nMessage> access = new LinkedHashMap<String, I18nMessage>();");
        println("{");
        for (JField fld : flds) {
            if (!fld.isStatic() || fld.isPrivate()) {
                continue;
            }

            JType ft = fld.getType();
            if (ft == null) {
                continue;
            }

            JClassType fct = ft.isClass();
            if (fct == null) {
                continue;
            }

            if (!fct.isAssignableTo(i18nClassType)) {
                continue;
            }

            println("access.put(\"" + fld.getName() + "\", " + clzSimpleName + "." + fld.getName() + ");");
        }
        println("}");

        println("public I18nMessage find(String key) { return access.get(key); }");
        println("public Set<String> getKeys() { return access.keySet(); }");
    }
}
