/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.gwtservlet;

import java.util.Set;

/**
 *
 * @author pmielanczuk
 */
public interface IServiceImplWithLocaleNamesOverride {

    public Set<String> getSupportedLocaleNames();
}
