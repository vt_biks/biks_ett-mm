/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author młydkowski
 */
public class ConfirmDialog extends BaseActionOrCancelDialog {

    //protected Label status;
//    protected Label komunikat;
    protected Widget messageBodyWidget;
    protected IParametrizedContinuation<Boolean> closeCont;
    protected String actionBtnCaption;
    protected String cancelBtnCaption;
    protected String dialogCaption;

    protected String getDefaultDialogCaption() {
        return I18n.potwierdz.get() /* I18N:  */;
    }

    protected String getDefaultActionBtnCaption() {
        return "OK" /* I18N: no */;
    }

    protected String getDefaultCancelBtnCaption() {
        return super.getCancelButtonCaption();
    }

    @Override
    protected String getDialogCaption() {
        return dialogCaption;
    }

    @Override
    protected String getActionButtonCaption() {
        return actionBtnCaption;
    }

    @Override
    protected String getCancelButtonCaption() {
        return cancelBtnCaption;
    }

    @Override
    protected void buildMainWidgets() {
        main.add(messageBodyWidget);
    }

    @Override
    protected void doAction() {
        closeCont.doIt(true);
    }

    @Override
    protected void doCancel() {
        closeCont.doIt(false);
    }

    public void buildAndShowDialog(String text, IContinuation saveCont) {
        buildAndShowDialog(text, I18n.tak.get() /* I18N:  */, saveCont);
    }

    public void buildAndShowDialog(String text, String actionButtonCaption, IContinuation saveCont) {
        HTML mbw = generateMessageBodyWidget(text);
        buildAndShowDialog(mbw, actionButtonCaption, saveCont);
    }

    public HTML generateMessageBodyWidget(String htmlTxt) {
        //this.attr = new EntityAttribute();
        HTML mbw = new HTML(htmlTxt);
        mbw.setStyleName("komunikat" /* I18N: no */);
        return mbw;
    }

    public void buildAndShowDialog(Widget messageBodyWidget, String actionButtonCaption,
            final IContinuation saveCont) {
        buildAndShowDialog(messageBodyWidget, getDefaultDialogCaption(), actionButtonCaption,
                getDefaultCancelBtnCaption(), new IParametrizedContinuation<Boolean>() {
                    public void doIt(Boolean param) {
                        if (param) {
                            saveCont.doIt();
                        }
                    }
                });
    }

    public void buildAndShowDialog(String htmlTxt, String dialogCaption, String actionBtnCaption,
            String cancelBtnCaption, final IParametrizedContinuation<Boolean> closeCont) {
        Widget w = generateMessageBodyWidget(htmlTxt);
        buildAndShowDialog(w, dialogCaption, actionBtnCaption, cancelBtnCaption, closeCont);
    }

    public void buildAndShowDialog(Widget messageBodyWidget, String dialogCaption, String actionBtnCaption,
            String cancelBtnCaption, final IParametrizedContinuation<Boolean> closeCont) {
        this.messageBodyWidget = messageBodyWidget;

        this.closeCont = closeCont;
        this.dialogCaption = dialogCaption;
        this.actionBtnCaption = actionBtnCaption;
        this.cancelBtnCaption = cancelBtnCaption;

        super.buildAndShowDialog();
    }

    public void updateBodyTextAndOrTitle(String optNewBodyText, String optNewTitle) {
        if (optNewBodyText != null) {
            Widget w = generateMessageBodyWidget(optNewBodyText);
            main.remove(messageBodyWidget);
            messageBodyWidget = w;
            main.insert(messageBodyWidget, 0);
        }
        if (optNewTitle != null) {
            dialogCaption = optNewTitle;
            updateDialogCaption();
        }
    }
}
