/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.ListBox;
import java.util.HashMap;
import java.util.Map;
import static pl.fovis.client.dialogs.dqm.AddOrEditDQMTestDialogBase.addLabel;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.fovis.common.dqm.DataQualityTestBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class AddOrEditDQMTestDialogForConnections extends AddOrEditDQMTestDialogBase {

    protected Map<Integer, Integer> indexToConnectionIdMap;

    @Override
    protected void addSourcePanel() {
        multiWidget = new DQMWidgetConfigMultiSourceForConnections(result.v2);

        multiWidget.getWidget(nodeId);
        indexToConnectionIdMap = new HashMap<Integer, Integer>();
        connectionWidget = new DQMWidgetConfigConnectionSource();
        connectionWidget.getWidget(nodeId);
        sourcePanel.add(addLabel(I18n.dqmPolaczenie.get()));

        lbxOneSource = new ListBox();
        int index = 0;

        for (ConnectionParametersDQMConnectionsBean cp : result.v2.values()) {
            lbxOneSource.addItem(cp.name);
            indexToConnectionIdMap.put(index++, cp.id);
        }
        lbxOneSource.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
//                setSelectedSourceWidget();
                cleanWidgets();
            }
        });
        sourcePanel.add(lbxOneSource);
    }

    @Override
    protected void addToSourceWidget() {
        sourceWidget.add(connectionWidget.getWidget(nodeId));
    }

    protected void setWidgetVisibility(boolean isMulti) {
        sourcePanel.setVisible(!isMulti);
        scopePanel.setVisible(!isMulti);
        connectionWidget.getWidget(nodeId).setVisible(true);

        multiWidget.getWidget(nodeId).setVisible(isMulti);

        if (!isMulti) {

            setSelectedSourceWidget(connectionWidget.getWidget(nodeId));

        } else {
            setSelectedSourceWidget(multiWidget.getWidget(nodeId));
        }
    }

    @Override
    protected IDQMWidgetConfig getSelectedWidget() {
        if (oneSourceTest.getValue()) {
            return connectionWidget;
        } else {
            return multiWidget;
        }
    }

    @Override
    protected int getSelectedIdx() {
        int index = 0;
        for (Map.Entry<Integer, Integer> id : indexToConnectionIdMap.entrySet()) {
            if (bean.connectionId != null && id.getValue().equals(bean.connectionId)) {
                return id.getKey();

            }
        }
        return index;
    }

    protected void setOtherValueInBean(DataQualityTestBean bean) {
        bean.connectionId = oneSourceTest.getValue() ? indexToConnectionIdMap.get(lbxOneSource.getSelectedIndex()) : null;
        bean.sourceName = oneSourceTest.getValue() ? result.v2.get(bean.connectionId).sourceType : null;
    }
}
