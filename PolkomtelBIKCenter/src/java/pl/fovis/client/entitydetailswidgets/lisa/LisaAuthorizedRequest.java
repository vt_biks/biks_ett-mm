/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.lisa.LisaAuthorizationDialog;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.TeradataException;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tbzowka
 * @param <T>
 */
public abstract class LisaAuthorizedRequest<T> {

    protected Integer lisaId;
    protected String lisaName;
    protected Integer nodeId;
    protected boolean hasBeenCheckedLogin = false;

    public LisaAuthorizedRequest(Integer lisaId, String lisaName) {
        this.lisaId = lisaId;
        this.lisaName = lisaName;
    }

    private AsyncCallback<T> callback = new StandardAsyncCallback<T>() {
        @Override
        public void onFailure(Throwable caught) {
            if (caught instanceof TeradataException) {
                doSomethingOptOnFailure();
                final TeradataException e = (TeradataException) caught;
                if (e.isBlockingProblem()) {
                    onActionFailure(e);
                    return;
                }
                if (!hasBeenCheckedLogin && nodeId != null) {
                    hasBeenCheckedLogin = true;
                    BIKClientSingletons.getLisaService().tryLoginToTeradata(lisaId, nodeId, new AsyncCallback<Boolean>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            showLoginDialog();
                        }

                        @Override
                        public void onSuccess(Boolean result) {
                            go();
                        }
                    });
                } else {
                    showLoginDialog();
                }
            } else {
//                super.onFailure(caught);
                onActionFailure(caught);
            }
        }

        @Override
        public void onSuccess(T result) {
            onActionSuccess(result);
            showSuccessInfo();
        }
    };

    private void showLoginDialog() {
        new LisaAuthorizationDialog().buildAndShowDialog(lisaName, new IParametrizedContinuation<List<String>>() {
            @Override
            public void doIt(List<String> authorizationData) {
                BIKClientSingletons.getLisaService().authorizeTeradataConnection(lisaId, authorizationData.get(0), authorizationData.get(1), new StandardAsyncCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        hasBeenCheckedLogin = true;
                        go();
                    }
                });
            }
        });
    }

    public LisaAuthorizedRequest setNodeId(int nodeId) {
        this.nodeId = nodeId;
        return this;
    }

    protected void showSuccessInfo() {
        BIKClientSingletons.showInfo(I18n.gotowe.get());
    }

    protected abstract void performServerAction(AsyncCallback<T> callback);

    public void go() {
        if (doBeforeAction()) {
            BIKClientSingletons.showInfo(I18n.zadanieWToku.get() /* i18n:  */);
            performServerAction(callback);
        }
    }

    public abstract void onActionSuccess(T result);

    public boolean doBeforeAction() {
        return true;
    }

    public void doSomethingOptOnFailure() {
        // NO OP
    }

    public void onActionFailure(Throwable caught) {
        if (caught instanceof TeradataException) {
            TeradataException e = (TeradataException) caught;
            BIKClientSingletons.showWarning(e.getMessage());
            return;
        }
        BIKClientSingletons.showWarning(I18n.bladKomunikacjiZTeradata.get());
    }
}
