/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.SapBoExPropConfigBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.AdminConfigBaseWidget;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author ctran
 */
public class AdminConfigSapBoExtPropWidget extends AdminConfigBaseWidget<SapBoExPropConfigBean> {

    protected TextBox tbxPath;
    protected HorizontalPanel hpTop;

    @Override
    protected void buildInnerWidget() {
        hpTop = new HorizontalPanel();
        hpTop.setSpacing(10);
        buildTop(hpTop);
        main.add(hpTop);
    }

    @Override
    public void populateWidgetWithData(SapBoExPropConfigBean data) {
        tbxPath.setText(data.path);
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_SAPBO;
    }

    protected void buildTop(CellPanel panelTop) {
        tbxPath = new TextBox();
        tbxPath.setWidth("552px");
        panelTop.add(new Label(I18n.sciezkaDoKonektoraDesigner.get()));
        panelTop.add(tbxPath);
        createButton(I18n.zapisz.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                callServiceToSaveBODesignerPath(tbxPath.getText(), new StandardAsyncCallback<Void>("error in" /* I18N: no:err-fail-in */ + " setBODesignerPath") {
                            @Override
                            public void onSuccess(Void result) {
                                BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get());
                            }
                        });
            }
        }, panelTop);
    }

    protected void callServiceToSaveBODesignerPath(String path, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().setBODesignerPath(path, getSourceName(), asyncCallback);
    }
}
