/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.widgets.sbavc;

import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import pl.fovis.common.sbavc.SearchByAttrValueCriteriaStartsWith;

/**
 *
 * @author tflorczak
 */
public class SearchByAttrValueCriteriaStartsWithWidget extends SearchByAttrValueCriteriaWidgetBase {

    protected TextBox startsWithTbx = new TextBox();

    @Override
    protected void createOptWidget() {
        row.setWidget(0, 4, startsWithTbx);
    }

    @Override
    public ISearchByAttrValueCriteria provideBean() {
        return (isValidInput() ? new SearchByAttrValueCriteriaStartsWith(getSelectedAttrId(), getSelectedAttrCaption(), startsWithTbx.getText(), negationChbx.getValue()) : null);
    }
}
