/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import static pl.fovis.client.BIKClientSingletons.fixFileResourceUrlEx;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.client.entitydetailswidgets.lisa.LisaAuthorizedRequest;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryBean;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.SimpleDateUtils;

/**
 *
 * @author ctran
 */
public class LisaExportDictionaryDialog extends BaseActionOrCancelDialog {

    protected LisaDictionaryBean selectedDict;
    protected List<LisaDictColumnBean> allColumnsOfSelectedDict;
    protected List<TextBox> listNames = new ArrayList<TextBox>();

    @Override
    protected String getDialogCaption() {
        return I18n.mapowanieNazwKolumn.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.eksport.get();
    }

    @Override
    protected void buildMainWidgets() {
        ScrollPanel scrollPanel = new ScrollPanel();
        scrollPanel.setHeight("400px");
        scrollPanel.setWidth("400px");
        Grid grid = new Grid(allColumnsOfSelectedDict.size() + 1, 2);
        int row = 0;
        grid.setWidget(row, 0, new HTML("<b>" + I18n.nazwaKolumny.get() + "</b>"));
        grid.setWidget(row++, 1, new HTML("<b>" + I18n.nazwaEksportu.get() + "</b>"));
        for (LisaDictColumnBean column : allColumnsOfSelectedDict) {
            TextBox tb = new TextBox();
            listNames.add(tb);
            tb.setText(column.titleToDisplay);

            grid.setWidget(row, 0, new Label(column.nameInDb));
            grid.setWidget(row++, 1, tb);
        }
        scrollPanel.add(grid);
        main.add(scrollPanel);
    }

    public void buildAndShowDialog(LisaDictionaryBean selectedDict, List<LisaDictColumnBean> allColumnsOfSelectedDict) {
        this.selectedDict = selectedDict;
        this.allColumnsOfSelectedDict = allColumnsOfSelectedDict;
        super.buildAndShowDialog();
    }

    @Override
    protected void doAction() {
        final List<String> mappedNames = new ArrayList<String>();
        for (int i = 0; i < allColumnsOfSelectedDict.size(); i++) {
            mappedNames.add(listNames.get(i).getText());
        }
        new LisaAuthorizedRequest<String>(selectedDict.lisaId, selectedDict.lisaName) {
            private BIKSProgressInfoDialog infoDialog;

            @Override
            protected void performServerAction(AsyncCallback<String> callback) {
                infoDialog = new BIKSProgressInfoDialog();
                infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaPrzygotowanieEksportu.get(), true);
                BIKClientSingletons.getLisaService().exportDictionary(selectedDict.lisaId, selectedDict.nodeId, mappedNames, callback);
            }

            @Override
            public void onActionSuccess(String serverFileName) {
                BIKClientSingletons.showInfo(I18n.gotowe.get());
                infoDialog.hideDialog();
                String fileType = serverFileName.substring(serverFileName.lastIndexOf("."), serverFileName.length());
                Window.open(fixFileResourceUrlEx(BaseUtils.encodeForHTMLTag(serverFileName),
                        selectedDict.displayName + "-" + SimpleDateUtils.dateToString(new Date()) + fileType),
                        "_blank",
                        "");
            }

            @Override
            public void doSomethingOptOnFailure() {
                infoDialog.hideDialog();
            }

            @Override
            public void onActionFailure(Throwable caught) {
                infoDialog.hideDialog();
                super.onActionFailure(caught); //To change body of generated methods, choose Tools | Templates.
            }
        }.setNodeId(selectedDict.nodeId).go();
    }

}
