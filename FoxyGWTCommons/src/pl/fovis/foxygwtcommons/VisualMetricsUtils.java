/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author beata
 */
public class VisualMetricsUtils {

    @SuppressWarnings("deprecation")
    public static void fixHorizontalSplitPanelAndWatchForSplitterMoves(final HorizontalSplitPanelWrapper hsp,
            final IParametrizedContinuation<Integer> onSplitterMove) {

        final IContinuation fixer = new IContinuation() {

            @Override
            public void doIt() {
                int width = VisualMetricsUtils.getWidthOfLeftHorizontalSplitPanelElement(hsp);
                onSplitterMove.doIt(width);
            }
        };

        hsp.addHandler(new MouseMoveHandler() {

            Timer t = new Timer() {

                @Override
                public void run() {
                    GWTUtils.runDeferred(fixer);
                }
            };

            @Override
            public void onMouseMove(MouseMoveEvent event) {
                if (hsp.isResizing()) {
                    //fixer.doIt();
                    GWTUtils.runDeferred(fixer);
                    // fix dla IE8
                    t.schedule(500);
                }
            }
        }, MouseMoveEvent.getType());

        VisualMetricsUtils.disableScrollPanelOfLeftHorizontalSplitPanelElement(hsp);

        GWTUtils.runDeferred(fixer);
    }

    public static void disableScrollPanelOfLeftHorizontalSplitPanelElement(HorizontalSplitPanelWrapper hsp) {
        hsp.getElement().getFirstChildElement().getFirstChildElement().getStyle().setOverflow(Overflow.HIDDEN);
    }

    public static int getWidthOfLeftHorizontalSplitPanelElement(HorizontalSplitPanelWrapper hsp) {
        return VisualMetricsUtils.getWidth(hsp.getElement().getFirstChildElement().getFirstChildElement());
    }

    public static int getWidth(Widget w) {
        return getWidth(w.getElement());
    }

    public static int getWidth(Element e) {
        return e.getClientWidth() - getHorizontalOverheadPropPx(e, "padding" /* I18N: no */);
    }

    public static int getHeight(Widget w) {
        return getHeight(w.getElement());
    }

    public static int getHeight(Element e) {
        return e.getClientHeight() - getVerticalOverheadPropPx(e, "padding" /* I18N: no */);
    }

    public static int getVerticalOverheadPropPx(Element e, String propNamePrefix) {
//        int top = getComputedStyleAssumingPx(e, propNamePrefix + "-top");
//        int bottom = getComputedStyleAssumingPx(e, propNamePrefix + "-bottom");
//        return top + bottom;
        return getVisualOverheadPropPx(e, propNamePrefix, "top" /* I18N: no */, "bottom" /* I18N: no */);
    }

    public static int getHorizontalOverheadPropPx(Element e, String propNamePrefix) {
        return getVisualOverheadPropPx(e, propNamePrefix, "left" /* I18N: no */, "right" /* I18N: no */);
    }

    public static int getVisualOverheadPropPx(Element e, String propNamePrefix,
            String propNameFirstSuffix, String propNameLastSuffix) {
        int first = getComputedStyleAssumingPx(e, propNamePrefix + "-" + propNameFirstSuffix);
        int last = getComputedStyleAssumingPx(e, propNamePrefix + "-" + propNameLastSuffix);
        return first + last;
    }

    public static int getComputedStyleAssumingPx(Element e, String styleAttrName) {
        String cs = getComputedStyle(e, styleAttrName);
//        if (styleAttrName.startsWith("padding")) {
//            Window.alert("styleAttrName: " + styleAttrName + ", value=" + cs);
//        }
        if (cs != null) {
            if (cs.toLowerCase().endsWith("px")) {
                cs = cs.substring(0, cs.length() - 2);
                try {
                    return Integer.parseInt(cs);
                } catch (Exception ex) {
                    return 0;
                }
            }
        }
        return 0;
    }

    public static native String getComputedStyle(Element _elem, String _style) /*-{

     var computedStyle;
     if (typeof _elem.currentStyle != 'undefined')
     { computedStyle = _elem.currentStyle; }
     else
     { computedStyle = document.defaultView.getComputedStyle(_elem, null); }

     return computedStyle[_style];
     }-*/;

    public static int getDynamicHeightForLowerWidget(Widget parent, Widget lowerWidget) {
        int absTopFp = parent.getAbsoluteTop();
        int absTopLower = lowerWidget.getAbsoluteTop();
        int fpHeight = getHeight(parent.getElement());
        int lowerPadding = getVerticalOverheadPropPx(lowerWidget.getElement(), "padding" /* I18N: no */);
        int restHeight = absTopLower - absTopFp;
        int heightToSet = fpHeight - restHeight - lowerPadding;
        return heightToSet;
    }

    protected void setHeightMethod2(Widget parent, Widget lowerWidget) {
        lowerWidget.setHeight(getDynamicHeightForLowerWidget(parent, lowerWidget) + "px");
    }

    @SuppressWarnings("deprecation")
    public static void getDynamicHeightPanelUnderDisclosurePanel(final DisclosurePanel hsp, final ScrollPanel foundItemsScrollPnl,
            final IParametrizedContinuation<Integer> handler) {

        final IContinuation fixer = new IContinuation() {

            @Override
            public void doIt() {
                int height = VisualMetricsUtils.getDynamicHeightForLowerWidget(foundItemsScrollPnl.getParent(), foundItemsScrollPnl);
                handler.doIt(height);
            }
        };
        final Timer t = new Timer() {

            @Override
            public void run() {
                GWTUtils.runDeferred(fixer);
            }
        };
        hsp.addOpenHandler(new OpenHandler<DisclosurePanel>() {

            @Override
            public void onOpen(OpenEvent<DisclosurePanel> event) {
                GWTUtils.runDeferred(fixer);
                t.schedule(500);
            }
        });

        hsp.addCloseHandler(new CloseHandler<DisclosurePanel>() {
            @Override
            public void onClose(CloseEvent<DisclosurePanel> event) {
                GWTUtils.runDeferred(fixer);
                t.schedule(500);
            }
        });

        GWTUtils.runDeferred(fixer);
    }

}
