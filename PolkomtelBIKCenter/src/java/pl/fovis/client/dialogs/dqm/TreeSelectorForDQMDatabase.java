/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author tflorczak
 */
public class TreeSelectorForDQMDatabase extends TreeSelectorForDQMFunction {

    @Override
    protected boolean isActionButtonEnabled() {
        return selectedNode != null && (selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_MSSQL_DATABASE)
                || selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_ORACLE_OWNER)
                || selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_POSTGRES_DATABASE));
    }

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzBazeDanych.get() /* I18N:  */;
    }

    @Override
    protected BikNodeTreeOptMode getTreeOptExtraMode() {
        return BikNodeTreeOptMode.DQMDatabase;
    }
}
