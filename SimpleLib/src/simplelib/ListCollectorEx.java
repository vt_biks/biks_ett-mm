/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.List;
import simplelib.BaseUtils.Projector;

/**
 *
 * @author wezyr
 */
public class ListCollectorEx<T, T2> extends CollectionCollectorEx<T, T2> {

    public ListCollectorEx(Projector<T, T2> itemProj, List<T2> list) {
        super(itemProj, list);
    }

    public ListCollectorEx(Projector<T, T2> itemProj) {
        super(itemProj);
    }

    public List<T2> getList() {
        return (List<T2>) getCollection();
    }
}
