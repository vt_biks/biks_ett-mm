/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import simplelib.BaseUtils.Projector;

/**
 *
 * @author wezyr
 */
public class NamedPropsBeanProjector<T extends INamedPropsBean, V> implements Projector<T, V> {
    private String propName;

    public NamedPropsBeanProjector(String propName) {
        this.propName = propName;
    }

    @SuppressWarnings("unchecked")
    public V project(T val) {
        return (V)val.getPropValue(propName);
    }
}
