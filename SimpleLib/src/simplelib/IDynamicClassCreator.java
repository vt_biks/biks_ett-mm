/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.Set;

/**
 *
 * @author wezyr
 * @param <T>
 */
public interface IDynamicClassCreator<T> {

    public T create(String simpleName);

    public Set<String> getNames();
}
