/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import static pl.fovis.client.dialogs.dqm.AddOrEditDQMTestDialogBase.addLabel;
import pl.fovis.common.dqm.DataQualityTestBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class DQMWidgetConfigProfile implements IDQMWidgetConfig {

    protected final static String SFTP_TEST_FILE_SUFFIX = "_yyyyMMddhhmmss.csv";
    protected final static String SFTP_ERROR_DATA_FILE_SUFFIX = "_yyyyMMddhhmmss_error.csv";
    protected TextBox tbxResultFileName;
    protected Label lblSFTPTestFileName;
    protected Label lblSFTPErrorDataFileName;
    protected VerticalPanel main;

    @Override
    public boolean validateWidgetsBeforeSave() {
        if (BaseUtils.isStrEmptyOrWhiteSpace(tbxResultFileName.getText())) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.nazwaPlikuNaSFTPNieMozeBycPusta.get(), I18n.blad.get(), null);
            return false;
        }
        return true;
    }

    @Override
    public void fillWidgets(DataQualityTestBean bean) {
        tbxResultFileName.setText(bean.resultFileName);
        updateLabelTexts();
    }

    @Override
    public void fillBean(DataQualityTestBean bean) {
        bean.resultFileName = tbxResultFileName.getText();
    }

    @Override
    public void cleanWidgets() {
        tbxResultFileName.setText("");
        lblSFTPTestFileName.setText("");
        lblSFTPErrorDataFileName.setText("");
    }

    @Override
    public Widget getWidget(Integer nodeId) {
        if (main == null) {
            buildWidget();
        }
        return main;
    }

    protected void buildWidget() {
        main = new VerticalPanel();
        //Profile panel
        FlexTable profileFlexTable = new FlexTable();
        profileFlexTable.setWidget(0, 0, addLabel(I18n.prefiksNazwyPlikuZSFTP.get()));
        profileFlexTable.setWidget(0, 1, tbxResultFileName = new TextBox());
        profileFlexTable.setWidget(1, 0, addLabel(I18n.nazwaPlikuZWykonaniamiTestNaSFTP.get()));
        profileFlexTable.setWidget(1, 1, lblSFTPTestFileName = addLabel(""));
        profileFlexTable.setWidget(2, 0, addLabel(I18n.nazwaPlikuZBlednymiDanymiTestuNaSFTP.get()));
        profileFlexTable.setWidget(2, 1, lblSFTPErrorDataFileName = addLabel(""));
        tbxResultFileName.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                updateLabelTexts();
            }
        });
        main.add(profileFlexTable);
        updateLabelTexts();
    }

    protected void updateLabelTexts() {
        String text = tbxResultFileName.getText().trim();
        boolean isStrEmpty = BaseUtils.isStrEmptyOrWhiteSpace(text);
        lblSFTPTestFileName.setText(isStrEmpty ? "" : text + SFTP_TEST_FILE_SUFFIX);
        lblSFTPErrorDataFileName.setText(isStrEmpty ? "" : text + SFTP_ERROR_DATA_FILE_SUFFIX);
    }
}
