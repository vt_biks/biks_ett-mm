/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.biadmin;

import java.util.List;
import java.util.Set;
import pl.bssg.metadatapump.common.SAPBOObjectBean;
import pl.bssg.metadatapump.common.SAPBOScheduleBean;

/**
 *
 * @author tflorczak
 */
public interface IBIAConnector {

    public void initAndLogin();

    public boolean isServiceInit();

    public void logout();

    public void pauseInstances(List<Integer> instancesToPause);

    public void resumeInstances(List<Integer> instancesToResume);

    public void deleteInstances(List<Integer> instancesToDelete);

    public void rescheduleInstances(List<Integer> instancesToReschedule, SAPBOScheduleBean params);

    public List<SAPBOScheduleBean> getBIASchedules(SAPBOScheduleBean filter);

    public List<SAPBOObjectBean> getBIAObjects(SAPBOObjectBean filter);

    public List<SAPBOObjectBean> getBIAUsers();

    public void changeBIANameAndDescr(Set<SAPBOObjectBean> objs, Integer manageType);

    public void changeBIAOwner(List<Integer> objs, SAPBOObjectBean newOwner);

    public List<SAPBOObjectBean> getBIAReports();

    public List<SAPBOObjectBean> getBIAUniverses();

    public List<SAPBOObjectBean> getBIAConnections();

    public List<SAPBOObjectBean> getBIAJobs();

    public void deleteBIAJobs(String prefix);

    public void cancelQuering();

}
