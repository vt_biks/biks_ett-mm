/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.trzy0.foxy.commons.annotations.DisableDynamicClassCreation;

/**
 *
 * @author wezyr
 */
@DisableDynamicClassCreation
public class TestTreeBikPage extends TreeBikPage {

    protected CheckBox cbActiveTestsOnly;

    public TestTreeBikPage(TreeBean tree) {
        super(tree, TreeBikPage.TREE_BIK_PAGE_ONE_COL_FOR_TREE);
    }

    @Override
    protected void addOptionalExtraWidgetsToTreeFooter(HorizontalPanel hPanel) {
        super.addOptionalExtraWidgetsToTreeFooter(hPanel);
        cbActiveTestsOnly = new CheckBox(I18n.aktywne.get() /* I18N:  */);
        cbActiveTestsOnly.setTitle(I18n.tylkoAktywne.get());
        cbActiveTestsOnly.setStyleName("dqcActiveTests");
        cbActiveTestsOnly.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                bikNodeTreeOptMode = cbActiveTestsOnly.getValue() ? BikNodeTreeOptMode.ActiveDQCTestsOnly : null;
                clearFilter();
            }
        });
        cbActiveTestsOnly.setWidth("100px");
        hPanel.add(cbActiveTestsOnly/*new Label("Aqq DQC!")*/);
//        hPanel.setWidth("150px");
    }

    @Override
    protected void clearFilterControls() {
        super.clearFilterControls();
        cbActiveTestsOnly.setValue(false, false);
        bikNodeTreeOptMode = null;
    }
}
