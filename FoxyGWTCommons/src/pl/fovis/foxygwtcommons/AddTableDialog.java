/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import pl.fovis.foxygwtcommons.i18n.I18n;

/**
 *
 * @author bfechner
 */
public class AddTableDialog extends BaseActionOrCancelDialog {

    protected TextBox rowText;
    protected TextBox colText;
    protected Label status;
    private IParametrizedContinuation</*Pair<Integer, Integer>*/String> saveCont;
    private Label rowLbl;
    private Label colLbl;

    @Override
    protected String getDialogCaption() {
        return I18n.wstawTabele.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.wstaw.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {

        status = new Label();
        main.add(status);
        Grid grid = new Grid(2, 2);
        grid.setCellSpacing(4);
        grid.setWidth("100%");

        grid.setWidget(0, 0, rowLbl = new Label(I18n.iloscWierszy.get() /* I18N:  */+ ":"));
        rowLbl.setStyleName("addNodeLbl");
        grid.setWidget(0, 1, rowText = new TextBox());
        rowText.setStyleName("addNode");
        rowText.setWidth("30px");

        grid.setWidget(1, 0, colLbl = new Label(I18n.iloscKolumn.get() /* I18N:  */+ ":"));
        colLbl.setStyleName("addNodeLbl");
        grid.setWidget(1, 1, colText = new TextBox());
        colText.setStyleName("addNode");
        colText.setWidth("30px");


        main.add(grid);
        main.setCellWidth(grid, "100%");
    }

    @Override
    protected void doAction() {

        //Window.alert("Do action?>4");        
        StringBuilder sb = new StringBuilder();
        sb.append("<table class='def'>");
        int rowCnt = BaseUtils.tryParseInt(rowText.getText());
        int colCnt = BaseUtils.tryParseInt(colText.getText());
        for (int i = 0; i < rowCnt; i++) {
            sb.append("<tr>");
            for (int j = 0; j < colCnt; j++) {
                sb.append("<td width=\"40px\">&" +"nbsp" /* I18N: no */+ ";</td>");
//                sb.append("<td width='").append(100 / BaseUtils.tryParseInteger(colText.getText())).append("%'>&nbsp;</td>");?
            }
            sb.append("</tr>");
        }
        sb.append("</table><br/>");
        String table = sb.toString();
        saveCont.doIt(table);
    }

    public void buildAndShowDialog(IParametrizedContinuation<String> saveCont) {
        this.saveCont = saveCont;

        super.buildAndShowDialog();
    }

    @Override
    protected boolean doActionBeforeClose() {
        int rowCnt = BaseUtils.tryParseInt(rowText.getText());
        int colCnt = BaseUtils.tryParseInteger(colText.getText());
        if (rowCnt > 100 || colCnt > 20 || colCnt <= 0 || rowCnt <= 0) {
            Window.alert(I18n.bladIloscWierszyIKolumnMusiBycDo.get() /* I18N:  */+ ": 100");
            return false;
        }
        return true;
    }
}
