/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.Label;
import pl.fovis.common.AttrHintBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author beata
 */
public class EditAttrHintDialog extends BaseRichTextAndTextboxDialog {

    protected IParametrizedContinuation<AttrHintBean> saveCont;

    @Override
    protected void addLabelsTitle() {
        lblTitle = new Label(I18n.nazwa.get() /* I18N:  */ + ":");
        lblArea = new Label(I18n.opis.get() /* I18N:  */ + ":");
    }

    @Override
    protected void innerDoAction(String title, String body) {
        AttrHintBean nameDesc = new AttrHintBean();
        nameDesc.hint = body;
        nameDesc.name = title;
        saveCont.doIt(nameDesc);
    }

    public void buildAndShowDialog(String name, String hint, IParametrizedContinuation<AttrHintBean> saveCont) {
        this.saveCont = saveCont;
        super.buildAndShowDialog(name, hint, false);
    }

    @Override
    protected String getDialogCaption() {
        return I18n.edytujOpis.get();
    }
}
