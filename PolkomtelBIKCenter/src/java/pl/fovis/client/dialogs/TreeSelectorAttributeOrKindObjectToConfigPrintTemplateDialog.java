/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.JoinedObjMode;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.LameRuntimeException;

/**
 *
 * @author bfechner
 */
public class TreeSelectorAttributeOrKindObjectToConfigPrintTemplateDialog extends TreeSelectorForJoinedObjectsDialog {

    String attrName;
    Integer parentNodeId;
    Integer nodeIdObjectKind;
    String nodeKindCode;
    String nodeKindCodeFromAttrLis;
    String parentName;

    public TreeSelectorAttributeOrKindObjectToConfigPrintTemplateDialog(Integer nodeIdObjectKind, String nodeKindCode) {
        this.nodeIdObjectKind = nodeIdObjectKind;
        this.nodeKindCode = nodeKindCode;
    }

    public TreeSelectorAttributeOrKindObjectToConfigPrintTemplateDialog(String nodeKindCode, String nodeKindCodeFromAttrLis) {
        this.nodeKindCode = nodeKindCode;
        this.nodeKindCodeFromAttrLis = nodeKindCodeFromAttrLis;
    }

    public TreeSelectorAttributeOrKindObjectToConfigPrintTemplateDialog(String parentName, String nodeKindCode, boolean ci) {
        this.nodeKindCode = nodeKindCode;
        this.parentName = parentName;
    }

    public void buildAndShowDialog(Integer parentNodeId, String attrName, IParametrizedContinuation<TreeNodeBean> saveCont,
            IJoinedObjSelectionManager<Integer> alreadyJoinedManager) {

        this.attrName = attrName;
        this.parentNodeId = parentNodeId;
        super.buildAndShowDialog(nodeId, saveCont, alreadyJoinedManager);
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {

        getService().getAttributeOrKindObjectToConfigPrintTemplate(parentNodeId, nodeIdObjectKind, nodeKindCode, nodeKindCodeFromAttrLis, parentName, callback);
    }

    protected boolean isInMultiSelectMode() {
        return true;
    }

    @Override
    protected Collection<Integer> getTreeIdsForFiltering() {
        return null;
    }

    protected boolean isStandardJoinedObjMode() {
        return false;
    }

    @Override
    protected String getDialogCaption() {

        if (BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND.equalsIgnoreCase(nodeKindCode)) {
            return I18n.dodajeszAtrybutDoSzablonu.get();
        } else if (BIKConstants.NODE_KIND_META_PRINT_ATTRIBUT.equalsIgnoreCase(nodeKindCode)) {
            return I18n.dodajeszTypObiektu.get();
        } else {
            return I18n.dodajeszAtrybut.get();
        }
    }

    protected boolean isHeaderVisible() {
        return false;
    }

    @Override
    protected void doAction() {
        final BIKSProgressInfoDialog dialog = new BIKSProgressInfoDialog();
        dialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwazapisywanie.get(), true);
        NodeKindBean nk = BIKClientSingletons.getNodeKindBeanByCode(
                nodeKindCode.equals(BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND) || nodeKindCode.equals(BIKConstants.NODE_KIND_META_NODE_KIND)
                ? BIKConstants.NODE_KIND_META_PRINT_ATTRIBUT
                : nodeKindCode.equals(BIKConstants.NODE_KIND_META_BUILT_IN_NODE_KIND) ? BIKConstants.NODE_KIND_META_PRINT_BUILT_IN_KIND
                : BIKConstants.NODE_KIND_META_PRINT_KIND
        );

        List<Integer> dO = new ArrayList<Integer>();
        for (Entry<Integer, JoinedObjMode> dov : directOverride.entrySet()) {

            if (dov.getValue() == JoinedObjMode.Single) {
                dO.add(dov.getKey());
            }
        }
        getService().createTreeNodesAttribute(parentNodeId, dO, nk.id, new StandardAsyncCallback<Integer>() {
            @Override
            public void onSuccess(Integer result) {
                dialog.hideDialog();
                TreeNodeBean tnb = new TreeNodeBean();
                tnb.id = result;
                saveCont.doIt(tnb);
            }

            @Override
            public void onFailure(Throwable caught) {
                dialog.hideDialog();
                LameRuntimeException e = (LameRuntimeException) caught;
                Window.alert(e.getMessage());
            }
        });
    }

}
