<map version="0.9.0"><attribute_registry SHOW_ATTRIBUTES="hide"/>
<node TEXT="Pawe&#x142; MonaS&#x142;owniki" ID="Freemind_Link_1"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="Uprawnienia po stronie aplikacji BIKS" ID="Freemind_Link_2"  POSITION="right"  COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Import z Excela" ID="Freemind_Link_3"  POSITION="right"  COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Logowanie na u&#x17c;ytkownika aplikacyjnym" ID="Freemind_Link_4"  POSITION="right"  COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Rozpozna&#x107; SanMargar" ID="Freemind_Link_5"  POSITION="right"  COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="MetaStudio" ID="Freemind_Link_6"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
</node>
<node TEXT="Pawe&#x142; prze&#x15b;le wymagania do aplikacji s&#x142;ownikowej" ID="Freemind_Link_7"  POSITION="right"  COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Modu&#x142;y" ID="Freemind_Link_18"  POSITION="left"  COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="Administracja" ID="Freemind_Link_19"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="Profile" ID="Freemind_Link_20"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="Kategoryzacje" ID="Freemind_Link_21"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Grupy u&#x17c;ytkownik&#xf3;w&#10; Edycja Profile" ID="Freemind_Link_22"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="U&#x17c;ytkownicy" ID="Freemind_Link_23"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
</node>
<node TEXT="Uprawnienia do s&#x142;ownik&#xf3;w" ID="Freemind_Link_24"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Zarz&#x105;dzanie u&#x17c;ytkownikami" ID="Freemind_Link_25"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Upr. Do kategoryzacji" ID="Freemind_Link_26"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Zatwierdzenie istniej&#x105;cych uprawnie&#x144;" ID="Freemind_Link_27"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
</node>
<node TEXT="S&#x142;owniki" ID="Freemind_Link_28"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="Wy&#x15b;wietlanie wg grup" ID="Freemind_Link_29"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Wy&#x15b;wietlanie wg podgrup" ID="Freemind_Link_30"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Funkcje s&#x142;ownika" ID="Freemind_Link_31"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="Nowy rekord" ID="Freemind_Link_32"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Edycja" ID="Freemind_Link_33"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Export" ID="Freemind_Link_34"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Import" ID="Freemind_Link_35"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
</node>
<node TEXT="Obecnie nie ma mo&#x17c;liwo&#x15b;ci&#10;Dodawania nowego s&#x142;ownika&#10;Zaszyte na sztywno&#10;Pawe&#x142; wys&#x142;a&#x142; pytanie o dokumentacj&#x119; Administratora" ID="Freemind_Link_36"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
</node>
<node TEXT="Kategoryzacja" ID="Freemind_Link_37"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="Przypisywanie do kategorii&#10;Warto&#x15b;ci z s&#x142;ownika" ID="Freemind_Link_38"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Jeden klucz do jednej kategorii" ID="Freemind_Link_39"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Nieprzydzielone" ID="Freemind_Link_40"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Alerty&#10;Delta zmian " ID="Freemind_Link_41"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Funkcje" ID="Freemind_Link_42"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="Rozpocz&#x119;cie procesu edycji" ID="Freemind_Link_43"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="Drag and drop" ID="Freemind_Link_44"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Eksportuj" ID="Freemind_Link_45"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Na screen" ID="Freemind_Link_46"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Usu&#x144;" ID="Freemind_Link_47"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
</node>
<node TEXT="Import" ID="Freemind_Link_48"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Export" ID="Freemind_Link_49"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Filtrowanie" ID="Freemind_Link_50"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="Zaawansowane filtrowanie warto&#x15b;ci" ID="Freemind_Link_51"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
</node>
<node TEXT="Wielko&#x15b;&#x107; ikon" ID="Freemind_Link_52"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="Pewnie do kosza" ID="Freemind_Link_53"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
</node>
<node TEXT="Mo&#x17c;liwo&#x15b;&#x107; Tworzenia&#10;Nowych Kategoryzacji" ID="Freemind_Link_54"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Od&#x15b;wie&#x17c;enie okna" ID="Freemind_Link_55"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
</node>
</node>
</node>
<node TEXT="Pawe&#x142;&#10;WIZJA" ID="Freemind_Link_8"  POSITION="right"  COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="Wymagania" ID="Freemind_Link_9"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="Mo&#x17c;liwo&#x15b;&#x107; utrzymywania, dodawania&#10;S&#x142;ownik&#xf3;w u&#x17c;ytkownik&#xf3;w nieporodukcyjnych&#10;(Import z Excela itp.)&#10;Istniej&#x105; tylko w BIKS" ID="Freemind_Link_10"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Jak utrzymywa&#x107; s&#x142;owniki na hutrowni" ID="Freemind_Link_11"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
</node>
<node TEXT="Do weryfikacji" ID="Freemind_Link_12"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /><node TEXT="M. Obr&#x119;bski" ID="Freemind_Link_13"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="J. Paryska" ID="Freemind_Link_14"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="W. Paw&#x142;owski" ID="Freemind_Link_15"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="H. Tarnowski" ID="Freemind_Link_16"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
<node TEXT="Architekci Danych" ID="Freemind_Link_17"   COLOR="#000000" BACKGROUND_COLOR="#B2B2FE" STYLE="bubble" ><edge COLOR="#B2B2FE" /></node>
</node>
</node>
</node>
</map>