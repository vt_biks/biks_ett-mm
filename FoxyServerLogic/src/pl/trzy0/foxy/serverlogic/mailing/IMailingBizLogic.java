/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.mailing;

import pl.trzy0.foxy.commons.IFoxyTransactionalPerformer;

/**
 *
 * @author wezyr
 */
public interface IMailingBizLogic extends IFoxyTransactionalPerformer {

    public MailToSendBean getMailToSend(boolean noLogging);

    public void setMailSendStatus(int mailId, String errMsg);
}
