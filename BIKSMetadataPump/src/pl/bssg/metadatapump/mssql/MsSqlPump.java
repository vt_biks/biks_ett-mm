/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.mssql;

import commonlib.LameUtils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.AdhocPartialBeanCollector;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.MsSqlExPropConfigBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.common.ServerConfigBean;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class MsSqlPump extends MetadataPumpBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected final static String DATABASE = "master";
    protected final static String TYPE_SERVER = "MSSQLServer";
    protected final static String TYPE_DATABASE = "MSSQLDatabase";
//    protected final static String TYPE_OWNER = "MSSQLOwner";
    protected final static String TYPE_TABLE = "MSSQLTable";
    protected final static String TYPE_VIEW = "MSSQLView";
    protected final static String TYPE_PROCEDURE = "MSSQLProcedure";
    protected final static String TYPE_FUNCTION = "MSSQLFunction";
    protected final static String TYPE_TABLE_FUNCTION = "MSSQLTableFunction";
    protected final static String TYPE_COLUMN = "MSSQLColumn";
    protected final static String TYPE_FOLDER = "MSSQLFolder";
    protected final static String EX_PROP_DESCRIPTION = "MS_Description";
    protected final static Map<String, String> tableKindMap = new HashMap<String, String>();
    protected final static Map<String, String> typeFolderNameMap = new HashMap<String, String>();
    protected final static Map<String, Integer> typeFolderVisualOrderMap = new HashMap<String, Integer>();

    static {
        tableKindMap.put("U", TYPE_TABLE);
        tableKindMap.put("P", TYPE_PROCEDURE);
        tableKindMap.put("V", TYPE_VIEW);
        tableKindMap.put("FN", TYPE_FUNCTION);
        tableKindMap.put("TF", TYPE_TABLE_FUNCTION);
        typeFolderNameMap.put(TYPE_TABLE, "Tabele");
        typeFolderNameMap.put(TYPE_VIEW, "Widoki");
        typeFolderNameMap.put(TYPE_PROCEDURE, "Procedury");
        typeFolderNameMap.put(TYPE_FUNCTION, "Funkcje skalarne");
        typeFolderNameMap.put(TYPE_TABLE_FUNCTION, "Funkcje tabelaryczne");
        typeFolderVisualOrderMap.put(TYPE_TABLE, 1);
        typeFolderVisualOrderMap.put(TYPE_VIEW, 2);
        typeFolderVisualOrderMap.put(TYPE_PROCEDURE, 3);
        typeFolderVisualOrderMap.put(TYPE_FUNCTION, 4);
        typeFolderVisualOrderMap.put(TYPE_TABLE_FUNCTION, 5);
    }
//    protected Set<String> ownerSet = new HashSet<String>();
    protected Set<String> tableSet = new HashSet<String>();
    protected Set<String> typeSet = new HashSet<String>();
    protected INamedSqlsDAO<Object> adhocDaoMSSQL;
    protected IBeanConnectionEx<Object> mssqlConnection = null;
    protected AdhocPartialBeanCollector<MsSqlBean> beanCollector;
    protected AdhocPartialBeanCollector<MsSqlIndexBean> beanIndexCollector;
    protected AdhocPartialBeanCollector<MsSqlForeignKeyBean> beanForeignKeyCollector;
    protected AdhocPartialBeanCollector<MsSqlColumnBean> beanColumnsCollector;
    protected AdhocPartialBeanCollector<MsSqlDependencyBean> beanDependencyCollector;
    protected AdhocPartialBeanCollector<MsSqlExPropBean> beanExPropertyCollector;
    protected Set<String> badServers = new HashSet<String>();
    protected Map<String, Set<String>> exPropMap = new HashMap<String, Set<String>>();
    protected Map<String, Set<String>> serverDatabases = new HashMap<String, Set<String>>();

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_MSSQL;
    }

//    public String getPumpGroup() {
//        return PumpConstants.PUMP_GROUP_MSSQL;
//    }
    @Override
    protected Pair<Integer, String> startPump() {
//        System.out.println("URUCHOMNIONO DLA INSTANCJI: " + instanceName);
        createCollectors();
        getMetadataFromServers();

        if (!beanCollector.wasSomethingAdded()) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "No connecting with MS SQL server or lack of databases!");
        }
        execNamedCommandWithCommit("updateMSSQLKeys"); // update PK, FK

//        execNamedCommandWithCommit("deleteMSSQLBadData", badServers); // insert into bik_mssql from aaa_mssql
        execNamedCommandWithCommit("insertMSSQLData", serverDatabases); // insert into bik_mssql from aaa_mssql

        if (logger.isInfoEnabled()) {
            logger.info("Update BIKS structure");
        }
        pumpLogger.pumpLog("Update BIKS structure", 80);

//        execNamedCommandWithCommit("moveMSSQLMetadataIntoBikNode");
        for (Map.Entry<String, Set<String>> entry : serverDatabases.entrySet()) {
            execNamedCommandWithCommit("moveMSSQLMetadataIntoBikNodeOneServer", entry.getKey(), BaseUtils.mergeWithSepEx(entry.getValue(), ","));
        }

        // extradata
        pumpLogger.pumpLog("Insert MS SQL extradata", 90);
//        execNamedCommandWithCommit("insertMSSQLExPropsExtradata", badServers); // insert into bik_mssql_extended_properties from aaa_mssql_extended_properties
//        execNamedCommandWithCommit("insertMSSQLIndexExtradata", badServers); // insert into bik_mssql_index from aaa_mssql_index
//        execNamedCommandWithCommit("insertMSSQLForeignKeyExtradata", badServers); // insert into bik_mssql_foreign_key from aaa_mssql_foreign_key
//        execNamedCommandWithCommit("insertMSSQLColumnExtradata", badServers); // insert into bik_mssql_column_extradata from aaa_mssql_column_extradata
//        execNamedCommandWithCommit("insertMSSQLDependencyExtradata", badServers); // insert into bik_mssql_dependency from aaa_mssql_dependency
        // insert from aaa_mssql% to bik_mssql%
        execNamedCommandWithCommit("insertMSSQLExPropsExtradataForOneServer", serverDatabases);
        execNamedCommandWithCommit("insertMSSQLIndexExtradataForOneServer", serverDatabases);
        execNamedCommandWithCommit("insertMSSQLForeignKeyExtradataForOneServer", serverDatabases);
        execNamedCommandWithCommit("insertMSSQLColumnExtradataForOneServer", serverDatabases);
        execNamedCommandWithCommit("insertMSSQLDependencyExtradataForOneServer", serverDatabases);

        if (logger.isInfoEnabled()) {
            logger.info("All right! Success!");
        }
        return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
    }

    protected void getMetadataFromServers() {
        // get connection properties
        List<ServerConfigBean> servers = adhocDao.createBeansFromNamedQry("getDBConfigForDB", ServerConfigBean.class, PumpConstants.SOURCE_NAME_MSSQL, instanceName);
        MsSqlExPropConfigBean configData = PumpUtils.readAdminBean("mssql", MsSqlExPropConfigBean.class, adhocDao);
        Boolean useBasicModeInDBConnectors = BaseUtils.tryParseBoolean((String) execNamedQuerySingleVal("getAppPropValue", true, "val", "useBasicModeInDBConnectors"), false);
        createExPropMap(configData);
        if (servers.isEmpty()) {
            throw new LameRuntimeException("Lack of servers!");
        }
        badServers.clear();
        typeSet.clear();
        serverDatabases.clear();
//        execNamedCommandWithCommit("deleteMSSQLTempTable"); // nie usuwamy wszsytkiego, tylko dana instancje
        int serversStepCount = 80 / servers.size();
        int miniStep = serversStepCount / 6; // 6 = {DB, Objects, Columns, Foreign Keys, Indieces, Dependencies}
        int actualServerNumber = 0;
        int actualStep = 0;
        for (ServerConfigBean serv : servers) {
            actualStep = 0;
            String serverNameWithSuffix = serv.name;
            String serverName = PumpUtils.getServerNameWithoutSuffix(serverNameWithSuffix);
            Set<String> databasesOfServer = new HashSet<String>();
            try {
                tableSet.clear();
                // create connection to MS SQL database
                mssqlConnection = new MssqlConnection(
                        new MssqlConnectionConfig(serv.server, serv.instance, DATABASE, serv.user, serv.password));
                if (mssqlConnection == null) {
                    pumpLogger.pumpLog("No connection with server: " + serv.server, null);
                    continue;
                }
                mssqlConnection.setDbStatementExecutionWarnThresholdMillis(-1);
                adhocDaoMSSQL = FoxyAppUtils.createNamedSqlsDAOFromJar(mssqlConnection, ADHOC_SQLS_PATH);

                String serverVersion = BeanConnectionUtils.execNamedQuerySingleVal(adhocDaoMSSQL, "getMSSQLServerVersion", false, "version");
                IMsSqlQueryNameProvider queryNameProvider;
                if (serverVersion.startsWith("8.")) { // 2000
                    queryNameProvider = new MsSql2000QueryNameProvider();
                } else if (serverVersion.startsWith("9.")) { // 2005
                    queryNameProvider = new MsSql2005QueryNameProvider();
                } else if (serverVersion.startsWith("10.")) { // 2008 & 2008 R2
                    queryNameProvider = new MsSql2008QueryNameProvider();
                } else if (serverVersion.startsWith("11.")) { // 2012
                    queryNameProvider = new MsSql2008QueryNameProvider();
                } else {
                    queryNameProvider = new MsSql2008QueryNameProvider();
                }

                // get databases
                pumpLogger.pumpLog(serverNameWithSuffix + ": loading: databases", actualServerNumber * serversStepCount + miniStep * (actualStep++));
                List<MsSqlBean> databases = adhocDaoMSSQL.createBeansFromNamedQry(queryNameProvider.getDatabases()/*"getMSSQLDatabases"*/, MsSqlBean.class, BaseUtils.splitBySep(serv.databaseFilter, ",", true));
                if (!databases.isEmpty()) {
                    execNamedCommandWithCommit("addMSSQLServerIfNotExists", serverName);
//                    beanCollector.add(new MsSqlBean(serv.name, TYPE_SERVER, null, null, serv.name + "|", null));
                }
                Map<String, String> objectMap = createObjectMapFromString(serv.objectFilter);
                // clear temp tables
                for (MsSqlBean database : databases) {
                    execNamedCommandWithCommit("deleteMSSQLTempTableForServerAndDatabase", serverName, database.name);
                }
                // set databases for server
                execNamedCommandWithCommit("setDatabasesForServer", serv.id, BaseUtils.mergeWithSepEx(databases, ",", new BaseUtils.Projector<MsSqlBean, String>() {

                    @Override
                    public String project(MsSqlBean val) {
                        return val.name;
                    }
                }));
                // get owner and tables and view and procedure and function
                pumpLogger.pumpLog(serverNameWithSuffix + ": loading: owners, tables, views, procedures and functions", actualServerNumber * serversStepCount + miniStep * (actualStep++));
                for (MsSqlBean database : databases) {
                    String databaseDescription = null;
                    try {
                        List<MsSqlExPropBean> dbExProp = adhocDaoMSSQL.createBeansFromNamedQry(queryNameProvider.getExtendedProperties()/*"getMSSQLDatabaseEXProp"*/, MsSqlExPropBean.class, database.name);
                        for (MsSqlExPropBean oneProp : dbExProp) {
                            if (oneProp.name.equals(EX_PROP_DESCRIPTION)) {
                                databaseDescription = oneProp.value;
                            } else if (exPropMap.get(TYPE_DATABASE).contains(oneProp.name)) {
                                beanExPropertyCollector.add(new MsSqlExPropBean(serverName, database.name, TYPE_DATABASE, null, null, null, oneProp.name, oneProp.value, serverName + "|" + database.name + "|"));
                            }
                        }
                    } catch (Exception e) {
                        // Brak uprawnien do tabeli ex_properties lub baza nieosiągalna. Trudno, idziemy dalej, czyli: cicho, cicho :)
                    }
                    beanCollector.add(new MsSqlBean(database.name, TYPE_DATABASE, databaseDescription, null, serverName + "|" + database.name + "|", serverName + "|"));
                    databasesOfServer.add(database.name);
                    try {
                        String optFilter = null;
                        if (objectMap != null && objectMap.containsKey(database.name)) {
                            optFilter = objectMap.get(database.name);
                        }
                        List<MsSqlBean> objects = adhocDaoMSSQL.createBeansFromNamedQry(queryNameProvider.getObjects()/*"getMSSQLTables"*/, MsSqlBean.class, database.name, optFilter);
//                        ownerSet.clear();
                        for (MsSqlBean bean : objects) {
                            String ownerName = bean.parentBranchNames;
                            String objectType = tableKindMap.get(bean.type.trim());
                            String parentKey = serverName + "|" + database.name + "|$" + objectType + "|";
//                            if (!ownerSet.contains(ownerName)) {
//                                ownerSet.add(ownerName);
//                                beanCollector.add(new MsSqlBean(ownerName, TYPE_OWNER, null, null, serv.name + "|" + database.name + "|" + ownerName + "|", serv.name + "|" + database.name + "|"));
//                            }
                            optAddTypeFolderIfNeeded(serverName, database.name, objectType, parentKey);
                            beanCollector.add(new MsSqlBean(ownerName + "." + bean.name, objectType, bean.extraInfo, bean.definitionText, serverName + "|" + database.name + "|" + ownerName + "|" + bean.name + "|", parentKey));
                            tableSet.add(database.name + '|' + ownerName + '|' + bean.name);
                        }
                    } catch (Exception e) {
                        pumpLogger.pumpLog("Error in getting objects for database: " + database.name, null);
                        continue;
                    }
                    // loading ex props for objects
                    try {
                        List<MsSqlExPropBean> objectExProp = adhocDaoMSSQL.createBeansFromNamedQry(queryNameProvider.getObjectEXProp()/*"getMSSQLObjectEXProp"*/, MsSqlExPropBean.class, database.name, serverName, false);
                        for (MsSqlExPropBean oneProp : objectExProp) {
                            String type = tableKindMap.get(oneProp.type.trim());
                            if (exPropMap.get(type).contains(oneProp.name)) {
                                oneProp.type = type;
                                beanExPropertyCollector.add(oneProp);
                            }
                        }
                    } catch (Exception e) {
                        // Brak uprawnien do tabeli ex_properties lub baza nieosiągalna. Trudno, idziemy dalej, czyli: cicho, cicho :)
                    }
                }
                if (useBasicModeInDBConnectors) {
                    System.out.println("Basic mode MSSQL! Instance: " + instanceName);
                    addToSuccessDBServer(serverName, databasesOfServer, actualServerNumber);
                    continue;
                }

                // get columns
                pumpLogger.pumpLog(serverNameWithSuffix + ": loading: columns", actualServerNumber * serversStepCount + miniStep * (actualStep++));
                for (MsSqlBean database : databases) {
                    try {
//                        long startTime = System.nanoTime();
                        List<MsSqlColumnBean> columns = adhocDaoMSSQL.createBeansFromNamedQry(queryNameProvider.getColumns()/*"getMSSQLColumns"*/, MsSqlColumnBean.class, database.name, serverName);
//                        long endTime = System.nanoTime();
//                        System.out.println("CreateBeansFromNamedQry takes: " + (endTime - startTime) * 0.000000001 + " sec.");
//                        startTime = System.nanoTime();
                        int columnOrder = 0;
                        for (MsSqlColumnBean column : columns) {
//                            if (tableSet.contains(database.name + column.owner + column.tableName)) {
                            if (tableSet.contains(column.uniqueName)) {
                                beanColumnsCollector.add(column);
                                beanCollector.add(new MsSqlBean(column.name, TYPE_COLUMN, null, null, column.branchName, column.parentBranchName, columnOrder++));
                            }
                        }
//                        endTime = System.nanoTime();
//                        System.out.println("Rest takes: " + (endTime - startTime) * 0.000000001 + " sec. Columns: " + columns.size());
                    } catch (Exception e) {
                        pumpLogger.pumpLog("Error in getting column for database: " + database.name, null);
                        continue;
                    }
                    // loading ex props for columns
                    try {
                        List<MsSqlExPropBean> objectExProp = adhocDaoMSSQL.createBeansFromNamedQry(queryNameProvider.getObjectEXProp()/*"getMSSQLObjectEXProp"*/, MsSqlExPropBean.class, database.name, serverName, true);
                        for (MsSqlExPropBean oneProp : objectExProp) {
                            if (exPropMap.get(TYPE_COLUMN).contains(oneProp.name)) {
                                beanExPropertyCollector.add(oneProp);
                            }
                        }
                    } catch (Exception e) {
                        // Brak uprawnien do tabeli ex_properties lub baza nieosiągalna. Trudno, idziemy dalej, czyli: cicho, cicho :)
                    }
                }

                // get indexes
                pumpLogger.pumpLog(serverNameWithSuffix + ": loading: indexes", actualServerNumber * serversStepCount + miniStep * (actualStep++));
                for (MsSqlBean database : databases) {
                    try {
                        List<MsSqlIndexBean> indexes = adhocDaoMSSQL.createBeansFromNamedQry(queryNameProvider.getIndexes()/*"getMSSQLIndexes"*/, MsSqlIndexBean.class, database.name, serverName);
                        for (MsSqlIndexBean index : indexes) {
                            beanIndexCollector.add(index);
                        }
                    } catch (Exception e) {
                        pumpLogger.pumpLog("Error in getting indexes for database: " + database.name, null);
                        continue;
                    }
                }

                // get foreign keys
                pumpLogger.pumpLog(serverNameWithSuffix + ": loading: foreign keys", actualServerNumber * serversStepCount + miniStep * (actualStep++));
                for (MsSqlBean database : databases) {
                    try {
                        List<MsSqlForeignKeyBean> keys = adhocDaoMSSQL.createBeansFromNamedQry(queryNameProvider.getForeignKeys()/*"getMSSQLForeignKeys"*/, MsSqlForeignKeyBean.class, database.name, serverName);
                        for (MsSqlForeignKeyBean key : keys) {
                            beanForeignKeyCollector.add(key);
                        }
                    } catch (Exception e) {
                        pumpLogger.pumpLog("Error in getting foreign keys for database: " + database.name, null);
                        continue;
                    }
                }
                // get dependencies
                pumpLogger.pumpLog(serverNameWithSuffix + ": loading: dependencies", actualServerNumber * serversStepCount + miniStep * (actualStep++));
                for (MsSqlBean database : databases) {
                    try {
                        List<MsSqlDependencyBean> keys = adhocDaoMSSQL.createBeansFromNamedQry(queryNameProvider.getDependencies()/*"getMSSQLDependencies"*/, MsSqlDependencyBean.class, database.name, serverName);
                        for (MsSqlDependencyBean key : keys) {
                            beanDependencyCollector.add(key);
                        }
                    } catch (Exception e) {
                        pumpLogger.pumpLog("Error in getting dependencies for database: " + database.name, null);
                        continue;
                    }
                }
            } catch (Exception e) {
                badServers.add(serverName);
                pumpLogger.pumpLog("Error for server: " + serverNameWithSuffix, null);
                if (logger.isErrorEnabled()) {
                    logger.error("Error for mssql serwer: " + serverNameWithSuffix, e);
                }
                continue;
            }
            addToSuccessDBServer(serverName, databasesOfServer, actualServerNumber);
        }
    }

    protected void addToSuccessDBServer(String serverName, Set<String> databasesOfServer, int actualServerNumber) {
        Set<String> setOnMap = serverDatabases.get(serverName);
        if (setOnMap == null) {
            setOnMap = new HashSet<String>();
        }
        setOnMap.addAll(databasesOfServer);
        serverDatabases.put(serverName, setOnMap);
//            serverDatabases.put(serverName, databasesOfServer);
        actualServerNumber++;
        flushCollectors();
    }

    protected void createCollectors() {
        beanCollector = new AdhocPartialBeanCollector<MsSqlBean>(100, adhocDao, "insertMSSQLMetadata");
        beanIndexCollector = new AdhocPartialBeanCollector<MsSqlIndexBean>(100, adhocDao, "insertMSSQLIndexMetadata");
        beanForeignKeyCollector = new AdhocPartialBeanCollector<MsSqlForeignKeyBean>(100, adhocDao, "insertMSSQLForeignKeyMetadata");
        beanColumnsCollector = new AdhocPartialBeanCollector<MsSqlColumnBean>(100, adhocDao, "insertMSSQLColumnMetadata");
        beanDependencyCollector = new AdhocPartialBeanCollector<MsSqlDependencyBean>(100, adhocDao, "insertMSSQLDependencyMetadata");
        beanExPropertyCollector = new AdhocPartialBeanCollector<MsSqlExPropBean>(100, adhocDao, "insertMSSQLExPropertyMetadata");
    }

    protected void flushCollectors() {
        beanCollector.flush();
        beanIndexCollector.flush();
        beanForeignKeyCollector.flush();
        beanColumnsCollector.flush();
        beanDependencyCollector.flush();
        beanExPropertyCollector.flush();
    }

    protected void optAddTypeFolderIfNeeded(String servName, String databaseName, String objectType, String folderKey) {
        if (!typeSet.contains(folderKey)) {
            beanCollector.add(new MsSqlBean(typeFolderNameMap.get(objectType), TYPE_FOLDER, null, null, folderKey, servName + "|" + databaseName + "|", typeFolderVisualOrderMap.get(objectType)));
            typeSet.add(folderKey);
        }
    }

    protected void createExPropMap(MsSqlExPropConfigBean configData) {
        exPropMap.clear();
        exPropMap.put(TYPE_DATABASE, PumpUtils.getExPropList(configData.exPropDatabase, null).keySet());
        exPropMap.put(TYPE_TABLE, PumpUtils.getExPropList(configData.exPropTable, null).keySet());
        exPropMap.put(TYPE_VIEW, PumpUtils.getExPropList(configData.exPropView, null).keySet());
        exPropMap.put(TYPE_PROCEDURE, PumpUtils.getExPropList(configData.exPropProcedure, null).keySet());
        exPropMap.put(TYPE_FUNCTION, PumpUtils.getExPropList(configData.exPropScalarFunction, null).keySet());
        exPropMap.put(TYPE_TABLE_FUNCTION, PumpUtils.getExPropList(configData.exPropTableFunction, null).keySet());
        exPropMap.put(TYPE_COLUMN, PumpUtils.getExPropList(configData.exPropColumn, null).keySet());
    }
}
