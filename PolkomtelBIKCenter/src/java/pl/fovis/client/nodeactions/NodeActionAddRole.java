/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.TreeNodeDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class NodeActionAddRole extends NodeActionUpdatingBase {

    @Override
    public String getCaption() {
        return I18n.dodajRoleUzytkownika.get() /* I18N:  */;
    }

    @Override
    public void execute() {
        new TreeNodeDialog().buildAndShowDialog(treeBean.nodeKindId, treeBean.nodeKindCode, null,
                node, treeBean.id, null, new IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>>() {
                    public void doIt(final Pair<TreeNodeBean, Map<String, AttributeBean>> param2) {
                      final  TreeNodeBean param=param2.v1;
                        BIKClientSingletons.getService().createTreeNodeWithTheUserRole(param.parentNodeId,
                                param.nodeKindId, param.name, param.objId, param.treeId,
                                new StandardAsyncCallback<Pair<Pair<Integer, Integer>, List<String>>>("Failure on" /* I18N: no */ + " createTreeNode") {
                                    public void onSuccess(Pair<Pair<Integer, Integer>, List<String>> result) {
                                        BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get() /* I18N:  */);
                                        param.id = result.v1.v1;
                                        param.objId = BaseUtils.safeToString(result.v1.v2);
                                        param.isFolder = true;
                                        param.branchIds = (node != null) ? (node.branchIds + param.id + "|") : (param.id + "|");
                                        ctx.addTreeGridNode(param); // a co jeśli drzewo jest puste? wtedy wywali sie na tej linii
                                        refreshRedisplayDataAndSelectRecord(result.v1.v1);
                                        refreshAndRedisplayDataByTreeCode(result.v2);

                                    }
                                });
                    }
                });

    }

    @Override
    public boolean isEnabledByDesign() {

        return (node == null && isInUserRolesTree()
                || isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_USERS_ROLE))
                //ww->authors: przerabiam na eksperta
                && isEffectiveExpert()
                && (node == null || !isNodeBuiltIn());
    }

//    protected boolean canBeEnabledByStateForThisNode() {
//        return !isTreeReadOnlyInner() && (node == null || isNodeFolder()) && !isNodeLinkedAnyhow();
//    }
//
//    @Override
//    protected boolean canBeEnabledByStateForThisNodeAndUserLoggedInByDesign() {
//        return canBeEnabledByStateForThisNode() && isEffectiveExpert();
//    }
}
