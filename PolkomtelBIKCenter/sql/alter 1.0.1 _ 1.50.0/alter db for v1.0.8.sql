exec sp_check_version '1.0.8';
go
----------------------------------------------------------------------
----------------------------------------------------------------------

----------------------------------------------------------------------
----------------------------------------------------------------------
--alter table bik_object_in_fvs drop constraint uk_bik_object_in_fvs;
--lub alter table bik_object_in_fvs drop constraint uk_aaa_object_in_fvs;  
drop table bik_object_in_fvs;
CREATE TABLE bik_object_in_fvs (
	id int IDENTITY(1,1) NOT NULL primary key,
	user_id int not null,
	date_added datetime not null,
	node_id int references bik_node (id) not null,
	constraint uk_bik_object_in_fvs unique (user_id, node_id)
);




drop table bik_object_in_history;

CREATE TABLE bik_object_in_history (
	id int IDENTITY(1,1) NOT NULL primary key,
	user_id int not null,
	node_id int references bik_node (id) not null,
	date_added datetime not null,
);







----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

insert into bik_attribute (name, node_kind_id) 
values ('Alternatywna nazwa', (select ID from bik_node_kind where code = 'Tezaurus'))


----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------



alter table dbo.bik_metapedia add node_id int 
go
update bik_metapedia set node_id = (select tree_node_id from aaa_mpart where bik_metapedia.subject = aaa_mpart.subject)

----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

exec sp_update_version '1.0.8', '1.0.9';
go

