/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.mssql;

/**
 *
 * @author tflorczak
 */
public class MsSqlIndexBean {

    public String name;
    public String server;
    public String owner;
    public String databaseName;
    public String tableName;
    public String columnName;
    public Integer columnPosition;
    public Boolean isUnique;
    public String type;
    public Boolean isPrimaryKey;
    public Integer partitionOrdinal;
    public String partitionSchema;
    public String partitionFunction;
    public String partitionFunctionType;

    public MsSqlIndexBean() {
    }
}
