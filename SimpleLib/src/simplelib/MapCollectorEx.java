/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import java.util.LinkedHashMap;
import java.util.Map;
import simplelib.BaseUtils.Projector;

/**
 *
 * @author wezyr
 */
public class MapCollectorEx<K, V, V2> implements ILameCollector<V> {
    private Map<K, V2> map;
    private Projector<? super V, K> keyPreProj;
    private Projector<? super V2, K> keyPostProj;
    private Projector<? super V, V2> itemProj;

    public MapCollectorEx(Projector<? super V, K> keyPreProj,
            Projector<? super V, V2> itemProj, Projector<? super V2, K> keyPostProj) {
        this.keyPreProj = keyPreProj;
        this.itemProj = itemProj;
        this.keyPostProj = keyPostProj;
    }

    public void setMap(Map<K, V2> map) {
        this.map = map;
    }

    protected Map<K, V2> createDefaultMap() {
        return new LinkedHashMap<K, V2>();
    }

    protected V2 convertItem(V item) {
        return itemProj.project(item);
    }

    protected void ensureMap() {
        if (map == null) {
            map = createDefaultMap();
        }

    }

    public void add(V item) {
        if (keyPreProj == null && keyPostProj == null) {
            throw new NullPointerException("projectors are both nulls");
        }

        ensureMap();

        V2 item2 = convertItem(item);
        K key;

        if (keyPreProj != null) {
            key = keyPreProj.project(item);
        } else {
            key = keyPostProj.project(item2);
        }

        map.put(key, item2);
    }

    public Map<K, V2> getMap() {
        ensureMap();
        return map;
    }
}
