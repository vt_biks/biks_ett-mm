/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

/**
 *
 * @author mmastalerz
 */
public interface IMyBIKSPage {

    public enum ReachableMyBIKSTabCode {Tutorial, EditUser};
    
    public void refreshAvatarImg();

    public void refreshTabButton(IMyBIKSTab tab);
    
    public void showTabByCode(ReachableMyBIKSTabCode tabCode);
}
