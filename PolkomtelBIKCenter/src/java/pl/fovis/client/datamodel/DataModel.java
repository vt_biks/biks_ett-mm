/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.datamodel;

import com.allen_sauer.gwt.dnd.client.DragEndEvent;
import com.allen_sauer.gwt.dnd.client.DragHandler;
import com.allen_sauer.gwt.dnd.client.DragStartEvent;
import com.allen_sauer.gwt.dnd.client.VetoDragException;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.ViewDataModelInFullScreenDialog;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.ErwinDiagramDataBean;
import pl.fovis.common.ErwinRelationshipBean;
import pl.fovis.common.ErwinShapeBean;
import pl.fovis.common.ErwinSubjectAreaBean;
import pl.fovis.common.ErwinSubjectAreaObjectRefBean;
import pl.fovis.common.ErwinTableBean;
import pl.fovis.common.ErwinTableColumnBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.PushButtonWithHideText;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.tecna.gwt.connectors.client.Diagram;
import simplelib.BaseUtils;

/**
 *
 * @author pgajda
 */
public class DataModel {

    public static int DATA_WIDGET_ADDITIONAL_X = 300;
    public static int DATA_WIDGET_ADDITIONAL_Y = 1000;
    public static int DATA_WIDGET_ADDITIONAL_LEFT_X = 15;
    public static double FONT_SIZE = 13;
    protected AbsolutePanel panelMain;
    protected HorizontalPanel controlPanel;
    protected WindowController windowController;
    protected double scale = 1.0;
    protected String activeSubjectAreaLongId;
    protected String preferredSubjectAreaLongIdToFirstShow;
    protected PushButtonWithHideText fullScreenBtn;
    protected EntityDetailsDataBean eddb;
    protected boolean isInDialogMode = false;
    protected String activeNodeObjId;
    protected ListBox listSubjectArea;

    public DataModel(boolean isInDialogMode) {
        this.isInDialogMode = isInDialogMode;
    }

    public DataModel(EntityDetailsDataBean eddb, boolean isInDialogMode, String preferredSubjectAreaLongIdToFirstShow) {
        this.eddb = eddb;
        this.isInDialogMode = isInDialogMode;
        this.preferredSubjectAreaLongIdToFirstShow = preferredSubjectAreaLongIdToFirstShow;
    }

    public double getScale() {
        return scale;
    }

    public void setEntityDetailsDataBean(EntityDetailsDataBean eddb) {
        this.eddb = eddb;
    }

    public EntityDetailsDataBean getEntityDetailsDataBean() {
        return this.eddb;
    }

    public WindowController getWindowController() {
        return windowController;
    }

    public void displayData() {

        downloadAllData();
    }

    public void displayAfterDataDown() {

        int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;

        if (eddb == null || eddb.erwinListTables == null) {
            return;
        }
        panelMain.clear();

        AbsolutePanel panel = new AbsolutePanel();;

        HashMap<String, ErwinShapeBean> shapeMap = new HashMap<String, ErwinShapeBean>();
        HashMap<String, DialogTable> dialogTableMap = new HashMap<String, DialogTable>();
        HashMap<String, HashSet<String>> subjectAreaObcject = new HashMap<String, HashSet<String>>();

        if (eddb.erwinErwinSubjectAreaObjectRefs != null) {
            for (ErwinSubjectAreaObjectRefBean ref : eddb.erwinErwinSubjectAreaObjectRefs) {
                HashSet<String> references = null;
                if (subjectAreaObcject.containsKey(ref.subjectAreaLongId)) {
                    references = subjectAreaObcject.get(ref.subjectAreaLongId);
                } else {
                    references = new HashSet<String>();
                    subjectAreaObcject.put(ref.subjectAreaLongId, references);
                }

                references.add(ref.objectLongId);
            }

            if (eddb.erwinErwinSubjectAreaObjectRefs.size() > 0 && activeSubjectAreaLongId == null) {
                activeSubjectAreaLongId = eddb.erwinErwinSubjectAreaObjectRefs.get(1).subjectAreaLongId;
            }

        }

        HashSet<String> allowedObjects = subjectAreaObcject.get(activeSubjectAreaLongId);

        if (eddb.erwinShapes != null) {
            for (ErwinShapeBean shape : eddb.erwinShapes) {
                shapeMap.put(shape.shapeId, shape);
                //shapeMap.put(shape.objectLongId, shape); ///!!!!TU BYLO ORYGINALNIE PRZEZ DLUGI CZAS
            }

            for (ErwinTableBean tab : eddb.erwinListTables) {
                if (!isObjectAllowed(allowedObjects, tab.tableId)) {
                    continue;
                }
                //ErwinShapeBean shape = shapeMap.get(tab.tableId);//!!!!
                ErwinShapeBean shape = shapeMap.get(tab.shapeID);
                if (shape.anchorPointX < minX) {
                    minX = shape.anchorPointX;
                }
                if (shape.anchorPointY < minY) {
                    minY = shape.anchorPointY;
                }
                if (shape.anchorPointX > maxX) {
                    maxX = shape.anchorPointX;
                }
                if (shape.anchorPointY > maxY) {
                    maxY = shape.anchorPointY;
                }
            }
        } else {
            minX = 0;
            maxX = 1000;
            minY = 0;
            maxY = 1000;
        }

        panel.setPixelSize((int) ((maxX - minX) * scale + DATA_WIDGET_ADDITIONAL_X), (int) ((maxY - minY) * scale) + DATA_WIDGET_ADDITIONAL_Y);
        panelMain.setPixelSize((int) ((maxX - minX) * scale + DATA_WIDGET_ADDITIONAL_X), (int) ((maxY - minY) * scale) + DATA_WIDGET_ADDITIONAL_Y);

        final Diagram diagram = new Diagram(panel);
        DialogTable activeDialogTable = null;

        String prefObjId = "DWH|" + listSubjectArea.getItemText(listSubjectArea.getSelectedIndex()) + "|";

        for (ErwinTableBean tab : eddb.erwinListTables) {

            if (!isObjectAllowed(allowedObjects, tab.tableId)) {
                continue;
            }

            DialogTable dt = new DialogTable(this, tab.name, tab.tableComment, prefObjId + modifyTableNameForObjId(tab.name) + "|");
            dialogTableMap.put(tab.tableId, dt);

            //ErwinShapeBean shape = shapeMap.get(tab.tableId);
            ErwinShapeBean shape = shapeMap.get(tab.shapeID);
            int posLeft = (int) ((shape.anchorPointX - minX) * scale) + DATA_WIDGET_ADDITIONAL_LEFT_X;
            int posTop = (int) (((maxY - minY) - (shape.anchorPointY - minY)) * scale);
            dt.setPosition(posLeft, posTop);
            panel.add(dt, posLeft, posTop);

            if (tab.objId != null && activeNodeObjId.equalsIgnoreCase(tab.objId)) {
                dt.setActive(true);
                activeDialogTable = dt;
            }
        }

        for (ErwinTableColumnBean col : eddb.erwinTableColumns) {
            DialogTable dt = dialogTableMap.get(col.tableId);
            if (dt != null) {
                boolean isColActive = col.objId != null && activeNodeObjId.equalsIgnoreCase(col.objId);

                dt.addTableColumn(col.tableColumnId, col.name, col.columnComment,
                        prefObjId + modifyTableNameForObjId(dt.getTableName()) + "|" + col.name + "|",
                        isColActive
                );
                if (isColActive) {
                    activeDialogTable = dt;
                }
            }
        }

        if (activeDialogTable != null) {
            int posLeft = activeDialogTable.getPositionLeft();
            int posTop = activeDialogTable.getPositionTop();
            setScrollPosition((posLeft - 200) > 0 ? (posLeft - 200) : posLeft, (posTop - 200) > 0 ? (posTop - 200) : posTop);
        }

        final HashMap<String, DialogTableConnector> dialogTableConnectors = new HashMap<String, DialogTableConnector>();

        for (ErwinRelationshipBean rel : eddb.erwinRelationships) {
            DialogTableConnector tmpConn = null;

            DialogTable dtParent = dialogTableMap.get(rel.parentTableLongId);
            DialogTable dtChild = dialogTableMap.get(rel.childTableLongId);

            if (dtParent == null || dtChild == null) {
                continue;
            }

            if (dialogTableConnectors.containsKey(rel.relationshipName)) {
                tmpConn = dialogTableConnectors.get(rel.relationshipName);
            } else {
                tmpConn = new DialogTableConnector(rel.relationshipName, diagram);
                dialogTableConnectors.put(rel.relationshipName, tmpConn);
                Widget startWidget = dtParent.getColumnWidget(rel.parentColLongId);
                Widget endWidget = dtChild.getColumnWidget(rel.childColLongId);

                tmpConn.setStartPoint(startWidget);
                tmpConn.setEndPoint(endWidget);
                dtParent.addTableConnector(tmpConn);
                dtChild.addTableConnector(tmpConn);

                String parentTableName = dtParent.getTableName();
                String childTableName = dtChild.getTableName();

                tmpConn.setStartTableName(parentTableName);
                tmpConn.setEndTableName(childTableName);
            }

            String startColumnName = dtParent.getColumnName(rel.parentColLongId);
            String endColumnName = dtChild.getColumnName(rel.childColLongId);
            String startPointComment = tmpConn.getStartPointComment();
            String endPointComment = tmpConn.getEndtPointComment();

            startPointComment += "\n" + startColumnName + ";";
            endPointComment += "\n" + endColumnName + ";";
            tmpConn.setStartPointComment(startPointComment);
            tmpConn.setEndPointComment(endPointComment);

        }

        panelMain.add(panel);

        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
            @Override
            public void execute() {
                //dtc.drawConnection();
                for (Iterator<DialogTableConnector> it = dialogTableConnectors.values().iterator(); it.hasNext();) {
                    DialogTableConnector tmpConn = it.next();
                    if (tmpConn != null) {
                        tmpConn.drawConnection();
                    }
                }
            }
        });

        windowController.getPickupDragController().addDragHandler(new DragHandler() {
            public void onDragEnd(DragEndEvent event) {
                //dtc.drawConnection(diagram);
                DialogTable dt = (DialogTable) event.getSource();
                dt.doOnDragEnd();
            }

            public void onDragStart(DragStartEvent event) {
            }

            public void onPreviewDragEnd(DragEndEvent event) throws VetoDragException {
            }

            public void onPreviewDragStart(DragStartEvent event) throws VetoDragException {
            }
        });

    }

    protected boolean isObjectAllowed(HashSet<String> allowedObjects, String objectId) {
        if (eddb.erwinErwinSubjectAreaObjectRefs == null || eddb.erwinErwinSubjectAreaObjectRefs.isEmpty()
                || (allowedObjects != null && allowedObjects.contains(objectId))) {
            return true;
        }
        return false;
    }

    public Widget buildWidgets() {

        panelMain = new AbsolutePanel();
        panelMain.setStyleName("dataModel");
        windowController = new WindowController(panelMain);
        return panelMain;
    }

    public Widget buildSimleContlolWidgets() {

        if (controlPanel == null) {
            controlPanel = new HorizontalPanel();
            Widget perListBox = getPercentageListBox();
            controlPanel.add(perListBox);
            controlPanel.setCellWidth(perListBox, "70px");

            Label label = new Label(I18n.obszarDanych.get() + " : ");
            controlPanel.add(label);
            controlPanel.setCellWidth(label, "110px");
            controlPanel.setCellHorizontalAlignment(label, HasHorizontalAlignment.ALIGN_RIGHT);
            controlPanel.add(getSubjectAreaListBox());

            controlPanel.setWidth("100%");

            selectAreaOnListBox();
            return controlPanel;
        } else {
            return controlPanel;
        }
    }

    public Widget buildContlolWidgets() {

        if (controlPanel == null) {
            buildSimleContlolWidgets();
            Widget fsBtn = getFullScreenBtn();
            controlPanel.add(fsBtn);
            controlPanel.setCellHorizontalAlignment(fsBtn, HasHorizontalAlignment.ALIGN_RIGHT);
        }

        return controlPanel;
    }

    protected Widget getPercentageListBox() {
        final ListBox listPercentage = new ListBox();
        for (int per = 100; per >= 10; per -= 10) {
            listPercentage.addItem(per + "%", per + "");
        }

        listPercentage.addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                scale = (double) BaseUtils.tryParseInteger(listPercentage.getValue(listPercentage.getSelectedIndex()), 100) / 100.0;
                displayAfterDataDown();
            }
        });

        listPercentage.setWidth("70px");
        return listPercentage;
    }

    protected Widget getSubjectAreaListBox() {
        listSubjectArea = new ListBox();
        if (eddb == null) {
            return listSubjectArea;
        }
        String selectedAreaOnTrea = null;
        if (eddb.node != null && eddb.node.objId != null) {
            List<String> areaList = BaseUtils.splitBySep(eddb.node.objId, "|");
            if (areaList.size() >= 1) {
                selectedAreaOnTrea = areaList.get(1);
            }
        }

        if (eddb.erwinSubjectAreas.isEmpty()) {
            listSubjectArea.addItem("<Model>", "000000");
        } else {
            activeSubjectAreaLongId = eddb.erwinSubjectAreas.get(0).subjectAreaId;
            for (ErwinSubjectAreaBean esab : eddb.erwinSubjectAreas) {
                listSubjectArea.addItem(esab.name, esab.subjectAreaId);

            }
        }
        listSubjectArea.addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                activeSubjectAreaLongId = listSubjectArea.getValue(listSubjectArea.getSelectedIndex());
                displayData();
            }
        });

        return listSubjectArea;
    }

    protected void selectAreaOnListBox() {

        String selectedAreaOnTrea = null;
        if (eddb.node != null && eddb.node.objId != null) {
            List<String> areaList = BaseUtils.splitBySep(eddb.node.objId, "|");
            if (areaList.size() >= 1) {
                selectedAreaOnTrea = areaList.get(1);
            }
        }

        if (selectedAreaOnTrea == null || listSubjectArea == null) {
            return;
        }

        int listSize = listSubjectArea.getItemCount();
        for (int ind = 0; ind < listSize; ind++) {

            String area = listSubjectArea.getItemText(ind);
            String areaId = listSubjectArea.getValue(ind);
            String areaTmp = area.replaceFirst("^([0-9_]*)", "");

            if (area.equalsIgnoreCase(selectedAreaOnTrea) || areaTmp.equalsIgnoreCase(selectedAreaOnTrea)) {
                listSubjectArea.setSelectedIndex(ind);
                activeSubjectAreaLongId = areaId;
            } else if (preferredSubjectAreaLongIdToFirstShow != null && areaId.equals(preferredSubjectAreaLongIdToFirstShow)) {
                listSubjectArea.setSelectedIndex(ind);
                activeSubjectAreaLongId = areaId;
            }
        }

    }

    protected Widget getFullScreenBtn() {

        fullScreenBtn = new PushButtonWithHideText(new Image("images/fullScreen.gif" /* I18N: no */));
        fullScreenBtn.setStyleName("BizDef-vWidget");
        fullScreenBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                new ViewDataModelInFullScreenDialog(eddb, activeSubjectAreaLongId).buildAndShowDialog();
            }
        });
        fullScreenBtn.setTitle(I18n.trybPełnoekranowy.get() /* I18N:  */);
        fullScreenBtn.setVisible(true);

        return fullScreenBtn;
    }

    public void downloadAllData() {

        if (eddb != null && eddb.node != null) {
            this.activeNodeObjId = eddb.node.objId.replaceAll("DWH\\|[^\\|]+", "DWH");
        }

        BIKClientSingletons.getService().getErwinListTables(activeSubjectAreaLongId, new StandardAsyncCallback<ErwinDiagramDataBean>("error in" /* I18N: no:err-fail-in */ + " getErwinListTables()") {
                    public void onSuccess(ErwinDiagramDataBean result) {
                        eddb.erwinListTables = result.erwinListTables;
                        eddb.erwinTableColumns = result.erwinTableColumns;
                        eddb.erwinRelationships = result.erwinRelationships;
                        eddb.erwinShapes = result.erwinShapes;

                        displayAfterDataDown();
                    }
                });
    }

    protected String modifyTableNameForObjId(String tableName) {

        if (tableName != null && tableName.length() > 2 && (tableName.charAt(0) == 'T' || tableName.charAt(0) == 't')) {

            int underscoreInd = tableName.indexOf('_');
            int tmpChar;

            for (int ind = 1; ind < underscoreInd; ind++) {
                tmpChar = tableName.charAt(ind);
                if (tmpChar < '0' || tmpChar > '9') {
                    return tableName;
                }
            }
            return tableName.substring(0, underscoreInd);
        } else {
            return tableName;
        }
    }

    protected void setScrollPosition(int left, int top) {

    }
}
