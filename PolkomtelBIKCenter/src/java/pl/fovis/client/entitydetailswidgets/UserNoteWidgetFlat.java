/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.NoteBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Pair;

/**
 *
 * @author mgraczkowski
 */
public class UserNoteWidgetFlat extends //EntityDetailsTabBase /*NoteWidgetBase *EntityDetailsWidgetFlatBase*/
        EntityDetailsTabWithItemsInEditableGridBase<NoteBean> {

    protected List<Pair<PushButton, Integer>> buttonsMap = new ArrayList<Pair<PushButton, Integer>>();
    private VerticalPanel mainNoteVp;
    public VerticalPanel innerNoteEntriesVp;
//    private ScrollPanel scPanel;
//    private VerticalPanel innerNoteEntriesVp;

    @Override
    public void displayData() {
        int noteCnt = BaseUtils.collectionSizeFix(items);
        innerNoteEntriesVp.clear();

        if (noteCnt == 0) {
            innerNoteEntriesVp.add(zeroMsg(I18n.brakKomentarzy.get() /* I18N:  */));
        } else {
            //andrzej

            FlexTable gp = new FlexTable();
            gp.setWidth("100%");
//            gp.setStyleName("gridJoinedObj");
            gp.setStyleName("newNoteGrid");
            setUpGridColumnsWidths(gp);
            int row = 0;
            for (int i = 0; i < noteCnt; i++) {
                final NoteBean note = items.get(i);

                Label nodeNameAtNote = new Label(note.nodeName);
                nodeNameAtNote.setStyleName("gp-label");
                gp.setWidget(row, 0, createSubjectAndBody(note.title, note.body));
                gp.setWidget(row, 1, nodeNameAtNote);
                gp.setWidget(row, 2, createDateAdded(note.dateAdded));
                gp.setWidget(row, 3, createFollowButton(note.nodeTreeCode, note.nodeId));

                gp.getCellFormatter().setStyleName(row, 0, "Notes" /* i18n: no:css-class */ + "-innerOneNoteEntryFp");
                gp.getCellFormatter().setStyleName(row, 1, "bg" /* I18N: no */);
                gp.getCellFormatter().setStyleName(row, 2, "bg" /* I18N: no */);
                gp.getCellFormatter().setStyleName(row, 3, "bg" /* I18N: no */);
                gp.getCellFormatter().setVerticalAlignment(row, 1, HasVerticalAlignment.ALIGN_TOP);
                gp.getCellFormatter().setVerticalAlignment(row, 2, HasVerticalAlignment.ALIGN_TOP);
                gp.getCellFormatter().setVerticalAlignment(row, 3, HasVerticalAlignment.ALIGN_TOP);

                gp = makeGridSpace(row, 4, gp);
                row = row + 2;

            }
            innerNoteEntriesVp.add(gp);

        }
    }

    @Override
    public Widget buildWidgets() {
        mainNoteVp = new VerticalPanel();
        innerNoteEntriesVp = new VerticalPanel();

        mainNoteVp.setWidth("100%");
        mainNoteVp.setStyleName("Notes" /* i18n: no:css-class */ + "-mainNoteVp");
        innerNoteEntriesVp.setWidth("100%");
        innerNoteEntriesVp.addStyleName("Notes" /* i18n: no:css-class */ + "-innerNoteEntriesVp");
//        scPanel = new ScrollPanel();
//        scPanel.add(innerNoteEntriesVp);
//        scPanel.setHeight(BIKClientSingletons.ORIGINAL_EDP_BODY + (BIKClientSingletons.isEffectiveExpertLoggedIn() ? 28 : 0) + "px");// bf:-->
//        mainNoteVp.add(scPanel);
        mainNoteVp.add(addScrollPanel(innerNoteEntriesVp));
//        mainNoteVp.add(innerNoteEntriesVp);

        return mainNoteVp;
    }

    protected void setUpGridColumnsWidths(HTMLTable gp) {
        gp.setWidth("100%");
        gp.getColumnFormatter().setWidth(0, "100%");
//        gp.getColumnFormatter().setWidth(2, "10%");
        gp.getColumnFormatter().setWidth(1, "200px");
        gp.getColumnFormatter().setWidth(2, "145px");
        gp.getColumnFormatter().setWidth(3, "26px");
    }

    //ww\jo: to chyba zbędna metoda? po co ona jest stworzona? nie można jej nie
    // pisać? skoro nic nie robi, nikt z niej nie korzysta itp.?
//    @Override
//    protected PushButton createDeleteButton(int nodeId, int paramNodeId, Object ob) {
//        return null;
//    }
    public FlowPanel createSubjectAndBody(String title, String body) {
        FlowPanel innerOneNoteEntryFp = new FlowPanel();
        innerOneNoteEntryFp.setStyleName("Notes" /* i18n: no:css-class */ + "-innerOneNoteEntryFp");
        HTML lab = new HTML("<b>" + BaseUtils.encodeForHTMLTag(title) + "</b>");
        lab.setStyleName("Notes" /* i18n: no:css-class */ + "-noteTitle");
        innerOneNoteEntryFp.add(lab);
        if (!BaseUtils.isStrEmptyOrWhiteSpace(body)) {
            HTML labBody = new HTML();
            labBody.setStyleName("Notes" /* i18n: no:css-class */ + "-labBody");
            labBody.setHTML(BaseUtils.encodeForHTMLTag(body));
            innerOneNoteEntryFp.add(labBody);
        }
        return innerOneNoteEntryFp;
    }

    @Override
    protected List<NoteBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
//        return data.notes;
        return BaseUtils.isCollectionEmpty(items) ? new ArrayList<NoteBean>() : items;
    }

    @Override
    public String getCaption() {
        return BIKGWTConstants.MENU_NAME_USER_COMMENTS;
    }

    @Override
    public boolean hasContent() {
        return isNodeKindCode(getNodeKindCode(), BIKGWTConstants.NODE_KIND_USER) && getItemCount() > 0;
//        return isInUserTree(data.node.treeCode);
    }

    @Override
    public boolean hasEditableControls() {
        return false;
    }

    @Override
    public String getTreeKindForSelected() {
        //ww->dynTrees: tu nie wiem czy nie TREE_KIND_USER_ROLES???
        //bf-> BIKGWTConstants.TREE_KIND_USER_ROLES -> zakladka kometnarze na drzewkach z uzytkownikami(np grupa uzytkowników)
        //zostanei ukryta gdy ukryjemy drzewko z rolami. Może retrun null -> wtedy nie będzie ukrywana.
        return null;
    }

    @Override
    public void optCallServiceToGetData(final IContinuation con) {
        BIKClientSingletons.getService().getNotes(nodeId, new AsyncCallback<List<NoteBean>>() {

            @Override
            public void onFailure(Throwable caught) {
                displayData();
            }

            @Override
            public void onSuccess(List<NoteBean> result) {
                items = result;
                con.doIt();
            }
        });
    }

    @Override
    public boolean isEnabled() {
        String nodeKindCode = getNodeKindCode();
        return BIKConstants.NODE_KIND_USER.equals(nodeKindCode);
    }
}
