﻿exec sp_check_version '1.2.5';
go

-----------------------------------------------------------------------
-- UWAGA! Autorem skryptu nie jest PG, tylko TF! Pomyłka w nazwie :P --
-----------------------------------------------------------------------

exec sp_add_menu_node '&FullClient', '@', '&ReportSchedule'

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

exec sp_update_version '1.2.5', '1.3';
go