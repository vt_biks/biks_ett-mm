/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.Image;

/**
 *
 * @author ctran
 */
public abstract class SimpleGridBeanAction<T> implements IGridBeanAction<T> {

    protected Image actImg;

    @Override
    public void setActImage(Image img) {
        actImg = img;
    }

    @Override
    public Image getActImage() {
        return actImg;
    }

}
