/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.menu;

import java.util.List;

/**
 *
 * @author wezyr
 */
public interface IMenuItemSubMenu extends IMenuItem {

    public List<IMenuItem> getSubItems();
}
