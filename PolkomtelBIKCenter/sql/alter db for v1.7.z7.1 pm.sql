exec sp_check_version '1.7.z7.1';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

-- poprzednia wersja w pliku: "alter db for v1.6.z9.13 tf.sql"
-- dodanie opcjonalnego filtrowania do poddrzewa
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_filter_bik_nodes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_filter_bik_nodes]
go

create procedure [dbo].[sp_filter_bik_nodes] (@txt varchar(max), 
  @opt_Sub_Tree_Root_Node_Id int,
  @tree_ids_filter varchar(8000) /*@optTreeId int*/, @opt_bik_node_filter varchar(max),
  @opt_extra_fields varchar(max))
as
begin
  set nocount on
  
  declare @diag_level int = 0
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': start!'
  
  declare @subtree_branch_ids varchar(max) = null;

  if @opt_Sub_Tree_Root_Node_Id is not null begin

    set @subtree_branch_ids = (select branch_ids from bik_node where id = @opt_Sub_Tree_Root_Node_Id);

    declare @branch_ids_patt varchar(max) = @subtree_branch_ids + '%';
    
    set @opt_bik_node_filter = case when @opt_bik_node_filter is null then '' else @opt_bik_node_filter + ' and ' end
      + ' branch_ids like ' + QUOTENAME(@branch_ids_patt, '''');
  end;

declare @sql varchar(max);

set @sql = 'select id from bik_node where id in (' + dbo.fn_bik_node_name_chunk_search_sql(@txt, @tree_ids_filter)--@optTreeId) 
  + ') and name like ''%' + replace(@txt, '''', '''''') + '%'' and is_deleted = 0' +
  case when @opt_bik_node_filter is null then '' else ' and (' + @opt_bik_node_filter + ')' end +
  case when @tree_ids_filter is null then '' else ' and (' + @tree_ids_filter + ')' end
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': have sql: sql=' + @sql
  -- print @sql
-- ostatnio wrzucone
declare @t table (id int not null primary key, parent_node_id int null);
-- parent_node_ids ostatnio wrzuconych - te będziemy wrzucać teraz
declare @p table (id int not null primary key);
-- wszystkie

if not exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##temp_tab_sp_filter_bik_nodes'))
  create table ##temp_tab_sp_filter_bik_nodes (spid int not null, id int not null, unique (spid, id));
else
  delete from ##temp_tab_sp_filter_bik_nodes where spid = @@spid;
  
  --declare @a table (id int not null primary key);

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': before exec sql'

  insert into @p (id)
  exec(@sql)

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': sql executed'
  
  while exists (select 1 from @p) begin
    --insert into @a select id from @p;
    insert into ##temp_tab_sp_filter_bik_nodes (spid, id) select @@spid, id from @p;

    delete from @t;

    insert into @t (id, parent_node_id) select id, parent_node_id from bik_node where id in (select id from @p);

    delete from @p;

    insert into @p 
    select distinct parent_node_id 
    from @t 
    --where parent_node_id is not null and parent_node_id not in (select id from @a);
    where parent_node_id is not null and parent_node_id not in (select id from ##temp_tab_sp_filter_bik_nodes where spid = @@spid);
  end;

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': after loop'

  declare @sql_txt_x varchar(max) = '
select bn.*,
    bnk.caption as node_kind_caption,
    bnk.code as node_kind_code,
    bnk.is_folder as is_folder,
    bt.name as tree_name, bt.tree_kind, bt.code as tree_code
    , case when disable_linked_subtree = 0 and exists 
        (select 1 from bik_node where is_deleted = 0 and parent_node_id = coalesce(bn.linked_node_id, bn.id)) then 0 else 1 end as has_no_children' +
        case when ltrim(rtrim(coalesce(@opt_extra_fields, ''))) <> '' then ', ' + @opt_extra_fields else '' end + '
    from bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    inner join bik_tree bt on bn.tree_id = bt.id
    --where bn.id in (select id from @a)
    where bn.id in (select id from ##temp_tab_sp_filter_bik_nodes where spid = ' + cast(@@spid as varchar(20)) + ')
    ' + case when @opt_Sub_Tree_Root_Node_Id is not null then ' and branch_ids like ' + QUOTENAME(@branch_ids_patt, '''') else '' end + '
    order by bn.visual_order, bnk.is_folder desc, bn.name
';

  exec(@sql_txt_x);

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': after final select'
end;
go

-- exec sp_filter_bik_nodes 'zlec', 2422712, 'tree_id in (34)', 'is_deleted = 0', 'descr'
-- select * from bik_tree where code = 'Reports2'

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z7.1', '1.7.z7.2';
go
