/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.dqm;

import java.util.Date;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.dqm.DQMReportScopeDialog;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class NodeActionDQMAddReport extends NodeActionDQMBase {

    public static final String DQM_COORDINATOR_FOLDER_OBJ_ID = "#dqmCoordinationFolder";

    @Override
    public boolean isEnabledByDesign() {
        return node != null && isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_DQM_FOLDER_COORDINATION) && BaseUtils.safeEquals(node.objId, DQM_COORDINATOR_FOLDER_OBJ_ID);
    }

    @Override
    public String getCaption() {
        return I18n.dodajRaportJakosciDanych.get();
    }

    @Override
    public void execute() {
        new DQMReportScopeDialog().buildAndShowDialog(new IParametrizedContinuation<Pair<Date, Date>>() {

            @Override
            public void doIt(Pair<Date, Date> param) {
                BIKClientSingletons.getDQMService().generateDQMReport(data.node.id, param.v1, param.v2, new StandardAsyncCallback<Integer>("Error in generateDQMReport") {

                    @Override
                    public void onSuccess(Integer result) {
                        BIKClientSingletons.showInfo(I18n.wygenerowanoRaport.get());
                        refreshRedisplayDataAndSelectRecord(result);
                    }
                });
            }
        });
    }
}
