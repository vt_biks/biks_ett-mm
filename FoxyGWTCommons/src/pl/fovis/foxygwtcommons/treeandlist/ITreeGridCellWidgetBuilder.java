/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.foxygwtcommons.treeandlist;

import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author wezyr
 */
public interface ITreeGridCellWidgetBuilder<ROW> {
    public Widget getOptWidget(ROW row, String colName);
}
