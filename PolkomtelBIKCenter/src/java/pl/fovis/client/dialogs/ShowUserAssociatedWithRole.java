///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package pl.fovis.client.dialogs;
//
//import com.google.gwt.user.client.ui.CheckBox;
//import com.google.gwt.user.client.ui.RadioButton;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import pl.fovis.common.SystemUserBean;
//import pl.fovis.common.i18npoc.I18n;
//import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
//import simplelib.IParametrizedContinuation;
//import simplelib.Pair;
//
///**
// *
// * @author Eugeniusz
// */
//public class ShowUserAssociatedWithRole extends BaseActionOrCancelDialog {
//
//    protected IParametrizedContinuation<Pair<List<Integer>, List<String>>> saveCont;
//    protected List<String> usersInGroup;
//    protected List<SystemUserBean> allUsers;
//    protected List<Integer> usersIdsInGroup = new ArrayList<Integer>();
//    protected Map<Integer, CheckBox> checkBoxMaps;
//    protected Map<Integer, RadioButton> radioButtonMaps;
//    protected HashMap<Integer, String> idToNamesMap;
//    protected boolean isWithCheckbox;
//    protected String caption;
//
//    @Override
//    protected String getDialogCaption() {
//        return I18n.usersAssociatedWithRole.get();
//    }
//
//    @Override
//    protected String getActionButtonCaption() {
//        return I18n.save.get();
//        //actionBtn   make it not visible
//    }
//
//    public void buildAndShowDialog(List<String> usersInGroup, List<SystemUserBean> allUsers,
//            String caption, boolean isWithCheckbox,
//            IParametrizedContinuation<Pair<List<Integer>, List<String>>> saveCont) {
//        this.saveCont = saveCont;
//        this.usersInGroup = usersInGroup;
//        this.allUsers = allUsers;
//        //nie jest potrzebny
//        this.caption = caption;
//        this.isWithCheckbox = isWithCheckbox;
//        super.buildAndShowDialog();
//    }
//
//    @Override
//    protected void doAction() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    protected void buildMainWidgets() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//}
