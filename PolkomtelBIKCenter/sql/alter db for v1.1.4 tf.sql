﻿exec sp_check_version '1.1.4';
go

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

	declare @node_kind_id int;
    select @node_kind_id = id
    from bik_node_kind
    where code = 'ReportQuery'

    update bik_node
    set obj_id = convert(varchar(30),aaa_query.report_node_id) + '|' + convert(varchar(30),aaa_query.universe_cuid) + '|' +  aaa_query.id_text
    from bik_node join aaa_query on bik_node.parent_node_id = aaa_query.report_node_id
    and obj_id = convert(varchar(30),aaa_query.report_node_id) + '|' + convert(varchar(30),aaa_query.universe_cuid) + '|' +  aaa_query.name
    join bik_node n3 on n3.id = aaa_query.report_node_id
    where bik_node.node_kind_id = @node_kind_id
    

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

exec sp_update_version '1.1.4', '1.1.4.1';
go
