/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.dqc;

import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.MetadataTestConnectionBase;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class DQCTestConnection extends MetadataTestConnectionBase {

    protected String server;
    protected String instance;
    protected String user;
    protected String password;
    protected String database;
    protected IBeanConnectionEx<Object> dqcConncetion = null;

    public DQCTestConnection(MssqlConnectionConfig mcfg) {
        super(mcfg);
    }

    @Override
    public Pair<Integer, String> testConnection() {
        try {
            server = execNamedQuerySingleVal("getConfigValue", false, "value", "dqc.server");
            instance = execNamedQuerySingleVal("getConfigValue", false, "value", "dqc.instance");
            database = execNamedQuerySingleVal("getConfigValue", false, "value", "dqc.database");
            user = execNamedQuerySingleVal("getConfigValue", false, "value", "dqc.user");
            password = execNamedQuerySingleVal("getConfigValue", false, "value", "dqc.password");
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Invalid login for data loading! Error: " + e.getMessage());
        }
        try {
            dqcConncetion = new MssqlConnection(
                    new MssqlConnectionConfig(server, instance, database, user, password));
            if (dqcConncetion == null) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connection with DQC DB!");
            }
            List<Map<String, Object>> execQry = dqcConncetion.execQry("select top 1 id_test from Tests");
            if (execQry == null || execQry.isEmpty()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Lack of access to the Tests table or the table is empty");
            }
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connection with DQC DB! Error: " + e.getMessage());
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, PumpConstants.PUMP_TEST_CONNECTION_OK);
    }
}
