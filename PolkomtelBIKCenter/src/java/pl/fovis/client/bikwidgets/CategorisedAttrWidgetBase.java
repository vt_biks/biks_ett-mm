/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.client.dialogs.EditAttrHintDialog;
import pl.fovis.common.AttrHintBean;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public abstract class CategorisedAttrWidgetBase extends CategorizedDictionaryWidgetBase<AttributeBean> {

    protected Map<Integer, Integer> atrIdsCnt = new HashMap<Integer, Integer>();
    protected int selectedIdCnt;
    protected Map<Integer, String> atrIdsCheckboxImage = new HashMap<Integer, String>();
    protected Map<Integer, PushButton> atrIdsCheckboxBtns = new HashMap<Integer, PushButton>();
    protected Map<Integer, PushButton> categoryCheckboxBtns = new HashMap<Integer, PushButton>();
    protected Map<Integer, String> categoryCheckboxImage = new HashMap<Integer, String>();
    protected Map<Integer, Integer> atrIdCntInCategory = new HashMap<Integer, Integer>();
    protected Set<Integer> selectedNodeKindIds;

    @Override
    protected int getCategoryIdForItem(AttributeBean item) {
        return item.categoryId;
    }

    protected String getCategoryNameForItem(AttributeBean item) {
        return item.atrCategory;
    }

    protected String getItemNameForItem(AttributeBean item) {
        return item.atrName;
    }

    //ww: nie tłumaczymy atrybutów dodatkowych
//    protected String getTranslatedItemCaptionById(int itemId) {
//        return getTranslatedItemCaption(getItemNameForItemId(itemId));
//    }
    @Override
    public PushButton createOptEditHintAttrButton(final String name) {

        PushButton editButton = new PushButton(new Image("images/hint.gif" /* I18N: no */), new Image("images/hint.gif" /* I18N: no */));
        editButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                new EditAttrHintDialog().buildAndShowDialog(name, BIKClientSingletons.getAttrHint(name), new IParametrizedContinuation<AttrHintBean>() {
                    public void doIt(final AttrHintBean param) {
                        param.hint = BIKClientSingletons.fixAttrHintValue(param.hint);

                        BIKClientSingletons.getService().updateAttrHint(param.name, param.hint, getAttributeType(), new StandardAsyncCallback<Void>() {
                            public void onSuccess(Void result) {
                                BIKClientSingletons.showInfo(I18n.edytowanoOpis.get() /* I18N:  */ + ".");
                            }
                        });
                        BIKClientSingletons.updateAttrHint(param.name, param.hint);
                    }
                });
            }
        });
        editButton.setStyleName("Attr" /* I18N: no */ + "-editBtn");
        editButton.setTitle(I18n.edytuj.get() /* I18N:  */);
        return editButton;
    }

    protected abstract int getAttributeType();

    protected void setCategoryCheckboxes(Map<Integer, Boolean> atrIdInCategoryCheck2) {
        String checkImg;
        checkImg = "0";
        for (Map.Entry<Integer, PushButton> btn : categoryCheckboxBtns.entrySet()) {
            if (!atrIdCntInCategory.containsKey(btn.getKey())) {
                btn.getValue().getUpFace().setImage(new Image("images/checkbox" /* I18N: no */ + checkImg + "." + "gif" /* I18N: no */));
            }
        }
        for (Map.Entry<Integer, Integer> ai : atrIdCntInCategory.entrySet()) {
            checkImg = ai.getValue() < itemsInCategory.get(ai.getKey()).size() || (atrIdInCategoryCheck2.get(ai.getKey()) != null) ? "2" : "1";
            categoryCheckboxBtns.get(ai.getKey()).getUpFace().setImage(new Image("images/checkbox" /* I18N: no */ + checkImg + "." + "gif" /* I18N: no */));
            categoryCheckboxImage.put(ai.getKey(), checkImg);
        }
    }
}
