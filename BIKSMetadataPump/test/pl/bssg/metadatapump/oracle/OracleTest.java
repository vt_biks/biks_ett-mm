/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.oracle;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import static pl.bssg.metadatapump.PumpUtils.getConnection;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.serverlogic.db.OracleConnection;
import pl.trzy0.foxy.serverlogic.db.OracleConnectionConfig;

/**
 *
 * @author tflorczak
 */
public class OracleTest {

    public static void main(String[] args) throws SQLException {
//        testViaDriverManager();
        testViaDS();
    }

    protected static void testViaDS() {
        IBeanConnectionEx<Object> mc = new OracleConnection(new OracleConnectionConfig("192.168.56.101", "orcl", "system", "oracle", 1521));
//        List<Map<String, Object>> results = mc.execQry("select * from table(test_person_string_trim())");
        List<Map<String, Object>> results = mc.execQry("select * from HR.test_tomek");
        System.out.println("results.size: " + results.size());
    }

    protected static void testViaDriverManager() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection("oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@//192.168.56.101:1521/orcl", "system", "oracle");
            Statement stat = conn.createStatement();
            ResultSet results = stat.executeQuery("select * from table(test_person_string_trim())");
//            ResultSet results = stat.executeQuery("select * from all_tables where rownum = 1");
            int columnCount = results.getMetaData().getColumnCount();
            while (results.next()) {
                int cc = results.getInt("case_count");
                int ec = results.getInt("error_count");
                System.out.println("test - columnCount = " + columnCount + ", cc = " + cc + ", ec = " + ec);
            }
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());

        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
}
