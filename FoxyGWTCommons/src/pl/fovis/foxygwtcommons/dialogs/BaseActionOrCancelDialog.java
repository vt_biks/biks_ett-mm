/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.Stack;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.DialogCaption;
import pl.fovis.foxygwtcommons.DialogCaptionWithCloseButton;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.IFoxyNotificationHandler;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StatusBarNotificationAnimator;
import pl.fovis.foxygwtcommons.VisualMetricsUtils;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IPredicate;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public abstract class BaseActionOrCancelDialog implements IHasInputControls {

    public static final String DIAG_MSG_KIND$DIALOG_SIZING = "DialogSizing";

    public static boolean USE_ON_DIALOG_NOTIFICATIONS = false;
//    private static final Logger logger = Logger.getLogger("BAOCD");
    public static final String ZINDEX_OVER_GLASS = "800087";
    public static final String ZINDEX_UNDER_GLASS = "800085";
    protected IFoxyNotificationHandler notificationHandler;
    protected DialogCaption mdc;
    private static Stack<BaseActionOrCancelDialog> dialogStack = new Stack<BaseActionOrCancelDialog>();
    protected MyDialogBox dialog;
    protected VerticalPanel main;
//    protected Button cancelBtn;
    protected PushButton cancelBtn;
//    protected Button actionBtn;
    protected PushButton actionBtn;

    public class MyDialogBox extends DialogBox {

        public MyDialogBox(boolean autoHide, boolean modal, DialogBox.Caption caption) {
            super(autoHide, modal, caption);
        }

        @Override
        protected void onPreviewNativeEvent(Event.NativePreviewEvent event) {
            if (event.getTypeInt() == Event.ONKEYUP) {
                NativeEvent ne = event.getNativeEvent();
                if (ne.getKeyCode() == KeyCodes.KEY_ESCAPE) {
                    if (isKeyEscapeCloseWindow()) {
                        event.cancel();
                        cancelClicked();
                    }
                } else if (ne.getKeyCode() == KeyCodes.KEY_ENTER) {
                    event.cancel();
                    onEnterPressed();
                }
            }
            super.onPreviewNativeEvent(event);
        }
    }

    public static boolean isShowDiagDialogSizingEnabled() {
        return ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$DIALOG_SIZING);
    }

    public static void showDiagDialogSizingMsgS(String msg) {

        BaseActionOrCancelDialog baocd = dialogStack.isEmpty() ? null : dialogStack.peek();

        String msgSuffix = baocd == null ? " (no dialog)" : baocd.getDiagDialogSizingMsgSuffix();

        ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$DIALOG_SIZING, msg + msgSuffix);
    }

    protected String getDiagDialogSizingMsgSuffix() {
        return ", main.height=" + VisualMetricsUtils.getHeight(main);
    }

    protected void showDiagDialogSizingMsg(String msg) {
        showDiagDialogSizingMsgS(msg);
    }

//    private static void log(String msg) {
//        logger.log(Level.SEVERE, msg);
//    }
    protected void addAdditionalButtons(HorizontalPanel hp) {
        // no-op
    }

    protected void setBottomWidgets(HorizontalPanel hp) {
        main.add(hp);
    }

    protected HorizontalPanel buildAndAddBottomWidgets() {
        HorizontalPanel hp = new HorizontalPanel();
        hp.setWidth("100%");
        //ww: wystarczy 25px, jednak... dobrze jak jest ciasno, ale bez przegięć,
        // bo trzeba będzie używać taśmy jak do chomika ;-)
        hp.setHeight("30px"); //ww: jednak 30px wymagane, żeby guziki miały miejsce na skakanie
        hp.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        //DOM.setStyleAttribute(hp.getElement(), "margin-top", "4px");
        // GWTUtils.setStyleAttribute(hp, "margin", "1px");
        hp.setStyleName("BaseActionOrCancelDialogFooterButtons");
        if (actionBtn != null) {
            hp.add(actionBtn);
            hp.setCellHorizontalAlignment(actionBtn, HasHorizontalAlignment.ALIGN_LEFT);
            hp.setCellWidth(actionBtn, "20%");
            actionBtn.addStyleName("action");
        }
        addAdditionalButtons(hp);
        cancelBtn.addStyleName("cancel");
        hp.add(cancelBtn);
        hp.setCellWidth(cancelBtn, "20%");
        hp.setCellHorizontalAlignment(cancelBtn, HasHorizontalAlignment.ALIGN_RIGHT);
//        hp.setCellVerticalAlignment(cancelBtn, HasVerticalAlignment.ALIGN_MIDDLE);
        setBottomWidgets(hp);
        hp.setSpacing(0);
        return hp;
    }

    protected abstract String getDialogCaption();

    protected abstract String getActionButtonCaption();

    protected abstract void buildMainWidgets();

    protected abstract void doAction();

    protected String getCancelButtonCaption() {
        return I18n.zamknij.get() /*I18N:  *//*"Anuluj"*/;
    }

    protected void doCancel() {
        // no-op
    }

    protected String getActionButtonCaptionIcon() {
        return null;
    }

    protected boolean doActionBeforeClose() {
        return true;
    }

    protected boolean doCancelBeforeClose() {
        return true;
    }
    private boolean alreadyHidden;

    public void hideDialog() {
        if (notificationHandler != null) {
            FoxyClientSingletons.removeNotificationHandler(notificationHandler);
        }
//        log("hideDialog starts");
        dialog.hide();

//        log("hideDialog: after dialog.hide");
        BaseActionOrCancelDialog onTop = dialogStack.isEmpty() ? null : dialogStack.peek();

        dialogStack.remove(this);

//        if (onTop != this || onTop == null || alreadyHidden) {
////            Window.alert("something wrong in dialogs: hideDialogs. onTop:" + onTop + ","
////                    + "this == onTop?: " + (this == onTop) + ", alreadyHidden: " + alreadyHidden);
//        }
        if (!dialogStack.empty() && (!alreadyHidden) && (onTop == this)) {
            BaseActionOrCancelDialog baocd = dialogStack.peek();
            GWTUtils.setStyleAttribute(baocd.dialog, "zIndex", ZINDEX_OVER_GLASS);
        }

        alreadyHidden = true;

//        log("hideDialog: ends");
    }

    protected void cancelClicked() {
        if (doCancelBeforeClose()) {
            hideDialog();
            doCancel();
        }
    }

    protected void onEnterPressed() {
        actionClicked();
    }

    protected void actionClicked() {
        if (doActionBeforeClose()) {
            hideDialog();
            doAction();
        }
    }

    protected Pair<Label, Label> getNotificationLabels() {
        return mdc.initNotificationLabels();
    }

    protected IFoxyNotificationHandler getNotificationHandler() {
        if (!USE_ON_DIALOG_NOTIFICATIONS) {
            return null;
        }
        Pair<Label, Label> labels = getNotificationLabels();
        if (labels == null) {
            return FoxyClientSingletons.notificationHandlerByDialogs;
        }

        return new StatusBarNotificationAnimator(labels.v1, labels.v2);
    }

    protected DialogCaption getDialogCaptionElem(HorizontalPanel bottomBtns) {
        return new DialogCaptionWithCloseButton(getDialogCaption(),
                new IContinuation() {
                    public void doIt() {
                        cancelClicked();
                    }
                });
    }

    public void buildAndShowDialog() {
//        log("buildAndShowDialog: starts");

        if (!dialogStack.isEmpty()) {
            BaseActionOrCancelDialog baocd = dialogStack.peek();
            if (baocd.dialog != null) {
                GWTUtils.setStyleAttribute(baocd.dialog, "zIndex", ZINDEX_UNDER_GLASS);
            }
        }

        dialogStack.push(this);

        main = new VerticalPanel();
        main.setWidth("100%");
        main.setStyleName("BaseActionOrCancelDialogMain");

        String actionButtonCaption = getActionButtonCaption();
        if (actionButtonCaption != null) {
            // actionBtn = new Button(actionButtonCaption);
            actionBtn = new PushButton(actionButtonCaption);
            actionBtn.setHTML(createHTMLForActionButtonCaptionIcon() + actionBtn.getHTML());
            NewLookUtils.makeCustomPushButton(actionBtn);
            // GWTUtils.setStyleAttribute(actionBtn, "float", "left");

            actionBtn.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    actionClicked();
                }
            });
        }

//        cancelBtn = new Button(getCancelButtonCaption());
        cancelBtn = new PushButton(getCancelButtonCaption());
        NewLookUtils.makeCustomPushButton(cancelBtn);
//        GWTUtils.setStyleAttribute(cancelBtn, "float", "right");
//        DOM.setStyleAttribute(cancelBtn.getElement(), "float", "right");
//        DOM.setElementAttribute(cancelBtn.getElement(), "style", "float: right;");

        cancelBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                cancelClicked();
            }
        });

//        log("buildAndShowDialog: before buildMainWidgets");
        buildMainWidgets();
//        log("buildAndShowDialog: after buildMainWidgets");
        HorizontalPanel hp = buildAndAddBottomWidgets();

        main.setSpacing(0);
        GWTUtils.setElementPropertyInt(main.getElement(), "cellPadding", 2);

        mdc = getDialogCaptionElem(hp);

        notificationHandler = getNotificationHandler();
        if (notificationHandler != null) {
            FoxyClientSingletons.addNotificationHandler(notificationHandler);
        }

        dialog = new MyDialogBox(false, true, mdc);
        //dialog.setText(getDialogCaption());
        dialog.add(main);

//        log("buildAndShowDialog: before afterBuildAllWidgetsBeforeShow");
        afterBuildAllWidgetsBeforeShow();

//        log("buildAndShowDialog: after afterBuildAllWidgetsBeforeShow");
        firstShowCentered();
//        log("buildAndShowDialog: ends");

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("buildAndShowDialog: after firstShowCentered, all done.");
        }
    }

    //ww: zrobione po to, by nadpisywać w miarę potrzeb - tu już wszystkie
    // widgety są stworzone, można coś ukryć schować itp. odwoływać się
    // do widgetów
    protected void afterBuildAllWidgetsBeforeShow() {
        // no-op
    }

    protected boolean canWidgetBeInitiallyFocused(Widget w) {
        return !(w instanceof PushButton);
    }

    protected void tryFocusFirstWidget() {
        GWTUtils.tryFocusFirstWidget(dialog, new IPredicate<Widget>() {
            public Boolean project(Widget val) {
                return canWidgetBeInitiallyFocused(val);
            }
        });
    }

    protected void firstShowCentered() {
//        log("firstShowCentered starts");
        if (dialog.isShowing()) {
//            Window.alert("illegal call to firstShowCentered - "
//                    + "dialog is already showing");
//            log("firstShowCentered ALREADY SHOWING!");
        }
//        dialog.setGlassEnabled(false);
        dialog.setGlassEnabled(dialogStack.size() < 2);  //ww: było tak: dialog.setGlassEnabled(true);
//        log("firstShowCentered after set glass enabled!");
//        dialog.setAnimationEnabled(false);
        dialog.setAnimationEnabled(true);
//        log("firstShowCentered after set animation enabled (true)!");
        dialog.center();
//        log("firstShowCentered after dialog.center!");
        //dialog.show();
        tryFocusFirstWidget();
//        log("firstShowCentered ends");
    }

    protected void recenterDialog() {
//        log("recenterDialog starts");
        if (!dialog.isShowing()) {
            /* W jednym przypadku ten komunikat jest niesłuszny - gdy dokonuje się inicjalnego wywołania selectTab() dla EntityDetailsPaneForDialog
             Window.alert("illegal call to recenterDialog - "
             + "before buildAndShowDialog has finished");
             */
//            log("recenterDialog ALREADY SHOWING!");
            return;
        }
//        log("recenterDialog before dialog.center();");
        dialog.center();
//        log("recenterDialog ends");
    }

    protected Pair<Integer, Integer> getSizeForScrollPanel() {
        int clientWidth = Window.getClientWidth();
        int clientHeight = Window.getClientHeight();
        Pair<Integer, Integer> res = new Pair<Integer, Integer>((int) (clientWidth * 0.97),
                (int) (clientHeight * 0.92));

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("getSizeForScrollPanel: clientSizes(w,h)=(" + clientWidth + ", " + clientHeight + "); sizes for sp=(" + res.v1 + ", " + res.v2 + ")");
        }

        return res;
    }

    protected void setScrollPanelSize(Widget scPanel, int height) {

        Pair<Integer, Integer> sizes = getSizeForScrollPanel();
        final int decreasedHeight = sizes.v2 - height;

//        scPanel.setSize(Window.getClientWidth() * 0.97 + "px", Window.getClientHeight() * 0.92 - height + "px");
        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("setScrollPanelSize: height decrement=" + height + ", final height=" + decreasedHeight);
        }
        scPanel.setSize(sizes.v1 + "px", decreasedHeight + "px");
    }

    public static boolean isShowingDialog() {
        return !dialogStack.isEmpty();
    }

    public static void hideAllDialogs() {
//        log("hideAllDialogs starts");

        while (!dialogStack.isEmpty()) {
            BaseActionOrCancelDialog dialog = dialogStack.peek();
            dialog.hideDialog();
        }
//        log("hideAllDialogs ends");
    }

    protected boolean isKeyEscapeCloseWindow() {
        return true;
    }

    protected String createHTMLForActionButtonCaptionIcon() {
        //tutaj na sztywno ustawiony jest styl obrazka, możliwe, że będzie trzeba go uogólnić
        String iconName = getActionButtonCaptionIcon();
        return !BaseUtils.isStrEmptyOrWhiteSpace(iconName)
                ? ("<" + "img  style=\"margin: 3px 4px 0px 0px; border: 0px none; float: left;\" src=\"images" /* I18N: no */ + "/" + iconName + "\"/>")
                : "";
    }

    public void updateDialogCaption() {
        mdc.setCaptionHtml(getDialogCaption());
    }
}
