/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.common;

/**
 *
 * @author pmielanczuk
 */
public class UnauthorizedServiceMethodAccessException extends BIKSAdmRuntimeException {

    public UnauthorizedServiceMethodAccessException() {
        //ww: do celów serializacji
    }

    public UnauthorizedServiceMethodAccessException(String msg) {
        super(msg);
    }
}
