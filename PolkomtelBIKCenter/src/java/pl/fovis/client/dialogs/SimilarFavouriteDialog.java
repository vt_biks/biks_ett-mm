/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.ObjectInFvsChangeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class SimilarFavouriteDialog extends BaseActionOrCancelDialog {

    protected int objId;
    protected String nodeName;
    protected String nodeKindCode;
    protected String treeCode;
    protected List<ObjectInFvsChangeBean> favouriteNodesWithSimilarityScore;
    protected IParametrizedContinuation<List<Integer>> saveCont;
    protected Map<CheckBox, Integer> checkboxes;

    @Override
    protected String getDialogCaption() {
        return I18n.dodajDoUlubionych.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.dodaj.get();
    }

    @Override
    protected void buildMainWidgets() {

        HorizontalPanel hp = new HorizontalPanel();
        CheckBox cb = new CheckBox("");
        cb.setValue(true);
        cb.setEnabled(false);
        hp.add(cb);
        
        String iconName = "images/" + BIKClientSingletons.getNodeKindIconNameByCode(nodeKindCode, treeCode) + ".gif";
        hp.add(BIKClientSingletons.createIconImg(iconName, "16px", "16px", BIKClientSingletons.getNodeKindCaptionByCode(nodeKindCode)));
        HTML nameLbl = new HTML("<b>" + nodeName + "</b>");
        nameLbl.setStyleName("BizDef");

        hp.add(nameLbl);
        VerticalPanel vp = new VerticalPanel();
        vp.setStyleName("similarFvs");
        vp.add(new Label(I18n.dodajeszDoUlubionych.get()));

        vp.add(hp);
        main.add(vp);
        main.add(new HTML("<b>" + I18n.istniejaElementyPodobne.get() + "</b>"));
        main.add(similarFvs());
    }

    protected FlexTable similarFvs() {
        int row = 0;
        checkboxes = new HashMap<CheckBox, Integer>();
        FlexTable ft = new FlexTable();
        ft.setStyleName("gridJoinedObj");
        ft.setWidth("100%");
        for (ObjectInFvsChangeBean fvs : favouriteNodesWithSimilarityScore) {

            CheckBox cb = new CheckBox("");
            String iconName = "images/" + BIKClientSingletons.getNodeKindIconNameByCode(fvs.code, fvs.treeCode) + ".gif";
            String optSimilarityScore = " [≈" + BaseUtils.doubleToString(fvs.similarityScore, 4, false, true) + "]";

            ft.setWidget(row, 0, cb);
            ft.setWidget(row, 1, BIKClientSingletons.createIconImg(iconName, "16px", "16px", BIKClientSingletons.getNodeKindCaptionByCode(fvs.code)));
            Label nameLbl = new Label(fvs.name + " " + optSimilarityScore);
            nameLbl.setTitle(setAncestorPath(fvs));

            ft.setWidget(row, 2, nameLbl);

            ft.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
            checkboxes.put(cb, fvs.nodeId);
            row++;

            ft.getColumnFormatter().setWidth(0, "16");
            ft.getColumnFormatter().setWidth(1, "16px");
            ft.getColumnFormatter().setWidth(2, "100%");

        }
        return ft;
    }

    public void buildAndShowDialog(int objId, String nodeName, String nodeKindCode, String treeCode,
            List<ObjectInFvsChangeBean> favouriteNodesWithSimilarityScore, IParametrizedContinuation<List<Integer>> saveCont) {
        this.saveCont = saveCont;
        this.objId = objId;
        this.nodeName = nodeName;
        this.nodeKindCode = nodeKindCode;
        this.treeCode = treeCode;
        this.favouriteNodesWithSimilarityScore = favouriteNodesWithSimilarityScore;
        super.buildAndShowDialog();
    }

    @Override
    protected void doAction() {
        List<Integer> nodeIds = new ArrayList<Integer>();
        for (Entry<CheckBox, Integer> cb : checkboxes.entrySet()) {
            if (cb.getKey().getValue()) {
                nodeIds.add(cb.getValue());
            }
        }
        saveCont.doIt(nodeIds);

    }

    protected String setAncestorPath(ObjectInFvsChangeBean object) {
        StringBuilder sb = new StringBuilder();
        sb.append(BIKClientSingletons.getMenuPathForTreeCode(object.treeCode));
        List<String> ancestorNodePath = object.ancestorNodePath;
        sb.append(BaseUtils.mergeWithSepEx(ancestorNodePath, BIKGWTConstants.RAQUO_STR_SPACED));
        return sb.toString();
    }

}
