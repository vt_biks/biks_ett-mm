/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.RandomAccess;
import java.util.Set;

/**
 *
 * @author wezyr
 */
public class BaseUtils {

    public static final String REPORT_TAG_PREFIX = "%{rp_";
    public static final String REPORT_TAG_SUFFIX = "_pr}%";

    private static IRegExpMatcherFactory matcherFactory = new RegExpMatcherFactoryPoison();
    public static final String WORD_SPLIT_REGEXP = "[ \t\n.,\\-\\\\+*!<>@#$%^&\\[\\]\\{\\}?\\:\\;\\~\\`\\=_|\\(\\)/\\\r\"\']+";
    private static IRegExpPattern wordSplitRegexpPatt;
    private static ISimpleRandomGenerator simpleRandom;
    private static IStringComparator comparator;
    public static final Comparator<String> PLAIN_STRING_COMPARATOR = makeComparator();

    public static final HashMap<Character, Character> POLISH_CHAR = new HashMap<Character, Character>();

    static {
        POLISH_CHAR.put('ą', 'a');
        POLISH_CHAR.put('Ą', 'A');
        POLISH_CHAR.put('ć', 'c');
        POLISH_CHAR.put('Ć', 'C');
        POLISH_CHAR.put('ę', 'e');
        POLISH_CHAR.put('Ę', 'E');
        POLISH_CHAR.put('ł', 'l');
        POLISH_CHAR.put('Ł', 'L');
        POLISH_CHAR.put('ń', 'n');
        POLISH_CHAR.put('Ń', 'N');
        POLISH_CHAR.put('ó', 'o');
        POLISH_CHAR.put('Ó', 'O');
        POLISH_CHAR.put('ś', 's');
        POLISH_CHAR.put('Ś', 'S');
        POLISH_CHAR.put('ź', 'z');
        POLISH_CHAR.put('Ź', 'Z');
        POLISH_CHAR.put('ż', 'z');
        POLISH_CHAR.put('Ż', 'Z');
    }

    public static void setComparator(IStringComparator comparator, boolean forceSet) {
        if (!forceSet && BaseUtils.comparator != null) {
            throw new LameRuntimeException("comparator can only be set once");
        }
        BaseUtils.comparator = comparator;
    }

    public static void setStringComparator(IStringComparator comparator) {
        setComparator(comparator, false);
    }

    public static IStringComparator getComparator() {
        return comparator;
    }

    public static IRegExpMatcherFactory getRegExpMatcherFactory() {
        return matcherFactory;
    }

    public static <T> List<T> addToArrayList(List<T> list, T item) {
        if (list == null) {
            list = new ArrayList<T>();
        }
        list.add(item);
        return list;
    }

    public static boolean isMapEmpty(Map<?, ?> map) {
        //if (true) throw new LameRuntimeException("doooopa");
        return map == null || map.isEmpty();
    }

    public static void setSimpleRandomGenerator(ISimpleRandomGenerator simpleRandomGenerator) {
        if (BaseUtils.simpleRandom != null) {
            throw new LameRuntimeException("simpleRandomGenerator can only be set once");
        }
        BaseUtils.simpleRandom = simpleRandomGenerator;
    }

    public static String emptyStrToDef(String val, String defVal) {
        return isStrEmpty(val) ? defVal : val;
    }

    //public static final Pattern wordSplitPatt = Pattern.compile(WORD_SPLIT_REGEXP);
    public static void setRegExpMatcherFactory(IRegExpMatcherFactory f) {
        matcherFactory = f;
    }

    public static String getFirstWords(String str, int wordCount) {
        return getFirstWords(str, wordCount, -1);
    }

    public static synchronized IRegExpPattern getWordSplitRegexpPatt() {
        if (wordSplitRegexpPatt == null) {
            wordSplitRegexpPatt = getRegExpMatcherFactory().compile(WORD_SPLIT_REGEXP, false, false);
        }
        return wordSplitRegexpPatt;
    }

    public static String getFirstWords(String str, int wordCount, int charLimit) {
        if (str == null) {
            return null;
        }

        //Matcher m = wordSplitPatt.matcher(str);
        String s = charLimit >= 0 ? str.substring(0, charLimit) : str;
//        if (charLimit >= 0) {
//            m.region(0, charLimit);
//        }

        IRegExpMatcher m = getWordSplitRegexpPatt().matcher(s);
        // matcherFactory.matcher(WORD_SPLIT_REGEXP, s);

        int i = 0;
        int lastPos = 0;
        int lastPosEnd = -1;
        while (i < wordCount && m.find()) {
            lastPos = m.start();
            lastPosEnd = m.end();
            i++;
        }

        if (i < wordCount) {
            // not found wordCount words...
            // last invoke of matcher.find() == false
            if (lastPosEnd == charLimit) {
                // do nothing -> lastPos is accurate
            } else {
                lastPos = lesserPos(str.length(), charLimit);
            }
        } else {
            lastPos = m.start();
        }

        return str.substring(0, lastPos);
    }

    public static String[] splitWords(String str) {
        return splitWords(str, 0);
    }

    public static String[] splitWords(String str, int firstCount) {
        if (str == null) {
            return null;
        }
        String[] arr = str.split(WORD_SPLIT_REGEXP, firstCount + 1);
        if (firstCount > 0 && arr.length > firstCount) {
            String[] na = new String[firstCount];
            System.arraycopy(arr, 0, na, 0, firstCount);
            arr = na;
        }
        return arr;
    }

    public static int countWords(String str) {
        return arrLengthFix(splitWords(str));
    }

    public static int lastPos(int pos1, int pos2, boolean negativeAlwaysFarther) {
        if (negativeAlwaysFarther) {
            if (pos1 == -1) {
                return pos1;
            }
            if (pos2 == -1) {
                return pos2;
            }
        }

        return pos1 > pos2 ? pos1 : pos2;
    }

    // -1 is always farther (after) than any non-negative pos
    public static int firstPos(int pos1, int pos2) {
        if (pos1 == -1) {
            return pos2;
        }
        if (pos2 == -1) {
            return pos1;
        }
        return Math.min(pos1, pos2);
        // ww: było - źle, brak testów jednostkowych się kłania ww ;-)
        //return pos1 != -1 && pos1 < pos2 ? pos1 : pos2;
    }

    // filename only, no dirs
    public static String dropFileExt(String val) {
        int dotPos = val.lastIndexOf(".");
        if (dotPos != -1) {
            return val.substring(0, dotPos);
        }
        return val;
    }

    // cnt == 1 -> split by just last sep
    // cnt == 2 -> split by second from end sep
    // cnt == 0 -> gives Pair(s, "")
    // gives Pair(null, s) if s has less than cnt occurences of sep
    public static Pair<String, String> splitByLastNthSep(String s, String sep, int cnt) {
        int pos = findNthLastPos(s, sep, cnt);
        String p1 = pos == -1 ? null : s.substring(0, pos);
        String p2 = pos == -1 ? s : s.substring(pos + sep.length());
        return new Pair<String, String>(p1, p2);
    }

    // returns file path with ending "/" or empty string if no path in fullFileName
    // for null returns null
    public static String extractFilePath(String fullFileName) {
        if (fullFileName == null) {
            return null;
        }

        String path = dropLastItems(fullFileName, "/", 1);
        if (!path.isEmpty() || fullFileName.startsWith("/")) {
            path = path + "/";
        }

        return path;
    }

    public static String dropFileExtFromFullPath(String val) {
        Pair<String, String> p = splitByLastNthSep(val, "/", 1);
        return (p.v1 == null ? "" : p.v1 + "/") + dropFileExt(p.v2);
    }

    public static boolean fileNameHasExt(String fileName, String ext,
            boolean extHasDot, boolean ignoreCase) {
        int noDotToOne = extHasDot ? 0 : 1;
        if (fileName.length() < ext.length() + noDotToOne) {
            return false;
        }
        String fileNameSuffix = fileName.substring(fileName.length() - ext.length());
        if (ignoreCase) {
            if (!ext.equalsIgnoreCase(fileNameSuffix)) {
                return false;
            }
        } else {
            if (!ext.equals(fileNameSuffix)) {
                return false;
            }
        }
        if (!extHasDot) {
            if (fileName.charAt(fileName.length() - ext.length() - 1) != '.') {
                return false;
            }
        }
        return true;
    }

    // filename only, no dirs, returns file ext with dot (".xyz")
    // or empty string if no ext
    public static String extractFileExt(String val) {
        return extractFileExt(val, true);
    }

    public static String extractFileExt(String val, boolean includeDot) {
        int dotPos = val.lastIndexOf(".");
        if (dotPos == -1) {
            return "";
        }
        return val.substring(dotPos + (includeDot ? 0 : 1));
    }

    // dir separator == '/'
    public static String dropFilePath(String fileWithPath) {
        return getLastItems(fileWithPath, "/", 1);
    }

    // dir separator == '/'
    public static String extractFileName(String fileWithPath) {
        return dropFilePath(fileWithPath);
    }

    public static String extractFileExtFromFullPath(String val) {
        return extractFileExtFromFullPath(val, true);
    }

    public static String extractFileExtFromFullPath(String val, boolean includeDot) {
        String fileName = dropFilePath(val);
        return extractFileExt(fileName, includeDot);
    }

    // warning: works shallowly! top level of map only!
    // i.e. map values of kind INamedPropsBean are not converted to maps!
    public static Map<String, Object> namedPropsBeanToMap(INamedPropsBean bean) {
        return namedPropsBeanToMap(bean, false);
    }

    public static Map<String, Object> namedPropsBeanToMap(INamedPropsBean bean, boolean deep) {
        if (bean == null) {
            return null;
        }

        Collection<String> propNames = bean.getPropNames();
        Map<String, Object> res = new LinkedHashMap<String, Object>(calcHashMapCapacity(propNames.size()));
        for (String propName : propNames) {
            Object value = bean.getPropValue(propName);
            if (deep && value instanceof INamedPropsBean) {
                INamedPropsBean innerBean = (INamedPropsBean) value;
                value = namedPropsBeanToMap(innerBean, true);
            }
            res.put(propName, value);
        }

        return res;
    }

    private static int lesserPos(int p1, int p2) {
        return p1 != -1 && p1 < p2 || p2 == -1 ? p1 : p2;
    }

    public static String echo(String s) {
        return "-- [crazy?[ --" + s + "-- ]dupa] -- (from hell!)";
    }

    public static String createPlaceHolder(int id, String openStr, String closeStr) {
        return openStr + BaseUtils.safeToString(id) + closeStr;
    }

    public static String replacePlaceHoldersWithValues(String expr, String openStr, String closeStr, Map<Integer, String> placeHolderValues) {
        for (Integer id : placeHolderValues.keySet()) {
            String value = placeHolderValues.get(id);
            expr = expr.replace(createPlaceHolder(id, openStr, closeStr), value);
        }
        return expr;
    }

    public static <T> List<T> reverseList(List<T> list) {
        List<T> ret = new ArrayList<T>();
        for (int i = list.size() - 1; i >= 0; i--) {
            ret.add(list.get(i));
        }
        return ret;
    }

    public static String ensureObjIdEndWith(String objId, String seperator) {
        if (!BaseUtils.isStrEmptyOrWhiteSpace(objId) && !objId.endsWith(seperator)) {
            objId += seperator;
        };
        return objId;
    }

    public static String changePolishChar2Latin(String value) {
        if (isStrEmptyOrWhiteSpace(value)) {
            return "";
        }
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < value.length(); i++) {
            Character c = value.charAt(i);
            if (POLISH_CHAR.containsKey(c)) {
                c = POLISH_CHAR.get(c);
            }
            s.append(c);
        }
        return s.toString();
    }

    public static Set<String> extractReportTags(String txt) {
        Set<String> res = new HashSet<String>();
        int lastPos = 0;
        do {
            int i = txt.indexOf(REPORT_TAG_PREFIX, lastPos);
            if (i >= 0) {
                int j = txt.indexOf(REPORT_TAG_SUFFIX, i + REPORT_TAG_PREFIX.length());
                if (j >= 0) {
                    String report = txt.substring(i + REPORT_TAG_PREFIX.length(), j);
                    res.add(report);
                }
                lastPos = i + REPORT_TAG_PREFIX.length();
            } else {
                lastPos = -1;
            }
        } while (lastPos >= 0 && lastPos < txt.length());
        return res;
    }

    public static interface Projector<V, T> {

        T project(V val);
    }

    public static class MapProjector<K, V> implements Projector<Map<K, V>, V> {

        private K key;

        public MapProjector(K key) {
            this.key = key;
        }

        public V project(Map<K, V> val) {
//            return ((Map<K, V>) val).get(key);
            return val.get(key);
        }
    }

    public static boolean safeEqualsAny(Object o, Object... os) {
        for (Object o2 : os) {
            if (safeEquals(o, o2)) {
                return true;
            }
        }

        return false;
    }

    public static boolean safeEqualsStr(String o1, String o2, boolean ignoreCase) {
        if (o1 == o2) {
            return true;
        }
        if (o1 == null || o2 == null) {
            return false;
        }
        return ignoreCase ? o1.equalsIgnoreCase(o2) : o1.equals(o2);
    }

    public static boolean safeEquals(Object o1, Object o2) {
        if (o1 == o2) {
            return true;
        }
        if (o1 == null || o2 == null) {
            return false;
        }
        return o1.equals(o2);
    }

    public static boolean isWhiteSpaceChar(char c) {
        return (c == ' ')
                || (c == '\r')
                || (c == '\n')
                || (c == '\t');
    }

    public static boolean isHTMLWhiteSpaceChar(char c) {
        return isWhiteSpaceChar(c);
//        return (c == ' ')
//                || (c == '\n')
//                || (c == '\t');
    }

    public static boolean isHTMLSeparatorCandidateChar(char c) {
        return (c == '?')
                || (c == '&')
                || (c == '=')
                || (c == '+')
                || (c == '-')
                || (c == '/')
                || (c == ':')
                || (c == '!')
                || (c == '.')
                || (c == ',')
                || (c == '_');
    }

    //ww: s == null jest konwertowane na pusty string
    public static String encodeForHTMLTag(final String s, final boolean escapeBackSlash, int wordBreakAfter) {
        final StringBuilder result = new StringBuilder();

        if (s != null) {
            //final StringCharacterIterator iterator = new StringCharacterIterator(s);
            //char character = iterator.current();
            int wordLen = 0;
            for (int idx = 0; idx < s.length(); idx++) {
                //while (character != CharacterIterator.DONE) {
                char character = s.charAt(idx);
                if (character == '<') {
                    result.append("&lt;");
                } else if (character == '>') {
                    result.append("&gt;");
                } else if (character == '\"') {
                    result.append("&quot;");
                } else if (character == '\'') {
                    result.append("&#039;");
                } else if (escapeBackSlash && character == '\\') {
                    result.append("&#092;");
                } else if (character == '&') {
                    result.append("&amp;");
                } else {
                    // Char ::= #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]
                    if (character == 9 || character == 0xA || character == 0xD
                            || character >= 0x20 && character <= 0xD7FF
                            || character >= 0xE000 && character <= 0xFFFD
                            || character >= 0x10000 && character <= 0x10FFFF) {
                        result.append(character);
                    } else {
                        result.append("&#").append((int) '?').append(";");
                    }
                }

                if (wordBreakAfter > 0) {
                    if (isHTMLWhiteSpaceChar(character)) {
                        wordLen = 0;
                    } else {
                        wordLen++;
                        if (wordLen > wordBreakAfter
                                || isHTMLSeparatorCandidateChar(character) && wordLen > wordBreakAfter / 2) {
                            result.append("<wbr />");
                            wordLen = 0;
                        }
                    }
                }

                //character = iterator.next();
            }
        }

        return result.toString();
    }

    public static String encodeForHTMLTag(final String s, final boolean escapeBackSlash) {
        return encodeForHTMLTag(s, escapeBackSlash, -1);
    }

    public static String encodeForHTMLTag(final String s) {
        return encodeForHTMLTag(s, true);
    }

    public static String encodeForHTMLWithNewLinesAsBRs(final String s) {
        String encoded = encodeForHTMLTag(s);

        return replaceNewLinesToRowSep(encoded, "<br />");
    }

    // returns number of items successfully processed (for null and empty string returns -1)
    // when processor returns false -> processing is stopped, item is considered as unsuccessfully
    // processed
    public static int processItemsInStr(String str, String sep,
            IParametrizedContinuationWithReturn<String, Boolean> processor,
            Integer allItemsProcessedVal) {
        if (str == null || str.length() == 0) {
            return -1;
        }

        int pos = 0;

        int cnt = 0;

        while (pos <= str.length()) {
            int newPos = str.indexOf(sep, pos);
            if (newPos == -1) {
                newPos = str.length();
            }
            String item = str.substring(pos, newPos);
            Boolean doContinue = processor.doIt(item);
            if (!doContinue) {
                return cnt;
            }
            pos = newPos + sep.length();
            cnt++;
        }

        return allItemsProcessedVal == null ? cnt : allItemsProcessedVal;
    }

    public static <T extends Collection<String>> T splitBySepIntoColl(String str, String sep, T coll) {
        return splitBySepIntoColl(str, sep, coll, false);
    }

    public static <T extends ILameCollector<String>> T splitBySepIntoCollector(String str, String sep,
            T collector, boolean doTrim) {
        if (str == null || str.isEmpty() || doTrim && str.trim().isEmpty()) {
            return collector;
        }

        int pos = 0;

        while (pos <= str.length()) {
            int newPos = str.indexOf(sep, pos);
            if (newPos == -1) {
                newPos = str.length();
            }
            String subStr = str.substring(pos, newPos);
            if (doTrim) {
                subStr = subStr.trim();
            }
            collector.add(subStr);
            pos = newPos + sep.length();
        }

        return collector;
    }

    @SuppressWarnings("unchecked")
    public static <T extends Collection<String>> T splitBySepIntoColl(String str, String sep, T coll, boolean doTrim) {
        return (T) splitBySepIntoCollector(
                str, sep,
                new SimpleCollectionCollector<String>(coll), doTrim).getCollection();

//        if (str == null || str.length() == 0) {
//            return coll;
//        }
//
//        int pos = 0;
//
//        while (pos <= str.length()) {
//            int newPos = str.indexOf(sep, pos);
//            if (newPos == -1) {
//                newPos = str.length();
//            }
//            String subStr = str.substring(pos, newPos);
//            if (doTrim) {
//                subStr = subStr.trim();
//            }
//            coll.add(subStr);
//            pos = newPos + sep.length();
//        }
//
//        return coll;
    }

    public static <T> T getLastItem(List<T> list) {
        return getLastItem(list, true);
    }

    public static <T> T getLastItem(List<T> list, boolean throwForNone) {
        if (list == null) {
            if (throwForNone) {
                throw new LameRuntimeException("list is null");
            } else {
                return null;
            }
        }
        int size = list.size();
        if (size == 0) {
            if (throwForNone) {
                throw new LameRuntimeException("list is empty");
            } else {
                return null;
            }
        }
        return list.get(size - 1);
    }

    public static <K, V> void putAllByKeys(Map<K, V> mDest, Map<K, V> mSrc, Collection<K> c) {
        for (K key : c) {
            mDest.put(key, mSrc.get(key));
        }
    }

    public static <T> String mergeWithSepEx(Iterable<T> elems, String sep) {
        if (elems == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        mergeWithSepIntoSb(elems, sb, sep);

        return sb.toString();
    }

    public static <T> void mergeWithSepIntoSb(Iterable<T> elems, StringBuilder sb, String sep) {
        boolean isSubseqElem = false;

        for (T elem : elems) {
            if (isSubseqElem) {
                sb.append(sep);
            } else {
                isSubseqElem = true;
            }

            String s = elem.toString();

            sb.append(s);
        }
    }

    public static String dropOptionalPrefix(String str, String prefix) {
        return str != null && prefix != null && str.startsWith(prefix) ? str.substring(prefix.length()) : str;
    }

    public static <T, V> String mergeWithSepEx(Iterable<T> elems, String sep, Projector<T, V> proj) {
        return mergeWithSepEx(elems, sep, proj, false);
    }

    public static <T, V> String mergeWithSepEx(Iterable<T> elems, String sep, Projector<T, V> proj, boolean suppressNulls) {
        if (elems == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        boolean first = true;

        for (T elem : elems) {
            V val = proj.project(elem);
            if (suppressNulls && val == null) {
                continue;
            }
            if (first) {
                first = false;
            } else {
                sb.append(sep);
            }
            sb.append(val);
        }

        return sb.toString();
    }

    public static <T> String mergeWithSepInts(Iterable<Integer> elems, String sep) {
        return mergeWithSepEx(elems, sep);
    }

    public static class IdentityProjector<V> implements Projector<V, V> {

        public V project(V val) {
            return val;
        }
    }
    public static final IdentityProjector<Object> identityProjector = new IdentityProjector<Object>();

    public static <V> String mergeWithSep(V[] a, String sep) {
        return mergeWithSep(a, identityProjector, sep);
    }

    public static String mergeNotEmptyParamsWithSep(String sep, Object... params) {
        //System.out.println("****mergeNotEmptyParamsWithSep****");
        if (params == null || params.length == 0) {
            return "";
        }

        List<String> strs = new ArrayList<String>();
        for (Object param : params) {
            String s = safeToString(param);
            if (!isStrEmpty(s)) {
                strs.add(s);
            }
        }
        return mergeWithSepEx(strs, sep);
    }

    public static <V, T> String mergeWithSep(V[] a, Projector<V, T> projector, String sep) {
        if (a == null || a.length == 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder(safeToStringNotNull(projector.project(a[0])));
        for (int i = 1; i < a.length; i++) {
            sb.append(sep).append(safeToStringNotNull(projector.project(a[i])));
        }

        return sb.toString();
    }

    public static String safeToStringDef(Object o, String defForNull) {
        if (o == null) {
            return defForNull;
        }
        return o.toString();
    }

    public static String safeToStringTxt(Object o) {
//        return o == null ? "<null>" : o.toString();
        return safeToStringDef(o, "<null>");
    }

    public static String safeToStringNotNull(Object o) {
//        if (o == null) {
//            return "";
//        }
//        return o.toString();
        return safeToStringDef(o, "");
    }

    public static String safeToString(Object o) {
        return safeToStringDef(o, null);
//        if (o == null) {
//            return null;
//        }
//        return o.toString();
    }

    public static List<String> splitBySep(String str, String sep) {
        return splitBySep(str, sep, false);
    }

    public static List<String> splitBySep(String str, String sep, boolean doTrim) {
        return splitBySepIntoColl(str, sep, new ArrayList<String>(), doTrim);
    }

    //ww: odpowiednik sqlowego coalesce
    public static <T> T coalesce(T... vals) {
        for (T val : vals) {
            if (val != null) {
                return val;
            }
        }
        return null;
    }

    public static <T> T nullToDef(T obj, T val) {
        return obj == null ? val : obj;
    }

    public static Pair<String, String> splitString(String s, String sep) {
        if (s == null) {
            return new Pair<String, String>(null, null);
        }
        int index = s.indexOf(sep);
        if (index == -1) {
            return new Pair<String, String>(s, null);
        } else {
            return new Pair<String, String>(s.substring(0, index), s.substring(index + sep.length(), s.length()));
        }
    }

    public static String capitalize(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static String camelize(String s) {
        String[] strs = s.split("[-]");
        StringBuilder sb = new StringBuilder();
        for (String s2 : strs) {
            sb.append(capitalize(s2));
        }
        return sb.toString();
    }

    public static int strLengthFix(String str) {
        return str == null ? 0 : str.length();
    }

    public static String compactWhiteSpaces(String str) {
        return str == null ? null : str.replaceAll("[ \n\t\r]+", " ");
    }

    public static String trimAndCompactSpaces(String str) {
        return str.trim().replaceAll("[ ]+", " ");
    }
    private static boolean[] whiteSpaceEquivalents;

    static {
        whiteSpaceEquivalents = new boolean[128];
        for (int i = 0; i <= 32; i++) {
            whiteSpaceEquivalents[i] = true;
        }
        String torpl = ".,;-=+*&@!#()^$?:/\\[]{}<>|~`'\"";
        for (int i = 0; i < torpl.length(); i++) {
            whiteSpaceEquivalents[torpl.charAt(i)] = true;
        }
    }

    public static String separatorsToSpace(String str) {
        char[] chrs = new char[str.length()];

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            chrs[i] = (c < whiteSpaceEquivalents.length && whiteSpaceEquivalents[c]) ? ' ' : c;
        }

        return new String(chrs);
    }

    public static String trimLeft(String s) {
        if (s == null) {
            return null;
        }

        int i;
        for (i = 0; i < s.length() && s.charAt(i) <= ' '; i++) {
            // empty
        }

        if (i == 0) {
            return s;
        }
        return s.substring(i);
    }

    public static String trimRight(String s) {
        if (s == null) {
            return null;
        }
        int i;
        for (i = s.length() - 1; i >= 0 && s.charAt(i) <= ' '; i--) {
            // empty
        }

        if (i == s.length() - 1) {
            return s;
        }
        return s.substring(0, i + 1);
    }

    public static String trimLeftAndRight(String s) {
        return trimLeft(trimRight(s));
    }

    public static <S, T> List<T> projectToList(Iterator<S> iter, Projector<? super S, T> projector) {
        List<T> res = new ArrayList<T>();
        while (iter.hasNext()) {
            S item = iter.next();
            res.add(projector.project(item));
        }
        return res;
    }

    public static <S, T> List<T> projectToList(Iterable<S> iterable, Projector<? super S, T> projector) {
        return projectToList(iterable.iterator(), projector);
    }

    public static String safeSubstring(String s, int start) {
        return safeSubstring(s, start, s.length());
    }

    public static String safeSubstring(String s, int start, int end) {
        if (s == null) {
            return null;
        }
        if (start > s.length()) {
            start = s.length();
        } else if (start < 0) {
            start = 0;
        }
        if (end > s.length()) {
            end = s.length();
        }
        if (end < start) {
            end = start;
        }
        return s.substring(start, end);
    }

    public static Double tryParseDouble(String s, Double defVal) {
        try {
            double d = Double.parseDouble(s);
            return d;
        } catch (Exception ex) {
            return defVal;
        }
    }

    public static Integer tryParseInteger(String s, Integer defVal) {
        try {
            Integer i = Integer.parseInt(s);
            return i;
        } catch (Exception ex) {
            return defVal;
        }
    }

    public static Boolean tryParseBoolean(String s, Boolean defVal) {
        try {
            boolean i = Boolean.parseBoolean(s);
            return i;
        } catch (Exception ex) {
            return defVal;
        }
    }

    public static Boolean tryParseBoolean(String s) {
        return tryParseBoolean(s, null);
    }

    public static Double tryParseDouble(String s) {
        return tryParseDouble(s, null);
    }

    public static Integer tryParseInteger(String s) {
        return tryParseInteger(s, null);
    }

    public static int tryParseInt(String s) {
        return tryParseInteger(s, 0);
    }

    public static Long tryParseLong(String s, Long defVal) {
        try {
            long l = Long.parseLong(s);
            return l;
        } catch (Exception ex) {
            return defVal;
        }
    }

    public static Object tryParseNumber(String s) {
        Integer i = tryParseInteger(s);
        if (i == null) {
            Double d = tryParseDouble(s);
            return d;
        }
        return i;
    }

    public static boolean isStrEmpty(String s) {
        return s == null || s.length() == 0;
    }

    public static boolean isStrEmptyOrWhiteSpace(String s) {
        return isStrEmpty(s) || s.trim().length() == 0;
    }

    public static String safeEnumName(Enum e) {
        return e == null ? null : e.name();
    }

    public static <E extends Enum<E>> E safeStringToEnum(Class<E> c, String s) {
        return s == null ? null : Enum.valueOf(c, s);
    }

    public static <E extends Enum<E>> E safeTryStringToEnum(Class<E> c, String s, E noSuchValDef) {
        try {
            return s == null ? null : Enum.valueOf(c, s);
        } catch (IllegalArgumentException ex) {
            return noSuchValDef;
        }
    }

    public static interface MakeSuccessor<T> {

        public T makeSuccessor(T val, int generation);
    }

    public static <T> T addAsUnique(Set<T> set, T val, MakeSuccessor<T> succ) {
        int i = 0;
        T val2 = val;

        while (set.contains(val2)) {
            val2 = succ.makeSuccessor(val, i++);
        }

        set.add(val2);

        return val2;
    }

    public static <K, V> HashMap<K, V> makeHashMapOfSize(int size) {
        return new HashMap<K, V>(calcHashMapCapacity(size));
    }

    public static <T> LinkedHashSet<T> makeLinkedHashSet(T[] arr) {
        LinkedHashSet<T> res = new LinkedHashSet<T>(calcHashMapCapacity(arr.length));
        res.addAll(Arrays.asList(arr));
        return res;
    }

    public static <T> int arrLengthFix(T[] arr) {
        return arr == null ? 0 : arr.length;
    }

    public static String replicateChar(char c, int count) {
        if (count == 0) {
            return "";
        }
        char[] a = new char[count];
        //for (int i = 0; i < count; i++) {
        //    a[i] = c;
        //}
        Arrays.fill(a, c);
        return new String(a);
    }

    public static String safeReplicateChar(char c, int count) {
        return count <= 0 ? "" : replicateChar(c, count);
    }

    public static <I extends Number> String padInt(I i, int digits) {
        return padIntEx(i, digits, '0');
    }

    //ww: do wrzucania separatora co kilka znaków, przydaje się np. do
    // wstawiania separatora tysięcy do liczb
    public static String chunkStringWithSep(String s, int chunkSize, String sep, boolean rightToLeft) {
        int len = s.length();

        if (chunkSize > 0 && len > chunkSize && !isStrEmpty(sep)) {
            int step;

            if (rightToLeft) {
                step = s.length() % chunkSize;
                if (step == 0) {
                    step = chunkSize;
                }
            } else {
                step = chunkSize;
            }

            StringBuilder sb = new StringBuilder();
            int p = 0;
            while (p < len) {
                int newP = p + step;
                sb.append(safeSubstring(s, p, newP));
                p = newP;
                step = chunkSize;
                if (p < len) {
                    sb.append(sep);
                }
            }

            s = sb.toString();
        }
        return s;
    }

    public static <I extends Number> String padIntEx(I i, int digits, char padChar, String optThousandsSep) {
        long val = i == null ? 0 : i.longValue();
        String sign = val < 0 ? "-" : "";
        String res = i == null ? "" : Long.toString(Math.abs(val));

        return leftPad(sign + chunkStringWithSep(res, 3, optThousandsSep, true), padChar, digits);
    }

    public static <I extends Number> String padIntEx(I i, int digits, char padChar) {
        return padIntEx(i, digits, padChar, null);
    }

    public static String padLeft(String val, char c, int minChars) {
        return leftPad(val, c, minChars);
    }

    public static String padStr(String val, char c, int minChars, boolean leftPadding) {
        String res;
        if (val.length() < minChars) {
            String padding = replicateChar(c, minChars - val.length());
            if (leftPadding) {
                res = padding + val;
            } else {
                res = val + padding;
            }
        } else {
            res = val;
        }
        return res;
    }

    public static String leftPad(String val, char c, int minChars) {
        return padStr(val, c, minChars, true);
    }

    // for negative -n.f gives -n
    public static long getIntegral(double d) {
        return (long) d;
    }

    // for negative -n.f gives -n
    public static long getIntegral(double d, boolean doRound) {
        return doRound ? Math.round(d) : (long) d;
    }

    // gives negative fractional for negative numbers
    public static double getFractional(double d) {
        return d - getIntegral(d);
    }

    public static String doubleToString(double d, int prec, boolean reduceFracPart, boolean doRound) {
        if (prec == 0) {
            return Long.toString(getIntegral(d, doRound));
        }
        long intPart = getIntegral(d);
        String intPartStr = Long.toString(intPart);
//        long fracPart = getIntegral(getFractional(d) * Math.pow(10, prec), doRound);
        long fracPart = getIntegral(getIntegral(getFractional(d) * Math.pow(10, prec + 1)) / 10d, doRound);
        long fracPartAbs = Math.abs(fracPart);
        String fracPartStr = leftPad(Long.toString(fracPartAbs), '0', prec);

//        if (fracPartStr.length() > prec) {
//            System.out.println("???? len=" + fracPartStr.length() + ", prec=" + prec + ", d=" + d + ", fracPartStr=" + fracPartStr);
//            fracPartStr = fracPartStr.substring(1);
//            intPartStr = Long.toString(intPart + 1);
//        }
        fracPartStr = "." + fracPartStr;

        if (reduceFracPart) {
            int zeroIdx = fracPartStr.length() - 1;
            while (zeroIdx > 0 && fracPartStr.charAt(zeroIdx) == '0') {
                zeroIdx--;
            }
            fracPartStr = zeroIdx == 0 ? "" : fracPartStr.substring(0, zeroIdx + 1);
        }

        return (intPart == 0 && fracPart < 0 ? "-" : "") + intPartStr + fracPartStr;
    }

    public static String doubleToString(double d, int prec, boolean reduceFracPart) {
        return doubleToString(d, prec, reduceFracPart, false);
    }

    public static String doubleToString(double d, int prec) {
        return doubleToString(d, prec, false);
    }

    //ww: size < 0 => unknown
    public static <K, V, VV> Map<K, VV> iterator2mapEx(int size, Iterator<V> iter, Projector<V, K> keyProjector,
            Projector<V, VV> valProjector) {
        Map<K, VV> res = size >= 0 ? new LinkedHashMap<K, VV>(calcHashMapCapacity(size))
                : new LinkedHashMap<K, VV>();
        while (iter.hasNext()) {
            V item = iter.next();
            K key = keyProjector.project(item);
            VV val = valProjector.project(item);
            res.put(key, val);
        }
        return res;
    }

    @SuppressWarnings("unchecked")
    public static <V> Projector<V, V> getIdentityProjector() {
        return (Projector<V, V>) identityProjector;
    }

    //ww: size < 0 => unknown
    public static <K, V> Map<K, V> iterator2mapEx(int size, Iterator<V> iter, Projector<V, K> projector) {
//        Collections.
        return iterator2mapEx(size, iter, projector, BaseUtils.<V>getIdentityProjector());
    }

    public static <K, V> Map<K, V> iterator2map(Iterator<V> iter, Projector<V, K> projector) {
        return iterator2mapEx(-1, iter, projector);
//        Map<K, V> res = new LinkedHashMap<K, V>();
//        while (iter.hasNext()) {
//            V item = iter.next();
//            K key = projector.project(item);
//            res.put(key, item);
//        }
//        return res;
    }

    public static <K, V> Map<K, V> iterable2map(Iterable<V> iterable, Projector<V, K> projector) {
        return iterator2map(iterable.iterator(), projector);
    }

    public static <K, V> Map<K, V> projectToMap(Iterable<V> iterable, Projector<V, K> projector) {
        return iterator2map(iterable.iterator(), projector);
    }

    public static <K, V> Map<K, V> projectToMap(Collection<V> coll, Projector<V, K> projector) {
        return iterator2mapEx(coll.size(), coll.iterator(), projector);
    }

    public static <K, V, VV> Map<K, VV> projectToMap(Collection<V> coll, Projector<V, K> keyProjector,
            Projector<V, VV> valProjector) {
        return iterator2mapEx(coll.size(), coll.iterator(), keyProjector, valProjector);
    }

    public static <K, V> Map<K, V> array2map(V[] arr, Projector<V, K> projector) {
        int len = arrLengthFix(arr);
        Map<K, V> res = new LinkedHashMap<K, V>(calcHashMapCapacity(len));

        for (int i = 0; i < len; i++) {
            V item = arr[i];
            K key = projector.project(item);
            res.put(key, item);
        }
        return res;
    }

    public static <T, T2> int findInCollectionEx(Collection<T> coll, T2 item, Projector<T, T2> proj) {
        int idx = 0;
        for (T collItem : coll) {
            T2 projectedCollItem = proj.project(collItem);
            if (BaseUtils.safeEquals(projectedCollItem, item)) {
                return idx;
            }
            idx++;
        }
        return -1;
    }

    public static <T> int findInCollection(Collection<T> coll, T item) {
        int idx = 0;
        for (T collItem : coll) {
            if (BaseUtils.safeEquals(collItem, item)) {
                return idx;
            }
            idx++;
        }
        return -1;
    }

    public static boolean isCollectionEmpty(Collection c) {
        return c == null || c.isEmpty();
    }

    // it is not safe - i.e. no nulls allowed
    public static int findNthPos(String s, String substr, int n) {
        if (n <= 0) {
            return 0;
        }

        int pos = -substr.length();
        while (n > 0) {
            pos = s.indexOf(substr, pos + substr.length());
            if (pos < 0) {
                return s.length();
            }
            n--;
        }
        return pos;
    }

    // it is not safe - i.e. no nulls allowed
    // n <= 0 -> returns s.length
    // n == 1 -> returns last pos
    // if there is less than n occurences substr in s
    public static int findNthLastPos(String s, String substr, int n) {
        int pos = s.length();
        for (; n > 0 && pos >= 0; n--) {
            pos = s.lastIndexOf(substr, pos - 1);
        }
        return pos;
    }

    public static String dropLastItems(String s, String itemSep, int dropCount) {
        if (s == null) {
            return null;
        }

        return safeSubstring(s, 0, findNthLastPos(s, itemSep, dropCount));
    }

    public static String dropFirstItems(String s, String itemSep, int dropCount) {
        if (s == null) {
            return null;
        }

        return safeSubstring(s, findNthPos(s, itemSep, dropCount) + itemSep.length());
    }

    // non-empty string starting with _a-z and having only _a-z0-9 chars
    // (ignoring case)
    public static boolean isProperIdent(String s) {
        if (isStrEmpty(s)) {
            return false;
        }

        char c = Character.toLowerCase(s.charAt(0));

        if (!(c == '_' || c >= 'a' && c <= 'z')) {
            return false;
        }

        for (int i = 1; i < s.length(); i++) {
            c = Character.toLowerCase(s.charAt(i));
            if (!(c == '_' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9')) {
                return false;
            }
        }

        return true;
    }

    public static String getFirstItems(String s, String sep, int n) {
        int pos = findNthPos(s, sep, n);
        return safeSubstring(s, 0, pos);
    }

    public static String getLastItems(String s, String sep, int n) {
        int pos = findNthLastPos(s, sep, n);
        if (pos == -1) {
            pos = 0;
        } else {
            pos = pos + sep.length();
        }

        return safeSubstring(s, pos);
    }

    public static String extractSimpleClassName(String className) {
        return getLastItems(className, ".", 1);
    }
    public static final String NULL_CLASS_NAME = "<null>";

    public static String safeGetClassName(Object o) {
        return o == null ? NULL_CLASS_NAME : o.getClass().getName();
    }

    public static String safeGetSimpleClassName(Object o) {
        return extractSimpleClassName(safeGetClassName(o));
    }

    public static <K, V> Map<K, V> dropNullKeys(Map<K, V> m) {
        Map<K, V> res = new LinkedHashMap<K, V>();

        for (Entry<K, V> e : m.entrySet()) {
            V val = e.getValue();
            if (val != null) {
                res.put(e.getKey(), val);
            }
        }

        return res;
    }

    public static String dropPrefixOrGiveNull(String s, String prefix) {
        return s != null && s.startsWith(prefix) ? s.substring(prefix.length()) : null;
    }

    public static Set<String> filterAndDropPrefix(Set<String> set, String prefix) {
        Set<String> res = new LinkedHashSet<String>();

        for (String item : set) {
            String droppedPrefixItem = dropPrefixOrGiveNull(item, prefix);
            if (droppedPrefixItem != null) {
                res.add(droppedPrefixItem);
            }
        }

        return res;
    }

    public static <K, V> Map<K, V> filterMap(Map<K, V> map, Collection<K> keys) {
        if (map == null) {
            throw new LameRuntimeException("map is null");
        }
        if (keys == null) {
            throw new LameRuntimeException("keys == null");
        }
        Map<K, V> res = new LinkedHashMap<K, V>();
        for (K key : keys) {
            if (map.containsKey(key)) {
                res.put(key, map.get(key));
            }
        }
        return res;
    }

    public static <K, V> Map<K, V> extractChangedEntries(Map<K, V> oldMap, Map<K, V> newMap) {
        return extractChangedEntries(oldMap, newMap, null, null);
    }

    // oldMap and newMap can be null (one of them or both), result is never null
    // (but it may be an empty map)
    // null value for key k is treated like no value for k,
    // null unchanged values are filtered out from result
    @SuppressWarnings(value = "unchecked")
    public static <K, V> Map<K, V> extractChangedEntries(Map<K, V> oldMap, Map<K, V> newMap,
            Set<String> exactChangedPaths, String parentPath) {
        Set<K> allKeys = new LinkedHashSet<K>();

        if (newMap != null) {
            allKeys.addAll(newMap.keySet());
        }
        if (oldMap != null) {
            allKeys.addAll(oldMap.keySet());
        }

        Map<K, V> res = new LinkedHashMap<K, V>();

        for (K key : allKeys) {
            V oldV = oldMap != null ? oldMap.get(key) : null;
            V newV = newMap != null ? newMap.get(key) : null;
            if (!safeEquals(oldV, newV)) {
                if (exactChangedPaths != null) {
                    String fullPathKey = isStrEmpty(parentPath)
                            ? safeToStringNotNull(key) : parentPath + "." + key;
                    if (oldV instanceof Map && newV instanceof Map) {
                        Map innerOldMap = (Map) oldV;
                        Map innerNewMap = (Map) newV;
                        extractChangedEntries(innerOldMap, innerNewMap,
                                exactChangedPaths, fullPathKey);
                    } else {
                        exactChangedPaths.add(fullPathKey);
                    }
                }

                res.put(key, newV);
            }
        }

        return res;
    }

    public static void addPrefixToItems(Collection<String> coll, String prefix, Collection<String> res) {
        for (String s : coll) {
            res.add(prefix + s);
        }
    }

    public static Set<String> addPrefixToItems(Set<String> set, String prefix) {
        Set<String> res = new LinkedHashSet<String>(calcHashMapCapacity(set.size()));
        addPrefixToItems(set, prefix, res);
        return res;
    }

//    public static String getCurrentStackTrace() {
//        Exception e = null;
//        try {
//            throw new LameRuntimeException("fake exception, just to get stack trace");
//        } catch (Exception ex) {
//            e = ex;
//        }
//        SimpleByteArrayOutputStream out = new SimpleByteArrayOutputStream();
//        PrintStream ps = new PrintStream(out);
//        //StringWriter sw = new StringWriter();
//        //PrintWriter pw = new PrintWriter(sw, true);
//        e.printStackTrace(ps);
////        try {
////            out.flush();
////        } catch(Exception ex) {
////
////        }
//
//        return out.toString();
//    }
    // just a copy of Arrays.copyOf ;-)
    public static byte[] copyOf(byte[] original, int newLength) {
        byte[] copy = new byte[newLength];
        System.arraycopy(original, 0, copy, 0,
                Math.min(original.length, newLength));
        return copy;
    }

    public static boolean safeStartsWith(String s, String prefix) {
        if (isStrEmpty(s)) {
            return isStrEmpty(prefix);
        }
        return s.startsWith(prefix);
    }

    public static int indexOfFirst(String s, String[] strs) {
        if (s == null) {
            return -1;
        }

        int pos = -1;
        for (String str : strs) {
            int strPos = s.indexOf(str);
            if (strPos > -1 && (pos == -1 || strPos < pos)) {
                pos = strPos;
            }
        }
        return pos;
    }

    public static String mergeTwoWithSep(String s1, String s2, String sep) {
        if (isStrEmpty(s1)) {
            return s2;
        }
        if (isStrEmpty(s2)) {
            return s1;
        }
        return s1 + sep + s2;
    }

    public static String mergePaths(String dirPath, String filePath, String pathSep, String levelUp) {
        if (filePath.startsWith(pathSep)) {
            return filePath;
        }

        while (levelUp.equals(getFirstItems(filePath, pathSep, 1))) {
            filePath = dropFirstItems(filePath, pathSep, 1);
            dirPath = dropLastItems(dirPath, pathSep, 1);
        }

        return mergeTwoWithSep(dirPath, filePath, pathSep);
    }

    public static <TS, TD, C extends Collection<TD>> C projectIntoCollection(
            Iterable<TS> src, C dst, Projector<TS, TD> projector, boolean dropNulls) {
        for (TS srcItem : src) {
            TD dstItem = projector.project(srcItem);
            if (!dropNulls || dstItem != null) {
                dst.add(dstItem);
            }
        }
        return dst;
    }

    public static <TS, TD> Set<TD> projectToSet(Iterable<TS> src,
            Projector<TS, TD> projector, boolean dropNull) {
        return projectIntoCollection(src, new LinkedHashSet<TD>(), projector, dropNull);
    }

    public static Set<String> dropPrefixOrGiveNullInSet(Set<String> strs,
            final String prefix, boolean dropNull) {
        return projectToSet(strs, new Projector<String, String>() {
            public String project(String val) {
                return dropPrefixOrGiveNull(val, prefix);
            }
        }, dropNull);
    }
    private static final String randomTokenChars = "QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR";

    public static String generateRandomToken(int length) {
        return generateRandomTokenEx(length, randomTokenChars);
    }

    public static String generateRandomTokenEx(int length, String randomTokenChars) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < length; i++) {
            int te = nextRandomInt(randomTokenChars.length());
            sb.append(randomTokenChars.charAt(te));
        }

        return sb.toString();
    }

    public static int nextRandomInt(int upperBound) {
        if (simpleRandom == null) {
            throw new LameRuntimeException("simple random generator is not initialized");
        }
        return simpleRandom.nextInt(upperBound);
    }

    public static void renderHtmlAttrs(Map<String, String> attrs, StringBuilder sb) {
        if (attrs != null) {
            for (Map.Entry<String, String> e : attrs.entrySet()) {
                String name = e.getKey();
                if (name == null) {
                    continue;
                }
                String value = e.getValue();
                if (value == null) {
                    continue;
                }
                sb.append(" ");
                sb.append(name);
                sb.append("=\"");
                sb.append(BaseUtils.encodeForHTMLTag(value));
                sb.append("\"");
            }
        }
    }

    public static int findItemInStr(String s, final String item, String sep) {
        int itemIdx = processItemsInStr(s, sep, new IParametrizedContinuationWithReturn<String, Boolean>() {
            public Boolean doIt(String param) {
                return !param.equals(item);
            }
        }, -1);
        return itemIdx;
    }

    public static boolean strHasItem(String s, String item, String sep) {
        return findItemInStr(s, item, sep) != -1;
    }

    // newItem == null -> remove old item if exists
    public static String strReplaceOrAddItem(String s, String oldItem, String newItem, String sep) {
        List<String> items = splitBySep(s, sep);
        int idx = items.indexOf(oldItem);
        if (idx != -1) {
            if (newItem != null) {
                items.set(idx, newItem);
            } else {
                items.remove(idx);
            }
        } else {
            if (newItem != null) {
                items.add(newItem);
            }
        }
        return mergeWithSepEx(items, sep);
    }

    public static String dropOptionalSuffix(String s, String suffix) {
        return dropOptionalSuffix(s, suffix, false);
    }

    public static String dropOneOfSuffixesOrReturnNull(String s, boolean ignoreCase, Iterable<String> suffixes) {
        for (String suffix : suffixes) {
            if (strHasSuffix(s, suffix, ignoreCase)) {
                return s.substring(0, s.length() - suffix.length());
            }
        }
        return null;
    }

    public static String dropOneOfSuffixesOrReturnNull(String s, boolean ignoreCase, String... suffixes) {
        return dropOneOfSuffixesOrReturnNull(s, ignoreCase, Arrays.asList(suffixes));
    }

    public static String dropOptionalSuffix(String s, String suffix, boolean ignoreCase) {
        if (s == null) {
            return null;
        }
        if (suffix == null || !strHasSuffix(s, suffix, ignoreCase)) {
            return s;
        }

        return s.substring(0, s.length() - suffix.length());
    }

    public static String dropOptionalPrefix(String s, String prefix, boolean ignoreCase) {
        if (s == null) {
            return null;
        }
        if (prefix == null || !strHasPrefix(s, prefix, ignoreCase)) {
            return s;
        }

        return s.substring(prefix.length());
    }

    public static <K, V> V safeMapGet(Map<K, V> map, K key) {
        return map == null ? null : map.get(key);
    }

    public static Set<String> splitUniqueBySep(String s, String sep) {
        return splitUniqueBySep(s, sep, false);
    }

    public static Set<String> splitUniqueBySep(String s, String sep, boolean doTrim) {
        if (s == null) {
            return null;
        }
        Set<String> res = new LinkedHashSet<String>();
        return splitBySepIntoColl(s, sep, res, doTrim);
    }

    public static List<String> splitByCapitals(String s) {
        if (s == null) {
            return null;
        }

        List<String> res = new ArrayList<String>();
        if (s.length() == 0) {
            return res;
        }

        int startPos = 0;
        int endPos = 1;

        while (endPos < s.length()) {
            char c = s.charAt(endPos);
            if (c >= 'A' && c <= 'Z') {
                res.add(s.substring(startPos, endPos));
                startPos = endPos;
            }
            endPos++;
        }

        res.add(s.substring(startPos));

        return res;
    }

    public static <T> Set<T> paramsAsSet(T... args) {
        Set<T> res = new LinkedHashSet<T>(calcHashMapCapacity(args.length));
//        for (T arg : args) {
//            res.add(arg);
//        }
        res.addAll(Arrays.asList(args));
        return res;
    }

    public static <T> List<T> paramsAsArrayList(T... args) {
        List<T> res = new ArrayList<T>(Arrays.asList(args));
        return res;
    }

    public static <T> List<T> paramsAsReadOnlyList(T... args) {
        return Arrays.asList(args);
    }

    public static String[] strParamsAsArray(String... args) {
        return args;
    }

    public static Object[] paramsAsArray(Object... args) {
        return args;
    }

    public static <T> T[] paramsAsArrayEx(T... args) {
        return args;
    }

    public static int fixPageNumByCountAndPageSize(int pageNum, int pageSize, int count) {
        int pageCount = (count + pageSize - 1) / pageSize;

        if (pageNum >= pageCount) {
            pageNum = pageCount - 1;
        }

        if (pageNum < 0) {
            pageNum = 0;
        }

        return pageNum;
    }

    public static int safeGetCollectionSize(Collection c) {
        return collectionSizeFix(c);
    }

    public static int collectionSizeFix(Collection c) {
        return c == null ? 0 : c.size();
    }

    @SuppressWarnings(value = "unchecked")
    public static <V, T extends Comparable> Comparator<V> makeComparator(final Projector<V, T> proj) {
        return new Comparator<V>() {
            public int compare(V o1, V o2) {
                T v1 = o1 == null ? null : proj.project(o1);
                T v2 = o2 == null ? null : proj.project(o2);
                if (v1 == null) {
                    if (v2 == null) {
                        return 0;
                    } else {
                        return -1;
                    }
                } else if (v2 == null) {
                    return 1;
                }
                return v1.compareTo(v2);
            }
        };
    }

    public static <K, V> Map<K, V> putInLinkedMap(Map<K, V> m, K k, V v) {
        if (m == null) {
            m = new LinkedHashMap<K, V>();
        }
        m.put(k, v);
        return m;
    }

    public static <K, V> V safeMapRemove(Map<K, V> m, K k) {
        if (m == null) {
            return null;
        }
        return m.remove(k);
    }

    public static <T> void addFirstItems(Collection<T> dest, Collection<T> src, int cnt) {
        if (src == null) {
            return;
        }

        Iterator<T> iter = src.iterator();
        while (cnt > 0 && iter.hasNext()) {
            T item = iter.next();
            dest.add(item);
            cnt--;
        }
    }

    public static List<Integer> preparePaginationPageNums(int pageNum, int pageSize, int objCount) {
//        System.out.println("preparePaginationPageNums: qq");
//        if (true) {
//            return new ArrayList<Integer>(BaseUtils.paramsAsSet(1, 2, 3, 4, 5, 6, 7, 9));
//        }

        pageNum = BaseUtils.fixPageNumByCountAndPageSize(pageNum, pageSize, objCount);

        int pageCount = (objCount + pageSize - 1) / pageSize;

        List<Integer> res = new ArrayList<Integer>();

        res.add(0);

        // special cases:
        // 1) pageCount < 5
        // 2) pageCount == 5
        // 3) pageNum <= 2
        // 4) pageNum >= pageCount - 4
        int minMovingPage = pageNum - 1;
        int maxMovingPage = pageNum + 1;

        if (pageNum < 2) {
            minMovingPage = 1;
            maxMovingPage = 3;
        } else if (pageNum > pageCount - 4) {
            minMovingPage = pageCount - 4;
            maxMovingPage = pageCount - 2;
        }

        if (minMovingPage <= 0) {
            minMovingPage = 1;
        }

        if (maxMovingPage >= pageCount - 1) {
            maxMovingPage = pageCount - 2;
        }

        for (int i = minMovingPage; i <= maxMovingPage; i++) {
            res.add(i);
        }

        if (pageCount > 1) {
            res.add(pageCount - 1);
        }

        return res;
    }

    public static LinkedHashSet<String> mergeAsLinkedHashSet(Collection<String> c1, Collection<String> c2) {
        LinkedHashSet<String> res = new LinkedHashSet<String>();
        if (c1 != null) {
            res.addAll(c1);
        }
        if (c2 != null) {
            res.addAll(c2);
        }
        return res;
    }

    public static <T> boolean collectionContainsFix(Collection<T> coll, T item) {
        return coll != null && coll.contains(item);
    }

    public static <T> Set<T> addToSet(Set<T> s, T item) {
        if (s == null) {
            s = new LinkedHashSet<T>();
        }
        s.add(item);
        return s;
    }

    public static <T> void removeFromIterator(Iterator<T> iter,
            IParametrizedContinuationWithReturn<? super T, Boolean> whatToRemove) {
        while (iter.hasNext()) {
            T item = iter.next();
            if (whatToRemove.doIt(item)) {
                iter.remove();
            }
        }
    }

    public static <T> void removeFromIterable(Iterable<T> iterable,
            IParametrizedContinuationWithReturn<? super T, Boolean> whatToRemove) {
        removeFromIterator(iterable.iterator(), whatToRemove);
    }

    public static boolean isObjectEmptyString(Object obj, boolean treatNullAsEmpty) {
        if (obj == null) {
            return treatNullAsEmpty;
        }
        if (obj instanceof String) {
            String str = (String) obj;
            return str.length() == 0;
        }
        return false;
    }

    public static String arrayToString(Object[] arr) {
        if (arr == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        sb.append("[");
        boolean first = true;
        for (Object o : arr) {
            if (first) {
                first = false;
            } else {
                sb.append(",");
            }
            sb.append(o);
        }
        sb.append("]");

        return sb.toString();
    }

    public static String mapToString(Map<?, ?> m, String keyValSep, String entrySep, SuppressOptions suppOp, boolean addCurlyBrackets) {
        if (m == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        if (addCurlyBrackets) {
            sb.append("{");
        }
        boolean first = true;
        for (Entry e : m.entrySet()) {
            Object val = e.getValue();
            if (suppOp.shouldSuppress(val)) {
                continue;
            }
            if (first) {
                first = false;
            } else {
                sb.append(entrySep);
            }
            if (val instanceof Object[]) {
                val = arrayToString((Object[]) val);
            }
            sb.append(e.getKey()).append(keyValSep).append(val);
        }
        if (addCurlyBrackets) {
            sb.append("}");
        }

        return sb.toString();
    }

    public enum SuppressOptions {

        DONT_SUPPRESS, SUPPRESS_NULLS, SUPPRESS_NULLS_AND_WHITESPACES;

        private boolean shouldSuppressNull() {
            return compareTo(SUPPRESS_NULLS) == 0 || compareTo(SUPPRESS_NULLS_AND_WHITESPACES) == 0;
        }

        public boolean shouldSuppress(Object o) {
            if (o == null && shouldSuppressNull()) {
                return true;
            }
            if (o != null && compareTo(SUPPRESS_NULLS_AND_WHITESPACES) == 0
                    && o.toString().trim().isEmpty()) {
                return true;
            }
            return false;
        }
    }

    public static String mapToString(Map<?, ?> m, String keyValSep, String entrySep, boolean suppressNulls, boolean addCurlyBrackets) {
        SuppressOptions suppOp = suppressNulls ? SuppressOptions.SUPPRESS_NULLS : SuppressOptions.DONT_SUPPRESS;
        return mapToString(m, keyValSep, entrySep, suppOp, addCurlyBrackets);
    }

    public static String mapToString(Map<?, ?> m) {
        return mapToString(m, ":", ",", false, true);
    }

    public static String[] mapToStringArray(Map<String, String> map) {
        final String[] strings = new String[map.size()];
        int i = 0;
        for (Map.Entry<String, String> e : map.entrySet()) {
            strings[i] = e.getKey() + '=' + e.getValue();
            i++;
        }
        return strings;
    }

    public static String limitStr(String str, int limit) {
        //limit = 10;
        //System.out.println("^^^limitStr^^^");
        if (strLengthFix(str) <= limit) {
            return str;
        }
        return str.substring(0, limit) + "...";
    }

    public static <T> T removeLastItem(List<T> list) {
        return popLastItem(list);
    }

    public static <T> T popLastItem(List<T> list) {
        int lastIdx = collectionSizeFix(list) - 1;
        if (lastIdx < 0) {
            throw new LameRuntimeException("list is null or empty");
        }

        return list.remove(lastIdx);
    }
    public static final String UPPERPL = "ĄĆĘŁŃÓŚŻŹ";
    public static final String LOWERPL = "ąćęłńóśżź";
    public static final String POLSKAWEMALEPL = "acelnoszz";
    public static final String POLSKAWEDUZEPL = POLSKAWEMALEPL.toUpperCase();
    public static final String POLSKIE_OGONKI_DUZE_I_MALE = UPPERPL + LOWERPL;
    public static final String POLSKAWE_DUZE_I_MALE = POLSKAWEDUZEPL + POLSKAWEMALEPL;

    //ww: tabele konwersji nie mogą być nullami, muszą mieć taką samą długość
    public static char convertChar(char c, String convertFromChars, String convertToChars) {
        int idx = convertFromChars.indexOf(c);

        return idx >= 0 ? convertToChars.charAt(idx) : c;
    }

    public static String deAccentPolishChars(String s) {
        return convertCharsByFromToStrs(s, POLSKIE_OGONKI_DUZE_I_MALE, POLSKAWE_DUZE_I_MALE);

//        char[] resCArr = new char[s.length()];
//
//        for (int i = 0; i < s.length(); i++) {
//            char c = s.charAt(i);
//            resCArr[i] = convertChar(c, POLSKIE_OGONKI_DUZE_I_MALE, POLSKAWE_DUZE_I_MALE);
//        }
//
//        return new String(resCArr);
    }

    public static String toUpperCasePl(String s) {
        char[] resCArr = new char[s.length()];

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int letterIdx = LOWERPL.indexOf(c);
            if (letterIdx >= 0) {
                resCArr[i] = UPPERPL.charAt(letterIdx);
            } else {
                resCArr[i] = Character.toUpperCase(c);
            }
        }

        return new String(resCArr);
    }

    public static String maleLiteryNaPolskawe(String str) {
        char[] resCArr = new char[str.length()];

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            int letterIdx;

            if (c >= 128) {
                letterIdx = LOWERPL.indexOf(c);
            } else {
                letterIdx = -1;
            }

            if (letterIdx >= 0) {
                resCArr[i] = POLSKAWEMALEPL.charAt(letterIdx);
            } else {
                resCArr[i] = c;
            }//Character.toUpperCase(c);
        }

        return new String(resCArr);
    }

    public static String convertCharsByFromToStrs(String str, String charsFrom, String charsTo) {
        char[] resCArr = new char[str.length()];

        for (int i = 0; i < str.length(); i++) {

            resCArr[i] = convertChar(str.charAt(i), charsFrom, charsTo);

//            char c = str.charAt(i);
//            int letterIdx;
//
//            letterIdx = charsFrom.indexOf(c);
//
//            if (letterIdx >= 0) {
//                resCArr[i] = charsTo.charAt(letterIdx);
//            } else {
//                resCArr[i] = c;
//            }//Character.toUpperCase(c);
        }

        return new String(resCArr);
    }

    public static String trueHighLowAsciiToChar(String str, char cc) {
        char[] resCArr = new char[str.length()];

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            if (c >= 128 || c <= 31) {
                resCArr[i] = cc;
            } else {
                resCArr[i] = c;
            }
        }

        return new String(resCArr);
    }

    // bez high, tylko low konwertuje
    public static String highLowAsciiToSpace(String str) {
        char[] resCArr = new char[str.length()];

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            if (/*c >= 128 ||*/c <= 31) {
                resCArr[i] = ' ';
            } else {
                resCArr[i] = c;
            }
        }

        return new String(resCArr);
    }

    public static int indexOfLastChar(String inStr, String chars) {
        for (int i = strLengthFix(inStr) - 1; i >= 0; i--) {
            char c = inStr.charAt(i);
            if (chars.indexOf(c) >= 0) {
                return i;
            }
        }
        return -1;
    }

    public static int indexOfFirstChar(String inStr, String chars) {
        for (int i = 0; i < strLengthFix(inStr); i++) {
            char c = inStr.charAt(i);
            if (chars.indexOf(c) >= 0) {
                return i;
            }
        }
        return -1;
    }

    public static boolean strContainsAnyOfChars(String str, String chars) {
        return indexOfFirstChar(str, chars) >= 0;
    }

    public static boolean strHasSuffix(String s, String suffix, boolean ignoreCase) {
        int suffixLen = strLengthFix(suffix);
        int strLen = strLengthFix(s);
        if (suffixLen == 0) {
            return true;
        }
        if (strLen < suffixLen) {
            return false;
        }
        String endPart = s.substring(strLen - suffixLen);
        if (ignoreCase) {
            return suffix.equalsIgnoreCase(endPart);
        } else {
            return suffix.equals(endPart);
        }
    }

    public static boolean strHasPrefix(String s, String prefix, boolean ignoreCase) {
        int prefixLen = strLengthFix(prefix);
        int strLen = strLengthFix(s);
        if (prefixLen == 0) {
            return true;
        }
        if (strLen < prefixLen) {
            return false;
        }
        String part = s.substring(0, prefixLen);
        if (ignoreCase) {
            return prefix.equalsIgnoreCase(part);
        } else {
            return prefix.equals(part);
        }
    }

    public static String substringFromEnd(String str, int len) {
        if (str == null) {
            return null;
        }
        int pos = str.length() - len;
        if (pos < 0) {
            pos = 0;
        }
        return str.substring(pos);
    }

    public static boolean boolNullToDef(Boolean b, boolean def) {
        return b == null ? def : b;
    }

    // dir sep = '/', gives null for null, "/" for empty string
    public static String ensureDirSepPrefix(String dirPath) {
        if (dirPath == null) {
            return null;
        }
        if (dirPath.length() == 0 || dirPath.charAt(0) != '/') {
            return '/' + dirPath;
        }
        return dirPath;
    }

    //4C - for concatenation - specjalna obsługa nulla i pustego stringa
    // wtedy daje pusty string -> zał. że jest to sklejane z nazwą pliku
    // która musi być pełna (lub względna - w katalogu roboczym)
    public static String ensureDirSepPostfix4C(String dirPath) {
        if (isStrEmpty(dirPath)) {
            return "";
        }
        return ensureDirSepPostfix(dirPath);
    }

    // dir sep = '/', gives null for null, "/" for empty string
    public static String ensureDirSepPostfix(String dirPath) {
        if (dirPath == null) {
            return null;
        }
        if (dirPath.length() == 0 || dirPath.charAt(dirPath.length() - 1) != '/') {
            return dirPath + '/';
        }
        return dirPath;
    }

    public static String getPrettyClassName(Object o) {
        if (o instanceof IPrettyNamedClass) {
            IPrettyNamedClass pnc = (IPrettyNamedClass) o;
            return pnc.getPrettyClassName();
        }
        return safeGetSimpleClassName(o);
    }

    public static <E extends Enum<E>> EnumSet<E> paramsToEnumSet(E p1, E... params) {
        return EnumSet.of(p1, params);
    }

    public static <T, V> void addAllWithProjector(Collection<T> dstColl, Collection<V> itemsToAdd,
            Projector<V, T> proj) {
        if (itemsToAdd == null) {
            return;
        }
        for (V item : itemsToAdd) {
            dstColl.add(proj.project(item));
        }
    }

    public static INamedPropsBean wrapAsNamedPropsBean(final MapHelper<String> mh) {
        return new INamedPropsBean() {
            public Object getPropValue(String propName) {
                return mh.get(propName);
            }

            public Set<String> getPropNames() {
                return mh.getPropNames();
            }
        };
    }

    public static INamedPropsBeanEx wrapAsNamedPropsBeanEx(final Map<String, Object> map) {
        return new INamedPropsBeanEx() {
            public Object getPropValue(String propName) {
                return map.get(propName);
            }

            public Set<String> getPropNames() {
                return map.keySet();
            }

            public void setPropValue(String propName, Object value) {
                map.put(propName, value);
            }
        };
    }

    @SuppressWarnings("unchecked")
    public static INamedPropsBeanWriter wrapAsNamedPropsBeanWriter(Object bean, boolean mandatory) {
        if (bean == null) {
            return null;
        }

        if (bean instanceof INamedPropsBeanWriter) {
            return (INamedPropsBeanWriter) bean;
        }
        if (bean instanceof Map) {
            return BaseUtils.wrapAsNamedPropsBeanEx((Map) bean);
        }

        if (mandatory) {
            throw new LameRuntimeException("wrapAsNamedPropsBeanWriter: "
                    + "cannot wrap object of class " + bean.getClass().getName());
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static INamedPropsBean wrapAsNamedPropsBean(Object bean, boolean mandatory) {
        if (bean == null) {
            return null;
        }

        if (bean instanceof INamedPropsBean) {
            return (INamedPropsBean) bean;
        }

        if (bean instanceof Map) {
            return wrapAsNamedPropsBeanEx((Map) bean);
        }

        if (bean instanceof MapHelper) {
            return wrapAsNamedPropsBean((MapHelper) bean);
        }

        if (mandatory) {
            throw new LameRuntimeException("wrapAsNamedPropsBean: "
                    + "cannot wrap object of class " + bean.getClass().getName());
        }

        return null;
    }
    public static final ProjectorToString projToStr = new ProjectorToString();

    public static class ProjectorToString<V> implements Projector<V, String> {

        public String project(V val) {
            return safeToString(val);
        }
    }

    @SuppressWarnings("unchecked")
    public static <V> ProjectorToString<V> getProjToString() {
        //Collections.emptyList()
        return (ProjectorToString) projToStr;
    }

    public static class CompoundProjector<V1, V2, V3> implements Projector<V1, V3> {

        private Projector<V1, V2> p1;
        private Projector<V2, V3> p2;

        public CompoundProjector(Projector<V1, V2> p1, Projector<V2, V3> p2) {
            this.p1 = p1;
            this.p2 = p2;
        }

        public V3 project(V1 val) {
            return p2.project(p1.project(val));
        }
    }

    @SuppressWarnings("unchecked")
    public static <V1, V2> Projector<V1, String> makeCompoundToStringProjector(Projector<V1, V2> p) {
        return new CompoundProjector<V1, V2, String>(p,
                projToStr //getProjToString()
        );
    }

    public static String tableColNameToBeanFieldName(String colName) {
        char[] res = colName.toLowerCase().toCharArray();

        int pos = 0;

        while (true) {
            pos = colName.indexOf("_", pos);
            if (pos == -1 || pos == colName.length() - 1) {
                break;
            }
            res[pos + 1] = Character.toUpperCase(res[pos + 1]);
            pos++;
        }

        return String.valueOf(res).replace("_", "");
    }

    public static String beanFieldNameToTableColName(String fieldName) {
        StringBuilder sb = new StringBuilder();

        int pos = 0;

        for (int i = 1; i < fieldName.length(); i++) {
            char c = fieldName.charAt(i);
            if (c >= 'A' && c <= 'Z') {
                if (pos > 0) {
                    sb.append("_");
                }
                sb.append(fieldName.substring(pos, i));
                pos = i;
            }
        }

        if (pos > 0) {
            sb.append("_");
        }
        sb.append(fieldName.substring(pos, fieldName.length()));

        return sb.toString().toLowerCase();
    }

    public static <T1, T2> IGenericResourceProvider<T2> compoundProvider(
            final IGenericResourceProvider<T1> provider,
            final Projector<T1, T2> proj) {
        return new CompoundGenericResourceProvider<T1, T2>(provider, proj);
//        return new IGenericResourceProvider<T2>() {
//
//            public T2 gainResource(String resourceName) {
//                return proj.project(provider.gainResource(resourceName));
//            }
//
//            public long getLastModifiedOfResource(String resourceName) {
//                return provider.getLastModifiedOfResource(resourceName);
//            }
//        };
    }

    public static <T1, T2> IGenericResourceProviderEx<T2> compoundProvider(
            final IGenericResourceProviderEx<T1> provider,
            final Projector<T1, T2> proj) {
        return new CompoundGenericResourceProviderEx<T1, T2>(provider, proj);

//        return new IGenericResourceProviderEx<T2>() {
//
//            public T2 gainResource(String resourceName) {
//                return proj.project(provider.gainResource(resourceName));
//            }
//
//            public long getLastModifiedOfResource(String resourceName) {
//                return provider.getLastModifiedOfResource(resourceName);
//            }
//
//            public Set<String> getResourceNames(String base, Set<String> fileExts, boolean recurseSubDirs) {
//                return provider.getResourceNames(base, fileExts, recurseSubDirs);
//            }
//        };
    }

    public static <T> ILameCollector<T> collect(ILameCollector<T> collector, Iterable<T> items) {
        for (T item : items) {
            collector.add(item);
        }
        return collector;
    }

    // everything should be lowercased first
    public static String getBestAcceptedAndSupportedLanguage(String acceptedLangs,
            final Set<String> supportedLangs, final String defaultLang) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(acceptedLangs)) {
            return defaultLang;
        }
        return splitBySepIntoCollector(acceptedLangs, ",", new ILameCollector<String>() {
            String bestLang = defaultLang;
            double bestQuality = -1;

            public void add(String item) {
                Pair<String, String> landq = splitString(item, ";");
                String qStr = landq.v2 == null ? null : landq.v2.trim();
                Pair<String, String> qp = splitString(qStr, "=");
                qStr = qp.v2 == null ? null : qp.v2.trim();
                double currQ = BaseUtils.isStrEmpty(qStr) ? 1.0d : tryParseDouble(qStr, 0.1d);
                if (currQ > bestQuality) {
                    String lStr = landq.v1.trim();
                    if (supportedLangs.contains(lStr)) {
                        bestLang = lStr;
                        bestQuality = currQ;
                    } else {
                        lStr = splitString(lStr, "-").v1;
                        if (supportedLangs.contains(lStr)) {
                            bestLang = lStr;
                            bestQuality = currQ;
                        }
                    }
                }
            }
        }, true).bestLang;
    }

    public static <T extends Comparable<T>> Comparator<T> makeComparator() {
        return new Comparator<T>() {
            public int compare(T o1, T o2) {
                if (o1 == null) {
                    return o2 == null ? 0 : -1;
                } else if (o2 == null) {
                    return 1;
                }
                return o1.compareTo(o2);
            }
        };
    }
    public static final Projector<Map, Object> anyMapValProjector = new Projector<Map, Object>() {
        public Object project(Map val) {
            if (val == null || val.keySet().isEmpty()) {
                return null;
            }
            return val.values().iterator().next();
        }
    };

    public static String replaceNewLinesToRowSep(String txt, String rowSep) {
        if (txt == null) {
            return null;
        }
        return txt.replace("\r\n", rowSep).replace("\n", rowSep).replace("\r", rowSep);
    }

    // to samo co poniższe replaceUnixNewLinesToWindowsNewLine, ale chyba
    // wolniejsze nieco ;-)
    public static String replaceUnixNewLinesToCorrectNewLine(String txt) {
        if (txt == null) {
            return null;
        }
        return txt.replace("\r\n", "\u0001").replace("\r", "\u0002").replace("\n", "\r\n").replace("\u0002", "\r\n").replace("\u0001", "\r\n");
    }

    // to samo co powyższe replaceUnixNewLinesToCorrectNewLine, ale chyba
    // szybsze nieco ;-)
    public static String replaceUnixNewLinesToWindowsNewLine(String txt) {
        if (txt == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        int pos = 0;
        int len = txt.length();
        while (pos < len) {
            int newPos = findFirstLineBreak(txt, pos, false);
            if (newPos < 0) {
                break;
            }
            sb.append(txt, pos, newPos).append("\r\n");
            pos = eatLineBreak(txt, newPos);
        }
        sb.append(txt, pos, len);
        return sb.toString();
    }

    public static String safeToLowerCase(String s) {
        if (s == null) {
            return null;
        }
        return s.toLowerCase();
    }

    // fileExts może zawierać '/' - oznacza, że chcemy też dostać w wyniku katalogi
    // rozszerzenia muszą zaczynać się od kropki
    public static Set<String> listResources(final IResourceNamesProvider np, String basePath,
            final Set<String> fileExts, boolean recurseSubdirs) {

        final boolean acceptDirs = fileExts.contains("/");

        return listResourcesEx(np, basePath, new IPredicate<String>() {
            public Boolean project(String val) {
                if (acceptDirs && np.isDirectory(val)) {
                    return true;
                }
                for (String fileExt : fileExts) {
                    if (fileNameHasExt(val, fileExt, true, true)) {
                        return true;
                    }
                }
                return false;
            }
        }, recurseSubdirs, acceptDirs);
    }

    public static Set<String> listResourcesEx(IResourceNamesProvider np, String basePath,
            IPredicate<String> optNameMatcher, boolean recurseSubdirs, boolean collectDirs) {
        final Set<String> res = new LinkedHashSet<String>();
        ILameCollector<String> c = new ILameCollector<String>() {
            public void add(String item) {
                res.add(item);
            }
        };
        listResourcesEx(np, basePath, optNameMatcher, recurseSubdirs, collectDirs, c);
        return res;
    }

    public static void listResourcesEx(IResourceNamesProvider np, String basePath,
            IPredicate<String> optNameMatcher, boolean recurseSubdirs, boolean collectDirs,
            ILameCollector<String> fileNameCollector) {
        Set<String> names = np.getNames(basePath);

        for (String name : names) {
            boolean isSD = (recurseSubdirs || !collectDirs) && np.isDirectory(name);

            if (optNameMatcher != null && optNameMatcher.project(name)
                    || optNameMatcher == null && (collectDirs || !isSD)) {
                fileNameCollector.add(name);
            }
            if (recurseSubdirs && isSD) {
                listResourcesEx(np, name, optNameMatcher, recurseSubdirs, collectDirs, fileNameCollector);
            }
        }
    }

    // "http:" --> "http:"
    // null  --> null
    // "http://" --> "" (too short)
    // "http://ala" --> "ala"
    // "https://ala" --> "ala"
    // "abcd:xyz" --> "z" (malformed)
    public static String dropHttpProtocol(String urlWithProto) {
        if (strLengthFix(urlWithProto) < 7) {
            return urlWithProto;
        }
        char c = urlWithProto.charAt(4);
        if (c == ':') {
            // 01234567
            // http://a
            return urlWithProto.substring(7);
        } else {
            // 012345678
            // https://a
            return urlWithProto.substring(8);
        }
    }

    public static String getDomainFromGlobalUrl(String url) {
        Pair<String, String> p = splitGlobalUrl(url);
        if (p == null) {
            return null;
        }
        return dropHttpProtocol(p.v1);
    }

    // url cannot be null!
    public static boolean isUrlGlobal(String url) {
        return url.length() >= 10 && url.charAt(6) == '/' && url.startsWith("http")
                && (url.charAt(4) == ':' || url.charAt(5) == ':');
    }

    // null and empty uri is local
    public static boolean isUriGlobal(String uri) {
        if (uri == null) {
            return false;
        }
        int uriLen = uri.length();
        int pos = 0;
        char c = '/';
        while (pos < uriLen) {
            c = uri.charAt(pos);

            if (c == ':' || c == '/') {
                break;
            }

            if (!(c == '+' || c == '.' || c == '-' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9' || c >= 'a' && c <= 'z')) {
                break;
            }

            pos++;
        }

        return c == ':';
    }

    // null                            -> null
    // "http[s]://ala.ma.kota.pl/xyz..." -> <"http[s]://ala.ma.kota.pl", "/xyz...">
    // "http[s]://ala.ma.kota.pl"        -> <"http[s]://ala.ma.kota.pl", "">
    // "/xyz..."                       -> <"", "/xyz...">
    public static Pair<String, String> splitGlobalUrl(String url) {
        if (url == null) {
            return null;
        }

        boolean isGlobal = isUrlGlobal(url);

        if (isGlobal) {
            char c = url.charAt(4);
            int offset = 7;
            if (c == ':') { //  "http://..."
                isGlobal = url.charAt(5) == '/';
            } else if (c == 's') { //  "https://..."
                isGlobal = url.charAt(5) == ':' && url.charAt(7) == '/';
                offset = 8;
            } else {
                isGlobal = false;
            }

            if (isGlobal) {
                int pos = url.indexOf('/', offset);
                if (pos >= 0) {
                    return new Pair<String, String>(url.substring(0, pos), url.substring(pos));
                }
                return new Pair<String, String>(url, "");
            }
        }

        return new Pair<String, String>("", url);
    }

    public static String replicateStr(String str, int repCnt) {
        if (repCnt == 0) {
            return "";
        }
        if (repCnt == 1) {
            return str;
        }

        StringBuilder holding = new StringBuilder(str.length() * repCnt);
        for (int a = 0; a < repCnt; a++) {
            holding.append(str);
        }
        return holding.toString();
    }

    public static <T, T1 extends T, T2 extends T> boolean equalByElements(Collection<T1> forIteration,
            Collection<T2> forContains) {
        int fiSize = safeGetCollectionSize(forIteration);
        if (fiSize != safeGetCollectionSize(forContains)) {
            return false;
        }
        if (fiSize == 0) {
            return true;
        }

        for (T1 item : forIteration) {
            if (!forContains.contains(item)) {
                return false;
            }
        }
        return true;
    }

    public static <T> String escapeAndMerge(Iterable<T> it, String sep) {
        if (it == null) {
            return null;
        }

        String twoSeps = sep + sep;
        StringBuilder sb = new StringBuilder();
        boolean first = true;

        for (T item : it) {
            String s = safeToStringDef(item, "");
            s = s.replace(sep, twoSeps);

            if (first) {
                first = false;
            } else {
                sb.append(sep);
            }

            sb.append(s);
        }
        return sb.toString();
    }

    public static List<String> splitAndUnescape(String merged, String sep) {
        if (merged == null) {
            return null;
        }

        List<String> res = new ArrayList<String>();

        int pos = 0;
        int len = merged.length();
        int sepLen = sep.length();
        String twoSep = sep + sep;

        while (pos < len) {
            int currPos = pos - sepLen;

            do {
                currPos = merged.indexOf(sep, currPos + sepLen);
                if (currPos == -1) {
                    break;
                }
                currPos += sepLen;
            } while (merged.startsWith(sep, currPos));

            if (currPos == -1) {
                break;
            }

            res.add(merged.substring(pos, currPos - sepLen).replace(twoSep, sep));
            pos = currPos;
        }

        res.add(merged.substring(pos).replace(twoSep, sep));
        return res;
    }

    @SuppressWarnings("unchecked")
    public static <K, IK, IV, IM extends Map<IK, IV>, M extends Map<K, IM>> void putInMapOfMap(M map, K k, IK ik, IV iv) {
        IM im = map.get(k);
        if (im == null) {
            im = (IM) new HashMap();
            map.put(k, im);
        }
        im.put(ik, iv);
    }

    public static String fixIdentName(String ident, String badCharSubst) {
        return fixIdentName(ident, badCharSubst, true);
    }

    public static String fixIdentName(String ident, String badCharSubst, boolean lowerCaseIdent) {
        if (isStrEmpty(ident)) {
            return ident;
        }
        if (isProperIdent(ident)) {
            return ident;
        }

        StringBuilder sb = new StringBuilder();

        char c0 = ident.charAt(0);
        char c = Character.toLowerCase(c0);

        if (!(c == '_' || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))) {
            sb.append(badCharSubst);
        } else {
            sb.append(lowerCaseIdent ? c : c0);
        }

        for (int i = 1; i < ident.length(); i++) {
            c0 = ident.charAt(i);
            c = Character.toLowerCase(c0);
            if (!(c == '_' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9')) {
                sb.append(badCharSubst);
            } else {
                sb.append(lowerCaseIdent ? c : c0);
            }
        }

        return sb.toString();
    }

    public static int countSubstrings(String inStr, String subStr) {
        if (isStrEmpty(inStr) || isStrEmpty(subStr)) {
            return 0;
        }
        int res = 0;
        int pos = 0;
        int inStrLen = inStr.length();
        int subStrLen = subStr.length();
        while (pos < inStrLen) {
            int newPos = inStr.indexOf(subStr, pos);
            if (newPos < 0) {
                break; //------- break !!! --------
            }
            res++;
            pos = newPos + subStrLen;
        }
        return res;
    }

    public static String replaceEnclosedParts(String startStr, String endStr,
            String inText, String replacement) {
        if (inText == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        int pos = 0;
        while (pos >= 0 && pos < inText.length()) {
            int posCommStart = inText.indexOf(startStr, pos);

            if (posCommStart < 0) {
                break;
            }

            int posCommEnd = inText.indexOf(endStr, posCommStart + startStr.length());

            if (posCommEnd < 0) {
                break;
            }

            sb.append(inText.substring(pos, posCommStart));

            if (replacement != null) {
                sb.append(replacement);
            }

            pos = posCommEnd + endStr.length();
        }

        sb.append(inText.substring(pos));

        return sb.toString();
    }

    public static <T> T safeArrayGet(T[] arr, int idx) {
        return (idx >= 0 && idx < arrLengthFix(arr)) ? arr[idx] : null;
    }

    public static <T> void removeManyFromList(List<T> items,
            IPredicate<? super T> removeOne) {
        if (items instanceof RandomAccess) {
            // fast for random access list
            removeManyFromArrayList(items, removeOne, false);
        } else {
            // sequential variant
            for (Iterator<T> iter = items.iterator(); iter.hasNext();) {
                T item = iter.next();
                if (removeOne.project(item)) {
                    iter.remove();
                }
            }
        }
    }

    public static <T> void removeManyFromList(List<T> items,
            final Set<T> itemsToBeRemoved, boolean trimToSize) {
        if (itemsToBeRemoved == null) {
            return;
        }

        removeManyFromList(items, new IPredicate<T>() {
            public Boolean project(T val) {
                return itemsToBeRemoved.contains(val);
            }
        });
    }

    public static <T> void removeManyFromArrayList(List<T> items,
            IPredicate<? super T> removeOne, boolean trimToSize) {
        if (items == null || removeOne == null) {
            return;
        }

        int j = 0; // destination idx
        for (int i = 0; i < items.size(); i++) {
            T item = items.get(i);
            if (removeOne.project(item)) {
                // no-op
            } else {
                if (j < i) {
                    items.set(j, item);
                }
                j++;
            }
        }

        for (int k = items.size() - 1; k >= j; k--) {
            items.remove(k);
        }

        if (trimToSize) {
            if (items instanceof ArrayList) {
                ArrayList ar = (ArrayList) items;
                ar.trimToSize();
            } else {
                throw new LameRuntimeException("list is not arraylist (it is: "
                        + getPrettyClassName(items)
                        + "), cannot trim to size!");
            }
        }
    }

    public static <T> void removeManyFromArrayList(List<T> items,
            final Set<T> itemsToBeRemoved, boolean trimToSize) {
        if (itemsToBeRemoved == null) {
            return;
        }

        removeManyFromArrayList(items, new IPredicate<T>() {
            public Boolean project(T val) {
                return itemsToBeRemoved.contains(val);
            }
        }, trimToSize);
    }

    public static <T> List<T> arrayToArrayList(T... a) {
        if (a == null) {
            return null;
        }
        List<T> res = new ArrayList<T>(a.length);
        res.addAll(Arrays.asList(a));

        return res;
    }

    public static int booleanToInt(Boolean b) {
        return booleanToInt(b, 1, 0);
    }

    public static int booleanToInt(Boolean b, int valForTrue, int valForFalse) {
        return booleanToInteger(b, valForTrue, valForFalse, valForFalse);
    }

    public static Integer booleanToInteger(Boolean b, Integer valForTrue, Integer valForFalse, Integer valForNull) {
        if (b == null) {
            return valForNull;
        }
        return b ? valForTrue : valForFalse;
    }

    //returns true if an element was removed as a result of this call
    public static <T> boolean safeCollectionRemove(Collection<? super T> c, T item) {
        if (c != null) {
            return c.remove(item);
        }
        return false;
    }

    public static boolean iterableIsEmpty(Iterable<?> iterable) {
        Iterator it = iterable.iterator();
        return !it.hasNext();
    }

    @SuppressWarnings("unchecked")
    public static <V, C extends Collection<V>> C projectIMToColl(
            Iterable<Map<String, Object>> src, C coll,
            String optColName, boolean dropNull) {
        Projector projector = optColName == null ? anyMapValProjector
                : new MapProjector(optColName);
        projectIntoCollection(src, coll, projector, dropNull);
        return coll;
    }

    public static <V> Set<V> projectIMToSet(Iterable<Map<String, Object>> src,
            String optColName, boolean dropNull) {
        return projectIMToColl(src, new LinkedHashSet<V>(), optColName, dropNull);
    }

    public static <V> List<V> projectIMToList(Iterable<Map<String, Object>> src,
            String optColName, boolean dropNull) {
        return projectIMToColl(src, new ArrayList<V>(), optColName, dropNull);
    }

    public static <V> int safeIterableIndexOf(Iterable<V> it, V val) {

        if (it == null) {
            return -1;
        }

        int i = 0;
        for (V item : it) {
            if (BaseUtils.safeEquals(item, val)) {
                return i;
            }
            i++;
        }

        return -1;
    }

    public static <V> boolean safeIterableContains(Iterable<V> it, V val) {

        if (it instanceof Set) {
            Set s = (Set) it;
            return s.contains(val);
        }

        return safeIterableIndexOf(it, val) != -1;
    }

    public static <T> boolean setAddOrRemoveItem(Set<T> set, T item, boolean doAdd) {
        if (doAdd) {
            return set.add(item);
        } else {
            return set.remove(item);
        }
    }

    public static boolean isHTMLTextEmpty(String text) {
        return BaseUtils.isStrEmptyOrWhiteSpace(text) || text.trim().equals("<br>");
    }

    public static Set<String> parseStringAndGetSetOfParams(String text, String startParam, String endParam) {
        int pos = 0;
        Set<String> stringSet = new LinkedHashSet<String>();
        while (pos != -1) {
            int indexOf = text.indexOf(startParam, pos);
            if (indexOf != -1) {
                int startIndex = indexOf + startParam.length();
                int endIndex = text.indexOf(endParam, startIndex);
                if (endIndex != -1) {
                    String substring = new String(text.substring(startIndex, endIndex));
                    stringSet.add(substring);
                    pos = endIndex + endParam.length();
                } else {
                    pos = -1;
                }
            } else {
                pos = -1;
            }
        }
        return stringSet;
    }

    public static boolean startsWithAny(String what, Iterable<String> prefixes) {
        for (String prefix : prefixes) {
            if (what.startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }

//    // (szybsza od niej jest getUncommonItems)
//    public static <T> Collection<T> getUnCommonItemsTF(Collection<T> col1, Collection<T> col2, Collection<T> dstCol) {
//        if(dstCol == null) {
//           dstCol = new ArrayList<T>();
//        }
//        Set<T> set1 = new HashSet<T>();
//        Set<T> commonSet = new HashSet<T>();
//        set1.addAll(col1);
//        for (T t : col2) {
//            if(set1.contains(t)) {
//                commonSet.add(t);
//            } else {
//                dstCol.add(t);
//            }
//        }
//        for (T t : col1) {
//            if(!commonSet.contains(t)) {
//                dstCol.add(t);
//            }
//        }
//        return dstCol;
//    }
//    // używa LameBaga ale nieco wolna (szybsza od niej jest getUncommonItems)
//    public static <T> Collection<T> getUncommonItemsPM1(Collection<T> coll1, Collection<T> coll2, Collection<T> dstColl) {
//        LameBag<T> bag = new LameBag<T>();
//        bag.addAll(coll1);
//        bag.addAll(coll2);
//
//        for (T item : bag) {
//            if (bag.getCount(item) == 1) {
//                dstColl.add(item);
//            }
//        }
//
//        return dstColl;
//    }
    // nie może być duplikatów na kolekcji coll2!!!
    public static <T, C extends Collection<T>> C getUncommonItems(Collection<T> coll1, Collection<T> coll2, C dstColl) {
        Set<T> s = new HashSet<T>(coll1);

        for (T item : coll2) {
            if (!s.remove(item)) {
                s.add(item);
            }
        }

        dstColl.addAll(s);

        return dstColl;
    }
//    public static void main(String[] args) {
//        Set<String> parseStringAndGetSetOfParams = parseStringAndGetSetOfParams("askjd als dnkas knasdkjn ${dasd as} dasd as${} das ${s}", "${", "}");
//        System.out.println("lista " + parseStringAndGetSetOfParams);
//
//        System.out.println("lista " + parseStringAndGetSetOfParams(
//                "askjd als dnkas knasdkjn |dasd as| dasd as|| das |s|", "|", "|"));
//
//        System.out.println("lista " + parseStringAndGetSetOfParams(
//                "askjd als dnkas knasdkjn $|dasd as| dasd as$|| das $|s|", "$|", "|"));
//
//        System.out.println("lista " + parseStringAndGetSetOfParams(
//                "askjd als dnkas knasdkjn |dasd as|$ dasd as||$ das |s|$", "|", "|$"));
//        Set s1 = new HashSet<String>();
//        Set s2 = new LinkedHashSet<String>();
//        s1.add("krowa");
//        s1.add("pies");
//        s1.add("kot");
//        s2.add("kot");
//        s2.add("pies");
//        s2.add("krowa");
//        if (s1.equals(s2)) {
//            System.out.println("equal");
//        } else {
//            System.out.println("not equal");
//        }
//    }
//    public static void main(String[] args) {
//        int size = 1500000;
//        Random r = new Random();
//
//        Set<Integer> lst1 = new HashSet<Integer>();
//        Set<Integer> lst2 = new HashSet<Integer>();
//
//        for (int i = 0; i < size; i++) {
//            int rnd1 = r.nextInt(size * 2);
//            int rnd2 = r.nextInt(size * 2);
//            lst1.add(rnd1);
//            lst2.add(rnd2);
//        }
//
//        //System.out.println("lst1: " + lst1);
//        //System.out.println("lst2: " + lst2);
//        System.out.println(SimpleDateUtils.toCanonicalString(new Date(), true) + ": start");
//        int uncCntTF = getUnCommonItemsTF(lst1, lst2, new ArrayList<Integer>()).size();
//        System.out.println(SimpleDateUtils.toCanonicalString(new Date(), true) + ": done TF, cnt=" + uncCntTF);
//        int uncCntPM1 = getUncommonItems(lst1, lst2, new ArrayList<Integer>()).size();
//        System.out.println(SimpleDateUtils.toCanonicalString(new Date(), true) + ": done PM1, cnt=" + uncCntPM1);
//        int uncCntPM2 = getUncommonItems2(lst1, lst2, new ArrayList<Integer>()).size();
//        System.out.println(SimpleDateUtils.toCanonicalString(new Date(), true) + ": done PM2, cnt=" + uncCntPM2);
//    }

    public static <I extends Number> String formatMapOfIntNumbers(Map<String, I> m, String keyValSep, String itemSep, String optAfterPadKeyRightSep,
            boolean padVals, boolean useThousandSep) {
        int maxKeyLen = 0;
        int maxValLen = 0; // będzie 0 gdy padVals == false
        final String THOUSAND_SEP = useThousandSep ? " " : null;

        if (optAfterPadKeyRightSep != null || padVals) {

            for (Entry<String, I> e : m.entrySet()) {
                int keyLen = strLengthFix(e.getKey());
                Number val = e.getValue();
                String valStr = padIntEx(val, 0, ' ', THOUSAND_SEP);
//                String valStr = val == null ? "" : Long.toString(val.longValue());
//
//                if (useThousandSep) {
//                    valStr = chunkStringWithSep(valStr, 3, " ", true);
//                }

                int valLen = valStr.length();

                if (keyLen > maxKeyLen) {
                    maxKeyLen = keyLen;
                }

                if (padVals && valLen > maxValLen) {
                    maxValLen = valLen;
                }
            }
        }

        StringBuilder sb = new StringBuilder();

        boolean isFirst = true;
        int keyValSepLen = keyValSep.length();

        for (Entry<String, I> e : m.entrySet()) {

            if (isFirst) {
                isFirst = false;
            } else {
                sb.append(itemSep);
            }

            String k = e.getKey() + keyValSep;

            if (optAfterPadKeyRightSep != null) {
                k = padStr(k, ' ', maxKeyLen + keyValSepLen, false);
            }

            sb.append(k);
            if (optAfterPadKeyRightSep != null) {
                sb.append(optAfterPadKeyRightSep);
            }

            Number val = e.getValue();
            String valStr = padIntEx(val, maxValLen, ' ', THOUSAND_SEP);

//            String valStr = val == null ? "" : Long.toString(val.longValue());
//
//            if (useThousandSep) {
//                valStr = chunkStringWithSep(valStr, 3, " ", true);
//            }
//            if (padVals) {
//                valStr = padStr(valStr, ' ', maxValLen, true);
//            }
            sb.append(valStr);
        }

        return sb.toString();
    }

    public static <T> T safeGetFirstItem(Iterable<T> iterable, T defVal) {
        if (iterable == null) {
            return defVal;
        }

        Iterator<T> iter = iterable.iterator();
        return iter.hasNext() ? iter.next() : defVal;
    }

    // dstXXX < 0 -> -dstXXX means max dst XXX (width or height)
    // dstXXX == 0 -> proportional on XXX (width or height)
    // returns: null -> no scaling needed
    public static Pair<Double, Double> getScaleFactors(int srcWidth, int srcHeight,
            int dstWidth, int dstHeight, boolean noNullRes) {
        if (dstWidth == 0 && dstHeight == 0) {
            return noNullRes ? new Pair<Double, Double>(1.0, 1.0) : null;
        }

        //int properDstWidth = Math.abs(dstWidth), properDstHeight = Math.abs(dstHeight);
        //Pair<Double, Double> res = new Pair<Double, Double>(1.0, 1.0);
        if (dstWidth > 0 && dstHeight > 0) {
            return new Pair<Double, Double>((double) dstWidth / srcWidth, (double) dstHeight / srcHeight);
        }

        if (dstWidth >= 0 && dstHeight >= 0) {
            double factor = dstWidth == 0 ? (double) dstHeight / srcHeight : (double) dstWidth / srcWidth;
            return new Pair<Double, Double>(factor, factor);
        }

        boolean anyPositive = dstWidth > 0 || dstHeight > 0;
        //int oldDstWidth = dstWidth, oldDstHeight = dstHeight;

        if (dstWidth < 0) {
            dstWidth = -dstWidth;
            if (srcWidth < dstWidth) {
                dstWidth = srcWidth;
            }
        }
        if (dstHeight < 0) {
            dstHeight = -dstHeight;
            if (srcHeight < dstHeight) {
                dstHeight = srcHeight;
            }
        }

        if (anyPositive) {
            return new Pair<Double, Double>((double) dstWidth / srcWidth, (double) dstHeight / srcHeight);
        }

        if (dstWidth == 0 || dstHeight == 0) {
            double factor = dstWidth == 0 ? (double) dstHeight / srcHeight : (double) dstWidth / srcWidth;
            return new Pair<Double, Double>(factor, factor);
        }

        // both oldDstXXX are negative
        //int maxWidth = dstWidth;
        //int maxHeight = dstHeight;
        double factor = (double) dstWidth / srcWidth;
        if (srcHeight * factor > dstHeight) {
            factor = (double) dstHeight / srcHeight;
        }
        return new Pair<Double, Double>(factor, factor);
    }

    public static Pair<Integer, Integer> getScaledDims(int srcWidth, int srcHeight,
            int dstWidth, int dstHeight) {
        Pair<Double, Double> factors = getScaleFactors(srcWidth, srcHeight, dstWidth, dstHeight, true);
        return new Pair<Integer, Integer>((int) (srcWidth * factors.v1), (int) (srcHeight * factors.v2));
    }

    public static String emptyStringOrWhiteSpaceToDef(String s, String def) {
        return isStrEmptyOrWhiteSpace(s) ? def : s;
    }

    public static String emptyStringOrWhiteSpaceToNull(String s) {
        return emptyStringOrWhiteSpaceToDef(s, null);
    }
    protected static final Map<Class, Class> primitiveToBoxedClassMap = new HashMap<Class, Class>() {
        {
            put(int.class, Integer.class);
            put(double.class, Double.class);
            put(boolean.class, Boolean.class);
            put(long.class, Long.class);
            put(byte.class, Byte.class);
            put(char.class, Character.class);
            put(short.class, Short.class);
            put(float.class, Float.class);
            put(void.class, Void.class);
        }
    };
    protected static final Map<String, Class> primitiveNameToPrimitiveClassMap = new HashMap<String, Class>();

    static {
        for (Class c : primitiveToBoxedClassMap.keySet()) {
            primitiveNameToPrimitiveClassMap.put(c.getName(), c);
        }
    }

    public static Class findPrimitiveClassByName(String name) {
        return primitiveNameToPrimitiveClassMap.get(name);
    }

    public static Class getEnvelopeForPrimitiveClass(Class prim) {
        return primitiveToBoxedClassMap.get(prim);
    }

    public static Class fixPrimitiveClassToEnvelope(Class c) {
        Class ret = getEnvelopeForPrimitiveClass(c);
        return nullToDef(ret, c);
    }

    public static <T, T2 extends T, C extends Collection<T>> C addMany(C coll, T2... items) {
        coll.addAll(Arrays.asList(items));
        return coll;
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> addManyLists(Collection<T>... cols) {
        List<T> res = null;
        for (Collection<T> col : cols) {
            if (col != null) {
                if (res == null) {
                    res = new ArrayList<T>();
                }
                res.addAll(col);
            }
        }
        return res;
    }

    public static <T> List<T> arrayToList(T... a) {
        return Arrays.asList(a);
    }

    public static <K, V> void putNewKeysInMap(Map<K, V> mDest, Map<K, V> mNewKeys) {
        for (Entry<K, V> e : mNewKeys.entrySet()) {
            K key = e.getKey();
            if (mDest.get(key) == null) {
                mDest.put(key, e.getValue());
            }
        }
    }

    public static <K, V> List<V> addToCollectingMapWithList(Map<K, List<V>> map, K k, V v) {
        List<V> list = map.get(k);
        if (list == null) {
            list = new ArrayList<V>();
            map.put(k, list);
        }
        list.add(v);
        return list;
    }

    public static <K, V> void addToCollectingMapWithLinkedSet(Map<K, Set<V>> map, K k, V v) {
        Set<V> list = map.get(k);
        if (list == null) {
            list = new LinkedHashSet<V>();
            map.put(k, list);
        }
        list.add(v);
    }

    //ww: gives null class for null object
    public static Class[] objectsToClasses(Object... objs) {
        int len = objs.length;
        Class[] cls = new Class[len];
        for (int i = 0; i < len; i++) {
            Object obj = objs[i];
            cls[i] = obj != null ? obj.getClass() : null;
        }
        return cls;
    }

    public static List<Class> objectsToClassList(Object... objs) {
        return Arrays.asList(objectsToClasses(objs));
    }

    public static String lowerCaseFirstChar(String s) {
        return StrUtils.capitalize(s);
    }

    public static int minOfParams(int... args) {
        int len = args.length;
        if (len == 0) {
            return 0;
        }
        int res = args[0];
        for (int i = 1; i < len; i++) {
            int val = args[i];
            if (val < res) {
                res = val;
            }
        }
        return res;
    }
    //ww: dobre to wyznaczania pierwszej pozycji w stringu, gdy wcześniej szukamy
    // kilku separatorów opcjonalnych (wtedy nieznalezione są często jako -1)

    public static int minNonNegativeOfParams(int... args) {
        int len = args.length;
        int res = -1;
        for (int i = 0; i < len; i++) {
            int val = args[i];
            if (val >= 0 && (res < 0 || val < res)) {
                res = val;
            }
        }
        return res;
    }

    //ww: zakładamy loadFactor = HashMap.DEFAULT_LOAD_FACTOR = 0.75f;
    public static int calcHashMapCapacity(int itemCnt) {
        return itemCnt * 4 / 3 + 1;
    }
    protected static Projector<Pair, Object> pairV1Projector = new Projector<Pair, Object>() {
        public Object project(Pair val) {
            return val.v1;
        }
    };
    protected static Projector<Pair, Object> pairV2Projector = new Projector<Pair, Object>() {
        public Object project(Pair val) {
            return val.v2;
        }
    };

    @SuppressWarnings("unchecked")
    public static <V1, V2> Projector<Pair<V1, V2>, V1> getPairV1Projector() {
        return (Projector) pairV1Projector;
    }

    @SuppressWarnings("unchecked")
    public static <V1, V2> Projector<Pair<V1, V2>, V2> getPairV2Projector() {
        return (Projector) pairV2Projector;
    }

    public static <V1, V2> V1 safeGetV1OfPair(Pair<V1, V2> p, V1 defForNullPair, V1 defForNullVal) {
        if (p == null) {
            return defForNullPair;
        }
        return nullToDef(p.v1, defForNullVal);
    }

    public static <V1, V2> V1 safeGetV1OfPair(Pair<V1, V2> p, V1 defForNull) {
        return safeGetV1OfPair(p, defForNull, defForNull);
    }

    public static <V1, V2> V1 safeGetV1OfPair(Pair<V1, V2> p) {
        return safeGetV1OfPair(p, null, null);
    }

    public static <V1, V2> V2 safeGetV2OfPair(Pair<V1, V2> p, V2 defForNullPair, V2 defForNullVal) {
        if (p == null) {
            return defForNullPair;
        }
        return nullToDef(p.v2, defForNullVal);
    }

    public static <V1, V2> V2 safeGetV2OfPair(Pair<V1, V2> p, V2 defForNull) {
        return safeGetV2OfPair(p, defForNull, defForNull);
    }

    public static <V1, V2> V2 safeGetV2OfPair(Pair<V1, V2> p) {
        return safeGetV2OfPair(p, null, null);
    }

    public static Set<String> toLowerCaseIterableToSet(Iterable<String> items, boolean dropNull) {
        return BaseUtils.projectToSet(items, new Projector<String, String>() {
            public String project(String val) {
                return BaseUtils.safeToLowerCase(val);
            }
        }, dropNull);
    }
    private static final String BIG_NUM_SUFFIXES = "kmgtp";
    private static final long big_num_powers[] = {
        1000L, // k
        1000L * 1000, // m
        1000L * 1000 * 1000, // g
        1000L * 1000 * 1000 * 1000, // t
        1000L * 1000 * 1000 * 1000 * 1000 // p
    };

    public static <N extends Number> String formatBigNum(N num) {
        return formatBigNum(num, false);
    }

    public static <N extends Number> String formatBigNum(N num, boolean reduceFracPart) {
        if (num == null || num.longValue() < big_num_powers[0]) {
            return BaseUtils.safeToString(num);
        }
        final double doubleValue = num.doubleValue();

        int powIdx = (int) (Math.log10(doubleValue) / 3) - 1;

        if (powIdx >= BIG_NUM_SUFFIXES.length()) {
            powIdx = BIG_NUM_SUFFIXES.length() - 1;
        }

        long base = big_num_powers[powIdx];

        double res = doubleValue / base;

        int prec;
        if (res < 10) {
            prec = 2;
        } else if (res < 100) {
            prec = 1;
        } else {
            prec = 0;
        }

        return BaseUtils.doubleToString(res, prec, reduceFracPart, true) + BIG_NUM_SUFFIXES.charAt(powIdx);
    }

    public static boolean strHasWhiteSpaces(String s) {
        if (isStrEmpty(s)) {
            return false;
        }

        int len = s.length();
        for (int i = 0; i < len; i++) {
            if (s.charAt(i) <= ' ') {
                return true;
            }
        }

        return false;
    }

    //ww: false dla nulla
    //ww: adres nie może zawierać białych znaków - na początku i na końcu
    // też nie!
    public static boolean isEmailValid(String email) {
        if (isStrEmptyOrWhiteSpace(email)) {
            return false;
        }

//        email = email.trim();
        if (strHasWhiteSpaces(email)) {
            return false;
        }

        Pair<String, String> p = BaseUtils.splitString(email, "@");
        if (BaseUtils.isStrEmptyOrWhiteSpace(p.v1)) {
            return false;
        }
        if (BaseUtils.isStrEmptyOrWhiteSpace(p.v2)) {
            return false;
        }
        if (p.v2.indexOf(".") == -1
                || p.v2.indexOf('@') >= 0) {
            return false;
        }

        return true;
    }

    /**
     *
     * @param safeColName bezpieczna nazwa columny bazy - tzn ze znakami "
     * (Double quotes)
     * @param safeColValue bezpieczna wartość kolumny, tzn już odpowiednio
     * wyciapkowanymi elementami, częsciowo zalezne od dialektu sql-a
     * @return jeśli safeColValue jest null to zwraca warunek jako "is null",
     * inaczej jako =safeColValue
     */
    public static String safeEqualsConditionInSqlWhere(String safeColName, String safeColValue) {
        if (safeColValue == null) {
            return safeColName + " is null";
        } else {
            return safeColName + "=" + safeColValue;
        }
    }

    // eofAsLineBreak == true  -> zwróci poz. za stringiem jak nie znajdzie
    // eofAsLineBreak == false -> zwróci -1 jak nie znajdzie
    public static int findFirstLineBreak(CharSequence content, int pos, boolean eofAsLineBreak) {
        // dla jasności: CR[LF]|LF
        //String lineBreakRegExp = "\\r?\\n|\\r";
        int len = content.length();
        while (pos < len) {
            char c = content.charAt(pos);

            if (c == '\r' || c == '\n') {
                break;
            }

            pos++;
        }

        // albo znaleziony CR lub LF, albo koniec stringa (też możliwe)
        return eofAsLineBreak || pos < len ? pos : -1;
    }

    // poprawnie zjada koniec linii CR[LF]|LF
    // gdy nie stoimy na końcu linii - tzn. znak na pozycji pos nie jest
    // CR lub LF (ew. koniec stringa) to rzuci błędem
    public static int eatLineBreak(CharSequence content, int pos) {
        int len = content.length();
        // przypadek szczególny - komentarz liniowy do końca stringa, brak końca linii
        if (pos == len) {
            return pos;
        }

        char c = content.charAt(pos);

        if (c != '\r' && c != '\n') {
            throw new LameRuntimeException("char at pos: " + pos + " is not CR|LF, char: \'" + c + "\'");
        }

        // LF albo CR bez LF za nim - jeden znak nowej linii jest tylko
        if (c == '\n' || pos + 1 < len && content.charAt(pos + 1) != '\n') {
            return pos + 1;
        }
        // było CRLF
        return pos + 2;
    }

    public static int findPosAfterFirstLineBreak(CharSequence content, int pos, boolean eofAsLineBreak) {
        return eatLineBreak(content, findFirstLineBreak(content, pos, eofAsLineBreak));
    }
    public static final char[] hexDigits = "0123456789ABCDEF".toCharArray();

    public static String byteToHex(int byteVal) {
//        StringBuilder sb = new StringBuilder(2);
//        appendByteAsHex(sb, byteVal);
//        return sb.toString();
        return appendByteAsHex(new StringBuilder(2), byteVal).toString();
    }

    public static StringBuilder appendByteAsHex(StringBuilder sb, int byteVal) {
        return sb.append(hexDigits[(byteVal >> 4) & 0xf]).append(hexDigits[byteVal & 0xf]);
    }

    public static StringBuilder appendBytesAsHex(StringBuilder sb, byte[] b, int startPos, int endPos) {
        int len = b.length;
        if (endPos > len) {
            endPos = len;
        }

        for (; startPos < endPos; startPos++) {
            appendByteAsHex(sb, b[startPos]);
        }

        return sb;
    }

    @SuppressWarnings("unchecked")
    public static Map projectIterOfMapToMapVals(Iterator<Map<?, ?>> iter, Object keyKey, Object valKey) {
        Map res = new LinkedHashMap();

        while (iter.hasNext()) {
            Map m = iter.next();
            Object keyVal = m.get(keyKey);
            Object valVal = m.get(valKey);
            res.put(keyVal, valVal);
        }

        return res;
    }

    public static Map projectCollOfMapToMapVals(Iterable<Map<?, ?>> coll, Object keyKey, Object valKey) {
        return projectIterOfMapToMapVals(coll.iterator(), keyKey, valKey);
    }

    @SuppressWarnings("unchecked")
    public static <K, V> Map projectCollOfMapToMapVals2(Iterable<Map<K, V>> coll, K keyKey, K valKey) {
        return projectCollOfMapToMapVals((Iterable) coll, keyKey, valKey);
    }

    protected static Map<String, Object> beanPropsToMapDeepInner(Map<String, Object> res, String prfx, INamedTypedPropsBean bean,
            IWrapperCreator<Object, INamedTypedPropsBean> wc) {
        Set<String> propNames = bean.getPropNames();
        for (String propName : propNames) {
            Object v = bean.getPropValue(propName);
            boolean isSimple = JsonConverter.isInstanceOfSimpleClass(v);
            if (!isSimple) {
                try {
                    INamedTypedPropsBean subBean = wc.create(v);
                    beanPropsToMapDeep(res, prfx + propName + ".", subBean, wc);
                } catch (Exception ex) {
                    isSimple = true;
                }
            }
            if (isSimple) {
                res.put(prfx + propName, v);
            }
        }
        return res;
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> beanPropsToMapDeep(Map<String, Object> res, String prfx, INamedTypedPropsBean bean,
            IWrapperCreator<?, INamedTypedPropsBean> wc) {
        return beanPropsToMapDeepInner(res, prfx, bean, (IWrapperCreator) wc);
    }

    public static Map<String, Object> beanPropsToMapDeep(INamedTypedPropsBean bean,
            IWrapperCreator<?, INamedTypedPropsBean> wc) {
        return beanPropsToMapDeep(new LinkedHashMap<String, Object>(), "", bean, wc);
    }

    public static String rightSubstring(String s, int charCnt) {
        if (s == null) {
            return s;
        }

        int sLen = s.length();
        int sPos = sLen - charCnt;
        if (sPos < 0) {
            sPos = 0;
        }
        return s.substring(sPos, sLen);
    }

    public static String ensureStrHasSuffix(String s, String suffix, boolean ignoreCase) {

        if (strHasSuffix(s, suffix, ignoreCase)) {
            return s;
        }

        return s + suffix;
    }

    // toCanonicalBigDecimal(String numVal, Double defVal).toString() będzie
    // postaci [-]?[cyfra]+[.][cyfra]+
    // np. dla 10 da 10.0, dla 10.00 da 10.0, dla 10.0 da 10.0
    // dla 10.05 da 10.05 dla 10.0500 da 10.05
    public static BigDecimal toCanonicalBigDecimal(String numVal, Double defVal) {
        BigDecimal bd;
        try {
            bd = new BigDecimal(numVal);
        } catch (Exception ex) {
            bd = defVal != null ? new BigDecimal(defVal) : null;
        }
        if (bd == null) {
            return null;
        }

        String asPlainStr = bd.toPlainString();

        if (asPlainStr.contains(".")) {
            int pos = asPlainStr.length() - 1;
            while (pos > 0 && asPlainStr.charAt(pos) == '0') {
                pos--;
            }

            asPlainStr = asPlainStr.substring(0, pos + 1);
            if (asPlainStr.charAt(asPlainStr.length() - 1) == '.') {
                asPlainStr = asPlainStr + "0";
            }
        } else {
            asPlainStr = asPlainStr + ".0";
        }

//        System.out.println("asPlainStr=" + asPlainStr);
        bd = new BigDecimal(asPlainStr);

        return bd;
    }

    public static BigDecimal toCanonicalBigDecimal(String numVal) {
        return toCanonicalBigDecimal(numVal, null);
    }

    public static boolean isAnyStringEmptyOrWhiteSpace(String... strings) {
        for (String string : strings) {
            if (BaseUtils.isStrEmptyOrWhiteSpace(string)) {
                return true;
            }
        }
        return false;
    }

    public static int parsePosition(String positionString) {
        int i = 0;
        int len = positionString.length();

        //szukamy pierwszego znaku nie cyfry i nie minusa
        // może takie nie być wcale
        while (i < len) {
            char c = positionString.charAt(i);
            if (c != '-' && (c < '0' || c > '9')) {
                break;
            }
            i++;
        }
        // obcinamy do samych poprawnych znaków cyfrowych
        // lub weźmie całość jak wszystkie poprawne
        return BaseUtils.tryParseInt(positionString.substring(0, i));
    }

    /**
     * Metoda sprawdza czy pierwsza argument "subversionCandidate" jest wersją
     * podrzędną drugiego argumentu: "baseVersion". Jeśli wersje są takie same,
     * metoda zwróci wartość true.
     *
     * @param subversionCandidate kandydat na subwersję
     * @param baseVersion wersja bazowa
     * @return true - wersje są równe lub pierwszy argument jest subwersją
     * drugiego argumentu false - w przeciwnym przypadku
     */
    public static boolean isSubVersionOfVersion(String subversionCandidate, String baseVersion) {
        if (isAnyStringEmptyOrWhiteSpace(subversionCandidate, baseVersion)) {
            return false;
        }
        if (BaseUtils.safeEquals(subversionCandidate, baseVersion)) {
            return true;
        }
        baseVersion = baseVersion + ".";
        return BaseUtils.safeStartsWith(subversionCandidate, baseVersion);
    }

    public static <T> HashSet<T> safeNewHashSet(Collection<T> optColl) {
        if (optColl == null) {
            return new HashSet<T>();
        }
        return new HashSet<T>(optColl);
    }

    public static <K, V> HashMap<K, V> safeNewHashMap(Map<K, V> optMap) {
        if (optMap == null) {
            return new HashMap<K, V>();
        }
        return new HashMap<K, V>(optMap);
    }

    /*np 1 000 000.00*/
    public static String chunkStringWithSepSpace(String value) {
        if (BaseUtils.isStrEmpty(value)) {
            return "0.00";
        }
        Pair<String, String> val = new Pair<String, String>();
        if (value.contains(".")) {
            val = splitString(value, ".");
        } else if (value.contains(",")) {
            val = splitString(value, ",");
        } else {
            val.v1 = value;
            val.v2 = "00";
        }
        String v2 = val.v2;
        if (v2.length() == 1) {
            v2 = v2 + "0";
        }

        return chunkStringWithSep(val.v1, 3, " ", true) + "." + v2;
    }

    public static String chunkHTMLWithSepSpace(String htmlInner) {
        String text = SimpleHtmlSanitizer.getText(htmlInner);
        return htmlInner.replace(text, chunkStringWithSepSpace(text));
    }

    private static final String forEncodingPolishLetters = "ĄĆĘŁŃÓŚŹŻąćęłńóśźż";

    public static String encodeCharsForHtml(String str, String charsToEncode, String hexCodes) {
        StringBuilder sb = new StringBuilder();

        int len = str.length();

        for (int i = 0; i < len; i++) {
            char c = str.charAt(i);
            int pos = charsToEncode.indexOf(c);
            if (pos >= 0) {
                String hexCode = hexCodes.substring(pos * 2, pos * 2 + 2);
                sb.append("&#x").append(hexCode).append(";");
            } else {
                sb.append(c);
            }
        }

        return sb.toString();
    }

    @Deprecated
    private static final String forEncodingWin1250Codes = "A5C6CAA3D1D38C8FAFB9E6EAB3F1F39C9FBF";
    @Deprecated
    private static final String forEncodingIso8859_2Codes = "A1C6CAA3D1D3A6ACAFB1E6EAB3F1F3B6BCBF";

    /* dwie poniższe metody są Deprecated, bo słabo działają */
    @Deprecated
    public static String encodeCharsForHtmlAsWin1250(String str) {
        return encodeCharsForHtml(str, forEncodingPolishLetters, forEncodingWin1250Codes);
    }

    @Deprecated
    public static String encodeCharsForHtmlAsIso8859_2(String str) {
        return encodeCharsForHtml(str, forEncodingPolishLetters, forEncodingIso8859_2Codes);
    }

    private static final Map<Character, String> charToHtmlEntity = new HashMap<Character, String>();

    static {
        charToHtmlEntity.put('Ą', "&#260;");
        charToHtmlEntity.put('ą', "&#261;");
        charToHtmlEntity.put('Ć', "&#262;");
        charToHtmlEntity.put('ć', "&#263;");
        charToHtmlEntity.put('Ę', "&#280;");
        charToHtmlEntity.put('ę', "&#281;");
        charToHtmlEntity.put('Ł', "&#321;");
        charToHtmlEntity.put('ł', "&#322;");
        charToHtmlEntity.put('Ń', "&#323;");
        charToHtmlEntity.put('ń', "&#324;");
        charToHtmlEntity.put('Ó', "&#211;");
        charToHtmlEntity.put('ó', "&#243;");
        charToHtmlEntity.put('Ś', "&#346;");
        charToHtmlEntity.put('ś', "&#347;");
        charToHtmlEntity.put('Ź', "&#377;");
        charToHtmlEntity.put('ź', "&#378;");
        charToHtmlEntity.put('Ż', "&#379;");
        charToHtmlEntity.put('ż', "&#380;");
    }

    public static String encodePolishCharsAsHtmlEntities(String str) {
        int len = str.length();

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < len; i++) {
            char c = str.charAt(i);
            String e = charToHtmlEntity.get(c);
            if (e != null) {
                sb.append(e);
            } else {
                sb.append(c);
            }
        }

        return sb.toString();
    }

    public static String getTrimStrToLength(String descr, int maxLengthOfDisplayedDescr) {
        descr = compactWhiteSpaces(descr);
        if (descr == null || descr.length() <= maxLengthOfDisplayedDescr) {
            return descr;
        }

        return descr.substring(0, maxLengthOfDisplayedDescr) + " (...)";
    }

    public static <B extends BeanWithIntIdBase> Map<Integer, B> makeBeanMap(Iterable<B> iterable) {
        return BaseUtils.projectToMap(iterable, new Projector<B, Integer>() {
            @Override
            public Integer project(B val) {
                return val.id;
            }
        });
    }

    public static StringBuilder appendNTimes(StringBuilder sb, String s, int n) {
        for (int i = 0; i < n; i++) {
            sb.append(s);
        }
        return sb;
    }

//    public static void main(String[] args) {
//        System.out.println("encoded Win1250: " + encodeCharsForHtmlAsWin1250("Zażółć gęślą jaźń"));
//        System.out.println("encoded Iso8859_2: " + encodeCharsForHtmlAsIso8859_2("Zażółć gęślą jaźń"));
//        System.out.println("encoded entities: " + encodePolishCharsAsHtmlEntities("Zażółć gęślą jaźń"));
//    }
}
