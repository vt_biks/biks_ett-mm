﻿exec sp_check_version '1.6.z5.2';
go


-- poprzednia wersja w: alter db for v1.2.4 tf.sql
-- fix na kolację
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbw]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbw]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbw](@tree_code varchar(255))
as
begin
	declare @diag_level int = 0;
	declare @AreaNodeKind int;
	declare @BExNodeKind int;
	declare @CUBENodeKind int;
	declare @MPRONodeKind int;
	declare @ODSONodeKind int;
	declare @ISETNodeKind int;
	declare @ObjectsFolderNodeKind int;
	declare @UNINodeKind int;
	declare @TIMNodeKind int;
	declare @KYFNodeKind int;
	declare @DPANodeKind int;
	declare @CHANodeKind int;
	declare @tree_id int;
	
	select @AreaNodeKind = id from bik_node_kind where code = 'BWArea'
	select @BExNodeKind = id from bik_node_kind where code = 'BWBEx'
	select @CUBENodeKind = id from bik_node_kind where code = 'BWCUBE'
	select @MPRONodeKind = id from bik_node_kind where code = 'BWMPRO'
	select @ODSONodeKind = id from bik_node_kind where code = 'BWODSO'
	select @ISETNodeKind = id from bik_node_kind where code = 'BWISET'
	select @ObjectsFolderNodeKind = id from bik_node_kind where code = 'BWObjsFolder'
	select @UNINodeKind = id from bik_node_kind where code = 'BWUni'
	select @TIMNodeKind = id from bik_node_kind where code = 'BWTim'
	select @KYFNodeKind = id from bik_node_kind where code = 'BWKyf'
	select @DPANodeKind = id from bik_node_kind where code = 'BWDpa'
	select @CHANodeKind = id from bik_node_kind where code = 'BWCha'
	select @tree_id = id from bik_tree where code = @tree_code;
	
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'
	
	create table #tmpNodes (si_id varchar(900) collate DATABASE_DEFAULT primary key, si_parentid varchar(1000) collate DATABASE_DEFAULT, si_kind varchar(100) collate DATABASE_DEFAULT, si_name varchar(1000), descr varchar(max) collate DATABASE_DEFAULT, visual_order int default 0);
	create table #tmpNodesLinked (si_id varchar(900) collate DATABASE_DEFAULT, si_parentid varchar(1000) collate DATABASE_DEFAULT);
	
	-- wrzucanie obszarów inf.
	insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
	select obj_name, parent, @AreaNodeKind, descr, null, -1 from bik_sapbw_area 
		
	if(@tree_code = 'BWReports')
	begin
		-- wrzucanie obszaru na zapytania nieprzypisane
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		values('$$BIKS_TEMP_ARCHIWUM$$', null, @AreaNodeKind, '(Archiwum)', 'Obszar gromadzący zapytania BEx, dla których nie odnaleziono dostawcy informacji. Obszar został wygenerowany automatycznie i nie znajduje się w repozytorium SAP BW.',100)

		-- wrzucanie zapytań BEx
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr)
		select convert(varchar(200),obj_name), coalesce((select ar.obj_name from bik_sapbw_provider pv left join bik_sapbw_area ar on pv.info_area=ar.obj_name where pv.obj_name = q.provider),'$$BIKS_TEMP_ARCHIWUM$$'), @BExNodeKind, case when rtrim(descr) <> '' then rtrim(descr) else obj_name end,'' from bik_sapbw_query q
	end;
	if(@tree_code = 'BWProviders')
	begin
		-- wrzucanie obszaru na dostawców nieprzypisanych
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		values('$$BIKS_TEMP_ARCHIWUM$$', null, @AreaNodeKind, '(Archiwum)', 'Obszar gromadzący dostawców informacji, dla których nie odnaleziono obszaru informacji. Obszar został wygenerowany automatycznie i nie znajduje się w repozytorium SAP BW.',100)

		-- wrzucanie folderów na cechy i wskaźniki
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		select obj_name + '|objectsFolder|', obj_name, @ObjectsFolderNodeKind, 'Cechy i wskaźniki', 'Folder zawierający obiekty informacji', -1 from bik_sapbw_provider

		-- wrzucanie dostawców informacji
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order) -- visual_order zgodny z tym w SAP GUI
		select obj_name, coalesce(area,'$$BIKS_TEMP_ARCHIWUM$$'), case type when 'MPRO' then @MPRONodeKind
																			when 'CUBE' then @CUBENodeKind
																			when 'ODSO' then @ODSONodeKind
																			when 'ISET' then @ISETNodeKind end,  
			case when rtrim(descr) <> '' then rtrim(descr) else obj_name end, null, case type when 'CUBE' then 1
																							when 'ISET' then 2
																							when 'MPRO' then 3
																							when 'ODSO' then 4 end  
		from bik_sapbw_provider xx left join (select obj_name as area from bik_sapbw_area) yy on xx.info_area = yy.area
		
		-- wrzucanie obiektów
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		select provider + '|' + obj_name, provider + '|objectsFolder|' ,case type when 'CHA' then @CHANodeKind 
																				  when 'UNI' then @UNINodeKind
																				  when 'KYF' then @KYFNodeKind
																				  when 'TIM' then @TIMNodeKind
																				  when 'DPA' then @DPANodeKind
																				  else @CHANodeKind end, 
																				  descr, null, case type when 'CHA' then 1
																										 when 'KYF' then 2
																										 when 'TIM' then 3
																										 when 'UNI' then 4
																										 when 'DPA' then 5
																										 else 6 end 
		from bik_sapbw_object 
		where provider_parent is null
		
		-- wrzucanie przepływów
		insert into #tmpNodesLinked(si_id,si_parentid)
		select obj_name, parent from bik_sapbw_flow
	end;
	
	-- usuwanie pustych folderów
	declare @rc int = 1

	while @rc > 0 
	begin
		delete from #tmpNodes
		where si_kind in (@AreaNodeKind, @ObjectsFolderNodeKind) and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	
	-- wdrażnie danych z tabeli tymczasowej to bik_node
	
	-- update istniejących
	update bik_node 
	set /*node_kind_id = #tmpNodes.si_kind,*/ name = ltrim(rtrim(#tmpNodes.si_name)), descr = #tmpNodes.descr,
		visual_order = #tmpNodes.visual_order, is_deleted = 0
	from #tmpNodes
	where bik_node.tree_id = @tree_id
	and bik_node.obj_id = #tmpNodes.si_id 
	
	-- wrzucanie nowych
	insert into bik_node(name,node_kind_id,tree_id,descr,obj_id, visual_order)
	select ltrim(rtrim(#tmpNodes.si_name)), #tmpNodes.si_kind, @tree_id, #tmpNodes.descr, #tmpNodes.si_id, #tmpNodes.visual_order
	from #tmpNodes --inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	--left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id and bik_node.linked_node_id is null
	left join (bik_node bn inner join bik_tree bt on bn.tree_id = bt.id and bn.tree_id = @tree_id and bn.linked_node_id is null) on bn.obj_id = #tmpNodes.si_id
	where bn.id is null;
	
	-- wrzucanie podlinkowanych
	insert into bik_node(name,parent_node_id,node_kind_id,tree_id,obj_id, linked_node_id, disable_linked_subtree)
	select lin.name, par.id, lin.node_kind_id, lin.tree_id, tmp.si_id, lin.id, 1 from #tmpNodesLinked tmp 
	left join bik_node lin on lin.obj_id = tmp.si_id and lin.is_deleted = 0 and lin.tree_id = @tree_id and lin.linked_node_id is null
	left join bik_node par on par.obj_id = tmp.si_parentid and par.is_deleted = 0 and par.tree_id = @tree_id and par.linked_node_id is null
	left join bik_node oryg on oryg.obj_id = tmp.si_id and oryg.is_deleted = 0 and oryg.tree_id = @tree_id and oryg.linked_node_id is not null
	where oryg.id is null and par.id is not null and lin.id is not null
	
	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join #tmpNodes as pt on bik_node.tree_id = @tree_id and bik_node.obj_id = pt.si_id
	inner join bik_node bk on bk.tree_id = @tree_id and bk.obj_id = pt.si_parentid 
	where bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.is_deleted = 0
	and bk.linked_node_id is null
	
	-- usuwanie starych
	update bik_node
	set is_deleted = 1
	from bik_node
	left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id
	where #tmpNodes.si_id is null
	and bik_node.tree_id = @tree_id
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @tree_id 
	and bn2.is_deleted = 1
	
	-- usuwanie starych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join #tmpNodesLinked on bik_node.obj_id = #tmpNodesLinked.si_id 
	where #tmpNodesLinked.si_id is null 
	and bik_node.tree_id = @tree_id
	and bik_node.linked_node_id is not null
	
	
	drop table #tmpNodes;
	drop table #tmpNodesLinked;
	
	exec sp_node_init_branch_id @tree_id, null;
	
end;
go

-- poprzednia wersja w: alter db for v1.6.z4 tf.sql
-- fix na brakujące kolumny w BO 4.x
if exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
as
begin
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	declare @boServer int = dbo.fn_get_bo_actual_server_id();

	delete from bik_sapbo_extradata
	from bik_sapbo_extradata
	inner join bik_node bn on bn.id = bik_sapbo_extradata.node_id
	where bn.tree_id in (@reportTreeId , @universeTreeId, @connectionTreeId)
	
	
	declare @tempTable table (
		id int identity(1,1) not null,
		column_name varchar(50) not null,
		table_name varchar(50) not null
	);
	
	-- ważna jest kolejnosc wstawiania
	insert into @tempTable(column_name,table_name)
	values
		('SI_AUTHOR','INFO_WEBI'),
		('SI_OWNER','aaa_global_props'),
		('SI_KEYWORD','aaa_global_props'),
		('SI_FILES__SI_PATH','aaa_global_props'),
		('SI_FILES__SI_FILE1','aaa_global_props'),
		('SI_CREATION_TIME','aaa_global_props'),
		('SI_UPDATE_TS','aaa_global_props'),
		('SI_CUID','aaa_global_props'),
		('SI_FILES__SI_VALUE1','aaa_global_props'),
		('SI_SIZE','aaa_global_props'),
		('SI_HAS_CHILDREN','aaa_global_props'),
		('SI_GUID','aaa_global_props'),
		('SI_INSTANCE','aaa_global_props'),
		('SI_OWNERID','aaa_global_props'),
		('SI_PROGID','aaa_global_props'),
		('SI_OBTYPE','aaa_global_props'),
		('SI_FLAGS','aaa_global_props'),
		('SI_CHILDREN','aaa_global_props'),
		('SI_RUID','aaa_global_props'),
		('SI_RUNNABLE_OBJECT','aaa_global_props'),
		('SI_CONTENT_LOCALE','aaa_global_props'),
		('SI_IS_SCHEDULABLE','aaa_global_props'),
		('SI_WEBI_DOC_PROPERTIES','aaa_global_props'),
		('SI_READ_ONLY','aaa_global_props'),
		('SI_LAST_SUCCESSFUL_INSTANCE_ID','aaa_global_props'),
		('SI_LAST_RUN_TIME','aaa_global_props'),
		('SI_TIMESTAMP','INFO_WEBI'),
		('SI_PROGID_MACHINE','INFO_WEBI'),
		('SI_ENDTIME','aaa_global_props'),
		('SI_STARTTIME','aaa_global_props'),
		('SI_SCHEDULE_STATUS','aaa_global_props'),
		('SI_RECURRING','aaa_global_props'),
		('SI_NEXTRUNTIME','aaa_global_props'),
		('SI_DOC_SENDER','aaa_global_props'),
		('SI_REVISIONNUM','APP_UNIVERSE'),
		('SI_APPLICATION_OBJECT','APP_UNIVERSE'),
		('SI_SHORTNAME','APP_UNIVERSE'),
		('SI_DATACONNECTION__SI_TOTAL','APP_UNIVERSE'),
		('SI_UNIVERSE__SI_TOTAL','INFO_WEBI')
		
	declare @sqlText varchar(max);
	declare @count int;
	select @count = count(*) from @tempTable
	
	set @sqlText = 'insert into bik_sapbo_extradata (node_id,author,owner,keyword,file_path,file_name,created,modified,cuid,files_value,size,has_children,guid,instance,owner_id,progid,obtype,flags,children,ruid,runnable_object,content_locale,is_schedulable,webi_doc_properties,read_only,last_successful_instance_id,last_run_time,timestamp,progid_machine,endtime,starttime,schedule_status,recurring,nextruntime,doc_sender,revisionnum,application_object,shortname,dataconnection_total,universe_total )
    select bik.id as node_id, '
		
	declare kurs cursor for 
	select column_name, table_name from @tempTable order by id 

	open kurs;
	declare @column varchar(50);
	declare @table varchar(50);
	declare @i int = 1;

	fetch next from kurs into @column, @table;
	while @@fetch_status = 0
	begin
		if exists(select * from sys.columns where name = @column and Object_ID = Object_ID(@table))    
		begin
			set @sqlText = @sqlText + @table + '.' + @column
		end
		else
		begin
			set @sqlText = @sqlText + 'null'
		end
		if(@i <> @count)
		begin
			set @sqlText = @sqlText + ', '
		end
		--
		set @i = @i + 1;
		fetch next from kurs into @column, @table;
	end
	close kurs;
	deallocate kurs;	

	set @sqlText = @sqlText + ' from aaa_global_props
    left join INFO_WEBI on aaa_global_props.si_id = INFO_WEBI.si_id
    left join APP_UNIVERSE on aaa_global_props.si_id = APP_UNIVERSE.si_id
    inner join bik_node bik on convert(varchar(30),aaa_global_props.si_id) = bik.obj_id
    where bik.tree_id in (' + convert(varchar(10),@reportTreeId) + ', ' + convert(varchar(10),@universeTreeId) + ', ' + convert(varchar(10),@connectionTreeId) + ') 
    and bik.is_deleted = 0
    and bik.linked_node_id is null
    order by bik.id'
    
    exec(@sqlText);
    
    delete from bik_sapbo_schedule
    from bik_sapbo_schedule
	inner join bik_node bn on bn.id = bik_sapbo_schedule.node_id
	where bn.tree_id = @reportTreeId
    
    insert into bik_sapbo_schedule(node_id,destination,owner,creation_time,nextruntime,expire,type,schedule_type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters,destination_email_to)
    select bn.id,destination,owner,creation_time,nextruntime,expire,type,schedule_type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters,destination_email_to 
    from aaa_report_schedule sch
    inner join bik_node bn on convert(varchar(30),sch.id) = bn.obj_id
    --inner join bik_tree bt on bt.id = bn.tree_id
    where bn.tree_id = @reportTreeId 
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    --and bt.code in ('Reports')
    

    
	if exists(select 1 from bik_sapbo_server ser 
		inner join bik_data_source_def def on ser.source_id = def.id
		where ser.id = @boServer and def.description = 'Business Objects 4.x') and exists(select 1 from sys.objects where type = 'U' and name = 'APP_COMMONCONNECTION')
	begin
		    delete from bik_sapbo_olap_connection
			from bik_sapbo_olap_connection
			inner join bik_node bn on bik_sapbo_olap_connection.node_id = bn.id
			where bn.tree_id = @connectionTreeId
			
			delete from @tempTable;
			
			insert into @tempTable(column_name,table_name)
			values
				('SI_CONNECTION_TYPE','APP_COMMONCONNECTION'),
				('SI_PROVIDER_CAPTION','APP_COMMONCONNECTION'),
				('SI_CUBE_CAPTION','APP_COMMONCONNECTION'),
				('SI_CUID','APP_COMMONCONNECTION'),
				('SI_CREATION_TIME','APP_COMMONCONNECTION'),
				('SI_UPDATE_TS','APP_COMMONCONNECTION'),
				('SI_OWNER','APP_COMMONCONNECTION'),
				('SI_NETWORK_LAYER','APP_COMMONCONNECTION'),
				('SI_MD_USERNAME','APP_COMMONCONNECTION')
				
			set @sqlText = '';
			set @count = 0;;
			select @count = count(*) from @tempTable
			
			set @sqlText = 'insert into bik_sapbo_olap_connection(node_id, type, provider_caption, cube_caption, cuid, created, modified, owner, network_layer, md_username)
			select bn.id, ';
				
			declare kurs cursor for 
			select column_name, table_name from @tempTable order by id 

			open kurs;
			set @column = '';
			set @table = '';
			set @i = 1;

			fetch next from kurs into @column, @table;
			while @@fetch_status = 0
			begin
				if exists(select * from sys.columns where name = @column and Object_ID = Object_ID(@table))    
				begin
					set @sqlText = @sqlText + @table + '.' + @column
				end
				else
				begin
					set @sqlText = @sqlText + 'null'
				end
				if(@i <> @count)
				begin
					set @sqlText = @sqlText + ', '
				end
				--
				set @i = @i + 1;
				fetch next from kurs into @column, @table;
			end
			close kurs;
			deallocate kurs;	

			set @sqlText = @sqlText + ' from APP_COMMONCONNECTION  
			inner join bik_node bn on bn.obj_id = convert(varchar(30),APP_COMMONCONNECTION.si_id) and bn.tree_id = ' + convert(varchar(10),@connectionTreeId);
		    
			exec(@sqlText);
			
			/*
			insert into bik_sapbo_olap_connection(node_id, type, provider_caption, cube_caption, cuid, created, modified, owner, network_layer, md_username)
			select bn.id, app.SI_CONNECTION_TYPE, app.SI_PROVIDER_CAPTION, app.SI_CUBE_CAPTION, app.SI_CUID, app.SI_CREATION_TIME, app.SI_UPDATE_TS, app.SI_OWNER, app.SI_NETWORK_LAYER, app.SI_MD_USERNAME 
			from APP_COMMONCONNECTION app 
			inner join bik_node bn on bn.obj_id = convert(varchar(30),app.si_id) and tree_id = @connectionTreeId
			*/
	end
end;
go


-- dodanie nodów i drzew dla Microstrategy - dla demo
if not exists(select 1 from bik_tree where code = 'MicrostrategyReports')
begin
	insert into bik_tree(name, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	values('Raporty', 'MicrostrategyReports', 'Metadata', 1, 0, 1, 0)
	
	insert into bik_tree(name, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	values('Kostki', 'MicrostrategyCubes', 'Metadata', 1, 0, 1, 0)
	
	insert into bik_tree(name, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	values('Obiekty', 'MicrostrategyObjects', 'Metadata', 1, 0, 1, 0)
	
	insert into bik_tree(name, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	values('Instancje Baz Danych', 'MicrostrategyDBInstance', 'Metadata', 1, 0, 1, 0)
	
	insert into bik_translation(code, txt, lang, kind)
	values('MicrostrategyReports', 'Reports', 'en', 'tree')
	
	insert into bik_translation(code, txt, lang, kind)
	values('MicrostrategyCubes', 'Cubes', 'en', 'tree')
	
	insert into bik_translation(code, txt, lang, kind)
	values('MicrostrategyObjects', 'Objects', 'en', 'tree')
	
	insert into bik_translation(code, txt, lang, kind)
	values('MicrostrategyDBInstance', 'DB Instance', 'en', 'tree')
	
	exec sp_add_menu_node 'metadata', 'MicroStrategy', 'microstrategyMeta'
	exec sp_add_menu_node 'microstrategyMeta', '@', '$MicrostrategyReports'
	exec sp_add_menu_node 'microstrategyMeta', '@', '$MicrostrategyCubes'
	exec sp_add_menu_node 'microstrategyMeta', '@', '$MicrostrategyObjects'
	exec sp_add_menu_node 'microstrategyMeta', '@', '$MicrostrategyDBInstance'
	-- dodać do szablonu node_kindy
	
	--------
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyAttribute', 'Atrybut', 'microstrategyAttribute', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyFact', 'Fakt', 'microstrategyFact', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyFilter', 'Filtr', 'microstrategyFilter', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyFolder', 'Folder', 'microstrategyFolder', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyCustomGroup', 'Grupa dostosowana', 'microstrategyCustomGroup', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyHierarchy', 'Hierarchia', 'microstrategyHierarchy', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyDBInstance', 'Instancja baz danych', 'microstrategyDBInstance', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyConsolidation', 'Konsolidacja', 'microstrategyConsolidation', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyCube', 'Kostka', 'microstrategyCube', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyDrillMap', 'Mapa drążenia', 'microstrategyDrillMap', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyMetric', 'Metryka', 'microstrategyMetric', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyProject', 'Projekt', 'microstrategyProject', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyPrompt', 'Prompt', 'microstrategyPrompt', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyTemplate', 'Szablon', 'microstrategyTemplate', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyTable', 'Tabela', 'microstrategyTable', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyTransformation', 'Transformacja', 'microstrategyTransformation', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyDashboard', 'Kokpit', 'microstrategyDashboard', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyDocument', 'Dokument', 'microstrategyDocument', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MicrostrategyReport', 'Raport', 'microstrategyReport', 'Metadata', 0, 13, 0)
	
	insert into bik_icon(name)
	values  ('microstrategyAttribute'),
			('microstrategyFact'),
			('microstrategyFilter'),
			('microstrategyFolder'),
			('microstrategyCustomGroup'),
			('microstrategyHierarchy'),
			('microstrategyDBInstance'),
			('microstrategyConsolidation'),
			('microstrategyCube'),
			('microstrategyDrillMap'),
			('microstrategyMetric'),
			('microstrategyProject'),
			('microstrategyPrompt'),
			('microstrategyTemplate'),
			('microstrategyTable'),
			('microstrategyTransformation'),
			('microstrategyDashboard'),
			('microstrategyDocument'),
			('microstrategyReport')
			
	insert into bik_translation(code, txt, lang, kind)
	values  ('MicrostrategyAttribute', 'Attribute', 'en', 'kind'),
			('MicrostrategyFact', 'Fact', 'en', 'kind'),
			('MicrostrategyFilter', 'Filter', 'en', 'kind'),
			('MicrostrategyFolder', 'Folder', 'en', 'kind'),
			('MicrostrategyCustomGroup', 'Custom group', 'en', 'kind'),
			('MicrostrategyHierarchy', 'Hierarchy', 'en', 'kind'),
			('MicrostrategyDBInstance', 'Database instance', 'en', 'kind'),
			('MicrostrategyConsolidation', 'Consolidation', 'en', 'kind'),
			('MicrostrategyCube', 'Cube', 'en', 'kind'),
			('MicrostrategyDrillMap', 'Drill Map', 'en', 'kind'),
			('MicrostrategyMetric', 'Metric', 'en', 'kind'),
			('MicrostrategyProject', 'Project', 'en', 'kind'),
			('MicrostrategyPrompt', 'Prompt', 'en', 'kind'),
			('MicrostrategyTemplate', 'Template', 'en', 'kind'),
			('MicrostrategyTable', 'Table', 'en', 'kind'),
			('MicrostrategyTransformation', 'Transformation', 'en', 'kind'),
			('MicrostrategyDashboard', 'Dashboard', 'en', 'kind'),
			('MicrostrategyDocument', 'Document', 'en', 'kind'),
			('MicrostrategyReport', 'Report', 'en', 'kind')
end;

-- dodanie nodów i drzew dla Oracle BI - dla demo
if not exists(select 1 from bik_tree where code = 'OracleBIReports')
begin

	insert into bik_tree(name, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	values('Raporty', 'OracleBIReports', 'Metadata', 1, 0, 1, 0)
	
	insert into bik_tree(name, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	values('Repozytorium', 'OracleBIRepository', 'Metadata', 1, 0, 1, 0)
	
	insert into bik_tree(name, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	values('Foldery', 'OracleBIFolders', 'Metadata', 1, 0, 1, 0)

	insert into bik_translation(code, txt, lang, kind)
	values('OracleBIReports', 'Reports', 'en', 'tree')
	
	insert into bik_translation(code, txt, lang, kind)
	values('OracleBIRepository', 'Repository', 'en', 'tree')
	
	insert into bik_translation(code, txt, lang, kind)
	values('OracleBIFolders', 'Folders', 'en', 'tree')
	
	exec sp_add_menu_node 'metadata', 'Oracle BI', 'oracleBIMeta'
	exec sp_add_menu_node 'oracleBIMeta', '@', '$OracleBIReports'
	exec sp_add_menu_node 'oracleBIMeta', '@', '$OracleBIRepository'
	exec sp_add_menu_node 'oracleBIMeta', '@', '$OracleBIFolders'
	-- dodać do szablonu node_kindy
	
	--------
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIAnalysis', 'Analiza', 'oracleBIAnalysis', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIAnalysisAndReports', 'Analizy i raporty interaktywne', 'oracleBIAnalysisAndReports', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIFilter', 'Filtr', 'oracleBIFilter', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIFolder', 'Folder', 'oracleBIFolder', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIDataModel', 'Model danych', 'oracleBIDataModel', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIMonit', 'Monitorujące filtry pulpitów informacyjnych', 'oracleBIMonit', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIReportsPublished', 'Raporty opublikowane', 'oracleBIReportsPublished', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIDashboard', 'Pulpit informacyjny', 'oracleBIDashboard', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIReport', 'Raport', 'oracleBIReport', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBITemplateChild', 'Szablon podrzędny', 'oracleBITemplateChild', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBITemplateStyles', 'Szablon styli', 'oracleBITemplateStyles', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIBusinessLayer', 'Warstwa biznesowa', 'oracleBIBusinessLayer', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIPhysicalLayer', 'Warstwa fizyczna', 'oracleBIPhysicalLayer', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIPresentationLayer', 'Warstwa prezentacji', 'oracleBIPresentationLayer', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBICondition', 'Warunek', 'oracleBICondition', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIReportTask', 'Zadanie raportowania', 'oracleBIReportTask', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIColumn', 'Kolumna', 'oracleBIColumn', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIDimension', 'Wymiar', 'oracleBIDimension', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIHierarchy', 'Hierarchia', 'oracleBIHierarchy', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBILogicalLevel', 'Poziom logiczny', 'oracleBILogicalLevel', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBIMeasure', 'Miara', 'oracleBIMeasure', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBITable', 'Tabela', 'oracleBITable', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OracleBITableFact', 'Tabela faktów', 'oracleBITableFact', 'Metadata', 0, 13, 0)
	
	
	insert into bik_icon(name)
	values  ('oracleBIAnalysis'),
			('oracleBIAnalysisAndReports'),
			('oracleBIFilter'),
			('oracleBIFolder'),
			('oracleBIMonit'),
			('oracleBIReportsPublished'),
			('oracleBIDashboard'),
			('oracleBIReport'),
			('oracleBITemplateChild'),
			('oracleBITemplateStyles'),
			('oracleBIBusinessLayer'),
			('oracleBIPhysicalLayer'),
			('oracleBIPresentationLayer'),
			('oracleBICondition'),
			('oracleBIColumn'),
			('oracleBIDimension'),
			('oracleBIHierarchy'),
			('oracleBILogicalLevel'),
			('oracleBIMeasure'),
			('oracleBITableFact'),
			('oracleBITable'),
			('oracleBIReportTask')
			
	insert into bik_translation(code, txt, lang, kind)
	values  ('OracleBIAnalysis', 'Analysis', 'en', 'kind'),
			('OracleBIAnalysisAndReports', 'Analysis and reports', 'en', 'kind'),
			('OracleBIFilter', 'Filter', 'en', 'kind'),
			('OracleBIFolder', 'Folder', 'en', 'kind'),
			('OracleBIMonit', 'Dashboards monitoring filters', 'en', 'kind'),
			('OracleBIReportsPublished', 'Reports published', 'en', 'kind'),
			('OracleBIDashboard', 'Dashboard', 'en', 'kind'),
			('OracleBIReport', 'Report', 'en', 'kind'),
			('OracleBITemplateChild', 'Template child', 'en', 'kind'),
			('OracleBITemplateStyles', 'Template styles', 'en', 'kind'),
			('OracleBIBusinessLayer', 'Business layer', 'en', 'kind'),
			('OracleBIPhysicalLayer', 'Physical layer', 'en', 'kind'),
			('OracleBIPresentationLayer', 'Presentation layer', 'en', 'kind'),
			('OracleBICondition', 'Condition', 'en', 'kind'),
			('OracleBIReportTask', 'Report task', 'en', 'kind'),
			('OracleBIColumn', 'Column', 'en', 'kind'),
			('OracleBIDimension', 'Dimension', 'en', 'kind'),
			('OracleBIHierarchy', 'Hierarchy', 'en', 'kind'),
			('OracleBILogicalLevel', 'Logical level', 'en', 'kind'),
			('OracleBIMeasure', 'Measure', 'en', 'kind'),
			('OracleBITable', 'Table', 'en', 'kind'),
			('OracleBITableFact', 'Fact table', 'en', 'kind')

end;


-- dodanie nodów i drzew dla Cognos - dla demo
if not exists(select 1 from bik_tree where code = 'CognosReports')
begin

	insert into bik_tree(name, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	values('Raporty', 'CognosReports', 'Metadata', 1, 0, 1, 0)
	
	insert into bik_tree(name, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	values('Projekty', 'CognosProjects', 'Metadata', 1, 0, 1, 0)
	
	--insert into bik_tree(name, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	--values('Pakiety', 'CognosPackages', 'Metadata', 1, 0, 1, 0)

	insert into bik_translation(code, txt, lang, kind)
	values('CognosReports', 'Reports', 'en', 'tree')
	
	insert into bik_translation(code, txt, lang, kind)
	values('CognosProjects', 'Projects', 'en', 'tree')
	
	--insert into bik_translation(code, txt, lang, kind)
	--values('CognosPackages', 'Packages', 'en', 'tree')
	
	exec sp_add_menu_node 'metadata', 'Cognos', 'cognosMeta'
	exec sp_add_menu_node 'cognosMeta', '@', '$CognosReports'
	exec sp_add_menu_node 'cognosMeta', '@', '$CognosProjects'
	--exec sp_add_menu_node 'cognosMeta', '@', '$CognosPackages'
	-- dodać do szablonu node_kindy
	
	--------
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('CognosColumn', 'Kolumna', 'cognosColumn', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('CognosDataSource', 'Źródło danych', 'cognosDataSource', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('CognosDimension', 'Wymiar', 'cognosDimension', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('CognosFilter', 'Filtr', 'cognosFilter', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('CognosHierarchy', 'Hierarchia', 'cognosHierarchy', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('CognosMeasure', 'Miara', 'cognosMeasure', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('CognosModel', 'Model', 'cognosModel', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('CognosPackage', 'Pakiet', 'cognosPackage', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('CognosParamMap', 'Mapa parametrów', 'cognosParamMap', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('CognosProject', 'Projekt', 'cognosProject', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('CognosReport', 'Raport', 'cognosReport', 'Metadata', 0, 13, 0)
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('CognosTable', 'Tabela', 'cognosTable', 'Metadata', 0, 13, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('CognosFolder', 'Folder', 'cognosFolder', 'Metadata', 0, 13, 0)	
		
	insert into bik_icon(name)
	values  ('cognosColumn'),
			('cognosDataSource'),
			('cognosDimension'),
			('cognosFilter'),
			('cognosHierarchy'),
			('cognosMeasure'),
			('cognosModel'),
			('cognosPackage'),
			('cognosParamMap'),
			('cognosProject'),
			('cognosReport'),
			('cognosTable'),
			('cognosFolder')
			
	insert into bik_translation(code, txt, lang, kind)
	values  ('CognosColumn', 'Column', 'en', 'kind'),
			('CognosDataSource', 'Data source', 'en', 'kind'),
			('CognosDimension', 'Dimension', 'en', 'kind'),
			('CognosFilter', 'Filter', 'en', 'kind'),
			('CognosHierarchy', 'Hierarchy', 'en', 'kind'),
			('CognosMeasure', 'Measure', 'en', 'kind'),
			('CognosModel', 'Model', 'en', 'kind'),
			('CognosPackage', 'Package', 'en', 'kind'),
			('CognosParamMap', 'Parameter map', 'en', 'kind'),
			('CognosProject', 'Project', 'en', 'kind'),
			('CognosReport', 'Report', 'en', 'kind'),
			('CognosTable', 'Table', 'en', 'kind'),
			('CognosFolder', 'Folder', 'en', 'kind')
	
end;

exec sp_update_version '1.6.z5.2', '1.6.z5.3';
go
