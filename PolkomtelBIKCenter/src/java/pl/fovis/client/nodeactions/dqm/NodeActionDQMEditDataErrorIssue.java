/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.dqm;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.dqm.AddOrEditDQMDataErrorIssueDialog;
import pl.fovis.common.dqm.DQMDataErrorIssueBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author ctran
 */
public class NodeActionDQMEditDataErrorIssue extends NodeActionDQMBase {

    @Override
    public boolean isEnabledByDesign() {
        return node != null && isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_DQM_ERROR_CLOSED, BIKGWTConstants.NODE_KIND_DQM_ERROR_OPENED);
    }

    @Override
    public String getCaption() {
        return I18n.edytujZgloszenie.get();
    }

    @Override
    public void execute() {
        BIKClientSingletons.getDQMService().getDQMDataErrorIssueByNodeId(node.id, new StandardAsyncCallback<DQMDataErrorIssueBean>() {

            @Override
            public void onSuccess(DQMDataErrorIssueBean result) {
                new AddOrEditDQMDataErrorIssueDialog().buildAndShowDialog(result, new IParametrizedContinuation<DQMDataErrorIssueBean>() {

                    @Override
                    public void doIt(DQMDataErrorIssueBean param) {
                        BIKClientSingletons.getDQMService().saveDataErrorIssue(param, new StandardAsyncCallback<Integer>("Error in saveDataErrorIssue") {

                            @Override
                            public void onSuccess(Integer result) {
                                BIKClientSingletons.showInfo(I18n.zapisanoZgloszenie.get());
                                refreshRedisplayDataAndSelectRecord(result);
                            }
                        });
                    }
                });
            }
        });
    }
}
