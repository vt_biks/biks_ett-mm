/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.playground;

import commonlib.LameJarResourceProvider;
import commonlib.LameUtils;
import commonlib.StringFromInputStreamResourceProvider;
import java.util.Collection;
import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.ParametrizedTextProviderByFoxyMillResource;
import pl.trzy0.foxy.commons.IParametrizedNamedTextProvider;
import pl.trzy0.foxy.commons.templates.IParamSignature;
import pl.trzy0.foxy.serverlogic.FoxyMillDaoAwareModel;
import pl.trzy0.foxy.serverlogic.db.SqlValueFormatterCurlyTimeStamp;
import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.IGenericResourceProvider;
import simplelib.IResourceFromProvider;
import simplelib.IValueFormatter;
import simplelib.JsonConverter;
import simplelib.ResourceFromProvider;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public abstract class FoxyMLPlaygroundBase {

    private static final String NAMED_SQLS_PACKAGE_PATH = LameUtils.getPackagePathOfClass(FoxyMLPlaygroundBase.class);

    protected abstract void letsPlay();
    private static final IValueFormatter formatter = new SqlValueFormatterCurlyTimeStamp();

    protected static IParametrizedNamedTextProvider createNamedSqls(String filePath) {
        IGenericResourceProvider<String> stringResProvider = new StringFromInputStreamResourceProvider(new LameJarResourceProvider(), "UTF-8");
        IResourceFromProvider<String> resource = new ResourceFromProvider<String>(stringResProvider, filePath);

        FoxyMillDaoAwareModel model = new FoxyMillDaoAwareModel();

        IParametrizedNamedTextProvider namedSqls = new ParametrizedTextProviderByFoxyMillResource(resource,
                FoxyMillUtils.makeRenderEnv(true, true, formatter),
                model);

        return namedSqls;
    }
    private IParametrizedNamedTextProvider namedSqls;

    private IParametrizedNamedTextProvider getNamedSqls() {
        if (namedSqls == null) {
            String baseClassName = FoxyMLPlaygroundBase.class.getSimpleName();
            //System.out.println("baseClassName=" + baseClassName);
            String baseClassNamePrefix = BaseUtils.dropOptionalSuffix(baseClassName, "Base" /* I18N:  */);
            String thisClassName = getClass().getSimpleName();
            String nameSuffix = BaseUtils.dropOptionalPrefix(thisClassName, baseClassNamePrefix);

            namedSqls = createNamedSqls(NAMED_SQLS_PACKAGE_PATH + "namedSqls" + nameSuffix + "." +"" +"" +"html" /* I18N:  */);
        }
        return namedSqls;
    }

    protected final String provideSql(String name, Object... args) {
        return getNamedSqls().provide(name, args);
    }

    protected final void dumpSql(String name, final Object... args) {
        IParamSignature sig = getNamedSqls().getSignature(name);

        StringBuilder sb = new StringBuilder();

        sb.append("namedSql: ").append(name).append(", " +"" +"" +"params" /* I18N:  */+ ": ");

        Collection<String> paramsNames = sig.getParamNames();

        if (paramsNames.isEmpty()) {
            sb.append("<no params>");
        } else {
            sb.append(BaseUtils.mergeWithSepEx(paramsNames, " ", new Projector<String, String>() {

                int idx = 0;

                public String project(String name) {
                    Object val = args[idx++];
                    return name + "=\'"
                            + //*
                            JsonConverter.convertObject(val)
                            //*/ /*formatter.valToString(val)*/
                            + "\'";
                }
            }));
        }

        sb.append("\n--- " +"" +"" +"rendered body starts in next line" /* I18N:  */+ " --->>>\n").append(getNamedSqls().provide(name, args)).append("\n---<<<--- " +"" +"" +"end of rendered body in previous line" /* I18N:  */+ "---");
        System.out.println("------------------------------------------------");
        System.out.println(sb.toString());
    }

    @SuppressWarnings("unchecked")
    protected static void startPlaying() {
        //new FoxyMLPlaygroundWezyr().letsPlay();
        Class c = LameUtils.getCurrentClass(1);
        //System.out.println("class c: " + c);
        try {
            Class<FoxyMLPlaygroundBase> cc = (Class<FoxyMLPlaygroundBase>) c;
            cc.newInstance().letsPlay();
        } catch (Exception ex) {
            throw new RuntimeException("error in startPlaying, found class" /* I18N:  */+ "=" + c, ex);
        }
    }
}
