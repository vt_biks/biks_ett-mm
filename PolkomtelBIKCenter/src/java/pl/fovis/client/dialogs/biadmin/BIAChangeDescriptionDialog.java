/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.biadmin;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.common.SAPBOObjectBean;
import static pl.fovis.client.bikpages.biadmin.BIAObjectManagerPage.KIND_TO_ICON_MAP;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class BIAChangeDescriptionDialog extends BaseActionOrCancelDialog {

    protected IParametrizedContinuation<Set<SAPBOObjectBean>> saveCont;
    protected Set<SAPBOObjectBean> objects;
    protected Map<Integer, TextBox> tbxMap = new HashMap<Integer, TextBox>();

    @Override
    protected String getDialogCaption() {
        return I18n.objectZmienOpis.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zmien.get();
    }

    @Override
    protected void buildMainWidgets() {
        FlexTable table = new FlexTable();
        table.setCellSpacing(10);
        //header
        table.setWidget(0, 0, new HTML("<b>" + I18n.scheduleID.get() + "</b>"));
        table.setWidget(0, 1, new HTML("<b></b>"));
        table.setWidget(0, 2, new HTML("<b>" + I18n.scheduleNazwa.get() + "</b>"));
        table.setWidget(0, 3, new HTML("<b>" + I18n.objectOpis.get() + "</b>"));
        table.setWidget(0, 4, new HTML("<b>" + I18n.objectNowyOpis.get() + "</b>"));
        int row = 1;
        for (SAPBOObjectBean object : objects) {
            table.setWidget(row, 0, new Label(String.valueOf(object.id)));
            table.setWidget(row, 1, new Image("images/" + KIND_TO_ICON_MAP.get(object.type) + ".gif"));
            table.setWidget(row, 2, new Label(object.name));
            table.setWidget(row, 3, new Label(object.description));
            TextBox tbx = new TextBox();
            tbx.setWidth("300px");
            tbx.setValue(object.description);
            table.setWidget(row, 4, tbx);
            tbxMap.put(object.id, tbx);
            row++;
        }
        ScrollPanel scrollPanel = new ScrollPanel(table);
        scrollPanel.setHeight("500px");
        main.add(scrollPanel);
    }

    @Override
    protected void doAction() {
        for (SAPBOObjectBean object : objects) {
            TextBox tbxDescr = tbxMap.get(object.id);
            object.description = tbxDescr.getValue();
        }
        saveCont.doIt(objects);
    }

    public void buildAndShowDialog(Set<SAPBOObjectBean> objects, IParametrizedContinuation<Set<SAPBOObjectBean>> saveCont) {
        this.saveCont = saveCont;
        this.objects = objects;
        super.buildAndShowDialog();
    }
}
