/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.servlet;

import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wezyr
 */
public interface IHttpResponseAware {
    public void setHttpResponse(HttpServletResponse response);
}
