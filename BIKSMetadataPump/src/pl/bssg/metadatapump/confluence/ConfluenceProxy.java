/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import simplelib.LameRuntimeException;

/**
 * Proxy class for IConfluenceConnector. It trying login again to Confluence, when the exception occures (session timeout).
 * @author tflorczak
 */
public class ConfluenceProxy implements InvocationHandler {

    protected final static int NUMBER_OF_FOR_ITERATION = 2;
    protected IConfluenceConnector target;

    public static IConfluenceConnector makeProxy(IConfluenceConnector target) {
        ConfluenceProxy myProxy = new ConfluenceProxy();
        myProxy.target = target;
        return (IConfluenceConnector) Proxy.newProxyInstance(target.getClass().getClassLoader(), new Class[]{IConfluenceConnector.class}, myProxy);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (!target.isServiceInitialized()) {
            target.initializeService();
        }
        if (!target.isLogged()) {
            target.login();
        }
        Object result = null;
        Exception caught = null;

        for (int i = 0; i < NUMBER_OF_FOR_ITERATION; i++) {
            try {
                result = method.invoke(target, args);
                caught = null;
                break;
            } catch (Exception e) {
                caught = e;
                target.login();
            }
        }
        if (caught != null) {
            throw new LameRuntimeException(caught.getMessage(), caught);
        }
        return result;
    }
}
