/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class VoteBean implements Serializable {

    public Double avg;
    public int count;
    public Integer userValue;
    public Integer sumalike;
    public Integer sumaunlike;
    public String code;
    public String caption;
    public String nodeKindCode;
    public String branchIds;
    public int nodeId;
}
