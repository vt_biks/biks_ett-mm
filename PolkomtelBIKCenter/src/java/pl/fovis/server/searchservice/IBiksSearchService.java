/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.searchservice;

import java.util.List;
import java.util.Set;
import org.apache.lucene.document.Document;

/**
 *
 * @author ctran
 */
public interface IBiksSearchService {

    public List<Document> search(String text, Iterable<Integer> selectedTreeIds, Iterable<String> selectedAttributeName, List<String> selectedOtherFilter, Set<Integer> nodeKindIds, String searchMode, boolean notUseKindAndAttrFilter);

//    public Integer countResult(String text, Iterable<Integer> selectedTreeIds, int pageNum, int pageSize);
    public List<Document> searchInTreeByNodeName(String text, List<Integer> treeIds);
}
