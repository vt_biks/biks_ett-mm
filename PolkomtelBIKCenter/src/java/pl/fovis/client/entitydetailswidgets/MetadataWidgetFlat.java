/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import java.util.List;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.JoinedObjsTabKind;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.JoinedObjBean;

/**
 *
 * @author bfechner
 */
public class MetadataWidgetFlat extends JoinedObjWidgetFlatBase<JoinedObjBean> {

    @Override
    public String getCaption() {
        return BIKGWTConstants.MENU_NAME_METADATA;
    }

    @Override
    protected List<JoinedObjBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
//        return cddb.metadataJOBs;
        return cddb.jobs.get(JoinedObjsTabKind.Metadata);
    }

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_METADATA;
    }
}
