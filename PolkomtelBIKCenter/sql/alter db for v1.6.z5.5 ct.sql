exec sp_check_version '1.6.z5.5';
go

if not exists (select 1 from syscolumns where  (id = OBJECT_ID('bik_lisa_dict_column')) and (name = 'dict_validation'))
begin
	alter table dbo.bik_lisa_dict_column add dict_validation varchar(100) NULL;
end;
go

if not exists (select 1 from syscolumns where  (id = OBJECT_ID('bik_lisa_dict_column')) and (name = 'col_validation'))
begin
	alter table dbo.bik_lisa_dict_column add col_validation varchar(100) NULL;
end;
go

if not exists (select 1 from syscolumns where  (id = OBJECT_ID('bik_lisa_dict_column')) and (name = 'text_displayed'))
begin
	alter table dbo.bik_lisa_dict_column add text_displayed varchar(100) NULL;
end;
go

if not exists (select 1 from syscolumns where  (id = OBJECT_ID('bik_lisa_dict_column')) and (name = 'validate_level'))
begin
	alter table dbo.bik_lisa_dict_column add validate_level int default 0;
end;
go

if not exists (select 1 from sys.tables where name = 'bik_lisa_dict_column_lookup')
begin
        create table bik_lisa_dict_column_lookup (
            id int identity primary key ,
            column_id int,
            value varchar(max));
end;
go

exec sp_update_version '1.6.z5.5', '1.6.z5.6';
go