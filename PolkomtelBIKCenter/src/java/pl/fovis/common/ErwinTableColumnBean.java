/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author pgajda
 */
public class ErwinTableColumnBean implements Serializable {

    public String tableColumnId;//long_id
    public String name;
    public Integer columnType;//column_type
    public String tableId;//table_long_id
    public String columnComment;
    public String parentColRefLongId;
    public String parentRelationshipRefLongId;
    public Integer nodeId;
    public String objId;
}
