/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import java.util.LinkedHashSet;
import java.util.Set;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.ConnectionParametersADBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class AdminConfigADWidget extends AdminConfigBIKBaseWidget<ConnectionParametersADBean> {

    protected ListBox lbxField;
    protected TextBox tbxDomains;
    protected Label lblField;
    protected CheckBox chbIsActive;
    protected PushButton btnSave;

    @Override
    protected void buildInnerWidget() {
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
            }
        }, false);
        createLine();
        lblField = createLabel(I18n.uzywajADAvatar.get() + ":");
        Set<String> attributesSet = new LinkedHashSet<String>();
        attributesSet.add("");
        attributesSet.addAll(PumpConstants.AD_USER_ATTRIBUTES.keySet());
        lbxField = createListBox(attributesSet);
        createLabel(I18n.domenyAD.get()/*"Domains: (use ',' as separator)"*/);
        tbxDomains = createTextBox();
        createLine();
        btnSave = createButton(I18n.zapisz.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ConnectionParametersADBean bean = new ConnectionParametersADBean();
                bean.avatarField = lbxField.getItemText(lbxField.getSelectedIndex());
                bean.domains = tbxDomains.getText();
                bean.isActive = chbIsActive.getValue() == true ? 1 : 0;
                BIKClientSingletons.getService().setADConnectionParameters(bean, new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " setADConnectionParameters") {
                            @Override
                            public void onSuccess(Void result) {
                                BIKClientSingletons.showInfo(I18n.zmienionParametrDoPolaczenZAD.get() /* I18N:  */);
                            }
                        });
            }
        });
        createTestConnectionWidget(I18n.testujPolaczenie.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().runADTestConnection(new StandardAsyncCallback<Pair<Integer, String>>("Error in" /* I18N: no */ + " runADTestConnection") {
                            @Override
                            public void onSuccess(Pair<Integer, String> result) {
                                setResultTestConnection(result);
                            }
                        });
            }
        });
    }

    @Override
    public void populateWidgetWithData(ConnectionParametersADBean data) {
        for (int i = 0; i < lbxField.getItemCount(); i++) {
            if (lbxField.getItemText(i).equals(data.avatarField)) {
                lbxField.setSelectedIndex(i);
                break;
            }
        }
        tbxDomains.setText(data.domains == null ? "" : data.domains);
        chbIsActive.setValue(data.isActive == 1);
        setWidgetsEnabled(chbIsActive.getValue());
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_AD;
    }
}
