﻿exec sp_check_version '1.8.4.25';
go

exec sp_add_attr_2_node_kind 'metaBiksAttributeDef', 'metaBiks.Używaj w systemie zarządzania regułami', 'Nie', 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksAttributeDef', 'metaBiks.Wyświetlaj jako liczbę', 'Nie', 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksAttributeDef', 'metaBiks.Typ atrybutu', 'shortText', 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksAttribute4NodeKind', 'metaBiks.Może być pusty', 'Tak', 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksAttribute4NodeKind', 'metaBiks.Publiczny', 'Tak', 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksAttribute4NodeKind', 'metaBiks.Pokaż na formatce nowego obiektu', 'Tak', 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksNodeKind', 'metaBiks.Folder', 'Tak', 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksNodeKind', 'metaBiks.Rejestr', 'Nie', 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksTreeKind', 'metaBiks.Pozwolić dowiązać', 'Nie', 1, null, null

exec sp_update_version '1.8.4.25', '1.8.4.26';
go