﻿exec sp_check_version '1.1.6.6';
go

insert into bik_admin(param,value)
select 'sapbo.openReportTemplate', val
from bik_app_prop
where name='openReportLinkTemplate'

insert into bik_admin(param,value)
values('dqc.isActive','1')

insert into bik_admin(param,value)
values('sapbo.isActive','1')

insert into bik_admin(param,value)
values('teradata.isActive','1')

insert into bik_data_source_def(description) 
values('MS SQL')


insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values ('MSSQLDatabase', 'Baza danych MSSQL', 'mssqlDB', 'Metadata', 0, 10)

insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values ('MSSQLOwner', 'Właściciel MSSQL', 'mssqlOwner', 'Metadata', 0, 9)

insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values ('MSSQLTable', 'Tabela MSSQL', 'mssqlTable', 'Metadata', 0, 8)

insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values ('MSSQLView', 'Widok MSSQL', 'mssqlView', 'Metadata', 0, 8)

insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values ('MSSQLProcedure', 'Procedura MSSQL', 'mssqlProcedure', 'Metadata', 0, 8)

insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values ('MSSQLFunction', 'Funkcja MSSQL', 'mssqlFunction', 'Metadata', 0, 8)

insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values ('MSSQLColumn', 'Kolumna MSSQL', 'mssqlColumn', 'Metadata', 0, 7)

insert into bik_tree(name,code,tree_kind)
values ('MS SQL', 'MSSQL', 'Metadata')

create table bik_mssql(
	name varchar(512) not null,
	type varchar(100) not null,
	extra_info varchar(max) null,
	definition_text varchar(max) null,
	branch_names varchar(max) not null,
	parent_branch_names varchar(max) null
);
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_mssql]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_mssql]
go

create procedure [dbo].[sp_insert_into_bik_node_mssql]
as
begin

	declare @mssql_tree_id int;
	select @mssql_tree_id=id from bik_tree where code='MSSQL'

	-- update
	update bik_node 
	set is_deleted = 0, descr = mssql.extra_info
	from bik_mssql mssql
	where bik_node.obj_id = mssql.branch_names
	and bik_node.tree_id=@mssql_tree_id
	and bik_node.linked_node_id is null

	-- insert nowych
	insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id)
	select null, bnk.id, ms.name, ms.extra_info, @mssql_tree_id, ms.branch_names from bik_mssql ms
	join bik_node_kind bnk on ms.type=bnk.code
	left join bik_node bn on bn.obj_id=ms.branch_names
	where bn.id is null
	order by ms.branch_names

	-- update parentów
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join bik_mssql as ms on bik_node.obj_id = ms.branch_names
	inner join bik_node bk on bk.obj_id = ms.parent_branch_names 
	where bik_node.linked_node_id is null
	and bik_node.tree_id = @mssql_tree_id
	and bik_node.is_deleted = 0

	-- usunięcie zbędnych
	update bik_node
	set is_deleted=1
	from bik_node left join bik_mssql on bik_node.obj_id=bik_mssql.branch_names
	where bik_mssql.name is null
	and linked_node_id is null
	and is_deleted=0
	and tree_id=@mssql_tree_id


exec sp_node_init_branch_id @mssql_tree_id,null

end;
go

-----------------------------------------------------------------------
-----------------------------------------------------------------------


insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values ('MSSQLServer', 'Server MSSQL', 'mssqlServer', 'Metadata', 0, 8)

create table bik_db_server(
	id int identity(1,1) not null primary key,
	source_id int not null references bik_data_source_def (id),
	name varchar(512) not null,
	server varchar(512) null,
	instance varchar(512) null,
	db_user varchar(512) null,
	db_password varchar(512) null,
	is_active bit not null
);
go

insert into bik_db_server(source_id, name, server,instance,db_user,db_password,is_active)
values ((select id from bik_data_source_def where description='MS SQL'), 'My First Server', '', '', '', '', 0)

if OBJECT_ID (N'dbo.fn_get_level_for_universe_table', N'FN') is not null
    drop function dbo.fn_get_level_for_universe_table;
go

create function dbo.fn_get_level_for_universe_table(@table_name varchar(max))
returns int
as
begin
  declare @tmp int = 0
  declare @result int = 0
  while @tmp <> -1
  begin
	 set @tmp = charindex('.',@table_name, @tmp + 1);
	 if @tmp<>0 set @result += 1;
	 else set @tmp = -1;
  end
  return @result
end
go

alter table bik_sapbo_universe_table
add default_schema_name varchar(max) null
go

alter table bik_sapbo_universe_table
add database_name varchar(max) null
go

alter table bik_sapbo_universe_table
add mssql_table_name varchar(max) null
go

alter table bik_sapbo_universe_connection
add default_schema_name varchar(max) null
go

alter table bik_sapbo_universe_connection
add database_name varchar(max) null
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_mssql_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
    
    declare @connection_kind int;
    select @connection_kind=id from bik_node_kind where code='DataConnection'    
    declare @universe_kind int;
    select @universe_kind=id from bik_node_kind where code='Universe'
    declare @query_kind int;
    select @query_kind=id from bik_node_kind where code='ReportQuery'

	---------- universe table --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	---------- universe table --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	---------- object --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	---------- object --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	---------- query --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id=@query_kind
	and bnparent.linked_node_id is null
	
	---------- query --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id=@query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id=@query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id=@query_kind
	and bnparent.linked_node_id is null	

	---------- universe --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- universe --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null


	
	---------- mssql table --> universe table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, ut.node_id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	---------- mssql table --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id=@query_kind
	and bnparent.linked_node_id is null
	
	---------- mssql table --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql table --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.mssql_table_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null

	---------- mssql DB --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_joined_objs obj on obj.src_node_id=ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id=obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 from bik_sapbo_universe_table ut 
	join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	join bik_node bn on bn.obj_id=ut.database_name
	and bn.is_deleted=0
	and bn.linked_node_id is null
	join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id=@query_kind
	and bnparent.linked_node_id is null		
	
    exec sp_move_bik_joined_objs_tmp
    
end;
go

-- poprzednia wersja w pliku: "alter db for v1.0.89.3 tf.sql"
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_metadata_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp

    declare @teradataKind int;
    select @teradataKind=id from bik_tree where code='Teradata'
    declare @schemaKind int;
    select @schemaKind=id from bik_node_kind where code='TeradataSchema'
    declare @tabKind int;
    select @tabKind=id from bik_node_kind where code='TeradataTable'
    declare @viewKind int;
    select @viewKind=id from bik_node_kind where code='TeradataView'
    declare @connection_kind int;
    select @connection_kind=id from bik_node_kind where code='DataConnection'
    declare @query_kind int;
    select @query_kind=id from bik_node_kind where code='ReportQuery'

    ----------   universe  -->  zapytania   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
    select bn.id as src_node_id, bnu.id as dst_node_id, 1 as type, 1 as inherit_to_descendants
    from bik_sapbo_query sbq
    join bik_node bn on sbq.node_id=bn.id
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
	where bn.is_deleted=0 

    ----------   connection  -->  zapytania   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id as src_node_id, bnc.id as dst_node_id, 1 as type
    from bik_sapbo_query sbq
    join bik_node bn on sbq.node_id=bn.id
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
    join bik_node bnc on bnc.obj_id=convert(varchar(30),uni.SI_DATACONNECTION__1) and
    bnc.is_deleted=0 and bnc.node_kind_id=(select id from bik_node_kind where code='DataConnection')
	where bn.is_deleted=0 

    ----------   universe  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnu.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')

    ----------   raport  -->  universe   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnu.id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')

    ----------   raport  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnc.id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
    join bik_node bnc on bnc.obj_id=convert(varchar(30),uni.SI_DATACONNECTION__1) and
    bnc.is_deleted=0 and bnc.node_kind_id=(select id from bik_node_kind where code='DataConnection')

	----------   connection  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnc.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
    join bik_node bnc on bnc.obj_id=convert(varchar(30),uni.SI_DATACONNECTION__1) and
    bnc.is_deleted=0 and bnc.node_kind_id=(select id from bik_node_kind where code='DataConnection')

    ----------   teradata Table/View  -->  obiekt   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)

    ----------   teradata Schema  -->  obiekt   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind

    ----------   universum  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type='Teradata' and b.is_derived=0

    ----------   teradata Table/View  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type='Teradata' and b.is_derived=0

    ----------   universum  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    where b.type='Teradata' and b.is_derived=0

    ----------   teradata Schema  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    where b.type='Teradata' and b.is_derived=0

    ----------   connection  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null

    ----------   teradata Schema  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null

    ----------   teradata Table/View  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null

    ----------   connection  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null 

    ----------   teradata Table/View  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   raport  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   teradata Schema  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id = @schemaKind
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   raport  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id = @schemaKind
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   zapytanie  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   zapytanie  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

	----------   obiekt  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct ids.object_node_id, bn.parent_node_id, 1 from aaa_ids_for_report ids join bik_node bn on ids.query_node_id=bn.id
	
	----------   UniverseTable  -->  teradata Table/View   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 from bik_sapbo_universe_table bsut 
	join bik_node bn on bsut.name_for_teradata=bn.obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null

	----------   teradata Table/View  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bsut.node_id, 1 from bik_sapbo_universe_table bsut 
	join bik_node bn on bsut.name_for_teradata=bn.obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null

	----------   UniverseTable  -->  teradata Schema   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 from bik_sapbo_universe_table bsut 
	join bik_node bn on bsut.schema_name=bn.obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	----------   UniverseAliasTable  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bsut1.node_id, 1 from bik_sapbo_universe_table bsut 
	join bik_sapbo_universe_table bsut1 on bsut.original_table = bsut1.their_id
	join bik_node bn on bn.id=bsut.node_id
	join bik_node bn2 on bn2.id=bsut1.node_id
	where bsut.is_alias=1 and bn2.parent_node_id=bn.parent_node_id

    exec sp_move_bik_joined_objs_tmp
	
end;
go

exec sp_update_version '1.1.6.6', '1.1.6.7';
go