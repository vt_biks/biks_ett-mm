/*

-- komu brakuje bazy?

select * from mltx_database md inner join mltx_registered_user mru on md.creator_id = mru.id
where database_name collate Polish_CI_AS not in (select name from sys.databases)

*/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'sp_mltx_create_missing_single_biks_db') AND type in (N'P', N'PC'))
DROP PROCEDURE sp_mltx_create_missing_single_biks_db
GO

create procedure sp_mltx_create_missing_single_biks_db(@email varchar(1000))
as
begin

  declare @db_name varchar(256) = (select md.database_name from mltx_database md inner join mltx_registered_user mru on md.creator_id = mru.id
    where mru.email = @email);

  if @db_name is null begin
    print 'There is not database in mltx_database for user ' + @email + ', nothing to do';
    return;
  end;
  
  declare @db_name_q varchar(1000) = quotename(@db_name, '[');
  
  if exists(select 1 from sys.databases where name = @db_name) begin
    print 'Database ' + @db_name + ' for user ' + @email + ' already exists, nothing to do';
    return;
  end;

  declare @db_template_id int = (select template_id from mltx_database where database_name = @db_name);

  print 'User ' + @email + ' should have db named ' + @db_name + ', but the db does not exist in sys.databases. Will create db with template id=' + 
    cast(@db_template_id as varchar(20));

  declare @source_location varchar(max) = (select file_path from mltx_database_template where id = @db_template_id);

  declare @destDir varchar(max) = (select val from mltx_app_prop where name = 'database_files_destination_path');

  if (@destDir is null)
  begin
    declare @regular varchar(max) = (select top 1 physical_name from sys.database_files where type = 0);
    declare @reversed varchar(max) = REVERSE(@regular);
    set @destDir = LEFT(@regular, len(@regular) - CHARINDEX('\', @reversed)+1) ;
  end

  declare @mdfDir varchar(max) = @destDir + @db_name+'.mdf';
  declare @ldfDir varchar(max) = @destDir +  @db_name+'_log.LDF';

  print 'Restoring template backup, source location=' + @source_location + ', destination dir=' + @destDir;

  declare @sql_txt varchar(max) = 'restore database ' + @db_name_q + ' from disk = ' + quotename(@source_location, '''') +
    ' with move ''boxi'' to ' + quotename(@mdfDir, '''') + ', move ''boxi_log'' to ' + quotename(@ldfDir, '''');

  --print(@sql_txt);
  exec(@sql_txt);

  set @sql_txt = 'ALTER DATABASE ' + @db_name_q + ' SET MULTI_USER WITH NO_WAIT';

  --print(@sql_txt);
  exec(@sql_txt);
  
  print 'Database created (restored from backup)';
  
  declare @db_id int = (select id from mltx_database where database_name = @db_name);

  insert into [MasterBIKS].[dbo].[mltx_login_database] (login, database_id, is_disabled)
  values (@email, @db_id, 0);

  set @sql_txt = '
insert into ' + @db_name_q + '..bik_system_user (login_name, is_disabled, name)
values (' + quotename(@email, '''') + ', 0, ' + quotename(@email, '''') + ')

declare @sys_user_id int = scope_identity()
  
insert into ' + @db_name_q + '..bik_user_right (user_id, right_id)
values (@sys_user_id, (select id from ' + @db_name_q + '..bik_right_role where code = ''AppAdmin''))';

  --print(@sql_txt);
  exec(@sql_txt);

  print 'User added to new db as AppAdmin, all finished!';
end;
go


-- exec sp_mltx_create_missing_single_biks_db 'tadeusz.kifner@ge.com';

/*
select * from mltx_registered_user
select * from mltx_database_template

insert into mltx_registered_user (first_name, last_name, email, password)
values ('W', 'W5', 'wildwezyr+5@gmail.com', 'ww5');

declare @mru_id int = scope_identity();

insert into mltx_database (database_name, creator_id, template_id)
values ('biks_empty_pl_z_palca_ww', @mru_id, 3) --<-- Pusta baza BIKS: biks_empty_pl_

*/
