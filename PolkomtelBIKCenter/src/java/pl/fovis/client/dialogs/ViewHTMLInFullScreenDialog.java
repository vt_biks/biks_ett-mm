/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Widget;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.InnerLinksWidgetFlat;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class ViewHTMLInFullScreenDialog extends ViewSomethingInBIKSDialog {

    protected String body;
    protected String dialogCaption;
    protected InnerLinksWidgetFlat linksForGlos;
    protected FlowPanel innerLinks;
    protected Map<Integer, Pair<String, String>> innerLinksMap;
    protected InlineLabel inlineLbl;
    protected HTMLPanel htmlp;
    protected Integer nodeId;

    @Override
    protected Widget getContentWidget() {
        innerLinks = new FlowPanel();
        innerLinks.setStyleName("BizDef-innerLinks");
        linksForGlos = new InnerLinksWidgetFlat();
        linksForGlos.setup(true, null);
        innerLinks.setVisible(!BaseUtils.isMapEmpty(innerLinksMap));
        innerLinks.add(linksForGlos.buildWidgets());
        linksForGlos.fetchAndShowLinksEx(innerLinksMap/*
         * , body
         */);
//        linksForGlos.fetchAndShowLinksForBlog(blogEntry);

        linksForGlos.setParamToClose(new IContinuation() {
            public void doIt() {
                cancelClicked();
            }
        });
//        HTML html = new HTML(body);
//        html.setStyleName("def-content");
        htmlp = new HTMLPanel(body);
        FlowPanel vp = new FlowPanel();
        vp.setStyleName("BizDef");
//        vp.add(html);
        if (innerLinksMap != null) {
            BIKClientSingletons.replaceHrefInnerLinksFromDescrToInlineLabel(innerLinksMap, htmlp);
        }
        if (nodeId != null) {
            BIKClientSingletons.parseDynamicNodeDesc(nodeId, htmlp);
        }
        vp.add(htmlp);
        vp.add(innerLinks);

        return vp;
    }

    @Override
    protected String getDialogCaption() {
        return dialogCaption;
    }

    public void buildAndShowDialog(String dialogCaption, String title, String body, Map<Integer, Pair<String, String>> innerLinksMap) {
        this.buildAndShowDialog(dialogCaption, title, body, innerLinksMap, null);
    }

    public void buildAndShowDialog(String dialogCaption, String title, String body, Map<Integer, Pair<String, String>> innerLinksMap, Integer nodeId) {
        this.dialogCaption = dialogCaption;
        this.body = body;
        this.innerLinksMap = innerLinksMap;
        this.nodeId = nodeId;
        super.buildAndShowDialog(title);
    }

//    setScrollPanelSize(Widget scPanel, int height) {
}
