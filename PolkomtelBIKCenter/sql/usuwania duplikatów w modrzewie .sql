﻿declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')

delete from bik_node where id in
(
select bn1.id from bik_node bn1 join bik_node bn2 on bn1.obj_id=bn2.obj_id and bn1.id <> bn2.id
where bn1.tree_id=@tree_id and bn2.tree_id=@tree_id and bn1.is_deleted=0 and bn2.is_deleted=0
and substring(bn1.obj_id, 2, len(bn1.obj_id)) in
(
select code from bik_node_kind where id in
(
select src_node_kind_id from bik_node_kind_4_tree_kind where relation_type='Dziecko' and tree_kind_id in (select id from bik_tree_kind) and is_deleted=0
union
select dst_node_kind_id from bik_node_kind_4_tree_kind where relation_type='Dziecko' and tree_kind_id in (select id from bik_tree_kind) and is_deleted=0
union
select coalesce(src_node_kind_id, dst_node_kind_id) from bik_node_kind_4_tree_kind where relation_type is null and tree_kind_id in (select id from bik_tree_kind) and is_deleted=0
)
and code not like 'metaBiks%'
)
)

declare nk4tk_cursor cursor local
		for select tk.code as tree_code, tk.caption as tree_name, snk.code as src_code, dnk.code as dst_code
		from bik_tree_kind tk
		join bik_node_kind snk on snk.id=tk.branch_node_kind_id
		join bik_node_kind dnk on dnk.id=tk.leaf_node_kind_id
		where tk.code not like 'metaBiks%' and tk.is_deleted=0
declare @tree_code varchar(max)
declare @tree_name varchar(max)
declare @src_code varchar(max)
declare @dst_code varchar(max)
open nk4tk_cursor
fetch next from nk4tk_cursor into @tree_code, @tree_name, @src_code, @dst_code
while @@fetch_status=0 begin
	set @tree_code='@'+@tree_code
	if @src_code is not null set @src_code='&'+@src_code
	set @dst_code='&'+@dst_code

	exec sp_add_menu_node 'knowledge', @tree_name, @tree_code
	if @src_code is null
		exec sp_add_menu_node @tree_code, '@', @dst_code
	else begin
		exec sp_add_menu_node @tree_code, '@', @src_code
		if @src_code <> @dst_code exec sp_add_menu_node @src_code, '@', @dst_code
		end

	fetch next from nk4tk_cursor into @tree_code, @tree_name, @src_code, @dst_code
end
close nk4tk_cursor

declare @treeOfTrees int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @treeOfTrees, null