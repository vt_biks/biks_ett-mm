-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-- 
-- custom right roles - zmiany, rozszerzenia
--
-- 1. wi�cej akcji na w�z�ach (m. in. ShowInTree)
-- 2. tabela bik_custom_right_role_action_branch_grant zamiast
--    tabeli bik_custom_right_role_view_branch_grant - umo�liwia 
--    filtrowanie drzewa dla wybieraczek (czyli dost�pno�� akcji 
--    AddJoinedObjs)
-- 3. procedura sp_recalculate_Custom_Right_Role_Action_Branch_Grants
--    - do przeliczania tabeli do filtrowania drzewa wg akcji
--
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

exec sp_check_version '1.6.z8.3';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

insert into bik_node_action (code)
select x.code
from
  (values 
('ShowAnticipatedObjLinkDialog'),
('AddAnyNode'),
('JumpToLinkedNode'),
('AddRole'),
('Rename'),
('RenameRole'),
('EditDescription'),
('NewNestableParent'),
('AddDocument'),
('AddLisaCategory'),
('DelLisaCategory'),
('MoveLisaCategory'),
('AddLisaCategorization'),
('AddObjectFromConfluence'),
('AddObjectToConfluence'),
('DQMAddTest'),
('DQMEditTest'),
('DQMDeleteTest'),
('AddDefinition'),
('AddVersionDefinition'),
('EditDefinition'),
('AddQuestionAndAnswer'),
('Copy'),
('Cut'),
('Paste'),
('Delete'),
('DeleteRole'),
('MoveInTree'),
('ResetVisualOrder'),
('AddBlogEntry'),
('EditBlogEntry'),
('LinkNode'),
('ChangeToLeaf'),
('ChangeToBranch'),
('AddBranch'),
('AddLeaf'),
('AddCustomTreeNode:dtk_MechanizmKontrolny'),
('AddCustomTreeNode:dtk_Regula'),
('AddUser'),
('LinkUser'),
('EditUser'),
('CopyJojnedObj'),
('LinkUserToRole'),
('LinkUserToRoleForBlogs'),
('EditAnyComment'),
('AddComment'),
('DownloadErrorDqmData'),
('AddJoinedObjs'),
('EditNodeDetails'),
('VoteForUsefulness'),
('ShowInTree')
) x(code) left join bik_node_action na on x.code = na.code
where na.code is null;
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

if not exists(select * from sys.objects where object_id = object_id(N'bik_custom_right_role_action_branch_grant') and type in (N'U'))
create table bik_custom_right_role_action_branch_grant (
  tree_id int not null references bik_tree (id),
  user_id int not null references bik_system_user (id),
  action_id int not null references bik_node_action (id),
  branch_ids varchar(1000) not null,
  unique (action_id, user_id, tree_id, branch_ids)
);
go

if exists(select * from sys.objects where object_id = object_id(N'bik_custom_right_role_view_branch_grant') and type in (N'U'))
  drop table bik_custom_right_role_view_branch_grant;
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].sp_recalculate_Custom_Right_Role_Action_Branch_Grants
GO

create procedure sp_recalculate_Custom_Right_Role_Action_Branch_Grants as
begin
  if coalesce((select val from bik_app_prop where name = 'customRightRolesTreeSelector'), '') = ''
    return;

  truncate table bik_custom_right_role_action_branch_grant;

  -- specjalna akcja widoczno�ci w drzewie na zak�adkach - ShowInTree
  declare @show_in_tree_act_id int = (select id from bik_node_action where code = 'ShowInTree');

  insert into bik_custom_right_role_action_branch_grant (action_id, user_id, tree_id, branch_ids)
  select distinct @show_in_tree_act_id, crrue.user_id, coalesce(crrue.tree_id, rts.tree_id) as tree_id, --crrue.node_id,
    coalesce(n.branch_ids, '') as branch_ids
  from bik_custom_right_role_user_entry crrue left join
      (select distinct crr.id as role_id, t.id as tree_id from
          bik_custom_right_role crr cross apply dbo.fn_split_by_sep(crr.tree_selector, ',', 7) x inner join bik_tree t on
      substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
      substring(x.str, 1, 1) <> '@' and t.code = x.str
   ) rts on crrue.tree_id is null and rts.role_id = crrue.role_id
  left join bik_node n on n.id = crrue.node_id
  where
    not exists
    (
      select --
        1
        --crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
        from bik_custom_right_role_user_entry crrue2 left join
          (select distinct crr.id as role_id, t.id as tree_id from
              bik_custom_right_role crr cross apply dbo.fn_split_by_sep(crr.tree_selector, ',', 7) x inner join bik_tree t on
          substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
          substring(x.str, 1, 1) <> '@' and t.code = x.str
       ) rts2 on crrue2.tree_id is null and rts2.role_id = crrue2.role_id
      left join bik_node n2 on n2.id = crrue2.node_id
      where crrue2.user_id = crrue.user_id and coalesce(n.branch_ids, '') like coalesce(n2.branch_ids, '') + '_%'
        and coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
        --and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
    );

  insert into bik_custom_right_role_action_branch_grant (action_id, user_id, tree_id, branch_ids)
  select distinct naicrr.action_id, crrue.user_id, coalesce(crrue.tree_id, rts.tree_id) as tree_id, --crrue.node_id,
    coalesce(n.branch_ids, '') as branch_ids
  from bik_custom_right_role_user_entry crrue inner join bik_node_action_in_custom_right_role naicrr on crrue.role_id = naicrr.role_id left join
      (select distinct crr.id as role_id, t.id as tree_id from
          bik_custom_right_role crr cross apply dbo.fn_split_by_sep(crr.tree_selector, ',', 7) x inner join bik_tree t on
      substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
      substring(x.str, 1, 1) <> '@' and t.code = x.str
   ) rts on crrue.tree_id is null and rts.role_id = crrue.role_id
  left join bik_node n on n.id = crrue.node_id
  where
    naicrr.action_id <> @show_in_tree_act_id and
    not exists
    (
      select --
        1
        --crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
        from bik_custom_right_role_user_entry crrue2 inner join bik_node_action_in_custom_right_role naicrr2 on crrue2.role_id = naicrr2.role_id left join
          (select distinct crr.id as role_id, t.id as tree_id from
              bik_custom_right_role crr cross apply dbo.fn_split_by_sep(crr.tree_selector, ',', 7) x inner join bik_tree t on
          substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
          substring(x.str, 1, 1) <> '@' and t.code = x.str
       ) rts2 on crrue2.tree_id is null and rts2.role_id = crrue2.role_id
      left join bik_node n2 on n2.id = crrue2.node_id
      where naicrr2.action_id = naicrr.action_id and crrue2.user_id = crrue.user_id and coalesce(n.branch_ids, '') like coalesce(n2.branch_ids, '') + '_%'
        and coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
        --and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
    );
end;
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

exec sp_update_version '1.6.z8.3', '1.6.z8.4';
go
