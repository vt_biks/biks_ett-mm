﻿exec sp_check_version '1.8.2.14';
go

delete from bik_attribute_linked 
where attribute_id in (select attr.id from bik_attribute attr join bik_attr_def adef on adef.id=attr.attr_def_id and adef.name='metaBIKS.Kolejność wyświetlenia')

delete from bik_attribute 
where attr_def_id in (select id from bik_attr_def where name='metaBIKS.Kolejność wyświetlenia')

delete from bik_attr_def where name='metaBIKS.Kolejność wyświetlenia'

declare @constraint_name varchar(max)= (
	select name from sys.default_constraints 
	where parent_object_id=object_id('bik_attribute') 
	and parent_column_id=(select column_id from sys.all_columns where object_id=object_id('bik_attribute') and name='visual_order')
)

if @constraint_name is not null
begin
	exec ('alter table bik_attribute drop constraint ' + @constraint_name) 
	alter table bik_attribute drop column visual_order
end
go

exec sp_update_version '1.8.2.14', '1.8.2.15';
go

  