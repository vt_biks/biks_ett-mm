/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import java.util.Map;
import simplelib.ILameCollector;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public interface INodesDataFetcher {

    public void fetchFullData(ILameCollector<Map<String, Pair<String, Float>>> collector);

    public void fetchChangedData(ILameCollector<Map<String, Pair<String, Float>>> collector);

}
