/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;

/**
 *
 * @author bfechner
 */
public abstract class StatisticsPageBase extends BikPageMinimalBase {

    protected VerticalPanel mainVp = new VerticalPanel();
    protected HorizontalPanel mainHp = new HorizontalPanel();
    protected VerticalPanel vp;
    protected FlexTable grid; //= new FlexTable();

    @Override
    protected Widget buildWidgets() {
        mainHp.setWidth("100%");
        mainHp.setHeight("100%");
        grid = new FlexTable();
        grid.setWidth("100%");
        grid.setStyleName("gridJoinedObj");
        vp = new VerticalPanel();
        show(vp);
        vp.setWidth("100%");
        mainVp.setWidth("100%");
        ScrollPanel scPanelFvsm = new ScrollPanel(vp);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(scPanelFvsm, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 18);
        scPanelFvsm.setStyleName("EntityDetailsPane");
        mainVp.add(scPanelFvsm);
        mainHp.add(mainVp);
        return mainHp;

    }

    protected abstract void show(VerticalPanel vp);

    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        show(vp);
    }

    protected HTML getHtmlAndStyle(String style, String text) {
        HTML ht = new HTML(text);
        ht.setStyleName(style);
        return ht;
    }

    protected HTML getHtmlAndStyle(String text) {
        return getHtmlAndStyle("Statistics-cnt", text);
    }
}
