/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.jasperreport;

import commonlib.LameUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.FieldNameConversion;

/**
 *
 * @author ctran
 */
public class ReportDataProvider {

    protected INamedSqlsDAO<Object> adhocDao;
    protected String reportFolder;
    protected String reportName;
    protected Integer nodeId;

    public ReportDataProvider(INamedSqlsDAO<Object> adhocDao, Integer nodeId, String reportName, String reportFolder) {
        this.adhocDao = adhocDao;
        this.nodeId = nodeId;
        this.reportName = reportName;
        this.reportFolder = reportFolder;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> provideDataForReport(String paramsStr) throws IOException {
        Map<String, Object> paramLists = LameUtils.readJsonAsMap(LameUtils.loadAsString(reportFolder + reportName + ".params"));
        Map<String, Object> dsLists = LameUtils.readJsonAsMap(LameUtils.loadAsString(reportFolder + reportName + ".ds"));
        Map<String, Object> paramValues = LameUtils.readJsonAsMap(paramsStr);
        paramValues.put("nodeId", nodeId);
        Map<String, Object> data = new HashMap<String, Object>();

        for (String query : dsLists.keySet()) {
            List<String> pList = (List<String>) paramLists.get(query);
            Object[] pValue = new Object[pList.size()];
            for (int i = 0; i < pList.size(); i++) {
                pValue[i] = paramValues.get(pList.get(i));
            }
            JRMapArrayDataSource ds = new JRMapArrayDataSource(adhocDao.execNamedQueryCFN(query, FieldNameConversion.ToLowerCase, pValue).toArray());
            data.put((String) dsLists.get(query), ds);
        }

        return data;
    }

}
