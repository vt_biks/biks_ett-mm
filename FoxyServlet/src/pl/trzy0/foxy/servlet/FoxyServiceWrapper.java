/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import java.util.Map;
import pl.trzy0.foxy.serverlogic.IFoxyService;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import pl.trzy0.foxy.serverlogic.IFoxyBizLogic;
import pl.trzy0.foxy.serverlogic.ServletConfigObj;

/**
 *
 * @author wezyr
 */
public class FoxyServiceWrapper<T extends IFoxyBizLogic, SCO extends ServletConfigObj/*, SD*/>
        implements HttpSessionBindingListener {

    public IFoxyService<T, SCO/*, SD*/> service;
    private Map<String, IFoxyService<T, SCO>> sharedServices;
    
    public FoxyServiceWrapper(IFoxyService<T, SCO/*, SD*/> service,
            Map<String, IFoxyService<T, SCO>> sharedServices) {
        this.service = service;
        this.sharedServices = sharedServices;
    }

    public void valueBound(HttpSessionBindingEvent arg0) {
    }

    public void valueUnbound(HttpSessionBindingEvent arg0) {
        sharedServices.remove(arg0.getSession().getId());
        service.unboundedFromSession();
    }
}
