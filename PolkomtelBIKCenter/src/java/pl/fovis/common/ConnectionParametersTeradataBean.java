/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

/**
 *
 * @author tflorczak
 */
public class ConnectionParametersTeradataBean extends ParametersBaseBean {

    public String driverClass;
    public String url;
    public String user;
    public String password;
    public String instanceId;
    public String select;
    public String maxErrorCount;
}
