/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import pl.fovis.server.contentsExtractor.IContentsExtractor;
import pl.fovis.server.fts.FtsByLucene;
import pl.fovis.server.fts.INodesDataFetcher;
import pl.fovis.server.fts.ISimilarNodesWizard;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;
import simplelib.ILameCollector;
import simplelib.Pair;
import simplelib.SingleValue;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public class NodesDataFetcherForLucene implements INodesDataFetcher {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected INamedSqlsDAO<Object> adhocDao;
    protected String databaseName;
    protected IContentsExtractor fileContentsExtractor;
    protected String baseFileDir;
    protected long maxFileSize;
    protected float fileContentsBoost;

    public NodesDataFetcherForLucene(INamedSqlsDAO<Object> adhocDao, String databaseName,
            IContentsExtractor fileContentsExtractor, String baseFileDir, long maxFileSize, float fileContentsBoost) {
        this.adhocDao = adhocDao;
        this.databaseName = databaseName;
        this.fileContentsExtractor = fileContentsExtractor;
        this.baseFileDir = baseFileDir == null ? null : BaseUtils.ensureDirSepPostfix4C(baseFileDir.replace("\\", "/"));
        this.maxFileSize = maxFileSize;
        this.fileContentsBoost = fileContentsBoost;
    }

    protected void captureFileNameAndExtractContents(Map<String, Object> sourceRow, Map<String, Pair<String, Float>> nodeData) {

        if (logger.isDebugEnabled()) {

            Map<String, String> sourceRowStr = new LinkedHashMap<String, String>();

            for (Entry<String, Object> e : sourceRow.entrySet()) {
                Object val = e.getValue();
                String valStr = BaseUtils.safeToString(val);
                if (valStr != null && valStr.length() > 100) {
                    valStr = valStr.substring(0, 100);
                }
                sourceRowStr.put(e.getKey(), BaseUtils.replaceNewLinesToRowSep(valStr, "\\n"));
            }

            logger.debug("captureFileNameAndExtractContents: sourceRow=" + sourceRowStr);
        }

        try {
            if (fileContentsExtractor == null || maxFileSize <= 0) {
                if (logger.isDebugEnabled()) {
                    logger.debug("captureFileNameAndExtractContents: fileContentsExtractor is null or maxFileSize <= 0");
                }
                return;
            }

            String fileName = (String) sourceRow.get("file_name");
            if (BaseUtils.isStrEmptyOrWhiteSpace(fileName)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("captureFileNameAndExtractContents: fileName is empty or whitespace only");
                }
                return;
            }

            String fullFileName = baseFileDir + fileName;
            if (logger.isDebugEnabled()) {
                logger.debug("captureFileNameAndExtractContents: fullFileName=" + fullFileName);
            }

            File f = new File(fullFileName);
            long fileLen = f.length();

            if (logger.isDebugEnabled()) {
                logger.debug("captureFileNameAndExtractContents: fileLen=" + fileLen);
            }

            if (fileLen > maxFileSize) {
                if (logger.isDebugEnabled()) {
                    logger.debug("captureFileNameAndExtractContents: file too large");
                }
                return;
            }

            if (!fileContentsExtractor.canExtractFromFile(fullFileName)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("captureFileNameAndExtractContents: cannot extract from file of this kind");
                }
                return;
            }

            if (logger.isDebugEnabled()) {
                logger.debug("captureFileNameAndExtractContents: before extract");
            }
            String fileContents = fileContentsExtractor.extractContents(fullFileName);
            if (logger.isInfoEnabled()) {
                logger.info("captureFileNameAndExtractContents: extracted contents (part)=" + BaseUtils.safeSubstring(fileContents, 0, 200));
            }

            nodeData.put(ISimilarNodesWizard.FILE_CONTENTS_ATTR_NAME, new Pair<String, Float>(fileContents, fileContentsBoost));

        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("can not extract file contents for sourceRow=" + sourceRow, ex);
            }
        }
    }

    @Override
    public void fetchFullData(final ILameCollector<Map<String, Pair<String, Float>>> collector) {
        boolean hasException = true;
        try {
            if (databaseName != null) {
                adhocDao.setCurrentDatabase(databaseName);
            }
            final Map<String, Pair<String, Float>> nodeData = new HashMap<String, Pair<String, Float>>();

            adhocDao.execNamedCommand("setAppPropValue", "indexing.fullData", "inProgress");
            adhocDao.finishWorkUnit(true);

            adhocDao.execNamedQueryExCFN("getAllNodesData", false, false, new ILameCollector<Map<String, Object>>() {

                @Override
                public void add(Map<String, Object> item) {
                    Integer nodeId = adhocDao.sqlNumberToInt(item.get("node_id"));
                    String nodeIdAsStr = nodeId.toString();
                    Integer treeId = adhocDao.sqlNumberToInt(item.get(ISimilarNodesWizard.TREE_ID_ATTR_NAME));
                    Integer nodeKindId = adhocDao.sqlNumberToInt(item.get(ISimilarNodesWizard.NODE_KIND_ID_ATTR_NAME));
                    boolean nextNode = false;
                    if (!nodeData.isEmpty()) {
                        String prevNodeIdAsStr = nodeData.get(FtsByLucene.NODE_ID_FIELD_NAME).v1;
                        if (!BaseUtils.safeEquals(nodeIdAsStr, prevNodeIdAsStr)) {
                            collector.add(nodeData);
                            nodeData.clear();
                            nextNode = true;
                        }
                    } else {
                        nextNode = true;
                    }

                    if (nextNode) {
                        nodeData.put(FtsByLucene.NODE_ID_FIELD_NAME, new Pair<String, Float>(nodeIdAsStr, 100.0f));
                        nodeData.put(ISimilarNodesWizard.TREE_ID_ATTR_NAME, new Pair<String, Float>(BaseUtils.safeToString(treeId), 100.0f));
                        nodeData.put(ISimilarNodesWizard.NODE_KIND_ID_ATTR_NAME, new Pair<String, Float>(BaseUtils.safeToString(nodeKindId), 100.0f));
                        nodeData.put(ISimilarNodesWizard.TREE_KIND_ATTR_NAME, new Pair<String, Float>((String) item.get("tree_kind"), 100.0f));
                        captureFileNameAndExtractContents(item, nodeData);
                    }

                    String name = (String) item.get("name");
                    String value = (String) item.get("value");
                    Integer boost = (Integer) item.get("boost");
                    nodeData.put(name, new Pair<String, Float>(value, boost.floatValue()));
                }
            }, FieldNameConversion.ToLowerCase);

            if (!nodeData.isEmpty()) {
                collector.add(nodeData);
            }

            hasException = false;

        } catch (Exception ex) {
            hasException = true;
            throw new RuntimeException("error in fetchFullData", ex);
        } finally {
            adhocDao.execNamedCommand("setAppPropValue", "indexing.fullData", hasException ? "failed" : "done");
            adhocDao.finishWorkUnit(!hasException);
        }
    }

    @Override
    public void fetchChangedData(final ILameCollector<Map<String, Pair<String, Float>>> collector) {

        if (logger.isInfoEnabled()) {
            logger.info("fetchChangedData starts");
        }

        boolean hasException = false;
        try {
            if (databaseName != null) {
                adhocDao.setCurrentDatabase(databaseName);
            }
            final Map<String, Pair<String, Float>> nodeData = new HashMap<String, Pair<String, Float>>();

            final SingleValue<Long> changeDetailId = new SingleValue<Long>();
            final SingleValue<Integer> changedNodeCnt = new SingleValue<Integer>(0);
            final SingleValue<Integer> changedAttrCnt = new SingleValue<Integer>(0);

            adhocDao.execNamedQueryExCFN("getChangedNodesData", false, false, new ILameCollector<Map<String, Object>>() {

                @Override
                public void add(Map<String, Object> item) {

                    changedAttrCnt.v++;

                    long currentChangeId = (Long) item.get("change_detail_id");

                    if (changeDetailId.v == null || changeDetailId.v < currentChangeId) {
                        changeDetailId.v = currentChangeId;
                    }

                    Integer nodeId = adhocDao.sqlNumberToInt(item.get("changed_node_id"));
                    Integer nodeIdAttr = adhocDao.sqlNumberToInt(item.get("node_id"));
                    Integer treeId = adhocDao.sqlNumberToInt(item.get(ISimilarNodesWizard.TREE_ID_ATTR_NAME));
                    Integer nodeKindId = adhocDao.sqlNumberToInt(item.get(ISimilarNodesWizard.NODE_KIND_ID_ATTR_NAME));

                    String nodeIdAsStr = nodeId.toString();
                    boolean nextNode = false;
                    if (!nodeData.isEmpty()) {
                        String prevNodeIdAsStr = nodeData.get(FtsByLucene.NODE_ID_FIELD_NAME).v1;
                        if (!BaseUtils.safeEquals(nodeIdAsStr, prevNodeIdAsStr)) {
                            changedNodeCnt.v++;
                            collector.add(nodeData);
                            nodeData.clear();
                            nextNode = true;
                        }
                    } else {
                        nextNode = true;
                    }

                    if (nextNode) {
                        nodeData.put(FtsByLucene.NODE_ID_FIELD_NAME, new Pair<String, Float>(nodeIdAsStr, 100.0f));
                        nodeData.put(ISimilarNodesWizard.TREE_ID_ATTR_NAME, new Pair<String, Float>(BaseUtils.safeToString(treeId), 100.0f));
                        nodeData.put(ISimilarNodesWizard.NODE_KIND_ID_ATTR_NAME, new Pair<String, Float>(BaseUtils.safeToString(nodeKindId), 100.0f));
                        nodeData.put(ISimilarNodesWizard.TREE_KIND_ATTR_NAME, new Pair<String, Float>((String) item.get("tree_kind"), 100.0f));
                        captureFileNameAndExtractContents(item, nodeData);
                    }

                    if (nodeIdAttr == null) {
                        nodeData.put(FtsByLucene.IS_DELETED_FIELD_NAME, new Pair<String, Float>("1", 100.0f));
                    } else {
                        String name = (String) item.get("name");
                        String value = (String) item.get("value");
                        Integer boost = (Integer) item.get("boost");
                        nodeData.put(name, new Pair<String, Float>(value, boost.floatValue()));
                    }
                }
            }, FieldNameConversion.ToLowerCase);

            if (!nodeData.isEmpty()) {
                changedNodeCnt.v++;
                collector.add(nodeData);
            }

            if (logger.isInfoEnabled()) {
                logger.info("fetchChangedData: lastChangeDetailId=" + changeDetailId.v + ", changed node cnt=" + changedNodeCnt.v + ", changed attr cnt=" + changedAttrCnt.v);
            }

            if (changeDetailId.v != null) {
                adhocDao.execNamedCommand("updateIndexing_lastIndexedChangeDetailId", changeDetailId.v);
//                adhocDao.execNamedCommand("setAppPropValue", "indexing.lastIndexedChangeDetailId", changeDetailId.v.toString());
            }
        } catch (Exception ex) {
            hasException = true;
            if (logger.isWarnEnabled()) {
                logger.warn("exception in fetchChangedData", ex);
            }
            throw new RuntimeException("error in fetchChangedData", ex);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("fetchChangedData ends: hasException=" + hasException);
            }
            adhocDao.finishWorkUnit(!hasException);
        }
    }
}
