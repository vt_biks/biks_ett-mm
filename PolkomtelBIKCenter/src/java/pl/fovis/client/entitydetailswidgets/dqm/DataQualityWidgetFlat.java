/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import java.util.List;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.JoinedObjsTabKind;
import pl.fovis.client.entitydetailswidgets.JoinedObjWidgetFlatBase;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.JoinedObjBean;

/**
 *
 * @author AMamcarz
 */
public class DataQualityWidgetFlat extends JoinedObjWidgetFlatBase<JoinedObjBean> {

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_QUALITY_METADATA;
    }

    @Override
    protected List<JoinedObjBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
//        return cddb.dqcJOBs;
        return cddb.jobs.get(JoinedObjsTabKind.Dqc);
    }

    @Override
    public String getCaption() {
        return /*BIKGWTConstants.MENU_NAME_DQC + "<br>" + */ BIKGWTConstants.MENU_NAME_DATA_QUALITY;
    }
}
