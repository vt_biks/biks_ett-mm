/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.menu;

/**
 *
 * @author wezyr
 */
public class MenuItemBase implements IMenuItem {

    private String caption;

    public MenuItemBase(String caption) {
        this.caption = caption;
    }

    public String getCaption() {
        return caption;
    }

}
