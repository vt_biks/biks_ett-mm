/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.caches;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import simplelib.INamedPropsBean;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public abstract class FoxyNamedPropsBeanObjectSupplierByManyBase<K, V extends INamedPropsBean>
        extends FoxyNamedPropsBeanObjectSupplierBase<K, V> {

    public FoxyNamedPropsBeanObjectSupplierByManyBase(String primaryKeyName,
            Set<String> altKeyNames, Set<String> keysUniqueOnNull) {
        super(primaryKeyName, altKeyNames, keysUniqueOnNull);
    }

    public FoxyNamedPropsBeanObjectSupplierByManyBase(String primaryKeyName,
            String altKeyNames, String keysUniqueOnNull) {
        super(primaryKeyName, altKeyNames, keysUniqueOnNull);
    }

    protected V getOneFromMany(Collection<V> manyForOne) {
        if (manyForOne.size() != 1) {
            throw new LameRuntimeException(
                    "getManyByKeys for single value returned " +
                    manyForOne.size() + " row(s)");
        }
        return manyForOne.iterator().next();
    }

    @Override
    public V getByKey(K key) {
        Collection<V> manyForOne = getManyByKeys(Collections.singletonList(key));
        return getOneFromMany(manyForOne);
    }

    protected abstract Collection<V> internalGetManyByKeys(Collection<K> keys);

    @Override
    public Collection<V> getManyByKeys(Collection<K> keys) {
        return internalGetManyByKeys(keys);
    }

    public V getByAltKeyVal(String keyName, Object val) {
        Collection<V> manyForOne = getManyByAltKeyVals(keyName, Collections.singletonList(val));
        return getOneFromMany(manyForOne);
    }
}
