/*
 * XMLMapperTest.java
 * JUnit based test
 *
 * Created on 9 maj 2007, 12:44
 */

package commonlib;

import java.io.ByteArrayInputStream;
import junit.framework.*;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class XMLMapperTest extends TestCase {
    
    public XMLMapperTest(String testName) {
        super(testName);
    }
    
    private void innerTestForIs(InputStream is) throws Exception {
        System.out.println("\n------------------------------------");
        byte[] bytes = LameUtils.inputStreamToByteArray(is);
        
        Map m = XMLMapper.readMap(new ByteArrayInputStream(bytes), "xxx");
        System.out.println(m);
        
        String s = LameUtils.loadAsString(new ByteArrayInputStream(bytes));
        System.out.println(s);
        //Reader r = new StringReader(s);
        //Map m2 = XMLMapper.readMap(is, "xxx2");
        //System.out.println(m2);
        
//        r = new StringReader(s);
//        Map m3 = XMLMapper.readMapOld(r);
//        System.out.println(m3);
    }
    
    private byte[] getContentForEncoding(String encoding) throws UnsupportedEncodingException {
        String cont = "<?xml version=\"1.0\" encoding=\"" + encoding + "\"?><root c=\"��ʣ�ӌ��\"/>";
        return cont.getBytes(encoding);
    }
    
    public void testReadMap() throws Exception {
        try {
            //String cont = "<?xml version=\"1.0\" encoding=\"Utf-8\"?><root c=\"��ʣ�ӌ��\"/>";
            //byte[] contBytes = cont.getBytes("Utf-8");
            
            InputStream is = getClass().getClassLoader().getResourceAsStream("test_enc_utf8.xml");
            
            innerTestForIs(is);
            innerTestForIs(new ByteArrayInputStream(getContentForEncoding("WINDOWS-1250")));
            innerTestForIs(new ByteArrayInputStream(getContentForEncoding("UTF-8")));
            innerTestForIs(new ByteArrayInputStream(getContentForEncoding("ISO-8859-2")));
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }
    
    public void testSkipMode() throws Exception {
        XMLMapper mapper = new XMLMapper();
        mapper.setIgnoreEmptyText(true);
        
        InputStream is = getClass().getClassLoader().getResourceAsStream("skipModeTest.xml");
        
        mapper.initializeXml(is);
        
        System.out.println(mapper.getNodeName());
        
        mapper.printNodeType();
        mapper.startWithSubNodes();
        
        while (mapper.hasMoreSubNodes()) {
            mapper.printNodeType();
            System.out.println("node name: [" + mapper.getNodeName() + "]");
            mapper.setSkipMode("hela".equals(mapper.getNodeName()));
            Object node = mapper.readSubNode();
            System.out.println("node content: [" + node + "]"); 
        }
        
        mapper.endWithSubNodes();
        
        mapper.finalizeXml();
    }
}
