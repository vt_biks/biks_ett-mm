/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.jdbc;

import commonlib.LameUtils;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import pl.bssg.metadatapump.MetadataTestConnectionBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.ConnectionParametersJdbcBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.serverlogic.db.ConnectionUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class JdbcTestConnection extends MetadataTestConnectionBase {

    protected Connection jdbcConn;
    protected int id;
    protected ConnectionParametersJdbcBean config;
    private static final ILameLogger logger = LameUtils.getMyLogger();

    public JdbcTestConnection(MssqlConnectionConfig mssqlConnLoadCfg, int id) {
        super(mssqlConnLoadCfg);
        this.id = id;
    }

    @Override
    public Pair<Integer, String> testConnection() {
        try {
            config = adhocDao.createBeanFromNamedQry("getJdbcConfigForInstanceId", ConnectionParametersJdbcBean.class, false, id);
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Invalid login for data loading! Error: " + e.getMessage());
        }
        try {
            jdbcConn = ConnectionUtils.getConnectionByConnectionString(config.jdbcDriverName, config.jdbcUrl, config.user, config.password, null);
            Statement statement = jdbcConn.createStatement();
            ResultSet rs = statement.executeQuery("select 1");
            if (rs == null || !rs.next()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No access to server: " + config.name + " or query return no results");
            }
        } catch (SQLException ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in JDBC test connection", ex);
            }
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "JDBC Pump error: " + ex.getMessage());
        } finally {
            if (jdbcConn != null) {
                PumpUtils.closeConnection(jdbcConn);
            }
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, PumpConstants.PUMP_TEST_CONNECTION_OK);
    }
}
