/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.dom.client.MouseWheelEvent;
import com.google.gwt.event.dom.client.MouseWheelHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.ui.DialogBox.Caption;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import simplelib.Pair;

/**
 *
 * @author uwa
 */
public abstract class DialogCaption implements Caption {
    protected FlowPanel fl = new FlowPanel();
    protected Widget root = fl;
    protected HorizontalPanel hp = new HorizontalPanel();
    protected HTML captionHtml;

    public HandlerRegistration addMouseDownHandler(MouseDownHandler handler) {
        return root.addDomHandler(handler, MouseDownEvent.getType());
    }

    public HandlerRegistration addMouseUpHandler(MouseUpHandler handler) {
        return root.addDomHandler(handler, MouseUpEvent.getType());
    }

    public HandlerRegistration addMouseOutHandler(MouseOutHandler handler) {
        return root.addDomHandler(handler, MouseOutEvent.getType());
    }

    public HandlerRegistration addMouseOverHandler(MouseOverHandler handler) {
        return root.addDomHandler(handler, MouseOverEvent.getType());
    }

    public HandlerRegistration addMouseMoveHandler(MouseMoveHandler handler) {
        return root.addDomHandler(handler, MouseMoveEvent.getType());
    }

    public HandlerRegistration addMouseWheelHandler(MouseWheelHandler handler) {
        return root.addDomHandler(handler, MouseWheelEvent.getType());
    }

    public String getHTML() {
        return captionHtml.getHTML();
    }

    public void setHTML(String html) {
        captionHtml.setHTML(html);
    }

    public String getText() {
        return captionHtml.getText();
    }

    public void setText(String text) {
        captionHtml.setText(text);
    }

    public void setHTML(SafeHtml html) {
        captionHtml.setHTML(html);
    }

    public void fireEvent(GwtEvent<?> event) {
        root.fireEvent(event);
    }

    public Widget asWidget() {
        return root;
    }

    public Pair<Label, Label> initNotificationLabels() {
        FlowPanel fp = new FlowPanel();
        fp.setStyleName("dialogNotificationContainer");
        
        InlineLabel infoLbl = new InlineLabel();
        InlineLabel errLbl = new InlineLabel();
        
        infoLbl.setStyleName("notificationLabel dialogCaptionNotification infoNotification");
        errLbl.setStyleName("notificationLabel dialogCaptionNotification errNotification");
        infoLbl.setVisible(false);
        errLbl.setVisible(false);
        
        int afterCaptionIdx = hp.getWidgetIndex(captionHtml) + 1;
        
        fp.add(errLbl);
        fp.add(infoLbl);
        
        hp.insert(fp, afterCaptionIdx);
//        hp.insert(errLbl, afterCaptionIdx);
//        hp.insert(infoLbl, afterCaptionIdx);
        
        return new Pair<Label, Label>(infoLbl, errLbl);
    }

    public void setCaptionHtml(String captionHtml) {
        this.captionHtml.setHTML(captionHtml);
    }
}
