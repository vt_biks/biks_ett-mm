/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.postgresql;

/**
 *
 * @author tflorczak
 */
public class PostgresForeignKeyBean {

    public String name;
    public String description;
    public String server;
    public String databaseName;
    public String fkSchema;
    public String fkTable;
    public String fkColumn;
    public String pkSchema;
    public String pkTable;
    public String pkColumn;

    public PostgresForeignKeyBean(String name, String description, String server, String databaseName, String fkSchema, String fkTable, String fkColumn, String pkSchema, String pkTable, String pkColumn) {
        this.name = name;
        this.description = description;
        this.server = server;
        this.databaseName = databaseName;
        this.fkSchema = fkSchema;
        this.fkTable = fkTable;
        this.fkColumn = fkColumn;
        this.pkSchema = pkSchema;
        this.pkTable = pkTable;
        this.pkColumn = pkColumn;
    }
}
