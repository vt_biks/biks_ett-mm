exec sp_check_version '1.0.87.5';
go
-- select * from bik_app_prop

----------------------
----------------------
----------------------


exec sp_set_app_prop 'search_order_by_exprs', 'fts_rank desc'
go

--------------------
--------------------
--------------------

alter table bik_node add vote_sum int not null default 0;
go
alter table bik_node add vote_cnt int not null default 0;
go
alter table bik_node add vote_val float not null default 0;
go


update bik_node
set vote_sum = vs, vote_cnt = vc, vote_val = case when vc = 0 then 0 else vs / vc - 2 end
from bik_node bn inner join 
(select node_id, sum(value) as vs, count(value) as vc
from bik_node_vote 
group by node_id
) bnv on bn.id = bnv.node_id
go


--------------------
--------------------
--------------------

exec sp_set_app_prop 'search_order_by_exprs', 'vote_val desc, fts_rank desc'
go

--------------------
--------------------
--------------------

exec sp_update_version '1.0.87.5', '1.0.87.6';
go
