/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.common.NewsBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author mmastalerz
 */
public class AddNewsDialog extends BaseActionOrCancelDialogEx {

    protected IParametrizedContinuation<NewsBean> saveCont;
    protected NewsBean news;

    @Override
    protected String getDialogCaption() {
        return I18n.dodajAktualnosc.get() /* I18N:  */;
    }

    @Override
    protected void doAction() {
        news.text = tbDescription.getValue();
        news.title = tbTytul.getText();
        saveCont.doIt(news);
    }

    public void buildAndShowDialog(NewsBean n, IParametrizedContinuation<NewsBean> saveCont) {
        this.saveCont = saveCont;
        if (n == null) {
            news = new NewsBean();
            news.text = "";
            news.title = "";
        } else {
            news = n;
        }
        super.buildAndShowDialog();
        if (tbTytul.getText().equals("") && tbDescription.getText().equals("")) {
            actionBtn.setEnabled(false);
        }
    }

    @Override
    protected String getTitle() {
        return this.news.title;
    }

    @Override
    protected String getBody() {
        return this.news.text;
    }
}
