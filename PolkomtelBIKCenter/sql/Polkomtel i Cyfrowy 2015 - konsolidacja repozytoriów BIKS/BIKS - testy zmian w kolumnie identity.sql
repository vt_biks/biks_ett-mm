if object_id('tmp_fix_ident_a') is not null
  drop table tmp_fix_ident_a;
go

create table tmp_fix_ident_a (id int not null identity primary key, code varchar(100) not null unique, descr varchar(max) null);
go

delete from tmp_fix_ident_a;

insert into tmp_fix_ident_a (code, descr) values
('ala', 'kot i pies'),
('hela', null),
('tereska', 'piesek'),
('ba�ka', 'baranek'),
('damianek', 'wianek'),
('romanek', 'dzbanek');

insert into tmp_fix_ident_a (code, descr) values
('�ukasz', 'mikrofala');

insert into tmp_fix_ident_a (code, descr) values
('�ukasz0', 'mikrofala');


if object_id('tmp_fix_ident_b') is not null
  drop table tmp_fix_ident_b;
go

create table tmp_fix_ident_b (id int not null identity primary key, code varchar(100) not null unique, a_id int not null foreign key references tmp_fix_ident_a (id));
go

select * from tmp_fix_ident_a


insert into tmp_fix_ident_b (code, a_id) values
('ala', 1),
('hela', 2),
('tereska', 3),
('damianek', 5),
('romanek', 6),
('�ukasz0', 8);


select *
from vw_ww_foreignkeys
where ReferencedTableName = 'tmp_fix_ident_a';



alter table tmp_fix_ident_b nocheck constraint FK__tmp_fix_id__a_id__6502C82F;


alter table tmp_fix_ident_b with check check constraint FK__tmp_fix_id__a_id__6502C82F;




if object_id('tmp_fix_ident_a_clone') is not null
  drop table tmp_fix_ident_a_clone;
go

select cast(10 as int) as id, code, descr into tmp_fix_ident_a_clone from tmp_fix_ident_a where 1 = 0;

alter table tmp_fix_ident_a_clone alter column id int not null;

alter table tmp_fix_ident_a_clone add constraint pk_tmp_fix_ident_a_clone primary key (id);



-- alter table tmp_fix_ident_a switch to tmp_fix_ident_a_clone;


-- alter table tmp_fix_ident_a_clone switch to tmp_fix_ident_a;

alter index uq__tmp_fix___357d4cf94e1f62d7 on tmp_fix_ident_a disable;
go

alter index uq__tmp_fix___357d4cf94e1f62d7 on tmp_fix_ident_a rebuild;
go


DBCC CHECKIDENT ('tmp_fix_ident_a');
go

DBCC CHECKIDENT ('tmp_fix_ident_a_clone');
go

exec sp_msforeachtable 'if objectproperty(object_id(''?''), ''tablehasidentity'') = 1 begin print ''------ table name: ?''; dbcc checkident (''?''); end;';
go


select * from tmp_fix_ident_a
select * from tmp_fix_ident_a_clone

update tmp_fix_ident_a_clone set id = id + 10;


insert into tmp_fix_ident_a (code, descr) values
('�ukasz2', 'mikrofala'),
('�ukasz3', 'mikrofala'),
('�ukasz4', 'mikrofala'),
('�ukasz5', 'mikrofala'),
('�ukasz6', 'mikrofala');


/*

--select * from bik_node;



select distinct substring(name, 1, case when charindex('_', name) = 0 then len(name) else charindex('_', name) end)
from sys.objects
where type = 'U'


select * from sys.objects where name like 'tmp%'


*/
