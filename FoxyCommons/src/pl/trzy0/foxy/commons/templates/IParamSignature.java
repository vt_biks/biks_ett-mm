/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons.templates;

import java.util.Collection;

/**
 *
 * @author wezyr
 */
public interface IParamSignature {
    public Collection<String> getParamNames();
    public ParamInfo getParamInfo(String paramName);
}
