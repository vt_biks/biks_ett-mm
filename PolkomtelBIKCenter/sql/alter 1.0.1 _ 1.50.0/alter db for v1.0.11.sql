exec sp_check_version '1.0.11';
go

----------------------------------------------------------------------
----------------------------------------------------------------------

declare @tree_id int
select @tree_id = ID from bik_tree where name = 'Teradata'

update bik_node
set is_deleted = 1
where tree_id = @tree_id and obj_id not like 'VD_US_%|%'

----------------------------------------------------------------------
----------------------------------------------------------------------
update bik_node
  set descr = nullif(ltrim(rtrim(SI_DESCRIPTION)), '')
  from aaa_global_props join bik_node on bik_node.obj_id = convert (varchar,aaa_global_props.si_id)
  join bik_tree on bik_tree.id = bik_node.tree_id
  where bik_tree.name in ('Raporty', '�wiaty obiekt�w', 'Po��czenia')
go

/*
update bik_node
set descr=null
where descr = ''
--go
*/

----------------------------------------------------------------------
----------------------------------------------------------------------
create table bik_sapbo_extradata(
	node_id int not null primary key references bik_node (id),
	author varchar(1000) null,
	owner varchar(1000) null,
	subject varchar(1000) null,
	comments varchar(8000) null,
	keyword varchar(1000) null,
	universe_name varchar(1000) null,
	connection_name  varchar(1000) null,
	universe_node_id int null references bik_node (id),
	connection_node_id int null references bik_node (id)
);
go


-- http://msdn.microsoft.com/en-us/library/ms177562.aspx
-- o nullif i case

    insert into bik_sapbo_extradata
    select bik.id as node_id, 
    nullif(ltrim(rtrim(p.SI_AUTHOR)), '') as author, 
    nullif(ltrim(rtrim(p.SI_OWNER)), '') as owner,
    nullif(ltrim(rtrim(p.SI_SUBJECT)), '') as subject, 
    nullif(ltrim(rtrim(p.SI_COMMENTS)), '') as comments, 
    nullif(ltrim(rtrim(p.SI_KEYWORD)), '') as keyword,
    au.SI_NAME as universe_name, 
    amdc.si_name as connection_name, 
    uni.id as universe_node_id, conn.id as connection_node_id    
    from aaa_global_props p left join INFO_WEBI iw on p.si_id = iw.si_id
    left join APP_UNIVERSE au on iw.SI_UNIVERSE__1 = au.si_id
    left join APP_METADATA__DATACONNECTION amdc on au.SI_DATACONNECTION__1 = amdc.si_id
    inner join bik_node bik on convert(varchar,p.si_id) = bik.obj_id
    left join bik_node uni on convert(varchar,au.si_id) = uni.obj_id and uni.tree_id=(select id from bik_tree where name='�wiaty obiekt�w')
    left join bik_node conn on convert(varchar, amdc.si_id) = conn.obj_id and conn.tree_id=(select id from bik_tree where name='Po��czenia')
    order by bik.id
go


-- zb�dne bo jest u�yty nullif powy�ej...
/*
update bik_sapbo_extradata
set author=null
where author = ''
--go

update bik_sapbo_extradata
set subject=null
where subject = ''
--go

update bik_sapbo_extradata
set owner=null
where owner = ''
--go

update bik_sapbo_extradata
set comments=null
where comments = ''
--go

update bik_sapbo_extradata
set keyword=null
where keyword = ''
--go
*/


----------------------------------------------------------------------
----------------------------------------------------------------------



exec sp_update_version '1.0.11', '1.0.12';
go
