﻿exec sp_check_version '1.1.9.15';
go

--print 'start '+cast(sysdatetime() as varchar(23)) 
--usuwanie attr_category_id
declare @fk_name sysname
select @fk_name = foreignKey_name from dbo.vw_ww_foreignkeys
where Table_Name = 'bik_role_for_node' and name = 'attr_category_id'
exec ('alter table bik_role_for_node drop constraint ' + @fk_name)
go

-- można usunąć kolumnę - sukces!
alter table bik_role_for_node drop column attr_category_id;
go

--zdjecie constraint z captiona bo można dodawać takie same role
declare @idx_name sysname
select @idx_name = i.name
from sys.indexes i
inner join sys.index_columns ic on i.index_id = ic.index_id and ic.object_id = i.object_id
inner join sys.columns c on ic.object_id = c.object_id and ic.column_id = c.column_id
inner join sys.objects o on i.object_id = o.object_id
where o.name = 'bik_role_for_node' and c.name = 'caption'
exec ('alter table bik_role_for_node drop constraint ' + @idx_name)
go
--print 'usuniecie constraint i attr_category_id'+cast(sysdatetime() as varchar(23))

insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values ('UserRole','Rola użytkownika','userRole','Users',1,6)

declare @node_kind_id int = (select scope_identity())

insert into bik_tree (name,node_kind_id,code,tree_kind,is_hidden,is_in_ranking)
values ('Użytkownicy',@node_kind_id,'UsersRoles','Users',0,0)

declare @tree_id int = (select scope_identity())
--print 'insert into bik_node_kind i bik_node '+cast(sysdatetime() as varchar(23)) 

declare @old_tree_id int =(select id from bik_tree where code='Users')
declare @node_kind_id_all_user_folder int = (select id from bik_node_kind where code='AllUsersFolder')
declare @node_kind_id_user_group int = (select id from bik_node_kind where code='UsersGroup')
--select * from bik_node_kind
update bik_node set tree_id=@tree_id where  tree_id=@old_tree_id and linked_node_id is null
and node_kind_id <>@node_kind_id_user_group

--print 'update użytkowników '+cast(sysdatetime() as varchar(23)) --dlugo
go

--------------------------------------------------------------------------------
--Nowe role jeśli są podobne do nich--------------------------------------------
--------------------------------------------------------------------------------
declare @node_kind_id int =(select id from bik_node_kind where code='UserRole')
declare @tree_id int =(select id from bik_tree where code='UsersRoles')

declare @name varchar(100)

-----------------------------------------------
declare @tmp table (name varchar(1000))
insert into @tmp(name) values ('Architekt Danych')
insert into @tmp(name) values ('Architekt Rozwiązań')
insert into @tmp(name) values ('Partner Biznesowy')
insert into @tmp(name) values ('Ekspert')
insert into @tmp(name) values ('Lider Biznesowy')
insert into @tmp(name) values ('Właściciel Biznesowy')

declare @tmp2 table (name varchar(1000))
insert into @tmp2 select t.name from bik_role_for_node join @tmp t on caption like '%'+t.name+'%' and caption<>t.name

insert into bik_role_for_node(caption) 
select distinct name from @tmp2 t where t.name not in (select caption from bik_role_for_node)
--print 'insert into bik_role_for_node '+cast(sysdatetime() as varchar(23)) 
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--Przezucenie rol do bik_node---------------------------------------------------
--------------------------------------------------------------------------------
insert into  bik_node 
(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,is_built_in,chunked_ver,disable_linked_subtree,search_rank,vote_sum,vote_cnt,vote_val,descr_plain,visual_order)
select NULL,@node_kind_id,caption,@tree_id,NULL,NUll,id,NULL,0,0,0,0,0,0,0,0,NULL,0 from bik_role_for_node
--go
--print 'insert into  bik_node  przezucenie roli'+cast(sysdatetime() as varchar(23))

-----------------------------------------------
------------Podpięcie ról pod rodziców--------
-----------------------------------------------
update bik_node set parent_node_id =x.id
from (select id,bn.name from bik_node bn,@tmp t where bn.tree_id=@tree_id and bn.node_kind_id=@node_kind_id  and bn.name=t.name and is_deleted=0)x
where bik_node.name like '%'+x.name+'%' and bik_node.name<>x.name and  bik_node.tree_id=@tree_id and bik_node.node_kind_id=@node_kind_id and is_deleted=0
--print 'Podpięcie ról pod rodziców '+cast(sysdatetime() as varchar(23))
--------------------------------------------------------------------    
--------------------------------------------------------------------     
--------------------------------------------------------------------    
--Podpięcie użytkowników--------------------------------------------  
go

-------------------------
alter table bik_role_for_node add node_id int not null default 0;
go
declare @tree_id int =(select id from bik_tree where code='UsersRoles')
update bik_role_for_node set node_id=x.node_id
from (select bn.id as node_id,br.id as role_id from bik_role_for_node br left join bik_node bn on br.id = (select convert(int, bn.obj_id))
where bn.tree_id=@tree_id and obj_id is not null)x where x.role_id=bik_role_for_node.id
go
alter table bik_role_for_node
add foreign key(node_id) references bik_node(id)
go
-------------------------

declare @tree_id int =(select id from bik_tree where code='UsersRoles')
declare @node_kind_id int =(select id from bik_node_kind where code='UserRole')
declare @node_kind_id2 int = (select id from bik_node_kind where code='User')

insert into  bik_node 
(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,is_built_in,chunked_ver,disable_linked_subtree,search_rank,vote_sum,vote_cnt,vote_val,descr_plain,visual_order)
select bn.id,@node_kind_id2,bu.name,@tree_id,bu.node_id,NUll,NULL,NULL,0,0,0,0,0,0,0,0,NULL,0 
from bik_node bn 
join bik_role_for_node brn on bn.obj_id=brn.id
join bik_user_in_node buin on brn.id=buin.role_for_node_id 
join bik_user bu on bu.id=buin.user_id
 where tree_id=@tree_id and node_kind_id=@node_kind_id and is_deleted=0
 group by caption,bu.node_id,bu.name,bn.id,bn.obj_id
go
--print 'Podpięcie użytkowników '+cast(sysdatetime() as varchar(23))
--------------------------------------------------------------------    
--------------------------------------------------------------------   
declare @tree_id int =(select id from bik_tree where code='UsersRoles')
exec sp_node_init_branch_id @tree_id, null
--print 'sp_node_init_branch_id '+cast(sysdatetime() as varchar(23))
go


-- tworzenie menu, poprzednia wersja w skrypcie "alter db for v1.1.9.12 pm.sql"

declare @t_id int = dbo.fn_tree_id_by_code('TreeOfTrees')

delete
--select * 
from bik_object_in_history where node_id in (select id from bik_node where tree_id = @t_id)

delete from bik_node where tree_id = @t_id
go

exec sp_add_menu_node null, 'Mój BIKS', '#MyBIKS'
exec sp_add_menu_node null, 'Biblioteka', 'metadata'
exec sp_add_menu_node 'metadata', 'SAP BO', 'sapbo'
exec sp_add_menu_node 'sapbo', '@', '$Reports'
exec sp_add_menu_node 'sapbo', '@', '$ObjectUniverses'
exec sp_add_menu_node 'sapbo', '@', '$Connections'

exec sp_add_menu_node 'metadata', 'Bazy danych', 'databases'
exec sp_add_menu_node 'databases', '@', '$Teradata'

exec sp_add_menu_node 'metadata', '@', '$DQC'
exec sp_add_menu_node null, '@', '$Glossary'
exec sp_add_menu_node null, 'Słowniki', '@Dictionary'
--exec sp_add_menu_node null, 'Kategoryzacje', '@Taxonomy'
--exec sp_add_menu_node null, 'Dokumenty', '$Documents'
--exec sp_add_menu_node null, 'Blogi', '$Blogs'
exec sp_add_menu_node null, 'Baza Wiedzy', 'knowledge'
exec sp_add_menu_node null, 'Społeczność BI', 'community'
exec sp_add_menu_node null, 'Admin', 'Admin'
--exec sp_add_menu_node null, 'Szukaj', '#Search'
exec sp_add_menu_node null, 'Szukaj', '#NewSearch'
		
exec sp_add_menu_node 'Admin', 'Słowniki', 'admin:dict' 
exec sp_add_menu_node 'admin:dict', 'Taksonomie', '#admin:dict:trees' 
exec sp_add_menu_node 'admin:dict', 'AppProps', '#admin:dict:bikAppProps'
exec sp_add_menu_node 'admin:dict', 'Użytkownicy systemu', '#admin:dict:sysUsers'
exec sp_add_menu_node 'admin:dict', 'Atrybuty', '#admin:dict:attrDefs'
exec sp_add_menu_node 'admin:dict', 'Role Użytkowników', '#admin:dict:role'
exec sp_add_menu_node 'admin:dict', 'Treść pomocy', '#admin:dict:attrHints'

exec sp_add_menu_node 'Admin', 'Zasilania', 'admin:load'
exec sp_add_menu_node 'admin:load', 'Logi', '#admin:load:logs'
exec sp_add_menu_node 'admin:load', 'Uruchamianie', '#admin:load:runManual'
exec sp_add_menu_node 'admin:load', 'Harmonogram', '#admin:load:schedule'
exec sp_add_menu_node 'admin:load', 'Konfiguracja połączeń', '#admin:load:cfg'

exec sp_add_menu_node 'Admin', 'Statystyki', 'admin:stat'
exec sp_add_menu_node 'admin:stat', 'Statystyki', '#admin:dict:statistics'
exec sp_add_menu_node 'admin:stat', 'Zalogowani użytkownicy', '#admin:dict:userstatistics'


exec sp_add_menu_node '$DQC', '@', '&DQCGroup'
exec sp_add_menu_node '&DQCGroup', '@', '&DQCTestSuccess'
exec sp_add_menu_node '&DQCGroup', '@', '&DQCTestFailed'
exec sp_add_menu_node '&DQCGroup', '@', '&DQCTestInactive'

exec sp_add_menu_node '$Teradata', '@', '&TeradataOwner'
exec sp_add_menu_node '&TeradataOwner', '@', '&TeradataSchema'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataView'
exec sp_add_menu_node '&TeradataView', '@', '&TeradataColumn'

exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataTable'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumn'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumnPK'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumnIDX'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataProcedure'
exec sp_add_menu_node '$Reports', '@', '&ReportFolder'
exec sp_add_menu_node '&ReportFolder', '@', '&Webi'
exec sp_add_menu_node '&ReportFolder', '@', '&FullClient'
exec sp_add_menu_node '&Webi', '@', '&ReportQuery'
exec sp_add_menu_node '&ReportQuery', '@', '&Dimension$Reports'
exec sp_add_menu_node '&ReportQuery', '@', '&Measure'
exec sp_add_menu_node '&ReportQuery', '@', '&Detail'
exec sp_add_menu_node '&ReportFolder', '@', '&Flash'
exec sp_add_menu_node '&ReportFolder', '@', '&Excel'
exec sp_add_menu_node '&ReportFolder', '@', '&Hyperlink'
exec sp_add_menu_node '&ReportFolder', '@', '&Powerpoint'
exec sp_add_menu_node '&ReportFolder', '@', '&Pdf'
exec sp_add_menu_node '&ReportFolder', '@', '&CrystalReport'
exec sp_add_menu_node '$ObjectUniverses', '@', '&UniversesFolder'
exec sp_add_menu_node '&UniversesFolder', '@', '&Universe'

exec sp_add_menu_node '&Universe', '@', '&UniverseDerivedTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseAliasTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseTable'

exec sp_add_menu_node '&Universe', '@', '&UniverseClass'
exec sp_add_menu_node '&UniverseClass', '@', '&Dimension'
exec sp_add_menu_node '&UniverseClass', '@', '&Measure'
exec sp_add_menu_node '&Dimension', '@', '&Detail'
exec sp_add_menu_node '&UniverseClass', '@', '&Filter'

exec sp_add_menu_node '$Connections', '@', '&ConnectionNetworkFolder'
exec sp_add_menu_node '&ConnectionNetworkFolder', '@', '&ConnectionEngineFolder'
exec sp_add_menu_node '&ConnectionEngineFolder', '@', '&DataConnection'

exec sp_add_menu_node 'knowledge', 'Dokumentacja', '$Documents'
exec sp_add_menu_node 'knowledge', 'Kategoryzacje', '@Taxonomy'

exec sp_add_menu_node '@Taxonomy', '@', '&TaxonomyEntity'

exec sp_add_menu_node 'community', 'Blogi', '$Blogs'
exec sp_add_menu_node 'community', 'Użytkownicy', '$UsersRoles'
exec sp_add_menu_node 'community', 'Użytkownicy', '$Users'
exec sp_add_menu_node 'community', 'Ranking użytkowników', '#Rank'  --new

exec sp_add_menu_node '$Documents', '@', '&DocumentsFolder'
exec sp_add_menu_node '&DocumentsFolder', '@', '&Document'
exec sp_add_menu_node '&DocumentsFolder', '@', '&Article'

exec sp_add_menu_node '$UsersRoles', '@', '&UserRole'
exec sp_add_menu_node '&UserRole', '@', '&User'

exec sp_add_menu_node '$Users', '@', '&UsersGroup'
exec sp_add_menu_node '&UsersGroup', '@', '&User'
exec sp_add_menu_node '$Blogs', '@', '&Blog'

exec sp_add_menu_node '&Blog', '@', '&BlogEntry'

exec sp_add_menu_node '$Glossary', '@', '&GlossaryCategory$Glossary'

exec sp_add_menu_node '&GlossaryCategory$Glossary', '@', '&Glossary'
exec sp_add_menu_node '&GlossaryCategory$Glossary', '@', '&GlossaryNotCorpo'
exec sp_add_menu_node '&GlossaryCategory$Glossary', '@', '&GlossaryVersion'

exec sp_add_menu_node '@Dictionary', '@', '&GlossaryCategory'

exec sp_add_menu_node '&GlossaryCategory', '@', '&Glossary'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryNotCorpo'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryVersion'


exec sp_add_menu_node 'databases', '@', '$MSSQL'
exec sp_add_menu_node 'metadata', 'SAS', 'sas'
exec sp_add_menu_node 'sas', '@', '$SASReports'
exec sp_add_menu_node 'sas', '@', '$SASInformationMaps'
exec sp_add_menu_node 'sas', '@', '$SASLibraries'
exec sp_add_menu_node 'sas', '@', '$SASCubes'

exec sp_add_menu_node '$MSSQL', '@', '&MSSQLServer'
exec sp_add_menu_node '&MSSQLServer', '@', '&MSSQLDatabase'
exec sp_add_menu_node '&MSSQLDatabase', '@', '&MSSQLOwner'
exec sp_add_menu_node '&MSSQLOwner', '@', '&MSSQLTable'
exec sp_add_menu_node '&MSSQLTable', '@', '&MSSQLColumn'
exec sp_add_menu_node '&MSSQLOwner', '@', '&MSSQLProcedure'
exec sp_add_menu_node '&MSSQLOwner', '@', '&MSSQLFunction'
exec sp_add_menu_node '&MSSQLOwner', '@', '&MSSQLView'
exec sp_add_menu_node '&MSSQLView', '@', '&MSSQLColumn'

exec sp_add_menu_node '$SASReports', '@', '&SASReportFolder'
exec sp_add_menu_node '&SASReportFolder', '@', '&SASWrs'

exec sp_add_menu_node '$SASInformationMaps', '@', '&SASInformationMapFolder'
exec sp_add_menu_node '&SASInformationMapFolder', '@', '&SASInformationMapCube'
exec sp_add_menu_node '&SASInformationMapCube', '@', '&SASFolder'
exec sp_add_menu_node '&SASFolder', '@', '&SASMeasure'
exec sp_add_menu_node '&SASFolder', '@', '&SASCategory'
exec sp_add_menu_node '&SASFolder', '@', '&SASFilter'

exec sp_add_menu_node '&SASInformationMapFolder', '@', '&SASInformationMap'

exec sp_add_menu_node '&SASInformationMap', '@', '&SASFolder$SAS'

exec sp_add_menu_node '&SASFolder$SAS', '@', '&SASMeasure'
exec sp_add_menu_node '&SASFolder$SAS', '@', '&SASCategory'
exec sp_add_menu_node '&SASFolder$SAS', '@', '&SASFilter'

exec sp_add_menu_node '$SASLibraries', '@', '&SASLibraryFolder'
exec sp_add_menu_node '&SASLibraryFolder', '@', '&SASLibrary'
exec sp_add_menu_node '&SASLibrary', '@', '&SASLibraryTable'
exec sp_add_menu_node '&SASLibraryTable', '@', '&SASLibraryColumnNum'
exec sp_add_menu_node '&SASLibraryTable', '@', '&SASLibraryColumnChar'
exec sp_add_menu_node '&SASLibraryTable', '@', '&SASLibraryColumnDate'

exec sp_add_menu_node '$SASCubes', '@', '&SASCubeFolder'
exec sp_add_menu_node '&SASCubeFolder', '@', '&SASCube'
exec sp_add_menu_node '&SASCube', '@', '&SASCubeMeasures'
exec sp_add_menu_node '&SASCubeMeasures', '@', '&SASMeasure'
exec sp_add_menu_node '&SASCubeMeasures', '@', '&SASCalculatedMeasure'
exec sp_add_menu_node '&SASCube', '@', '&SASCubeDimension'
exec sp_add_menu_node '&SASCubeDimension', '@', '&SASHierarchy'
exec sp_add_menu_node '&SASHierarchy', '@', '&SASLevel'

go
/*
declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go*/

declare @tree_of_trees_id int

select @tree_of_trees_id = id from bik_tree where code = 'TreeOfTrees'

declare @dynamicTreeNodes table (node_id int not null primary key, code varchar(255) not null unique)

insert into @dynamicTreeNodes (node_id, code)
select id, substring(obj_id, 2, len(obj_id))
from bik_node with(index(idx_bik_node_tree_id_is_deleted))
where tree_id = @tree_of_trees_id and is_deleted = 0
and obj_id like '@%'

declare @sqlTxt varchar(max) = ''


update bik_tree
set @sqlTxt = @sqlTxt + 'exec sp_add_menu_node ''@' + t.tree_kind + ''', ''@'', ''$' + t.code + '''
'
from bik_tree t inner join @dynamicTreeNodes dtn on dtn.code = t.tree_kind
left join bik_node totn on totn.tree_id = @tree_of_trees_id and totn.is_deleted = 0 and '$' + t.code = totn.obj_id
where totn.id is null

if ltrim(rtrim(@sqlTxt)) <> ''
exec(@sqlTxt)

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go


exec sp_update_version '1.1.9.15', '1.1.9.16';
go

