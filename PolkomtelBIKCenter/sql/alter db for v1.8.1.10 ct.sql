﻿exec sp_check_version '1.8.1.10';
go

delete bik_node_kind_4_tree_kind where is_deleted=1
delete from bik_node_kind_4_tree_kind  
where id not in (
	select min(id) as id from bik_node_kind_4_tree_kind 
	group by tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type, is_deleted
) 

if not exists (select 1 from bik_app_prop where name='enableCustomTreeMode') 
insert into bik_app_prop(name, val) values('enableCustomTreeMode', '1') 
else update bik_app_prop set val='1' where name='enableCustomTreeMode'

declare @branchNodeKindId int = (select max(id) from bik_node_kind where is_deleted=0)
declare @leafNodeKindId int = (select min(id) from bik_node_kind where is_deleted=0)

if not exists (select 1 from bik_tree_kind where code='metaBIKS')
insert into bik_tree_kind(code, caption, branch_node_kind_id, leaf_node_kind_id)
values ('metaBIKS', 'Meta BIKS', @branchNodeKindId, @leafNodeKindId)
else
update bik_tree_kind set caption='Meta BIKS', branch_node_kind_id=@branchNodeKindId, leaf_node_kind_id=@leafNodeKindId, is_deleted=0, allow_linking=0
where code='metaBIKS'
exec sp_update_version '1.8.1.10', '1.8.2';
