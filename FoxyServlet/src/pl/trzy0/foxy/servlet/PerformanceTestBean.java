/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.servlet;

import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public class PerformanceTestBean {
    public int repeats = 10000;

    public int cnt = 1000;

    public Integer[] getNums(int cnt) {
        System.out.println("getNums: start");
        Integer[] res = new Integer[cnt];
        for (int i = 0; i < cnt; i++) {
            res[i] = i;
        }
        System.out.println("getNums: end");
        return res;
    }

    public Integer[] getAllNums() {
        System.out.println("getAllNums: start");
        Integer[] res = getNums(cnt);
//        Integer[] res = new Integer[cnt];
//        for (int i = 0; i < cnt; i++) {
//            res[i] = i;
//        }
        System.out.println("getAllNums: end");
        return res;
    }

    public String getCellColor(int x) {
        return "#ff00ff";
    }

    public String getRowBackgroundColor(int x) {
        return "#ffffff";
    }

    public int getRowSize() {
        return 10;
    }

    public long getNanoTime() {
        return System.nanoTime();
    }

    public String elapsedTime(long startNanos, long endNanos) {
        return //String.valueOf(
                BaseUtils.doubleToString((endNanos - startNanos) / 1000000000.0, 6);
    }
}
