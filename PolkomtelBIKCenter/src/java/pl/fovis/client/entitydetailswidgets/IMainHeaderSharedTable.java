/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author beata
 */
public interface IMainHeaderSharedTable {

    public void addRow(String name, boolean isVisible, Object value, Widget... actWidget);

    public void addTranslatedRow(String name, Object value, Widget... actWidget);
    
    public void setColumnWidthForNonActiveWidget();

    public void clear();
}
