/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.filesystem;

import commonlib.LameUtils;
import jcifs.smb.NtlmPasswordAuthentication;
import pl.bssg.metadatapump.MetadataTestConnectionBase;
import pl.bssg.metadatapump.common.ConnectionParametersFileSystemBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class FileSystemTestConnection extends MetadataTestConnectionBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected int id;

    public FileSystemTestConnection(MssqlConnectionConfig mcfg, int id) {
        super(mcfg);
        this.id = id;
    }

    @Override
    public Pair<Integer, String> testConnection() {
        try {
            ConnectionParametersFileSystemBean config = adhocDao.createBeanFromNamedQry("getFileSystemConnectionParametersForInstanceId", ConnectionParametersFileSystemBean.class, false, id);
            IFileDelegator file;
            if (config.isRemote) {
                NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(config.domain, config.user, config.password);
                file = new SmbFileDelegator(PumpFileUtils.fixPathIfNessecary(config.path), auth);
            } else {
                file = new LocalFileDelegator(config.path);
            }
            if (!file.isDirectory()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Failed: target is not a folder.");
            }
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, "Success. There are " + file.getFileCount() + " objects.");
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in File System test connection", ex);
            }
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Failed: " + ex.getMessage());
        }
    }
}
