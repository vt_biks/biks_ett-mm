
exec sp_check_version '1.1.8.8';
go

if not exists (select * from bik_app_prop where name = 'myBIKSDemoMsg' )

begin

insert into bik_app_prop (name, val, is_editable)
	values(
		'myBIKSDemoMsg',
		'<h3>Witaj w systemie BI Knowledge System � wersja demonstracyjna </h3> BI Knowledge System jest narz�dziem, kt�re pozwala na zarz�dzanie wiedz� zwi�zan� z systemami Business Intelligence.Wersja demonstracyjna aplikacji jest w pe�ni funkcjonalnym programem, kt�ry umo�liwia normaln� prac� w systemie i pozwala pozna� korzy�ci p�yn�ce z jego u�ytkowania. Zmiany zapisane przez u�ytkownika odwiedzaj�cego wersj� demonstracyjn� BI Knowledge System b�d� usuwane na koniec ka�dego dnia roboczego. Wszystkie zawarte w niej dane s� przyk�adowe. Tre�ci opis�w obiekt�w mog� nie by� zgodne z prawd�, a zbie�no�� nazwisk u�ytkownik�w jest przypadkowa.',
		0	
	);

end


if not exists  (select * from bik_app_prop where name = 'myBIKSGeneralMsg')

begin
	
insert into bik_app_prop (name, val, is_editable)
	values(
		'myBIKSGeneralMsg',
		'<h3>Witaj w systemie BI Knowledge System</h3> Jest to narz�dzie, kt�re pozwala na zarz�dzanie wiedz� zwi�zan� z systemami Business Intelligence. Umo�liwia gromadzenie w jednymi miejscu og�lnej i szczeg�owej wiedzy zwi�zanej z wykorzystaniem BI w firmie w taki spos�b, aby brak dost�pu do os�b ze specjalistyczn� wiedz� nie powodowa� zatrzymania proces�w zwi�zanych z podejmowaniem decyzji. BI Knowledge System pomaga r�wnie� budowa� spo�eczno�� u�ytkownik�w BI daj�c im mo�liwo�� nieskr�powanej komunikacji i wymiany wiedzy.',
		0	
	);	
end	
		
exec sp_update_version '1.1.8.8', '1.1.8.8#A';
go
	