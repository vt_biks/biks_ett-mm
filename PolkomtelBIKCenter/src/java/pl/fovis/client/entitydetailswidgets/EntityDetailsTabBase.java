/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.HTML;
import java.util.Date;
import java.util.Set;
import pl.fovis.client.IEntityDetailsWidgetsContainer;
import pl.fovis.common.BIKCenterUtils;
import simplelib.SimpleDateUtils;

/**
 *
 * @author bfechner
 */
public abstract class EntityDetailsTabBase extends EntityDetailsWidgetFlatBase implements IEntityDetailsWidgetTabReady {

    protected IEntityDetailsWidgetsContainer edwc;

    protected HTML createDateAdded(Date dateAdded) {
        HTML dateAddedHTML = new HTML(SimpleDateUtils.toCanonicalString(dateAdded));
        dateAddedHTML.setStyleName("Notes" /* i18n: no:css-class */ + "-dateAdded");
        return dateAddedHTML;
    }

    protected HTML createUserName(String userName, String loginName,/*
             * boolean activ, String oldLoginName,
             */ String isAdmin, String optCssClass) {
        HTML userNameHTML = new HTML(BIKCenterUtils.getBikUserNodeName(loginName, userName/*
                 * , activ, oldLoginName
                 */) + " " + isAdmin);
        if (optCssClass != null) {
            userNameHTML.addStyleName(optCssClass);
        }
        return userNameHTML;
    }

    @Override
    public void setEntityDetailsWidgetsContainer(IEntityDetailsWidgetsContainer edwc) {
        this.edwc = edwc;
    }

    protected void refreshTabCaption() {
        edwc.refreshTabCaption(this);
    }

    protected Set<String> getAdditionalsTreeKind() {
        return null;
    }
}
