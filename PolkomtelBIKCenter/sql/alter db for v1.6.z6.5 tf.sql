﻿exec sp_check_version '1.6.z6.5';
go

if not exists(select 1 from bik_app_prop where name = 'showFullPathInJoinedObjsPane')
begin
	insert into bik_app_prop(name,val, is_editable)
	values('showFullPathInJoinedObjsPane', 'false', 1)
end;
go

exec sp_update_version '1.6.z6.5', '1.6.z6.6';
go