/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.mltx;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author lbiegniewski
 */
public class MultiXRegistrationDetailsBean extends MultiRegistrationBean implements Serializable {
    public Integer id;
    public String token;
    public Integer isValid;
    public Date createdAt;
    public Date expiresAt;
    public Integer status;
    public String caption;
    public String filePath;
    public String restorePrefix;
    public String lang;
}
