/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.lucene;

import pl.fovis.server.lucene.BikLuceneFullTextSearcher;
import commonlib.LameUtils;
import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.FSDirectory;
import pl.fovis.server.lucene.NodeIndexingTaskData.TaskKind;
import pl.fovis.server.IBOXIServiceForSearch;
import simplelib.BaseUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class BikLuceneSearcherHelperThread extends Thread {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    public static final int INDEX_VER = 16;
    private static final int PACK_SIZE = 100;
    private BlockingQueue<NodeIndexingTaskData> unindexedPending = new LinkedBlockingQueue<NodeIndexingTaskData>();
    private IBOXIServiceForSearch service;
    private StandardAnalyzer analyzer;
    private File luceneDirFile;
    private IndexWriter writer;
    private boolean mustCreateNewIndex;
    //do sprawdzenia
    private int currentIndexInDB;

    public BikLuceneSearcherHelperThread(IBOXIServiceForSearch service, StandardAnalyzer analyzer, File luceneDirFile, int currentIndexInDB) {
        this.service = service;
        this.analyzer = analyzer;
        this.luceneDirFile = luceneDirFile;
        this.mustCreateNewIndex = (currentIndexInDB != INDEX_VER || !luceneDirFile.exists());
        this.currentIndexInDB = currentIndexInDB;
    }

    public void resetIndex() {
        if (mustCreateNewIndex) {
            service.setIndexVerIntoBikNode(null, -1);
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                NodeIndexingTaskData dr = unindexedPending.take();
                processDocument(dr);
            }
        } catch (InterruptedException ex) {
            //Logger.getLogger(BIKLuceneFullTextSearcher.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            if (logger.isDebugEnabled()) {
                logger.debug("exit while in BikFullTextSearcher.run()");
            }
        }
    }

    protected void dumpNodeIndexingTaskData(NodeIndexingTaskData dr) {
//        if (logger.isDebugEnabled()) logger.debug("processDocument: nodeId=" + dr.nodeId + ", name=" + dr.name
//                + ", isDeleted=" + dr.isDeleted + ", indexVer=" + dr.indexVer + ", "
//                + "treeCode=" + dr.treeCode + ", descr=" + dr.description);
    }

    protected void processDocument(NodeIndexingTaskData dr) {
        try {
            buildWriterIfNeeded();

            dumpNodeIndexingTaskData(dr);

            boolean isAddUpdateOrDelete = dr.kind == TaskKind.AddUpdateOrDelete;

//            if (logger.isDebugEnabled()) {
//                logger.debug("FTS: processDocument: dr.nodeId=" + dr.nodeId);
//            }

            switch (dr.kind) {
                case AddUpdateOrDelete:
                    if (dr.isDeleted) {
                        writer.deleteDocuments(makeDeleteTermForNode(dr.nodeId));
                    } else if (dr.indexVer < 0) {
                        Document rresult = buildDoc(dr);
                        writer.updateDocument(makeDeleteTermForNode(dr.nodeId), rresult);
                    } else {
                        Document rresult = buildDoc(dr);
                        writer.addDocument(rresult);
                    }
                    break;
                case DeleteTree:
                    Term t = new Term("treeCode", dr.treeCode);
                    writer.deleteDocuments(t);
                    break;
            }
//            if (dr.isDeleted) {
//                writer.deleteDocuments(makeDeleteTermForNode(dr.nodeId));
//            } else if (dr.indexVer == null) {
//                Term t = new Term("treeCode", dr.treeCode);
//                writer.deleteDocuments(t);
//            } else if (dr.indexVer < 0) {
//                writer.updateDocument(makeDeleteTermForNode(dr.nodeId), rresult);
//            } else {
//                writer.addDocument(rresult);
//            }

            if (isAddUpdateOrDelete) {
//                if (logger.isDebugEnabled()) {
//                    logger.debug("FTS: processDocument:setIndexVerIntoBikNode: dr.nodeId=" + dr.nodeId);
//                }
                service.setIndexVerIntoBikNode(Collections.singleton(dr.nodeId), INDEX_VER);
            }
        } catch (Exception ex) {
            service.finishWorkUnit(false);
            throw new RuntimeException(ex);
        }

        if (unindexedPending.size() % PACK_SIZE == 0) {
            if (logger.isInfoEnabled()) {
                logger.info("dr: " + dr.nodeId);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("FTS: finishing work unit");
            }
            service.finishWorkUnit(true);
            closeWriter();
        }
    }

//    private boolean mustCreateNewIndex() {
//        boolean res = currentIndexInDB != INDEX_VER;
//        //if (logger.isDebugEnabled()) logger.debug("in mustCreateNewIndex: res=" + res);
//        return res;
//    }
    private void buildWriter() {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("mustCreateNewIndex: " + mustCreateNewIndex + " currentIndexInDB: " + currentIndexInDB + "");
            }
//            writer = new IndexWriter(
//                    FSDirectory.open(luceneDirFile),
//                    analyzer,
//                    mustCreateNewIndex,
//
//                    IndexWriter.MaxFieldLength.UNLIMITED);
            IndexWriterConfig writerConfig = new IndexWriterConfig(BikLuceneFullTextSearcher.LUCENE_VER, analyzer);
            writerConfig.setOpenMode((mustCreateNewIndex) ? IndexWriterConfig.OpenMode.CREATE : IndexWriterConfig.OpenMode.CREATE_OR_APPEND);

            writer = new IndexWriter(FSDirectory.open(luceneDirFile), writerConfig);

            if (mustCreateNewIndex) {
                mustCreateNewIndex = false;
                service.setIndexVer(INDEX_VER);
                service.finishWorkUnit(true);
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public int unindexedPendingCount() {
        return unindexedPending.size();
    }

    public void addToUnindexedPending(List<NodeIndexingTaskData> newDocs) {
        if (logger.isDebugEnabled()) {
            logger.debug("----- adding to UnindexedPending: " + newDocs.size() + " -----");
        }

        for (NodeIndexingTaskData nitd : newDocs) {
            dumpNodeIndexingTaskData(nitd);
        }

        unindexedPending.addAll(newDocs);
    }

    private Document buildDoc(NodeIndexingTaskData result) {
        //create document object
        Document document = new Document();
        //create field objects and add to document
        Field nodeIdField = new Field("nodeId",
                Integer.toString(result.nodeId), Field.Store.YES,
                Field.Index.NOT_ANALYZED);
        document.add(nodeIdField);

        Field nameField = new Field("name",
                result.name, Field.Store.YES,
                Field.Index.ANALYZED);
//        do odkomentowania
//        nameField.setBoost(new Float(result.rangeInSearch + new Float(1)));
        document.add(nameField);

        if (!BaseUtils.isStrEmptyOrWhiteSpace(result.description)) {
            Field descriptionField = new Field("description",
                    result.description, Field.Store.YES,
                    Field.Index.ANALYZED);
//                    do odkomentowania
//            descriptionField.setBoost(new Float(result.rangeInSearch + new Float(0.5)));
            document.add(descriptionField);
        }

//        if (!BaseUtils.isStrEmptyOrWhiteSpace(result.keyWords)) {
//            Field keyWordsField = new Field("keyWords",
//                    result.keyWords, Field.Store.YES,
//                    Field.Index.ANALYZED);
//            if (logger.isDebugEnabled()) {
//                logger.debug("----- keyWords: " + result.keyWords + " -----");
//            }
//            cos2 = 0.5f + result.rangeInSearch;
//            keyWordsField.setBoost(cos2);
//            document.add(keyWordsField);
//        }

        Field nodeKindCodeField = new Field("nodeKindCode",
                result.nodeKindCode, Field.Store.YES,
                Field.Index.NOT_ANALYZED);
        document.add(nodeKindCodeField);

        Field nodeKindCaptionField = new Field("nodeKindCaption",
                result.nodeKindCaption, Field.Store.YES,
                Field.Index.NOT_ANALYZED);
        document.add(nodeKindCaptionField);

        Field treeNameField = new Field("treeName",
                result.treeName, Field.Store.YES,
                Field.Index.NOT_ANALYZED);
        document.add(treeNameField);

        Field treeCodeField = new Field("treeCode",
                result.treeCode, Field.Store.YES,
                Field.Index.NOT_ANALYZED);
        document.add(treeCodeField);

//        if (logger.isDebugEnabled()) logger.debug("name: " + result.name + " name range: " + cos1 + " description range: " + cos2);



        return document;
    }

    protected Term makeDeleteTermForNode(int nodeId) {
        return new Term("nodeId", Integer.toString(nodeId));
    }

    protected void buildWriterIfNeeded() {
        if (writer == null) {
            buildWriter();
        }
    }

    private void closeWriter() {
        try {
            writer.optimize();
            writer.commit();
            writer.close();
            writer = null;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
