/*
 * JavaScriptUtilsTest.java
 * JUnit based test
 *
 * Created on 30 październik 2007, 12:54
 */

package commonlib;

import simplelib.JavaScriptExprBuilder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import junit.framework.*;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class JavaScriptUtilsTest extends TestCase {
    
    public JavaScriptUtilsTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
    }

    protected void tearDown() throws Exception {
    }

    /**
     * Test of escapeJavaScriptAndQuote method, of class commonlib.JavaScriptExprBuilder.
     */
    public void testEscapeJavaScriptAndQuote() {
        assertEquals("\"ala\"", JavaScriptExprBuilder.escapeJavaScriptAndQuote("ala"));
        assertEquals("null", JavaScriptExprBuilder.escapeJavaScriptAndQuote(null));
    }

    /**
     * Test of mapConstant method, of class commonlib.JavaScriptExprBuilder.
     */
    public void testMapConstant() {
        Map<String, Object> m = new LinkedHashMap();
        m.put("a", "x");
        m.put("b", null);
        m.put("c", "z");
        assertEquals("{\"a\":\"x\",\"b\":null,\"c\":\"z\"}", JavaScriptExprBuilder.mapConstant(m));
        assertEquals("{}", JavaScriptExprBuilder.mapConstant(new HashMap()));
        assertEquals("null", JavaScriptExprBuilder.mapConstant(null));
    }
    
}
