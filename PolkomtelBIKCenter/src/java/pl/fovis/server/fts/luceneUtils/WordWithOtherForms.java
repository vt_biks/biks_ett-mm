/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts.luceneUtils;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author pmielanczuk
 */
public class WordWithOtherForms {

    public Set<String> words = new LinkedHashSet<String>();

    public WordWithOtherForms() {
    }

    public WordWithOtherForms(String baseWord) {
        words.add(baseWord);
    }

    @Override
    public String toString() {
        return "WordWithOtherForms{" + "words=" + words + '}';
    }

}
