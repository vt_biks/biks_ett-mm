/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class DBMssqlIndexBean implements Serializable {

    public String name;
    public String type;
    public boolean isUnique;
    public String columnName;
    public int columnPosition;
    public boolean isPrimaryKey;
    public int partitionOrdinal;
    public String partitionColumnName;
    //
    public String icon;
    public Integer nodeId;
    public String treeCode;

    public DBMssqlIndexBean() {
    }

    public DBMssqlIndexBean(String name, String type, String columnName, String partitionColumnName, String icon) {
        this.name = name;
        this.type = type;
        this.columnName = columnName;
        this.partitionColumnName = partitionColumnName;
        this.icon = icon;
    }
}
