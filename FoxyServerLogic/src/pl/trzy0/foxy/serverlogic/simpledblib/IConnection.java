/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.serverlogic.simpledblib;

/**
 *
 * @author wezyr
 */
public interface IConnection {
    public int execCommand(String sqlTxt);
    public void startBatch(String sqlTxt, int[] paramTypes);
    public void addToBatch(Object[] parVals);
    public void execBatch();
    public void clearBatch();
}
