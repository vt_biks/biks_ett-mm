/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.lisa;

//import com.teradata.jdbc.jdbc_4.util.JDBCException;
import commonlib.LameUtils;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.TeradataException;
import pl.fovis.server.LisaServiceImpl.CancallableRunner;
import pl.fovis.server.LisaServiceImpl.ConnectionRemover;
import pl.trzy0.foxy.commons.FoxyRuntimeException;
import pl.trzy0.foxy.commons.IBeanConnectionThreadHandle;
import pl.trzy0.foxy.serverlogic.db.TeradataConnection;
import simplelib.IContinuationWithReturn;
import simplelib.IParametrizedContinuation;
import simplelib.IParametrizedContinuationWithReturn;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pku
 */
public class AuthorizedTeradataConnection {

    private final TeradataConnection connection;
    private final ConnectionRemover remover;
    private static final ILameLogger logger = LameUtils.getMyLogger();
    private final CancallableRunner cancellableRunner;

    public AuthorizedTeradataConnection(TeradataConnection conn, ConnectionRemover remover, CancallableRunner cancellableRunner) {
        this.connection = conn;
        this.remover = remover;
        this.cancellableRunner = cancellableRunner;
    }

    public List<Map<String, Object>> execQry(String qryTxt) throws TeradataException {
        try {
            return connection.execQry(qryTxt);
        } catch (RuntimeException e) {
            throw closeAndRemoveConnection(e);
        }
    }

    public <T> List<T> createBeansFromQry(String qryTxt, final Class<T> beanClass) throws TeradataException {
        try {
            return connection.createBeansFromQry(qryTxt, beanClass);
        } catch (RuntimeException e) {
            throw closeAndRemoveConnection(e);
        }

    }

    private Throwable getRootException(Throwable e) {
        if (!(e instanceof RuntimeException)) {
            return e;
        }
        Throwable cause = e.getCause();
        if (cause == null) {
            return e;
        }
        return getRootException(cause);

    }

    private TeradataException closeAndRemoveConnection(Exception e) {
        //remover.removeConnection();
        Throwable rootCause = getRootException(e);
        if (logger.isWarnEnabled()) {
            logger.warn("[" + "LISA] error connecting to Teradata" /* i18n: no */, rootCause);
        }

        TeradataException retE;
        if (rootCause instanceof UnknownHostException) {
            retE = new TeradataException(I18n.nieznanySerwerBazyDanych.get() /* i18n:  */);
        } else if (rootCause instanceof SocketTimeoutException) {
            retE = new TeradataException(I18n.serwerBazyDanychNieOdpowiada.get() /* i18n:  */);
        } else {//if (rootCause instanceof JDBCException) {
            String mes = rootCause.getMessage();
            retE = new TeradataException(mes);
            //jeśli zle haslo lub login, wtedy inaczej obslugiwane
            if (mes != null && mes.contains("Error 8017")) {
                retE.setIsBlocking(false);
            }
            remover.logError(rootCause.getMessage());
        }
//        } else {
//            retE = new TeradataAuthorizationException(rootCause.getMessage());
//            //retE.setIsBlocking(false);
//            remover.logError(rootCause.getMessage());
//        }
        return retE;
    }

    public String valToString(Object value) {

        if (value != null && value instanceof java.util.Date) {
            return "'" + LisaServerUtils.getDateStringInDefaultDateFormat((java.util.Date) value) + "'";
        } else {
            return connection.valToString(value);
        }
    }

    public Object execQrySingleVal(String qryTxt) throws TeradataException {
        return execQrySingleValEx(qryTxt, false);
    }

    public Object execQrySingleValEx(String qryTxt, boolean allowNoResults) throws TeradataException {
        try {
            return connection.execQrySingleVal(qryTxt, allowNoResults);
        } catch (RuntimeException e) {
            throw closeAndRemoveConnection(e);
        }
    }

    public void execWithJdbcConnection(IParametrizedContinuation<Connection> iParametrizedContinuation) throws TeradataException {
        try {
            connection.execWithJdbcConnection(iParametrizedContinuation);
        } catch (RuntimeException e) {
            throw closeAndRemoveConnection(e);
        }
    }

    public <T> T execWithJdbcConnection(IParametrizedContinuationWithReturn<Connection, T> iParametrizedContinuation) throws TeradataException {
        try {
            return connection.execWithJdbcConnection(iParametrizedContinuation);
        } catch (RuntimeException e) {
            throw closeAndRemoveConnection(e);
        }
    }

    public List<Map<String, Object>> runCancellableQry(final String query, String ticket) throws TeradataException {
        IBeanConnectionThreadHandle handle = connection.getCurrentThreadHandle();
        try {
            List<Map<String, Object>> list = cancellableRunner.runCancellableJob(ticket, handle, new IContinuationWithReturn<List<Map<String, Object>>>() {
                public List<Map<String, Object>> doIt() {
                    return connection.execQry(query);
                }
            });
            return list;
        } catch (RuntimeException e) {
            throw closeAndRemoveConnection(e);
        }

    }

    public String getTeradataMode() {
        return connection.getTeradataMode();
    }

    public void execQryWithoutParamsWithQueryBand(final String query, final String queryBandCmd) throws TeradataException {
        execWithQueryBand(queryBandCmd, new IParametrizedContinuation<Connection>() {

            @Override
            public void doIt(Connection conn) {
                try {
                    Statement stmt = conn.createStatement();
                    stmt.execute(query);
                } catch (SQLException ex) {
                    throw new FoxyRuntimeException(ex.getMessage());
                }
            }
        });
    }

    public void execWithQueryBand(final String queryBandCmd, final IParametrizedContinuation<Connection> doExecCont) throws TeradataException {
        execWithJdbcConnection(new IParametrizedContinuation<Connection>() {
            @Override
            public void doIt(Connection conn) {
                Statement stmt = null;
                String tmode = getTeradataMode();

                try {
                    conn.commit();

                    stmt = conn.createStatement();
                    try {
                        if (tmode.equals("TERA")) {
                            stmt.execute("BT");
                        }

                        //Dodaj QUERY_BAND
                        stmt.execute(queryBandCmd);
                        conn.setAutoCommit(false);
                        doExecCont.doIt(conn);
                    } finally {
                        if (stmt != null) {
                            stmt.execute("SET QUERY_BAND = NONE for TRANSACTION");
                            if (tmode.equals("TERA")) {
                                stmt.execute("ET");
                            }
                            stmt.close();
                        }
                    }
                    conn.commit();
                } catch (Exception ex) {
                    try {
                        conn.rollback();
                    } catch (SQLException exIn) {
                        //Trzeba to przerobic, aby CallableStatement był w pl.trzy0.foxy.serverlogic.db, szybka poprawka dla Jerzego
                        //inaczej sie nie wygrzebę z tego NAS-a, przy NAS etap2 to zmienić!!!!
                    }
                    throw new FoxyRuntimeException(ex.getMessage());
                }
            }
        });
    }
}
