﻿exec sp_check_version '1.1.8.1';
go

create table bik_tutorial(
id int identity(1,1) not null primary key,
title varchar(255) not null,
text varchar(max) not null,
);
go

declare @table_name sysname = 'bik_statistic_dict', @column_name sysname = 'description'
declare @idx_name sysname
select @idx_name = i.name
from sys.indexes i
inner join sys.index_columns ic on i.index_id = ic.index_id and ic.object_id = i.object_id
inner join sys.columns c on ic.object_id = c.object_id and ic.column_id = c.column_id
inner join sys.objects o on i.object_id = o.object_id
where o.name = @table_name and c.name = @column_name
exec ('alter table ' +  @table_name + ' drop constraint ' + @idx_name)
go

------------------------------------------------------------------------------------------------------------
--------------------------------------SAS-------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------

insert into bik_tree (name, node_kind_id, code, tree_kind) values ('Raporty', null, 'SASReports', 'Metadata')
insert into bik_tree (name, node_kind_id, code, tree_kind) values ('Mapy Informacyjne', null, 'SASInformationMaps', 'Metadata')
insert into bik_tree (name, node_kind_id, code, tree_kind) values ('Biblioteki SAS', null, 'SASLibraries', 'Metadata')
insert into bik_tree (name, node_kind_id, code, tree_kind) values ('OLAP', null, 'SASCubes', 'Metadata')

exec sp_update_version '1.1.8.1', '1.1.8.2';
go
