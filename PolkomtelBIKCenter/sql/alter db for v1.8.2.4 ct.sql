﻿exec sp_check_version '1.8.2.4';
go
 
if exists(select * from bik_app_prop where name = 'allowDuplicateJoinedObjs')
update bik_app_prop set val = 'false' where name = 'allowDuplicateJoinedObjs';
else 
insert into bik_app_prop(name, val) values('allowDuplicateJoinedObjs', 'false')

exec sp_update_version '1.8.2.4', '1.8.2.5';
go

 