exec sp_check_version '1.0.16';
go
----------------------------------------------------------------------
----------------------------------------------------------------------
declare @sqltxt varchar(8000) = 
(select 'alter table bik_tree drop constraint ' + cc.name
from sys.check_constraints cc inner join syscolumns sc on cc.parent_object_id = sc.id and cc.parent_column_id = sc.colid
where cc.parent_object_id = OBJECT_ID('bik_tree')
  and sc.name = 'tree_kind'
)
  
execute(@sqltxt)
go

alter table bik_tree add constraint CK_bik_tree_tree_kind check(tree_kind in (0, 1, 2, 3))
go
----------------------------------------------------------------------
----------------------------------------------------------------------
declare @nodeKindId int
select @nodeKindId = id from bik_node_kind where code = 'Folder'
insert into bik_tree(name, tree_kind, node_kind_id)
values('Załączniki', 3, @nodeKindId)
go
----------------------------------------------------------------------
----------------------------------------------------------------------
insert into bik_node_kind(code, caption)
values('Attachment', 'Załącznik')
go
----------------------------------------------------------------------
----------------------------------------------------------------------
declare @nodeKindId int
declare @treeId int
select @nodeKindId = id from bik_node_kind where code = 'Folder'
select @treeId = id from bik_tree where name = 'Załączniki'
insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id, branch_ids, obj_id, descr, is_deleted)
values(null, @nodeKindId, 'Wszystkie załączniki', @treeId, null, null, null, null, 0)
go

update bik_node
set branch_ids = convert(varchar, id) + '|'
where name = 'Wszystkie załączniki'
go
----------------------------------------------------------------------
----------------------------------------------------------------------
declare @nodeKindId int
declare @parentNodeId int
declare @parentBranchIds varchar(1000)
declare @treeId int
select @nodeKindId = id from bik_node_kind where code = 'Attachment'
select @parentNodeId = id, @parentBranchIds = branch_ids from bik_node where name = 'Wszystkie załączniki'
select @treeId = id from bik_tree where name = 'Załączniki'

insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id, branch_ids, obj_id, descr, is_deleted)
select @parentNodeId, @nodeKindId, caption, @treeId, null, @parentBranchIds, id, null, 0
from bik_attachment
go
----------------------------------------------------------------------
----------------------------------------------------------------------
insert into bik_joined_objs(src_node_id, dst_node_id)
select ba.node_id, bn.id
from bik_node bn join bik_attachment ba on bn.obj_id = convert(varchar, ba.id)
go
----------------------------------------------------------------------
----------------------------------------------------------------------
update bik_attachment
set node_id = bn.id
from bik_node bn join bik_attachment ba on bn.obj_id = convert(varchar, ba.id)
go
----------------------------------------------------------------------
----------------------------------------------------------------------
declare @parentNodeId int
declare @parentBranchIds varchar(1000)
select @parentNodeId = id, @parentBranchIds = branch_ids from bik_node where name = 'Wszystkie załączniki'
update bik_node
set obj_id = null, branch_ids = @parentBranchIds + convert(varchar, id) + '|'
where  parent_node_id = @parentNodeId
go
----------------------------------------------------------------------
----------------------------------------------------------------------
exec sp_update_version '1.0.16', '1.0.17';
go

