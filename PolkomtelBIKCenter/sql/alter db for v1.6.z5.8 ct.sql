exec sp_check_version '1.6.z5.8';
go

declare @treeId int = (select id from bik_tree where code = 'DictionaryDWH');

update bik_tree
set tree_kind = 'Dictionary' where id = @treeId;

set @treeId = (select id from bik_tree where code = 'TreeOfTrees');
update bik_node
set is_deleted = 0 where (tree_id = @treeId) and (obj_id in ('&DWHCategorization', '&DWHCategory', '&DWHDictionary'))

declare @parentId int = (select top 1 id from bik_node where obj_id = '$DictionaryDWH' and is_deleted = 0 and tree_id = @treeId);
declare @nodeId int = (select id from bik_node where obj_id = '&DWHDictionary' and tree_id = @treeId);

update bik_node 
set parent_node_id = @parentId
where id = @nodeId;

exec sp_node_init_branch_id @treeId, null
go

exec sp_update_version '1.6.z5.8', '1.6.z5.9';
go
