/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.PrintDialog;
import pl.fovis.common.PrintTemplateBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author bfechner
 */
public class NodeActionPrintDocument extends NodeActionUpdatingBase {

    @Override
    public boolean isEnabledByDesign() {
        if (!BIKClientSingletons.isEnablePrinting()) {
            return false;
        }

        if (treeBean == null) {
            return false;
        }
//        return (BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_IMPORT_TREE, treeBean.id)
//                || BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_TO_HTML, treeBean.id)
//                || BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_TO_PDF, treeBean.id))
//                //                && !BIKClientSingletons.disableImportExport()
//                //  && !BaseUtils.isMapEmpty(data.printTemplates)
//                && !"DictionaryDWH".equalsIgnoreCase(treeBean.code) &&
        return !isMetaBiks();

    }

    @Override
    public String getCaption() {
        return I18n.drukuj.get();
    }

    @Override
    public void execute() {

        BIKClientSingletons.getService().getPrintTemplates(node.nodeKindCode, new StandardAsyncCallback<Map<String, PrintTemplateBean>>() {
            @Override
            public void onSuccess(Map<String, PrintTemplateBean> result) {
                PrintDialog dialog = new PrintDialog(node.id, node.name, BIKClientSingletons.getTreeIdOfCurrentTab(), node.nodeKindId, node.nodeKindCode, result);
                dialog.buildAndShowDialog();
            }
        });
    }

}
