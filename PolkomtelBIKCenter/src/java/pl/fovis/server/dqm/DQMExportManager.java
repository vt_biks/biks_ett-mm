/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import commonlib.UrlMakingUtils;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import pl.fovis.common.dqm.DQMConstanst;
import pl.fovis.common.dqm.DataQualityRequestBean;
import pl.fovis.server.DQMServiceImpl;
import pl.fovis.server.ExportManagerBase;
import simplelib.ILameCollector;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class DQMExportManager extends ExportManagerBase {

    protected DQMServiceImpl service;

    public DQMExportManager(DQMServiceImpl service) {
        this.service = service;
    }

    protected String getFileNamePrefix(long longRequestId) {
        DataQualityRequestBean dqmTestRequest = service.getDqmTestRequest(longRequestId);
        String encodedTestName = UrlMakingUtils.encodeStringForUrlWW(dqmTestRequest.testName).replace("+", "%20");
        String fileNamePrefix = encodedTestName + "_" + SimpleDateUtils.dateTimeToStringNoSep(dqmTestRequest.startTimestamp);
        return fileNamePrefix;
    }

    boolean useLowMem = true;

    @Override
    protected Pair<String, IParametrizedContinuation<ILameCollector<Map<String, Object>>>> optGetFileNamePrefixAndContentProducer(HttpServletRequest req) {
        if (!useLowMem) {
            return null;
        }

        String requestId = req.getParameter(DQMConstanst.DQM_EXPORT_REQUEST_ID_PARAM_NAME);
        final Long longRequestId = Long.valueOf(requestId);
        return new Pair<String, IParametrizedContinuation<ILameCollector<Map<String, Object>>>>(getFileNamePrefix(longRequestId), new IParametrizedContinuation<ILameCollector<Map<String, Object>>>() {

            @Override
            public void doIt(ILameCollector<Map<String, Object>> param) {
                service.getDQMRequestErrors(longRequestId, param);
            }
        });
    }

    @Override
    protected Pair<String, List<Map<String, Object>>> getFileNamePrefixAndContent(HttpServletRequest req) {
        String requestId = req.getParameter(DQMConstanst.DQM_EXPORT_REQUEST_ID_PARAM_NAME);
        Long longRequestId = Long.valueOf(requestId);
        List<Map<String, Object>> content = service.getDQMRequestErrors(longRequestId);

//        DataQualityRequestBean dqmTestRequest = service.getDqmTestRequest(longRequestId);
//        String encodedTestName = UrlMakingUtils.encodeStringForUrlWW(dqmTestRequest.testName).replace("+", "%20");
//        String fileNamePrefix = encodedTestName + "_" + SimpleDateUtils.dateTimeToStringNoSep(dqmTestRequest.startTimestamp);
        return new Pair<String, List<Map<String, Object>>>(getFileNamePrefix(longRequestId), content);
    }

    @Override
    protected String getFileFormat(HttpServletRequest req) {
        return req.getParameter(DQMConstanst.DQM_EXPORT_PARAM_NAME);
    }
}
