/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class DefDialog extends BaseActionOrCancelDialog {

    private String text;
    private IParametrizedContinuation<Boolean> iParametrizedContinuation;

    @Override
    protected String getDialogCaption() {
        return "";
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */; //I18n.zatwierdz.get() /* i18n:  */;
    }

    @Override
    protected void buildMainWidgets() {
        VerticalPanel hp = new VerticalPanel();
        hp.setWidth("500px");
        HTML def = new HTML(text);
        def.setStyleName("komunikat" /* I18N: no:css-class */);
        hp.add(def);
        main.add(hp);
    }

    @Override
    protected void doAction() {
        iParametrizedContinuation.doIt(true);
    }

    @Override
    protected void doCancel() {
        iParametrizedContinuation.doIt(false);
    }

    public void buildAndShowDialog(String text, IParametrizedContinuation<Boolean> iParametrizedContinuation) {
        this.text = text;
        this.iParametrizedContinuation = iParametrizedContinuation;
        super.buildAndShowDialog();
    }

    @Override
    protected String getCancelButtonCaption() {
        return I18n.anuluj.get() /* I18N:  */;//I18n.zamknij.get() /* I18N:  *//*"Anuluj"*/;
    }
}
