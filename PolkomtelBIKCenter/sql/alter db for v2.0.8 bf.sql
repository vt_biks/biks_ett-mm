exec sp_check_version '2.0.8';
GO


if not exists (select 1 from bik_app_prop where name='enablePrinting' )
insert into bik_app_prop(name, val) values ('enablePrinting', 'true')
else 
update bik_app_prop set val='true' where name='enablePrinting'
go 


declare @actionId int;
if not exists (select * from bik_node_action where code = 'PrintDocument')
begin

insert into bik_node_action (code) values ('PrintDocument')
--declare @actionId int;
select  @actionId=id from bik_node_action where code='PrintDocument'

insert into bik_node_action_in_custom_right_role (action_id,role_id)
select distinct @actionId,role_id from bik_node_action_in_custom_right_role
where action_id = (select id from bik_node_action where code='Rename')

end

if not exists (select * from bik_node_action where code = 'SaveTree')
begin

insert into bik_node_action (code) values ('SaveTree')
--declare @actionId int;
select  @actionId=id from bik_node_action where code='SaveTree'

insert into bik_node_action_in_custom_right_role (action_id,role_id)
select distinct @actionId,role_id from bik_node_action_in_custom_right_role
where action_id = (select id from bik_node_action where code='Rename')

end



if not exists (select * from bik_node_action where code = 'AddAttributeForKind')
begin

insert into bik_node_action (code) values ('AddAttributeForKind')
--declare @actionId int;
select  @actionId=id from bik_node_action where code='AddAttributeForKind'

insert into bik_node_action_in_custom_right_role (action_id,role_id)
select distinct @actionId,role_id from bik_node_action_in_custom_right_role
where action_id = (select id from bik_node_action where code='Rename')
end

exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;



exec sp_update_version '2.0.8','2.0.9';
GO
