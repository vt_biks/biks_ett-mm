/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.tags;

import pl.foxys.foxymill.exprlang.IParsedExpression;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public abstract class TagWithExpr extends TagBase {

    protected IParsedExpression parsedExpr;

    protected abstract void renderWithExprVal(IRenderCtx renderCtx, Object exprVal);

    protected boolean isExprAttrRequired() {
        return true;
    }

    protected Object evalExprAttrVal(IRenderCtx renderCtx) {
        return parsedExpr == null ? null : parsedExpr.eval(renderCtx);
    }

    @Override
    public void internalRender(IRenderCtx renderCtx) {
        Object val = evalExprAttrVal(renderCtx);

        handleModelDrivenEvents(renderCtx);
        
        renderWithExprVal(renderCtx, val);
    }

    @Override
    protected void prepareSelf() {
        parsedExpr = attrToExpr("expr");
        if (parsedExpr == null && isExprAttrRequired()) {
            throw new LameRuntimeException("required expr attr is missing");
        }
    }
}
