/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.reportsdk;

import commonlib.LameUtils;
import commonlib.ProcessUtils;
import commonlib.StreamGobbler;
import java.io.File;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.sapbo.javasdk.SapBOBean;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class Report4Pump extends ReportPump {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_SAPBO4;
    }

    @Override
    protected Pair<Integer, String> startPump() {
        SapBOBean configBean = getAdhocDao().createBeanFromNamedQry("getBOConnectionParamatersForSource", SapBOBean.class, true, getSourceName(), instanceName);
        if (configBean.isBo40) { // zaślepka, nic się nie załaduje
            execNamedCommandWithCommit("createBOInstanceFunctions", getSourceName(), instanceName);
            execNamedCommandWithCommit("prepareMultiInstanceTablesForReportSDK");

            // łącze metadane
            pumpLogger.pumpLog("Adding associations between metadata", 98);
            execNamedCommandWithCommit("insertIntoJOTeradataMetadataConnections"); // for Teradata
            execNamedCommandWithCommit("insertIntoJOMSSQLMetadataConnections"); // for MS SQL
            execNamedCommandWithCommit("insertIntoJOOracleMetadataConnections"); // for Oracle

//        execNamedCommandWithCommit("finishMultiInstanceTablesForReportSDK"); //
        } else { // odpalam z lini poleceń pompkę do BO 4.1
            String path = execNamedQuerySingleVal("getConfigValue", true, "value", "sapbo4.reportPath");
            File file = new File(path);
            if (!file.exists()) {
                return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Wrong path or file (BIKSPumpSAPBO41ReportSDK.jar) does not exists!");
            }

            MssqlConnectionConfig connectionConfig = pumpLogger.getConnectionConfig();

            try {
//                String[] callAndArgs = {"cmd.exe", "/C", "java -jar \"" + path + "\"", connectionConfig.server, (connectionConfig.instance != null) ? connectionConfig.instance : "", connectionConfig.database, connectionConfig.user, connectionConfig.password, "\"" + instanceName + "\""};
                String[] callAndArgs = {"cmd.exe", "/C", "java -Xmx1024M -jar \"" + path + "\" " + connectionConfig.server + " " + (connectionConfig.instance == null ? "\"\"" : connectionConfig.instance) + " " + connectionConfig.database + " " + connectionConfig.user + " " + connectionConfig.password + " \"" + instanceName + "\""};
//                System.out.println("Polecenie: " + Arrays.toString(callAndArgs));
                Process p = ProcessUtils.executeProcess(callAndArgs);
                StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), "ERROR", true);
                StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), "OUTPUT", true);
                errorGobbler.start();
                outputGobbler.start();
                //
                p.waitFor();
                errorGobbler.join();
                String errorMsg = errorGobbler.getErrorMsg();
                if (errorMsg != null) {
                    return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, errorMsg);
                }
            } catch (InterruptedException ie) {
                if (logger.isErrorEnabled()) {
                    logger.error("InterruptedException Error: " + ie.getMessage(), ie);
                }
                throw new LameRuntimeException(ie);
            }
        }
        return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
    }
}
