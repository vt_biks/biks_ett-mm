/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import commonlib.ILameResourceProvider;
import commonlib.LameLoggersConfigReloader;
import commonlib.StringFromInputStreamResourceProvider;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.Map.Entry;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.IFileUploadParam;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.commons.SimpleFileUploadParam;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import simplelib.*;

/**
 *
 * @author wezyr
 */
public class FoxyServletUtils {

    private static final String LAMELOGGERS_CONFIG_PROPERTIES_FILENAME = "/WEB-INF/lameLoggersConfig.properties";
    private static boolean isLameLoggersConfigReaderInited = false;

    public static Map<String, Object> getMultiPartParamsAsMap(HttpServletRequest req,
            long fileSizeMax, boolean lazyGetContent, boolean emptyMapForNonMultiPart,
            Map<String, Object> res) {
        boolean isMultipart = ServletFileUpload.isMultipartContent(req);

        if (!isMultipart) {
            return emptyMapForNonMultiPart ? Collections.<String, Object>emptyMap() : null;
        }

        //Map<String, Object> res = new HashMap<String, Object>();

        // Create a factory for disk-based file items
        FileItemFactory factory = new DiskFileItemFactory();

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
        try {
            if (fileSizeMax >= 0) {
                upload.setFileSizeMax(fileSizeMax);
            }
            List /*
                     * FileItem
                     */ items = upload.parseRequest(req);

            // Process the uploaded items
            Iterator iter = items.iterator();
            while (iter.hasNext()) {
                final FileItem item = (FileItem) iter.next();

                if (item.isFormField()) {
                    // zwykłe pole
                    String name = item.getFieldName();
                    String value;
                    try {
                        value = item.getString("UTF-8");
                    } catch (UnsupportedEncodingException ex) {
                        throw new LameRuntimeException(ex);
                    }

                    res.put(name, value);

                } else {
                    IFileUploadParam fup;

                    if (lazyGetContent) {
                        fup = new LazyFileUploadParam(item);
                    } else {
                        // plik
                        String fileName = item.getName();
                        fup = new SimpleFileUploadParam(fileName, item.get(), item.getSize());
                    }
                    String fieldName = item.getFieldName();
                    res.put(fieldName, fup);
                }
            }

        } catch (FileUploadException ex) {
            //ex.printStackTrace();
            throw new LameRuntimeException(ex);
        }

        return res;
    }

    public static Map<String, Object> getParamsAsMapEx(HttpServletRequest req,
            long fileSizeMax) {
        @SuppressWarnings("unchecked")
        Map<String, Object> m = (Map) getParamsAsMap(req);
        getMultiPartParamsAsMap(req, fileSizeMax, true, false, m);
        return m;
    }

    public static Map<String, String> getParamsAsMap(HttpServletRequest req) {
        Map<String, String> res = new LinkedHashMap<String, String>();
        @SuppressWarnings("unchecked")
        Map<String, String[]> m = (Map<String, String[]>) req.getParameterMap();
        for (Entry<String, String[]> e : m.entrySet()) {
            String name = e.getKey();
            String[] val = e.getValue();
            if (val.length >= 1) {
                res.put(name, val[0]);
            }
        }

        return res;
    }

    // localna ścieżka do serwleta (i z nim!)
    public static String getLocalServletUrl(HttpServletRequest request) {
        return getLocalWebAppUrl(request) + request.getServletPath();
    }

    // localna ścieżka, ale tylko do webapp - bez serwleta!
    public static String getLocalWebAppUrl(HttpServletRequest request) {
        return request.getContextPath();
    }

    // pełna ścieżka aż do serwleta (i z nim!)
    public static String getRemoteServletUrl(HttpServletRequest request) {
        return getRemoteWebAppUrl(request) + request.getServletPath();
    }

    // pełna ścieżka, ale tylko do webapp - bez serwleta!
    // wynik nie zawiera "/" na końcu, czyli trzeba sobie dokleić
    public static String getRemoteWebAppUrl(HttpServletRequest request) {
        StringBuilder url = new StringBuilder();
        int port = request.getServerPort();
        if (port < 0) {
            port = 80; // Work around java.net.URL bug
        }
        String scheme = request.getScheme();
        url.append(scheme);
        url.append("://");
        url.append(request.getServerName());
        if ((scheme.equals("http") && (port != 80)) || (scheme.equals("https") && (port != 443))) {
            url.append(':');
            url.append(port);
        }
        url.append(request.getContextPath());
        return url.toString();
    }

    public static String getCookieValue(HttpServletRequest request,
            String cookieName,
            String defaultValue) {
        Cookie c = findCookie(request, cookieName);
        if (c == null) {
            return defaultValue;
        }
        return c.getValue();
    }

    public static Cookie findCookie(HttpServletRequest request,
            String cookieName) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                Cookie cookie = cookies[i];
                if (cookieName.equals(cookie.getName())) {
                    return cookie;
                }
            }
        }
        return null;
    }

    public static void setCookie(HttpServletRequest request, HttpServletResponse response,
            String cookieName, Object cookieValue, int maxAgeSeconds, String domain) {
        Cookie cookie = new Cookie(cookieName, BaseUtils.safeToString(cookieValue));
        cookie.setMaxAge(maxAgeSeconds);
        cookie.setPath(BaseUtils.isStrEmpty(request.getContextPath()) ? "/" : request.getContextPath());
        if (domain != null) {
            cookie.setDomain(domain);
        }
        response.addCookie(cookie);
    }

    public static String getWebAppRealNameByRealPath(String realPath) {
        realPath = realPath.replace("\\", "/");
        realPath = BaseUtils.dropOptionalSuffix(realPath, "/");

        return BaseUtils.getLastItems(realPath, "/", 1);
    }

    //NaqNaqApp
    public static String getWebAppRealName(ServletContext servletCtx) {
        String contextPath = servletCtx.getContextPath();
        if (BaseUtils.isStrEmptyOrWhiteSpace(contextPath)) {
            String realPath = servletCtx.getRealPath("/");
            return getWebAppRealNameByRealPath(realPath);
        }

        return BaseUtils.dropOptionalPrefix(contextPath, "/");
    }

    public static IGenericResourceProvider<String> getStringResourceProviderFromServlet(ServletContext servletCtx) {
        ILameResourceProvider resProvider = new ServletResouceProvider(servletCtx);
        return new StringFromInputStreamResourceProvider(resProvider, "UTF-8");
    }
    
    public static IResourceFromProvider<String> getResourceFromServlet(ServletContext servletCtx, String resourceName) {
        return new ResourceFromProvider<String>(getStringResourceProviderFromServlet(servletCtx), resourceName);
    }

    public static INamedSqlsDAO<Object> createNamedSqlsDAOForServlet(IBeanConnectionEx<Object> connection,
            ServletContext servletCtx, String resourceName) {
        return FoxyAppUtils.createNamedSqlsDAO(connection, new ServletResouceProvider(servletCtx), resourceName);
    }

    public static void setupLameLoggersConfigReader(ServletContext servletContext) {
        if (isLameLoggersConfigReaderInited) {
            return;
        }

        isLameLoggersConfigReaderInited = true;
        ILameResourceProvider resProvider = new ServletResouceProvider(servletContext);

        LameLoggersConfigReloader.setLameLoggersConfigResource(new WatchedResourceFromProvider<InputStream>(
                new ResourceFromProvider<InputStream>(resProvider, LAMELOGGERS_CONFIG_PROPERTIES_FILENAME)));
    }
}
