------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- 1. usuniÍcie tabeli mltx_question.
-- 2. przeniesienie kolumny is_visible do question_lang
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

------------------------------------------------------

exec sp_mltx_check_version '1.7'
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

alter table mltx_question_lang add is_visible smallint not null
go

update ql
set is_visible = q.is_visible
from mltx_question_lang ql
join mltx_question q on ql.question_id = q.id
go

alter table mltx_question_lang drop constraint fk_question_lang_question
go

alter table mltx_user_answer drop constraint fk_mltx_user_answer_question
go

drop table mltx_question
go

alter table mltx_user_answer  with check add  constraint fk_mltx_user_answer_question foreign key(question_id)
references mltx_question_lang (id)
go

alter table mltx_question_lang add visual_order int not null default 1
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_mltx_update_version '1.7', '1.8'
go