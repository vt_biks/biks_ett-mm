/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import java.util.Map;
import simplelib.IContinuation;

/**
 *
 * @author beata
 */
public class TreeAndNodeKindTreeForAddAtrs extends TreeAndNodeKindTreeCheckBox {

    protected IContinuation onSelectionChangedCont;

    public void setOnSelectionChanged(IContinuation onSelectionChangedCont) {
        this.onSelectionChangedCont = onSelectionChangedCont;
    }

    @Override
    protected boolean calcCheckBoxValueForTreeIdNode(Map<String, Object> row) {
        return areAllDescendantsOneVal(row, true);
    }

    @Override
    protected boolean expandDynamicTreesInMenu() {
        return false;
    }

    @Override
    protected boolean isExpandAllNodes() {
        return true;
    }

    @Override
    protected void selectionChanged() {
        if (onSelectionChangedCont != null) {
            onSelectionChangedCont.doIt();
        }
    }
}
