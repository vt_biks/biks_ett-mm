﻿exec sp_check_version '1.3.0.1';
go

create table bik_tutorial_translation(
id int identity(1,1) NOT NULL primary key,
code varchar(512)  not null,
title varchar(128)  not null,
txt varchar(max) not null,
lang varchar(3),
movie varchar(64)
);
go

alter table bik_tutorial add code varchar(64) null;
go


update bik_tutorial set code = 'Introduction' where title='Wprowadzenie';
update bik_tutorial set code = 'MyBIKS' where title='Moduł Mój BIKS';
update bik_tutorial set code = 'Search' where title='Wyszukiwanie';
update bik_tutorial set code = 'AppNavigation' where title='Nawigacja po aplikacji';
update bik_tutorial set code = 'UseCases' where title='Przypadki użycia';
update bik_tutorial set code = 'Analysis' where title='Analizy';
update bik_tutorial set code = 'AddNews' where title='Dodawanie aktualności';
update bik_tutorial set code = 'AddAttributes' where title='Dodawanie atrybutów';
update bik_tutorial set code = 'UpdateContent' where title='Modyfikacja treści';
update bik_tutorial set code = 'LinkElements' where title='Podlinkowywanie elementów';
update bik_tutorial set code = 'AddEditDefinitions' where title='Tworzenie i edycja definicji';
update bik_tutorial set code = 'AddConnections' where title='Tworzenie powiązań';
go

alter table bik_tutorial add constraint UQ_bik_tutorial_code unique(code);
go
insert into bik_tutorial_translation(title,txt,movie,code,lang) select 'Introduction','en'+text, 'Introduction', code, 'en' from bik_tutorial where title='Wprowadzenie';
insert into bik_tutorial_translation(title,txt,movie,code,lang) select 'My BIKS','en'+text, 'My BIKS', code, 'en' from bik_tutorial where title='Moduł Mój BIKS';
insert into bik_tutorial_translation(title,txt,movie,code,lang) select 'Search','en'+text, 'Search', code, 'en' from bik_tutorial where title='Wyszukiwanie';
insert into bik_tutorial_translation(title,txt,movie,code,lang) select 'Navigation','en'+text, 'Navigation', code, 'en' from bik_tutorial where title='Nawigacja po aplikacji';
insert into bik_tutorial_translation(title,txt,movie,code,lang) select 'Use cases','en'+text, 'Use cases', code, 'en' from bik_tutorial where title='Przypadki użycia';
insert into bik_tutorial_translation(title,txt,movie,code,lang) select 'Analysis','en'+text, 'Analysis', code, 'en' from bik_tutorial where title='Analizy';
insert into bik_tutorial_translation(title,txt,movie,code,lang) select 'Adding news','en'+text, null, code, 'en' from bik_tutorial where title='Dodawanie aktualności';
insert into bik_tutorial_translation(title,txt,movie,code,lang) select 'Adding attributes','en'+text, null, code, 'en' from bik_tutorial where title='Dodawanie atrybutów';
insert into bik_tutorial_translation(title,txt,movie,code,lang) select 'Updating content','en'+text, null, code, 'en' from bik_tutorial where title='Modyfikacja treści';
insert into bik_tutorial_translation(title,txt,movie,code,lang) select 'Linking elements together','en'+text, null, code, 'en' from bik_tutorial where title='Podlinkowywanie elementów';
insert into bik_tutorial_translation(title,txt,movie,code,lang) select 'Adding/Updating definitions','en'+text, null, code, 'en' from bik_tutorial where title='Tworzenie i edycja definicji';
insert into bik_tutorial_translation(title,txt,movie,code,lang) select 'Adding connections','en'+text, null, code, 'en' from bik_tutorial where title='Tworzenie powiązań';
exec sp_update_version '1.3.0.1', '1.3.0.2';
go






