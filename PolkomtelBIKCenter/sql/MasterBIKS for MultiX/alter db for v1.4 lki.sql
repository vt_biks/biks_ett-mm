------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- 1. nowe table wspierające system ankiet
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

------------------------------------------------------

exec sp_mltx_check_version '1.4'
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

create table mltx_question(
	id int IDENTITY(1,1) primary key NOT NULL,
	is_visible smallint not null
) 

go


create table mltx_question_lang(
	id int IDENTITY(1,1) primary key NOT NULL,
	question_id int not null,
	lang varchar(3) not null,
	text varchar(500) not null
)

go


alter table mltx_question_lang  with check add  constraint fk_question_lang_question foreign key(question_id)
references mltx_question (id)
go

alter table mltx_question_lang check constraint fk_question_lang_question
go


create table mltx_user_answer(
	question_id int not null,
	text varchar(500) not null,
	user_id int not null
)

go



alter table mltx_user_answer  with check add  constraint fk_mltx_user_answer_mltx_user foreign key(user_id)
references mltx_registered_user (id)
go

alter table mltx_user_answer check constraint fk_mltx_user_answer_mltx_user
go

alter table mltx_user_answer  with check add  constraint fk_mltx_user_answer_question foreign key(question_id)
references mltx_question (id)
go

alter table mltx_user_answer check constraint fk_mltx_user_answer_question
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_mltx_update_version '1.4', '1.5'
go
