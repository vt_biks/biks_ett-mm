/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.PushButton;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.SystemUserRoleWidget;
import pl.fovis.client.dialogs.AddRoleDialog;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author Eugeniusz
 */
public class SystemUserRolePage extends SystemPage {

    protected void openAddRoleOrGroupDialog() {
        new AddRoleDialog().buildAndShowDialog(null, I18n.dodajRole.get(), new IParametrizedContinuation<List<String>>() {

            @Override
            public void doIt(List<String> param) {
                BIKClientSingletons.getService().addSystemRole(param.get(0), param.get(1), null, new StandardAsyncCallback<Integer>() {

                    @Override
                    public void onSuccess(Integer result) {
                        //zmieniłem na role
                        //TODO: change label identificator(name)
                        BIKClientSingletons.showInfo(result == 1 ? I18n.rolaDodana.get() : I18n.rolaIstnieje.get() /* I18N:  */);
                        show();
                    }
                });
            }
        });
    }

    protected void show() {
        dataFetchBroker.dataFetchStarting(IPageDataFetchBroker.TreeDataRequestKind.Full,
                BIKClientSingletons.getService().getGroupRootObjects(
                        new TreeDataFetchAsyncCallback<List<SystemUserGroupBean>>(null, dataFetchBroker) {
                            @Override
                            public void doOnSuccess(List<SystemUserGroupBean> t) {

                                if (t == null || t.isEmpty()) {
                                    rightPanel.clear();
                                    ///Window.alert(t.size()+"");
                                }
                                //false passed to initWithFullData hides 2nd level of nodes
                                treeWidget.initWithFullData(t, false, null, null);
                                Map<String, Object> record = treeWidget.getTreeGrid().getSelectedRecord();
                                treeWidget.getTreeGrid().selectFirstRecord();

                            }
                        }
                ), null);
    }

    @Override
    public String getId() {
        return "admin:dict:konfUpr";
    }

    protected void setTitleToAddBtn(PushButton addButton) {
        addButton.setTitle(I18n.dodajRole.get());
    }

    public void populateRightPanelWithGroup(final SystemUserGroupBean result) {
        rightPanel.clear();
        rightPanel.add(new SystemUserRoleWidget().buildAndShowDialog(result));
    }
    
    @Override
    public boolean isPageEnabled() {
        return false;
    }
}
