/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.mltx;

import java.io.Serializable;

/**
 *
 * @author lbiegniewski
 */
public class MultiXDatabaseTemplateBean implements Serializable {

    public Integer id;
    public String caption;
    public String filePath;
    public String restorePrefix;
    public String lang;
}
