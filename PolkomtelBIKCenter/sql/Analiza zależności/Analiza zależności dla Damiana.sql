IF OBJECT_ID('tempdb..#xxx') IS NOT NULL 
drop table #xxx
go

declare @rapTreeId int = (select id from bik_tree where code = 'Reports')
declare @rapWebiKindId int = (select dbo.fn_node_kind_id_by_code('Webi'))
declare @rapDeskiKindId int = (select dbo.fn_node_kind_id_by_code('FullClient'))
declare @rapCrystalKindId int = (select dbo.fn_node_kind_id_by_code('CrystalReport'))
declare @rapExcelKindId int = (select dbo.fn_node_kind_id_by_code('Excel'))
declare @univKindId int = (select dbo.fn_node_kind_id_by_code('Universe'))
declare @connKindId int = (select dbo.fn_node_kind_id_by_code('DataConnection'))
declare @connTreeId int = (select id from bik_tree where code = 'Connections')
declare @univTreeId int = (select id from bik_tree where code = 'ObjectUniverses')

select
		rap.name as report_name,
		univ.name as univ_name,
		conn.name as conn_name,
		cast('' as varchar(max)) as rap_parent_path, 
		rap.parent_node_id as rap_ancestor_id,
		cast('' as varchar(max)) as univ_parent_path, 
		univ.parent_node_id as univ_ancestor_id,
		bnk.caption
into #xxx
from
		bik_node rap
		join bik_node_kind bnk on bnk.id=rap.node_kind_id
		left join ( bik_joined_objs univjoins 
		left join bik_node univ on univjoins.dst_node_id = univ.id ) on rap.id = univjoins.src_node_id and univ.is_deleted = 0 and univ.linked_node_id is null and univ.tree_id = @univTreeId and univ.node_kind_id = @univKindId
		left join ( bik_joined_objs connjoins
		left join bik_node conn on conn.id = connjoins.dst_node_id ) on rap.id = connjoins.src_node_id and conn.is_deleted = 0 and conn.linked_node_id is null and conn.tree_id = @connTreeId and conn.node_kind_id = @connKindId
where
		rap.is_deleted = 0 and rap.linked_node_id is null and rap.tree_id = @rapTreeId and rap.node_kind_id in (@rapWebiKindId,@rapDeskiKindId,@rapCrystalKindId,@rapExcelKindId)
go


while 1 = 1 begin
  declare @rc int = null

  update #xxx
  set rap_parent_path = bn.name + '\' + rap_parent_path, rap_ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #xxx.rap_ancestor_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go


while 1 = 1 begin
  declare @rc int = null

  update #xxx
  set univ_parent_path = bn.name + '\' + univ_parent_path, univ_ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #xxx.univ_ancestor_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go


select report_name as Raport, caption as Typ_raportu, univ_name as Universum, conn_name as Connection, rap_parent_path as Raport_sciezka, univ_parent_path as Universum_sciezka from #xxx
order by Raport

