﻿exec sp_check_version '1.0.80';
go
	
	update bik_node_kind set code='CrystalReport',caption='CrystalReport',icon_name='crystal'
    where code='CristalReports'
    insert into bik_node_kind values ('Excel','Excel','excel','Metadata',0)
    insert into bik_node_kind values ('FullClient','FullClient','client','Metadata',0)
    insert into bik_node_kind values ('Pdf','pdf','pdf','Metadata',0)
    insert into bik_node_kind values ('Hyperlink','Hyperlink','link','Metadata',0)
   
 


	insert into bik_admin values('sapbo.path','C:/temp/designer-pump/BOXIUniversDataPump.exe')
	create nonclustered index [idx_node_kind_id_linked_node_id] on bik_node (linked_node_id, node_kind_id)
------------------------------------------------------------------------------
	insert into bik_node_kind values ('Rtf','Rtf','rtf','Metadata',0)
	insert into bik_node_kind values ('Txt','Txt','txt','Metadata',0)
	insert into bik_node_kind values ('Powerpoint','Powerpoint','powerpoint','Metadata',0)
	insert into bik_node_kind values ('Word','Word','word','Metadata',0)

------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
if OBJECT_ID (N'dbo.fn_node_kind_code_for_metadata_type', N'FN') is not null
    drop function dbo.fn_node_kind_code_for_metadata_type;
go

create function dbo.fn_node_kind_code_for_metadata_type(@metadata_type varchar(256))
returns varchar(255)
with execute as caller
as
begin
  return (
    select
      case @metadata_type
        when 'MetaData.DataConnection' then 'DataConnection'
        when 'Folder' then 'MetadataFolder'
        when 'Webi' then 'Webi'
        when 'Flash' then 'Flash'
        when 'CrystalReport' then 'CrystalReport'
        when 'Universe' then 'Universe'
        when 'UniverseClass' then 'UniverseClass'
        when 'UniverseObject' then 'UniverseObject'
        when 'UniverseTable' then 'UniverseTable'
        when 'UniverseColumn' then 'UniverseColumn'
        when 'SCHEMA' then 'TeradataSchema'
        when 'TABLE' then 'TeradataTable'
        when 'VIEW' then 'TeradataView'
        when 'COLUMN' then 'TeradataColumn'
        when 'PROCEDURE' then 'TeradataProcedure'
        when 'Excel' then 'Excel'
        when 'FullClient' then 'FullClient'
        when 'Pdf' then 'Pdf'
        when 'Hyperlink' then 'Hyperlink'
        when 'Rtf' then 'Rtf'
        when 'Txt' then 'Txt'
        when 'Powerpoint' then 'Powerpoint'
        when 'Word' then 'Word'
        else 'Other'
    end
  )
end
go

------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_node
go

create procedure [dbo].[sp_insert_into_bik_node](@tree_code varchar(255))
as
	declare @table_folders varchar(255);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	--print cast(sysdatetime() as varchar(22)) + ': faza 1'
	
	select @tree_id = id 
	from bik_tree
	where code=@tree_code;

	create table #primaryIds(si_id varchar(1000) primary key);
	create table #secondaryIds(si_id varchar(1000) primary key);

	if(@tree_code!='Reports' and @tree_code!='Teradata')
	begin
		set @table_folders = ' APP_FOLDER where 1=1 ';
		
		if(@tree_code = 'Connections')
		begin
			set @kinds = '''MetaData.DataConnection''';
			exec('insert into #primaryIds
					select convert(varchar,SI_ID) as SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');
			exec('insert into #secondaryIds 
					select convert(varchar,SI_ID) as SI_ID
					from APP_UNIVERSE
					where SI_DATACONNECTION__1 in (select * from #primaryIds)');			
		end;
		if(@tree_code = 'ObjectUniverses')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select convert(varchar,SI_ID) as SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_code='Reports')
	begin
		-- wybranie folderów raportów bez raportów użytkowników
		set @table_folders = ' INFO_FOLDER where si_name != ''~Webintelligence''';
		--set @table_folders = ' INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 is null or SI_PATH__SI_FOLDER_NAME2 != ''User Folders''';
		set @kinds = '''Webi'', ''Flash'', ''CrystalReport'',''Excel'',''FullClient'',''Pdf'',''Hyperlink''
		,''Rtf'',''Txt'',''Powerpoint'',''Word''';
	end;
	
    declare @sql nvarchar(2000);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		--print cast(sysdatetime() as varchar(22)) + ': faza 4.1 - teradata przed sp_teradata_init_branch_names'
		exec sp_teradata_init_branch_names;
		--print cast(sysdatetime() as varchar(22)) + ': faza 4.2 - teradata po sp_teradata_init_branch_names'
		set @sql = N'select branch_names as SI_ID, 
											case 
												when SUBSTRING(branch_names, 1,len(branch_names) - len(name + ''|'')) =''''
													then null
												else SUBSTRING(branch_names, 1,len(branch_names) - len(name + ''|'')) end
												as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata
				/*where parent_id is not null or name like ''VD_US_%''*/';
	end;
	else
	begin
		if(@tree_code = 'Reports')
		-- sztuczny podzial na root folder i user folders dla raportów
		set @sql = N'select SI_PARENT_FOLDER as SI_ID, (select si_id from INFO_FOLDER where SI_NAME=''User Folders'') as SI_PARENTID, SI_KIND, SI_PATH__SI_FOLDER_NAME1 as SI_NAME, NULL as DESCR
				 from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' 
				 and SI_NAME!=''~Webintelligence''

				 union all
				
				 select si_id, case
									when SI_PARENTID=0 and si_name!=''User Folders''
										then (select si_id from INFO_FOLDER where SI_NAME=''Root Folder'')
									when si_id in(select si_id from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 = ''User Folders'')
                                        then SI_PARENTID
									when SI_PARENTID in(select si_id from INFO_FOLDER)
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case
																			when SI_NAME=''Root Folder''
																				then ''Foldery korporacyjne''
																			when SI_NAME=''User Folders''
																				then ''Foldery użytkowników''
																			else SI_NAME end, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 								
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +')
				union all
				
				select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR
				from aaa_global_props where si_parentid in (select si_parentid from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' and SI_NAME!=''~Webintelligence'') and si_kind!=''Folder''
				';
		if(@tree_code = 'Connections')
			set @sql = N'select si_id, case
									when SI_NAME=''Connections''
										then 0
									when SI_PARENTID in(select si_id from ' + @table_folders +') 
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 
													
				' and SI_NAME!=''Dynamic Cascading Prompts''
				
				 union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') 
						
				union all		  

								select SI_ID, SI_DATACONNECTION__1 as SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
								from APP_UNIVERSE
								where SI_DATACONNECTION__1 in (select * from #primaryIds) and
									SI_KIND in('+ @kinds +')
	
								union all
								
								select SI_ID, SI_UNIVERSE__1 as SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
								from INFO_WEBI
								where SI_UNIVERSE__1 in (select * from #secondaryIds)
								and SI_KIND in('+ @kinds +')';

		if(@tree_code = 'ObjectUniverses')
			set @sql = N'select si_id, case 
									when SI_NAME=''Universes''
										then 0
									when SI_PARENTID in(select si_id from ' + @table_folders +') 
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 
													
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') 
				
				union all
			
								select SI_ID, SI_UNIVERSE__1 as SI_PARENTID, SI_KIND,  SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
								from INFO_WEBI
								where SI_UNIVERSE__1 in (select * from #primaryIds) and SI_KIND in ('+ @kinds +')';
	end;					

	--print cast(sysdatetime() as varchar(22)) + ': faza 6 - przed zapytaniem'
	print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(1000) primary key, si_parentid varchar(1000), si_kind varchar(max), si_name varchar(max), descr varchar(max));

	set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
				select si_id, si_parentid, dbo.fn_node_kind_code_for_metadata_type(si_kind), si_name, DESCR
			    from (' + @sql + ') xxx';
			   
	EXECUTE(@sql);

	--print cast(sysdatetime() as varchar(22)) + ': faza 6.5 - po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/

	declare @rc int = 1

	while @rc > 0 begin
	
	delete from #tmpNodes
	where si_kind = 'MetadataFolder' and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/	

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = #tmpNodes.si_name,
		is_deleted = 0, descr = #tmpNodes.descr, tree_id = @tree_id
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id /*and tree_id = @tree_id*/ and bik_node.linked_node_id is null
	
	--update podlinkowanych
	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = #tmpNodes.si_name,
		 descr = #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id /*and tree_id = @tree_id*/ and not bik_node.linked_node_id is null

	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, #tmpNodes.si_name, @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id 
	where bik_node.id is null;

	set nocount on;

	drop table #primaryIds;
	drop table #secondaryIds;
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id --and bik_node.tree_id = @tree_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid where bik_node.linked_node_id is null--and bk.tree_id = @tree_id 

	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in ('UniverseColumn', 'UniverseTable', 'UniverseObject', 'UniverseClass')
				
	drop table #tmpNodes

	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
end;
go

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_sapbo_extradata]
GO

create procedure sp_insert_into_bik_node_sapbo_extradata
as
begin
insert into bik_sapbo_extradata  
    select bik.id as node_id, 
    nullif(ltrim(rtrim(p.SI_AUTHOR)), '') as author, 
    nullif(ltrim(rtrim(p.SI_OWNER)), '') as owner,
    nullif(ltrim(rtrim(p.SI_KEYWORD)), '') as keyword,
    uni.id as universe_node_id, conn.id as connection_node_id    
    from aaa_global_props p left join INFO_WEBI iw on p.si_id = iw.si_id
    left join APP_UNIVERSE au on iw.SI_UNIVERSE__1 = au.si_id
    left join APP_METADATA__DATACONNECTION amdc on au.SI_DATACONNECTION__1 = amdc.si_id
    inner join bik_node bik on convert(varchar,p.si_id) = bik.obj_id
    left join bik_node uni on convert(varchar,au.si_id) = uni.obj_id and uni.tree_id=(select id from bik_tree where name='Światy obiektów') and uni.is_deleted=0
    left join bik_node conn on convert(varchar, amdc.si_id) = conn.obj_id and conn.tree_id=(select id from bik_tree where name='Połączenia') and conn.is_deleted=0
    left join bik_sapbo_extradata spe on bik.id =spe.node_id
    where spe.node_id is null
    order by bik.id 
end
go


exec sp_update_version '1.0.80', '1.0.81';
go