/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.TextArea;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;

/**
 *
 * @author ctran
 */
public class ImportTreeLogDialog extends BaseActionOrCancelDialog {

    protected TreeBean treeBean;
    protected TextArea log = new TextArea();

    @Override
    protected String getDialogCaption() {
        return I18n.logi.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return "OK";
    }

    @Override
    protected void buildMainWidgets() {
        log.setHeight("600px");
        log.setWidth("800px");
        main.add(log);
        BIKClientSingletons.getService().getImportTreeLog(treeBean.id, new StandardAsyncCallback<List<String>>() {

            @Override
            public void onSuccess(List<String> result) {
                StringBuilder sb = new StringBuilder();
                for (String msg : result) {
                    sb.append(msg).append("\n");
                }
                log.setText(sb.toString());

                BIKClientSingletons.getService().setTreeImportStatus(treeBean.id, TreeImportLogStatus.SAW, new StandardAsyncCallback<Void>() {

                    @Override
                    public void onSuccess(Void result) {
                    }
                });
            }
        });
    }

    @Override
    protected void doAction() {
    }

    public void buildAndShowDialog(TreeBean b) {
        this.treeBean = b;
        super.buildAndShowDialog();
    }

    public enum TreeImportLogStatus {

        RUNNING(3), ERROR(2), SUCCESS(1), SAW(0);

        protected int code;

        private TreeImportLogStatus(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }
}
