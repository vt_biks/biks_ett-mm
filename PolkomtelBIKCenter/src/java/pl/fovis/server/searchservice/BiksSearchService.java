/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.searchservice;

import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import pl.bssg.searchengine.SearchUtils;
import pl.bssg.searchengine.Searcher;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog.AttributeType;
import pl.fovis.common.BIKConstants;
import pl.fovis.server.BIKCenterConfigObj;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.logging.ILameLogger;

/**
 *
 * @author ctran
 */
public class BiksSearchService implements IBiksSearchService {

    public static final int MAX_RETURNED_OBJ = 999;

    protected static final ILameLogger logger = LameUtils.getMyLogger();

    protected INamedSqlsDAO adhocDao;
    protected Searcher searcher;

    protected String indexDirectory;

    public BiksSearchService(BIKCenterConfigObj cfg) {
        indexDirectory = BiksSearchConstant.getIndexDirectory(cfg);
        adhocDao = FoxyAppUtils.createNamedSqlsDAOFromJar(new MssqlConnection(cfg.connConfig), BiksSearchConstant.ADHOC_SQLS_PATH);
        searcher = new Searcher(indexDirectory, BiksSearchConstant.getAnalyzer());
    }

    @Override
    public List<Document> search(String text, Iterable<Integer> selectedTreeIds, Iterable<String> selectedAttributeName, List<String> selectedOtherFilter, Set<Integer> nodeKindIds, String searchMode, boolean notUseKindAndAttrFilter) {
        try {
//            text = text.trim().toLowerCase();
            text = text.trim();
            ensureIndexingIsFinished();
            Query query = null;
//            System.out.println(text);
            if (BIKConstants.SEARCH_MODE_NORMAL.equals(searchMode)) {
                query = buildSearchQueryInAllTextFieldsOfSelectedTrees(text, selectedTreeIds, selectedAttributeName, selectedOtherFilter, nodeKindIds, notUseKindAndAttrFilter);
            } else {
                query = buildAdvancedSearchQuery(text, selectedTreeIds, nodeKindIds);
            }
//            System.out.println(query);
            return searcher.search(query);
        } catch (Throwable ex) {
            throw new LameRuntimeException("BiksSearchService error: " + ex.getMessage(), ex);
        }
    }

    @Override
    public List<Document> searchInTreeByNodeName(String text, List<Integer> treeIds) {
//        try {
//            text = text.trim();
////            BooleanQuery.Builder builder = new BooleanQuery.Builder();
////            builder.add(buildSearchQueryInAllTextFields(text), BooleanClause.Occur.MUST);
////            BooleanQuery.Builder treeIdQueyrBuilder = new BooleanQuery.Builder();
////            for (Integer treeId : treeIds) {
////                treeIdQueyrBuilder.add(parseQuery(BaseUtils.safeToString(treeId), new String[]{BiksSearchConstant.BIK_TREE_ID}), BooleanClause.Occur.SHOULD);
////            }
////            builder.add(treeIdQueyrBuilder.build(), BooleanClause.Occur.MUST);
////            return searcher.search(builder.build(), 0, 999).v2;
//            return searcher.search(buildSearchQueryInAllTextFieldsOfSelectedTrees(text, treeIds), 0, 999).v2;
//        } catch (Throwable ex) {
//            throw new LameRuntimeException("BiksSearchService error: " + ex.getMessage(), ex);
//        }
        List<String> selectedOtherFilter = new ArrayList<String>();
        selectedOtherFilter.add(BiksSearchConstant.BIK_NODE_NAME);
        selectedOtherFilter.add(BiksSearchConstant.BIK_NODE_DESC);
        selectedOtherFilter.add(BiksSearchConstant.BIK_FILE_CONTENT);
        return search(text, new HashSet<Integer>(treeIds), null, selectedOtherFilter, null, BIKConstants.SEARCH_MODE_NORMAL, true);/* bfechner */

    }

    private Query parseQuery(String text, List<String> fields) {
        BooleanQuery.Builder builder = new BooleanQuery.Builder();
        for (String field : fields) {
            builder.add(SearchUtils.parse(text, field), BooleanClause.Occur.SHOULD);
        }
        return builder.build();
    }

    private List<String> normalizeFieldNames4Search(List<String> fields) {
        for (int i = 0; i < fields.size(); i++) {
            String name = fields.get(i);
            fields.set(i, "@" + name.replace(" ", "_").toLowerCase());
        }
        return fields;
    }

    private Query buildSearchQueryInAllTextFields(String text, Iterable<Integer> selectedTreeIds, Iterable<String> selectedAttributeName, List<String> selectedOtherFilter, Iterable<Integer> nodeKindIds, boolean notUseKindAndAttrFilter) {
        BooleanQuery.Builder builder = new BooleanQuery.Builder();
        List<String> attrNames = null;
        try {
            attrNames = BeanConnectionUtils.execNamedQuerySingleColAsList(adhocDao, "getAttributesOfSelectedTrees", "name", selectedTreeIds, selectedAttributeName, nodeKindIds, notUseKindAndAttrFilter);
        } catch (Exception e) {

        }

        if (attrNames != null) {
            attrNames = normalizeFieldNames4Search(attrNames);
        } else {
            attrNames = new ArrayList<String>();
        }

        attrNames.addAll(selectedOtherFilter);
//        attrNames.addAll(Arrays.asList(BiksSearchConstant.ALL_TEXT_FIELDS));
        builder.add(parseQuery(text, attrNames), BooleanClause.Occur.SHOULD);

        return builder.build();
    }

    private Query buildSearchQueryInAllTextFieldsOfSelectedTrees(String text, Iterable<Integer> selectedTreeIds, Iterable<String> selectedAttributeName, List<String> selectedOtherFilter, Iterable<Integer> nodeKindIds, boolean notUseKindAndAttrFilter) {
        BooleanQuery.Builder builder = new BooleanQuery.Builder();
        builder.add(buildSearchQueryInAllTextFields(text, selectedTreeIds, selectedAttributeName, selectedOtherFilter, nodeKindIds, notUseKindAndAttrFilter), BooleanClause.Occur.MUST);
        BooleanQuery.Builder treeIdQueyrBuilder = new BooleanQuery.Builder();
        List<String> fields = new ArrayList<String>();
        fields.add(BiksSearchConstant.BIK_TREE_ID);
        for (Integer treeId : selectedTreeIds) {
            treeIdQueyrBuilder.add(parseQuery(BaseUtils.safeToString(treeId), fields), BooleanClause.Occur.SHOULD);
        }
        builder.add(treeIdQueyrBuilder.build(), BooleanClause.Occur.MUST);
        return builder.build();
    }

//    @Override
//    public Integer countResult(String text, Iterable<Integer> selectedTreeIds, int pageNum, int pageSize) {
//        try {
//            text = text.trim();
//            ensureIndexingIsFinished();
//            return searcher.countResult(buildSearchQueryInAllTextFieldsOfSelectedTrees(text, selectedTreeIds), pageNum, pageSize);
//        } catch (Throwable ex) {
//            throw new LameRuntimeException("BiksSearchService error: " + ex.getMessage(), ex);
//        }
//    }
    private void ensureIndexingIsFinished() {
        if (IndexTask.isIndexing()) {
            throw new LameRuntimeException("Trwa indeksowanie. Proszę spróbować póżniej...");
        }
    }

    private Query buildAdvancedSearchQuery(String text, Iterable<Integer> selectedTreeIds, Iterable<Integer> nodeKindIds) throws LameRuntimeException {
        List<Map<String, Object>> list = adhocDao.execNamedQueryCFN("getAttributesOfSelectedTrees", null, selectedTreeIds, null, nodeKindIds, true);
        List<String> numberAttrNames = new ArrayList<String>();
        List<String> dateAttrNames = new ArrayList<String>();
        for (Map<String, Object> m : list) {
            if ((Integer) m.get("display_as_number") == 1) {
                numberAttrNames.add((String) m.get("name"));
            } else if (AttributeType.data.name().equals((String) m.get("type_attr"))
                    || AttributeType.datetime.name().equals((String) m.get("type_attr"))) {
                dateAttrNames.add((String) m.get("name"));
            }
        }
        numberAttrNames = normalizeFieldNames4Search(numberAttrNames);
        dateAttrNames = normalizeFieldNames4Search(dateAttrNames);
        text = "(" + text + ") AND (" + BiksSearchConstant.BIK_TREE_ID + ":0";
        for (Integer treeId : selectedTreeIds) {
            text += (" OR " + BiksSearchConstant.BIK_TREE_ID + ":" + treeId);
        }
        text += ")";
        BiksQueryParser qp = new BiksQueryParser(new HashSet<String>(numberAttrNames), new HashSet<String>(dateAttrNames), BiksSearchConstant.BIK_NODE_DESC, BiksSearchConstant.getAnalyzer());
        qp.setLowercaseExpandedTerms(false);
        qp.setAllowLeadingWildcard(true);
        try {
            return qp.parse(text);
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
    }
}
