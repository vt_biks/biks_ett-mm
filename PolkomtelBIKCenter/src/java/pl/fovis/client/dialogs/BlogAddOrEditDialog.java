/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import java.util.Date;
import pl.fovis.common.BlogBean;
import simplelib.IParametrizedContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author tflorczak
 */
public class BlogAddOrEditDialog extends BaseRichTextAndTextboxDialog {

    protected IParametrizedContinuation<BlogBean> saveCont;
    protected BlogBean record;

    @Override
    protected String getDialogCaption() {
        return I18n.wpisNaBlogu.get() /* I18N:  */;
    }

    protected void innerDoAction(String title, String body) {
        BlogBean blogEntity = new BlogBean();
        blogEntity.content = /*deleteBadURL(*/ body/*)*/;
        blogEntity.dateAdded = (record != null) ? record.dateAdded : new Date();
        blogEntity.subject = title;
        blogEntity.id = record != null ? record.id : 0;
        blogEntity.nodeId = (record != null) ? record.nodeId : 0;
        saveCont.doIt(blogEntity);
        //cancelClicked();
    }

    public void buildAndShowDialog(BlogBean blog/*Map<String, Object> record*/, IParametrizedContinuation<BlogBean> saveCont) {
        this.saveCont = saveCont;
//        this.record = record;
        this.record = blog;
        super.buildAndShowDialog(blog != null ? blog.subject : null, blog != null ? blog.content : null);
    }
}
