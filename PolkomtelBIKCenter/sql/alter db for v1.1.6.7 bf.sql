﻿exec sp_check_version '1.1.6.7';
go

--is_auxiliary = 0 - główny, = 1 - pomocniczy
alter table bik_user_in_node 
add is_auxiliary int not null default 0
go


alter table bik_user_in_node
drop constraint UQ_user_id_node_id_role_for_node_id


exec sp_update_version '1.1.6.7', '1.1.6.8';
go