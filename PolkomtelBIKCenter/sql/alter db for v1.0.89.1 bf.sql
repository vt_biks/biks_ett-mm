﻿exec sp_check_version '1.0.89.1';
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------
update bik_node set is_deleted=1 where id=(select bn.id from bik_node bn 
join bik_tree bt on bn.tree_id=bt.id 
where bn.name='Artykuły' and bt.code='Glossary')


declare @tree_id int;

select @tree_id = id 
	from bik_tree
	where code='FAQ';
	
exec sp_node_init_branch_id @tree_id, null;


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

exec sp_update_version '1.0.89.1', '1.0.89.2';
go
