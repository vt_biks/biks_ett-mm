/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import commonlib.connectionpool.ConnectionPoolManagerViaDataSource;
import pl.trzy0.foxy.commons.MySqlConnectionCfg;
import pl.trzy0.foxy.serverlogic.IConnectionDialectHelper;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class MySqlConnection extends PooledJdbcConnectionBase<MySqlConnectionCfg> {

    public MySqlConnection() {
        // no-op
    }

    public MySqlConnection(String userName, String password, String serverName, String databaseName) {
        initConnection(userName, password, serverName, databaseName);
    }

    public void initConnection(MySqlConnectionCfg cfg) {
        initConnection(cfg.user, cfg.password, cfg.server, cfg.database);
    }

    protected void initConnection(String userName, String password, String serverName, String databaseName) {
        try {
            MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();

            dataSource.setServerName(serverName);
            dataSource.setDatabaseName(databaseName);
            dataSource.setUser(userName);
            dataSource.setPassword(password);
            cpm = //new ConnectionPoolManagerByDataSource(dataSource);
                    new ConnectionPoolManagerViaDataSource(dataSource, 30, 30);
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
    }

    @Override
    protected IConnectionDialectHelper createDialectHelper() {
        return new SqlValueFormatterCurlyTimeStamp();
    }
}
