/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import commonlib.LameUtils;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.BooleanQuery.Builder;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import pl.fovis.common.SearchWizardHitResult;
import static pl.fovis.server.fts.FtsByLucene.COLLECTIVE_FIELD_NAME;
import static pl.fovis.server.fts.FtsByLucene.NODE_ID_FIELD_NAME;
import pl.fovis.server.fts.luceneUtils.ISearchWizardBucketManager;
import pl.fovis.server.fts.luceneUtils.LowercasingMorfologicWithBaseFormAnalyzer;
import pl.fovis.server.fts.luceneUtils.LuceneLameUtils;
import pl.fovis.server.fts.luceneUtils.QueryWords;
import simplelib.BaseUtils;
import simplelib.ILameCollector;
import simplelib.Pair;
import simplelib.StackTraceUtil;
import simplelib.logging.ILameLogger;
import simplelib.logging.ILameLogger.LameLogLevel;

/**
 *
 * @author pmielanczuk
 */
public class LuceneSimilarNodesWizard implements ISimilarNodesWizard {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    public static final int NUMBER_OF_THREADS = 4;

    protected static ExecutorService pool = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    protected static boolean destroying = false;

    public LameLogLevel timingLogLevel = LameLogLevel.Stage;
    protected String indexDir;
    protected INodesDataFetcher fetcher;
    protected int indexedNodeCnt = 0;
    private long nanoTimeStartForIndexer;

    protected ILameCollector<Map<String, Pair<String, Float>>> nodeDataIndexer = new ILameCollector<Map<String, Pair<String, Float>>>() {

        @Override
        public void add(Map<String, Pair<String, Float>> item) {
            if (destroying) {
                throw new RuntimeException("nodeDataIndexer throws exception due to destroying");
            }
            if (indexedNodeCnt == 0) {
                logTiming("before first row from fetcher", -1, nanoTimeStartForIndexer);
            }
            indexNodeData(item);
            indexedNodeCnt++;
            if (destroying) {
                throw new RuntimeException("nodeDataIndexer throws exception due to destroying");
            }
        }
    };
    protected Analyzer analyzer;
    protected IndexWriter writer;
    protected IndexReader reader;
    protected IndexSearcher searcher;
//    protected QueryParser parser;
    protected Directory dir;
    protected final Object readerSearcherLock = new Object();
    protected final Object indexJobLock = new Object();
    protected boolean indexingInProgress = false;
    protected boolean hasPendingJob = false;
    protected Set<Integer> explainForNodeIds = null;
    protected Future<?> jobInProgressOrDone = null;
    public static final Set<String> technicalFields = BaseUtils.paramsAsSet(NODE_ID_FIELD_NAME, TREE_ID_ATTR_NAME, NODE_KIND_ID_ATTR_NAME, TREE_KIND_ATTR_NAME);
    public static final Set<String> fieldsToIgnoreForOccurences = new HashSet<String>(technicalFields);

    {
        fieldsToIgnoreForOccurences.add(COLLECTIVE_FIELD_NAME);
    }

    public LuceneSimilarNodesWizard(String indexDir) {
        this.indexDir = indexDir;
    }

    protected void logTiming(String eventDescr, long processedItems, long nanoTimeStart) {
        if (!logger.isEnabledAtLevel(timingLogLevel)) {
            return;
        }

        long nanoTimeEnd = System.nanoTime();

        long timeTakenNanos = nanoTimeEnd - nanoTimeStart;

        String aboutItems = "";
        if (processedItems >= 0) {
            aboutItems = ", processed items: " + processedItems;
            if (processedItems > 0) {
                aboutItems += ", time per item: " + BaseUtils.doubleToString(timeTakenNanos / (double) processedItems / 1000000.0, 3) + " ms";
            }
        }

        logger.logAtLevel(timingLogLevel, eventDescr + ": time taken="
                + BaseUtils.doubleToString(timeTakenNanos / 1000000000.0, 3) + " s" + aboutItems);
    }

    protected void addJob(Runnable job) {

        synchronized (indexJobLock) {
            if (!indexingInProgress && hasPendingJob) {
                return;
            }

            if (indexingInProgress) {
                if (hasPendingJob) {
                    return;
                }
                hasPendingJob = true;
            } else {
                hasPendingJob = true;
                jobInProgressOrDone = pool.submit(job);
            }
        }
    }

    protected void indexDataFromFetcher(final boolean fullFetch) {
        addJob(new Runnable() {

            @Override
            public void run() {

                long nanoTimeStart = System.nanoTime();
                indexedNodeCnt = 0;

                if (logger.isInfoEnabled()) {
                    logger.info("indexDataFromFetcher thread run - starting job: fullFetch=" + fullFetch);
                }

                synchronized (indexJobLock) {
                    indexingInProgress = true;
                    hasPendingJob = false;
                }

                try {
                    nanoTimeStartForIndexer = System.nanoTime();
                    if (fullFetch) {
                        fetcher.fetchFullData(nodeDataIndexer);
                    } else {
                        fetcher.fetchChangedData(nodeDataIndexer);
                    }
                } catch (Exception ex) {
                    if (logger.isInfoEnabled()) {
                        logger.info("error in indexDataFromFetcher\n"
                                + StackTraceUtil.getCustomStackTrace(ex));
                    }
                    throw new RuntimeException("error in indexDataFromFetcher", ex);
                } finally {
                    if (logger.isInfoEnabled()) {
                        logger.info("indexDataFromFetcher thread run - finishing job: fullFetch=" + fullFetch);
                    }

                    logTiming("indexDataFromFetcher - indexing", indexedNodeCnt, nanoTimeStart);
                    if (!destroying) {
                        finalizeIndexing();
                    }

                    synchronized (indexJobLock) {
                        indexingInProgress = false;
                        boolean pj = hasPendingJob;
                        hasPendingJob = false;
                        if (pj && !destroying) {
                            indexDataFromFetcher(false);
                        }
//                        jobInProgressOrDone = null;
                    }
                }
            }
        });
    }

    @Override
    public boolean hasIndex() {
        synchronized (readerSearcherLock) {
            return reader != null;
        }
    }

    @Override
    public boolean isIndexingInProgress() {
        boolean willWait = false;
        synchronized (indexJobLock) {
            if (jobInProgressOrDone != null && !jobInProgressOrDone.isDone()) {
                willWait = true;
            }
        }
        return willWait;
    }

    @Override
    public void waitForFinalizeIndexingIfInProgress() {

        boolean willWait = isIndexingInProgress();

        if (willWait) {
            if (logger.isStageEnabled()) {
                logger.stage("starting wait for finalizeIndexing...");
            }
            try {
                jobInProgressOrDone.get();
            } catch (Exception ex) {
                throw new RuntimeException("exception in wait for job", ex);
            }
            if (logger.isStageEnabled()) {
                logger.stage("done waiting for finalizeIndexing");
            }
        } else {
            if (logger.isStageEnabled()) {
                logger.stage("no indexing in progress, will not wait");
            }
        }
    }

    @Override
    public void setUp(final INodesDataFetcher fetcher, boolean forceRecreate) {
        this.fetcher = fetcher;

        final boolean fullFetch = setUpLucene(forceRecreate);

        indexDataFromFetcher(fullFetch);
    }

    @Override
    public void setUpReadOnly() {
        setUpLucene(false);
        finalizeIndexing();
    }

    @Override
    public void nodeDataChanged() {
        if (fetcher == null) {
            finalizeIndexing();
        } else {
            indexDataFromFetcher(false);
        }
    }

    public static String fixAttrValue(String attrName, String val) {

        if (val == null) {
            return null;
        }

//        val = val.toLowerCase();
        int len = val.length();

        char[] chars = new char[len];
        boolean hasChange = false;
        for (int i = 0; i < len; i++) {
            char c = val.charAt(i);
            switch (c) {
                case '.':
                case ',':
                case ':':
                case '(':
                case ')':
                case '{':
                case '}':
                case '[':
                case ']':
                case '<':
                case '>':
                case '!':
                case '?':
                case '|':
                case '"':
                case '\'':
                case '\\':
                case '/':
                case ';':
                case '~':
                case '`':
                case '@':
                case '#':
                case '$':
                case '%':
                case '^':
                case '&':
                case '_':
                    chars[i] = ' ';
                    hasChange = true;
                    break;
                default:
                    chars[i] = c;
            }
        }
        return hasChange ? new String(chars) : val;
    }

    protected boolean setUpLucene(boolean forceRecreate) {

        try {
//            analyzer = new MorfologikAnalyzer();
            analyzer = new LowercasingMorfologicWithBaseFormAnalyzer();
//            analyzer = new SimpleAnalyzer();
//            analyzer = new MySimpleAnalyzer();

            File dirF = new File(indexDir);

            boolean mustCreateIndex = forceRecreate || !dirF.exists();

            Path path = Paths.get(indexDir);

//            Directory dir = FSDirectory.open(path);
            dir = FSDirectory.open(path);

            if (fetcher != null) {
                IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

                if (mustCreateIndex) {
                    // Create a new index in the directory, removing any
                    // previously indexed documents:
                    iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
                } else {
                    // Add new documents to an existing index:
                    iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
                }

                // Optional: for better indexing performance, if you
                // are indexing many documents, increase the RAM
                // buffer.  But if you do this, increase the max heap
                // size to the JVM (eg add -Xmx512m or -Xmx1g):
                //
                iwc.setRAMBufferSizeMB(128.0);

                writer = new IndexWriter(dir, iwc);
                writer.commit();
            }

//            reader = DirectoryReader.open(dir);
//            searcher = new IndexSearcher(reader);
//            parser = new QueryParser(COLLECTIVE_FIELD_NAME, analyzer);
//            parser.setAllowLeadingWildcard(true);
//            parser.setDefaultOperator(QueryParser.Operator.AND);
            return mustCreateIndex;
        } catch (Exception ex) {
            throw new RuntimeException("error while initializing Lucene", ex);
        }
    }

    protected void finalizeIndexing() {

        long nanoTimeStart = System.nanoTime();

        try {
            if (logger.isInfoEnabled()) {
                logger.info("finalize indexing");
            }
            if (fetcher != null) {
                writer.commit();
            }

            synchronized (readerSearcherLock) {
                reader = DirectoryReader.open(dir);
                searcher = new IndexSearcher(reader);
            }
        } catch (IOException ex) {
            throw new RuntimeException("exception in finalizeIndexing", ex);
        }

        logTiming("finalizeIndexing", -1, nanoTimeStart);
    }

    protected void deleteDocument(String nodeId) throws IOException {

        if (logger.isDebugEnabled()) {
            logger.debug("deleteDocument: nodeId=" + nodeId);
        }

        Term term = new Term(FtsByLucene.NODE_ID_FIELD_NAME, nodeId);

        writer.deleteDocuments(new Term[]{term});
    }

    protected void indexNodeData(Map<String, Pair<String, Float>> nodeData) {

//        if (logger.isInfoEnabled()) logger.info("indexNodeData: " + nodeData);
        String objId = BaseUtils.safeGetV1OfPair(nodeData.get(NODE_ID_FIELD_NAME));
        String isDeletedAsStr = BaseUtils.safeGetV1OfPair(nodeData.get(FtsByLucene.IS_DELETED_FIELD_NAME));
        boolean isDeleted = !BaseUtils.isStrEmpty(isDeletedAsStr) && !BaseUtils.safeEquals(isDeletedAsStr, "0");

        try {

            if (isDeleted) {
                deleteDocument(objId);
                return;
            }

            Document doc = new Document();

//        Field fldObjId = new IntField(NODE_ID_FIELD_NAME, objId, Field.Store.YES);
//        Field fldObjId = new TextField(NODE_ID_FIELD_NAME, Integer.toString(objId), Field.Store.YES);
//        doc.add(fldObjId);
//        boolean hasVals = false;
            StringBuilder sb = new StringBuilder();

            for (Map.Entry<String, Pair<String, Float>> e : nodeData.entrySet()) {
                String attrName = e.getKey();
                final Pair<String, Float> value = e.getValue();
                Object attrVal = value.v1;
                Float boost = value.v2;

                if (attrVal == null) {
                    continue;
                }

                boolean isTechnicalField = technicalFields.contains(attrName);
                String attrValStr = attrVal.toString();

//                String attrValStr = attrVal.toString().toLowerCase();
                if (!isTechnicalField) {
                    attrValStr = fixAttrValue(attrName, attrValStr);
                }

                if (BaseUtils.isStrEmptyOrWhiteSpace(attrValStr)) {
                    continue;
                }

//            hasVals = true;
                if (!isTechnicalField) {
                    if (sb.length() != 0) {
                        sb.append(" ");
                    }
                    sb.append(attrValStr);
                }

                Field attrField = new TextField(attrName, attrValStr,
                        Field.Store.YES
                //                        FtsByLucene.NODE_ID_FIELD_NAME.equals(attrName)
                //                                ? Field.Store.YES
                //                                : Field.Store.NO
                );

//                if (attrName.equals("name")) {
//                    boost = 4.0f;
//                } else {
//                    boost = -1.0f;
//                }
                if (boost > 0.0f) {
                    attrField.setBoost(boost);
                }
                doc.add(attrField);
            }

//        if (hasVals) {
            if (sb.length() > 0) {
                doc.add(new TextField(COLLECTIVE_FIELD_NAME, sb.toString(), Field.Store.YES));

                boolean isUpdate = true; // to-do
                if (isUpdate) {
                    writer.updateDocument(new Term(NODE_ID_FIELD_NAME, objId), doc);
                } else {
                    writer.addDocument(doc);
                }
            } else {
                if (logger.isWarnEnabled()) {
                    logger.warn("indexNodeData: deleting node with id=" + objId + " - empty but not marked as deleted");
                }
                writer.deleteDocuments(new Term[]{new Term(NODE_ID_FIELD_NAME, objId)});
            }
        } catch (IOException ex) {
            throw new RuntimeException("error in indexDoc with id=" + objId, ex);
        }
    }

    protected Query createSingleMltQuery(int docId, Reader r, String fieldName, Float boost) throws IOException {
        MoreLikeThis mlt = new MoreLikeThis(reader);
        mlt.setMinTermFreq(0);
        mlt.setMinDocFreq(0);
//        mlt.setFieldNames(new String[]{/*"name", "descr",*/FtsByLucene.COLLECTIVE_FIELD_NAME});
//        mlt.setFieldNames(new String[]{"name", "descr"});
        mlt.setFieldNames(new String[]{fieldName});
        if (boost != null) {
            mlt.setBoost(true);
            mlt.setBoostFactor(boost);
        }
        mlt.setAnalyzer(analyzer);
//        Query mltQ = mlt.like(docId);
        Query mltQ = mlt.like(fieldName, r);
        return mltQ;
    }

    protected Query prepareMoreLikeThisQuery(int nodeId) throws IOException {

        Query q0 = new TermQuery(new Term(FtsByLucene.NODE_ID_FIELD_NAME, Integer.toString(nodeId)));

        TopDocs td = searcher.search(q0, 1);

        ScoreDoc[] hits = td.scoreDocs;

        if (hits.length == 0) {
            if (logger.isInfoEnabled()) {
                logger.info("prepareMoreLikeThisQuery: no results for nodeId=" + nodeId);
            }
            return null;
        }

        if (hits.length > 1 && logger.isWarnEnabled()) {
            logger.warn("prepareMoreLikeThisQuery(" + nodeId + "): lucene search by node_id returned more than 1 hit (" + hits.length + ")");
        }

        int docId = hits[0].doc;

//        MoreLikeThis mlt = new MoreLikeThis(reader);
//        mlt.setMinTermFreq(0);
//        mlt.setMinDocFreq(0);
////        mlt.setFieldNames(new String[]{/*"name", "descr",*/FtsByLucene.COLLECTIVE_FIELD_NAME});
////        mlt.setFieldNames(new String[]{"name", "descr"});
//        mlt.setFieldNames(null);
//        mlt.setBoost(true);
////        mlt.setBoostFactor(2);
//        mlt.setAnalyzer(analyzer);
//        Query mltQ = mlt.like(docId);
//        BooleanQuery bq = new BooleanQuery();
//        bq.add(mltQ, BooleanClause.Occur.MUST);
//        bq.add(q0, BooleanClause.Occur.MUST_NOT);
        Document doc = reader.document(docId, BaseUtils.paramsAsSet("name", "descr", COLLECTIVE_FIELD_NAME));
        String nameVal = fixAttrValue(COLLECTIVE_FIELD_NAME, doc.get("name"));
        String descrVal = fixAttrValue(COLLECTIVE_FIELD_NAME, doc.get("descr"));
        String collectiveFieldVal = fixAttrValue(COLLECTIVE_FIELD_NAME, doc.get(COLLECTIVE_FIELD_NAME));

        if (logger.isDebugEnabled()) {
            logger.debug("prepareMoreLikeThisQuery: collectiveFieldVal=" + collectiveFieldVal);
        }

        BooleanQuery.Builder bqb = new BooleanQuery.Builder();

        bqb.add(q0, BooleanClause.Occur.MUST_NOT);
        if (nameVal != null) {
            bqb.add(createSingleMltQuery(docId, new StringReader(nameVal), "name", 1000000f), BooleanClause.Occur.SHOULD);
            bqb.add(createSingleMltQuery(docId, new StringReader(nameVal), "descr", 10000f), BooleanClause.Occur.SHOULD);
            bqb.add(createSingleMltQuery(docId, new StringReader(nameVal), COLLECTIVE_FIELD_NAME, 100f), BooleanClause.Occur.SHOULD);
        }
        if (descrVal != null) {
            bqb.add(createSingleMltQuery(docId, new StringReader(descrVal), "name", 10000f), BooleanClause.Occur.SHOULD);
            bqb.add(createSingleMltQuery(docId, new StringReader(descrVal), "descr", 1000f), BooleanClause.Occur.SHOULD);
            bqb.add(createSingleMltQuery(docId, new StringReader(descrVal), COLLECTIVE_FIELD_NAME, 10f), BooleanClause.Occur.SHOULD);
        }
        bqb.add(createSingleMltQuery(docId, new StringReader(collectiveFieldVal), "name", 100f), BooleanClause.Occur.SHOULD);
        bqb.add(createSingleMltQuery(docId, new StringReader(collectiveFieldVal), "descr", 10f), BooleanClause.Occur.SHOULD);
        bqb.add(createSingleMltQuery(docId, new StringReader(collectiveFieldVal), COLLECTIVE_FIELD_NAME, 1f), BooleanClause.Occur.SHOULD);

        return bqb.build();
    }

    protected <T> List<SearchWizardHitResult> gatherQueryResults(Query query, double minScore,
            int maxResults, QueryWords queryWords, boolean filterOutWhenNoOccurences,
            ISearchWizardBucketManager<T> bucketManager) throws IOException {

        long nanoTimeStart = System.nanoTime();

        if (query == null) {
            if (logger.isInfoEnabled()) {
                logger.info("no query, result: null");
            }
            return null;
        }

//        Map<Integer, Float> res = new LinkedHashMap<Integer, Float>();
//        List<SearchWizardHitResult> res = new ArrayList<SearchWizardHitResult>();
        Map<T, List<SearchWizardHitResult>> resMap = new HashMap<T, List<SearchWizardHitResult>>();

        if (maxResults < 0) {
            maxResults = Integer.MAX_VALUE;
        }

        int queryMaxResults = bucketManager == null ? maxResults : Integer.MAX_VALUE;

        int maxResultInAllBuckets = bucketManager == null ? maxResults : bucketManager.getBucketCount() * maxResults;

        TopDocs results = searcher.search(query, queryMaxResults);
        ScoreDoc[] hits = results.scoreDocs;

        int numTotalHits = results.totalHits;

        logTiming("gatherQueryResults (search for query)", numTotalHits, nanoTimeStart);

        if (logger.isDebugEnabled()) {
            logger.debug("gatherQueryResults: " + numTotalHits + " total matching documents");
        }

        int start = 0;
        int end = Math.min(numTotalHits, queryMaxResults);

        int collectedResults = 0;

        float maxScore = 0;

        for (int i = start; i < end; i++) {

            final ScoreDoc hit = hits[i];

            if (i == start) {
                maxScore = hit.score;
            }

            float currentScoreRel = maxScore > 0 ? hit.score / maxScore : (float) 1.0;

            if (currentScoreRel < minScore) {
                if (logger.isDebugEnabled()) {
                    logger.debug("current score rel (" + currentScoreRel + " < minScore (" + minScore + "), breaking at item #" + i);
                }
                break;
            }

            Document doc = searcher.doc(hit.doc);

            if (logger.isDebugEnabled()) {

                Map<String, String> vals = new LinkedHashMap<String, String>();
                for (IndexableField f : doc.getFields()) {
                    vals.put(f.name(), f.stringValue());
                }
                logger.debug("gatherQueryResults: result #" + i + ": " + vals);
            }

//                String path = doc.get("path");
            String objIdStr = doc.get(FtsByLucene.NODE_ID_FIELD_NAME);
            Integer objId = BaseUtils.tryParseInteger(objIdStr);
            if (objId != null) {
//                SimilarNodeBean snb = new SimilarNodeBean();
//                snb.id = objId;
//                snb.relevance = hit.score;
//                res.add(snb);
//                res.put(objId, hit.score);

                Set<String> occurenceFields;

                if (queryWords != null) {
                    occurenceFields = LuceneLameUtils.calcOccurenceInFields(analyzer, queryWords, doc, fieldsToIgnoreForOccurences);
                    if (logger.isDebugEnabled()) {
                        logger.debug("  occurences: " + occurenceFields);
                    }
                    if (occurenceFields.isEmpty()) {
                        if (filterOutWhenNoOccurences) {
                            if (logger.isDebugEnabled()) {
                                logger.debug("  no occurences - filtering out this result");
                            }
                            continue;
                        } else {
                            if (logger.isErrorEnabled()) {
                                logger.error("unexpected no occurences");
                            }
                        }
                    }
                } else {
                    occurenceFields = null;
                }

                T bucketKey = bucketManager == null ? null : bucketManager.assignToBucket(doc);

                if (bucketManager != null && bucketKey == null) {
                    continue;
                }

                List<SearchWizardHitResult> bucketList = resMap.get(bucketKey);

                if (bucketList == null) {
                    bucketList = new ArrayList<SearchWizardHitResult>();
                    resMap.put(bucketKey, bucketList);
                }

                if (bucketList.size() >= maxResults) {
                    continue;
                }

                collectedResults++;

                bucketList.add(new SearchWizardHitResult(objId, currentScoreRel, occurenceFields));
//                res.add(new SearchWizardHitResult(objId, currentScoreRel, occurenceFields)); //                res.put(objId, currentScoreRel);

                if (--maxResultInAllBuckets <= 0) {
                    break;
                }

                if (explainForNodeIds != null && explainForNodeIds.contains(objId)) {
                    System.out.println("+++ explain for " + objId + ":\n" + searcher.explain(query, hit.doc));
                }

//                if (sb.length() > 0) {
//                    sb.append(",");
//                }
//                sb.append("(").append(objIdStr).append(",").append(i).append(")");
//                if (logger.isInfoEnabled()) logger.info((i + 1) + ". " + objIdStr);
//                if (logger.isInfoEnabled()) logger.info("   doc id: " + hits[i].doc);
            } else {
                if (logger.isInfoEnabled()) {
                    logger.info((i + 1) + ". " + "No objId for this document");
                }
            }

            //if (logger.isInfoEnabled()) logger.info("Explanation for objId=" + objId + ": " + searcher.explain(query, hit.doc) + "\n\n");
        }

//            if (logger.isInfoEnabled()) logger.info("check in biks db:\n");
//            if (logger.isInfoEnabled()) logger.info("select * from bik_node n inner join (values " + sb + ") as v(node_id, vo) on n.id = v.node_id order by v.vo;\n\n");
        List<SearchWizardHitResult> res = new ArrayList<SearchWizardHitResult>(collectedResults);

        for (List<SearchWizardHitResult> v : resMap.values()) {
            res.addAll(v);
        }

        logTiming("gatherQueryResults (total)", res.size(), nanoTimeStart);

        return res;
    }

    @Override
    public <T> Map<Integer, Float> findSimilarNodeIds(int nodeId, double minScore, int limit,
            ISearchWizardBucketManager<T> bucketManager) {

        long nanoTimeStart = System.nanoTime();

        synchronized (readerSearcherLock) {
            try {
//            reader = DirectoryReader.open(dir);
//            searcher = new IndexSearcher(reader);
                if (reader == null) {
                    if (logger.isWarnEnabled()) {
                        logger.warn("findSimilarNodeIds: no reader");
                    }
                    return null;
                }
                Query q = prepareMoreLikeThisQuery(nodeId);
                if (logger.isInfoEnabled()) {
                    logger.info("mlt query=" + q);
                }

                Map<Integer, Float> res = LuceneLameUtils.convertSearchHitsToMap(
                        gatherQueryResults(q, minScore, limit, null, false, bucketManager));

                logTiming("findSimilarNodeIds", -1, nanoTimeStart);

                return res;
            } catch (IOException ex) {
                throw new RuntimeException("error in findSimilarNodeIds, nodeId=" + nodeId, ex);
            }
        }
    }

    @Override
    public List<SearchWizardHitResult> findNodeIds(String searchText, double minScore, int limit,
            String optAdvancedQueryText, boolean advancedQueryResultsMustOccur) {

        long nanoTimeStart = System.nanoTime();

        synchronized (readerSearcherLock) {
            try {
//            reader = DirectoryReader.open(dir);
//            searcher = new IndexSearcher(reader);
                if (reader == null) {
                    if (logger.isWarnEnabled()) {
                        logger.warn("findNodeIds: no reader");
                    }
                    return null;
                }

//                Collection<String> fs = MultiFields.getIndexedFields(reader);
//                String[] fields = fs.toArray(new String[fs.size()]);
//
//                Occur[] flags = new Occur[fields.length];
//                for (int i = 0; i < flags.length; i++) {
//                    flags[i] = Occur.SHOULD;
//                }
//
//                Query q = MultiFieldQueryParser.parse(searchText, fields, flags, analyzer);
//                Query q = new ComplexPhraseQueryParser("name", analyzer).parse(searchText);
                //Query q = new QueryParser("name", analyzer).parse(searchText);
//                Term t = new Term("name", "annapurna");
//                System.out.println("doc freq for term " + t + "=" + reader.docFreq(t));
//                Query q = new TermQuery(t);
//
//                //TokenStream ts = analyzer.tokenStream("name", "annapurna");
//                q = new PrefixQuery(new Term("name", "anna"));
                String searchTextFixed = fixAttrValue(COLLECTIVE_FIELD_NAME, searchText);

//                Map<String, Float> boosts = new LinkedHashMap<String, Float>();
//                boosts.put("name", 4f);
//                boosts.put("descr", 2f);
//                boosts.put(COLLECTIVE_FIELD_NAME, 1f);
//                String[] fields = new String[boosts.size()];
//                fields = boosts.keySet().toArray(fields);
//
//                MultiFieldQueryParser mfqp = new MultiFieldQueryParser(fields, analyzer/*, boosts*/);
//                mfqp.setDefaultOperator(QueryParser.Operator.AND);
//                mfqp.setAllowLeadingWildcard(true);
//                Query q = mfqp.parse(searchTextFixed);
//                StandardQueryParser parser = new StandardQueryParser(analyzer);
//                parser.setFieldsBoost(boosts);
//                BooleanQuery.Builder builder = new BooleanQuery.Builder();
//                for (String fieldName : fields) {
//                    builder.add(parser.parse(searchText, fieldName), BooleanClause.Occur.SHOULD);
//                }
//                Query q = builder.build();
                QueryWords queryWords = LuceneLameUtils.parseTextByAnalyzerToQueryWords(analyzer, searchTextFixed, "name");
                if (logger.isDebugEnabled()) {
                    logger.debug("findNodeIds: searchText=[" + searchText + "]");
                    logger.debug("findNodeIds: fixed searchText=[" + searchTextFixed + "]");
                    logger.debug("findNodeIds: query words: " + queryWords);
                }

                if (logger.isDebugEnabled()) {
                    logger.debug("findNodeIds: field names with all plain words: " + LuceneLameUtils.fieldsWithWords(reader, queryWords.plainWords, fieldsToIgnoreForOccurences));
                }

                Set<String> fieldsToSearch = new HashSet<String>();

                if (queryWords.plainWords.isEmpty()) {
                    fieldsToSearch.add("name");
                    fieldsToSearch.add("descr");
                    fieldsToSearch.add(COLLECTIVE_FIELD_NAME);
                } else {
                    Set<String> fieldsWithWords = LuceneLameUtils.fieldsWithWords(reader, queryWords.plainWords, fieldsToIgnoreForOccurences);
                    fieldsToSearch.addAll(fieldsWithWords);
                }

                if (logger.isDebugEnabled()) {
                    logger.debug("findNodeIds: fieldsToSearch: " + fieldsToSearch);
                }

                Set<String> searchWords = LuceneLameUtils.splitIntoWords(searchTextFixed);
                String queryTextFixed = "+" + BaseUtils.mergeWithSepEx(searchWords, " +");

                if (logger.isDebugEnabled()) {
                    logger.debug("findNodeIds: queryTextFixed=" + queryTextFixed);
                }

                Builder builder = new BooleanQuery.Builder();

                for (String fieldName : fieldsToSearch) {
                    QueryParser qp = new QueryParser(fieldName, analyzer);
                    qp.setAllowLeadingWildcard(true);
                    Query q = qp.parse(queryTextFixed);
                    builder.add(q, BooleanClause.Occur.SHOULD);
                }

                boolean filterOutWhenNoOccurences = true; //fieldsToSearch.contains(COLLECTIVE_FIELD_NAME);

                boolean hasAdvancedQueryText = !BaseUtils.isStrEmptyOrWhiteSpace(optAdvancedQueryText);

                if (hasAdvancedQueryText) {
                    QueryParser qp = new QueryParser(COLLECTIVE_FIELD_NAME, analyzer);
                    qp.setAllowLeadingWildcard(true);
                    Query q = qp.parse(optAdvancedQueryText);
                    builder.add(q, advancedQueryResultsMustOccur ? BooleanClause.Occur.MUST : BooleanClause.Occur.SHOULD);
                }

                Query q = builder.build();

//                String[] fields = fieldsToSearch.toArray(new String[fieldsToSearch.size()]);
//                MultiFieldQueryParser mfqp = new MultiFieldQueryParser(fields, analyzer/*, boosts*/);
//                mfqp.setDefaultOperator(QueryParser.Operator.AND);
//                mfqp.setAllowLeadingWildcard(true);
//                Query q = mfqp.parse(queryTextFixed);
                if (logger.isInfoEnabled()) {
                    logger.info("findNodeIds: query=" + q);
                }

                List<SearchWizardHitResult> res = gatherQueryResults(q,
                        minScore, limit, hasAdvancedQueryText ? null : queryWords,
                        filterOutWhenNoOccurences, null);

                logTiming("findNodeIds", -1, nanoTimeStart);

                return res;
            } catch (Exception ex) {
                throw new RuntimeException("error in findNodeIds, searchText=" + searchText, ex);
            }
        }
    }

    @Override
    public void destroy() {

        if (logger.isInfoEnabled()) {
            logger.info("LuceneSimilarNodesWizard: destroy start");
        }

        destroying = true;

        pool.shutdown();

//        try {
//            pool.awaitTermination(30, TimeUnit.SECONDS);
//        } catch (Exception ex) {
//            throw new RuntimeException("exception in destroy", ex);
//        } finally {
//            pool.shutdownNow();
//            pool = null;
//        }
        if (logger.isInfoEnabled()) {
            logger.info("LuceneSimilarNodesWizard: destroy after pool shutdown");
        }

        pool.shutdownNow();

        try {
            IndexReader r = reader;
            reader = null;
            writer.close();
            r.close();
            analyzer.close();
            dir.close();
        } catch (Exception ex) {
            throw new RuntimeException("exception in destroy 2", ex);
        }

        if (logger.isInfoEnabled()) {
            logger.info("LuceneSimilarNodesWizard: destroy end");
        }
    }

//    public static void main(String[] args) {
//        System.out.println("fixed=" + fixAttrValue("aqq", "report.two"));
//    }
}
