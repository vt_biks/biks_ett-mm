/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.IPaginatedItemsDisplayer;
import pl.fovis.client.bikwidgets.MyBIKSNewsGrid;
import pl.fovis.client.dialogs.AddOrEditNewsDialog;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.NewsBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author mmastalerz
 */
public class MyBIKSNewsTab extends MyBIKSTabWithButtonBase<NewsBean> {

    @Override
    public String getCaption() {
        return I18n.aktualnosci.get() /* I18N:  */;
    }

    @Override
    public String getButtonStyle() {
        return "labelLink-news";
    }

    @Override
    protected boolean isHeaderBtnEnabled(boolean forEmptyFilteredItems) {
        return BIKClientSingletons.isEffectiveExpertLoggedInEx(BIKConstants.ACTION_NEWS_EDITOR);
//      return BIKClientSingletons.isEffectiveExpertLoggedIn();
    }

    @Override
    protected IPaginatedItemsDisplayer<NewsBean> createPaginatedItemsDisplayer() {
        return new MyBIKSNewsGrid();
    }

    @Override
    protected String getFilterCheckBoxCaption() {
        return I18n.pokazTylkoNieprzeczytane.get() /* I18N:  */;
    }

    @Override
    protected List<NewsBean> filterAllItems() {
        final List<NewsBean> l = new ArrayList<NewsBean>();
        for (NewsBean nb : allItems) {
            if (!nb.isRead) {
                l.add(nb);
            }
        }
        return l;
    }

    @Override
    protected String getOptHeaderBtnCaption() {
        return I18n.dodajAktualnosc.get() /* I18N:  */;
    }

    @Override
    protected String getHeaderBtnStyle() {
        return "News" /* i18n: no:css-class */ + "-addNewsEntry";
    }

    @Override
    protected void headerButtonClicked() {

        new AddOrEditNewsDialog().buildAndShowDialog(new NewsBean(), new IParametrizedContinuation<NewsBean>() {
            @Override
            public void doIt(NewsBean param) {
                BIKClientSingletons.getService().addNews(param, new StandardAsyncCallback<NewsBean>() {
                    @Override
                    public void onSuccess(NewsBean result) {
                        BIKClientSingletons.showInfo(I18n.dodanoAktualnosc.get() /* I18N:  */);
                        allItems.add(0, result);
                        displayData();
                        refreshTabButton();
                    }
                });
            }
        });

    }

    @Override
    protected String getCaptionWhenListEmpty() {
        return I18n.brakAktualnosci.get() /* I18N:  */;
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<NewsBean>> callback) {
        if (BIKClientSingletons.isUserLoggedIn()) {
            BIKClientSingletons.getService().getNewsByUser(callback);
        }
    }
}
