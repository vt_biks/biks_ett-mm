/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

/**
 *
 * @author tflorczak
 */
public class ConnectionParametersDQCBean extends ParametersBaseBean {

    public String server;
    public String instance;
    public String database;
    public String user;
    public String password;
    public Integer activeFilter;
    public String groupFilter;
    public Integer withInactiveTests;
    public Integer checkTestActivity;
}
