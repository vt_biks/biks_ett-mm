/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.LameUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class FoxySqlCache {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    private static final String foxySqlCacheKeyPrefix = "--$fsck>:>$";
    private static final String foxySqlCacheKeyPostfix = "$<:<fsck$.\n";
    private static final Set<String> defaultSqlCacheKeys = Collections.singleton("");
    protected Map<String, Map<String, FoxySqlCacheItem>> caches = new HashMap<String, Map<String, FoxySqlCacheItem>>();

    public static String makeFoxySqlCacheKeysTxt(String keys) {
        return foxySqlCacheKeyPrefix + keys + foxySqlCacheKeyPostfix;
    }

    public static Set<String> extractCacheKeys(String sqlTxt) {
        if (BaseUtils.isStrEmpty(sqlTxt)) {
            return defaultSqlCacheKeys;
        }

        int idx = 0;
        int len = BaseUtils.strLengthFix(sqlTxt);

        while (idx < len && BaseUtils.isWhiteSpaceChar(sqlTxt.charAt(idx))) {
            idx++;
        }

        if (!sqlTxt.startsWith(foxySqlCacheKeyPrefix, idx)) {
            return defaultSqlCacheKeys;
        }

        int startPos = idx + foxySqlCacheKeyPrefix.length();
        int endPos = sqlTxt.indexOf(foxySqlCacheKeyPostfix, startPos);
        if (endPos == -1) {
            throw new LameRuntimeException("there is foxySqlCacheKeyPrefix ("
                    + foxySqlCacheKeyPrefix + ") but no postfix ("
                    + foxySqlCacheKeyPostfix + ") in sqlTxt:\n" + sqlTxt);
        }
        String keys = sqlTxt.substring(startPos, endPos);
        Set<String> res = BaseUtils.splitUniqueBySep(keys, ",", true);
        return res.isEmpty() ? defaultSqlCacheKeys : res;
    }

    public FoxySqlCacheItem findInCache(Set<String> keys, String sqlTxt) {
        //Set<String> keys = extractCacheKeys(sqlTxt);
        String keyOne = keys.iterator().next();
        Map<String, FoxySqlCacheItem> items = caches.get(keyOne);
        FoxySqlCacheItem item = items == null ? null : items.get(sqlTxt);
        if (item == null) {
            // miss
            if (logger.isDebugEnabled()) {
                logger.debug("cache miss: " + sqlTxt);
            }
            return null;
        } else {
            // hit
            if (logger.isDebugEnabled()) {
                logger.debug("cache hit: " + sqlTxt + ", saved time " + item.duration + " millis");
            }
            return item;
        }
    }

    public void invalidateBySqlTxt(String cmdTxt) {
        Set<String> keys = extractCacheKeys(cmdTxt);
        for (String key : keys) {
            Map<String, FoxySqlCacheItem> items = caches.get(key);
            if (items != null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("removing " + items.size() + " item(s) for key (" + key + ")");
                }
                for (FoxySqlCacheItem item : items.values()) {
                    removeItemFromOtherLists(key, item);
                }
            }
        }
    }

    protected void removeItemFromOtherLists(String key, FoxySqlCacheItem item) {
        String sqlTxt = item.sqlTxt;
        for (String k : item.keys) {
            if (!BaseUtils.safeEquals(k, key)) {
                Map<String, FoxySqlCacheItem> items = caches.get(k);
                if (items != null) {
                    items.remove(sqlTxt);
                }
            }
        }
    }

    public void addToCache(FoxySqlCacheItem item) {
        for (String key : item.keys) {
            Map<String, FoxySqlCacheItem> im = caches.get(key);
            if (im == null) {
                im = new HashMap<String, FoxySqlCacheItem>();
                caches.put(key, im);
            }
            im.put(item.sqlTxt, item);
            //BaseUtils.putInMapOfMap(caches, key, item.sqlTxt, item);
        }
    }
}
