/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.gwtservlet;

import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.serverlogic.IFoxyServiceOnServer;

/**
 *
 * @author pmielanczuk
 */
public interface IFoxyServiceConfigurator<CFG, SD, SO> {

    public String getAdhocSqlsWebPath();

    public Class<CFG> getConfigBeanClass();

    public Class<SD> getSessionDataClass();

    public IFoxyServiceOnServer<CFG, SD, SO> getServiceImpl();

    public String getAppName();

    public IBeanConnectionEx<Object> createDBConnection(CFG cfg);
}
