package pl.trzy0.foxy.commons.tags;

import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class FoxyParsingRuntimeException extends LameRuntimeException {

    private int rowNum;
    private int colNum;
    private String msg;
    private String sourceFileName;

    public FoxyParsingRuntimeException(String message, int rowNum, int colNum, String sourceFileName) {
        super("parsing exception in file " + sourceFileName + " at row: " + rowNum +
                ", col: " + colNum + ": " + message);
        this.sourceFileName = sourceFileName;
        this.msg = message;
        this.rowNum = rowNum;
        this.colNum = colNum;
    }

    public String getMsg() {
        return msg;
    }

    public int getRowNum() {
        return rowNum;
    }

    public int getColNum() {
        return colNum;
    }
}
