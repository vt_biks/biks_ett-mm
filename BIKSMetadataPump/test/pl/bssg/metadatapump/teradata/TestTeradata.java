/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.teradata;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import pl.bssg.metadatapump.PumpUtils;

/**
 *
 * @author tflorczak
 */
public class TestTeradata {

    public static void main(String[] args) {

        Connection tc = PumpUtils.getConnection("com.teradata.jdbc.TeraDriver", "jdbc:teradata://192.168.167.21", "dbc", "dbc");
        try {
            Statement createStatement = tc.createStatement();
            System.out.println("===================================");
//            ResultSet executeQuery = createStatement.executeQuery("SELECT * FROM DBC.TABLES where DatabaseName='SQLJ' or DatabaseName='GISData'");
//            ResultSet executeQuery = createStatement.executeQuery("SELECT * FROM DBC.COLUMNS where DatabaseName='SQLJ' and TableName='JAR_JAR_USAGE'");
//            ResultSet executeQuery = createStatement.executeQuery("SELECT * FROM DBC.TVM where TVMNameI='SQLJ'");
//            ResultSet executeQuery = createStatement.executeQuery("SELECT * FROM DBC.DBASE");
            ResultSet executeQuery = createStatement.executeQuery("SELECT * FROM DBC.Databases");
            while (executeQuery.next()) {
                for (int i = 1; i < 64; i++) {
                    System.out.print("" + executeQuery.getString(i) + ", ");
                }
                System.out.println("");
                System.out.println("============================");
                System.out.println("");
            }

        } catch (SQLException ex) {
            System.out.println("" + ex);
        } catch (RuntimeException re) {
            System.out.println("Błąd z konfiguracją!\nUpewnij się że jest ona odpowiednio ustawiona w zakłądce Admin oraz czy zawiera poprawne zapytanie Select");
        }
    }
}
