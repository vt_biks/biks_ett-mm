﻿exec sp_check_version '1.7.z5.2';
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees') 
if not exists(select * from bik_node where tree_id = @tree_id and obj_id = '#admin:dict:sysGroupUsers')
begin
	exec sp_add_menu_node 'admin:dict', 'Grupy użytkowników', '#admin:dict:sysGroupUsers'
	exec sp_node_init_branch_id @tree_id, null
	
	update bik_node set is_deleted=1 where obj_id='#admin:dict:sysGroupUsers'
	
	insert into bik_translation(code, txt, lang, kind)
	values ('#admin:dict:sysGroupUsers', 'User groups', 'en', 'node')
end
go


if exists(select * from sys.objects obj where obj.parent_object_id = OBJECT_ID('bik_custom_right_role_user_entry') and obj.type = ('UQ'))
begin
	declare @constName varchar(512);
	select @constName = obj.name from sys.objects obj where obj.parent_object_id = OBJECT_ID('bik_custom_right_role_user_entry') and obj.type = ('UQ')
	
	exec('alter table bik_custom_right_role_user_entry drop constraint ' + @constName);
end;
go

alter table bik_custom_right_role_user_entry add constraint UQ_bik_custom_right_role_user_entry UNIQUE (user_id, role_id,tree_id,node_id,group_id );

exec sp_update_version '1.7.z5.2', '1.7.z5.3';
go
