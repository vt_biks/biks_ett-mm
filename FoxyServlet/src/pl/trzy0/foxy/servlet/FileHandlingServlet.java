/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import commonlib.LameUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.Tuple3;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public class FileHandlingServlet extends HttpServlet {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected String dirForUpload;
    private static Map<String, String> fileExtToContentTypeMap = new HashMap<String, String>() {
        {
            put(".doc", "application/msword");
            put(".docx", "application/vnd.openxmlformats"); //lub application/vnd.openxmlformats-officedocument.wordprocessingml.document
            put(".xls", "application/excel");
            put(".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            put(".ppt", "application/powerpoint");
            put(".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
            put(".rtf", "application/rtf");
            put(".csv", " text/csv");
            //open office
            put(".odt", "application/vnd.oasis.opendocument.text");
            put(".ods", "application/vnd.oasis.opendocument.spreadsheet");
            put(".odp", "application/vnd.oasis.opendocument.presentation");
            put(".odg", "application/vnd.oasis.opendocument.graphics");
            put(".odc", "application/vnd.oasis.opendocument.chart");
            put(".odf", "application/vnd.oasis.opendocument.formula");
            put(".odi", "application/vnd.oasis.opendocument.image");
            put(".odm", "application/vnd.oasis.opendocument.text-master");
        }
    };

    protected int getUploadFileSizeMax() {
        return -1;
    }

    protected void serveFile(String fileName, HttpServletResponse response) {

        String filePath = dirForUpload;

        // Check if file name is supplied to the request.
        if (fileName != null) {
            // Strip slashes (avoid directory sniffing by hackers!).
            fileName = fileName.replaceAll("\\\\|/", "");
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("serveFile: no file");
            }
            return;
        }
        // Prepare file object.
        File file = new File(filePath, fileName);

        // Check if file actually exists in filesystem.
        if (!file.exists()) {
            if (logger.isDebugEnabled()) {
                logger.debug("serveFile: file \"" + fileName + "\" does not exist");
            }
            return;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("serveFile: file exists");
        }
        // Get content type by filename.
        String contentType = URLConnection.guessContentTypeFromName(fileName);
        if (contentType == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("extractFileExt: \"" + BaseUtils.extractFileExt(fileName) + "\"");
            }
            contentType = fileExtToContentTypeMap.get(BaseUtils.extractFileExt(fileName));
        }
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        if (logger.isDebugEnabled()) {
            logger.debug("serveFile: content type: " + contentType);
        }

        // Prepare streams.
        BufferedInputStream input = null;
        BufferedOutputStream output = null;
        try {
            // Open image file.
            input = new BufferedInputStream(new FileInputStream(file));

            int contentLength = input.available();

            // Init servlet response.
            response.setContentLength(contentLength);
            response.setCharacterEncoding("UTF-8");
            response.setContentType(contentType);
            response.setHeader("Content-disposition", "inline; filename=\"" + fileName + "\""); //attachment inline
            output = new BufferedOutputStream(response.getOutputStream());

            // Write file contents to response.
            while (contentLength-- > 0) {
                output.write(input.read());
            }
            // Finalize task.
            output.flush();
        } catch (IOException e) {
            if (logger.isErrorEnabled()) {
                logger.error("serveFile: something went wrong while serving file: " + fileName);
            }
            e.printStackTrace();
        } finally {
            // Gently close streams.
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    if (logger.isErrorEnabled()) {
                        logger.error("serveFile: something went wrong while closing file: " + fileName);
                    }
                    e.printStackTrace();
                }
            }
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    if (logger.isErrorEnabled()) {
                        logger.error("serveFile: something went wrong while closing output for file: " + fileName);
                    }
                    e.printStackTrace();
                }
            }
        }
    }

    protected byte[] preprocessUploadedFile(byte[] fileContent, String fileName) {
        return fileContent;
    }

    protected void processUploadRequest(HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        //System.out.println("----------------- UPLOAD ----------------");

        Map<String, Object> res = new HashMap<String, Object>();

        // Create a factory for disk-based file items
        FileItemFactory factory = new DiskFileItemFactory();

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
        try {
            int fileSizeMax = getUploadFileSizeMax();
            if (fileSizeMax >= 0) {
                upload.setFileSizeMax(fileSizeMax);
            }
            List /* FileItem */ items = upload.parseRequest(request);

            // Process the uploaded items
            Iterator iter = items.iterator();
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();

                if (item.isFormField()) {
                    // zwykłe pole
                    String name = item.getFieldName();
                    String value;
                    try {
                        value = item.getString("UTF-8");
                    } catch (UnsupportedEncodingException ex) {
                        throw new LameRuntimeException(ex);
                    }

                    res.put(name, value);

                } else {
                    // plik
                    String fieldName = item.getFieldName();
                    String fileName = item.getName();
                    long sizeInBytes = item.getSize();

                    res.put(fieldName, new Tuple3<String, byte[], Long>(fileName, item.get(), sizeInBytes));
                }
            }

        } catch (FileUploadException ex) {
            //ex.printStackTrace();
            throw new LameRuntimeException(ex);
        }
        @SuppressWarnings("unchecked")
        Tuple3<String, byte[], Long> t3 = (Tuple3) res.get("file");
        String fileName = t3.v1;
        if (logger.isDebugEnabled()) {
            logger.debug("processUploadRequest: fileName = " + fileName);
        }

        byte[] fileBytes = preprocessUploadedFile(t3.v2, fileName);

        String ext = BaseUtils.extractFileExtFromFullPath(fileName);
        //LameUtils.extractFileExtNoDot(fileName);
        //System.out.println("------- dir for upload: \"" + configObj.dirForUpload + "\" -------");
        File dir = new File(dirForUpload);

        File file = File.createTempFile("file_", ext, dir);

        FileOutputStream fileOut = new FileOutputStream(file);
        fileOut.write(fileBytes);
        fileOut.close();

        PrintWriter out = response.getWriter();
        out.print(file.getName());
        out.close();
    }

    @Override
    public void doPost(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf8");
        String fileName = request.getParameter("get");
        if (!BaseUtils.isStrEmptyOrWhiteSpace(fileName)) {
            serveFile(fileName, response);
        } else {
            processUploadRequest(request, response);
        }
    }

    @Override
    public void doGet(
            HttpServletRequest req,
            HttpServletResponse res)
            throws ServletException, IOException {
        doPost(req, res);
    }
}
