/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import java.io.Serializable;
import pl.fovis.foxygwtcommons.dialogs.validators.IValidationStateChangeHandler;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pmielanczuk
 */
public abstract class FoxyValidatingDialogBase<B extends Serializable> extends FoxySerializableDialogBase<B, B> {

    public static String VALIDATION_ERROR_CSS_CLASS_NAME = "validationError"; //ww: nie jest final aby można sobie było to nadpisać
    protected B bean;
//    protected IParametrizedContinuation<B> actionCont;
//    protected B outputBean;
    protected IAsyncDialogAction<B> dialogAction;
//    protected Map<Widget, Set<IWidgetValueValidator>> widgetsWithErrors = new HashMap<Widget, Set<IWidgetValueValidator>>();
    private boolean isNewBeanMode;
    protected boolean isActionInProgress;

    public void buildAndShowDialog(B inBean, IAsyncDialogAction<B> dialogAction) {
        this.bean = inBean;
        this.isNewBeanMode = inBean == null;
        this.dialogAction = dialogAction;
        buildAndShowDialog();
    }

    protected boolean isNewBeanMode() {
        return isNewBeanMode;
    }

    @Deprecated
    public void buildAndShowDialog(B inBean, final IParametrizedContinuation<B> actionCont) {
        buildAndShowDialog(inBean, new IAsyncDialogAction<B>() {
            public void doIt(B bean, IParametrizedContinuation<String> onCompleteCont) {
                onCompleteCont.doIt(null);
                actionCont.doIt(bean);
            }
        });
    }

    protected void addValidators() {
        // no-op
    }

    protected void customFillControlsBeforeShow() {
        // no-op
    }

    @Override
    protected void afterBuildAllWidgetsBeforeShow() {
        super.afterBuildAllWidgetsBeforeShow();
        if (bean == null) {
            bean = createBean();
        }
        fillControlsFromBean(bean);
        customFillControlsBeforeShow();

//        serializer.setValueChangedHandler(new IParametrizedContinuation<Object>() {
//            @Override
//            public void doIt(Object param) {
//                inputControlValueChanged((Widget) param);
//            }
//        });
        serializer.setValidationStateChangeHandler(new IValidationStateChangeHandler() {
            public void stateChanged(Widget inputControl, boolean isNowInvalid) {
                inputControl.setStyleName(VALIDATION_ERROR_CSS_CLASS_NAME, isNowInvalid);
            }

            public void setErrorMessage(Widget inputControl, String errMsg) {
                inputControl.setTitle(errMsg);
            }

            public void updateFinished(boolean isAnythingNowInvalid) {
                updateActionButtonEnabled();
            }
        });

        addValidators();

        @SuppressWarnings("unchecked")
        Class<B> outputBeanClass = (Class<B>) bean.getClass();
        serializer.setOutputBeanClass(outputBeanClass);

        checkForInvalidValues();
    }

    protected abstract B createBean();

    @Override
    protected final boolean doActionBeforeClose() {

        if (isActionInProgress) {
            return false;
        }

        boolean hasErrors = checkForInvalidValues();

        if (hasErrors) {
            return false;
        }

        serializeFormAndPerformAction();

        return false;
    }

    @Override
    protected final void doAction() {
        //no-op
        // i tak zostanie
    }

    //ww: true -> wartości się nie walidują, jest błąd
    // false -> nie znaleziono błędów, jest OK
    protected boolean checkForInvalidValues() {

//        Iterable<IWidgetValueValidator> allValidators = getAllValidators();
//        validateValidators(allValidators);
        serializer.validateValidators(serializer.getAllValidators());

        return //!invalidControlsMap.isEmpty();
                serializer.hasValidationErrors();
    }

//    protected void validateValidators(Iterable<IWidgetValueValidator> validatorsToExecute) {
//        serializer.validateValidators(validatorsToExecute);
//    }
    protected void customFillBeanBeforeAction() {
        // no-op
    }

    protected void setIsActionInProgress(boolean isInProgress) {
        this.isActionInProgress = isInProgress;
        updateActionButtonEnabled();
//        if (actionBtn != null) {
//            actionBtn.setEnabled(!isActionInProgress);
//        }
    }

    protected void serializeFormAndPerformAction() {
        setIsActionInProgress(true);

        try {
            fillBeanFromControls(bean);
            customFillBeanBeforeAction();

            dialogAction.doIt(bean, new IParametrizedContinuation<String>() {
                public void doIt(String param) {
                    if (param != null) {
                        if (!"".equals(param)) {
                            setIsActionInProgress(false);
                            Window.alert(param);
                        } else {
                            //no-op
                            //nie pokazuj alert, nie zamyka okno
                            setIsActionInProgress(false);
                            actionBtn.setEnabled(true);
                            actionBtn.removeStyleName("gwt-PushButton-up-disable");
                            actionBtn.addStyleName("gwt-PushButton-up");
                        }
                    } else {
                        hideDialog();
                    }
                }
            });
        } catch (Exception ex) {
            setIsActionInProgress(false);
            throw new RuntimeException("exception in" /* i18n: no */ + " serializeFormAndPerformAction", ex);
        }
//        actionCont.doIt(bean);
//        hideDialog();
    }

    protected boolean isNoActionMode() {
        return actionBtn == null;
    }

    protected boolean isUpdateButtonEnabledByAdditionalConditions() {
        return true;
    }

    protected boolean isUpdateButtonEnabled() {
        return !isActionInProgress
                && isUpdateButtonEnabledByAdditionalConditions()
                && !serializer.hasValidationErrors();
    }

    protected void updateActionButtonEnabled() {
        if (actionBtn != null) {
            actionBtn.setEnabled(isUpdateButtonEnabled());
        }
    }
//    protected List<IWidgetValueValidator> getValidatorsForControl(Widget inputControl) {
//        return serializer.getValidatorsForControl(inputControl);
//    }
//    protected List<Object> getControlsForValidator(IWidgetValueValidator validator) {
//        return serializer.getControlsForValidator(validator);
//    }
//    protected Iterable<IWidgetValueValidator> getAllValidators() {
//        return serializer.getAllValidators();
//    }
//    protected void inputControlValueChanged(Widget inputControl) {
//        validateValidators(getValidatorsForControl(inputControl));
//    }
}
