/*
 * MuppetMaker.java
 *
 * Created on 28 grudzie� 2006, 09:57
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package commonlib;

import simplelib.LameRuntimeException;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import simplelib.SimpleDateUtils;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class MuppetMaker {

    static ILameLogger logger = LameLoggerFactory.getLogger(MuppetMaker.class);
    //LameLogger.getClassLogger();

    public static String toDiagString(final Field f) {
        return f.getType().getCanonicalName() + " " + f.getName();
    }

    public static String toDiagString(final Method m) {
        Class[] parTypes = m.getParameterTypes();
        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (Class parType : parTypes) {
            sb.append(sep);
            sb.append(parType.getCanonicalName());
            sep = ", ";
        }
        return Modifier.toString(m.getModifiers()) + " " + m.getReturnType().getCanonicalName() + " " + m.getName() + "(" + sb.toString() + ");";
    }

    @SuppressWarnings("unchecked")
    public static <T> T convertToCompatible(Class<T> target, Object val, Object parentMuppet) {
        if (val == null) {
            return null;
        }
        if (target.isAssignableFrom(val.getClass())) {
            return (T) val;
        }
        if (target.isArray()) {
            List subNodes = (List) ((Map) val).get("#subNodes");
            List subMuppets = new ArrayList(subNodes.size());
            for (Object subNode : subNodes) {
                if (subNode instanceof Map) {
                    if (target.getComponentType().isAssignableFrom(subNode.getClass())) {
                        subMuppets.add(subNode);
                    } else {
                        subMuppets.add(newMuppet(target.getComponentType(), (Map) subNode, parentMuppet));
                    }
                }
            }
            Object res = Array.newInstance(target.getComponentType(), subMuppets.size());
            for (int i = 0; i < subMuppets.size(); i++) {
                Array.set(res, i, subMuppets.get(i));
            }

            return (T) res;
        }
        if (val instanceof Map) {
            return (T) newMuppet(target, (Map) val, parentMuppet);
        }

        String strVal = val.toString();

        if (target.isEnum()) {
            Enum ttt = Enum.valueOf((Class<Enum>) target, strVal);
            return (T) ttt;
        }

        if (target == float.class || target == Float.class) {
            return (T) Float.valueOf(strVal);
        }
        if (target == double.class || target == Double.class) {
            return (T) Double.valueOf(strVal);
        }
        if (target == long.class || target == Long.class) {
            return (T) Long.valueOf(strVal);
        }
        if (target == int.class || target == Integer.class) {
            return (T) Integer.valueOf(strVal);
        }
        if (target == boolean.class || target == Boolean.class) {
            return (T) Boolean.valueOf(strVal);
        }
        if (target == Date.class) {
            return (T) SimpleDateUtils.parseFromCanonicalString(strVal);
        }
        return (T) strVal;
    }

    public static void setProperty(Object obj, String propName, Object propVal) {
        setProperty(obj, propName, propVal, false);
    //MuppetMerger.setProperty(obj, propName, propVal);
    }

    public static void setProperty(Object obj, String propName, Object propVal, boolean caseInsensitive) {
        String setterName = LameUtils.getSetter(propName);
        //logger.debug("propName: " + propName + ", setterName: " + setterName);
        Method[] methods = obj.getClass().getMethods();
        for (Method met : methods) {
            //logger.debug("method: " + toDiagString(met));
            boolean nameEquals = LameUtils.equalStrings(met.getName(), setterName, caseInsensitive);
            boolean paramOk = nameEquals && met.getParameterTypes().length == 1;
            boolean returnOk = paramOk && met.getReturnType() == Void.TYPE;
            //logger.debug("nameEquals: " + nameEquals + ", paramOk: " + paramOk + ", returnOk: " + returnOk);

            if (/*nameEquals && paramOk &&*/returnOk) {
                try {
                    if (logger.isDebugEnabled()) {
                        logger.debug("invoking setter: " + toDiagString(met));
                    }
                    met.invoke(obj, convertToCompatible(met.getParameterTypes()[0], propVal, obj));
                } catch (Exception ex) {
                    throw new LameRuntimeException("error setting prop (via method): " + propName +
                            " for object " + obj + " of class " + LameUtils.safeGetClassName(obj) + ", msg: " + ex.getMessage(), ex);
                }
                return;
            }
        }
        try {
            Field fld = null;

            if (caseInsensitive) {
                for (Field fld2 : obj.getClass().getFields()) {
                    if (LameUtils.equalStrings(fld2.getName(), propName, caseInsensitive)) {
                        fld = fld2;
                        break;
                    }
                }
            } else {
                fld = obj.getClass().getField(propName);
            }
            if (fld == null) {
                throw new LameRuntimeException("no field or method for property: " + propName);
            }

            if (logger.isDebugEnabled()) {
                logger.debug("setting field: " + toDiagString(fld));
            }
            fld.set(obj, convertToCompatible(fld.getType(), propVal, obj));
        } catch (Exception ex) {
            throw new LameRuntimeException("error setting field: " + propName +
                    " for object " + obj + " of class " + LameUtils.safeGetClassName(obj) + ", msg: " + ex.getMessage(), ex);
        }
    }

    public static <T> T newMuppet(Class<T> c, Map<String, Object> m, Object parentMuppet) {
        T obj;
        Constructor<T> cons = null;
        try {
            if (parentMuppet != null) {
                try {
                    if (logger.isDebugEnabled()) {
                        logger.debug("looking for constructor of class " + c.getSimpleName() + "(" + parentMuppet.getClass().getSimpleName() + ")");
                    }
                    cons = c.getConstructor(parentMuppet.getClass());
                    if (logger.isDebugEnabled()) {
                        logger.debug("found constructor of class " + c.getSimpleName() + "(" + parentMuppet.getClass().getSimpleName() + ")");
                    }
                } catch (NoSuchMethodException ex) {
                    // specjalnie puste...
                }
            }
            if (cons != null) {
                obj = cons.newInstance(parentMuppet);
            } else {
                obj = c.newInstance();
            }
        } catch (Exception ex) {
            throw new LameRuntimeException("Cannot instantiate new muppet of class: " + c.getName() + " with" +
                    (cons != null ? "" : "out") + " parent cons", ex);
        }

        for (Entry<String, Object> e : m.entrySet()) {
            String key = e.getKey();
            if (key.startsWith("#")) {
                if ("#subNodes".equals(key)) {
                    for (Object o : (List) e.getValue()) {
                        if (o instanceof Map) {
                            Map innerMap = (Map) o;
                            setProperty(obj, (String) innerMap.get("#name"), innerMap);
                        }
                    }
                } else if ("#name".equals(key)) {
                    //ok, ignorujemy
                } else {
                    throw new LameRuntimeException("unknown #-key: " + key);
                }
            } else {
                setProperty(obj, key, e.getValue());
            }
        }

        return obj;
    }

    public static <T> T newMuppet(Class<T> c, Map<String, Object> m) {
        return newMuppet(c, m, null);
    }

    public static <T> T newMuppet(Class<T> c, InputStream is, Object parentMuppet, String location) {
        Map<String, Object> muppetMap;
        try {
            muppetMap = XMLMapper.readMap(is, location);
        } catch (Exception ex) {
            throw new LameRuntimeException("error converting xmlContent to muppetMap, msg: " + ex.getMessage(), ex);
        }
        return newMuppet(c, muppetMap, parentMuppet);
    }

    public static <T> T newMuppet(Class<T> c, byte[] bytes, Object parentMuppet, String location) {
        return newMuppet(c, new ByteArrayInputStream(bytes), parentMuppet, location);
    }

    public static <T> T newMuppet(Class<T> c, byte[] bytes, String location) {
        return newMuppet(c, bytes, null, location);
    }

    public static <T> T newMuppet(Class<T> c, InputStream is, String location) {
        return newMuppet(c, is, null, location);
    }

    public static <T> T newBeanMuppet(Class<T> c, Map<String, Object> beanMap) {
        return newBeanMuppetWithPrefix(c, beanMap, "");
    }

    public static <T> T newBeanMuppetWithPrefix(Class<T> c,
            Map<String, Object> beanMap, String prefix) {
        Map<String, Object> muppetMap = new LinkedHashMap<String, Object>();
        for (Entry<String, Object> e : beanMap.entrySet()) {
            muppetMap.put(LameUtils.tableColNameToBeanFieldName(
                    LameUtils.cutPrefix(e.getKey(), prefix)), e.getValue());
        }
        return newMuppet(c, muppetMap, null);
    }

    public static <T> T newMuppetFromFile(Class<T> c, String fileName) {
        Map<String, Object> muppetMap;
        try {
            //muppetMap = XMLMapper.readMap(new FileReader(fileName));
            muppetMap = XMLMapper.readMap(new FileInputStream(fileName), fileName);
        } catch (Exception ex) {
            throw new LameRuntimeException("error converting xml file " + fileName + " to muppetMap, msg: " + ex.getMessage(), ex);
        }
        return newMuppet(c, muppetMap);
    }
//     public static <T> T newMuppet(Class<T> c, String xmlContent, Object parentMuppet) {
//        return newMuppet(c, new ByteArrayInputStream(xmlContent.getBytes()), parentMuppet);
//        /* Map<String, Object> muppetMap;
//        try {
//            muppetMap = XMLMapper.readMap(new StringReader(xmlContent));
//        } catch (Exception ex) {
//            throw new LameRuntimeException("error converting xmlContent to muppetMap", ex);
//        }
//        return newMuppet(c, muppetMap, parentMuppet);
//         */
//    }
//
//     public static <T> T newMuppet(Class<T> c, String xmlContent) {
//        return newMuppet(c, xmlContent, null);
//    }
}
