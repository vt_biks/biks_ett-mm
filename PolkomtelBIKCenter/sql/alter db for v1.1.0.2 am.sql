﻿
--naprawa numerka wersji
update bik_app_prop
set val = '1.1.0.2'
where name = 'bik_ver' and val = '1.1.1'
go
-------------------------------------------------------------------------
-------------------------------------------------------------------------
exec sp_check_version '1.1.0.2';
go
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
if  exists (select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[bik_node]') and name = N'idx_bik_node_parent_node_id_linked_node_id_is_deleted')
	drop index idx_bik_node_parent_node_id_linked_node_id_is_deleted on bik_node 
go
-------------------------------------------------------------------------
create nonclustered index idx_bik_node_parent_node_id_linked_node_id_is_deleted on bik_node (parent_node_id, linked_node_id, is_deleted)
go
-------------------------------------------------------------------------
exec sp_update_version '1.1.0.2', '1.1.0.3';
go
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------