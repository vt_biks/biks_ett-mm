/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class PumpGroupBean implements Serializable {

    public Date startDate;
    public int interval;
//    public List<String> sources;

    public PumpGroupBean() {
    }

    public PumpGroupBean(Date startDate, int interval) {
        this.startDate = startDate;
        this.interval = interval;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PumpGroupBean other = (PumpGroupBean) obj;
        if (this.startDate != other.startDate && (this.startDate == null || !this.startDate.equals(other.startDate))) {
            return false;
        }
        if (this.interval != other.interval) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (this.startDate != null ? this.startDate.hashCode() : 0);
        hash = 79 * hash + this.interval;
        return hash;
    }
}
