/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.common.ConnectionParametersSapBoExBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.IAdminConfigBaseWidget;

/**
 *
 * @author ctran
 */
public class AdminConfigSapBoExWidget implements IAdminConfigBaseWidget<ConnectionParametersSapBoExBean> {

    protected AdminConfigSapBoWidget adminWidget = new AdminConfigSapBoWidget();
    protected AdminConfigSapBoExtPropWidget extPropWidget = new AdminConfigSapBoExtPropWidget();

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_SAPBO;
    }

    @Override
    public void populateWidgetWithData(ConnectionParametersSapBoExBean data) {
        adminWidget.populateWidgetWithData(data.servers);
        extPropWidget.populateWidgetWithData(data.exProp);
    }

    @Override
    public Widget buildWidget() {
        TabPanel tabPanel = new TabPanel();
        tabPanel.setAnimationEnabled(true);
        tabPanel.add(adminWidget.buildWidget(), I18n.serwery.get());
        tabPanel.add(extPropWidget.buildWidget(), I18n.sciezkiDoPlikow.get());
        tabPanel.selectTab(0);
        return tabPanel;
    }
}
