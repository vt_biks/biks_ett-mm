/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.entitydetailswidgets.IGridBeanAction;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.ObjectInHistoryBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author beata
 */
public class ObjectInHistoryBeanExtractor extends NodeAwareBeanExtractorBase<ObjectInHistoryBean> {

    public ObjectInHistoryBeanExtractor() {
        super(false);
    }

    public Integer getNodeId(ObjectInHistoryBean b) {
        return b.nodeId;
    }

    public String getTreeCode(ObjectInHistoryBean b) {
        return b.tabId;
    }

    protected String getName(ObjectInHistoryBean b) {
        return b.name;
    }

    protected String getNodeKindCode(ObjectInHistoryBean b) {
        return b.code;
    }

    public String getBranchIds(ObjectInHistoryBean b) {
        return null;
    }

    @Override
    public int getAddColCnt() {
        return 2;
    }

    @Override
    public Object getAddColVal(ObjectInHistoryBean b, int addColIdx) {
        return addColIdx == 0 ? b.histCount : createDateAdded(b.dateAdded, "dqc" /* I18N: no */ + "-dateAdded");
    }

    @Override
    public boolean createGridColumnNames(FlexTable gp) {
        int col = 1;
        gp.setWidget(0, col++, new Label(I18n.nazwa.get() /* I18N:  */));
        gp.setWidget(0, col++, new Label(I18n.odwiedzin.get() /* I18N:  */));
        gp.setWidget(0, col++, new Label(I18n.ostatnieOdwiedziny.get() /* I18N:  */));
        gp.setWidget(0, col++, new Label(""));
        return true;
    }

    @Override
    protected List<IGridBeanAction<ObjectInHistoryBean>> createActionList() {
        List<IGridBeanAction<ObjectInHistoryBean>> res = new ArrayList<IGridBeanAction<ObjectInHistoryBean>>();
        res.add(actView);
        res.add(actFollow);
        return res;
    }
}
