exec sp_check_version '1.0.1';
go

----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_check_version]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_check_version]
GO

create procedure sp_check_version (@ver varchar(255))
as
	declare @version varchar(255)
begin	
	select @version = val from bik_app_prop;
	if(@version != @ver)
	begin
		raiserror(N'!!! Wrong version number %s. Current version is %s. !!!',--Message text
				20,
				-1,
				@ver, @version) with log;
				print N'Wrong number of version.Current version is %s';
	end
	
end
go

----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

alter table bik_node add branch_ids varchar(1000) null;
go

----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_node_init_branch_id]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_node_init_branch_id]
GO

create procedure sp_node_init_branch_id (@opt_tree_id int, @opt_node_id int)
as
begin  
  set nocount on

  create table #ids (id int not null primary key);
  
  insert into #ids (id) 
  select ID from bik_node 
  where 
    (@opt_tree_id is null or tree_id = @opt_tree_id)
    and 
    (  @opt_node_id is null and parent_node_id is null 
    or 
       @opt_node_id is not null and id = @opt_node_id
    )
   ;
    
  create table #child_ids (id int not null primary key);
    
  while exists (select 1 from #ids)
  begin
    update bik_node
    set branch_ids = coalesce((select parent.branch_ids from bik_node as parent where parent.id = bik_node.parent_node_id), '')
    + CAST(id as varchar(10)) + '|'
    where bik_node.id in (select id from #ids);
    
    truncate table #child_ids;
    
    insert into #child_ids (id)
    select ID from bik_node where parent_node_id in (select id from #ids);
    
    truncate table #ids;
    
    insert into #ids (id) select ID from #child_ids;
  end
    
  drop table #ids;
  drop table #child_ids;
end
go


--update bik_node set branch_ids = null

--exec sp_node_init_branch_id null, 200


--exec sp_update_version '1.0.1', '1.0.2';
--go


exec sp_node_init_branch_id null, null
go


----------------------------------------------------------------------
----------------------------------------------------------------------

exec sp_check_version '1.0.1';
go

----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
/*Atrubuty dodatkowe*/
create table bik_attribute(
	id int not null identity(1,1) primary key,
	name varchar(255) not null,
	node_kind_id int not null references bik_node_kind(id),
	constraint uk_bik_attribute unique (name, node_kind_id)
);
go

create table bik_attribute_linked(
	id int not null IDENTITY(1,1) primary key,
	attribute_id int not null references bik_attribute(id),
	value varchar(max) null,
	node_id int not null references bik_node(id)
);
go
----------------------------------------------------------------------
----------------------------------------------------------------------
/*U�ytkownicy*/
create table bik_user(
	id int not null IDENTITY(1,1) primary key,
	name varchar(250) not null unique,
	email varchar(250) null,
	phone_num varchar(200) null,
	kind int not null,
	short_descr varchar(max) null,
	password varchar(250) null
);
go

insert into bik_user (name, email, phone_num, kind,short_descr, password)
select name, email, phone_num, kind,short_descr, password
from aaa_user;
go
----------------------------------------------------------------------
----------------------------------------------------------------------
/*Blogi*/
create table bik_blog(
	id int not null identity(1,1) primary key,
	content varchar(max) not null,
	subject nvarchar(150) not null,
	user_id int not null references bik_user(id),
	date_added datetime not null,
);
go

create table bik_blog_linked(
	id int not null identity(1,1) primary key,
	blog_id int not null references bik_blog(id),
	node_id int not null references bik_node(id),
	constraint uk_bik_blog_linked unique (node_id, blog_id)
);
go
----------------------------------------------------------------------
----------------------------------------------------------------------
/*S�owa kluczowe*/
create table bik_keyword(
	id int not null identity(1,1) primary key,
	body varchar(800) not null unique
);
go

create table bik_keyword_linked(
	id int not null identity(1,1) primary key,
	keyword_id int not null references bik_keyword(id),
	node_id int not null references bik_node(id),
	constraint uk_bik_keyword_linked unique (node_id, keyword_id)	
);
go
----------------------------------------------------------------------
----------------------------------------------------------------------
/*Notatki*/
create table bik_note(
	id int not null identity(1,1) primary key,
	title varchar(255) not null,
	body varchar(max) null,
	user_id int not null references bik_user(id),
	date_added datetime not null,
	node_id int not null references bik_node(id)
);
go
----------------------------------------------------------------------
----------------------------------------------------------------------
/*Metapedia*/
create table bik_metapedia(
	id int not null identity(1,1) primary key,
	subject varchar(800) not null,
	body varchar(max) not null,
	official smallint null,
);
go

insert into bik_metapedia (subject, body, official)
select subject, body, official
from aaa_mpart 
go

create table bik_metapedia_linked(
	id int not null identity(1,1) primary key,
	metapedia_id int not null references bik_metapedia(id),
	node_id int not null references bik_node(id),
	constraint uk_bik_metapedia_linked unique (node_id, metapedia_id)
);
go

insert into bik_metapedia_linked(metapedia_id, node_id)
select bik_metapedia.id, bik_node.id
  from bik_metapedia join aaa_mpart on bik_metapedia.body=aaa_mpart.body and bik_metapedia.subject=aaa_mpart.subject and bik_metapedia.official=aaa_mpart.official
  	join aaa_tree_node on aaa_mpart.tree_node_id=aaa_tree_node.id 
	join bik_node on aaa_tree_node.id = bik_node.obj_id;
go

create table bik_metapedia_keyword(
	id int not null identity(1,1) primary key,
	metapedia_id int not null references bik_metapedia(id),
	keyword_id int not null references bik_keyword(id),
	constraint uk_metapedia_keyword unique (keyword_id, metapedia_id)
);
go
----------------------------------------------------------------------
----------------------------------------------------------------------
/*Za��czniki*/
create table bik_attachment(
	id int not null identity(1,1) primary key,
	href varchar(max) not null,
	caption varchar(max) not null,
	node_id int not null references bik_node(id),
);
go
----------------------------------------------------------------------
----------------------------------------------------------------------
create table bik_specialist(
	proj_id int null references bik_user(id),
	spon_id int null references bik_user(id),
	expe_id int null references bik_user(id),
	node_id int not null references bik_node(id),
);
go

----------------------------------------------------------------------
----------------------------------------------------------------------
/*dodanie nowej tabeli bik_tree*/
alter table bik_tree
	add tree_kind int check(tree_kind in(0,1,2));
go

update bik_tree set tree_kind = 1 - is_metadata
go

update bik_tree set tree_kind=2 where name='Tezaurus'
go
--alter table bik_tree 
  --drop column is_metadata;
  
  
----------------------------------------------------------------------
---------------------------------------------------------------------- 
/*Teradata*/
create table bik_data_source_def(
	id int not null identity(1,1) primary key,
	description char(50) null,
);
go
SET IDENTITY_INSERT bik_data_source_def ON
GO

insert into bik_data_source_def(id, description) values(1, 'TERADATA DBMS')
insert into bik_data_source_def(id, description) values(2, 'BO')
GO

SET IDENTITY_INSERT bik_data_source_def OFF
GO

create table bik_data_load_log(
	id int not null identity(1,1) primary key,
    data_source_type_id int not null references bik_data_source_def(id),
	creation_time datetime not null,
    status int not null,
    status_description varchar(1000) null
);
go

create table bik_teradata(
	id int not null identity(1,1) primary key,
	parent_teradata_id int null references bik_teradata(id),
    data_load_log_id int not null references bik_data_load_log(id),
    type varchar(100) not null,
	name varchar(100) not null,
    extra_info varchar(5000) null,
    branch_names varchar(1000) null,
    constraint uk_teradata_object unique (data_load_log_id, parent_teradata_id, type, name)
);
go

----------------------------------------------------------------------
----------------------------------------------------------------------

  
exec sp_update_version '1.0.1', '1.0.2';
go
