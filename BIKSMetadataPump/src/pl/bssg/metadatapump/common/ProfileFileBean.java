/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class ProfileFileBean implements Serializable {

//    %LIBS FID GLOBAL AKEY1 AKEY2 AKEY3 AKEY4 AKEY5 AKEY6 AKEY7 DEL SYSSN NETLOC PARFID MPLCTDD DFTDES DFTORD DFTHDR DFTDES1 LTD USER FILETYP EXIST EXTENDLENGTH FSN FDOC QID1 ACCKEYS VAL4EXT PREDAEN SCREEN RFLAG DFLAG UDACC UDFILE FPN UDPRE UDPOST PUBLISH ZRSFILE GLREF RECTYP PTRUSER PTRTLD LOG ARCHFILES ARCHKEY PTRTIM PTRUSERU PTRTLDU PTRTIMU LISTDFT LISTREQ DES
    public String libs; // Library Name                             12  T
    public String fid; // File Name                                256  U
    public String global; // Global Name                               8  T
    public String akey1; // Access Key 1                             12  T
    public String akey2; // Access Key 2                             12  T
    public String akey3; // Access Key 3                             12  T
    public String akey4; // Access Key 4                             12  T
    public String akey5; // Access Key 5                             12  T
    public String akey6; // Access Key 6                             12  T
    public String akey7; // Access Key 7                             12  T
    public Integer del; // ASCII Delimiter                           3  N
    public String syssn; // System Name                               3  U
    public Integer netloc; // Network Location                          1  N
    public String parfid; // Inheritance File Name                    12  U
    public String mplctdd; // Implicit Data Dictionary Reference       25  T
    public String dftdes; // Look-up Table List                       200  T
    public String dftord; // Descending Order                          1  L
    public String dfthdr; // Default Heading                          78  T
    public String dftdes1; // Look-up Table List (Line 2)              200  T
    public Date ltd; // Last Updated                             10  D
    public String usr; // User ID                                  20  T
    public Integer filetyp; // File Type                                 1  N
    public Integer exist; // Record Existed Indicator (node number)    6  N
    public String extendlength; // Extended File and Column Names Allowed    1  L
    public String fsn; // File Short Name                          12  T
    public String fdoc; // Documentation File Name                  30  U
    public String qid1; // Query                                    100  T
    public String acckeys; // Primary Keys                             100  U
    public String val4ext; // Valid for Extraction                      1  L
    public String predaen; // Data Entry Pre-Processor                 255  T
    public String screen; // File Maintenance Data Entry Screen IDs   12  U
    public String rflag; // File Maintenance Restriction Flag         1  L
    public String dflag; // Deletion Restriction Flag                 1  L
    public String udacc; // Primary Keys Access Routine               8  U
    public String udfile; // Record Filer Routine                      8  U
    public String fpn; // Data Item Protection Filename             4  U
    public String udpre; // User Defined Authorization Routine       20  U
    public String udpost; // User Defined File Post Processor         20  U
    public String publish; // Publish Routine                          30  T
    public String zrsfile; // Rutyna restrykcji                        10  T
    public String glref; // MUMPS Global Reference                   100  T
    public Integer rectyp; // Record Type                               2  N
    public String ptruser; // Data Item Name (User Created)            12  U
    public String ptrtld; // Data Item Name (Date Created)            12  U
    public String log; // Enable Automatic Logging                  1  L
    public String archfiles; // Archive Filelist                         255  T
    public String archkey; // Archive Key                              12  U
    public String ptrtim; // Data Item@Name (Time Created)            12  U
    public String ptruseru; // Date Item Name (User Last Updated)       12  U
    public String ptrtldu; // Data Item Name (Date Last Updated)       12  U
    public String ptrtimu; // Data Item Name (Time Last Updated)       12  U
    public String listdft; // Default Data Item List                   450  T
    public String listreq; // Required Data Item List                  500  T
    public String des; // Description                              40  T

    public ProfileFileBean() {
    }

    public ProfileFileBean(String libs, String fid, String global, String akey1, String akey2, String akey3, String akey4, String akey5, String akey6, String akey7, Integer del, String syssn, Integer netloc, String parfid, String mplctdd, String dftdes, String dftord, String dfthdr, String dftdes1, Date ltd, String usr, Integer filetyp, Integer exist, String extendlength, String fsn, String fdoc, String qid1, String acckeys, String val4ext, String predaen, String screen, String rflag, String dflag, String udacc, String udfile, String fpn, String udpre, String udpost, String publish, String zrsfile, String glref, Integer rectyp, String ptruser, String ptrtld, String log, String archfiles, String archkey, String ptrtim, String ptruseru, String ptrtldu, String ptrtimu, String listdft, String listreq, String des) {
        this.libs = libs;
        this.fid = fid;
        this.global = global;
        this.akey1 = akey1;
        this.akey2 = akey2;
        this.akey3 = akey3;
        this.akey4 = akey4;
        this.akey5 = akey5;
        this.akey6 = akey6;
        this.akey7 = akey7;
        this.del = del;
        this.syssn = syssn;
        this.netloc = netloc;
        this.parfid = parfid;
        this.mplctdd = mplctdd;
        this.dftdes = dftdes;
        this.dftord = dftord;
        this.dfthdr = dfthdr;
        this.dftdes1 = dftdes1;
        this.ltd = ltd;
        this.usr = usr;
        this.filetyp = filetyp;
        this.exist = exist;
        this.extendlength = extendlength;
        this.fsn = fsn;
        this.fdoc = fdoc;
        this.qid1 = qid1;
        this.acckeys = acckeys;
        this.val4ext = val4ext;
        this.predaen = predaen;
        this.screen = screen;
        this.rflag = rflag;
        this.dflag = dflag;
        this.udacc = udacc;
        this.udfile = udfile;
        this.fpn = fpn;
        this.udpre = udpre;
        this.udpost = udpost;
        this.publish = publish;
        this.zrsfile = zrsfile;
        this.glref = glref;
        this.rectyp = rectyp;
        this.ptruser = ptruser;
        this.ptrtld = ptrtld;
        this.log = log;
        this.archfiles = archfiles;
        this.archkey = archkey;
        this.ptrtim = ptrtim;
        this.ptruseru = ptruseru;
        this.ptrtldu = ptrtldu;
        this.ptrtimu = ptrtimu;
        this.listdft = listdft;
        this.listreq = listreq;
        this.des = des;
    }

    @Override
    public String toString() {
        return "ProfileFileBean{" + "libs=" + libs + ", fid=" + fid + ", global=" + global + ", akey1=" + akey1 + ", akey2=" + akey2 + ", akey3=" + akey3 + ", akey4=" + akey4 + ", akey5=" + akey5 + ", akey6=" + akey6 + ", akey7=" + akey7 + ", del=" + del + ", syssn=" + syssn + ", netloc=" + netloc + ", parfid=" + parfid + ", mplctdd=" + mplctdd + ", dftdes=" + dftdes + ", dftord=" + dftord + ", dfthdr=" + dfthdr + ", dftdes1=" + dftdes1 + ", ltd=" + ltd + ", usr=" + usr + ", filetyp=" + filetyp + ", exist=" + exist + ", extendlength=" + extendlength + ", fsn=" + fsn + ", fdoc=" + fdoc + ", qid1=" + qid1 + ", acckeys=" + acckeys + ", val4ext=" + val4ext + ", predaen=" + predaen + ", screen=" + screen + ", rflag=" + rflag + ", dflag=" + dflag + ", udacc=" + udacc + ", udfile=" + udfile + ", fpn=" + fpn + ", udpre=" + udpre + ", udpost=" + udpost + ", publish=" + publish + ", zrsfile=" + zrsfile + ", glref=" + glref + ", rectyp=" + rectyp + ", ptruser=" + ptruser + ", ptrtld=" + ptrtld + ", log=" + log + ", archfiles=" + archfiles + ", archkey=" + archkey + ", ptrtim=" + ptrtim + ", ptruseru=" + ptruseru + ", ptrtldu=" + ptrtldu + ", ptrtimu=" + ptrtimu + ", listdft=" + listdft + ", listreq=" + listreq + ", des=" + des + '}';
    }

}
