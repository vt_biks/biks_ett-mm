/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs.validators;

import com.google.gwt.user.client.ui.Widget;
import pl.fovis.foxygwtcommons.dialogs.FoxyDialogSerializer;

/**
 *
 * @author pmielanczuk
 */
public class WidgetValueValidatorParsing<BB, INB extends BB, OUTB extends BB> implements IWidgetValueValidator {

    protected final Widget inputControl;
    private final FoxyDialogSerializer<BB, INB, OUTB> serializer;

    public WidgetValueValidatorParsing(final FoxyDialogSerializer<BB, INB, OUTB> serializer, Widget inputControl) {
        this.serializer = serializer;
        this.inputControl = inputControl;
    }

    public String validate() throws FoxyGWTValidationException {
        // to może rzucić błędem - będzie znaczyło, że się nie zwalidowało
        serializer.getInputControlValue(inputControl);
        return null;
    }
}
