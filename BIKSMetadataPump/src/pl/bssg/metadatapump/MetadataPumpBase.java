/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import commonlib.LameUtils;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public abstract class MetadataPumpBase implements IBIKPumpTaskNew {

//    static {
//        LameUtils.tryReadAndApplyLameLoggersConfig("/lameLoggersConfig.properties");
//    }
    protected static final int PACKAGE_SIZE = 500;
    private static final ILameLogger logger = LameUtils.getMyLogger();
    public static String ADHOC_SQLS_PATH = LameUtils.getPackagePathOfClass(MetadataPumpBase.class) + "metadataAdhocSqls.html";
//    protected IBeanConnectionEx<Object> dbConnection;
    protected INamedSqlsDAO<Object> adhocDao;
    protected IPumpLogger pumpLogger;
    protected String instanceName = null;
    private IBeanConnectionEx<Object> mc;
    protected String urlForPublicAccess;
    protected String dirForUpload;

    protected INamedSqlsDAO<Object> getAdhocDao() {
        return adhocDao;
    }

    public final void setAdhocDao(INamedSqlsDAO<Object> adhocDao) {
        this.adhocDao = adhocDao;
    }

    protected IBeanConnectionEx<Object> getMc() {
        return mc;
    }

    @Override
    public final Integer setConnectionAndLog(String urlForPublicAccess, String dirForUpload,
            IBeanConnectionEx<Object> mc, INamedSqlsDAO<Object> adhoDAO, IPumpLogger pumpLogger) {
        this.urlForPublicAccess = urlForPublicAccess;
        this.dirForUpload = dirForUpload;
        setPumpLoggerFromExecutor(pumpLogger);
        this.adhocDao = adhoDAO;
        this.mc = mc;
        return insertMainLog();
    }

    @Override
    public void setOptPumpInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

//    public void setConnectionWithoutLog(IBeanConnectionEx<Object> mc) {
//        this.mc = mc;
//        this.adhocDao = BIKPumpExecutor.createAdhocDao(mc);
//    }
//    public abstract String getSourceName();
    @Override
    public String getPumpGroup() {
        return getSourceName();
    }

    @Override
    public boolean isActionAfterNeeded() {
        return true;
    }

//    protected abstract Integer insertMainLog();
    protected Integer insertMainLog() {
        return insertStartLog(getSourceName(), instanceName);
    }

    protected static INamedSqlsDAO<Object> createAdhocDao(IBeanConnectionEx<Object> mc) {
        return FoxyAppUtils.createNamedSqlsDAOFromJar(mc, ADHOC_SQLS_PATH);
    }

    @Override
    public final Pair<Integer, String> pump() {
//        insertStartLog(getSourceName()); // wstawianie logu jest z poziomu executora
        return startPump();
    }

    protected abstract Pair<Integer, String> startPump();

//    protected void commitTran() {
//        dbConnection.finalizeTran(true);
//    }
    protected void execNamedCommandWithCommit(String name, Object... args) {
        adhocDao.execNamedCommand(name, args);
//        commitTran(); // polaczenie jest juz w autocommitmode
    }

    protected <V> V execNamedQuerySingleVal(String qryName, boolean allowNoResult,
            String optColumnName, Object... args) {
        return BeanConnectionUtils.<V>execNamedQuerySingleVal(adhocDao, qryName, allowNoResult,
                optColumnName, args);
    }

    protected <V> Set<V> execNamedQuerySingleColAsSet(String qryName, String optColumnName, Object... args) {
        return BeanConnectionUtils.execNamedQuerySingleColAsSet(getAdhocDao(), qryName, optColumnName, args);
    }

    protected void execCommand(String command) {
        mc.execCommand(command);
//        commitTran(); // polaczenie jest juz w autocommitmode
    }

    protected String toSqlString(Object... args) {
        return BeanConnectionUtils.mergeAsSqlStrings(mc, args);
    }

    protected int insertStartLog(String source, String serverName) {
        int insertedLogId = (Integer) execNamedQuerySingleVal("insertStartLog", true, "id", source, serverName);
//        commitTran(); // polaczenie jest juz w autocommitmode
        return insertedLogId;
    }

    protected int getLastStartLog(String source) {
        return (Integer) execNamedQuerySingleVal("getInsertedLogId", false, "id", source);
    }

    protected Connection getConnection(String driverClass, String url, String user, String password) {
        return PumpUtils.getConnection(driverClass, url, user, password, logger);
    }

    protected void closeConnection(Connection connection) {
        PumpUtils.closeConnection(connection, logger);
    }

    protected void insertListToDBInPackages(List list, String queryName) {
        insertListToDBInPackagesEx(list, queryName, PACKAGE_SIZE);
    }

    protected void insertListToDBInPackagesEx(List list, String queryName, int packageSize) {
        int listStart = 0;
        while (listStart < list.size()) {
            if (list.size() - listStart <= packageSize) {
                execNamedCommandWithCommit(queryName, list.subList(listStart, list.size()));
            } else {
                execNamedCommandWithCommit(queryName, list.subList(listStart, listStart + packageSize));
            }
            listStart = listStart + packageSize;
        }
    }

    public static Map<String, String> createObjectMapFromString(String filterString) {
        if (filterString == null || filterString.isEmpty()) {
            return null;
        }
        Map<String, String> map = new HashMap<String, String>();
        List<String> filterForDB = BaseUtils.splitBySep(filterString, ";");
        int line = 0;
        for (String string : filterForDB) {
            line++;
            if (string.trim().isEmpty()) {
                continue;
            }
            int index = string.indexOf("=");
            if (index != -1) {
                map.put(new String(string.substring(0, index)), new String(string.substring(index + 1)));
            } else {
                throw new LameRuntimeException("error in line " + line + ": " + string);
            }
        }
        return map;
    }

    protected void setPumpLoggerFromExecutor(IPumpLogger pumpLogger) {
        if (!isEmbeded()) {
            this.pumpLogger = pumpLogger;
        }
    }

    protected boolean isEmbeded() {
        return false;
    }
}
