/*
 * RenderCounters.java
 *
 * Created on 21 marzec 2007, 15:59
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public interface RenderCounters {
    void increaseRenderCounter(String tagName);
    
    void increaseRenderCounter(String tagName, int increaseBy);
    
    Map<String, Integer> getRenderCounters();
}
