/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence;

/**
 *
 * @author tflorczak
 */
public class ConfluenceConfigBean {

    public Integer isActive;
    public String url;
    public String user;
    public String password;
    public Integer checkChildrenCount;

    public ConfluenceConfigBean() {
    }

    public ConfluenceConfigBean(Integer isActive, String url, String user, String password, Integer checkChildrenCount) {
        this.isActive = isActive;
        this.url = url;
        this.user = user;
        this.password = password;
        this.checkChildrenCount = checkChildrenCount;
    }
}
