/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.FileUploadDialogForDocuments;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.FoxyFileUpload;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class NodeActionAddDocument extends NodeActionUpdatingBase {

    @Override
    public String getCaption() {
        return I18n.dodajDokument.get() /* I18N:  */;
    }

    @Override
    public void execute() {
        if (!isSocialUserLoggedInWithInfo()) {
            return;
        }

        final FileUploadDialogForDocuments fud = new FileUploadDialogForDocuments(BIKClientSingletons.getUploadableChildrenNodeKinds(node.nodeKindUploadableChildrenKinds));
        fud.buildAndShowDialog(new IParametrizedContinuation<Pair<String, FoxyFileUpload>>() {
            @Override
            public void doIt(Pair<String, FoxyFileUpload> param) {
                BIKClientSingletons.getService().addBikDocument(node.id,
                        param.v2.getClientFileName(), param.v2.getServerFileName(), param.v1, new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on" /* I18N: no */ + " addNewDocument") {
                    @Override
                    public void onSuccess(Pair<Integer, List<String>> result) {
                        BIKClientSingletons.showInfo(I18n.dodanoDokument.get() /* I18N:  */);
                        refreshRedisplayDataAndSelectRecord(result.v1/*, false*/);
                        refreshAndRedisplayDataByTreeCode(result.v2);
                        //fud.dialog.hide();
                    }
                });
            }
        });
    }

    @Override
    public boolean isEnabledByDesign() {
        boolean isEnabled = !(isNodeLinkedAnyhow() || isTreeReadOnly())
                && BIKClientSingletons.getUploadableChildrenNodeKinds(node.nodeKindUploadableChildrenKinds) != null
                && BIKClientSingletons.isLoggedUserHasPrivilege2PublicAttr(node)
                && !(BIKConstants.NODE_KIND_META_PRINT_ATTRIBUT.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(data.node.nodeKindId))
                || BIKConstants.NODE_KIND_META_PRINT_KIND.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(data.node.nodeKindId))
                || BIKConstants.NODE_KIND_META_PRINT_BUILT_IN_KIND.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(data.node.nodeKindId))
                || BIKConstants.NODE_KIND_META_PRINTS_TEMPLATES.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(data.node.nodeKindId))
                || BIKConstants.NODE_KIND_META_PRINT.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(data.node.nodeKindId)));
        return isEnabled;
//        return isNodeNotLinkedAnyhowNorReadOnlyTreeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_DOCUMENTS_FOLDER)/* && isSystemUserBikUserLoggedIn()*/;
    }
}
