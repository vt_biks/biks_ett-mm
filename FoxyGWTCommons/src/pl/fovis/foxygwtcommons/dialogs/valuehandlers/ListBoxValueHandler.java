/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs.valuehandlers;

import com.google.gwt.user.client.ui.ListBox;

/**
 *
 * @author pmielanczuk
 */
public class ListBoxValueHandler implements IWidgetValueHandler<ListBox> {

    public boolean isWidgetClassHandled(Object widget) {
        return widget instanceof ListBox;
    }

    public Object getValueFromWidget(ListBox lb, Class targetValueClass, GetValueExtraInfo extraInfo) {
        int idx = lb.getSelectedIndex();
        return idx < 0 ? null : lb.getValue(idx);
    }

    public void setValueToWidget(ListBox widget, Object value, SetValueExtraInfo extraInfo) {
        // no-op
    }
}
