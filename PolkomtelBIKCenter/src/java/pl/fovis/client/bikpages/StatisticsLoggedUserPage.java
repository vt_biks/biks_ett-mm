/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author bfechner
 */
public class StatisticsLoggedUserPage extends StatisticsPageBase {

//    public String getCaptionDeprecated() {
//        return I18n.zalogowaniUzytkownicy.get() /* I18N:  */;
//    }
    public String getId() {
        return "admin:dict:userstatistics";
    }

    public void show(VerticalPanel vp) {

        vp.clear();
        grid.clear();

        BIKClientSingletons.getService().getAllUserLogged(new StandardAsyncCallback<List<String>>() {
            public void onSuccess(List<String> result) {
                grid.clear();
                int row = 0;
                for (String u : result) {
                    if (!BaseUtils.isStrEmpty(u)) {
                        grid.setWidget(row, 0, getHtmlAndStyle("Statistics" /* i18n: no */ + "-nrLogUser", "" + (row + 1)));
                        grid.setWidget(row, 1, getHtmlAndStyle("Statistics-title", u));
                        grid.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-grey" : "RelatedObj-oneObj-white");
                        row++;
                    }
                }
                grid.getColumnFormatter().setWidth(0, "20px");
            }
        });
        final Label l = new Label(I18n.listaUzytkownikow.get() /* I18N:  */);
        l.setStyleName("Statistics" /* i18n: no */ + "-tableTitleName");

        vp.add(l);
        vp.add(grid);
    }
}
