/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.util.Map;
import simplelib.LameRuntimeException;
import simplelib.JsonConverter;

/**
 *
 * @author wezyr
 */
public class JsonConverterEx extends JsonConverter {

    public JsonConverterEx(Class superClass) {
        super(superClass);
    }
    
    @Override
    protected boolean convertCompound(Object o, StringBuilder sb) {
        boolean converted = super.convertCompound(o, sb);

        if (converted) {
            return true;
        }

        Class currentSuperClass = getCurrentSuperClass();
        Map map = MuppetToMapProjector.convertToMap(o, currentSuperClass);

        if (map.isEmpty()) {
            throw new LameRuntimeException("cannot get properties from object " + o + " of class " +
                    o.getClass().getCanonicalName() + ", empty property map, " + getContextInfo());
        }

        convertMap(map, sb);

        return true;
    }

    public static String convertObject(Object o, Class superClass) {
        JsonConverter converter = new JsonConverterEx(superClass);
        StringBuilder sb = new StringBuilder();
        converter.convertObject(o, sb);
        return sb.toString();
    }

    public static String convertObject(Object o) {
        return convertObject(o, (Class)null);
    }
}
