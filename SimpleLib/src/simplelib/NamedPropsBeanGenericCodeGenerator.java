/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.Date;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class NamedPropsBeanGenericCodeGenerator {

    public static interface IPropClass {

        public boolean isPrimitiveType();

        public boolean isAssignableTo(Class c);

        public boolean isAssignableFrom(Class c);

        public String getFullName();
    }

    public static interface IMainClass {

        public String getSimpleName();

        public Iterable<String> getPropNames();

        public IPropClass getPropClass(String name);
    }
    protected IMainClass mainClass;
    protected String mainClassNameSimple;
    protected StringBuilder sb = new StringBuilder();
    protected Iterable<String> propNames;
//    protected Set<String> additionalImports = new LinkedHashSet<String>();

    public NamedPropsBeanGenericCodeGenerator(IMainClass mainClass) {
        this.mainClass = mainClass;
        this.propNames = mainClass.getPropNames();
        this.mainClassNameSimple = mainClass.getSimpleName();
    }

    private String makeExpressionForPropAssignment(String propName) {

        StringBuilder mySb = new StringBuilder();

        IPropClass propClass = //MuppetMerger.getPropertyClass(mainClass, propName, true);
                mainClass.getPropClass(propName);

        String accessMethod = null;
        String accessPrefix = "";
//        boolean isPropClassPrimitive = propClass.isPrimitiveType();

        if (propClass.isAssignableFrom(Integer.class) /*&& !isPropClassPrimitive*/) {
            accessMethod = "Integer";
        } else if (propClass.isAssignableFrom(int.class)) {
            accessMethod = "Int";
        } else if (propClass.isAssignableFrom(String.class)) {
            accessMethod = "String";
        } else if (propClass.isAssignableFrom(boolean.class)) {
            accessMethod = "Bool";
        } else if (propClass.isAssignableFrom(double.class)) {
            accessMethod = "Double";
        } else if (propClass.isAssignableFrom(Double.class)) {
            accessMethod = "Double";
        } else if (propClass.isAssignableFrom(Boolean.class)) {
            accessMethod = "Boolean";
        } else if (propClass.isAssignableFrom(Date.class)) {
            accessMethod = "Date";
        } else if (propClass.isAssignableFrom(Map.class)) {
            accessMethod = "Map";
            accessPrefix = "(Map)";
        }

//        final String propClassSimpleName = propClass.getSimpleName();
        final String propClassFullName = propClass.getFullName();
        if (accessMethod != null) {
            mySb.append(accessPrefix).append("JsonValueHelper.get").append(accessMethod).append("(value)");
        } else {
            if (propClass.isAssignableTo(Enum.class)) {
//                additionalImports.add(propClass.getFullName());
                mySb.append("BaseUtils.safeStringToEnum(").append(propClassFullName).append(".class, JsonValueHelper.getString(value))");
            } else /*if (propClass.isAssignableTo(Collection.class))*/ {
                mySb.append("(").append(propClassFullName).append(")value");
            } /*else {
             throw new LameRuntimeException("property " + propName + " of class " + mainClass.getSimpleName()
             + " has unsupported type " + propClassSimpleName);
             }*/
        }

        return mySb.toString();
    }

    protected void makeNamedPropsBean() {
        String className = mainClass.getSimpleName();

        sb.append("private static final Map<String, Class> propClasses = new LinkedHashMap<String, Class>();\n");
        sb.append("private static final Map<String, INamedPropGetterSetter<").append(className).append(">> namedProps = new LinkedHashMap<String, INamedPropGetterSetter<").append(className).append(">>();\n");
        sb.append("static {\n");
        for (String prop : propNames) {
            sb.append("  propClasses.put(\"").append(prop).append("\", ").append(mainClass.getPropClass(prop).getFullName()).append(".class);\n");
            sb.append("  namedProps.put(\"").append(prop).append("\", new INamedPropGetterSetter<").append(className).append(">(){\n");
            sb.append("  public Object getValue(").append(className).append(" o) { return o.").append(prop).append("; }\n");
            sb.append("  public void setValue(").append(className).append(" o, Object value) { o.").append(prop).append(" = ").append(makeExpressionForPropAssignment(prop)).append("; } });\n");
        }
        sb.append("}\n");

        sb.append("  public static Set<String> getPropNamesStatic() { return namedProps.keySet();\n }\n");
        sb.append("  public static Object getPropValueStatic(").append(className).append(" bean, String propName) {\n");
        sb.append("    INamedPropGetter<").append(className).append("> getter = namedProps.get(propName);\n");
        sb.append("    if (getter == null) throw new LameRuntimeException(\"no prop with name: \" + propName + \" in class: \" + ").append(className).append(".class.getName());\n");
        sb.append("    return getter.getValue(bean);\n  }\n");
        sb.append("  public static void setPropValueStatic(").append(className).append(" bean, String propName, Object value) {\n");
        sb.append("    INamedPropSetter<").append(className).append("> setter = namedProps.get(propName);\n");
        sb.append("    if (setter == null) throw new LameRuntimeException(\"no prop with name: \" + propName + \" in class: \" + ").append(className).append(".class.getName());\n");
        sb.append("    setter.setValue(bean, value);\n  }\n");
        sb.append("  public static Class getPropClassStatic(String propName) {\n");
        sb.append("    Class ccc = propClasses.get(propName);\n");
        sb.append("    if (ccc == null) throw new LameRuntimeException(\"no prop with name: \" + propName + \" in class: \" + ").append(className).append(".class.getName());\n");
        sb.append("    return ccc;\n  }\n");

        sb.append("  public Set<String> getPropNames() { return getPropNamesStatic(); }\n");
        sb.append("  public Object getPropValue(String propName) { return getPropValueStatic(bean, propName); }\n");
        sb.append("  public void setPropValue(String propName, Object value) { setPropValueStatic(bean, propName, value); }\n");
        sb.append("  public Class getPropClass(String propName) { return getPropClassStatic(propName); }\n");
        sb.append("  public ").append(className).append(" getBean() { return bean; }\n");
    }

    public static String makeMethods(IMainClass mainClass) {

        NamedPropsBeanGenericCodeGenerator generator = new NamedPropsBeanGenericCodeGenerator(mainClass);
        generator.makeNamedPropsBean();
        String code = generator.sb.toString();

        String simpleClassName = mainClass.getSimpleName();
        String wrapperClassName = simpleClassName + "Wrapper";

        return "public class " + wrapperClassName + " implements INamedTypedPropsBean<"
                + simpleClassName + "> {\n"
                + "private " + simpleClassName + " bean;\n"
                + "private " + wrapperClassName + "(" + simpleClassName + " bean) { this.bean = bean; }\n"
                + code
                + "}";
    }
//    public static void main(String[] args) throws ClassNotFoundException {
////        System.out.println("simple: " + int.class.getSimpleName());
////        System.out.println("canon: " + int.class.getCanonicalName());
////        System.out.println("name: " + int.class.getName());
//        
//        System.out.println("int.class.isAssignableFrom(Integer.class)? " + int.class.isAssignableFrom(Integer.class));
//        System.out.println("Integer.class.isAssignableFrom(int.class)? " + Integer.class.isAssignableFrom(int.class));
//        
//        System.out.println("findClass int: " + Class.forName("java.lang.int"));
//    }
}
