/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author efedchenko
 */
public class EditEventAttributeRow extends AbstractAddAnyNodeDialog {

    protected String eventName;
    protected IParametrizedContinuation<List<AttributeBean>> saveCont;

    public void buildAndShowDialog(Integer nodeKindId, Integer treeId, boolean isNodeFromDynamicTree, List<TreeNodeBean> tnb, TreeNodeBean node,
            List<AttributeBean> attr, String eventName, IParametrizedContinuation<List<AttributeBean>> saveCont) {
        this.nodeKindId = nodeKindId;
        this.treeId = treeId;
        this.isNodeFromDynamicTree = isNodeFromDynamicTree;
        this.saveCont = saveCont;
        this.nodeKinds = tnb;
        this.node = node;
        this.attrListForViewing = attr;
        this.eventName = eventName;
        if (!BIKClientSingletons.isSysAdminLoggedIn() && attrListForViewing != null) {
            isEnabledForEdition = false;
        }
        super.buildAndShowDialog();

        actionBtn.setText(I18n.zapisz.get());
        tbxName.setEnabled(false);
        if (!isEnabledForEdition) {
            actionBtn.setVisible(false);
        }
    }

    @Override
    protected String getDialogCaption() {
        return I18n.editEventRow.get();
    }

    @Override
    public Widget getCaption() {
        Label nameLbl = new Label(I18n.podajNazweZdarzenia.get());
        nameLbl.setStyleName("addNodeLbl");
        nameLbl.setWidth(getTopLabelWidth());
        return nameLbl;
    }

    @Override
    protected void fillAttributeFields() {
        if (!BaseUtils.isCollectionEmpty(attrListForViewing)) {
            atrContainsValues = true;
            tbxName.setValue(eventName);
        }
    }

    @Override
    protected void doAction() {
        saveCont.doIt(new ArrayList<AttributeBean>(absMap.values()));
    }

    //        ArrayList<AttributeBean> result = new ArrayList<AttributeBean>(absMap.values());
//        for (AttributeBean ab : attrListForViewing) {
//            for (AttributeBean abRes : result) {
//                if (abRes.atrLinValue == ab.atrLinValue) {
//                    result.remove(ab);
//                    break;
//                }
//            }
//        }
//        saveCont.doIt(result);
}
