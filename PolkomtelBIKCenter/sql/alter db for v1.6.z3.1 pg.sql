exec sp_check_version '1.6.z3.1';
go


if not exists(select * from sys.objects where object_id = object_id(N'bik_erwin_table') and type in (N'U'))
	create table [dbo].[bik_erwin_table](
		[long_id] [varchar](67) not null primary key,
		[name] [varchar](100) not null,
		[schema_name] [varchar](100),
		[physical_name] [varchar](100) not null,
		[table_type] [int] not null,
		[shapes_ref] [varchar](67) not null,
		[data_load_log_id] [int],
		[table_comment] [varchar](2048)
	)
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_erwin_table_column') and type in (N'U'))
	create table [dbo].[bik_erwin_table_column](
		[long_id] [varchar](67) not null primary key,
		[name] [varchar](100) not null,
		[physical_name] [varchar](100),
		[column_type] [int] not null,
		[table_long_id] [varchar](67) not null,
		[data_load_log_id] [int],
		[column_comment] [varchar](2048),
		[parent_col_ref_long_id] [varchar](67),
		[parent_relationship_ref_long_id] [varchar](67)  
	)
go


if not exists(select * from sys.objects where object_id = object_id(N'[bik_erwin_shape]') and type in (N'U'))
	create table [dbo].[bik_erwin_shape](
		[long_id] [varchar](67) not null primary key,
		[area_long_id] [varchar](67) not null,
		[model_object_ref] [varchar](67) not null,
		[anchor_point] [varchar](23) not null,
		[fixed_size_point] [varchar](23),	
		[data_load_log_id] [int]
	)
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_erwin_shape]') and name = N'idx_bik_erwin_shape_area_long_id')
	create nonclustered index [idx_bik_erwin_shape_area_long_id] on [dbo].[bik_erwin_shape]
	(
		area_long_id asc
	)

--bik_erwin_relation_shape
if not exists(select * from sys.objects where object_id = object_id(N'[bik_erwin_relation_shape]') and type in (N'U'))
	create table [dbo].[bik_erwin_relation_shape](
		[long_id] [varchar](67) not null primary key,
		[model_object_ref] [varchar](67) not null,
		[parent_connector_point] [varchar](23) not null,	
		[child_connector_point] [varchar](23) not null,	
		[child_shape_ref] [varchar](67) not null,
		[parent_shape_ref] [varchar](67) not null
	)
go

--
if not exists(select * from sys.objects where object_id = object_id(N'[bik_erwin_relationship]') and type in (N'U'))
	create table [dbo].[bik_erwin_relationship](
		[long_id] [varchar](67) not null primary key,
		[name] [varchar](100) not null,
		[relation_type] [int] not null,
		[key_group_long_id] [varchar](67),
		[parent_table_long_id] [varchar](67) not null,
		[child_table_long_id] [varchar](67) not null,
		[relationship_comment] [varchar](2048)
	)
go

--ErwinKeyGroup
if not exists(select * from sys.objects where object_id = object_id(N'[bik_erwin_key_group]') and type in (N'U'))
	create table [dbo].[bik_erwin_key_group](
		[long_id] [varchar](67) not null primary key,
		[name] [varchar](100),
		[key_group_type] [varchar](10) not null
	)
go

--ErwinKeyGroupMember
if not exists(select * from sys.objects where object_id = object_id(N'[bik_erwin_key_group_member]') and type in (N'U'))
	create table [dbo].[bik_erwin_key_group_member](
		[long_id] [varchar](67) not null primary key,
		[key_group_long_id] [varchar](67) not null,
		[table_column_long_id] [varchar](67) not null
	)
go

if not exists(select * from sys.objects where object_id = object_id(N'[bik_erwin_subject_area]') and type in (N'U'))
	create table [dbo].[bik_erwin_subject_area](
		[long_id] [varchar](67) not null primary key,
		[name] [varchar](100),
		[object_order] int not null
	)
go

if not exists(select * from sys.objects where object_id = object_id(N'[bik_erwin_subject_area_object_ref]') and type in (N'U'))
	create table [dbo].[bik_erwin_subject_area_object_ref](
		[subject_area_long_id] [varchar](67) not null,
		[object_long_id] [varchar](67) not null,
		primary key (subject_area_long_id, object_long_id)
	)
go

--scheduler
if not exists( select * from bik_data_source_def where description = 'Erwin Data Model' )  
  begin
	insert into bik_data_source_def(description)
	values ('Erwin Data Model')    
	
    declare @sourceId int;
    select @sourceId = id from bik_data_source_def where description = 'Erwin Data Model'

	insert into bik_schedule(source_id, hour, minute, interval, is_schedule, priority)
	values(@sourceId, 0, 0, 1, 0, 12)
  end

if not exists( select * from dbo.bik_admin where param = 'erwin.isActive' )
	insert into bik_admin(param,value) values ('erwin.isActive', '0')
  
if not exists( select * from dbo.bik_admin where param = 'erwin.path' )
	insert into bik_admin(param,value) values ('erwin.path', 'C:/Temp/ERWIN_MODEL.xml')

--lista dostepnych konektorow
/*
if not exists(select * from bik_app_prop where name = 'availableConnectors' and val like '%Erwin Data Model%')
	update bik_app_prop
		set val = val + ',Erwin Data Model'
		where name = 'availableConnectors'
*/

	
exec sp_update_version '1.6.z3.1', '1.6.z3.2';
go