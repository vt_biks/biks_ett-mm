/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import commonlib.LameUtils;
import commonlib.ThreadedJobExecutor;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import pl.bssg.metadatapump.PumpUtils;
import static pl.bssg.metadatapump.ThreadedPumpExecutor.MILLIS_FOR_MINUTE;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.common.dqm.DQMConstanst;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.fovis.common.dqm.DQMScheduleBean;
import pl.fovis.common.dqm.DataQualityRequestBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 * Klasa do zarządzania uruchamianiem testów - zarówno ręcznie (manual), jak i z
 * harmonogramu
 *
 * @author tflorczak
 */
public class DQMTestManager /*extends BaseExecutor*/ implements Runnable {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected ThreadedJobExecutor jobExecutor;
    protected IBIKSDQMSubservice service;
    protected String dirForDownload;
    protected Timer timer;

    public DQMTestManager(IBIKSDQMSubservice service, String dirForDownload) {
        this.jobExecutor = new ThreadedJobExecutor();
        this.service = service;
        this.dirForDownload = dirForDownload;

        this.timer = new Timer();
    }

    @Override
    public void run() {
//        initExecutor();
        jobExecutor.run();
    }

    public void addTestToJobExecutorQueue(final int testId, final Integer multiSourceId, final boolean isManualRunning) {
        jobExecutor.addJob(new Runnable() {
            @Override
            public void run() {
                executeTest(testId, multiSourceId, isManualRunning);
            }
        });
    }

    protected void executeTest(final int testId, final Integer multiSourceId, final boolean isManual) {
        service.execInAutoCommitMode(new IContinuation() {

            @Override
            public void doIt() {
                try {
                    DQMTestBean test = service.getDQMConfigForTest(testId);
                    boolean isMultiSourcesOK = true;
                    if (isManual) { // uruchomienie ręczne - uruchamiamy test ze wszystkimi testami podrzędnymi
                        List<DQMMultiSourceBean> multiSources = service.getDQMTestMultiSources(test.testNodeId);
//                    if (multiSources != null) {
                        for (DQMMultiSourceBean source : multiSources) {
                            executeOneSourceTest(source);
                        }
                        // check czy wszystkie testy podrzedne sie wykonaly w przypadku multizrodlowych testow
                        isMultiSourcesOK = multiSources.isEmpty() || checkMultiSourceIsSuccessForTest(testId);
//                    }
                        if (isMultiSourcesOK) {
                            executeMainTest(test, true, multiSources);
                        }
                    } else if (multiSourceId == null) { // uruchomienie z harmonogramu testu bez testow podrzednych
                        // check czy wszystkie testy podrzedne sie wykonaly w przypadku multizrodlowych testow
                        isMultiSourcesOK = test.dbServerId != null || checkMultiSourceIsSuccessForTest(testId);
                        if (isMultiSourcesOK) {
                            executeMainTest(test, false, null);
                        }
                        fixOneTimeSchedules(testId);
                    } else { // uruchomienie z harmonogramu testu podrzednego
                        DQMMultiSourceBean oneSource = service.getDQMTestMultiSource(multiSourceId);
                        executeOneSourceTest(oneSource);
                    }
                } catch (Exception e) {
                    // catch na najwyzszym poziomie
                    if (logger.isErrorEnabled()) {
                        logger.error("Error in DQMTestManager.executeTest", e);
                    }
                }
            }
        });
    }

    protected void fixOneTimeSchedules(int testId) {
//        System.out.println("fixOneTimeSchedules for test: " + testId);
        service.fixOneTimeSchedules(testId);
    }

    protected void fixOneTimeOneMultiSourceSchedule(int testId, Integer multiSourceId) {
        service.fixOneTimeOneMultiSourceSchedule(testId, multiSourceId);
    }

    protected boolean checkMultiSourceIsSuccessForTest(int testId) {
        return service.checkMultiSourceIsSuccessForTest(testId);
    }

    protected ITestExecutor getTestExecutorForSource(String sourceName) {
        ITestExecutor executor = null;
        if (sourceName != null) {
            sourceName = sourceName.trim();
        }
        if (sourceName == null) { // test na bazie BIKSa
            executor = new SQLServerTestExecutor(service);
        } else if (sourceName.equals(PumpConstants.SOURCE_NAME_MSSQL)) {
            executor = new SQLServerTestExecutor(service);
        } else if (sourceName.equals(PumpConstants.SOURCE_NAME_ORACLE)) {
            executor = new OracleTestExecutor(service);
        } else if (sourceName.equals(PumpConstants.SOURCE_NAME_POSTGRESQL)) {
            executor = new PostgreSQLTestExecutor(service);
        } else if (sourceName.equals(PumpConstants.SOURCE_NAME_TERADATA)) {
            executor = new TeradataTestExecutor(service);
        } else if (sourceName.equals(PumpConstants.SOURCE_NAME_PROFILE)) {
            executor = new ProfileTestExecutor(service, dirForDownload);
        }
        return executor;
    }

    protected void executeOneSourceTest(DQMMultiSourceBean oneSource) {
        ITestExecutor dataProvider = getTestExecutorForSource(oneSource.sourceName);
        try {
            long initConnStart;
            long initConnEnd;
            long logId = service.insertDQMLog(oneSource.testId, oneSource.id);
            service.setScheduleStatusFlag(oneSource.testId, oneSource.id);
            try {

                initConnStart = System.nanoTime();
                boolean initConnection = dataProvider.initConnection(oneSource);
                initConnEnd = System.nanoTime();
                if (!initConnection) {
                    service.updateDQMLog(logId, I18n.bladPolaczenia.get(), true);
                    return;
                }
            } catch (Exception e) {
                service.updateDQMLog(logId, I18n.errorBrakPlikowDoPobrania.get(), true);
                return;
            }
            service.updateDQMLog(logId, I18n.inicjowanieZakonczoneWczytywanieDanych.get(), false);
            try {
                int dataReadSecs = dataProvider.pumpDataToBiksDatabase();
                service.updateRequestDataReadSecs(logId, dataReadSecs
                        + (int) ((initConnEnd - initConnStart) / 1000 / 1000000));

            } catch (Exception e) {
                if (logger.isErrorEnabled()) {
                    logger.error("Error in save data in BIKS database (dataProvider.pumpDataToBiksDatabase())", e);
                }
                service.updateDQMLog(logId, I18n.bladWZapisieDanychWBD.get() + ": " + e.getMessage(), true);
                dataProvider.closeConnection();
                dataProvider.exit();
                return;
            }

            // zamykanie połączenia
            service.updateDQMLog(logId, I18n.zamykaniePolaczenia.get(), false);
            dataProvider.closeConnection();
            dataProvider.exit();
            service.updateDQMLog(logId, I18n.sukces.get(), true);
        } finally {
            fixOneTimeOneMultiSourceSchedule(oneSource.testId, oneSource.id);
        }
    }

    protected void executeMainTest(DQMTestBean test, boolean isManual, List<DQMMultiSourceBean> optMultiSources) {

        if (test.dbServerId == null) { // wieloźródłowy
            if (optMultiSources == null) // brak danych dla podtestów
            {
                optMultiSources = service.getDQMTestMultiSources(test.testNodeId);
            }
        } else { // jednoźródłowy - optMultiSources są dla niego zbędne (nie powinno ich być nawet)
            optMultiSources = null;
        }

        long preInitConnStart;
        long preInitConnEnd;
        long initConnStart;
        long initConnEnd;

        ITestExecutor executor = getTestExecutorForSource(test.sourceName);
        long logId = service.insertDQMLog(test.id, null);
        List<DQMTestBean> requests = null;
        try {
            preInitConnStart = System.nanoTime();
            requests = executor.preInitAndGetRequestToRun(test, optMultiSources);
            preInitConnEnd = System.nanoTime();
        } catch (Exception e) {
            service.updateDQMLog(logId, I18n.errorBrakPlikowDoPobrania.get(), true);
            return;
        }
        if (requests == null) {
            service.updateDQMLog(logId, I18n.bladPolaczenia.get(), true);
            return;
        }
        if (requests.isEmpty()) {
            service.updateDQMLog(logId, I18n.errorBrakNowegoPlikuDoPobrania.get(), true);
            return;
        }

        int totalDataReadSecs = (int) ((preInitConnEnd - preInitConnStart) / 1000 / 1000000);

        // wykonywanie testów. Domyślnie pętla przejdzie 1 raz: 1 uruchomienie = 1 wykonanie.
        // dla profile: może pobrać kilka plików o różnych datach wykonania
        for (DQMTestBean request : requests) {
            // łączenie sie z bazą
            service.updateDQMLog(logId, I18n.laczenieZBazaDanych.get(), false);
            initConnStart = System.nanoTime();
            boolean isInitSuccess = executor.initConnection(request);//testFunctionConf, useFunctionInsteadSQLQuery ? test.functionDatabase : test.queryDatabase);
            initConnEnd = System.nanoTime();

            totalDataReadSecs += (int) ((initConnEnd - initConnStart) / 1000 / 1000000);

            if (!isInitSuccess) {
                service.updateDQMLog(logId, I18n.bladPolaczenia.get(), true);
                continue;
            }
            // wykonywanie testu
            service.updateDQMLog(logId, I18n.wykonywanieTestu.get(), false);
            DataQualityRequestBean bean = new DataQualityRequestBean(test.id, request.testNodeId, isManual ? 1 : 0, request.optStartedDate);
            long requestId = service.insertDQMRequest(bean);
            bean.id = requestId;
            List<Map<String, Object>> results = null;
            Long caseCount = null;
            Long errorCount = null;

            long runTestStart;
            long runTestEnd;

            try {
//                runTestStart = System.currentTimeMillis();
                runTestStart = System.nanoTime();
                results = executor.runTest();
                runTestEnd = System.nanoTime();
            } catch (Exception e) {
                if (logger.isErrorEnabled()) {
                    logger.error("Error in test running", e);
                }
                service.updateDQMLog(logId, I18n.bladWUruchomieniuTestu.get() + ": " + e.getMessage(), true);
                closeConnection(executor, requestId);
                continue;
            }
            // odczytanie wyników
            try {
                Map<String, Object> row = results.get(0);
                caseCount = getLongValue(row.get("case_count"));
                errorCount = getLongValue(row.get("error_count"));
                if (results.size() != 1 || results.get(0).size() != 2 || caseCount == null || errorCount == null) {
                    throw new Exception();
                }
            } catch (Exception e) {
                if (logger.isErrorEnabled()) {
                    logger.error("The test result inconsistent with the expected format", e);
                }
                service.updateDQMLog(logId, I18n.nieprawidlowyWynikFunkcji.get(), true);
                closeConnection(executor, requestId);
                continue;
            }
            // zapisywanie wyniku
            bean.caseCount = caseCount;//getLongValue(row.get("case_count"));
            bean.errorCount = errorCount;//getLongValue(row.get("error_count"));
            if (bean.caseCount == 0) {
                bean.errorPercentage = 0.0;
                bean.passed = 1;
            } else {
                bean.errorPercentage = bean.errorCount.doubleValue() / bean.caseCount.doubleValue();
                bean.passed = bean.errorPercentage > request.errorThreshold ? 0 : 1;
            }

            long errorDataStart = 0;
            long errorDataEnd = 0;

            // gdy są błędy, uruchamiamy funkcję do pobrania ich
            if (bean.errorCount > 0) {
                service.updateDQMLog(logId, I18n.wykonywanieFunkcjiZBledami.get(), false);
                List<Map<String, Object>> errorResults = null;
                int errorCnt = 0;
                String tmpTableName = DQMConstanst.DQM_TEMP_TABLE_PREFIX + BaseUtils.generateRandomToken(20);
                try {
//                    errorDataStart = System.currentTimeMillis();
                    errorDataStart = System.nanoTime();
//                    errorResults = executor.getErrorData();
                    executor.pumpErrorDataToBiksDatabase(tmpTableName);
                    errorCnt = service.countRecordNumber(tmpTableName);
                    errorDataEnd = System.nanoTime();
//                    if (errorResults == null || errorResults.isEmpty()) {
//                        service.updateDQMLog(logId, I18n.niePobranoBlednychDanych.get(), true);
//                        errorResults = null;
//                    }
                    if (errorCnt == 0) {
                        service.updateDQMLog(logId, I18n.niePobranoBlednychDanych.get(), true);
                    }
                } catch (Exception e) {
                    if (logger.isErrorEnabled()) {
                        logger.error("Error in getting error data SQL", e);
                    }
                    service.updateDQMLog(logId, I18n.bladWSQLPobierajacymDane.get() + ": " + e.getMessage(), true);
                }
//                if (errorResults == null) {
//                    closeConnection(executor, requestId);
//                    continue;
//                }
                if (errorCnt == 0) {
                    closeConnection(executor, requestId);
                    continue;
                }
                // zapisywanie błędów do DB
                service.updateDQMLog(logId, I18n.zapisywanieBledowDoBazyDanych.get(), false);
                service.updateDQMLog(logId, I18n.liczbaBledow.get() + ": " + errorCnt + "." + I18n.trwaZapisywaniaDoDysku.get() + "...", false);
                Pair<String, String> files = service.saveErrorDataOnDisk(tmpTableName);
                service.dropTmpTable(tmpTableName);
                bean.xlsxFileName = files.v1;
                bean.csvFileName = files.v2;
//                ContinuationBeanCollector<DQMErrorBean> beanCollector = new ContinuationBeanCollector<DQMErrorBean>(500, new IParametrizedContinuation<List<DQMErrorBean>>() {
//
//                    @Override
//                    public void doIt(List<DQMErrorBean> param) {
//                        service.insertErrorResults(param);
//                    }
//                });
//                int rowNumber = 0;
//                for (Map<String, Object> errorResult : errorResults) {
//                    int columnNumber = 0;
//                    for (Map.Entry<String, Object> entrySet : errorResult.entrySet()) {
//                        Object value = entrySet.getValue();
//                        beanCollector.add(new DQMErrorBean(requestId, rowNumber, columnNumber, entrySet.getKey(), value == null ? null : value.toString()));
//                        columnNumber++;
//                    }
//                    rowNumber++;
//                }
//                beanCollector.flush();
            }

            totalDataReadSecs += ((runTestEnd - runTestStart) + (errorDataEnd - errorDataStart)) / 1000 / 1000000;

            service.updateRequestDataReadSecs(logId, totalDataReadSecs);

            service.updateDQMRequest(bean);
            service.updateDQMLog(logId, I18n.usuwanieStarychBlednychDanych.get(), false);
            service.deleteOldErrorData(test.id);
            service.updateDQMLog(logId, I18n.usuwanieTabelTymczasowych.get(), false);
            service.dropTempTables(test.id);
            // zamykanie połączenia
            service.updateDQMLog(logId, I18n.zamykaniePolaczenia.get(), false);
            closeConnection(executor, null);
            service.updateDQMLog(logId, I18n.sukces.get(), true);

            //ww: żeby nie doliczyć czasu następnemu w serii
            // choć nie! to jest to samo logId, więc doliczamy!
//            totalDataReadSecs = 0;
        }
        executor.exit();
//            System.out.println("KONIEC");
    }

    protected Long getLongValue(Object val) {
        Long valToReturn;
        try {
            if (val instanceof Integer) {
                valToReturn = ((Integer) val).longValue();
            } else if (val instanceof String) {
                valToReturn = Long.valueOf((String) val);
            } else if (val instanceof BigDecimal) {
                valToReturn = ((BigDecimal) val).longValue();
            } else {
                valToReturn = (Long) val;
            }
        } catch (ClassCastException e) {
            valToReturn = null;
        }
        return valToReturn;
    }

    public synchronized void setScheduler(List<DQMScheduleBean> beans) {
        timer.cancel();
        timer.purge();
        timer = new Timer();
        for (final DQMScheduleBean scheduleBean : beans) {
            if (scheduleBean.isSchedule) {
                Date dateSchedule = PumpUtils.getStartedDateForSchedule(scheduleBean);
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        addTestToJobExecutorQueue(scheduleBean.testId, scheduleBean.multiSourceId, false);
                    }
                };
                if (scheduleBean.interval == 0 && dateSchedule != null) {
                    timer.schedule(task, dateSchedule);
                } else if (scheduleBean.interval > 0) {
                    timer.scheduleAtFixedRate(task, dateSchedule, MILLIS_FOR_MINUTE * scheduleBean.interval);
                }
            }
        }
    }

    protected void closeConnection(ITestExecutor executor, Long optRequestIdToDelete) {

        if (logger.isDebugEnabled()) {
            logger.debug("closeConnection starts");
        }

        executor.closeConnection();
        if (optRequestIdToDelete != null) {
            service.deleteDQMRequest(optRequestIdToDelete);
        }
    }
}
