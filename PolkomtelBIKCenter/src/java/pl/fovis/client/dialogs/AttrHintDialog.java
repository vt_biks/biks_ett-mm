/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author beata
 */
public class AttrHintDialog extends BaseActionOrCancelDialog {

    protected int id;
    protected String name;
    protected String hint;

    @Override
    protected String getDialogCaption() {
        return I18n.wyjasnienie.get() /* I18N:  */;
    }

    @Override
    protected String getCancelButtonCaption() {
        return I18n.zamknij.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return null;
    }

    @Override
    protected void buildMainWidgets() {

//        HTML titleLabel = new HTML("<b>Tytuł:</b>");
//        titleLabel.setHeight("100%");
//        titleLabel.setWidth(600 + "px");

        Label label = new Label(name);
        label.setStyleName("BizDef-titleLblsub");

//        HTML contentLabel = new HTML("<br/><b>Treść:</b>");
//        contentLabel.setHeight("100%");
//        contentLabel.setWidth("100%");

        HorizontalPanel caption = new HorizontalPanel();
        caption.add(label);
        caption.setStyleName("BizDef-mainLbl");
        FlowPanel vp = new FlowPanel();
        vp.setStyleName("BizDef");
        vp.add(new HTML(hint));

        ScrollPanel scPanel = new ScrollPanel(vp);
//        scPanel.setHeight(Window.getClientHeight() * 0.92 - 63 - 50 + "px");
//        scPanel.setWidth(Window.getClientWidth() * 0.97+ "px");

        setScrollPanelSize(scPanel, 113);
//        scPanel.setSize(Window.getClientWidth() + "px", Window.getClientHeight() + "px");
//        main.setSize(Window.getClientWidth() * 0.96 + "px", Window.getClientHeight() * 0.92 + "px");
        main.add(caption);
        main.add(scPanel);
    }

    @Override
    protected void doAction() {
        //no-op
    }

    public void buildAndShowDialog(String name, String hint) {
//        this.id = id;
        this.name = name;
        this.hint = hint;
        super.buildAndShowDialog();
    }
}
