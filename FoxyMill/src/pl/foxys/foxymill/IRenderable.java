/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill;

import pl.foxys.foxymill.renderctx.IRenderCtx;

/**
 *
 * @author wezyr
 */
public interface IRenderable {
    public void render(//IRenderContext renderCtx
            IRenderCtx renderCtx);
}
