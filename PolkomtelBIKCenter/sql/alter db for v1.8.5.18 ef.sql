exec sp_check_version '1.8.5.18';
GO

IF NOT EXISTS (SELECT 1 FROM bik_app_prop WHERE name = 'hideAttrAndNodeKindBtnsOnRight')
INSERT INTO bik_app_prop (
	name
	,val
	,is_editable
	)
VALUES (
	'hideAttrAndNodeKindBtnsOnRight'
	,'false'
	,1
	)

exec sp_update_version '1.8.5.18'
	,'1.8.5.19';
GO