/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.tags;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.SimpleHtmlSanitizer;

/**
 *
 * @author wezyr
 */
public class TagsUtils {

    public static Pair<List<TagData>, String> parseHtmlFileEx(String fileName, String html) {
        StringStreamParser parser = new StringStreamParser(html, fileName);
        String exclamations = parser.getExclamations();
        List<TagData> tags = new ArrayList<TagData>();
        while (parser.hasNext()) {
            tags.add(parser.next());
        }
        return new Pair<List<TagData>, String>(tags, exclamations);
    }

    public static List<TagData> parseHtmlFile(String fileName, String html) {
        return parseHtmlFileEx(fileName, html).v1;
//        StringStreamParser parser = new StringStreamParser(html, fileName);
//        List<TagData> tags = new ArrayList<TagData>();
//        while (parser.hasNext()) {
//            tags.add(parser.next());
//        }
//        return tags;
    }

    public static boolean validateHtml(String html, Set<String> allowedTags) {
        ValidatingTagLibrary vtl = new ValidatingTagLibrary(allowedTags);
        try {
            TagForestBuilder<Object> builder = new TagForestBuilder<Object>(vtl);
            List<TagData> tagsData = parseHtmlFile("???", html);
            builder.buildFoxyTagForest(tagsData.iterator());
        } catch (FoxyTagsRuntimeException ex) {
            return false;
        }
        return true;
    }

    public static final Set<String> standardRichTextTags = BaseUtils.splitUniqueBySep(
            "b,i,strong,li,ul,ol,img,a,div,span,br,hr,p,em,blockquote,sub,sup", ",", true);
    private static final ValidatingTagLibrary standardRTVTLib = new ValidatingTagLibrary(standardRichTextTags);
    private static final TagForestBuilder<Object> standardRTVTBuilder = new TagForestBuilder<Object>(standardRTVTLib);
    private static final ValidatingTagLibrary xmlVTLib = new ValidatingTagLibrary(null);
    private static final TagForestBuilder<Object> xmlVTBuilder = new TagForestBuilder<Object>(xmlVTLib);

    public static boolean validateAsStandardRichTextHtml(String html) {
        //ValidatingTagLibrary vtl = new ValidatingTagLibrary(standardRichTextTags);
        try {
            List<TagData> tagsData = parseHtmlFile("???", html);
            standardRTVTBuilder.buildFoxyTagForest(tagsData.iterator());
        } catch (FoxyTagsRuntimeException ex) {
            return false;
        }
        return true;
    }

    public static boolean validateAsProperXml(String html) {
        try {
            List<TagData> tagsData = parseHtmlFile("???", html);
            xmlVTBuilder.buildFoxyTagForest(tagsData.iterator());
        } catch (FoxyTagsRuntimeException ex) {
            return false;
        }
        return true;
    }

    public static String sanitizeHtml(String html) {
        return SimpleHtmlSanitizer.sanitize(html);
    }
}
