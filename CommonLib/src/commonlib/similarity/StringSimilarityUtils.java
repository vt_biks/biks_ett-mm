/*
 * StringSimilarityUtils.java
 *
 * Created on 4 wrzesie� 2007, 18:18
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib.similarity;

/**
 *
 * @author wezyr
 */
public class StringSimilarityUtils {
    
    /** Creates a new instance of StringSimilarityUtils */
    public StringSimilarityUtils() {
    }
    
    public static int calcDamerauLevenshteinDistance(String s1, String s2) {
        /*
           int DamerauLevenshteinDistance(char str1[1..lenStr1], char str2[1..lenStr2])
           // d is a table with lenStr1+1 rows and lenStr2+1 columns
           declare int d[0..lenStr1, 0..lenStr2]
           // i and j are used to iterate over str1 and str2
           declare int i, j, cost
         
           for i from 0 to lenStr1
               d[i, 0] := i
           for j from 1 to lenStr2
               d[0, j] := j
         
           for i from 1 to lenStr1
               for j from 1 to lenStr2
                   if str1[i] = str2[j] then cost := 0
                                        else cost := 1
                   d[i, j] := minimum(
                                        d[i-1, j  ] + 1,     // deletion
                                        d[i  , j-1] + 1,     // insertion
                                        d[i-1, j-1] + cost   // substitution
                                    )
                   if(i > 1 and j > 1 and str1[i] = str2[j-1] and str1[i-1] = str2[j]) then
                       d[i, j] := minimum(
                                        d[i, j],
                                        d[i-2, j-2] + cost   // transposition
                                     )
         
         
           return d[lenStr1, lenStr2]
         */
        int[][] d = new int[s1.length() + 1][];
        for (int i = 0; i <= s1.length(); i++) {
            d[i] = new int[s2.length() + 1];
            d[i][0] = i;
        }
        
        for (int j = 0; j <= s2.length(); j++) {
            d[0][j] = j;
        }
        
        int cost;
        int pc1, pc2, pc3, pcn;
        
        for (int i = 1; i <= s1.length(); i++) {
            for (int j = 1; j <= s2.length(); j++) {
                cost = s1.charAt(i-1) == s2.charAt(j-1) ? 0 : 1;
                pc1 = d[i-1][j] + 1;
                pc2 = d[i][j-1] + 1;
                pc3 = d[i-1][j-1] + cost;
                if (pc1 <= pc2 && pc1 <= pc3)
                    d[i][j] = pc1;
                else if (pc2 <= pc1 && pc2 <= pc3)
                    d[i][j] = pc2;
                else
                    d[i][j] = pc3;
                
                if(i > 1 && j > 1 && s1.charAt(i-1) == s2.charAt(j-2) && s1.charAt(i-2) == s2.charAt(j-1))
                    d[i][j] = Math.min(
                            d[i][j],
                            d[i-2][j-2] + cost   // transposition
                            );
            }
        }

        int res = d[s1.length()][s2.length()];
        
        //System.out.println("resticted edit length for s1=" + s1 + ", s2=" + s2 + ": " + res);
        
        return res;
    }
}
