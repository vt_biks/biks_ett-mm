/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

/**
 *
 * @author wezyr
 */
public interface IRegExpMatcherFactory {

    public IRegExpPattern compile(String pattern, boolean ignoreCase, boolean multiline);
    //public IRegExpMatcher createMatcher(String pattern, String str);
}
