﻿exec sp_check_version '1.2.2.1';
go


if  exists (select * from sys.indexes where object_id = object_id('[dbo].[bik_user_right]') and name = 'uk_bik_user_right_user_right')
alter table [dbo].[bik_user_right] drop constraint [uk_bik_user_right_user_right]
go

alter table [dbo].[bik_user_right] add  constraint [uk_bik_user_right_user_right] unique nonclustered 
(
	[user_id] asc,
	[right_id] asc,
	[tree_id] asc
)with (pad_index  = off, statistics_norecompute  = off, sort_in_tempdb = off, ignore_dup_key = off, online = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
go

exec sp_update_version '1.2.2.1', '1.2.2.2';
go
