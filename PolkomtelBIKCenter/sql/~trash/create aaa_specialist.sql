USE [boxi]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[aaa_specialist](
	[proj_id] [int] NULL,
	[spon_id] [int] NULL,
	[expe_id] [int] NULL,
	[si_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[si_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[aaa_specialist]  WITH CHECK ADD FOREIGN KEY([proj_id])
REFERENCES [dbo].[aaa_user] ([id])
GO

ALTER TABLE [dbo].[aaa_specialist]  WITH CHECK ADD FOREIGN KEY([spon_id])
REFERENCES [dbo].[aaa_user] ([id])
GO

ALTER TABLE [dbo].[aaa_specialist]  WITH CHECK ADD FOREIGN KEY([expe_id])
REFERENCES [dbo].[aaa_user] ([id])
GO