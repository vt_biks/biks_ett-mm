/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence.wuwu;

import java.util.ArrayList;
import java.util.List;
import simplelib.deferredstringconverter.DeferredStringConverter;
import simplelib.deferredstringconverter.IDeferredRegExprReplacer;

/**
 *
 * @author pmielanczuk
 */
public class ConfluenceContentFixer {

    // 1. urle do obiektów z Confluence sprawdzić czy są w BIKS i ew. poprawić
    //    na urle BIKSowe
    // 2. inne urle względne do zasobów cyfropedii zamieniać na bezwzględne
    //    do Cyfropedii
    // 3. specjalne pseudolinki do obiektów hurtowni zamieniać na linki do
    //    obiektów BIKS - o ile taki obiekt już w BIKS jest wczytany
    //---
    // A. trzeba jeszcze sprawdzać już wrzucone do BIKSa obiekty Confluence
    //    czy w nich nie ma odwołań do nowych i jeżeli są - poprawić linki
    //---
    // Z. Albo inaczej - to się będzie wywoływać tuż przed wyświetleniem na
    //    ekranie użytkownika, więc wtedy już wszystko wiadomo (co jest w BIKS,
    //    a czego nie ma)
    //---
//    public static final String REL_HREF_QUOTE_REGEXPR = "href[ \\t\\n\\r]*=[ \\t\\n\\r]*[\"][ \\t\\n\\r]*/([^\"]*)[\"]";
//    protected static final IRegExpPattern REL_HREF_PATT_Q = compileRegExpr(REL_HREF_QUOTE_REGEXPR);
//    protected static final IRegExpPattern REL_HREF_PATT_A = compileRegExpr(REL_HREF_QUOTE_REGEXPR.replace("\"", "'"));
    //---
    protected String absoluteUrlBase;
    protected IConfluenceObjectImportedChecker importedChecker;
    protected String biksBaseUrl;

    public ConfluenceContentFixer(String biksBaseUrl, String absoluteUrlBase,
            IConfluenceObjectImportedChecker importedChecker) {
        this.biksBaseUrl = biksBaseUrl;
        this.absoluteUrlBase = absoluteUrlBase;
        this.importedChecker = importedChecker;
    }

    public String fixContent(String confluenceObjHtml, Integer optSelectedNodeId) {
        List<IDeferredRegExprReplacer<?, ?>> replacers = new ArrayList<IDeferredRegExprReplacer<?, ?>>();
        replacers.add(new DeferredConfluenceObjAnchorReplacer(biksBaseUrl, absoluteUrlBase, importedChecker, optSelectedNodeId));
        replacers.add(new DeferredConfluenceHrefReplacer(absoluteUrlBase));
//        replacers.add(new DeferredConfluenceDwhLinkReplacer(importedChecker));
        return DeferredStringConverter.convert(replacers, confluenceObjHtml == null ? "" : confluenceObjHtml);
    }
//    public static void main(String[] args) {
//        LameUtils.initFactories();
//        ConfluenceContentFixer ccf = new ConfluenceContentFixer("http://aqq.bssg.pl/", new IConfluenceObjectImportedChecker() {
//            @Override
//            public Map<Long, String> checkConfluenceImportedObjs(Collection<Long> objIds) {
//                Map<Long, String> m = BaseUtils.makeHashMapOfSize(1);
//                m.put(10L, "http://biksunio.bssg.pl/id_10.html");
//                return m;
//            }
//
//            @Override
//            public Map<String, String> checkDwhImportedObjs(Collection<String> dwhObjNames) {
//                Map<String, String> res = new HashMap<String, String>();
//                for (String name : dwhObjNames) {
//                    if (BaseUtils.safeEquals(name, "aqq")) {
//                        res.put("aqq", "<a href=\"http://biksunio.bssg.pl/node_id_" + name + ".html\">" + name + "</a>");
//                    }
//                }
//                return res;
//            }
//        });
//
//        System.out.println(ccf.fixContent("<a href=\"/a<a href='/qq'>piesla\">kot</a>"));
//        System.out.println(ccf.fixContent("<a href='/ala'>kot</a>"));
//        System.out.println(ccf.fixContent("<a href='/ala'>kot</a><a href='/hela'>pies</a>"));
//        System.out.println(ccf.fixContent("<a href='/ala'>kot</a>  <a href='/hela'>pies</a>"));
//        System.out.println(ccf.fixContent("<a href='/page_10'>kot</a>"));
//        System.out.println(ccf.fixContent("<a href='/page_111'>kot</a>"));
//        System.out.println(ccf.fixContent("ala [{[kot]}] aqq"));
//        System.out.println(ccf.fixContent("ala [{[aqq]}] aqq"));
//        System.out.println(ccf.fixContent("ala [{[aqq]}] <a href=\"/aqq.html\">bbb</a>"));
//        System.out.println(ccf.fixContent("ala [{[kot]}][{[aqq]}][{[kot]}] [{[aqq]}] aqq[{[kot]}][{[aqq]}]"));
//        System.out.println(ccf.fixContent("<img src='/ala'/>kot  <a href='/hela'>pies</a>"));
//    }
}
