/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class PreMapForMap<K, V> implements IPreMap<K, V> {
protected Map<K, V> map;
    
    public PreMapForMap(Map<K, V>  map) {
        this.map= map;
    }

    public Collection<K> keySet() {
        return map.keySet();
    }

    public V get(K key) {
        return map.get(key);
    }
}
