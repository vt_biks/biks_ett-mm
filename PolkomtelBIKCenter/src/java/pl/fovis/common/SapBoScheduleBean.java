/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class SapBoScheduleBean implements Serializable {

    public String destination;
    public String owner;
    public Date creationTime;
    public Date nextruntime;
    public Date expire;
    public int type;
    public Integer scheduleType;
    public int intervalMinutes;
    public int intervalHours;
    public int intervalDays;
    public int intervalMonths;
    public int intervalNthDay;
    public String format;
    public String parameters;
    public String destinationEmailTo;

}
