/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;

/**
 *
 * @author wezyr
 */
public class SearchFullResult implements Serializable {

    public static enum IndexCrawlStatus {

        NoIndex, NoChangeTracking, CrawlInProgress, CrawlComplete
    }
    //---
    public String searchedText;
    public Collection<ISearchByAttrValueCriteria> sbavcs;
    public Collection<Integer> searchedTreeIds;
    public Collection<Integer> searchedKindIds;
    public Collection<Integer> excludedKindIds;
    //---
    public List<SearchResult> foundItems;
    public Map<Integer, Integer> kindIdsAndCounts;
    public boolean calcKindCounts;
    public Map<Integer, String> ancestorNames;
    //---
    public IndexCrawlStatus indexCrawlStatus;
}
