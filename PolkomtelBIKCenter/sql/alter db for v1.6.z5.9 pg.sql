exec sp_check_version '1.6.z5.9';
go

--skrypt moze byv wykonywany wiele razy, nie musi byc dodatwkoch warunkow

declare @tree_id int = dbo.fn_tree_id_by_code('DictionaryDWH')
declare @node_kind_id int = dbo.fn_node_kind_id_by_code('DWHCategory')

update bik_node set visual_order = -1 where tree_id = @tree_id and node_kind_id = @node_kind_id and is_deleted = 0
	and obj_id like '%:0'
	
exec sp_update_version '1.6.z5.9', '1.6.z5.10';
go
