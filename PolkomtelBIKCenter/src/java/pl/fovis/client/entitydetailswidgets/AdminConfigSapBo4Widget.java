/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import pl.bssg.metadatapump.common.PumpConstants;

/**
 *
 * @author tflorczak
 */
public class AdminConfigSapBo4Widget extends AdminConfigSapBoWidget {

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_SAPBO4;
    }

    @Override
    protected boolean isVisibleBo40CheckBox() {
        return true;
    }
}
