/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BlogBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author beata
 */
public class UserBlogBeanExtractor extends BlogBeanExtractor {

    protected Map<Integer, String> idsAndNamesFromRoleBranches;

    public UserBlogBeanExtractor(boolean isAddJoinedButtonEnabled, int currentNodeId, Map<Integer, String> idsAndNamesFromBranches) {
        super(isAddJoinedButtonEnabled, currentNodeId);
        this.idsAndNamesFromRoleBranches = idsAndNamesFromBranches;
    }

    @Override
    public Integer getNodeId(BlogBean b) {
        return b.nodeIdEntry;
    }

    @Override
    public int getAddColCnt() {
        return BIKClientSingletons.isDescriptionInJoinedPanelVisible() ? 2 : 1;
    }

    @Override
    public Object getAddColVal(BlogBean b, int addColIdx) {
        //return b.subject != null ? b.blogSubject+"_tyt" : "";
        if (BIKClientSingletons.isDescriptionInJoinedPanelVisible() && addColIdx == 0) {
            return getDescription(b.descrPlain);
        }

        List<Integer> nodeIdsFromRoleBranch = BIKCenterUtils.splitBranchIdsEx(b.roleBranchIds, false);
        List<String> nodeNames = new ArrayList<String>();
//        nodeIdsFromRoleBranch.remove(nodeIdsFromRoleBranch.size() - 1);
        for (Integer id : nodeIdsFromRoleBranch) {
            if (idsAndNamesFromRoleBranches.get(id) != null) {
                nodeNames.add(idsAndNamesFromRoleBranches.get(id));
            }
        }
        String pathStr = !BaseUtils.isCollectionEmpty(nodeNames) ? BaseUtils.mergeWithSepEx(nodeNames, BIKGWTConstants.RAQUO_STR_SPACED) : "";
        return pathStr + (b.isAuxiliary ? " (" + I18n.pomocniczy.get() + ")" : "") // " (" + (!b.isAuxiliary ? I18n.glowny.get() /* I18N:  */ : I18n.pomocniczy.get() /* I18N:  */) + ")"
                + (b.inheritToDescendants ? " *" : "");
    }

//    @Override
//    public int getActionCnt() {
//        return 2;
//    }
    @Override
    protected List<IGridBeanAction<BlogBean>> createActionList() {
        List<IGridBeanAction<BlogBean>> res = new ArrayList<IGridBeanAction<BlogBean>>();
        res.add(actView);
        res.add(actFollow);
        return res;
    }

    @Override
    public int howManyTimesNameColumnWiderThanOther() {
        return 3;
    }
}
