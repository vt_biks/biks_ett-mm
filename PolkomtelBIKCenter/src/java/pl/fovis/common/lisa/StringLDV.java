/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;
import simplelib.BaseUtils;

/**
 *
 * @author lbiegniewski
 */
public class StringLDV implements LisaDictionaryValue, Serializable, IsSerializable {

    String value;

    public StringLDV() {
    }

    public StringLDV(String value) {
        this.value = value;
    }

    public String getValue() {
        return (value != null) ? value.trim() : value;
    }

    public void setValue(Object o) {
        this.value = BaseUtils.safeToString(o);
    }

    @Override
    public String toString() {
        return value;
    }
}
