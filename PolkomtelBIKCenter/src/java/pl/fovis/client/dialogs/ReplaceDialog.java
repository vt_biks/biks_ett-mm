/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class ReplaceDialog extends BaseActionOrCancelDialog {

    private IParametrizedContinuation<Pair<String, String>> saveCont;
    protected TextBox findValue;
    protected TextBox replaceValue;
    protected String name;

    @Override
    protected String getDialogCaption() {
        return name;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zamien.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        findValue = new TextBox();
        findValue.addStyleName("sendUrlInput");
        replaceValue = new TextBox();
        replaceValue.addStyleName("sendUrlInput");
        main.add(new Label(I18n.znajdz.get()));
        main.add(findValue);
        main.add(new Label(I18n.zamienNa.get()));
        main.add(replaceValue);
    }

    @Override
    protected void doAction() {
        if (!findValue.getValue().equals("")) {
            saveCont.doIt(new Pair<String, String>(findValue.getValue(), replaceValue.getValue()));
        }
    }

    public void buildAndShowDialog(String name, IParametrizedContinuation<Pair<String, String>> saveCont) {
        this.name = name;
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }
}
