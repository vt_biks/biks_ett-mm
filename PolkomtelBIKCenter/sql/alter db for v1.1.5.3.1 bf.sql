﻿exec sp_check_version '1.1.5.3.1';
go
------------------------------------
------------------------------------

-- poprzednia wersja w pliku: "alter db for 1.1.4.2 bf"
delete from bik_node where tree_id = dbo.fn_tree_id_by_code('TreeOfTrees')
go

exec sp_add_menu_node null, 'Mój BIKS', '#MyBIKS'
exec sp_add_menu_node null, 'Biblioteka', 'metadata'
exec sp_add_menu_node 'metadata', 'SAP BO', 'sapbo'
exec sp_add_menu_node 'sapbo', '@', '$Reports'
exec sp_add_menu_node 'sapbo', '@', '$ObjectUniverses'
exec sp_add_menu_node 'sapbo', '@', '$Connections'
exec sp_add_menu_node 'metadata', '@', '$Teradata'
exec sp_add_menu_node 'metadata', '@', '$DQC'
exec sp_add_menu_node null, '@', '$Glossary'
--exec sp_add_menu_node null, 'Kategoryzacje', '@Taxonomy'
--exec sp_add_menu_node null, 'Dokumenty', '$Documents'
--exec sp_add_menu_node null, 'Blogi', '$Blogs'
exec sp_add_menu_node null, 'Baza Wiedzy', 'knowledge'
exec sp_add_menu_node null, 'Społeczność BI', 'users'
exec sp_add_menu_node null, 'Admin', '#Admin'
exec sp_add_menu_node null, 'Szukaj', '#Search'


exec sp_add_menu_node '$DQC', '@', '&DQCGroup'
exec sp_add_menu_node '&DQCGroup', '@', '&DQCTestSuccess'
exec sp_add_menu_node '&DQCGroup', '@', '&DQCTestFailed'
exec sp_add_menu_node '&DQCGroup', '@', '&DQCTestInactive'

exec sp_add_menu_node '$Teradata', '@', '&TeradataOwner'
exec sp_add_menu_node '&TeradataOwner', '@', '&TeradataSchema'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataView'
exec sp_add_menu_node '&TeradataView', '@', '&TeradataColumn'

exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataTable'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumn'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumnPK'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumnIDX'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataProcedure'
exec sp_add_menu_node '$Reports', '@', '&ReportFolder'
exec sp_add_menu_node '&ReportFolder', '@', '&Webi'
exec sp_add_menu_node '&ReportFolder', '@', '&FullClient'
exec sp_add_menu_node '&Webi', '@', '&ReportQuery'
exec sp_add_menu_node '&ReportQuery', '@', '&Dimension$Reports'
exec sp_add_menu_node '&ReportQuery', '@', '&Measure'
exec sp_add_menu_node '&ReportQuery', '@', '&Detail'
exec sp_add_menu_node '&ReportFolder', '@', '&Flash'
exec sp_add_menu_node '&ReportFolder', '@', '&Excel'
exec sp_add_menu_node '&ReportFolder', '@', '&Hyperlink'
exec sp_add_menu_node '&ReportFolder', '@', '&Powerpoint'
exec sp_add_menu_node '&ReportFolder', '@', '&Pdf'
exec sp_add_menu_node '&ReportFolder', '@', '&CrystalReport'
exec sp_add_menu_node '$ObjectUniverses', '@', '&UniversesFolder'
exec sp_add_menu_node '&UniversesFolder', '@', '&Universe'

exec sp_add_menu_node '&Universe', '@', '&UniverseDerivedTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseAliasTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseTable'

exec sp_add_menu_node '&Universe', '@', '&UniverseClass'
exec sp_add_menu_node '&UniverseClass', '@', '&Dimension'
exec sp_add_menu_node '&UniverseClass', '@', '&Measure'
exec sp_add_menu_node '&Dimension', '@', '&Detail'
exec sp_add_menu_node '&UniverseClass', '@', '&Filter'

exec sp_add_menu_node '$Connections', '@', '&ConnectionNetworkFolder'
exec sp_add_menu_node '&ConnectionNetworkFolder', '@', '&ConnectionEngineFolder'
exec sp_add_menu_node '&ConnectionEngineFolder', '@', '&DataConnection'

exec sp_add_menu_node 'knowledge', 'Artykuły', '$Articles'
exec sp_add_menu_node 'knowledge', 'Dokumenty', '$Documents'
exec sp_add_menu_node 'knowledge', 'Kategoryzacje', '@Taxonomy'

exec sp_add_menu_node '@Taxonomy', '@', '&TaxonomyEntity'

exec sp_add_menu_node 'users', 'Blogi', '$Blogs'
exec sp_add_menu_node 'users', 'Użytkownicy', '$Users'


exec sp_add_menu_node '$Documents', '@', '&DocumentsFolder'
exec sp_add_menu_node '&DocumentsFolder', '@', '&Document'

exec sp_add_menu_node '$Users', '@', '&UsersGroup'
exec sp_add_menu_node '&UsersGroup', '@', '&User'
exec sp_add_menu_node '$Blogs', '@', '&Blog'

exec sp_add_menu_node '&Blog', '@', '&BlogEntry'

exec sp_add_menu_node '$Articles', '@', '&ArticleFolder'
exec sp_add_menu_node '&ArticleFolder', '@', '&Article'

exec sp_add_menu_node '$Glossary', '@', '&GlossaryCategory'

exec sp_add_menu_node '&GlossaryCategory', '@', '&Glossary'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryNotCorpo'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryVersion'

go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

exec sp_update_version '1.1.5.3.1', '1.1.5.4';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

