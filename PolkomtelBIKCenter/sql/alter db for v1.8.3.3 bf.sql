﻿exec sp_check_version '1.8.3.3';
go

	if not exists (select 1 from sys.objects where object_id=object_id('bik_attribute_hyperlink')  )
	create table bik_attribute_hyperlink (
		id int not null identity(1,1) primary key,
		src_node_id int not null,
		dst_node_id int not null,
		attribute_id int not null
		);
		
	
	DECLARE attr_cursor CURSOR  
	FOR SELECT bal.node_id,bal.attribute_id,bal.value FROM bik_attribute_linked  bal
	join bik_attribute ba on bal.attribute_id=ba.id
	join bik_attr_def bad on bad.id=ba.attr_def_id where ba.is_deleted=0 and bad.is_deleted=0 and coalesce(ba.type_attr,bad.type_attr) like 'HyperlinkIn%'
	and name not like 'metaBIKS%'
	--and node_id not in (select id from bik_node where tree_id in (select id from bik_tree where tree_kind='metaBIKS'))
	declare @node_id int, @attribute_id int , @value  varchar(2000)

	OPEN attr_cursor  
	FETCH NEXT FROM attr_cursor into  @node_id ,@attribute_id,@value ; 
	

	while @@fetch_status = 0
	begin
		insert into bik_attribute_hyperlink(src_node_id,dst_node_id,attribute_id)
		select SUBSTRING(str,0, CHARINDEX('_',str,0)) ,@node_id,@attribute_id    from
		dbo.fn_split_by_sep(@value,'|',7)	where SUBSTRING(str,0, CHARINDEX('_',str,0)) is not null and SUBSTRING(str,0, CHARINDEX('_',str,0)) not like ''
		and  not exists (select * from bik_attribute_hyperlink bah 
		where bah.src_node_id=SUBSTRING(str,0, CHARINDEX('_',str,0))
		 and bah.dst_node_id=@node_id and attribute_id=@attribute_id)

	FETCH NEXT FROM attr_cursor into @node_id ,@attribute_id,@value ; 
	end

	close attr_cursor;

	deallocate attr_cursor;
		
exec sp_update_version '1.8.3.3', '1.8.3.4';
go

