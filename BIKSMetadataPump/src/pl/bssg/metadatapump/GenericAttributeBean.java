/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

/**
 *
 * @author tflorczak
 */
public class GenericAttributeBean {

    public String objId;
    public String name;
    public Object value;

    public GenericAttributeBean() {
    }

    public GenericAttributeBean(String objId, String name, Object val) {
        this.objId = objId;
        this.name = name;
        this.value = val;
    }
}
