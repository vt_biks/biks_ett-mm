/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.contentsExtractor;

/**
 *
 * @author pmielanczuk
 */
public class CompoundContentsExtractor implements IContentsExtractor {

    protected IContentsExtractor[] extractors;

    public CompoundContentsExtractor(IContentsExtractor... extractors) {
        this.extractors = extractors;
    }

    protected int findExtractorIdx(String fullFilePath) {
        for (int i = 0; i < extractors.length; i++) {
            if (extractors[i].canExtractFromFile(fullFilePath)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean canExtractFromFile(String fullFilePath) {
        return findExtractorIdx(fullFilePath) >= 0;
    }

    @Override
    public String extractContents(String fullFilePath) {
//        String fileName = BaseUtils.extractFileName(fullFilePath);
        int extractorIdx = findExtractorIdx(fullFilePath);
        System.out.println("will use extractor: extractorIdx=" + extractorIdx);
        return extractors[extractorIdx].extractContents(fullFilePath);
    }

    public static CompoundContentsExtractor makeExtractorForPlainTextAndTika(String pathToTikaJar, String plainTextExts, String tikaExts) {
        PlainTextContentsExtractor e1 = new PlainTextContentsExtractor("txt|log|sql|csv");
        TikaContentsExtractor e2 = new TikaContentsExtractor(pathToTikaJar, "*");
        return new CompoundContentsExtractor(e1, e2);
    }
}
