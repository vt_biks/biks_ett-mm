package pl.fovis.common.i18npoc;

import simplelib.i18nsupport.I18nMessage;
import simplelib.i18nsupport.I18nMessagePlEn;
import simplelib.i18nsupport.I18nMessages;

public class I18n implements I18nMessages<I18nMessagePlEn> {

    public static final I18nMessage filtrujWPoddrzewie = new I18nMessagePlEn("Filtruj w poddrzewie", "Filter in subtree");

    public static final I18nMessage zmienionoObszaryUprawnien = new I18nMessagePlEn("Zmieniono obszary uprawnień", "Privilege labels modified");
    public static final I18nMessage edycjaObszarowUprawnien = new I18nMessagePlEn("Edycja obszarów uprawnień", "Edit privilege labels");
    public static final I18nMessage obszaryUprawnien = new I18nMessagePlEn("Obszary uprawnień", "Privilege labels");
    public static final I18nMessage zobaczSugerowanePowiazania = new I18nMessagePlEn("Zobacz sugerowane powiązania", "Suggested associations");
    public static final I18nMessage podobneObiektyDla = new I18nMessagePlEn("Podobne obiekty dla", "Objects similar to");
    public static final I18nMessage brakAktywnychTestow = new I18nMessagePlEn("Brak testów", "No tests");

    public static final I18nMessage lacznyCzasPrzetwarzaniaDanychPrzezKonektor
            = new I18nMessagePlEn("Łączny czas wszystkich operacji przetwarzania danych przez konektor",
                    "Total time of all data processing operations of connector");
    public static final I18nMessage lacznyCzasZapisuWBIKS = new I18nMessagePlEn("Łączny czas wszystkich pozostałych operacji (bez odczytu)",
            "Total time of all other operations (excluding read operations)");
    public static final I18nMessage lacznyCzasOdczytuDanych = new I18nMessagePlEn("Łączny czas wszystkich operacji odczytu danych z systemu",
            "Total time of all read operations");

    public static final I18nMessage uzytkownikWylaczonyNastapiWylogowanie = new I18nMessagePlEn("Użytkownik został wyłączony, nastąpi wylogowanie z BIKS.",
            "User is disabled, there will be logout from BIKS.");
    public static final I18nMessage okMozeszKontynuowacPrace = new I18nMessagePlEn("OK, możesz kontynuować pracę",
            "OK, You may continue your work");

    public static final I18nMessage oczekiwanieNaZalogowanie = new I18nMessagePlEn("Oczekiwanie na zalogowanie na innej zakładce",
            "Waiting for login on other tab");

    public static final I18nMessage niezalogowany = new I18nMessagePlEn("niezalogowany",
            "unlogged");
    public static final I18nMessage zalogowanyJakoInny = new I18nMessagePlEn("zalogowany jako inny użytkownik",
            "logged in as different user");

    public static final I18nMessage aktualnieJestes = new I18nMessagePlEn("Aktualnie jesteś {0}.\n\nZaloguj się na innej zakładce na użytkownika {1} aby kontynuować.",
            "Currently You are {0}.\n\nLogin on other tab as user {1} to continue.");

    public static final I18nMessage jestesWylogowany = new I18nMessagePlEn("Zostałeś wylogowany z BIKS",
            "You have been unlogged from BIKS");
    public static final I18nMessage jestesZalogowanyJakoInny = new I18nMessagePlEn("Jesteś zalogowany do BIKS jako inny użytkownik",
            "You are logged to BIKS as different user");
    public static final I18nMessage abyKontynuowacZalogujSieSimple = new I18nMessagePlEn("Aby kontynuować - zaloguj się ponownie.",
            "To continue You must relogin.");

    public static final I18nMessage abyKontynuowacZalogujSie = new I18nMessagePlEn("Aby kontynuować - zaloguj się ponownie na aktualnej zakładce (możliwość utraty aktualnej pracy) lub otwórz inną i tam się zaloguj (to pozwoli na kontynuowanie pracy na tej zakładce).",
            "To continue You must relogin on current tab (possible loss of current work) or open new tab and login there (this will allow to keep your current work on this tab).");

    public static final I18nMessage przelogujSieNaAktZakl = new I18nMessagePlEn("Przeloguj się na aktualnej zakładce",
            "Relogin on current tab");
    public static final I18nMessage zalogujSieNaInnejZakl = new I18nMessagePlEn("Zaloguj się na innej zakładce",
            "Login on new tab");

    public static final I18nMessage czyNaPewnoMozeszUtracicPrace = new I18nMessagePlEn("Czy na pewno chcesz się przelogować?\n\nOtwarta są okna dialogowe, to może spowodować utratę aktualnej pracy!",
            "Are You sure to relogin?\n\nThere are dialogs open, possible lost of current work!");

    public static final I18nMessage bladKomunikacjiW = new I18nMessagePlEn("Błąd komunikacji w: ", "Error in communication on: ");
    public static final I18nMessage poDodaniuDrzewa = new I18nMessagePlEn(
            "Aby nowe drzewo pojawiło się w menu, konieczne jest przeładowanie aplikacji.\n\nCzy chcesz to zrobić teraz?",
            "It is necessary to reload application to see new tree in menu.\n\nDo You want to reload application now?");

    public static final I18nMessage poEdycjiDrzewa = new I18nMessagePlEn(
            "Aby wyświetlić menu z uwzględnieniem modyfikacji drzewa, konieczne jest przeładowanie aplikacji.\n\n"
            + "Czy chcesz to zrobić teraz?",
            "Tree importing may be still in processing. Please wait.\n"
            + "It is necessary to reload application to see modified tree in menu.\n\n"
            + "Do You want to reload application now?");

    public static final I18nMessage poUsunieciuDrzewa = new I18nMessagePlEn(
            "Aby wyświetlić menu nie zawierające usuniętego drzewa, konieczne jest przeładowanie aplikacji.\n\nCzy chcesz to zrobić teraz?",
            "It is necessary to reload application to see menu without deleted tree.\n\nDo You want to reload application now?");

    public static final I18nMessage bladTworzeniaNowegoDrzewa = new I18nMessagePlEn("Błąd podczas tworzenia nowego drzewa", "Failure on create tree");

    public static final I18nMessage wZwiazkuZaAutoLogout = new I18nMessagePlEn("W związku z długim czasem braku aktywności, nastąpi automatyczne wylogowanie z systemu BIKS.\n\nZaloguj się ponownie, aby korzystać z systemu.", "Due to long period of inactivity, there will be auto-logout.\n\nLogin again to use BIKS.");
    public static final I18nMessage modyfikacjaDanych = new I18nMessagePlEn("Modyfikacja danych", "Data modification");
    public static final I18nMessage tylkoOdczyt = new I18nMessagePlEn("Tylko odczyt", "Read only");
    public static final I18nMessage edytujSzablonZwyklegoUzytkownika = new I18nMessagePlEn("Edytuj szablon Zwykłego użytkownika", "Edit Regular user template");
    public static final I18nMessage uzytkownikNierozpoznany = new I18nMessagePlEn("(Użytkownik nierozpoznany)", "(Unrecognized user)");
    public static final I18nMessage wyszukiwanieZaawansowane = new I18nMessagePlEn("Wyszukiwanie zaawansowane", "Advanced search");
    public static final I18nMessage zaawansowane = new I18nMessagePlEn("Zaawansowane", "Advanced");
    public static final I18nMessage zwykle = new I18nMessagePlEn("Zwykłe", "Normal");
    public static final I18nMessage criterions = new I18nMessagePlEn("Kryteria", "Criterions");
    public static final I18nMessage and = new I18nMessagePlEn("i", "and");
    public static final I18nMessage trwaImportowanie = new I18nMessagePlEn("Proces importowania drzewa może jeszcze trwać. Prosimy o cierpliwość.", "Tree importing tree may be still in processing. Please wait.");
    public static final I18nMessage podajKryterium = new I18nMessagePlEn("Podaj kryteria do wyszukiwania", "Add criteria to search");
    public static final I18nMessage liczbaWyswietlonychPozycjiNaStronie = new I18nMessagePlEn("Liczba wyświetlonych pozycji na stronie", "Number of items displayed per page");
    public static final I18nMessage niePrawidlowaWartosc = new I18nMessagePlEn("Podana wartość nie jest prawidłowa", "You entered invalid number");

    public static final I18nMessage foundAnticipatedObject = new I18nMessagePlEn("Znaleziono planowany obiekt", "Anticipated object has been found");
    public static final I18nMessage anticipatedObjectNotFound = new I18nMessagePlEn("Planowany obiekt nie został znaleziony", "Anticipated object not found");

    public static final I18nMessage restrictLocationToSubobjects = new I18nMessagePlEn("Planowany obiekt będzie podobiektem w", "Anticipated object will be descendant of");
    public static final I18nMessage generatedLinkForAnticipatedObj = new I18nMessagePlEn("Link do planowanego obiektu (skopiuj poniższy link do schowka)", "Generated link for anticipated object (copy following link to clipboard)");
    public static final I18nMessage enterAnticipatedObjName = new I18nMessagePlEn("Podaj nazwę planowanego obiektu", "Enter name of anticipated object");
    public static final I18nMessage anticipatedObjLinkAct = new I18nMessagePlEn("Generuj link do planowanego obiektu", "Generate link for anticipated object");
    public static final I18nMessage advancedSearchDataInfo = new I18nMessagePlEn("Daty należy podawać w formacie: <i>yyyy-mm-dd hh:mm:ss</i>, <br>np: <i>2014-05-19 13:39:09</i>", "Dates should be given in the following format:  <i>yyyy-mm-dd hh:mm:ss</i>, <br>for example: <i>2014-05-19 13:39:09</i>");

    // TŁUMACZENIA WBUDOWANE / BUILD IN
    public static final I18nMessage BUILD_allUsers = new I18nMessagePlEn("Wszyscy użytkownicy", "All users");
    public static final I18nMessage BUILD_allTests = new I18nMessagePlEn("Wszystkie testy", "All tests");
    public static final I18nMessage BUILD_universeTable = new I18nMessagePlEn("Tabele", "Tables");
    public static final I18nMessage BUILD_sapbwFeature = new I18nMessagePlEn("Cechy i wskaźniki", "Characteristic and measures");
    public static final I18nMessage BUILD_tables = new I18nMessagePlEn("Tabele", "Tables");
    public static final I18nMessage BUILD_views = new I18nMessagePlEn("Widoki", "Views");
    public static final I18nMessage BUILD_procedures = new I18nMessagePlEn("Procedury", "Stored Procedures");
    public static final I18nMessage BUILD_packages = new I18nMessagePlEn("Pakiety", "Packages");
    public static final I18nMessage BUILD_functions = new I18nMessagePlEn("Funkcje", "Functions");
    public static final I18nMessage BUILD_scalarFunctions = new I18nMessagePlEn("Funkcje skalarne", "Scalar-valued Functions");
    public static final I18nMessage BUILD_tableFunctions = new I18nMessagePlEn("Funkcje tabelaryczne", "Table-valued Functions");
    //
    public static final I18nMessage doneLoadingAttributes = new I18nMessagePlEn("Zakończenia łądowania attrybutów", "Done loading attributes");
    public static final I18nMessage incorrectLink = new I18nMessagePlEn("Nieproprawny link", "The link is incorrect");
    public static final I18nMessage linkWasAlreadyUsed = new I18nMessagePlEn("Link został już wykorzystany", "The link was already used");
    public static final I18nMessage linkIsNotValid = new I18nMessagePlEn("Link jest nieważny", "The link is invalid");
    public static final I18nMessage resettingDbInProgressPleaseWait = new I18nMessagePlEn("Trwa resetowanie bazy danych, proszę czekać...", "Resetting database in progress, please wait...");
    public static final I18nMessage pleaseWait = new I18nMessagePlEn("Proszę czekać...", "Please wait...");
    public static final I18nMessage changeContext = new I18nMessagePlEn("Zmień kontekst", "Change context");
    public static final I18nMessage sendingRegistrationEmail = new I18nMessagePlEn("Wysyłanie maila rejestracyjnego", "Sending registration e-mail");
    public static final I18nMessage sendingResetEmail = new I18nMessagePlEn("Wysyłanie maila resetującego", "Sending reseting e-mail");
    public static final I18nMessage email = new I18nMessagePlEn("E-mail", "E-mail");
    public static final I18nMessage niepoprawnyAdresEmail = new I18nMessagePlEn("Niepoprawny adres e-mail", "Invalid email");
    public static final I18nMessage emailNieJestPoprawny = new I18nMessagePlEn("{0} nie jest poprawnym adresem e-mail", "{0} is not a valid email address");
    public static final I18nMessage maxLiczbaAdresow = new I18nMessagePlEn("Możesz wysłać zaproszenia dla maksymalnie {0} użytkowników", "You can only invite {0} users");
    public static final I18nMessage rightRoleRegularUser = new I18nMessagePlEn("Zwykły użytkownik", "Regular user");
    public static final I18nMessage rightRoleAuthor = new I18nMessagePlEn("Autor", "Author");
    public static final I18nMessage rightRoleEditor = new I18nMessagePlEn("Redaktor", "Editor");
    public static final I18nMessage rightRoleExpert = new I18nMessagePlEn("Ekspert", "Expert");
    public static final I18nMessage rightRoleAppAdmin = new I18nMessagePlEn("Admin aplikacji", "App Admin");
    public static final I18nMessage rightRoleAdministrator = new I18nMessagePlEn("Administrator", "Administrator");
    public static final I18nMessage nazwaTabeliReferencjiWBazieDanych = new I18nMessagePlEn("Nazwa tabeli referencji w bazie danych", "Reference table name in database") /* i18n: . */;
    public static final I18nMessage nazwaKolumnyReferencji = new I18nMessagePlEn("Nazwa kolumny referencji", "Reference column name") /* i18n: . */;
    public static final I18nMessage loginJestJuzUzywanyWybierzInny = new I18nMessagePlEn("Login jest już używany. Wybierz inny.", "This login is already in use. Choose different one") /* I18N:  */;
    public static final I18nMessage rejestracja = new I18nMessagePlEn("Rejestracja", "Register") /* I18N:  */;
    public static final I18nMessage bledyIdentyfikatorKategoryzacji = new I18nMessagePlEn("Błędny identyfikator kategoryzacji", "Invalid categorization id") /* I18N:  */;
    public static final I18nMessage bladKomunikacjiZTeradata = new I18nMessagePlEn("Błąd komunikacji z Teradatą", "Teradata communication error") /* i18n: . */;
    public static final I18nMessage daneSlownika = new I18nMessagePlEn("Dane słownika", "Dictionary main data") /* i18n: . */;
    public static final I18nMessage importuj = new I18nMessagePlEn("Importuj", "Import") /* i18n: . */;
    public static final I18nMessage wyswietlanaNazwaSlownika = new I18nMessagePlEn("Wyświetlana nazwa słownika", "Dictionary name to display") /* i18n: . */;
    public static final I18nMessage nazwaSlownikaWBazieDanych = new I18nMessagePlEn("Nazwa słownika w bazie danych - operacyjna (widok lub tabela)", "Dictionary name in database - operational (view or table)") /* i18n: . */;
    public static final I18nMessage nazwaSlownikaWBazieDanychFaktyczna = new I18nMessagePlEn("Nazwa słownika w bazie danych - faktyczna (tylko tabela)", "Dictionary name in database - real (only table)") /* i18n: . */;
    public static final I18nMessage schedulePaused = new I18nMessagePlEn("Wstrzymany", "Paused") /* I18N:  */;
    public static final I18nMessage schedulePending = new I18nMessagePlEn("Oczekujące", "Pending") /* I18N:  */;
    public static final I18nMessage scheduleFailure = new I18nMessagePlEn("Zakończony błędem", "Failure") /* I18N:  */;
    public static final I18nMessage scheduleComplete = new I18nMessagePlEn("Zakończony sukcesem", "Complete") /* I18N:  */;
    public static final I18nMessage scheduleRunning = new I18nMessagePlEn("Uruchomiony", "Running") /* I18N:  */;
    public static final I18nMessage instancja = new I18nMessagePlEn("Instancja", "Instance") /* I18N:  */;
    public static final I18nMessage enterEmail = new I18nMessagePlEn("Podaj adres email", "Enter email address");
    public static final I18nMessage resetingPassword = new I18nMessagePlEn("Restartowanie hasła", "Restarting password");
    public static final I18nMessage forgotPassword = new I18nMessagePlEn("Zapomniałeś hasła?", "Forgot password?");
    // dla BF: po wrzuceniu obrazka z [Search in BIKS] jako images/searchHPBtn_en.gif,
    // zmienić drugi tekst tutaj na searchHPBtn_en.gif
    public static final I18nMessage searchHPBtn = new I18nMessagePlEn("searchHPBtn.gif", "searchHPBtn_en.gif") /* i18n: button with text */;
    public static final I18nMessage noValueAttribute = new I18nMessagePlEn("ma wartość", "has value") /* i18n:  */;
    public static final I18nMessage singleValueAttributeEquals = new I18nMessagePlEn("równa się", "equals") /* i18n:  */;
    public static final I18nMessage singleValueAttributeContains = new I18nMessagePlEn("zawiera", "contains") /* i18n:  */;
    public static final I18nMessage singleValueAttributeSqlLike = new I18nMessagePlEn("LIKE (T-SQL)", "LIKE (T-SQL)") /* i18n:  */;
    public static final I18nMessage singleValueAttributeIn = new I18nMessagePlEn("występuje na liście", "in list") /* i18n:  */;
    public static final I18nMessage singleValueAttributeStartsWith = new I18nMessagePlEn("zaczyna się na", "starts with") /* i18n:  */;
    public static final I18nMessage singleValueAttributeEndsWith = new I18nMessagePlEn("kończy się na", "ends with") /* i18n:  */;
    public static final I18nMessage fromTo = new I18nMessagePlEn("z przedziału", "between") /* i18n: */;
    public static final I18nMessage greaterThan = new I18nMessagePlEn(">=", ">=") /* i18n: */;
    public static final I18nMessage smallerThan = new I18nMessagePlEn("<=", "<=") /* i18n: */;
    public static final I18nMessage equal = new I18nMessagePlEn("=", "=") /* i18n: */;
    public static final I18nMessage pleaseChoose = new I18nMessagePlEn("Proszę wybrać", "Please choose") /* i18n:  */;
    public static final I18nMessage notUniqueColumn = new I18nMessagePlEn("Nie ma relacji 1-1", "Not in relation 1-1") /* i18n:  */;
    public static final I18nMessage like = new I18nMessagePlEn("LIKE", "LIKE") /* i18n:  */;

    // not translated
    // single occurrence
    // pl.fovis.client.bikpages.lisa.LisaLogsPage
    public static final I18nMessage dataAkcji = new I18nMessagePlEn("Data akcji", "Time of modification") /* i18n:  */; // #1
    // not translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.lisa.LisaAuthorizedRequest
    public static final I18nMessage gotowe = new I18nMessagePlEn("Gotowe", "Done") /* i18n:  */; // #2
    // not translated
    // single occurrence
    // pl.fovis.client.bikpages.lisa.LisaConnectionPage
    public static final I18nMessage nazwaBazyDanych = new I18nMessagePlEn("Nazwa głównej bazy danych", "Main database name") /* i18n:  */; // #4
    // not translated
    // single occurrence
    // pl.fovis.client.bikpages.lisa.LisaConnectionPage
    public static final I18nMessage nazwaSerwera = new I18nMessagePlEn("Nazwa serwera", "Server name") /* i18n:  */; // #5
    // not translated
    // single occurrence
    // pl.fovis.client.bikpages.lisa.LisaLogsPage
    public static final I18nMessage nazwaSlownika = new I18nMessagePlEn("Nazwa słownika", "Dictionary name") /* i18n:  */; // #6
    public static final I18nMessage pobieranieDanychSlownikaGotowe = new I18nMessagePlEn("Pobieranie danych słownika - gotowe", "Downloading data dictionary - finished") /* i18n:  */; // #6
    public static final I18nMessage pobieranieDanychProcesowErwin = new I18nMessagePlEn("Pobieranie danych procesow - gotowe", "Downloading data process - finished") /* i18n:  */; // #6
    // not translated
    // single occurrence
    // pl.fovis.server.LisaServiceImpl
    public static final I18nMessage nieZdefinioKluczaGlownegoWTymSlo = new I18nMessagePlEn("Nie zdefiniowano klucza głównego w tym słowniku", "Primary key is not defined in this dictionary") /* i18n:  */; // #7
    // not translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.ColumnAvailabilityText
    public static final I18nMessage niewidoczna = new I18nMessagePlEn("niewidoczna", "not visible") /* i18n:  */; // #8
    // not translated
    // single occurrence
    // pl.fovis.server.lisa.AuthorizedTeradataConnection
    public static final I18nMessage nieznanySerwerBazyDanych = new I18nMessagePlEn("Nieznany serwer bazy danych", "Unknown database host") /* i18n:  */; // #9
    // not translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AdminConfigDBBaseWidget
    public static final I18nMessage optionalObjectFilterUseTAsAliasT = new I18nMessagePlEn("Opcjonalny filtr na obiekty (korzystaj z 't' jako alias do sysobject)", "Optional object filter (use t as alias to sysobject") /* i18n:  */; // #10
    // not translated
    // single occurrence
    // pl.fovis.client.bikpages.lisa.LisaLogsPage
    public static final I18nMessage podjetaAkcja = new I18nMessagePlEn("Podjęta akcja", "Action taken") /* i18n:  */; // #11
    public static final I18nMessage koniecWyszukiwania = new I18nMessagePlEn("Koniec wyszukiwania", "Search ended") /* i18n:  */; // #11
    // not translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.lisa.LisaDictionaryColumnEditionDialog
    // pl.fovis.client.dialogs.lisa.LisaBeanDisplay
    public static final I18nMessage poziomDostepu = new I18nMessagePlEn("Poziom dostępu", "Access level") /* i18n:  */; // #12
    // not translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.lisa.LisaDictionaryContent
    public static final I18nMessage pozycjaZostalaZmieniona = new I18nMessagePlEn("Pozycja została zmieniona", "The position has been modified") /* i18n:  */; // #13
    public static final I18nMessage dodajPozycje = new I18nMessagePlEn("Dodaj pozycję", "Add position") /* i18n:  */;
    // not translated
    // single occurrence
    // pl.fovis.server.LisaServiceImpl
    public static final I18nMessage proszePodacPoprawnyLoginIHasloDo = new I18nMessagePlEn("Proszę podać poprawny login i hasło do połączenia z TERADATĄ", "Please provide valid login and password for TERADATA connection") /* i18n:  */; // #14
    // not translated
    // single occurrence
    // pl.fovis.client.bikpages.lisa.LisaConnectionPage
    public static final I18nMessage schemat = new I18nMessagePlEn("Nazwa słownikowej bazy danych", "Dictionary database name") /* i18n:  */; // #15
    // not translated
    // single occurrence
    // pl.fovis.server.lisa.AuthorizedTeradataConnection
    public static final I18nMessage serwerBazyDanychNieOdpowiada = new I18nMessagePlEn("Serwer bazy danych nie odpowiada", "Database server does not respond") /* i18n:  */; // #16
    // not translated
    // single occurrence
    // pl.fovis.client.dialogs.ViewTutorialInFullScreenDialog
    public static final I18nMessage tutorialEntryUpdated = new I18nMessagePlEn("Tutorial entry updated") /* i18n:  */; // #18
    // not translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.ColumnAvailabilityText
    public static final I18nMessage tylkoDoOdczytu = new I18nMessagePlEn("tylko do odczytu", "read only") /* i18n:  */; // #19
    // not translated
    // single occurrence
    // pl.fovis.client.bikpages.lisa.LisaConnectionPage
    public static final I18nMessage typTransakcji = new I18nMessagePlEn("Typ transakcji", "Transaction mode") /* i18n:  */; // #20
    // not translated
    // single occurrence
    // pl.fovis.client.bikpages.lisa.LisaLogsPage
    public static final I18nMessage uzytkownikBIKS = new I18nMessagePlEn("Użytkownik BIKS", "BIKS username") /* i18n:  */; // #21
    // not translated
    // single occurrence
    // pl.fovis.client.bikpages.lisa.LisaLogsPage
    public static final I18nMessage uzytkownikTERADATA = new I18nMessagePlEn("Użytkownik TERADATA", "TERADATA login") /* i18n:  */; // #22
    // not translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.lisa.LisaAuthorizedRequest
    public static final I18nMessage zadanieWToku = new I18nMessagePlEn("Żądanie w toku", "Processing request") /* i18n:  */; // #23
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AuthorBeanExtractor
    public static final I18nMessage administratorBloga = new I18nMessagePlEn("Administrator bloga", "Blog Admin") /* I18N:  */; // #24
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage adresURL = new I18nMessagePlEn("Adres URL", "URL address") /* I18N:  */; // #25
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage adresaci = new I18nMessagePlEn("Adresaci", "Recipients") /* I18N:  */; // #26
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.TreeBeanExtractor
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage akcje = new I18nMessagePlEn("Akcje", "Actions") /* I18N:  */; // #27
    public static final I18nMessage akcja = new I18nMessagePlEn("Akcja", "Action") /* I18N:  */; // #27
    // is translated
    // single occurrence
    // pl.fovis.client.menu.MenuBuilder
    public static final I18nMessage aktualnieWybranyJezyk = new I18nMessagePlEn("Aktualnie wybrany język", "Current language") /* I18N:  */; // #28
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSNewsTab
    public static final I18nMessage aktualnosci = new I18nMessagePlEn("Komunikaty", "Announcements") /* I18N:  */; // #29
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage aktywnoscKonta = new I18nMessagePlEn("Aktywność konta", "Account activity") /* I18N:  */; // #30
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminSchedulePage
    public static final I18nMessage aktywny = new I18nMessagePlEn("Aktywny", "Active") /* I18N:  */; // #31
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.LoginDialog
    public static final I18nMessage alboZalogujSieDomenowoSSO = new I18nMessagePlEn("Albo zaloguj się domenowo (SSO", "For domain logon (SSO") /* I18N:  */; // #32
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DefDialog
    public static final I18nMessage anuluj = new I18nMessagePlEn("Anuluj", "Cancel") /* I18N:  */; // #33
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAttributeDialog
    public static final I18nMessage atrybut = new I18nMessagePlEn("atrybut", "attribute") /* I18N:  */; // #34
    public static final I18nMessage wykonajSkrypt = new I18nMessagePlEn("Wykonaj skrypt", "Run script") /* I18N:  */; // #34
    public static final I18nMessage wykonaj = new I18nMessagePlEn("Wykonaj", "Run") /* I18N:  */; // #34
    public static final I18nMessage skrypt = new I18nMessagePlEn("Skrypt", "Script") /* I18N:  */; // #34
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrUpdateAttributeAndTheirKinds
    public static final I18nMessage atrybutDodatkowy = new I18nMessagePlEn("Atrybut dodatkowy", "Auxiliary attribute") /* I18N:  */; // #35
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage atrybutOPodanejNazwieJuzIstnieje = new I18nMessagePlEn("Atrybut o podanej nazwie już istnieje", "Attribute with the specified name already exists") /* I18N:  */; // #36
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AttributeBikPage
    public static final I18nMessage atrybuty = new I18nMessagePlEn("Atrybuty", "Attributes") /* I18N:  */; // #37
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AttributeBikPage
    public static final I18nMessage atrybutyDodatkowe = new I18nMessagePlEn("Atrybuty dodatkowe", "Auxiliary attributes") /* I18N:  */; // #38
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AttributeBikPage
    public static final I18nMessage atrybutySystemowe = new I18nMessagePlEn("Atrybuty systemowe", "System attributes") /* I18N:  */; // #39
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.PickAuthorBlogDialog
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage autor = new I18nMessagePlEn("Autor", "Author") /* I18N:  */; // #40
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.BIKGWTConstants
    // pl.fovis.client.dialogs.PickAuthorBlogDialog
    public static final I18nMessage autorzy = new I18nMessagePlEn("Autorzy", "Authors") /* I18N:  */; // #41
    // is translated
    // single occurrence
    // pl.fovis.client.bikcenterEntryPoint
    public static final I18nMessage bazaDanychJestWWersjiV = new I18nMessagePlEn("Baza danych jest w wersji: v", "Database version: v") /* I18N:  */; // #42
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage bazaWiedzy = new I18nMessagePlEn("Baza Wiedzy", "Knowledge Base") /* I18N:  */; // #43
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage bazyDanych = new I18nMessagePlEn("Bazy danych", "Databases") /* I18N:  */; // #44
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage biblioteka = new I18nMessagePlEn("Biblioteka", "Library") /* I18N:  */; // #45
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    public static final I18nMessage biezaceHaslo = new I18nMessagePlEn("Bieżące hasło", "Current password") /* I18N:  */; // #46
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikwidgets.BikRequestChartOld
    // pl.fovis.client.bikwidgets.BikRequestChart
    public static final I18nMessage blad = new I18nMessagePlEn("Błąd", "Error") /* I18N:  */; // #47
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.LoginDialog
    public static final I18nMessage bladLogowaniaSprobujPonownie = new I18nMessagePlEn("Błąd logowania, spróbuj ponownie", "Logon error, try again") /* I18N:  */; // #48
    // is translated
    // single occurrence
    // pl.fovis.client.bikcenterEntryPoint
    public static final I18nMessage bladPolaczenPrawdopoTrwaInstalac = new I18nMessagePlEn("Błąd połączenia, prawdopodobnie trwa instalacja nowej wersji", "Connection error, probably instalation of new version is in progress") /* I18N:  */; // #49
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.BIKGWTConstants
    // pl.fovis.client.dialogs.PickBlogEntryDialog
    public static final I18nMessage blogi = new I18nMessagePlEn("Blogi", "Blogs") /* I18N:  */; // #50
    // is translated
    // single occurrence
    // pl.fovis.client.BIKClientSingletons
    public static final I18nMessage blogi_wtf = new I18nMessagePlEn("Blogi", "Blogs") /* I18N: wtf? */; // #51
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.bikpages.SystemUserBeanExtractor
    // pl.fovis.client.bikpages.TreeBeanExtractor
    // pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase
    public static final I18nMessage brak = new I18nMessagePlEn("brak", "none") /* I18N: ok? */; // #52
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage brak2 = new I18nMessagePlEn("Brak", "Unassigned") /* I18N: czy przypisany */; // #53
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage brak3 = new I18nMessagePlEn("brak", "none") /* I18N:  */; // #54
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSNewsTab
    public static final I18nMessage brakAktualnosci = new I18nMessagePlEn("Brak komunikatów", "No announcements") /* I18N:  */; // #55
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage brakAktywnycInstancjNieMoznaUruc = new I18nMessagePlEn("Brak aktywnych serwerów. Nie można uruchomić zasilania. Przejdź do konfiguracji zasileń i skonfiguruj serwer, zanim ją uruchomisz", "No active servers. Cannot run data load. Go to data load config tab and configure server before running loading") /* I18N:  */; // #56
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedSystemAttrDictWidget
    public static final I18nMessage brakAtrybutowZaznaczWezel = new I18nMessagePlEn("Brak atrybutów, zaznacz węzeł", "No attributes, select node") /* I18N:  */; // #57
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.UserRankPageTab
    public static final I18nMessage brakDanychDoZbudowanRankinguRank = new I18nMessagePlEn("Brak danych do zbudowania rankingu. Ranking powstaje na podstawie treści dodanych przez użytkowników", "No data to build ranking. Ranking is based on user generated content") /* I18N:  */; // #58
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.entitydetailswidgets.AttachmentsRolesWidgetFlat
    // pl.fovis.client.entitydetailswidgets.AttachmentsWidgetFlat
    public static final I18nMessage brakDokumentacji = new I18nMessagePlEn("Brak dokumentacji", "No documentation") /* I18N:  */; // #59
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage brakDostepu = new I18nMessagePlEn("Brak dostępu", "Access denied") /* I18N:  */; // #60
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.MyBIKSHistoryTab
    // pl.fovis.client.bikpages.MyBIKSHistoryTabNew
    public static final I18nMessage brakHistorii = new I18nMessagePlEn("Brak historii", "No history") /* I18N:  */; // #61
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage brakIndeksuWyszukiwNieMoznaWyszu = new I18nMessagePlEn("brak indeksu wyszukiwarki, nie można wyszukiwać. Najprawdopodobniej trwa zasilanie, poczekaj aż zostanie ukończone lub skontaktuj się z administratorem", "missing search index, cannot search. Most likely data load is in progress, wait until it finishes or contact your administrator") /* I18N:  */; // #62
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.entitydetailswidgets.UserNoteWidgetFlat
    // pl.fovis.client.bikpages.MyBIKSNotesTab
    // pl.fovis.client.entitydetailswidgets.NoteWidgetFlat
    public static final I18nMessage brakKomentarzy = new I18nMessagePlEn("Brak komentarzy", "No comments") /* I18N:  */; // #63
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.DataPumpPage
    public static final I18nMessage brakKonektorow = new I18nMessagePlEn("Brak konektorów", "No connectors") /* I18N:  */; // #64
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSVoteTab
    public static final I18nMessage brakOcen = new I18nMessagePlEn("Brak ocen", "No votes") /* I18N:  */; // #65
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage brakOcen2 = new I18nMessagePlEn("brak ocen", "no votes") /* I18N:  */; // #66
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.InnerLinksWidgetFlat
    public static final I18nMessage brakOdsylaczy = new I18nMessagePlEn("Brak odsyłaczy", "No links") /* I18N:  */; // #67
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.dialogs.EditHelpDescriptionDialog
    // pl.fovis.client.dialogs.HelpDialog
    // pl.fovis.client.bikpages.HelpPageTree
    public static final I18nMessage brakOpisu = new I18nMessagePlEn("Brak opisu", "No description") /* I18N:  */; // #68
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.EntityDetailsTabPaginationBase
    public static final I18nMessage brakPowiazan = new I18nMessagePlEn("Brak powiązań", "No associations") /* I18N:  */; // #69
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage brakSzukanejFrazyWpiszCzegoSzuka = new I18nMessagePlEn("Brak szukanej frazy, wpisz czego szukasz powyżej", "No search term, type what you are looking for above") /* I18N:  */; // #70
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSFavouriteTab
    public static final I18nMessage brakUlubionych = new I18nMessagePlEn("Brak ulubionych", "No favourites") /* I18N:  */; // #71
    // is translated
    // single occurrence
    // pl.fovis.server.BOXIServiceImpl
    public static final I18nMessage brakWersjiKorporacyjnej = new I18nMessagePlEn("Brak wersji korporacyjnej", "No corporate version") /* I18N:  */; // #72
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.BlogsWidget
    public static final I18nMessage brakWpisow = new I18nMessagePlEn("Brak wpisów", "No entries") /* I18N:  */; // #73
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage brakWynikowDla = new I18nMessagePlEn("Brak wyników dla", "No results for") /* I18N:  */; // #74
    public static final I18nMessage brakWynikow = new I18nMessagePlEn("Brak wyników", "No results") /* I18N:  */; // #74
    // is translated
    // single occurrence
    // pl.fovis.client.EDPForms
    public static final I18nMessage brakZdefinioAtrybutoDlaDanegoTyp = new I18nMessagePlEn("Brak zdefiniowanych atrybutów dla danego typu", "No attributes for this type") /* I18N:  */; // #75
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminSchedulePage
    public static final I18nMessage co14Dni = new I18nMessagePlEn("Co 14 dni", "Every 14 days") /* I18N:  */; // #76
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminSchedulePage
    public static final I18nMessage co3Dni = new I18nMessagePlEn("Co 3 dni", "Every 3 days") /* I18N:  */; // #77
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminSchedulePage
    public static final I18nMessage co5Dni = new I18nMessagePlEn("Co 5 dni", "Every 5 days") /* I18N:  */; // #78
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminSchedulePage
    public static final I18nMessage co7Dni = new I18nMessagePlEn("Co 7 dni", "Every 7 days") /* I18N:  */; // #79
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminSchedulePage
    public static final I18nMessage codziennie = new I18nMessagePlEn("Codziennie", "Everyday") /* I18N:  */; // #80
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.DataSourceStatusBeanExtractor
    public static final I18nMessage czasTrwaniaS = new I18nMessagePlEn("Czas trwania", "Elapsed time") /* I18N:  */; // #81
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.JoinTypeRolesWidget
    public static final I18nMessage czyDlaJuzIstniejaPowiazanWykonac = new I18nMessagePlEn("Czy dla już istniejących powiązań wykonać akcję", "Execute action for already existing associations") /* I18N:  */; // #82
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage czyNAPEWNOChceszZatrzymaProcesZa = new I18nMessagePlEn("Czy NA PEWNO chcesz zatrzymać proces zasilający i uruchomić go ponownie? "
            + "Używanie tej opcji zaleca się jedynie w przypadku awarii procesu zasilania", "Are you sure you want to stop data load process and run it again? "
            + "This is recommended only in case of data load failure") /* I18N:  */; // #83
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.TreeBeanExtractor
    public static final I18nMessage czyNAPEWNOUsunacDrzewoWrazZJegoE = new I18nMessagePlEn("Czy NA PEWNO usunąć drzewo wraz z jego elementami? Usunięcie drzewa jest NIEODWRACALNE", "Are you sure you want to delete tree with all it's items? Deleting tree is irreversible") /* I18N:  */; // #84
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedAddAttrDictWidget
    public static final I18nMessage czyNaPewnoChceszUsunacAtrybut = new I18nMessagePlEn("Czy na pewno chcesz usunąć atrybut", "Are you sure you want to delete attribute") /* I18N:  */; // #85
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage czyNaPewnoRozpoczacZasilanieZ = new I18nMessagePlEn("Czy na pewno rozpocząć zasilanie z", "Are you sure you want to start data load from") /* I18N:  */; // #86
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.entitydetailswidgets.GenericBeanExtractorBase
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage czyNaPewnoUsunac = new I18nMessagePlEn("Czy na pewno usunąć", "Are you sure you want to delete") /* I18N:  */; // #87
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewsActionExtractor
    public static final I18nMessage czyNaPewnoUsunacAktualnosc = new I18nMessagePlEn("Czy na pewno usunąć komunikat", "Are you sure you want to delete announcement item") /* I18N:  */; // #88
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AttributeWidget
    public static final I18nMessage czyNaPewnoUsunacAtrybut = new I18nMessagePlEn("Czy na pewno usunąć atrybut", "Are you sure you want to delete attribute") /* I18N:  */; // #89
    public static final I18nMessage czyNaPewnoUsunacZdarzenie = new I18nMessagePlEn("Czy na pewno usunąć zdarzenie", "Are you sure you want to delete event") /* I18N:  */; // #89
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.nodeactions.NodeActionDelete
    // pl.fovis.client.nodeactions.NodeActionDeleteRole
    public static final I18nMessage czyNaPewnoUsunacElement = new I18nMessagePlEn("Czy na pewno usunąć element", "Are you sure you want to delete item") /* I18N:  */; // #90
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedDictionaryWidgetBase
    public static final I18nMessage czyNaPewnoUsunacKategoriUsuniesz = new I18nMessagePlEn("Czy na pewno usunąć kategorię? Usuniesz wraz z nią podrzędne role i atrybuty", "Are you sure you want to delete category? It will also delete descendant roles and attributes") /* I18N:  */; // #91
    public static final I18nMessage bladPrzyDodawaniuWiersza = new I18nMessagePlEn("Błąd przy dodawaniu! Wpisz poprawne wartości.", "Error in adding! Insert correct values!") /* I18N:  */;
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.NoteWidgetFlat
    public static final I18nMessage czyNaPewnoUsunacKomentarz = new I18nMessagePlEn("Czy na pewno usunąć komentarz", "Are you sure you want to delete comment") /* I18N:  */; // #92
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedRoleForNodeDictWidget
    public static final I18nMessage czyNaPewnoUsunacRoleWrazUsuwaszZ = new I18nMessagePlEn("Czy na pewno usunąć role? Wraz usuwasz z nim połączonych już użytkowników", "Are you sure to delete role? It will also delete users linked with this role") /* I18N:  */; // #93
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.ObjectInFvsBeanExtractor
    public static final I18nMessage czyNaPewnoUsunacZUlubionyElement2 = new I18nMessagePlEn("Czy na pewno usunąć z ulubionych element", "Are you sure to delete from favourites this item") /* I18N:  */; // #94
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    public static final I18nMessage czyNaPewnoUsunacZdjecie = new I18nMessagePlEn("Czy na pewno usunąć zdjęcie", "Are you sure to delete photo") /* I18N:  */; // #95
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.RolesDialog
    public static final I18nMessage dalej = new I18nMessagePlEn("Dalej", "Next") /* I18N:  */; // #96
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage data = new I18nMessagePlEn("Data", "Date") /* I18N:  */; // #97
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage dataDodania = new I18nMessagePlEn("Data dodania", "Date added") /* I18N:  */; // #98
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminSchedulePage
    public static final I18nMessage dataRozpoczecia = new I18nMessagePlEn("Data rozpoczęcia", "Start date") /* I18N:  */; // #99
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.DataSourceStatusBeanExtractor
    public static final I18nMessage dataUruchomienia = new I18nMessagePlEn("Data uruchomienia", "Execution date") /* I18N:  */; // #100
    public static final I18nMessage dataZakoczenia = new I18nMessagePlEn("Data zakończenia", "End date") /* I18N:  */; // #100
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.MetapediaDialog
    public static final I18nMessage definicjKorporacZostanieZamienio = new I18nMessagePlEn("Definicja korporacyjna zostanie zamieniona na inną", "Corporate definition will be replaced by another") /* I18N:  */; // #101
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeNodeDialog
    public static final I18nMessage definicjOTakiejNazwieJuzIstnieje = new I18nMessagePlEn("Definicja o takiej nazwie już istnieje", "Definition with the specified name already exists") /* I18N:  */; // #102
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.MetapediaDialog
    public static final I18nMessage definicjZostanieDodanaPodPojecie = new I18nMessagePlEn("Definicja zostanie dodana pod pojęciem biznesowym", "Definition will be added under business term") /* I18N:  */; // #103
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.MetapediaDialog
    public static final I18nMessage definicja = new I18nMessagePlEn("Definicja", "Definition") /* I18N:  */; // #104
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage definicjaKorporacyjna = new I18nMessagePlEn("Definicja korporacyjna", "Corporate definition") /* I18N:  */; // #105
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage dlugiTekst = new I18nMessagePlEn("Opis ANSI", "Description ANSI") /* I18N:  */; // #106
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage dni = new I18nMessagePlEn("dni", "days") /* I18N:  */; // #107
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.BikRequestChart
    public static final I18nMessage dniOdOstatniegoWykonania = new I18nMessagePlEn("dni od ostatniego wykonania", "days from last execution") /* I18N:  */; // #108
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage dniaKazdegoMiesiaca = new I18nMessagePlEn("dnia każdego miesiąca", "(-th) day of month") /* I18N:  */; // #109
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.ChangeCategoryDialog
    public static final I18nMessage doKategorii = new I18nMessagePlEn("do kategorii", "to category") /* I18N:  */; // #110
    // is translated
    // single occurrence
    // pl.fovis.client.BIKClientSingletons
    public static final I18nMessage doObejrzenFilmuPotrzebnJestFlash = new I18nMessagePlEn("Do obejrzenia filmu potrzebny jest Flash Player w wersji 9.0 lub wyższej."
            + "<a href=\"http://get.adobe.com/pl/flashplayer/\"> Pobierz Flash Player", "To view this video you need Flash Player v9.0 or higher"
            + "<a href=\"http://get.adobe.com/pl/flashplayer/\"> Download Flash Player") /* I18N:  */; // #111
    // is translated
    // multiple occurrences: 5
    // pl.fovis.client.nodeactions.NodeActionNewNestableParent
    // pl.fovis.client.dialogs.AddOrEditAttributeDialog
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    // pl.fovis.client.dialogs.TreeNodeDialog
    // pl.fovis.client.bikpages.TreeBikPage
    public static final I18nMessage dodaj = new I18nMessagePlEn("Dodaj", "Add") /* I18N:  */; // #112
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.dialogs.AddNewsDialog
    // pl.fovis.client.dialogs.AddOrEditNewsDialog
    // pl.fovis.client.bikpages.MyBIKSNewsTab
    public static final I18nMessage dodajAktualnosc = new I18nMessagePlEn("Dodaj komunikat", "Add announcement") /* I18N:  */; // #113
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.AddOrEditQuestionAndAnswer
    // pl.fovis.client.nodeactions.NodeActionAddQuestionAndAnswer
    public static final I18nMessage dodajArtykul = new I18nMessagePlEn("Dodaj artykuł", "Add article") /* I18N:  */; // #114
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.bikwidgets.CategorizedSystemAttrDictWidget
    // pl.fovis.client.bikwidgets.CategorizedAddAttrDictWidget
    // pl.fovis.client.entitydetailswidgets.AttributeWidget
    public static final I18nMessage dodajAtrybut = new I18nMessagePlEn("Dodaj atrybut", "Add attribute") /* I18N:  */; // #115
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AuthorWidgetFlat
    public static final I18nMessage dodajAutora = new I18nMessagePlEn("Dodaj autora", "Add author") /* I18N:  */; // #116
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionAddDefinition
    public static final I18nMessage dodajDefinicje = new I18nMessagePlEn("Dodaj definicję", "Add definition") /* I18N:  */; // #117
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.EntityInFvsWidget
    public static final I18nMessage dodajDoUlubionych = new I18nMessagePlEn("Dodaj do ulubionych", "Add to favourites") /* I18N:  */; // #118
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.FileUploadDialogForDocuments
    // pl.fovis.client.nodeactions.NodeActionAddDocument
    public static final I18nMessage dodajDokument = new I18nMessagePlEn("Dodaj załącznik", "Add attachment") /* I18N:  */; // #119
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AttachmentsWidgetFlat
    public static final I18nMessage dodajDokumentacje = new I18nMessagePlEn("Dodaj dokumentację", "Add attachment") /* I18N:  */; // #120
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionLinkNode
    public static final I18nMessage dodajDowiazanie = new I18nMessagePlEn("Dodaj dowiązanie", "Add link") /* I18N:  */; // #121
    public static final I18nMessage addAnyNode = new I18nMessagePlEn("Dodaj ...", "Add ...");
    public static final I18nMessage addEvent = new I18nMessagePlEn("Dodaj zdarzenie", "Add new event");
    public static final I18nMessage editEventRow = new I18nMessagePlEn("Edytuj zdarzenie", "Edit event");
    public static final I18nMessage dodanoNoweZdarzenie = new I18nMessagePlEn("Dodano nowe zdarzenie", "Event added") /* I18N:  */; // #156
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.DataDicionaryPage
    public static final I18nMessage dodajDrzewo = new I18nMessagePlEn("Dodaj drzewo", "Add tree") /* I18N:  */; // #122
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage dodajDziedzicPowiazanDlaObiektow = new I18nMessagePlEn("Dodaj dziedziczone powiązanie dla obiektów bezpośrednio podrzędnych", "Add inheritable association for directly descendant objects") /* I18N:  */; // #123
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionAddBranch
    public static final I18nMessage dodajGalaz = new I18nMessagePlEn("Dodaj", "Add") /* I18N:  */; // #124
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedDictionaryWidgetBase
    public static final I18nMessage dodajKategorie = new I18nMessagePlEn("Dodaj kategorie", "Add category") /* I18N:  */; // #125
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.lisa.NodeActionAddLisaCategory
    public static final I18nMessage dodajKategorie2 = new I18nMessagePlEn("Dodaj kategorię", "Add category") /* I18N:  */; // #126
    public static final I18nMessage usunKategorie = new I18nMessagePlEn("Usuń kategorię", "Delete category");
    public static final I18nMessage przeniesKategorie = new I18nMessagePlEn("Przenieś kategorię", "Move category");
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.AddNoteDialog
    // pl.fovis.client.entitydetailswidgets.NoteWidgetFlat
    public static final I18nMessage dodajKomentarz = new I18nMessagePlEn("Dodaj komentarz", "Add comment") /* I18N:  */; // #127
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionAddLeaf
    public static final I18nMessage dodajLisc = new I18nMessagePlEn("Dodaj", "Add") /* I18N:  */; // #128
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage dodajNiedziedPowiazanDlaObiektow = new I18nMessagePlEn("Dodaj niedziedziczone powiązanie dla obiektów bezpośrednio podrzędnych", "Add noninheritable association for directly descendant objects") /* I18N:  */; // #129
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedRoleForNodeDictWidget
    public static final I18nMessage dodajNowaRole = new I18nMessagePlEn("Dodaj nową rolę", "Add new role") /* I18N:  */; // #130
    // is translated
    // multiple occurrences: 5
    // pl.fovis.client.entitydetailswidgets.JoinedObjWidgetFlatBase
    // pl.fovis.client.nodeactions.NodeActionLinkUserToRole
    // pl.fovis.client.entitydetailswidgets.SpecialistWidget
    // pl.fovis.client.entitydetailswidgets.UserInNodeWidgetFlatBase
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage dodajPowiazanie = new I18nMessagePlEn("Dodaj powiązanie", "Add association") /* I18N:  */; // #131
    public static final I18nMessage dodajPowiazanieZFiltrowanie = new I18nMessagePlEn("Dodaj powiązanie z filtrowaniem", "Add association") /* I18N:  */; // #131
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionAddRole
    public static final I18nMessage dodajRoleUzytkownika = new I18nMessagePlEn("Dodaj rolę użytkownika", "Add user role") /* I18N:  */; // #132
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage dodajSzablonDrzew = new I18nMessagePlEn("Dodaj szablon drzew", "Add tree template") /* I18N:  */; // #133
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage dodajTutaj = new I18nMessagePlEn("Dodaj tutaj", "Add here") /* I18N:  */; // #134
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.nodeactions.NodeActionAddUser
    // pl.fovis.client.dialogs.SystemUserDialog
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage dodajUzytkownika = new I18nMessagePlEn("Dodaj użytkownika", "Add user") /* I18N:  */; // #135
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionAddVersionDefinition
    public static final I18nMessage dodajWersjeDefinicji = new I18nMessagePlEn("Dodaj wersję definicji", "Add version of definition") /* I18N:  */; // #136
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionAddBlogEntry
    public static final I18nMessage dodajWpis = new I18nMessagePlEn("Dodaj wpis", "Add blog entry") /* I18N:  */; // #137
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.BlogsWidget
    public static final I18nMessage dodajWpisBlogowy = new I18nMessagePlEn("Dodaj wpis blogowy", "Add blog entry") /* I18N:  */; // #138
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeNodeDialog
    public static final I18nMessage dodajeszElementPod = new I18nMessagePlEn("Dodajesz", "Add") /* I18N:  */; // #139
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeNodeDialog
    public static final I18nMessage dodajeszNowyElementGlowny = new I18nMessagePlEn("Dodajesz nowy element główny", "Add new root item") /* I18N:  */; // #140
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage dodajeszPowiazanieDla = new I18nMessagePlEn("Dodajesz powiązanie dla", "Add associations for") /* I18N:  */; // #141
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSNewsTab
    public static final I18nMessage dodanoAktualnosc = new I18nMessagePlEn("Dodano komunikat", "Announcement added") /* I18N:  */; // #142
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionAddQuestionAndAnswer
    public static final I18nMessage dodanoArtykul = new I18nMessagePlEn("Dodano artykuł", "Article added") /* I18N:  */; // #143
    // is translated
    // single occurrence
    // pl.fovis.client.EDPForms
    public static final I18nMessage dodanoAtrybut = new I18nMessagePlEn("Dodano atrybut", "Attribute added") /* I18N:  */; // #144
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AuthorWidgetFlat
    public static final I18nMessage dodanoAutora = new I18nMessagePlEn("Dodano autora", "Author added") /* I18N:  */; // #145
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.EntityInFvsWidget
    public static final I18nMessage dodanoDo = new I18nMessagePlEn("dodano do", "added to") /* I18N: favs */; // #146
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.nodeactions.NodeActionAddDocument
    // pl.fovis.client.dialogs.TreeSelectorForAttachments
    public static final I18nMessage dodanoDokument = new I18nMessagePlEn("Dodano dokument", "Document added") /* I18N:  */; // #147
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionLinkNode
    public static final I18nMessage dodanoDowiazanie = new I18nMessagePlEn("Dodano dowiązanie", "Link added") /* I18N:  */; // #148
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.DataDicionaryPage
    public static final I18nMessage dodanoDrzewo = new I18nMessagePlEn("Dodano drzewo", "Tree added") /* I18N:  */; // #149
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedDictionaryWidgetBase
    public static final I18nMessage dodanoKategorie = new I18nMessagePlEn("Dodano kategorie", "Category added") /* I18N:  */; // #150
    // is translated
    // single occurrence
    // pl.fovis.client.EDPForms
    public static final I18nMessage dodanoKomentarz = new I18nMessagePlEn("Dodano komentarz", "Comment added") /* I18N:  */; // #151
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionAddDefinition
    public static final I18nMessage dodanoNowaDefinicje = new I18nMessagePlEn("Dodano nową definicję", "Definition added") /* I18N:  */; // #152
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedRoleForNodeDictWidget
    public static final I18nMessage dodanoNowaRole = new I18nMessagePlEn("Dodano nową rolę", "Role added") /* I18N:  */; // #153
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionAddVersionDefinition
    public static final I18nMessage dodanoNowaWersjeDefinicji = new I18nMessagePlEn("Dodano nową wersję definicji", "New version of definition added") /* I18N:  */; // #154
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedAddAttrDictWidget
    public static final I18nMessage dodanoNowyAtrybut = new I18nMessagePlEn("Dodano nowy atrybut", "Attribute added") /* I18N:  */; // #155
    // is translated
    // multiple occurrences: 4
    // pl.fovis.client.nodeactions.NodeActionAddRole
    // pl.fovis.client.nodeactions.NodeActionAddBranch
    // pl.fovis.client.nodeactions.NodeActionAddLeaf
    // pl.fovis.client.nodeactions.NodeActionNewNestableParent
    public static final I18nMessage dodanoNowyElement = new I18nMessagePlEn("Dodano nowy element", "Item added") /* I18N:  */; // #156
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.nodeactions.NodeActionLinkUserToRoleForBlogs
    // pl.fovis.client.entitydetailswidgets.SpecialistWidget
    public static final I18nMessage dodanoPowiazanie = new I18nMessagePlEn("Dodano powiązanie", "Association added") /* I18N:  */; // #157
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage dodanoTypDrzewa = new I18nMessagePlEn("Dodano typ drzewa", "Tree type added") /* I18N:  */; // #158
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.UserDialog
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage dodanoUzytkownika = new I18nMessagePlEn("Dodano użytkownika", "User added") /* I18N:  */; // #159
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionAddBlogEntry
    public static final I18nMessage dodanoWpis = new I18nMessagePlEn("Dodano wpis", "entry added") /* I18N:  */; // #160
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AttachmentsWidgetFlat
    public static final I18nMessage dodanoZalacznik = new I18nMessagePlEn("Dodano załącznik", "Attachment added") /* I18N:  */; // #161
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    public static final I18nMessage dodanoZdjecie = new I18nMessagePlEn("Dodano zdjęcie", "Photo added") /* I18N:  */; // #162
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage dodanychWpisow = new I18nMessagePlEn("Dodanych wpisów", "Added entries") /* I18N:  */; // #163
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SAPBOExtradataWidget
    public static final I18nMessage dodatkoweID = new I18nMessagePlEn("Dodatkowe ID", "Additional IDs") /* I18N:  */; // #164
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.MetapediaDialog
    public static final I18nMessage dodawaniePojeciaBiznesowego = new I18nMessagePlEn("Dodawanie pojęcia biznesowego", "Add business term") /* I18N:  */; // #165
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage dokumentacja = new I18nMessagePlEn("Dokumentacja", "Documentation") /* I18N:  */; // #166
    // is translated
    // single occurrence
    // pl.fovis.client.BIKClientSingletons
    public static final I18nMessage dokumentacja_wtf = new I18nMessagePlEn("Dokumentacja", "Documentation") /* I18N: wtf? */; // #167
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage domyslne = new I18nMessagePlEn("Domyślne", "Default") /* I18N:  */; // #168
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage drzewo = new I18nMessagePlEn("Drzewo", "Tree") /* I18N:  */; // #169
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage dzien = new I18nMessagePlEn("dzień", "day") /* I18N:  */; // #170
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.EditTutorialDialog
    public static final I18nMessage editTutorial = new I18nMessagePlEn("Edytuj samouczek", "Edit tutorial") /* I18N:  */; // #171
    // is translated
    // multiple occurrences: 4
    // pl.fovis.client.dialogs.EditHelpDescriptionDialog
    // pl.fovis.client.dialogs.AttrOrRoleEditDialog
    // pl.fovis.client.dialogs.lisa.LisaBeanDisplay
    // pl.fovis.client.bikpages.SystemUserBeanExtractor
    public static final I18nMessage edycja = new I18nMessagePlEn("Edycja", "Edit") /* I18N:  */; // #172
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    public static final I18nMessage edycjaDanych = new I18nMessagePlEn("Mój Profil", "My profile") /* I18N:  */; // #173
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.LisaDictionaryColumnEditionDialog
    public static final I18nMessage edycjaKolumnySlownika = new I18nMessagePlEn("Edycja kolumny słownika", "Edit dictionary column") /* I18N:  */; // #174
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.EditEntityKeywordsDialog
    public static final I18nMessage edycjaSlowKluczowych = new I18nMessagePlEn("Edycja słów kluczowych", "Edit keywords") /* I18N:  */; // #175
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.LisaDictDialog
    public static final I18nMessage edycjaSlownika = new I18nMessagePlEn("Edycja słownika", "Edit dictionary") /* I18N:  */; // #176
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.ColumnAvailabilityText
    public static final I18nMessage edytowalna = new I18nMessagePlEn("edytowalna", "editable") /* I18N:  */; // #177
    // is translated
    // single occurrence
    // pl.fovis.client.EDPForms
    public static final I18nMessage edytowanoAtrybut = new I18nMessagePlEn("Edytowano atrybut", "Attribute edited") /* I18N:  */; // #178

    public static final I18nMessage attrRowEdited = new I18nMessagePlEn("Edytowano atrybuty zdarzenia", "Attribute edited") /* I18N:  */; // #178
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedDictionaryWidgetBase
    public static final I18nMessage edytowanoKategorie = new I18nMessagePlEn("Edytowano kategorie", "Category edited") /* I18N:  */; // #179
    // is translated
    // single occurrence
    // pl.fovis.client.EDPForms
    public static final I18nMessage edytowanoKomentarz = new I18nMessagePlEn("Edytowano komentarz", "Comment edited") /* I18N:  */; // #180
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorisedAttrWidgetBase
    public static final I18nMessage edytowanoOpis = new I18nMessagePlEn("Edytowano opis", "Description edited") /* I18N:  */; // #181
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionEditBlogEntry
    public static final I18nMessage edytowanoWpis = new I18nMessagePlEn("Edytowano wpis", "Blog entry edited") /* I18N:  */; // #182
    // is translated
    // multiple occurrences: 15
    // pl.fovis.client.nodeactions.NodeActionEditUser
    // pl.fovis.client.dialogs.HelpDialog
    // pl.fovis.client.bikpages.NewsActionExtractor
    // pl.fovis.client.dialogs.AddOrEditAttributeDialog
    // pl.fovis.client.dialogs.ViewTutorialInFullScreenDialog
    // pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase
    // pl.fovis.client.bikwidgets.CategorisedAttrWidgetBase
    // pl.fovis.client.entitydetailswidgets.GenericBeanExtractorBase
    // pl.fovis.client.entitydetailswidgets.lisa.LisaDictGridDisplay
    // pl.fovis.client.entitydetailswidgets.NoteWidgetFlat
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    // pl.fovis.client.entitydetailswidgets.AttributeWidget
    // pl.fovis.client.bikwidgets.CategorizedDictionaryWidgetBase
    // pl.fovis.client.bikpages.HelpPageTree
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage edytuj = new I18nMessagePlEn("Zmień", "Change") /* I18N:  */; // #183
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditNewsDialog
    public static final I18nMessage edytujAktualnosc = new I18nMessagePlEn("Edytuj komunikat", "Edit announcement") /* I18N:  */; // #184
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedAddAttrDictWidget
    public static final I18nMessage edytujAtrybut = new I18nMessagePlEn("Zmień", "Change") /* I18N:  */; // #185
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage edytujDane = new I18nMessagePlEn("Edytuj dane", "Edit data") /* I18N:  */; // #186
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionEditDefinition
    public static final I18nMessage edytujDefinicje = new I18nMessagePlEn("Edytuj definicję", "Edit definition") /* I18N:  */; // #187
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.EditDescriptionDialog
    public static final I18nMessage edytujElement = new I18nMessagePlEn("Edytuj element", "Edit item") /* I18N:  */; // #188
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedDictionaryWidgetBase
    public static final I18nMessage edytujKategorie = new I18nMessagePlEn("Edytuj kategorie", "Edit category") /* I18N:  */; // #189
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.nodeactions.NodeActionEditDescription
    // pl.fovis.client.dialogs.EditAttrHintDialog
    public static final I18nMessage edytujOpis = new I18nMessagePlEn("Zmień opis", "Change description") /* I18N:  */; // #190
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage edytujSzablonDrzewa = new I18nMessagePlEn("Edytuj szablon drzewa", "Edit tree template") /* I18N:  */; // #191
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionEditBlogEntry
    public static final I18nMessage edytujWpisBlogowy = new I18nMessagePlEn("Edytuj wpis blogowy", "Edit blog entry") /* I18N:  */; // #192
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage edytujZaznaczonySzablonDrzew = new I18nMessagePlEn("Edytuj zaznaczony szablon drzew", "Edit selected tree template") /* I18N:  */; // #193
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorForJoinedObjectsDialogNew
    public static final I18nMessage edytujesPowiazanDlaUzytkown = new I18nMessagePlEn("Edytujesz powiązania dla użytkownika", "Edit associations for user") /* I18N:  */; // #194
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeNodeDialog
    public static final I18nMessage edytujeszNazweElementu = new I18nMessagePlEn("Edytujesz nazwę elementu", "Edit item name") /* I18N:  */; // #195
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage edytujeszPowiazaniaDla = new I18nMessagePlEn("Edytujesz powiązania dla", "Edit associations for") /* I18N:  */; // #196
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSHomePageTab
    public static final I18nMessage ekspert = new I18nMessagePlEn("Ekspert", "Expert") /* I18N:  */; // #197
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.TreeBikPage
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage fILTRUJ = new I18nMessagePlEn("FILTRUJ drzewo", "FILTER tree") /* I18N:  */; // #198
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.ViewSlideShowlInFullScreenDialog
    // pl.fovis.client.dialogs.ViewTutorialInFullScreenDialog
    public static final I18nMessage film = new I18nMessagePlEn("Film", "Video") /* I18N:  */; // #199
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage filtrowaWynikowWgTypowObiektow = new I18nMessagePlEn("Filtrowanie wyników wg typów obiektów", "Filter by object types") /* I18N:  */; // #200
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.BaseBIKTreeWidget
    public static final I18nMessage filtrowanieZakonczone = new I18nMessagePlEn("Filtrowanie zakończone", "Finished filtering") /* I18N:  */; // #201
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage filtruj = new I18nMessagePlEn("Filtruj", "Filter") /* I18N:  */; // #202
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage glosariusz = new I18nMessagePlEn("Glosariusz", "Glossary") /* I18N:  */; // #203
    // is translated
    // single occurrence
    // pl.fovis.client.BIKClientSingletons
    public static final I18nMessage glosariusz_wtf = new I18nMessagePlEn("Glosariusz", "Glossary") /* I18N: wtf? */; // #204
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.TreeBeanExtractor
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage glowneMenu = new I18nMessagePlEn("główne menu", "main menu") /* I18N:  */; // #205
    // is translated
    // multiple occurrences: 4
    // pl.fovis.common.JoinTypeRoles
    // pl.fovis.client.entitydetailswidgets.UserBlogBeanExtractor
    // pl.fovis.client.entitydetailswidgets.UserRoleInNodeBeanExtractor
    // pl.fovis.client.entitydetailswidgets.UserInNodeBeanExtractor
    public static final I18nMessage glowny = new I18nMessagePlEn("Główny", "Main") /* I18N:  */; // #206
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.JoinTypeRolesWidget
    public static final I18nMessage glownyAAktualniUstawionNaGlowneZ = new I18nMessagePlEn("Główny, a aktualnie ustawione na główne z tą rolą i w", "Main and currently set as main with this role in") /* I18N: wtf? */; // #207
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage godzinI = new I18nMessagePlEn("godzin i", "hours and") /* I18N:  */; // #208
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminSchedulePage
    public static final I18nMessage godzina = new I18nMessagePlEn("Godzina", "Hour") /* I18N:  */; // #209
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage grupach = new I18nMessagePlEn("grupach", "groups") /* I18N:  */; // #210
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.TreeBikPage
    public static final I18nMessage grupeUzytkownikow = new I18nMessagePlEn("grupę użytkowników", "users group") /* I18N:  */; // #211
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminSchedulePage
    public static final I18nMessage harmonogram = new I18nMessagePlEn("Harmonogram", "Schedule") /* I18N:  */; // #212
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage haslaNieSaTakieSame = new I18nMessagePlEn("Hasła nie są takie same", "Passwords are not identical") /* I18N:  */; // #213
    // is translated
    // multiple occurrences: 4
    // pl.fovis.client.dialogs.lisa.LisaAuthorizationDialog
    // pl.fovis.client.dialogs.LoginDialog
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage haslo = new I18nMessagePlEn("Hasło", "Password") /* I18N:  */; // #214
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage hasloNieMozeBycPuste = new I18nMessagePlEn("Hasło nie może być puste", "Password cannot be empty") /* I18N:  */; // #215
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.MyBIKSHistoryTab
    // pl.fovis.client.bikpages.MyBIKSHistoryTabNew
    public static final I18nMessage historia = new I18nMessagePlEn("Historia", "History") /* I18N:  */; // #216
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.JoinTypeRolesWidget
    public static final I18nMessage iObiektachPodrzednych = new I18nMessagePlEn("i obiektach podrzędnych", "and descendant objects") /* I18N:  */; // #217
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage ikonaGalezi = new I18nMessagePlEn("Ikona gałęzi", "Branch icon") /* I18N:  */; // #218
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage ikonaLiscia = new I18nMessagePlEn("Ikona liścia", "Leaf icon") /* I18N:  */; // #219
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage indeksWyszukiwarkiJestAktualnie = new I18nMessagePlEn("indeks wyszukiwarki jest aktualnie", "search index is currently") /* I18N:  */; // #220
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage indexy = new I18nMessagePlEn("Indeksy", "Indexes") /* I18N:  */; // #221
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.EntityDetailsPaneDialog
    public static final I18nMessage informacjeSzczegolowe = new I18nMessagePlEn("Informacje szczegółowe", "Detailed information") /* I18N:  */; // #222
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.MetapediaDialog
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage inna = new I18nMessagePlEn("inna", "other") /* I18N:  */; // #223
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.HelpPageTree
    public static final I18nMessage inne = new I18nMessagePlEn("Inne", "Other") /* I18N:  */; // #224
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage innych = new I18nMessagePlEn("innych", "others") /* I18N:  */; // #225
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.MetapediaDialog
    public static final I18nMessage istniejaca = new I18nMessagePlEn("istniejącą", "existing") /* I18N:  */; // #226
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AdminConfigDBBaseWidget
    public static final I18nMessage istniejeJuzTakaNazwaSerwera = new I18nMessagePlEn("Istnieje już taka nazwa serwera", "Such server name already exists") /* I18N:  */; // #227
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.MyBIKSFavouriteGrid
    public static final I18nMessage jakoPrzeczytane = new I18nMessagePlEn("jako przeczytane", "as read") /* I18N:  */; // #228
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.MyBIKSFavouriteGrid
    public static final I18nMessage jakoPrzeczytany = new I18nMessagePlEn("jako przeczytany", "as read") /* I18N:  */; // #203
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage jakoscDanych = new I18nMessagePlEn("Jakość danych", "Data quality") /* I18N:  */; // #230
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage jednorazowo = new I18nMessagePlEn("Jednorazowo", "Once") /* I18N:  */; // #231
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKBikPage
    public static final I18nMessage jestesZalogowanyJako = new I18nMessagePlEn("Jesteś zalogowany jako", "You are logged in as") /* I18N:  */; // #232
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.HelpPageTree
    public static final I18nMessage jezyk = new I18nMessagePlEn("Język", "Language") /* I18N:  */; // #233
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.UserDialog
    public static final I18nMessage juzIstnieje = new I18nMessagePlEn("już istnieje", "already exists") /* I18N:  */; // #234
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.MetapediaDialog
    public static final I18nMessage juzIstniejeCzyZastapic = new I18nMessagePlEn("już istnieje. Czy zastąpić", "already exists. Are you sure to replace it") /* I18N:  */; // #235
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.nodeactions.lisa.NodeActionAddLisaCategory
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage kategoria = new I18nMessagePlEn("Kategoria", "Category") /* I18N:  */; // #236
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage kategoryzacje = new I18nMessagePlEn("Kategoryzacje", "Categorizations") /* I18N:  */; // #237
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.LoginDialog
    public static final I18nMessage klikajacTUTAJ = new I18nMessagePlEn("Klikając TUTAJ", "click HERE") /* I18N:  */; // #238
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.EntityVoteValsWidget
    public static final I18nMessage kliknijAbyUsunac = new I18nMessagePlEn("Kliknij aby usunąć", "Click to delete") /* I18N:  */; // #239
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage kliknijAbyZmienicIkoneDlaGalezi = new I18nMessagePlEn("Kliknij, aby zmienić ikonę dla gałęzi", "Click to change branch icon") /* I18N:  */; // #240
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage kliknijAbyZmienicIkoneDlaLiscia = new I18nMessagePlEn("Kliknij, aby zmienić ikonę dla liścia", "Click to change leaf icon") /* I18N:  */; // #241
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.lisa.LisaDictionaryColumnEditionDialog
    // pl.fovis.client.dialogs.lisa.LisaBeanDisplay
    public static final I18nMessage kluczGlowny = new I18nMessagePlEn("Klucz główny", "Primary key") /* I18N:  */; // #242
    public static final I18nMessage latinKolumna = new I18nMessagePlEn("LATIN kolumna", "LATIN column") /* I18N:  */; // #242
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.nodeactions.NodeActionResetVisualOrder
    // pl.fovis.client.nodeactions.NodeActionMoveInTreeBase
    public static final I18nMessage kolejnoscZmieniona = new I18nMessagePlEn("Kolejność zmieniona", "Order changed") /* I18N:  */; // #243
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.BIKGWTConstants
    // pl.fovis.client.entitydetailswidgets.IndexWidgetFlat
    public static final I18nMessage kolumny = new I18nMessagePlEn("Kolumny", "Columns") /* I18N:  */; // #244
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage komentarze = new I18nMessagePlEn("Komentarze", "Comments") /* I18N:  */; // #245
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage komentarzeUzytkownika = new I18nMessagePlEn("Komentarze użytkownika", "User comments") /* I18N:  */; // #246
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.DataPumpPage
    public static final I18nMessage konfiguracjaPolaczen = new I18nMessagePlEn("Konfiguracja połączeń", "Connections config") /* I18N:  */; // #247
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.nodeactions.NodeActionCopy
    // pl.fovis.client.dialogs.TreeSelectorForUsersToCopyRole
    public static final I18nMessage kopiuj = new I18nMessagePlEn("Kopiuj", "Copy") /* I18N:  */; // #248
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionCopyJojnedObj
    public static final I18nMessage kopiujPowiazania = new I18nMessagePlEn("Kopiuj powiązania", "Copy associations") /* I18N:  */; // #249
    public static final I18nMessage powiazaniaZWyzszegoPoziomu = new I18nMessagePlEn("Połącz z", "Add associations") /* I18N:  */; // #249
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.MetapediaDialog
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage korporacyjna = new I18nMessagePlEn("korporacyjna", "corporate") /* I18N:  */; // #250
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage krotkiTekst = new I18nMessagePlEn("Krótki tekst", "Short text") /* I18N:  */; // #251
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage lacznie = new I18nMessagePlEn("łącznie", "total") /* I18N:  */; // #252
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.UserRankPageGrid
    public static final I18nMessage liczbaDodanychWpisow = new I18nMessagePlEn("Liczba dodanych wpisów", "Number of entries added") /* I18N:  */; // #253
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage linkWewnetrzny = new I18nMessagePlEn("Link wewnętrzny wielokrotnego wyboru", "Internal link multi") /* I18N:  */; // #254
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage linkZewnetrzny = new I18nMessagePlEn("Link zewnętrzny", "External link") /* I18N:  */; // #255
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.lisa.LisaMetadataPage
    public static final I18nMessage listaSlownikow = new I18nMessagePlEn("Lista słowników", "Dictionary list") /* I18N:  */; // #256
    public static final I18nMessage listaSlownikowNieJestPusta = new I18nMessagePlEn("Lista słowników nie jest pusta", "Dictionary list is not empty") /* I18N:  */; // #256
    public static final I18nMessage listaInstancji = new I18nMessagePlEn("Lista instancji", "Instances list") /* I18N:  */; // #256
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.StatisticsLoggedUserPage
    public static final I18nMessage listaUzytkownikow = new I18nMessagePlEn("Lista użytkowników", "List of users") /* I18N:  */; // #257
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminLogsPage
    public static final I18nMessage logi = new I18nMessagePlEn("Logi", "Logs") /* I18N:  */; // #258
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminLogsPage
    public static final I18nMessage logiZOstatnichZasilen = new I18nMessagePlEn("Logi z ostatnich zasileń", "Logs of last loads") /* I18N:  */; // #259
    // is translated
    // multiple occurrences: 5
    // pl.fovis.client.dialogs.lisa.LisaAuthorizationDialog
    // pl.fovis.client.dialogs.LoginDialog
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    // pl.fovis.client.bikpages.SystemUserBeanExtractor
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage login = new I18nMessagePlEn("Login", "Login") /* I18N:  */; // #260
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.UserDialog
    public static final I18nMessage loginZAD = new I18nMessagePlEn("Login z AD", "AD login") /* I18N:  */; // #261
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.LoginDialog
    public static final I18nMessage logowanie = new I18nMessagePlEn("Logowanie", "Logon") /* I18N:  */; // #262
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.LisaAuthorizationDialog
    public static final I18nMessage logowanieDoTeradaty = new I18nMessagePlEn("Logowanie do Teradaty", "Teradata logon") /* I18N:  */; // #263
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage lokalizacjaPliku = new I18nMessagePlEn("Lokalizacja pliku", "File location") /* I18N: wtf? */; // #264
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.TreeBeanExtractor
    public static final I18nMessage miejsceWMenu = new I18nMessagePlEn("Miejsce w menu", "Position in menu") /* I18N:  */; // #265
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage mies = new I18nMessagePlEn("mies", "months") /* I18N:  */; // #266
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage minut = new I18nMessagePlEn("minut", "minutes") /* I18N:  */; // #267
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage mojBIKS = new I18nMessagePlEn("Mój BIKS", "My BIKS") /* I18N:  */; // #268
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSNotesTab
    public static final I18nMessage mojeKomentarze = new I18nMessagePlEn("Moje komentarze", "My comments") /* I18N:  */; // #269
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSVoteTab
    public static final I18nMessage mojeOceny = new I18nMessagePlEn("Moje oceny", "My votes") /* I18N:  */; // #270
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKBikPage
    public static final I18nMessage musiszSieZalogowac = new I18nMessagePlEn("Musisz się zalogować", "Logon required") /* I18N:  */; // #271
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.lisa.LisaDictGridDisplay
    public static final I18nMessage nD = new I18nMessagePlEn("n/d", "n/d") /* I18N: wtf? */; // #272
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.TreeBikPage
    public static final I18nMessage naNajwyzszymPoziomie = new I18nMessagePlEn("na najwyższym poziomie", "at root level") /* I18N:  */; // #273
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage naPozycji = new I18nMessagePlEn("na pozycji", "at position") /* I18N:  */; // #274
    // is translated
    // single occurrence
    // pl.fovis.client.bikcenterEntryPoint
    public static final I18nMessage naSerwerzeZainstalJestInnaWersja = new I18nMessagePlEn("Na serwerze zainstalowana jest inna wersja BIKS, musisz odświeżyć aplikację w przeglądarce", "Server has different version installed, must refresh application in browser") /* I18N:  */; // #275
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.StatisticsPage
    public static final I18nMessage najczesciejUzywanyParametr = new I18nMessagePlEn("Najczęściej używany parametr", "Most used parameter") /* I18N:  */; // #276
    // is translated
    // single occurrence
    // pl.fovis.client.bikcenterEntryPoint
    public static final I18nMessage najprawdWymaganyJestSVNUPDATEIUr = new I18nMessagePlEn("Skontaktuj się z administratorem aplikacji", "Please contact aplication administrator") /* I18N:  */; // #277
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.entitydetailswidgets.EntityDetailsTabPaginationBase
    // pl.fovis.client.bikwidgets.WidgetWithPagination
    public static final I18nMessage nastepnaStrona = new I18nMessagePlEn("Następna strona", "Next page") /* I18N:  */; // #278
    // is translated
    // multiple occurrences: 18
    // pl.fovis.client.dialogs.EditAttrHintDialog
    // pl.fovis.client.dialogs.EditDescriptionDialog
    // pl.fovis.client.bikpages.StatisticsPage
    // pl.fovis.client.dialogs.PickUserDialogBase
    // pl.fovis.client.entitydetailswidgets.DescendantNodesSubTreeWidgetFlat
    // pl.fovis.client.bikpages.ObjectInHistoryBeanExtractor
    // pl.fovis.client.bikpages.HelpHintExtractor
    // pl.fovis.client.dialogs.UserDialog
    // pl.fovis.client.dialogs.lisa.ChangeCategoryDialog
    // pl.fovis.client.bikpages.AttrHintExtractor
    // pl.fovis.client.bikpages.TreeBikPage
    // pl.fovis.client.bikwidgets.TreeAndNodeKindTreeBase
    // pl.fovis.client.dialogs.TreeNodeDialog
    // pl.fovis.client.entitydetailswidgets.IndexWidgetFlat
    // pl.fovis.client.bikwidgets.MyBIKSHistoryGrid
    // pl.fovis.client.dialogs.SystemUserDialog
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage nazwa = new I18nMessagePlEn("Nazwa", "Name") /* I18N:  */; // #279
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.AddOrUpdateAttributeAndTheirKinds
    // pl.fovis.client.dialogs.AttrOrRoleEditDialog
    public static final I18nMessage nazwaAtrybutu = new I18nMessagePlEn("Nazwa atrybutu", "Attribute name") /* I18N:  */; // #280
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.TreeBeanExtractor
    public static final I18nMessage nazwaDrzewa = new I18nMessagePlEn("Nazwa drzewa", "Tree name") /* I18N:  */; // #281
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage nazwaDrzewaNieMozeBycPusta = new I18nMessagePlEn("Nazwa drzewa nie może być pusta", "Tree name cannot be empty") /* I18N:  */; // #282
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage nazwaGalezi = new I18nMessagePlEn("Nazwa gałęzi", "Branch name") /* I18N:  */; // #283
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.lisa.LisaBeanDisplay
    // pl.fovis.client.dialogs.lisa.LisaDictionaryColumnEditionDialog
    public static final I18nMessage nazwaKolumny = new I18nMessagePlEn("Nazwa kolumny", "Column name") /* I18N:  */; // #284
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage nazwaLiscia = new I18nMessagePlEn("Nazwa liścia", "Leaf name") /* I18N:  */; // #285
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage nazwaLisciaIGaleziNieMozeBycTaka = new I18nMessagePlEn("Nazwa liścia i gałęzi nie może być taka sama", "Names of leaf and branch cannot be the same") /* I18N:  */; // #286
    // is translated
    // multiple occurrences: 4
    // pl.fovis.client.dialogs.AddOrUpdateItemNameDialog
    // pl.fovis.client.dialogs.AttrOrRoleEditDialog
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage nazwaNieMozeBycPusta = new I18nMessagePlEn("Nazwa nie może być pusta", "Name cannot be empty") /* I18N:  */; // #287
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage nazwaObiektu = new I18nMessagePlEn("Nazwa", "Name") /* I18N:  */; // #288
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminSchedulePage
    public static final I18nMessage nazwaSystemu = new I18nMessagePlEn("Nazwa systemu", "Name of system") /* I18N:  */; // #289
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage nazwaTypuDrzewa = new I18nMessagePlEn("Nazwa typu drzewa", "Name of tree type") /* I18N:  */; // #290
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.lisa.LisaBeanDisplay
    // pl.fovis.client.dialogs.lisa.LisaDictionaryColumnEditionDialog
    public static final I18nMessage nazwaWyswietlana = new I18nMessagePlEn("Nazwa wyświetlana", "Display name") /* I18N:  */; // #291
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.SystemUserBeanExtractor
    public static final I18nMessage nie = new I18nMessagePlEn("Nie", "No") /* I18N:  */; // #292
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.LisaBeanDisplay
    public static final I18nMessage nie2 = new I18nMessagePlEn("nie", "no") /* I18N:  */; // #293
    public static final I18nMessage nie3 = new I18nMessagePlEn("Nie", "Not") /* I18N:  */; // #293
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage nieMaszUprawnieDoWyswietlTegoRap = new I18nMessagePlEn("Nie masz uprawnień do wyświetlenia tego raportu", "Insufficient privileges to display this report") /* I18N:  */; // #294
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage nieMoznaPowiazacWezlaZNimSamym = new I18nMessagePlEn("Nie można powiązać węzła z nim samym", "Node cannot be associated with itself") /* I18N:  */; // #295
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage nieMoznaUruchomicZasilania = new I18nMessagePlEn("Nie można uruchomić zasilania", "Cannot start data load process") /* I18N:  */; // #296
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage nieMoznaWybracSamegoSiebie = new I18nMessagePlEn("Nie można wybrać samego siebie", "Cannot select the same node") /* I18N: wtf? */; // #297
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage niePokazujWMenu = new I18nMessagePlEn("Nie pokazuj w menu", "Do not show in menu") /* I18N:  */; // #298
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorForAttachments
    public static final I18nMessage nieZnalezionoDokumentu = new I18nMessagePlEn("Nie znaleziono dokumentu", "Document not found") /* I18N:  */; // #299
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAttributeDialog
    public static final I18nMessage niepoprawnyAdresStrony = new I18nMessagePlEn("Niepoprawny adres strony", "Invalid page address") /* I18N:  */; // #300
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.VoteBeanExtractor
    // pl.fovis.client.entitydetailswidgets.EntityVoteValsWidget
    public static final I18nMessage nieuzyteczne = new I18nMessagePlEn("nieużyteczne", "unuseful") /* I18N:  */; // #301
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.MetapediaDialog
    public static final I18nMessage nowa = new I18nMessagePlEn("nową", "new") /* I18N:  */; // #302
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.MyBIKSFavouriteGrid
    public static final I18nMessage nowe = new I18nMessagePlEn("Nowe", "New") /* I18N:  */; // #303
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.bikwidgets.MyBIKSNewsGrid
    // pl.fovis.client.bikwidgets.MyBIKSFavouriteGrid
    // pl.fovis.client.bikpages.MyBIKBikPage
    public static final I18nMessage nowy = new I18nMessagePlEn("Nowy", "New") /* I18N:  */; // #304
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.EntityInFvsWidget
    public static final I18nMessage obiekt = new I18nMessagePlEn("Obiekt", "Object") /* I18N:  */; // #305
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage obiektJestUruchamiOstatnieDniaMi = new I18nMessagePlEn("Obiekt jest uruchamiany ostatniego dnia miesiąca", "Last day of every month") /* I18N:  */; // #306
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage obiektJestUruchamiWPierwszyPonie = new I18nMessagePlEn("Obiekt jest uruchamiany w pierwszy poniedziałek każdego miesiąca", "First monday of every month") /* I18N:  */; // #307
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage obiektJestUruchamiZgodnieZKalend = new I18nMessagePlEn("Obiekt jest uruchamiany zgodnie z kalendarzem", "According to calendar") /* I18N:  */; // #308
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage obiektJestUruchamiany = new I18nMessagePlEn("Obiekt jest uruchamiany", "At") /* I18N:  */; // #309
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage obiektUruchamianyCo = new I18nMessagePlEn("Obiekt uruchamiany co", "Every") /* I18N:  */; // #310
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage obiektUruchamianyCoTydzien = new I18nMessagePlEn("Obiekt uruchamiany co tydzień", "Every week") /* I18N:  */; // #311
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeAndDictionarySelectorForUsers
    public static final I18nMessage obiektowPodrzednych = new I18nMessagePlEn("obiektow podrzędnych", "descendant objects") /* I18N:  */; // #312
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage obiektyPodrzedne = new I18nMessagePlEn("Obiekty podrzędne", "Descendant objects") /* I18N:  */; // #313
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.RateDialog
    public static final I18nMessage ocen = new I18nMessagePlEn("Oceń", "Rate") /* I18N:  */; // #314
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.EntityVoteValsWidget
    public static final I18nMessage ocenJako = new I18nMessagePlEn("Oceń jako", "Rate as") /* I18N:  */; // #315
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.RateDialog
    public static final I18nMessage ocenPozniej = new I18nMessagePlEn("Oceń poźniej", "Rate later") /* I18N:  */; // #316
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.RateDialog
    public static final I18nMessage ocenTutorial = new I18nMessagePlEn("Oceń samouczek", "Rate tutorial") /* I18N:  */; // #317
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.EntityVoteValsWidget
    public static final I18nMessage ocenaDodana = new I18nMessagePlEn("Ocena dodana", "Vote added") /* I18N:  */; // #318
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.VoteBeanExtractor
    // pl.fovis.client.entitydetailswidgets.EntityVoteValsWidget
    public static final I18nMessage ocenilesJako = new I18nMessagePlEn("Oceniłeś jako", "Rated as") /* I18N:  */; // #319
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikwidgets.UserRankPageGrid
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage oceny = new I18nMessagePlEn("Oceny", "Votes") /* I18N:  */; // #320
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminLogsPage
    public static final I18nMessage odswiez = new I18nMessagePlEn("Odśwież", "Refresh") /* I18N:  */; // #321
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.ObjectInHistoryBeanExtractor
    // pl.fovis.client.bikwidgets.MyBIKSHistoryGrid
    public static final I18nMessage odwiedzin = new I18nMessagePlEn("Wyświetleń", "Views") /* I18N:  */; // #322
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage opcjonalnaPodgrupaMenu = new I18nMessagePlEn("Opcjonalna podgrupa menu", "Optional menu subgroup") /* I18N:  */; // #323
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.dialogs.EditAttrHintDialog
    // pl.fovis.client.dialogs.EditDescriptionDialog
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage opis = new I18nMessagePlEn("Opis", "Description") /* I18N: description */; // #324
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.LisaDictionaryColumnEditionDialog
    public static final I18nMessage opisKolumnny = new I18nMessagePlEn("Opis kolumnny", "Column description") /* I18N:  */; // #325
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.LisaBeanDisplay
    public static final I18nMessage opisKolumny = new I18nMessagePlEn("Opis kolumny", "Column description") /* I18N:  */; // #326
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.HomePageUserTypeDesc
    public static final I18nMessage opisUprawnienUzytkownika = new I18nMessagePlEn("Opis uprawnień użytkownika", "User privileges description") /* I18N:  */; // #327
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage osobUznaloToZaUzyteczne = new I18nMessagePlEn("osób uznało to za użyteczne", "users rated this as useful") /* I18N:  */; // #328
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.StatisticsPage
    public static final I18nMessage ostatniDzien = new I18nMessagePlEn("Ostatni dzień", "Last day") /* I18N:  */; // #329
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.StatisticsPage
    public static final I18nMessage ostatniTydzien = new I18nMessagePlEn("Ostatni tydzień", "Last week") /* I18N:  */; // #330
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.entitydetailswidgets.EntityDetailsTabPaginationBase
    // pl.fovis.client.bikwidgets.WidgetWithPagination
    public static final I18nMessage ostatniaStrona = new I18nMessagePlEn("Ostatnia strona", "Last page") /* I18N:  */; // #331
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.StatisticsPage
    public static final I18nMessage ostatnie30Dni = new I18nMessagePlEn("Ostatnie 30 dni", "Last 30 days") /* I18N:  */; // #332
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.ObjectInHistoryBeanExtractor
    // pl.fovis.client.bikwidgets.MyBIKSHistoryGrid
    public static final I18nMessage ostatnieOdwiedziny = new I18nMessagePlEn("Ostatnie wyświetlenie", "Last viewed") /* I18N:  */; // #333
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage otworzWSAPBO = new I18nMessagePlEn("Otwórz w SAP BO", "Open in SAP BO") /* I18N:  */; // #334
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.MyBIKSFavouriteGrid
    public static final I18nMessage oznaczElement = new I18nMessagePlEn("Oznacz element", "Mark item") /* I18N:  */; // #335
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.ViewTutorialInFullScreenDialog
    public static final I18nMessage oznaczJakoNiePrzeczytane = new I18nMessagePlEn("Oznacz jako nie przeczytane", "Mark as unread") /* I18N:  */; // #336
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.ViewTutorialInFullScreenDialog
    public static final I18nMessage oznaczJakoPrzeczytane = new I18nMessagePlEn("Oznacz jako przeczytane", "Mark as read") /* I18N:  */; // #337
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSFavouriteTab
    public static final I18nMessage oznaczWszystkiElementyJakoPrzecz = new I18nMessagePlEn("Oznacz wszystkie elementy jako przeczytane", "Mark all items as read") /* I18N:  */; // #338
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.MyBIKSFavouriteGrid
    public static final I18nMessage oznaczWszystkieElementyW = new I18nMessagePlEn("Oznacz wszystkie elementy w", "Mark all items in") /* I18N:  */; // #339
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSFavouriteTab
    public static final I18nMessage oznaczonoElementyJakoPrzeczytane = new I18nMessagePlEn("Oznaczono elementy jako przeczytane", "Mark items as read") /* I18N:  */; // #340
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage pelneZasilanie = new I18nMessagePlEn("Pełne zasilanie", "Full data load") /* I18N:  */; // #341
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.entitydetailswidgets.EntityDetailsTabPaginationBase
    // pl.fovis.client.bikwidgets.WidgetWithPagination
    public static final I18nMessage pierwszaStrona = new I18nMessagePlEn("Pierwsza strona", "First page") /* I18N:  */; // #342
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorForAttachments
    public static final I18nMessage pobierzNowy = new I18nMessagePlEn("Pobierz nowy", "New attachment") /* I18N:  */; // #343
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.lisa.NodeActionAddLisaCategory
    public static final I18nMessage podajNazwe = new I18nMessagePlEn("Podaj nazwę", "Enter name") /* I18N:  */; // #344
    public static final I18nMessage podajNazweZdarzenia = new I18nMessagePlEn("Nazwa", "Enter name") /* I18N:  */; // #344
    public static final I18nMessage wybierzTypWezla = new I18nMessagePlEn("Typ węzła", "Node type") /* I18N:  */; // #344
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage podajWartosciAtrybutu = new I18nMessagePlEn("Podaj wartości atrybutu", "Enter attribute value") /* I18N:  */; // #345
    public static final I18nMessage podajWartosciAtrybutuBoolean = new I18nMessagePlEn("Podaj dwie wartości atrybutu", "Enter two attribute value") /* I18N:  */; // #345
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage podajWartosciAtrybutuPoPrzecink = new I18nMessagePlEn("Podaj wartości atrybutu po przecinkach", "Enter attribute values separated with comma") /* I18N:  */; // #346

    public static final I18nMessage podajWartosciAtrybutuDlaWartosciLogicznej = new I18nMessagePlEn("Podaj wartości atrybutu na zasadzie:<BR> prawda,fałsz, np. tak,nie", "Enter two attribute values <BR>separated with comma, ex.: yes,no") /* I18N:  */; // #346
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    public static final I18nMessage podaneBiezaceHasloJestBledne = new I18nMessagePlEn("Podane bieżące hasło jest błędne", "Entered password is invalid") /* I18N:  */; // #347
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage podanyLoginJuzIstnieje = new I18nMessagePlEn("Podany login już istnieje", "Specified login already exists") /* I18N:  */; // #348
    // is translated
    // multiple occurrences: 4
    // pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase
    // pl.fovis.client.dialogs.TreeSelectorForAttachments
    // pl.fovis.client.treeandlist.TreeNodeBeanAsMapStorage
    // pl.fovis.client.BIKClientSingletons
    public static final I18nMessage podglad = new I18nMessagePlEn("Podgląd", "Preview") /* I18N:  */; // #349
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage pojawiSieW = new I18nMessagePlEn("pojawi się w", "will be displayed in") /* I18N:  */; // #350
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage pokazOpis = new I18nMessagePlEn("Pokaż opis", "Show description") /* I18N:  */; // #351
    public static final I18nMessage pokazPowiazania = new I18nMessagePlEn("Pokaż powiązania", "Show description") /* I18N:  */; // #351
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.entitydetailswidgets.ShowSQLWidgetBase
    // pl.fovis.client.entitydetailswidgets.TableWidget
    // pl.fovis.client.entitydetailswidgets.SAPBOExtradataWidget
    public static final I18nMessage pokazSQL = new I18nMessagePlEn("Pokaż SQL", "Show SQL") /* I18N:  */; // #352
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSNewsTab
    public static final I18nMessage pokazTylkoNieprzeczytane = new I18nMessagePlEn("Pokaż tylko nieprzeczytane", "Show unread items only") /* I18N:  */; // #353
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSFavouriteTab
    public static final I18nMessage pokazTylkoZmodyfikowaneUlubione = new I18nMessagePlEn("Pokaż tylko zmodyfikowane ulubione", "Show modified only") /* I18N:  */; // #354
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.entitydetailswidgets.MetadataRolesWidgetFlat
    // pl.fovis.client.entitydetailswidgets.MetadataWidgetFlat
    public static final I18nMessage pokazTypyObiektowPowiazanych = new I18nMessagePlEn("Pokaż typy obiektów powiązanych", "Types of associated objects to display") /* I18N:  */; // #355
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage pokazZaawansowane = new I18nMessagePlEn("Pokaż zaawansowane", "Show advanced") /* I18N:  */; // #356
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.MyBIKSFavouriteGrid
    public static final I18nMessage pokazaneJest20Z = new I18nMessagePlEn("Pokazane jest 20 z", "Showing 20 of") /* I18N:  */; // #357
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeAndDictionarySelectorForUsers
    public static final I18nMessage pokazujWObiektachPodrzednych = new I18nMessagePlEn("Pokazuj w obiektach podrzędnych", "Show in descendant objects") /* I18N:  */; // #358
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrUpdateAttributeAndTheirKinds
    public static final I18nMessage polaNazwaAtrybutuINodeKindNieMog = new I18nMessagePlEn("Pola nazwa atrybutu i node kind nie moga być puste", "Fields attribute name and node kind cannot be empty") /* I18N:  */; // #359
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.UserDialog
    public static final I18nMessage polaNazwaIEmailSaWymagane = new I18nMessagePlEn("Pola nazwa i email są wymagane", "Fields name and email are required") /* I18N:  */; // #360
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.BaseRichTextAndTextboxDialog
    public static final I18nMessage polaTytulITrescNieMogaBycPuste = new I18nMessagePlEn("Pola tytuł i treść nie mogą być puste", "Fields title and content are required") /* I18N:  */; // #361
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage poleJednokrotnegoWyboru = new I18nMessagePlEn("Pole jednokrotnego wyboru", "Single choice field") /* I18N:  */; // #362
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage selectOneJoinedObject = new I18nMessagePlEn("Wybór jednego z wielu powiązań", "Single joined objects") /* I18N:  */; // #362
    public static final I18nMessage sql = new I18nMessagePlEn("SQL", "SQL") /* I18N:  */; // #362
    public static final I18nMessage wykonalnySkrypt = new I18nMessagePlEn("Wykonalny skrypt", "Runnable script") /* I18N:  */; // #362
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage poleJednokrotnegoWyboru2 = new I18nMessagePlEn("Pole jednokrotnego wyboru2", "Single choice field2") /* I18N:  */; // #362
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage poleLoginJestWymagane = new I18nMessagePlEn("Pole login jest wymagane", "Login is required") /* I18N:  */; // #363
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.MetapediaDialog
    public static final I18nMessage poleTytulNieMozeBycPuste = new I18nMessagePlEn("Pole tytuł nie może być puste", "Title is required") /* I18N:  */; // #364
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage poleWielokrotnegoWyboru = new I18nMessagePlEn("Pole wielokrotnego wyboru", "Multiple choice field") /* I18N:  */; // #365

    public static final I18nMessage wartoscLogiczna = new I18nMessagePlEn("Wartość logiczna", "Boolean") /* I18N:  */; // #365
    public static final I18nMessage wartoscWyliczana = new I18nMessagePlEn("Wartość wyliczana", "Calculation") /* I18N:  */; // #365
    public static final I18nMessage javascript = new I18nMessagePlEn("Javascript", "Javascript") /* I18N:  */; // #365
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage polecZnajomemu = new I18nMessagePlEn("Udostępnij", "Share") /* I18N:  */; // #366
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.dialogs.HelpDialog
    // pl.fovis.client.menu.LinkTabPanel
    // pl.fovis.client.BIKClientSingletons
    public static final I18nMessage pomoc = new I18nMessagePlEn("Pomoc", "Help") /* I18N:  */; // #367
    // is translated
    // multiple occurrences: 4
    // pl.fovis.common.JoinTypeRoles
    // pl.fovis.client.entitydetailswidgets.UserBlogBeanExtractor
    // pl.fovis.client.entitydetailswidgets.UserRoleInNodeBeanExtractor
    // pl.fovis.client.entitydetailswidgets.UserInNodeBeanExtractor
    public static final I18nMessage pomocniczy = new I18nMessagePlEn("Pomocniczy", "Auxiliary") /* I18N:  */; // #368
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.entitydetailswidgets.EntityDetailsTabPaginationBase
    // pl.fovis.client.bikwidgets.WidgetWithPagination
    public static final I18nMessage poprzedniaStrona = new I18nMessagePlEn("Poprzednia strona", "Previous page") /* I18N:  */; // #369
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorForUsersToCopyRole
    public static final I18nMessage powiazaniaWRoliGlownej = new I18nMessagePlEn("Powiązania w roli głównej", "Associations in main role") /* I18N:  */; // #370
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorForUsersToCopyRole
    public static final I18nMessage powiazaniaWRoliPomocniczej = new I18nMessagePlEn("Powiązania w roli pomocniczej", "Associations in auxiliary role") /* I18N:  */; // #371
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.dialogs.TreeSelectorForUserRolesDialog
    // pl.fovis.client.dialogs.TreeSelectorForJoinedObjectsDialogNew
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage powiazaniaZapisaneOdswiezanie = new I18nMessagePlEn("Powiązania zapisane, odświeżanie", "Associations saved, refreshing") /* I18N:  */; // #372
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage powtorzHaslo = new I18nMessagePlEn("Powtórz hasło", "Repeat password") /* I18N:  */; // #373
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage poziom = new I18nMessagePlEn("poziom", "level") /* I18N:  */; // #374
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSHomePageTab
    public static final I18nMessage poznajBIKnowledgSystemZSamouczk = new I18nMessagePlEn("Poznaj BI Knowledge System z Samouczkiem", "Explore BI Knowledge System with our Tutorial") /* I18N:  */; // #375
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorForUsersToCopyRole
    public static final I18nMessage pozostaw = new I18nMessagePlEn("Pozostaw", "Keep") /* I18N:  */; // #376
    // is translated
    // single occurrence
    // pl.fovis.common.JoinTypeRoles
    public static final I18nMessage pozostawAktualneUstawienWObiekta = new I18nMessagePlEn("Pozostaw aktualne ustawienia w obiektach podrzędnych, a w", "Keep current associations in descendant objects and in") /* I18N: wtf? */; // #377
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeAndDictionarySelectorForUsers
    public static final I18nMessage pozostawiszRoleGlowneDla = new I18nMessagePlEn("Pozostawisz role głowne dla", "You will keep main roles for") /* I18N:  */; // #378
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage pozwalajNaDodawanieDowiazan = new I18nMessagePlEn("Pozwalaj na dodawanie dowiązań", "Allow adding links") /* I18N:  */; // #379
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage pozycje = new I18nMessagePlEn("pozycje", "items") /* I18N: pozycje od-do */; // #380
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.lisa.LisaDictionaryContent
    public static final I18nMessage pozycjeZostalyPrzeniesDoNowejKat = new I18nMessagePlEn("Pozycje zostały przeniesione do nowej kategorii", "Items have been moved to new category") /* I18N:  */; // #381
    public static final I18nMessage kategoriaZostalaPrzeniesiona = new I18nMessagePlEn("Kategoria została przeniesiona", "Kategory was moved") /* I18N:  */; // #381
    public static final I18nMessage pozycjeZostalyUsuniete = new I18nMessagePlEn("Pozycje zostały usunięte ze słownika", "Items have been deleted from dictionary") /* I18N:  */;
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.ChangeCategoryDialog
    public static final I18nMessage pozycjiDoPrzeniesienia = new I18nMessagePlEn("Pozycji do przeniesienia", "Items to move") /* I18N:  */; // #382
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSHomePageTab
    public static final I18nMessage prezentaNarzedziBIKnowledgSystem = new I18nMessagePlEn("Prezentacja narzędzia BI Knowledge System", "Presentation of BI Knowledge System") /* I18N:  */; // #383
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionBase
    public static final I18nMessage proszePrzypiszSobieUzutkownSlole = new I18nMessagePlEn("Proszę przypisz sobie użytkownika społecznościowego", "Assign a social user to your system user") /* I18N:  */; // #384
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage proszeWybracDrzewaKtorychUzytkow = new I18nMessagePlEn("Proszę wybrać drzewa, których użytkownik jest autorem", "Select trees in which this user will act as author") /* I18N:  */; // #385
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionBase
    public static final I18nMessage proszeZwrocicSieDoAdministOPrzyp = new I18nMessagePlEn("Proszę zwrócić się do administratora o przypisanie \n"
            + " użytkownika społecznościowego.", "Please ask BIKS admin to assign you to a social user") /* I18N:  */; // #386
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage przebudowywany = new I18nMessagePlEn("przebudowywany", "rebuilt") /* I18N:  */; // #387
    // is translated
    // multiple occurrences: 4
    // pl.fovis.client.dialogs.EntityDetailsPaneDialog
    // pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase
    // pl.fovis.client.treeandlist.TreeNodeBeanAsMapStorage
    // pl.fovis.client.BIKClientSingletons
    public static final I18nMessage przejdz = new I18nMessagePlEn("Przejdź", "Go to") /* I18N:  */; // #388
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.nodeactions.NodeActionJumpToLinkedNode
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage przejdzDoOryginalu = new I18nMessagePlEn("Przejdź do oryginału", "Go to original") /* I18N:  */; // #389
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionPromoteToDefinition
    public static final I18nMessage przemianowanoNaDefinicje = new I18nMessagePlEn("Przemianowano na definicję", "Promoted to definition") /* I18N:  */; // #390
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionPromoteToDefinition
    public static final I18nMessage przemianujNaDefinicje = new I18nMessagePlEn("Przemianuj na definicję", "Promote to definition") /* I18N:  */; // #391
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorForUsersToCopyRole
    public static final I18nMessage przenies = new I18nMessagePlEn("Przenieś", "Move") /* I18N:  */; // #392
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.BiksPopUpMenu
    public static final I18nMessage przeniesNaDol = new I18nMessagePlEn("Przenieś na dół", "Move to bottom") /* I18N:  */; // #393
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.BiksPopUpMenu
    public static final I18nMessage przeniesNaGore = new I18nMessagePlEn("Przenieś na górę", "Move to top") /* I18N:  */; // #394
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.BiksPopUpMenu
    public static final I18nMessage przeniesNizej = new I18nMessagePlEn("Przenieś niżej", "Move below") /* I18N:  */; // #395
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.BiksPopUpMenu
    public static final I18nMessage przeniesWyzej = new I18nMessagePlEn("Przenieś wyżej", "Move above") /* I18N:  */; // #396
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage przerwij = new I18nMessagePlEn("Przerwij", "Cancel") /* I18N:  */; // #397
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage przeszukiwaneZakladki = new I18nMessagePlEn("Przeszukiwane zakładki", "Searched tabs") /* I18N:  */; // #398
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSHomePageTab
    public static final I18nMessage przykladoweHasla = new I18nMessagePlEn("Przykładowe hasła", "Sample search terms") /* I18N:  */; // #399
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSHomePageTab
    public static final I18nMessage przykladoweLinki = new I18nMessagePlEn("Przykładowe linki", "Sample links") /* I18N:  */; // #400
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionLinkUser
    public static final I18nMessage przypisanoUzytkownikaDoGrupy = new I18nMessagePlEn("Przypisano użytkownika do grupy", "User assigned to group") /* I18N:  */; // #401
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeAndDictionarySelectorForUsers
    public static final I18nMessage przypisanoUzytkownikaDoRoli = new I18nMessagePlEn("Przypisano użytkownika do roli", "User assigned to role") /* I18N:  */; // #402
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage przypisany = new I18nMessagePlEn("Przypisany", "Assigned") /* I18N:  */; // #403
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionLinkUser
    public static final I18nMessage przypiszDoGrupy = new I18nMessagePlEn("Przypisz do grupy", "Assign to group") /* I18N:  */; // #404
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionLinkUserToRole
    public static final I18nMessage przypiszDoRoli = new I18nMessagePlEn("Przypisz do roli", "Assign to role") /* I18N:  */; // #405
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionLinkUserToRoleForBlogs
    public static final I18nMessage przypiszJakoRedaktor = new I18nMessagePlEn("Przypisz jako redaktor", "Assign as editor") /* I18N:  */; // #406
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeAndDictionarySelectorForUsers
    public static final I18nMessage przypiszUzytkownikaDoRoli = new I18nMessagePlEn("Przypisz użytkownika do roli", "Assign user to role") /* I18N:  */; // #407
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage przywrocOryginalStanPowiazanWPod = new I18nMessagePlEn("Przywróć oryginalny stan powiązań w poddrzewie", "Revert to original associations in subtree") /* I18N:  */; // #408
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage przywrocOryginalStanPowiazanWTym = new I18nMessagePlEn("Przywróć oryginalny stan powiązania w tym obiekcie", "Revert to original associations in this item") /* I18N:  */; // #409
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.UserRankPage
    // pl.fovis.client.bikpages.UserRankPageTab
    public static final I18nMessage rankingUzytkownikow = new I18nMessagePlEn("Ranking użytkowników", "User ranking") /* I18N:  */; // #410
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SAPBOExtradataWidget
    public static final I18nMessage raportOfflineRaportJestOdswiezaZ = new I18nMessagePlEn("Raport offline - raport jest odświeżany zgodnie ze zdefiniowanym harmonogramem lub na żądanie", "No (refreshes on demand or according to schedule)") /* I18N:  */; // #411
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SAPBOExtradataWidget
    public static final I18nMessage raportOnlineOdswiezaWMomencieJeg = new I18nMessagePlEn("Raport online - odświeżany w momencie jego uruchomienia (otwarcia)", "Yes") /* I18N:  */; // #412
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedAddAttrDictWidget
    public static final I18nMessage razy = new I18nMessagePlEn("razy", "time(s)") /* I18N:  */; // #413
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage resetujProces = new I18nMessagePlEn("Resetuj proces", "Restart data load process") /* I18N: reset vs restart? */; // #414
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.dialogs.MetapediaDialog
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    // pl.fovis.server.BOXIServiceImpl
    public static final I18nMessage robocza = new I18nMessagePlEn("robocza", "working") /* I18N:  */; // #415
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage rolach = new I18nMessagePlEn("rolach", "roles") /* I18N:  */; // #416
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.RoleForNodeBikPage
    public static final I18nMessage roleUzytkownikow = new I18nMessagePlEn("Role użytkowników", "User roles") /* I18N:  */; // #417
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage rozpoczetoZasilanie = new I18nMessagePlEn("Rozpoczęto zasilanie", "Data load started") /* I18N:  */; // #418
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.MyBIKSTutorialTab
    // pl.fovis.client.bikpages.MyBIKBikPage
    public static final I18nMessage samouczek = new I18nMessagePlEn("Samouczki", "Tutorials") /* I18N:  */; // #419
    public static final I18nMessage samouczek2 = new I18nMessagePlEn("Samouczek", "Tutorial") /* I18N:  */; // #419
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage serwerFTP = new I18nMessagePlEn("Serwer FTP", "FTP server") /* I18N:  */; // #420
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SapBoScheduleWidget
    public static final I18nMessage skrzynkaOdbiorcza = new I18nMessagePlEn("Skrzynka odbiorcza", "Inbox") /* I18N: wtf? */; // #421
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.EditEntityKeywordsDialog
    public static final I18nMessage slowaKluczowe = new I18nMessagePlEn("Słowa kluczowe", "Keywords") /* I18N:  */; // #422
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage slownikPojecBiznesowych = new I18nMessagePlEn("Słownik Pojęć Biznesowych", "Business Terms Glossary") /* I18N:  */; // #423
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage slownikSkrotow = new I18nMessagePlEn("Słownik Skrótów", "Acronyms Dictionary") /* I18N:  */; // #424
    // is translated
    // single occurrence
    // pl.fovis.client.BIKClientSingletons
    public static final I18nMessage slownik_wtf = new I18nMessagePlEn("Słownik", "Dictionary") /* I18N: wtf? */; // #425
    public static final I18nMessage slownik_dwh_wtf = new I18nMessagePlEn("Słownik DWH", "DWH Dictionaries");

    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage slowniki = new I18nMessagePlEn("Słowniki", "Dictionaries") /* I18N:  */; // #426
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionResetVisualOrder
    public static final I18nMessage sortujAlfabetycznie = new I18nMessagePlEn("Sortuj alfabetycznie", "Sort alphabetically") /* I18N:  */; // #427
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage spolecznoscBI = new I18nMessagePlEn("Społeczność BI", "BI Community") /* I18N:  */; // #428
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.StatisticsPage
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage statystyki = new I18nMessagePlEn("Statystyki", "Statistics") /* I18N:  */; // #429
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.ViewTutorialInFullScreenDialog
    public static final I18nMessage streszczenieFilmu = new I18nMessagePlEn("Streszczenie filmu", "Video summary") /* I18N:  */; // #430
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSHomePageTab
    public static final I18nMessage stronaStartowa = new I18nMessagePlEn("Strona startowa", "Home page") /* I18N:  */; // #431
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage stronaZ = new I18nMessagePlEn("strona {0} z {1}", "page {0} of {1}") /* I18N:  */; // #432
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.entitydetailswidgets.EntityDetailsTabPaginationBase
    // pl.fovis.client.bikwidgets.WidgetWithPagination
    public static final I18nMessage stronaZPozOdDoWszystkich = new I18nMessagePlEn("Strona: {0} z {1}, pozycje od {2} do {3} (wszystkich: {4})", "Page: {0} of {1}, items from {2} to {3} (total: {4})") /* I18N: ok */; // #433
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage systemZrodlowy = new I18nMessagePlEn("System źródłowy", "Source system") /* I18N:  */; // #434
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikcenterEntryPoint
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage szukaj = new I18nMessagePlEn("Szukaj", "Search") /* I18N:  */; // #435
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage szukanaFraza = new I18nMessagePlEn("Szukana fraza", "Searched term") /* I18N:  */; // #436
    public static final I18nMessage wartoscAttrybutu = new I18nMessagePlEn("Wartosc attrybutu", "Attribute value") /* I18N:  */; // #436
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.SystemUserBeanExtractor
    public static final I18nMessage tak = new I18nMessagePlEn("Tak", "Yes") /* I18N:  */; // #437
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.LisaBeanDisplay
    public static final I18nMessage tak2 = new I18nMessagePlEn("tak", "yes") /* I18N:  */; // #438
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.AddOrUpdateItemNameDialog
    // pl.fovis.client.dialogs.AttrOrRoleEditDialog
    public static final I18nMessage takaNazwaJuzIstnieje = new I18nMessagePlEn("Taka nazwa już istnieje", "Such name already exists") /* I18N:  */; // #439
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.DataDicionaryPage
    public static final I18nMessage taksonomie = new I18nMessagePlEn("Taksonomie", "Taxonomies") /* I18N:  */; // #440
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.UserDialog
    public static final I18nMessage telefon = new I18nMessagePlEn("Telefon", "Phone") /* I18N:  */; // #441
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedAddAttrDictWidget
    public static final I18nMessage tenAtrybutJestUzyty = new I18nMessagePlEn("Ten atrybut jest użyty", "This attribute is used") /* I18N:  */; // #442
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AdminConfigBIKBaseWidget
    public static final I18nMessage testPolaczenZakonczoNiepowod = new I18nMessagePlEn("Test połączenia zakończony niepowodzeniem", "Test connection: failure") /* I18N:  */; // #443
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AdminConfigBIKBaseWidget
    public static final I18nMessage testPolaczeniaZakonczonySukcesem = new I18nMessagePlEn("Test połączenia zakończony sukcesem", "Test connection: success") /* I18N:  */; // #444
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorForJoinedObjectsDialog
    public static final I18nMessage toPowiazanieJuzIstnieje = new I18nMessagePlEn("To powiązanie już istnieje", "Such association already exists") /* I18N:  */; // #445
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.LoginDialog
    // pl.fovis.client.dialogs.UserDialog
    public static final I18nMessage toSieNiePowinnoWywolac = new I18nMessagePlEn("to się nie powinno wywołać", "this should not be called") /* I18N:  */; // #446
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.entitydetailswidgets.AuthorBeanExtractor
    // pl.fovis.client.entitydetailswidgets.UserInNodeBeanExtractor
    public static final I18nMessage toSieNigdyNieWywola = new I18nMessagePlEn("to się nigdy nie wywoła", "This won't be called") /* I18N:  */; // #447
    // is translated
    // multiple occurrences: 4
    // pl.fovis.client.dialogs.BaseActionOrCancelDialogEx
    // pl.fovis.client.dialogs.BaseRichTextAndTextboxDialog
    // pl.fovis.client.dialogs.AddOrEditAttributeDialog
    // pl.fovis.client.bikpages.HelpPageTree
    public static final I18nMessage tresc = new I18nMessagePlEn("Treść", "Content") /* I18N:  */; // #448
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.AddOrEditAttributeDialog
    public static final I18nMessage trescNieMozeBycPusta = new I18nMessagePlEn("Treść nie może być pusta", "Content cannot be empty") /* I18N:  */; // #449
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.HelpPageTree
    public static final I18nMessage trescPomocy = new I18nMessagePlEn("Treść pomocy", "Help content") /* I18N:  */; // #450
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage trwaWyszukiwanie = new I18nMessagePlEn("Trwa wyszukiwanie", "Searching in progress") /* I18N:  */; // #451
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage tutajBedzieOpis = new I18nMessagePlEn("tutaj będzie opis", "description goes here") /* I18N:  */; // #452
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.JoinTypeRolesWidget
    public static final I18nMessage tychObiektach = new I18nMessagePlEn("tych obiektach", "these objects") /* I18N:  */; // #453
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.DQCTreeBikPage
    public static final I18nMessage tylkoAktywne = new I18nMessagePlEn("Tylko aktywne", "Only active") /* I18N:  */; // #454
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.JoinTypeRolesWidget
    public static final I18nMessage tymObiekcie = new I18nMessagePlEn("tym obiekcie", "this object") /* I18N:  */; // #455
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.entitydetailswidgets.IndexWidgetFlat
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage typ = new I18nMessagePlEn("Typ", "Type") /* I18N:  */; // #456
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.lisa.LisaDictionaryColumnEditionDialog
    // pl.fovis.client.dialogs.lisa.LisaBeanDisplay
    public static final I18nMessage typDanych = new I18nMessagePlEn("Typ danych", "Data type") /* I18N:  */; // #457
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.TreeBeanExtractor
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage typDrzewa = new I18nMessagePlEn("Typ drzewa", "Tree type") /* I18N:  */; // #458
    // is translated
    // multiple occurrences: 6
    // pl.fovis.client.dialogs.PickBlogEntryDialog
    // pl.fovis.client.dialogs.BaseActionOrCancelDialogEx
    // pl.fovis.client.dialogs.AddOrEditAttributeDialog
    // pl.fovis.client.dialogs.BaseRichTextAndTextboxDialog
    // pl.fovis.client.dialogs.MetapediaDialog
    // pl.fovis.client.bikpages.HelpPageTree
    public static final I18nMessage tytul = new I18nMessagePlEn("Tytuł", "Title") /* I18N:  */; // #459
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage uWAGA = new I18nMessagePlEn("UWAGA", "WARNING") /* I18N:  */; // #460
    // is translated
    // single occurrence
    // pl.fovis.client.bikcenterEntryPoint
    public static final I18nMessage uWAGANiekompaWersjaAplikacjIBazy = new I18nMessagePlEn("UWAGA!!!\n\nNiekompatybilna wersja aplikacji i bazy.\nAplikacja jest w wersji: v", "Warning!!!\n\nApplication version incompatible with db version.\nVersion of application: v") /* I18N:  */; // #461
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionBase
    public static final I18nMessage uWAGAWZwiazkuZNowymiFunkcjonJaki = new I18nMessagePlEn("UWAGA!!!\n\nW związku z nowymi funkcjonalnościami,\n"
            + "musisz posiadać użytkownika społecznościowego", "WARNING!!!\n\nNew features came\n"
            + "therefore you must have an associated social user.") /* I18N:  */; // #437
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage ukryjZaawansowane = new I18nMessagePlEn("Ukryj zaawansowane", "Hide advanced") /* I18N:  */; // #463
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSFavouriteTab
    public static final I18nMessage ulubione = new I18nMessagePlEn("Ulubione", "Favourites") /* I18N:  */; // #464
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.EntityInFvsWidget
    public static final I18nMessage ulubionych = new I18nMessagePlEn("ulubionych", "favourites") /* I18N: added to/removed from */; // #465
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage umiejscowenieWMenu = new I18nMessagePlEn("Umiejscowenie w menu", "Position in menu") /* I18N:  */; // #466
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.bikpages.SystemUserPage
    // pl.fovis.client.bikpages.SystemUserBeanExtractor
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage uprawnienia = new I18nMessagePlEn("Uprawnienia", "Privileges") /* I18N:  */; // #467
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage uruchamianie = new I18nMessagePlEn("Uruchamianie", "Run on demand") /* I18N:  */; // #468
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminSchedulePage
    public static final I18nMessage uruchamiany = new I18nMessagePlEn("Uruchamiany", "Executed") /* I18N: wtf? */; // #469
    public static final I18nMessage czestotliwosc = new I18nMessagePlEn("Częstotliwość (min)", "Frequency (min)") /* I18N: wtf? */; // #469
    public static final I18nMessage tlumaczenieCzestotliwosci = new I18nMessagePlEn("Częstotliwość: jednorazowo = 0, codziennie = 1440, co 3 dni = 4320, co 5 dni = 7200, co 7 dni = 10800, co 30 dni = 43200", "Frequency: once = 0, everyday = 1440, every 3 days = 4320, every 5 days = 7200, every 7 days = 10800, every 30 days = 43200") /* I18N: wtf? */; // #469
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage uruchom = new I18nMessagePlEn("Uruchom", "Execute") /* I18N:  */; // #470
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage uruchomZasilanie = new I18nMessagePlEn("Uruchom zasilanie", "Run data load") /* I18N:  */; // #471
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.ConnectionServerDialog
    // pl.fovis.client.dialogs.ConnectionDatabaseNameDialog
    public static final I18nMessage ustaw = new I18nMessagePlEn("Ustaw", "Set") /* I18N:  */; // #472
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.ConnectionDatabaseNameDialog
    public static final I18nMessage ustawBazeDanychDlaPolaczenia = new I18nMessagePlEn("Ustaw bazę danych dla połączenia", "Set database for connection") /* I18N:  */; // #473
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.ConnectionServerDialog
    public static final I18nMessage ustawSerwerDlaPolaczenia = new I18nMessagePlEn("Ustaw serwer dla połączenia", "Set server for connection") /* I18N:  */; // #474
    // is translated
    // multiple occurrences: 12
    // pl.fovis.client.nodeactions.NodeActionDelete
    // pl.fovis.client.nodeactions.NodeActionDeleteRole
    // pl.fovis.client.entitydetailswidgets.GenericBeanExtractorBase
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    // pl.fovis.client.bikpages.NewsActionExtractor
    // pl.fovis.client.bikpages.ObjectInFvsBeanExtractor
    // pl.fovis.client.entitydetailswidgets.NoteWidgetFlat
    // pl.fovis.client.entitydetailswidgets.AttributeWidget
    // pl.fovis.client.bikwidgets.CategorizedAddAttrDictWidget
    // pl.fovis.client.bikpages.TreeBeanExtractor
    // pl.fovis.client.bikwidgets.CategorizedDictionaryWidgetBase
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage usun = new I18nMessagePlEn("Usuń", "Delete") /* I18N:  */; // #475
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.JoinTypeRolesWidget
    public static final I18nMessage usun2 = new I18nMessagePlEn("usuń", "delete") /* I18N:  */; // #476
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage usunPowiazanDlaObiektowBezposreP = new I18nMessagePlEn("Usuń powiązania dla obiektów bezpośrednio podrzędnych", "Delete associations for directly descendant objects") /* I18N:  */; // #477
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage usunPowiazanDlaWszystkiObiektowP = new I18nMessagePlEn("Usuń powiązania dla wszystkich obiektów podrzędnych", "Delete associations for all descendant objects") /* I18N:  */; // #478
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionDeleteRole
    public static final I18nMessage usunRole = new I18nMessagePlEn("Usuń rolę", "Delete role") /* I18N:  */; // #479
    // is translated
    // single occurrence
    // pl.fovis.common.JoinTypeRoles
    public static final I18nMessage usunW = new I18nMessagePlEn("Usuń w", "Delete in") /* I18N: wtf? */; // #480
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.ObjectInFvsBeanExtractor
    // pl.fovis.client.entitydetailswidgets.EntityInFvsWidget
    public static final I18nMessage usunZUlubionych = new I18nMessagePlEn("Usuń z ulubionych", "Remove from favourites") /* I18N:  */; // #481
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeAndDictionarySelectorForUsers
    public static final I18nMessage usunieszRoleDla = new I18nMessagePlEn("Usuniesz role dla", "You will remove roles for") /* I18N:  */; // #482
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.MyBIKSFavouriteGrid
    public static final I18nMessage usuniete = new I18nMessagePlEn("Usunięte", "Deleted") /* I18N:  */; // #483
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.entitydetailswidgets.AttributeWidget
    // pl.fovis.client.bikwidgets.CategorizedAddAttrDictWidget
    public static final I18nMessage usunietoAtrybut = new I18nMessagePlEn("Usunięto atrybut", "Attribute deleted") /* I18N:  */; // #484
    public static final I18nMessage usunietoZdarzenie = new I18nMessagePlEn("Usunięto zdarzenie", "Event deleted") /* I18N:  */; // #484
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AuthorBeanExtractor
    public static final I18nMessage usunietoAutora = new I18nMessagePlEn("Usunięto autora", "Author deleted") /* I18N:  */; // #485
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.TreeBeanExtractor
    public static final I18nMessage usunietoDrzewo = new I18nMessagePlEn("Usunięto drzewo", "Tree deleted") /* I18N:  */; // #486
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.nodeactions.NodeActionDelete
    // pl.fovis.client.nodeactions.NodeActionDeleteRole
    public static final I18nMessage usunietoElement = new I18nMessagePlEn("Usunięto element", "Item deleted") /* I18N:  */; // #487
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedDictionaryWidgetBase
    public static final I18nMessage usunietoKategorie = new I18nMessagePlEn("Usunięto kategorię", "Category deleted") /* I18N:  */; // #488
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.NoteWidgetFlat
    public static final I18nMessage usunietoKomentarz = new I18nMessagePlEn("Usunięto komentarz", "Comment deleted") /* I18N:  */; // #489
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.EntityVoteValsWidget
    public static final I18nMessage usunietoOcene = new I18nMessagePlEn("Usunięto ocenę", "Vote deleted") /* I18N:  */; // #490
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.entitydetailswidgets.JoinedObjBeanExtractor
    // pl.fovis.client.entitydetailswidgets.UserRoleInNodeBeanExtractor
    // pl.fovis.client.entitydetailswidgets.UserInNodeBeanExtractor
    public static final I18nMessage usunietoPowiazanie = new I18nMessagePlEn("Usunięto powiązanie", "Association deleted") /* I18N:  */; // #491
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedRoleForNodeDictWidget
    public static final I18nMessage usunietoRole = new I18nMessagePlEn("Usunięto role", "Role deleted") /* I18N:  */; // #492
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.BlogBeanExtractor
    public static final I18nMessage usunietoWpis = new I18nMessagePlEn("Usunięto wpis", "Entry deleted") /* I18N:  */; // #493
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.EntityInFvsWidget
    public static final I18nMessage usunietoZ = new I18nMessagePlEn("usunięto z", "removed from") /* I18N: favs */; // #494
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewsActionExtractor
    public static final I18nMessage usunietoZAktualnosci = new I18nMessagePlEn("Usunięto z komunikatów", "Deleted from announcements") /* I18N:  */; // #495
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.ObjectInFvsBeanExtractor
    public static final I18nMessage usunietoZUlubionych = new I18nMessagePlEn("Usunięto z ulubionych", "Deleted from favourites") /* I18N:  */; // #496
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AttachmentBeanExtractor
    public static final I18nMessage usunietoZalacznik = new I18nMessagePlEn("Usunieto załącznik", "Attachment deleted") /* I18N:  */; // #497
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    public static final I18nMessage usunietoZdjecie = new I18nMessagePlEn("Usunięto zdjęcie", "Photo deleted") /* I18N:  */; // #498
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.MyBIKSFavouriteGrid
    public static final I18nMessage usuniety = new I18nMessagePlEn("Usunięty", "Deleted") /* I18N:  */; // #499
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.TreeKindDialog
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage utworz = new I18nMessagePlEn("Utwórz", "Create") /* I18N:  */; // #500
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage utworzNowy = new I18nMessagePlEn("Utwórz nowy", "Create new") /* I18N:  */; // #501
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage utworzNowySzablonDrzewa = new I18nMessagePlEn("Utwórz nowy szablon drzewa", "Create new tree template") /* I18N:  */; // #502
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage uzupelniNazwyDlaTypuGaleziILisci = new I18nMessagePlEn("Uzupełnij nazwy dla typu, gałęzi i liścia", "Names of tree type, branch and leaf cannot be empty") /* I18N:  */; // #503
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.MetapediaDialog
    public static final I18nMessage uzupelnijWersjeDefinicji = new I18nMessagePlEn("Uzupełnij wersję definicji", "Version cannot be empty") /* I18N:  */; // #504
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.VoteBeanExtractor
    // pl.fovis.client.entitydetailswidgets.EntityVoteValsWidget
    public static final I18nMessage uzyteczne = new I18nMessagePlEn("użyteczne", "useful") /* I18N:  */; // #505
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage uzytecznosc = new I18nMessagePlEn("Użyteczność", "Usefulness") /* I18N:  */; // #506
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionLinkUser
    public static final I18nMessage uzytkownJestJuzPrzypisaDoTejGrup = new I18nMessagePlEn("Użytkownik jest już przypisany do tej grupy", "User is already assigned to this group") /* I18N:  */; // #507
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeAndDictionarySelectorForUsers
    public static final I18nMessage uzytkownJestJuzPrzypisaDoTejRoli = new I18nMessagePlEn("Użytkownik jest już przypisany do tej roli", "User is already assigned to this role") /* I18N:  */; // #508
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage uzytkownJuzPolaczonWybierzInnego = new I18nMessagePlEn("Użytkownik już połączony. Wybierz innego", "User is already assigned. Select another") /* I18N:  */; // #509
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.JoinTypeRolesWidget
    public static final I18nMessage uzytkownPosiadaObiektowPowiazan = new I18nMessagePlEn("Użytkownik posiada obiektów powiązanych", "User has associated objects:") /* I18N: wtf? */; // #510
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.UserRankPageGrid
    public static final I18nMessage uzytkownUznaloWpisyZaUzyteczn = new I18nMessagePlEn("użytkowników uznało wpisy za użyteczne", "users found the entries useful") /* I18N:  */; // #511
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.dialogs.PickUserDialogBase
    // pl.fovis.client.BIKGWTConstants
    // pl.fovis.client.bikwidgets.UserRankPageGrid
    public static final I18nMessage uzytkownicy = new I18nMessagePlEn("Użytkownicy", "Users") /* I18N:  */; // #512
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage uzytkownicySystemu = new I18nMessagePlEn("Użytkownicy systemu", "System users") /* I18N:  */; // #513
    // is translated
    // single occurrence
    // pl.fovis.client.BIKClientSingletons
    public static final I18nMessage uzytkownicy_wtf = new I18nMessagePlEn("Użytkownicy", "Users") /* I18N: wtf? */; // #514
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.UserDialog
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage uzytkownik = new I18nMessagePlEn("Użytkownik", "User") /* I18N:  */; // #515
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.UserDialog
    public static final I18nMessage uzytkownikOLoginie = new I18nMessagePlEn("Użytkownik o loginie", "User with login") /* I18N:  */; // #516
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.UserDialog
    public static final I18nMessage uzytkownikONazwie = new I18nMessagePlEn("Użytkownik o nazwie", "User with name") /* I18N:  */; // #517
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.SystemUserBeanExtractor
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage uzytkownikSpol2 = new I18nMessagePlEn("Użytkownik społ", "Social user") /* I18N:  */; // #518
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage uzytkownikSpolecznosciowy = new I18nMessagePlEn("Użytkownik Społecznościowy", "Social user") /* I18N:  */; // #519
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorForJoinedObjectsDialogNew
    public static final I18nMessage wRoli = new I18nMessagePlEn("w roli", "in role") /* I18N:  */; // #520
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.BaseBIKTreeWidget
    public static final I18nMessage wczytujeDaneProszeCzekac = new I18nMessagePlEn("Wczytuję dane, proszę czekać", "Loading data, please wait") /* I18N:  */; // #521
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.MetapediaDialog
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage wersja = new I18nMessagePlEn("Wersja", "Version:") /* I18N: dwukropek po angielsku, tak ma być! */; // #522
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.MetapediaDialog
    public static final I18nMessage wersjaKorporacZostanieZastapio = new I18nMessagePlEn("Wersja korporacyjna zostanie zastąpiona", "Corporate version will be replaced") /* I18N:  */; // #523
    // is translated
    // single occurrence
    // pl.fovis.client.bikcenterEntryPoint
    public static final I18nMessage wersjaNaSerwerze = new I18nMessagePlEn("Wersja na serwerze", "Version on server") /* I18N:  */; // #524
    // is translated
    // single occurrence
    // pl.fovis.client.bikcenterEntryPoint
    public static final I18nMessage wersjaWPrzegladarce = new I18nMessagePlEn("Wersja w przeglądarce", "Version in browser") /* I18N:  */; // #525
    // is translated
    // single occurrence
    // pl.fovis.client.menu.MenuBuilder
    public static final I18nMessage witaj = new I18nMessagePlEn("Witaj", "Welcome") /* I18N:  */; // #526
    // is translated
    // single occurrence
    // pl.fovis.client.menu.MenuBuilder
    public static final I18nMessage witajNieznajomy = new I18nMessagePlEn("Witaj, nieznajomy", "Welcome, unknown") /* I18N:  */; // #527
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionPaste
    public static final I18nMessage wklej = new I18nMessagePlEn("Wklej", "Paste") /* I18N:  */; // #528
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionPaste
    public static final I18nMessage wklejonoElement = new I18nMessagePlEn("Wklejono element", "Item pasted") /* I18N:  */; // #529
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage wlaczony = new I18nMessagePlEn("Włączony", "Enabled") /* I18N:  */; // #530
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage wpis = new I18nMessagePlEn("Wpis", "Blog entry") /* I18N: blog */; // #531
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.BlogAddOrEditDialog
    public static final I18nMessage wpisNaBlogu = new I18nMessagePlEn("Wpis na Blogu", "Blog entry") /* I18N:  */; // #532
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage wpiszInnaFrazeLubWybierzInneZakl = new I18nMessagePlEn("wpisz inną frazę lub wybierz inne zakładki/typy obiektów do przeszukiwania", "enter different phrase or select different tabs/object types to search for") /* I18N:  */; // #533
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSHomePageTab
    public static final I18nMessage wpiszSzukanaFraze = new I18nMessagePlEn("Wpisz szukaną frazę", "Enter search phrase") /* I18N:  */; // #534
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage wprowadzFrazeDoFiltru = new I18nMessagePlEn("Wprowadź frazę do filtru", "Enter phrase for filter") /* I18N:  */; // #535
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminSchedulePage
    public static final I18nMessage wprowawaPoprawneGodzinyWFormacie = new I18nMessagePlEn("Wprowawadź poprawne godziny w formacie hh:mm! Np", "Enter valid hours in hh:mm! Eg") /* I18N:  */; // #536
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionDeleteRole
    public static final I18nMessage wrazZElementaPodrzednUsunieszWsz = new I18nMessagePlEn("wraz z elementami podrzędnymi?"
            + " Usuniesz wszystkie powiązania użytkowników z tą rolą", "with descendant items? This will also delete all associations of users and this role") /* I18N:  */; // #537
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionDelete
    public static final I18nMessage wrazZElementamiPodrzednymi = new I18nMessagePlEn("wraz z elementami podrzędnymi", "with descendant items") /* I18N:  */; // #538
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage wszyscy = new I18nMessagePlEn("Wszyscy", "All") /* I18N:  */; // #539
    // is translated
    // multiple occurrences: 2
    // pl.fovis.common.MenuExtender
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage wszystkie = new I18nMessagePlEn("Wszystkie", "All") /* I18N:  */; // #540
    public static final I18nMessage wszystkieDrzewa = new I18nMessagePlEn("Wszystkie drzewa", "All trees") /* I18N:  */; // #540
    public static final I18nMessage wszystkieAtrybuty = new I18nMessagePlEn("Wszystkie atrybuty", "All attributes") /* I18N:  */; // #540
    public static final I18nMessage wszystkieObiekty = new I18nMessagePlEn("Wszystkie obiekty", "All objects") /* I18N:  */; // #540
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.entitydetailswidgets.MetadataWidgetFlat
    // pl.fovis.client.entitydetailswidgets.MetadataRolesWidgetFlat
    public static final I18nMessage wszystko = new I18nMessagePlEn("Wszystko", "All types") /* I18N:  */; // #541
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.dialogs.TreesDialog
    // pl.fovis.client.dialogs.PickIconDialog
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage wybierz = new I18nMessagePlEn("Wybierz", "Select") /* I18N:  */; // #542
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedDictionaryWidgetBase
    public static final I18nMessage wybierzAtrybut = new I18nMessagePlEn("Wybierz atrybut", "Select attribute") /* I18N:  */; // #543
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreesDialog
    public static final I18nMessage wybierzDrzewaUzytkownika = new I18nMessagePlEn("Wybierz drzewa użytkownika", "Select trees for user") /* I18N:  */; // #544
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.PickIconDialog
    public static final I18nMessage wybierzIkone = new I18nMessagePlEn("Wybierz ikonę", "Select icon") /* I18N:  */; // #545
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage wybierzInstancje = new I18nMessagePlEn("Wybierz serwer", "Select server") /* I18N:  */; // #546
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.AttrOrRoleEditDialog
    // pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog
    public static final I18nMessage wybierzKategorie = new I18nMessagePlEn("Wybierz kategorie", "Select category") /* I18N:  */; // #547
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage wybierzPozycjeWMenuWJakiejUmiesc = new I18nMessagePlEn("Wybierz pozycję w menu w jakiej umieścić drzewo", "Select location in menu for tree placement") /* I18N:  */; // #548
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.RolesDialog
    public static final I18nMessage wybierzRole = new I18nMessagePlEn("Wybierz rolę", "Select role") /* I18N:  */; // #549
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.ConnectionDatabaseNameDialog
    public static final I18nMessage wybierzSerwerBazeDanychOrazDomys = new I18nMessagePlEn("Wybierz serwer, bazę danych oraz domyślny schemat", "Select server, database and default schema") /* I18N:  */; // #550
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.ConnectionServerDialog
    public static final I18nMessage wybierzSerwerOracle = new I18nMessagePlEn("Wybierz serwer Oracle", "Select Oracle server") /* I18N:  */; // #551
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    public static final I18nMessage wybierzTypDrzewa = new I18nMessagePlEn("Wybierz typ drzewa", "Select tree type") /* I18N:  */; // #552
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikwidgets.JoinTypeRolesWidget
    // pl.fovis.client.dialogs.TreeSelectorForJoinedObjectsDialogNew
    public static final I18nMessage wybierzTypPowiazania = new I18nMessagePlEn("Wybierz typ powiązania", "Select association type") /* I18N:  */; // #553
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.PickUserDialogBase
    public static final I18nMessage wybierzUzytkownika = new I18nMessagePlEn("Wybierz użytkownika", "Select user") /* I18N:  */; // #554
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.PickBlogEntryDialog
    public static final I18nMessage wybierzWpisBlogowy = new I18nMessagePlEn("Wybierz wpis blogowy", "Select blog entry") /* I18N:  */; // #555
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage wybierzZKategoriElementPodrzedn = new I18nMessagePlEn("Wybierz z kategorii element podrzędny", "Select descendant item from one of main categories") /* I18N:  */; // #556
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage wybierzZakladkiTypyObiektowDoPrz = new I18nMessagePlEn("Wybierz zakładki/typy obiektów do przeszukiwania", "Select tabs/object types to search for") /* I18N:  */; // #557
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeSelectorForAttachments
    public static final I18nMessage wybierzZalacznik = new I18nMessagePlEn("Wybierz załącznik", "Select attachment") /* I18N:  */; // #558
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.JoinTypeRolesWidget
    public static final I18nMessage wybranycObiektacIObiektacPodrzed = new I18nMessagePlEn("wybranych obiektach  i obiektach podrzędnych", "selected and descendant objects") /* I18N:  */; // #559
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.JoinTypeRolesWidget
    public static final I18nMessage wybranychObiektach = new I18nMessagePlEn("wybranych obiektach", "selected objects") /* I18N:  */; // #560
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.JoinTypeRolesWidget
    public static final I18nMessage wybranymObiekcie = new I18nMessagePlEn("wybranym obiekcie", "selected object") /* I18N:  */; // #561
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage wyczyscFiltr = new I18nMessagePlEn("Wyczyść filtr", "Clear filter") /* I18N:  */; // #562
    public static final I18nMessage proszeUzupelnicWszyskieDane = new I18nMessagePlEn("Proszę uzupełnić wszyskie dane", "Please complete all fields") /* I18N:  */; // #562
// is translated
    // single occurrence
    // pl.fovis.client.bikpages.TreeBikPage
    public static final I18nMessage wyczyscFiltrZwinWszystko = new I18nMessagePlEn("Wyczyść filtr / zwiń wszystko", "Clear filter/hide all") /* I18N:  */; // #563
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.AttrHintDialog
    // pl.fovis.client.BIKClientSingletons
    public static final I18nMessage wyjasnienie = new I18nMessagePlEn("Wyjaśnienie", "Explanation") /* I18N:  */; // #564
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.bikwidgets.BikRequestChartOld
    // pl.fovis.client.bikwidgets.BikRequestChart
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage wykonania = new I18nMessagePlEn("Wykonania testu", "Test executions") /* I18N:  */; // #565
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage wykresWykonan = new I18nMessagePlEn("Wykres wykonań testu", "Test execution chart") /* I18N:  */; // #566
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.BikRequestChartOld
    public static final I18nMessage wykresWykonanTestu = new I18nMessagePlEn("Wykres wykonań testu", "Test execution chart") /* I18N:  */; // #567
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.bikpages.SystemUserBeanExtractor
    // pl.fovis.client.dialogs.SystemUserDialog
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage wylaczony = new I18nMessagePlEn("Wyłączony", "Disabled") /* I18N:  */; // #568
    // is translated
    // single occurrence
    // pl.fovis.client.menu.MenuBuilder
    public static final I18nMessage wyloguj = new I18nMessagePlEn("Wyloguj", "Log out") /* I18N:  */; // #569
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage wynikiDla = new I18nMessagePlEn("Wyniki dla", "Results for") /* I18N:  */; // #570
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.FileUploadDialogForDocuments
    public static final I18nMessage wyslij = new I18nMessagePlEn("Wyślij", "Send") /* I18N:  */; // #571
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage wystepowanie = new I18nMessagePlEn("Występowanie", "Occurrence") /* I18N:  */; // #572
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage wystepowanieW_yes = new I18nMessagePlEn("Występowanie w", "Occurrence in") /* I18N: yes */; // #573
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.BaseBIKTreeWidget
    public static final I18nMessage wyswietlam = new I18nMessagePlEn("wyświetlam", "displaying") /* I18N:  */; // #574
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.BaseBIKTreeWidget
    public static final I18nMessage wyswietlamZnalezionePozycje = new I18nMessagePlEn("Wyświetlam znalezione pozycje", "Displaying found items") /* I18N:  */; // #575
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage wyszukiwMozeZwracacTerazCzesciow = new I18nMessagePlEn("wyszukiwarka może zwracać teraz częściowe lub nieaktualne wyniki", "search engine can return partial or outdated results") /* I18N:  */; // #576
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage wyszukiwZakonczoWszystkiWynikow = new I18nMessagePlEn("Wyszukiwanie zakończone, wszystkich wyników", "Finished searching, total results") /* I18N:  */; // #577
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage wyszukiwanieZostaloPrzerwane = new I18nMessagePlEn("wyszukiwanie zostało przerwane", "search cancelled") /* I18N:  */; // #578
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.BaseBIKTreeWidget
    public static final I18nMessage wyszukuje = new I18nMessagePlEn("Wyszukuję", "Searching") /* I18N:  */; // #579
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionCut
    public static final I18nMessage wytnij = new I18nMessagePlEn("Wytnij", "Cut") /* I18N:  */; // #580
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.ChangeCategoryDialog
    public static final I18nMessage zKategorii = new I18nMessagePlEn("z kategorii", "from category") /* I18N:  */; // #581
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikwidgets.UserRankPageGrid
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage z_noSingleLetter = new I18nMessagePlEn("z", "of") /* I18N: x users out of y users */; // #582
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.MyBIKSFavouriteGrid
    public static final I18nMessage zaktualizowane = new I18nMessagePlEn("Zaktualizowane", "Updated") /* I18N:  */; // #583
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.HelpDialog
    public static final I18nMessage zaktualizowanoPomoc = new I18nMessagePlEn("Zaktualizowano pomoc", "Help updated") /* I18N:  */; // #584
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.MyBIKSFavouriteGrid
    public static final I18nMessage zaktualizowany = new I18nMessagePlEn("Zaktualizowany", "Updated") /* I18N:  */; // #585
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.StatisticsLoggedUserPage
    public static final I18nMessage zalogowaniUzytkownicy = new I18nMessagePlEn("Zalogowani użytkownicy", "Logged users") /* I18N:  */; // #586
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.LoginDialog
    // pl.fovis.client.menu.MenuBuilder
    public static final I18nMessage zaloguj = new I18nMessagePlEn("Zaloguj", "Log in") /* I18N:  */; // #587
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.LoginDialog
    public static final I18nMessage zalogujSieKlasycznPodajacLoginIH = new I18nMessagePlEn("Zaloguj się klasycznie, podając login i hasło", "For enterprise logon, give login and password") /* I18N:  */; // #588
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionChangeToBranch
    public static final I18nMessage zamienNaGalaz = new I18nMessagePlEn("Zamień na", "Change to") /* I18N:  */; // #589
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionChangeToLeaf
    public static final I18nMessage zamienNaLisc = new I18nMessagePlEn("Zamień na", "Change to") /* I18N:  */; // #590
    // is translated
    // single occurrence
    // pl.fovis.common.JoinTypeRoles
    public static final I18nMessage zamienNaPomocnicze = new I18nMessagePlEn("zamień na pomocnicze", "replace to auxiliary") /* I18N: wtf? */; // #591
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.JoinTypeRolesWidget
    public static final I18nMessage zamienNaPomocniczeLubUsun = new I18nMessagePlEn("Zamień na pomocnicze lub usuń", "Replace to auxiliary or delete") /* I18N: wtf? */; // #592
    // is translated
    // single occurrence
    // pl.fovis.common.JoinTypeRoles
    public static final I18nMessage zamienNaPomocniczeW = new I18nMessagePlEn("Zamień na pomocnicze w", "Replace to auxiliary in") /* I18N: wtf? */; // #593
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeAndDictionarySelectorForUsers
    public static final I18nMessage zamienisRoleGlowneNaPomocnicDla = new I18nMessagePlEn("Zamienisz role główne na pomocnicze dla", "You will change main roles to auxiliary for") /* I18N:  */; // #594
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.dialogs.HomePageUserTypeDesc
    // pl.fovis.client.dialogs.AttrHintDialog
    // pl.fovis.client.dialogs.EntityDetailsPaneDialog
    public static final I18nMessage zamknij = new I18nMessagePlEn("Zamknij", "Close") /* I18N:  */; // #595
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.ConnectionDatabaseNameDialog
    public static final I18nMessage zapisanoBazeDanychDlaPolaczenia = new I18nMessagePlEn("Zapisano bazę danych dla połączenia", "Saved database for connection") /* I18N:  */; // #596
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.ConnectionServerDialog
    public static final I18nMessage zapisanoSerwerDlaPolaczenia = new I18nMessagePlEn("Zapisano serwer dla połączenia", "Server for the connection has been saved") /* I18N:  */; // #597
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.bikwidgets.CategorizedSystemAttrDictWidget
    // pl.fovis.client.bikpages.lisa.LisaConnectionPage
    // pl.fovis.client.bikwidgets.CategorizedAddAttrDictWidget
    public static final I18nMessage zapisanoZmiany = new I18nMessagePlEn("Zapisano zmiany", "Changes have been saved") /* I18N:  */; // #598
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.dialogs.TreeSelectorForUserRolesDialog
    // pl.fovis.client.dialogs.TreeSelectorForJoinedObjectsDialogNew
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage zapisywaniePowiazanProszeCzekac = new I18nMessagePlEn("Zapisywanie powiązań, proszę czekać", "Saving associations, please wait") /* I18N:  */; // #599
    // is translated
    // multiple occurrences: 23
    // pl.fovis.client.dialogs.DefDialog
    // pl.fovis.client.dialogs.AddOrUpdateItemNameDialog
    // pl.fovis.client.dialogs.EditEntityKeywordsDialog
    // pl.fovis.client.dialogs.BaseActionOrCancelDialogEx
    // pl.fovis.client.dialogs.lisa.LisaDictDialog
    // pl.fovis.client.dialogs.UserDialog
    // pl.fovis.client.dialogs.lisa.LisaDictionaryColumnEditionDialog
    // pl.fovis.client.dialogs.AddOrUpdateAttributeAndTheirKinds
    // pl.fovis.client.entitydetailswidgets.AdminConfigSASWidget
    // pl.fovis.client.dialogs.AttrOrRoleEditDialog
    // pl.fovis.client.dialogs.lisa.SerializableLisaDialogBase
    // pl.fovis.client.entitydetailswidgets.AdminConfigSapBWWidget
    // pl.fovis.client.entitydetailswidgets.AdminConfigADWidget
    // pl.fovis.client.dialogs.AddOrEditAttributeDialog
    // pl.fovis.client.dialogs.TreeKindDialog
    // pl.fovis.client.dialogs.SystemUserDialog
    // pl.fovis.client.entitydetailswidgets.AdminConfigTeradataWidget
    // pl.fovis.client.entitydetailswidgets.AdminConfigDQCWidget
    // pl.fovis.client.entitydetailswidgets.AdminConfigDBBaseWidget
    // pl.fovis.client.bikpages.lisa.LisaConnectionPage
    // pl.fovis.client.dialogs.BaseRichTextAndTextboxDialog
    // pl.fovis.client.dialogs.DataDicionaryForTreeKindDialog
    // pl.fovis.client.dialogs.TreeSelectorDialogBase
    public static final I18nMessage zapisz = new I18nMessagePlEn("Zapisz", "Save") /* I18N:  */; // #600
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.bikpages.AdminSchedulePage
    // pl.fovis.client.bikwidgets.CategorizedSystemAttrDictWidget
    // pl.fovis.client.bikwidgets.CategorizedAddAttrDictWidget
    public static final I18nMessage zapiszZmiany = new I18nMessagePlEn("Zapisz zmiany", "Save changes") /* I18N:  */; // #601
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.ViewSQLDialog
    public static final I18nMessage zapytanieSQL = new I18nMessagePlEn("Zapytanie SQL", "SQL query") /* I18N:  */; // #602
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage zasilaniJestJuzUruchomiLubNieWsz = new I18nMessagePlEn("Zasilanie jest już uruchomione lub nie wszystkie fazy zostały ukończone. Czy na pewno chcesz je uruchomić ponownie (wybrać tylko w sytuacji przerwania poprzedniego połączenia, np. w skutek awarii lub przerwania działania", "Data load is already running but not all its phases are completed. Are you sure you want to restart it (choose only in case of abnormal termination of the previous call, e.g. due to a malfunction or if it stopped working") /* I18N:  */; // #603
    // is translated
    // single occurrence
    // pl.fovis.client.BIKGWTConstants
    public static final I18nMessage zasilania = new I18nMessagePlEn("Zasilania", "Data loads") /* I18N:  */; // #604
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.DataSourceStatusBeanExtractor
    public static final I18nMessage zasilanieWTrakcie = new I18nMessagePlEn("Zasilanie w trakcie", "Data load in progress") /* I18N:  */; // #605
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage zatrzymany = new I18nMessagePlEn("zatrzymany", "stopped") /* I18N:  */; // #606
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.RateDialog
    public static final I18nMessage zatwierdzOcene = new I18nMessagePlEn("Zatwierdź ocenę", "Submit rating") /* I18N:  */; // #607
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.lisa.LisaDictionaryContent
    public static final I18nMessage zaznaczPozycjeDoPrzeniesienia = new I18nMessagePlEn("Zaznacz pozycje do przeniesienia", "Select items to move") /* I18N:  */; // #608
    public static final I18nMessage zaznaczPozycjeDoSkasowania = new I18nMessagePlEn("Zaznacz pozycje do skasowania", "Select items to delete") /* I18N:  */;
    public static final I18nMessage proszeUzupelnickolumnyZKluczem = new I18nMessagePlEn("Proszę uzupełnić kolmumny z kluczem", "Pleas feel the comulms with key") /* I18N:  */;
    public static final I18nMessage nicNieZmieniono = new I18nMessagePlEn("Nic nie zmieniono!", "Nothing to change!") /* I18N:  */;
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedAddAttrDictWidget
    public static final I18nMessage zaznaczWezel = new I18nMessagePlEn("zaznacz węzeł", "select node") /* I18N:  */; // #609
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewSearchBikPage
    public static final I18nMessage zaznaczWybraneTypyObiektowDoFilt = new I18nMessagePlEn("Zaznacz wybrane typy obiektów do filtrowania powyżej lub \'Wszystkie", "Select object types for filtering above or select 'All") /* I18N:  */; // #610
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionEditDescription
    public static final I18nMessage zedytowanoOpis = new I18nMessagePlEn("Zedytowano opis", "Description modified") /* I18N:  */; // #611
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    // pl.fovis.client.dialogs.lisa.LisaBeanDisplay
    // pl.fovis.client.dialogs.TreeNodeDialog
    public static final I18nMessage zmien = new I18nMessagePlEn("Zmień", "Modify") /* I18N:  */; // #612
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.PickAuthorBlogDialog
    public static final I18nMessage zmienAdministratoraBloga = new I18nMessagePlEn("Zmień administratora bloga", "Change blog admin") /* I18N:  */; // #613
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SAPBOExtradataWidget
    public static final I18nMessage zmienBazeDanych = new I18nMessagePlEn("Zmień bazę danych", "Change database") /* I18N:  */; // #614
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    public static final I18nMessage zmienDane = new I18nMessagePlEn("Zmień dane", "Modify data") /* I18N:  */; // #615
    // is translated
    // single occurrence
    // pl.fovis.client.menu.MenuBuilder
    public static final I18nMessage zmienJezyk = new I18nMessagePlEn("Zmień język", "Switch language") /* I18N:  */; // #616
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.dialogs.lisa.ChangeCategoryDialog
    // pl.fovis.client.entitydetailswidgets.lisa.LisaDictionaryContent
    public static final I18nMessage zmienKategorie = new I18nMessagePlEn("Zmień kategorię", "Change category") /* I18N:  */; // #617
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.lisa.ChangeCategoryDialog
    public static final I18nMessage zmienKategorieWybranychWezlow = new I18nMessagePlEn("Zmień kategorię wybranych węzłów", "Change category for selected nodes") /* I18N:  */; // #618
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionRename
    public static final I18nMessage zmienNazwe = new I18nMessagePlEn("Zmień nazwę", "Rename") /* I18N:  */; // #619
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionRenameRole
    public static final I18nMessage zmienNazweRoli = new I18nMessagePlEn("Zmień nazwę roli", "Rename role") /* I18N:  */; // #620
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.entitydetailswidgets.UserInNodeWidgetFlatBase
    // pl.fovis.client.nodeactions.NodeActionLinkUserToRole
    public static final I18nMessage zmienRole = new I18nMessagePlEn("Zmień rolę", "Change role") /* I18N:  */; // #621
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.SAPBOExtradataWidget
    public static final I18nMessage zmienSerwer = new I18nMessagePlEn("Zmień serwer", "Change server") /* I18N:  */; // #622
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AdminConfigADWidget
    public static final I18nMessage zmienionParametrDoPolaczenZAD = new I18nMessagePlEn("Zmieniono parametry do połączenia z AD", "Parameters to connect to AD have been changed") /* I18N:  */; // #623
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AdminConfigDQCWidget
    public static final I18nMessage zmienionParametrDoPolaczenZDQC = new I18nMessagePlEn("Zmieniono parametry do połączenia z DQC", "Parameters to connect to DQC have been changed") /* I18N:  */; // #624
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AdminConfigSapBWWidget
    public static final I18nMessage zmienionParametrDoPolaczenZSAPBW = new I18nMessagePlEn("Zmieniono parametry do połączenia z SAP BW", "Parameters to connect to SAP BW have been changed") /* I18N:  */; // #625
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AdminConfigSASWidget
    public static final I18nMessage zmienionParametrDoPolaczenZSAS = new I18nMessagePlEn("Zmieniono parametry do połączenia z SAS", "Parameters to connect to SAS have been changed") /* I18N:  */; // #626
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AdminConfigTeradataWidget
    public static final I18nMessage zmienionParametrDoPolaczenZTerad = new I18nMessagePlEn("Zmieniono parametry do połączenia z Teradatą", "Parameters to connect to Teradata have been changed") /* I18N:  */; // #627
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AuthorBeanExtractor
    public static final I18nMessage zmienionoAutora = new I18nMessagePlEn("Zmieniono autora", "Author has been changed") /* I18N:  */; // #628
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionChangeToLeaf
    public static final I18nMessage zmienionoGalazNaLisc = new I18nMessagePlEn("Zmieniono gałąź na liść", "Branch has been converted to leaf") /* I18N:  */; // #629
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminSchedulePage
    public static final I18nMessage zmienionoHarmonogram = new I18nMessagePlEn("Zmieniono harmonogram", "Schedule has been modified") /* I18N:  */; // #630
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionChangeToBranch
    public static final I18nMessage zmienionoLiscNaGalaz = new I18nMessagePlEn("Zmieniono liść na gałąź", "Leaf has been converted to branch") /* I18N:  */; // #631
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.nodeactions.NodeActionRenameRole
    // pl.fovis.client.nodeactions.NodeActionRename
    public static final I18nMessage zmienionoNazwe = new I18nMessagePlEn("Zmieniono nazwę", "Name modified") /* I18N:  */; // #632
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.bikpages.HelpHintExtractor
    // pl.fovis.client.bikpages.AttrHintExtractor
    // pl.fovis.client.bikpages.HelpPageTree
    public static final I18nMessage zmienionoOpis = new I18nMessagePlEn("Zmieniono opis", "Description modified") /* I18N:  */; // #633
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.AdminConfigDBBaseWidget
    public static final I18nMessage zmienionoParametryDoPolaczenia = new I18nMessagePlEn("Zmieniono parametry do połączenia", "Connection parameters have been modified") /* I18N:  */; // #634
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.UserInNodeBeanExtractor
    public static final I18nMessage zmienionoUzytkownika = new I18nMessagePlEn("Zmieniono użytkownika", "User modified") /* I18N:  */; // #635
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    public static final I18nMessage zmienionoZdjecie = new I18nMessagePlEn("Zmieniono zdjęcie", "Photo modified") /* I18N:  */; // #636
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.NewsActionExtractor
    public static final I18nMessage zmodyfikowanoAktualnosc = new I18nMessagePlEn("Zmodyfikowano komunikat", "Announcement modified") /* I18N:  */; // #637
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedAddAttrDictWidget
    public static final I18nMessage zmodyfikowanoAtrybut = new I18nMessagePlEn("Zmodyfikowano atrybut", "Attribure modified") /* I18N:  */; // #638
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.MyBIKSEditUserTab
    public static final I18nMessage zmodyfikowanoDane = new I18nMessagePlEn("Zmodyfikowano dane", "Data modified") /* I18N:  */; // #639
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.NodeActionEditDefinition
    public static final I18nMessage zmodyfikowanoDefinicje = new I18nMessagePlEn("Zmodyfikowano definicję", "Definition modified") /* I18N:  */; // #640
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.TreeBeanExtractor
    public static final I18nMessage zmodyfikowanoDrzewo = new I18nMessagePlEn("Zmodyfikowano drzewo", "Tree modified") /* I18N:  */; // #641
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.CategorizedRoleForNodeDictWidget
    public static final I18nMessage zmodyfikowanoRole = new I18nMessagePlEn("Zmodyfikowano role", "Role modified") /* I18N:  */; // #642
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.TreeKindDialog
    public static final I18nMessage zmodyfikowanoTypDrzewa = new I18nMessagePlEn("Zmodyfikowano typ drzewa", "Tree type modified") /* I18N:  */; // #643
    // is translated
    // multiple occurrences: 3
    // pl.fovis.client.dialogs.UserDialog
    // pl.fovis.client.bikpages.SystemUserBeanExtractor
    // pl.fovis.client.dialogs.SystemUserDialog
    public static final I18nMessage zmodyfikowanoUzytkownika = new I18nMessagePlEn("Zmodyfikowano użytkownika", "User modified") /* I18N:  */; // #644
    // is translated
    // single occurrence
    // pl.fovis.client.bikwidgets.MyBIKSFavouriteGrid
    public static final I18nMessage zmodyfikowanychElementow = new I18nMessagePlEn("zmodyfikowanych elementów", "modified items") /* I18N:  */; // #645
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.BaseBIKTreeWidget
    public static final I18nMessage znalezionychPozycji = new I18nMessagePlEn("Znalezionych pozycji", "Found items") /* I18N:  */; // #646
    // is translated
    // single occurrence
    // pl.fovis.client.entitydetailswidgets.MainHeaderWidget
    public static final I18nMessage zobaczDokument = new I18nMessagePlEn("Zobacz dokument", "View document") /* I18N:  */; // #647
    // is translated
    // single occurrence
    // pl.fovis.client.BIKClientSingletons
    public static final I18nMessage zobaczTez = new I18nMessagePlEn("Zobacz też", "See also") /* I18N:  */; // #648
    // is translated
    // single occurrence
    // pl.fovis.client.nodeactions.lisa.NodeActionAddLisaCategory
    public static final I18nMessage zostalaDodana = new I18nMessagePlEn("została dodana", "has been added") /* I18N:  */; // #649
    // is translated
    // single occurrence
    // pl.fovis.client.dialogs.MetapediaDialog
    public static final I18nMessage zostanieZastapiona = new I18nMessagePlEn("zostanie zastąpiona", "will be replaced") /* I18N:  */; // #650
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage zresetowanoProcesZasilajacy = new I18nMessagePlEn("Zresetowano proces zasilający", "Data load process has been restarted") /* I18N: reset vs restart? */; // #651
    // is translated
    // single occurrence
    // pl.fovis.client.bikpages.AdminRunPage
    public static final I18nMessage zresetujProcesZasilajacy = new I18nMessagePlEn("Zresetuj proces zasilający", "Restart data load process") /* I18N: reset vs restart? */; // #652
    // is translated
    // multiple occurrences: 2
    // pl.fovis.client.bikpages.MyBIKSHomePageTab
    // pl.fovis.client.bikpages.SystemUserPage
    public static final I18nMessage zwyklyUzytkownik = new I18nMessagePlEn("Zwykły użytkownik", "Regular user") /* I18N:  */; // #653
    // not translated
    // no occurrences
    public static final I18nMessage agregate = new I18nMessagePlEn("Agregate") /* i18n:  */; // #654
    // not translated
    // no occurrences
    public static final I18nMessage applicationObject = new I18nMessagePlEn("Application Object") /* i18n:  */; // #655
    // not translated
    // no occurrences
    public static final I18nMessage aqqGetBikAllArtsDoneRes_noMaybeIdent = new I18nMessagePlEn("aqq getBikAllArts! done! res") /* i18n: no?maybe-ident */; // #656
    // not translated
    // no occurrences
    public static final I18nMessage aqqGetBikAllArtsStart_noMaybeIdent = new I18nMessagePlEn("aqq getBikAllArts! start") /* i18n: no?maybe-ident */; // #657
    // not translated
    // no occurrences
    public static final I18nMessage author = new I18nMessagePlEn("Author") /* i18n:  */; // #658
    // not translated
    // no occurrences
    public static final I18nMessage autorCreatedBy = new I18nMessagePlEn("Autor [Created by") /* i18n:  */; // #659
    // not translated
    // no occurrences
    public static final I18nMessage bikcenter = new I18nMessagePlEn("bikcenter") /* i18n:  */; // #660
    // not translated
    // no occurrences
    public static final I18nMessage butPleaseDoNotWorryCauseThisAppS = new I18nMessagePlEn("But please do not worry, cause this app should work quite properly (kind of") /* i18n:  */; // #661
    // not translated
    // no occurrences
    public static final I18nMessage cN = new I18nMessagePlEn("CN") /* i18n:  */; // #662
    // not translated
    // no occurrences
    public static final I18nMessage clientNumber = new I18nMessagePlEn("Client number") /* i18n:  */; // #663
    // not translated
    // no occurrences
    public static final I18nMessage czyNaPewnoUsunacElementDivClassP = new I18nMessagePlEn("Czy na pewno usunąć element" + ": <div class='PUWelement'><img src='images") /* i18n:  */; // #664
    // not translated
    // no occurrences
    public static final I18nMessage czyNaPewnoUsunacZUlubionyElement = new I18nMessagePlEn("Czy na pewno usunąć z ulubionych element: <div class='Delete-Element'><img src") /* i18n:  */; // #665
    // not translated
    // no occurrences
    public static final I18nMessage d_noSingleLetter = new I18nMessagePlEn("D") /* i18n: no?single-letter */; // #666
    // not translated
    // no occurrences
    public static final I18nMessage dataOstatniegoUruchomienia = new I18nMessagePlEn("Data ostatniego uruchomienia") /* i18n:  */; // #667
    // not translated
    // no occurrences
    public static final I18nMessage databaseEngine = new I18nMessagePlEn("Database engine") /* i18n:  */; // #668
    // not translated
    // no occurrences
    public static final I18nMessage db = new I18nMessagePlEn("db") /* i18n:  */; // #669
    // not translated
    // no occurrences
    public static final I18nMessage doObejrzenFilmuPotrzebnJestFlash_ok = new I18nMessagePlEn("Do obejrzenia filmu potrzebny jest Flash Player w wersji 9.0 lub wyższej."
            + "<a href=\"http://get.adobe.com/pl/flashplayer/\"> Pobierz Flash Player") /* i18n: ok */; // #670
    // not translated
    // no occurrences
    public static final I18nMessage do_ = new I18nMessagePlEn("do") /* i18n:  */; // #671
    // not translated
    // no occurrences
    public static final I18nMessage docSender = new I18nMessagePlEn("Doc Sender") /* i18n:  */; // #672
    // is translated
    // no occurrences
    public static final I18nMessage dodajNowy = new I18nMessagePlEn("Dodaj nowy", "Add new") /* I18N:  */; // #673
    // is translated
    // no occurrences
    public static final I18nMessage dodanoNowySerwer = new I18nMessagePlEn("Dodano nowy serwer", "New server added") /* I18N:  */; // #674
    // not translated
    // no occurrences
    public static final I18nMessage doneNewTeradataConnection = new I18nMessagePlEn("done new teradata connection") /* i18n:  */; // #675
    // not translated
    // no occurrences
    public static final I18nMessage edycja2 = new I18nMessagePlEn("edycja") /* i18n:  */; // #676
    // not translated
    // no occurrences
    public static final I18nMessage endRequestIsSent = new I18nMessagePlEn("end - request is sent") /* i18n:  */; // #677
    // not translated
    // no occurrences
    public static final I18nMessage endtime = new I18nMessagePlEn("Endtime") /* i18n:  */; // #678
    // not translated
    // no occurrences
    public static final I18nMessage failedWithTeradataConnection = new I18nMessagePlEn("failed with teradata connection") /* i18n:  */; // #679
    // not translated
    // no occurrences
    public static final I18nMessage filtr = new I18nMessagePlEn("Filtr") /* i18n:  */; // #680
    // is translated
    // no occurrences
    public static final I18nMessage firma = new I18nMessagePlEn("Firma", "Company") /* I18N:  */; // #681
    // not translated
    // no occurrences
    public static final I18nMessage flags = new I18nMessagePlEn("Flags") /* i18n:  */; // #682
    // is translated
    // no occurrences
    public static final I18nMessage formaty = new I18nMessagePlEn("Formaty", "Formats") /* I18N:  */; // #683
    // not translated
    // no occurrences
    public static final I18nMessage glossary = new I18nMessagePlEn("Glossary") /* i18n:  */; // #684
    // not translated
    // no occurrences
    public static final I18nMessage godzinaNastepnegoUruchomienia = new I18nMessagePlEn("Godzina następnego uruchomienia") /* i18n:  */; // #685
    // not translated
    // no occurrences
    public static final I18nMessage godzinaUtworzenia = new I18nMessagePlEn("Godzina utworzenia") /* i18n:  */; // #686
    // not translated
    // no occurrences
    public static final I18nMessage godzinaWygasniecia = new I18nMessagePlEn("Godzina wygaśnięcia") /* i18n:  */; // #687
    // not translated
    // no occurrences
    public static final I18nMessage gotRowsFromServerCnt = new I18nMessagePlEn("got rows from server, cnt") /* i18n:  */; // #688
    // not translated
    // no occurrences
    public static final I18nMessage iDCUID = new I18nMessagePlEn("ID, CUID") /* i18n:  */; // #689
    // is translated
    // no occurrences
    public static final I18nMessage imie = new I18nMessagePlEn("Imię", "First name") /* I18N:  */; // #691
    // is translated
    // no occurrences
    public static final I18nMessage inicjaly = new I18nMessagePlEn("Inicjały", "Initials") /* I18N:  */; // #692
    // not translated
    // no occurrences
    public static final I18nMessage innyFax = new I18nMessagePlEn("Inny fax") /* i18n:  */; // #693
    // not translated
    // no occurrences
    public static final I18nMessage innyKomorkowy = new I18nMessagePlEn("Inny komórkowy") /* i18n:  */; // #694
    // not translated
    // no occurrences
    public static final I18nMessage innyPager = new I18nMessagePlEn("Inny pager") /* i18n:  */; // #695
    // not translated
    // no occurrences
    public static final I18nMessage innyTelefon = new I18nMessagePlEn("Inny telefon") /* i18n:  */; // #696
    // not translated
    // no occurrences
    public static final I18nMessage innyTelefonIP = new I18nMessagePlEn("Inny telefon IP") /* i18n:  */; // #697
    // not translated
    // no occurrences
    public static final I18nMessage istniejeJuzTakaNazwaSerweraSerwe = new I18nMessagePlEn("Istnieje już taka nazwa serwera! Serwer nie został dodany") /* i18n:  */; // #698
    // not translated
    // no occurrences
    public static final I18nMessage jestOKZgodne = new I18nMessagePlEn("Jest OK, zgodne") /* i18n:  */; // #699
    // not translated
    // no occurrences
    public static final I18nMessage kodPocztowy = new I18nMessagePlEn("Kod pocztowy") /* i18n:  */; // #700
    // not translated
    // no occurrences
    public static final I18nMessage krajISO = new I18nMessagePlEn("Kraj ISO") /* i18n:  */; // #701
    // not translated
    // no occurrences
    public static final I18nMessage krajRegion = new I18nMessagePlEn("Kraj/region") /* i18n:  */; // #702
    // not translated
    // no occurrences
    public static final I18nMessage language = new I18nMessagePlEn("Language") /* i18n:  */; // #703
    // not translated
    // no occurrences
    public static final I18nMessage liczbaObiektowPodrzednChildren = new I18nMessagePlEn("Liczba obiektów podrzędnych (children") /* i18n:  */; // #704
    // not translated
    // no occurrences
    //public static final I18nMessage like = new I18nMessagePlEn("like") /* i18n:  */; // #705
    // not translated
    // no occurrences
    public static final I18nMessage loadBOMetadataWithUsersReports = new I18nMessagePlEn("Load BO metadata with Users Reports") /* i18n:  */; // #706
    // not translated
    // no occurrences
    public static final I18nMessage manager = new I18nMessagePlEn("Manager") /* i18n:  */; // #707
    // not translated
    // no occurrences
    public static final I18nMessage many = new I18nMessagePlEn("many") /* i18n:  */; // #708
    // is translated
    // no occurrences
    public static final I18nMessage miasto = new I18nMessagePlEn("Miasto", "City") /* I18N:  */; // #709
    // not translated
    // no occurrences
    public static final I18nMessage miejsceDocelowe = new I18nMessagePlEn("Miejsce docelowe") /* i18n:  */; // #710
    // not translated
    // no occurrences
    public static final I18nMessage nameAutor = new I18nMessagePlEn("name=Autor") /* i18n:  */; // #711
    // not translated
    // no occurrences
    public static final I18nMessage nameNazwa = new I18nMessagePlEn("name=Nazwa") /* i18n:  */; // #712
    // is translated
    // no occurrences
    public static final I18nMessage nazwaLogowania = new I18nMessagePlEn("Nazwa logowania", "Login name") /* I18N:  */; // #713
    // not translated
    // no occurrences
    public static final I18nMessage nazwaPliku = new I18nMessagePlEn("Nazwa pliku") /* i18n:  */; // #714
    // not translated
    // no occurrences
    public static final I18nMessage nazwaSkrocona = new I18nMessagePlEn("Nazwa skrócona") /* i18n:  */; // #715
    // is translated
    // no occurrences
    public static final I18nMessage nazwaTechniczna = new I18nMessagePlEn("Nazwa techniczna", "Technical name") /* I18N:  */; // #716
    // is translated
    // no occurrences
    public static final I18nMessage nazwisko = new I18nMessagePlEn("Nazwisko", "Last name") /* I18N:  */; // #717
    // not translated
    // no occurrences
    public static final I18nMessage networkLayer = new I18nMessagePlEn("Network layer") /* i18n:  */; // #718
    // not translated
    // no occurrences
    public static final I18nMessage nextruntime = new I18nMessagePlEn("Nextruntime") /* i18n:  */; // #719
    // not translated
    // no occurrences
    public static final I18nMessage nieMoznaUsunacOstatniegoSerwera = new I18nMessagePlEn("Nie można usunąć ostatniego serwera") /* i18n:  */; // #720
    // not translated
    // no occurrences
    public static final I18nMessage nieSaZgodneLocaleLangsWynikZImpl = new I18nMessagePlEn("Nie są zgodne localeLangs, wynik z impl") /* i18n:  */; // #721
    // not translated
    // no occurrences
    public static final I18nMessage noSuchMethod = new I18nMessagePlEn("no such method") /* i18n:  */; // #722
    // not translated
    // no occurrences
    public static final I18nMessage none = new I18nMessagePlEn("none") /* i18n:  */; // #723
    // not translated
    // no occurrences
    public static final I18nMessage nowy2 = new I18nMessagePlEn("nowy") /* i18n:  */; // #724
    // not translated
    // no occurrences
    public static final I18nMessage numerTelefonu = new I18nMessagePlEn("Numer telefonu", "Phone number") /* i18n:  */; // #725
    // not translated
    // no occurrences
    public static final I18nMessage obtype = new I18nMessagePlEn("Obtype") /* i18n:  */; // #726
    // not translated
    // no occurrences
    public static final I18nMessage official = new I18nMessagePlEn("official") /* i18n:  */; // #727
    // not translated
    // no occurrences
    public static final I18nMessage oficjalny = new I18nMessagePlEn("Oficjalny") /* i18n:  */; // #728
    // not translated
    // no occurrences
    public static final I18nMessage opublikowanoWRepozytoriumBO = new I18nMessagePlEn("Opublikowano w repozytorium BO") /* i18n:  */; // #729
    // not translated
    // no occurrences
    public static final I18nMessage oracleServer = new I18nMessagePlEn("Oracle server") /* i18n:  */; // #730
    // not translated
    // no occurrences
    public static final I18nMessage osobaOdpowiedzialna = new I18nMessagePlEn("Osoba odpowiedzialna") /* i18n:  */; // #731
    // not translated
    // no occurrences
    public static final I18nMessage ostatniaModyfikaWRepozytoBO = new I18nMessagePlEn("Ostatnia modyfikacja w repozytorium BO") /* i18n:  */; // #732
    // not translated
    // no occurrences
    public static final I18nMessage ostatniaModyfikacja = new I18nMessagePlEn("Ostatnia modyfikacja") /* i18n:  */; // #733
    // is translated
    // no occurrences
    public static final I18nMessage ostatniaZmiana = new I18nMessagePlEn("Ostatnia zmiana", "Last change") /* I18N:  */; // #734
    // is translated
    // no occurrences
    public static final I18nMessage ostatnioZmienionyPrzez = new I18nMessagePlEn("Ostatnio zmieniony przez", "Last changed by") /* I18N:  */; // #735
    // not translated
    // no occurrences
    public static final I18nMessage parametrWprowadzPrzezUzytkownWMo = new I18nMessagePlEn("Parametry wprowadzane przez użytkownika w momencie odświeżania raportu") /* i18n:  */; // #736
    // is translated
    // no occurrences
    public static final I18nMessage parametry = new I18nMessagePlEn("Parametry", "Parameters") /* I18N:  */; // #737
    // not translated
    // no occurrences
    public static final I18nMessage pion = new I18nMessagePlEn("Pion") /* i18n:  */; // #738
    // not translated
    // no occurrences
    public static final I18nMessage pl = new I18nMessagePlEn("pl") /* i18n:  */; // #739
    // is translated
    // no occurrences
    public static final I18nMessage pleaseProvideValidUserAndPasswor = new I18nMessagePlEn("Proszę podać poprawny login i hasło do połączenia z TERADATĄ ", "Please, provide valid user and password for TERADATA connection") /* I18N:  */; // #740
    // not translated
    // no occurrences
    public static final I18nMessage podajNazweSerwera = new I18nMessagePlEn("Podaj nazwę serwera") /* i18n:  */; // #741
    // not translated
    // no occurrences
    public static final I18nMessage podglad__ = new I18nMessagePlEn("Podgląd") /* i18n: . */; // #742
    // not translated
    // no occurrences
    public static final I18nMessage podwladny = new I18nMessagePlEn("Podwładny") /* i18n:  */; // #743
    // is translated
    // no occurrences
    public static final I18nMessage polaTytulITrescSaWymagane = new I18nMessagePlEn("Pola tytuł i treść są wymagane", "Fields title and content are required") /* I18N:  */; // #744
    // not translated
    // no occurrences
    public static final I18nMessage pomoc__ = new I18nMessagePlEn("Pomoc") /* i18n: . */; // #745
    // not translated
    // no occurrences
    public static final I18nMessage poprawka = new I18nMessagePlEn("Poprawka") /* i18n:  */; // #746
    // not translated
    // no occurrences
    public static final I18nMessage pozycjeOd = new I18nMessagePlEn("pozycje: od") /* i18n:  */; // #747
    // not translated
    // no occurrences
    public static final I18nMessage prod = new I18nMessagePlEn("prod") /* i18n:  */; // #748
    // not translated
    // no occurrences
    public static final I18nMessage przejdz_ok = new I18nMessagePlEn("Przejdź") /* i18n: ok */; // #749
    // not translated
    // no occurrences
    public static final I18nMessage raportWebiRaportFlashSwiatObiekt = new I18nMessagePlEn("Raport Webi,Raport Flash,Świat obiektów,"
            + "Połączenie,Folder,Schemat,Tabela,Widok,Procedura") /* i18n:  */; // #750
    // not translated
    // no occurrences
    public static final I18nMessage raportWebiRaportFlashSwiatObiekt_wtf = new I18nMessagePlEn("Raport Webi,Raport Flash,Świat obiektów,"
            + "Połączenie,Folder,Schemat,Tabela,Widok,Procedura") /* i18n: wtf?wtf!wtf? */; // #751
    // not translated
    // no occurrences
    public static final I18nMessage recurring = new I18nMessagePlEn("Recurring") /* i18n:  */; // #752
    // not translated
    // no occurrences
    public static final I18nMessage root = new I18nMessagePlEn("root") /* i18n:  */; // #753
    // not translated
    // no occurrences
    public static final I18nMessage rowsAddedToTreeOptimized = new I18nMessagePlEn("rows added to tree (optimized") /* i18n:  */; // #754
    // not translated
    // no occurrences
    public static final I18nMessage rowsConvertedToMaps = new I18nMessagePlEn("rows converted to maps") /* i18n:  */; // #755
    // not translated
    // no occurrences
    public static final I18nMessage rozmiarWBajtach = new I18nMessagePlEn("Rozmiar w bajtach") /* i18n:  */; // #756
    // not translated
    // no occurrences
    public static final I18nMessage sapbo = new I18nMessagePlEn("sapbo") /* i18n:  */; // #757
    // is translated
    // no occurrences
    // merged to: zapisz
    public static final I18nMessage save = new I18nMessagePlEn("Zapisz", "Save") /* I18N:  */; // #758
    // not translated
    // no occurrences
    public static final I18nMessage says = new I18nMessagePlEn("says") /* i18n:  */; // #759
    // not translated
    // no occurrences
    public static final I18nMessage select = new I18nMessagePlEn("Select") /* i18n:  */; // #760
    // not translated
    // no occurrences
    public static final I18nMessage selectTop5P = new I18nMessagePlEn("select top 5 p") /* i18n:  */; // #761
    // not translated
    // no occurrences
    public static final I18nMessage single = new I18nMessagePlEn("single") /* i18n:  */; // #762
    // not translated
    // no occurrences
    public static final I18nMessage size = new I18nMessagePlEn("Size") /* i18n:  */; // #763
    // not translated
    // no occurrences
    public static final I18nMessage skrytkaPocztowa = new I18nMessagePlEn("Skrytka pocztowa") /* i18n:  */; // #764
    // not translated
    // no occurrences
    public static final I18nMessage slowaKluczoweZRepozytoriumBO = new I18nMessagePlEn("Słowa kluczowe z repozytorium BO") /* i18n:  */; // #765
    // not translated
    // no occurrences
    public static final I18nMessage sn = new I18nMessagePlEn("sn") /* i18n:  */; // #766
    // not translated
    // no occurrences
    public static final I18nMessage source = new I18nMessagePlEn("Source") /* i18n:  */; // #767
    // not translated
    // no occurrences
    public static final I18nMessage sposobOdswiezaniaRaportu = new I18nMessagePlEn("Sposób odświeżania raportu") /* i18n:  */; // #768
    // not translated
    // no occurrences
    public static final I18nMessage stanowisko = new I18nMessagePlEn("Stanowisko") /* i18n:  */; // #769
    // not translated
    // no occurrences
    public static final I18nMessage startUser = new I18nMessagePlEn("start. user") /* i18n:  */; // #770
    // not translated
    // no occurrences
    public static final I18nMessage starting = new I18nMessagePlEn("starting") /* i18n:  */; // #771
    // not translated
    // no occurrences
    public static final I18nMessage starttime = new I18nMessagePlEn("Starttime") /* i18n:  */; // #772
    // is translated
    // no occurrences
    public static final I18nMessage strona = new I18nMessagePlEn("strona", "page") /* I18N:  */; // #773
    // is translated
    // no occurrences
    public static final I18nMessage strona2 = new I18nMessagePlEn("Strona", "Page") /* I18N:  */; // #774
    // is translated
    // no occurrences
    public static final I18nMessage stronaDomowa = new I18nMessagePlEn("Strona domowa", "Home page") /* I18N:  */; // #775
    // is translated
    // no occurrences
    public static final I18nMessage stronaWwww = new I18nMessagePlEn("Strona www", "Url") /* I18N:  */; // #776
    // not translated
    // no occurrences
    public static final I18nMessage systemId = new I18nMessagePlEn("System id") /* i18n:  */; // #777
    // not translated
    // no occurrences
    public static final I18nMessage systemNumber = new I18nMessagePlEn("System number") /* i18n:  */; // #778
    // not translated
    // no occurrences
    public static final I18nMessage telefonDomowy = new I18nMessagePlEn("Telefon domowy") /* i18n:  */; // #779
    // not translated
    // no occurrences
    public static final I18nMessage telefonFax = new I18nMessagePlEn("Telefon fax") /* i18n:  */; // #780
    // not translated
    // no occurrences
    public static final I18nMessage telefonIP = new I18nMessagePlEn("Telefon IP") /* i18n:  */; // #781
    // not translated
    // no occurrences
    public static final I18nMessage telefonKomorkowy = new I18nMessagePlEn("Telefon komórkowy") /* i18n:  */; // #782
    // not translated
    // no occurrences
    public static final I18nMessage telefonPager = new I18nMessagePlEn("Telefon pager") /* i18n:  */; // #783
    // not translated
    // no occurrences
    public static final I18nMessage teradata2 = new I18nMessagePlEn("teradata") /* i18n:  */; // #784
    // not translated
    // no occurrences
    public static final I18nMessage timestamp = new I18nMessagePlEn("Timestamp") /* i18n:  */; // #785
    // not translated
    // no occurrences
    public static final I18nMessage titleTytul = new I18nMessagePlEn("title=Tytuł") /* i18n:  */; // #786
    // not translated
    // no occurrences
    public static final I18nMessage typCyklu = new I18nMessagePlEn("Typ cyklu") /* i18n:  */; // #787
    // not translated
    // no occurrences
    public static final I18nMessage type2 = new I18nMessagePlEn("Type") /* i18n:  */; // #788
    // is translated
    // no occurrences
    public static final I18nMessage ulica = new I18nMessagePlEn("Ulica", "Street") /* I18N:  */; // #789
    // not translated
    // no occurrences
    public static final I18nMessage usunZaznaczony = new I18nMessagePlEn("Usuń zaznaczony") /* i18n:  */; // #790
    // not translated
    // no occurrences
    public static final I18nMessage utworzono = new I18nMessagePlEn("Utworzono") /* i18n:  */; // #792
    // not translated
    // no occurrences
    public static final I18nMessage utworzony = new I18nMessagePlEn("Utworzony") /* i18n:  */; // #793
    // not translated
    // no occurrences
    public static final I18nMessage uwagi = new I18nMessagePlEn("Uwagi") /* i18n:  */; // #794
    // is translated
    // no occurrences
    public static final I18nMessage uzyskajDostep = new I18nMessagePlEn("Uzyskaj dostęp", "Gain access") /* I18N:  */; // #795
    // is translated
    // no occurrences
    // merged to: uzytkownikSpol2
    public static final I18nMessage uzytkownikSpol = new I18nMessagePlEn("Użytkownik społ", "Social user") /* I18N:  */; // #796
    // not translated
    // no occurrences
    public static final I18nMessage withProperSignature = new I18nMessagePlEn("with proper signature") /* i18n:  */; // #797
    // is translated
    // no occurrences
    public static final I18nMessage wlasciciel = new I18nMessagePlEn("Właściciel", "Owner") /* I18N:  */; // #798
    // not translated
    // no occurrences
    public static final I18nMessage wlascicielWRepozytoriumBO = new I18nMessagePlEn("Właściciel w repozytorium BO") /* i18n:  */; // #799
    // is translated
    // no occurrences
    public static final I18nMessage wojewodztwo = new I18nMessagePlEn("Województwo", "Voivodeship") /* I18N:  */; // #800
    // is translated
    // no occurrences
    public static final I18nMessage wszystkich = new I18nMessagePlEn("wszystkich", "total") /* I18N: pagination */; // #801
    // not translated
    // no occurrences
    public static final I18nMessage wyjasnienie__ = new I18nMessagePlEn("Wyjaśnienie") /* i18n: . */; // #802
    // not translated
    // no occurrences
    public static final I18nMessage wynikZAdhoc = new I18nMessagePlEn("wynik z adhoc") /* i18n:  */; // #803
    // is translated
    // no occurrences
    public static final I18nMessage wystepowanieW = new I18nMessagePlEn("Występowanie w", "Occurrence in") /* I18N:  */; // #804
    // not translated
    // no occurrences
    public static final I18nMessage wystepujeWObszarze = new I18nMessagePlEn("Występuje w obszarze") /* i18n:  */; // #805
    // is translated
    // no occurrences
    public static final I18nMessage wyswietlanaNazwa = new I18nMessagePlEn("Wyświetlana nazwa", "Displayed name") /* I18N:  */; // #806
    // not translated
    // no occurrences
    public static final I18nMessage z1PozycjeOd2Do = new I18nMessagePlEn("z {1}, pozycje od {2} do") /* i18n:  */; // #807
    // not translated
    // no occurrences
    public static final I18nMessage zSerwera = new I18nMessagePlEn("z serwera") /* i18n:  */; // #808
    // not translated
    // no occurrences
    public static final I18nMessage zarzadza = new I18nMessagePlEn("Zarządza") /* i18n:  */; // #809
    // not translated
    // no occurrences
    // merged to: zatwierdz2
    public static final I18nMessage zatwierdz = new I18nMessagePlEn("Zatwierdź") /* i18n:  */; // #810
    // not translated
    // no occurrences
    public static final I18nMessage zatwierdz2 = new I18nMessagePlEn("Zatwierdź") /* i18n:  */; // #811
    public static final I18nMessage zatwierdzModyfikacje = new I18nMessagePlEn("Zatwierdź zmiany", "Confirm all changes") /* i18n:  */;
    // is translated
    // no occurrences
    public static final I18nMessage zdjecie = new I18nMessagePlEn("Zdjęcie", "Photo") /* I18N:  */; // #812
    // not translated
    // no occurrences
    public static final I18nMessage zmienionParametrDoPolaczenZSAPBO = new I18nMessagePlEn("Zmieniono parametry do połączenia z SAPBO") /* i18n:  */; // #813
    // not translated
    // no occurrences
    public static final I18nMessage zmodyfikowany = new I18nMessagePlEn("Zmodyfikowany") /* i18n:  */; // #814
    public static final I18nMessage bladKonfiguracjiDrzewa = new I18nMessagePlEn("Błąd konfiguracji drzewa w BIKS, wyagany kontakt z administratorem", "Invalid tree configuration, please contact system administrator") /* I18N:  */;
    public static final I18nMessage brakWskazanejTabeliDlaSlownika = new I18nMessagePlEn("Brak wskazanej tabeli dla słownika", "Invalid table name for dictionary") /* I18N:  */;
    public static final I18nMessage brakWskazanejTabeliRelacyjnej = new I18nMessagePlEn("Brak wskazanej tabeli referencyjnej", "Invalid reference table name") /* I18N:  */;
    public static final I18nMessage blednaNazwaKolumnyRelacyjnej = new I18nMessagePlEn("Błędna nazwa kolumny referencyjnej", "Invalid reference column name") /* I18N:  */;
    public static final I18nMessage brakKluczaPodstawowego = new I18nMessagePlEn("Brak klucza podstawowego, uzupełnij metadane ręcznie", "No primary key column found, update dictionary's metadata manually") /* I18N:  */;
    public static final I18nMessage domyslnaNazwaKategDlaNieprzypisanych = new I18nMessagePlEn("Domyślna nazwa kategorii dla nieprzypisanych", "Default category name for unassigned items") /* I18N:  */;
    public static final I18nMessage usunWszystkieSesieTeradaty = new I18nMessagePlEn("Usuń wszystkie sesje teradaty", "Delete all teradata's sessions") /* I18N:  */;
    public static final I18nMessage sesieZostalyUsuniete = new I18nMessagePlEn("Sesje zostały usunięte", "Sesiond was deleted") /* I18N:  */;
    public static final I18nMessage dodajKategoryzacje = new I18nMessagePlEn("Dodaj kategoryzację", "Add categorization") /* I18N:  */;
    public static final I18nMessage kategoryzacja = new I18nMessagePlEn("Kategoryzacja", "Categorization") /* I18N:  */;
    public static final I18nMessage wyciete = new I18nMessagePlEn("Wycięte", "Cut") /* I18N:  */; // #483
    public static final I18nMessage wklejone = new I18nMessagePlEn("Wklejone", "Pasted") /* I18N:  */; // #483
    public static final I18nMessage wyciety = new I18nMessagePlEn("Wycięty", "Cut") /* I18N:  */; // #304
    public static final I18nMessage wklejony = new I18nMessagePlEn("Wklejony", "Pasted") /* I18N:  */; // #304
    public static final I18nMessage proszePodacAdresEmailUzytkownika = new I18nMessagePlEn("Proszę podać adres email użytkownika", "Please fill in your email address") /* I18N:  */;
    public static final I18nMessage wprowadzHaslo = new I18nMessagePlEn("Wprowadź hasło", "Enter password") /* I18N:  */;
    public static final I18nMessage powtorzHaslo2 = new I18nMessagePlEn("Powtórz hasło", "Repeat the password") /* I18N:  */;
    //    public static final I18nMessage rejestrujDarmoweKontoBiks = new I18nMessagePlEn("Rejestruj darmowe konto BIKS", "Register my free BIKS account") /* I18N:  */;
    public static final I18nMessage rejestrujDarmoweKontoBiks = new I18nMessagePlEn("Rejestruj", "Register") /* I18N:  */;
    public static final I18nMessage zalozDarmoweKonto = new I18nMessagePlEn("Załóż darmowe konto próbne w BIKS", "Create trial account in BIKS");
    /* I18N:  */

    public static final I18nMessage trwaWeryfikacja = new I18nMessagePlEn("Trwa weryfikacja", "Trwa weryfikacja");
    /* I18N:  */

    public static final I18nMessage bladWeryfikacji = new I18nMessagePlEn("Błąd weryfikacji", "Verification error");
    /* I18N:  */

    public static final I18nMessage zatwierdzDane = new I18nMessagePlEn("Zatwierdz dane", "Confirm data");
    /* I18N:  */

    public static final I18nMessage reset = new I18nMessagePlEn("Resetuj", "Reset");
    /* I18N:  */

    public static final I18nMessage podajDaneDoZalożeniaKonta = new I18nMessagePlEn("Podaj dane do założenia konta", "Give additional data for account creation");
    /* I18N:  */

    public static final I18nMessage wybierzSzablonBazyIZakonczRejestracje = new I18nMessagePlEn("Wybierz szablon bazy i zakończ proces rejestracji.", "Choose databa template and finish registration process.");
    /* I18N:  */

    public static final I18nMessage wystapilBladWCzasieRejestracji = new I18nMessagePlEn("Wystąpił błąd w czasie rejestracji.", "An error occured during registration process.");
    /* I18N:  */

    public static final I18nMessage bledneHaslo = new I18nMessagePlEn("Błędne hasło. Wpisz ponownie to samo hasło w obu polach.", "Invalid password. Reenter password in both fields.");
    /* I18N:  */

    public static final I18nMessage proszePodacAdresEmail = new I18nMessagePlEn("Proszę podać adres email", "Please provide valid email address");
    /* I18N:  */

    public static final I18nMessage naPodanyAdresWyslanoWiadomosc = new I18nMessagePlEn("Na podany adres wysłano wiadomość zawierającą informacje o logowaniu do aplikacji BIKS.", "A message with logging details has been sent at the address you provided.");
    /* I18N:  */

    public static final I18nMessage naPodanyPrzezCiebieAdres = new I18nMessagePlEn("* Na podany przez Ciebie adres email zostaną wysłane szczegółowe informacje dotyczące logowania do aplikacji.", "* Detailed information about logging into applicaton will be send at the address you provide.");
    /* I18N:  */

    public static final I18nMessage naPodanyPrzezCiebieAdresReset = new I18nMessagePlEn("* Na podany przez Ciebie adres email zostanie wysłany link do zrestartowania hasła.", "* Detailed information about logging into applicaton will be send at the address you provide.");
    /* I18N:  */

    public static final I18nMessage errorLink = new I18nMessagePlEn("Błędny link, spróbuj zresetować hasło jeszcze raz.", "Error, try to reset password one more time.");
    public static final I18nMessage trwaFinalizowanieProcesuRejestracji = new I18nMessagePlEn("Trwa finalizowanie procesu rejestracji. Może to potrwać kilka minut. Dziękujemy za cierpliwość. ", "Finalizing registration process. It may take a few minutes. Thank you for your patience. ");
    public static final I18nMessage rejestracjaZakonczonaPomyslnie = new I18nMessagePlEn("Rejestracja zakończona pomyślnie.", "Registration process finished successfully.");
    public static final I18nMessage resetowanieHaslaZakonczonePomyslnie = new I18nMessagePlEn("Resetowanie hasła zakończone pomyślnie.", "Password reset process finished successfully.");
    public static final I18nMessage witamyWSystemieZarzadzaniaWiedza = new I18nMessagePlEn("Witamy w Systemie zarządzania wiedzą BIKS", "Welcome to BIKS knowledge management system");
    public static final I18nMessage witamyWBiks = new I18nMessagePlEn("Witaj, {0}"
            + "Właśnie wykonałeś pierwszy krok w celu wypróbowania Business Integration Knowledge System (BIKS) – systemu do zarządzania wiedzą.{1}"
            + "Aby dokończyć proces rejestracji dla konta {2} kliknij poniższy link:{3}{4}{5}"
            + "Następnie zapoznaj się z aplikacją i koniecznie zaproś swoich znajomych do współpracy.{6}"
            + "Pamiętaj o zapoznaniu się z samouczkami i koniecznie zobacz Prezentację BIKS.{7}"
            + "Jeżeli będziesz miał jakieś pytania, prześlij je do nas na adres {8}{9}{10}",
            "Welcome, {0}"
            + "You have just taken the first step to try out Business Integration Knowledge System (BIKS) - system for knowledge management.{1}"
            + "To complete the registration process for account {2}, please click the following link:{3}{4}{5}"
            + "Study the application and make sure to invite your friends to cooperate.{6}"
            + "Make sure to read the tutorials and see the BIKS presentation.{7}"
            + "If you have any questions, email us at: {8}{9}{10}");
    public static final I18nMessage zaproszenieDoBiks = new I18nMessagePlEn("Witamy w Systemie zarządzania wiedzą BIKS, {0}"
            + "{1} zaprosił Cię do wspólnego zarządzania wiedzą w Business Integration Knowledge System (BIKS).{2}{3}"
            //            + "Dzieli Cię jeden krok od wypróbowania BI Knowledge System.{3}"
            + "Aby dokończyć proces rejestracji dla konta {4} kliknij poniższy link:{5}{6}{7}"
            + "Następnie zapoznaj się z aplikacją i koniecznie zaproś swoich znajomych do współpracy.{8}"
            + "Pamiętaj o zapoznaniu się z samouczkami i koniecznie zobacz Prezentację BIKS.{9}"
            + "Jeżeli będziesz miał jakieś pytania, prześlij je do nas na adres {10}{11}{12}",
            "Welcome to BIKS knowledge management system, {0}"
            + "{1} invites you to manage knowledge with him  in the Business Integration Knowledge System (BIKS).{2}{3}"
            //            + "Only one step left to try BI Knowledge System.{3}"
            + "To complete the registration process for account {4}, please click the following link:{5}{6}{7}"
            + "Study the application and make sure to invite your friends to cooperate.{8}"
            + "Make sure to read the tutorials and see the BIKS presentation.{9}"
            + "If you have any questions, email us at: {10}{11}{12}");
    public static final I18nMessage biksUser = new I18nMessagePlEn("Użytkownik systemu BIKS o adresie: {0}", "BIKS system user with email address: {0}");
    public static final I18nMessage resetPasswordMailTiltle = new I18nMessagePlEn("Odzyskiwanie hasła", "Password retrieval");
    public static final I18nMessage resetPasswordMail = new I18nMessagePlEn("Aby zresetować hasło kliknij poniższy link:{0}", "In order to reset password process please use this link:{0}");
    public static final I18nMessage pozdrawiamyZespolBiks = new I18nMessagePlEn("Pozdrawiamy, Zespół BIKS.", "Regards, BIKS team.");
    public static final I18nMessage powodzeniaZespolBiks = new I18nMessagePlEn("Powodzenia! {0}Zespół BIKS.", "Good luck! {0}BIKS team.");
    public static final I18nMessage linkRejestracyjnyWygasl = new I18nMessagePlEn("Link rejestracyjny wygasł. Zarejestruj się ponownie.", "Your registration link has expired. Please repeat registration process.");
    public static final I18nMessage dlaPodanegoAdresuEmailIstniejeJuzKonto = new I18nMessagePlEn("Dla podanego adresu email istnieje już konto. Spróbuj rejestracji podając inny adres email.", "An account has been already registered for provided email address. Try to register on a diffrent email.");
    public static final I18nMessage zaprosUzytkownikow = new I18nMessagePlEn("Zaproś użytkownika/ów", "Invite user/s");
    public static final I18nMessage resetujBaze = new I18nMessagePlEn("Resetuj bazę", "Reset database");
    public static final I18nMessage resetujBazeOpis = new I18nMessagePlEn("Przywróć ustawienia z szablonu wybranego przy rejestracji.", "Restore settings from template choosen during registration process.");
    public static final I18nMessage zachowacZaproszonych = new I18nMessagePlEn("Czy zachować zaproszonych użytkowników?", "Should invited users be preserved?");
    public static final I18nMessage nieIstniejeKontoDlaPodanegoAdresuEmail = new I18nMessagePlEn("Nie istnieje konto dla podanego adresu email.", "There is no account for email address, that has been given.");
    public static final I18nMessage nazwaBazyDanychMultix = new I18nMessagePlEn("Nazwa bazy danych", "Database name");
    public static final I18nMessage ostatniaAktywnosc = new I18nMessagePlEn("Ostatnia aktywność", "Last activity");
    public static final I18nMessage logowaloSie = new I18nMessagePlEn("Logowało się", "Total users logged");
    public static final I18nMessage zaproszonych = new I18nMessagePlEn("Zaproszonych", "Invited");
    public static final I18nMessage wartosc = new I18nMessagePlEn("Wartość", "Value");
    public static final I18nMessage dataOstatniejOperacji = new I18nMessagePlEn("Data ostatniej operacji", "Last action date");
    public static final I18nMessage linkZostalWyslany = new I18nMessagePlEn("Link do resetowania hasła został wysłany na podany adres email.", "Reset link has been already sent for your e-mail address.");
    public static final I18nMessage position = new I18nMessagePlEn("Stanowisko", "Position");
    public static final I18nMessage additionRemarks = new I18nMessagePlEn("Dodatkowe uwagi", "Additional remarks");
    public static final I18nMessage podajAdresyUzytkownikow = new I18nMessagePlEn("Podaj maksymalnie 5 adresów email rozdzielonych przecinkami:", "Input up to 5 email addresses separated with a comma:");
    public static final I18nMessage posiadaDostep = new I18nMessagePlEn("Użytkownik o adresie {0} już posiada dostęp do bazy.", "The user with email {0} already has access to this database.");
    public static final I18nMessage juzZaproszony = new I18nMessagePlEn("Użytkownik o adresie {0} został już zaproszony do tej bazy.", "The user with email {0} has already received an invitation to this database.");
    public static final I18nMessage potwierdzonoZaproszenie = new I18nMessagePlEn("Zaproszenie potwierdzone.", "Invitation confirmed.");
    public static final I18nMessage refreshMultixStatistics = new I18nMessagePlEn("Odśwież statystyki", "Refresh statistics");
    public static final I18nMessage loginToMultixStatisticsPage = new I18nMessagePlEn("Logowanie do statystyk Mulitx", "Login to Multix statistics");
    public static final I18nMessage fieldRequired = new I18nMessagePlEn("Pole jest wymagane. Maksymalna długość wprowadzanej wartości to {0} znaków.", "This field is required. Maximum length can't exceed {0} characters.");
    public static final I18nMessage kasowaniaSlownikaSukces = new I18nMessagePlEn("Kasowanie słownika zakończone sukcesem", "Dictionary deleting successful") /* i18n:  */;
    public static final I18nMessage kasowaniaSlownikaBlad = new I18nMessagePlEn("Kasowanie słownika zakończone niepowodzeniem", "Dictionary deleting failed") /* i18n:  */;
    public static final I18nMessage czyNaPewnoUsunaSlownik = new I18nMessagePlEn("Czy na pewno usunąć Slownik", "Are you sure you want to delete dictionary") /* I18N:  */;
    public static final I18nMessage usunSlownik = new I18nMessagePlEn("Usuń słownik", "Delete dictionary") /* I18N:  */;
    public static final I18nMessage elementyDoSkategoryzowania = new I18nMessagePlEn("Następujące elementy do skategoryzowania", "The following items to categorize");
    public static final I18nMessage elementyDoSkategoryzowaniaWSlowniku = new I18nMessagePlEn("Elementy do skategoryzowania w słowniku", "Items to be categorized in the dictionary");
    public static final I18nMessage noweElementyDoSkategoryzowania = new I18nMessagePlEn("Nowe elementy slownika do skategoryzowania. Szczegóły w Komunikatach", "New elements of the dictionary to categorize. Details in the announcements");
    public static final I18nMessage doSkategoryzawania = new I18nMessagePlEn("do skategoryzawania", "to categorize");
    public static final I18nMessage elementow = new I18nMessagePlEn("elementów", "elements");
    public static final I18nMessage zmienionParametrDoPolaczenZConfluence = new I18nMessagePlEn("Zmieniono parametry do połączenia z Confluence", "Parameters to connect to Confluence have been changed");
    public static final I18nMessage dodajObiektZConfluence = new I18nMessagePlEn("Dodaj obiekt z Confluence", "Add object from Confluence");
    public static final I18nMessage zapisanoObiektyZConfluence = new I18nMessagePlEn("Zapisano obiekty z Confluence", "Objects from Confluence have been saved");
    public static final I18nMessage otworzWConfluence = new I18nMessagePlEn("Otwórz w Confluence", "Open in Confluence") /* I18N:  */; // #334
    public static final I18nMessage kluczeObce = new I18nMessagePlEn("Klucze obce", "Foreign keys");
    public static final I18nMessage columns = new I18nMessagePlEn("Kolumny", "Columns");
    public static final I18nMessage column = new I18nMessagePlEn("Kolumna", "Column");
    public static final I18nMessage tablePartitioning = new I18nMessagePlEn("Partycjonowanie tabeli", "Table partitioning");
    public static final I18nMessage dependencies = new I18nMessagePlEn("Zależy od", "Depends");
    public static final I18nMessage allowNulls = new I18nMessagePlEn("Null?", "Allow nulls");
    public static final I18nMessage identity = new I18nMessagePlEn("Autonumerowanie", "Identity");
    public static final I18nMessage functionType = new I18nMessagePlEn("Typ funkcji", "Function type");
    public static final I18nMessage indexName = new I18nMessagePlEn("Nazwa indeksu", "Index name");
    public static final I18nMessage partitionFunction = new I18nMessagePlEn("Funkcja partycjonująca", "Partition function");
    public static final I18nMessage partitionSchema = new I18nMessagePlEn("Schemat partycjonowania", "Partition schema");
    public static final I18nMessage domyslnie = new I18nMessagePlEn("Domyślnie", "Default");
    public static final I18nMessage partycjonowanyPo = new I18nMessagePlEn("Partycjonowany po", "Partition by");
    public static final I18nMessage partycjonowanePo = new I18nMessagePlEn("Partycjonowane po: ", "Partition by: ");
    public static final I18nMessage obszarDanych = new I18nMessagePlEn("Obszar", "Subject area");
    public static final I18nMessage diagram = new I18nMessagePlEn("Diagram", "Diagram");
    public static final I18nMessage trybPełnoekranowy = new I18nMessagePlEn("Tryb pełnoekranowy", "Full screen mode");
    public static final I18nMessage zmienionParametrDoPolaczenErwinDM = new I18nMessagePlEn("Zmieniono parametry do połączenia z Erwin Data Model", "Parameters to connect to Erwin Data Model have been changed") /* I18N:  */; // #625
    public static final I18nMessage obiektyZalezne = new I18nMessagePlEn("Obiekty zależne", "Dependent objects");
    public static final I18nMessage obiektyZalezneFK = new I18nMessagePlEn("Odwołanie poprzez klucz obcy", "Reference by a foreign key");
    public static final I18nMessage obiektyZalezneDep = new I18nMessagePlEn("Odwołanie poprzez zależność", "Reference by a dependency");
    public static final I18nMessage confluenceName = new I18nMessagePlEn("Confluence", "Confluence");
    public static final I18nMessage odswiezObiektZConfluencea = new I18nMessagePlEn("Odśwież obiekt z Confluence'a", "Refresh Confluence object");
    public static final I18nMessage odswiezanieObiektuZConfluence = new I18nMessagePlEn("Odświeżanie obiektu z Confluence'a", "Refreshing Confluence object");
    public static final I18nMessage odswiezonoObiektZConfluence = new I18nMessagePlEn("Odświeżono obiekt z Confluence'a", "Object from Confluence was refreshed");
    public static final I18nMessage publikujArtykulDoConfluence = new I18nMessagePlEn("Publikuj artykuł do Confluence'a", "Publish article to Confluence");
    public static final I18nMessage zalogujJako = new I18nMessagePlEn("Zaloguj jako", "Log in as");
    public static final I18nMessage publikuj = new I18nMessagePlEn("Publikuj", "Publish");
    public static final I18nMessage bladWZapisieObiektowZConfluence = new I18nMessagePlEn("Błąd w zapisie obiektów", "Error in save objcets");
    public static final I18nMessage pobieranieObiketowZConfluenceProszeCzekac = new I18nMessagePlEn("Trwa pobieranie obiektów z Confluence, proszę czekać...", "Getting objects from Confluence in progress, please wait...");
    public static final I18nMessage publikowanieArtykuluDoConfluenceProszeCzekac = new I18nMessagePlEn("Trwa publikowanie artykułu do Confluence, proszę czekać...", "Publishing article to Confluence in progress, please wait...");
    public static final I18nMessage podajUzytkownikaIhasloDoCOnfluence = new I18nMessagePlEn("Podaj użytkownika i hasło do Confluence'a:", "Type login and password to your Confluence user");
    public static final I18nMessage opublikowanoArtykul = new I18nMessagePlEn("Opublikowano artykuł", "An article was published");
    public static final I18nMessage nieprawidlowyLoginAbyOpublikowacArtykul = new I18nMessagePlEn("Nieprawidłowy login / hasło lub brak uprawnień do publikacji artykułu!", "Invalid username / password or no permission to publish the article!");
    public static final I18nMessage wybierzMiejsceDodaniaArtykulu = new I18nMessagePlEn("Wybierz miejsce dodania artykułu", "Choose a location added article");
    public static final I18nMessage obiektZostanieDodanyWBIKSPod = new I18nMessagePlEn("Obiekt zostanie dodany w BIKS pod folderem: ", "Object will be added in BIKS under the directory: ");
    public static final I18nMessage obiektZostanieDodanyJakoRoot = new I18nMessagePlEn("Obiekt zostanie dodany w BIKS na najwyższym poziomie", "Object will be added in BIKS as root");
    public static final I18nMessage wybierzObiektyDoDodania = new I18nMessagePlEn("Wybierz obiekty do dodania", "Select the objects to add");
    public static final I18nMessage wybierzObiektyDoEksportu = new I18nMessagePlEn("Wybierz obiekty do eksportu", "Select the objects to export");
    public static final I18nMessage logujSSO = new I18nMessagePlEn("Loguj SSO", "SSO logon");
    public static final I18nMessage zmienIkony = new I18nMessagePlEn("Zmień ikony", "Change icons");
    public static final I18nMessage zmienIkone = new I18nMessagePlEn("Zmień ikonę", "Change icon");
    public static final I18nMessage wybierzIkonyDlaDrzewa = new I18nMessagePlEn("Wybierz ikony dla drzewa", "Select icons for tree");
    public static final I18nMessage zapisanoIkonyDlaDrzewa = new I18nMessagePlEn("Zapisano ikony dla drzewa", "Saved icons for tree");
    public static final I18nMessage wybierzSposobLogowania = new I18nMessagePlEn("Wybierz sposób logowania", "Select a login method");
    public static final I18nMessage logowanieKlasyczne = new I18nMessagePlEn("Logowanie klasyczne (użytkownik BIKS)", "Enterprise logon (BIKS user)");
    public static final I18nMessage logowanieDomenoweSSO = new I18nMessagePlEn("Logowanie domenowe (SSO)", "Implicit domain logon (SSO)");
    public static final I18nMessage logowanieDomenoweNaLogin = new I18nMessagePlEn("Logowanie domenowe (na wybrany login)", "Explicit domain logon");
    public static final I18nMessage domyslnaDomena = new I18nMessagePlEn("<Domena domyślna>", "<Default domain>");
    public static final I18nMessage zapisywanieParametrowPolaczenProszeCzekac = new I18nMessagePlEn("Trwa zapisywanie parametrów połączeń, proszę czekać...", "Saving config parameters in progress, please wait...");
    public static final I18nMessage trwaUsuwanieDrzewaProszeCzekac = new I18nMessagePlEn("Trwa usuwanie drzewa, proszę czekać...", "Deleting tree in progress, please wait...");
    public static final I18nMessage legendaEdycjiPowiazan = new I18nMessagePlEn("Edycja powiązań", "Edit association");
    public static final I18nMessage listaDostepnychWartosci = new I18nMessagePlEn("Walidacja z listy", "Validation from list");
    public static final I18nMessage zBazy = new I18nMessagePlEn("Walidacja z bazy danych", "Validation from database");
    public static final I18nMessage tekstWyswietlanyZ = new I18nMessagePlEn("Tekst wyświetlany z", "Text displayed from");
    public static final I18nMessage nieZostaniePotwierdzone = new I18nMessagePlEn("Bez walidacji", "Will not be validated");
    public static final I18nMessage atrybutWiliczanyAutomatycznie = new I18nMessagePlEn("<Atrybut wyliczany automatycznie>", "<Attribute calculated automatically>");
    public static final I18nMessage mojeObiekty = new I18nMessagePlEn("Moje obiekty", "My objects");
    public static final I18nMessage obiektyUzytkownikow = new I18nMessagePlEn("Obiekty użytkowników", "Users objects");
    public static final I18nMessage wybierzTypDokumentu = new I18nMessagePlEn("Typ dokumentu", "Document type");
    public static final I18nMessage dodajTest = new I18nMessagePlEn("Dodaj test", "Add test");
    public static final I18nMessage edytujTest = new I18nMessagePlEn("Edytuj test", "Edit test");
    public static final I18nMessage przypiszTest = new I18nMessagePlEn("Przypisz test do grupy", "Assign test to a group");
    public static final I18nMessage aktywujTest = new I18nMessagePlEn("Aktywuj test", "Activate test");
    public static final I18nMessage dezaktywujTest = new I18nMessagePlEn("Dezaktywuj test", "Deactivate test");
    public static final I18nMessage dodanoTest = new I18nMessagePlEn("Dodano test", "Test added");
    public static final I18nMessage zmienionoTest = new I18nMessagePlEn("Zmieniono test", "Test was changed");
    public static final I18nMessage przypisanoTestDoGrupy = new I18nMessagePlEn("Przypisano test do grupy", "Test assigned to group");
    public static final I18nMessage usunTest = new I18nMessagePlEn("Usuń test", "Delete test");
    public static final I18nMessage dezaktywwowanoTest = new I18nMessagePlEn("Dezaktywowano test", "Test was deactivated");
    public static final I18nMessage aktywwowanoTest = new I18nMessagePlEn("Aktywowano test", "Test was activated");
    public static final I18nMessage wybranyTestJestJuzPrzypisany = new I18nMessagePlEn("Wybrany test jest już przypisany do grupy. Wybierz inny.", "The selected test is already assigned to the group. Select another.");
    public static final I18nMessage przypisujeszTestDoGrupy = new I18nMessagePlEn("Przypisujesz test do grupy: ", "Assign a test to a group: ");
    public static final I18nMessage przypisz = new I18nMessagePlEn("Przypisz", "Assign");
    public static final I18nMessage usunZGrupy = new I18nMessagePlEn("Usuń z grupy", "Delete from group");
    public static final I18nMessage czyNaPewnoUsunacTest = new I18nMessagePlEn("Czy na pewno usunąć test", "Are you sure you want to delete test");
    public static final I18nMessage czyNaPewnoUsunacTestZGrupy = new I18nMessagePlEn("Czy na pewno usunąć z grupy test", "Are you sure you want to unassign test");
    public static final I18nMessage istniejeTakiTest = new I18nMessagePlEn("Istnieje test o takiej nazwie. Podaj inną nazwę.", "There is a test with this name. Please enter a different name.");
    public static final I18nMessage wybierzFunkcje = new I18nMessagePlEn("Wybierz funkcję", "Select a function");
    public static final I18nMessage wybierzBazeDanych = new I18nMessagePlEn("Wybierz bazę danych", "Select a database");
    public static final I18nMessage funkcjaTestujaca = new I18nMessagePlEn("Funkcja testująca", "Test function");
    public static final I18nMessage funkcjaZwracajacaBledy = new I18nMessagePlEn("Funkcja zwracająca błędne dane (opcjonalnie)", "Error returning function (optional)");
    public static final I18nMessage zrodlo = new I18nMessagePlEn("Źródło", "Source");
    public static final I18nMessage wybierzFunkcjeTestujaca = new I18nMessagePlEn("Wybierz funkcję testujacą", "Select testing function");
    public static final I18nMessage nazwaTestu = new I18nMessagePlEn("Nazwa testu", "Test name");
    public static final I18nMessage opisTestu = new I18nMessagePlEn("Opis testu", "Test description");
    public static final I18nMessage nazwaBiznesowa = new I18nMessagePlEn("Nazwa biznesowa", "Long name");
    public static final I18nMessage definicjaTestu = new I18nMessagePlEn("Definicja testu", "Test definition");
    public static final I18nMessage czestotliwoscUruchamianiaTest = new I18nMessagePlEn("Częstotliwość uruchamiania testu", "Sampling method");
    public static final I18nMessage sposobUruchamianiaTestu = new I18nMessagePlEn("Sposób uruchamiania testu", "Additional information");
    public static final I18nMessage progBledu = new I18nMessagePlEn("Próg błędu", "Error threshold");
    public static final I18nMessage progBleduJestNieprawidlowy = new I18nMessagePlEn("Próg błędu jest nieprawidłowy.", "Error threshold is invalid.");
    public static final I18nMessage rozpoczynamTest = new I18nMessagePlEn("Rozpoczynam test.", "Test starting.");
    public static final I18nMessage bladPolaczenia = new I18nMessagePlEn("Błąd połączenia", "Connection error");
    public static final I18nMessage laczenieZBazaDanych = new I18nMessagePlEn("Łączenie z bazą danych", "Connection with database");
    public static final I18nMessage wykonywanieTestu = new I18nMessagePlEn("Wykonywanie testu", "Test starting");
    public static final I18nMessage nieprawidlowyWynikFunkcji = new I18nMessagePlEn("Wynik testu niezgodny z oczekiwanym formatem", "The test result inconsistent with the expected format");
    public static final I18nMessage zamykaniePolaczenia = new I18nMessagePlEn("Zamykanie połaczenia", "Connection closing");
    public static final I18nMessage sukces = new I18nMessagePlEn("Sukces", "Success");
    public static final I18nMessage bladWUruchomieniuTestu = new I18nMessagePlEn("Błąd w uruchomieniu testu", "Error in test running");
    public static final I18nMessage bladWSQLPobierajacymDane = new I18nMessagePlEn("Błąd w SQL pobierającym błędne dane", "Error in getting wrong data SQL");
    public static final I18nMessage niePobranoBlednychDanych = new I18nMessagePlEn("Nie pobrano błędnych danych, a test wykazał, że takie istnieją", "Not collected data errors, and the test showed that they exist");
    public static final I18nMessage testWTrakcie = new I18nMessagePlEn("Wykonywanie w trakcie", "Execution in progress") /* I18N:  */; // #605
    public static final I18nMessage logiZOstatnichWykonanTestow = new I18nMessagePlEn("Logi z ostatnich wykonań testów", "Logs from recent tests execution") /* I18N:  */; // #605
    public static final I18nMessage logiZOstatnichWykonanArchiwizacji = new I18nMessagePlEn("Logi z ostatnich wykonań archiwizacji", "Logs from recent backup") /* I18N:  */; // #605
    public static final I18nMessage wykonywanieFunkcjiZBledami = new I18nMessagePlEn("Są błędy. Wykonywanie funkcji do pobrania błedów.", "There are errors! Execution error function.");
    public static final I18nMessage zapisywanieBledowDoBazyDanych = new I18nMessagePlEn("Zapisywanie błędnych rekordów do bazy danych", "Saving error records to database");
    public static final I18nMessage pobierzBledneDane = new I18nMessagePlEn("Pobierz błędne dane", "Download invalid data");
    public static final I18nMessage bledy = new I18nMessagePlEn("Błędy", "Errors");
    public static final I18nMessage wybierzFormatExportu = new I18nMessagePlEn("Wybierz format exportu", "Select export format");
    public static final I18nMessage glebokoscDrzewa = new I18nMessagePlEn("Głębokość drzewa", "Tree's depth");
    public static final I18nMessage zmienionParametrDoPolaczenZProfile = new I18nMessagePlEn("Zmieniono parametry do połączenia z Profile", "Parameters to connect to Profile have been changed");
    public static final I18nMessage trzymajBledneDaneDlaOstatnichWykonan = new I18nMessagePlEn("Trzymaj błędne dane dla ostatnich wykonań", "Keep error data for last requests");
    public static final I18nMessage usuwanieStarychBlednychDanych = new I18nMessagePlEn("Usuwanie starych błędnych danych", "Deleting old error data");
    public static final I18nMessage wpiszPoprawneWartosci = new I18nMessagePlEn("Wpisz poprawne wartości.", "Insert correct values.");
    public static final I18nMessage co30Dni = new I18nMessagePlEn("Co 30 dni", "Every 30 days");
    public static final I18nMessage uruchomTest = new I18nMessagePlEn("Uruchom Test", "Run Test");
    public static final I18nMessage harmonogramAktywny = new I18nMessagePlEn("Harmonogram<br>aktywny", "Active<br>schedule");
    public static final I18nMessage uruchomionoTest = new I18nMessagePlEn("Uruchomiono test ", "Test is started ");
    public static final I18nMessage bazaDanych = new I18nMessagePlEn("Baza danych", "Database");
    public static final I18nMessage zapytanieSprawdzajaceCzyWystepujaBledy = new I18nMessagePlEn("Zapytanie sprawdzające czy występują błędy", "Testing query");
    public static final I18nMessage zapytanieZwracajaceDane = new I18nMessagePlEn("Zapytanie zwracające błędne dane", "Error returning query");
    public static final I18nMessage zapytanieTestujaceNieMozeBycPuste = new I18nMessagePlEn("Zapytanie testujące nie może być puste", "Testing query cannot be empty");
    public static final I18nMessage zapytaniePobierajaceDaneNieMozeBycPuste = new I18nMessagePlEn("Zapytanie pobierające dane nie może być puste", "Data query cannot be empty");
    public static final I18nMessage prefiksNazwyPlikuZSFTP = new I18nMessagePlEn("Prefiks nazwy pliku z wykonaniem testu na FTP", "File name prefix of the test result on FTP");
    public static final I18nMessage prefiksPliku = new I18nMessagePlEn("Prefiks nazwy pliku", "File name prefix");
    public static final I18nMessage nazwaPlikuZWykonaniamiTestNaSFTP = new I18nMessagePlEn("Nazwa pliku z wykonaniem testu na FTP", "File name of the test result on FTP");
    public static final I18nMessage nazwaPlikuZBlednymiDanymiTestuNaSFTP = new I18nMessagePlEn("Nazwa pliku z błędnymi danymi na FTP", "File name of the error data on FTP");
    public static final I18nMessage errorBrakPlikowDoPobrania = new I18nMessagePlEn("Brak pliku (plików) do pobrania", "Lack of file (files) to download.");
    public static final I18nMessage errorBrakNowegoPlikuDoPobrania = new I18nMessagePlEn("Brak nowego pliku (plików) do pobrania", "Lack of new file (files) to download.");
    public static final I18nMessage nazwaPlikuNaSFTPNieMozeBycPusta = new I18nMessagePlEn("Nazwa pliku na FTP nie może być pusta", "FTP file name cannot be empty");
    public static final I18nMessage kontoUzytkownikaJestWylaczone = new I18nMessagePlEn("Konto jest wyłączone. Skontaktuj się z Administratorem.", "The user account is disabled. Please contact the Administrator.");
    public static final I18nMessage bazaDanychLubPlik = new I18nMessagePlEn("Baza danych / Plik z danymi", "Database / File with data");
    public static final I18nMessage zapytaniePobierajaceDane = new I18nMessagePlEn("Zapytanie pobierające dane", "Data query name");
    public static final I18nMessage dodajZrodlo = new I18nMessagePlEn("Dodaj źródło", "Add source");
    public static final I18nMessage edytujZrodlo = new I18nMessagePlEn("Edytuj źródło", "Edit source");
    public static final I18nMessage nazwaKodowaZrodla = new I18nMessagePlEn("Nazwa kodowa źródła", "Code source name");
    public static final I18nMessage plikZDanymi = new I18nMessagePlEn("Plik z danymi", "Data file");
    public static final I18nMessage nazwaPlikuZDanymiNaFTP = new I18nMessagePlEn("Nazwa pliku z danymi na FTP", "Data file name on FTP");
    public static final I18nMessage nazwaKodowaJestPusta = new I18nMessagePlEn("Nazwa kodowa źródła jest pusta", "Source code name is empty!");
    public static final I18nMessage zaMaloZrodel = new I18nMessagePlEn("Wybierz przynajmniej jedno źródło", "Select at least one source");
    public static final I18nMessage konektor = new I18nMessagePlEn("Konektor", "Connector");
    public static final I18nMessage serwer = new I18nMessagePlEn("Serwer", "Server");
    public static final I18nMessage status = new I18nMessagePlEn("Status", "Status");
    public static final I18nMessage liczbaPlikow = new I18nMessagePlEn("Liczba plików", "File number");
    public static final I18nMessage testujPolaczenie = new I18nMessagePlEn("Testuj połączenie", "Test connection");
    public static final I18nMessage domenyAD = new I18nMessagePlEn("Domeny (oddzielone średnikiem):", "Domains (use ';' as separator):");
    public static final I18nMessage uzywajADAvatar = new I18nMessagePlEn("Pole AD z linkem do zdjęcia użytkownika", "Use user AD field as url to user avatar");
    public static final I18nMessage ukryjHaslo = new I18nMessagePlEn("Ukryj hasło", "Hide password");
    public static final I18nMessage sprawdzajLiczbePotomkow = new I18nMessagePlEn("Sprawdzaj liczbę potomków", "Check children count");
    public static final I18nMessage port = new I18nMessagePlEn("Port", "Port");
    public static final I18nMessage bladWPobieraniuDanych = new I18nMessagePlEn("Błąd w pobieraniu danych", "Error in getting data");
    public static final I18nMessage bladWZapisieDanychWBD = new I18nMessagePlEn("Błąd w zapisie danych do bazy BIKS", "Error in save data in BIKS database");
    public static final I18nMessage pobieranieDanych = new I18nMessagePlEn("Pobieranie danych ze źródła", "Getting data from source");
    public static final I18nMessage zapisDanychDoBD = new I18nMessagePlEn("Zapis danych do bazy BIKSa", "Saving data in BIKS database");
    public static final I18nMessage usuwanieTabelTymczasowych = new I18nMessagePlEn("Usuwanie tabel tymczasowych", "Deleting temporary tables");
    public static final I18nMessage trwaUsuwanieSerwera = new I18nMessagePlEn("Trwa usuwanie serwera.", "Deleting server.");
    public static final I18nMessage czyNaPewnoChceszUsunacSerwer = new I18nMessagePlEn("Czy na pewno chcesz usunąć wybrany serwer?", "Are you sure, you want to delete selected server?");
    public static final I18nMessage spolecznosc = new I18nMessagePlEn("Społeczność", "Community");
    public static final I18nMessage pobierz = new I18nMessagePlEn("Pobierz", "Download");
    public static final I18nMessage requestID = new I18nMessagePlEn("ID Wykonania", "Request ID");
    public static final I18nMessage errorCount = new I18nMessagePlEn("Liczba błędnych rekordów [Lbr]", "Error count");
    public static final I18nMessage caseCount = new I18nMessagePlEn("Liczba przetestowanych rekordów [Lpr]", "Case count");
    public static final I18nMessage startTimestamp = new I18nMessagePlEn("Data wykonania", "Start Timestamp");
    public static final I18nMessage errorCountDivCaseCount = new I18nMessagePlEn("[Lbr / Lpr] (%)", "Error count / Case count (%)");
    public static final I18nMessage errorCountDivCaseCountShort = new I18nMessagePlEn("[Lbr / Lpr] (%)", "Error count / Case count (%)");
    public static final I18nMessage inicjowanieZakonczoneWczytywanieDanych = new I18nMessagePlEn("Inicjowanie połączenia zakończone. Wczytywanie danych.", "Initializing connection completed. Loading data");
    public static final I18nMessage databaseFilter = new I18nMessagePlEn("Filtr na bazy danych (oddzielone przecinkiem - ',')", "Database filter: (use ',' as separator)");
    public static final I18nMessage activeRequestForTest = new I18nMessagePlEn("Aktywne wykonania testu w dniach (-1 - nie dotyczy)", "Active request for test parameter in days (-1 for disable)");
    public static final I18nMessage showInactiveTests = new I18nMessagePlEn("Pokazuj nieaktywne testy", "Show inactive tests");
    public static final I18nMessage sprawdzajAktywnoscTestu = new I18nMessagePlEn("Sprawdzaj aktywność testu", "Check test activity");
    public static final I18nMessage ignorujGrupyTestow = new I18nMessagePlEn("Ignoruj grupy testów (oddzielone przecinkiem - ',')", "Ignore test groups: (which use comma (,) as separator, without space)");
    public static final I18nMessage sciezkaDoPliku = new I18nMessagePlEn("Ścieżka do pliku", "Path to file");
    public static final I18nMessage sciezkaDoKatalogu = new I18nMessagePlEn("Ścieżka do katalogu", "Path to folder");
    public static final I18nMessage wybierzServerDoSkonfigurowania = new I18nMessagePlEn("Wybierz serwer do skonfigurowania", "Select server to be configured");
    public static final I18nMessage dodajNowySerWer = new I18nMessagePlEn("Dodaj nowy serwer", "Add new server");
    public static final I18nMessage istniejeTakiSerwer = new I18nMessagePlEn("Istnieje serwer o takiej nazwie! Wybierz inną.", "Server name exists! Choose a different name. The server was not added");
    public static final I18nMessage serwerZostalDodany = new I18nMessagePlEn("Serwer został dodany", "New server was added");
    public static final I18nMessage usunZaznaczonySerwer = new I18nMessagePlEn("Usuń zaznaczony serwer", "Delete selected server");
    public static final I18nMessage usunietoSerwer = new I18nMessagePlEn("Usunięto serwer", "Server was removed");
    public static final I18nMessage serwery = new I18nMessagePlEn("Serwery", "Servers");
    public static final I18nMessage wlasciwosciRozszerzone = new I18nMessagePlEn("Właściwości rozszerzone", "Extended properties");
    public static final I18nMessage tabela = new I18nMessagePlEn("Tabela", "Table");
    public static final I18nMessage widok = new I18nMessagePlEn("Widok", "View");
    public static final I18nMessage procedura = new I18nMessagePlEn("Procedura", "Procedure");
    public static final I18nMessage wartoscDomyslna = new I18nMessagePlEn("Wartość domyślna", "Default value");
    public static final I18nMessage funkcjaSkalarna = new I18nMessagePlEn("Funkcja skalarna", "Scalar function");
    public static final I18nMessage funkcjaTabelaryczna = new I18nMessagePlEn("Funkcja tabelaryczna", "Table function");
    public static final I18nMessage kolumna = new I18nMessagePlEn("Kolumna", "Column");
    public static final I18nMessage naPrzyklad = new I18nMessagePlEn("Na przykład", "For example");
    public static final I18nMessage ftpMetadataFolder = new I18nMessagePlEn("FTP folder z plikami metadanych", "FTP Path to folder with metadata files");
    public static final I18nMessage ftpTestFolder = new I18nMessagePlEn("FTP folder z plikami testów", "FTP Path to folder with test files");
    public static final I18nMessage ftpHost = new I18nMessagePlEn("FTP Host", "FTP Host");
    public static final I18nMessage ftpPort = new I18nMessagePlEn("FTP Port", "FTP Port");
    public static final I18nMessage ftpUser = new I18nMessagePlEn("FTP Użytkownik", "FTP User");
    public static final I18nMessage ftpPass = new I18nMessagePlEn("FTP Hasło", "FTP Password");
    public static final I18nMessage sciezkiDoPlikow = new I18nMessagePlEn("Ścieżki do plików", "Paths to files");
    public static final I18nMessage sciezkaDoKonektoraDesigner = new I18nMessagePlEn("Ścieżka do pliku konektora Designer SDK (.exe):", "Path to designer pump (.exe):");
    public static final I18nMessage sciezkaDoKonektoraReportSDK41 = new I18nMessagePlEn("Ścieżka do pliku konektora Report SDK 4.1+ (.jar):", "Path to Report SDK pump for BO 4.1+(.jar):");
    public static final I18nMessage sciezkaDoKonektoraIDTSDK41 = new I18nMessagePlEn("Ścieżka do pliku konektora IDT SDK 4.1+ (.jar):", "Path to IDT SDK pump for BO 4.1+(.jar):");
    public static final I18nMessage sciezkaDoNarzedziKlienckich41 = new I18nMessagePlEn("Ścieżka do narzędzi klienckiech BO 4.1+:", "Path to client tools for BO 4.1+:");
    public static final I18nMessage sciezkaDoLcmCli = new I18nMessagePlEn("Ścieżka do LCMCLI (.jar):", "Path to LCMCLI (.jar):");
    public static final I18nMessage sciezkaDoDrl = new I18nMessagePlEn("Ścieżka do katalogu plików .drl:", "Path to .drl folder:");
    public static final I18nMessage jobCuid = new I18nMessagePlEn("JOB_CUID", "JOB_CUID");
    public static final I18nMessage liczbaObiektowDoBackupowania = new I18nMessagePlEn("Liczba obiektów do archiwizacji", "Number of objects to backup");
    public static final I18nMessage sciezkaDoJREx86 = new I18nMessagePlEn("Ścieżka do folderu bin JRE x86:", "Path to JRE x86 bin folder:");
    public static final I18nMessage przed = new I18nMessagePlEn("Przed", "Before");
    public static final I18nMessage ostatniElement = new I18nMessagePlEn("<ostatni element>", "<as last item>");
    public static final I18nMessage pobierajObiektyUzytkownikow = new I18nMessagePlEn("Pobieraj obiekty użytkowników", "Load BO metadata with users reports");
    public static final I18nMessage linkDoOpenDoc = new I18nMessagePlEn("Link do OpenDoc", "Open report link template");
    public static final I18nMessage sprawdzajUprawnieniaUzytkownika = new I18nMessagePlEn("Sprawdzaj uprawnienia uzytkownika przy otwieraniu raportu", "Check user access before open report");
    public static final I18nMessage wersjaBO40 = new I18nMessagePlEn("BusinessObjects w wersji 4.0", "Is BusinessObjects in version 4.0");
    public static final I18nMessage pozycjaWMenu = new I18nMessagePlEn("Pozycja w menu", "Menu position");
    public static final I18nMessage zapytaniePobierajaceTabele = new I18nMessagePlEn("Zapytanie pobierające schematy", "SQL SELECT statement for Teradata databases");
    public static final I18nMessage maksymalnaIloscBledowDlaShowTables = new I18nMessagePlEn("Maksymalna ilość błedów dla showtable (domyslnie 0)", "Max error count for showtable (default 0)");
    public static final I18nMessage plik = new I18nMessagePlEn("Plik", "File");
    public static final I18nMessage testJednozrodlowy = new I18nMessagePlEn("Test jednoźródłowy", "One-source test");
    public static final I18nMessage testWielozrodlowy = new I18nMessagePlEn("Test wieloźródłowy", "Many-source test");
    public static final I18nMessage czasOdczytuDanych = new I18nMessagePlEn("Odczyt danych źródłowych", "Data read time");
    public static final I18nMessage zapisDanyWBiks = new I18nMessagePlEn("Zapis danych w BIKS", "Data write time");
    public static final I18nMessage zapiszDrzewo = new I18nMessagePlEn("Zapisz drzewo", "Save tree");
    public static final I18nMessage calkowityCzasTrwania = new I18nMessagePlEn("Całkowity czas trwania", "Full elepsed time");
    public static final I18nMessage wyswietlajJakoLiczbe = new I18nMessagePlEn("Wyświetlaj jako liczbę", "Display as number");
    public static final I18nMessage wprowadzonaWartoscNieJestLiczba = new I18nMessagePlEn("Wprowadzona wartość nie jest liczbą", "Entered value is not a number") /* I18N:  */; // #45
    public static final I18nMessage system = new I18nMessagePlEn("System", "System");
    public static final I18nMessage eksportCsv = new I18nMessagePlEn("Eksport drzewa", "Export tree");
    public static final I18nMessage eksportDrzewa = new I18nMessagePlEn("Eksport drzewa (.zip)", "Export tree (.zip)");
    public static final I18nMessage generujHtml = new I18nMessagePlEn("Tworzenie dokumentu (.html)", "Create document (.html)");
    public static final I18nMessage generujPdf = new I18nMessagePlEn("Tworzenie dokumentu (.pdf)", "Create document (.pdf)");
    public static final I18nMessage eksportWynikow = new I18nMessagePlEn("Eksport wyników", "Export results");
    public static final I18nMessage eksportDowiazaniPowiazan = new I18nMessagePlEn("Eksport dowiązań/powiązań", "Export links/associations");
    public static final I18nMessage tworzenieHTML = new I18nMessagePlEn("Tworzenie HTML", "Create HTML");
//    public static final I18nMessage eksportPowiazan = new I18nMessagePlEn("Eksport powiązań", "Export associations");
    public static final I18nMessage importDrzewa = new I18nMessagePlEn("Import drzewa", "Import tree");
    public static final I18nMessage plikMetadata = new I18nMessagePlEn("Plik metadata", "Metadata file");
    public static final I18nMessage importDowiazanLubPowiazan = new I18nMessagePlEn("Import dowiązań/powiązań", "Import links/associations");
    public static final I18nMessage importPowiazan = new I18nMessagePlEn("Import powiązań", "Import associations");
    public static final I18nMessage typTestu = new I18nMessagePlEn("Typ testu", "Test type");
    public static final I18nMessage zrodla = new I18nMessagePlEn("Źródła", "Sources");
    public static final I18nMessage dataWykonaniaTestu = new I18nMessagePlEn("Data wykonania testu", "Date of test request");
    public static final I18nMessage zakresDanych = new I18nMessagePlEn("Zakres danych", "The scope of data");
    public static final I18nMessage pokazZakresDanych = new I18nMessagePlEn("Pokaż zakres danych", "Show the scope of data");
    public static final I18nMessage importDanychDoDrzewa = new I18nMessagePlEn("Trwa import danych do drzewa. Proszę czekać.", "Importing data to BIKS tree. Please wait.");
    public static final I18nMessage zaimportowanoDaneDoDrzewa = new I18nMessagePlEn("Zaimportowano dane do drzewa", "Imported data to the tree");
    public static final I18nMessage classicView = new I18nMessagePlEn("Widok obiektów: klasyczny", "Classic");
    public static final I18nMessage differentView = new I18nMessagePlEn("Widok obiektów: z opisem", "With description");
    public static final I18nMessage adresSerwera = new I18nMessagePlEn("Serwer", "Server");
    public static final I18nMessage dodajOcene = new I18nMessagePlEn("Dodaj ocenę", "Add rate");
    public static final I18nMessage edytujOcene = new I18nMessagePlEn("Edytuj ocenę", "Edit rate");
    public static final I18nMessage skontrolujOkresowaOceneDanych = new I18nMessagePlEn("Skontroluj ocenę jakości danych", "Check the data quality assessment");
    public static final I18nMessage dodajRaportJakosciDanych = new I18nMessagePlEn("Dodaj raport jakości danych", "Add the data quality report");
    public static final I18nMessage pobierzXLS = new I18nMessagePlEn("Pobierz XLSX", "Download XLSX");
    public static final I18nMessage odswiezRaport = new I18nMessagePlEn("Odśwież raport", "Refresh the report");
    public static final I18nMessage okresOd = new I18nMessagePlEn("Okres od", "Period from");
    public static final I18nMessage okresDoPelny = new I18nMessagePlEn("Okres do", "Period to");
    public static final I18nMessage okresDo = new I18nMessagePlEn("do", "to");
    public static final I18nMessage grupaDanych = new I18nMessagePlEn("Grupa danych", "Data group");
    public static final I18nMessage opiekunGrupyDanych = new I18nMessagePlEn("Opiekun grupy danych", "Data group leader");
    public static final I18nMessage dataSporzadzenia = new I18nMessagePlEn("Data sporządzenia", "Created date");
    public static final I18nMessage poprzedniPoziomJakosci = new I18nMessagePlEn("Poprzedni poziom jakości", "Previous data quality level");
    public static final I18nMessage aktualnyPoziomJakosci = new I18nMessagePlEn("Aktualny poziom jakości", "Actual data quality level");
    public static final I18nMessage istotnoscGrupyDanych = new I18nMessagePlEn("Istotność grupy danych", "Significance of data group");
    public static final I18nMessage planDzialania = new I18nMessagePlEn("Plan działania", "Action plan");
    public static final I18nMessage dodanoOcene = new I18nMessagePlEn("Dodano ocenę", "Evaluation added");
    public static final I18nMessage zedytowanoOcene = new I18nMessagePlEn("Zedytowano ocenę", "Evaluation edited");
    public static final I18nMessage ocenaJakosciDanych = new I18nMessagePlEn("Ocena jakości danych", "Data Quality Evaluation");
    public static final I18nMessage okresOdNieMozeBycPusty = new I18nMessagePlEn("Okres od musi być wypełniony", "The period from must be filled");
    public static final I18nMessage okresDoNieMozeBycPusty = new I18nMessagePlEn("Okres do musi być wypełniony", "The period to must be filled");
    public static final I18nMessage wybierzOpiekunaGrupDanych = new I18nMessagePlEn("Wybierz opiekuna grup danych", "Choose data group leader");
    public static final I18nMessage bladWOkresachObowiazywania = new I18nMessagePlEn("Brak poprawnych okresów obowiązywania oceny", "No valid evaluation periods");
    public static final I18nMessage kontrolaOkresowejOcenyJakosci = new I18nMessagePlEn("Kontrola Okresowej Oceny Jakości", "Periodic Quality Assessment Inspection");
    public static final I18nMessage akceptuj = new I18nMessagePlEn("Akceptuj", "Accept");
    public static final I18nMessage odrzuc = new I18nMessagePlEn("Odrzuć", "Reject");
    public static final I18nMessage skontrolowanoOkresowaOceneJakosci = new I18nMessagePlEn("Skontrolowano okresową ocenę jakości", "Periodical quality assessment inspected");
    public static final I18nMessage odrzuconoOceneJakosciDanych = new I18nMessagePlEn("odrzucono ocenę jakości", "Quality assessment rejected");
    public static final I18nMessage proszePoprawicOcene = new I18nMessagePlEn("Proszę poprawić ocenę.", "Please correct assessment.");
    public static final I18nMessage wybierzZakresRaportu = new I18nMessagePlEn("Wybierz zakres raportu", "Select report scope");
    public static final I18nMessage wygenerowanoRaport = new I18nMessagePlEn("Wygenerowano raport", "Report was generated");
    public static final I18nMessage nr = new I18nMessagePlEn("Nr", "Lp");
    public static final I18nMessage raportJakosciDanych = new I18nMessagePlEn("Raport jakości danych", "Data Quality report");
    public static final I18nMessage czyChceszOdswiezycRaportZaOkres = new I18nMessagePlEn("Czy chcesz odświeżyć raport?", "Do you want to refresh the report?");
    public static final I18nMessage odswiezonoRaport = new I18nMessagePlEn("Odświeżono raport", "Report was refreshed");
    public static final I18nMessage brakOkresowDoDodania = new I18nMessagePlEn("Brak okresów do dodania lub dodano już oceny za wszystkie okresy. Poczekaj aż zakończy się okres i spróbuj ponownie.", "No periods to be added or already added assessment for all periods. Wait until the period ends and try again.");
    public static final I18nMessage okresSprawozdawczy = new I18nMessagePlEn("Okres oceny", "Evaluation period");
    public static final I18nMessage raportNaDzien = new I18nMessagePlEn("Raport na dzień", "Report for day");
    public static final I18nMessage statusOceny = new I18nMessagePlEn("Status oceny", "Evaluation status");
    public static final I18nMessage brakOceny = new I18nMessagePlEn("Brak oceny", "Lack of evaluation");
    public static final I18nMessage ocenaOdrzucona = new I18nMessagePlEn("Ocena odrzucona", "Evaluation rejected");
    public static final I18nMessage ocenaZaakceptowana = new I18nMessagePlEn("Ocena zaakceptowana", "Evaluation accepted");
    public static final I18nMessage nieSkontolowano = new I18nMessagePlEn("Nie skontrolowano oceny", "Evaluation not checked");
    public static final I18nMessage usersGroup = new I18nMessagePlEn("Grupy użytkowników", "Users group");
    public static final I18nMessage userGroup = new I18nMessagePlEn("Grupa użytkowników", "User group");
    public static final I18nMessage loadQuery = new I18nMessagePlEn("Zapytania zasileń", "Queries to load");
    public static final I18nMessage jdbcUrl = new I18nMessagePlEn("JDBC Url", "JDBC Url");
    public static final I18nMessage proszeWybrac = new I18nMessagePlEn("Proszę wybrać", "Please choose");
    public static final I18nMessage wybierzDrzewo = new I18nMessagePlEn("Wybierz drzewo", "Choose a tree");
    public static final I18nMessage klasaSterownika = new I18nMessagePlEn("Klasa sterownika", "Driver class");
    public static final I18nMessage userInGroup = new I18nMessagePlEn("Użytkownicy w grupie", "Users in group:");
    public static final I18nMessage groupIn = new I18nMessagePlEn("Należy do grup:", "Member of:");
    public static final I18nMessage inGroup = new I18nMessagePlEn("Zawiera grupy:", "Members:");
    public static final I18nMessage dodajZgloszenie = new I18nMessagePlEn("Dodaj błąd", "Add error");
    public static final I18nMessage otwarty = new I18nMessagePlEn("Otwarty", "Open");
    public static final I18nMessage zamkniety = new I18nMessagePlEn("Zamknięty", "Closed");
    public static final I18nMessage zgloszenieBleduDanych = new I18nMessagePlEn("Zgłoszenie błędu danych", "Data error issue");
    public static final I18nMessage dodanoZgloszenie = new I18nMessagePlEn("Dodano zgłoszenie", "Issue added");
    public static final I18nMessage zapisanoZgloszenie = new I18nMessagePlEn("Zapisano zgłoszenie", "Issue saved");
    public static final I18nMessage edytujZgloszenie = new I18nMessagePlEn("Edytuj zgłoszenie", "Edit issue");
    public static final I18nMessage id = new I18nMessagePlEn("id", "id");
    public static final I18nMessage lp = new I18nMessagePlEn("Lp.", "No.");
    public static final I18nMessage liczbaBledow = new I18nMessagePlEn("Liczba błędów", "Number of issues");
    public static final I18nMessage szablon = new I18nMessagePlEn("Szablon", "Template");
    public static final I18nMessage jakNapisacZapytanie = new I18nMessagePlEn("Jak napisać zapytanie?", "How to construct query?");
    public static final I18nMessage jdbcZapytanie = new I18nMessagePlEn("Zapytanie powinno zwracań następujące kolumny:<br><br><b>OBJ_ID</b> - identyfikator obiektu - musi być unikalny i nie może być wartością &lt;null&gt;<br><b>PARENT_OBJ_ID</b> - identyfikator rodzica obiektu - informuje o obiekcie nadrzędnym. &lt;null&gt; jeśli obiekt nie ma rodzica (tzw. root)<br><b>NAME </b>- nazwa obiektu<br><b>NODE_KIND_CODE</b> - typ węzła z bazy BIKS, np.: <i>DocumentsFolder, </i><i>Document, UsersGroup, User, Txt, Blog, BlogEntry, Article, Excel, Pdf, Word, Powerpoint, Webi, Glossary, GlossaryCategory</i><br><b>DESCR </b>- opis obiektu<br><b>VISUAL_ORDER</b> - kolejność wyświetlania. Jeśli nie podana (null), obiekty sortowane są po nazwie.<br><br>W kolejnych kolumnach należy podawać nazwy atrybutów, jakie mają być dodane do obiektów.", "Query should return the following columns:<br><br><b>OBJ_ID</b> - the object identifier - must be unique and can not be value &lt;null&gt;<br><b>PARENT_OBJ_ID</b> - ID of parent object - inform the parent object. &lt;null&gt; if the object does not have a parent (ie. root)<br><b>NAME </b>- the name of the object<br><b>NODE_KIND_CODE</b> - the type of node from BIKS database, eg.: <i>DocumentsFolder, </i><i>Document, UsersGroup, User, Txt, Blog, BlogEntry, Article, Excel, Pdf, Word, Powerpoint, Webi, Glossary, GlossaryCategory</i><br><b>DESCR </b>- description of the object<br><b>VISUAL_ORDER</b> - a sequence number. If not specified, objects are sorted by name.<br><br>Next column should be named as how the attributes to be added.");
    public static final I18nMessage domena = new I18nMessagePlEn("Domena", "Domain");
    public static final I18nMessage pobierajPliki = new I18nMessagePlEn("Pobieraj pliki", "Download files");
    public static final I18nMessage zasilenieDoDrzewa = new I18nMessagePlEn("Wyniki zapytań zostaną zasilone do drzewa", "Queries' results will be saved to tree");
    public static final I18nMessage zasilenieDoPlikow = new I18nMessagePlEn("Wyniki zapytań zostaną zapisane do plików", "Queries' results will be saved to files");
    public static final I18nMessage zaladuj = new I18nMessagePlEn("Załaduj", "Reload");
    public static final I18nMessage wybierOpcjiDoEksportu = new I18nMessagePlEn("Wybierz opcji do eksportu", "Select export options");
    public static final I18nMessage eksportuj = new I18nMessagePlEn("Eksportuj", "Export");
    public static final I18nMessage typWezla = new I18nMessagePlEn("Typ węzła", "Node kind");
    public static final I18nMessage sciezkaDoObiektu = new I18nMessagePlEn("Ścieżka do obiektu", "Path to object");
    public static final I18nMessage formatPliku = new I18nMessagePlEn("Format pliku", "File format");
    public static final I18nMessage wTrakciePrzygotowaniaWynikow = new I18nMessagePlEn("W trakcie przygotowania wyników...", "Preparing results...");
    public static final I18nMessage uwzglednijUprawnieniaZDomen = new I18nMessagePlEn("Uwzględnij uprawnienia z grup", "Include permissions from groups");
    public static final I18nMessage rodzajUzytkownika = new I18nMessagePlEn("Wybierz typ konta do dodania", "Select account type to add");
    public static final I18nMessage kontoZAD = new I18nMessagePlEn("Konto z AD", "Active Directory account");
    public static final I18nMessage kontoKlasyczne = new I18nMessagePlEn("Konto klasyczne (enterprise)", "Enterprise account");
    public static final I18nMessage dla = new I18nMessagePlEn("dla", "for");
    public static final I18nMessage zapytanieSQLDlaPowiazanychObiektow = new I18nMessagePlEn("Zapytanie SQL dla powiązanych obiektów", "SQL Query for joined objects");
    public static final I18nMessage jdbcPowiazaniaZapytanie = new I18nMessagePlEn("Zapytanie powinno zwracań następujące kolumny:<br><br><b>SRC_ID</b> - identyfikator obiektu (OBJ_ID) z zapytania bazowego - obiekt dla którego ma pokazać się powiązanie<br><b>DST_ID</b> - identyfikator obiektu (OBJ_ID) z zapytania bazowego - obiekt do którego prowadzi powiązanie<br>", "Query should return the following columns:<br><br><b>SRC_ID</b> - the object identifier (OBJ_ID) of the basic question - the source odbject<br><b>DST_ID</b> - identifier object (OBJ_ID) of the basic question - the destination object<br>");
    public static final I18nMessage trwaPrzygotowanieEksportu = new I18nMessagePlEn("Trwa przygotowanie eksportu", "Preparing export");
    public static final I18nMessage trwaPrzygotowanieImportu = new I18nMessagePlEn("Trwa przygotowanie importu", "Preparing import");
    public static final I18nMessage typUwierzytelnienia = new I18nMessagePlEn("Typ uwierzytelnienia", "Authentication mode");
    public static final I18nMessage pytajPrzyPolaczeniu = new I18nMessagePlEn("Pytaj przy połączeniu", "Ask before connect");
    public static final I18nMessage uzywajLogin = new I18nMessagePlEn("Używaj login", "Use login");
    public static final I18nMessage dodajFolder = new I18nMessagePlEn("Dodaj folder", "Add folder");
    public static final I18nMessage zgrupowanieOperacji = new I18nMessagePlEn("Zgrupowanie operacji", "Operations grouping");
    public static final I18nMessage podanaWartoscNieJestLiczba = new I18nMessagePlEn("Podana wartość nie jest liczbą", "Not valid number");
    public static final I18nMessage rss = new I18nMessagePlEn("RSS", "RSS");
    public static final I18nMessage przygotowywaniePliku = new I18nMessagePlEn("Przygotowanie pliku z wynikami testu...", "Prepering the file.");
    public static final I18nMessage granicaRozmiaruPlikow = new I18nMessagePlEn("Granica rozmiaru plików (MB)", "Max file size (MB)");
    public static final I18nMessage maskaPlikow = new I18nMessagePlEn("Maska plików (np.: *.java, File???.doc)", "File name patterns (example: *.java, File???.doc)");
    public static final I18nMessage maska = new I18nMessagePlEn("Maska", "Wildcard");
    public static final I18nMessage dataModyfikaji = new I18nMessagePlEn("Data modyfikacji", "Modification date");
    public static final I18nMessage pobieraniePlikow = new I18nMessagePlEn("Pobieranie plików", "Download files");
    public static final I18nMessage eksport = new I18nMessagePlEn("Eksport", "Export");
    public static final I18nMessage eksportGeneruj = new I18nMessagePlEn("Eksport/Tworzenie dokumentu", "Export/Create document");
    public static final I18nMessage wybierzSlownikDoEksportu = new I18nMessagePlEn("Wybierz słownik do eksportu", "Select dictionary to export");
    public static final I18nMessage wybierzSlownikDoImportu = new I18nMessagePlEn("Wybierz słownik do importu", "Select dictionary to import");
    public static final I18nMessage mapowanieNazwKolumn = new I18nMessagePlEn("Mapowanie nazw kolumn", "Mapping columns' name");
    public static final I18nMessage nazwaEksportu = new I18nMessagePlEn("Nazwa eksportu", "Export name");
    public static final I18nMessage konfiguracjaImportu = new I18nMessagePlEn("Konfiguracja importu", "Import config");
    public static final I18nMessage trybImportu = new I18nMessagePlEn("Tryb importu", "Import type");
    public static final I18nMessage nadpisywanie = new I18nMessagePlEn("Nadpisywanie", "Override");
    public static final I18nMessage dodawanie = new I18nMessagePlEn("Dodawanie", "Add");
    public static final I18nMessage usuwanie = new I18nMessagePlEn("Usuwanie", "Delete");
    public static final I18nMessage nazwaWPliku = new I18nMessagePlEn("Nazwa w pliku", "Name in file");
    public static final I18nMessage ostatnieWykonanieTestu = new I18nMessagePlEn("Ostatnie wykonanie testu", "Last date of test request");
    public static final I18nMessage dataWykonaniaDlaProfile = new I18nMessagePlEn("Data wygenerowania danych z Profile", "Date of creating Profile file");
    public static final I18nMessage biksaktualnosci = new I18nMessagePlEn("Komunikaty BIKS", "BIKS Announcements");
    public static final I18nMessage biksaktualnosciTytul = new I18nMessagePlEn("Najnowsze komunikaty od BIKSa", "Latest BIKS announcements!");
    public static final I18nMessage daneNieSaPrawidlowe = new I18nMessagePlEn("Dane nie są prawidłowe", "Data is not valid");
    public static final I18nMessage liczbyKolumnyNieSieZgadzaja = new I18nMessagePlEn("Liczy kolumny nie się zgadzają", "No. of columns are not valid");
    public static final I18nMessage brakKolumn = new I18nMessagePlEn("Brak kolumny", "Lack of columns");
    public static final I18nMessage walidacjaNieUdalaSie = new I18nMessagePlEn("Walidacja nie udała się", "Validation was not successful");
    public static final I18nMessage powtarzajaSieRekordyWPliku = new I18nMessagePlEn("Powtarzają się linie w pliku", "Duplicate lines in files");
    public static final I18nMessage juzIstniejaWBazieNiektoreRekordy = new I18nMessagePlEn("Już istnieją w bazie niektóre rekordy", "Some records exists in database");
    public static final I18nMessage dodanoZaktualizowano = new I18nMessagePlEn("Dodano/Zaktualizowano", "Added/Updated");
    public static final I18nMessage rekordy = new I18nMessagePlEn("Rekordy", "Records");
    public static final I18nMessage analizuj = new I18nMessagePlEn("Analizuj", "Analyse");
    public static final I18nMessage przegladImportu = new I18nMessagePlEn("Przegląd importu", "Import overview");
    public static final I18nMessage dodanoDoSlownika = new I18nMessagePlEn("Zostaną dodane do słownika", "Add to dictionary");
    public static final I18nMessage usunietoZSlownika = new I18nMessagePlEn("Zostaną usunięte ze słownika", "Delete from dictionary");
    public static final I18nMessage zaktualizowanoWSlowniku = new I18nMessagePlEn("Zostaną zaktualizowane w słowniku", "Update in dictionary");
    public static final I18nMessage niePrzeszloWalidacji = new I18nMessagePlEn("Nie przeszło walidacji", "Do not pass validation");
    public static final I18nMessage brakWartosciKluczaGlownego = new I18nMessagePlEn("Brak wartości klucza głównego", "Primary key has null or empty value");
    public static final I18nMessage pokaz = new I18nMessagePlEn("Pokaż", "Show");
    public static final I18nMessage listaAkcji = new I18nMessagePlEn("Lista akcji", "Action list");
    public static final I18nMessage lista = new I18nMessagePlEn("Lista", "List");
    public static final I18nMessage nrLinii = new I18nMessagePlEn("Nr. linii", "Line nr.");
    public static final I18nMessage dodajeszDoUlubionych = new I18nMessagePlEn("Dodajesz do ulubionych obiekt", "You are adding object to favourites");
    public static final I18nMessage istniejaElementyPodobne = new I18nMessagePlEn("Istnieją elementy podobne. Czy chcesz je także dodać?", "There are similar elements. Do you want to add them?");
    public static final I18nMessage descendantsObjectInFvsDeleted = new I18nMessagePlEn("Usunięty", "Deleted");
    public static final I18nMessage descendantsObjectInFvsNew = new I18nMessagePlEn("Nowy", "New");
    public static final I18nMessage descendantsObjectInFvsJoinedObjIdsAdded = new I18nMessagePlEn("Dodano powiązań", "Associations added");
    public static final I18nMessage descendantsObjectInFvsJoinedObjIdsDeleted = new I18nMessagePlEn("Usunięto powiązań", "Associations deleted");
    public static final I18nMessage descendantsObjectInFvsChangedAttrs = new I18nMessagePlEn("Zmieniono atrybuty", "Attributes changed");
    public static final I18nMessage descendantsObjectInFvsDqcTestFailCnt = new I18nMessagePlEn("Negatywnych wyników testu", "Negative test results");
    public static final I18nMessage descendantsObjectInFvsNewCommentCnt = new I18nMessagePlEn("Nowych komentarzy", "New comments");
    public static final I18nMessage descendantsObjectInFvsVoteValDeltaAdd = new I18nMessagePlEn("Wartość głosów wzrosła o", "Votes' value increased by");
    public static final I18nMessage descendantsObjectInFvsVoteValDeltaDelete = new I18nMessagePlEn("Wartość głosów zmalała o", "Votes' value decreased by");
    public static final I18nMessage descendantsObjectInFvsVoteCntDeltaAdd = new I18nMessagePlEn("Dodano głosów", "Votes added");
    public static final I18nMessage descendantsObjectInFvsVoteCntDeltaDelete = new I18nMessagePlEn("Usunięto głosów", "Votes deleted");
    public static final I18nMessage descendantsObjectInFvsCut = new I18nMessagePlEn("Wycięty", "Cut out");
    public static final I18nMessage descendantsObjectInFvsPast = new I18nMessagePlEn("Wklejony", "Pasted");
    public static final I18nMessage objectInFvsCntJoinedObjChange = new I18nMessagePlEn("Zmieniono dowiązania  dla", "Joins changed for");
    public static final I18nMessage objectInFvsCntUpdate = new I18nMessagePlEn("Zaktualizowano atrybuty dla", "Updated attributes for");
    public static final I18nMessage objectInFvsCntNewComments = new I18nMessagePlEn("Dodano komentarze dla", "New commentc for");
    public static final I18nMessage objectInFvsCntVoteChange = new I18nMessagePlEn("Zmieniono głosy dla", "Votes changed for");
    public static final I18nMessage objectInFvsCntDqcTestFails = new I18nMessagePlEn("Negatywne wyniki dla", "Negative results for");
    public static final I18nMessage objectInFvsCntCut = new I18nMessagePlEn("Wycięte", "Cut out");
    public static final I18nMessage objectInFvsCntPast = new I18nMessagePlEn("Wklejone", "Pasted");
    public static final I18nMessage objectInFvsCntObjects = new I18nMessagePlEn("obiektów", "Objects'");
    public static final I18nMessage objectInFvsCntObject = new I18nMessagePlEn("obiektu", "Object's");
    public static final I18nMessage scheduleID = new I18nMessagePlEn("ID", "ID");
    public static final I18nMessage scheduleParentID = new I18nMessagePlEn("ID rodzica", "Parent ID");
    public static final I18nMessage scheduleStatus = new I18nMessagePlEn("Status", "Status");
    public static final I18nMessage scheduleNazwa = new I18nMessagePlEn("Nazwa", "Name");
    public static final I18nMessage scheduleStart = new I18nMessagePlEn("Start", "Start");
    public static final I18nMessage scheduleKoniec = new I18nMessagePlEn("Koniec", "End");
    public static final I18nMessage scheduleWlasciciel = new I18nMessagePlEn("Właściciel", "Owner");
    public static final I18nMessage scheduleIDRaportu = new I18nMessagePlEn("ID raportu", "Report ID");
    public static final I18nMessage scheduleCykliczny = new I18nMessagePlEn("Cykliczny", "Recurring");
    public static final I18nMessage scheduleCzasTrwania = new I18nMessagePlEn("Czas trwania", "Duration");
    public static final I18nMessage scheduleMiejsceDocelowe = new I18nMessagePlEn("Miejsce docelowe", "Destination");
    public static final I18nMessage scheduleMiejsceDoceloweSzczegoly = new I18nMessagePlEn("Miejsce docelowe - szczegóły", "Destination path");
    public static final I18nMessage scheduleZmodyfikowano = new I18nMessagePlEn("Zmodyfikowano", "Updated");
    public static final I18nMessage scheduleUtworzono = new I18nMessagePlEn("Utworzono", "Created");
    public static final I18nMessage scheduleBlad = new I18nMessagePlEn("Treść błędu", "Error message");
    public static final I18nMessage scheduleInstanceId = new I18nMessagePlEn("ID instancji", "Instance ID");
    public static final I18nMessage schedulePobranoDuzoRekordow = new I18nMessagePlEn("Za dużo wyników! Pobrano pierwsze 1000 rekordów (posortowane roznąco po SI_ID). Dodaj dodatkowe filtry, aby ograniczyć ilość rekordów!", "Too many results! First 1000 records downloaded (sorted in ascending order by SI_ID). Add additional filters to limit the amount of records!");
    public static final I18nMessage scheduleOstatniaGodzina = new I18nMessagePlEn("Ostatnia godzina", "Last hour");
    public static final I18nMessage scheduleOstatniDzien = new I18nMessagePlEn("Ostatni dzień", "Last day");
    public static final I18nMessage scheduleOstatniTydzien = new I18nMessagePlEn("Ostatni tydzień", "Last week");
    public static final I18nMessage scheduleOstatniMiesiac = new I18nMessagePlEn("Ostatnie 30 dni", "Last 30 days");
    public static final I18nMessage scheduleZPrzedzialu = new I18nMessagePlEn("Z przedziału", "Between Dates");
    public static final I18nMessage scheduleCUID = new I18nMessagePlEn("CUID", "CUID");
    public static final I18nMessage scheduleGUID = new I18nMessagePlEn("GUID", "GUID");
    public static final I18nMessage scheduleRUID = new I18nMessagePlEn("RUID", "RUID");
    public static final I18nMessage scheduleidFolderu = new I18nMessagePlEn("ID folderu", "Folder ID");
    public static final I18nMessage pokazTypyObiektow = new I18nMessagePlEn("Pokaż typy", "Types to display") /* I18N:  */; // #355
    public static final I18nMessage scheduleWstrzymaj = new I18nMessagePlEn("Wstrzymaj", "Pause");
    public static final I18nMessage scheduleWznow = new I18nMessagePlEn("Wznów", "Resume");
    public static final I18nMessage scheduleHarmonogramujPonownie = new I18nMessagePlEn("Harmonogramuj ponownie", "Re-schedule");
    public static final I18nMessage scheduleUsun = new I18nMessagePlEn("Usuń instancje", "Delete instance");
    public static final I18nMessage scheduleWstrzymanoInstancje = new I18nMessagePlEn("Wstrzymano instancje", "Instances was paused");
    public static final I18nMessage scheduleWznowionoInstancje = new I18nMessagePlEn("Wznowiono instancje", "Instances was resume");
    public static final I18nMessage scheduleZaharmonogramowano = new I18nMessagePlEn("Zaharmonogramowano instancje", "Instances was re-schedule");
    public static final I18nMessage scheduleUsunieto = new I18nMessagePlEn("Usunieto instancje", "Instances was deleted");
    public static final I18nMessage scheduleTrwaWykonywanieAkcji = new I18nMessagePlEn("Trwa wykonywanie akcji", "Action in progress");
    public static final I18nMessage brakzmianwulubionych = new I18nMessagePlEn("Brak zmian w ulubionych", "No updates in favorite tab");
    public static final I18nMessage wysylajPowiadomieniaMailowe = new I18nMessagePlEn("Wysyłaj powiadomienia mailowe", "Send e-mail notifications");
    public static final I18nMessage zmienHaslo = new I18nMessagePlEn("Zmień hasło", "Change password");
    public static final I18nMessage zmienZdjecie = new I18nMessagePlEn("Zmień zdjecie", "Change avatar");
    public static final I18nMessage dodajZdjecie = new I18nMessagePlEn("Dodaj zdjecie", "Add avatar");
    public static final I18nMessage dodawanieUzytkownika = new I18nMessagePlEn("Dodawanie użytkownika...", "Adding user...");
    public static final I18nMessage brakSugerowanychElementow = new I18nMessagePlEn("Brak sugerowanych elementów", "No suggested items");
    public static final I18nMessage wyswietlajPlikiNowszeNiz = new I18nMessagePlEn("Wyświetlaj pliki nowsze niż", "Show files since");
    public static final I18nMessage sugerowaneElementy = new I18nMessagePlEn("Sugerowane elementy", "Suggested items");
    public static final I18nMessage sugerowaneElementyPytanie = new I18nMessagePlEn("System BIKS zauważył, że oglądałeś ostatnio poniższe obiekty. Może warto by było dodać je do ulubionych, aby na bieżąco być informowanym o zmianach?", "BIKS noticed that you have recently watched these objects. Add them to your favorites and be informed about any changes.");
    public static final I18nMessage biInstances = new I18nMessagePlEn("Instancje BI", "BI Instance");
    public static final I18nMessage konfiguracjaSerwera = new I18nMessagePlEn("Konfiguracja serwera", "Server configuration");
    public static final I18nMessage folderZapisu = new I18nMessagePlEn("Folder zapisu", "Saved folder");
    public static final I18nMessage wszystkiePolaSaWymagane = new I18nMessagePlEn("Wszystkie pola są wymagane", "All field are required");
    public static final I18nMessage nieprawidlowyFormat = new I18nMessagePlEn("Nieprawidłowy format", "Invalid format");
    public static final I18nMessage scheduleReports = new I18nMessagePlEn("Raporty", "Reports");
    public static final I18nMessage scheduleFolders = new I18nMessagePlEn("Foldery", "Folders");
    public static final I18nMessage scheduleSelectReports = new I18nMessagePlEn("Wybierz raporty", "Select reports");
    public static final I18nMessage scheduleSelectFolders = new I18nMessagePlEn("Wybierz foldery", "Select folders");
    public static final I18nMessage scheduleSelected = new I18nMessagePlEn("Wybranych", "Selected");
    public static final I18nMessage scheduleConnections = new I18nMessagePlEn("Połączenia", "Connections");
    public static final I18nMessage scheduleConnection = new I18nMessagePlEn("Połączenie", "Connection");
    public static final I18nMessage scheduleUniverses = new I18nMessagePlEn("Świat obiektów", "Universes");
    public static final I18nMessage scheduleSelectConnections = new I18nMessagePlEn("Wybierz połączenia", "Select folders");
    public static final I18nMessage deleteFilters = new I18nMessagePlEn("Usuń filtry", "Delete filters");
    public static final I18nMessage scheduleZmodyfikowanoRaport = new I18nMessagePlEn("Data modyfikacji raportu", "Updated Report");
    public static final I18nMessage scheduleDatabaseEngine = new I18nMessagePlEn("Silnik bazy danych", "DatabaseEngine");
    public static final I18nMessage scheduleZmodyfikowanoSwiatobiektow = new I18nMessagePlEn("Data modyfikacji świata obiektów", "Updated Universe");
    public static final I18nMessage zarzadzaj = new I18nMessagePlEn("Zarządzaj", "Manage");
    public static final I18nMessage raporty = new I18nMessagePlEn("Raporty", "Reports");
    public static final I18nMessage swiatyobiektow = new I18nMessagePlEn("Światy obiektów", "Universes");
    public static final I18nMessage polaczenia = new I18nMessagePlEn("Połączenia", "Connections");
    public static final I18nMessage polaczenie = new I18nMessagePlEn("Połączenie", "Connection");
    public static final I18nMessage grupyUzytkownikow = new I18nMessagePlEn("Grupy użytkowników", "Users groups");
    public static final I18nMessage typObiektu = new I18nMessagePlEn("Typ obiektu", "Object Type");
    public static final I18nMessage lokalizacja = new I18nMessagePlEn("Lokalizacja", "Location");
    public static final I18nMessage zdalnie = new I18nMessagePlEn("Zdalnie", "Remote");
    public static final I18nMessage trwaUsuwanie = new I18nMessagePlEn("Trwa usuwanie...", "Deleting...");
    public static final I18nMessage objectZmienionoNazwe = new I18nMessagePlEn("Zmieniono nazwę", "Name was changed");
    public static final I18nMessage objectZmienionoOpis = new I18nMessagePlEn("Zmieniono opis", "Description was changed");
    public static final I18nMessage objectZmienionoWlasciciela = new I18nMessagePlEn("Zmieniono właściciela", "Owner was changed");
    public static final I18nMessage objectZmienNazwe = new I18nMessagePlEn("Zmień nazwę", "Change name");
    public static final I18nMessage objectZmienOpis = new I18nMessagePlEn("Zmień opis", "Change description");
    public static final I18nMessage objectZmienWlasciciela = new I18nMessagePlEn("Zmień właściciela", "Change owner");
    public static final I18nMessage konfiguracja = new I18nMessagePlEn("Konfiguracja", "Configuration");
    public static final I18nMessage objectOpis = new I18nMessagePlEn("Opis", "Description");
    public static final I18nMessage objectNowyOpis = new I18nMessagePlEn("Nowy opis", "New description");
    public static final I18nMessage objectNowaNazwa = new I18nMessagePlEn("Nowa nazwa", "New name");
    public static final I18nMessage objectDodajPrefiks = new I18nMessagePlEn("Dodaj prefiks", "Add prefix");
    public static final I18nMessage objectDodajPostfiks = new I18nMessagePlEn("Dodaj postfiks", "Add postfix");
    public static final I18nMessage zamien = new I18nMessagePlEn("Zamień", "Replace");
    public static final I18nMessage wykonajDlaKazdego = new I18nMessagePlEn("Wykonaj dla każdego", "Execute for each");
    public static final I18nMessage znajdz = new I18nMessagePlEn("Znajdź:", "Find:");
    public static final I18nMessage zamienNa = new I18nMessagePlEn("Zamień na:", "Replace with:");
    public static final I18nMessage gitRepozytorium = new I18nMessagePlEn("GIT repozytorium", "GIT repository");
    public static final I18nMessage zarchiwizowanoPlikow = new I18nMessagePlEn("Zarchiwizowano plików", "Archive files");
    public static final I18nMessage dataZakonczenia = new I18nMessagePlEn("Data zakończenia", "End date") /* I18N:  */; // #99
    public static final I18nMessage cuidProcesu = new I18nMessagePlEn("CUID zadania", "Task CUID") /* I18N:  */; // #99
    public static final I18nMessage auditRefreshCnt = new I18nMessagePlEn("Liczba odświeżeń", "Times refreshed");
    public static final I18nMessage auditViewCnt = new I18nMessagePlEn("Liczba otworzeń", "Times opened");
    public static final I18nMessage auditLastRefresh = new I18nMessagePlEn("Ostatnie odświeżenie", "Last refresh");
    public static final I18nMessage auditLastView = new I18nMessagePlEn("Ostatnie otworzenie", "Last opened");
    public static final I18nMessage auditUserLastView = new I18nMessagePlEn("Ostatnio otworzony przez", "Last opened by");
    public static final I18nMessage auditUserLastRefresh = new I18nMessagePlEn("Ostatnio odświeżony przez", "Last refreshed by");
    public static final I18nMessage procesow = new I18nMessagePlEn("proces(ów)", "process(es)");
    public static final I18nMessage audit = new I18nMessagePlEn("Audyt", "Audit");
    public static final I18nMessage objectManager = new I18nMessagePlEn("Zarządzanie obiektami", "Object Manager");
    public static final I18nMessage objectManagerAncestroID = new I18nMessagePlEn("ID przodka", "Ancestor ID");
    public static final I18nMessage auditRefreshCnt30day = new I18nMessagePlEn("Liczba odświeżeń z ostatnich 30 dni", "Number of refreshes in the last 30 days");
    public static final I18nMessage auditViewCnt30day = new I18nMessagePlEn("Liczba otworzeń z ostatnich 30 dni", "Number of openings in the last 30 days");
    public static final I18nMessage scheduleHarmonogramuj = new I18nMessagePlEn("Harmonogramuj", "Schedule");
    public static final I18nMessage scheduleHarmonogramujZTymiSamymiParametrami = new I18nMessagePlEn("Harmonogramuj z tymi samymi parametrami", "Schedule with the same parameters");
    public static final I18nMessage scheduleHarmonogramujZInnymiParametrami = new I18nMessagePlEn("Harmonogramuj z innymi parametrami", "Schedule with different parameters");
    public static final I18nMessage scheduleCykl = new I18nMessagePlEn("Cykl", "Cycle");
    public static final I18nMessage scheduleDataRozpoczecia = new I18nMessagePlEn("Data rozpoczęcia", "Start date");
    public static final I18nMessage scheduleDataZakonczenia = new I18nMessagePlEn("Data zakończenia", "End date");
    public static final I18nMessage scheduleFormaty = new I18nMessagePlEn("Formaty", "Formats");
    public static final I18nMessage scheduleDni = new I18nMessagePlEn("Dni(N)", "Days(N)");
    public static final I18nMessage scheduleMiesiac = new I18nMessagePlEn("Miesiac(N)", "Month(N)");
    public static final I18nMessage scheduleEmailOd = new I18nMessagePlEn("Od", "From");
    public static final I18nMessage scheduleEmailDo = new I18nMessagePlEn("Do", "To");
    public static final I18nMessage scheduleEmailTemat = new I18nMessagePlEn("Temat", "Subject");
    public static final I18nMessage scheduleEmailKomunikat = new I18nMessagePlEn("Wiadomość", "Message");
    public static final I18nMessage scheduleFilesNazwaUzytkownika = new I18nMessagePlEn("Nazwa uzytkownika", "User name");
    public static final I18nMessage scheduleFilesHaslo = new I18nMessagePlEn("Hasło", "Password");
    public static final I18nMessage scheduleFilesKatalog = new I18nMessagePlEn("Katalog", "Catalog");
    public static final I18nMessage scheduleNow = new I18nMessagePlEn("Teraz", "Now");
    public static final I18nMessage scheduleOnce = new I18nMessagePlEn("Raz", "Once");
    public static final I18nMessage scheduleDaily = new I18nMessagePlEn("Codziennie", "Daily");
    public static final I18nMessage scheduleMonthly = new I18nMessagePlEn("Co miesiąc", "Monthly");
    public static final I18nMessage scheduleWebi = new I18nMessagePlEn("Webi", "Webi");
    public static final I18nMessage scheduleExcel = new I18nMessagePlEn("Excel", "Excel");
    public static final I18nMessage schedulePdf = new I18nMessagePlEn("Pdf", "Pdf");
    public static final I18nMessage scheduleDefault = new I18nMessagePlEn("Domyślne", "Default");
    public static final I18nMessage scheduleEmail = new I18nMessagePlEn("Email", "Email");
    public static final I18nMessage scheduleFiles = new I18nMessagePlEn("System Plików", "File system");
    public static final I18nMessage dqmOgolne = new I18nMessagePlEn("Ogólne", "General");
    public static final I18nMessage dqmPolaczenia = new I18nMessagePlEn("Połączenia", "Connections");
    public static final I18nMessage dqmService = new I18nMessagePlEn("Service", "Service");
    public static final I18nMessage dqmPolaczenie = new I18nMessagePlEn("Połączenie", "Connection");
    public static final I18nMessage serwerBO = new I18nMessagePlEn("Serwer BO", "Server BO");
    public static final I18nMessage bazaAudytu = new I18nMessagePlEn("Baza audytu", "Audit database");
    public static final I18nMessage atrybutyObiektow = new I18nMessagePlEn("Atrybuty obiektów", "Objects' attributes");
    public static final I18nMessage atrybutyPowiazan = new I18nMessagePlEn("Atrybuty powiązań", "Associations' attributes");
    public static final I18nMessage attributeView = new I18nMessagePlEn("Widok obiektów: z atrybutami", "With attributes");
    public static final I18nMessage edytujAtrybuty = new I18nMessagePlEn("Zmień", "Change") /* I18N:  */; // #185
    public static final I18nMessage wartoscAtrybutuNieMozeBycPusta = new I18nMessagePlEn("Wartości atrybutów nie mogą być puste. Uzupełnij wartość dla atrybutu: ", "Attributes' values can not be empty. Complete attribute value:") /* I18N:  */; // #185
    public static final I18nMessage zaznaczAtrybutGlowny = new I18nMessagePlEn("Zaznacz atrybut główny", "Select the main attribute") /* I18N:  */; // #185
    public static final I18nMessage atrybutyZostalyZaktualizowane = new I18nMessagePlEn("Atrybuty zostały zaktualizowane ", "Attributes have been updated") /* I18N:  */; // #185
    public static final I18nMessage atrybutyDoPowiazania = new I18nMessagePlEn("Atrybuty do powiązania pomiędzy ", "Attributes to associations between") /* I18N:  */; // #185
    public static final I18nMessage atrybutGlowny = new I18nMessagePlEn("Atrybut główny ", "Main attribute") /* I18N:  */; // #185
    public static final I18nMessage brakZdefiniowanychAtrybutów = new I18nMessagePlEn("Brak zdefiniowanych atrybutów powiązań ", "No defined association attributes") /* I18N:  */; // #185
    public static final I18nMessage zdefiniowanaWartość = new I18nMessagePlEn("Zdefiniowana wartość", "Defined value") /* I18N:  */; // #185
    public static final I18nMessage wartoscDlaWezlaZrodlowego = new I18nMessagePlEn("Wartość dla węzła źródłowego", "Value for source node") /* I18N:  */; // #185
    public static final I18nMessage wartoscDlaWezlaDocelowego = new I18nMessagePlEn("Wartość dla węzła docelowego", "Value for target node") /* I18N:  */; // #185
    public static final I18nMessage podajWartoscDlaWezlaZrodlowego = new I18nMessagePlEn("Podaj wartość dla węzła źródłowego", "Provide value for source node") /* I18N:  */; // #185
    public static final I18nMessage podajWartoscDlaWezlaDocelowego = new I18nMessagePlEn("Podaj wartość dla węzła docelowego", "Provide value for target node") /* I18N:  */; // #185
    public static final I18nMessage atrybutDodanyWiecejNizJedenRaz = new I18nMessagePlEn("został dodany więcej niż jeden raz. Usuń duplikaty.", "has been added more than once. Delete duplicates.") /* I18N:  */; // #185
    public static final I18nMessage zmienil = new I18nMessagePlEn("zmienił(a)", "changed") /* I18N:  */; // #185
    public static final I18nMessage utworzyl = new I18nMessagePlEn("utworzył(a)", "created") /* I18N:  */; // #185
    public static final I18nMessage skasowal = new I18nMessagePlEn("skasował(a)", "deleted") /* I18N:  */; // #185
    public static final I18nMessage staraWartosc = new I18nMessagePlEn("Stara wartość", "Old value") /* I18N:  */; // #185
    public static final I18nMessage nowaWartosc = new I18nMessagePlEn("Nowa wartość", "New Value") /* I18N:  */; // #185
    public static final I18nMessage przypisanaDo = new I18nMessagePlEn("Przypisana do", "Assigned to") /* I18N:  */; // #185
    public static final I18nMessage edycjaInstancji = new I18nMessagePlEn("Edycja instancji", "Edit instance") /* I18N:  */; // #185
    public static final I18nMessage nowaInstancja = new I18nMessagePlEn("Nowa instancja", "New instance") /* I18N:  */; // #185
    public static final I18nMessage nazwaInstancji = new I18nMessagePlEn("Nazwa instancji", "Instance name") /* I18N:  */; // #185
    public static final I18nMessage czyChceszUsunac = new I18nMessagePlEn("Czy chcesz usunąć", "Are you sure to delete") /* I18N:  */; // #185
    public static final I18nMessage trwaPostepowanie = new I18nMessagePlEn("Proces został uruchomiony. Proszę czekać", "Processing. Please wait.") /* I18N:  */; // #185
    public static final I18nMessage trwaZapisywaniaDoDysku = new I18nMessagePlEn("Zapisywanie na dysku", "Writting to disk") /* I18N:  */; // #185
    public static final I18nMessage metadane = new I18nMessagePlEn("Metadane", "Metadata") /* I18N:  */; // #185
    public static final I18nMessage osoby = new I18nMessagePlEn("Użytkownicy", "Users") /* I18N:  */; // #185
    public static final I18nMessage notatki = new I18nMessagePlEn("Notatki", "Notes") /* I18N:  */; // #185
    public static final I18nMessage miary = new I18nMessagePlEn("Miary", "Measures") /* I18N:  */; // #185
    public static final I18nMessage uzywanyWDrools = new I18nMessagePlEn("Użyj w systemie zarządzania regułami", "Use in rules management system") /* I18N:  */; // #185
    public static final I18nMessage HTMLText = new I18nMessagePlEn("Opis HTML", "Description HTML") /* I18N:  */; // #106
    public static final I18nMessage czyChceszDodacNumeracje = new I18nMessagePlEn("Dodaj numerację", "Add numbering") /* I18N:  */; // #106
    public static final I18nMessage importLinkAndJoinedObjsHelpMessage = new I18nMessagePlEn("Zostaną wyliczone automatycznie ID obiektów dotyczących drzew. Czy chcesz kontynuować?", "Automatically generate objects' ID of trees. Do you want continue?") /* I18N:  */; // #106
    public static final I18nMessage dodajGrupe = new I18nMessagePlEn("Dodaj grupę", "Add group") /* I18N:  */; // #106
    public static final I18nMessage nazwaGrupy = new I18nMessagePlEn("Nazwa grupy", "Group name") /* I18N:  */; // #106
    public static final I18nMessage wprowadzNazweGrupy = new I18nMessagePlEn("Wprowadź nazwę grupy", "Enter group name") /* I18N:  */; // #106
    public static final I18nMessage grupaDodana = new I18nMessagePlEn("Grupa została dodana", "Group has been added") /* I18N:  */; // #106
    public static final I18nMessage przypiszUzytkownika = new I18nMessagePlEn("Edytuj listę użytkowników przypisanych do grupy", "Edit list of users assigned to group") /* I18N:  */; // #106
    public static final I18nMessage uzytkownikPrzyspisany = new I18nMessagePlEn("Lista użytkowników przypisanych do grupy została zaktualizowana", "List of users assigned to group updated") /* I18N:  */; // #106
    public static final I18nMessage grupaIstnieje = new I18nMessagePlEn("Grupa o podanej nazwie już istnieje", "Group name already exists") /* I18N:  */; // #106
    public static final I18nMessage kodDrzewa = new I18nMessagePlEn("Kod drzewa", "Tree code") /* I18N:  */; // #106
    public static final I18nMessage grupaUsunieta = new I18nMessagePlEn("Grupa została usunięta", "Group deleted") /* I18N:  */; // #106
    public static final I18nMessage nazwaZostalaZmieniona = new I18nMessagePlEn("Nazwa grupy została zmieniona", "The name has been modified") /* i18n:  */; // #13
    public static final I18nMessage nalezyDoGrupy = new I18nMessagePlEn("Należy do grup", "Belongs to group") /* I18N:  */; // #404
    public static final I18nMessage automatyczieWyliczycIdObietowPrzyImporcie = new I18nMessagePlEn("Automatycznie wyliczyć ID obiektów przy imporcie", "Auto recalculate objects' ID when importing") /* I18N:  */; // #404
    public static final I18nMessage zasilaniePrzyrostowe = new I18nMessagePlEn("Zasilanie przyrostowe", "Incremental loading") /* I18N:  */; // #404
    public static final I18nMessage kopiujUprawnienia = new I18nMessagePlEn("Kopiuj uprawnienia", "Copy privileges") /* I18N:  */; // #404
    public static final I18nMessage kopiujUprawnieniaUzytkownika = new I18nMessagePlEn("Kopiuj uprawnienia użytkownika", "Copy user privileges") /* I18N:  */; // #404
    public static final I18nMessage uprawnieniaSkopiowane = new I18nMessagePlEn("Uprawnienia zostały skopiowane", "Privileges copied") /* I18N:  */; // #106
    public static final I18nMessage zarzadzanieAtrybutami = new I18nMessagePlEn("Zarządzanie atrybutami", "Management attributes") /* I18N:  */; // #39
    public static final I18nMessage loginADNieIstnieje = new I18nMessagePlEn("Login AD nie istnieje", "AD login does not exists") /* I18N:  */; // #106
    public static final I18nMessage uaktualnij = new I18nMessagePlEn("Uaktualnij", "Update") /* I18N:  */; // #106
    public static final I18nMessage masoweWypelanianieAtrybutow = new I18nMessagePlEn("Masowe wypełnianie atrybutów", "Mass filling of attributes") /* I18N:  */; // #39
    public static final I18nMessage brakAutoObjIdDlaNastepujacychDrzew = new I18nMessagePlEn("Brak własności auto_obj_id dla następujących drzew", "Lack of auto_obj_id properties for the following trees") /* I18N:  */; // #106
    public static final I18nMessage dodajAtrybutyDoPowiazan = new I18nMessagePlEn("Dodaj atrybuty do powiązań", "Add attributes to associations") /* I18N:  */; // #106
    public static final I18nMessage dodanoAtrybutDoPowiazan = new I18nMessagePlEn("Dodano atrybut do powiązań", "Attribute added") /* I18N:  */; // #144
    public static final I18nMessage usunAtrybut = new I18nMessagePlEn("Usuń atrybut", "Delete attributes") /* I18N:  */; // #144
    public static final I18nMessage powiazania = new I18nMessagePlEn("Powiązania", "Associations") /* I18N:  */; // #144
    public static final I18nMessage wymagany = new I18nMessagePlEn("wymagany", "Required") /* I18N:  */; // #144
    public static final I18nMessage niewymagany = new I18nMessagePlEn("nie wymagany", "Optional") /* I18N:  */; // #144
    public static final I18nMessage zostalzmienony = new I18nMessagePlEn("został zmieniony na", "changed to") /* I18N:  */; // #144
    public static final I18nMessage widoczny = new I18nMessagePlEn("widoczny", "visible") /* I18N:  */; // #144
    public static final I18nMessage niewidoczny = new I18nMessagePlEn("niewidoczny", "hidden") /* I18N:  */; // #144
    public static final I18nMessage wypełnijAtryubuty = new I18nMessagePlEn("Wartości dla następujacych atrybutów nie mogą być puste lub podane wartości nie są prawidłowe: ", "Values for the following attributes can not be empty or the given values are invalid:") /* I18N:  */; // #144
    public static final I18nMessage sortowanieKolumn = new I18nMessagePlEn("(W celu posortowania obiektów w kolumnach, kliknij na odpowiedni nagłówek: Atrybut, Węzeł, Czy folder, Wymagany, Widoczny)");
    public static final I18nMessage nodeIdNiePuste = new I18nMessagePlEn("Pole Node Id nie może być puste", "Node Id can not be empty") /* I18N:  */; // #144
    public static final I18nMessage nieMoznaWczytacRaportu = new I18nMessagePlEn("Nie można wczytać raportu", "Can not compile report") /* I18N:  */; // #144
    public static final I18nMessage podajZakresDat = new I18nMessagePlEn("Podaj zakres dat", "Date range");
    public static final I18nMessage nieDotyczy = new I18nMessagePlEn("Nie dotyczy", "Not related");
    public static final I18nMessage obecneDrzewoKonfiguracji = new I18nMessagePlEn("Obecne drzewo konfiguracji", "Current configuration tree");
    public static final I18nMessage uwstaicDrzewoJakoDrzewoKonfiguracji = new I18nMessagePlEn("Ustawić drzewo {0} jako drzewo konfiguracji", "Set {0} as configuration tree");
    public static final I18nMessage zbudujDrzewoKonfiguracjiZObecnejStrukturySystemu = new I18nMessagePlEn("Zbuduj drzewo konfiguracji z obecnej struktury systemu", "Build configuration tree from current system");
    public static final I18nMessage zbudujDrzewoKonfiguracjiZObecnejStrukturySystemuBezKasowania = new I18nMessagePlEn("Zbuduj drzewo konfiguracji z obecnej struktury systemu bez kasowania istniejącego", "Build configuration tree from current system without deleting the old tree");
    public static final I18nMessage wdrozycDrzewoKonfiguracji = new I18nMessagePlEn("Implementuj drzewo konfiguracji", "Implement config tree");
    public static final I18nMessage implementuj = new I18nMessagePlEn("Implementuj", "Implement");
    public static final I18nMessage zbuduj = new I18nMessagePlEn("Zbuduj", "Build");
    public static final I18nMessage aktualizuj = new I18nMessagePlEn("Aktualizuj", "Update");
    public static final I18nMessage dodano = new I18nMessagePlEn("Aktualizuj", "Update");
    public static final I18nMessage podgladKategoriiAtrybutow = new I18nMessagePlEn("Podgląd kategorii atrybutów", "Attribute's catalog overview");
    public static final I18nMessage podgladDefinicjiAtrybutow = new I18nMessagePlEn("Podgląd definicji atrybutów", "Attribute's definition overview");
    public static final I18nMessage podgladAtrybutow = new I18nMessagePlEn("Podgląd atrybutów", "Attribute overview");
    public static final I18nMessage podgladNodeKind = new I18nMessagePlEn("Podgląd typów węzłów", "Node kind overview");
    public static final I18nMessage podgladTreeKind = new I18nMessagePlEn("Podgląd typów drzew", "Tree kind overview");
    public static final I18nMessage proszeOdswiezycPrzegladarke = new I18nMessagePlEn("Proszę odświeżyć przeglądarkę", "Please refresh browser");
    public static final I18nMessage brakObiektowDoDowiazania = new I18nMessagePlEn("Brak obiektów do dowiązania", "No objects to link");
    public static final I18nMessage brakObiektowDoPowiazania = new I18nMessagePlEn("Brak obiektów do powiązania", "No objects to associate");
    public static final I18nMessage brakTypowWezlow = new I18nMessagePlEn("Brak zdefiniowanych typów węzłów do dodania", "No defined node types to add");
    public static final I18nMessage kategorieAtrybutow = new I18nMessagePlEn("Kategorię atrybutów", "Attributes' catalog");
    public static final I18nMessage definicjeAtrybutu = new I18nMessagePlEn("Definicję atrybutu", "Attribute's definition");
    public static final I18nMessage definicjeTypuObiektu = new I18nMessagePlEn("Definicję typu obiektu", "Node's kind");
    public static final I18nMessage definicjeTypuDrzewa = new I18nMessagePlEn("Definicję typu drzewa", "Tree's kind");
    public static final I18nMessage typObiektuWDrzewie = new I18nMessagePlEn("Typ obiektu w drzewie", "Node's kind in tree's kind");
    public static final I18nMessage zwiazanyTypObiektu = new I18nMessagePlEn("Związany typ obiektu", "Related node's kind");
    public static final I18nMessage brakZdefiniowanychTypowObiektow = new I18nMessagePlEn("Brak zdefiniowanych typów obiektów", "There are no defined object types");
    public static final I18nMessage zaznaczTypWezla = new I18nMessagePlEn("Zaznacz typ węzła", "Select node type");
    public static final I18nMessage uzupelniNazwe = new I18nMessagePlEn("Uzupełnij nazwę", "Name cannot be empty") /* I18N:  */; // #503

    public static final I18nMessage brakObiektowDoUtowrzeniaHyperlinku = new I18nMessagePlEn("Brak obiektu do utworzeniu linku wewnętrznego", "No object to create an internal link");
    public static final I18nMessage linkWewnetrznyPojedynczy = new I18nMessagePlEn("Link wewnętrzny jednokrotnego wyboru", "Internal mono link") /* I18N:  */; // #254
    public static final I18nMessage dodajeszDowiazanieDla = new I18nMessagePlEn("Dodajesz dowiązanie dla ", "Adding link to ");
    public static final I18nMessage edytujeszDowiazanieDla = new I18nMessagePlEn("Edytujesz dowiązanie dla ", "Editing link to ");
    public static final I18nMessage dodajPustyAtrybut = new I18nMessagePlEn("Dodaj pusty atrybut", "Add attribute");
    public static final I18nMessage edytujNaPustyAtrybut = new I18nMessagePlEn("Zmień na pusty", "Change to empty");
    public static final I18nMessage nadajUprawnienia = new I18nMessagePlEn("Nadaj uprawnienia", "Give permissions");
    public static final I18nMessage userNames = new I18nMessagePlEn("Osoby", "Users");
    public static final I18nMessage uzytkownikMaUprawnienia = new I18nMessagePlEn(" już ma uprawnienia do tego węzła", " already has permissions to this node");
    public static final I18nMessage uprawnieniaNadane = new I18nMessagePlEn("Nadano uprawnienia", "Permissions granted");
    public static final I18nMessage usersBtn = new I18nMessagePlEn("Uprawnienia", "Permissions") /* I18N:  */;
    public static final I18nMessage assignedUsersAreShown = new I18nMessagePlEn("Użytkownicy z uprawnieniami", "Assigned users") /* I18N:  */;
    public static final I18nMessage obiektUsuniety = new I18nMessagePlEn("Obiekt {0} został usunięty", "Object {0} deleted") /* I18N:  */;
    public static final I18nMessage brakUzytkownikow = new I18nMessagePlEn("Brak użytkowników o takich nazwach", "Users with such names are not found") /* I18N:  */;
    public static final I18nMessage brakGrupUzytkownikow = new I18nMessagePlEn("Nie znaleziono grup użytkowników o tych nazwach", "User groups with such names are not found") /* I18N:  */;
    public static final I18nMessage wrongFileExtension = new I18nMessagePlEn("Plik z takim rozszerzeniem nie może być załadowany", "File with this extension can not be loaded");
    public static final I18nMessage linkWewnetrznyZdodatkowaWartoscia = new I18nMessagePlEn("Link wewnętrzny wielokrotnego wyboru z dodatkową wartoscią", "Internal multi link with additional mean") /* I18N:  */;

    public static final I18nMessage zastosujMenu = new I18nMessagePlEn("Zastosuj konfigurację", "Apply configuration");
    public static final I18nMessage dodajZakladke = new I18nMessagePlEn("Dodaj zakładkę", "Add submenu");

    public static final I18nMessage nieUdaloSieZapisac = new I18nMessagePlEn("Nie udało się zapisać wartość atrybutu: ", "The attribute value could not be saved: ");
    public static final I18nMessage wypelniejPoleAtrybutu = new I18nMessagePlEn("Wypełnij pole atrybutu", "Fill the attribute field");
    public static final I18nMessage doNotFilterHtml = new I18nMessagePlEn("Nie filtruj html", "Do not filter html");
    public static final I18nMessage fillNameField = new I18nMessagePlEn("Proszę uzupełnienić pole z nazwą", "Fill name field");
    public static final I18nMessage zaznaczRole = new I18nMessagePlEn("Zaznacz rolę", "Select the role");
    public static final I18nMessage zaznaczUzytkownikow = new I18nMessagePlEn("Zaznacz odpowiednich użytkowników", "Select the appropriate users");
    public static final I18nMessage wyczyscZaznaczenia = new I18nMessagePlEn("Wyczyść", "Clear") /* I18N:  */; // #562
    public static final I18nMessage usunietoWartoscAtrybutu = new I18nMessagePlEn("Usunięto wartość atrybutu", "The value of the attribute has been removed") /* I18N:  */; // #562
    public static final I18nMessage nazwaZapytania = new I18nMessagePlEn("Nazwa zapytania", "SQL name");
    public static final I18nMessage wybierzZapytanie = new I18nMessagePlEn("Wybierz zapytanie", "Select query");
    public static final I18nMessage zalaczniki = new I18nMessagePlEn("Zalączniki", "Attachment");
    public static final I18nMessage obiekty = new I18nMessagePlEn("Obiekty", "Objects");
    public static final I18nMessage drzewa = new I18nMessagePlEn("Drzewa", "Trees") /* I18N:  */; // #169
    public static final I18nMessage nazwaObiektuSearch = new I18nMessagePlEn("Nazwa obiektu", "Name of the object");
    public static final I18nMessage opisObiektuSearch = new I18nMessagePlEn("Opis obiektu", "Description of the object");
    public static final I18nMessage zaznaczWszystko = new I18nMessagePlEn("Zaznacz wszystko", "Select All");
    public static final I18nMessage odznaczWszystko = new I18nMessagePlEn("Odznacz wszystko", "Unselect All");
    public static final I18nMessage cantEditAttr = new I18nMessagePlEn("Brak wartości do wyboru z listy lub nie masz do nich uprawnień.", "User has no rights to edit this attribute") /* I18N:  */;
    public static final I18nMessage pobierzRaport = new I18nMessagePlEn("Pobierz raport", "Get report") /* I18N:  */;
    public static final I18nMessage dodajRole = new I18nMessagePlEn("Dodaj rolę", "Add role") /* I18N:  */; // #106
    public static final I18nMessage rolaDodana = new I18nMessagePlEn("Rola została dodana", "Role has been added") /* I18N:  */; // #106
    public static final I18nMessage rolaIstnieje = new I18nMessagePlEn("Rola o podanej nazwie już istnieje", "Role with specified name already exists") /* I18N:  */; // #106
    public static final I18nMessage profilUprawnien = new I18nMessagePlEn("Profil uprawnień", "Privileges profile") /* I18N:  */; // #467
    public static final I18nMessage nazwaRoli = new I18nMessagePlEn("Nazwa roli", "Role name") /* I18N:  */; // #467
    public static final I18nMessage wprowadzNazweRoliOrazOpis = new I18nMessagePlEn("Pola z nazwą roli oraz opisem mają być wypełnione", "Fill out the fields with the name of the role and description") /* I18N:  */; // #106
    public static final I18nMessage podgladUprawnien = new I18nMessagePlEn("Podgląd uprawnień dla roli ", "Rights preview for role");
    public static final I18nMessage nadajUprawnieniaDlaRoli = new I18nMessagePlEn("Nadaj/usuń uprawnienia dla roli ", "Grant rights to role ");
    public static final I18nMessage rolaUsunieta = new I18nMessagePlEn("Rola została usunięta", "Role deleted") /* I18N:  */; // #106
    public static final I18nMessage dodajLubEdytAtrybut = new I18nMessagePlEn("Dodaj/Edytuj atrybut", "Add/Edit attribute") /* I18N:  */; //
    public static final I18nMessage aktywne = new I18nMessagePlEn("Aktywne", "Active") /* I18N:  */; // #454
    public static final I18nMessage trwazapisywanie = new I18nMessagePlEn("Trwa zapisywanie. Proszę czekać", "Processing. Please wait.") /* I18N:  */; // #185
    public static final I18nMessage dodajAtrybutDoSzablonyWydruku = new I18nMessagePlEn("Dodaj atrybuty do szablonu wydruku", "");
    public static final I18nMessage dodajAtrybutZagniezdzony = new I18nMessagePlEn("Dodaj atrybut zagnieżdżony", "");
    public static final I18nMessage dodajTypObiektuZagniezdzony = new I18nMessagePlEn("Dodaj typ obiektu zagnieżdżony", "");
    public static final I18nMessage dodajSzablonWydruku = new I18nMessagePlEn("Dodaj szablon wydruku", "");
    public static final I18nMessage dodajGrupeSzablonow = new I18nMessagePlEn("Dodaj grupę szablonów", "");
    public static final I18nMessage szablonWydruku = new I18nMessagePlEn("Szablon wydruku", "");
    public static final I18nMessage grupaSzablonow = new I18nMessagePlEn("Grupa szablonów", "");
    public static final I18nMessage drukuj = new I18nMessagePlEn("Drukuj", "");
    public static final I18nMessage wydruk = new I18nMessagePlEn("Wydruk", "");
    public static final I18nMessage dodajeszAtrybutDoSzablonu = new I18nMessagePlEn("Dodajesz atrybut do szablonu", "");
    public static final I18nMessage dodajeszTypObiektu = new I18nMessagePlEn("Dodajesz typ obiektu", "");
    public static final I18nMessage dodajeszAtrybut = new I18nMessagePlEn("Dodajesz atrybut", "");
    public static final I18nMessage formatWydruku = new I18nMessagePlEn("Format wydruku", "");
    public static final I18nMessage printStrona = new I18nMessagePlEn("Strona", "Page") /* I18N:  */; // #32
    public static final I18nMessage printZ = new I18nMessagePlEn("z", "of") /* I18N:  */; // #32
    public static final I18nMessage dataIgodzina = new I18nMessagePlEn("Data i godzina", "") /* I18N:  */; // #32
    public static final I18nMessage resetuSortowanie = new I18nMessagePlEn("Zastosuj kolejność z definicji obiektu", "") /* I18N:  */; // #427
    public static final I18nMessage ustawStatus = new I18nMessagePlEn("Ustaw status", "") /* I18N:  */; // #427
    public static final I18nMessage statusUstawiony = new I18nMessagePlEn("Status został ustawiony", "") /* I18N:  */; // #427
    public static final I18nMessage dataStatusu = new I18nMessagePlEn("Data statusu", "") /* I18N:  */; // #427

;

//   public static final I18nMessage fILTRUJ = new I18nMessagePlEn("FILTRUJ drzewo", "FILTER tree") /* I18N:  */; // #198
/*ó
     i18n categories counts:
     null: 772
     .: 4
     <--proper-->: 815
     <--proper:no?-->: 3
     <--proper:regular-->: 812
     <-all->: 1384
     [translated]: 630
     [untranslated]: 23
     [unused]: 162
     added to/removed from: 1
     blog: 1
     czy przypisany: 1ó
     description: 1
     dwukropek po angielsku, tak ma być!: 1
     favs: 2
     no: 521
     no!ident: 16
     no!not-msg: 1
     no:column-name: 2
     no:css-class: 6
     no:err-fail-in: 19
     no:px-dims: 1
     no:sql: 2
     no:wtf?: 1
     no?maybe-ident: 2
     no?single-letter: 1
     ok: 3
     ok?: 1
     pagination: 1
     pozycje od-do: 1
     reset vs restart?: 3
     wtf?: 17
     wtf?wtf!wtf?: 1
     x users out of y users: 1
     yes: 1
 */
}
