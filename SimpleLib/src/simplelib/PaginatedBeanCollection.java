/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author wezyr
 */
public class PaginatedBeanCollection<T> {
    public int count;
    public Collection<T> beans;
    public List<Integer> paginationPageNums;
    public int currPageNum;
    public int offset;
    public int pageSize;
    
    public PaginatedBeanCollection() {
        // no-op
    }

    public PaginatedBeanCollection(Collection<T> beans, int currPageNum, int pageSize, int count) {
        init(beans, currPageNum, pageSize, count);
    }

    public void init(Collection<T> beans, int currPageNum, int pageSize, int count) {
        this.beans = beans;
        this.count = count;
        this.pageSize = pageSize;
        this.currPageNum = BaseUtils.fixPageNumByCountAndPageSize(currPageNum, pageSize, count);
        this.offset = this.currPageNum * pageSize;
        this.paginationPageNums = BaseUtils.preparePaginationPageNums(this.currPageNum, pageSize, count);
    }

    public static <T> PaginatedBeanCollection<T> imitatePagination(List<T> allBeans, int currPageNum, int pageSize) {
        //System.out.println("imitatePagination: ***");
        int cnt = allBeans.size();
        currPageNum = BaseUtils.fixPageNumByCountAndPageSize(currPageNum, pageSize, cnt);
        int toIndex = currPageNum * pageSize + pageSize;
        if (toIndex > cnt) {
            toIndex = cnt;
        }
        List<T> beans = new ArrayList<T>(); //allBeans.subList(currPageNum * pageSize, toIndex);
        for (int i = currPageNum * pageSize; i < toIndex; i++) {
            beans.add(allBeans.get(i));
        }
        return new PaginatedBeanCollection<T>(beans, currPageNum, pageSize, cnt);
    }
}
