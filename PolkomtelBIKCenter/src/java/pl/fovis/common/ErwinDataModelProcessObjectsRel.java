/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author pgajda
 */
public class ErwinDataModelProcessObjectsRel implements Serializable {

    public String symbProc;
    public String idTypProcess;
    public String area;
    public String dbName;
    public String procStatus;
    public Date procDate;
    public int nodeId;
    //
    public String objName;
}
