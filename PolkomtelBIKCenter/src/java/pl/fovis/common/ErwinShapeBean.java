/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author pgajda
 */
public class ErwinShapeBean implements Serializable {

    public String shapeId;
    public String objectLongId;
    public Integer anchorPointX;
    public Integer anchorPointY;
    public Integer fixedSizeX;
    public Integer fixedSizeY;
}
