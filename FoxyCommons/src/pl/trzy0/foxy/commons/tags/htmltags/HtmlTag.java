/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.tags.htmltags;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.trzy0.foxy.commons.tags.TagData;
import simplelib.BaseUtils;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class HtmlTag implements IHtmlTag {

    private static ILameLogger logger = LameLoggerFactory.getLogger(HtmlTag.class);
    private TagData tagData;
    private Collection<IHtmlTag> subTags;
    private boolean isVerbatimWhiteSpace;
    private boolean isInRootIdClass;
    private IHtmlTag parent;

    public HtmlTag(TagData tagData, boolean isVerbatimWhiteSpace,
            boolean isInRootIdClass, IHtmlTag parent) {
        this.tagData = tagData;
        this.isVerbatimWhiteSpace = isVerbatimWhiteSpace;
        this.isInRootIdClass = isInRootIdClass;
        this.parent = parent;
    }

    public boolean isInRootIdClass() {
        return isInRootIdClass;
    }

    public void setSubTags(Collection<IHtmlTag> subTags) {
        this.subTags = subTags;
    }

    public boolean isVerbatimWhiteSpace() {
        return isVerbatimWhiteSpace;
    }
    private Map<String, IHtmlTag> tagsById;

    public static List<String> putSubTagsIdsInMap(Map<String, IHtmlTag> map, Collection<IHtmlTag> subTags) {
        List<String> res = new ArrayList<String>();
        if (subTags != null) {
            for (IHtmlTag subTag : subTags) {
                Map<String, IHtmlTag> subIds = subTag.getTagsById();
                res.addAll(subIds.keySet());
                map.putAll(subIds);
            }
        }
        return res;
    }

    private void putIdsInMap() {
        String id = getId();
        if (id != null) {
            tagsById.put(id, this);
        }
        putSubTagsIdsInMap(tagsById, subTags);
    }

    private void initTagsById() {
        tagsById = new HashMap<String, IHtmlTag>();
        putIdsInMap();
    }

    public IHtmlTag getById(String id) {
        IHtmlTag ht = getTagsById().get(id);

        if (ht == null) {
            logger.errorAndThrowNew("no tag with id=" + id);
        }

        return ht;
    }

    public String getId() {
        String id = tagData.getAttrVal("foxy:id");
        if (id == null) {
            id = tagData.getAttrVal("id");
        }
        return id;
    }

    public Collection<IHtmlTag> getSubTags() {
        return subTags;
    }

    public Map<String, IHtmlTag> getTagsById() {
        if (tagsById == null) {
            initTagsById();
        }
        return tagsById;
    }

    public boolean isTextTag() {
        return tagData.isTextTag();
    }

    public String getName() {
        return tagData.getName();
    }

    public Map<String, String> getAttrs() {
        return tagData.getAttrs();
    }

    public boolean isClosedInPlace() {
        return tagData.getLevelDelta() == 0;
    }

    public String getFileName() {
        return tagData.sourceFileName;
    }
    private Map<String, String> attributes;

    public void setAttribute(String name, String val) {
        if (attributes == null) {
            attributes = new HashMap<String, String>();
        }
        if (val != null) {
            attributes.put(name, val);
        } else {
            attributes.remove(name);
        }
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public String getAttribute(String name) {
        String val = BaseUtils.safeMapGet(attributes, name);
        if (val == null) {
            val = BaseUtils.safeMapGet(tagData.getAttrs(), name);
        }
        return val;
    }

    public String getTagName() {
        return getName();
    }

    public Map<String, String> getTagAttrs() {
        Map<String, String> attrsOverride = getAttributes();
        Map<String, String> attrs = getAttrs();

        if (!BaseUtils.isMapEmpty(attrsOverride) && !BaseUtils.isMapEmpty(attrs)) {
            Map<String, String> attrs2 = new HashMap<String, String>(attrs);
            attrs2.putAll(attrsOverride);
            attrs = attrs2;
        } else if (!BaseUtils.isMapEmpty(attrsOverride)) {
            attrs = attrsOverride;
        }

        return attrs;
    }

    @SuppressWarnings("unchecked")
    public Collection<IRenderableTag> getRenderableSubTags() {
        Map<String, String> attrsOverride = getAttributes();
        String innerHtml = BaseUtils.safeMapGet(attrsOverride, null);
        if (innerHtml != null) {
            return Collections.singletonList(
                    (IRenderableTag) new RenderableTagForInnerHtml(innerHtml));
        }
        return (Collection) subTags;
    }

    public IHtmlTag getParent() {
        return parent;
    }
}
