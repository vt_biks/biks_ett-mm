/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author ctran
 */
public class AttrSearchableBean implements IsSerializable {

    public int id;
    public String caption;
    public String typeAttr;
    public int displayAsNumber;
    public String valueOpt;
}
