﻿exec sp_check_version '1.1.5.4';
go
------------------------------------
------------------------------------

alter table bik_attr_def add is_built_in int not null default 0
go

alter table bik_attr_def add hint varchar(max)
go

alter table bik_attr_category add is_built_in int not null default 0
go

insert into bik_attr_category(name, is_deleted, is_built_in)
values('SAP BO', 0, 1)
go

insert into bik_attr_category(name, is_deleted, is_built_in)
values('DQC', 0, 1)
go

insert into bik_attr_def(name, attr_category_id, is_deleted, is_built_in)
select ah.name,(select id from bik_attr_category where name = 'SAP BO'), 0, 1 from bik_attr_hint ah left join bik_attr_def ad on ad.name=ah.name
where ad.id is null and ah.name not in ('Podpowiedź do wyszukiwarki', 'Legenda edycji powiązań')

update bik_attr_def
set hint = ah.hint
from bik_attr_def ad join bik_attr_hint ah on ah.name=ad.name

update bik_attr_def
set attr_category_id=(select id from bik_attr_category where name = 'DQC')
where name in ('Long name','Benchmark definition', 'Error Threshold', 'Sampling Method', 'Additional Information', 'Create Date', 'Modify Date', 'Suspend Date')

/*update bik_attr_def
set is_deleted = 1
where name = 'Password' and is_built_in = 1*/

delete from bik_attr_hint
where id in (select ah.id from bik_attr_hint ah left join bik_attr_def ad on ad.name=ah.name where ad.id is not null)

alter table bik_attr_def drop constraint UQ_bik_attr_def_name;
go
alter table bik_attr_def add constraint UQ_bik_attr_def_name unique (name,is_built_in);
go

alter table bik_attr_category drop constraint UQ_bik_attr_category_name;
go
alter table bik_attr_category add constraint UQ_bik_attr_category_name unique (name,is_built_in);
go

alter table bik_attribute drop column name;
go

create table bik_attr_system(
id int identity not null primary key,
node_kind_id int not null references bik_node_kind (id),
attr_id int not null references bik_attr_def (id),
is_visible int not null default 1
);
go
	
alter table bik_attr_system add constraint uk_bik_attr_system unique (node_kind_id,attr_id);
go



insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Słowa kluczowe z repozytorium BO', (select id from bik_attr_category where name='SAP BO'), 'Ang. Keyword', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Opublikowano w repozytorium BO', (select id from bik_attr_category where name='SAP BO'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Właściciel w repozytorium BO', (select id from bik_attr_category where name='SAP BO'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Autor [Created by]', (select id from bik_attr_category where name='SAP BO'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Ostatnia modyfikacja w repozytorium BO', (select id from bik_attr_category where name='SAP BO'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Utworzony', (select id from bik_attr_category where name='SAP BO'), '', 1, 0)



--------------------------------------------------------------
--------------------------------------------------------------
-- node kindy
declare @DataConnection int;
select @DataConnection=id from bik_node_kind where code='DataConnection'
declare @Webi int;
select @Webi=id from bik_node_kind where code='Webi'
declare @Flash int;
select @Flash=id from bik_node_kind where code='Flash'
declare @CrystalReport int;
select @CrystalReport=id from bik_node_kind where code='CrystalReport'
declare @Universe int;
select @Universe=id from bik_node_kind where code='Universe'
declare @Excel int;
select @Excel=id from bik_node_kind where code='Excel'
declare @FullClient int;
select @FullClient=id from bik_node_kind where code='FullClient'
declare @Pdf int;
select @Pdf=id from bik_node_kind where code='Pdf'
declare @Hyperlink int;
select @Hyperlink=id from bik_node_kind where code='Hyperlink'
declare @Rtf int;
select @Rtf=id from bik_node_kind where code='Rtf'
declare @Txt int;
select @Txt=id from bik_node_kind where code='Txt'
declare @Powerpoint int;
select @Powerpoint=id from bik_node_kind where code='Powerpoint'
declare @Word int;
select @Word=id from bik_node_kind where code='Word'
declare @ReportQuery int;
select @ReportQuery=id from bik_node_kind where code='ReportQuery'
declare @Measure int;
select @Measure=id from bik_node_kind where code='Measure'
declare @Dimension int;
select @Dimension=id from bik_node_kind where code='Dimension'
declare @Detail int;
select @Detail=id from bik_node_kind where code='Detail'
declare @Filter int;
select @Filter=id from bik_node_kind where code='Filter'
declare @ReportFolder int;
select @ReportFolder=id from bik_node_kind where code='ReportFolder'
declare @UniversesFolder int;
select @UniversesFolder=id from bik_node_kind where code='UniversesFolder'
declare @ConnectionFolder int;
select @ConnectionFolder=id from bik_node_kind where code='ConnectionFolder'
declare @DQCTestSuccess int;
select @DQCTestSuccess=id from bik_node_kind where code='DQCTestSuccess'
declare @DQCTestFailed int;
select @DQCTestFailed=id from bik_node_kind where code='DQCTestFailed'
declare @DQCTestInactive int;
select @DQCTestInactive=id from bik_node_kind where code='DQCTestInactive'

--atrybuty
declare @Agregate int;
select @Agregate=id from bik_attr_def where name='Agregate'
declare @ApplicationObject int;
select @ApplicationObject=id from bik_attr_def where name='Application Object'
declare @Clientnumber int;
select @Clientnumber=id from bik_attr_def where name='Client number'
declare @ContentLocale int;
select @ContentLocale=id from bik_attr_def where name='ContentLocale'
declare @Dataostatniegouruchomienia int;
select @Dataostatniegouruchomienia=id from bik_attr_def where name='Data ostatniego uruchomienia'
declare @Databaseengine int;
select @Databaseengine=id from bik_attr_def where name='Database engine'
declare @DocSender int;
select @DocSender=id from bik_attr_def where name='Doc Sender'
declare @Endtime int;
select @Endtime=id from bik_attr_def where name='Endtime'
declare @ExpectedResult int;
select @ExpectedResult=id from bik_attr_def where name='Expected Result'
declare @Filtr int;
select @Filtr=id from bik_attr_def where name='Filtr'
declare @Flags int;
select @Flags=id from bik_attr_def where name='Flags'
declare @Guid int;
select @Guid=id from bik_attr_def where name='Guid'
declare @HasChildren int;
select @HasChildren=id from bik_attr_def where name='HasChildren'
declare @Id int;
select @Id=id from bik_attr_def where name='Id'
declare @IDCUID int;
select @IDCUID=id from bik_attr_def where name='ID, CUID'
declare @Instance int;
select @Instance=id from bik_attr_def where name='Instance'
declare @IsSchedulable int;
select @IsSchedulable=id from bik_attr_def where name='IsSchedulable'
declare @Language int;
select @Language=id from bik_attr_def where name='Language'
declare @LastSuccessfulInstanceId int;
select @LastSuccessfulInstanceId=id from bik_attr_def where name='LastSuccessfulInstanceId'
declare @Liczbachildren int;
select @Liczbachildren=id from bik_attr_def where name='Liczba obiektów podrzędnych (children)'
declare @Nazwapliku int;
select @Nazwapliku=id from bik_attr_def where name='Nazwa pliku'
declare @Nazwaskrocona int;
select @Nazwaskrocona=id from bik_attr_def where name='Nazwa skrócona'
declare @Networklayer int;
select @Networklayer=id from bik_attr_def where name='Network layer'
declare @Nextruntime int;
select @Nextruntime=id from bik_attr_def where name='Nextruntime'
declare @Obtype int;
select @Obtype=id from bik_attr_def where name='Obtype'
declare @Ostatniamodyfikacja int;
select @Ostatniamodyfikacja=id from bik_attr_def where name='Ostatnia modyfikacja'
declare @Owner_id int;
select @Owner_id=id from bik_attr_def where name='Owner_id'
declare @Password int;
select @Password=id from bik_attr_def where name='Password'
declare @Poprawka int;
select @Poprawka=id from bik_attr_def where name='Poprawka'
declare @Progid int;
select @Progid=id from bik_attr_def where name='Progid'
declare @ProgidMachine int;
select @ProgidMachine=id from bik_attr_def where name='ProgidMachine'
declare @ReadOnly int;
select @ReadOnly=id from bik_attr_def where name='ReadOnly'
declare @Recurring int;
select @Recurring=id from bik_attr_def where name='Recurring'
declare @Rozmiar int;
select @Rozmiar=id from bik_attr_def where name='Rozmiar w bajtach'
declare @Ruid int;
select @Ruid=id from bik_attr_def where name='Ruid'
declare @RunnableObject int;
select @RunnableObject=id from bik_attr_def where name='RunnableObject'
declare @ScheduleStatus int;
select @ScheduleStatus=id from bik_attr_def where name='ScheduleStatus'
declare @Select int;
select @Select=id from bik_attr_def where name='Select'
declare @Server int;
select @Server=id from bik_attr_def where name='Server'
declare @Size int;
select @Size=id from bik_attr_def where name='Size'
declare @Source int;
select @Source=id from bik_attr_def where name='Source'
declare @Sposob int;
select @Sposob=id from bik_attr_def where name='Sposób odświeżania raportu'
declare @Starttime int;
select @Starttime=id from bik_attr_def where name='Starttime'
declare @Statystyki int;
select @Statystyki=id from bik_attr_def where name='Statystyki'
declare @Systemid int;
select @Systemid=id from bik_attr_def where name='System id'
declare @Systemnumber int;
select @Systemnumber=id from bik_attr_def where name='System number'
declare @Tabele int;
select @Tabele=id from bik_attr_def where name='Tabele'
declare @Timestamp int;
select @Timestamp=id from bik_attr_def where name='Timestamp'
declare @Typ int;
select @Typ=id from bik_attr_def where name='Typ'
declare @Type int;
select @Type=id from bik_attr_def where name='Type'
declare @User int;
select @User=id from bik_attr_def where name='User'
declare @Utworzono int;
select @Utworzono=id from bik_attr_def where name='Utworzono'
declare @Where int;
select @Where=id from bik_attr_def where name='Where'
declare @Zmodyfikowany int;
select @Zmodyfikowany=id from bik_attr_def where name='Zmodyfikowany'
declare @SuspendDate int;
select @SuspendDate=id from bik_attr_def where name='Suspend Date'
declare @AdditionalInformation int;
select @AdditionalInformation=id from bik_attr_def where name='Additional Information'
declare @SamplingMethod int;
select @SamplingMethod=id from bik_attr_def where name='Sampling Method'
declare @Longname int;
select @Longname=id from bik_attr_def where name='Long name'
declare @ModifyDate int;
select @ModifyDate=id from bik_attr_def where name='Modify Date'
declare @ErrorThreshold int;
select @ErrorThreshold=id from bik_attr_def where name='Error Threshold'
declare @CreateDate int;
select @CreateDate=id from bik_attr_def where name='Create Date'
declare @Benchmarkdefinition int;
select @Benchmarkdefinition=id from bik_attr_def where name='Benchmark definition'
declare @Keyword int;
select @Keyword=id from bik_attr_def where name='Słowa kluczowe z repozytorium BO'
declare @Opublikowano int;
select @Opublikowano=id from bik_attr_def where name='Opublikowano w repozytorium BO'
declare @Wlasciciel int;
select @Wlasciciel=id from bik_attr_def where name='Właściciel w repozytorium BO'
declare @Autor int;
select @Autor=id from bik_attr_def where name='Autor [Created by]'
declare @OstatniaModyfikacjawBO int;
select @OstatniaModyfikacjawBO=id from bik_attr_def where name='Ostatnia modyfikacja w repozytorium BO'
declare @ParametryWprowadzane int;
select @ParametryWprowadzane=id from bik_attr_def where name='Parametry wprowadzane przez użytkownika w momencie odświeżania raportu'
declare @Utworzony int;
select @Utworzony=id from bik_attr_def where name='Utworzony'



-- dopasowanie

-- conn
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @IDCUID, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @Utworzono, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @Ostatniamodyfikacja, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @Server, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @User, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @Password, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @Source, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @Networklayer, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @Type, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @Clientnumber, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @Databaseengine, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @Language, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @Systemnumber, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DataConnection, @Systemid, 1)

--webi
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @IDCUID, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @Guid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @Ruid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @Progid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @Nazwapliku, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @Dataostatniegouruchomienia, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @Rozmiar, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @Keyword, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @ContentLocale, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @Sposob, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @Opublikowano, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @Wlasciciel, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @Autor, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @OstatniaModyfikacjawBO, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Webi, @ParametryWprowadzane, 1)

-- foldery raportow, universow i polaczen
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ReportFolder, @IDCUID, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ReportFolder, @Guid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ReportFolder, @Ruid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ReportFolder, @Progid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ReportFolder, @Liczbachildren, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ReportFolder, @Utworzony, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ReportFolder, @Zmodyfikowany, 1)
--
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@UniversesFolder, @IDCUID, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@UniversesFolder, @Guid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@UniversesFolder, @Ruid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@UniversesFolder, @Progid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@UniversesFolder, @Liczbachildren, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@UniversesFolder, @Utworzony, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@UniversesFolder, @Zmodyfikowany, 1)
--
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ConnectionFolder, @IDCUID, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ConnectionFolder, @Guid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ConnectionFolder, @Ruid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ConnectionFolder, @Progid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ConnectionFolder, @Liczbachildren, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ConnectionFolder, @Utworzony, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ConnectionFolder, @Zmodyfikowany, 1)

--Universum
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Universe, @IDCUID, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Universe, @Guid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Universe, @Ruid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Universe, @Progid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Universe, @Nazwapliku, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Universe, @Rozmiar, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Universe, @Statystyki, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Universe, @Opublikowano, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Universe, @Wlasciciel, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Universe, @OstatniaModyfikacjawBO, 1)

--Obiekty
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Dimension, @Typ, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Dimension, @Select, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Dimension, @Where, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Detail, @Typ, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Detail, @Select, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Detail, @Where, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Measure, @Typ, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Measure, @Select, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Measure, @Where, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Measure, @Agregate, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Filter, @Typ, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Filter, @Where, 1)

-- Zapytania raportowe
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ReportQuery, @Id, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@ReportQuery, @Filtr, 1)

--inne raporty
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Hyperlink, @IDCUID, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Hyperlink, @Guid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Hyperlink, @Ruid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Hyperlink, @Progid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Hyperlink, @Nazwapliku, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Hyperlink, @Keyword, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Hyperlink, @Opublikowano, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Hyperlink, @Wlasciciel, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Hyperlink, @OstatniaModyfikacjawBO, 1)
--
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@FullClient, @IDCUID, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@FullClient, @Guid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@FullClient, @Ruid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@FullClient, @Progid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@FullClient, @Nazwapliku, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@FullClient, @Rozmiar, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@FullClient, @Keyword, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@FullClient, @Dataostatniegouruchomienia, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@FullClient, @Opublikowano, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@FullClient, @Wlasciciel, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@FullClient, @OstatniaModyfikacjawBO, 1)
--
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Excel, @IDCUID, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Excel, @Guid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Excel, @Ruid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Excel, @Progid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Excel, @Nazwapliku, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Excel, @Rozmiar, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Excel, @Keyword, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Excel, @Opublikowano, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Excel, @Wlasciciel, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Excel, @OstatniaModyfikacjawBO, 1)
--
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Flash, @IDCUID, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Flash, @Guid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Flash, @Ruid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Flash, @Progid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Flash, @Nazwapliku, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Flash, @Rozmiar, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Flash, @Keyword, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Flash, @Opublikowano, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Flash, @Wlasciciel, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Flash, @OstatniaModyfikacjawBO, 1)
--
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Powerpoint, @IDCUID, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Powerpoint, @Guid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Powerpoint, @Ruid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Powerpoint, @Progid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Powerpoint, @Nazwapliku, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Powerpoint, @Rozmiar, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Powerpoint, @Keyword, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Powerpoint, @Opublikowano, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Powerpoint, @Wlasciciel, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Powerpoint, @OstatniaModyfikacjawBO, 1)
--
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Pdf, @IDCUID, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Pdf, @Guid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Pdf, @Ruid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Pdf, @Progid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Pdf, @Nazwapliku, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Pdf, @Rozmiar, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Pdf, @Keyword, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Pdf, @Opublikowano, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Pdf, @Wlasciciel, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@Pdf, @OstatniaModyfikacjawBO, 1)
--
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@CrystalReport, @IDCUID, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@CrystalReport, @Guid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@CrystalReport, @Ruid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@CrystalReport, @Progid, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@CrystalReport, @Nazwapliku, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@CrystalReport, @Rozmiar, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@CrystalReport, @Keyword, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@CrystalReport, @Opublikowano, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@CrystalReport, @Wlasciciel, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@CrystalReport, @OstatniaModyfikacjawBO, 1)

-- DQC Tests
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestSuccess, @Longname, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestSuccess, @Benchmarkdefinition, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestSuccess, @ErrorThreshold, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestSuccess, @SamplingMethod, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestSuccess, @AdditionalInformation, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestSuccess, @CreateDate, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestSuccess, @ModifyDate, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestSuccess, @SuspendDate, 1)
--
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestFailed, @Longname, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestFailed, @Benchmarkdefinition, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestFailed, @ErrorThreshold, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestFailed, @SamplingMethod, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestFailed, @AdditionalInformation, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestFailed, @CreateDate, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestFailed, @ModifyDate, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestFailed, @SuspendDate, 1)
--
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestInactive, @Longname, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestInactive, @Benchmarkdefinition, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestInactive, @ErrorThreshold, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestInactive, @SamplingMethod, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestInactive, @AdditionalInformation, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestInactive, @CreateDate, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestInactive, @ModifyDate, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@DQCTestInactive, @SuspendDate, 1)
--------------------------------------------------------------
--------------------------------------------------------------


exec sp_update_version '1.1.5.4', '1.1.5.5';
go