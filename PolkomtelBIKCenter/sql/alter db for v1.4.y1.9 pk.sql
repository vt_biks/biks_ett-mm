﻿exec sp_check_version '1.4.y1.9';
go

update bik_admin set param = 'lisateradata.schemaName' where param = 'lisateradata.schema';
go

exec sp_update_version '1.4.y1.9', '1.4.y1.10';
go