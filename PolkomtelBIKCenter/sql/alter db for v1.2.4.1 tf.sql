﻿exec sp_check_version '1.2.4.1';
go

alter table bik_sapbo_universe_table
add server_name varchar(200) null;
go

create table bik_sapbo_connection_to_db (
	connection_node_id int not null unique references bik_node(id),
	server_name varchar(200) null,
	database_name varchar(200) null,
	default_schema_name varchar(200) null
);
go

-- poprzednia wersja w pliku: alter db for v1.1.8.6 tf.sql
-- fix b.type = 'Teradata'
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_metadata_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp

    declare @teradataKind int;
    select @teradataKind = id from bik_tree where code='Teradata'
    declare @schemaKind int;
    select @schemaKind = id from bik_node_kind where code='TeradataSchema'
    declare @tabKind int;
    select @tabKind = id from bik_node_kind where code='TeradataTable'
    declare @viewKind int;
    select @viewKind = id from bik_node_kind where code='TeradataView'
    declare @connection_kind int;
    select @connection_kind = id from bik_node_kind where code='DataConnection'
    declare @query_kind int;
    select @query_kind = id from bik_node_kind where code='ReportQuery'

    ----------   universe  -->  zapytania   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
    select bn.id as src_node_id, bnu.id as dst_node_id, 1 as type, 1 as inherit_to_descendants
    from bik_sapbo_query sbq
    join bik_node bn on sbq.node_id=bn.id
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
	where bn.is_deleted=0 

    ----------   connection  -->  zapytania   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id as src_node_id, bnc.id as dst_node_id, 1 as type
    from bik_sapbo_query sbq
    join bik_node bn on sbq.node_id=bn.id
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
    join bik_node bnc on bnc.obj_id=convert(varchar(30),uni.SI_DATACONNECTION__1) and
    bnc.is_deleted=0 and bnc.node_kind_id=(select id from bik_node_kind where code='DataConnection')
	where bn.is_deleted=0 

    ----------   universe  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnu.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')

    ----------   raport  -->  universe   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnu.id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')

    ----------   raport  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnc.id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
    join bik_node bnc on bnc.obj_id=convert(varchar(30),uni.SI_DATACONNECTION__1) and
    bnc.is_deleted=0 and bnc.node_kind_id=(select id from bik_node_kind where code='DataConnection')

	----------   connection  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnc.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
    join bik_node bnc on bnc.obj_id=convert(varchar(30),uni.SI_DATACONNECTION__1) and
    bnc.is_deleted=0 and bnc.node_kind_id=(select id from bik_node_kind where code='DataConnection')

    ----------   teradata Table/View  -->  obiekt   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)

    ----------   teradata Schema  -->  obiekt   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind

    ----------   universum  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type='Teradata' and b.is_derived=0

    ----------   teradata Table/View  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type='Teradata' and b.is_derived=0

    ----------   universum  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    where b.type='Teradata' and b.is_derived=0

    ----------   teradata Schema  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    where b.type='Teradata' and b.is_derived=0

    ----------   connection  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null

    ----------   teradata Schema  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null

    ----------   teradata Table/View  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null

    ----------   connection  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null 

    ----------   teradata Table/View  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   raport  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   teradata Schema  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id = @schemaKind
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   raport  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id = @schemaKind
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   zapytanie  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   zapytanie  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

	----------   obiekt  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct ids.object_node_id, bn.parent_node_id, 1 from aaa_ids_for_report ids join bik_node bn on ids.query_node_id=bn.id
	
	----------   UniverseTable  -->  teradata Table/View   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.name_for_teradata = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'

	----------   teradata Table/View  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bsut.node_id, 1 from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.name_for_teradata = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'

	----------   UniverseTable  -->  teradata Schema   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.schema_name = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'
	
	----------   UniverseAliasTable  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bsut1.node_id, 1 from bik_sapbo_universe_table bsut 
	join bik_sapbo_universe_table bsut1 on bsut.original_table = bsut1.their_id
	join bik_node bn on bn.id = bsut.node_id
	join bik_node bn2 on bn2.id = bsut1.node_id
	where bsut.is_alias = 1 and bn2.parent_node_id = bn.parent_node_id

	-- usuniecie alias table - univ table
	exec sp_delete_bik_joined_objs_by_kinds_fast 'UniverseTable', 'UniverseAliasTable'
	
    exec sp_move_bik_joined_objs_tmp 'TeradataView,TeradataTable,TeradataSchema,Webi,ReportQuery', 'Universe,DataConnection,Measure,Dimension,Detail,Filter,Webi,ReportQuery,UniverseAliasTable,UniverseDerivedTable,UniverseTable'
	
end;
go



-- poprzednia wersja w: alter db for v1.1.7.7 tf.sql
-- dodanie name_for_teradata i schema_name dla Oracle
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_objects_from_Designer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_objects_from_Designer]
GO

create procedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]
as
	declare @tree_id int;
	declare @kind_id int;
	declare @dimension_nki int, @measure_nki int, @detail_nki int, @filtr_nki int;
begin--begin procedure

	----wybranie id odpowiedniego drzewa
	select @tree_id = id
	from bik_tree
	where code = 'ObjectUniverses';	
	
	----wybranie id odpowiedniego kind'a
	select @kind_id = id
	from bik_node_kind
	where code = 'UniverseClass';
		
	create table #tmpNodes (si_id varchar(1000) , si_parentid varchar(1000), si_kind_id int, si_name varchar(max), si_description varchar(max));

	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Class
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select si_id_designer, si_parentid, @kind_id, name, description
	from aaa_universe_class  
	
	
	----wybranie id odpowiedniego kind'a
	--detail
	select @detail_nki = id
	from bik_node_kind
	where code = 'Detail';
	--demension
	select @dimension_nki = id
	from bik_node_kind
	where code = 'Dimension';
	--measure
	select @measure_nki = id
	from bik_node_kind
	where code = 'Measure';
	--filtr
	select @filtr_nki = id
	from bik_node_kind
	where code = 'Filter';
	
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Obj
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select obj_branch, si_parentid, 
			case
				when qualification = 'dsDetailObject' then @detail_nki
				when qualification = 'dsMeasureObject' then @measure_nki
				when qualification = 'dsDimensionObject' then @dimension_nki
				end as node_kind_id,
		 name, description
	from aaa_universe_obj
							 						 
	--wrzucam do tabeli tymczasowej wszystkie dane z Universe_filrt
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select uf.filtr_branch,	uc.si_id_designer, @filtr_nki, uf.name, nullif(ltrim(rtrim(uf.description)), '') 
	from aaa_universe_filter uf join aaa_universe_class uc on uf.universe_class_id = uc.id
	
	declare @folder_node_kind_id int;
	select @folder_node_kind_id = dbo.fn_node_kind_id_by_code('UniverseTablesFolder');

	--wrzucam katalogi do których są wrzucane tabele
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)
	select convert(varchar(10),global_props_si_id) + '|tableCatalog', convert(varchar(10),global_props_si_id), @folder_node_kind_id, 'Tabele'
	from aaa_universe 


	--wybranie id odpowiedniego kind'a
	declare @table_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseTable');
	declare @table_alias_node_kind_id int =  dbo.fn_node_kind_id_by_code('UniverseAliasTable');
	declare @table_derived_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseDerivedTable');
							 
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Table	
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)		
	select convert(varchar(10), their_id) + '|' + convert(varchar(1000), universe_branch), convert(varchar(1000), universe_branch) + '|tableCatalog', 
	case
				when is_alias = 1 then @table_alias_node_kind_id
				when is_derived = 1 then @table_derived_node_kind_id
				else @table_node_kind_id
				end as node_kind_id, name 
	from aaa_universe_table 
	
	declare @universe_column_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseColumn');
	
	----wrzucam do tymczasowej tabeli wszystkie dane z Universe_Column
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch) + '|' + uc.name, 
		convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch), @universe_column_node_kind_id, uc.name, REPLACE(REPLACE(uc.type,'ds',''),'Column','')
	from aaa_universe_table ut join aaa_universe_column uc on ut.id = uc.universe_table_id
	
	--aktualizacja istniejących węzłów w drzewie							
	update bik_node 
	set parent_node_id = null, node_kind_id = #tmpNodes.si_kind_id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.si_description, tree_id = @tree_id
	from #tmpNodes 
	where bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id and bik_node.linked_node_id is null;
	
	--dorzucanie nowych węzłów w drzewie
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select #tmpNodes.si_kind_id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.si_description
	from #tmpNodes left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
		and bik_node.tree_id = @tree_id
	where bik_node.id is null;	
	
	--uaktualnianie parentów w nodach
	declare @universe_kind_id int;
	select @universe_kind_id = id
	from bik_node_kind
	where code = 'Universe';
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id and bik_node.node_kind_id = pt.si_kind_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id 
		and  (
		 (pt.si_kind_id=@detail_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@dimension_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@measure_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@filtr_nki and bk.node_kind_id=@kind_id) or
		 (pt.si_kind_id=@universe_column_node_kind_id and (bk.node_kind_id=@table_node_kind_id or bk.node_kind_id=@table_alias_node_kind_id or bk.node_kind_id=@table_derived_node_kind_id)) or
		 (pt.si_kind_id=@kind_id and (bk.node_kind_id = @kind_id or bk.node_kind_id = @universe_kind_id))or
		 ((pt.si_kind_id=@table_node_kind_id or pt.si_kind_id=@table_alias_node_kind_id or pt.si_kind_id=@table_derived_node_kind_id) and bk.node_kind_id=@folder_node_kind_id)or
		 (pt.si_kind_id=@folder_node_kind_id and bk.node_kind_id=@universe_kind_id)
		 )
	where bk.linked_node_id is null and bik_node.parent_node_id is null;
		
	--usuwanie nodów
	update bik_node
	set is_deleted = 1
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and bik_node.node_kind_id in (@kind_id, @detail_nki, @dimension_nki, @measure_nki, -- do usuniecia tabele i kolumny i folder tabel
	  @filtr_nki);
	
	-- usuwanie obiektow, wrzuconych ze zlym parentem --> blad w Universum  
	declare @folder_universe_node_kind_id int;
	select @folder_universe_node_kind_id = dbo.fn_node_kind_id_by_code('UniversesFolder');
	
	declare @universe_node_kind_id int;
	select @universe_node_kind_id = dbo.fn_node_kind_id_by_code('Universe');
	
	update bik_node
	set is_deleted = 1
	where parent_node_id is null and tree_id = @tree_id and node_kind_id not in (@universe_node_kind_id, @folder_universe_node_kind_id)
	
	drop table #tmpNodes;
	--uzupełnianie danych w tabelach extra 
	--dla object
	create table #tmpObjExtra(text_of_select varchar(max),text_of_where varchar(max), type varchar(155), aggregate_function varchar(50), node_id int)
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select univ_obj.text_of_select, univ_obj.text_of_where, univ_obj.type, univ_obj.aggregate_function, bik_node.id
	from aaa_universe_obj univ_obj join bik_node on bik_node.obj_id = univ_obj.obj_branch 
		and bik_node.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)-- = @obj_node_kind_id;
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select null, uf.where_clause, uf.type, null, bik_node.id
	from aaa_universe_filter uf join bik_node on bik_node.obj_id = uf.filtr_branch and bik_node.node_kind_id = @filtr_nki
	
	update bik_sapbo_universe_object
	set text_of_select = #tmpObjExtra.text_of_select, text_of_where = #tmpObjExtra.text_of_where, type = #tmpObjExtra.type,  
		           aggregate_function = #tmpObjExtra.aggregate_function 
	from #tmpObjExtra join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id;
	
	insert into bik_sapbo_universe_object(text_of_select, text_of_where, type, aggregate_function, node_id)
	select #tmpObjExtra.text_of_select,#tmpObjExtra.text_of_where, #tmpObjExtra.type, #tmpObjExtra.aggregate_function, #tmpObjExtra.node_id 
	from #tmpObjExtra left join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id
	where bik_sapbo_universe_object.id is null;

	drop table #tmpObjExtra;
	
	--dla tabeli
	
	update bik_sapbo_universe_table
	set is_alias = ut.is_alias, is_derived = ut.is_derived, original_table = ut.original_table, sql_of_derived_table = ut.sql_of_derived_table, 
		sql_of_derived_table_with_alias = ut.sql_of_derived_table_with_alias, their_id = ut.their_id, node_id = bik_node.id, type = ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
	where linked_node_id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	
	insert into bik_sapbo_universe_table(is_alias, is_derived, original_table, sql_of_derived_table, sql_of_derived_table_with_alias, their_id, node_id, type)
	select ut.is_alias, ut.is_derived, ut.original_table, ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, ut.their_id, 
					bik_node.id as node_id, ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
		
	where  bik_node.linked_node_id is null and bsut.id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)

	--uzupełnianie name_for_teradata: Teradata i Oracle
	update bik_sapbo_universe_table
	set name_for_teradata = replace(bik_node.name,'.', '|') + '|'
	from bik_sapbo_universe_table 
	inner join bik_node on bik_sapbo_universe_table.node_id = bik_node.id
	where bik_sapbo_universe_table.type in ('Teradata', 'ODBC', 'Oracle OCI') and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 0

	update bik_sapbo_universe_table
	set name_for_teradata = tabo.name_for_teradata
	from bik_sapbo_universe_table 
	inner join bik_sapbo_universe_table tabo on bik_sapbo_universe_table.original_table = tabo.their_id 
	inner join bik_node bn on bn.id = bik_sapbo_universe_table.node_id
	inner join bik_node bn2 on bn2.id = tabo.node_id
	where bik_sapbo_universe_table.type in ('Teradata', 'ODBC', 'Oracle OCI') and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 1 
	and bn2.parent_node_id = bn.parent_node_id
	
	update bik_sapbo_universe_table
	set schema_name = left(name_for_teradata, charindex('|',name_for_teradata))
	where type in ('Teradata', 'ODBC', 'Oracle OCI') and is_derived = 0
	
	--dla kolumn
	update bik_sapbo_universe_column
	set name = uc.name, type = uc.type, table_id = bik_node.id--bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			--join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
			join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bik_node.id--bsut.id
	where linked_node_id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	
	insert into bik_sapbo_universe_column(name, type, table_id)
	select uc.name, uc.type, bik_node.id--bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			--join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id =bik_node.id--bsut.id
	where linked_node_id is null and bsuc.id is null  and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)

	--delete from bik_sapbo_universe_column i bik_sapbo_universe_column nie jest potrzebne bo w bik_node jest ustawiont isDeleted na 1:)
	
	delete from bik_sapbo_universe_connection;
	
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id, type, database_engine, client_number, language, system_number, system_id)
	select aaa_universe_connection.server, aaa_universe_connection.user_name, aaa_universe_connection.password, aaa_universe_connection.database_source, 
		aaa_universe_connection_networklayer.name, bik_node.id,
		aaa_universe_connection.type, aaa_universe_connection.database_enigme, aaa_universe_connection.client_number, aaa_universe_connection.language,
		aaa_universe_connection.system_number, aaa_universe_connection.system_id
	from aaa_universe_connection join aaa_universe_connection_networklayer on aaa_universe_connection.connetion_networklayer_id = aaa_universe_connection_networklayer.id
		join bik_node on aaa_universe_connection.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		left join bik_sapbo_universe_connection on bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections' and bik_sapbo_universe_connection.id is null;
	
	-- dodanie statystyk dla światów obiektów	
	update bik_sapbo_extradata
	set statistic = u.statistic
	from bik_sapbo_extradata join bik_node bn on bn.id = bik_sapbo_extradata.node_id
		join bik_node_kind bnk on bn.node_kind_id = bnk.id
		join aaa_universe u on convert(varchar,u.global_props_si_id) = bn.obj_id
	where bnk.code = 'Universe'	
	
	-- ustawianie visual order dla folderów tabel
	declare @nk_id int = (select id from bik_node_kind where code = 'UniverseTablesFolder')
	update bik_node set visual_order = -1 where node_kind_id = @nk_id
				
	exec sp_delete_linked_nodes_where_orignal_is_deleted;	
	exec sp_node_init_branch_id @tree_id, null;
end;
go

-- poprzednia wersja w pliku: alter db for v1.1.8.6 tf.sql
-- fix na drzewko MS SQL
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_mssql_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
    
    declare @connection_kind int;
    select @connection_kind = id from bik_node_kind where code='DataConnection'    
    declare @universe_kind int;
    select @universe_kind = id from bik_node_kind where code='Universe'
    declare @query_kind int;
    select @query_kind = id from bik_node_kind where code='ReportQuery'
    declare @mssqlTree int;
    select @mssqlTree = id from bik_tree where code = 'MS SQL'

	---------- universe table --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	
	---------- universe table --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	
	---------- object --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	
	---------- object --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	
	---------- query --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- query --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null	

	---------- universe --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- universe --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null


	
	---------- mssql table --> universe table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, ut.node_id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	---------- mssql table --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- mssql table --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql table --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null

	---------- mssql DB --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null		
	
    exec sp_move_bik_joined_objs_tmp '@MSSQL', '@Reports, @ObjectUniverses, @Connections'
    
end;
go

-- poprzednia wersja w pliku: alter db for v1.0.98 tf.sql
-- fix folderków
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbo_connections]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbo_connections]
as
begin

	create table #tmp_network (name varchar(max));
	create table #tmp_engine (db_engine varchar(max),network_layer varchar(max));
	create table #tmp_conn (conn_node_id int,parent_node_id int);

	declare @connTree int;
	select @connTree = id from bik_tree where code='Connections'
	declare @connFolderKind int;
	select @connFolderKind = id from bik_node_kind where code='ConnectionFolder'
	declare @connEngineFolderKind int;
	select @connEngineFolderKind = id from bik_node_kind where code='ConnectionEngineFolder'
	declare @connNetworkFolderKind int;
	select @connNetworkFolderKind = id from bik_node_kind where code='ConnectionNetworkFolder'
	declare @parentNodeId int;
	select @parentNodeId = id from bik_node where obj_id = '1'/*name='Połączenia'*/ and tree_id = @connTree and node_kind_id = @connFolderKind and is_deleted = 0 and linked_node_id is null and parent_node_id is null

	-- wrzucenie network layers folders
	insert into #tmp_network(name)
	select distinct connetion_networklayer_name from bik_sapbo_universe_connection

	-- przywrocenie dobrych folderkow, usunietych po 1 fazie zasilania
	update bik_node
	set is_deleted = 0
	where tree_id = @connTree
	and is_deleted = 1
	and linked_node_id is null
	and node_kind_id = @connNetworkFolderKind
	and parent_node_id = @parentNodeId
	and name in (select name from #tmp_network)

	-- dodanie nowych, jeśli są nowe
	insert into bik_node (parent_node_id, node_kind_id, name, tree_id)
	select @parentNodeId,@connNetworkFolderKind, case when #tmp_network.name is null then 'Inne' else #tmp_network.name end, @connTree
	from #tmp_network 
	left join bik_node bn on bn.tree_id = @connTree and bn.is_deleted = 0 and bn.name = #tmp_network.name
	and bn.linked_node_id is null and bn.node_kind_id = @connNetworkFolderKind
	where bn.id is null;

	-- usuniecie starych/niepotrzebnych
	update bik_node
	set is_deleted = 1
	where node_kind_id = @connNetworkFolderKind
	and name not in (select name from #tmp_network)
	and linked_node_id is null
	and is_deleted = 0

	-- wrzucenie DB engine folders
	insert into #tmp_engine(db_engine, network_layer)
	select distinct case when database_engine is null then 'Inne' else database_engine end,connetion_networklayer_name from bik_sapbo_universe_connection

	-- przywrocenie dobrych folderkow, usunietych po 1 fazie zasilania
	update bik_node
	set is_deleted = 0
	where tree_id = @connTree
	and is_deleted = 1
	and linked_node_id is null
	and node_kind_id = @connEngineFolderKind
	and parent_node_id in (select id from bik_node where tree_id = @connTree and is_deleted = 0 and linked_node_id is null and node_kind_id = @connNetworkFolderKind)
	and name in (select db_engine from #tmp_engine)

	-- dodanie nowych, jeśli są nowe
	insert into bik_node (parent_node_id, node_kind_id, name, tree_id)
	select (select id from bik_node where tree_id = @connTree and is_deleted = 0 and name = #tmp_engine.network_layer and linked_node_id is null and node_kind_id = @connNetworkFolderKind),@connEngineFolderKind, #tmp_engine.db_engine, @connTree
	from #tmp_engine 
	left join bik_node bn on bn.tree_id = @connTree and bn.is_deleted = 0 and bn.name = #tmp_engine.db_engine
	and bn.linked_node_id is null and bn.node_kind_id = @connEngineFolderKind
	and bn.parent_node_id = (select id from bik_node where tree_id = @connTree and is_deleted = 0 and name = #tmp_engine.network_layer and linked_node_id is null and node_kind_id = @connNetworkFolderKind)
	where bn.id is null;

	-- usuniecie starych/niepotrzebnych	
	update bik_node
	set is_deleted = 1
	where is_deleted = 0 
	and node_kind_id = @connEngineFolderKind
	and name not in (select db_engine from #tmp_engine)
	and linked_node_id is null

	-- podłączenie połączeń pod odpowiednie foldery
	insert into #tmp_conn(conn_node_id,parent_node_id)
	select bsuc.node_id, bn.id 
	from bik_sapbo_universe_connection bsuc 
	inner join bik_node bn on bn.tree_id = @connTree 
	and bn.is_deleted = 0
	and bn.name = case when bsuc.database_engine is null then 'Inne' else bsuc.database_engine end
	and bn.linked_node_id is null 
	and bn.node_kind_id = @connEngineFolderKind
	inner join bik_node bn2 on bn.parent_node_id = bn2.id
	and bn2.name = bsuc.connetion_networklayer_name

	update bik_node
	set parent_node_id = #tmp_conn.parent_node_id
	from #tmp_conn
	where bik_node.id = #tmp_conn.conn_node_id

	drop table #tmp_engine;
	drop table #tmp_network;
	drop table #tmp_conn;

	exec sp_node_init_branch_id @connTree, null;

end;
go


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_oracle_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_oracle_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_oracle_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
    
    declare @connection_kind int;
    select @connection_kind = id from bik_node_kind where code='DataConnection'    
    declare @universe_kind int;
    select @universe_kind = id from bik_node_kind where code='Universe'
    declare @query_kind int;
    select @query_kind = id from bik_node_kind where code='ReportQuery'
    declare @oracleTree int;
    select @oracleTree = id from bik_tree where code = 'Oracle'

	---------- universe table --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	
	---------- universe table --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	
	---------- object --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	
	---------- object --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	
	---------- query --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- query --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null	

	---------- universe --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- universe --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null


	
	---------- oracle table --> universe table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, ut.node_id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	---------- oracle table --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- oracle table --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- oracle table --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- oracle DB --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null

	---------- oracle DB --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- oracle DB --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null		
	
    exec sp_move_bik_joined_objs_tmp '@Oracle', '@Reports, @ObjectUniverses, @Connections'
    
end;
go

exec sp_update_version '1.2.4.1', '1.2.4.2';
go