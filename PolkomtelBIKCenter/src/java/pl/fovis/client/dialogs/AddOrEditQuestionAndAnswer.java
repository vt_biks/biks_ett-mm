/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.common.FrequentlyAskedQuestionsBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author AMamcarz
 */
public class AddOrEditQuestionAndAnswer extends BaseRichTextAndTextboxDialog {

    private IParametrizedContinuation<FrequentlyAskedQuestionsBean> saveCont;
    private FrequentlyAskedQuestionsBean record;

    @Override
    protected String getDialogCaption() {
        return I18n.dodajArtykul.get() /* I18N:  */;//"Q&A";
    }

    protected void innerDoAction(String title, String body) {
        FrequentlyAskedQuestionsBean qAndABean = new FrequentlyAskedQuestionsBean();
        qAndABean.id = (record != null) ? record.id : 0;
        qAndABean.questionText = title;
        qAndABean.answerText = body;
        saveCont.doIt(qAndABean);
    }

    public void buildAndShowDialog(FrequentlyAskedQuestionsBean qAndA, IParametrizedContinuation<FrequentlyAskedQuestionsBean> saveCont) {
        this.saveCont = saveCont;
        this.record = qAndA;
        super.buildAndShowDialog();
    }
}
