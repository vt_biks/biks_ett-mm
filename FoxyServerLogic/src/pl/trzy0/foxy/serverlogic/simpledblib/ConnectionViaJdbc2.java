/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.serverlogic.simpledblib;

import commonlib.DateUtils;
import simplelib.LameRuntimeException;
import commonlib.LameUtils;
import commonlib.LameUtils.SqlFlavor;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class ConnectionViaJdbc2 extends ConnectionViaJdbc {
    
    private Statement batchStmt;
    
    public ConnectionViaJdbc2(Map<String, Object> props) {
        super(props);
    }
    
    private List<String> batchTxtSplit;
    
    @Override
    public void startBatch(String sqlTxt, int[] paramTypes) {
        if (batchStmt != null) {
            throw new LameRuntimeException("cannot start new batch (\"" +
                    sqlTxt + "\") while other (\"" + batchTxt + "\") is started");
        }
        try {
            batchStmt = conn.createStatement();
            //conn.setAutoCommit(false);
        } catch (Exception ex) {
            throw new LameRuntimeException("error preparing statement for sql: \"" + sqlTxt + "\"", ex);
        }
        batchTxt = sqlTxt;
        this.paramTypes = paramTypes;
        batchTxtSplit = LameUtils.splitBySep(sqlTxt, "?");
    }

    protected String getParValAsSqlTxt(Object parVal, int jdbcType) {
        if (parVal == null)
            return "NULL";
        
        String res;
        
        switch (jdbcType) {
            case Types.INTEGER:
            case Types.DOUBLE:
                res = parVal.toString();
                break;
            case Types.DATE:
                res = "convert(datetime, '" + 
                        DateUtils.getDateAsISO8601DateTimeString((Date) parVal) + 
                        "', 126)";
                break;
            case Types.VARCHAR:
                res = LameUtils.toSqlString(parVal, SqlFlavor.Other);
                break;
            default:
                throw new LameRuntimeException("jdbcType=" + jdbcType + " is not supported, val=\"" + parVal + "\"");
        }
        
        return res;
    }
    
    protected String getExpliciteSqlForBatchParams(Object[] parVals) {
        StringBuilder sb = new StringBuilder();
        
        for (int i = 0; i < parVals.length; i++) {
            String txtPart = batchTxtSplit.get(i);
            sb.append(txtPart).append(getParValAsSqlTxt(parVals[i], paramTypes[i]));
        }

        sb.append(batchTxtSplit.get(parVals.length));
        
        return sb.toString();
    }
    
    @Override
    public void addToBatch(Object[] parVals) {
        if (batchStmt == null) {
            throw new LameRuntimeException("no batch is started");
        }
        try {
            batchStmt.addBatch(getExpliciteSqlForBatchParams(parVals));
        } catch (Exception ex) {
            throw new LameRuntimeException("error adding to batch for sql: \"" +
                    batchTxt + "\" and param values: " +
                    Arrays.toString(parVals), ex);
        }
    }

    @Override
    public void execBatch() {
        try {
            batchStmt.executeBatch();
            conn.commit();
        } catch (Exception ex) {
            throw new LameRuntimeException("error executing batch for sql: \"" +
                    batchTxt + "\"", ex);
        }
        clearBatch();
    }

    @Override
    public void clearBatch() {
        try {
            if (batchStmt != null) {
                batchStmt.clearBatch();
                batchStmt.close();
                conn.rollback();
            }
            //conn.setAutoCommit(true);
        } catch (Exception ex) {
            throw new LameRuntimeException("error clearing batch for sql: \"" +
                    batchTxt + "\"", ex);
        }
        batchStmt = null;
        batchTxt = null;
    }
}
