﻿exec sp_check_version '1.1.8.10';
go

create table bik_schedule(
	id int identity not null primary key,
	source_id int not null references bik_data_source_def(id),
	hour int not null,
	minute int not null,
	interval int not null,
	date_started date null,
	is_schedule bit not null,
	priority int not null
);
go


insert into bik_schedule(source_id,hour,minute,interval,date_started,is_schedule,priority)
values((select id from bik_data_source_def where description='Business Objects'),20,0,1,null,1,5)

insert into bik_schedule(source_id,hour,minute,interval,date_started,is_schedule,priority)
values((select id from bik_data_source_def where description='Teradata DBMS'),20,0,1,null,1,3)

insert into bik_schedule(source_id,hour,minute,interval,date_started,is_schedule,priority)
values((select id from bik_data_source_def where description='DQC'),20,0,1,null,1,7)

insert into bik_schedule(source_id,hour,minute,interval,date_started,is_schedule,priority)
values((select id from bik_data_source_def where description='MS SQL'),20,0,1,null,0,4)

insert into bik_schedule(source_id,hour,minute,interval,date_started,is_schedule,priority)
values((select id from bik_data_source_def where description='Active Directory'),20,0,1,null,1,8)


alter table bik_data_load_log alter column status_description varchar(max) null
go


exec sp_update_version '1.1.8.10', '1.1.8.11';
go