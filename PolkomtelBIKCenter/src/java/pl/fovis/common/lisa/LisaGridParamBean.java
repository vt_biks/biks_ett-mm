/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.entitydetailswidgets.lisa.LisaFiltrColumnValueBase;

/**
 *
 * @author pku
 */
public class LisaGridParamBean implements Serializable {

    public int limit;
    public int offset;
    public Integer categoryNodeId;
    public String nodeKindCode;
    public boolean isAcsending;
    public String sortColumnName;
    public List<LisaFiltrColumnValueBase> infoToFiltr = new ArrayList<LisaFiltrColumnValueBase>();

    public LisaGridParamBean() {
    }

    public LisaGridParamBean(int offset, int limit, Integer categoryId, String nodeKindCode) {
        this(offset, limit, categoryId, nodeKindCode, null, true, null);
    }

    public LisaGridParamBean(int offset, int limit, Integer categoryId, String nodeKindCode, List<LisaFiltrColumnValueBase> infoToFiltr) {
        this(offset, limit, categoryId, nodeKindCode, infoToFiltr, true, null);
    }

    public LisaGridParamBean(int offset, int limit, Integer categoryId, String nodeKindCode, List<LisaFiltrColumnValueBase> infoToFiltr, boolean isAcsending, String sortColumnName) {
        this.offset = offset;
        this.limit = limit;
        this.categoryNodeId = categoryId;
        this.nodeKindCode = nodeKindCode;
        this.isAcsending = isAcsending;
        this.sortColumnName = sortColumnName;
        if (infoToFiltr != null) {
            this.infoToFiltr = infoToFiltr;
        }
    }
}
