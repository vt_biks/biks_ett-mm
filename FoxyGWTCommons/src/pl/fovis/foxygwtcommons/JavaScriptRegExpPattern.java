/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import simplelib.BaseUtils;
import simplelib.IRegExpMatcher;
import simplelib.IRegExpPattern;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public class JavaScriptRegExpPattern implements IRegExpPattern {

    private static final String[] optionsArray = {"", "i" /* I18N: no */, "m" /* I18N: no */, "im" /* I18N: no */};
    private String pattern;
//    private boolean ignoreCase;
//    private boolean multiline;
    private String options;

    private native Object createRegExpr(String pattern, String options) /*-{
     return new RegExp(pattern, options);
     }-*/;

    public JavaScriptRegExpPattern(String pattern, boolean ignoreCase, boolean multiline) {
        this.pattern = pattern;
        int idx = BaseUtils.booleanToInt(ignoreCase, 0, 1) + BaseUtils.booleanToInt(multiline, 0, 2);
        this.options = "g" /* I18N: no */ + optionsArray[idx];
    }

    public IRegExpMatcher matcher(String str) {
        return new JavaScriptRegExpMatcher(createRegExpr(pattern, options), str);
    }
}
