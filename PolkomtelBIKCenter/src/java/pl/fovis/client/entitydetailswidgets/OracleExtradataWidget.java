/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import pl.fovis.common.BIKConstants;

/**
 *
 * @author tflorczak
 */
public class OracleExtradataWidget extends DatabaseExtradataWidget {

    @Override
    protected boolean isTableOrView() {
        return getData().node.nodeKindCode.equals(BIKConstants.NODE_KIND_ORACLE_TABLE) || getData().node.nodeKindCode.equals(BIKConstants.NODE_KIND_ORACLE_VIEW);
    }

    @Override
    protected String getFKColumnIconName() {
        return "images/oracleColumnIDX.gif";
    }

    @Override
    protected String getClusteredIndexIconName() {
        return "images/oracleIndex.gif";
    }

    @Override
    protected String getNonClusteredIndexIconName() {
        return "images/oracleIndex.gif";
    }

    @Override
    protected boolean treeIdFilter() {
        return getData().node.treeCode.equals(BIKConstants.TREE_CODE_ORACLE);
    }
}
