/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.HTML;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddOrEditNewsDialog;
import pl.fovis.client.entitydetailswidgets.IGridBeanAction;
import pl.fovis.client.entitydetailswidgets.SimpleGridBeanAction;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.NewsBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author mmastalerz
 */
public class NewsActionExtractor {

    public IParametrizedContinuation<Pair<Boolean, Boolean>> dataChangedHandler;
    protected IGridBeanAction<NewsBean> actNewsEdit = new SimpleGridBeanAction<NewsBean>() {
        public String getActionIconUrl(NewsBean b) {
            if (b == null) {
                return null;
            }
            return getIconUrlByIconName("edit_gray");
        }

        public String getActionIconHint(NewsBean b) {
            return I18n.edytuj.get() /* I18N:  */;
        }

        public void executeAction(NewsBean b) {
            execActEdit(b);
        }

        public boolean isVisible(NewsBean b) {
            return true;
        }

        public boolean isEnabled(NewsBean b) {
            return BIKClientSingletons.isEffectiveExpertLoggedInEx(BIKConstants.ACTION_NEWS_EDITOR);
//            return BIKClientSingletons.isEffectiveExpertLoggedIn();
        }

        public IGridBeanAction.TypeOfAction getActionType() {
            return IGridBeanAction.TypeOfAction.Edit;
        }
    };
    protected IGridBeanAction<NewsBean> actNewsDelete = new SimpleGridBeanAction<NewsBean>() {
        public String getActionIconUrl(NewsBean b) {
            if (b == null) {
                return null;
            }
            return getIconUrlByIconName("trash_gray");
        }

        public String getActionIconHint(NewsBean b) {
            return I18n.usun.get() /* I18N:  */;
        }

        public void executeAction(final NewsBean b) {

            //
            HTML element = new HTML(
                    I18n.czyNaPewnoUsunacAktualnosc.get() /* I18N:  */ + ": <div class='Delete-Element'><img src='images/news.gif'/> <b>" + b.title + "</b></div>");
            element.setStyleName("komunikat" /* I18N: no:css-class */);
            new ConfirmDialog().buildAndShowDialog(element, I18n.usun.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            execActDelete(b);
                        }
                    });
        }

        public boolean isVisible(NewsBean b) {
            return true;
        }

        public boolean isEnabled(NewsBean b) {
            return BIKClientSingletons.isEffectiveExpertLoggedInEx(BIKConstants.ACTION_NEWS_EDITOR);
//            return BIKClientSingletons.isEffectiveExpertLoggedIn();
        }

        public IGridBeanAction.TypeOfAction getActionType() {
            return IGridBeanAction.TypeOfAction.Delete;
        }
    };

    public List<IGridBeanAction<NewsBean>> createActionList() {
        List<IGridBeanAction<NewsBean>> res = new ArrayList<IGridBeanAction<NewsBean>>();
        res.add(actNewsEdit);
        res.add(actNewsDelete);
        return res;
    }

    protected static String getIconUrlByIconName(String iconName) {
        return "images" /* I18N: no */ + "/" + iconName + "." + "gif" /* I18N: no */;
    }

    protected void execActDelete(final NewsBean b) {
        BIKClientSingletons.getService().deleteNews(b.id, new StandardAsyncCallback<Void>() {
            public void onSuccess(Void result) {
                refreshView(I18n.usunietoZAktualnosci.get() /* I18N:  */, false, false);
            }
        });
    }

    protected void execActEdit(final NewsBean b) {
//        BIKClientSingletons.getService().getNewsToEdit(b.id, new StandardAsyncCallback<NewsBean>() {
//            public void onSuccess(final NewsBean result) {
        new AddOrEditNewsDialog() //                new AddNewsDialog()
                .buildAndShowDialog(b, new IParametrizedContinuation<NewsBean>() {
                    public void doIt(NewsBean param) {
                        BIKClientSingletons.getService().updateNews(param, new StandardAsyncCallback<Void>() {
                            public void onSuccess(Void result) {
                                refreshView(I18n.zmodyfikowanoAktualnosc.get() /* I18N:  */, true, true);
                            }
                        });
                    }
                });
//
//            }
//        });
    }

    public String getName(NewsBean b) {
        return null;
    }

    public String getOptIconHint(NewsBean b) {
        return null;
    }

    protected void refreshView(String msg, boolean refetch, boolean resetPosition) {
        if (dataChangedHandler != null) {
            dataChangedHandler.doIt(new Pair<Boolean, Boolean>(refetch, resetPosition));
            BIKClientSingletons.showInfo(msg);
        }
    }

    public void setDataChangedHandler(IParametrizedContinuation<Pair<Boolean, Boolean>> handler) {
        this.dataChangedHandler = handler;
    }
}
