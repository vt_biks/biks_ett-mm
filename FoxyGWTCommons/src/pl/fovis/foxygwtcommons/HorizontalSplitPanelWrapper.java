/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author pmielanczuk
 */
@SuppressWarnings("deprecation")
public class HorizontalSplitPanelWrapper implements IsWidget {

    private Panel hsp = new com.google.gwt.user.client.ui.HorizontalSplitPanel();

    @Override
    public Widget asWidget() {
        return hsp;
    }

    public com.google.gwt.user.client.Element getElement() {
        return hsp.getElement();
    }

    public <H extends EventHandler> HandlerRegistration addHandler(final H handler, GwtEvent.Type<H> type) {
        return hsp.addHandler(handler, type);
    }

    public boolean isResizing() {
        return ((com.google.gwt.user.client.ui.HorizontalSplitPanel) hsp).isResizing();
    }

    public void setWidth(String width) {
        hsp.setWidth(width);
    }

    public void setSplitPosition(String pos) {
        ((com.google.gwt.user.client.ui.HorizontalSplitPanel) hsp).setSplitPosition(pos);
    }

    public void add(IsWidget w) {
        hsp.add(w);
    }
}
//    @SuppressWarnings("deprecation")
//    private com.google.gwt.user.client.ui.HorizontalSplitPanel aqq;
//    @SuppressWarnings("deprecation")
//    public static void testM(com.google.gwt.user.client.ui.HorizontalSplitPanel x) {
//        if (x.isResizing()) {
//            Window.alert("xxx" + x);
//        }
//    }
//    @SuppressWarnings("deprecation")
//    private com.google.gwt.user.client.ui.HorizontalSplitPanel getHSP() {
//        return (com.google.gwt.user.client.ui.HorizontalSplitPanel) hsp;
//    }
