/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.Window;
import java.util.Stack;
import pl.fovis.foxygwtcommons.dialogs.Dialogs;
import pl.fovis.foxygwtcommons.i18n.I18n;
import pl.trzy0.foxy.commons.FoxyCommonConsts;
import simplelib.BaseUtils;
import simplelib.IContinuationWithReturn;

/**
 *
 * @author pmielanczuk
 */
public class FoxyClientSingletons {
    //ww: domyślnie servlet obsługujący upload/download plików jest zamapowany
    // w web.xml na /fsb. jeżeli to ma byc inne mapowanie, trzeba to nadpisać!

    public static String FILE_HANDLING_SERVLET_NAME = "fsb" /* I18N: no */;
    public static final IFoxyNotificationHandler notificationHandlerByDialogs = new IFoxyNotificationHandler() {
        public void showInfo(String msg) {
            Dialogs.alert(msg, I18n.informacja.get() /* I18N:  */, null);
        }

        public void showWarning(String err) {
            Window.alert(I18n.warning.get() /* I18N:  */ + ": " + err);
        }

        public boolean isInfoModal() {
            return false;
        }

        public boolean isWarningModal() {
            return true;
        }
    };
    private static IFoxyNotificationHandler rootNotificationHandler = notificationHandlerByDialogs;
    protected static Stack<IFoxyNotificationHandler> notificationHandlers = new Stack<IFoxyNotificationHandler>();
//    public static final String ERROR_IN_COMMUNICATION_MSG = I18n.bladKomunikacji.get() /* I18N:  */;
    public final static StandardAsyncCallback<Void> doNothingCallback = new StandardAsyncCallback<Void>() {
        @Override
        public void onSuccess(Void result) {
            // no-op
        }
    };

    public static IFoxyNotificationHandler getNotificationHandler() {
        return notificationHandlers.isEmpty() ? rootNotificationHandler : notificationHandlers.peek();
    }

    private static IContinuationWithReturn<Boolean> notificationDisabler;

    protected static boolean areNotificationsDisabled() {
        if (notificationDisabler == null) {
            return false;
        }

        return BaseUtils.boolNullToDef(notificationDisabler.doIt(), false);
    }

    public static void showInfo(String msg) {
        if (areNotificationsDisabled()) {
            return;
        }
        getNotificationHandler().showInfo(msg);
    }

    public static void showWarning(String err, Throwable ex) {
        if (areNotificationsDisabled()) {
            return;
        }
        IFoxyNotificationHandler nh = getNotificationHandler();
        if (nh instanceof IFoxyNotificationHandlerEx) {
            IFoxyNotificationHandlerEx nhe = (IFoxyNotificationHandlerEx) nh;
            nhe.showWarning(err, ex);
        } else {
            nh.showWarning(err);
        }
    }

    public static void showWarning(String err) {
        if (areNotificationsDisabled()) {
            return;
        }
        getNotificationHandler().showWarning(err);
    }

    public static void showError(String err) {
        Window.alert(I18n.blad.get() /* I18N:  */ + ": " + err);
    }

    public static void setRootNotificationHandler(IFoxyNotificationHandler notificationHandler) {
        FoxyClientSingletons.rootNotificationHandler = notificationHandler;
    }

    public static boolean isInfoModal() {
        return getNotificationHandler().isInfoModal();
    }

    public static boolean isWarningModal() {
        return getNotificationHandler().isWarningModal();
    }

    public static void addNotificationHandler(IFoxyNotificationHandler notificationHandler) {
        notificationHandlers.add(notificationHandler);
    }

    public static void removeNotificationHandler(IFoxyNotificationHandler notificationHandler) {
        notificationHandlers.remove(notificationHandler);
    }

    public static String getFixedFileResourceUrl(String uploadedFileOrFullUrl) {
        return getFixedFileResourceUrlEx(uploadedFileOrFullUrl, null);
    }

    public static String getFixedFileResourceUrlEx(String uploadedFileOrFullUrl, String optClientFileName) {
        return getFixedFileResourceUrl(FILE_HANDLING_SERVLET_NAME, uploadedFileOrFullUrl, optClientFileName);
    }

    public static String getFixedFileResourceUrl(String uploadServletName, String uploadedFileOrFullUrl, String optClientFileName) {
        return uploadedFileOrFullUrl.startsWith("file_" /* I18N: no */)
                ? getFullFileUploadUrlWithServletName(uploadServletName, uploadedFileOrFullUrl, optClientFileName)
                : uploadedFileOrFullUrl;
//                BaseUtils.isUrlGlobal(urlPartOrFull) ? urlPartOrFull
//                : getFullFileUploadUrl(uploadServletName, urlPartOrFull);
    }

    public static String getFullFileUploadUrl(String uploadedFileName) {
        return getFullFileUploadUrl(uploadedFileName, null);
    }

    public static String getFullFileUploadUrl(String uploadedFileName, String optClientFileName) {
        return getFullFileUploadUrlWithServletName(FILE_HANDLING_SERVLET_NAME, uploadedFileName, optClientFileName);
    }

    public static String getFullFileUploadUrlWithServletName(String uploadServletName, String uploadedFileName, String optClientFileName) {
//        Window.alert("getFullFileUploadUrlWithServletName: optClientFileName = " + (optClientFileName == null ? "" : optClientFileName));
//        Window.alert("getFullFileUploadUrlWithServletName: URL.encodeQueryString(optClientFileName) = " + (optClientFileName == null ? "" : URL.encodeQueryString(optClientFileName)));
        return getWebAppUrlPrefixForOtherServlet(uploadServletName) + "?get=" /* I18N: no */ + uploadedFileName
                + (optClientFileName != null ? "&" + FoxyCommonConsts.CLIENT_FILE_NAME_SERVLET_PARAM_NAME + "="
                        + URL.encodeQueryString(optClientFileName) : "");
    }

    public static String getWebAppUrlPrefixForFileHandlingServlet() {
        return getWebAppUrlPrefixForOtherServlet(FILE_HANDLING_SERVLET_NAME);
    }

    protected static String getWebAppUrlPrefixForOtherServlet(String servletName) {
        //ww: to będzie np. http://localhost:8084/BIKS/pl.fovis.bikcenter/
        String urlBase = GWT.getModuleBaseURL();
        //ww: GWT.getModuleName() to np. pl.fovis.bikcenter
//        Window.alert("urlBase=" + urlBase + ", moduleName=" + GWT.getModuleName());
        //ww: usuwa / z końca
        urlBase = urlBase.substring(0, urlBase.length() - 1);
        //ww: odnajduje / oddzielający pakiet od nazwy servletu
        int pos = urlBase.lastIndexOf("/");
        urlBase = urlBase.substring(0, pos);
//        Window.alert("cropped urlBase=" + urlBase);
        return urlBase + "/" + servletName;
    }

    //ww: zamiast tego należy używać getFullFileUploadUrl("fsb", fileName)
    // gdzie fsb to przykładowa (i standardowa nazwa servletu do uploadu i serwowania plików)
    @Deprecated
    public static String getWebAppUrlPrefixForUploadedFile(String servletNameAndSthMore) {
        return getWebAppUrlPrefixForOtherServlet(servletNameAndSthMore);
    }

    protected static void setupRpcRequestBuilderOneParam(String paramName, String reqExtraParamName) {
        FoxyRpcRequestBuilder.setReqExtraParam(reqExtraParamName, Window.Location.getParameter(paramName));
    }

    protected static void setupRpcRequestBuilderOneParam(String name) {
        setupRpcRequestBuilderOneParam(name, name);
    }

    public static void setNotificationDisabler(IContinuationWithReturn<Boolean> nd) {
        notificationDisabler = nd;
    }
}
