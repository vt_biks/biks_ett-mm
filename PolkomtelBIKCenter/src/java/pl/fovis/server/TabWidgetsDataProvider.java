/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.GlobalThreadStopWatch;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.AttachmentBean;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BlogBean;
import pl.fovis.common.IDstNodeIdExtractor;
import pl.fovis.common.IHasSimilarityScore;
import pl.fovis.common.JoinedObjBean;
import pl.fovis.common.JoinedObjsTabKindEx;
import pl.fovis.common.NoteBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.UserBean;
import pl.fovis.common.UserInNodeBean;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.FieldNameConversion;
import simplelib.Pair;

/**
 * Klasa, która dostarcza dane do zakładek z powiązaniami (biblioteka, blogi, użytkownicy, dokumenty, komentarze, itp.)
 * @author tflorczak
 */
public class TabWidgetsDataProvider {

    protected INamedSqlsDAO<Object> adhocDao;
    protected IBOXIServiceForTabWidgetDP service;

    public TabWidgetsDataProvider(INamedSqlsDAO<Object> adhocDao, IBOXIServiceForTabWidgetDP service) {
        this.adhocDao = adhocDao;
        this.service = service;
    }

    public Pair<List<UserInNodeBean>, Map<Integer, String>> getUsersInNode(int nodeId, String branchIds) {
        String avatarField = service.getAvatarFieldForAd();
        List<Integer> res = BIKCenterUtils.splitBranchIdsEx(branchIds, true);
        List<UserInNodeBean> usersInNodesList = adhocDao.createBeansFromNamedQry("getUsersInNode", UserInNodeBean.class, nodeId, avatarField);
        Map<String, UserInNodeBean> userMainRolesInNodes = new HashMap<String, UserInNodeBean>();
        Map<String, UserInNodeBean> userAuxiliaryRolesInNodes = new HashMap<String, UserInNodeBean>();
        for (UserInNodeBean ub : usersInNodesList) {
            if (ub.isAuxiliary) {
                userAuxiliaryRolesInNodes.put(ub.roleForNodeId + "_" + ub.userName, ub);
            } else {
                userMainRolesInNodes.put(ub.roleForNodeId + "_" + ub.isAuxiliary, ub);
            }
        }
        for (int i = res.size() - 1; i >= 0; i--) {
            usersInNodesList = adhocDao.createBeansFromNamedQry("getUsersInParentNode", UserInNodeBean.class, res.get(i), avatarField);
            for (UserInNodeBean ub : usersInNodesList) {
                if (ub.isAuxiliary) {
                    userAuxiliaryRolesInNodes.put(ub.roleForNodeId + "_" + ub.userName, ub);
                } else {
                    if (!userMainRolesInNodes.containsKey(ub.roleForNodeId + "_" + ub.isAuxiliary)) {
                        userMainRolesInNodes.put(ub.roleForNodeId + "_" + ub.isAuxiliary, ub);
                    }
                }
            }
        }
        for (UserInNodeBean ub : userMainRolesInNodes.values()) {
            userAuxiliaryRolesInNodes.put(ub.roleForNodeId + "_" + ub.userName, ub);
        }

        Set<Integer> nodeInUsersBranch = new HashSet<Integer>();
        for (Map.Entry<String, UserInNodeBean> e : userAuxiliaryRolesInNodes.entrySet()) {
            UserInNodeBean uinb = e.getValue();
            if (uinb != null) {
                nodeInUsersBranch.addAll(BIKCenterUtils.splitBranchIdsEx(uinb.userInRoleBranchIds, false));
            }
        }
        Map<Integer, String> mapOfNodeNamesByIdsFromUsersBranch = getIdsAndNamesFromBranches(nodeInUsersBranch);

        return new Pair<List<UserInNodeBean>, Map<Integer, String>>(new ArrayList<UserInNodeBean>(userAuxiliaryRolesInNodes.values()), mapOfNodeNamesByIdsFromUsersBranch);
    }

    protected List<JoinedObjBean> getJoinedObjs(int nodeId, Integer linkingParentId) {
        TreeNodeBean node = service.getNodeBeanById(nodeId, false);
        return getRelatedBeansWithDstNodeId("getBikJoinedObjsInner", JoinedObjBean.class, node, linkingParentId,
                //DstNodeIdExtractorForBeanWithDstNodeId.<JoinedObjBean>getInstance()
                //new DstNodeIdExtractorForBeanWithDstNodeId<JoinedObjBean>()
                new IDstNodeIdExtractor<JoinedObjBean>() {
                    @Override
                    public Integer getDstNodeId(JoinedObjBean bean) {

//                        System.out.println("getDstNodeId: bean class=" + BaseUtils.safeGetClassName(bean));
//                        Object dstIdAsObj = bean.dstId;
//                        System.out.println("getDstNodeId: dstId class=" + BaseUtils.safeGetClassName(dstIdAsObj));
                        return bean.dstId;
                    }

                    @Override
                    public String getTreeKind(JoinedObjBean bean) {
                        return bean.treeKind;
                    }
                }, null);
    }

    protected List<JoinedObjBean> getBikJoinedObjsInnerFromHyperlink(int nodeId, Integer linkingParentId) {
        TreeNodeBean node = service.getNodeBeanById(nodeId, false);
        return getRelatedBeansWithDstNodeId("getBikJoinedObjsInnerFromHyperlink", JoinedObjBean.class, node, linkingParentId,
                //DstNodeIdExtractorForBeanWithDstNodeId.<JoinedObjBean>getInstance()
                //new DstNodeIdExtractorForBeanWithDstNodeId<JoinedObjBean>()
                new IDstNodeIdExtractor<JoinedObjBean>() {
                    @Override
                    public Integer getDstNodeId(JoinedObjBean bean) {

//                        System.out.println("getDstNodeId: bean class=" + BaseUtils.safeGetClassName(bean));
//                        Object dstIdAsObj = bean.dstId;
//                        System.out.println("getDstNodeId: dstId class=" + BaseUtils.safeGetClassName(dstIdAsObj));
                        return bean.dstId;
                    }

                    @Override
                    public String getTreeKind(JoinedObjBean bean) {
                        return bean.treeKind;
                    }
                }, null);
    }

    public List<BlogBean> getBlogs(int nodeId, Integer linkingParentId) {
        String avatarField = service.getAvatarFieldForAd();
        TreeNodeBean node = service.getNodeBeanById(nodeId, false);
        return getRelatedBeansWithDstNodeId("bikEntityBlog", BlogBean.class, node, linkingParentId,
                new IDstNodeIdExtractor<BlogBean>() {
                    @Override
                    public Integer getDstNodeId(BlogBean bean) {
                        return bean.nodeId;
                    }

                    @Override
                    public String getTreeKind(BlogBean bean) {
                        return BIKGWTConstants.TREE_KIND_BLOGS;
                    }
                }, avatarField == null ? BIKConstants.DEFAULT_AD_FIELD_NAME : avatarField);
    }

    public List<BlogBean> getUserBlogs(int nodeId) {
        UserBean user = service.bikUserByNodeId(nodeId);
        return adhocDao.createBeansFromNamedQry("getBikUserBlogs", BlogBean.class, user.id);
    }

    public List<AttachmentBean> getAttachments(int nodeId, Integer linkingParentId) {
        //long startTime = System.currentTimeMillis();
        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
            GlobalThreadStopWatch.startProcess("addBeansWithDstNodeIdAndInherited");
        }
        try {
            TreeNodeBean node = service.getNodeBeanById(nodeId, false);
            return getRelatedBeansWithDstNodeId("bikEntityAttachments", AttachmentBean.class, node, linkingParentId,
                    new IDstNodeIdExtractor<AttachmentBean>() {
                        @Override
                        public Integer getDstNodeId(AttachmentBean bean) {
                            return bean.nodeId;
                        }

                        @Override
                        public String getTreeKind(AttachmentBean bean) {
                            return BIKGWTConstants.TREE_KIND_DOCUMENTS;
                        }
                    }, null);
        } finally {
//            long endTime = System.currentTimeMillis();
//            System.out.println("getRelatedAttachment: elapsed time: " + (endTime - startTime) + " millis");
            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.endProcess();
            }
        }
    }

    public List<NoteBean> getNotes(int nodeId) {
        List<NoteBean> notes;
        String avatarField = service.getAvatarFieldForAd();
        UserBean user = service.bikUserByNodeId(nodeId);
        if (user == null) { // normalne komentarze
            notes = adhocDao.createBeansFromNamedQry("bikEntityNotes", NoteBean.class, nodeId, avatarField);
        } else { // moje komentarza na MYBIKS
            notes = adhocDao.createBeansFromNamedQry("bikUserEntityNotes", NoteBean.class, user.id, avatarField);
        }
        return notes;
    }

    protected <T> List<T> getRelatedBeansWithDstNodeId(String queryName,
            Class<T> beanClass, TreeNodeBean node, Integer linkingParentId, IDstNodeIdExtractor<T> extractor, String avatarField) {
        Map<Integer, T> res = new LinkedHashMap<Integer, T>();
        List<T> resDuplicates = new ArrayList<T>();
        addBeansWithDstNodeIdAndInherited(res, resDuplicates, node, node.id, false, queryName, beanClass,
                extractor, avatarField);
        if (linkingParentId != null) {
            TreeNodeBean linkingParentNode = service.getNodeBeanById(linkingParentId, false);
            addBeansWithDstNodeIdAndInherited(res, resDuplicates, linkingParentNode, node.id, true, queryName, beanClass,
                    extractor, avatarField);
        }
//        return new ArrayList<T>(res.values());
        return resDuplicates;

    }

    protected <T> void addBeansWithDstNodeIdAndInherited(
            Map<Integer, T> res, List<T> resDuplicates, TreeNodeBean n, int nodeId, boolean includeInheritedFromThisNode,
            String queryName, Class<T> beanClass,
            IDstNodeIdExtractor<T> extractor, String avatarField) {
        //long startTime = System.currentTimeMillis();
        Map<Integer, Float> similarNodes = service.getOptSimilarNodes(nodeId);
        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
            GlobalThreadStopWatch.startProcess("addBeansWithDstNodeIdAndInherited");
        }
        try {
            List<Integer> ancIds = BIKCenterUtils.splitBranchIdsEx(n.branchIds, true);
//            if (includeInheritedFromThisNode) {
            ancIds.add(n.id);
//            } else {
//                addBeansWithDstNodeIdWithoutDuplicates(res,
//                        avatarField == null ? adhocDao.createBeansFromNamedQry(queryName, beanClass, nodeId, false, similarNodes == null ? null : similarNodes, nodeId)
//                                : adhocDao.createBeansFromNamedQry(queryName, beanClass, nodeId, false, avatarField, similarNodes == null ? null : similarNodes, nodeId),
//                        extractor);
//            }

            if (!ancIds.isEmpty()) {
                List<T> inherited
                        = (avatarField == null ? adhocDao.createBeansFromNamedQry(queryName, beanClass, ancIds, true, similarNodes == null ? null : similarNodes, nodeId)
                                : adhocDao.createBeansFromNamedQry(queryName, beanClass, ancIds, true, avatarField, similarNodes == null ? null : similarNodes, nodeId));
                addBeansWithDstNodeIdWithDuplicatesAndSimilar(res, resDuplicates, inherited, extractor);
                
                /*było  addBeansWithDstNodeIdWithoutDuplicates ale musialy dojsc zduplikowane rekordy z atrybutow typu hyperlink
                */
            }

            //long endTime = System.currentTimeMillis();
            //System.out.println("addBeansWithDstNodeIdAndInherited: elapsed time: " + (endTime - startTime) + " millis");
        } finally {
            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.endProcess();
            }
        }
    }

    protected <T> void addBeansWithDstNodeIdWithoutDuplicates(
            Map<Integer, T> res, Iterable<T> beansToAdd,
            IDstNodeIdExtractor<T> extractor) {
        //long startTime = System.currentTimeMillis();
        Map<String, JoinedObjsTabKindEx> treeKindToJoinedObjsTabKindMap = BIKCenterUtils.makeTreeKindToJoinedObjsTabKindMap(service.getDynamicTreeKindCodes());
        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
            GlobalThreadStopWatch.startProcess("addBeansWithDstNodeIdWithoutDuplicates");
        }
        try {

            int suggestSimilarMaxResults = service.getSuggestSimilarMaxResults();

//            int currentSimilarResults = 0;
            Map<JoinedObjsTabKindEx, Integer> currentSimilarResults = new EnumMap<JoinedObjsTabKindEx, Integer>(JoinedObjsTabKindEx.class);
            for (JoinedObjsTabKindEx jotk : JoinedObjsTabKindEx.values()) {
                currentSimilarResults.put(jotk, 0);
            }

            for (T bean : res.values()) {
                if (bean instanceof IHasSimilarityScore) {
                    IHasSimilarityScore hss = (IHasSimilarityScore) bean;
                    if (hss.getSimilarityScore() != null) {
                        JoinedObjsTabKindEx jotk = treeKindToJoinedObjsTabKindMap.get(extractor.getTreeKind(bean));
                        if (jotk != null) {
//                        currentSimilarResults++;
                            currentSimilarResults.put(jotk, currentSimilarResults.get(jotk) + 1);
                        }
                    }
                }
            }

            for (T beanToAdd : beansToAdd) {

                boolean hasSimilarityScore = false;

                if (beanToAdd instanceof IHasSimilarityScore) {
                    IHasSimilarityScore hss = (IHasSimilarityScore) beanToAdd;
                    hasSimilarityScore = hss.getSimilarityScore() != null;
                }

                Integer dstNodeId = //beanToAdd.getDstNodeId();
                        extractor.getDstNodeId(beanToAdd);

                T prevAddedBean = res.get(dstNodeId);
                boolean prevHasScore = false;

                if (prevAddedBean instanceof IHasSimilarityScore) {
                    IHasSimilarityScore phss = (IHasSimilarityScore) prevAddedBean;
                    prevHasScore = phss.getSimilarityScore() != null;
                }

                if (prevAddedBean == null || prevHasScore) {

                    if (hasSimilarityScore) {

                        JoinedObjsTabKindEx jotk = treeKindToJoinedObjsTabKindMap.get(extractor.getTreeKind(beanToAdd));
                        int csr = jotk == null ? suggestSimilarMaxResults : currentSimilarResults.get(jotk);

                        if (csr >= suggestSimilarMaxResults) {
                            continue;
                        }
                        if (!prevHasScore) {
//                            currentSimilarResults++;
                            currentSimilarResults.put(jotk, csr + 1);
                        }
                    }

                    res.put(dstNodeId, beanToAdd);
                }
                    }
        } finally {
            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.endProcess();
            }
            //long endTime = System.currentTimeMillis();
            //System.out.println("addBeansWithDstNodeIdWithoutDuplicates: elapsed time: " + (endTime - startTime) + " millis");
        }
    }
    
    
    protected <T> void addBeansWithDstNodeIdWithDuplicatesAndSimilar(
            Map<Integer, T> res, List<T> resDuplicates, Iterable<T> beansToAdd,
            IDstNodeIdExtractor<T> extractor) {
        //long startTime = System.currentTimeMillis();
        Map<String, JoinedObjsTabKindEx> treeKindToJoinedObjsTabKindMap = BIKCenterUtils.makeTreeKindToJoinedObjsTabKindMap(service.getDynamicTreeKindCodes());
        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
            GlobalThreadStopWatch.startProcess("addBeansWithDstNodeIdWithoutDuplicates");
        }
        try {

            int suggestSimilarMaxResults = service.getSuggestSimilarMaxResults();

//            int currentSimilarResults = 0;
            Map<JoinedObjsTabKindEx, Integer> currentSimilarResults = new EnumMap<JoinedObjsTabKindEx, Integer>(JoinedObjsTabKindEx.class);
            for (JoinedObjsTabKindEx jotk : JoinedObjsTabKindEx.values()) {
                currentSimilarResults.put(jotk, 0);
            }

            for (T bean : res.values()) {
                if (bean instanceof IHasSimilarityScore) {
                    IHasSimilarityScore hss = (IHasSimilarityScore) bean;
                    if (hss.getSimilarityScore() != null) {
                        JoinedObjsTabKindEx jotk = treeKindToJoinedObjsTabKindMap.get(extractor.getTreeKind(bean));
                        if (jotk != null) {
//                        currentSimilarResults++;
                            currentSimilarResults.put(jotk, currentSimilarResults.get(jotk) + 1);
                        }
                    }
                }
            }

            for (T beanToAdd : beansToAdd) {

                boolean hasSimilarityScore = false;

                if (beanToAdd instanceof IHasSimilarityScore) {
                    IHasSimilarityScore hss = (IHasSimilarityScore) beanToAdd;
                    hasSimilarityScore = hss.getSimilarityScore() != null;
                }

                Integer dstNodeId = //beanToAdd.getDstNodeId();
                        extractor.getDstNodeId(beanToAdd);

                T prevAddedBean = res.get(dstNodeId);
                boolean prevHasScore = false;

                if (prevAddedBean instanceof IHasSimilarityScore) {
                    IHasSimilarityScore phss = (IHasSimilarityScore) prevAddedBean;
                    prevHasScore = phss.getSimilarityScore() != null;
                }

                if (prevAddedBean == null ||!hasSimilarityScore/* || prevHasScore*/) {

                    if (hasSimilarityScore) {

                        JoinedObjsTabKindEx jotk = treeKindToJoinedObjsTabKindMap.get(extractor.getTreeKind(beanToAdd));
                        int csr = jotk == null ? suggestSimilarMaxResults : currentSimilarResults.get(jotk);

                        if (csr >= suggestSimilarMaxResults) {
                            continue;
                        }
                        if (!prevHasScore) {
//                            currentSimilarResults++;
                            currentSimilarResults.put(jotk, csr + 1);
                        }
                    }

                    res.put(dstNodeId, beanToAdd);
                    resDuplicates.add(beanToAdd);
                }
            }
        } finally {
            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.endProcess();
            }
            //long endTime = System.currentTimeMillis();
            //System.out.println("addBeansWithDstNodeIdWithoutDuplicates: elapsed time: " + (endTime - startTime) + " millis");
        }
    }
    

    protected Map<Integer, String> getIdsAndNamesFromBranches(Set<Integer> idsFromBranches) {
        Map<Integer, String> mapOfNodeNamesByIdsFromUsersBranch = new LinkedHashMap<Integer, String>();
        List<Map<String, Object>> nodeIdsAndNamesFromUsersBranch;
        idsFromBranches.toArray();
        if (!idsFromBranches.isEmpty()) {
            nodeIdsAndNamesFromUsersBranch = adhocDao.execNamedQueryCFN("getIdsAndNames",
                    FieldNameConversion.ToJavaPropName, idsFromBranches);
            for (Map<String, Object> m : nodeIdsAndNamesFromUsersBranch) {
                mapOfNodeNamesByIdsFromUsersBranch.put((Integer) m.get("id" /* I18N: no */), (String) m.get("name" /* I18N: no */));
            }
        }
        return mapOfNodeNamesByIdsFromUsersBranch;
    }
}
