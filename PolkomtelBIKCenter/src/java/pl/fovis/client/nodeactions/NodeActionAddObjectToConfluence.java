/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.client.dialogs.ConfluenceLoginDialog;
import pl.fovis.client.dialogs.SimplyDefinitionDialog;
import pl.fovis.client.dialogs.TreeSingleSelectorForConfluenceDialog;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class NodeActionAddObjectToConfluence extends NodeActionConfluenceBase {

    public String getCaption() {
        return I18n.publikujArtykulDoConfluence.get();
    }

    public void execute() {
        new SimplyDefinitionDialog().buildAndShowDialog(null, new IParametrizedContinuation<Pair<String, String>>() {
            public void doIt(final Pair<String, String> article) {
                new TreeSingleSelectorForConfluenceDialog().buildAndShowDialogForSingleMode(new IParametrizedContinuation<ConfluenceObjectBean>() {
                    public void doIt(final ConfluenceObjectBean selectedNode) {
                        final ConfluenceLoginDialog loginDialog = new ConfluenceLoginDialog();
                        loginDialog.buildAndShowDialog(BIKClientSingletons.getLoggedUserBean().loginName, new IParametrizedContinuation<Pair<String, String>>() {
                            public void doIt(Pair<String, String> login) {
                                final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                                infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.publikowanieArtykuluDoConfluenceProszeCzekac.get(), true);
                                BIKClientSingletons.getService().publishArticleToConfluence(
                                        BaseUtils.splitString(Window.Location.getHref(), "#").v1,
                                        article.v1, article.v2, login.v1, login.v2, selectedNode.id, selectedNode.space, node != null ? node.id : null, treeBean.id, new AsyncCallback<Void>() {
                                            public void onFailure(Throwable caught) {
                                                infoDialog.hideDialog();
                                                BIKClientSingletons.showError(I18n.nieprawidlowyLoginAbyOpublikowacArtykul.get());
                                            }

                                            public void onSuccess(Void result) {
                                                infoDialog.hideDialog();
                                                loginDialog.hideDialog();
                                                BIKClientSingletons.showInfo(I18n.opublikowanoArtykul.get());
                                                if (node == null) {
                                                    refreshAndRedisplayData();
                                                } else {
                                                    refreshRedisplayDataAndSelectRecord(node.id);
                                                }
                                            }
                                        });
                            }
                        });
                    }
                });
            }
        });
    }
}
