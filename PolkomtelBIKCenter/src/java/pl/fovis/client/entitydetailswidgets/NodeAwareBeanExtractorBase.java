/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.EntityDetailsPaneDialog;
import pl.fovis.common.JoinedObjMode;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author beata
 * @param <T>
 */
public abstract class NodeAwareBeanExtractorBase<T> extends GenericBeanExtractorBase<T> implements IGridNodeAwareBeanPropsExtractor<T> {

    boolean isUsingOnDialog;
    protected boolean isDescriptionVisible = false;
    public static final int MAX_LENGTH_OF_DISPLAYED_DESCRIPTION = 200;

    protected IGridBeanAction<T> actView = new SimpleGridBeanAction<T>() {
        public String getActionIconUrl(T b) {
            Integer nodeId = getNodeId(b);
            if (nodeId == null) {
                return null;
            }
            return getIconUrlByIconName("loupe" /* I18N: no */);
        }

        public String getActionIconHint(T b) {
            return I18n.podglad.get() /* I18N:  */;
        }

        public void executeAction(T b) {
            execActView(b);
        }

        public boolean isVisible(T b) {
            return !isUsingOnDialog;
//            return true;
        }

        public boolean isEnabled(T b) {
            return true;
        }

        public IGridBeanAction.TypeOfAction getActionType() {
            return IGridBeanAction.TypeOfAction.View;
        }
    };
    protected IGridBeanAction<T> actFollow = new SimpleGridBeanAction<T>() {
        public String getActionIconUrl(T b) {
            Integer nodeId = getNodeId(b);
            if (nodeId == null) {
                return null;
            }
            return getIconUrlByIconName("link_arrows");
        }

        public String getActionIconHint(T b) {
            return I18n.przejdz.get() /* I18N:  */;
        }

        public void executeAction(T b) {
            //BaseActionOrCancelDialog.hideAllDialogs();
            execActFollow(b);
        }

        public boolean isVisible(T b) {
            return true;
        }

        public boolean isEnabled(T b) {
            return true;
        }

        public IGridBeanAction.TypeOfAction getActionType() {
            return IGridBeanAction.TypeOfAction.Follow;
        }
    };
    protected IGridBeanAction<T> nodeAwareActEdit = new SimpleGridBeanAction<T>() {
        public String getActionIconUrl(T b) {
            Integer nodeId = getNodeId(b);
            if (nodeId == null) {
                return null;
            }
            return getIconUrlByIconName("edit_gray");
        }

        public String getActionIconHint(T b) {
            return I18n.edytuj.get() /* I18N:  */;
        }

        public void executeAction(T b) {
            execActEdit(b);
        }

        public boolean isEnabled(T b) {
            return isActEditBtnEnabled(b);
//            return isEffectiveExpertLoggedInEx(b);
//            return BIKClientSingletons.isEffectiveExpertLoggedIn();
        }

        public boolean isVisible(T b) {
            return true;
        }

        public IGridBeanAction.TypeOfAction getActionType() {
            return IGridBeanAction.TypeOfAction.Edit;
        }
    };

    public NodeAwareBeanExtractorBase(boolean isAddJoinedButtonEnabled) {
        super(isAddJoinedButtonEnabled);
    }

    @Override
    protected List<IGridBeanAction<T>> createActionList() {
        List<IGridBeanAction<T>> res = new ArrayList<IGridBeanAction<T>>();
//        if (BIKClientSingletons.enableAttributeJoinedObjs()) {
//            res.add(actEditAttr);
//        }
        res.add(actView);
        res.add(actFollow);
        res.add(actDelete);
        return res;
    }

    protected void execActView(T b) {
        EntityDetailsPaneDialog edpd = new EntityDetailsPaneDialog();
        edpd.buildAndShowDialog(getNodeId(b), null, getTreeCode(b));
    }

    protected void execActFollow(T b) {
        BIKClientSingletons.getTabSelector().submitNewTabSelection(getTreeCode(b), getNodeId(b), true);
    }

    protected abstract String getNodeKindCode(T b);

//    protected abstract String getTreeCode(T b);
    @Override
    public String getIconUrl(T b) {
        return getIconUrlByNodeKindCode(getNodeKindCode(b), getTreeCode(b));
    }

    @Override
    public String getOptIconHint(T b) {
        String nodeKindCode = getNodeKindCode(b);
        return nodeKindCode != null ? BIKClientSingletons.getNodeKindCaptionByCode(nodeKindCode) : null;
    }

    public static String getIconUrlByNodeKindCode(String nodeKindCode, String treeCode) {
        return getIconUrlByIconName(BIKClientSingletons.getNodeKindIconNameByCode(nodeKindCode, treeCode));
    }

//
//    public String getActionIconUrl(T b, int idx) {
//        return canExecuteAction(b, idx) ? getIconUrlByIconName(iconByIdx.get(idx)) : null;
//    }
//
//    public String getActionIconHint(T b, int idx) {
//        return iconHintByIdx.get(idx);
//    }
    public boolean isRowVisible(T b, int addColIdx) {
        return true;
    }

//    protected String getStarIcon(boolean isStarredUser) {
//        return (isStarredUser ? " <img src=\"images/star.png\" style=\"width: 12px; height: 12px;\">" : "");
//    }
    protected String getNameWithOptStarAsHtml(String name, boolean addStar) {
        return BaseUtils.encodeForHTMLTag(name) + (addStar ? " <img src=\"images/star.png\" style=\"width: 12px; height: 12px;\">" : "");
    }

    public boolean hasChildren(T b) {
        return false;
    }

    public boolean hasNameHints() {
        return true;
    }

    public int howManyTimesNameColumnWiderThanOther() {
        return 2;
    }

    protected boolean hasInheritedMarkByName(T b) {
        return false;
    }

    @Override
    public String getOptExtraStyleForRow(T b) {
        return getOptSimilarityScore(b) == null ? null : "similarObject";
    }

    @Override
    public String getNameAsHtml(T b) {
        String res = super.getNameAsHtml(b);
        if (hasInheritedMarkByName(b)) {
            res = res + " *";
        }

        Double optSimilarityScore = getOptSimilarityScore(b);

        if (optSimilarityScore != null) {
            res += " [≈" + BaseUtils.doubleToString(optSimilarityScore, 4, false, true) + "]";
        }

        return res;
    }

    public void setUsingOnDialog(boolean isWidgetRunFromDialog) {
        isUsingOnDialog = isWidgetRunFromDialog;
    }

    @Override
    public boolean isDeleteBtnVisible(T b) {
        return !isUsingOnDialog;
    }

    public String getDescription(String descr) {
        return BaseUtils.getTrimStrToLength(descr, MAX_LENGTH_OF_DISPLAYED_DESCRIPTION);
    }

    public boolean isActEditBtnEnabled(T b) {
        return isEffectiveExpertLoggedInEx(b);
    }

    protected Integer getCurrentNodeId() {
        return null;
    }

    @Override
    protected void execActJoinSimilarNode(T b) {
        BIKClientSingletons.showInfo(I18n.zapisywaniePowiazanProszeCzekac.get());

        Map<Integer, JoinedObjMode> idsToJoin = new HashMap<Integer, JoinedObjMode>();
        idsToJoin.put(getNodeId(b), JoinedObjMode.Single);

        BIKClientSingletons.getService().updateJoinedObjsForNode(getCurrentNodeId(), false, idsToJoin,
                new HashMap<Integer, Pair<JoinedObjMode, Boolean>>(), getRefreshingCallback(getCurrentNodeId()));
    }

    @Override
    public Integer getJoinedObjId(T b) {
        return null;
    }
}
