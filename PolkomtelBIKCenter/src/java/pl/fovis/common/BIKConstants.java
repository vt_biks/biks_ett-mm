/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.util.HashMap;
import java.util.Map;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.trzy0.foxy.commons.FoxyCommonConsts;
import simplelib.i18nsupport.I18nMessage;

/**
 *
 * @author pmielanczuk
 */
// i18n-default: no
public class BIKConstants extends PumpConstants implements FoxyCommonConsts {

    // !!!!!!!!!!!!!!!!!!! ********** !!!!!!!!!!!!!!!!!!!
    // !!!!!!!!!!!!!!!!!!! ********** !!!!!!!!!!!!!!!!!!!
    // UWAGA: dla stałych działających tylko w GWT jest teraz wydzielona klasa
    // BIKGWTConstants
    // !!!!!!!!!!!!!!!!!!! ********** !!!!!!!!!!!!!!!!!!!
    // !!!!!!!!!!!!!!!!!!! ********** !!!!!!!!!!!!!!!!!!!
    //ww: główna wersja - tą zmieniamy... nie podajemy tutaj literki v!!!s
    public static final String APP_VERSION_MAJOR = "2.0.12";
    public static final String SVN_REVISION = "10949"; //ww: <---- to się samo zmienia
    //---
    public static final String APP_VERSION = APP_VERSION_MAJOR + MAJOR_MINOR_VERSION_SEP + SVN_REVISION;
    //---
    public static final String TRANSLATION_KIND_ADEF = "adef";

    public static final String TRANSLATION_KIND_ADCAT = "adcat";
    //---
    public static final String NODE_KIND_REPORT_FOLDER = "ReportFolder";
    public static final String NODE_KIND_UNIVERSE_FOLDER = "UniversesFolder";
    public static final String NODE_KIND_CONNECTION_FOLDER = "ConnectionFolder";
    public static final String NODE_KIND_CONNECTION_ENGINE_FOLDER = "ConnectionEngineFolder";
    public static final String NODE_KIND_CONNECTION_NETWORK_FOLDER = "ConnectionNetworkFolder";
    public static final String NODE_KIND_UNIVERSE_TABLES_FOLDER = "UniverseTablesFolder";
    public static final String NODE_KIND_DOCUMENTS_FOLDER = "DocumentsFolder";
    public static final String NODE_KIND_WEBI = "Webi";
    public static final String NODE_KIND_FLASH = "Flash";
    public static final String NODE_KIND_EXCEL = "Excel";
    public static final String NODE_KIND_PDF = "Pdf";
    public static final String NODE_KIND_WORD = "Word";
    public static final String NODE_KIND_FULL_CLIENT = "FullClient";
    public static final String NODE_KIND_HYPERLINK = "Hyperlink";
    public static final String NODE_KIND_TXT = "Txt";
    public static final String NODE_KIND_POWERPOINT = "Powerpoint";
    public static final String NODE_KIND_RTF = "Rtf";
    public static final String NODE_KIND_MEASURE = "Measure";
    public static final String NODE_KIND_DIMENSION = "Dimension";
    public static final String NODE_KIND_DETAIL = "Detail";
    public static final String NODE_KIND_ATTRIBUTE = "Attribute";
    public static final String NODE_KIND_FILTER = "Filter";
    public static final String NODE_KIND_CRYSTAL_REPORTS = "CrystalReport";
    public static final String NODE_KIND_UNIVERSE = "Universe";
    public static final String NODE_KIND_UNIVERSE_UNX = "DSL.MetaDataFile";
    public static final String NODE_KIND_DATACONNECTION = "DataConnection";
    public static final String NODE_KIND_CONNECTION_OLAP = "CommonConnection";
    public static final String NODE_KIND_CONNECTION_RELATIONAL = "CCIS.DataConnection";
    public static final String NODE_KIND_REPORT_QUERY = "ReportQuery";
    public static final String NODE_KIND_REPORT_SCHEDULE = "ReportSchedule";
    public static final String NODE_KIND_UNIVERSE_CLASS = "UniverseClass";
    public static final String NODE_KIND_UNIVERSE_TABLE = "UniverseTable";
    public static final String NODE_KIND_UNIVERSE_TABLE_ALIAS = "UniverseAliasTable";
    public static final String NODE_KIND_UNIVERSE_TABLE_DERIVED = "UniverseDerivedTable";
    public static final String NODE_KIND_UNIVERSE_COLUMN = "UniverseColumn";
    public static final String NODE_KIND_TERADATA_OWNER = "TeradataOwner";
    public static final String NODE_KIND_TERADATA_SCHEMA = "TeradataSchema";
    public static final String NODE_KIND_TERADATA_TABLE = "TeradataTable";
    public static final String NODE_KIND_TERADATA_COLUMN = "TeradataColumn";
    public static final String NODE_KIND_TERADATA_COLUMN_PK = "TeradataColumnPK";
    public static final String NODE_KIND_TERADATA_COLUMN_IDX = "TeradataColumnIDX";
    public static final String NODE_KIND_TERADATA_VIEW = "TeradataView";
    public static final String NODE_KIND_TERADATA_PROCEDURE = "TeradataProcedure";
    public static final String NODE_KIND_TERADATA_AREA = "TeradataArea";
    public static final String NODE_KIND_TERADATA_SERVER = "TeradataServer";
    public static final String NODE_KIND_DQC_TEST_SUCCESS = "DQCTestSuccess";
    public static final String NODE_KIND_DQC_TEST_FAILED = "DQCTestFailed";
    public static final String NODE_KIND_DQC_TEST_INACTIVE = "DQCTestInactive";
    public static final String NODE_KIND_DQC_GROUP = "DQCGroup";
    public static final String NODE_KIND_DQC_ALL_TESTS_FOLDER = "DQCAllTestsFolder";
    public static final String NODE_KIND_DQM_TEST_SUCCESS = "DQMTestSuccess";
    public static final String NODE_KIND_DQM_TEST_FAILED = "DQMTestFailed";
    public static final String NODE_KIND_DQM_TEST_INACTIVE = "DQMTestInactive";
    public static final String NODE_KIND_DQM_TEST_NOT_STARTED = "DQMTestNotStarted";
    public static final String NODE_KIND_DQM_GROUP = "DQMGroup";
    public static final String NODE_KIND_DQM_FOLDER_RATE = "DQMFolderRate";
    public static final String NODE_KIND_DQM_FOLDER_COORDINATION = "DQMFolderCoordination";
    public static final String NODE_KIND_DQM_ERROR_REGISTER = "DQMErrorRegister";
    public static final String NODE_KIND_DQM_ERROR_OPENED = "DQMOpenedDataErrorIssue";
    public static final String NODE_KIND_DQM_ERROR_CLOSED = "DQMClosedDataErrorIssue";
    public static final String NODE_KIND_DQM_REPORT = "DQMReport";
    public static final String NODE_KIND_DQM_RATE = "DQMRate";
    public static final String NODE_KIND_DQM_RATE_ACCEPTED = "DQMRateAccepted";
    public static final String NODE_KIND_DQM_RATE_REJECTED = "DQMRateRejected";
//    public static final String NODE_KIND_DQM_ALL_TESTS_FOLDER = "DQMAllTestsFolder"; // no need anymore...
//    public static final String NODE_KIND_TAXONOMY_ENTITY = "TaxonomyEntity"; // nie używany - nie odnosimy się do tego kinda bezpośrednio bo jest on już "dynamiczny"
    public static final String NODE_KIND_SYSTEM = "System";
    public static final String NODE_KIND_SYSTEM_GROUP = "SystemGroup";
    public static final String NODE_KIND_APPLICATION = "Application";
    public static final String NODE_KIND_GLOSSARY = "Glossary";
    public static final String NODE_KIND_GLOSSARY_CATEGORY = "GlossaryCategory";
    public static final String NODE_KIND_GLOSSARY_VERSION = "GlossaryVersion";
    public static final String NODE_KIND_GLOSSARY_NOT_CORPO = "GlossaryNotCorpo";
    public static final String NODE_KIND_KEYWORD = "Keyword";
    public static final String NODE_KIND_DOCUMENT = "Document";
    public static final String NODE_KIND_USERS_GROUP = "UsersGroup";
    public static final String NODE_KIND_USER = "User";
    public static final String NODE_KIND_BLOG = "Blog";
    public static final String NODE_KIND_BLOG_ENTRY = "BlogEntry";
    public static final String NODE_KIND_ALL_USERS_FOLDER = "AllUsersFolder";
    public static final String NODE_KIND_KEYWORD_FOLDER = "KeywordFolder";
    public static final String NODE_KIND_ARTICLE_FOLDER = "ArticleFolder";
    public static final String NODE_KIND_ARTICLE = "Article";
    public static final String NODE_KIND_MSSQL_DATABASE = "MSSQLDatabase";
//    public static final String NODE_KIND_MSSQL_OWNER = "MSSQLOwner";
    public static final String NODE_KIND_MSSQL_TABLE = "MSSQLTable";
    public static final String NODE_KIND_MSSQL_VIEW = "MSSQLView";
    public static final String NODE_KIND_MSSQL_PROCEDURE = "MSSQLProcedure";
    public static final String NODE_KIND_MSSQL_FUNCTION = "MSSQLFunction";
    public static final String NODE_KIND_MSSQL_TABLE_FUNCTION = "MSSQLTableFunction";
    public static final String NODE_KIND_MSSQL_COLUMN = "MSSQLColumn";
    public static final String NODE_KIND_MSSQL_COLUMN_PK = "MSSQLColumnPK";
    public static final String NODE_KIND_MSSQL_COLUMN_FK = "MSSQLColumnFK";
    public static final String NODE_KIND_MSSQL_SERVER = "MSSQLServer";
    public static final String NODE_KIND_MSSQL_FOLDER = "MSSQLFolder";
    public static final String NODE_KIND_POSTGRES_SERVER = "PostgresServer";
    public static final String NODE_KIND_POSTGRES_DATABASE = "PostgresDatabase";
    public static final String NODE_KIND_POSTGRES_SCHEMA = "PostgresSchema";
    public static final String NODE_KIND_POSTGRES_TABLE = "PostgresTable";
    public static final String NODE_KIND_POSTGRES_VIEW = "PostgresView";
    public static final String NODE_KIND_POSTGRES_FUNCTION = "PostgresFunction";
    public static final String NODE_KIND_POSTGRES_COLUMN = "PostgresColumn";
    public static final String NODE_KIND_POSTGRES_COLUMN_PK = "PostgresColumnPK";
    public static final String NODE_KIND_POSTGRES_COLUMN_FK = "PostgresColumnFK";
    public static final String NODE_KIND_POSTGRES_FOLDER = "PostgresFolder";
    public static final String NODE_KIND_ORACLE_SERVER = "OracleServer";
    public static final String NODE_KIND_ORACLE_OWNER = "OracleOwner";
    public static final String NODE_KIND_ORACLE_TABLE = "OracleTable";
    public static final String NODE_KIND_ORACLE_VIEW = "OracleView";
    public static final String NODE_KIND_ORACLE_FOLDER = "OracleFolder";
    public static final String NODE_KIND_ORACLE_PROCEDURE = "OracleProcedure";
    public static final String NODE_KIND_ORACLE_PACKAGE = "OraclePackage";
    public static final String NODE_KIND_ORACLE_FUNCTION = "OracleFunction";
    public static final String NODE_KIND_ORACLE_COLUMN = "OracleColumn";
    public static final String NODE_KIND_ORACLE_COLUMN_PK = "OracleColumnPK";
    public static final String NODE_KIND_ORACLE_COLUMN_FK = "OracleColumnFK";
    public static final String NODE_KIND_SAS_REPORT_FOLDER = "SASReportFolder";
    public static final String NODE_KIND_SAS_WRS = "SASWrs";
    public static final String NODE_KIND_SAS_INFORMATION_MAP_FOLDER = "SASInformationMapFolder";
    public static final String NODE_KIND_SAS_INFORMATION_MAP = "SASInformationMap";
    public static final String NODE_KIND_SAS_MEASURE = "SASMeasure";
    public static final String NODE_KIND_SAS_CATEGORY = "SASCategory";
    public static final String NODE_KIND_SAS_FILTER = "SASFilter";
    public static final String NODE_KIND_SAS_LIBRARY_FOLDER = "SASLibraryFolder";
    public static final String NODE_KIND_SAS_LIBRARY = "SASLibrary";
    public static final String NODE_KIND_SAS_LIBRARY_TABLE = "SASLibraryTable";
    public static final String NODE_KIND_SAS_LIBRARY_COLUMN_CHAR = "SASLibraryColumnChar";
    public static final String NODE_KIND_SAS_LIBRARY_COLUMN_NUM = "SASLibraryColumnNum";
    public static final String NODE_KIND_SAS_LIBRARY_COLUMN_DATE = "SASLibraryColumnDate";
    public static final String NODE_KIND_SAS_FOLDER = "SASFolder";
    public static final String NODE_KIND_SAS_CUBE_DIMENSION = "SASCubeDimension";
    public static final String NODE_KIND_SAS_CALCULATED_MEASURE = "SASCalculatedMeasure";
    public static final String NODE_KIND_SAS_CUBE_MEASURES = "SASCubeMeasures";
    public static final String NODE_KIND_SAS_LEVEL = "SASLevel";
    public static final String NODE_KIND_SAS_HIERARCHY = "SASHierarchy";
    public static final String NODE_KIND_SAS_INFORMATION_MAP_CUBE = "SASInformationMapCube";
    public static final String NODE_KIND_SAS_CUBE = "SASCube";
    public static final String NODE_KIND_SAS_DASHBOARD = "SASDashboard";
    public static final String NODE_KIND_SAS_CUBE_FOLDER = "SASCubeFolder";
    public static final String NODE_KIND_USERS_ROLE = "UserRole";
    public static final String NODE_KIND_SAPBW_OBJECTS_FOLDER = "BWObjsFolder";
    public static final String NODE_KIND_SAPBW_AREA = "BWArea";
    public static final String NODE_KIND_SAPBW_BEX = "BWBEx";
    public static final String NODE_KIND_SAPBW_MPRO = "BWMPRO";
    public static final String NODE_KIND_SAPBW_CUBE = "BWCUBE";
    public static final String NODE_KIND_SAPBW_ODSO = "BWODSO";
    public static final String NODE_KIND_SAPBW_ISET = "BWISET";
    public static final String NODE_KIND_SAPBW_UNI = "BWUni";
    public static final String NODE_KIND_SAPBW_TIM = "BWTim";
    public static final String NODE_KIND_SAPBW_KYF = "BWKyf";
    public static final String NODE_KIND_SAPBW_DPA = "BWDpa";
    public static final String NODE_KIND_SAPBW_CHA = "BWCha";
    public static final String NODE_KIND_CONFLUENCE_FOLDER = "ConfluenceFolder";
    public static final String NODE_KIND_CONFLUENCE_SPACE = "ConfluenceSpace";
    public static final String NODE_KIND_CONFLUENCE_PAGE = "ConfluencePage";
    public static final String NODE_KIND_CONFLUENCE_BLOG = "ConfluenceBlog";
    public static final String NODE_KIND_LISA_DICTIONARIES = "DWHDictionaries";
    public static final String NODE_KIND_LISA_DICTIONARY = "DWHDictionary";
    public static final String NODE_KIND_LISA_CATEGORIZATION = "DWHCategorization";
    public static final String NODE_KIND_LISA_CATEGORY = "DWHCategory";
    public static final String NODE_KIND_LISA_FOLDER = "TeradataFolder";
    public static final String NODE_KIND_PROFILE_LIBRARY = "ProfileLibrary";
    public static final String NODE_KIND_PROFILE_FILE = "ProfileFile";
    public static final String NODE_KIND_PROFILE_ITEM = "ProfileDataItem";
    public static final String NODE_KIND_RAW_FILE = "RawFile";
    public static final String NODE_KIND_META_MENU_GROUP = "metaBiksMenuGroup";
    public static final String NODE_KIND_META_MENU_ITEM = "metaBiksMenuItem";
    public static final String NODE_KIND_META_MENU_TREE_TEMPLATE = "metaBiksMenuTreeTemplate";
    public static final String NODE_KIND_META_DEFINITION_GROUP = "metaBiksDefinitionGroup";
    public static final String NODE_KIND_META_ATTRIBUTES_CATALOG = "metaBiksAttributesCatalog";
    public static final String NODE_KIND_META_ATTRIBUTE_TYPE = "metaBiksAttributeDef";
    public static final String NODE_KIND_META_ATTR_4_NODE_KIND = "metaBiksAttribute4NodeKind";
    public static final String NODE_KIND_META_NODE_KIND = "metaBiksNodeKind";
    public static final String NODE_KIND_META_BUILT_IN_NODE_KIND = "metaBiksBuiltInNodeKind";
    public static final String NODE_KIND_META_TREE_KIND = "metaBiksTreeKind";
    public static final String NODE_KIND_META_NODE_KIND_4_TREE_KIND = "metaBiksNodeKind4TreeKind";
    public static final String NODE_KIND_META_RELATED_NODE_KIND = "metaBiksNodeKindInRelation";
    public static final String NODE_KIND_META_MENU_CONF_MAIN_MENU = "metaBiksMainMenu";
    public static final String METABIKS_ATTRIBUTE_ASSOC_ROLE = "metaBiks.Przypisana rola";

    public static final String NODE_KIND_META_PRINTS_TEMPLATES = "metaBiksPrintsTemplates";
    public static final String NODE_KIND_META_PRINT_ATTRIBUT = "metaBiksPrintAttribut";
    public static final String NODE_KIND_META_PRINT = "metaBiksPrint";
    public static final String NODE_KIND_META_PRINT_KIND = "metaBiksPrintKind";
    public static final String NODE_KIND_META_PRINT_BUILT_IN_KIND = "metaBiksPrintBuiltInKind";

    //---
    public static final String TREE_CODE_TERADATADATAMODEL = "TeradataDataModel";
    public static final String TREE_CODE_TERADATA = "Teradata";
    public static final String TREE_CODE_DQC = "DQC";
    public static final String TREE_CODE_DQM = "DQM";
    public static final String TREE_CODE_DQM_DOKUMENTACJA = "DQMDokumentacja";
    public static final String TREE_CODE_ORACLE = "Oracle";
    public static final String TREE_CODE_POSTGRES = "PostgreSQL";
    public static final String TREE_CODE_MSSQL = "MSSQL";
    public static final String TREE_CODE_JDBC = "JDBC";
    public static final String TREE_CODE_PROFILE = "Profile";
//    public static final String TREE_CODE_GLOSSARY = "Glossary";
//    public static final String TREE_CODE_SHORTCUTS_GLOSSARY = "ShortcutsGlossary";
//    public static final String TREE_CODE_BUSINESS_GLOSSARY = "BusinessGlossary";
    public static final String TREE_CODE_DOCUMENTS = "Documents";
    public static final String TREE_CODE_USERS = "Users";
//    public static final String TREE_CODE_BLOGS = "Blogs";
//    public static final String TREE_CODE_ARTICLES = "Articles";
    public static final String TREE_CODE_TREEOFTREES = "TreeOfTrees";
    public static final String TREE_CODE_SAS_REPORTS = "SASReports";
    public static final String TREE_CODE_SAS_INFORMATION_MAPS = "SASInformationMaps";
    public static final String TREE_CODE_SAS_LIBRARIES = "SASLibraries";
    public static final String TREE_CODE_SAS_CUBES = "SASCubes";
    public static final String TREE_CODE_USER_ROLES = "UserRoles";
    public static final String TREE_CODE_SAPBW_REPORTS = "BWReports";
    public static final String TREE_CODE_SAPBW_PROVIDERS = "BWProviders";
//    public static final String TREE_CODE_CONFLUENCE = "AtlassianConfluence";
    public static final String TREE_CODE_DICTIONARIES_DWH = "DictionaryDWH";
    public static final String TREE_CODE_FILE_SYSTEM = "FileSystem";
    //---
    public static final String TREE_KIND_DYNAMIC_TREE = "dtk_";
    public static final String TREE_KIND_METADATA = "Metadata";
    public static final String TREE_KIND_META_BIKS = "metaBiks";
    public static final String TREE_KIND_META_BIKS_MENU_CONF = "metaBiksMenuConf";
//    public static final String TREE_KIND_TAXONOMY = "Taxonomy"; // zastąpiony Dynamic Tree
    public static final String TREE_KIND_GLOSSARY = "Glossary";
    public static final String TREE_KIND_DICTIONARY = "Dictionary";
    public static final String TREE_KIND_DICTIONARY_DWH = "DictionaryDWH";
    public static final String TREE_KIND_DOCUMENTS = "Documents";
    public static final String TREE_KIND_USERS = "Users";
    public static final String TREE_KIND_CONFLUENCE = "Confluence";
    //ww: nowe od v1.1.9.17
    public static final String TREE_KIND_USER_ROLES = "UserRoles";
    public static final String TREE_KIND_BLOGS = "Blogs";
//    public static final String TREE_KIND_ARTICLES = "Articles";
    public static final String TREE_KIND_QUALITY_METADATA = "QualityMetadata";
    //---
    public static final String APP_PROP_MY_OBJECTS_TREE_CODE = "myObjectsTree";
    public static final String APP_PROP_MY_OBJECTS_NODE_KINDS = "myObjectsNodeKinds";
    public static final String APP_PROP_MY_OBJECTS_ROLES = "myObjectsInRole";
    public static final String APP_PROP_MY_OBJECTS_ATTRIBUTES = "myObjectsAttributes";
    public static final String APP_PROP_DISABLE_BUILT_IN_ROLES = "disableBuiltInRoles";

//    public static final String APP_PROP_ADDITIONAL_DOCUMENT_KINDS = "additionalDocumentKinds";
    //---
    public static final String MYBIKSTAB_SHORT_CODE_NEWS = "n";
    public static final String MYBIKSTAB_SHORT_CODE_FAVOURITES = "f";
    //---
    public static final String BUILD_FOLDER_TABLE = "Tabele";
    public static final String BUILD_FOLDER_VIEW = "Widoki";
    public static final String BUILD_FOLDER_PROCEDURE = "Procedury";
    public static final String BUILD_FOLDER_PACKAGES = "Pakiety";
    public static final String BUILD_FOLDER_FUNCTION = "Funkcje";
    public static final String BUILD_FOLDER_SCALAR_FUNCTION = "Funkcje skalarne";
    public static final String BUILD_FOLDER_TABLE_FUNCTION = "Funkcje tabelaryczne";
    //---
    public static final String DEFAULT_AD_FIELD_NAME = "url";
    public static final Map<String, Object> SPECIAL_NODE_KIND_TRANSLATIONS = new HashMap<String, Object>();
    public static final Map<String, I18nMessage> SPECIAL_MSSQL_NODE_KIND_TRANSLATIONS = new HashMap<String, I18nMessage>();
    //---
    public static final String TREE_EXPORT_PARAM_NAME = "treeexportresults";
    public static final String TREE_EXPORT_ACTION_PARAM_NAME = "treeexportact";
    public static final String TREE_EXPORT_BINDING_PARAM_NAME = "bindingtreeid";
    public static final String TREE_EXPORT_DEPTH_PARAM_NAME = "depth";
    public static final String TREE_EXPORT_NUMBERING_PARAM_NAME = "numbering";
    public static final String TREE_EXPORT_ACTION_LINKED_ASSOCIATIONS = "linkedasscociations";
    public static final String TREE_EXPORT_ACTION_CREATE_DOCUMENT = "createdocument";
    public static final String SEARCH_RESULTS_EXPORT_PARAM_NAME = "searchResultsExport";
    public static final String SEARCH_REQUEST_TICKET_PARAM_NAME = "searchRequestTicket";
    public static final String EXPORT_OPTIONS_PARAM_NAME = "options";
    public static final String EXPORT_OPTIONS_NAME = "name";
    public static final String EXPORT_OPTIONS_NODE_KIND = "nodeKind";
    public static final String EXPORT_OPTIONS_DESCR = "descr";
    public static final String EXPORT_OPTIONS_OBJ_PATH = "objPath";
    public static final String EXPORT_OPTIONS_EVALUATION = "evaluation";
    public static final String EXPORT_OPTIONS_APPEARANCE = "appearance";
    public static final String TREE_EXPORT_TREE_ID_PARAM_NAME = "treeid";
    public static final String TREE_EXPORT_SELECTED_NODE_PARAM_NAME = "selectedNodes";
    public static final String TREE_EXPORT_TREE_NAME_PARAM_NAME = "treename";
    public static final String TREE_EXPORT_BINDED_TREE_NAME_PARAM_NAME = "binedtreename";
    public static final String TREE_EXPORT_PRINT = "print";
    public static final String TREE_EXPORT_SELECTED_NODE_NAME = "selectedNodeName";
    public static final String TREE_EXPORT_NODE_KIND_ID = "nodekindid";
    public static final String TREE_EXPORT_NODE_KIND_CODE = "nodekindcode";
    public static final String TREE_EXPORT_TEMPLATE = "template";
    public static final String TREE_EXPORT_TEMPLATE_NODE_ID = "templateNodeId";
    public static final String TREE_EXPORT_LEVEL = "level";
    public static final String TREE_EXPORT_SHOW_PAGE = "showPage";
    public static final String TREE_EXPORT_SHOW_NEW_LINE = "showNewLine";
    public static final String TREE_EXPORT_SHOW_AUTHOR = "showAuthor";
    public static final String EXPORT_FILE_NAME = "filename";
    public static final String EXPORT_FORMAT_XLSX = "xlsx";
    public static final String EXPORT_FORMAT_CSV = "csv";
    public static final String EXPORT_FORMAT_HTML = "html";
    public static final String EXPORT_FORMAT_PDF = "pdf";
    public static final String EXPORT_FORMAT_ZIP = "zip";
    public static final String JASPER_EXPORT_NODE_ID = "nodeId";
    public static final String JASPER_EXPORT_EXTENSION = "extension";
    public static final String JASPER_EXPORT_FORMAT_HTML = "jhtml";
    public static final String JASPER_EXPORT_PARAM_NAME = "jasperexportresults";
    public static final String JASPER_EXPORT_NAME_PARAM_NAME = "reportName";
    public static final String SEARCH_PHRASE_PARAM_NAME = "searchPhrase";
    public static final String BIAINSTANCE_RESULTS_EXPORT_PARAM_NAME = "biInstanceResultsExport";
    public static final String BIAINSTANCE_PHRASE_PARAM_NAME = "BI Instance";
    public static final String BIAINSTANCE_SCHEDULE_TYPE = "scheduleType";
    public static final String BIAINSTANCE_RECURRING = "recurring";
    public static final String BIAINSTANCE_ID = "id";
    public static final String BIAINSTANCE_PARENT_ID = "parentId";
    public static final String BIAINSTANCE_OWNER = "owner";
    public static final String BIAINSTANCE_ANCESTOR_ID = "ancestorId";
    public static final String BIAINSTANCE_INTERVAL_HOURS = "intervalHours";
    public static final String BIAINSTANCE_STARTED = "started";
    public static final String BIAINSTANCE_ENDED = "ended";
    public static final String BIAINSTANCE_REPORTS = "reportsIds";
    public static final String BIAINSTANCE_FOLDERS = "ancestorIds";
    public static final String BIAINSTANCE_CONNECTIONS = "connectionsNodeId";
    public static final String BIA_AUDIT_RESULTS_EXPORT_PARAM_NAME = "biaAuditResultsExport";
    public static final String BIA_AUDIT_PHRASE_PARAM_NAME = "Audit";
    public static final String BIA_OBJECT_ID = "id";
    public static final String BIA_OBJECT_NAME = "name";
    public static final String BIA_OBJECT_OWNER = "owner";
    public static final String BIA_OBJECT_PARENT_ID = "parentId";
    public static final String BIA_OBJECT_MANAGE_TYPE = "manageType";
    public static final String BIA_OBJECT_RESULTS_EXPORT_PARAM_NAME = "biaObjectResultsExport";
    public static final String BIA_OBJECT_PHRASE_PARAM_NAME = "ObjectManager";
    public static final String SEARCH_MODE_NORMAL = "normal";
    public static final String SEARCH_MODE_ADVANCED = "advanced";
    //---
//    public static final String FULL_HREF_REQ_CTX_PARAM_NAME = "fullHref";
    //---
    public static final String OPEN_PLACEHOLDER = "(";
    public static final String CLOSE_PLACEHOLDER = ")";
    public static final String RSS = "rss";
    public static final String DOWNLOAD_FILE_SYSTEM = "downloadFS";
    public static final String NAS_FILE_PREFIX = "file_NAS_";
    public static final String DROOLS_TEMPLATE_ATTRIBUTE_NAME = "Szablon reguły";
    public static final String DROOLS_PARAM_ATTRIBUTE_NAME = "Parametry reguły";
    public static final String APP_PROP_SYS_CONF = "metadataTree";
    public static final String APP_PROP_MENU_CONF = "menuConfigTree";

    static {
        SPECIAL_MSSQL_NODE_KIND_TRANSLATIONS.put(BUILD_FOLDER_TABLE, I18n.BUILD_tables);
        SPECIAL_MSSQL_NODE_KIND_TRANSLATIONS.put(BUILD_FOLDER_VIEW, I18n.BUILD_views);
        SPECIAL_MSSQL_NODE_KIND_TRANSLATIONS.put(BUILD_FOLDER_PROCEDURE, I18n.BUILD_procedures);
        SPECIAL_MSSQL_NODE_KIND_TRANSLATIONS.put(BUILD_FOLDER_PACKAGES, I18n.BUILD_packages);
        SPECIAL_MSSQL_NODE_KIND_TRANSLATIONS.put(BUILD_FOLDER_FUNCTION, I18n.BUILD_functions);
        SPECIAL_MSSQL_NODE_KIND_TRANSLATIONS.put(BUILD_FOLDER_SCALAR_FUNCTION, I18n.BUILD_scalarFunctions);
        SPECIAL_MSSQL_NODE_KIND_TRANSLATIONS.put(BUILD_FOLDER_TABLE_FUNCTION, I18n.BUILD_tableFunctions);
        //
        SPECIAL_NODE_KIND_TRANSLATIONS.put(NODE_KIND_ALL_USERS_FOLDER, I18n.BUILD_allUsers);
        SPECIAL_NODE_KIND_TRANSLATIONS.put(NODE_KIND_DQC_ALL_TESTS_FOLDER, I18n.BUILD_allTests);
//        SPECIAL_NODE_KIND_TRANSLATIONS.put(NODE_KIND_DQM_ALL_TESTS_FOLDER, I18n.BUILD_allTests);
        SPECIAL_NODE_KIND_TRANSLATIONS.put(NODE_KIND_UNIVERSE_TABLES_FOLDER, I18n.BUILD_universeTable);
        SPECIAL_NODE_KIND_TRANSLATIONS.put(NODE_KIND_SAPBW_OBJECTS_FOLDER, I18n.BUILD_sapbwFeature);
        SPECIAL_NODE_KIND_TRANSLATIONS.put(NODE_KIND_MSSQL_FOLDER, SPECIAL_MSSQL_NODE_KIND_TRANSLATIONS);
        SPECIAL_NODE_KIND_TRANSLATIONS.put(NODE_KIND_POSTGRES_FOLDER, SPECIAL_MSSQL_NODE_KIND_TRANSLATIONS);
        SPECIAL_NODE_KIND_TRANSLATIONS.put(NODE_KIND_ORACLE_FOLDER, SPECIAL_MSSQL_NODE_KIND_TRANSLATIONS);
    }
    //---
    public static final String NODE_ACTION_CODE_SHOW_IN_TREE = "ShowInTree";
    /*
     * isEffectiveExpertLoggedInEx
     */
    public static final String ACTION_NEWS_EDITOR = "#NewsEditor";
    /*
     * Dodawanie,usuwanie,edycja Aktualności
     */

    public static final String ACTION_WELCOME_MSG = "#WelcomeMsg";
    public static final String ACTION_LINK_USER_TO_ROLE = "LinkUserToRole";
    /*
     * taka jak w przypisz do roli
     */

    public static final String ACTION_ADD_DOCUMENT = "AddDocument";
    public static final String ACTION_ADD_ANY_NODE = "AddAnyNodeForKind";
    public static final String ACTION_ADD_EVENT = "AddEvent";
    public static final String ACTION_ADD_CUSTOM_TREE_NODE = "AddCustomTreeNode";
    /*
     * rola jak na akcji dodaj dokument w drzewie
     */

 /*

     */
    public static final String ACTION_EXPORT_TO_PDF = "ExportToPDF";
    public static final String ACTION_EXPORT_TO_HTML = "ExportToHTML";
    /*
     * SysAdmin
     */

    public static final String ACTION_EXPORT_IMPORT_TREE = "#ExportImportTree";
    public static final String ACTION_OTHER_ACTIONS_SYS_ADMIN = "#OtherActionsSysAdmin";
    public static final String ACTION_IS_IN_ULTIMATE_EDIT_MODE = "#isInUltimateEditMode";
    public static final String ACTION_PUMP_ORACLE = "#PumpOracle";
    public static final String ACTION_PUMP_MSSQL = "#PumpMssql";

    /*
     * isAppAdminLoggedIn
     */
    public static final String ACTION_TUTORIAL_EDITOR = "#TutorialEditor";
    public static final String ACTION_MY_OBJECTS_TAB_VISIBLE = "#MyObjectsTabVisible";
    public static final String ACTION_HELP_EDITOR = "#HelpEditor";
    /*
     * Jak ktos ma dostęp do edycji pomocy na zakładce Admin to i na każdej
     * innej zakładce.Można zmienić per drzewo
     */

    public static final String ACTION_ASSIGN_USER = "#AssignUser";
    public static final String ACTION_OTHER_ACTIONS_APP_ADMIN = "#OtherActionsAppAdmin";
    //---
    public static final int KEEP_SESSION_ALIVE_FREQ_MILLIS = 10000;
//    public static final String KEEP_SESSION_ALIVE_REQUEST_PARAM_SEP = "@{#}@";
    //---
    public static final String BIKS_ID_Polkomtel = "7CB602FD-4C29-4C5D-A3A9-82E814EFADD2";
    public static final String BIKS_ID_Cyfrowy = "B424EC5D-2D08-45AE-874D-A4E95FA61880";
    public static final String BIKS_ID_PlusBank = "5005832A-6D1D-4F70-BB6D-32146BF003AA";

    public static final String BASIC_TEMPLATE = "Podstawowy";
}
