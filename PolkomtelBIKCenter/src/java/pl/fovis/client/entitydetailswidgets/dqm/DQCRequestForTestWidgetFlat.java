/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import com.google.gwt.user.client.ui.HTML;
import java.util.Date;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.entitydetailswidgets.EntityDetailsTabPaginationBase;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.dqm.DataQualityRequestBean;
import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class DQCRequestForTestWidgetFlat extends EntityDetailsTabPaginationBase<DataQualityRequestBean> {

    @Override
    protected List<DataQualityRequestBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
        return data.dataQualityTestRequests;
    }

    @Override
    public String getCaption() {
        return /*BIKGWTConstants.MENU_NAME_DQC + "<br>" + */ BIKGWTConstants.MENU_NAME_REQUESTS;
    }

    @Override
    protected HTML createDateAdded(Date dateAdded) {
        HTML dateAddedHTML = new HTML(SimpleDateUtils.toCanonicalString(dateAdded));
        dateAddedHTML.setStyleName("dqc-dateAdded");
        return dateAddedHTML;
    }

    @Override
    public boolean hasEditableControls() {
        return false;
    }

    @Override
    protected NodeAwareBeanExtractorBase<DataQualityRequestBean> getBeanExtractor() {
        EntityDetailsDataBean data = getData();
        return new DataQualityRequestBeanExtractor(data.dataQualityTestExtradata.errorThreshold, data, data.node.treeCode.equals(BIKConstants.TREE_CODE_DQM));
    }

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_QUALITY_METADATA;
    }

    @Override
    protected BikCustomButton createAddJoinedButton(Set<String> treeKinds) {
        return null;
    }

    @Override
    protected boolean hasAddJoinedButton() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        String nodeKindCode = getNodeKindCode();
//            @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DQM_TEST_SUCCESS, BIKConstants.NODE_KIND_DQM_TEST_NOT_STARTED, BIKConstants.NODE_KIND_DQM_TEST_INACTIVE, BIKConstants.NODE_KIND_DQM_TEST_FAILED, BIKConstants.NODE_KIND_DQC_TEST_SUCCESS, BIKConstants.NODE_KIND_DQC_TEST_FAILED, BIKConstants.NODE_KIND_DQC_TEST_INACTIVE})
        return BIKConstants.NODE_KIND_DQM_TEST_SUCCESS.equals(nodeKindCode) || BIKConstants.NODE_KIND_DQM_TEST_NOT_STARTED.equals(nodeKindCode) || BIKConstants.NODE_KIND_DQM_TEST_INACTIVE.equals(nodeKindCode) || BIKConstants.NODE_KIND_DQM_TEST_FAILED.equals(nodeKindCode) || BIKConstants.NODE_KIND_DQC_TEST_SUCCESS.equals(nodeKindCode) || BIKConstants.NODE_KIND_DQC_TEST_FAILED.equals(nodeKindCode) || BIKConstants.NODE_KIND_DQC_TEST_INACTIVE.equals(nodeKindCode);
    }
}
