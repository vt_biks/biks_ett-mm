/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class TeradataConnectionParametersBean  implements Serializable{

    public String driverClass;
    public String url;
    public String user;
    public String password;
    public String select;
    public String maxErrorCount;

}
