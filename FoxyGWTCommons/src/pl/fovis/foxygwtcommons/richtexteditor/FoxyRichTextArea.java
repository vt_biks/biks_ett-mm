/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.richtexteditor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.BodyElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.HeadElement;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.dom.client.LinkElement;
import com.google.gwt.event.dom.client.MouseEvent;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.InitializeEvent;
import com.google.gwt.event.logical.shared.InitializeHandler;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.foxygwtcommons.selection.Range;
import pl.fovis.foxygwtcommons.selection.Selection;
import simplelib.BaseUtils;
import simplelib.IRegExpMatcher;
import simplelib.IRegExpPattern;

/**
 *
 * @author pmielanczuk
 */
public class FoxyRichTextArea extends RichTextArea implements MouseOutHandler, MouseOverHandler {

    protected boolean m_isInText = false;
    protected Range m_lastRange = null;

    public FoxyRichTextArea() {
        attachMouseEventHandlers();
        attachCssPrivate();
    }

    // private aby nie pyszczył w konstuktorze (overridable method call in constructor)
    private void attachMouseEventHandlers() {
        addMouseOutHandler(this);
        addMouseOverHandler(this);
    }

    // private aby nie pyszczył w konstuktorze (overridable method call in constructor)
    private void attachCssPrivate() {
        attachCss();
    }

    protected void attachCss() {
        addInitializeHandler(new InitializeHandler() {

            public void onInitialize(InitializeEvent ie) {
                IFrameElement iFrame = IFrameElement.as(getElement());
                Document document = iFrame.getContentDocument();
                BodyElement body = document.getBody();
                HeadElement head = HeadElement.as(Element.as(body.getPreviousSibling()));
                LinkElement styleLink = document.createLinkElement();
                styleLink.setType("text/css" /* I18N: no */);
                styleLink.setRel("stylesheet" /* I18N: no */);
                styleLink.setHref(getCssFileName());
                head.appendChild(styleLink);

//                StyleElement style = document.createStyleElement();
//                head.appendChild(style);
//                style.setInnerText("body {"
//                        + " color: #094F8B;"
//                        + "  font: 13px/19.2px sans-serif;"
//                        + "}\n" /*+ ".def-code{"
//                        + "background: none repeat scroll 0 0 #F9F9F9;"
//                        + " border: 1px dashed #2F6FAB;"
//                        + " overflow: auto;"
//                        + " padding: 1em;"
//                        + " font-family:monospace;"
//                        + " font-size:11.4333px;"
//                        + "    line-height:13.7167px;"
//                        + " }\n"
//                        + "#toc, .toc, .mw-warning{"
//                        + " background-color: #F9F9F9;"
//                        + " border: 1px solid #AAAAAA;"
//                        + " font-size: 95%;"
//                        + " padding: 5px;"
//                        + " min-width:300px;"
//                        + "   font-family:sans-serif;"
//                        + " }\n"
//                        + "h1,h2,h3{background: none repeat scroll 0 0 transparent;"
//                        + " border-bottom: 1px solid #CCDAEC;"
//                        + " font-weight: normal;"
//                        + " margin: 0;"
//                        + " overflow: hidden;"
//                        + " padding-bottom: 0.17em;"
//                        + " adding-top: 0.5em;"
//                        + " width: auto;"
//                        + " font-family:sans-serif;"
//                        + " }\n"
//                        + "a {"
//                        + " cursor: pointer;"
//                        + " text-decoration: none;"
//                        + " }\n"
//                        + "table.def {background: none repeat scroll 0 0 #F9F9F9;"
//                        + " border: 1px solid #AAAAAA;border-collapse: collapse;"
//                        + " margin: 1em 1em 1em 0;  width:100%;"
//                        + " }\n"
//                        + ".def th, .def td { border: 1px solid #AAAAAA;padding: 0.2em;"
//                        + " }\n"
//                         */);
//                  style.setInnerText(MyResources.resources.richTextAreaCSS().getText());;
//
            }
        });
    }

    protected String getCssFileName() {
        return "css/css.css" /* I18N: no */;
    }

    public Selection getSelection() {
        Selection res = null;
        try {
            JavaScriptObject window = getWindow();
            res = Selection.getSelection(window);
        } catch (Exception ex) {
            //GWT.log("Error getting the selection", ex);
            // zwróci nulla
        }
        return res;
    }

    public Range getRange() {
        if (m_lastRange == null) {
            return getSelection().getRange();
        } else {
            return m_lastRange;
        }
    }

    /**
     * This captures the selection when the mouse leaves the RTE, because in IE
     * the selection indicating the cursor position is lost once another widget
     * gains focus. Could be implemented for IE only.
     */
    public void captureSelection() {
        try {
            if (getRange().getDocument() != null) {
                m_lastRange = getRange();
            }
        } catch (Exception ex) { // rzuca wyjątek dla zaznaczenia zdjęcia
//            GWT.log("Error capturing selection for IE", ex);
        }
    }

    public void onMouseOut(MouseOutEvent event) {
        if (m_isInText && isOnTextBorder(event)) {
            m_isInText = false;
            captureSelection();
        }
    }

    public void onMouseOver(MouseOverEvent event) {
        if (!m_isInText) {
            m_isInText = true;
//            setFocus(true);
            m_lastRange = null;
        }
    }

    @SuppressWarnings("unchecked")
    private boolean isOnTextBorder(MouseEvent event) {
        Widget sender = (Widget) event.getSource();
        int twX = getAbsoluteLeft();
        int twY = getAbsoluteTop();
        int x = event.getClientX() - twX;
        int y = event.getClientY() - twY;
        int width = getOffsetWidth();
        int height = getOffsetHeight();
        return ((sender == this)
                && ((x <= 0) || (x >= width)
                || (y <= 0) || (y >= height)));
    }

    public JavaScriptObject getWindow() {
        return getWindow(this);
//        IFrameElement frame = getElement().cast();
//        return getWindow(frame);
    }

    public static JavaScriptObject getWindow(RichTextArea area) {
        IFrameElement frame = area.getElement().cast();
        return getWindow(frame);
    }

    public static native JavaScriptObject getWindow(IFrameElement iFrame) /*-{
     var iFrameWin = iFrame.contentWindow || iFrame.contentDocument;

     if( !iFrameWin.document )
     {
     iFrameWin = iFrameWin.getParentNode(); //FBJS version of parentNode
     }
     return iFrameWin;
     }-*/;

    public String getHTMLNoFixFullPro() {
        return super.getHTML();
    }

    @Override
    public String getHTML() {
        return replaceTableWidth(deleteBadURL(super.getHTML()));
    }

    protected String deleteBadURL(String oldTxt) {
        return replaceSpanToH2(oldTxt.replace("\"" + GWT.getHostPageBaseURL(), "\"").replace("'" + GWT.getHostPageBaseURL(), "'"));
    }
    //usuwanie starych rzeczy z nagłówków

    protected String replaceSpanToH2(String oldTxt) {//zmiany ze starych h2
        return oldTxt.replace("<h2><" + "span" /* I18N: no */, "<" + "h" /* I18N: no */ + "2").replace("</span></h2>", "</h2>");
    }
    //i zamiana szerokości w % na stałą wartość

    public static String replaceTableWidth(String html) {
        String pattern = "(" + "width" /* I18N: no */ + "=\"[0-9]+%\")";
        IRegExpPattern htmlUrlPattern
                = BaseUtils.getRegExpMatcherFactory().compile(pattern,
                        true, true);
        IRegExpMatcher htmlUrlMatcher = htmlUrlPattern.matcher(html);
        while (htmlUrlMatcher.find()) {
            html = html.replace(htmlUrlMatcher.group(1), "width=\"40px" /* I18N: no */ + "\"");
        }
        return html;
    }
}
