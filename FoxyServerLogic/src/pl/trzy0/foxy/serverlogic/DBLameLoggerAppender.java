/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import java.util.Collection;
import simplelib.logging.ILameLogger.LameLogLevel;
import simplelib.logging.LameLoggerAppenderBase;
import simplelib.logging.LameLoggerFactory;
import simplelib.logging.LameLoggerMsg;

/**
 *
 * @author wezyr
 */
public class DBLameLoggerAppender extends LameLoggerAppenderBase {

    public static final LameLogLevel dbMinLogLevel = LameLogLevel.Debug;
    private static boolean appenderAdded = false;

    public static synchronized void addAppender(IFoxyBizLogic bizLogic) {
        if (true) {
            return;
        }
        
        if (!appenderAdded) {
            LameLoggerFactory.addAppender(new DBLameLoggerAppender(dbMinLogLevel, bizLogic));
            appenderAdded = true;
        }
    }
    private IFoxyBizLogic bizLogic;

    private DBLameLoggerAppender(LameLogLevel minLogLevel, IFoxyBizLogic bizLogic) {
        super(minLogLevel);
        this.bizLogic = bizLogic;
    }

    protected void sendMsgToOutput(LameLoggerMsg msg) {
        // no-op, not used
    }

    @Override
    public void sendMsgsToOutput(Collection<LameLoggerMsg> msgs,
            LameLogLevel minLogLevelInMsgs, LameLogLevel maxLogLevelInMsgs) {
        msgs = filterMsgs(msgs, minLogLevelInMsgs, maxLogLevelInMsgs);
        if (msgs != null) {
            bizLogic.logMsgs(msgs, getMinLogLevel());
        }
    }
}
