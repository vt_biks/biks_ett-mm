/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author bfechner
 */
public class ObjectInFvsBean implements Serializable {

    public int userId;
    public int nodeId;
    public Date dateAdded;
    public String name;
    public String code;
    public String caption;
    public String tabId;
}
