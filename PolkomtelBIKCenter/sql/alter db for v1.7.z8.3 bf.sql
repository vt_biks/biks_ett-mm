﻿exec sp_check_version '1.7.z8.3';
go

update bik_app_prop set val=

'<body><div style="color:#094f8b">Witaj %{name}%, 
<br/><br/>
<table width="100%" border="0">  
<tr><td style="text-align:center"><h2 style="color:#094f8b">Aktualności</h2></td></tr> 
<tr><td style="text-align:center;padding-bottom: 15px;"> W systemie BIKS została dodana nowa aktualność: </td></tr>
<tr><td style="border: 2px solid #094f8b;padding-left: 20px;padding-right: 20px; padding-bottom: 20px;">
<h3 style="color:#094f8b">%{newsTitle}%</h3> %{newsBody}%</td></tr>
<tr><td><br/><br/> </td></tr> 
<tr><td style="font-size: 12px;color: #b9b9b9;line-height: 1.5">Ten e-mail został wygenerowany automatycznie. Prosimy na niego nie odpowiadać. </td></tr>  <tr><td style="font-size: 12px;color: #b9b9b9;line-height: 1.5">Jeśli nie chcesz otrzymywać powiadomień wejdź na %{page}% i odznacz opcję w edycji danych. </td></tr>  </table>  </body>'
where name='emailAdhocBody';
go


exec sp_update_version '1.7.z8.3', '1.7.z8.4';
go
