select * from sys.fulltext_indexes

select * 
from 
  bik_searchable_attr_val av 
  inner join containstable(bik_searchable_attr_val, *, '"writeoff.numer_konta" OR ("writeoff" AND "numer" AND "konta")') sr on av.id = sr.[key]
where value like '%writeoff.numer_konta%'



select * 
from 
  bik_searchable_attr_val av 
  inner join containstable(bik_searchable_attr_val, *, '"SELECT weekly.typ_serwisu, weekly.tydzien, weekly.data_startu_tygodnia, weekly.do_mb, weekly.liczba_uzytkownikow"') sr on av.id = sr.[key]


-- "do" przeszkadza!
select * 
from 
  bik_searchable_attr_val av 
  inner join containstable(bik_searchable_attr_val, *, '"SELECT" and "weekly" and "typ" and "serwisu" and "weekly" and "tydzien" and "weekly" and "data" and "startu" and "tygodnia" and "weekly" and "do" and "mb" and "weekly" and "liczba" and "uzytkownikow"') sr on av.id = sr.[key]

-- bez "do" jest OK
select * 
from 
  bik_searchable_attr_val av 
  inner join containstable(bik_searchable_attr_val, *, '"SELECT" and "weekly" and "typ" and "serwisu" and "weekly" and "tydzien" and "weekly" and "data" and "startu" and "tygodnia" and "weekly" and "mb" and "weekly" and "liczba" and "uzytkownikow"') sr on av.id = sr.[key]



-- SELECT weekly.typ_serwisu, weekly.tydzien, weekly.data_startu_tygodnia, weekly.do_mb, weekly.liczba_uzytkownikow

select * from sys.fulltext_languages

select * from sys.fulltext_stopwords

select * from sys.fulltext_stoplists

EXEC sp_help_fulltext_system_components 'all'

--------------------------

-- czy "do" przeszkadza?
-- je�eli przeszkadza - nie b�dzie wynik�w
select * 
from 
  bik_searchable_attr_val av 
  inner join containstable(bik_searchable_attr_val, *, '"SELECT" and "weekly" and "typ" and "serwisu" and "weekly" and "tydzien" and "weekly" and "data" and "startu" and "tygodnia" and "weekly" and "do" and "mb" and "weekly" and "liczba" and "uzytkownikow"') sr on av.id = sr.[key]
  
  
-- indeks z systemow� stop-list�
-- tutaj "do" przeszkadza!
  if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val'))
  exec('drop fulltext index on bik_searchable_attr_val')

declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_searchable_attr_val') and is_primary_key = 1)
exec('CREATE FULLTEXT INDEX ON bik_searchable_attr_val (value, fixed_value) key index ' + @pk_name)



-- indeks bez stop-listy
-- tutaj "do" nie przeszkadza!
  if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val'))
  exec('drop fulltext index on bik_searchable_attr_val')

declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_searchable_attr_val') and is_primary_key = 1)
--select @pk_name
exec('CREATE FULLTEXT INDEX ON bik_searchable_attr_val (value, fixed_value) key index ' + @pk_name + ' with stoplist = off')



--------------------------
--------------------------
--------------------------


        select top 20 bn.id as node_id,
        bn.name,
        bnk.code as node_kind_code,
        bnk.caption as node_kind_caption,
        bt.name as tree_name,
        bt.code as tree_code,
        bn.vote_sum,
        bn.vote_cnt
        from (
        
        select
        bn.[id],
        bn.[node_kind_id],
        bn.[name],
        bn.[tree_id],
        bn.[search_rank],
        bn.[vote_sum],
        bn.[vote_cnt],
        bn.[vote_val],
        sr.sum_combined_rank, sr.max_combined_rank, sr.sum_search_weight, sr.max_search_weight
                
        from
        bik_node bn
        
            inner join
            (
            select node_id,
            sum(sr.[rank] * coalesce(ap.search_weight, 1)) as sum_combined_rank,
            max(sr.[rank] * coalesce(ap.search_weight, 1)) as max_combined_rank,
            sum(coalesce(ap.search_weight, 1)) as sum_search_weight,
            max(coalesce(ap.search_weight, 1)) as max_search_weight,
            max(case when av.value like '%id[_]konto%' then 1 else 0 end) as has_it
            from 
            containstable(bik_searchable_attr_val, *, '"id" AND "konto"') sr 
            inner --loop 
            join
            bik_searchable_attr_val av --wi[.]th (in dex(PK_ _bik_sear_ _3213E83F21FD228E)) 
            on av.id = sr.[key]
            inner join bik_searchable_attr a on av.attr_id = a.id
            left join bik_searchable_attr_props ap on a.name = ap.name
            --where
        --av.tree_id in (1,2,3,4,8,12,15,14,16,6,7,13,18)
        --and av.node_kind_id in (42)             
            --and av.value like '%id[_]konto%'
            group by node_id
            ) sr on sr.node_id = bn.id
        
        where bn.is_deleted = 0 and (bn.linked_node_id is null or bn.disable_linked_subtree <> 0)
        and bn.tree_id in (1,2,3,4,8,12,15,14,16,6,7,13,18)
        and bn.node_kind_id in (42)
        and sr.has_it = 1
        ) as bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
        inner join bik_tree bt on bn.tree_id = bt.id
        order by bn.max_search_weight desc, bn.search_rank + bnk.search_rank desc, bn.vote_sum desc, sum_combined_rank desc
