/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.templates;

import java.util.List;
import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.ParserTask;
import pl.foxys.foxymill.RenderEnvironment;
import pl.foxys.foxymill.tags.ITag;
import pl.trzy0.foxy.commons.templates.IParsedTemplate;
import pl.trzy0.foxy.commons.templates.ITemplateParser;

/**
 *
 * @author wezyr
 */
public class FoxyMillParser implements ITemplateParser {

    public static final boolean DEFAULT_EXPRLANG_MVEL = true;
    public static final boolean DISABLE_TAGS_CACHING = true;
    public static final RenderEnvironment defaultRenderEnv = FoxyMillUtils.makeRenderEnvForHTML(
            DEFAULT_EXPRLANG_MVEL, DISABLE_TAGS_CACHING);
    private RenderEnvironment renderEnv;

    public FoxyMillParser() {
        this(defaultRenderEnv);
    }

    public FoxyMillParser(RenderEnvironment renderEnv) {
        this.renderEnv = renderEnv;
    }

    public FoxyMillParser(boolean defaultMvel, boolean disableCaching) {
        this(FoxyMillUtils.makeRenderEnvForHTML(defaultMvel, disableCaching));
    }

    public IParsedTemplate parse(String fileName, String content) {
        if (content == null) {
            return null;
        }
        
        List<ITag> tags = ParserTask.parse(fileName, content);
        FoxyMillUtils.prepareTags(tags, renderEnv);
        return new FoxyMillParsedTemplate(tags);
    }
}
