/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.IJoinedObjSelectionManager;
import pl.fovis.client.dialogs.TreeSelectorForAttachments;
import pl.fovis.common.AttachmentBean;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author młydkowski
 */
public class AttachmentsWidgetFlat extends EntityDetailsTabPaginationBase<AttachmentBean> {

    private void showSelectorForm(final int nodeId) {
        final TreeSelectorForAttachments treeSelector
                = new TreeSelectorForAttachments();

        final IJoinedObjSelectionManager<Integer> alreadyJoinedManager = getAlreadyJoinedManager();

        treeSelector.buildAndShowDialog(nodeId, new IParametrizedContinuation<TreeNodeBean>() {
            @Override
            public void doIt(TreeNodeBean selected) {

                BIKClientSingletons.getService().addBikJoinedObject(
                        nodeId, selected.id, treeSelector.isInheritToDescendantsChecked(),
                        new StandardAsyncCallback<Void>("Failure on" /* I18N: no */ + " linkBikDocument") {
                            @Override
                            public void onSuccess(Void result) {
                                BIKClientSingletons.showInfo(I18n.dodanoZalacznik.get() /* I18N:  */);
                                refreshAndRedisplayData();
                            }
                        });
            }
        }, alreadyJoinedManager);
    }

    @Override
    public boolean hasContent() {
        return !isInUserTree(getNodeTreeCode()) && super.hasContent();
    }

    @Override
    protected List<AttachmentBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
//        return data.attachments;
        return BaseUtils.isCollectionEmpty(items) ? new ArrayList<AttachmentBean>() : items;
    }

    @Override
    public String getCaption() {
        return /*BIKGWTConstants.MENU_NAME_KNOWLEDGE + "<br>" +*/ BIKGWTConstants.MENU_NAME_DOCUMENTS;
    }

    @Override
    public boolean hasEditableControls() {
        return !isInUserTree(getNodeTreeCode()) && items != null;
    }

    @Override
    protected NodeAwareBeanExtractorBase<AttachmentBean> getBeanExtractor() {
        return new AttachmentBeanExtractor(hasAddJoinedButton, nodeId);
    }

    @Override
    protected String textWhenNoItems() {
        return I18n.brakDokumentacji.get() /* I18N:  */;
    }

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_DOCUMENTS;
    }

    @Override
    protected BikCustomButton createAddJoinedButton(final Set<String> treeKinds) {
        BikCustomButton btn;
        btn = new BikCustomButton(I18n.dodajDokumentacje.get() /* I18N:  */, "Attachs" /* I18N: no */ + "-addAttachEntry",
                new IContinuation() {
                    @Override
                    public void doIt() {
                        showSelectorForm(nodeId);
                    }
                });
        addWidgetVisibledByState(btn);
        return btn;
    }

    @Override
    protected boolean isChangedViewDescriptionInJoinedPanelVisible() {
        return true;
    }

    @Override
    protected boolean isViewWithAttributeRbInJoinedPanelVisible() {
        return true;
    }

    @Override
    public void optCallServiceToGetData(final IContinuation con) {
        BIKClientSingletons.getService().getAttachments(nodeId, getData().node.linkedNodeId, new AsyncCallback<List<AttachmentBean>>() {

            @Override
            public void onFailure(Throwable caught) {
                displayData();
            }

            @Override
            public void onSuccess(List<AttachmentBean> result) {
                items = result;
                con.doIt();
            }
        });
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
