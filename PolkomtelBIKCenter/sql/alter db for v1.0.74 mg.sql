﻿exec sp_check_version '1.0.74';
go

update bik_tree
set node_kind_id = null
from bik_tree t
inner join bik_node_kind nk on nk.id = t.node_kind_id
where nk.code = 'Metadata'
go

delete from bik_node_kind where code = 'Metadata'
go

exec sp_update_version '1.0.74', '1.0.75';
go
