/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import com.google.gwt.event.logical.shared.ShowRangeEvent;
import com.google.gwt.event.logical.shared.ShowRangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.bssg.metadatapump.common.SAPBOScheduleBean;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.syntaxhighlighter.BrushFactory;
import pl.fovis.foxygwtcommons.syntaxhighlighter.SyntaxHighlighter;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;
import pl.trzy0.foxy.commons.FoxyCommonUtils;
import simplelib.BaseUtils;
import simplelib.ILameCollector;
import simplelib.IPredicate;
import simplelib.IRegExpMatcher;
import simplelib.IRegExpPattern;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author wezyr
 */
public class BIKCenterUtils extends FoxyCommonUtils {

//    private static Pattern htmlUrlPattern =
//            Pattern.compile("<a[^>]+href[\\s]*=[\"\']#[a-zA-Z]*/([-]?[0-9]+)[\"\'][^>]*>([^<]+)</a>",
//            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    public static final String HTML_URL_PATTERN_PREFIX = "<a[^>]+" + "href[\\s]*=[\\s" /* I18N: no */ + "]*[\"\']#[a-zA-Z0-9]*/(";
    public static final String A_PATTERN_PREFIX = "href[\\s]*=[\\s" /* I18N: no */ + "]*[\"\'](";
    public static final String A_PATTERN_SUFFIX = ")\" " + "style=\"text-decoration:none;color" /* I18N: no */ + ": #0000aa;\"";
    public static final String A_IE_PATTERN_PREFIX = "none?\" href" /* I18N: no */ + "=\"(";
    public static final String A_IE_PATTERN_SUFFIX = ")\"";
    public static final IRegExpPattern HTML_URL_PATTERN
            = BaseUtils.getRegExpMatcherFactory().compile(HTML_URL_PATTERN_PREFIX + nodeIdPattern + htmlUrlPatternSuffix,
                    true, true);

    public static String removeHrefsFromHtml(String html, Set<Integer> nodeIds) {
        StringBuilder buffer = new StringBuilder();
        String sep = "";
        for (Integer id : nodeIds) {
            buffer.append(sep);
            buffer.append(id.toString());
            sep = "|";
        }
        IRegExpPattern htmlUrlPattern
                = BaseUtils.getRegExpMatcherFactory().compile(HTML_URL_PATTERN_PREFIX + buffer.toString() + htmlUrlPatternSuffix,
                        true, true);
        IRegExpMatcher htmlUrlMatcher = htmlUrlPattern.matcher(html);
        buffer.setLength(0);
        int index = 0;
        while (htmlUrlMatcher.find()) {
            buffer.append(html.substring(index, htmlUrlMatcher.start()));
            buffer.append(htmlUrlMatcher.group(2));
            index = htmlUrlMatcher.end();
        }
        if (index < html.length()) {
            buffer.append(html.substring(index));
        }
        return buffer.toString();
    }

    public static String replaceContentsFromHtmlTreeCode(String html, int nodeId, String treeCode, String ex) {

        html = html.replace("<h2><" + "span" /* i18n: no */, "<h2").replace("</span></h2>", "</h2>"); //stare nagłówki na nowe
//        String pr = "(none?\")?[\\s]?href[\\s]*=[\\s]*[\"\']([#][^<]+)(style=\"text-decoration:none;color: #0000aa;)?\"";
        html = replaceContentsFromHtmlTreeCodeEx(html, nodeId, treeCode, ex, A_PATTERN_PREFIX + caption + A_PATTERN_SUFFIX);
        html = replaceContentsFromHtmlTreeCodeEx(html, nodeId, treeCode, ex, A_PATTERN_PREFIX + caption + A_IE_PATTERN_SUFFIX);
        html = replaceContentsFromHtmlTreeCodeEx(html, nodeId, treeCode, ex, A_IE_PATTERN_PREFIX + caption + A_IE_PATTERN_SUFFIX);

        //do linków do drzewek (dodawanie id zeby na wyskakującym dialogu dało sie klikać w linki ktore zamkną okienko)
        IRegExpMatcher htmlUrlMatcher = HTML_URL_PATTERN.matcher(html.toLowerCase());
        while (htmlUrlMatcher.find()) {
            html = html.replace(htmlUrlMatcher.group(1).trim() + "\"", htmlUrlMatcher.group(1).trim() + "\" " + "id" /* I18N: no */ + "=" + htmlUrlMatcher.group(1).trim() + ex + "\"");//spis
        }

        return html;
    }

    protected static String replaceContentsFromHtmlTreeCodeEx(String html, int nodeId, String treeCode, String ex, String pattern) {
        IRegExpPattern htmlUrlPattern
                = BaseUtils.getRegExpMatcherFactory().compile(pattern,
                        true, true);
        IRegExpMatcher htmlUrlMatcher = htmlUrlPattern.matcher(html.toLowerCase());

        //ww->prim: co robi ten kod, jaki tu jest algorytm? wydaje się to bardzo mocno
        // NIEEFEKTYWNE??!!??
        Set<String> contents = new LinkedHashSet<String>();
        while (htmlUrlMatcher.find()) {
            String grp1 = htmlUrlMatcher.group(0);
            contents.add(grp1);
            String text = replaceText(htmlUrlMatcher.group(1)).replace("#", "")/*.toLowerCase()*/;
            html = html.replace(text, treeCode + "/" + nodeId + "_" + text + ex);//w tekscie
        }
        return html;
    }

//    public static <B extends BeanWithIntIdBase> Map<Integer, B> makeBeanMap(Iterable<B> iterable) {
//        return BaseUtils.projectToMap(iterable, new Projector<B, Integer>() {
//            public Integer project(B val) {
//                return val.id;
//            }
//        });
//    }
    public static String getBikUserNodeName(String loginName, String name/*
     * , boolean activ, String oldName
     */) {
        StringBuilder nodeName = new StringBuilder();
//        if (activ) {
        nodeName.append(getBikUserNodeNameAndLogin(loginName, name));
//        } else {
//            nodeName.append("konto usunięte  (").append(loginName).append(")");
//        }
        return nodeName.toString();
    }

    public static String getBikUserNodeNameAndLogin(String loginName, String name) {

        StringBuilder nodeName = new StringBuilder();
        if (!name.equals(loginName) && !name.contains("(" + loginName + ")")) {
            nodeName.append(name).append(" (").append(loginName).append(")");
        } else {
            nodeName.append(name);
        }
        return nodeName.toString();
    }

    public static AppPropsBeanEx readAppPropBeans(Collection<AppPropBean> appProps) {
        AppPropsBeanEx res = new AppPropsBeanEx();

        for (AppPropBean apb : appProps) {
            String propName = apb.name;
            if (res.getPropNames().contains(propName)) {
                try {
                    res.setPropValue(propName, apb.val);
                } catch (Exception ex) {
                    // no-op
                }
            }
        }

        res.calcExProps();

        return res;
    }

    public static List<Integer> splitBranchIdsEx(final String branchIds, boolean withOutSelf) {
        return splitBranchIdsIntoColl(branchIds, withOutSelf, new ArrayList<Integer>());
    }

    public static <T extends Collection<Integer>> T splitBranchIdsIntoColl(String branchIds, boolean withOutSelf, final T coll) {
        branchIds = BaseUtils.dropLastItems(branchIds, "|", 1 + (withOutSelf ? 1 : 0));

        BaseUtils.splitBySepIntoCollector(branchIds, "|", new ILameCollector<String>() {
            public void add(String item) {
                //if (!BaseUtils.isStrEmptyOrWhiteSpace(item)) {
                coll.add(Integer.parseInt(item));
                //}
            }
        }, true);
        return coll;
    }

    public static <T extends Collection<String>> T splitBranchIdsAsStringIntoColl(String branchIds, boolean withOutSelf, final T coll) {
        branchIds = BaseUtils.dropLastItems(branchIds, "|", 1 + (withOutSelf ? 1 : 0));

        BaseUtils.splitBySepIntoCollector(branchIds, "|", new ILameCollector<String>() {
            public void add(String item) {
                coll.add(item);
            }
        }, true);
        return coll;
    }

    public static String buildAncestorPathByNodeNames(String branchIds, final Map<Integer, String> nodeNames,
            final Collection<Integer> missingNamesIds) { //"»"

        List<Integer> ancestorIds = BIKCenterUtils.splitBranchIdsEx(branchIds, false);

        boolean someNodesMissing = false;
        for (int ancestorId : ancestorIds) {
            if (nodeNames == null || !nodeNames.containsKey(ancestorId)) {
                if (missingNamesIds != null) {
                    missingNamesIds.add(ancestorId);
                }
                someNodesMissing = true;
            }
        }

        if (someNodesMissing) {
            return null;
        }

        return BaseUtils.mergeWithSepEx(ancestorIds, BIKConstants.RAQUO_STR_SPACED,
                new BaseUtils.Projector<Integer, String>() {
            public String project(Integer val) {
                return nodeNames.get(val);
            }
        });
    }

    public static String buildAncestorPathByStorage(String branchIds, final ITreeRowAndBeanStorage<Integer, ?, TreeNodeBean> storage) { //"»"

        return BaseUtils.mergeWithSepEx(splitBranchIdsEx(branchIds, false), BIKConstants.RAQUO_STR_SPACED,
                new BaseUtils.Projector<Integer, String>() {
            @Override
            public String project(Integer val) {
                TreeNodeBean tnb = storage.getBeanById(val);
                return tnb != null ? tnb.name : "?";
            }
        });
    }

    public static List<TreeNodeBean> getRootNodes(Iterable<TreeNodeBean> nodes) {
        List<TreeNodeBean> res = new ArrayList<TreeNodeBean>();

        for (TreeNodeBean tnb : nodes) {
            if (tnb.parentNodeId == null) {
                res.add(tnb);
            }
        }

        return res;
    }

    private static Pair<List<String>, List<String>> normalizeTxtWithoutQuotation(String a) {
        List<String> forContains = new ArrayList<String>();
        List<String> forLike = new ArrayList<String>();
        String[] noQuotation = a.split("[ ]");

        for (String s : noQuotation) {
            boolean endsWithAsterisk = s.endsWith("*");

            // dodatkowo: gwiazdka może być na końcu słowa
            // ucinamy gwiazdkę z końca
            String sNoAsterisk = endsWithAsterisk
                    ? s.substring(0, s.length() - 1) : s;

            String s2 = sNoAsterisk;
            s2 = s2.replaceAll("[_]+", " ");

            boolean hasSpecialChars = s2.contains(" ") || s2.contains(".");
            if (hasSpecialChars) {
                // były znaki specjalne kropka i podkreślenie
                String sLike = "%" + sNoAsterisk.replace("_", "[_]")
                        + "%";
                forLike.add(sLike);
            }
            boolean specialWasLast = s2.endsWith(" ");
            s2 = s2.trim();
            if (!BaseUtils.isStrEmptyOrWhiteSpace(s2)) {
                forContains.add(s2 + (!specialWasLast && endsWithAsterisk ? "*" : ""));
            }
        }
        Pair<List<String>, List<String>> p = new Pair<List<String>, List<String>>(forContains, forLike);
        return p;
    }

    // nigdy nie zwraca nulla, gdy przekazany tekst jest niepoprawny składniowo, to
    // w wyniku będzie para <null, pusta lista>
    // pierwsza lista może być też pusta gdy w drugiej coś jest - np. jakaś dziwna fraza
    // z samymi kropkami, podkreśleniami i gwiazdkami
    public static Pair<List<String>, List<String>> normalizeTextForFTSAndOptLike(String txt) {
        List<String> forLike = new ArrayList<String>();
        Pair<List<String>, List<String>> res = new Pair<List<String>, List<String>>(null, forLike);

        if (txt == null) {
            return res;
        }
        List<String> forContains = new ArrayList<String>();
        res.v1 = forContains;

        boolean bb = txt.endsWith("\"");
        String[] a = txt.split("[\"]");

        int lgh = a.length;
        boolean b = false;
        if (a.length % 2 == 0) {
            b = !bb;
            lgh--;
        }

        for (int i = 0; i < lgh; i++) {
            if (i % 2 != 0) {
                String ss = a[i].trim();
                //ss.trim();
                if (!BaseUtils.isStrEmptyOrWhiteSpace(ss)) {
                    forContains.add(ss);
                }
            } else {
                String normTxt = normalizeTextForFTS(a[i], true);
                if (!BaseUtils.isStrEmptyOrWhiteSpace(normTxt)) {
                    Pair<List<String>, List<String>> p = normalizeTxtWithoutQuotation(normTxt);
                    forContains.addAll(p.v1);
                    forLike.addAll(p.v2);
                }
            }
        }
        if (b) {
            String normTxt = normalizeTextForFTS(a[lgh], true);
            if (!BaseUtils.isStrEmptyOrWhiteSpace(normTxt)) {
                Pair<List<String>, List<String>> p = normalizeTxtWithoutQuotation(normTxt);
                forContains.addAll(p.v1);
                forLike.addAll(p.v2);
            }
        }
        //pg: nie ma sensu wchodzić jeśli wielkość tabeli jest nieparzysta z dwóch powodów:
        //1. już wszystkie części wyrażenia były dodane ponieważ lgh nie jest dekrementowane dla nieparzystych, więc w pętli for
        //powyżej przerobi wszystkie części wyrażenia
        //2. powoduje błąd IndexOutOfBoundException
        if (a.length % 2 == 0 && bb) {
            String ss = a[lgh].trim();
            //ss.trim();
            if (!BaseUtils.isStrEmptyOrWhiteSpace(ss)) {
                forContains.add(ss);
            }
        }
        return res;
    }

    public static String normalizeTextForFTS(String txt) {
        return normalizeTextForFTS(txt, false);
    }

    // błędny tekst wyszukiwania (tego nie ma sensu szukać) -> null
    // inne teksty poprawia:
    // 1) separatory, znaki działań, cudzysłowy, nawiasy itp. zamienia na spacje
    // 2) usuwa nadmiarowe białe znaki (z przodu i tyłu, powielone w środku)
    // gwiazdka (*) ma specjalne znaczenie - albo jest sama gwiazdka
    // (szukaj wszystkiego), albo gwiazdka na końcu ciągu znaków (np. ala*)
    // co oznacza szukanie po prefiksie
    // useSqlExt == true -> kropka i podkreślenie jest traktowane jak zwykły znak
    public static String normalizeTextForFTS(String txt, boolean useSqlExt) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(txt)) {
            return null;
        }

        char[] cs = txt.toCharArray();

        for (int i = 0; i < txt.length(); i++) {
            char c = cs[i];
            if (c > 127 || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '*'
                    || c >= 'A' && c <= 'Z' /* Return of the Misia Code Jedi! */ || (c == '"')
                    || (useSqlExt && (c == '_' || c == '.' || c == ':' || c == '-'))) {
                // c jest ok, nic nie robimy
            } else {
                cs[i] = ' ';
            }
        }

        String txtFix = (new String(cs)).trim();

        // zamień wielokrotne gwiazki na jedną
        txtFix = txtFix.replaceAll("[*]+", "*");
        // gwiazdka po jakiej jest nie spacja - zamiast gwiazdki będzie spacja,
        // reszta będzie obcięta
        txtFix = txtFix.replaceAll("[*][^ ]+", " ");
        // spacja + gwiazdka -> zostaje sama spacja bez gwiazdki
        txtFix = txtFix.replaceAll("[ ][*]", " ");
        // kompaktujemy wielokrotne spacje -> na jedną
        txtFix = txtFix.replaceAll("[ ]+", " ");
        // trimujemy ostatecznie
        txtFix = txtFix.trim();

        return txtFix;
    }

    public static List<Integer> getProperTreeIdsWithTreeKind(Collection<TreeBean> allTrees,
            final String treeKind) {
        //ww: wersja jawna - z pętlą - pętla jest copy-paste'owana za każdym
        // razem bez sensu, przypadki różnią się tylko warunkiem w IFie...
//        List<Integer> res = new ArrayList<Integer>();
//
//        for (TreeBean t : allTrees) {
//            if (BaseUtils.safeEquals(t.treeKind, treeKind)) {
//                res.add(t.id);
//            }
//        }
//
//        return res;
        return getProperTreeIdsEx(allTrees, new IPredicate<TreeBean>() {
            public Boolean project(TreeBean t) {
                return BaseUtils.safeEquals(t.treeKind, treeKind);
            }
        });
    }

    public static List<Integer> getProperTreeIdsWithoutOneTree(Collection<TreeBean> allTrees,
            final int treeId) {
        //ww: wersja jawna - z pętlą - pętla jest copy-paste'owana za każdym
        // razem bez sensu, przypadki różnią się tylko warunkiem w IFie...
//        List<Integer> res = new ArrayList<Integer>();
//
//        for (TreeBean t : allTrees) {
//            if (t.id != treeId) {
//                res.add(t.id);
//            }
//        }
//
//        return res;
        return getProperTreeIdsEx(allTrees, new IPredicate<TreeBean>() {
            public Boolean project(TreeBean t) {
                // odrzucamy "modrzew" (drzewo drzew)
                return t.id != treeId && !BaseUtils.safeEquals(t.code, BIKConstants.TREE_CODE_TREEOFTREES);
            }
        });
    }

    public static List<Integer> getProperTreeIdsEx(Collection<TreeBean> allTrees, IPredicate<TreeBean> isTreeProper) {
        List<Integer> res = new ArrayList<Integer>();

        for (TreeBean t : allTrees) {
            if (isTreeProper.project(t)) {
                res.add(t.id);
            }
        }

        return res;
    }
    private static String htmlUrlPatternPrefixEx = "<a([^>]+)" + "href[\\s]*=[\\s" /* I18N: no */ + "]*[\"\']#[a-zA-Z0-9]*/(";
    protected static final String htmlUrlPatternSuffixEx = ")[\"\']([^>]*)>([^<]+)</a>";

    public static String removeOrFixHrefsFromHtml(String html, Map<Integer, String> nodeIds, String urlForPublicAccess) {
        StringBuilder buffer = new StringBuilder();
        String sep = "";
        for (Integer id : nodeIds.keySet()) {
            buffer.append(sep);
            buffer.append(id.toString());
            sep = "|";
        }
        IRegExpPattern htmlUrlPattern
                = BaseUtils.getRegExpMatcherFactory().compile(htmlUrlPatternPrefixEx + buffer.toString() + htmlUrlPatternSuffixEx,
                        true, true);
        IRegExpMatcher htmlUrlMatcher = htmlUrlPattern.matcher(html);
        buffer.setLength(0);
        int index = 0;
        while (htmlUrlMatcher.find()) {
            String grp2 = htmlUrlMatcher.group(2);
            Integer nodeId = BaseUtils.tryParseInteger(grp2);
            String properTreeCode = nodeIds.get(nodeId);

            buffer.append(html.substring(index, htmlUrlMatcher.start()));
            if (properTreeCode == null) {
                // usuń
                buffer.append(htmlUrlMatcher.group(4));
            } else {
                // napraw treeCode
                buffer.append("<" + "a" /* I18N: no */).append(htmlUrlMatcher.group(1)).append(" " + "href" /* I18N: no */ + "='#")
                        .append(urlForPublicAccess)
                        .append(properTreeCode).append("/").append(nodeId).append("'").
                        append(htmlUrlMatcher.group(3)).append(">").append(htmlUrlMatcher.group(4)).append("</a>");
            }
            index = htmlUrlMatcher.end();
        }
        if (index < html.length()) {
            buffer.append(html.substring(index));
        }
        return buffer.toString();
    }

    public static void setupDateTimeBoxWithLimit(final DateBox dateBox, String valueOpt, String format) {
        DateTimeFormat dateFormat = DateTimeFormat.getFormat(format);
        dateBox.setFormat(new DateBox.DefaultFormat(dateFormat));
        List<String> l = BaseUtils.splitBySep(valueOpt, ",");
        if (l.size() == 2) {
            final Date dateFrom = SimpleDateUtils.parseFromCanonicalString(l.get(0));
            final Date dateTo = SimpleDateUtils.parseFromCanonicalString(l.get(1));
            if (dateFrom != null && dateTo != null) {
                dateBox.getDatePicker().addShowRangeHandler(new ShowRangeHandler<Date>() {

                    @Override
                    public void onShowRange(ShowRangeEvent<Date> event) {
                        Date start = event.getStart();
                        Date end = event.getEnd();
                        while (start.before(end)) {
                            if ((start.after(dateTo) || start.before(dateFrom)) && dateBox.getDatePicker().isDateVisible(start)) {
                                dateBox.getDatePicker().setTransientEnabledOnDates(false, start);
                            }
                            start = SimpleDateUtils.addDays(start, 1);
                        }
                    }
                });
            }
        }
    }

    public static void setupDateBoxWithLimit(final DateBox dateBox, String valueOpt) {
        setupDateTimeBoxWithLimit(dateBox, valueOpt, "yyyy-MM-dd");
        /* DateTimeFormat dateFormat = DateTimeFormat.getFormat("yyyy-MM-dd");
        dateBox.setFormat(new DateBox.DefaultFormat(dateFormat));
        List<String> l = BaseUtils.splitBySep(valueOpt, ",");
        if (l.size() == 2) {
            final Date dateFrom = SimpleDateUtils.parseFromCanonicalString(l.get(0));
            final Date dateTo = SimpleDateUtils.parseFromCanonicalString(l.get(1));
            if (dateFrom != null && dateTo != null) {
                dateBox.getDatePicker().addShowRangeHandler(new ShowRangeHandler<Date>() {

                    @Override
                    public void onShowRange(ShowRangeEvent<Date> event) {
                        Date start = event.getStart();
                        Date end = event.getEnd();
                        while (start.before(end)) {
                            if ((start.after(dateTo) || start.before(dateFrom)) && dateBox.getDatePicker().isDateVisible(start)) {
                                dateBox.getDatePicker().setTransientEnabledOnDates(false, start);
                            }
                            start = SimpleDateUtils.addDays(start, 1);
                        }
                    }
                });
            }
        }*/
    }

    public static enum MenuNodeBeanType {

        Group, Tree, TreeTemplate, NodeKind, CustomPage/*, ExternalTree*/

    };

    //ww: dla specjalnych typów menu zwraca '&', '@', '$', '#',
    // dla pozycji menu bez typu zwraca jakiś znak - pierwszą literę
    // dla pozycji z pustym kodem (code), zwraca ' ', dla nulla też ' '
    // czyli null traktowany jest jak menu grupowe
    public static MenuNodeBeanType getMenuNodeBeanType(MenuNodeBean mnb) {
        String code = mnb == null ? null : mnb.code;
        char c = BaseUtils.isStrEmpty(code) ? ' ' : code.charAt(0);

        switch (c) {
            case '@':
                return MenuNodeBeanType.TreeTemplate;
            case '$':
                return MenuNodeBeanType.Tree;
            case '&':
                return MenuNodeBeanType.NodeKind;
            case '#':
//                if (code != null && code.length() > 1 && code.charAt(1) == '$') {
//                    return MenuNodeBeanType.ExternalTree;
//                } else {
                return MenuNodeBeanType.CustomPage;
//                }
            default:
                return MenuNodeBeanType.Group;
        }
    }

    //ww: albo optType nie jest podany (null, wtedy true dla dowolnego menu),
    // albo typ menu się zgadza z optType
    public static boolean isMenuOfOptType(MenuNodeBean mnb, MenuNodeBeanType optType) {
        return optType == null || getMenuNodeBeanType(mnb) == optType;
    }

    public static String createUrl(String url) {
        return createUrlEx(url, null);
    }

    public static String createUrlEx(String url, String optLinkCaptionOverride) {
        url = BaseUtils.trimAndCompactSpaces(url);
        Pair<String, String> ur = BaseUtils.splitString(url, " ");
        url = ur.v1;
        String nameUrl = BaseUtils.nullToDef(optLinkCaptionOverride, ur.v2);
        return "<" + "a href" /* I18N: no */ + "=" + (isUrlGlobal(url) ? "" : "http" /* I18N: no */ + "://") + url + "  " + "target" /* I18N: no */ + "=\"_blank\">" + (nameUrl != null ? nameUrl : url) + "</a>";
    }

    public static boolean isUrlGlobal(String url) {
        return (url.startsWith("http" /* I18N: no */ + "://") || url.startsWith("https" /* I18N: no */ + "://"));
    }

    public static String createSQLSyntaxHighlighterHTML(String plainHTML) {
        if (plainHTML == null) {
            return null;
        }
        return SyntaxHighlighter.highlight(plainHTML, BrushFactory.newSqlBrush(), false);
    }

    public static boolean getAppPropValAsBool(String valAsStr) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(valAsStr)) {
            return false;
        }
        valAsStr = valAsStr.toLowerCase().trim();
        return valAsStr.equals("1") || valAsStr.equals("true" /* I18N: no */);
    }

    public static Widget createHtmlLine(String size) {
        return new HTML("<" + "HR WIDTH" /* I18N: no */ + "=\"" + size + "\" " + "ALIGN=LEFT" /* I18N: no */ + ">");
    }

    public static String trimRemoteUserByDomain(String remoteUser) {
        if (remoteUser.contains("\\")) {
            remoteUser = BaseUtils.splitString(remoteUser, "\\").v2;
        }
        return remoteUser;
    }

    public static Map<String, JoinedObjsTabKindEx> makeTreeKindToJoinedObjsTabKindMap(Collection<String> dynamicTreeKinds) {
        Map<String, JoinedObjsTabKindEx> map = new HashMap<String, JoinedObjsTabKindEx>();

        map.put(BIKGWTConstants.TREE_KIND_METADATA, JoinedObjsTabKindEx.Metadata);
        map.put(BIKGWTConstants.TREE_KIND_CONFLUENCE, JoinedObjsTabKindEx.Confluence);
        map.put(BIKGWTConstants.TREE_KIND_QUALITY_METADATA, JoinedObjsTabKindEx.Dqc);
        map.put(BIKGWTConstants.TREE_KIND_GLOSSARY, JoinedObjsTabKindEx.Glossary);
        map.put(BIKGWTConstants.TREE_KIND_DICTIONARY, JoinedObjsTabKindEx.Dictionary);
        map.put(BIKGWTConstants.TREE_KIND_DICTIONARY_DWH, JoinedObjsTabKindEx.Dictionary);

        map.put(BIKGWTConstants.TREE_KIND_DYNAMIC_TREE, JoinedObjsTabKindEx.Taxonomy);
        for (String treeKind : dynamicTreeKinds) {
            map.put(treeKind, JoinedObjsTabKindEx.Taxonomy);
        }

        map.put(BIKGWTConstants.TREE_KIND_BLOGS, JoinedObjsTabKindEx.Blogs);
        map.put(BIKGWTConstants.TREE_KIND_DOCUMENTS, JoinedObjsTabKindEx.Documents);

        return map;
    }

    public static String actionsDescendantsObjectInFvs(ObjectInFvsChangeExBean o) {
        Set<String> actionsInFvs = new LinkedHashSet<String>();
        if (o.nodeState == -1) {
            actionsInFvs.add(I18n.descendantsObjectInFvsDeleted.get());
        } else {
            if (o.nodeState == 1) {
                actionsInFvs.add(I18n.descendantsObjectInFvsNew.get());
            }
            if (!BaseUtils.isStrEmpty(o.joinedObjIdsAdded)) {
                actionsInFvs.add(I18n.descendantsObjectInFvsJoinedObjIdsAdded.get() + ": " + BaseUtils.splitBySep(o.joinedObjIdsAdded, "|").size());
            }
            if (!BaseUtils.isStrEmpty(o.joinedObjIdsDeleted)) {
                actionsInFvs.add(I18n.descendantsObjectInFvsJoinedObjIdsDeleted.get() + ": " + BaseUtils.splitBySep(o.joinedObjIdsDeleted, "|").size());
            }
            if (!BaseUtils.isStrEmpty(o.changedAttrs)) {

                actionsInFvs.add(I18n.descendantsObjectInFvsChangedAttrs.get() + ": " + o.changedAttrs/* + BaseUtils.splitBySep(o.changedAttrs, "|").size()*/);
            }
            if (o.dqcTestFailCnt > 0) {
                actionsInFvs.add(I18n.descendantsObjectInFvsDqcTestFailCnt.get() + ": " + o.dqcTestFailCnt);
                actionsInFvs.add("Ostatnie wykonanie testu: " + (o.code.equals(BIKConstants.NODE_KIND_DQC_TEST_SUCCESS) ? "pozytywne" : "negatywne"));

            }
            if (o.newCommentCnt > 0) {
                actionsInFvs.add(I18n.descendantsObjectInFvsNewCommentCnt.get() + ": " + o.newCommentCnt);
            }
            if (o.voteCntDelta != 0 || o.voteValDelta != 0) {

                if (o.voteValDelta != 0) {
                    actionsInFvs.add((o.voteValDelta > 0 ? I18n.descendantsObjectInFvsVoteValDeltaAdd.get()
                            : I18n.descendantsObjectInFvsVoteValDeltaDelete.get()) + ": " + Math.abs(o.voteCntDelta));
                }
                if (o.voteCntDelta != 0) {
                    actionsInFvs.add((o.voteCntDelta > 0 ? I18n.descendantsObjectInFvsVoteCntDeltaAdd.get()
                            : I18n.descendantsObjectInFvsVoteCntDeltaDelete.get()) + ": " + Math.abs(o.voteCntDelta));
                }
            }

            if (o.cutParentId != null && o.cutParentId > 0) {
                actionsInFvs.add(I18n.descendantsObjectInFvsCut.get());
            }
            if (o.isPasted > 0) {
                actionsInFvs.add(I18n.descendantsObjectInFvsPast.get() /*+ ": " + o.isPasted*/);
            }

        }
        return BaseUtils.mergeWithSepEx(actionsInFvs, ", ");
    }

    /* BI INSTANCE */
    public static String getActionObjectInFvs(FvsChangeExBean n) {
        Set<String> actionFvs = new HashSet<String>();
        Map<String, Integer> ac = new HashMap<String, Integer>();
        ac.put(I18n.usuniete.get() + ":", n.cntDelete);
        ac.put(I18n.usuniete.get() + ":", n.cntDelete);
        ac.put(I18n.nowe.get() + ":", n.cntNew);
        ac.put(I18n.objectInFvsCntUpdate.get(), n.cntUpdate);
        ac.put(I18n.objectInFvsCntJoinedObjChange.get(), n.cntJoinedObjChange);
        ac.put(I18n.objectInFvsCntNewComments.get(), n.cntNewComments);
        ac.put(I18n.objectInFvsCntVoteChange.get(), n.cntVoteChange);
        ac.put(I18n.objectInFvsCntDqcTestFails.get(), n.cntDqcTestFails);
        ac.put(I18n.objectInFvsCntCut.get() + ":", n.cutParentId);
        ac.put(I18n.objectInFvsCntPast.get() + ":", n.isPasted);

        for (Entry<String, Integer> em : ac.entrySet()) {
            Integer cnt = em.getValue();
            if (cnt != null && cnt > 0) {
                String action = em.getKey();
                actionFvs.add(action + " " + cnt
                        + (action.contains(":") ? "" : " "
                        + (cnt == 1 ? I18n.objectInFvsCntObject.get() : I18n.objectInFvsCntObjects.get())));
            }
        }
        return BaseUtils.mergeWithSepEx(actionFvs, ", ");
    }

    public static String getDuration(SAPBOScheduleBean object) {
        if (object.getEnded() == null) {
            return "00:00:00";
        } else {
            Date now = new Date();
            Date truncateDay = SimpleDateUtils.truncateDay(now);
            Date addMillis = SimpleDateUtils.addMillis(truncateDay, object.ended.getTime() - object.started.getTime());
            return SimpleDateUtils.timeToString(addMillis, false);
        }
    }

    public static String getStatus(int scheduleType) {
        String toReturn = "";
        switch (scheduleType) {
            case 0: //ISchedulingInfo.ScheduleStatus.RUNNING:
                toReturn = I18n.scheduleRunning.get() /* I18N:  */;
                break;
            case 1: //ISchedulingInfo.ScheduleStatus.COMPLETE:
                toReturn = I18n.scheduleComplete.get() /* I18N:  */;
                break;
            case 3: //ISchedulingInfo.ScheduleStatus.FAILURE:
                toReturn = I18n.scheduleFailure.get() /* I18N:  */;
                break;
            case 8: //ISchedulingInfo.ScheduleStatus.PAUSED:
                toReturn = I18n.schedulePaused.get() /* I18N:  */;
                break;
            case 9: //ISchedulingInfo.ScheduleStatus.PENDING:
                toReturn = I18n.schedulePending.get() /* I18N:  */;
                break;
        }
        return toReturn;
    }

    public static String getRecurring(int reccuring) {
        return reccuring == 1 ? I18n.tak.get() : I18n.nie.get();
    }

    public static boolean isCheckboxOrComboBoxType(String attrTypeName) {
        return attrTypeName.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.checkbox.name())
                || attrTypeName.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.combobox.name()) /* || name.equals(AttributeType.selectOneJoinedObject.name())*/;
    }

    public static boolean isBooleanType(String attrType) {
        return attrType.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.comboBooleanBox.name());
    }

    public static boolean isDateType(String attrType) {
        return attrType.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.data.name())
                || attrType.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.datetime.name());
    }

    public static boolean isShortTextType(String attrType) {
        return attrType.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.shortText.name());
    }
}
