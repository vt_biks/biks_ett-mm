exec sp_check_version '2.0.11';
GO

if not exists (select 1 from bik_app_prop where name='showAddStatus' )
insert into bik_app_prop(name, val) values ('showAddStatus', 'false')
else 
update bik_app_prop set val='false' where name='showAddStatus'
go 


update bik_attribute set is_required=0,is_visible=0,is_deleted=1  where attr_def_id in (select id from bik_attr_Def where name like 'metaBiks.Status')


exec sp_update_version '2.0.11','2.0.12';
GO
