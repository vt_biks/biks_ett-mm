/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

/**
 *
 * @author mmastalerz
 */
public interface IPaginatedItemsProvider<T> {

//    //wskaźnik refetch odpowiada na pytanie, czy ponownie zaciągnąć listę beanów z bazy?
//    //wskaźnik resetPosition odpowiada na pytanie, czy po odświeżeniu ma przejść na pierwszą stronę?
//    public void refresh(boolean refetch, boolean resetPosition, List<T> allItems);
    
    //---
    public void refetchItems(boolean resetPosition);
    //---
    public void recalcItemsToDisplay(boolean resetPosition);
    //---
    public void itemsChanged();
}
