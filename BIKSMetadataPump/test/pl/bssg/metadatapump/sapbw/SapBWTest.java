/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbw;

import commonlib.LameUtils;
import java.util.List;
import java.util.Map;
import simplelib.BaseUtils;
import simplelib.CSVReader;

/**
 *
 * @author tflorczak
 */
public class SapBWTest {

    public static void main(String[] args) {

        String csvContent = LameUtils.loadAsString("C:\\Projekty\\Konektor Sap Bw\\Paczka 6\\ZTCT_META.CSV");
        CSVReader reader = new CSVReader(csvContent, false);
        List<Map<String, String>> content = reader.readRowsAsMaps(BaseUtils.splitBySep("INFOCUBE,IPLEVEL,PARTCUBE,IOBJNM,FILETYPE,TCTBWOTYPE,INFOPROVUP,INFOPROVTXT,IOBJTXT,ENDLEVEL,INFOAREA,IOBJTYPE,QUERYUPDATE,QUERYOWNER,QUERYEDIT", ","));
        System.out.println("list size: " + content.size());
    }
}
