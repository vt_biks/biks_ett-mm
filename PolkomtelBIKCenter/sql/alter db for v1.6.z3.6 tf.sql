﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- Konektor do IDT SDK
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_check_version '1.6.z3.6';
go

alter table aaa_universe alter column universe_connetion_id int null;
go

alter table ami_universe alter column universe_connetion_id int null;
go

alter table aaa_universe_class alter column si_id_designer varchar(900) null;
go

alter table ami_universe_class alter column si_id_designer varchar(900) null;
go

alter table aaa_universe_obj alter column obj_branch varchar(900) null;
go

alter table ami_universe_obj alter column obj_branch varchar(900) null;
go

alter table aaa_universe_filter alter column filtr_branch varchar(900) null;
go

alter table ami_universe_filter alter column filtr_branch varchar(900) null;
go

alter table aaa_universe_table alter column their_id int null;
go

alter table ami_universe_table alter column their_id int null;
go

if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_sapbo_universe_table]') and name = N'UNIQ_unique_their_id_bik_sapbo_universe_table')
begin
	alter table bik_sapbo_universe_table drop constraint UNIQ_unique_their_id_bik_sapbo_universe_table;
	alter table bik_sapbo_universe_table alter column their_id varchar(256) not null;
	alter table bik_sapbo_universe_table alter column original_table varchar(256) null;
	alter table bik_sapbo_universe_table add constraint UQ_bik_sapbo_universe_table_their_id_node_id unique (their_id, node_id);
end;
go


if not exists(select 1 from bik_admin where param = 'sapbo4.jREx86Path')
begin
	insert into bik_admin(param, value) values('sapbo4.jREx86Path', '')
end;
go


if not exists (select * from bik_node_kind where code = 'attribute')
begin
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('Attribute', 'Atrybut', 'attribute', 'Metadata', 0, 12, 0)
	
	insert into bik_translation(code, txt, lang, kind)
	values ('Attribute', 'Attribute', 'en', 'kind')
	
	insert into bik_icon(name)
	values ('attribute')
	
	declare @attr int = dbo.fn_node_kind_id_by_code('Attribute')
	declare @selectId int, @whereId int, @typId int
	select @selectId = id from bik_attr_def where name = 'Select' and is_built_in = 1
	select @whereId = id from bik_attr_def where name = 'Where' and is_built_in = 1
	select @typId = id from bik_attr_def where name = 'Typ' and is_built_in = 1

	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values (@attr, @selectId, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values (@attr, @whereId, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values (@attr, @typId, 1)
	
	--select * from bik_node where tree_id = 17 order by branch_ids
end;

if not exists(select * from sys.objects where object_id = object_id(N'[aaa_universe_connection_idt]') and type in (N'U'))
begin 
	create table aaa_universe_connection_idt (
		id int identity(1,1) not null primary key,
		si_id int not null unique,
		connetion_name varchar(900) not null,
		connetion_path varchar(900) not null,
		server varchar(255) null,
		user_name varchar(255) null,
		password varchar(255) null,
		database_source varchar(500) null,
		network_layer varchar(500) not null,
		type varchar(255) null,
		database_engine varchar(255) null,
		client_number int null,
		language varchar(10) null,
		system_number varchar(255) null,
		system_id varchar(255) null,
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'[ami_universe_connection_idt]') and type in (N'U'))
begin 
	create table ami_universe_connection_idt (
		server_instance_id int not null references bik_sapbo_server(id),
		id int not null,
		si_id int not null unique,
		connetion_name varchar(900) not null,
		connetion_path varchar(900) not null,
		server varchar(255) null,
		user_name varchar(255) null,
		password varchar(255) null,
		database_source varchar(500) null,
		network_layer varchar(500) not null,
		type varchar(255) null,
		database_engine varchar(255) null,
		client_number int null,
		language varchar(10) null,
		system_number varchar(255) null,
		system_id varchar(255) null,
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'[ami_app_dsl_metadatafile]') and type in (N'U'))
begin 
	create table ami_app_dsl_metadatafile (
		server_instance_id int not null references bik_sapbo_server(id),
		si_id int not null,
		si_name varchar(max) null,
		si_parent_folder_cuid varchar(max) null,
		si_update_ts datetime null,
		si_cuid varchar(max) null
	);
end;
go


if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_universe_table') and name = 'idt_connection_id')
begin
	alter table aaa_universe_table add idt_connection_id int null references aaa_universe_connection_idt(id) ;
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('ami_universe_table') and name = 'idt_connection_id')
begin
	alter table ami_universe_table add idt_connection_id int null;
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_universe_table') and name = 'idt_id')
begin
	alter table aaa_universe_table add idt_id varchar(500) null;
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('ami_universe_table') and name = 'idt_id')
begin
	alter table ami_universe_table add idt_id varchar(500) null;
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_universe_table') and name = 'idt_original_id')
begin
	alter table aaa_universe_table add idt_original_id varchar(500) null;
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('ami_universe_table') and name = 'idt_original_id')
begin
	alter table ami_universe_table add idt_original_id varchar(500) null;
end;
go

if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[aaa_universe_class]') and name = N'UNIQ_unique_si_id_aaa_universe_class')
begin
	alter table aaa_universe_class drop constraint UNIQ_unique_si_id_aaa_universe_class;
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[aaa_universe_class]') and name = N'UQ_aaa_universe_class_si_id_designer')
begin
	alter table aaa_universe_class add constraint UQ_aaa_universe_class_si_id_designer unique (si_id_designer);
end;
go

if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[aaa_universe_obj]') and name = N'UNIQ_unique_si_id_aaa_universe_obj')
begin
	alter table aaa_universe_obj drop constraint UNIQ_unique_si_id_aaa_universe_obj;
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[aaa_universe_obj]') and name = N'UQ_aaa_universe_obj_obj_branch')
begin
	alter table aaa_universe_obj add constraint UQ_aaa_universe_obj_obj_branch unique (obj_branch);
end;
go

if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[aaa_universe_filter]') and name = N'UNIQ_unique_si_id_universe_class_id')
begin
	alter table aaa_universe_filter drop constraint UNIQ_unique_si_id_universe_class_id;
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[aaa_universe_filter]') and name = N'UQ_aaa_universe_filter_filtr_branch')
begin
	alter table aaa_universe_filter add constraint UQ_aaa_universe_filter_filtr_branch unique (filtr_branch);
end;
go

if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[aaa_universe_table]') and name = N'UNIQ_unique_their_id_aaa_universe_table')
begin
	alter table aaa_universe_table drop constraint UNIQ_unique_their_id_aaa_universe_table;
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[aaa_universe_table]') and name = N'UQ_unique_their_id_idt_id_aaa_universe_table')
begin
	alter table aaa_universe_table add constraint UQ_unique_their_id_idt_id_aaa_universe_table unique (their_id, idt_id, universe_id);
end;
go

if not exists (select * from sys.foreign_keys where parent_object_id = object_id(N'[dbo].[bik_sapbo_universe_column]') and name = N'FK_bik_sapbo_universe_column_bik_node_id')
begin
	alter table bik_sapbo_universe_column add constraint FK_bik_sapbo_universe_column_bik_node_id foreign key (table_id) references bik_node (id);
end;
go


-- poprzednia wersja w: alter db for v1.3.0.10 tf.sql
-- fix pod IDT SDK
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_objects_from_Designer]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]
go

create procedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]
as
begin--begin procedure

	declare @diag_level int = 0;

	----wybranie id odpowiedniego drzewa
	declare @tree_id int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	
	----wybranie id odpowiedniego kind'a
	declare @class_kind_id int = dbo.fn_node_kind_id_by_code('UniverseClass');
	declare @folder_universe_node_kind_id int = dbo.fn_node_kind_id_by_code('UniversesFolder');
	declare @detail_nki int = dbo.fn_node_kind_id_by_code('Detail');
	declare @attribute_nki int = dbo.fn_node_kind_id_by_code('Attribute');
	declare @dimension_nki int = dbo.fn_node_kind_id_by_code('Dimension');
	declare @measure_nki int = dbo.fn_node_kind_id_by_code('Measure');
	declare @filtr_nki int = dbo.fn_node_kind_id_by_code('Filter');
	declare @folder_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseTablesFolder');
	declare @table_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseTable');
	declare @table_alias_node_kind_id int =  dbo.fn_node_kind_id_by_code('UniverseAliasTable');
	declare @table_derived_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseDerivedTable');
	declare @universe_kind_id int = dbo.fn_node_kind_id_by_code('Universe');
	declare @universe_unx_kind_id int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
	declare @universe_column_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseColumn');
		
	create table #tmpNodes (si_id varchar(1000) collate DATABASE_DEFAULT, si_parentid varchar(1000) collate DATABASE_DEFAULT, si_kind_id int, si_name varchar(max) collate DATABASE_DEFAULT, si_description varchar(max) collate DATABASE_DEFAULT);

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Wrzucanie do tabeli tymczasowej #tmpNodes'
	
	----wrzucam do tymczasowej tabeli wszystkie dane z Universe_Class
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select si_id_designer, si_parentid, @class_kind_id, name, description
	from aaa_universe_class  
	
	
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Obj
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select obj_branch, si_parentid, 
			case
				when qualification = 'dsDetailObject' then @detail_nki
				when qualification = 'dsMeasureObject' then @measure_nki
				when qualification = 'dsDimensionObject' then @dimension_nki
				when qualification = 'Attribute' then @attribute_nki
				end as node_kind_id,
		 name, description
	from aaa_universe_obj
							 						 
	--wrzucam do tabeli tymczasowej wszystkie dane z Universe_filrt
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select uf.filtr_branch,	uc.si_id_designer, @filtr_nki, uf.name, coalesce(ltrim(rtrim(uf.description)), '') 
	from aaa_universe_filter uf 
	inner join aaa_universe_class uc on uf.universe_class_id = uc.id

	--wrzucam katalogi do których są wrzucane tabele
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)
	select convert(varchar(10),global_props_si_id) + '|tableCatalog', convert(varchar(10),global_props_si_id), @folder_node_kind_id, 'Tabele'
	from aaa_universe 

							 
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Table	
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)		
	select convert(varchar(256), their_id) + '|' + convert(varchar(256), universe_branch), convert(varchar(1000), universe_branch) + '|tableCatalog', 
	case
				when is_alias = 1 then @table_alias_node_kind_id
				when is_derived = 1 then @table_derived_node_kind_id
				else @table_node_kind_id
				end as node_kind_id, name 
	from aaa_universe_table
	where their_id is not null
	union all -- tabele z IDT
	select convert(varchar(256), idt_id) + '|' + convert(varchar(256), universe_branch), convert(varchar(1000), universe_branch) + '|tableCatalog', 
	case
				when is_alias = 1 then @table_alias_node_kind_id
				when is_derived = 1 then @table_derived_node_kind_id
				else @table_node_kind_id
				end as node_kind_id, name 
	from aaa_universe_table
	where idt_id is not null
	
	
	----wrzucam do tymczasowej tabeli wszystkie dane z Universe_Column
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar(256), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch) + '|' + uc.name, 
		convert(varchar(256), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch), @universe_column_node_kind_id, uc.name, REPLACE(REPLACE(uc.type,'ds',''),'Column','')
	from aaa_universe_table ut 
	inner join aaa_universe_column uc on ut.id = uc.universe_table_id
	where ut.their_id is not null
	union all -- kolumny z IDT
	select convert(varchar(256), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch) + '|' + uc.name, 
		convert(varchar(256), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch), @universe_column_node_kind_id, uc.name, uc.type
	from aaa_universe_table ut 
	inner join aaa_universe_column uc on ut.id = uc.universe_table_id
	where ut.idt_id is not null
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Update bik_node'
	
	--aktualizacja istniejących węzłów w drzewie							
	update bik_node 
	set parent_node_id = null, node_kind_id = #tmpNodes.si_kind_id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.si_description
	from #tmpNodes 
	where bik_node.tree_id = @tree_id and bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id and bik_node.linked_node_id is null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Insert into bik_node'
	
	--dorzucanie nowych węzłów w drzewie
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select #tmpNodes.si_kind_id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.si_description
	from #tmpNodes left join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where bik_node.id is null;	
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Update parentów'
	
	--uaktualnianie parentów w nodach
	update bik_node 
	set parent_node_id = bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.tree_id = @tree_id and bik_node.obj_id = pt.si_id and bik_node.node_kind_id = pt.si_kind_id
		inner join bik_node bk on bk.tree_id = @tree_id and bk.obj_id = pt.si_parentid
		and  (
		 (pt.si_kind_id = @detail_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @class_kind_id)) or
		 (pt.si_kind_id = @dimension_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @class_kind_id)) or
		 (pt.si_kind_id = @measure_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @class_kind_id)) or
		 (pt.si_kind_id = @attribute_nki and (bk.node_kind_id = @dimension_nki or bk.node_kind_id = @measure_nki)) or
		 (pt.si_kind_id = @filtr_nki and bk.node_kind_id=@class_kind_id) or
		 (pt.si_kind_id = @universe_column_node_kind_id and (bk.node_kind_id=@table_node_kind_id or bk.node_kind_id=@table_alias_node_kind_id or bk.node_kind_id=@table_derived_node_kind_id)) or
		 (pt.si_kind_id = @class_kind_id and (bk.node_kind_id = @class_kind_id or bk.node_kind_id in (@universe_kind_id, @universe_unx_kind_id)))or
		 ((pt.si_kind_id = @table_node_kind_id or pt.si_kind_id=@table_alias_node_kind_id or pt.si_kind_id=@table_derived_node_kind_id) and bk.node_kind_id=@folder_node_kind_id)or
		 (pt.si_kind_id = @folder_node_kind_id and bk.node_kind_id in (@universe_kind_id, @universe_unx_kind_id))
		 )
	where bk.linked_node_id is null and bik_node.parent_node_id is null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Usuwanie nodów'
		
	--usuwanie nodów
	update bik_node
	set is_deleted = 1
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and bik_node.node_kind_id in (@class_kind_id, @detail_nki, @dimension_nki, @measure_nki,
	  @filtr_nki, @attribute_nki, @folder_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id, @table_node_kind_id, @universe_column_node_kind_id);
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Usuwanie błędnych danych w Universum'
	
	-- usuwanie obiektow, wrzuconych ze zlym parentem --> blad w Universum  
	update bik_node
	set is_deleted = 1
	where parent_node_id is null and tree_id = @tree_id and node_kind_id not in (@universe_kind_id, @universe_unx_kind_id, @folder_universe_node_kind_id)
	
	drop table #tmpNodes;
	--uzupełnianie danych w tabelach extra 
	--dla object
	create table #tmpObjExtra(text_of_select varchar(max) collate DATABASE_DEFAULT, text_of_where varchar(max) collate DATABASE_DEFAULT, type varchar(155) collate DATABASE_DEFAULT, aggregate_function varchar(50) collate DATABASE_DEFAULT, node_id int);
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Wrzucanie do tableli tymczasowej #tmpObjExtra'
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select univ_obj.text_of_select, univ_obj.text_of_where, univ_obj.type, univ_obj.aggregate_function, bik_node.id
	from aaa_universe_obj univ_obj join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = univ_obj.obj_branch 
		and bik_node.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki, @attribute_nki)-- = @obj_node_kind_id;
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select null, uf.where_clause, uf.type, null, bik_node.id
	from aaa_universe_filter uf inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = uf.filtr_branch and bik_node.node_kind_id = @filtr_nki
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Wrzucanie do tabel bik_sapbo...'
	
	update bik_sapbo_universe_object
	set text_of_select = #tmpObjExtra.text_of_select, text_of_where = #tmpObjExtra.text_of_where, type = #tmpObjExtra.type,  
		           aggregate_function = #tmpObjExtra.aggregate_function 
	from #tmpObjExtra join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id;
	
	insert into bik_sapbo_universe_object(text_of_select, text_of_where, type, aggregate_function, node_id)
	select #tmpObjExtra.text_of_select,#tmpObjExtra.text_of_where, #tmpObjExtra.type, #tmpObjExtra.aggregate_function, #tmpObjExtra.node_id 
	from #tmpObjExtra left join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id
	where bik_sapbo_universe_object.id is null;

	drop table #tmpObjExtra;
	
	--dla tabel z designera
	update bik_sapbo_universe_table
	set is_alias = ut.is_alias, is_derived = ut.is_derived, original_table = convert(varchar(512), ut.original_table), sql_of_derived_table = ut.sql_of_derived_table, 
		sql_of_derived_table_with_alias = ut.sql_of_derived_table_with_alias, node_id = bik_node.id, type = ucn.name
	from aaa_universe_table ut 
		inner join aaa_universe u on ut.universe_id = u.id 
		inner join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		inner join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch)
		inner join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512), ut.their_id) and bsut.node_id = bik_node.id
	where bik_node.linked_node_id is null and bik_node.is_deleted = 0 
		and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
		and ut.their_id is not null
	
	--dla tabel z idt
	update bik_sapbo_universe_table
	set is_alias = ut.is_alias, is_derived = ut.is_derived, original_table = convert(varchar(512), ut.idt_original_id), sql_of_derived_table = ut.sql_of_derived_table, 
		sql_of_derived_table_with_alias = ut.sql_of_derived_table_with_alias, node_id = bik_node.id, type = conidt.network_layer
	from aaa_universe_table ut 
		inner join aaa_universe_connection_idt conidt on ut.idt_connection_id = conidt.id
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch)
		inner join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512), ut.idt_id) and bsut.node_id = bik_node.id
	where bik_node.linked_node_id is null and bik_node.is_deleted = 0 
		and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
		and ut.idt_id is not null
	
	insert into bik_sapbo_universe_table(is_alias, is_derived, original_table, sql_of_derived_table, sql_of_derived_table_with_alias, their_id, node_id, type)
	select ut.is_alias, ut.is_derived, convert(varchar(512), ut.original_table), ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, convert(varchar(512), ut.their_id), 
					bik_node.id as node_id, ucn.name
	from aaa_universe_table ut 
		inner join aaa_universe u on ut.universe_id = u.id 
		inner join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		inner join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch)
		left join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512),ut.their_id) and bsut.node_id = bik_node.id
	where bik_node.linked_node_id is null and bik_node.is_deleted = 0
		and bsut.id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
		and ut.their_id is not null
	union all -- dla idt
	select ut.is_alias, ut.is_derived, convert(varchar(512), ut.idt_original_id), ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, convert(varchar(512), ut.idt_id), 
					bik_node.id as node_id, conidt.network_layer
	from aaa_universe_table ut
		inner join aaa_universe_connection_idt conidt on ut.idt_connection_id = conidt.id
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch)
		left join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512),ut.idt_id) and bsut.node_id = bik_node.id
	where bik_node.linked_node_id is null and bik_node.is_deleted = 0
		and bsut.id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
		and ut.idt_id is not null

	--uzupełnianie name_for_teradata: Teradata i Oracle (network_layer na razie tylko z 3.1)
	update bik_sapbo_universe_table
	set name_for_teradata = replace(bik_node.name,'.', '|') + '|'
	from bik_sapbo_universe_table 
	inner join bik_node on bik_sapbo_universe_table.node_id = bik_node.id
	where bik_sapbo_universe_table.type in ('Teradata', 'ODBC', 'Oracle OCI', 'ODBC Drivers') and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 0

	update bik_sapbo_universe_table
	set name_for_teradata = tabo.name_for_teradata
	from bik_sapbo_universe_table 
	inner join bik_sapbo_universe_table tabo on bik_sapbo_universe_table.original_table = tabo.their_id 
	inner join bik_node bn on bn.id = bik_sapbo_universe_table.node_id
	inner join bik_node bn2 on bn2.id = tabo.node_id
	where bik_sapbo_universe_table.type in ('Teradata', 'ODBC', 'Oracle OCI', 'ODBC Drivers') and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 1 
	and bn2.parent_node_id = bn.parent_node_id
	
	update bik_sapbo_universe_table
	set schema_name = left(name_for_teradata, charindex('|',name_for_teradata))
	where type in ('Teradata', 'ODBC', 'Oracle OCI', 'ODBC Drivers') and is_derived = 0
	
	--dla kolumn z designera
	update bik_sapbo_universe_column
	set /*name = uc.name, */type = uc.type--, table_id = bik_node.id
	from aaa_universe_column uc 
		inner join aaa_universe_table ut on uc.universe_table_id = ut.id
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch)
		inner join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512), ut.their_id) and bsut.node_id = bik_node.id
		inner join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bik_node.id
	where linked_node_id is null and bik_node.is_deleted = 0
	and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	and ut.their_id is not null
	
	--dla kolumn z idt
	update bik_sapbo_universe_column
	set /*name = uc.name, */type = uc.type--, table_id = bik_node.id
	from aaa_universe_column uc 
		inner join aaa_universe_table ut on uc.universe_table_id = ut.id
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch)
		inner join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512), ut.idt_id) and bsut.node_id = bik_node.id
		inner join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bik_node.id
	where linked_node_id is null and bik_node.is_deleted = 0
	and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	and ut.idt_id is not null
	
	insert into bik_sapbo_universe_column(name, type, table_id)
	select uc.name, uc.type, bik_node.id
	from aaa_universe_column uc 
		inner join aaa_universe_table ut on uc.universe_table_id = ut.id
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch)
		inner join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512), ut.their_id) and bsut.node_id = bik_node.id
		left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bik_node.id
	where linked_node_id is null and bik_node.is_deleted = 0
	and bsuc.id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	and ut.their_id is not null
	union all -- dla idt
	select uc.name, uc.type, bik_node.id
	from aaa_universe_column uc 
		inner join aaa_universe_table ut on uc.universe_table_id = ut.id
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch)
		inner join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512), ut.idt_id) and bsut.node_id = bik_node.id
		left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bik_node.id
	where linked_node_id is null and bik_node.is_deleted = 0
	and bsuc.id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	and ut.idt_id is not null

	--delete from bik_sapbo_universe_table i bik_sapbo_universe_column nie jest potrzebne bo w bik_node jest ustawiont isDeleted na 1:)
	
	delete from bik_sapbo_universe_connection 
	from bik_sapbo_universe_connection conn
	inner join bik_node bn on bn.id = conn.node_id
	where bn.tree_id = @connectionTreeId
	
	-- dla polaczen z Designera
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id, type, database_engine, client_number, language, system_number, system_id)
	select aaa_universe_connection.server, aaa_universe_connection.user_name, aaa_universe_connection.password, aaa_universe_connection.database_source, 
		aaa_universe_connection_networklayer.name, bik_node.id,
		aaa_universe_connection.type, aaa_universe_connection.database_enigme, aaa_universe_connection.client_number, aaa_universe_connection.language,
		aaa_universe_connection.system_number, aaa_universe_connection.system_id
	from aaa_universe_connection 
		inner join aaa_universe_connection_networklayer on aaa_universe_connection.connetion_networklayer_id = aaa_universe_connection_networklayer.id
		inner join bik_node on bik_node.tree_id = @connectionTreeId and aaa_universe_connection.connetion_name = bik_node.name and bik_node.is_deleted = 0
		left join bik_sapbo_universe_connection on bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_sapbo_universe_connection.id is null;
	
	-- dla polaczen z IDT
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id, type, database_engine, client_number, language, system_number, system_id)
	select aaa_universe_connection_idt.server, aaa_universe_connection_idt.user_name, aaa_universe_connection_idt.password, aaa_universe_connection_idt.database_source, 
		aaa_universe_connection_idt.network_layer, bik_node.id,
		aaa_universe_connection_idt.type, aaa_universe_connection_idt.database_engine, aaa_universe_connection_idt.client_number, aaa_universe_connection_idt.language,
		aaa_universe_connection_idt.system_number, aaa_universe_connection_idt.system_id
	from aaa_universe_connection_idt
		inner join bik_node on bik_node.tree_id = @connectionTreeId and convert(varchar(10),aaa_universe_connection_idt.si_id) = convert(varchar(10),bik_node.obj_id) and bik_node.is_deleted = 0
		left join bik_sapbo_universe_connection on bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_sapbo_universe_connection.id is null;	
	
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Update extradaty'
	
	-- dodanie statystyk dla światów obiektów	
	update bik_sapbo_extradata
	set statistic = u.statistic
	from bik_sapbo_extradata 
		inner join bik_node bn on bn.id = bik_sapbo_extradata.node_id
		inner join aaa_universe u on convert(varchar(30),u.global_props_si_id) = bn.obj_id
	where bn.node_kind_id = @universe_kind_id
		and bn.tree_id = @tree_id
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Ustawianie visual_order'
	
	-- ustawianie visual order dla folderów tabel
	update bik_node set visual_order = -1 where node_kind_id = @folder_node_kind_id
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Wykonywanie procedur: sp_delete_linked i sp_node_init_branch'
		
	exec sp_delete_linked_nodes_where_orignal_is_deleted;	
	exec sp_node_init_branch_id @tree_id, null;
end;
go

-- poprzednia wersja w pliku: alter db for v1.3.0.6 tf.sql
-- fix pod IDT SDK
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_object_table]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbo_object_table]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbo_object_table]
as
begin

	declare @tree_id int = dbo.fn_get_bo_actual_universe_tree_id();

	declare @universe_table_kind int = dbo.fn_node_kind_id_by_code('UniverseTable');
	declare @universe_alias_table_kind int = dbo.fn_node_kind_id_by_code('UniverseAliasTable');
	declare @universe_derived_table_kind int = dbo.fn_node_kind_id_by_code('UniverseDerivedTable');
	declare @filtr_kind int = dbo.fn_node_kind_id_by_code('Filter');
	declare @detail_nki int = dbo.fn_node_kind_id_by_code('Detail');
	declare @attribute_nki int = dbo.fn_node_kind_id_by_code('Attribute');
	declare @dimension_nki int = dbo.fn_node_kind_id_by_code('Dimension');
	declare @measure_nki int = dbo.fn_node_kind_id_by_code('Measure');

	delete from bik_sapbo_object_table
	from bik_sapbo_object_table bo
	inner join bik_node obj on obj.id = bo.object_node_id
	inner join bik_node tab on tab.id = bo.table_node_id
	where obj.tree_id = @tree_id
	or tab.tree_id = @tree_id

	insert into bik_sapbo_object_table(object_node_id,table_node_id)
	select *
	from (
	select case when uot.obj_type = 1 then bn.id else bn3.id end as obj_node_ide, coalesce(bn2.id, bnidttable.id) as table_node_id
	from aaa_universe_obj_tables uot
	inner join aaa_universe_table ut on uot.universe_table_id = ut.id
	left join bik_node bn2 on bn2.tree_id = @tree_id and bn2.obj_id = convert(varchar(256), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch)
	and bn2.is_deleted = 0
	and bn2.linked_node_id is null
	and bn2.node_kind_id in (@universe_table_kind, @universe_alias_table_kind, @universe_derived_table_kind)
	left join bik_node bnidttable on bnidttable.tree_id = @tree_id and bnidttable.obj_id = convert(varchar(256), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch)
	and bnidttable.is_deleted = 0
	and bnidttable.linked_node_id is null
	and bnidttable.node_kind_id in (@universe_table_kind, @universe_alias_table_kind, @universe_derived_table_kind)
	left join aaa_universe_obj uo on uot.universe_obj_id = uo.id
	left join bik_node bn on bn.tree_id = @tree_id
	and bn.obj_id = uo.obj_branch
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bn.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki, @attribute_nki)
	left join aaa_universe_filter uf on uot.universe_obj_id = uf.id
	left join bik_node bn3 on bn3.tree_id = @tree_id 
	and bn3.obj_id = uf.filtr_branch
	and bn3.is_deleted = 0
	and bn3.linked_node_id is null
	and bn3.node_kind_id = @filtr_kind
	) x
	where obj_node_ide is not null and table_node_id is not null

	insert into bik_joined_objs(src_node_id,dst_node_id,type)
	select object_node_id, table_node_id, 1 
	from bik_sapbo_object_table a 
	inner join bik_node obj on obj.id = a.object_node_id and obj.tree_id = @tree_id
	inner join bik_node tab on tab.id = a.table_node_id and tab.tree_id = @tree_id
	left join bik_joined_objs b on a.object_node_id = b.src_node_id and a.table_node_id = b.dst_node_id
	where b.id is null

	insert into bik_joined_objs(src_node_id,dst_node_id,type)
	select table_node_id, object_node_id, 1 
	from bik_sapbo_object_table a
	inner join bik_node obj on obj.id = a.object_node_id and obj.tree_id = @tree_id
	inner join bik_node tab on tab.id = a.table_node_id and tab.tree_id = @tree_id
	left join bik_joined_objs b on a.table_node_id = b.src_node_id and a.object_node_id = b.dst_node_id
	where b.id is null

end;--procedure
go


-- poprzednia wersja w: alter db for v1.4.y2 tf.sql
-- fix pod IDT SDK
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_universe_objects]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_joined_objs_universe_objects
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_universe_objects]
as
begin
	declare @repTree int = dbo.fn_get_bo_actual_report_tree_id();
	declare @uniTree int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connTree int = dbo.fn_get_bo_actual_connection_tree_id();
	declare @boServer int = dbo.fn_get_bo_actual_server_id();
	declare @webiKind int = dbo.fn_node_kind_id_by_code('Webi');
    declare @uniKind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcKind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');
    
    -- dodanie polaczen universe <-> connection
    exec sp_prepare_bik_joined_objs_tmp
	
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id, bnu.id, 1 
    from aaa_universe uni
	inner join aaa_universe_connection con on uni.universe_connetion_id = con.id
	inner join bik_node bn on bn.tree_id = @connTree and bn.name = con.connetion_name 
	inner join bik_node bnu on bnu.tree_id = @uniTree and bnu.obj_id = convert(varchar(30), uni.global_props_si_id)
	where bn.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bnu.node_kind_id = @uniKind
	and bnu.is_deleted = 0
	and bnu.linked_node_id is null
	
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bnu.id, bn.id, 1 
    from aaa_universe uni
	inner join aaa_universe_connection con on uni.universe_connetion_id = con.id
	inner join bik_node bn on bn.tree_id = @connTree and bn.name = con.connetion_name 
	inner join bik_node bnu on bnu.tree_id = @uniTree and bnu.obj_id = convert(varchar(30), uni.global_props_si_id)
	where bn.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bnu.node_kind_id = @uniKind
	and bnu.is_deleted = 0
	and bnu.linked_node_id is null
	
	-- dla IDT
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id, bnu.id, 1 
    from aaa_universe uni
    inner join aaa_universe_table tab on uni.id = tab.universe_id
	inner join aaa_universe_connection_idt con on tab.idt_connection_id = con.id
	inner join bik_node bn on bn.tree_id = @connTree and bn.obj_id = convert(varchar(30), con.si_id)
	inner join bik_node bnu on bnu.tree_id = @uniTree and bnu.obj_id = convert(varchar(30), uni.global_props_si_id)
	where bn.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bnu.node_kind_id in (@uniKind, @uniNewKind)
	and bnu.is_deleted = 0
	and bnu.linked_node_id is null
	
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bnu.id, bn.id, 1 
    from aaa_universe uni
    inner join aaa_universe_table tab on uni.id = tab.universe_id
	inner join aaa_universe_connection_idt con on tab.idt_connection_id = con.id
	inner join bik_node bn on bn.tree_id = @connTree and bn.obj_id = convert(varchar(30), con.si_id)
	inner join bik_node bnu on bnu.tree_id = @uniTree and bnu.obj_id = convert(varchar(30), uni.global_props_si_id)
	where bn.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bnu.node_kind_id in (@uniKind, @uniNewKind)
	and bnu.is_deleted = 0
	and bnu.linked_node_id is null
	
	/*
	-- dodanie polaczen universe <-> conn OLAP
	if exists(select 1 from bik_sapbo_server ser 
		inner join bik_data_source_def def on ser.source_id = def.id
		where ser.id = @boServer and def.description = 'Business Objects 4.x') and exists(select 1 from sys.objects where type = 'U' and name = 'APP_DSL__METADATAFILE')
	begin
		declare @connColCode varchar(255) = 'SI_SL_UNIVERSE_TO_CONNECTIONS__'
		declare @num int = 1
		declare @connTableCodeBase varchar(255) = @connColCode
		set @connColCode = @connColCode + cast(@num as varchar(20))
		
		declare @table table (
			col_name varchar(max) not null
		);
		
		while exists(select 1 from sys.columns where name = @connColCode and Object_ID = Object_ID('APP_DSL__METADATAFILE'))
		begin
			insert into @table(col_name)
			values (@connColCode)
			
			set @num = @num + 1  
			set @connColCode = @connTableCodeBase + cast(@num as varchar(20))
		end
		
		declare curs cursor for 
		select col_name from @table order by col_name 

		open curs;
		declare @column varchar(50);

		fetch next from curs into @column;
		while @@fetch_status = 0
		begin
			declare @sql1 varchar(max);
			declare @sql2 varchar(max);
			set @sql1 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnu.id, bnc.id, 1 from APP_DSL__METADATAFILE uni
			inner join bik_node bnu on bnu.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),uni.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),uni.' + @column + ')
			where bnu.is_deleted = 0
			and bnc.is_deleted = 0'
			
			set @sql2 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnc.id, bnu.id, 1 from APP_DSL__METADATAFILE uni
			inner join bik_node bnu on bnu.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),uni.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),uni.' + @column + ')
			where bnu.is_deleted = 0
			and bnc.is_deleted = 0'

			exec(@sql1);
			exec(@sql2);
			fetch next from curs into @column;
		end
		close curs;
		deallocate curs;
	end
	*/
	
    exec sp_move_bik_joined_objs_tmp 'Universe,DSL.MetaDataFile', 'DataConnection,CCIS.DataConnection,CommonConnection'
    
    /*
    -- dodanie polaczen report <-> uni,conn
    -- tylko dla BO 4.0, gdyż nie działa Report SDK
	if exists(select 1 from bik_sapbo_server ser 
		inner join bik_data_source_def def on ser.source_id = def.id
		where ser.id = @boServer and def.description = 'Business Objects 4.x') and exists(select 1 from sys.objects where type = 'U' and name = 'INFO_WEBI')
	begin
	    
		exec sp_prepare_bik_joined_objs_tmp
	    
		---------------------
		declare @webiColCode varchar(255) = 'SI_UNIVERSE__'
		declare @numWebi int = 1
		declare @webiTableCodeBase varchar(255) = @webiColCode
		set @webiColCode = @webiColCode + cast(@numWebi as varchar(20))
		
		declare @tableWebi table (
			col_name varchar(max) not null
		);
		
		while exists(select 1 from sys.columns where name = @webiColCode and Object_ID = Object_ID('INFO_WEBI'))
		begin
			insert into @tableWebi(col_name)
			values (@webiColCode)
			
			set @numWebi = @numWebi + 1  
			set @webiColCode = @webiTableCodeBase + cast(@numWebi as varchar(20))
		end
		
		declare cursWebi cursor for 
		select col_name from @tableWebi order by col_name 

		open cursWebi;
		declare @columnWebi varchar(50);

		fetch next from cursWebi into @columnWebi;
		while @@fetch_status = 0
		begin
			declare @sqlWebi1 varchar(max);
			declare @sqlWebi2 varchar(max);
			declare @sqlWebi3 varchar(max);
			declare @sqlWebi4 varchar(max);
			
			set @sqlWebi1 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnu.id, bnc.id, 1 from INFO_WEBI web
			inner join bik_node bnu on bnu.tree_id = ' + cast(@repTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),web.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),web.' + @columnWebi + ')
			where bnu.is_deleted = 0 and bnc.is_deleted = 0
			and bnu.node_kind_id = ' + cast(@webiKind as varchar(30)) + ' and bnc.node_kind_id in (' + cast(@uniKind as varchar(30)) + ',' + cast(@uniNewKind as varchar(30)) + ')'
			
			set @sqlWebi2 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnc.id, bnu.id, 1 from INFO_WEBI web
			inner join bik_node bnu on bnu.tree_id = ' + cast(@repTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),web.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),web.' + @columnWebi + ')
			where bnu.is_deleted = 0 and bnc.is_deleted = 0
			and bnu.node_kind_id = ' + cast(@webiKind as varchar(30)) + ' and bnc.node_kind_id in (' + cast(@uniKind as varchar(30)) + ',' + cast(@uniNewKind as varchar(30)) + ')'
			
			set @sqlWebi3 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnj.id, bnu.id, 1 from INFO_WEBI web
			inner join bik_node bnu on bnu.tree_id = ' + cast(@repTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),web.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),web.' + @columnWebi + ')
			inner join bik_joined_objs obj on obj.src_node_id = bnc.id and obj.type = 1
			inner join bik_node bnj on obj.dst_node_id = bnj.id
			where bnu.is_deleted = 0 and bnc.is_deleted = 0 and bnj.is_deleted = 0
			and bnu.node_kind_id = ' + cast(@webiKind as varchar(30)) + ' and bnc.node_kind_id in (' + cast(@uniKind as varchar(30)) + ',' + cast(@uniNewKind as varchar(30)) + ')
			and bnj.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnj.node_kind_id in (' + cast(@dcKind as varchar(30)) + ',' + cast(@dcNewKind as varchar(30)) + ',' + cast(@dcNewOLAPKind as varchar(30)) + ')'

			set @sqlWebi4 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnu.id, bnj.id, 1 from INFO_WEBI web
			inner join bik_node bnu on bnu.tree_id = ' + cast(@repTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),web.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),web.' + @columnWebi + ')
			inner join bik_joined_objs obj on obj.src_node_id = bnc.id and obj.type = 1
			inner join bik_node bnj on obj.dst_node_id = bnj.id
			where bnu.is_deleted = 0 and bnc.is_deleted = 0 and bnj.is_deleted = 0
			and bnu.node_kind_id = ' + cast(@webiKind as varchar(30)) + ' and bnc.node_kind_id in (' + cast(@uniKind as varchar(30)) + ',' + cast(@uniNewKind as varchar(30)) + ')
			and bnj.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnj.node_kind_id in (' + cast(@dcKind as varchar(30)) + ',' + cast(@dcNewKind as varchar(30)) + ',' + cast(@dcNewOLAPKind as varchar(30)) + ')'

			exec(@sqlWebi1);
			exec(@sqlWebi2);
			exec(@sqlWebi3);
			exec(@sqlWebi4);
			
			--print @sqlWebi1
			--print @sqlWebi2
			--print @sqlWebi3
			--print @sqlWebi4
			
			fetch next from cursWebi into @columnWebi;
		end
		close cursWebi;
		deallocate cursWebi;
		---------------------
		
		exec sp_move_bik_joined_objs_tmp 'Webi', 'DataConnection,CCIS.DataConnection,CommonConnection,Universe,DSL.MetaDataFile'
	end
	*/
    
	-- dodanie polaczen universe objcets <-> uni,conn
    create table #tmpBranch (branch_id varchar(max) collate DATABASE_DEFAULT, node_id int);

    insert into #tmpBranch(branch_id,node_id)
    select bn.branch_ids, bn2.id 
    from bik_node bn
    inner join bik_joined_objs jo on jo.src_node_id = bn.id
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    where bn.tree_id = @uniTree
    and bn.node_kind_id in (@uniKind, @uniNewKind)
    and bn.linked_node_id is null
    and bn.is_deleted = 0
    and bn2.tree_id = @connTree
    and bn2.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
    and bn2.linked_node_id is null
    and bn2.is_deleted = 0

    exec sp_prepare_bik_joined_objs_tmp

	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
	select bn.id, a.node_id, 1 
	from #tmpBranch a 
	inner join bik_node bn on bn.branch_ids like a.branch_id + '%'
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	where node_kind_id not in (@uniKind, @uniNewKind)
		
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
	select bn2.id, bn.id, 1 
	from bik_node bn 
	inner join bik_node bn2 on bn2.branch_ids like bn.branch_ids + '%'
	and bn2.is_deleted = 0
	and bn2.linked_node_id is null
	and bn2.node_kind_id not in (@uniKind, @uniNewKind)
	and bn.tree_id = @uniTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bn.node_kind_id in (@uniKind, @uniNewKind)

    exec sp_move_bik_joined_objs_tmp 'Universe,DSL.MetaDataFile,DataConnection,CCIS.DataConnection,CommonConnection', 'UniverseClass,Measure,Dimension,Detail,Attribute,Filter,UniverseAliasTable,UniverseDerivedTable,UniverseTable,UniverseTablesFolder'
	
	drop table #tmpBranch
end;
go

-- poprzednia wersja w pliku: alter db for v1.5.x.7 tf.sql
-- fix pod IDT
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_metadata_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
	
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();

    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @universeNodeKind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');

    ----------   zapytanie  -->  universe    ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id as src_node_id, bnu.id as dst_node_id, 1 as type
    from bik_sapbo_query sbq
    inner join bik_node bn on sbq.node_id = bn.id
    --inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_sapbo_extradata ex on ex.cuid = sbq.universe_cuid
    inner join bik_node bnu on bnu.id = ex.node_id 
	where bn.tree_id = @reportTreeId and bn.is_deleted = 0 
		and bnu.tree_id = @universeTreeId and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind,@uniNewKind)

    ----------   zapytanie  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id as src_node_id, bnc.id as dst_node_id, 1 as type
    from bik_sapbo_query sbq
    inner join bik_node bn on sbq.node_id = bn.id
    --inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_sapbo_extradata ex on ex.cuid = sbq.universe_cuid
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
		and bnu.id = ex.node_id and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
		and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
		and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	where bn.tree_id = @reportTreeId
		and bn.is_deleted = 0 

    ----------   universe  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnu.id as src_node_id, bnr.id as dst_node_id, 1 as type
    from aaa_query sbq
    --inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_sapbo_extradata ex on ex.cuid = sbq.universe_cuid
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
		and bnu.id = ex.node_id
		and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
	inner join bik_node bnr on bnr.id = sbq.report_node_id
		and bnr.tree_id = @reportTreeId and bnr.is_deleted = 0

    ----------   raport  -->  universe   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnr.id as src_node_id, bnu.id as dst_node_id, 1 as type
    from aaa_query sbq
    --inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_sapbo_extradata ex on ex.cuid = sbq.universe_cuid
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
		and bnu.id = ex.node_id
		and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
	inner join bik_node bnr on bnr.id = sbq.report_node_id
		and bnr.tree_id = @reportTreeId and bnr.is_deleted = 0

    ----------   raport  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnr.id as src_node_id, bnc.id as dst_node_id, 1 as type
    from aaa_query sbq
    --inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_sapbo_extradata ex on ex.cuid = sbq.universe_cuid
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
		and bnu.id = ex.node_id
		and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
		and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
		and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	inner join bik_node bnr on bnr.id = sbq.report_node_id
		and bnr.tree_id = @reportTreeId and bnr.is_deleted = 0

	----------   connection  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnc.id as src_node_id, bnr.id as dst_node_id, 1 as type
    from aaa_query sbq
    --inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_sapbo_extradata ex on ex.cuid = sbq.universe_cuid
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
		and bnu.id = ex.node_id
		and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
		and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
		and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	inner join bik_node bnr on bnr.id = sbq.report_node_id
		and bnr.tree_id = @reportTreeId and bnr.is_deleted = 0

	----------   obiekt  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct ids.object_node_id, bn.parent_node_id, 1 
    from aaa_ids_for_report ids 
    inner join bik_node bn on ids.query_node_id = bn.id
		and bn.tree_id = @reportTreeId
		and bn.is_deleted = 0
    inner join bik_node bno on bno.id = ids.object_node_id
		and bno.tree_id = @universeTreeId
		and bno.is_deleted = 0
	
	----------   UniverseAliasTable  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bsut1.node_id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_sapbo_universe_table bsut1 on bsut.original_table = bsut1.their_id
	inner join bik_node bn on bn.id = bsut.node_id
	inner join bik_node bn2 on bn2.id = bsut1.node_id
	where bsut.is_alias = 1 and bn2.parent_node_id = bn.parent_node_id
		and bn.tree_id = @universeTreeId
		and bn2.tree_id = @universeTreeId

	-- usuniecie alias table - univ table
	exec sp_delete_bik_joined_objs_by_kinds_fast 'UniverseTable', 'UniverseAliasTable'
	
    exec sp_move_bik_joined_objs_tmp 'Webi,ReportQuery', 'Universe,DSL.MetaDataFile,DataConnection,CommonConnection,CCIS.DataConnection,Measure,Dimension,Detail,Filter'
	
end;
go

-- fix na modrzewia dla bo4.1 - idt
declare @treeOfTreesId int = dbo.fn_tree_id_by_code('TreeOfTrees');

if not exists (select * from bik_node where parent_node_id in (
				select id from bik_node where tree_id = @treeOfTreesId and obj_id like '&DSL.MetaDataFile%' ))
begin

	declare @obj_id varchar(900);
	declare idt_curs cursor for 
	select obj_id from bik_node where tree_id = @treeOfTreesId and obj_id like '&DSL.MetaDataFile%' 
		
	open idt_curs
	fetch next from idt_curs into @obj_id
	while @@fetch_status = 0
	begin 
		set @obj_id = replace(@obj_id,'&DSL.MetaDataFile','');

		declare @child varchar(100), @parent varchar(100);
		
		set @parent = '&DSL.MetaDataFile' + @obj_id
		set @child = '&UniverseDerivedTable' + @obj_id + '>UNX';
		exec sp_add_menu_node @parent, '@', @child
		set @child = '&UniverseAliasTable' + @obj_id + '>UNX';
		exec sp_add_menu_node @parent, '@', @child
		set @child = '&UniverseTable' + @obj_id + '>UNX';
		exec sp_add_menu_node @parent, '@', @child
		set @child = '&UniverseClass' + @obj_id + '>UNX';
		exec sp_add_menu_node @parent, '@', @child
		set @parent = '&UniverseClass' + @obj_id + '>UNX';
		set @child = '&Dimension' + @obj_id + '>UNX';
		exec sp_add_menu_node @parent, '@', @child
		set @child = '&Measure' + @obj_id + '>UNX';
		exec sp_add_menu_node @parent, '@', @child
		set @child = '&Filter' + @obj_id + '>UNX';
		exec sp_add_menu_node @parent, '@', @child
		set @parent = '&Dimension' + @obj_id + '>UNX';
		set @child = '&Attribute' + @obj_id + '>UNX';
		exec sp_add_menu_node @parent, '@', @child
		set @parent = '&Measure' + @obj_id + '>UNX';
		exec sp_add_menu_node @parent, '@', @child
		
		
		fetch next from idt_curs into @obj_id
	end
	close idt_curs;
	deallocate idt_curs;

	exec sp_node_init_branch_id @treeOfTreesId, null

end;
go


exec sp_update_version '1.6.z3.6', '1.6.z3.7';
go