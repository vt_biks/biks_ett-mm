/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.util.Iterator;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.IStringComparator;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
//i18n-default: no
public class BIKCenterUtilsTestBase {

    protected void fail(String msg) {
        throw new RuntimeException(msg);
    }

    protected void proceedWithCatched(RuntimeException ex) {
        throw ex;
    }

    protected void checkExtractedNodeIds(Set<Integer> actual, int... expected) {
        try {
            if (actual == null) {
                fail("actual in null");
            }

            if (actual.size() != expected.length) {
                fail("expected result size" + "=" + expected.length + ", " + "actual size is" + " " + actual.size());
            }
            Iterator<Integer> iter = actual.iterator();
            int i = 0;
            //for (int i = 0; i < actual.size(); i++) {
            while (iter.hasNext()) {
                //Integer actInt = actual.get(i);
                Integer actInt = iter.next();
                int expInt = expected[i];

                if (actInt == null) {
                    fail("actual val at index" + ": " + i + " " + "is null, size is ok" + "=" + actual.size());
                }

                if (actInt != expInt) {
                    fail("actual val at index" + ": " + i + " (=" + actInt + ") " + "is not equal to expected" + " (="
                            + expInt + "), " + "size is ok" + "=" + actual.size());
                }
                i++;
            }
        } catch (RuntimeException ex) {
            proceedWithCatched(ex);
        }
    }

    protected void checkRemovedHrefsFromHtml(String actual, String expected) {
        try {
            if (actual == null) {
                fail("actual in null");
            }

            if (actual.length() != expected.length()) {
                fail("expected result size" + "=" + expected.length() + ", " + "actual size is" + " " + actual.length()
                        + "\n " + "expected" + " = \"" + expected + "\", " + "actual" + " = \"" + actual + "\" - " + "not equal");
            }
            if (!actual.equals(expected)) {
                fail("expected" + " = \"" + expected + "\", " + "actual" + " = \"" + actual + "\" - " + "not equal");
            }
        } catch (RuntimeException ex) {
            proceedWithCatched(ex);
        }
    }

    public void innerTestExtractNodeIdsFromHtml() {
        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml(""));

        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a href='#Glossary/10'>" + "ala ma kota" + "</a>"),
                10);

        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a href='#Glossary/10'>" + "ala ma kota</a> a kota nie ma <a href='#Glossary/20'>ali" + "</a>"),
                10, 20);

        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a href='http://onet.pl'>" + "ala ma kota</a> a kota nie ma <a href='#Glossary/13'>ali" + "</a>"),
                13);

        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a href=\"http://onet.pl\">" + "ala ma kota</a> a kota nie ma <a href=\"#Glossary/13\">ali" + "</a>"),
                13);

        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a    \n"
                + "href=\"http://onet.pl\">" + "ala ma kota</a> a kota nie ma <a href =\"#Glossary/13\">ali" + "</a>"),
                13);

        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a    \n"
                + "href=\"http://onet.pl\">" + "ala ma kota</a> a kota nie ma <a  href='#Glossary/13' >\nali" + "\n</a>"),
                13);

        //moje
        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a    \n"
                + "href=\"http://onet.pl 14 \"> " + "ala ma kota</a> a kota nie ma <a  href='#Glossary/13' >\nali" + "\n</a>"),
                13);


        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a    \n"
                + "href =\"http://onet.pl 14 \"> " + "ala ma kota</a> a kota nie ma <a  href='#Glossary/13' >\nali" + "\n </a> "),
                13);

        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml(" " + "fdandgnds ngdsons dignsdfa <a razdwa trzy 13></a><a    \n"
                + "href =\"http://onet.pl 14 \"> ala ma kota</a> a kota nie ma <a  href='#Glossary/13' >\nali" + "\n </a> "),
                13);


        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a    \n"
                + "href=\"http://onet.pl  \"> " + "ala ma kota</a> a kota nie ma <a    href \n =\"#Glossary/15\" >\nali" + "\n</a>"),
                15);

        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a    \n"
                + "href=\"http://onet.pl  \"> " + "ala ma kota</a> a kota nie ma <a    href \n =\"#Glossary/a15\" >\nali\n</a> <a    href \n =\"#Glossary/7\" >\nali" + "\n</a>"),
                7);

        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a    \n"
                + "href=\"http://onet.pl  \"> " + "ala ma kota</a> a kota nie ma <a    href \n =\"#Glossary/a15\" >\nali\n</a> <a    href \n =\"#Glossary/7\" >\nali" + "\n</a>"),
                7);

        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a href='#Glossary/alamakota10'>" + "ala ma kota" + "</a>"));

        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a href='#Glossary/'>" + "ala ma kota" + "</a>"));

        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a href='#Glossary/0'>" + "ala ma kota" + "</a>"), 0);
        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a href='#Glossary/-1'>" + "ala ma kota" + "</a>"), -1);
        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a href='#Glossary/-666'>" + "ala ma kota" + "</a>"), -666);
        //checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a href='#Glossary/-666'>ala ma kota</a>"), -667);

        checkExtractedNodeIds(BIKCenterUtils.extractNodeIdsFromHtml("<a href='#Glossary/10'>" + "ala ma kota</a> "
                + "a kota nie ma <a href='#Glossary/20'>ali</a>"
                + " <a href='#Tezio/33'>ali</a>"
                + " barbary <a \t \n href = \"#Tezio/-34\">  s" + ".  \n</a>"),
                10, 20, 33, -34);
    }

    public void innerTestRemoveHrefsFromHtml() {
        //checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("", new LinkedHashSet (Arrays.asList(23))), "");
        //String s = BIKCenterUtils.removeHrefsFromHtml("", new LinkedHashSet(Arrays.asList(23)));
        //String s = BIKCenterUtils.removeHrefsFromHtml("", new LinkedHashSet<Integer>(Arrays.asList(23)));
        //String s = BIKCenterUtils.removeHrefsFromHtml("", BaseUtils.paramsAsSet(23));
        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("", BaseUtils.paramsAsSet(23)), "");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a href='#Glossary/10'>" + "ala ma kota" + "</a>", BaseUtils.paramsAsSet(10)),
                "ala ma kota");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a href='#Glossary/10'>" + "ala ma kota</a> a kota nie ma <a href='#Glossary/20'>ali" + "</a>", BaseUtils.paramsAsSet(10)),
                "ala ma kota a kota nie ma <a href='#Glossary/20'>ali" + "</a>");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a href='http://onet.pl'>" + "ala ma kota</a> a kota nie ma <a href='#Glossary/13'>ali" + "</a>", BaseUtils.paramsAsSet(13)),
                "<a href='http://onet.pl'>" + "ala ma kota</a> a kota nie ma ali");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a href=\"http://onet.pl\">" + "ala ma kota</a> a kota nie ma <a href=\"#Glossary/13\">ali" + "</a>", BaseUtils.paramsAsSet(13)),
                "<a href=\"http://onet.pl\">" + "ala ma kota</a> a kota nie ma ali");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a    \n"
                + "href=\"http://onet.pl\">" + "ala ma kota</a> a kota nie ma <a href =\"#Glossary/13\">ali" + "</a>", BaseUtils.paramsAsSet(13)),
                "<a    \n"
                + "href=\"http://onet.pl\">" + "ala ma kota</a> a kota nie ma ali");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a    \n"
                + "href=\"http://onet.pl\">" + "ala ma kota</a> a kota nie ma <a  href='#Glossary/13' >\nali" + "\n</a>", BaseUtils.paramsAsSet(13)),
                "<a    \n"
                + "href=\"http://onet.pl\">" + "ala ma kota</a> a kota nie ma \nali" + "\n");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a    \n"
                + "href=\"http://onet.pl 14 \"> " + "ala ma kota</a> a kota nie ma <a  href='#Glossary/13' >\nali" + "\n</a>", BaseUtils.paramsAsSet(13)),
                "<a    \n"
                + "href=\"http://onet.pl 14 \"> " + "ala ma kota</a> a kota nie ma \nali" + "\n");


        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a    \n"
                + "href =\"http://onet.pl 14 \"> " + "ala ma kota</a> a kota nie ma <a  href='#Glossary/13' >\nali" + "\n </a> ", BaseUtils.paramsAsSet(13)),
                "<a    \n"
                + "href =\"http://onet.pl 14 \"> " + "ala ma kota</a> a kota nie ma \nali" + "\n  ");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml(" " + "fdandgnds ngdsons dignsdfa <a razdwa trzy 13></a><a    \n"
                + "href =\"http://onet.pl 14 \"> ala ma kota</a> a kota nie ma <a  href='#Glossary/13' >\nali" + "\n </a> ", BaseUtils.paramsAsSet(13)),
                " " + "fdandgnds ngdsons dignsdfa <a razdwa trzy 13></a><a    \n"
                + "href =\"http://onet.pl 14 \"> ala ma kota</a> a kota nie ma \nali" + "\n  ");


        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a    \n"
                + "href=\"http://onet.pl  \"> " + "ala ma kota</a> a kota nie ma <a    href \n =\"#Glossary/15\" >\nali" + "\n</a>", BaseUtils.paramsAsSet(15)),
                "<a    \n"
                + "href=\"http://onet.pl  \"> " + "ala ma kota</a> a kota nie ma \nali" + "\n");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a    \n"
                + "href=\"http://onet.pl  \"> " + "ala ma kota</a> a kota nie ma <a    href \n =\"#Glossary/a15\" >\nali\n</a> <a    href \n =\"#Glossary/7\" >\nali" + "\n</a>", BaseUtils.paramsAsSet(7)),
                "<a    \n"
                + "href=\"http://onet.pl  \"> " + "ala ma kota</a> a kota nie ma <a    href \n =\"#Glossary/a15\" >\nali\n</a> \nali" + "\n");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a    \n"
                + "href=\"http://onet.pl  \"> " + "ala ma kota</a> a kota nie ma <a    href \n =\"#Glossary/a15\" >\nali\n</a> <a    href \n =\"#Glossary/7\" >\nali" + "\n</a>", BaseUtils.paramsAsSet(7)),
                "<a    \n"
                + "href=\"http://onet.pl  \"> " + "ala ma kota</a> a kota nie ma <a    href \n =\"#Glossary/a15\" >\nali\n</a> \nali" + "\n");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a href='#Glossary/alamakota10'>" + "ala ma kota" + "</a>", BaseUtils.paramsAsSet(10)),
                "<a href='#Glossary/alamakota10'>" + "ala ma kota" + "</a>");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a href='#Glossary/'>" + "ala ma kota" + "</a>", BaseUtils.paramsAsSet(10)),
                "<a href='#Glossary/'>" + "ala ma kota" + "</a>");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a href='#Glossary/0'>" + "ala ma kota" + "</a>", BaseUtils.paramsAsSet(0)),
                "ala ma kota");
        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a href='#Glossary/-1'>" + "ala ma kota" + "</a>", BaseUtils.paramsAsSet(-1)),
                "ala ma kota");
        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a href='#Glossary/-666'>" + "ala ma kota" + "</a>", BaseUtils.paramsAsSet(-666)),
                "ala ma kota");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("<a href='#Glossary/10'>" + "ala ma kota</a> "
                + "a kota nie ma <a href='#Glossary/20'>ali</a>"
                + " <a href='#Tezio/33'>ali</a>"
                + " barbary <a \t \n href = \"#Tezio/-34\">  s" + ".  \n</a>", BaseUtils.paramsAsSet(-34)),
                "<a href='#Glossary/10'>" + "ala ma kota</a> "
                + "a kota nie ma <a href='#Glossary/20'>ali</a>"
                + " <a href='#Tezio/33'>ali</a>"
                + " barbary   s" + ".  \n");

        checkRemovedHrefsFromHtml(BIKCenterUtils.removeHrefsFromHtml("h<a href='#Tezio/61'>ali" + "</a>", BaseUtils.paramsAsSet(61)), "hali");
    }

    protected void checkValues(String one, String two, int expected) {
        try {
            if (one == null || two == null) {
                fail("String is null");
            }
            IStringComparator comparator = BaseUtils.getComparator();

            if (comparator.compare(one, two) != expected) {
                fail("Error in compare" + ": " + one + " " + "and" + " " + two + ". " + "Expected" + ": " + expected + " , " + "result" + ": " + comparator.compare(one, two));
            }

        } catch (RuntimeException ex) {
            proceedWithCatched(ex);
        }
    }

    public void innerTestCompareValues() {
        //BaseUtils.setStringComparator(new SimpleStringComparator(), true);

        checkValues("a", "ą", -1);
        checkValues("A", "Ą", -1);
        checkValues("Z", "Ż", -1);
        checkValues("Z", "Ź", -1);
        checkValues("Ź", "Ż", -1);
        checkValues("Ć", "Ę", -1);
        checkValues("Ę", "Ą", 1);
        checkValues("w", "ą", 1);
        checkValues("p", "A", 1);
        checkValues("a", "b", -1);
        checkValues("ala ma kota", "bąćźsdfg", -1);
        checkValues("ąddsd", "ąeźsdfg", -1);

        checkValues("ala", "al", 1);
        checkValues("al", "ala", -1);
        checkValues("alą", "ala", 1);

        checkValues("alź", "alż", -1);
        checkValues("Żółw", "Źółw", 1);
        checkValues("Żółwinka", "Żółw", 1);

        // dodac kilka
    }
}
