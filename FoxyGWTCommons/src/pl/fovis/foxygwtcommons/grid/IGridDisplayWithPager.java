/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.grid;

/**
 *
 * @author ctran
 */
public interface IGridDisplayWithPager {

    public void navigate(int pageNum);

    public int getMaxPageNum();

    public int getPageNum();

}
