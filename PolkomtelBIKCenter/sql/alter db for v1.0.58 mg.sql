﻿exec sp_check_version '1.0.58';
go

if OBJECT_ID (N'dbo.fn_node_kind_code_for_metadata_type', N'FN') is not null
    drop function dbo.fn_node_kind_code_for_metadata_type;
go

create function dbo.fn_node_kind_code_for_metadata_type(@metadata_type varchar(256))
returns varchar(255)
with execute as caller
as
begin
  return (
    select
      case @metadata_type
        when 'MetaData.DataConnection' then 'DataConnection'
        when 'Folder' then 'MetadataFolder'
        when 'Webi' then 'Webi'
        when 'Flash' then 'Flash'
        when 'CristalReports' then 'CristalReports'
        when 'Universe' then 'Universe'
        when 'UniverseClass' then 'UniverseClass'
        when 'UniverseObject' then 'UniverseObject'
        when 'UniverseTable' then 'UniverseTable'
        when 'UniverseColumn' then 'UniverseColumn'
        when 'SCHEMA' then 'TeradataSchema'
        when 'TABLE' then 'TeradataTable'
        when 'VIEW' then 'TeradataView'
        when 'COLUMN' then 'TeradataColumn'
        when 'PROCEDURE' then 'TeradataProcedure'
        else 'Other'
    end
  )
end
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node]
GO

create procedure [dbo].[sp_insert_into_bik_node](@tree_name varchar(255))
as
	declare @table_folders varchar(255);
	declare @si_id varchar(1000), @si_parentid varchar(1000), @si_kind varchar(max), @si_name varchar(max);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	--print cast(sysdatetime() as varchar(23)) + ': faza 1'
	
	select @tree_id = id 
	from bik_tree
	where name=@tree_name;

	create table #primaryIds(si_id varchar(1000) primary key);
	create table #secondaryIds(si_id varchar(1000) primary key);

	if(@tree_name!='Raporty' and @tree_name!='Teradata')
	begin
		set @table_folders = ' APP_FOLDER where 1=1 ';
		
		if(@tree_name = 'Połączenia')
		begin
			set @kinds = '''MetaData.DataConnection''';
			exec('insert into #primaryIds
					select convert(varchar,SI_ID) as SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');
			exec('insert into #secondaryIds 
					select convert(varchar,SI_ID) as SI_ID
					from APP_UNIVERSE
					where SI_DATACONNECTION__1 in (select * from #primaryIds)');			
		end;
		if(@tree_name = 'Światy obiektów')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select convert(varchar,SI_ID) as SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_name='Raporty')
	begin
		set @table_folders = ' INFO_FOLDER where si_name != ''~Webintelligence''';
		set @kinds = '''Webi'', ''Flash'', ''CristalReports''';
		
	end;
	
    declare @sql nvarchar(1000);
				
declare @node_kind_id int;
set @sql = N'';
if(@tree_name = 'Teradata')
begin
	--print cast(sysdatetime() as varchar(23)) + ': faza 4.1 - teradata przed sp_teradata_init_branch_names'
	exec sp_teradata_init_branch_names;
	--print cast(sysdatetime() as varchar(23)) + ': faza 4.2 - teradata po sp_teradata_init_branch_names'
	set @sql = N'select branch_names as SI_ID, 
											case 
												when SUBSTRING(branch_names, 1,len(branch_names) - len(name + ''|'')) =''''
													then null
												else SUBSTRING(branch_names, 1,len(branch_names) - len(name + ''|'')) end
												as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME
				 from bik_teradata
				/*where parent_id is not null or name like ''VD_US_%''*/';
	
end;
else
begin
	
	set @sql = N'select si_id, case 
									when SI_PARENTID in(select si_id from ' + @table_folders +') 
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, SI_NAME 
				 from ' + @table_folders + 
													
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +')';
		if(@tree_name = 'Połączenia')
			set @sql = @sql + N'union all		  

								select SI_ID, SI_DATACONNECTION__1 as SI_PARENTID, SI_KIND, SI_NAME 
								from APP_UNIVERSE
								where SI_DATACONNECTION__1 in (select * from #primaryIds)
	
								union all
								
								select SI_ID, SI_UNIVERSE__1 as SI_PARENTID, SI_KIND, SI_NAME
								from INFO_WEBI
								where SI_UNIVERSE__1 in (select * from #secondaryIds)';
							
		if(@tree_name = 'Światy obiektów')
			set @sql = @sql + N'union all
			
								select SI_ID, SI_UNIVERSE__1 as SI_PARENTID, SI_KIND,  SI_NAME
								from INFO_WEBI
								where SI_UNIVERSE__1 in (select * from #primaryIds)';
end;					

	--print cast(sysdatetime() as varchar(23)) + ': faza 6 - przed zapytaniem'

--print 'zapytanie: ' + @sql;

create table #tmpNodes (si_id varchar(1000) primary key, si_parentid varchar(1000), si_kind varchar(max), si_name varchar(max));

set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name) 
		    select si_id, si_parentid, fn_node_kind_code_for_metadata_type(si_kind), si_name 
		    from (' + @sql + ') xxx';
EXECUTE(@sql);

	--print cast(sysdatetime() as varchar(23)) + ': faza 6.5 - po wrzuceniu do #tmpNodes'

insert into bik_node_kind (code,caption) 
select distinct si_kind, si_kind
from #tmpNodes 
where not exists (select 1 from bik_node_kind where code = si_kind);

update bik_node 
set parent_node_id = null, node_kind_id = bik_node_kind.id, name = #tmpNodes.si_name,
  is_deleted = 0
from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
where bik_node.obj_id = #tmpNodes.si_id and tree_id = @tree_id;

insert into bik_node (node_kind_id, name, tree_id, obj_id)
select bik_node_kind.id, #tmpNodes.si_name, @tree_id, #tmpNodes.si_id
from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	left join bik_node on bik_node.obj_id = #tmpNodes.si_id 
where bik_node.id is null;

	--print cast(sysdatetime() as varchar(23)) + ': faza 7.0 - po ciężkiej walce bez kursora?'

	--print cast(sysdatetime() as varchar(23)) + ': faza 9.5 - po pętli, zamknięte kursory, usuwamy pustaki z bik_node'

set nocount on;

drop table #primaryIds;
drop table #secondaryIds;
--drop table #emptyFolders;

	--print cast(sysdatetime() as varchar(23)) + ': faza 10 - przed uaktualnieniem parentów'
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id

	--print cast(sysdatetime() as varchar(23)) + ': faza 11 - po uaktualnieniu parentów'
	
	update --delete from 
	bik_node
	set is_deleted = 1
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not #tmpNodes.si_kind in ('UniverseColumn', 'UniverseTable', 'UniverseObject', 'UniverseClass') ;

	declare @folder_node_kind_id int

	select @folder_node_kind_id = ID from bik_node_kind where code = 'Folder';

	declare @rc int = 1

	while @rc > 0 begin
		--delete from 
		update bik_node
		set is_deleted = 1
		where tree_id = @tree_id and node_kind_id = @folder_node_kind_id and is_deleted = 0
		 and not exists(select 1 from bik_node g where g.parent_node_id = bik_node.id and g.is_deleted = 0)

		set @rc = @@ROWCOUNT
	end;--end loop
				
	--drop table #pomTable;
	drop table #tmpNodes

	--print cast(sysdatetime() as varchar(23)) + ': faza 12 - po delete zbędnych'

	exec sp_node_init_branch_id @tree_id, null;

	--print cast(sysdatetime() as varchar(23)) + ': faza 13 - po sp_node_init_branch_id, finito!'
end;
go


update bik_node_kind
set is_folder = 0
where code in ('Blog')
go

 exec sp_update_version '1.0.58', '1.0.59';
 go
 