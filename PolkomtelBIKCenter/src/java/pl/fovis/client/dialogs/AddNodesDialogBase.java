/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.IGridItemsProvider;
import pl.fovis.client.bikwidgets.IPaginatedItemsDisplayer;
import pl.fovis.client.bikwidgets.NodesGridBase;
import pl.fovis.client.bikwidgets.WidgetWithPagination;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.LameBag;

/**
 *
 * @author bfechner
 */
public abstract class AddNodesDialogBase extends BaseActionOrCancelDialog {

    protected IParametrizedContinuation<TreeNodeBean> saveCont;
    protected int nodeKindId;
    protected int treeId;
    protected NodesGridBase paginatorFvs;//= new NodesToLinkGrid();

    public void buildAndShowDialog(int nodeKindId, int treeId, List<TreeNodeBean> nodes, IParametrizedContinuation<TreeNodeBean> saveCont) {
        this.nodeKindId = nodeKindId;
        this.allItems = nodes;
        this.treeId = treeId;
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }

    protected String getActionButtonCaption() {
        return BaseUtils.isCollectionEmpty(allItems) ? null : I18n.dodaj.get() /* I18N:  */;
    }

    protected void buildMainWidgets() {

        if (BaseUtils.isCollectionEmpty(allItems)) {
            main.add(new Label(getTextWhenKindsListEmpty()));
        } else {
            show(main);
        }
    }

    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        if (BIKClientSingletons.isUserLoggedIn()) {
            BIKClientSingletons.getService().getNodesByLinkedKind(
                    nodeKindId, getRelationType(), treeId, callback);
        }
    }

    protected abstract String getTextWhenKindsListEmpty();

    protected abstract String getRelationType();

    protected String getCode(TreeNodeBean n) {
        return n.nodeKindCode;
    }

    protected int getPageSize() {
        return 15;
    }

    protected abstract IPaginatedItemsDisplayer<TreeNodeBean> createPaginatedItemsDisplayer();

    protected List<TreeNodeBean> filteredItemsByKind(List<TreeNodeBean> items) {
        prepareItemFiltering();
        final List<TreeNodeBean> fvs = new ArrayList<TreeNodeBean>();

        for (TreeNodeBean fc : items) {
            if (shouldShowItem(fc)) {
                fvs.add(fc);
            }
        }
        return fvs;
    }

    protected boolean shouldShowItem(TreeNodeBean e) {
        return activeKinds.contains(getCode(e));
    }

    protected HashSet activeKinds;
    private FlowPanel checkBoxContainer;
    protected List<CheckBox> checkboxes = new ArrayList<CheckBox>();
    private HorizontalPanel filters;

    protected void prepareItemFiltering() {
        boolean allActive = true;
        activeKinds = new HashSet<String>();
        for (int i = 1; i < getFilterCnt(); i++) {
            if (isFilterOn(i)) {
                activeKinds.add(getFilterName(i));
            } else {
                allActive = false;
            }
        }
        if (getFilterCnt() > 0) {
            setFilterActive(0, allActive);
        }
    }

    protected int getFilterCnt() {
        return checkboxes.size();
    }

    protected boolean isFilterOn(int idx) {
        return checkboxes.get(idx).getValue();
    }

    protected String getFilterName(int idx) {
        return checkboxes.get(idx).getName();
    }

    protected void setFilterActive(int idx, boolean active) {
        checkboxes.get(idx).setValue(active);
    }

    protected void clearCheckboxes() {
        checkboxes.clear();
    }

    protected void recreateFilterCheckboxes(List<TreeNodeBean> allItems) {

        LameBag<String> counts = new LameBag<String>();
        List<TreeNodeBean> fvs = allItems;//

        if (fvs != null) {
            for (TreeNodeBean fc : fvs) {
                counts.add(getCode(fc));
            }
        }
        clearCheckboxes();
        addCheckbox("*", "<b>" + I18n.wszystko.get() /* I18N:  */ + "</b> (" + BaseUtils.collectionSizeFix(fvs) + ")",
                true, new IContinuation() {
                    public void doIt() {
                        boolean check = isFilterOn(0);
                        for (int i = 1; i < getFilterCnt(); i++) {
                            setFilterActive(i, check);
                        }
                        displayData();
                    }
                });

        for (String kind : counts.uniqueSet()) {

            addCheckbox(kind,
                    BIKClientSingletons.getHtmlForNodeKindIconWithText(kind, null, BIKClientSingletons.getNodeKindCaptionByCode(kind) + " (" + counts.getCount(kind) + ")"), true,
                    new IContinuation() {
                        public void doIt() {
                            displayData();
                        }
                    });
        }

        finalizeCheckboxes();
    }

    protected void addOptionalFilterByKindWidgets(List<TreeNodeBean> allItems) {
        optionalFilterByKindWidgets.clear();
        if (BaseUtils.collectionSizeFix(allItems) > 0) {
            VerticalPanel vp = new VerticalPanel();
            filters = new HorizontalPanel();
            filters.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
            HTML lblFilters = new HTML(I18n.pokazTypyObiektow.get() /* I18N:  */ + ":");
            lblFilters.setStyleName("FvsKind");
            filters.setWidth("100%");
            GWTUtils.setStyleAttribute(filters, "minHeight", "25px");
            checkBoxContainer = new FlowPanel();
            filters.add(lblFilters);
            filters.add(checkBoxContainer);
            vp.add(filters);
            vp.setStyleName("Fvs-filters");
            recreateFilterCheckboxes(allItems);
            optionalFilterByKindWidgets.add(vp);
        }
    }

    protected void addCheckbox(String name, String caption, boolean active, final IContinuation contWhenChanged) {
        final CheckBox cb = new CheckBox(caption, true);
        cb.setStyleName("Filters-Checkbox");
        cb.setValue(active);
        cb.setName(name);
        //tymczasowa szerokość wstawiona tutaj.
        //cb.setWidth("155px");
        cb.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent ce) {
                contWhenChanged.doIt();
            }
        });
        checkboxes.add(cb);
    }

    protected void finalizeCheckboxes() {
        checkBoxContainer.clear();
        for (CheckBox cb : checkboxes) {
            checkBoxContainer.add(cb);
        }
    }

    protected CheckBox filterItemsCB = new CheckBox();

    protected boolean isHeaderBtnEnabled(boolean forEmptyFilteredItems) {
        return false;
    }

    public List<TreeNodeBean> prepareAllItemsToDisplay() {
        return filteredItemsByKind(allItems);
    }

    protected VerticalPanel activeTab = new VerticalPanel();
    protected List<TreeNodeBean> allItems;
    protected WidgetWithPagination<TreeNodeBean> pagin;
    protected Widget paginationWidget;
    protected VerticalPanel optionalFilterByKindWidgets = new VerticalPanel();
    protected IGridItemsProvider<TreeNodeBean> dataPaginator = new IGridItemsProvider<TreeNodeBean>() {
        public void itemsChanged() {
//            refreshTabButton();
        }

        public void refetchItems() {
            fetchAndDisplayData();
        }
    };

    protected void displayData() {

        activeTab.clear();
        activeTab.add(optionalFilterByKindWidgets);
        List<TreeNodeBean> allItemsToDisplay = prepareAllItemsToDisplay();
        if (paginationWidget == null) {
            pagin = new WidgetWithPagination<TreeNodeBean>(getPageSize(),
                    dataPaginator,
                    createPaginatedItemsDisplayer());
            paginationWidget = pagin.buildWidgets();
        }
        activeTab.add(paginationWidget);
        pagin.displayData(allItemsToDisplay, allItems);

    }

    protected void fetchAndDisplayData() {
        callServiceToGetData(new StandardAsyncCallback<List<TreeNodeBean>>() {
            public void onSuccess(List<TreeNodeBean> result) {
                allItems = result;
                addOptionalFilterByKindWidgets(allItems);
                displayData();
            }
        });
    }

    public void show(VerticalPanel vp) {
        vp.clear();
        activeTab.setWidth("100%");
        vp.setWidth("100%");
        vp.add(activeTab);

        addOptionalFilterByKindWidgets(allItems);
        displayData();
        //  fetchAndDisplayData();
    }

}
