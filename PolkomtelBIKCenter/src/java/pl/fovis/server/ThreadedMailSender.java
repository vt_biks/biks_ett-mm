/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import javax.mail.MessagingException;
import static pl.bssg.metadatapump.ThreadedPumpExecutor.MILLIS_FOR_24H;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.FvsChangeExBean;
import pl.fovis.common.NewsBean;
import pl.fovis.common.ObjectInFvsChangeExBean;
import pl.fovis.common.UserBean;
import pl.fovis.common.i18npoc.I18n;
import pl.trzy0.foxy.commons.MailServerConfig;
import pl.trzy0.foxy.serverlogic.mailing.SendMail;
import simplelib.BaseUtils;
import simplelib.IRegExpMatcher;
import simplelib.IRegExpPattern;
import simplelib.Pair;
import simplelib.SimpleDateUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class ThreadedMailSender extends BIKSThreadedWorker/* implements Runnable *//* implements Runnable *//* implements Runnable *//* implements Runnable *//* implements Runnable *//* implements Runnable *//* implements Runnable *//* implements Runnable */ {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected BIKSServiceBaseForSingleBIKS service;
    protected MailServerConfig cfg;
    protected String urlForPublicAccess;
    protected String dirForUpload;

    public ThreadedMailSender(BIKSServiceBaseForSingleBIKS service, MailServerConfig cfg, String urlForPublicAccess, String dirForUpload) {
        this.service = service;
        this.cfg = cfg;
        this.urlForPublicAccess = urlForPublicAccess;
        this.dirForUpload = dirForUpload;
    }

    protected void addToJobExecutorQueue(final String[] reciptiens, final String subject, final String body, final Map<String, String> embeddedFiles) {
        jobExecutor.addJob(new Runnable() {
            @Override
            public void run() {
                sendMail(reciptiens, subject, body, embeddedFiles);
            }
        });
    }

    protected void sendMailWithCC(String[] reciptiens, String[] cc, String subject, String body, Map<String, String> embeddedFiles) {
        try {
            SendMailWithLog.sendMessageWithCC(cfg,service, reciptiens, cc, subject, body, embeddedFiles);
        } catch (MessagingException ex) {
            logger.error("Error in sending mail to: " + reciptiens, ex);
            ex.printStackTrace();
        }
    }

    protected void sendMail(String[] reciptiens, String subject, String body, Map<String, String> embeddedFiles) {
        sendMailWithCC(reciptiens, new String[]{}, subject, body, embeddedFiles);
    }

    public synchronized void sendMailAdhoc(NewsBean news) {
        try {
            List<UserBean> ubs = service.getUsersToSendEmail();
            String subject = service.getSensitiveAppPropValue("emailAdhocTitle");
            String body = service.getSensitiveAppPropValue("emailAdhocBody");

            final Map<String, String> embeddedFiles = new HashMap<String, String>();
            String newsImg = createImageInEmail("news", "i1", embeddedFiles);

            body = body.replace("%{newsTitle}%", newsImg + " " + news.title)
                    .replace("%{newsBody}%", news.text)
                    .replace("%{page}%", urlForPublicAccess);

            Pair<String, Map<Integer, Pair<String, String>>> bodyAndInnerLinks = service.extractInnerLinks(body, true);
            body = bodyAndInnerLinks.v1;
            Pair<String, Map<String, String>> bodyAndEmbeddedFiles = fixHrefsImgFromEmailBody(body);
            body = bodyAndEmbeddedFiles.v1;
            embeddedFiles.putAll(bodyAndEmbeddedFiles.v2);
//            int i = 0;
            for (UserBean ub : ubs) {
//                body = body.replace("%{name}%", ub.name);
                String[] reciptiens = {ub.email};
                addToJobExecutorQueue(reciptiens, subject, body.replace("%{name}%", ub.name), embeddedFiles/*bodyAndEmbeddedFiles.v2*/);
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Ustawienie harmonogramu do wysyłania maili
     *
     * @param timeAsString - czas wysyłki maila w postaci: 9:00
     */
    public synchronized void schedule() {
        String timeAsString = service.getSensitiveAppPropValue("emailScheduleTime");
        destroy();
        timer = new Timer();
        Pair<String, String> timeToStart = BaseUtils.splitString(timeAsString, ":");
        Date dateSchedule = new Date();
        dateSchedule = SimpleDateUtils.newDate(SimpleDateUtils.getYear(dateSchedule), SimpleDateUtils.getMonth(dateSchedule), SimpleDateUtils.getDay(dateSchedule), BaseUtils.tryParseInt(timeToStart.v1), BaseUtils.tryParseInt(timeToStart.v2));
        if (dateSchedule.before(new Date())) {
            dateSchedule = SimpleDateUtils.addDays(dateSchedule, 1);
        }
        /// schedule
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getInfoAndAddToJobQueue();
            }
        }, dateSchedule, MILLIS_FOR_24H);
    }

    protected void getInfoAndAddToJobQueue() {
        List<UserBean> ubs = service.getUsersToSendEmail();

        for (UserBean ub : ubs) {
            Map<String, String> embeddedFiles = new HashMap<String, String>();
            String newsImg = createImageInEmail("news", "i1", embeddedFiles);
            StringBuilder sbNews = new StringBuilder();
            /*Aktualności*/
            List<NewsBean> nbs = service.getNotReadNewsByUserInner(ub.id);
            for (NewsBean nb : nbs) {
                sbNews.append("<h3>")
                        .append(newsImg)
                        .append("<b>")
                        .append(nb.title)
                        .append("</b></h3>")
                        .append(nb.text);
            }
            boolean issbNewsEmpty = BaseUtils.isStrEmptyOrWhiteSpace(sbNews.toString());
            if (issbNewsEmpty) {
                sbNews.append("<div style='padding-top: 20px;'>")
                        .append(I18n.brakAktualnosci.get())
                        .append("</div>");
                embeddedFiles.clear();
            }
            String news = sbNews.toString();
            Pair<String, Map<Integer, Pair<String, String>>> bodyAndInnerLinks = service.extractInnerLinks(news, true);
            news = bodyAndInnerLinks.v1;
            Pair<String, Map<String, String>> bodyAndEmbeddedFiles = fixHrefsImgFromEmailBody(news);
            news = bodyAndEmbeddedFiles.v1;
            embeddedFiles.putAll(bodyAndEmbeddedFiles.v2);
            /* Ulubione*/
            List<FvsChangeExBean> favouritesExForUser = service.getChangedFavouritesExForUser(ub.id);
            StringBuilder sbFvs = new StringBuilder();
            for (FvsChangeExBean fvs : favouritesExForUser) {
                sbFvs.append(createImageInEmail(fvs.iconName, fvs.code, embeddedFiles))
                        .append("<b>")
                        .append(createLinkInEmail(fvs.tabId, fvs.nodeId, fvs.name))
                        .append("</b> <font color='red'>(")
                        .append(BIKCenterUtils.getActionObjectInFvs(fvs))
                        .append(")</font><br/>");
                List<ObjectInFvsChangeExBean> fvsc = service.getChangedDescendantsObjectInFvsEx(fvs.nodeId, ub.id);
                sbFvs.append("<ul>");
                for (ObjectInFvsChangeExBean c : fvsc) {
                    sbFvs.append("<li style=\"list-style: none\">")
                            .append(createImageInEmail(c.iconName, c.code, embeddedFiles))
                            .append(createLinkInEmail(c.tabId, c.nodeId, c.name))
                            .append("<font color='red'> (")
                            .append(BIKCenterUtils.actionsDescendantsObjectInFvs(c))
                            .append(")</font>")
                            .append("</li>");
                }
                sbFvs.append("</ul>");
            }
            String fvs = sbFvs.toString();
            boolean isFvsEmpty = BaseUtils.isStrEmptyOrWhiteSpace(fvs);
            if (isFvsEmpty) {
                fvs = I18n.brakzmianwulubionych.get();
            }

            String se;
            Pair<String, Map<String, String>> suggestedElements = getSuggestedElements(ub.id);
            boolean isSuggestedElementsEmpty = suggestedElements == null;
            if (!isSuggestedElementsEmpty) {
                se = suggestedElements.v1;
                embeddedFiles.putAll(suggestedElements.v2);
            } else {
                se = I18n.brakSugerowanychElementow.get();
            }

            final String subject = service.getSensitiveAppPropValue("emailScheduleTitle");
            String body = service.getSensitiveAppPropValue("emailScheduleBody");

            final String[] reciptiens = {ub.email};

            body = body//.replace("%{bodyFvs}%", fvs)
                    //                    .replace("%{suggestedElements}%", se)
                    .replace("%{name}%", ub.name)
                    .replace("%{page}%", urlForPublicAccess);
            body = replaceMailBody(body, fvs, isFvsEmpty, "<tr class=\"favorite\">", "</tr class=\"favorite\">", "%{bodyFvs}%");
            body = replaceMailBody(body, news, issbNewsEmpty, "<tr class=\"news\">", "</tr class=\"news\">", "%{bodyNews}%");
            body = replaceMailBody(body, se, isSuggestedElementsEmpty, "<tr class=\"suggested\">", "</tr class=\"suggested\">", "%{suggestedElements}%");

            if (isFvsEmpty && issbNewsEmpty) {
//            if (BaseUtils.isCollectionEmpty(favouritesExForUser) && BaseUtils.isCollectionEmpty(nbs)) {
                continue;
            }
            addToJobExecutorQueue(reciptiens, subject, body, embeddedFiles);
        }
    }

    protected String replaceMailBody(String mailBody, String bodyFvsOrNews, boolean isFvsOrNewsEmpty, String prefix, String suffix, String textToReplace) {
        if (isFvsOrNewsEmpty) {
            int prefixIdx = mailBody.indexOf(prefix);
            int suffixIdx = mailBody.indexOf(suffix);
            if (prefixIdx != -1 && suffixIdx != -1) {
                String startBody = mailBody.substring(0, prefixIdx);
                String endBody = mailBody.substring(suffixIdx, mailBody.length()).replace(suffix, "");
                mailBody = startBody + " " + endBody;
            }
        }
        mailBody = mailBody.replace(textToReplace, bodyFvsOrNews)
                .replace(prefix, "<tr>")
                .replace(suffix, "</tr>");
        return mailBody;
    }

    protected String createImageInEmail(String iconName, String code, Map<String, String> files) {
        String imgWithPath = getImgResourceInWar(iconName);
        if (imgWithPath != null) {

            files.put(code, getImgResourceInWar(iconName));
            return "<img src=\"cid:" + code + "\"> ";
        }
        return "";
    }

    protected String getImgResourceInWar(String img) {
        URL resource = getClass().getClassLoader().getResource("images/" + img + ".gif");
        if (resource == null) {
            return null;
        }
        String path = resource.getPath();
        if (logger.isInfoEnabled()) {
            logger.info("Stara sciezka: " + path);
        }
        path = path.replace("%20", " ");
        if (logger.isInfoEnabled()) {
            logger.info("Nowa sciezka: " + path);
        }
        return path;
    }

    protected String createLinkInEmail(String treeCode, int nodeId, String name) {
        StringBuilder url = new StringBuilder();
        url.append("<" + "a" /* I18N: no */).append(" " + "href=" /* I18N: no */)
                .append(urlForPublicAccess)
                .append("/#")
                .append(treeCode)
                .append("/").append(nodeId)
                .append(" style=\"text-decoration: none ;color: #094f8b\"") //#094f8b
                .append(">&nbsp;")
                .append(name)
                .append("</a>");
        return url.toString();
    }

    protected Pair<String, Map<String, String>> fixHrefsImgFromEmailBody(String emailBody) {
        StringBuilder buffer = new StringBuilder();
        IRegExpPattern htmlUrlPattern
                = BaseUtils.getRegExpMatcherFactory().compile("fsb\\?get=" + "file_([a-zA-Z0-9]*).([a-zA-Z0-9]*)",
                        true, true);
        IRegExpMatcher htmlUrlMatcher = htmlUrlPattern.matcher(emailBody);
        int index = 0;
        Map<String, String> files = new HashMap<String, String>();
        while (htmlUrlMatcher.find()) {
            buffer.append(emailBody.substring(index, htmlUrlMatcher.start()));
            String file = "file_" + htmlUrlMatcher.group(1) + "." + htmlUrlMatcher.group(2);
            files.put(htmlUrlMatcher.group(0), dirForUpload + "/" + file);
            buffer.append("cid:").append(htmlUrlMatcher.group(0));
            buffer.append("");
            index = htmlUrlMatcher.end();

        }
        if (index < emailBody.length()) {
            buffer.append(emailBody.substring(index));
        }

        return new Pair<String, Map<String, String>>(buffer.toString(), files);
    }

    protected Pair<String, Map<String, String>> getSuggestedElements(int userId) {
        List<FvsChangeExBean> ses = service.getSuggestedElements(userId);
        if (ses.isEmpty()) {
            return null;
        }
        StringBuilder suggestedElements = new StringBuilder();
        Map<String, String> embeddedFiles = new HashMap<String, String>();
        for (FvsChangeExBean se : ses) {
            suggestedElements
                    .append("<p><b>")
                    .append(createImageInEmail(se.iconName, se.code, embeddedFiles))
                    .append(createLinkInEmail(se.tabId, se.nodeId, se.name))
                    .append("</b></p>");
        }
        return new Pair<String, Map<String, String>>(suggestedElements.toString(), embeddedFiles);
    }
}
