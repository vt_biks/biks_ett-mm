/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class DataQualityGroupBean implements Serializable {

    public int nodeId;
    public int idTest;
    public String treeCode;
    public String name; // nazwa testu
    public String type; // nodeKind.code
    public String testHint; // opis do nazwy testu - bussiness name
    public Date requestHint; // data wykonania requestu
    public String link; // link do raportu
    public boolean isSuccess;
    public boolean isError;
    public int requestId;
}
