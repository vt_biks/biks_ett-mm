/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.common.SapBoUniverseTableBean;
import simplelib.IContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author AMamcarz
 */
public class TableWidget extends EntityDetailsWidgetFlatBase {

    protected VerticalPanel mainVp;
//    protected VerticalPanel gridPanel;
    protected BikCustomButton btnShowSQL = null;
    protected IMainHeaderSharedTable imhst;

    public TableWidget(IMainHeaderSharedTable imhst) {
        this.imhst = imhst;
    }

    @Override
    public boolean hasContent() {
        return (getData().tables != null);
    }

    @Override
    public void displayData() {
        fetchAndShowTableEx(getData().tables);
        /*
         * gridPanel.clear(); if (data.tables != null) { // int noteCnt =
         * data.tables.size(); // Grid gp = new Grid(noteCnt * 2, 4); //
         * gp.setWidth("100%"); // gp.setStyleName("gridJoinedObj"); // int row
         * = 0; // for (int i = 0; i < noteCnt; i++) { final
         * SapBoUniverseTableBean table = data.tables;//.get(i); VerticalPanel
         * gp = new VerticalPanel(); gp.add(new HTML(table.type)); btnShowSQL =
         * new BikCustomButton("Pokaż SQL", "lookupTrashBtn", new
         * IContinuation() {
         *
         * public void doIt() { getForms().viewSQLDef("SQL", (table.fhsql !=
         * null ? table.fhsql : "-")); } }); // gp.add(new HTML(table.fhsql));
         * if (table.fhsql != null && !table.fhsql.equals("")) {
         * gp.add(btnShowSQL); }
         *
         * // gp.setWidget(row, 0, BikIcon.createIcon("images/table.gif",
         * "RelatedObj-ObjIcon")); // gp.setWidget(row, 1, new
         * HTML(table.name)); // gp.setWidget(row, 2, new HTML(table.type)); //
         * gp.setWidget(row, 3, new HTML(table.fhsql)); //
         * gp.getRowFormatter().setStyleName(row, row == 0 || row % 2 == 0 ?
         * "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey"); // row++; // }
         * gridPanel.add(gp);
        }
         */
    }

    public void fetchAndShowTableEx(SapBoUniverseTableBean tables) {
//        gridPanel.clear();
        if (tables != null) {
            final SapBoUniverseTableBean table = tables;
//            VerticalPanel gp = new VerticalPanel();
//            gp.add(new HTML(table.type));
            imhst.addTranslatedRow(null, null, new HTML(table.type));
            btnShowSQL = new BikCustomButton(I18n.pokazSQL.get() /* I18N:  */, "lookupTrashBtn",
                    new IContinuation() {

                        public void doIt() {
                            getForms().viewSQLDef("SQL" /* I18N: no */, (table.fhsql != null ? table.fhsql : "-"));
                        }
                    });
            if (table.fhsql != null && !table.fhsql.equals("")) {
//                gp.add(btnShowSQL);
                imhst.addTranslatedRow(null, null, btnShowSQL);
            }
//            gridPanel.add(gp);
        }
    }

    public Widget buildWidgets() {
        mainVp = new VerticalPanel();
//        gridPanel = new VerticalPanel();
//        mainVp.add(gridPanel);
        return mainVp;
    }

    public boolean isEnabledDeprecated(String code, int count) {
        return true;
    }
}
