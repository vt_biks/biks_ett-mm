/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.ErwinDataModelProcessObjectsRel;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.lisa.ImportOverviewBean;
import pl.fovis.common.lisa.LisaAuthenticationBean;
import pl.fovis.common.lisa.LisaConstants.ImportMode;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryBean;
import pl.fovis.common.lisa.LisaDictionaryValue;
import pl.fovis.common.lisa.LisaGridParamBean;
import pl.fovis.common.lisa.LisaImportActionBean;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import pl.fovis.common.lisa.LisaLogBean;
import pl.fovis.common.lisa.TeradataException;
import simplelib.Pair;

/**
 *
 * @author tbzowka
 */
@RemoteServiceRelativePath("boxiservice")
public interface LisaService extends RemoteService {

    public List<LisaDictColumnBean> getDictionaryColumns(String dictionaryName);

    public void updateDictionaryColumns(LisaDictColumnBean bean) throws TeradataException;

    public List<LisaDictColumnBean> getDictionaryColumns(Integer categoryNodeId);

    public List<LisaDictColumnBean> getDictionaryColumnsForDic(Integer dicNodeId);

    public List<Map<String, LisaDictionaryValue>> getDictionaryRecords(Integer instanceId, LisaGridParamBean params, String requestTicket) throws TeradataException;

    public Integer getCountDictionaryRecords(Integer instanceId, LisaGridParamBean params, String requestTicket) throws TeradataException;

    public List<LisaDictionaryBean> getDictionaries(Integer instanceId);

//    public void updateDictionaryContent(Map<String, LisaDictionaryValue> record, Integer categoryNodeId, boolean fromDictonary) throws TeradataException;
    public void updateDictionaryContents(Integer instanceId, List<LisaDictColumnBean> columnInfo, Set<Map<String, LisaDictionaryValue>> record, Integer categoryNodeId, boolean fromDictonary) throws TeradataException;

    public void updateDictionaryContents(Integer instanceId, Set<Map<String, LisaDictionaryValue>> record, Integer categoryNodeId, boolean fromDictonary) throws TeradataException;

    public void deleteDictionaryContent(Integer instanceId, Set<Map<String, LisaDictionaryValue>> record, Integer categoryNodeId, boolean fromDictonary) throws TeradataException;

    public void insertDictionaryContent(Integer instanceId, List<LisaDictColumnBean> columnInfo, Map<String, LisaDictionaryValue> record, Integer categoryNodeId, boolean fromDictonary) throws TeradataException;

    public void insertDictionaryContent(Integer instanceId, Map<String, LisaDictionaryValue> record, Integer categoryNodeId, boolean fromDictonary) throws TeradataException;

    public TreeNodeBean saveNewCategory(Integer instanceId, String categoryName, Integer categoryTypeNodeId) throws TeradataException;

    public List<String> deleteCategory(Integer instanceId, TreeNodeBean nodeToDelete) throws TeradataException;

    public List<String> moveCategory(Integer instanceId, TreeNodeBean nodeToMove, final Integer newCategId) throws TeradataException;

    public Pair<TreeNodeBean, TreeNodeBean> saveNewCategorization(Integer instanceId, final String categorizationName, Integer dictionaryParentNodeId) throws TeradataException;

    public void authorizeTeradataConnection(Integer instanceId, String username, String password) throws TeradataException;

    public void moveValuesToCategory(Integer instanceId, Set<Map<String, LisaDictionaryValue>> values, Integer oldCategoryNodeId, Integer newCategoryNodeId) throws TeradataException;
//    public LisaTeradataConnectionBean getLisaConnectionData();
//    public void updateLisaConnectionData(LisaTeradataConnectionBean bean);

    public List<LisaDictColumnBean> getDictionaryColumnsByNodeId(Integer nodeId);

    public List<LisaLogBean> getLisaLogs(LisaGridParamBean params);

    public String addNewDictionary(Integer instanceId, LisaDictionaryBean b) throws TeradataException;

    public void deleteDictionary(Integer instanceId, String dictionaryName) throws TeradataException;

//    public Boolean checkLisaUnassignedCategory() throws TeradataException;
    public Boolean deleteAllTeredataSessions() throws TeradataException;

    public List<String> getDistinctColumnValues(Integer nodeId, LisaDictColumnBean bean, String value, int numberFetchItem);

//    public Integer checkIsUniqueColumn(String dictName, String colName);
    public String getOriginalValueFromReference(Integer instanceId, LisaDictColumnBean columnBean, String value);

    public List<String> getValidationList(int columnIdInBIK);

    public void addValidationItem(int columnIdInBIK, String value);

    public void deleteValidationItem(int columnIdInBIK, String value);

    public List<ErwinDataModelProcessObjectsRel> getDataModelProcessObjectsRel(int nodeId, String areaName, boolean dashboardObjects);

    public LisaAuthenticationBean getLisaAuthentication(int nodeId);

    public void saveOrAddLisaAuthentication(LisaAuthenticationBean bean);

    public Boolean tryLoginToTeradata(Integer instanceId, Integer nodeId) throws TeradataException;

    public void addLisaFolder(int parentNodeId, String folderName);

    public String exportDictionary(Integer instanceId, Integer nodeId, List<String> mappedNames) throws TeradataException;

    public List<String> extractColumnNamesFromFile(String serverFileName);

    public void importDictionary(Integer instanceId, int nodeId, Map<String, String> mappedNames, String serverFileName, ImportMode importMode) throws TeradataException;

    public ImportOverviewBean analyseDictWithImportedFile(Integer instanceId, int nodeId, Map<String, String> mappedNames, String serverFileName, ImportMode importMode) throws TeradataException;

    public void clearLisaImportCache(String serverFileName);

    public List<LisaImportActionBean> getActionList(String serverFileName, String actionCode, int offset, int limit);

    public Integer getCategoryzationNodeId(Integer categoryId);

    public Boolean validateRecords(Integer instanceId, List<LisaDictColumnBean> columnInfo, Set<Map<String, LisaDictionaryValue>> records, Integer nodeId, boolean editFromDictonary);

    public List<LisaInstanceInfoBean> getLisaInstanceList();

    public void updateLisaInstanceInfo(LisaInstanceInfoBean bean);

    public void addLisaInstanceInfo(LisaInstanceInfoBean bean);

    public void deleteLisaInstance(Integer instanceId) throws TeradataException;

    public LisaInstanceInfoBean getLisaInstanceByNodeId(int nodeId);
}
