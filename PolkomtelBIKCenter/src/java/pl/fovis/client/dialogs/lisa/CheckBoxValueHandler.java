/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.user.client.ui.CheckBox;
import pl.fovis.foxygwtcommons.dialogs.validators.FoxyGWTValidationException;
import pl.fovis.foxygwtcommons.dialogs.valuehandlers.IWidgetValueHandler;

/**
 *
 * @author tbzowka
 */
class CheckBoxValueHandler implements IWidgetValueHandler<CheckBox> {

    @Override
    public boolean isWidgetClassHandled(Object widget) {
        return widget instanceof CheckBox;
    }

    @Override
    public Object getValueFromWidget(CheckBox widget, Class targetValueClass, GetValueExtraInfo extraInfo) throws FoxyGWTValidationException {
        return widget.getValue();
    }

    @Override
    public void setValueToWidget(CheckBox widget, Object value, SetValueExtraInfo extraInfo) {
        widget.setValue((Boolean) value);
    }
}
