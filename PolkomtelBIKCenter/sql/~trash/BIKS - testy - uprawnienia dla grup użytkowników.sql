-----------------------------------------------------
-- specjalne tabelki pomocniczne itscc
-- iterative tarjan's strongly connected components
-----------------------------------------------------

if exists(select * from sys.objects where object_id = object_id(N'itscc_graph_edge') and type in (N'U'))
  drop table itscc_graph_edge;
go

if exists(select * from sys.objects where object_id = object_id(N'itscc_graph_node') and type in (N'U'))
  drop table itscc_graph_node;
go

create table itscc_graph_node (node_id varchar(17) not null primary key);
go

create table itscc_graph_edge (src_node_id varchar(17) not null references itscc_graph_node(node_id),
  dst_node_id varchar(17) not null references itscc_graph_node(node_id),
  unique (src_node_id, dst_node_id), unique (dst_node_id, src_node_id));
go



-----------------------------------------------------
-- pierwsza procedura testowa, test bardzo prosty
-----------------------------------------------------
if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_test_sug_grants_1]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_test_sug_grants_1]
go

create procedure [dbo].[sp_test_sug_grants_1] as
begin
  set nocount on;

  delete from itscc_graph_edge;
  delete from itscc_graph_node;

  insert into itscc_graph_node (node_id) values
  ('A'), ('B'), ('C');

  insert into itscc_graph_edge (src_node_id, dst_node_id) values
  ('A', 'B'),
  ('B', 'C');
  
  delete from bik_custom_right_role_user_entry where group_id is not null;
  delete from bik_system_group_in_group;
  delete from bik_system_user_in_group;
  delete from bik_system_user_group;

  insert into bik_system_user_group (name, is_built_in)
  select node_id, 0
  from itscc_graph_node;

  insert into bik_system_group_in_group (group_id, member_group_id)
  select pg.id, cg.id
  from itscc_graph_edge e inner join bik_system_user_group pg on e.src_node_id = pg.name
  inner join bik_system_user_group cg on e.dst_node_id = cg.name;
  
  insert into bik_custom_right_role_user_entry (role_id, group_id)
  values
    ((select id from bik_custom_right_role where code = 'DqmAdmin'), (select id from bik_system_user_group where name = 'A'));


  insert into bik_system_user_in_group (user_id, group_id)
  values
    ((select id from bik_system_user where login_name = 'aqq'), (select id from bik_system_user_group where name = 'C'));

  exec sp_calc_itscc_for_sugs;

  exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;

  select t.code, crrabg.branch_ids, crrabg.action_id, na.code
  from bik_custom_right_role_action_branch_grant crrabg inner join bik_tree t on t.id = crrabg.tree_id
    inner join bik_node_action na on crrabg.action_id = na.id
  where user_id = (select id from bik_system_user where login_name = 'aqq');
end;
go

-- select * from bik_system_user_group;

-- exec sp_test_sug_grants_1;



------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------


-----------------------------------------------------
-- uniwersalna procedura testowa (all in one, aio)
-----------------------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_test_sug_grants_aio]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_test_sug_grants_aio]
go

-- aio = all in one ;-)
create procedure [dbo].[sp_test_sug_grants_aio](@groups varchar(max), @rights_for_groups varchar(max), @users_in_groups varchar(max)) as
begin
  set nocount on;

  delete from itscc_graph_edge;
  delete from itscc_graph_node;

  declare @chains table (idx int not null primary key, chain varchar(max));

  insert into @chains select fs.idx, fs.str from dbo.fn_split_by_sep(@groups, ',', 7) fs;

  insert into itscc_graph_node (node_id)
  select distinct ss.str
  from @chains c cross apply dbo.fn_split_by_sep(c.chain, '>', 7) ss;

  declare @chain_cnt int = (select count(*) from @chains);

  declare @chain_num int = 1;
  
  while @chain_num <= @chain_cnt
  begin
    declare @chain varchar(max) = (select chain from @chains where idx = @chain_num);
    declare @chain_parts table (idx int not null primary key, part varchar(max));
    delete from @chain_parts;
    insert into @chain_parts select idx, str from dbo.fn_split_by_sep(@chain, '>', 7) ss;
    
    declare @part_cnt int = (select count(*) from @chain_parts);
    declare @part_num int = 1;
    
    while @part_num < @part_cnt
    begin
      declare @src_node_id varchar(max) = (select part from @chain_parts where idx = @part_num);
      declare @dst_node_id varchar(max) = (select part from @chain_parts where idx = @part_num + 1);
    
      --print '@src_node_id = ' + @src_node_id + ', @dst_node_id = ' + @dst_node_id;
    
      if not exists (select 1 from itscc_graph_edge where src_node_id = @src_node_id and dst_node_id = @dst_node_id)
        insert into itscc_graph_edge (src_node_id, dst_node_id) values (@src_node_id, @dst_node_id);
    
      set @part_num += 1;
    end;
    
    set @chain_num += 1;
  end;

  --select * from itscc_graph_node;
  --select * from itscc_graph_edge;

  delete from bik_custom_right_role_user_entry where group_id is not null;
  delete from bik_system_group_in_group;
  delete from bik_system_user_in_group;
  delete from bik_system_user_group;

  insert into bik_system_user_group (name, is_built_in)
  select node_id, 0
  from itscc_graph_node;

  insert into bik_system_group_in_group (group_id, member_group_id)
  select pg.id, cg.id
  from itscc_graph_edge e inner join bik_system_user_group pg on e.src_node_id = pg.name
  inner join bik_system_user_group cg on e.dst_node_id = cg.name;

  exec sp_calc_itscc_for_sugs;

  --select * from bik_system_user_group;
  
  ------------------------------------------------------------------------
  -- uprawnienia dla grup
  --   declare @rights_for_groups varchar(max) = 'A:DqmAdmin,Z:DqmEditor@DQM,B:DqmUser@201848';

  delete from bik_custom_right_role_user_entry where role_id is not null;
  
  declare @rfg table (idx int not null primary key, g varchar(max));
  
  insert into @rfg (idx, g)
  select idx, str from dbo.fn_split_by_sep(@rights_for_groups, ',', 7);
  
  declare @rfg_cnt int = (select count(*) from @rfg);
  declare @rfg_num int = 1;
  
  while @rfg_num <= @rfg_cnt
  begin
    declare @rfg_g varchar(max) = (select g from @rfg where idx = @rfg_num);
    set @rfg_num += 1;
  
    declare @grp_colon_pos int = charindex(':', @rfg_g);
  
    if @grp_colon_pos = 0
    begin
      print 'bad right-for-group entry: ' + quotename(@rfg_g, '"') + ', idx = ' + cast(@rfg_num - 1 as varchar(20));      
      continue;
    end;
  
    declare @grp_name varchar(max) = substring(@rfg_g, 1, @grp_colon_pos - 1);
    declare @grp_role varchar(max) = substring(@rfg_g, @grp_colon_pos + 1, len(@rfg_g));
    
    declare @grp_tree_or_node_pos int = charindex('@', @grp_role);
    declare @grp_tree_or_node varchar(max) = case when @grp_tree_or_node_pos = 0 then null else substring(@grp_role, @grp_tree_or_node_pos + 1, len(@grp_role)) end;
  
    if @grp_tree_or_node_pos <> 0 set @grp_role = substring(@grp_role, 1, @grp_tree_or_node_pos - 1);
  
    declare @grp_id int = (select id from bik_system_user_group where name = @grp_name);
    
    declare @grp_role_id int = (select id from bik_custom_right_role where code = @grp_role);
    
    declare @grp_tree_id int = (select id from bik_tree where code = @grp_tree_or_node);
    
    declare @grp_node_id int = null;
    
    if @grp_tree_id is null and isnumeric(@grp_tree_or_node) = 1
    begin
      set @grp_node_id = cast(@grp_tree_or_node as int);
      select @grp_tree_id = tree_id from bik_node where id = @grp_node_id;
    end;
  
    if @grp_id is null or @grp_role_id is null or (@grp_tree_or_node is not null and @grp_tree_id is null)
    begin
      print 'bad right-for-group entry: ' + quotename(@rfg_g, '"') + ', idx = ' + cast(@rfg_num - 1 as varchar(20)) +
        ', @grp_role = ' + @grp_role +
        case when @grp_id is null then ', @grp_id is null' else '' end +
        case when @grp_role_id is null then ', @grp_role_id is null' else '' end +
        case when @grp_tree_id is null then ', @grp_tree_id is null' else '' end +
        case when @grp_node_id is null then ', @grp_node_id is null' else '' end;              
      continue;
    end;
  
    insert into bik_custom_right_role_user_entry (group_id, role_id, tree_id, node_id)
    values (@grp_id, @grp_role_id, @grp_tree_id, @grp_node_id);
  end;
  
  select e.*, r.code, g.name
  from bik_custom_right_role_user_entry e inner join bik_system_user_group g on e.group_id = g.id
    inner join bik_custom_right_role r on e.role_id = r.id
  where role_id is not null;
  
  ---------------------------------------------------------------------------
  --   declare @users_in_groups varchar(max) = 'aqq:B+C,zzz:A+X+Y';

  declare @uig table (login varchar(max), group_name varchar(max));
  
  insert into @uig
  select distinct ltrim(rtrim(substring(f.str, 1, charindex(':', f.str) - 1))) as login, s.str as group_name
  from dbo.fn_split_by_sep(@users_in_groups, ',', 7) f
    cross apply dbo.fn_split_by_sep(substring(f.str, charindex(':', f.str) + 1, len(f.str)), '+', 7) s
  ; --order by login, group_name
  
  insert into bik_system_user (login_name, password, is_disabled, name)
  select distinct login, substring(login, 1, 1), 0, login
  from @uig where login not in (select login_name from bik_system_user);
  
  delete from bik_system_user_in_group
  where user_id in (select id from bik_system_user where login_name in (select login from @uig));
  
  insert into bik_system_user_in_group (user_id, group_id)
  select su.id, sug.id
  from @uig uig inner join bik_system_user su on uig.login = su.login_name
    inner join bik_system_user_group sug on sug.name = uig.group_name;

  exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;

  select 
    --bg.*, su.login_name, t.code as tree_code
    su.login_name, t.code as tree_code, bg.branch_ids, count(*) as action_cnt, min(action_id) as min_action_id, max(action_id) as max_action_id
  from bik_custom_right_role_action_branch_grant bg inner join bik_system_user su on bg.user_id = su.id
  inner join bik_tree t on bg.tree_id = t.id
  where su.login_name in (select login from @uig)
  group by su.login_name, t.code, bg.branch_ids
  order by su.login_name, tree_code, branch_ids;
end;
go



/*
  declare @groups varchar(max) = 'A>B>C, B>D>A, X> Y > Z>X, C>Z';
  declare @rights_for_groups varchar(max) = 'A:DqmAdmin,Z:DqmEditor@DQM,B:DqmUser@201848';
  declare @users_in_groups varchar(max) = 'aqq:B+C,zzz:A+X+Y';

  select * from bik_custom_right_role

select * from bik_node_action_in_custom_right_role
where role_id = 1

DQM, DQMDokumentacja
18, 70


  select * from bik_tree

select name, branch_ids, * from bik_node where tree_id = 70 order by 2

2015-07-08 Ocena jako�ci danych - Insourcer / Podinsourcer	116797|272270|272340|

2015-07-08 Ocena jako�ci danych - Insourcer / Podinsourcer	272338|272339|272343|

2015-07-08 Ocena jako�ci danych - Insourcer / Podinsourcer	272338|272346|272347|



  exec sp_test_sug_grants_aio 'A>B>C', 'A:DqmEditor@DQM, C:DqmUser@272340, B:DqmUser@116797', 'zzz:C, xxx:B'


  exec sp_test_sug_grants_aio 'A>B>C>D', 'A:DqmEditor@DQM, C:DqmUser@116797, B:DqmUser@272340, D:DqmUser@DQMDokumentacja', 'zzz:C, xxx:B, yyy:D'

  exec sp_test_sug_grants_aio 'A>B>C>D', 'A:DqmEditor@DQM, C:DqmUser@116797, B:DqmUser@272340, D:DqmUser@DQMDokumentacja', 'zzz:C, xxx:B, yyy:D+A'

  exec sp_test_sug_grants_aio 'A>B>C>D>B, D>E, A1>B1>C1', 'A:DqmEditor@DQM, B:DqmUser@272340, C:DqmUser@272343, D:DqmUser@272347, A1:DqmEditor@DQMDokumentacja, E:DqmUser@DQMDokumentacja', 'xxx:B, yyy:C, zzz:D, xx1:E, xx2:C1'


*/

