/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.tags;

import commonlib.LameUtils;
import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import simplelib.BaseUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class LogTag extends TagBase {

    public enum LogTagMode {

        Verbose, Silent, Render
    };
    private static final ILameLogger logger = LameUtils.getMyLogger();
    private String loggerKind;
    private LogTagMode loggerMode;
    private boolean loggerEnabled;

    @Override
    protected void initSelf() {
        loggerKind = attrs.get("kind");
        String modeStr = attrs.get("mode");
        if (modeStr == null) {
            loggerMode = LogTagMode.Verbose;
        } else {
            loggerMode = LogTagMode.valueOf(BaseUtils.capitalize(modeStr));
        }
    }

    @Override
    protected void prepareSelf() {
        loggerEnabled = renderEnv.isEnabledLoggerKind(loggerKind);
    }

    @Override
    public void internalRender(IRenderCtx renderCtx) {
        if (!loggerEnabled || !logger.isDebugEnabled()) {
            return;
        }

        String res = FoxyMillUtils.renderToString(renderCtx, subTags);
        if (loggerMode == LogTagMode.Verbose) {
            logger.debug("<" + (loggerKind == null ? "" : loggerKind) + "> " + res);
        } else if (loggerMode == LogTagMode.Render) {
            renderCtx.getOutputDevice().append(res);
        } // else be silent
    }
}
