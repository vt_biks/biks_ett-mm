/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.DateUtils;
import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.gwtservlet.AdhocSqlsName;
import pl.fovis.client.MultiKSService;
import pl.fovis.common.AppPropBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BIKRightRoles;
import pl.fovis.common.MultixReturnMessage;
import pl.fovis.common.MultixStatisticsBean;
import pl.fovis.common.ServerRegistrationDataBean;
import pl.fovis.common.StartupDataBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.TokenType;
import static pl.fovis.common.TokenType.INVITATION;
import static pl.fovis.common.TokenType.REGISRTATION;
import static pl.fovis.common.TokenType.RESET_PASSWORD;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.UserBean;
import pl.fovis.common.UserExtBean;
import pl.fovis.common.UserRegistrationDataBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.mltx.DatabaseListBoxData;
import pl.fovis.common.mltx.DatabaseListboxElem;
import pl.fovis.common.mltx.IdName;
import pl.fovis.common.mltx.InternalUserBean;
import pl.fovis.common.mltx.InvitationConfirmResult;
import pl.fovis.common.mltx.InvitationResult;
import pl.fovis.common.mltx.MultiRegistrationBean;
import pl.fovis.common.mltx.MultiXAppPropBean;
import pl.fovis.common.mltx.MultiXDatabaseTemplateBean;
import pl.fovis.common.mltx.MultixConstants;
import pl.fovis.common.mltx.MultixRegisteredUserBean;
import pl.fovis.common.mltx.MultixStatisticsAuthenticationBean;
import pl.fovis.common.mltx.MultixStatsEventType;
import pl.fovis.server.mltx.MultiXRegistrationStatus;
import pl.trzy0.foxy.commons.FoxyRuntimeException;
import pl.trzy0.foxy.commons.MailServerConfig;
import pl.trzy0.foxy.serverlogic.mailing.SendMail;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IContinuationWithReturn;
import simplelib.Pair;
import simplelib.i18nsupport.I18nMessage;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pku
 */
@AdhocSqlsName("adhocSqlsMultiX.xml")
public class MultiKSServiceImpl extends BIKSServiceBase implements MultiKSService, IMultiXLoginsSubservice {

    private final static ILameLogger logger = LameUtils.getMyLogger();
    public static final String CHARS_FOR_DB_NAME_SUFFIX_TOKEN = "0123456789ABCDEFGHJKLMNPQRSTUVWXYZ"; //34 znaki
    private final int TOKEN_LENGTH = 128;
    private final int MAX_INVITATIONS = 5;
    private final int VALIDITY_PERIOD = 1000 * 60 * 60 * 24;
//    private IBIKSStartupSubservice startupSubservice;
//    private BOXIService boxiService;
//    private IBIKSUserCreationSubservice userCreationSubservice;

//    MultiKSServiceImpl(BOXIServiceImpl boxiService) {
//        this.boxiService = boxiService;
//    }
    @Override
    protected void initWithConfig(BIKCenterConfigObj configBean) {
        super.initWithConfig(configBean);

        if (isMultiXMode()) {
            setSharedMailServerCfg(readMailServerConfigFromMasterBiks());
        }

//        setUrlForPublicAccess(configBean.urlForPublicAccess);
//        DynamicWaffleSecurityFilter.setUseWaffle(
//                !isMultiXMode()
//                && startupSubservice.getShowSSOLoginDialog());
    }

    @PerformOnMasterBiksDb
    protected MailServerConfig readMailServerConfigFromMasterBiks() {
        Map<String, MultiXAppPropBean> emailProps = BaseUtils.projectToMap(createBeansFromNamedQry("getMultiXEmailProperties", MultiXAppPropBean.class),
                new BaseUtils.Projector<MultiXAppPropBean, String>() {
                    @Override
                    public String project(MultiXAppPropBean val) {
                        return val.name;
                    }
                });

        // String host, String port, String user, String password, String fromAddress, boolean ssl
        MailServerConfig res = new MailServerConfig(
                emailProps.get(MultixConstants.MULTIX_PROP_EMAIL_SMTP_HOST_NAME).val,
                emailProps.get(MultixConstants.MULTIX_PROP_EMAIL_SMTP_PORT).val,
                emailProps.get(MultixConstants.MULTIX_PROP_EMAIL_USER).val,
                emailProps.get(MultixConstants.MULTIX_PROP_EMAIL_PASSWORD).val,
                emailProps.get(MultixConstants.MULTIX_PROP_EMAIL_FROM).val,
                BaseUtils.tryParseBoolean(emailProps.get(MultixConstants.MULTIX_PROP_EMAIL_SSL).val));

        return res;
    }

//    public void setStartupSubservice(IBIKSStartupSubservice startupSubservice) {
//        this.startupSubservice = startupSubservice;
//    }
//
//    public void setUserCreationSubserrvice(IBIKSUserCreationSubservice userCreationSubservice) {
//        this.userCreationSubservice = userCreationSubservice;
//    }
//    protected void setUrlForPublicAccess(String url) {
//        this.urlForPublicAccess = url;
//    }
    @Override
    @PerformOnDefaultDb
    public StartupDataBean getStartupData() {
        StartupDataBean sdb;

        final boolean isMultiXMode = isMultiXMode();

        boolean hasNoSingleBiksDb = getOptSingleBiksDbForCurrentRequest() == null;

        if (isMultiXMode && hasNoSingleBiksDb) {
//            if (isUserLoggedIn()) {
//                sdb = startupSubservice.getStartupData();
//            } else {
            sdb = new StartupDataBean();
            sdb.appProps = //startupSubservice.getNonSensitiveAppProps();
                    getNonSensitiveMltxAppProps();
            sdb.appVersion = MultixConstants.MLTX_APP_VERSION;
            sdb.canSendMails = isSharedMailServerCfgValid();

            String optLoggedUserName = getLoggedUserName();

            if (optLoggedUserName != null) {
                sdb.defaultSingleBiksDb = getDefaultDbNameForMultiXLogin(optLoggedUserName);
            }
//            }
        } else {
//            sdb = startupSubservice.getStartupData();
            sdb = performOnSingleBiks(new IContinuationWithReturn<StartupDataBean>() {

                @Override
                public StartupDataBean doIt() {
                    return systemUsersSubservice.getStartupData();
                }
            });

            if (isMultiXMode) {
                sdb.isUserCreatorOfThisDb = isLoggedUserCreatorOfCurrentSingleBiksDb();
            }
        }

        sdb.isMultiMode = isMultiXMode;

        return sdb;
    }

    @PerformOnMasterBiksDb
    protected List<AppPropBean> getNonSensitiveMltxAppProps() {
        List<AppPropBean> appProps = createBeansFromNamedQry("getNonSensitiveMltxAppProps", AppPropBean.class);
        return appProps;
    }

    @Override
    @RequireSingleBiksDb
    @PerformOnAnyDb
    public void addMultiXLoginToExistingDb(String login) {
        //addMultiXLoginToExistingDb(login, getCurrentDatabase().get());
        addMultiXLoginToExistingDb(login, getSingleBiksDbForCurrentRequest());
    }

    @Override
    @PerformOnAnyDb
    public void addMultiXLoginToExistingDb(String login, String dbName) {
        if (!isMultiXMode()) {
            return;
        }

        performQueryOnMasterBiks("addMultiXLoginToExistingDb", login, dbName);
    }

    @Override
    @RequireSingleBiksDb
    @PerformOnAnyDb
    public void replaceMultiXLogin(String login, String oldLogin) {
        if (!isMultiXMode() || BaseUtils.safeEquals(login, oldLogin)) {
            return;
        }
        //performQueryOnDefaultDatabase(true, "replaceMultiXLogin", login, oldLogin, getCurrentDatabase().get());
        performQueryOnMasterBiks("replaceMultiXLogin", login, oldLogin, getSingleBiksDbForCurrentRequest());
    }

    @PerformOnAnyDb
    private void performQueryOnMasterBiks(final String qryName, final Object... args) {
        performOnMasterBiks(new IContinuation() {
            @Override
            public void doIt() {
                execNamedCommand(qryName, args);
            }
        });
    }
//    private Object performSingleValQueryOnDefaultDatabase(final String qryName, final Object... args) {
//        return performOnMasterBiks(new IContinuationWithReturn<Object>() {
//            @Override
//            public Object doIt() {
//                return execNamedQuerySingleVal(qryName, true, null, args);
//            }
//        });
//    }
//    private <T> List<T> createBeansFromNamedQryOnDefaultDb(final String qryName, final Class<T> clazz, final Object... args) {
//        return performOnDefaultDatabase(new IParametrizedContinuationWithReturn< String, List< T>>() {
//
//            public List<T> doIt(String param) {
//                return createBeansFromNamedQry(qryName, clazz, args);
//            }
//        });
//    }

    @Override
    @PerformOnAnyDb
    public boolean multiXLoginExistsForDatabase(final String login, final String dbName) {
        if (!isMultiXMode()) {
            return false;
        }
//        Integer loginsCount = (Integer) performSingleValQueryOnDefaultDatabase("loginsForDbCount", login, dbName);
        Integer loginsCount
                = performOnMasterBiks(new IContinuationWithReturn<Integer>() {

                    @Override
                    public Integer doIt() {
                        return execNamedQuerySingleValAsInteger("loginsForDbCount", true, null, login, dbName);
                    }
                });

        return loginsCount > 0;
    }

    @Override
    @PerformOnMasterBiksDb
    @RequireSingleBiksDb
    public boolean multiXLoginExistsForCurrentDatabase(final String login) {
        //return multiXLoginExistsForDatabase(login, getCurrentDatabase().get());

        final String dbName = getSingleBiksDbForCurrentRequest();

        return multiXLoginExistsForDatabase(login, dbName);
    }

    @Override
    @PerformOnMasterBiksDb
    public String sendConfirmationEmail(MultiRegistrationBean mrb, String localAccessUrl) {
        //return sendEmailWithToken(mrb.email, localAccessUrl, false, MultixConstants.CONFIRMATION_TOKEN, TokenType.REGISRTATION, getCurrentDatabase().get(), null);
        return sendEmailWithToken(mrb.email, localAccessUrl, false, MultixConstants.CONFIRMATION_TOKEN, TokenType.REGISRTATION, null, null);
    }

    @Override
    @PerformOnSingleBiksDb
    @RequireMasterBiksDb
    public String sendInvitationEmail(String mailString, final String role, final String localAccessUrl) {

        SystemUserBean loggedUserBean = systemUsersSubservice.getLoggedUserBean();

        if ((BIKRightRoles.RightRole.Expert.getCode().equals(role) && !BIKRightRoles.isSysUserEffectiveExpert(loggedUserBean))
                || (BIKRightRoles.RightRole.AppAdmin.getCode().equals(role) && !BIKRightRoles.isSysUserEffectiveAppAdmin(loggedUserBean))) {
            throw new FoxyRuntimeException("insufficient rights");
        }

        String ret;

        if (BaseUtils.isStrEmptyOrWhiteSpace(mailString)) {
            return I18n.niepoprawnyAdresEmail.get();
        }

        final String[] emails = mailString.split(",");

        if (emails.length > MAX_INVITATIONS) {
            return I18n.maxLiczbaAdresow.format(MAX_INVITATIONS);
        }

        for (int i = 0; i < emails.length; i++) {
            emails[i] = emails[i].trim();
        }

        for (String email : emails) {
            if (!BaseUtils.isEmailValid(email)) {
                return I18n.emailNieJestPoprawny.format(email);
            }
        }

        Integer exists;
        for (String email : emails) {
            exists = execNamedQuerySingleVal("checkForInternalUser", false, null, email);
            if (exists == 1) {
                return I18n.posiadaDostep.format(email);
            }
        }

        final String dbName = getSingleBiksDbForCurrentRequest();

//        final SystemUserBean loggedUserBean = getLoggedUserBean();
        ret = performOnMasterBiks(new IContinuationWithReturn<String>() {
            @Override
            public String doIt() {
                for (String email : emails) {
                    Integer exists = execNamedQuerySingleVal("checkForUser", false, null, email, dbName);
                    if (exists == 1) {
                        return I18n.juzZaproszony.format(email);
                    }
                }
//                IdName db = getDbToReset(getLoggedUserName(), dbName);

                for (String email : emails) {
                    String ret = sendEmailWithToken(email, localAccessUrl, true,
                            MultixConstants.INVITE_TOKEN, TokenType.INVITATION, dbName, role);
                    if (ret != null) {
                        return ret;
                    }
                }
                return null;
            }
        });
        return ret;
    }

    @Override
    @PerformOnMasterBiksDb
    public String verifyInvitationTokenAndCreateAccount(final UserRegistrationDataBean registrationData, final Map<Integer, String> map) {
//        Pair<String, String> ret = performOnDefaultDatabase(new IParametrizedContinuationWithReturn<String, Pair<String, String>>() {
//            @Override
//            public Pair<String, String> doIt(String param) {
//                String dbName = execNamedQuerySingleVal("getDbName", false, null, registrationData.token);
//                registrationData.email = execNamedQuerySingleVal("getMailForToken", false, null, registrationData.token);
//                execNamedCommand("addRegisteredUser", registrationData);
//                String role = execNamedQuerySingleVal("getRoleForToken", false, null, registrationData.token);
//                return new Pair<String, String>(dbName, role);
//            }
//        });
        String dbName = execNamedQuerySingleVal("getDbName", false, null, registrationData.token);
        registrationData.email = execNamedQuerySingleVal("getMailForToken", false, null, registrationData.token);
        execNamedCommand("addRegisteredUser", registrationData);
        String role = execNamedQuerySingleVal("getRoleForToken", false, null, registrationData.token);

        addUserWithSocialUserToDb(registrationData.email, registrationData.password, registrationData.phoneNum, dbName, role);

//        performOnDefaultDatabase(true, new IContinuation() {
//            @Override
//            public void doIt() {
        Integer id = execNamedQuerySingleVal("getUserId", false, null, registrationData.email);
        execNamedCommand("multiXInvalidateTokenAfterRegistration", registrationData, MultiXRegistrationStatus.FINISHED);
        execNamedCommand("mltxInsertAnswers", map, id);
        //execNamedCommand("createLogin", registrationData.email, dbName);
//            }
//        });
//            markTokenAsClicked(registrationData.token);
        return null;
    }

    @Override
    @PerformOnMasterBiksDb
    public InvitationResult inspectUser(String token) {
        MultixReturnMessage verification = verifyToken(token, TokenType.INVITATION);

        if (verification.type.equals(MultixReturnMessage.msgType.error)) {
            return new InvitationResult(InvitationConfirmResult.Error);
        } else if (verification.type.equals(MultixReturnMessage.msgType.alreadyUsed)) {
            return new InvitationResult(InvitationConfirmResult.tokenNotValid);
        }
        String dbName = execNamedQuerySingleVal("getDbName", true, null, token);
        String mail = execNamedQuerySingleVal("getMailForToken", false, null, token);
        String role = execNamedQuerySingleVal("getRoleForToken", false, null, token);

        if (execNamedQuerySingleValAsInteger("checkForMultiXUniqueUser", false, null, mail) != 0) {
            String pass = execNamedQuerySingleVal("getPasswordForLogin", true, null, mail);
            try {
                addUserWithSocialUserToDb(mail, pass, null, dbName, role);
            } catch (Exception e) {
                return new InvitationResult(InvitationConfirmResult.Error);
            }
            markTokenAsClicked(token);
            BOXISessionData sd = getSessionData();
            if (sd != null && sd.loginName != null && sd.loginName.equalsIgnoreCase(mail)) {
//                sd.xInstanceDbNames.add(dbName);
                sd.addAccessibleDbName(dbName);
                return new InvitationResult(InvitationConfirmResult.NoRegistration, dbName);
            } else {
                return new InvitationResult(InvitationConfirmResult.NoRegistration);
            }
        }

        return new InvitationResult(InvitationConfirmResult.RegistrationNeeded);
    }

    @PerformOnMasterBiksDb
    // dbName != null (czyli określona baza SingleBiks) potrzebne dla
    //   RESET_PASSWORD ???
    private String sendEmailWithToken(String email, String localAccessUrl, boolean multipleAccountsAllowed, String tokenType, TokenType tokenTypeId, String dbName, String role) {
        if (!BaseUtils.isEmailValid(email)) {
            return I18n.niepoprawnyAdresEmail.get();
        }

        String token = BaseUtils.generateRandomToken(TOKEN_LENGTH);
        Date expiresAt = DateUtils.addInterval(new Date(), VALIDITY_PERIOD);

        String brTag = "<br/>";

        if (tokenTypeId == TokenType.RESET_PASSWORD) {
            if (execNamedQuerySingleValAsInteger("checkForMultiXUniqueUser", false, null, email) == 0) {
                return I18n.nieIstniejeKontoDlaPodanegoAdresuEmail.get();
            }
            execNamedCommand("addRegistrationToken", email, token, expiresAt, MultiXRegistrationStatus.NEW, tokenTypeId, dbName);
        } else {
            if (!multipleAccountsAllowed) {
                if (execNamedQuerySingleValAsInteger("checkForMultiXUniqueEmail", false, null, email) > 0) {
                    return I18n.dlaPodanegoAdresuEmailIstniejeJuzKonto.get();
                }
            }
            execNamedCommand("multiXInvalidateTokens", null, email, MultiXRegistrationStatus.TOKEN_EXPIRED_ERROR, tokenTypeId.actionType);
            Integer tokenId = execNamedQuerySingleVal("addRegistrationToken", false, "id", email, token, expiresAt, MultiXRegistrationStatus.NEW, tokenTypeId, dbName);
            if (tokenTypeId == TokenType.INVITATION) {
                execNamedCommand("addInvitation", tokenId, role);
            }
        }

        String accessUrlBase = BaseUtils.nullToDef(urlForPublicAccess, localAccessUrl);
        String localeUrlPart = "locale=" + I18nMessage.getCurrentLocaleName();

        String urlSep;

        if (accessUrlBase.contains("?")) {
            urlSep = "&";
        } else if (accessUrlBase.endsWith("/")) {
            urlSep = "?";
        } else {
            urlSep = "/?";
        }

        String LINK = accessUrlBase + urlSep + localeUrlPart + "#" + tokenType + "/" + token;
        final String[] recipients = {email};

        try {
            Map<String, MultiXAppPropBean> emailProps = BaseUtils.projectToMap(createBeansFromNamedQry("getMultiXEmailProperties", MultiXAppPropBean.class
            ), new BaseUtils.Projector<MultiXAppPropBean, String>() {
                @Override
                public String project(MultiXAppPropBean val) {
                    return val.name;
                }
            }
            );

            String msg;// = "";

            switch (tokenTypeId) {
                case INVITATION:
                    MultixRegisteredUserBean invitingUser = createBeanFromNamedQry("getMltxUserForEmail", MultixRegisteredUserBean.class, false, getLoggedUserName());
                    String userName = I18n.biksUser.format(invitingUser.email);

                    if (!BaseUtils.isStrEmpty(invitingUser.firstName)
                            && !BaseUtils.isStrEmpty(invitingUser.lastName)) {
                        userName = invitingUser.firstName + " " + invitingUser.lastName + " (" + invitingUser.email + ")";
                    }
                    msg = I18n.zaproszenieDoBiks.format(brTag, brTag + userName, brTag + brTag, brTag + brTag, email, brTag + brTag, LINK, brTag + brTag, brTag + brTag, brTag + brTag, MultixConstants.SUPPORT_EMAIL_ADDRESS, brTag + brTag, I18n.powodzeniaZespolBiks.format(brTag));
                    break;
                case REGISRTATION:
                    msg = I18n.witamyWBiks.format(brTag + brTag, brTag + brTag, email, brTag + brTag, LINK, brTag + brTag, brTag + brTag, brTag + brTag, MultixConstants.SUPPORT_EMAIL_ADDRESS + brTag, brTag, I18n.powodzeniaZespolBiks.format(brTag));
                    break;
                case RESET_PASSWORD:
                    msg = I18n.resetPasswordMail.format(brTag + brTag + LINK + brTag + brTag) + I18n.pozdrawiamyZespolBiks.format(brTag);
                    break;
                default: // REGISTRATION
                    msg = I18n.witamyWBiks.format(brTag + brTag, brTag + brTag, brTag + brTag, LINK, brTag + brTag, brTag + brTag, brTag + brTag, MultixConstants.SUPPORT_EMAIL_ADDRESS + brTag, brTag, I18n.powodzeniaZespolBiks.format(brTag));

                    break;
            }

            SendMail.sendMessage(
                    emailProps.get(MultixConstants.MULTIX_PROP_EMAIL_SMTP_HOST_NAME).val,
                    emailProps.get(MultixConstants.MULTIX_PROP_EMAIL_SMTP_PORT).val,
                    emailProps.get(MultixConstants.MULTIX_PROP_EMAIL_USER).val,
                    emailProps.get(MultixConstants.MULTIX_PROP_EMAIL_PASSWORD).val, recipients,
                    I18n.witamyWSystemieZarzadzaniaWiedza.get(), msg,
                    emailProps.get(MultixConstants.MULTIX_PROP_EMAIL_FROM).val,
                    BaseUtils.tryParseBoolean(emailProps.get(MultixConstants.MULTIX_PROP_EMAIL_SSL).val), null, null);

            execNamedCommand("multiXUpdateRegistrationStatus", MultiXRegistrationStatus.MAIL_SENT, token);

        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("sendConfirmationEmail: fail in sendMessage", ex);
            }
            execNamedCommand("multiXUpdateRegistrationStatus", MultiXRegistrationStatus.MAIL_ERROR, token);
//            throw new FoxyRuntimeException(ex.getMessage());
        }
        return null;
    }

    @Override
    @PerformOnMasterBiksDb
    public String verifyTokenAndCreateDB(final UserRegistrationDataBean userRegistrationDataBean, Map<Integer, String> map) {
        MultixReturnMessage returnMsg = verifyToken(userRegistrationDataBean.token, TokenType.REGISRTATION);
        if (returnMsg.type.isError()) {
            return returnMsg.msg;
        }
        final ServerRegistrationDataBean registrationDetails = createBeanFromNamedQry("getMultiXValidRegistrationDetails", ServerRegistrationDataBean.class, true, userRegistrationDataBean.token, userRegistrationDataBean.templateId);
        String dbName = registrationDetails.restorePrefix + BaseUtils.generateRandomTokenEx(8, CHARS_FOR_DB_NAME_SUFFIX_TOKEN);

        createNewDatabase(dbName, registrationDetails.filePath);

        try { //utworzenie bazy danych nastapilo poza transakcja
            registrationDetails.password = userRegistrationDataBean.password;
            userRegistrationDataBean.email = registrationDetails.email;
            if (map != null) {
                registrationDetails.creatorId = execNamedQuerySingleVal("addRegistrationUser", false, "id", userRegistrationDataBean);
            } else {
                String pass = execNamedQuerySingleVal("getPasswordForLogin", true, null, userRegistrationDataBean.email);
                userRegistrationDataBean.password = pass;
                registrationDetails.creatorId = execNamedQuerySingleVal("getMultiXUniqueId", false, "id", registrationDetails.email);
            }

            markTokenAsClicked(userRegistrationDataBean.token);

            createBeanFromNamedQry("createDatabaseAndGetData", IdName.class, false,
                    dbName, registrationDetails);

            addUserWithSocialUserToDb(userRegistrationDataBean.email, userRegistrationDataBean.password, userRegistrationDataBean.phoneNum, dbName, BIKRightRoles.RightRole.AppAdmin.getCode());
            execNamedCommand("multiXInvalidateTokenAfterRegistration", userRegistrationDataBean, MultiXRegistrationStatus.FINISHED);
            if (map != null) {
                execNamedCommand("mltxInsertAnswers", map, registrationDetails.creatorId);
            }
        } catch (Exception e) {
            try {
                dropDatabase(dbName);
            } catch (Exception ex2) {
                if (logger.isErrorEnabled()) {
                    logger.error("fail in drop database!", ex2);
                }
            }
            throw new RuntimeException(e);
        }

        return null;
    }

    @PerformOnMasterBiksDb
    private void markTokenAsExpiredIfSo(final String token, final TokenType type) {
        execInAutoCommitMode(new IContinuation() {
            public void doIt() {
                execNamedCommand("multiXInvalidateTokens", token, null, MultiXRegistrationStatus.TOKEN_EXPIRED_ERROR, type.actionType);
            }
        });
    }

    @PerformOnMasterBiksDb
    private void markTokenAsClicked(final String token) {
        getAdhocDao().execInAutoCommitMode(new IContinuation() { // link has been clicked, setting status
            public void doIt() {
                execNamedCommand("multiXUpdateRegistrationStatus", MultiXRegistrationStatus.LINK_CLICKED, token);
            }
        });
    }

//    private void createNewDatabase(final String dbName, final ServerRegistrationDataBean registrationDetails) {
//        createNewDatabase(dbName, registrationDetails.filePath);
//    }
    @PerformOnMasterBiksDb
    private void createNewDatabase(final String dbName, final String path) {
        getAdhocDao().execInAutoCommitMode(new IContinuation() {
            public void doIt() {
                execNamedCommand("createNewDBInstance", dbName, path, MultixConstants.APP_PROP_NAME_DATABASE_DEST_PATH);
            }
        });
    }

    // trzeba to robić na MasterBiks, bo inaczej możemy nie usunąć bazy, na
    // której właśnie próbujemy wykonać instrukcję drop (może nie da się usunąć
    // current db)
    @PerformOnMasterBiksDb
    private void dropDatabase(final String dbName) {
        getAdhocDao().execInAutoCommitMode(new IContinuation() {
            @Override
            public void doIt() {
                execNamedCommand("dropDatabase", dbName);
            }
        });
    }

    @PerformOnSingleBiksDb
    private Integer addUserToDatabase(final String email, final String password, final String role) {
        return systemUsersSubservice.addBikSystemUserInternal(false, email, password,
                false, null, prepareSuitableRole(role), null, new ArrayList<Integer>(), false, "sys");
    }

//    @PerformOnSingleBiksDb(useExpliciteDb = true)
//    private Integer addUserToDatabase(UserRegistrationDataBean registrationDetails, String role, String dbName) {
//        return addUserToDatabase(registrationDetails.email, registrationDetails.password, dbName, role);
//    }
    @PerformOnSingleBiksDb(useExpliciteDb = true)
    private void updateUserPasswordInDatabase(final String email, final String password, Collection<String> dbNames) {
        for (String dbName : dbNames) {
//            getSessionData().loggedUser = new SystemUserBean();
//            getSessionData().xInstanceDbName = dbName;
//            setCurrentDatabaseFromSession();
//        Set<String> rights = new HashSet<String>();
//        rights.add("Administrator");//ZMIENIC
            performOnSingleBiksExpliciteDb(dbName, new IContinuation() {
                @Override
                public void doIt() {
                    execNamedCommand("updateBikSystemUserEx", email, password);
//                    updateBikSystemUserInternal(false, email, password,
//                            BIKRightRoles.getAppAdminRightRoles());
                }
            });

//            setDefaultDatabaseAsCurrent();
//            getSessionData().loggedUser = null;
//            getSessionData().xInstanceDbName = null;
        }
    }

    @Override
    @PerformOnMasterBiksDb
    public List<MultiXDatabaseTemplateBean> getDatabaseTemplates() {
        return createBeansFromNamedQry("getMultiXDatabaseTemplatesForLocale", MultiXDatabaseTemplateBean.class, I18nMessage.getCurrentLocaleName());
    }

    @PerformOnMasterBiksDb
    protected String getMltxAppPropVal(String name) {
        return execNamedQuerySingleVal("getMltxAppPropVal", true, null, name);
    }

    @Override
    @PerformOnMasterBiksDb
    public String getServerSupportedLocaleNamesStr() {
        return getMltxAppPropVal("languages" /* I18N: no */);
    }

    @Override
    @PerformOnSingleBiksDb
    @RequireMasterBiksDb
    public String resetDB(final boolean preserveInvited) {

//        if (!isUserCreatorOfCurrentSingleBiksDb(getLoggedUserBean().loginName)) {
//            return I18n.nie.get();
//        }
        boolean isLoggedUserCreator = performOnMasterBiks(new IContinuationWithReturn<Boolean>() {

            @Override
            public Boolean doIt() {
                return isLoggedUserCreatorOfCurrentSingleBiksDb();
            }
        });

        if (!isLoggedUserCreator) {
            return I18n.nie.get();
        }

        final List<InternalUserBean> allUsers = createBeansFromNamedQry("getAllBikSystemUsersWithPass", InternalUserBean.class);

        extractUserRoles(allUsers);

        final String dbName = getSingleBiksDbForCurrentRequest();

        return performOnMasterBiks(
                new IContinuationWithReturn<String>() {
                    @Override
                    public String doIt() {
//                        IdName db = getDbToReset(getLoggedUserBean(), dbName);
//                        final String currDb = db.name;

                        String restoreFrom = execNamedQuerySingleVal("mltxGetTemplateToRestorePath", true, null, dbName);
                        final String creatorLogin = execNamedQuerySingleVal("mltxGetCreatorLogin", true, null, dbName);
                        dropDatabase(dbName);
                        createNewDatabase(dbName, restoreFrom);
                        try {
                            performOnSingleBiks(new IContinuation() {
                                @Override
                                public void doIt() {
                                    restoreUsers(allUsers, creatorLogin, preserveInvited);
                                }
                            });
                            if (!preserveInvited) {
                                cleanseInvitations(dbName, creatorLogin);
                            }
                        } catch (Exception e) {
                            try {
                                dropDatabase(dbName);
                            } catch (Exception ex2) {
                                if (logger.isErrorEnabled()) {
                                    logger.error("fail in drop database!", ex2);
                                }
                            }
                            throw new RuntimeException(e);
                        }

                        return !getSessionData().loginName.equals(creatorLogin) ? "success" : null;
                    }
                }
        );

    }

    @Override
    @PerformOnMasterBiksDb
    @RequireLoggedUser
    @RequireSingleBiksDb
    public DatabaseListBoxData getAvailableDatabases() {
        List<DatabaseListboxElem> list = getDatabasesForLogin(getLoggedUserName());
//        List<DatabaseListboxElem> list = createBeansFromNamedQryOnDefaultDb("getDatabasesForLogin", DatabaseListboxElem.class,
//                getLoggedUserBean().loginName);
        DatabaseListBoxData data = new DatabaseListBoxData();
        data.elems = list;
        //String dbName = getCurrentDatabase().get();
        String dbName = getSingleBiksDbForCurrentRequest();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).id.equals(dbName)) {
                data.selectedIndex = i;
                return data;
            }
        }
        data.selectedIndex = -1;
        return data;
    }

    @Override
    @PerformOnMasterBiksDb
    public MultixReturnMessage verifyToken(String token, TokenType type) {
        markTokenAsExpiredIfSo(token, type);

        final String actionType = execNamedQuerySingleVal("getMultiXTokenType", true, "action_type", token);

        if (actionType == null) {
            String r = execNamedQuerySingleVal("getInvalidMultiXTokenType", true, "action_type", token);
            if (r != null) {
                return new MultixReturnMessage(I18n.linkWasAlreadyUsed.get(), MultixReturnMessage.msgType.alreadyUsed);
            } else {
                return new MultixReturnMessage(I18n.incorrectLink.get(), MultixReturnMessage.msgType.error);
            }
        } else if (!type.actionType.equals(actionType)) {
            return new MultixReturnMessage(I18n.linkRejestracyjnyWygasl.get(), MultixReturnMessage.msgType.error);
        }

        if (execNamedQuerySingleValAsInteger("checkForMultiXUniqueUserByToken", false, null, token) != 0) {
            return new MultixReturnMessage(null, MultixReturnMessage.msgType.userGotAccount);
        }
        return new MultixReturnMessage(null, MultixReturnMessage.msgType.ok);
    }

    @Override
    @PerformOnMasterBiksDb
    public List<MultixStatisticsBean> getMultiXStatistics(MultixStatisticsAuthenticationBean msab) {

        if (!authenticateUserForMultixStatistics(msab)) {
            return null;
        }

        return createBeansFromNamedQry("getMultiXStatistics", MultixStatisticsBean.class,
                MultixStatsEventType.USER_LOGIN.name, MultixStatsEventType.LAST_ACTIVITY.name, MultixStatsEventType.TOTAL_INVITED.name);
    }

    @Override
    @PerformOnMasterBiksDb
    public String sendResetPassword(String email, String localAccessUrl) {
        return sendEmailWithToken(email, localAccessUrl, true, MultixConstants.RESET_TOKEN, TokenType.RESET_PASSWORD, null, null);
    }

    @Override
    @PerformOnMasterBiksDb
    public String resetPassword(String token, String password) {
        MultixReturnMessage multixReturnMessage = verifyToken(token, TokenType.RESET_PASSWORD);
        if (multixReturnMessage.type != MultixReturnMessage.msgType.userGotAccount) {
            return multixReturnMessage.msg;
        }

        String email = execNamedQuerySingleVal("getMultiXLoginForToken", true, "email", token);
        if (email == null) {
            return I18n.errorLink.get();
        }
//        List<String> dbNames = execNamedQuerySingleColAsList("getDatabasesForLogin", "id", email);
        Set<String> dbNames = loadDbNames(email);

        if (dbNames.isEmpty()) {
            return I18n.errorLink.get();
        }
        updateUserPasswordInDatabase(email, password, dbNames);
        execNamedCommand("mltxResetPassword", email, password);
        markTokenAsClicked(token);
        return null;

    }

//    private IdName getDbToReset(String loginName, String dbName) {
//        return createBeanFromNamedQry("mltxGetDatabaseForUser", IdName.class, true, loginName, dbName);
//    }
    @PerformOnSingleBiksDb
    private void restoreUsers(List<InternalUserBean> allUsers, String creatorLogin, boolean preserveInvited) {
        for (InternalUserBean iUsr : allUsers) {
            if (preserveInvited || (!preserveInvited && iUsr.loginName.equals(creatorLogin))) {
                if (execNamedQuerySingleValAsInteger("getBikSystemUserCountByLogin", false, null, iUsr.loginName) == 0) {
                    UserBean bikUser = createBikSocialUser(iUsr.loginName, iUsr.phoneNum);
                    execNamedQuerySingleVal("addBikSystemUserEx", false, "id" /* I18N: no */,
                            iUsr.loginName, iUsr.password, iUsr.isDisabled, bikUser.id, iUsr.userRightRoleCodes, iUsr.userAuthorTreeIDs, null, null, null);
//                    addUserWithSocialUserToDb(iUsr.loginName, iUsr.password, currDb, BIKRightRoles.RightRole.RegularUser.getCode());
//                    userCreationSubservice.updateBikSystemUser(iUsr.id, iUsr.loginName, null, iUsr.isDisabled, iUsr.userId, iUsr.userName, iUsr.userRightRoleCodes, iUsr.userAuthorTreeIDs);
                }
            }
        }
    }

    @PerformOnMasterBiksDb
    private void cleanseInvitations(String dbName, String login) {
        execNamedCommand("cleanseInvitations", dbName, login);
    }

    @Override
    @PerformOnMasterBiksDb
    public List<IdName> getQuestions() {
        return createBeansFromNamedQry("mltxGetQuestions", IdName.class, I18nMessage.getCurrentLocaleName());
    }

    @Override
    @PerformOnAnyDb
    public Boolean authenticateUserForMultixStatistics(MultixStatisticsAuthenticationBean msab) {
        return msab != null && BaseUtils.safeEquals(msab.login, "mstat") && BaseUtils.safeEquals(msab.pass, "mstat");
    }

    @PerformOnSingleBiksDb
    private void extractUserRoles(List<InternalUserBean> allUsers) {
        Set<String> roles;
        for (InternalUserBean iUsr : allUsers) {
            roles = execNamedQuerySingleColAsSet("getRightRolesForSystemUser", "code", iUsr.id);
            iUsr.userRightRoleCodes = roles;
        }
    }

    @PerformOnAnyDb
    private Set<String> prepareSuitableRole(String role) {
        if (role == null || role.equals(BIKRightRoles.RightRole.RegularUser.getCode())) {
            return null;
        } else {
            return Collections.singleton(role);
        }
    }

    @PerformOnAnyDb
    private Map<Integer, Set<Pair<Integer, Integer>>> prepareSuitableCrrEntries(Integer roleId, String role) {
        if (role == null || role.equals(BIKRightRoles.RightRole.RegularUser.getCode())) {
            return null;
        } else {
            Map<Integer, Set<Pair<Integer, Integer>>> res = new HashMap<Integer, Set<Pair<Integer, Integer>>>();
            if (roleId != null) {
                Set<Pair<Integer, Integer>> ents = new HashSet<Pair<Integer, Integer>>();
                ents.add(new Pair<Integer, Integer>(null, null));
                res.put(roleId, ents);
            }
            return res;
        }

    }

    @Override
    @PerformOnMasterBiksDb
    public void changeAccessToDatabase(final boolean isDisabled, final String loginName, final String dbName) {
        execNamedCommand("changeAccessToDatabase", isDisabled, loginName, dbName);
    }

    @PerformOnMasterBiksDb
    private boolean isLoggedUserCreatorOfCurrentSingleBiksDb() {
        String login = getLoggedUserName();

        if (login == null) {
            return false;
        }
//        return performOnDefaultDatabase(true, new IParametrizedContinuationWithReturn<String, Boolean>() {
//            public Boolean doIt(String currentDb) {
        Integer count = execNamedQuerySingleVal("isUserCreatorOfDb", false, null, login, getSingleBiksDbForCurrentRequest());
        return count > 0;
//            }
//        });
    }

//    @PerformOnSingleBiksDb(useExpliciteDb = true)
//    private void addUserWithSocialUserToDb(String login, String pass, final String dbName, final String role) {
//        UserRegistrationDataBean reg = new UserRegistrationDataBean();
//        reg.email = login;
//        reg.password = pass;
//        addUserWithSocialUserToDb(reg, dbName, role);
//    }
    @PerformOnSingleBiksDb(useExpliciteDb = true)
    private void addUserWithSocialUserToDb(final String login, final String pass, final String phoneNum, final String dbName, final String role) {
        performOnSingleBiksExpliciteDb(dbName, new IContinuation() {
            @Override
            public void doIt() {
                Integer userExists = execNamedQuerySingleVal("checkIfSystemUserExists", false, null, login);
                if (userExists != 1) {
                    UserBean socialUser = createBikSocialUser(login, phoneNum);
                    Integer systemUserId = addUserToDatabase(login, pass, role);
                    //id, login, password, isDisebled, userId, systemUserName, userRightRoleCodes, treeIds, isSendMails, customRightRoleUserEntries
                    Integer roleId = getRoleIdByCode(role);
                    execNamedCommand("updateBikSystemUser", systemUserId, login, null, false,
                            socialUser.id, socialUser.name, prepareSuitableRole(role), null, false, prepareSuitableCrrEntries(roleId, role), null);
                } else {
                    throw new FoxyRuntimeException("Token not valid!");
                }
            }

        });
    }

    @PerformOnSingleBiksDb
    private Integer getRoleIdByCode(String role) {
        return execNamedQuerySingleValAsInteger("getRoleIdByCode", true, null, role);
    }

    @PerformOnSingleBiksDb
    private UserBean createBikSocialUser(String email, String phoneNum) {
        TreeNodeBean repo = createBeanFromNamedQry("getAllUsersTreeNode", TreeNodeBean.class, false, BIKConstants.NODE_KIND_ALL_USERS_FOLDER,
                BIKConstants.TREE_CODE_USER_ROLES);
        UserExtBean user = new UserExtBean();
//        user.name = registrationData.email;
//        user.email = registrationData.email;
//        user.phoneNum = registrationData.phoneNum;
        user.name = email;
        user.email = email;
        user.phoneNum = phoneNum;

        Pair<Integer, List<String>> ret = systemUsersSubservice.addBikTreeNodeUser(repo, user);
        Integer bikUserNodeId = ret.v1;

        return createBeanFromNamedQry("getBikUserByNodeId", UserBean.class, false, bikUserNodeId);
    }

//    @PerformOnSingleBiksDb
//    private UserBean createBikSocialUser(InternalUserBean iUsr) {
//        UserRegistrationDataBean reg = new UserRegistrationDataBean();
//        reg.email = iUsr.loginName;
//        reg.phoneNum = iUsr.phoneNum;
//        return createBikSocialUser(reg);
//    }
    @PerformOnMasterBiksDb
    protected List<DatabaseListboxElem> getDatabasesForLogin(final String login) {
        return createBeansFromNamedQry("getDatabasesForLogin", DatabaseListboxElem.class, login);
    }

    @Override
    @PerformOnMasterBiksDb
    public Set<String> loadDbNames(final String login) {
        List<DatabaseListboxElem> dbs = getDatabasesForLogin(login);
//        List<DatabaseListboxElem> dbs = performOnDefaultDatabase(new IParametrizedContinuationWithReturn<String, List<DatabaseListboxElem>>() {
//            public List<DatabaseListboxElem> doIt(String param) {
//                return createBeansFromNamedQry("getDatabasesForLogin", DatabaseListboxElem.class, login);
//            }
//        });
        return BaseUtils.projectToSet(dbs, new BaseUtils.Projector<DatabaseListboxElem, String>() {
            @Override
            public String project(DatabaseListboxElem val) {
                return val.id;
            }
        }, true);
    }

    @Override
    @PerformOnMasterBiksDb
    public String getDefaultDbNameForMultiXLogin(final String login) {
        if (!isMultiXMode()) {
            return null;
        }

        return execNamedQuerySingleVal("mltxGetDefaultDb", true, "database_name", login);
//        setCurrentDbForAdhoc(defaultDatabaseName);
//        List<DatabaseListboxElem> list = createBeansFromNamedQry("getDatabasesForLogin", DatabaseListboxElem.class, login);
//
//
//        String databaseForLogin = execNamedQuerySingleVal("mltxGetCreatedByUserDb", true, "database_name", login);
//        if (databaseForLogin == null) {
//            databaseForLogin = execNamedQuerySingleVal("mltxGetFirstInvitedDb", true, "database_name", login);
//        }
//
//        return databaseForLogin;
    }

//    @PerformOnMasterBiksDb
//    protected void setDefaultDBAndAccessibleForLoggedUsr(SystemUserBean usrBean, BOXISessionData sessionData) {
//        if (isMultiXMode()) {
////            usrBean.defaultDbName = getDefaultDbNameForMultiXLogin(sessionData.loginName);
//            if (!sessionData.isLoggedIn()) {
//                sessionData.setAccessibleDbNames(loadDbNames(sessionData.loginName));
//            }
//        }
//    }
}
