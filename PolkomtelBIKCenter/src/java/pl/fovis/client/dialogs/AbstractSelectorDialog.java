/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BOXIServiceAsync;
import pl.fovis.client.bikpages.AbstractBIKTreeWidget;
import pl.fovis.client.bikpages.IPageDataFetchBroker;
import pl.fovis.client.bikpages.PageDataFetchBroker;
import pl.fovis.client.bikpages.TreeDataFetchAsyncCallback;
import pl.fovis.client.bikwidgets.BikItemButtonBig;
import pl.fovis.client.treeandlist.IBean4TreeExtractor;
import pl.fovis.client.treeandlist.TreeNodeBeanAsMapStorage;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.JoinedObjBasicGenericInfoBean;
import pl.fovis.common.JoinedObjMode;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.popupmenu.PopupMenuDisplayer;
import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeOrListGridRightClickHandler;
import pl.fovis.foxygwtcommons.treeandlist.ITreeGridCellWidgetBuilder;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;
import pl.fovis.foxygwtcommons.wezyrtreegrid.InlineFlowPanel;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 * @param <BEAN>
 * @param <IDT>
 */
public abstract class AbstractSelectorDialog<BEAN, IDT> extends BaseActionOrCancelDialog /*implements IBean4TreeExtractor<BEAN, IDT>*/ {

    private static final String ADDING_ONE_JOINED_OBJ_FOR_MSG = I18n.dodajeszPowiazanieDla.get() /* I18N:  */ + ": ";
    private static final String EDITING_MANY_JOINED_OBJS_FOR_MSG = I18n.edytujeszPowiazaniaDla.get() /* I18N:  */ + ": ";
    private static final String EDITING_MANY_JOINED_OBJS_HINT_NAME = "EditnigJoinedObj";
    protected boolean useLazyLoading = true;//false; //
    protected IParametrizedContinuation<BEAN> saveCont;
    protected AbstractBIKTreeWidget<BEAN, IDT> bikTree;
    protected BEAN selectedNode = null;
    //ww:mltx 2013.09.26 - kasuję to kuriozum, WTF, po co to było?
    //    protected TreeNodeBean lastSelectedNode = null;
    protected TextBox filterBox;
    protected BikItemButtonBig filterButton;
    private final CheckBox cbFilterInSubTree = new CheckBox();
    protected Integer currentFilterInSubTreeNodeId = null;
//    protected String currentFilterInSubTreeNamePath;
    protected String currentFilterVal = "";
    protected CheckBox inheritToDescendantsCb;
    protected IDT nodeId;
    //protected String optNodeNamePathStr;
    protected Label headLbl;// = new Label(ADDING_ONE_JOINED_OBJ_FOR_MSG + "...");
//    protected Set<Integer> alreadyJoinedObjIds;
//    protected Set<Integer> fixedJoinedObjIds;
    protected IJoinedObjSelectionManager<IDT> alreadyJoinedManager;
    protected Map<IDT, JoinedObjMode> directOverride = new HashMap<IDT, JoinedObjMode>();
    protected List<IDT> parentsDirectOverrideOrIdWhenParentNull = new ArrayList<IDT>();
    protected List<IDT> treeIdWhenParentIdIsNull = new ArrayList<IDT>();/* do rozwijania sztucznych gałęzi*/

    // Boolean == true -> all descendants, false -> children only

    protected Map<IDT, Pair<JoinedObjMode, Boolean>> subNodesOverride = new HashMap<IDT, Pair<JoinedObjMode, Boolean>>();
    protected Map<IDT, InlineHTML> htmlForJOMImg = new HashMap<IDT, InlineHTML>();
    protected Map<IDT, InlineLabel> lblForJOM = new HashMap<IDT, InlineLabel>();
    protected Set<IDT> ancestorIdsOfJoinedObjs = new HashSet<IDT>();
    protected boolean isInFilterBox;
    protected Set<IDT> alreadyJoinedObjIds;
    protected IPageDataFetchBroker dataFetchBroker = new PageDataFetchBroker();
    protected IBean4TreeExtractor<BEAN, IDT> beanExtractor = null;

    protected abstract boolean isInheritToDescendantsCheckBoxVisible();

    protected abstract void callServiceToGetData(AsyncCallback<List<BEAN>> callback);

    protected abstract Collection<Integer> getTreeIdsForFiltering();

    //protected abstract String getMessageForUnselectableNode(TreeNodeBean tnb);
    //ww: powinna być klasa bazowa dialogów BIKSowych i tam to powinno siedzieć
    // zauważmy, że BaseActionOrCancelDialog jest ogólną klasą bazową dialogów
    // nie ograniczoną tylko do BIKSa, więc siedzi w innym pakiecie
    // a wszystkie dialogi biksowe mają lub moga mieć coś wspólnego - bo są
    // w BIKSie używane - np. właśnie ten service...
    protected BOXIServiceAsync getService() {
        return BIKClientSingletons.getService();
    }

    protected void fetchData() {
        fetchData(null);
    }

    protected void processAndShowNodes(final List<BEAN> result, IDT optNodeIdToSelect) {
        if (isGrouppingByMenuEnabled()) {
            groupResultsByMenu(result);
        } else {
            optFixResults(result);
        }
        bikTree.initWithFullData(result, useLazyLoading, getTreeOptExtraMode(), getOptFilteringNodeActionCode());
        bikTree.selectEntityById(optNodeIdToSelect);
        if (BIKClientSingletons.expandSelectedNodesInHyperlinkAttribute()) {
            bikTree.expandNodesByIds(/*directOverride.keySet()*/
                    parentsDirectOverrideOrIdWhenParentNull
            );
        }

    }

    protected void optFixResults(List<BEAN> result) {
        // no op
    }

    protected BikNodeTreeOptMode getTreeOptExtraMode() {
        return null;
    }

    protected String getOptFilteringNodeActionCode() {
        return "AddJoinedObjs";
    }

    protected boolean calcHasNoChildren() {
        return true;
    }

    protected void fetchData(final IDT optNodeIdToSelect) {

        final AsyncCallback<List<BEAN>> callback = new TreeDataFetchAsyncCallback<List<BEAN>>("Communication failed" /* I18N: no */,
                dataFetchBroker) {
                    protected void doOnSuccess(List<BEAN> result) {
                        if (!BaseUtils.isCollectionEmpty(result)) {
                            processAndShowNodes(result, optNodeIdToSelect);
                        } else {
                            BIKClientSingletons.showWarning(I18n.cantEditAttr.get());
                        }
                    }
                };
        //bikTree.isGetDataInProgress = true;
        callServiceToGetData(callback);
    }

    @Override
    protected String getDialogCaption() {
        return I18n.dodajPowiazanie.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return isInMultiSelectMode() ? I18n.zapisz.get() /* I18N:  */ : I18n.wybierz.get() /* I18N:  */;
    }

    protected boolean mustHideLinkedNodes() {
        return true;
    }

    protected String getHeadLblPrefixMsg() {
        return isInMultiSelectMode() ? EDITING_MANY_JOINED_OBJS_FOR_MSG : ADDING_ONE_JOINED_OBJ_FOR_MSG;
    }

    protected Widget setOptionalWigetAfterHeadLabel() {
        return null;
    }

    protected String getHeadLabelInitialText() {
        return getHeadLblPrefixMsg() + "...";
    }

    @Override
    protected void buildMainWidgets() {
        //if (optNodeNamePathStr != null) {
        String headLblCaption = getHeadLabelInitialText();
        if (headLblCaption != null) {
            headLbl = new Label(headLblCaption);
        } else {
            headLbl = new Label();
        }
        Widget headLblWidget;

        if (!isNodesIdFromExternalData() && isInMultiSelectMode() && BIKClientSingletons.helpHasHint(EDITING_MANY_JOINED_OBJS_HINT_NAME)) {
            HorizontalPanel headHp = new HorizontalPanel();
            HorizontalPanel headHpLblOpt = new HorizontalPanel();
            headHp.setWidth("100%");
            headHpLblOpt.add(headLbl);
            headHp.add(headHpLblOpt);
            Widget optionalWidget = setOptionalWigetAfterHeadLabel();
            if (optionalWidget != null) {
                headHpLblOpt.add(optionalWidget);
                GWTUtils.setStyleAttribute(headLbl, "padding" /* I18N: no */, "0px 10px 0px 0");
            }

            //headHp.setCellWidth(hintBtn, ZINDEX_OVER_GLASS)
            Widget hintBtn = BIKClientSingletons.createHelpHintButton(EDITING_MANY_JOINED_OBJS_HINT_NAME, I18n.legendaEdycjiPowiazan.get());
            headHp.add(hintBtn);
            headHp.setCellHorizontalAlignment(hintBtn, HasHorizontalAlignment.ALIGN_RIGHT);
            //headHp.setCellWidth(hintBtn, ZINDEX_OVER_GLASS)
            headLblWidget = headHp;
        } else {
            headLblWidget = headLbl;
        }

        if (isHeaderVisible()) {
            main.add(headLblWidget); //main.add(headLbl);
        }
        headLblWidget.setStyleName("AttrHeadLbl treeSelectorHeadLbl");
        //}

        Widget optRightSideWidget = buildOptionalRightSide();

        Panel mainBodyContainer;

        if (optRightSideWidget != null) {
            VerticalPanel leftVp = new VerticalPanel();
            leftVp.setStyleName("EntityDetailsPane");

            //leftVp.setWidth("600px");//main.setWidth("600px");
            //leftVp.setHeight("300px");//main.setHeight("300px");
            HorizontalPanel hp = new HorizontalPanel();
            hp.add(leftVp);
            hp.add(optRightSideWidget);
            main.add(hp);
            mainBodyContainer = leftVp;
        } else {
            //main.setStyleName("EntityDetailsPane");
            mainBodyContainer = main;
        }

        mainBodyContainer.setWidth("600px");
        setHeadLabel();

        buildTreeMainWidgets(mainBodyContainer);

        fetchData();
    }

    protected boolean isHeaderVisible() {
        return true;
    }

    protected void setHeadLabel() {
        if (headLbl == null) {
            return;
        }
        optGetNodePathAndCenter(new IContinuation() {
            public void doIt() {
                if (dialog != null && dialog.isVisible()) {
                    dialog.center();
                }
            }
        });
    }

    protected void optGetNodePathAndCenter(IContinuation con) {
        con.doIt();
    }

    protected void fetchCurrentFilterInSubTreeNamePathAndUpdateFilterButtonTitle() {
//        currentFilterInSubTreeNamePath = currentFilterInSubTreeNodeId == null ? null : BIKCenterUtils.buildAncestorPathByStorage(getBikTree().getCurrentNodeBean().branchIds, getBikTree().getTreeStorage());
    }

    protected Integer getCurrentNodeIdAsInteger() {
        return null;
        //return bikTree.getCurrentNodeId();
    }

    protected void doFilter() {
        //jezeli nic nie zostalo wpisane to wroc do pierwotnego wygladu

        currentFilterVal = filterBox.getText();

        if (currentFilterVal.equals("")) {
            currentFilterInSubTreeNodeId = null;
//            currentFilterInSubTreeNamePath = null;
            fetchData();
        } else {
            currentFilterInSubTreeNodeId = cbFilterInSubTree.isEnabled() && cbFilterInSubTree.getValue() ? getCurrentNodeIdAsInteger() : null;

            if (currentFilterInSubTreeNodeId != null) {
                fetchCurrentFilterInSubTreeNamePathAndUpdateFilterButtonTitle();
            }

            bikTree.filterTreeEx(currentFilterVal, currentFilterInSubTreeNodeId, useLazyLoading,
                    getTreeIdsForFiltering(), getTreeOptExtraMode(), getOptFilteringNodeActionCode());
        }

        configureButtons();
    }

    protected void doAfterDoubleClickOnTree(Map<String, Object> param) {
        setSelectedNodeFromBikTree(param);
        actionClicked();
    }

    protected Widget buildOptionalRightSide() {
        return null;
    }

    protected void resetJoinedObjModeForSubTree(IDT nodeId) {
        Map<String, Object> row = bikTree.getTreeStorage().getRowById(nodeId);

        Collection<Map<String, Object>> childRows = bikTree.getTreeStorage().getChildRows(row);

        if (childRows == null) {
            return;
        }

        for (Map<String, Object> childRow : childRows) {
            @SuppressWarnings("unchecked")
            IDT childId = (IDT) childRow.get(TreeNodeBeanAsMapStorage.ID_FIELD_NAME);
            subNodesOverride.remove(childId);
            directOverride.remove(childId);

            BEAN childNode = bikTree.getTreeStorage().getBeanById(childId);

            InlineHTML imgHtml = htmlForJOMImg.get(childId);
            if (imgHtml != null) {
                JoinedObjMode selectedVal = getNodeSelectedValue(childNode);
                final boolean isFixed = isNodeSelectionFixed(childNode);
                imgHtml.setHTML(generateHtmlForJoinedObjImg(selectedVal, isFixed));
            }

//            InlineLabel lbl = lblForJOM.get(childId);
//            setJOMLabelStyles(childNode, lbl);
            updateLabelStylesOfNode(childNode);

            resetJoinedObjModeForSubTree(childId);
        }
    }

    protected void updateLabelStylesOfNode(BEAN node) {
        InlineLabel lbl = lblForJOM.get(getExtractor().extractId(node));
        setJOMLabelStyles(node, lbl);
    }

    protected void updateLabelStylesOfNodeById(IDT id) {
        updateLabelStylesOfNode(bikTree.getTreeStorage().getBeanById(id));
    }

    protected void setJoinedObjModeForDescendants(BEAN node, JoinedObjMode mode, boolean allDescendants) {
        if (!isNodeSelectable(node)) {
            Map<String, Object> row = bikTree.getTreeStorage().getRowById(getExtractor().extractId(node));
            Collection<Map<String, Object>> childRows = bikTree.getTreeStorage().getChildRows(row);

            for (Map<String, Object> childRow : childRows) {
                @SuppressWarnings("unchecked")
                IDT childId = (IDT) childRow.get(TreeNodeBeanAsMapStorage.ID_FIELD_NAME);
                if (allDescendants) {
                    subNodesOverride.put(childId, new Pair<JoinedObjMode, Boolean>(mode, allDescendants));
                }
                directOverride.put(childId, mode);
                resetJoinedObjModeForSubTree(childId);
            }
        } else {
            subNodesOverride.put(getExtractor().extractId(node), new Pair<JoinedObjMode, Boolean>(mode, allDescendants));
            resetJoinedObjModeForSubTree(getExtractor().extractId(node));
        }
    }

    protected void showPopupMenu(Map<String, Object> dataRow, int clientX, int clientY) {
        ITreeRowAndBeanStorage<IDT, Map<String, Object>, BEAN> storage = bikTree.getTreeStorage();
        final BEAN node = storage.getBeanById(storage.getTreeBroker().getNodeId(dataRow));
        //(Integer) dataRow.get(TreeNodeBeanAsMapStorage.ID_FIELD_NAME));

        JoinedObjMode directJOM = directOverride.get(getExtractor().extractId(node));

        if (getExtractor().extractHasNoChildren(node) && directJOM == null) {
            return;
        }

        PopupMenuDisplayer popupMenu = new PopupMenuDisplayer();

        if (!getExtractor().extractHasNoChildren(node)) {
            popupMenu.addMenuItem(I18n.dodajDziedzicPowiazanDlaObiektow.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            setJoinedObjModeForDescendants(node, JoinedObjMode.WithInheritance, false);
                        }
                    });
            popupMenu.addMenuItem(I18n.dodajNiedziedPowiazanDlaObiektow.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            setJoinedObjModeForDescendants(node, JoinedObjMode.Single, false);
                        }
                    });
            popupMenu.addMenuSeparator();
            popupMenu.addMenuItem(I18n.usunPowiazanDlaObiektowBezposreP.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            setJoinedObjModeForDescendants(node, JoinedObjMode.NotJoined, false);
                        }
                    });
            popupMenu.addMenuItem(I18n.usunPowiazanDlaWszystkiObiektowP.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            setJoinedObjModeForDescendants(node, JoinedObjMode.NotJoined, true);
                        }
                    });
            popupMenu.addMenuSeparator();
            popupMenu.addMenuItem(I18n.przywrocOryginalStanPowiazanWPod.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            setJoinedObjModeForDescendants(node, null, true);
                        }
                    });
        }
        JoinedObjMode currVal = getNodeSelectedValue(node);
        JoinedObjMode origVal = getNodeSelectedValueEx(node, true);
        if (currVal != origVal) {
            boolean isFixed = isNodeSelectionFixed(node);
            if (!isFixed) {
                popupMenu.addMenuItem(I18n.przywrocOryginalStanPowiazanWTym.get() /* I18N:  */, new IContinuation() {
                            public void doIt() {
                                directOverride.remove(getExtractor().extractId(node));

                                JoinedObjMode currVal = getNodeSelectedValue(node);
                                JoinedObjMode origVal = getNodeSelectedValueEx(node, true);

                                if (currVal != origVal) {
                                    directOverride.put(getExtractor().extractId(node), origVal);
                                }
                                InlineHTML imgHtml = htmlForJOMImg.get(getExtractor().extractId(node));
                                imgHtml.setHTML(generateHtmlForJoinedObjImg(origVal, false));
                                updateLabelStylesOfNode(node);
                            }
                        });
            }
        }

        popupMenu.showAtPosition(clientX, clientY);
    }

    public boolean isInheritToDescendantsChecked() {
        return inheritToDescendantsCb == null ? false : inheritToDescendantsCb.getValue();
    }

    protected void setSelectedNodeFromBikTree(Map<String, Object> param) {
        selectedNode = //(TreeNodeBean) param.get(BaseBIKTreeWidget.TREE_NODE_BEAN_KEY_IN_ROW);
                bikTree.getNodeBeanByDataNode(param);
        configureButtons();
    }

    protected void configureButtons() {
        actionBtn.setEnabled(isActionButtonEnabled());
        updateFilterButtonEnabled();
    }

    protected boolean isActionButtonEnabled() {
        if (isInMultiSelectMode()) {
            if (false) {
                for (JoinedObjMode value : directOverride.values()) {
                    if (!value.equals(JoinedObjMode.NotJoined)) {
                        return true;
                    }
                }
                return false;
            }

            for (Entry<IDT, JoinedObjMode> e : directOverride.entrySet()) {
                BEAN node = bikTree.getTreeStorage().getBeanById(e.getKey());

                if (node != null && (hasChangeInNodeById(node) || hasChangeInDescendants(node))) {
                    return true;
                }
            }

            for (Entry<IDT, Pair<JoinedObjMode, Boolean>> e : subNodesOverride.entrySet()) {
                if (e.getValue().v1 != null) {
                    return true;
                }
            }

            return false;

        } else {
            //ww:mltx dodałem spr. selectedNode != null
            return selectedNode != null && isNodeSelectable(selectedNode) && (isNodesIdFromExternalData()
                    || !BaseUtils.safeEquals(getExtractor().extractId(selectedNode), nodeId));
        }
    }

    protected void bikTreeNodeChanged(Map<String, Object> param) {
        setSelectedNodeFromBikTree(param);
    }

    @Override
    protected boolean doActionBeforeClose() {
        if (isInMultiSelectMode()) {
            return true;
        }

        if (isNodeSelectable(selectedNode)) {
            if (!isNodesIdFromExternalData() && BaseUtils.safeEquals(getExtractor().extractId(selectedNode), nodeId)) {
                Window.alert(I18n.nieMoznaWybracSamegoSiebie.get() /* I18N:  */ + ".");
                return false;
            }
            String messageForUnselectable = getMessageForUnselectableNode(selectedNode);

            boolean canSelect = messageForUnselectable == null;
            if (canSelect) {
                return true;
            } else {
                Window.alert(messageForUnselectable);
                return false;
            }
        } else {
            Window.alert(I18n.wybierzZKategoriElementPodrzedn.get() /* I18N:  */ + ".");
            return false;
        }
    }

    protected void buildAndShowDialog(IDT nodeId, IParametrizedContinuation<BEAN> saveCont,
            IJoinedObjSelectionManager<IDT> alreadyJoinedManager) {
//        this.optNodeNamePathStr = optNodeNamePath == null ? null
//                : BaseUtils.mergeWithSepEx(optNodeNamePath, " » ");
        this.saveCont = saveCont;
        this.nodeId = nodeId;
        this.alreadyJoinedManager = alreadyJoinedManager;

        buildAlreadyJoinedObjIds();

        super.buildAndShowDialog();
    }

    protected void buildAndShowDialog(IDT nodeId, IParametrizedContinuation<BEAN> saveCont,
            IJoinedObjSelectionManager<IDT> alreadyJoinedManager, boolean useLazyLoading) {
        this.useLazyLoading = useLazyLoading;
        buildAndShowDialog(nodeId, saveCont, alreadyJoinedManager);
    }

    protected void buildAlreadyJoinedObjIds() {
        if (alreadyJoinedManager != null) {
            alreadyJoinedObjIds = new HashSet<IDT>(alreadyJoinedManager.getJoinedObjIds());

            for (IDT id : alreadyJoinedObjIds) {
                JoinedObjBasicGenericInfoBean jobi = alreadyJoinedManager.getInfoForJoinedNodeById(id);
                splitBranchIdsIntoColl(jobi.branchIds, true, ancestorIdsOfJoinedObjs);
            }
        }
    }

    protected abstract Collection<IDT> splitBranchIdsIntoColl(String branchIds, boolean withOutSelf, Collection<IDT> coll);

    protected boolean isInMultiSelectMode() {
        return alreadyJoinedManager != null && isInheritToDescendantsCheckBoxVisible();
    }

    // defaultowo na wszystkich węzłach da się kliknąć
    protected boolean isNodeSelectable(BEAN node) {
        return true;
    }

    // może zwrócić NULL
    protected JoinedObjBasicGenericInfoBean getJoinedObjBasicInfo(IDT dstNodeId) {
        if (alreadyJoinedManager == null || !alreadyJoinedObjIds.contains(dstNodeId)) {
            return null;
        }

        JoinedObjBasicGenericInfoBean jobi = alreadyJoinedManager.getInfoForJoinedNodeById(dstNodeId);

        if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
            ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "jobi for id=" + dstNodeId + ": " + jobi);
        }

        return jobi;
    }

    protected boolean isJobiFixed(JoinedObjBasicGenericInfoBean jobi) {
        boolean res = jobi != null && (jobi.type || isNodesIdFromExternalData() || !BaseUtils.safeEquals(jobi.srcId, nodeId));

        if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
            ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "isJobiFixed jobi=" + jobi + ", nodeId=" + nodeId + ", result: " + res);
        }

        return res;
    }

    protected boolean isNodeSelectionFixed(BEAN node) {
        if (isNodeKindIdInLinkedKind(node)) {
            return true;
        }

        if (!isNodesIdFromExternalData() && BaseUtils.safeEquals(getExtractor().extractId(node), nodeId)) {

            if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
                ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "isNodeSelectionFixed (true) for id=" + getExtractor().extractId(node) + ", nodeId=" + nodeId);
            }

            return true;
        }

        JoinedObjBasicGenericInfoBean jobi = getJoinedObjBasicInfo(getExtractor().extractId(node));
        return isJobiFixed(jobi);
    }

    protected boolean isNodeKindIdInLinkedKind(BEAN node) {
        return false;
    }

    protected JoinedObjMode getNodeSelectedValueEx(BEAN node, boolean noOverride) {
        JoinedObjBasicGenericInfoBean jobi = getJoinedObjBasicInfo(getExtractor().extractId(node));

        if (!isJobiFixed(jobi) && !noOverride) {
            JoinedObjMode jobiOverride = directOverride.get(getExtractor().extractId(node));
            if (jobiOverride != null) {
                if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
                    ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "getNodeSelectedValueEx (true) for id=" + getExtractor().extractId(node) + ", nodeId=" + nodeId + ", from override: " + jobiOverride);
                }
                return jobiOverride;
            }

            List<IDT> ancestorNodeIds = (List<IDT>) splitBranchIdsIntoColl(getExtractor().extractBranchIds(node), true, new ArrayList<IDT>());
            JoinedObjMode fromAncestor = null;
            boolean isParent = true;
            for (int i = ancestorNodeIds.size() - 1; i >= 0; i--) {
                IDT ancestorNodeId = ancestorNodeIds.get(i);
                Pair<JoinedObjMode, Boolean> ancestorOverride = subNodesOverride.get(ancestorNodeId);
                if (ancestorOverride != null && (ancestorOverride.v2 || isParent)) {
                    fromAncestor = ancestorOverride.v1;
                    break;
                }
                isParent = false;
            }
            if (fromAncestor != null) {
                return fromAncestor;
            }
        }

        if (jobi == null) {
            if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
                ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "getNodeSelectedValueEx (true) for id=" + getExtractor().extractId(node) + ", nodeId=" + nodeId + ", no jobi");
            }
            return JoinedObjMode.NotJoined;
        }

        if (jobi.inheritToDescendants || isNodesIdFromExternalData() || !BaseUtils.safeEquals(jobi.srcId, nodeId)) {
            if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
                ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "getNodeSelectedValueEx (true) for id=" + getExtractor().extractId(node) + ", nodeId=" + nodeId + ", jobi=" + jobi + ", with inheritance");
            }
            return JoinedObjMode.WithInheritance;
        }

        if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
            ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "getNodeSelectedValueEx (true) for id=" + getExtractor().extractId(node) + ", nodeId=" + nodeId + ", jobi=" + jobi + ", single");
        }
        return JoinedObjMode.Single;
    }

    protected JoinedObjMode getNodeSelectedValue(BEAN node) {
        return getNodeSelectedValueEx(node, false);
    }

    protected String generateHtmlForJoinedObjImg(BEAN node) {
        JoinedObjMode selectedVal = getNodeSelectedValue(node);
        boolean isFixed = isNodeSelectionFixed(node);
        return generateHtmlForJoinedObjImg(selectedVal, isFixed);
    }

    protected String generateHtmlForJoinedObjImg(JoinedObjMode selectedVal, boolean isFixed) {
        return "<" + "img src='images" /* I18N: no */ + "/joined_"
                + selectedVal.imgNamePart + (isFixed ? "_fixed" : "")
                + "." + "png' style=\"width: 16px; height: 16px;\" border=\"0\" class" /* I18N: no */ + "=\"treeIcon\"/>";
    }

    protected boolean hasChangeInDescendants(BEAN node) {
        return false;
    }

    protected boolean hasChangeInNodeById(BEAN node) {
        JoinedObjMode currVal = getNodeSelectedValue(node);
        JoinedObjMode origVal = getNodeSelectedValueEx(node, true);
        return currVal != origVal;
    }

    protected boolean isNodeOriginallyJoined(BEAN node) {
        return getJoinedObjBasicInfo(getExtractor().extractId(node)) != null;
        //alreadyJoinedManager != null && alreadyJoinedManager.getInfoForJoinedNodeById(node.id) != null;
    }

    protected boolean hasJoinedNodesInDescendantsById(BEAN node) {
        return alreadyJoinedManager != null && ancestorIdsOfJoinedObjs.contains(getExtractor().extractId(node));
    }

    protected void setJOMLabelStyles(BEAN node, InlineLabel lbl) {
        lbl.setStyleName("");
        if (hasChangeInNodeById(node)) {
            lbl.addStyleName("tree-selector-hasChangeInNode");
        }
        if (isNodeOriginallyJoined(node)) {
            lbl.addStyleName("tree-selector-isNodeOriginallyJoined");
        }
        if (hasJoinedNodesInDescendantsById(node)) {
            lbl.addStyleName("tree-selector-hasJoinedNodesInDescendants");
        }
        if (hasChangeInDescendants(node)) {
            lbl.addStyleName("tree-selector-hasChangeInDescendants");
        }
    }

    protected Widget createWidgetForNameColumn(final BEAN node) {
        InlineLabel lbl = new InlineLabel(getExtractor().extractName(node));
        setJOMLabelStyles(node, lbl);

        lblForJOM.put(getExtractor().extractId(node), lbl);

        if (!isNodeSelectable(node)) {
            return lbl;
        }

        JoinedObjMode selectedVal = getNodeSelectedValue(node);
        final boolean isFixed = isNodeSelectionFixed(node);
        InlineFlowPanel ifp = new InlineFlowPanel();
        final InlineHTML imgHtml = new InlineHTML(generateHtmlForJoinedObjImg(selectedVal, isFixed));

        if (!isFixed) {
            htmlForJOMImg.put(getExtractor().extractId(node), imgHtml);

            imgHtml.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    JoinedObjMode nextVal = getNodeSelectedValue(node).getNextVal(isStandardJoinedObjMode());
                    directOverride.put(getExtractor().extractId(node), nextVal);
                    imgHtml.setHTML(generateHtmlForJoinedObjImg(nextVal, isFixed));
                    updateLabelStylesOfNode(node);
                    optHandlerWhenNodeSelected(node);
                    configureButtons();
                }
            });
        }

        ifp.add(imgHtml);
        ifp.add(lbl);
        return ifp;
    }

    protected void optHandlerWhenNodeSelected(BEAN node) {
        //no-op
    }

    protected String getMessageForUnselectableNode(BEAN tnb) {
        if (!isNodesIdFromExternalData() && BaseUtils.safeEquals(getExtractor().extractId(tnb), nodeId)) {
            return I18n.nieMoznaPowiazacWezlaZNimSamym.get() /* I18N:  */;
        }
        return null;
    }

    //ww: nie rób focusa wcale
    @Override
    protected void tryFocusFirstWidget() {
        // no-op
    }

    @Override
    protected void onEnterPressed() {
        if (isInFilterBox) {
            doFilter();
        } else {
            super.onEnterPressed();
        }
    }

    protected void treeHasNoData() {
        setSelectedNodeFromBikTree(null);
    }

    protected void buildTreeMainWidgets(Panel mainBodyContainer) {

        bikTree = getTreeWidget();

        Widget treeWidget = bikTree.getTreeGridWidget();
        //treeWidget.setSize("100%", "100%");
        treeWidget.setSize("600px", "100%");
        mainBodyContainer.add(treeWidget);

        HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);

        if (bikTree.isSubTreeFilteringCapable()) {
            cbFilterInSubTree.setTitle(I18n.filtrujWPoddrzewie.get());
            cbFilterInSubTree.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    updateFilterButtonEnabled();
                }
            });

            hPanel.add(cbFilterInSubTree);
            hPanel.setCellWidth(cbFilterInSubTree, "22px");
        }

        filterBox = new TextBox();
        filterBox.setStyleName("filterBox");
        filterBox.setWidth("164px");

        filterBox.addFocusHandler(new FocusHandler() {
            public void onFocus(FocusEvent event) {
                isInFilterBox = true;
            }
        });

        filterBox.addBlurHandler(new BlurHandler() {
            public void onBlur(BlurEvent event) {
                isInFilterBox = false;
            }
        });

        hPanel.add(filterBox);
        hPanel.setCellWidth(filterBox, "180px");
        hPanel.setStyleName("treeBikFilterBox");
        hPanel.setWidth("100%");

        filterBox.addKeyUpHandler(new KeyUpHandler() {
            public void onKeyUp(KeyUpEvent event) {
                updateFilterButtonEnabled();
            }
        });

        IContinuation filterCont = new IContinuation() {
            public void doIt() {
                doFilter();
            }
        };
        filterButton = new BikItemButtonBig(I18n.fILTRUJ.get() /* I18N:  */, filterCont);

        hPanel.add(filterButton);
        hPanel.setCellHorizontalAlignment(filterButton, HasHorizontalAlignment.ALIGN_LEFT);
        mainBodyContainer.add(hPanel);

        if (isInMultiSelectMode()) { // multi mode
            bikTree.setCellWidgetBuilder(new ITreeGridCellWidgetBuilder<BEAN>() {
                public Widget getOptWidget(BEAN row, String colName) {
                    if (!BaseUtils.safeEquals(colName, TreeNodeBeanAsMapStorage.NAME_FIELD_NAME)) {
                        return null;
                    }
                    return createWidgetForNameColumn(row);
                }
            });
            addPopupMenuInMultiSelectMode();
        } else { // single mode
            bikTree.addSelectionChangedHandler(new IParametrizedContinuation<Map<String, Object>>() {
                public void doIt(Map<String, Object> param) {
                    bikTreeNodeChanged(param);
                }
            });
            bikTree.addDoubleClickHandler(new IParametrizedContinuation<Map<String, Object>>() {
                public void doIt(Map<String, Object> param) {
                    doAfterDoubleClickOnTree(param);
                }
            });
        }

        bikTree.addSelectionChangedHandler(new IParametrizedContinuation<Map<String, Object>>() {

            @Override
            public void doIt(Map<String, Object> param) {
                updateFilterButtonEnabled();
            }
        });
        if (actionBtn != null) {
            actionBtn.setEnabled(false);
        }
//        tf: zbędny, podwójny fetch
//        fetchData();
    }

    protected void updateFilterButtonEnabled() {

        Integer currentNodeIdAsInteger = getCurrentNodeIdAsInteger();
        cbFilterInSubTree.setEnabled(currentNodeIdAsInteger != null);

        boolean subTreeFilteringChanged = (currentFilterInSubTreeNodeId != null) != (cbFilterInSubTree.isEnabled() && cbFilterInSubTree.getValue())
                || !BaseUtils.safeEquals(currentFilterInSubTreeNodeId, currentNodeIdAsInteger);
        boolean textHasChanged = !BaseUtils.safeEquals(filterBox.getText(), currentFilterVal);
        filterButton.setEnabled(subTreeFilteringChanged || textHasChanged);

        boolean filterIsOn = !BaseUtils.isStrEmpty(currentFilterVal);

        GWTUtils.addOrRemoveStyleName(filterButton, "filterIsOn", filterIsOn);

        if (filterIsOn) {
            if (currentFilterInSubTreeNodeId == null) {
                filterButton.setTitle("Filtrowanie [" + currentFilterVal + "]");
            }
//            filterButton.setTitle("Filtrowanie [" + currentFilterVal + "]"
//                    + (currentFilterInSubTreeNodeId != null ? " w poddrzewie węzła: ..." : ""));
        } else {
            filterButton.setTitle("Brak filtrowania");
        }
    }

    protected void addPopupMenuInMultiSelectMode() {
        bikTree.addRightClickHandler(new IFoxyTreeOrListGridRightClickHandler<Map<String, Object>>() {
            public void doIt(Map<String, Object> dataRow, int clientX, int clientY, boolean isRowSelected) {
                showPopupMenu(dataRow, clientX, clientY);
            }
        });
    }

    protected IBean4TreeExtractor<BEAN, IDT> getExtractor() {
        if (beanExtractor == null) {
            beanExtractor = createBeanExtractor();
        }
        return beanExtractor;
    }

    public abstract IBean4TreeExtractor<BEAN, IDT> createBeanExtractor();

    protected abstract AbstractBIKTreeWidget<BEAN, IDT> getTreeWidget();

    // tf: flaga mówiąca, czy id są z zewnętrznego systemu i nie powinny być porównywalne z nodeId z bik_node
    // (np aby nie móc dodać powiązanie do samego siebie)
    // defaultowo false
    protected boolean isNodesIdFromExternalData() {
        return false;
    }

    protected void groupResultsByMenu(List<BEAN> result) {
        // no op
    }

    protected boolean isGrouppingByMenuEnabled() {
        return false;
    }

    protected boolean isStandardJoinedObjMode() {
        return true;
    }
}
