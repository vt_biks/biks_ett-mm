/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import pl.fovis.common.SystemUserGroupBean;

/**
 *
 * @author tflorczak
 */
public class GroupBeanExtractor implements IBean4TreeExtractor<SystemUserGroupBean, String> {

    @Override
    public String extractId(SystemUserGroupBean b) {
        return b.objId;
    }

    @Override
    public String extractParentId(SystemUserGroupBean b) {
        return b.parentId;
    }

    @Override
    public String extractName(SystemUserGroupBean b) {
        return b.name;
    }

    @Override
    public boolean extractHasNoChildren(SystemUserGroupBean b) {
        return b.hasNoChildren;
    }

    @Override
    public int extractVisualOrder(SystemUserGroupBean b) {
        return 0;
    }

    @Override
    public boolean extractIsFolder(SystemUserGroupBean b) {
        return true; // ?
    }

    @Override
    public String extractBranchIds(SystemUserGroupBean b) {
        return null; // ????
    }
}
