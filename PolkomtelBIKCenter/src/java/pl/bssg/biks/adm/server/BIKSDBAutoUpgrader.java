/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.server;

import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.serverlogic.db.autoupdater.MSSQLDBAutoUpgrader;

/**
 *
 * @author pmielanczuk
 */
public class BIKSDBAutoUpgrader extends MSSQLDBAutoUpgrader {

    protected int procId;
    protected IAppAndDBVerInfoServiceForUpgrader service;

    public BIKSDBAutoUpgrader(int procId, IAppAndDBVerInfoServiceForUpgrader service,
            String appVer, String alterSqlsDir, IBeanConnectionEx<Object> conn) {
        super(appVer, alterSqlsDir, conn);
        this.procId = procId;
        this.service = service;
    }

    @Override
    protected void startAlterScriptForDbVer(String dbVer) {
        service.execNamedCommand("dbauUpdateProcess", procId, 0);
    }

    @Override
    protected void finishedAlterScriptForDbVer(String newDbVer) {
        service.updateProcessFinalDbVer(procId, newDbVer);
    }

    @Override
    protected String getCurrentDBVer() {
        return service.getDbVer(/*isBIKSInstance*/);
    }

    @Override
    protected void logExecution(String msg) {
        super.logExecution(msg);
        service.execNamedCommand("dbauLogMsg", procId, msg);
    }
}
