/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.wezyrtreegrid;

/**
 *
 * @author pmielanczuk
 */
public interface IExpandCollapseHandler<ROW> {

    public void rowExpandedCollapsed(ROW row);
}
