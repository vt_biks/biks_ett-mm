/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.common.TutorialBean;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 * nie uzywane narazie do oceniania tutoriali
 */
public class RateDialog extends BaseActionOrCancelDialog {

    protected int id;
    protected String title;
    protected IParametrizedContinuation<TutorialBean> saveCont;

    @Override
    protected String getDialogCaption() {
        return I18n.ocen.get() /* I18N:  */;
    }
    List<RadioButton> radioButtons = new ArrayList<RadioButton>();

    @Override
    protected String getActionButtonCaption() {
        return I18n.zatwierdzOcene.get() /* I18N:  */;
    }

    @Override
    protected String getCancelButtonCaption() {
        return I18n.ocenPozniej.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        HorizontalPanel hp = new HorizontalPanel();
        for (int i = 1; i < 6; i++) {
            RadioButton rb = new RadioButton("Evaluation" /* I18N: no */, i + "");
            radioButtons.add(rb);
            rb.addStyleName("Tutorial" /* I18N: no */+ "-radioBtn");
            hp.add(rb);
        }


        Label titleLb = new Label(I18n.ocenTutorial.get() /* I18N:  */+ ": " + title);
        titleLb.setStyleName("addNoteLbl");

        main.add(titleLb);
        main.add(hp);

    }

    @Override
    protected void doAction() {
        TutorialBean tb = new TutorialBean();
        tb.id = id;
        for (RadioButton rb : radioButtons) {
            if (rb.getValue()) {
//                tb.rate = radioButtons.indexOf(rb)+1;
            }
        }
        saveCont.doIt(tb);
    }

    public void buildAndShowDialog(int id, String title, IParametrizedContinuation<TutorialBean> saveCont) {
        this.id = id;
        this.title = title;
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }
}
