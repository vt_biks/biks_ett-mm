/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.client;

import pl.bssg.biks.adm.common.UnauthorizedServiceMethodAccessException;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author pmielanczuk
 */
public abstract class AdmAsyncCallback<T> extends StandardAsyncCallback<T> {

    @Override
    public void onFailure(Throwable caught) {
        if (caught instanceof UnauthorizedServiceMethodAccessException) {
            super.onFailure(caught); //To change body of generated methods, choose Tools | Templates.
        } else {
            super.onFailure(caught); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
