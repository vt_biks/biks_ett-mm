/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.ad;

import com4j.COM4J;
import com4j.ComException;
import com4j.Variant;
import com4j.typelibs.activeDirectory.IADs;
import com4j.typelibs.activeDirectory.IADsOpenDSObject;
import com4j.typelibs.activeDirectory.IADsUser;
import com4j.typelibs.ado20.ClassFactory;
import com4j.typelibs.ado20._Command;
import com4j.typelibs.ado20._Connection;
import com4j.typelibs.ado20._Recordset;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.StackTraceUtil;

/**
 *
 * @author tflorczak
 */
public class ADLoginAuthenticationChecker {

    protected String domain;
    protected String username;
    protected String password;

    public ADLoginAuthenticationChecker(String domain, String username, String password) {
        this.domain = domain;
        this.username = username;
        this.password = password;
    }

    public boolean checkAuthentication() {
        if (BaseUtils.isStrEmptyOrWhiteSpace(domain)) {
            domain = "rootDSE";
        } else {
            domain += "/rootDSE";
        }
        try {
            IADs rootDSE = COM4J.getObject(IADs.class, "LDAP://" + domain, null);
            String defaultNamingContext = (String) rootDSE.get("defaultNamingContext");

            _Connection con = ClassFactory.createConnection();
            con.provider("ADsDSOObject");
            con.open("Active Directory Provider", ""/*default*/, ""/*default*/, -1/*default*/);

            _Command cmd = ClassFactory.createCommand();
            cmd.activeConnection(con);

            cmd.commandText("<LDAP://" + defaultNamingContext + ">;(sAMAccountName=" + username + ");distinguishedName;subTree");
            _Recordset rs = cmd.execute(null, Variant.getMissing(), -1/*default*/);
            if (rs.eof()) {
                throw new LameRuntimeException("No such user: " + username);
            }

            String dn = rs.fields().item("distinguishedName").value().toString();

            // now we got the DN of the user
            IADsOpenDSObject dso = COM4J.getObject(IADsOpenDSObject.class, "LDAP:", null);

            // to do bind with DN as the user name, the flag must be 0
            IADsUser usr = null;
            try {
                usr = dso.openDSObject("LDAP://" + dn, dn, password, 0).queryInterface(IADsUser.class);
            } catch (ComException e) {
                throw new LameRuntimeException("Incorrect password for " + username);
            }
        } catch (Exception e) {
//            System.out.println("Error in checkAuthentication() for login: " + username + " and domain: " + domain + ". Error message: "
//                    + e.getMessage());
            System.out.println("Error in checkAuthentication() for login: " + username + " and domain: " + domain + ". Error message: "
                    + StackTraceUtil.getCustomStackTrace(e));
            return false;
        }
        return true;
    }
}
