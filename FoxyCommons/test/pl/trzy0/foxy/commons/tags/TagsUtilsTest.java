/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.tags;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import simplelib.BaseUtils;
import static org.junit.Assert.*;

/**
 *
 * @author wezyr
 */
public class TagsUtilsTest {

    public TagsUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testValidateHtml() {
        assertEquals(true, TagsUtils.validateHtml("<html>ala ma kota</html>", null));

        assertEquals(true, TagsUtils.validateHtml("<html>ala <b>ma</b> kota</html>", null));

        assertEquals(false, TagsUtils.validateHtml("<html><b>ala <i>ma</b></i> kota</html>", null));

        assertEquals(true, TagsUtils.validateHtml("<html><b>ala <i>ma</i></b> kota</html>", null));

        assertEquals(true, TagsUtils.validateHtml("<html><b>ala <i>ma</i></b> kota</html>",
                BaseUtils.splitUniqueBySep("html, b, i", ",", true)));
        assertEquals(false, TagsUtils.validateHtml("<html><b>ala <i>ma</i></b> kota</html>",
                BaseUtils.splitUniqueBySep(" b, i", ",", true)));

        assertEquals(true, TagsUtils.validateHtml("<b>ala <i>ma</i></b> kota",
                BaseUtils.splitUniqueBySep(" b, i", ",", true)));

        assertEquals(false, TagsUtils.validateHtml("<b>ala <i>ma</i></b> kota",
                BaseUtils.splitUniqueBySep("i", ",", true)));
    }

    @Test
    public void testValidateAsStandardRichTextHtml() {
        assertEquals(false,
                TagsUtils.validateAsStandardRichTextHtml("<html>ala ma kota</html>"));

        assertEquals(false, TagsUtils.validateAsStandardRichTextHtml("<html>ala <b>ma</b> kota</html>"));

        assertEquals(false, TagsUtils.validateAsStandardRichTextHtml("<html><b>ala <i>ma</b></i> kota</html>"));

        assertEquals(true, TagsUtils.validateAsStandardRichTextHtml("<b>ala <i>ma</i></b> kota"));

        assertEquals(true, TagsUtils.validateAsStandardRichTextHtml("<b>ala <i>ma</i></b> kota"));

        assertEquals(true, TagsUtils.validateAsStandardRichTextHtml("<b>ala <i>ma</i></b><br /> <span>kota</span>"));

        assertEquals(false, TagsUtils.validateAsStandardRichTextHtml("<strong>ala <i>ma</strong></i></b> kota"));
    }
}
