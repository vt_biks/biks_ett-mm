/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence.wuwu;

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author pmielanczuk
 */
public interface IConfluenceObjectImportedChecker {

    public Map<String, String> checkConfluenceImportedObjs(Collection<String> objIds, Integer optSelectedNodeId);

    public Map<String, String> checkConfluenceImportedLogins(Collection<String> logins);

    public Map<String, String> checkDwhImportedObjs(Collection<String> dwhObjNames);
}
