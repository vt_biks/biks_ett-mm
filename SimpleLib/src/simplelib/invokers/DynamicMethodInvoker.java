/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.invokers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import simplelib.LameRuntimeException;

/**
 *
 * @author pmielanczuk
 */
public class DynamicMethodInvoker<T> implements IDynamicMethodInvoker<T> {

    protected Map<Class<T>, IDynamicMethodInvoker<? extends T>> invokers = new HashMap<Class<T>, IDynamicMethodInvoker<? extends T>>();
    protected List<Class<T>> invokerBaseClasses = new ArrayList<Class<T>>();
    protected List<IDynamicMethodInvoker<? extends T>> registeredInvokers = new ArrayList<IDynamicMethodInvoker<? extends T>>();

    public IDynamicMethodInvoker<T> findInvokerForClass(Class c) {
        return null;
    }

    @SuppressWarnings("unchecked")
    public Object invokeMethod(T obj, String methodName, Object... args) {
        Class c = obj.getClass();
//        @SuppressWarnings("unchecked")
        IDynamicMethodInvoker<T> dmi = (IDynamicMethodInvoker<T>) invokers.get(c);

        if (dmi == null) {
            dmi = findInvokerForClass(c);
            if (dmi == null) {
                throw new LameRuntimeException("cannot find one-class invoker for class " + c);
            }
            invokers.put(c, dmi);
        }

        return dmi.invokeMethod(obj, methodName, args);
    }

    public void addOneClassMethodInvoker(Class<T> c, IDynamicMethodInvoker<? extends T> dmi) {
        if (invokers.containsKey(c)) {
            throw new LameRuntimeException("invoker for base class " + c + " already registered");
        }
        invokerBaseClasses.add(c);
        registeredInvokers.add(dmi);
        invokers.put(c, dmi);
    }
}
