/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import pl.bssg.gwtservlet.IRequestLifecycleAwareService;
import pl.bssg.gwtservlet.IServiceRequestContext;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.mltx.MultixConstants;
import pl.trzy0.foxy.commons.FoxyCommonConsts;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.commons.MailServerConfig;
import pl.trzy0.foxy.serverlogic.FoxyBizLogicBase;
import pl.trzy0.foxy.serverlogic.IFoxySessionDataProvider;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;
import simplelib.IContinuation;
import simplelib.IContinuationWithReturn;
import simplelib.JsonValueHelper;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public class BIKSServiceBase extends FoxyBizLogicBase<BIKCenterConfigObj, BOXISessionData, BiksSharedSessionObj> implements IRequestLifecycleAwareService {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    private static final String NODE_FILTERING_ACTION_CODE_FOR_CURRENT_REQUEST = "nodeFilteringActionCodeForCurrentRequest";
    private static final String NODE_FILTERING_ACTION_ID_FOR_CURRENT_REQUEST = "nodeFilteringActionIdForCurrentRequest";
    protected String defaultDatabaseName;
    private boolean isMultiXMode;
    protected boolean isDefaultDbASingeBiksInstance;
    protected IBIKSSystemUsersSubservice systemUsersSubservice;
    protected IMultiXLoginsSubservice multiXLoginsSubservice;
//    protected static final ThreadLocal<String> currentFullHref = new ThreadLocal<String>();
    protected String urlForPublicAccess;
//    protected boolean isNexusMode = true;

//    protected ThreadLocal<String> getCurrentDatabase() {
//        return sharedServiceObj.currentSingleBiksDb;
//    }
    protected String getOptSingleBiksDbForCurrentRequest() {
//        return sharedServiceObj.currentSingleBiksDb.get();
        return isMultiXMode() ? sharedServiceObj.currentSingleBiksDb.get() : defaultDatabaseName;
    }

    @RequireSingleBiksDb
    protected String getSingleBiksDbForCurrentRequest() {

        String dbName = getOptSingleBiksDbForCurrentRequest();

        if (dbName == null) {
            throw new LameRuntimeException("getSingleBiksDbForCurrentRequest: singleBiksDbForRequest is not set!");
        }

        return dbName;
    }

    protected void setSingleBiksDbForCurrentRequest(String dbName) {

        boolean useDefault = BaseUtils.safeEquals(dbName, defaultDatabaseName);

        if (isMultiXMode) {
            if (useDefault && !isDefaultDbASingeBiksInstance) {
                throw new LameRuntimeException("setSingleBiksDbForRequest: MultiXMode, dbName=" + dbName + " is default, but has no singleBIKS objs");
            }
            // ustawiamy tylko dla trybu MultiX
            sharedServiceObj.currentSingleBiksDb.set(dbName);
        } else {
            if (!useDefault && dbName != null) {
                throw new LameRuntimeException("setSingleBiksDbForRequest: singleBiksMode, dbName=" + dbName + ", defaultDatabaseName=" + defaultDatabaseName);
            }
            // tu nie ustawiamy currentSingleBiksDb, bo dla trybu SingleBiks
            // nie korzystamy z wartości w currentSingleBiksDb
        }
    }

    protected void setOptFilteringNodeActionCodeForCurrentRequest(String nodeActionCode) {
        getRequestTempStorage().put(NODE_FILTERING_ACTION_CODE_FOR_CURRENT_REQUEST, nodeActionCode);
    }

    protected void setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs() {
        setOptFilteringNodeActionCodeForCurrentRequest("AddJoinedObjs");
    }

    public Integer getNodeFilteringActionIdForCurrentRequest() {
        Integer actionId = (Integer) getRequestTempStorage().get(NODE_FILTERING_ACTION_ID_FOR_CURRENT_REQUEST);

        if (actionId == null) {
            String actionCode = BaseUtils.nullToDef((String) getRequestTempStorage().get(NODE_FILTERING_ACTION_CODE_FOR_CURRENT_REQUEST),
                    BIKConstants.NODE_ACTION_CODE_SHOW_IN_TREE);
            actionId = execNamedQuerySingleValAsInteger("getNodeActionNameByCode", true, null, actionCode);
            getRequestTempStorage().put(NODE_FILTERING_ACTION_ID_FOR_CURRENT_REQUEST, actionId);
        }

        return actionId;
    }

    public void setCommonSubservices(IBIKSSystemUsersSubservice systemUsersSubservice,
            IMultiXLoginsSubservice multiXLoginsSubservice) {
        this.systemUsersSubservice = systemUsersSubservice;
        this.multiXLoginsSubservice = multiXLoginsSubservice;
    }

    protected boolean isInMultiXModeAndNoSingleBiksDbDetermined() {
        return isMultiXMode() && getOptSingleBiksDbForCurrentRequest() == null;
    }

    @Override
    public void init(BIKCenterConfigObj configBean, IFoxySessionDataProvider<BOXISessionData> sessionDataProvider, INamedSqlsDAO<Object> adhocDao, BiksSharedSessionObj sharedServiceObj) {
        try {
            super.init(configBean, sessionDataProvider, adhocDao, sharedServiceObj);
        } finally {
            setSingleBiksDbForCurrentRequest(null);
        }
    }

    @Override
    @PerformOnDefaultDb
    protected void initWithConfig(BIKCenterConfigObj configBean) {
        super.initWithConfig(configBean);
        defaultDatabaseName = configBean.connConfig.database;
//        getCurrentDatabase().set(defaultDatabaseName);

        //setSingleBiksDbForRequest(defaultDatabaseName);
        this.urlForPublicAccess = configBean.urlForPublicAccess;

        Map<String, Object> row = execNamedQuerySingleRowCFN("isMultiX", false, FieldNameConversion.ToLowerCase);
        String isMultiXStr = (String) row.get("is_multi_x_str");
        int hasBikUserTabVal = sqlNumberInRowToInt(row, "has_bik_user_tab");
        this.isMultiXMode = JsonValueHelper.getBool(isMultiXStr);
        this.isDefaultDbASingeBiksInstance = hasBikUserTabVal > 0;
    }

    // czy jesteśmy w trybie singleBIKSa czy MultiXa - jak to statycznie określa
    // konfiguracja połączenia?
    protected boolean isMultiXMode() {
        return isMultiXMode;
    }

    protected ThreadLocal<Map<Integer, Pair<Long, String>>> getOverwrittenMods() {
        return sharedServiceObj.overwrittenMods;
    }

    protected void optCustomizeServiceMethodCallDefaultRequiredContext(Map<String, Boolean> drc) {
        // no-op
    }

    protected Map<String, Boolean> getServiceMethodCallDefaultRequiredContext() {
        Map<String, Boolean> res = new HashMap<String, Boolean>();
        optCustomizeServiceMethodCallDefaultRequiredContext(res);
        return res;
    }

    protected Map<String, Boolean> getServiceMethodCallActualRequiredContext(IServiceRequestContext ctx) {

        Map<String, Boolean> res = getServiceMethodCallDefaultRequiredContext();

        Method m = ctx.getServiceImplMethod();

        if (m.getAnnotation(RequireLoggedUser.class) != null) {
            res.put("isLoggedIn", true);
        }

        if (m.getAnnotation(PerformOnDefaultDb.class) != null) {
            res.remove("onSingle");
            res.remove("onMaster");
        }

        if (m.getAnnotation(PerformOnAnyDb.class) != null) {
            res.remove("onSingle");
            res.remove("onMaster");
            res.remove("hasSingle");
            res.remove("hasMaster");
        } else {

            if (m.getAnnotation(RequireMasterBiksDb.class) != null) {
                res.put("hasMaster", true);
            }
            if (m.getAnnotation(RequireSingleBiksDb.class) != null) {
                res.put("hasSingle", true);
            }
            if (m.getAnnotation(PerformOnMasterBiksDb.class) != null) {
                res.put("onMaster", true);
                res.put("onSingle", false);
            }
            if (m.getAnnotation(PerformOnSingleBiksDb.class) != null) {
                res.put("onSingle", true);
                res.put("onMaster", false);
            }

            if (Boolean.TRUE.equals(res.get("onMaster"))) {
                res.put("hasMaster", true);
            }

            if (Boolean.TRUE.equals(res.get("onSingle"))) {
                res.put("hasSingle", true);
            }
        }

        return res;
    }

    protected Map<String, Boolean> getActualRequestContext(IServiceRequestContext ctx) {
        Map<String, Boolean> res = new HashMap<String, Boolean>();

        res.put("isLoggedIn", isUserLoggedIn());
        res.put("hasMaster", isMultiXMode());
        res.put("hasSingle", calcProperSingleBiksDbForServiceMethodCall(ctx) != null);

        return res;
    }

    protected RuntimeException makeBadCallException(IServiceRequestContext ctx, String txt) {
        return new LameRuntimeException("cannot call service method " + ctx.getServiceMethodName() + " implemented in "
                + BaseUtils.safeGetClassName(this) + ": " + txt);
    }

    protected String calcProperSingleBiksDbForServiceMethodCall(IServiceRequestContext ctx) {

        if (/*!isNexusMode && */!isMultiXMode()) {
            return defaultDatabaseName;
        }

        String requestedDatabaseName = ctx.getContextParamValue(MultixConstants.DB_NAME_PARAM);
        if (BaseUtils.isStrEmptyOrWhiteSpace(requestedDatabaseName)) {
            requestedDatabaseName = null;
        } else {
            BOXISessionData sessionData = getSessionData();

            if (!sessionData.isAccessibleDbName(requestedDatabaseName)) {
                return null;
            }
        }

        return requestedDatabaseName;
    }

    protected void checkAndSetupServiceMethodCallContext(IServiceRequestContext ctx) {

        BOXISessionData sessionData = getSessionData();

        String ssoUserName = sessionData.remoteUser;

        // jeżeli to nie będzie null, to na koniec tego użytkownika dodamy
        // jako zalogowanego do odpowiedniej bazy SingleBiks
        SystemUserBean sysUsrBean = null;

        String singleBiksDb;

        // jeżeli użytkownik nie jest zalogowany, ale jest określony przez SSO
        // i nie ma flagi, że mamy to ignorować - spróbujemy wlogować go po SSO
        if (!isUserLoggedIn() && !sessionData.ignoreRemoteUser
                && !BaseUtils.isStrEmptyOrWhiteSpace(ssoUserName)) {

            // poniższe działanie w trybie MultiX jest trochę na wyrost,
            // ale zaraz się może okazać, że jak jest grupa firm, to jednak
            // mają MultiXa i wchodzą po SSO ;-)
            //
            // ta instrukcja jest ok, bo w trybie MultiX chcemy działać na bazie MasterBiks,
            // a w trybie SingleBiks - na bazie domyślnej SingleBiks
            // a to jest ta sama baza (defaultDatabaseName)
            setCurrentDbForAdhoc(defaultDatabaseName);

            // wykonaj logowanie w trybie zwykłego SSO (domenowe)
            Pair<String, SystemUserBean> dbAndSysUserBeanPair = systemUsersSubservice.performUserLoginEx(null, null, null);

            if (dbAndSysUserBeanPair == null || dbAndSysUserBeanPair.v2 == null) {
                // lamusek się nie dostał, więc więcej nie sprawdzaj automatycznie
                sessionData.ignoreRemoteUser = true;
            } else {
                sysUsrBean = dbAndSysUserBeanPair.v2;
            }
        }

        // po tym wywołaniu wiemy, że w trybie MultiX, gdy ta baza jest określona,
        // to jest ona osiągalna (accessible) dla zalogowanego użytkownika
        // gdy użytkownik nie jest zalogowany, to w trybie MultiX ta baza jest nieokreślona (null)
        // a w trybie SingleBiks to jest zawsze defaultDatabaseName bez sprawdzania
        // (bo tu nie musi być zalogowany - możliwy ograniczony dostęp dla niezalogowanych)
        singleBiksDb = calcProperSingleBiksDbForServiceMethodCall(ctx);

        Map<String, Boolean> rc = getServiceMethodCallActualRequiredContext(ctx);
        Map<String, Boolean> ac = getActualRequestContext(ctx);

        Map<String, Pair<Boolean, Boolean>> errs = new HashMap<String, Pair<Boolean, Boolean>>();

        for (Entry<String, Boolean> e : rc.entrySet()) {
            String name = e.getKey();

            if (BaseUtils.strHasPrefix(name, "on", false)) {
                continue;
            }

            Boolean val = e.getValue();

            if (val == null) {
                continue;
            }

            Boolean aV = ac.get(name);

            if (!val.equals(aV)) {
                errs.put(name, new Pair<Boolean, Boolean>(val, aV));
            }
        }

        if (!errs.isEmpty()) {
            throw makeBadCallException(ctx, "some context requirements are not met: " + errs);
        }

        String adhocDb = defaultDatabaseName;

//        boolean onMaster = Boolean.TRUE.equals(rc.get("onMaster"));
        boolean onSingle = Boolean.TRUE.equals(rc.get("onSingle"));
        //boolean hasMaster = Boolean.TRUE.equals(rc.get("hasMaster"));
        //boolean hasSingle = Boolean.TRUE.equals(rc.get("hasSingle"));
        //boolean isLoggedIn = Boolean.TRUE.equals(rc.get("isLoggedIn"));

        if (onSingle) {
            // albo tryb SingleBiks (!MultiX), albo tryb MultiX i można określić bazę SingleBiks
            if (isMultiXMode()) {
//                if (singleBiksDb == null) {
//                    throw makeBadCallException(ctx, "MultiX mode but no singleBiksDb");
//                }
//                if (onSingle) {
                adhocDb = singleBiksDb;
//                }
            }
        }

//        if (onMaster || hasMaster) {
//            if (!isMultiXMode()) {
//                throw makeBadCallException(ctx, "not in required MultiX mode");
//            }
//        }
        // to może ustawić nulla i to jest ok - gdy jesteśmy w trybie MultiX,
        // ale nie zalogowani i metoda odwołuje się tylko do bazy MasterBiks
        setSingleBiksDbForCurrentRequest(singleBiksDb);

        String loginName = sessionData.loginName;

        // jeżeli mamy zalogowanego użytkownika, to sprawdzimy, czy jego
        // dane (SystemUserBean) są wczytane do aktualnej bazy SingleBiks
        if (loginName != null && singleBiksDb != null) {
            if (!sessionData.isUserLoggedToSingleBiksDb(singleBiksDb)) {
                // na tej bazie nie był jeszcze w tej sesji - wchodzi pierwszy raz,
                // więc to ustawi też statystyki logowania na bazie

                if (sysUsrBean == null) {
                    setCurrentDbForAdhoc(singleBiksDb);
                    String lun = getLoggedUserName();
                    sysUsrBean = systemUsersSubservice.bikSystemUserByLogin(lun, null);

//                    System.out.println("after bikSystemUserByLogin: lun=" + lun + ", sysUsrBean=" + sysUsrBean + ", methodName=" + ctx.getServiceMethodName());
                    if (sysUsrBean == null) {
                        // no to pasztet, sprawdźmy jak wielki
                        if (onSingle) {
                            if (isMultiXMode()) {
                                throw new LameRuntimeException("user " + loginName + " does not (longer) exist on db " + singleBiksDb);
                            }
                        } else {
                            // czyścimy info o wybranej bazie SingleBiks
                            adhocDb = defaultDatabaseName;
                            if (isMultiXMode()) {
                                setSingleBiksDbForCurrentRequest(null);
                            }
                        }
                    }
                }

                if (sysUsrBean != null) {
                    systemUsersSubservice.setLoggedUserBeanInSessionData(sysUsrBean);
                }
            }
        }

        setCurrentDbForAdhoc(adhocDb);
    }

    @Override
//    public void requestStarting(String requestUrl) {
    public void requestStarting(IServiceRequestContext ctx) {
        getOverwrittenMods().set(new HashMap<Integer, Pair<Long, String>>());

        getRequestTempStorage().put(MultixConstants.DB_NAME_PARAM, ctx.getContextParamValue(MultixConstants.DB_NAME_PARAM));

        checkAndSetupServiceMethodCallContext(ctx);

        if (logger.isDebugEnabled()) {
            logger.debug("requestStarting: done, method=" + ctx.getServiceMethodName() + ", session.id=" + getSessionId() + ", singleBiksDb=" + getOptSingleBiksDbForCurrentRequest());
        }
    }

    protected void dumpCurrentSessionMultiXInfo(String msg) {
        if (!logger.isDebugEnabled()) {
            return;
        }

        String info;
        BOXISessionData sd = getSessionData();
        if (sd == null) {
            info = "session is null";
        } else {
            info = "loggedUser=" + (!isUserLoggedIn() ? "<null>" : sd.loginName)
                    //                    + "; xInstanceDbNames=" + sd.xInstanceDbNames + "; remoteUser=" + sd.remoteUser
                    + "; xInstanceDbNames=" + sd.getAccessibleDbNames() + "; remoteUser=" + sd.remoteUser
                    + "; ignoreRemoteUser=" + sd.ignoreRemoteUser;
        }
        logger.debug("dumpCurrentSessionMultiXInfo: " + msg + ". " + info);
    }

//    protected void setDefaultDatabaseAsCurrent() {
//        setCurrentDbForAdhoc(defaultDatabaseName);
//    }
    protected void setCurrentDbForAdhoc(String databaseName) {
        if (logger.isDebugEnabled()) {
            dumpCurrentSessionMultiXInfo("before setCurrentDatabase: " + databaseName);
        }
        getAdhocDao().setCurrentDatabase(databaseName);
    }

    protected <T> T performOnSingleBiks(IContinuationWithReturn<T> todo) {
        return performOnDatabaseInner(true, getSingleBiksDbForCurrentRequest(), todo);
    }

    protected void performOnSingleBiksExpliciteDb(String dbName, IContinuation todo) {
        performOnDatabaseInner(true, dbName, todo);
    }

    protected <T> T performOnSingleBiksExpliciteDb(String dbName, IContinuationWithReturn<T> todo) {
        return performOnDatabaseInner(true, dbName, todo);
    }

    protected void performOnMasterBiks(final IContinuation todo) {
        performOnDatabaseInner(false, defaultDatabaseName, new IContinuationWithReturn<Void>() {

            @Override
            public Void doIt() {
                todo.doIt();
                return null;
            }
        });
    }

    protected <T> T performOnMasterBiks(IContinuationWithReturn<T> todo) {
        return performOnDatabaseInner(false, defaultDatabaseName, todo);
    }

//    protected synchronized <T> T performOnDatabase(boolean switchDbContext, String dbName, IParametrizedContinuationWithReturn<String, T> todo) {
//        return performOnDatabaseInner(switchDbContext, dbName, todo);
//    }
    private synchronized void performOnDatabaseInner(boolean performOnSingleBiksDb, String dbName, final IContinuation todo) {
        performOnDatabaseInner(performOnSingleBiksDb, dbName, new IContinuationWithReturn<Void>() {

            @Override
            public Void doIt() {
                todo.doIt();
                return null;
            }
        });
    }

    private synchronized <T> T performOnDatabaseInner(boolean performOnSingleBiksDb, String dbName, IContinuationWithReturn<T> todo) {
        String currentDbName = getAdhocDao().getCurrentDatabase();
        //boolean isSwitchRequired = !currentDbName.equals(dbName);
        boolean isSwitchRequired = !BaseUtils.safeEquals(currentDbName, dbName);
        if (isSwitchRequired) {
            //getAdhocDao().setCurrentDatabase(dbName);
            setCurrentDbForAdhoc(dbName);
        }

        try {
            //String currentContextDb = getCurrentDatabase().get();
            String currentContextDb = getOptSingleBiksDbForCurrentRequest();

            if (performOnSingleBiksDb) {
                //getCurrentDatabase().set(dbName);
                setSingleBiksDbForCurrentRequest(dbName);
            }

            try {
                return todo.doIt();
            } finally {
                if (performOnSingleBiksDb) {
                    //getCurrentDatabase().set(currentContextDb);
                    setSingleBiksDbForCurrentRequest(currentContextDb);
                }
            }

        } finally {
            if (isSwitchRequired) {
                //getAdhocDao().setCurrentDatabase(currentDbName);
                setCurrentDbForAdhoc(currentDbName);
            }
        }
    }

//    protected <T> T performOnDefaultDatabase(IParametrizedContinuationWithReturn<String, T> todo) {
////        return performOnDatabase(switchDbContext, defaultDatabaseName, todo);
//        return performOnMasterBiks(todo);
//    }
//    protected void performOnDatabase(boolean switchDbContext, String dbName, final IContinuation todo) {
//        performOnDatabase(switchDbContext, dbName, new IParametrizedContinuationWithReturn<String, Void>() {
//            @Override
//            public Void doIt(String param) {
//                todo.doIt();
//                return null;
//            }
//        });
//    }
    protected void performOnSingleBiks(final IContinuation todo) {
        performOnSingleBiks(new IContinuationWithReturn<Void>() {
            @Override
            public Void doIt() {
                todo.doIt();
                return null;
            }
        });
    }

//    protected void performOnDefaultDatabase(boolean switchDbContext, final IContinuation todo) {
//        //performOnDatabase(switchDbContext, defaultDatabaseName, todo);
//        performOnMasterBiks(new IParametrizedContinuationWithReturn<String, Void>() {
//
//            @Override
//            public Void doIt(String param) {
//                todo.doIt();
//                return null;
//            }
//        });
//    }
    protected String getLisaTeradataLogin(Integer instanceId) {
        Map<String, String> logins = getSessionData().lisaTeradataLogins;
//        return logins == null ? null : logins.get(getCurrentDatabase().get());
        return logins == null ? null : logins.get(combineSingleBiksDbForCurrentRequestAndLisaInstanceId(instanceId));
    }

//    private void checkIfUserStillExists(SystemUserBean user) {
//        createBeanFromNamedQry("bikSystemUserByLogin", SystemUserBean.class, false, user.loginName, null);
//    }
    protected boolean isBIKSInstance(String dbInstanceName) {
//        boolean isBIKSInstance = !isMultiXMode() || !BaseUtils.safeEquals(dbInstanceName, defaultDatabaseName);
//        return isBIKSInstance;

        //ww: jeżeli nie jesteśmy w trybie MultiX, albo aktualna baza nie jest bazą MasterBIKS - to OK
        if (!isMultiXMode || !BaseUtils.safeEquals(dbInstanceName, defaultDatabaseName)) {
            return true;
        }

        //ww: teraz wiadomo, że aktualną bazą jest MasterBIKS, ale jeszcze jest szansa,
        // że możemy mieć na niej jednocześnie tabelki biksowe
        return isDefaultDbASingeBiksInstance;
    }

//    protected boolean isCurrentDatabaseABIKSInstance() {
////        return isBIKSInstance(getCurrentDatabase().get());
//        return isBIKSInstance(sharedServiceObj.currentSingleBiksDb.get());
//    }
    @Override
    public void requestFinishing(IServiceRequestContext ctx, Throwable optExceptionInServiceMethodCall) {

//        System.out.println("requestFinishing: methodName=" + ctx.getServiceMethodName()
//                + ", clientLoggedUserName=" + getClientLoggedUserName() + ", serverLoggedUserName=" + getLoggedUserName());
        String optSingleBiksDb = getOptSingleBiksDbForCurrentRequest();

        // gdy baza SingleBiks jest określona, to przełączymy się na nią,
        // bo maybeSetModifiedTreesInfoInResponse zakłada, że określone,
        // to już przełączone, ale potrafi też działać dla nieokreślonej bazy
        // SingleBiks
        if (optSingleBiksDb != null) {
            setCurrentDbForAdhoc(optSingleBiksDb);
        }

        systemUsersSubservice.maybeSetModifiedTreesInfoInResponse();

        if (optExceptionInServiceMethodCall != null) {
            if (logger.isErrorEnabled()) {
                logger.error("requestFinishing: had exception in call to " + ctx.getServiceMethodName() + ", getServiceExtraResponseData=" + getServiceExtraResponseData());
            }
        }

        getOverwrittenMods().remove();

        sharedServiceObj.currentSingleBiksDb.remove();
    }

//    protected void checkIfBiksDbObjsAccessibleFromCurrentDb() {
//        if (!isCurrentDatabaseABIKSInstance()) {
////            throw new LameRuntimeException("checkIfBiksDbObjsAccessibleFromCurrentDb: current db is not a biks instance (" + getCurrentDatabase().get() + ")");
//            throw new LameRuntimeException("checkIfBiksDbObjsAccessibleFromCurrentDb: current db is not a biks instance (" + getOptSingleBiksDbForCurrentRequest() + ")");
//        }
//    }
    protected String getLoggedUserName() {
        BOXISessionData sd = getSessionData();
        if (sd == null) {
            sd = new BOXISessionData();
        }
        return (sd == null ? null : getSessionData().loginName);
    }

    protected boolean isUserLoggedIn() {
        return getLoggedUserName() != null;
    }

    protected void destroySharedServiceObj() {
        for (BOXIServiceImplData implData : sharedServiceObj.instanceDataMap.values()) {
            implData.destroy();
        }
    }

    @Override
    protected void setServiceExtraResponseData(String data) {
        super.setServiceExtraResponseData(data);
        // poniższe tylko na potrzeby getServiceExtraResponseData, czyli do debugowania
        getRequestTempStorage().put(FoxyCommonConsts.SERVICE_EXTRA_RESPONSE_DATA_HEADER_NAME, data);
    }

    @Deprecated
    protected String getServiceExtraResponseData() {
        return (String) getRequestTempStorage().get(FoxyCommonConsts.SERVICE_EXTRA_RESPONSE_DATA_HEADER_NAME);
    }

    @PerformOnAnyDb
    protected boolean isSharedMailServerCfgValid() {
        synchronized (sharedServiceObj) {
            MailServerConfig cfg = sharedServiceObj.mailServerCfg;
            return cfg != null && !BaseUtils.isStrEmptyOrWhiteSpace(cfg.host)
                    && !BaseUtils.isStrEmptyOrWhiteSpace(cfg.user)
                    && !BaseUtils.isStrEmptyOrWhiteSpace(cfg.port);
        }
    }

    @PerformOnAnyDb
    protected void setSharedMailServerCfg(MailServerConfig cfg) {
        synchronized (sharedServiceObj) {
            sharedServiceObj.mailServerCfg = cfg;
        }
    }

    @PerformOnAnyDb
    protected MailServerConfig getSharedMailServerCfg() {
        synchronized (sharedServiceObj) {
            return sharedServiceObj.mailServerCfg;
        }
    }

    protected String combineSingleBiksDbForCurrentRequestAndLisaInstanceId(Integer instanceId) {
        return getSingleBiksDbForCurrentRequest() + "_" + instanceId;
    }

    protected String combineLisaInstanceIdAndUsername(Integer instanceId, String username) {
        return instanceId + "_" + username;
    }
}
