if OBJECT_ID (N'dbo.fn_ww_table_key_cols', N'FN') is not null
    drop function dbo.fn_ww_table_key_cols;
go

create function dbo.fn_ww_table_key_cols(@table_name sysname, @with_col_types int, @key_level int)
returns varchar(max)
as
begin
  declare @txt varchar(max) = ''

  declare @idx_name sysname = (select top 1 constraint_name from
  (
select distinct i.name as constraint_name,
  case when i.is_primary_key = 1 then 'pk' when i.is_unique_constraint = 1 then 'uk' else 'ux' end as constraint_kind
 from sys.tables t
inner join sys.schemas s on t.schema_id = s.schema_id
inner join sys.indexes i on i.object_id = t.object_id
where i.index_id > 0    
and i.type in (1, 2) -- clustered & nonclustered only
and i.is_disabled = 0
and i.is_hypothetical = 0
and (i.is_primary_key = 1 or @key_level > 0 and i.is_unique_constraint = 1 or @key_level > 1 and i.is_unique = 1)
and t.object_id = object_id(@table_name)
) x order by case constraint_kind when 'pk' then 0 when 'uk' then 1 else 2 end)
    
select 
  @txt = @txt +
  c.column_name + 
  case when @with_col_types <> 0 then
  ' ' + c.DATA_TYPE + case when c.character_maximum_length is null then ''
    when c.character_maximum_length < 0 then '(max)' else '(' + cast(c.CHARACTER_MAXIMUM_LENGTH as varchar(10)) + ')' end + 
    ' ' + case when c.is_nullable = 'NO' then 'not' else '' end + ' null'
  else '' end + ','
from 
  (
select s.name as table_schema, t.name as table_name, i.name as constraint_name, c.name as column_name,
  case when i.is_primary_key = 1 then 'pk' when i.is_unique_constraint = 1 then 'uk' else 'ux' end as constraint_kind,
  ic.key_ordinal as ordinal_position
 from sys.tables t
inner join sys.schemas s on t.schema_id = s.schema_id
inner join sys.indexes i on i.object_id = t.object_id
inner join sys.index_columns ic on ic.object_id = t.object_id and i.index_id = ic.index_id
	inner join sys.columns c on c.object_id = t.object_id and
		ic.column_id = c.column_id
where i.name = @idx_name
and i.index_id > 0    
and i.type in (1, 2) -- clustered & nonclustered only
and i.is_disabled = 0
and i.is_hypothetical = 0
and ic.key_ordinal > 0
and (i.is_primary_key = 1 or @key_level > 0 and i.is_unique_constraint = 1 or @key_level > 1 and i.is_unique = 1)
) tc inner join information_schema.columns c 
    on tc.table_schema = c.TABLE_SCHEMA
      and tc.TABLE_NAME = c.TABLE_NAME and tc.COLUMN_NAME = c.COLUMN_NAME
order by tc.ORDINAL_POSITION

  return case when @txt = '' then 
    '/* no ' + case @key_level when 0 then 'pk' when 1 then 'uk' else 'ux' end + ' on ' + @table_name + ' */'
     else substring(@txt, 1, len(@txt) - 1) end
end
go

/*
 select dbo.fn_ww_primary_key_cols('bik_help', 1)
 select dbo.fn_ww_primary_key_cols('bik_help', 0)
 select dbo.fn_ww_primary_key_cols('bik_node', 1)

 select dbo.fn_ww_table_key_cols('bik_node', 1, 0)
 select dbo.fn_ww_table_key_cols('bik_node', 1, 1)

 select dbo.fn_ww_table_key_cols('bik_joined_objs', 1, 1)
 select dbo.fn_ww_table_key_cols('bik_joined_objs', 1, 0)

 select * from (
 select name, dbo.fn_ww_primary_key_cols(name, 0) as pk_cols
 from sys.objects where name like 'bik[_]%'
 ) x where pk_cols like '/' + '*%'
 
 select * from information_schema.table_constraints
 select distinct constraint_type from information_schema.table_constraints
 
*/


if OBJECT_ID (N'dbo.fn_ww_fk_join_cond', N'FN') is not null
    drop function dbo.fn_ww_fk_join_cond;
go

create function dbo.fn_ww_fk_join_cond(@fk_name sysname, @ree_alias sysname, @red_alias sysname)
returns varchar(max)
as
begin
  declare @txt varchar(max) = ''
  declare @and_sep varchar(max) = ' and '

  select @txt = @txt + @and_sep + @ree_alias + '.' + name + '=' + @red_alias + '.' + ReferencedColumn
  from vw_ww_foreignkeys
  where ForeignKey_Name = @fk_name

  return substring(@txt, len(@and_sep) + 1, len(@txt))
end
go


/*

select dbo.fn_ww_fk_join_cond('FK_bik_joined_objs_dst_node_id', 'ree', 'red')

*/


if exists(select 1 from sys.objects where object_id = object_id('vw_ww_db_table_stats') and type = 'V')
drop view vw_ww_db_table_stats
go

create view vw_ww_db_table_stats as
SELECT 
    t.NAME AS TableName,
    p.rows AS RowCounts,
    SUM(a.total_pages) * 8 AS TotalSpaceKB, 
    SUM(a.used_pages) * 8 AS UsedSpaceKB, 
    (SUM(a.total_pages) - SUM(a.used_pages)) * 8 AS UnusedSpaceKB
FROM 
    sys.tables t
INNER JOIN      
    sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN 
    sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN 
    sys.allocation_units a ON p.partition_id = a.container_id
WHERE 
    t.NAME NOT LIKE 'dt%' 
    AND t.is_ms_shipped = 0
    AND i.OBJECT_ID > 255 
GROUP BY 
    t.Name, p.Rows
go




if OBJECT_ID (N'dbo.fn_ww_self_join_cond', N'FN') is not null
    drop function dbo.fn_ww_self_join_cond;
go

create function dbo.fn_ww_self_join_cond(@cols varchar(max), @ree_alias sysname, @red_alias sysname)
returns varchar(max)
as
begin
  declare @txt varchar(max) = ''
  declare @and_sep varchar(max) = ' and '

  select @txt = @txt + @and_sep + @ree_alias + '.' + str + '=' + @red_alias + '.' + str from dbo.fn_split_by_sep(@cols, ',', 3)
  order by idx
  
  return substring(@txt, len(@and_sep) + 1, len(@txt))
end
go


-- select dbo.fn_ww_self_join_cond('node_id', 'ree', 'tt')
-- select dbo.fn_ww_self_join_cond('node_id,user_id', 'ree', 'tt')
