/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.i18nsupport;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import java.util.Set;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.i18nsupport.I18nMessage;
import simplelib.i18nsupport.I18nMessagePlEn;
import simplelib.i18nsupport.ICurrentLocaleNameProvider;

/**
 *
 * @author pmielanczuk
 */
public class GWTCurrentLocaleNameProvider implements ICurrentLocaleNameProvider {

    private static boolean isFactorySet = false;
    private static String currentLocaleName;
    private static Set<String> commonSupportedLocaleNames = I18nMessagePlEn.localeNames;

    public static Set<String> getCommonSupportedLocaleNames() {
        return commonSupportedLocaleNames;
    }

    public static synchronized void setFactory() {
        if (!isFactorySet) {
            isFactorySet = true;
            I18nMessage.setCurrentLocaleNameProvider(new GWTCurrentLocaleNameProvider());
        }
    }

    public static String getCurrentLocaleNameStatic() {
        return currentLocaleName == null ? LocaleInfo.getCurrentLocale().getLocaleName() : currentLocaleName;
    }

    @Override
    public String getCurrentLocaleName() {
        return getCurrentLocaleNameStatic();
    }

    public static void setCurrentLocaleName(String localeName, Set<String> serverSupportedLocaleNames) {
        currentLocaleName = localeName;
        commonSupportedLocaleNames = I18nMessagePlEn.localeNames;
        if (!BaseUtils.isCollectionEmpty(serverSupportedLocaleNames)) {
            commonSupportedLocaleNames.retainAll(serverSupportedLocaleNames);
        }
    }
    private static final ServerSideI18nSupportingServiceAsync i18nSupportingService = GWT.create(ServerSideI18nSupportingService.class);

    static {
        ServiceDefTarget endpoint = (ServiceDefTarget) i18nSupportingService;
        endpoint.setServiceEntryPoint(/*"/" +*/FoxyClientSingletons.FILE_HANDLING_SERVLET_NAME);
    }

    @Deprecated
    public static void setupCurrentLocaleOnModuleLoad(ServerSideI18nSupportingServiceAsync optService) {
        setupCurrentLocaleOnModuleLoad(optService, null);
    }

    public static void setupCurrentLocaleOnModuleLoad(ServerSideI18nSupportingServiceAsync optService,
            final IContinuation optContAfterLocaleSet) {
        setupCurrentLocaleOnModuleLoad(optService, optContAfterLocaleSet, null, null);
    }

    public static void setupCurrentLocaleOnModuleLoad(ServerSideI18nSupportingServiceAsync optService,
            final IContinuation optContAfterLocaleSet,
            final IParametrizedContinuation<Throwable> optContOnSetLangFailure,
            final String optForcedLang) {

        String localeNameFromUrl;
        final Set<String> localeNames = I18nMessagePlEn.localeNames;

        if (optForcedLang == null) {
            String localeQueryParamName = LocaleInfo.getLocaleQueryParam();
            localeNameFromUrl = Window.Location.getParameter(localeQueryParamName);
        } else {
            localeNameFromUrl = optForcedLang;
        }

        // w urlu jest podany jakiś język, ale nie jest obsługiwany na kliencie
        // więc poprawiamy go pierwszym obsługiwanym
        if (!BaseUtils.isStrEmptyOrWhiteSpace(localeNameFromUrl) && !localeNames.contains(localeNameFromUrl)) {
            localeNameFromUrl = BaseUtils.safeGetFirstItem(localeNames, null);
        }

        final String localeNameFromUrlFixed
                = localeNames.contains(localeNameFromUrl)
                        ? localeNameFromUrl : BaseUtils.safeGetFirstItem(localeNames, "?");

        AsyncCallback<LocaleNegotiationResultBean> callback = new StandardAsyncCallback<LocaleNegotiationResultBean>() {
            @Override
            public void onSuccess(LocaleNegotiationResultBean result) {
                String suggestedClientLocaleName = result.clientLocaleName;
                if (optForcedLang == null) {
                    // co zwrócił serwer - czy coś co znamy? jeżeli nie, to bierzemy
                    // taki jak był w URLu po poprawce
                    if (!localeNames.contains(suggestedClientLocaleName)) {
                        suggestedClientLocaleName = localeNameFromUrlFixed;
                    }
                } else {
                    suggestedClientLocaleName = optForcedLang;
                }
                GWTCurrentLocaleNameProvider.setCurrentLocaleName(suggestedClientLocaleName,
                        result.serverSupportedLocaleNames);

                if (optContAfterLocaleSet != null) {
                    optContAfterLocaleSet.doIt();
                }
            }

            @Override
            public void onFailure(Throwable caught) {
                GWTCurrentLocaleNameProvider.setCurrentLocaleName(localeNameFromUrlFixed, null);
                if (optContOnSetLangFailure != null) {
                    optContOnSetLangFailure.doIt(caught);
                } else {
                    super.onFailure(caught);
                }
            }
        };

        if (optService == null) {
            optService = i18nSupportingService;
        }
        optService.setSessionLocaleName(localeNameFromUrl, localeNames, callback);
    }
}
