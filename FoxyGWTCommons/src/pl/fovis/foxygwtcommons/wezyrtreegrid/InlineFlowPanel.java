/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.wezyrtreegrid;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.InsertPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author wezyr
 */
public class InlineFlowPanel extends ComplexPanel implements InsertPanel.ForIsWidget {

    /**
     * Creates an empty flow panel.
     */
    public InlineFlowPanel() {
        setElement((Element) DOM.createSpan());
    }

    /**
     * Adds a new child widget to the panel.
     *
     * @param w the widget to be added
     */
    @Override
    @SuppressWarnings("deprecation")
    public void add(Widget w) {
        add(w, getElement());
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("clear not supported" /* I18N: no */ + "!");
    }

    @Override
    public void insert(IsWidget w, int beforeIndex) {
        insert(asWidgetOrNull(w), beforeIndex);
    }

    /**
     * Inserts a widget before the specified index.
     *
     * @param w the widget to be inserted
     * @param beforeIndex the index before which it will be inserted
     * @throws IndexOutOfBoundsException if <code>beforeIndex</code> is out of
     *           range
     */
    @Override
    @SuppressWarnings("deprecation")
    public void insert(Widget w, int beforeIndex) {
        insert(w, getElement(), beforeIndex, true);
    }
}
