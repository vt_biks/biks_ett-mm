/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.ICheckEnabledByDesign;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.NoteBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Pair;

/**
 *
 * @author młydkowski
 */
public class NoteWidgetFlat extends //EntityDetailsTabBase/* NoteWidgetBase /*extends EntityDetailsWidgetFlatBase*/
        EntityDetailsTabWithItemsInEditableGridBase<NoteBean> {

    public static final String NODE_ACTION_EDIT_OWN_COMMENT = "EditOwnComment";
    public static final String NODE_ACTION_EDIT_ANY_COMMENT = "EditAnyComment";
    public static final String NODE_ACTION_ADD_COMMENT = "AddComment";
    public static final Set<String> NODE_ACTIONS_COMMENTS = BaseUtils.paramsAsSet(/*NODE_ACTION_EDIT_OWN_COMMENT,*/NODE_ACTION_EDIT_ANY_COMMENT, NODE_ACTION_ADD_COMMENT);

    protected List<Pair<PushButton, Integer>> buttonsMap = new ArrayList<Pair<PushButton, Integer>>();
    private ScrollPanel scPanel = new ScrollPanel();
    public VerticalPanel innerNoteEntriesVp;
    protected VerticalPanel mainNoteVp = new VerticalPanel();
    protected VerticalPanel mainVp = new VerticalPanel();
    private VerticalPanel vp = new VerticalPanel();
    private FlowPanel addBtnNoteFp = new FlowPanel();
    private boolean normalPlainUser = true;

    public NoteWidgetFlat() {
        BIKClientSingletons.registerOptNodeActionCodes(NODE_ACTIONS_COMMENTS);
    }

    @Override
    public Widget buildWidgets() {
        mainNoteVp.clear();
        mainVp.clear();;
        mainVp.setStyleName("Notes" /* i18n: no:css-class */ + "-mainNoteVp");
        vp.clear();
        addBtnNoteFp.clear();
        scPanel.clear();
        mainNoteVp.setWidth("100%");
        vp.setWidth("100%");
        vp.addStyleName("obj" /* I18N: no */ + "-innerObjEntriesVp");
        mainNoteVp.addStyleName("RelatedObj");

        mainNoteVp.add(vp);
        addBtnNoteFp.add(addAddNoteButton());

        scPanel.add(mainNoteVp);
        scPanel.setHeight(BIKClientSingletons.ORIGINAL_EDP_BODY + "px");
//        scPanel.setHeight(BIKClientSingletons.ORIGINAL_EDP_BODY - (BIKClientSingletons.isUserLoggedIn() && !BIKClientSingletons.isEffectiveExpertLoggedIn() && widgetsActive ? 28 : 0) + "px");
        mainVp.add(scPanel);
        mainVp.add(addBtnNoteFp);

//        if (BIKClientSingletons.isLoggedUserAuthorOfTree(getData().node.treeId)
//                || BIKClientSingletons.isBranchAuthorLoggedin(getData().node.branchIds)
//                || BIKClientSingletons.isEffectiveExpertLoggedIn()) {
//            normalPlainUser = true;
//        }
        return mainVp;
    }

    protected PushButton createDeleteButton(final int noteId, int userId) {
        //ww: tu chyba brakuje sprawdzenia czy zalogowany jest autor komentarza
        // albo jest to admin / ekspert?

        PushButton removeButton = new PushButton(new Image("images/trash_gray.gif" /* I18N: no */), new Image("images/trash_red.png" /* I18N: no */));
        removeButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                new ConfirmDialog().buildAndShowDialog(I18n.czyNaPewnoUsunacKomentarz.get() /* I18N:  */ + "?", I18n.usun.get() /* I18N:  */, new IContinuation() {
                            public void doIt() {
                                BIKClientSingletons.getService().deleteBikNote(noteId, new StandardAsyncCallback<Void>("Failure on" /* I18N: no */ + " deleteComment") {
                                    public void onSuccess(Void result) {
                                        BIKClientSingletons.showInfo(I18n.usunietoKomentarz.get() /* I18N:  */);
                                        refreshAndRedisplayData();
                                    }
                                });
                            }
                        });
            }
        });
        removeButton.addStyleName("Notes" /* i18n: no:css-class */ + "-trashBtn");
        removeButton.setTitle(I18n.usun.get() /* I18N:  */);
        addToButtons(userId, removeButton);
        return removeButton;

    }

    protected PushButton createEditButton(int noteId, final NoteBean note) {
        PushButton editButton = new PushButton(new Image("images/edit_gray.png" /* I18N: no */), new Image("images/edit_yellow.png" /* I18N: no */));

        editButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                getForms().showNotesForm(nodeId, note);
            }
        });
        editButton.addStyleName("Note" /* I18N: no */ + "-editBtn");
        editButton.setTitle(I18n.edytuj.get() /* I18N:  */);

        addToButtons(note.userId, editButton);
        return editButton;
    }

    protected BikCustomButton addAddNoteButton() {
        actionBtn = new BikCustomButton(I18n.dodajKomentarz.get() /* I18N:  */, "Notes" /* i18n: no:css-class */ + "-addNoteEntry",
                new IContinuation() {
                    public void doIt() {
                        addNewNoteForm();
                    }
                });
        addWidgetVisibledByState(actionBtn);

//        actionBtn.setEnabled(
//                BIKClientSingletons.isNodeActionEnabledByCustomRightRoles(NODE_ACTION_ADD_COMMENT, getData().node.treeId, getData(),
//                        new ICheckEnabledByDesign() {
//
//                            @Override
//                            public boolean isEnabledByDesign() {
//                                return true;
//                            }
//                        })
//        );
        return actionBtn;
    }

    protected void setUpGridColumnsWidths(HTMLTable gp) {
        gp.setWidth("100%");
        gp.getColumnFormatter().setWidth(0, "37px");

//        gp.getColumnFormatter().setWidth(1, "100%");
//        gp.getColumnFormatter().setWidth(2, "10%");
        gp.getColumnFormatter().setWidth(3, "145px");
        gp.getColumnFormatter().setWidth(4, "26px");
        if (normalPlainUser) {
            gp.getColumnFormatter().setWidth(5, "26px");
        }
    }

    protected void addToButtons(int autorId, PushButton button) {
        buttonsMap.add(new Pair<PushButton, Integer>(button, autorId));
    }

    protected void addNewNoteForm() {
        // nowa notatka - później zostanie zmienione
        getForms().showNotesForm(nodeId, new NoteBean(-1));
    }

    @Override
    public void displayData() {
        vp.clear();

        if (BaseUtils.isCollectionEmpty(items)) {
            vp.add(zeroMsg(I18n.brakKomentarzy.get() /* I18N:  */));
            vp.setWidth("100%");
            mainNoteVp.setStyleName("Notes" /* i18n: no:css-class */ + "-mainNoteVp");
            mainVp.setStyleName("Joined" /* I18N: no */ + "-mainJoinedVp");
        } else {
            vp.add(buildGrid());
        }
    }

    protected Widget buildGrid() {
        FlexTable gp = new FlexTable();
        gp.setWidth("100%");
        gp.setStyleName("newNoteGrid");
        setUpGridColumnsWidths(gp);
        int row = 0;
        for (int i = 0; i < items.size(); i++) {
            final NoteBean note = items.get(i);
            HTML lab = new HTML("<b>" + BaseUtils.encodeForHTMLTag(note.title) + "</b> ");

            lab.setStyleName("Notes" /* i18n: no:css-class */ + "-noteTitle");

            FlowPanel oneNoteEntryFp = new FlowPanel();
            oneNoteEntryFp.setStyleName("Notes" /* i18n: no:css-class */ + "-oneNoteEntryFp");//strzaleczka

            HorizontalPanel oneNoteEntryHl = new HorizontalPanel();
            oneNoteEntryHl.setStyleName("Notes" /* i18n: no:css-class */ + "-oneNoteEntryHl");
            oneNoteEntryHl.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);

            FlowPanel innerOneNoteEntryFp = new FlowPanel();
            innerOneNoteEntryFp.setStyleName("Notes" /* i18n: no:css-class */ + "-innerOneNoteEntryFp");
            oneNoteEntryFp.add(innerOneNoteEntryFp);

            oneNoteEntryHl.add(lab);

            innerOneNoteEntryFp.add(lab);
            String body = note.body;
            if (!BaseUtils.isStrEmptyOrWhiteSpace(body)) {
                HTML labBody = new HTML();
                labBody.setStyleName("Notes" /* i18n: no:css-class */ + "-labBody");
                labBody.setHTML(BaseUtils.encodeForHTMLTag(body));
                innerOneNoteEntryFp.add(labBody);
            }

            gp.setWidget(row, 0, BIKClientSingletons.createAvatarImage(note.avatar, 32, 32, "images/user_icon.gif" /* I18N: no */, "RelatedObj-ObjIcon", null));
            gp.getCellFormatter().setVerticalAlignment(row, 0, HasVerticalAlignment.ALIGN_TOP);
            gp.getCellFormatter().setHorizontalAlignment(row, 0, HasHorizontalAlignment.ALIGN_LEFT);

            HorizontalPanel hp = new HorizontalPanel();
            hp.add(new HTML(BaseUtils.encodeForHTMLTag(note.authorName)));
            if (BIKClientSingletons.isStarredSystemUser(note.userId)) {
                hp.add(BIKClientSingletons.createIconImg("images/star.png" /* I18N: no */, "12px", "12px", null));
            }
            gp.setWidget(row, 2, hp);
            gp.setWidget(row, 1, oneNoteEntryFp);
            gp.setWidget(row, 3, createDateAdded(note.dateAdded));
            gp.setWidget(row, 4, createEditButton(nodeId, note));

            gp.getCellFormatter().setStyleName(row, 2, "bg" /* I18N: no */);
            gp.getCellFormatter().setStyleName(row, 3, "bg" /* I18N: no */);
            gp.getCellFormatter().setStyleName(row, 4, "bg" /* I18N: no */);

            gp.getCellFormatter().setVerticalAlignment(row, 2, HasVerticalAlignment.ALIGN_TOP);
            gp.getCellFormatter().setVerticalAlignment(row, 3, HasVerticalAlignment.ALIGN_TOP);
            gp.getCellFormatter().setVerticalAlignment(row, 4, HasVerticalAlignment.ALIGN_TOP);

            if (normalPlainUser) {
                gp.setWidget(row, 5, createDeleteButton(note.id, note.userId));
                gp.getCellFormatter().setStyleName(row, 5, "bg" /* I18N: no */);
                gp.getCellFormatter().setVerticalAlignment(row, 5, HasVerticalAlignment.ALIGN_TOP);
                gp = makeGridSpace(row, 6, gp);
            } else {
                gp = makeGridSpace(row, 5, gp);
            }
            row = row + 2;

            oneNoteEntryHl.setCellWidth(lab, "80%");

        }
        return gp;
    }

    @Override
    public void refreshButtons(final boolean isEditonEnabled) {
        final Integer currentUserId = BIKClientSingletons.getLoggedUserId();
//                BIKClientSingletons.getLoggedUserBean().userId;

        for (Pair<PushButton, Integer> p : buttonsMap) {
            PushButton b = p.v1;
            final int userId = p.v2;
            String actCode = BaseUtils.safeEquals(userId, currentUserId) ? NODE_ACTION_EDIT_OWN_COMMENT : NODE_ACTION_EDIT_ANY_COMMENT;
            b.setEnabled( //                    BIKClientSingletons.isSysAdminLoggedIn());
                    //ww->authors: przerabiam na eksperta

                    BIKClientSingletons.isNodeActionEnabledByCustomRightRoles(actCode, getData(), new ICheckEnabledByDesign() {

                        @Override
                        public boolean isEnabledByDesign() {
                            return isEditonEnabled && BaseUtils.safeEquals(userId, currentUserId)
                            || BIKClientSingletons.isEffectiveExpertLoggedIn();
                        }
                    })
            );
        }
    }

//    public boolean deprecatedIsEnabled(String code, int count) {
//        return !isInUserTree(code) && BIKClientSingletons.isUserLoggedIn();
//    }
    @Override
    protected List<NoteBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
//        return data.notes;
        return BaseUtils.isCollectionEmpty(items) ? new ArrayList<NoteBean>() : items;
    }

    @Override
    public String getCaption() {
        return BIKGWTConstants.MENU_NAME_COMMENTS;
    }

    @Override
    public boolean hasEditableControls() {
        return !isInUserTree(getNodeTreeCode());
    }

    @Override
    public boolean hasContent() {
        return getItemCount() > 0 && !isInUserTree(getNodeTreeCode());
    }

    @Override
    protected String getOptNodeActionCode() {
        return NODE_ACTION_ADD_COMMENT;
    }

    @Override
    protected boolean isEditionEnabledByDesign() {
        return widgetsActive && BIKClientSingletons.isUserLoggedIn();
    }

//    @Override
//    public boolean isEditonEnabled() {
//        //ww: tutaj edycja jest dopuszczalna dla każdego zalogowanego
//        // edycja rozumiana jako dodawanie nowego komentarza, nie mylić z edycją
//        // poszczególnych komentarzy i ich kasowaniem
//        return widgetsActive && BIKClientSingletons.isUserLoggedIn();
//    }
    @Override
    public String getTreeKindForSelected() {
        return null;
    }

    @Override
    public void optCallServiceToGetData(final IContinuation con) {
        BIKClientSingletons.getService().getNotes(nodeId, new AsyncCallback<List<NoteBean>>() {

            @Override
            public void onFailure(Throwable caught) {
                displayData();
            }

            @Override
            public void onSuccess(List<NoteBean> result) {
                items = result;
                con.doIt();
            }
        });
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
