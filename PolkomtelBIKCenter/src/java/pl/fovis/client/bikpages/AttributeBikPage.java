/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.AdminJoinedAttributeWidget;
import pl.fovis.client.bikwidgets.AutomaticFillAttributeWidget;
import pl.fovis.client.bikwidgets.CategorizedAddAttrDictWidget;
import pl.fovis.client.bikwidgets.CategorizedSystemAttrDictWidget;
import pl.fovis.client.bikwidgets.ManagmentAttrubuteWidget;
import pl.fovis.client.bikwidgets.TreeAndNodeKindTreeForAddAtrs;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import simplelib.IContinuation;

/**
 *
 * @author beata
 */
public class AttributeBikPage extends BikPageMinimalBase implements IBikPageForAppAdmin {

    protected FlowPanel right = new FlowPanel();
    protected ScrollPanel treeScrollPnl = new ScrollPanel();
    protected ScrollPanel attrScrollPnl = new ScrollPanel();
    protected ScrollPanel attrSysScroll = new ScrollPanel();
    protected ScrollPanel attrMangcroll = new ScrollPanel();
    protected TreeAndNodeKindTreeForAddAtrs treeForAddArts;
    protected CategorizedAddAttrDictWidget catAddAttrDicWid;
    protected CategorizedSystemAttrDictWidget catAddAttrBuiltDicWid;
    protected ManagmentAttrubuteWidget managmentAttrubuteWidget;
    protected AutomaticFillAttributeWidget automaticFillAttributeWidget;
    protected AdminJoinedAttributeWidget adminJoinedAttributeWid;
    protected TabPanel tabPanel;

    protected int optDiffHeight = !BIKClientSingletons.enableAttributeJoinedObjs() ? 0 : 40;

    @Override
    protected Widget buildWidgets() {

        if (!BIKClientSingletons.enableAttributeJoinedObjs()) {
            return buildInnerWidgetsObjectsAttrs();
        }

        TabPanel mainTb = new TabPanel();
        mainTb.setAnimationEnabled(true);
        mainTb.setHeight("100%");
        mainTb.setWidth("100%");
        mainTb.add(buildInnerWidgetsObjectsAttrs(), I18n.atrybutyObiektow.get());
        mainTb.add(buildInnerWidgetsJoinAttrs(), I18n.atrybutyPowiazan.get());
        mainTb.add(buildInnerWidgetsManagmentAttrs(), I18n.zarzadzanieAtrybutami.get());
        mainTb.add(buildnnerWidgetsAutomaticFillAttribute(), I18n.masoweWypelanianieAtrybutow.get());
        mainTb.selectTab(0);
        return mainTb;
    }

    protected Widget buildInnerWidgetsObjectsAttrs() {
        HorizontalPanel main = new HorizontalPanel();
        tabPanel = new TabPanel();
        tabPanel.setAnimationEnabled(true);
        tabPanel.setWidth("100%");
        FlowPanel left = new FlowPanel();
        left.setStyleName("EntityDetailsPane");
        GWTUtils.setStyleAttribute(left, "padding" /* I18N: no */, "4px");

        GWTUtils.setStyleAttribute(right, "padding" /* I18N: no */, "4px");
        right.setStyleName("EntityDetailsPane");

        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(left,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 8 - optDiffHeight);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(right,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 8 - optDiffHeight);

        main.add(left);

        main.add(right);

        main.setCellWidth(right, "100%");
        main.setCellHeight(left, "100%");
        main.setCellHeight(right, "100%");
//
//        HTML searchTabsLbl = new HTML("<h3><b>Atrybuty</b></h3>");
//        searchTabsLbl.addStyleName("searchPageBigTxt");
//
//        left.add(searchTabsLbl);

        left.add(treeScrollPnl);
        treeForAddArts = new TreeAndNodeKindTreeForAddAtrs();

        treeScrollPnl.add(treeForAddArts.buildWidgets());
        treeScrollPnl.setWidth("450px");

        catAddAttrDicWid = new CategorizedAddAttrDictWidget();
        catAddAttrBuiltDicWid = new CategorizedSystemAttrDictWidget();

//treeForAddArts.getCheckedNodeKindIdsOnTree()
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(treeScrollPnl,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 4 - optDiffHeight/*
         * - 28
         */);

        treeForAddArts.setOnSelectionChanged(new IContinuation() {
            public void doIt() {
                catAddAttrDicWid.recalcCheckBoxesByNodeKindIds(treeForAddArts.getCheckedNodeKindIdsOnTree());
                catAddAttrBuiltDicWid.recalcCheckBoxesByNodeKindIds(treeForAddArts.getCheckedNodeKindIdsOnTree());
            }
        });
        attrScrollPnl.add(catAddAttrDicWid.buildWidgets());
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(attrScrollPnl,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 50 - optDiffHeight//4
        /*
         * - 28
         */);
        attrSysScroll.add(catAddAttrBuiltDicWid.buildWidgets());
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(attrSysScroll,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 50 - optDiffHeight//4
        /*
         * - 28
         */);
        tabPanel.add(attrScrollPnl, I18n.atrybutyDodatkowe.get() /* I18N:  */);
        tabPanel.add(attrSysScroll/*catAddAttrBuiltDicWid.buildWidgets()*/, I18n.atrybutySystemowe.get() /* I18N:  */);

        tabPanel.selectTab(0);
        right.add(tabPanel);
//        right.add(catAddAttrDicWid.buildWidgets());
        catAddAttrDicWid.displayData();
        catAddAttrBuiltDicWid.displayData();

        return main;
    }

//    public String getCaptionDeprecated() {
//        return I18n.atrybuty.get() /* I18N:  */;
//    }
    public String getId() {
        return "admin:dict:attrDefs";
    }

    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
//        throw new UnsupportedOperationException("Not supported yet.");
        catAddAttrDicWid.displayData();
        catAddAttrBuiltDicWid.displayData();
    }

    protected Widget buildInnerWidgetsJoinAttrs() {
        adminJoinedAttributeWid = new AdminJoinedAttributeWidget();

        Widget main = adminJoinedAttributeWid.buildWidgets();
        adminJoinedAttributeWid.displayData();
        return main;
    }

    protected Widget buildInnerWidgetsManagmentAttrs() {
//        adminJoinedAttributeWid = new AdminJoinedAttributeWidget();
        managmentAttrubuteWidget = new ManagmentAttrubuteWidget();
        Widget main = managmentAttrubuteWidget.buildWidgets();
//        managmentAttrubuteWidget.refreshGrid();
        return main;
    }

    protected Widget buildnnerWidgetsAutomaticFillAttribute() {
        automaticFillAttributeWidget = new AutomaticFillAttributeWidget();
        Widget main = automaticFillAttributeWidget.buildWidgets();
        return main;
    }

}
