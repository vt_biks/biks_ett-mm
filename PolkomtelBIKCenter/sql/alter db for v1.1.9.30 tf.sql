﻿exec sp_check_version '1.1.9.30';
go

-- dodanie wczesniej usunietej procedury, poniewaz jest ona uzywana w pompce z Designera
-- teraz w niej wywolujemy nowa procedure
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_connections_for_universe_objects]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_connections_for_universe_objects
go

create procedure [dbo].[sp_insert_connections_for_universe_objects]
as
begin
	exec sp_insert_into_bik_joined_objs_universe_objects;
end
go

exec sp_update_version '1.1.9.30', '1.2';
go
