package pl.trzy0.foxy.commons;

public class MailServerConfig {

    public String host;
    public String port;
    public String user;
    public String password;
    public String fromAddress;
    public boolean ssl;

    public MailServerConfig() {
    }

    public MailServerConfig(String host, String port, String user, String password, String fromAddress, boolean ssl) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
        this.fromAddress = fromAddress;
        this.ssl = ssl;
    }

    @Override
    public String toString() {
        return "MailServerConfig{" + "host=" + host + ", port=" + port + ", user=" + user + ", password=" + password + ", fromAddress=" + fromAddress + ", ssl=" + ssl + '}';
    }
}
