/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

/**
 *
 * @author tflorczak
 */
public class ConnectionParametersProfileBean extends ParametersBaseBean {

    public String path;
    public String testPath;
    public String host;
    public String port;
    public String user;
    public String password;

}
