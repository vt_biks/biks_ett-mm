/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.common.TutorialBean;
import simplelib.IParametrizedContinuation;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import simplelib.IContinuation;

/**
 *
 * @author bfechner
 */
public class ViewTutorialInFullScreenDialog extends ViewMovieInFullScreenDialog {

    protected TutorialBean tb;
    protected IParametrizedContinuation<TutorialBean> saveCont;
    protected FlowPanel vp = new FlowPanel();

    @Override
    protected Widget getContentWidget() {
        vp.setStyleName("BizDef");

        setupMainContent();

        return vp;
    }

    @Override
    protected String getDialogCaption() {
        return "BI Knowledge System" /* I18N: no */ + " – " + tb.title;
    }

    public void buildAndShowDialog(TutorialBean tb, IParametrizedContinuation<TutorialBean> saveCont) {
        this.saveCont = saveCont;
        this.tb = tb;
        super.buildAndShowDialog();
    }

    @Override
    protected String getActionButtonCaption() {
        if (!BIKClientSingletons.isUserLoggedIn()) {
            return null;
        }
        return !tb.isReaded ? I18n.oznaczJakoPrzeczytane.get() /* I18N:  */ : I18n.oznaczJakoNiePrzeczytane.get() /* I18N:  */;
    }

    @Override
    protected boolean isKeyEscapeCloseWindow() {
        return false;
    }

    @Override
    protected void doAction() {
        if (/*!tb.isReaded &&*/BIKClientSingletons.isUserLoggedIn()) {
            BIKClientSingletons.getService().markTutorialAsRead(tb.id, new StandardAsyncCallback<Void>() {
                public void onSuccess(Void result) {
                    tb.isReaded = !tb.isReaded;
                    saveCont.doIt(tb);
                }
            });
        }
    }

    @Override
    protected void addAdditionalButtons(HorizontalPanel hp) {
        if (BIKClientSingletons.isAppAdminLoggedIn(BIKConstants.ACTION_TUTORIAL_EDITOR)) {
            hp.add(NewLookUtils.newCustomPushButton(I18n.edytuj.get() /* I18N:  */, new ClickHandler() {
                public void onClick(ClickEvent event) {
                    new EditTutorialDialog().buildAndShowDialog(tb, new IContinuation() {
                        public void doIt() {
                            BIKClientSingletons.getService().updateTutorial(tb.id, tb.title, tb.text, new StandardAsyncCallback<Void>() {
                                public void onSuccess(Void result) {
                                    BIKClientSingletons.showInfo(I18n.tutorialEntryUpdated.get() /* i18n:  */);
                                    updateDialogCaption();
                                    setupMainContent();
                                }
                            });
                        }
                    });
                }
            }));
        }
    }

    protected void setupMainContent() {
        String title = tb.title;
        String movie = tb.movie;

        vp.clear();

        boolean isMovie = movie != null;
        if (isMovie) {
            vp.add(new HTML("<h2>" + title + " - " + I18n.film.get() /* I18N:  */ + "</h2><br>"));
            vp.add(BIKClientSingletons.getMovie(movie, 1050, 585));
            vp.add(new HTML("<br>"));
        }
        vp.add(new HTML("<h2>" + title + (isMovie ? " - " + I18n.streszczenieFilmu.get() /* I18N:  */ + "</h2>" : "")));
        vp.add(new HTML("<br>" + tb.text));
    }
}
