    select bn.*,
    bnk.caption as node_kind_caption,
    bnk.code as node_kind_code,
    bnk.is_folder as is_folder,
    bt.name as tree_name, bt.tree_kind, bt.code as tree_code
    , case when exists 
        (select 1 from bik_node where is_deleted = 0 and parent_node_id = coalesce(bn.linked_node_id, bn.id)) then 0 else 1 end as has_no_children
    from bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    inner join bik_tree bt on bn.tree_id = bt.id

    where bn.is_deleted = 0 and 
     parent_node_id
    = 35360
    order by bn.name
    

    select *, case when exists 
        (select 1 from bik_node where is_deleted = 0 and parent_node_id = coalesce(bn.linked_node_id, bn.id)) then 0 else 1 end as has_no_children
    from 
    (select bn.*,
    bnk.caption as node_kind_caption,
    bnk.code as node_kind_code,
    bnk.is_folder as is_folder,
    bt.name as tree_name, bt.tree_kind, bt.code as tree_code    
    from bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    inner join bik_tree bt on bn.tree_id = bt.id
    where bn.is_deleted = 0 and 
     parent_node_id
    = 35360
    ) bn 
    order by bn.name
    
    select bn.*,
    bnk.caption as node_kind_caption,
    bnk.code as node_kind_code,
    bnk.is_folder as is_folder,
    bt.name as tree_name, bt.tree_kind, bt.code as tree_code    
    , case when exists 
        (select 1 from bik_node where is_deleted = 0 and parent_node_id = coalesce(bn.linked_node_id, bn.id)) then 0 else 1 end as has_no_children
    from bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    inner join bik_tree bt on bn.tree_id = bt.id
    where bn.is_deleted = 0 and 
     parent_node_id
    = 35360
    order by bn.name
