/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons.tags.htmltags;

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public interface ISimpleHtmlTag {
    public Collection<? extends ISimpleHtmlTag> getSubTags();

    public boolean isTextTag();

    public String getName();

    public Map<String, String> getAttrs();

    public boolean isClosedInPlace();

    public String getFileName();
}
