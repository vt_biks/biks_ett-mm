exec sp_check_version '1.7.z7.19';
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_add_node_kinds_to_tree_of_trees]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_add_node_kinds_to_tree_of_trees
go

create procedure [dbo].[sp_add_node_kinds_to_tree_of_trees](@treeCode varchar(max))
as
begin
	declare @tree_of_trees_id int = (select dbo.fn_tree_id_by_code('TreeOfTrees'))
    declare @tree_id int = (select dbo.fn_tree_id_by_code(@treeCode))
    declare @parent_node_id int = (select dbo.fn_node_id_by_tree_code_and_obj_id('TreeOfTrees', '$' + @treeCode))

    declare @menu_item_node_id int = dbo.fn_node_kind_id_by_code('MenuItem');
    
    insert into bik_node(parent_node_id, node_kind_id, tree_id, name, obj_id)
    select @parent_node_id, @menu_item_node_id, @tree_of_trees_id, '@', '&' + tmp.code
    from (select distinct nk.code from bik_node bn inner join bik_node_kind nk on bn.node_kind_id = nk.id where tree_id = @tree_id) tmp
    where not exists (select * from bik_node where parent_node_id = @parent_node_id and obj_id = '&' + tmp.code)
end    
go

declare c cursor for select code from bik_tree where code like 'FileSystem%';
open c;

declare @treeCode varchar(max);
fetch next from c into @treeCode
while @@fetch_status = 0 begin
	exec [dbo].[sp_add_node_kinds_to_tree_of_trees] @treeCode;

	fetch next from c into @treeCode;
end

close c;
deallocate c;
drop procedure [dbo].sp_add_node_kinds_to_tree_of_trees

exec sp_update_version '1.7.z7.19', '1.7.z7.20';
go