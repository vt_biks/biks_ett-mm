/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill.tags;

import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class ElseTag extends TagBase {

    //private IfTag prevIfTag;

    @Override
    public void internalRender(//IRenderContext
            IRenderCtx renderCtx) {
        if (!renderCtx.popIfCondVal()) {
            FoxyMillUtils.render(subTags, renderCtx);
        }
    }

    @Override
    protected void initSelf() {
        if (prevTag instanceof IfTag) {
            IfTag prevIfTag = (IfTag)prevTag;
            prevIfTag.followedByElse = true;
        } else {
            throw new LameRuntimeException("expected if tag before else tag");
        }
    }
}
