/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.genericimporter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pl.bssg.metadatapump.AdhocPartialBeanCollector;
import pl.bssg.metadatapump.AttributeUsageBean;
import pl.bssg.metadatapump.GenericNodeBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class AttributeUsageCalculator {

    public static final int PACKAGE_SIZE = 1000;
    public static final String ATTRIBUTE_USAGE_TREE_CODE = "AttributeUsage";
    public static final String TYPE_ATTRIBUTE_CATEGORY = "AttributeCategory";
    public static final String TYPE_ATTRIBUTE_NAME = "Attribute";
    public static final String TYPE_ATTRIBUTE_SINGLE_VALUE = "AttributeSingleValue";

    protected AdhocPartialBeanCollector<GenericNodeBean> beanCollector;
    protected INamedSqlsDAO<Object> adhocDao;
    protected Set<String> allObjIds = new HashSet<String>();

    public AttributeUsageCalculator(INamedSqlsDAO<Object> adhocDao) {
        this.adhocDao = adhocDao;
    }

    public Pair<Integer, String> startPump() {
        adhocDao.execNamedCommand("deleteFromAmgNodeBase");
        beanCollector = new AdhocPartialBeanCollector<GenericNodeBean>(PACKAGE_SIZE, adhocDao, "insertIntoGenericTable");
        List<AttributeUsageBean> tmp = adhocDao.createBeansFromNamedQry("getMultiOptionsAttributes", AttributeUsageBean.class);

        for (AttributeUsageBean bean : tmp) {
            String categoryName = bean.categoryName;
            addToBeanCollector(categoryName, null, TYPE_ATTRIBUTE_CATEGORY, categoryName);
            String attrNodeId = categoryName + "|" + bean.attrName;
            addToBeanCollector(attrNodeId, categoryName, TYPE_ATTRIBUTE_NAME, bean.attrName);
            if (bean.attrValue != null) {
                List<String> values = BaseUtils.splitBySep(bean.attrValue, ",");
                for (int i = 0; i < values.size(); i++) {
                    addToBeanCollector(attrNodeId + "|" + values.get(i), attrNodeId, TYPE_ATTRIBUTE_SINGLE_VALUE, values.get(i));
                }
            }
        }
        beanCollector.flush();
        // move to bik_node
        adhocDao.execNamedCommand("insertGenericNodesIntoBikNode", ATTRIBUTE_USAGE_TREE_CODE);

        return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, "Success!");
    }

    private void addToBeanCollector(String objId, String parentObjId, String nodeKindCode, String name) {
        if (allObjIds.contains(objId)) {
            return;
        }
        allObjIds.add(objId);
        beanCollector.add(new GenericNodeBean(BaseUtils.ensureObjIdEndWith(objId, "|"), BaseUtils.ensureObjIdEndWith(parentObjId, "|"), nodeKindCode, name, ""));
    }
}
