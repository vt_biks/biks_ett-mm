﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- Dorobienie automatycznych powiązań (nieusuwalnych)
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_check_version '1.6.y4';
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_user_in_node') and name = 'is_built_in')
begin
	alter table bik_user_in_node add is_built_in int not null default(0);
end;
go

declare @roleForNodeId int = (select id from bik_role_for_node where code = 'AuthorOfObject')

delete from bik_user_in_node where role_for_node_id = @roleForNodeId

insert into bik_user_in_node (user_id, node_id, role_for_node_id, inherit_to_descendants, is_auxiliary, is_built_in)
select us.id as user_node_id, ex.node_id as element_node_id, @roleForNodeId, 0, 0, 1
from bik_sapbo_extradata as ex
inner join bik_node as bn on ex.node_id = bn.id and bn.is_deleted = 0
inner join bik_system_user as sus on coalesce(ex.author, ex.owner) = sus.login_name
inner join bik_user as us on sus.user_id = us.id
left join bik_user_in_node as uin on uin.user_id = us.id and uin.node_id = ex.node_id and uin.role_for_node_id = @roleForNodeId
where uin.id is null
    
insert into bik_user_in_node (user_id, node_id, role_for_node_id, inherit_to_descendants, is_auxiliary, is_built_in)
select us.id as user_node_id, ex.node_id as element_node_id, @roleForNodeId, 0, 0, 1
from bik_confluence_extradata ex
inner join bik_system_user as sus on ex.author = sus.login_name
inner join bik_user as us on sus.user_id = us.id
left join bik_user_in_node as uin on uin.user_id = us.id and uin.node_id = ex.node_id and uin.role_for_node_id = @roleForNodeId
where uin.id is null


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_confluence_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_confluence_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_confluence_connections]
as
begin

	declare @roleForNodeId int = (select id from bik_role_for_node where code = 'AuthorOfObject')
    declare @roleNode int = (select node_id from bik_role_for_node where id = @roleForNodeId)
    declare @userNodeKindId int = dbo.fn_node_kind_id_by_code('User')
    declare @userTreeId int = dbo.fn_tree_id_by_code('UserRoles')

    insert into bik_user_in_node (user_id, node_id, role_for_node_id, inherit_to_descendants, is_auxiliary, is_built_in)
    select us.id as user_node_id, ex.node_id as element_node_id, @roleForNodeId, 0, 0, 1
    from bik_confluence_extradata ex
    inner join bik_system_user as sus on ex.author = sus.login_name
    inner join bik_user as us on sus.user_id = us.id
    left join bik_user_in_node as uin on uin.user_id = us.id and uin.node_id = ex.node_id and uin.role_for_node_id = @roleForNodeId
    where uin.id is null

    insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id)
    select distinct @roleNode, @userNodeKindId, ubn.name, @userTreeId, ubn.id
    from bik_confluence_extradata ex
    inner join bik_system_user as sus on ex.author = sus.login_name
    inner join bik_user as us on sus.user_id = us.id
    inner join bik_node as ubn on ubn.id = us.node_id and ubn.is_deleted = 0
    left join bik_node as bn on bn.parent_node_id = @roleNode and bn.linked_node_id = ubn.id and bn.is_deleted = 0
    where bn.id is null

    exec sp_node_init_branch_id  null, @roleNode

end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_tree_icon') and type in (N'U'))
begin
	create table bik_tree_icon (
		id int not null identity(1,1) primary key,
		tree_id int not null references bik_tree(id),
		node_kind_id int not null references bik_node_kind(id),
		icon varchar(512) not null,
		constraint UQ_bik_tree_ikon_tree_id_node_kind_id unique (tree_id, node_kind_id)
	);
end;
go



exec sp_update_version '1.6.y4', '1.6.y4.1';
go
