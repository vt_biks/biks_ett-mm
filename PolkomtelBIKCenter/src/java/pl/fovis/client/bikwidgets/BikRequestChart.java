/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.googlecode.gchart.client.GChart;
import java.util.Date;
import java.util.List;
import pl.fovis.common.dqm.DataQualityRequestBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.SimpleDateUtils;

/**
 *
 * @author beata
 */
public final class BikRequestChart extends GChart {

    protected double format(Date data) {
        String a = data.toString().substring(0, 10);
        DateTimeFormat dateFormatter = DateTimeFormat.getFormat("yyyy-MM-dd");
        return dateFormatter.parse(a).getTime();
    }

    public BikRequestChart(Double errorThreshold, List<DataQualityRequestBean> requests) {
        setChartTitle("<b>35 " + I18n.dniOdOstatniegoWykonania.get() /* I18N:  */ + "</b>");
        setStyleName("dqc-chart");
        setChartSize(550, 160);

        getXAxis().setAxisLabel(I18n.wykonania.get() /* I18N:  */ + " ");
        getXAxis().setTickLabelFormat("=(" + "Date)dd.MM.yyyy" /* I18N: no */ + " HH:mm");

        Date nextDay = SimpleDateUtils.addMillis(requests.get(0).startTimestamp, 1000 * 60 * 60 * 24);
        Date nextDateWithoutHours = SimpleDateUtils.newDate(SimpleDateUtils.getYear(nextDay), SimpleDateUtils.getMonth(nextDay), SimpleDateUtils.getDay(nextDay));
        Date twoWeeksAgo = new Date(nextDateWithoutHours.getTime() - ((long) (1000 * 60 * 60 * 24) * 35));
        getXAxis().setAxisMin(twoWeeksAgo.getTime());
        getXAxis().setAxisMax(nextDateWithoutHours.getTime());

        getXAxis().setTickCount(8);
        getYAxis().setAxisLabel(I18n.blad.get() /* I18N:  */ + "(%)");
        getYAxis().setTickLabelFontSize(9);
        getXAxis().setTickLabelFontSize(9);

        // requests for test
        addCurve();
        getCurve().getSymbol().setSymbolType(GChart.SymbolType.LINE);
        getCurve().getSymbol().setFillThickness(2);
        getCurve().getSymbol().setFillSpacing(1);
        getCurve().getSymbol().setBackgroundColor("blue" /* I18N: no */);

//        int requestsSize = requests.size();
        for (DataQualityRequestBean request : requests) {
            if (request.errorCount != null && request.caseCount != null && request.caseCount != 0 && request.startTimestamp.getTime() - twoWeeksAgo.getTime() > 0) {
                getCurve().addPoint(request.startTimestamp.getTime(), 100 * (double) request.errorCount / (double) request.caseCount);
            } else if (request.caseCount != null && request.caseCount == 0 && request.startTimestamp.getTime() - twoWeeksAgo.getTime() > 0) {
                getCurve().addPoint(request.startTimestamp.getTime(), 0);
            }
        }
//        for (int i = 0; i < requestsSize; i++) {
//
//            if (requests.get(i).errorCount != null && requests.get(i).caseCount != null && requests.get(i).caseCount != 0) {
//
//                if (requests.get(i).startTimestamp.getTime() - twoWeeksAgo.getTime() > 0) {
//                    getCurve().addPoint(requests.get(i).startTimestamp.getTime(), 100 * (double) requests.get(i).errorCount / (double) requests.get(i).caseCount);
//
//                }
//            }
//        }
        getCurve().setLegendLabel(I18n.errorCountDivCaseCountShort.get()/*"Error count / Case count"*/ /* I18N: no */);
        setLegendLocation(GChart.LegendLocation.INSIDE_TOPRIGHT);
        setLegendYShift(50);

        // error treshold
        addCurve();
        getCurve().getSymbol().setSymbolType(GChart.SymbolType.LINE);
        getCurve().getSymbol().setFillThickness(2);
        getCurve().getSymbol().setFillSpacing(1);
        getCurve().getSymbol().setBackgroundColor(
                getCurve().getSymbol().getBorderColor());

        for (DataQualityRequestBean request : requests) {
            if (request.startTimestamp.getTime() - twoWeeksAgo.getTime() > 0) {
                getCurve().addPoint(request.startTimestamp.getTime(), errorThreshold * 100);
            }
        }
//        for (int i = 0; i < requestsSize; i++) {
//            if (requests.get(i).startTimestamp.getTime() - twoWeeksAgo.getTime() > 0) {
//                getCurve().addPoint(requests.get(i).startTimestamp.getTime(), errorThreshold * 100);
//            }
//        }
        getCurve().setLegendLabel(I18n.progBledu.get() /*"Error Threshold"*/ /* I18N: no */);
    }
}
