/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.client;

import com.google.gwt.user.client.rpc.RemoteService;
import java.util.Map;
import pl.bssg.biks.adm.common.AuthenticationDataBean;
import pl.bssg.biks.adm.common.MultiInfoBean;
import pl.bssg.gwtservlet.DisableCallLoggingForServiceImplMethod;
import simplelib.LameRuntimeException;

/**
 *
 * @author pmielanczuk
 */
public interface AppAndDBVerInfoService extends RemoteService {

//    public Map<String, Set<String>> checkSqlFilesDir(Map<String, String> dirs);
    public MultiInfoBean getInfo(AuthenticationDataBean adb,
            String optDBInstanceNameForLogs, Integer minLogId,
            Map<String, String> sqlFilesDirsToCheck) throws LameRuntimeException;

//    public List<DbauLogBean> getLogMsgs(AuthenticationDataBean adb, int procId, Integer minLogId);
    public int runDbUpgradeProcess(AuthenticationDataBean adb, String dbInstanceName, String dbLogin, String dbPass, String sqlFilesDir);

    @DisableCallLoggingForServiceImplMethod
    public void keepSessionAlive();
}
