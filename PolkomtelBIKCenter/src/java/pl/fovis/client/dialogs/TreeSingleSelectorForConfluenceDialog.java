/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.bssg.metadatapump.common.ConfluenceObjectBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class TreeSingleSelectorForConfluenceDialog extends TreeSelectorForConfluenceDialog {

    protected IParametrizedContinuation<ConfluenceObjectBean> saveContForConfluenceSingleRow;

    @Override
    protected boolean isInMultiSelectMode() {
        return false;
    }

    @Override
    protected void doAction() {
//        BIKClientSingletons.showInfo(I18n.zapisywaniePowiazanProszeCzekac.get() /* I18N:  */ + "...");
        saveContForConfluenceSingleRow.doIt(selectedNode);
    }

    public void buildAndShowDialogForSingleMode(IParametrizedContinuation<ConfluenceObjectBean> saveCont) {
        this.saveContForConfluenceSingleRow = saveCont;
        super.buildAndShowDialog(null, null, null);
    }

    @Override
    protected boolean isNodeSelectable(ConfluenceObjectBean node) {
        return node.kind.equals(BIKConstants.NODE_KIND_CONFLUENCE_PAGE);
    }

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzMiejsceDodaniaArtykulu.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.wybierz.get() /* I18N:  */;
    }

    @Override
    protected String getHeadLblPrefixMsg() {
        return I18n.obiektZostanieDodanyWBIKSPod.get();
    }
}
