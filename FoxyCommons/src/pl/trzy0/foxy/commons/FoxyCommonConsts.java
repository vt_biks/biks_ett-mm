/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

/**
 *
 * @author wezyr
 */
public interface FoxyCommonConsts {

    //ww: separator oddzielający wersję główną od pomocniczej
    // np. 1.1.6.4 - główna (bez pomocniczej)
    // 1.1.6.4#1 - główna to 1.1.6.4, pomocnicza 1 itp.
    public static final String MAJOR_MINOR_VERSION_SEP = "#";
    //---
//    public static final boolean enablePumpToServerLogAppender = true;
    //---
    public static final String LAQUO_STR = "«";
    public static final String RAQUO_STR = "»";
    public static final String RAQUO_STR_SPACED = " " + RAQUO_STR + " ";
    //---
    //ww: można nadpisać, więc nie jest final
    public static String CLIENT_FILE_NAME_SERVLET_PARAM_NAME = "cfn";
    //---
    public static final String REQ_EXTRA_PARAM_PREFIX = "foxy_r_";
    //---
    public static final String REQUEST_PARAM_NAME_FOR_LOCALE_NAME = "locale";
    //---
    public static final String SERVICE_EXTRA_RESPONSE_DATA_HEADER_NAME = "_ServiceExtraResponseData_";
    public static final String FOXY_APP_INSTANCE_ID_HEADER_NAME = "_FoxyAppInstanceId_";
    public static final String FOXY_LOGGED_USER_NAME_HEADER_NAME = "_FoxyLoggedUserName_";
    public static final String FOXY_REQUEST_TIMING_AFTER_COMMIT_TIMESTAMP_HEADER_NAME = "_foxy_request_timing_after_commit_timestamp_";
    //---
    public static final String DIAG_MSG_KIND$GLOBAL_UNCAUGHT_EXCEPTIONS = "globalUncaughtExceptions";
}
