/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.PushButton;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.SystemUserGroupWidget;
import pl.fovis.client.dialogs.AddSystemUserGroupDialog;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class SystemUserGroupPage extends SystemPage {

    protected void openAddRoleOrGroupDialog() {
        new AddSystemUserGroupDialog().buildAndShowDialog(null, I18n.dodajGrupe.get(), new IParametrizedContinuation<String>() {

            @Override
            public void doIt(String param) {

                BIKClientSingletons.getService().addSystemUserGroup(param, null, new StandardAsyncCallback<Integer>() {

                    @Override
                    public void onSuccess(Integer result) {
                        BIKClientSingletons.showInfo(result == 1 ? I18n.grupaDodana.get() : I18n.grupaIstnieje.get() /* I18N:  */);
                        show();
                    }
                });
            }
        });
    }

    protected void show() {
        dataFetchBroker.dataFetchStarting(IPageDataFetchBroker.TreeDataRequestKind.Full,
                BIKClientSingletons.getService().getGroupRootObjects(
                        new TreeDataFetchAsyncCallback<List<SystemUserGroupBean>>(null, dataFetchBroker) {
                            @Override
                            public void doOnSuccess(List<SystemUserGroupBean> t) {

                                if (t == null || t.isEmpty()) {
                                    rightPanel.clear();
                                    ///Window.alert(t.size()+"");
                                }

                                treeWidget.initWithFullData(t, true, null, null);
                                treeWidget.getTreeGrid().selectFirstRecord();
                            }
                        }
                ), null);
    }

    @Override
    public String getId() {
        return "admin:dict:sysGroupUsers";
    }

    protected void setTitleToAddBtn(PushButton addButton) {
        addButton.setTitle(I18n.dodajGrupe.get());
    }

    public void populateRightPanelWithGroup(final SystemUserGroupBean result) {
        rightPanel.clear();
        rightPanel.add(new SystemUserGroupWidget().buildAndShowDialog(result));
    }
}
