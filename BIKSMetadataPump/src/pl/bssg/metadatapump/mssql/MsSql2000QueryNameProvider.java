/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.mssql;

/**
 *
 * @author tflorczak
 */
public class MsSql2000QueryNameProvider implements IMsSqlQueryNameProvider {

    @Override
    public String getDatabases() {
        return "getMSSQL2000Databases";
    }

    @Override
    public String getExtendedProperties() {
        return "getMSSQL2000DatabaseEXProp";
    }

    @Override
    public String getObjects() {
        return "getMSSQL2000Tables";
    }

    @Override
    public String getObjectEXProp() {
        return "getMSSQL2000ObjectEXProp";
    }

    @Override
    public String getColumns() {
        return "getMSSQL2000Columns";
    }

    @Override
    public String getIndexes() {
        return "getMSSQL2000Indexes";
    }

    @Override
    public String getForeignKeys() {
        return "getMSSQL2000ForeignKeys";
    }

    @Override
    public String getDependencies() {
        return "getMSSQL2000Dependencies";
    }

    @Override
    public String getGrants() {
        return "getMSSQL2000Grants";
    }
}
