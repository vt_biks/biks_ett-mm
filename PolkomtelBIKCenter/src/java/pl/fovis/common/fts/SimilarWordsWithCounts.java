/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.fts;

import java.io.Serializable;

/**
 *
 * @author pmielanczuk
 */
public class SimilarWordsWithCounts implements Serializable {

    public String word;
    public int usageInObjCount;
    public double weight;
}
