package pl.fovis.server.lucene;

import java.io.Serializable;

/**
 *
 * @author AMamcarz
 */
public class NodeIndexingTaskData implements Serializable //extends SearchResult
{

    public static enum TaskKind {

        AddUpdateOrDelete, DeleteTree
    };
    public TaskKind kind = TaskKind.AddUpdateOrDelete;
    public int nodeId;
    public boolean isDeleted;
    public String name;
    public String description;
    public String keyWords;
    public String nodeKindCode;
    public String nodeKindCaption;
    public String treeName;
    public String treeCode;
    public int indexVer;
    public Float rangeInSearch;
}
