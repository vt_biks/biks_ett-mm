/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import java.util.Map;
import pl.fovis.common.MultixReturnMessage;
import pl.fovis.common.MultixStatisticsBean;
import pl.fovis.common.StartupDataBean;
import pl.fovis.common.TokenType;
import pl.fovis.common.UserRegistrationDataBean;
import pl.fovis.common.mltx.DatabaseListBoxData;
import pl.fovis.common.mltx.IdName;
import pl.fovis.common.mltx.InvitationResult;
import pl.fovis.common.mltx.MultiRegistrationBean;
import pl.fovis.common.mltx.MultiXDatabaseTemplateBean;
import pl.fovis.common.mltx.MultixStatisticsAuthenticationBean;

/**
 *
 * @author pku
 */
public interface MultiKSServiceAsync {

    public void getStartupData(AsyncCallback<StartupDataBean> asyncCallback);

    public void sendConfirmationEmail(MultiRegistrationBean mrb, String localAccessUrl, AsyncCallback<String> asyncCallback);

    public void sendInvitationEmail(String email, String role, String localAccessUrl, AsyncCallback<String> asyncCallback);

    public void verifyTokenAndCreateDB(UserRegistrationDataBean bean, Map<Integer, String> map, AsyncCallback<String> asyncCallback);

    public void getDatabaseTemplates(AsyncCallback<List<MultiXDatabaseTemplateBean>> asyncCallback);

    public void resetDB(boolean preserveInvited, AsyncCallback<String> standardAsyncCallback);

    public void getAvailableDatabases(AsyncCallback<DatabaseListBoxData> asyncCallback);

    public void verifyToken(String token, TokenType tokenType, AsyncCallback<MultixReturnMessage> asyncCallback);

    public void getMultiXStatistics(MultixStatisticsAuthenticationBean msab, AsyncCallback<List<MultixStatisticsBean>> asyncCallback);

    public void sendResetPassword(String email, String localAccessUrl, AsyncCallback<String> asyncCallback);

    public void resetPassword(String token, String password, AsyncCallback<String> asyncCallback);

    public void verifyInvitationTokenAndCreateAccount(UserRegistrationDataBean registrationData, Map<Integer, String> map, AsyncCallback<String> standardAsyncCallback);

    public void inspectUser(String token, AsyncCallback<InvitationResult> standardAsyncCallback);

    public void getQuestions(AsyncCallback<List<IdName>> asyncCallback);

    public void authenticateUserForMultixStatistics(MultixStatisticsAuthenticationBean msab, AsyncCallback<Boolean> asyncCallback);
}
