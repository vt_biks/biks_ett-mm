/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author tflorczak
 */
public class LinkedAlsoBean implements Serializable {

    public int linkedNodeId;
    public String caption;
    public List<String> ancestorNodePathByNodeId;
    public String branchIds;
    public String treeCode;
    public String icon;
}
