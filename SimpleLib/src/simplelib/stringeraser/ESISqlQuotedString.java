/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.stringeraser;

/**
 *
 * @author pmielanczuk
 */
public class ESISqlQuotedString extends ESISqlPrefixSuffixDelimitedString {

    public ESISqlQuotedString(char quoteChar) {
        super("" + quoteChar, "" + quoteChar);
    }

    @Override
    protected int findAfterEscapePos(String content, int suffixPos) {
        int len = content.length();
        if (suffixPos + 1 == len || content.charAt(suffixPos + 1) != prefix.charAt(0)) {
            return -1;
        }
        return suffixPos + 2;
    }
}
