/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.oracle;

/**
 *
 * @author tflorczak
 */
public class OracleProcedureBean {

    public int id;
    public String name;
    public String owner;
    public String type;
    public Integer line;
    public String text;
    public String uniqueProcedureName;

    public OracleProcedureBean() {
    }

    public OracleProcedureBean(int id, String name, String owner, String type, Integer line, String text) {
        this.id = id;
        this.name = name;
        this.owner = owner;
        this.type = type;
        this.line = line;
        this.text = text;
        this.uniqueProcedureName = owner + name;
    }
}
