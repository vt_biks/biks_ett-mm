/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.mssql;

/**
 *
 * @author tflorczak
 */
public class MsSqlColumnBean {

    public String name;
    public String owner;
    public String tableName;
    public String databaseName;
    public String serverName;
    public Integer columnId;
    public String description;
    public Boolean isNullable;
    public String type;
//    public Integer size;
    public String defaultField;
    public Long seedValue;
    public Long incrementValue;
    // technical
    public String uniqueName;
    public String branchName;
    public String parentBranchName;

    public MsSqlColumnBean() {
    }
}
