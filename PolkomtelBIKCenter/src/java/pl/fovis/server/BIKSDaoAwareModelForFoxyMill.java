/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.util.HashSet;
import java.util.Set;
import pl.fovis.common.BIKRightRoles;
import pl.fovis.common.SystemUserBean;
import pl.trzy0.foxy.serverlogic.FoxyMillDaoAwareModel;
import simplelib.RawString;

/**
 *
 * @author tflorczak
 */
public class BIKSDaoAwareModelForFoxyMill extends FoxyMillDaoAwareModel {

    protected BOXIServiceImpl boxiService;
    protected SystemUserBean artificialAdmin;

    public BIKSDaoAwareModelForFoxyMill(BOXIServiceImpl boxiService) {
        super();
        this.boxiService = boxiService;
    }

    public boolean isShowFullPathInJoinedObjsPane() {
        return boxiService.parseAppPropValueToBoolean("showFullPathInJoinedObjsPane");
    }

    public Integer getLoggedUserId() {
        Integer userId = boxiService.getLoggedUserId();
        if (userId == null && boxiService.getNDroolsInstance() > 0) {
            userId = boxiService.getLastLoggecUserId();
            if (userId == null || userId < 0) {
                userId = boxiService.getArtificialAdminId();
            }
        }
        return userId;
    }

    public Set<Integer> getTreeIdsForCustomRightRoles() {
        SystemUserBean sysUser = boxiService.getLoggedUserBean();
        if (boxiService.getNDroolsInstance() > 0) {
//            //ct: brzydki hack
            sysUser = new SystemUserBean();
//            Integer lastLoggedUserId = boxiService.getLastLoggecUserId();
////            System.out.println("lastLoggedUserId=" + BaseUtils.safeToString(lastLoggedUserId));
//            if (lastLoggedUserId != null && lastLoggedUserId > 0) {
//                sysUser.userRightRoleCodes = boxiService.getRightRolesForSystemUserByUserId(lastLoggedUserId);
//            } else {
            sysUser.userRightRoleCodes = new HashSet<String>();
            sysUser.userRightRoleCodes.add("AppAdmin");
            sysUser.userRightRoleCodes.add("Administrator");
//            }
        } else {
            if (sysUser != null) {
                sysUser.userRightRoleCodes = boxiService.getRightRolesForSystemUserByUserId(sysUser.id);
            }
        }
        return BIKRightRoles.isSysUserEffectiveAppAdmin(sysUser) ? null : boxiService.getTreeIdsForCustomRightRoles();
    }

    public Integer getNodeFilteringActionIdForCurrentRequest() {
        return boxiService.getNodeFilteringActionIdForCurrentRequest();
    }

    public boolean isSysUserEffectiveExpert() {
        return BIKRightRoles.isSysUserEffectiveExpert(boxiService.getLoggedUserBean());
    }

    public int getMaxDataQualityLastRequests() {
        return boxiService.getMaxDataQualityLastRequests();
    }

    public RawString deleteNodes(Iterable<Integer> ids) {
//        boxiService.registerChangeInNodes(ids);
        return new RawString("");
    }

    public RawString deleteNode(Integer id) {
//        boxiService.registerChangeInNode(id);
        return new RawString("");
    }

    public boolean isRecalculateCustomRightRoleRutsEnabled() {
        return boxiService.isRecalculateCustomRightRoleRutsEnabled();
    }
}
