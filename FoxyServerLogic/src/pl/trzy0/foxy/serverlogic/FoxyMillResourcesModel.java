/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

/**
 *
 * @author wezyr
 */
public class FoxyMillResourcesModel extends FoxyMillResourcesModelBase {

    private String clientLang;

    public FoxyMillResourcesModel(IApplicationEnvironment appEnv, String clientLang) {
        super(appEnv);
        this.clientLang = clientLang;
    }

    @Override
    public String getClientLang() {
        return clientLang;
    }
}
