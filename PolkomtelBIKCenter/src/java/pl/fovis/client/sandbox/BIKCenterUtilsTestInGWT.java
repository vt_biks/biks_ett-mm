/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.sandbox;

import com.google.gwt.user.client.Window;
import pl.fovis.common.BIKCenterUtilsTestBase;
import pl.fovis.foxygwtcommons.JavaScriptStringComparatorFactory;
import pl.fovis.foxygwtcommons.JavaScriptRegExpMatcherFactory;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public class BIKCenterUtilsTestInGWT extends BIKCenterUtilsTestBase {

    static {
        JavaScriptRegExpMatcherFactory.setFactory();
        JavaScriptStringComparatorFactory.setFactory();
    }
    protected boolean failed;

    @Override
    protected void proceedWithCatched(RuntimeException ex) {
        failed = true;
        Window.alert("ERROR in test" /* I18N:  */ + " [BIKCenterUtilsTestInGWT]: " + ex.getMessage() + "\n\n" + "But please do not worry, cause this app should work quite properly (kind of" /* I18N:  */ + ")...\n\n;-)");
    }

    public void testIt() {
        innerTestExtractNodeIdsFromHtml();
        innerTestCompareValues();
        innerTestRemoveHrefsFromHtml();

        if (!failed) {
            Window.alert("OK, testing [BIKCenterUtilsTestInGWT]: PASSED" /* I18N:  */ + "!");
        }
    }
}
