/*
 * StrUtilsTest.java
 * JUnit based test
 *
 * Created on 21 wrzesie� 2007, 11:56
 */

package commonlib;

import simplelib.StrUtils;
import junit.framework.*;

/**
 *
 * @author wezyr
 */
public class StrUtilsTest extends TestCase {
    
    public StrUtilsTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
    }

    protected void tearDown() throws Exception {
    }

    /**
     * Test of leftSubstring method, of class commonlib.StrUtils.
     */
    public void testLeftSubstring() {
        assertEquals("ala", StrUtils.leftSubstring("ala ma kota", 3, 3, false));
        assertEquals("la ", StrUtils.leftSubstring("ala ma kota", 3, 3, true));
        assertEquals("ala ma kota", StrUtils.leftSubstring("ala ma kota", 11, 11, false));
        assertEquals("ala ma kota", StrUtils.leftSubstring("ala ma kota", 11, 111, false));
        assertEquals("ala ma kota", StrUtils.leftSubstring("ala ma kota", 10, 111, true));
        assertEquals("ala ma kota", StrUtils.leftSubstring("ala ma kota", 10, 11, true));
        assertEquals("", StrUtils.leftSubstring("ala ma kota", 7, 0, true));
        assertEquals("", StrUtils.leftSubstring("ala ma kota", 7, 0, false));
        assertEquals("ala ma", StrUtils.leftSubstring("ala ma kota", 6, 11, false));
        assertEquals("ala ma ", StrUtils.leftSubstring("ala ma kota", 6, 11, true));
        assertEquals(null, StrUtils.capitalize(null));
        assertEquals("", StrUtils.capitalize(""));
        assertEquals("A", StrUtils.capitalize("a"));
        assertEquals("Tralalala", StrUtils.capitalize("Tralalala"));        
        assertEquals("Smok wawelski", StrUtils.capitalize("smok wawelski"));        
    }
}
