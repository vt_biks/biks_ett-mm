/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class JsonReader {

    private static ILameLogger logger = LameLoggerFactory.getLogger(JsonReader.class);

    public static Map<String, Object> safeTryReadMap(String jsonString) {
        Map<String, Object> map = null;

        try {
            map = readMap(jsonString);
        } catch (Exception ex) {
            logger.warn("cannot read map: \"" + jsonString + "\"", ex);
        }

        if (map == null) {
            map = new HashMap<String, Object>();
        }

        return map;
    }

    public static MapHelper<String> safeTryReadMapHelper(String jsonString) {
        return new MapHelper<String>(safeTryReadMap(jsonString));
    }

    public static MapHelper<String> safeReadMapHelper(String jsonString) {
        Map<String, Object> map = readMap(jsonString);
        return new MapHelper<String>(map);
    }

    public static MapHelper<String> readMapHelper(String jsonString) {
        Map<String, Object> map = readMap(jsonString);
        return map == null ? null : new MapHelper<String>(map);
    }

    // there must be array, no null allowed here
    @SuppressWarnings(value = "unchecked")

    private static <T> void innerReadListIntoColl(Collection<T> coll, Task t, String startChar, String endChar) {
        t.readRequiredToken(startChar);

        boolean first = true;
        while (true) {
            Token nextToken = t.lookupToken();
            String nextTokenStr = nextToken.str;

            if (endChar.equals(nextTokenStr)) {
                break;
            }

            if (!first) {
                t.readRequiredToken(",");
            } else {
                first = false;
            }

            T obj = (T) readObject(t);
            coll.add(obj);
        }

        t.readRequiredToken(endChar);
    }

    private static List<Object> innerReadList(Task t, String startChar, String endChar) {
        if (checkHasNull(t)) {
            return null;
        }

        List<Object> coll = new ArrayList<Object>();
        innerReadListIntoColl(coll, t, startChar, endChar);
        return coll;
    }

    private static List<Object> readList(Task t) {
        return innerReadList(t, "[", "]");
    }

    public static List<Object> readParamList(Task t) {
        return innerReadList(t, "(", ")");
    }

    public static <T> LinkedHashSet<T> readLinkedHashSet(String jsonString) {
        Task t = new Task(jsonString);
        if (checkHasNull(t)) {
            return null;
        }

        LinkedHashSet<T> res = new LinkedHashSet<T>();
        innerReadListIntoColl(res, t, "[", "]");
        return res;
    }

    public static List<Object> readList(String jsonString) {
        Task t = new Task(jsonString);
        return innerReadList(t, "[", "]");
    }

    public static boolean isDigitChar(char c) {
        return c >= '0' && c <= '9' || c == '.' || c == '-' || c == '+';
    }

    public static enum TokenKind {

        StringLiteral, Ident, Number, Date, SpecialChar, EndOfText, Null,
        Boolean
    }

    public static class Token {

        public String str;
        public Object val;
        public TokenKind kind;

        public Token(String str, Object val, TokenKind kind) {
            this.str = str;
            this.val = val;
            this.kind = kind;
        }
    }

    public static class Task {

        public String jsonString;
        public int currPos;
        protected int currCommentStartPos = -1;

        public Task(String jsonString) {
            this.jsonString = jsonString;
            this.currPos = 0;
        }

        private Token innerReadToken(boolean lookup) {
            //System.out.println("innerReadToken(" + lookup + ") starts at pos: " + currPos);
            skipWhiteChars();
            if (!hasMoreChars()) {
                return new Token("", null, TokenKind.EndOfText);
            }

            int oldPos = currPos;
            String res;
            try {
                char c = jsonString.charAt(currPos);
                if (c == '\"' || c == '\'') {
                    return readStringLiteral(c);
                }
                if (isDigitChar(c)) {
                    return readNumber();
                }

                while (hasMoreChars() && !isCurrentCharWhite()) {
                    char cc = jsonString.charAt(currPos);
                    boolean breakChar = cc == ':' || cc == ',' || cc == '['
                            || cc == ']' || cc == '{' || cc == '}' || cc == '(' || cc == ')';
                    if (breakChar) {
                        if (currPos == oldPos) {
                            currPos++;
                        }
                        break;
                    }
                    currPos++;
                }
                res = jsonString.substring(oldPos, currPos);
                Object val = res;
                TokenKind kind = TokenKind.Ident;
                if ("null".equals(res)) {
                    val = null;
                    kind = TokenKind.Null;
                } else if ("true".equals(res) || "false".equals(res)) {
                    val = "true".equals(res);
                    kind = TokenKind.Boolean;
                } else if ("new".equals(res)) {
                    return readDate(oldPos);
                }
                return new Token(res, val, kind);
            } finally {
                if (lookup) {
                    currPos = oldPos;
                }
            }
        }

        public Token readToken() {
            return innerReadToken(false);
        }

        public Token lookupToken() {
            return innerReadToken(true);
        }

        public boolean readToken(String token) {
            skipWhiteChars();
            int endPos = currPos + token.length();
            if (endPos > jsonString.length()) {
                return false;
            }
            String substr = jsonString.substring(currPos, endPos);
            if (token.equals(substr)) {
                currPos = endPos;
                return true;
            } else {
                return false;
            }
        }

        public void readRequiredToken(String token) {
            if (!readToken(token)) {
                throw new LameRuntimeException("expected token: \"" + token
                        + "\" at position: " + currPos + ", but found: "
                        + BaseUtils.safeSubstring(jsonString, currPos, 20));
            }
        }

        public boolean hasMoreChars() {
            return currPos < jsonString.length();
        }

        private boolean isCurrentCharWhite() {
            if (!hasMoreChars()) {
                return false;
            }

            char currChar = jsonString.charAt(currPos);
//            return (c == ' ' || c == '\n' || c == '\r' || c == '\t');

            if (currCommentStartPos >= 0) {
                boolean isBlockComment = jsonString.charAt(currCommentStartPos + 1) == '*';
                final char prevChar = jsonString.charAt(currPos - 1);

                if (isBlockComment) {
                    if (currPos >= currCommentStartPos + 4 && prevChar == '/' && jsonString.charAt(currPos - 2) == '*') {
                        currCommentStartPos = -1;
                    }
                } else {
                    if (prevChar == '\n' || prevChar == '\r') {
                        currCommentStartPos = -1;
                    }
                }
            }

            if (currCommentStartPos >= 0 || currChar == ' ' || currChar == '\n' || currChar == '\r' || currChar == '\t') {
                return true;
            }

            if (currChar == '/' && currPos + 1 < jsonString.length()) {
                char nextChar = jsonString.charAt(currPos + 1);
                if (nextChar == '/' || nextChar == '*') {
                    currCommentStartPos = currChar;
                }
            }

            return currCommentStartPos >= 0;
        }

        private Token readDate(int oldPos) {
            Token classNameToken = readToken();
            if (classNameToken.kind != TokenKind.Ident) {
                throw new LameRuntimeException("expected class name for constructor");
            }
            String className = classNameToken.str;
            if (!"Date".equals(className)) {
                throw new LameRuntimeException("unsupported class name for constructor: \"" + className + "\"");
            }

            List<Object> params = readParamList(this);
            int paramCount = params.size();
            if (paramCount != 3 && paramCount != 5 && paramCount != 6 && paramCount != 7) {
                throw new LameRuntimeException("unsupported param count for new Date: " + paramCount);
            }
            int year = readInt(params, 0);
            int month = readInt(params, 1) + 1;
            int day = readInt(params, 2);
            int hours = readInt(params, 3);
            int minutes = readInt(params, 4);
            int secods = readInt(params, 5);
            int millis = readInt(params, 6);
            Date d = SimpleDateUtils.newDateWithMillis(year, month, day,
                    hours, minutes, secods, millis);
            return new Token(jsonString.substring(oldPos, currPos), d, TokenKind.Date);
        }

        private int readInt(List<Object> params, int i) {
            Object o = i < params.size() ? params.get(i) : 0;
            Object num = BaseUtils.tryParseNumber(BaseUtils.safeToString(o));
            if (num instanceof Integer) {
                return (Integer) num;
            } else {
                return num != null ? ((Double) num).intValue() : 0;
            }
        }

        private Token readNumber() {
            int oldPos = currPos;
            while (hasMoreChars()) {
                char c = jsonString.charAt(currPos);
                if (!isDigitChar(c)) {
                    break;
                }
                currPos++;
            }
            String numStr = jsonString.substring(oldPos, currPos);
            Object numObj = BaseUtils.tryParseNumber(numStr);
            if (numObj == null) {
                throw new LameRuntimeException("cannot parse number: \"" + numStr + "\" at pos: " + oldPos);
            }
            return new Token(numStr, numObj, TokenKind.Number);
        }

        private Token readStringLiteral(char c) {
            int oldPos = currPos;
            currPos++;
            boolean prevBaskslash = false;
            while (hasMoreChars() && (prevBaskslash || jsonString.charAt(currPos) != c)) {
                if (prevBaskslash) {
                    prevBaskslash = false;
                } else {
                    prevBaskslash = jsonString.charAt(currPos) == '\\';
                }
                currPos++;
            }
            if (!hasMoreChars()) {
                throw new LameRuntimeException("expected end of string literal \"" + c + "\", but end of text reached");
            }
            currPos++;
            String str = jsonString.substring(oldPos, currPos);
            String val = StringEscapeUtils.unescapeQuotedJava(str);
            //System.out.println("string literal: \"" + str + "\"");
            return new Token(str, val, TokenKind.StringLiteral);
        }

        private void skipWhiteChars() {
            while (isCurrentCharWhite()) {
                currPos++;
            }
        }
    }

    private static boolean checkHasNull(Task t) {
        Token lookuped = t.lookupToken();
        if (lookuped.kind == TokenKind.Null) {
            return true;
        }
        return false;
    }

    public static Map<String, Object> readMap(Task t) {
        if (checkHasNull(t)) {
            return null;
        }

        t.readRequiredToken("{");

        Map<String, Object> res = new LinkedHashMap<String, Object>();

        boolean first = true;
        while (true) {
            Token nextToken = t.lookupToken();
            String nextTokenStr = nextToken.str;

            if ("}".equals(nextTokenStr)) {
                break;
            }

            if (!first) {
                t.readRequiredToken(",");
            } else {
                first = false;
            }

            Token keyToken = t.readToken();
            if (keyToken.kind == TokenKind.SpecialChar || keyToken.kind == TokenKind.EndOfText) {
                throw new LameRuntimeException("unexpected token kind: " + keyToken.kind.name());
            }
            String key = BaseUtils.safeToString(keyToken.val);
            t.readRequiredToken(":");
            Object obj = readObject(t);
            res.put(key, obj);
            //System.out.println("key=" + key + ",obj=" + obj);
        }

        t.readRequiredToken("}");
        return res;
    }

    public static Map<String, Object> readMap(String jsonString) {
        Task t = new Task(jsonString);
        return readMap(t);
    }

    public static Object readObject(Task t) {
        Token token = t.lookupToken();
        String tokenStr = token.str;
        if ("{".equals(tokenStr)) {
            return readMap(t);
        }
        if ("[".equals(tokenStr)) {
            return readList(t);
        }
        if ("null".equals(tokenStr)) {
            t.readToken();
            return null;
        }

        return t.readToken().val;
    }

    public static Object readObject(String jsonString) {
        return readObject(new Task(jsonString));
    }
}
