/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import pl.fovis.client.IEntityDetailsWidgetsContainer;

/**
 *
 * @author wezyr
 */
public interface IEntityDetailsWidgetTabReady extends IEntityDetailsWidget {

    // nazwa zakładki
    public String getCaption();

    // wartość 0 -> brak danych
    // wartość > 0 -> tyle danych na zakładce będzie
    // wartość < 0 albo NULL -> są dane, ale ich liczenie nie dotyczy
    //            (nie pokazuj licznika gdy ten subwidget jest jako zakładka)
    // bo ten widget nie wyświetla prostego grida z rekordami (beanami)
    public Integer getItemCount();

    public void setEntityDetailsWidgetsContainer(IEntityDetailsWidgetsContainer tcr);

    // sprawdza czy aktualnie zalogowany (lub bez logowania - ale raczej wymagamy
    // logowania do edycji) użytkownik ma prawo do edycji danych na tej zakładce
    // zazwyczaj admin lub ekspert mogą edytować, inni zalogowani i niezalogowani - nie
    public boolean isEditonEnabled();

    public String getTreeKindForSelected();

    // Czy jest dostępna - wynika z node kindów
    public boolean isEnabled();
}
