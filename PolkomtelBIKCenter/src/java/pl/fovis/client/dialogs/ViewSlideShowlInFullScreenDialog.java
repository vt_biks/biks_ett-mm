/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.i18nsupport.I18nMessage;

/**
 *
 * @author bfechner
 */
public class ViewSlideShowlInFullScreenDialog extends ViewMovieInFullScreenDialog {

    protected String caption;

    @Override
    protected Widget getContentWidget() {
        FlowPanel vp = new FlowPanel();
        vp.setStyleName("BizDef");
        vp.add(new HTML("<h2>" + caption + " - " + I18n.film.get() /* I18N:  */ + " </h2><br>"));
        String cln = I18nMessage.getCurrentLocaleName();
        String movieNameSuffix = BaseUtils.safeEquals(cln, "pl" /* i18n: no */) ? "" : ("_" + cln);
        vp.add(BIKClientSingletons.getMovie("slideshow" /* I18N: no */ + movieNameSuffix, 1050, 585));
        return vp;
    }

    @Override
    protected String getDialogCaption() {
        return caption;
    }

    @Override
    public void buildAndShowDialog(String caption) {
        this.caption = caption;
        super.buildAndShowDialog(null);
    }
}
