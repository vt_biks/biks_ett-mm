/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.user.client.ui.Widget;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryValue;

/**
 *
 * @author ctran
 */
public class LisaFiltrColumnValueBetween extends LisaFiltrColumnValueBase {

    public LisaDictionaryValue fromValue;
    public LisaDictionaryValue toValue;

    public LisaFiltrColumnValueBetween() {
    }

    public LisaFiltrColumnValueBetween(LisaDictColumnBean columnBean, Widget w1, Widget w2) {
        super(columnBean.nameInDb);
        fromValue = parseFromWidget(columnBean, w1);
        toValue = parseFromWidget(columnBean, w2);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("from ").append(fromValue.getValue().toString()).append(" to ").append(toValue.getValue().toString()).append(" ");
        return s.toString();
    }

}
