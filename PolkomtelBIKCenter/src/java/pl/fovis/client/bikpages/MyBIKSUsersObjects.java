/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.ChildrenWithAttributesWidget;
import pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.MyObjectsBean;
import pl.fovis.common.UserObjectBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class MyBIKSUsersObjects extends MyBIKSTabBase {

    @Override
    public String getCaption() {
        return I18n.obiektyUzytkownikow.get();
    }

    @Override
    public String getButtonStyle() {
        return "labelLink-usersobjects";
    }

    @Override
    public void show(final VerticalPanel vp) {
        vp.clear();
        callServiceToGetData(new StandardAsyncCallback<List<UserObjectBean>>("Error in getMyObjects()") {

            @Override
            public void onSuccess(List<UserObjectBean> result) {
                for (UserObjectBean userObject : result) {
                    if (BaseUtils.isCollectionEmpty(userObject.objects)) {
                        continue;
                    }
                    VerticalPanel userPanel = new VerticalPanel();
                    userPanel.setWidth("100%");
                    userPanel.add(new HTML("<h3>" + userObject.user.name + "</h3>"));
                    for (MyObjectsBean object : userObject.objects) {
                        if (BaseUtils.isCollectionEmpty(object.objects)) {
                            continue;
                        }
                        userPanel.add(new HTML("<b>" + object.role + "</b>"));
                        userPanel.add(new ChildrenWithAttributesWidget(null, object.objects, BIKClientSingletons.getMyObjectsAttributeNames()).buildWidget());
                        userPanel.add(EntityDetailsWidgetFlatBase.createPostSpacing());
                    }
                    userPanel.add(EntityDetailsWidgetFlatBase.createPostSpacing());
                    if (userPanel.getWidgetCount() > 2) {
                        vp.add(userPanel);
                    }
                }
            }
        });
    }

    @Override
    public boolean isVisible() {
        return BIKClientSingletons.isMyObjectsTabVisible() && BIKClientSingletons.isAppAdminLoggedIn(BIKConstants.ACTION_MY_OBJECTS_TAB_VISIBLE);
    }

    protected void callServiceToGetData(AsyncCallback<List<UserObjectBean>> callback) {
        BIKClientSingletons.getService().getUsersObjects(callback);
    }

}
