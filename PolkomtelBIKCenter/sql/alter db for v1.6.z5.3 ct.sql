exec sp_check_version '1.6.z5.3';
go

if not exists (select 1 from syscolumns where  (id = OBJECT_ID('bik_lisa_dict_column')) and (name = 'column_length'))
begin
	alter table dbo.bik_lisa_dict_column add column_length int NULL;
end;
go

if not exists (select 1 from syscolumns where  (id = OBJECT_ID('bik_lisa_dict_column')) and (name = 'nullable'))
begin
	alter table dbo.bik_lisa_dict_column add nullable varchar(10) NULL;
end;
go

if not exists (select 1 from syscolumns where  (id = OBJECT_ID('bik_lisa_dict_column')) and (name = 'decimal_total_digits'))
begin
	alter table dbo.bik_lisa_dict_column add decimal_total_digits int NULL;
end;
go
 
if not exists (select 1 from syscolumns where  (id = OBJECT_ID('bik_lisa_dict_column')) and (name = 'decimal_fractional_digits'))
begin
	alter table dbo.bik_lisa_dict_column add decimal_fractional_digits int NULL;
end;
go

exec sp_update_version '1.6.z5.3', '1.6.z5.4';
go
