﻿exec sp_check_version '1.7.z12.1';
go
if not exists (select * from bik_app_prop where name = 'deleteUserInRoleDQMForDeletedUser')
begin
	insert into bik_app_prop (name, val, is_editable) 
	values ('deleteUserInRoleDQMForDeletedUser', 'false', 1)
end
go

if (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA' -- PB
begin
	update bik_app_prop set val='true' where name= 'deleteUserInRoleDQMForDeletedUser';

end
go

exec sp_update_version '1.7.z12.1', '1.7.z12.2';
go