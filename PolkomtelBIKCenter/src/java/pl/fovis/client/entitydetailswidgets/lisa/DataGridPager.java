/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.grid.PagerBase;

/**
 *
 * @author ctran
 */
public class DataGridPager extends PagerBase implements IsWidget {

    protected IDataGrid page;

    public DataGridPager(LisaDictionaryContentDisplayedByDataGrid page) {
        super();
        //
        pageInfoLbl.setText(I18n.strona.get() + " 1");
        this.page = page;
    }

    @Override
    public void navigate(int pageNum) {
        if (pageNum < 0) {
            pageNum = 0;
        }
        pageInfoLbl.setText(I18n.strona.get() + " " + (pageNum + 1));
        this.page.changePage(pageNum);
    }

    @Override
    public int getPageNum() {
        if (page.isHasMoreData()) {
            return this.page.getPageNum();
        } else {
            return getMaxPageNum() - 1;
        }
    }

    @Override
    public int getMaxPageNum() {
        return (page.getBeanCnt() - 1) / page.getPageSize();
    }

    @Override
    public Widget asWidget() {
        return hp;
    }
}
