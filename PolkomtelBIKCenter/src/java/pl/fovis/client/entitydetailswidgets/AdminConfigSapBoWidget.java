/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.ConnectionParametersSapBoBean;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.IParametrizedContinuationWithReturn;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class AdminConfigSapBoWidget extends AdminConfigManyServersBase<List<ConnectionParametersSapBoBean>, ConnectionParametersSapBoBean> {

    protected TextBox tbxName;
    protected TextBox tbxServer;
    protected TextBox tbxUser;
    protected TextBox tbxPassword;
    protected TextBox tbxOpenReport;
    protected ListBox lbxField;
//    protected CheckBox chbPw;
    protected CheckBox chbSapUsers;
    protected CheckBox chbCheckRights;
    protected CheckBox chbIsBo40;
    protected PushButton btnSave;
    protected CheckBox chbIsActive;
    protected int serverId;
    protected int itemIndexToSelect;
    protected ConnectionParametersSapBoBean actualServer;
    protected List<String> metadataFolders;
    protected Map<Integer, Integer> indexMap;

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_SAPBO;
    }

    @Override
    protected void activateForServer(String id) {
        ConnectionParametersSapBoBean bean = serversMap.get(id);
        tbxName.setText(bean.name);
        tbxServer.setText(bean.server);
        tbxUser.setText(bean.userName);
        tbxPassword.setText(bean.password);
        tbxOpenReport.setText(bean.openReportTemplate);
        chbSapUsers.setValue(bean.withUsers);
        chbCheckRights.setValue(bean.checkRights);
        chbIsBo40.setValue(bean.isBo40);
        chbIsActive.setValue(bean.isActive);
        setWidgetsEnabled(chbIsActive.getValue());
        serverId = Integer.parseInt(id);
        actualServer = bean;
//        resultTest.setText("");
        lbxField.clear();
        itemIndexToSelect = 0;
        int iterator = 0;
        indexMap.clear();
        for (String obj : metadataFolders) {
            if (!obj.equals(bean.name)) {
                int idx = lbxField.getItemCount();
                lbxField.addItem(I18n.przed.get() + " " + obj);
                indexMap.put(idx, iterator);
            } else {
                itemIndexToSelect = lbxField.getItemCount();
            }
            iterator++;
        }
        int idx = lbxField.getItemCount();
        lbxField.addItem(I18n.ostatniElement.get());
        indexMap.put(idx, iterator);
        lbxField.setSelectedIndex(itemIndexToSelect);
    }

    @Override
    protected void buildRightWidget(CellPanel panelRight) {
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
            }
        }, false, panelRight);
        createLine(panelRight);
        createLabel(I18n.nazwa.get(), panelRight);
        tbxName = createTextBox(panelRight);
        createLabel(I18n.serwer.get(), panelRight);
        tbxServer = createTextBox(panelRight);
        createLabel(I18n.uzytkownik.get(), panelRight);
        tbxUser = createTextBox(panelRight);
        createLabel(I18n.haslo.get(), panelRight);
        tbxPassword = createPasswordTextBox(panelRight);
//        chbPw = createAndAddCheckBox(I18n.ukryjHaslo.get(), new ValueChangeHandler<Boolean>() {
//            @Override
//            public void onValueChange(ValueChangeEvent<Boolean> event) {
//                tbxPassword.getElement().setAttribute("type" /* I18N: no */, chbPw.getValue() ? "password" /* I18N: no */ : "text" /* I18N: no */);
//            }
//        }, panelRight);
        chbSapUsers = createAndAddCheckBoxWithOutHandler(I18n.pobierajObiektyUzytkownikow.get(), panelRight);
        createLabel(I18n.linkDoOpenDoc.get(), panelRight);
        tbxOpenReport = createTextBox(panelRight);
        chbCheckRights = createAndAddCheckBoxWithOutHandler(I18n.sprawdzajUprawnieniaUzytkownika.get(), panelRight);
        chbIsBo40 = createAndAddCheckBoxWithOutHandler(I18n.wersjaBO40.get(), panelRight);
        chbIsBo40.setVisible(isVisibleBo40CheckBox());
        createLine(panelRight);
        createLabel(I18n.pozycjaWMenu.get(), panelRight);
        List<MenuNodeBean> menuNodes = BIKClientSingletons.getMenuNodes();
        Integer metadataNodeId = null;
        for (MenuNodeBean node : menuNodes) {
            if (node.code.equals(BIKGWTConstants.MENU_CODE_METADATA)) {
                metadataNodeId = node.id;
                break;
            }
        }
        metadataFolders = new ArrayList<String>();
        if (metadataNodeId != null) {
            for (MenuNodeBean node : menuNodes) {
                if (node.parentNodeId != null && node.parentNodeId.intValue() == metadataNodeId.intValue()) {
                    metadataFolders.add(node.name.equals("@") ? node.code.replace("$", "") : node.name);
                }
            }
        }
        indexMap = new HashMap<Integer, Integer>();
//        metadataFolders.add("<as last item>");
        lbxField = createListBoxEx(null, panelRight);
        lbxField.setWidth("200px");
        createLine(panelRight);
        btnSave = createButton(I18n.zapisz.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (serverNames.contains(tbxName.getText()) && !tbxName.getText().equals(listbox.getItemText(getSelectedIndex()))) {
//                    Window.alert("Server name exists! Choose a difrent name. The server was not added!");
                    new SimpleInfoDialog().buildAndShowDialog(I18n.istniejeTakiSerwer.get(), null, null);
                } else {
                    final ConnectionParametersSapBoBean bean = new ConnectionParametersSapBoBean();
                    bean.id = actualServer.id;
                    bean.name = tbxName.getText();
                    bean.server = tbxServer.getText();
                    bean.userName = tbxUser.getText();
                    bean.password = tbxPassword.getText();
//                    bean.path = tbxPath.getText();
                    bean.reportTreeId = actualServer.reportTreeId;
                    bean.universeTreeId = actualServer.universeTreeId;
                    bean.connectionTreeId = actualServer.connectionTreeId;
                    bean.reportTreeCode = actualServer.reportTreeCode;
                    bean.universeTreeCode = actualServer.universeTreeCode;
                    bean.connectionTreeCode = actualServer.connectionTreeCode;
                    bean.openReportTemplate = tbxOpenReport.getText();
                    bean.withUsers = chbSapUsers.getValue();
                    bean.checkRights = chbCheckRights.getValue();
                    bean.isBo40 = chbIsBo40.getValue();
                    bean.isActive = chbIsActive.getValue();
                    final int newIndexToSelect = indexMap.get(lbxField.getSelectedIndex());
                    bean.visualOrder = newIndexToSelect;
                    callServiceToSaveData(bean, new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " setConnectionParameters: " + getSourceName()) {
                                @Override
                                public void onSuccess(Void result) {
                                    BIKClientSingletons.setOpenReportLink(bean.openReportTemplate, actualServer.reportTreeId);
                                    serversMap.put(String.valueOf(bean.id), bean);
                                    serverNames.remove(listbox.getItemText(getSelectedIndex()));
                                    serverNames.add(bean.name);
                                    listbox.setItemText(getSelectedIndex(), bean.name);
                                    if (newIndexToSelect <= metadataFolders.size() && newIndexToSelect != itemIndexToSelect) {
                                        String oldName = metadataFolders.get(itemIndexToSelect);
                                        metadataFolders.remove(itemIndexToSelect);
                                        if (newIndexToSelect > itemIndexToSelect) { // elem. dodawany jest dalej na liście niż elem. usuwany
                                            metadataFolders.add(newIndexToSelect - 1, oldName);
                                        } else {
                                            metadataFolders.add(newIndexToSelect, oldName);
                                        }
                                    }
                                    activateForServer(String.valueOf(bean.id));
                                    resultTest.setText("");
                                    BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get());
                                }
                            });
                }
            }
        }, panelRight);
        createTestConnectionWidget(panelRight, new IParametrizedContinuationWithReturn<Void, Integer>() {

            @Override
            public Integer doIt(Void param) {
                return serverId;
            }
        });
    }

    @Override
    protected void callServiceToAddNewServer(String source, String serverName, boolean needSeparateSchedule, AsyncCallback<ConnectionParametersSapBoBean> asyncCallback) {
        BIKClientSingletons.getService().addBOConfig(source, serverName, asyncCallback);
    }

    @Override
    protected void callServiceToDeleteServer(int id, String serverName, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().deleteBOConfig(id, asyncCallback);
    }

    protected void callServiceToSaveData(ConnectionParametersSapBoBean bean, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().setSapBoConnectionParameters(bean, asyncCallback);
    }

    @Override
    protected void callServiceToTestConnection(int id, AsyncCallback<Pair<Integer, String>> asyncCallback) {
        BIKClientSingletons.getService().runSapBoTestConnection(id, asyncCallback);
    }

    @Override
    protected void doOptActionAfterAddServer(String serverName) {
        metadataFolders.add(serverName);
    }

    protected boolean isVisibleBo40CheckBox() {
        return false;
    }
}
