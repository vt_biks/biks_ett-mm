/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.treeandlist;

import java.util.Collection;
import pl.fovis.foxygwtcommons.ITreeBroker;

/**
 *
 * @author wezyr
 */
public interface ITreeRowStorage<IDT, TR> {

    //public IDT getIdValOfRow(TN row);
    public ITreeBroker<TR, IDT> getTreeBroker();

    public TR getRowById(IDT nodeId);

    public void addRow(TR row);

    public Collection<TR> getChildRows(TR row);

    public Collection<TR> getAllRows();
    
    //ww: usuwa wiersz wraz z potomkami
    public void deleteRowById(IDT nodeId);
}
