/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.connectionpool;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;
import javax.sql.DataSource;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author wezyr
 */
public class ConnectionPoolManagerViaDataSource extends ConnectionPoolManagerBase {

    protected final DataSource ds;
    protected final AtomicInteger connectionId = new AtomicInteger(0);

    public ConnectionPoolManagerViaDataSource(DataSource ds, int poolSize, int secsTimeout, IParametrizedContinuation<Connection> prepareNewConnCont) {
        super(poolSize, secsTimeout, prepareNewConnCont);
        this.ds = ds;
    }

    public ConnectionPoolManagerViaDataSource(DataSource ds, int poolSize, int secsTimeout) {
        this(ds, poolSize, secsTimeout, null);
    }

    @Override
    protected Connection openNewConnectionInternal() throws SQLException {
        Connection c = ds.getConnection();
//        System.out.println("ThreadId = " + Thread.currentThread().getId() + " connectionId=" + connectionId.getAndIncrement());
        return c;
    }
}
