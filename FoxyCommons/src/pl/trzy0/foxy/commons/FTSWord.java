/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

/**
 *
 * @author wezyr
 */
public class FTSWord {

    public String word;
    public FTSWordOrigin kind;
    public int idx;
    public int pos;
    public int len;
    public double weight;

    public FTSWord(String word, FTSWordOrigin kind, int idx, int pos, int len, double weight) {
        this.word = word;
        this.kind = kind;
        this.idx = idx;
        this.pos = pos;
        this.len = len;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "FTSWord{" + "word=" + word + ", kind=" + kind + ", idx=" + idx + ", pos=" + pos + ", len=" + len + ", weight=" + weight + '}';
    }
}
