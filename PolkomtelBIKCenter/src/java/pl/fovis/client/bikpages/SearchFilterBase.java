/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.GWTUtils;
import simplelib.IContinuation;

/**
 *
 * @author bfechner
 */
public abstract class SearchFilterBase {

    protected TextBox filterTb;

    protected FlowPanel getAllCheckboxFpWithStyle(CheckBox allCbx) {
        FlowPanel allFp = new FlowPanel();
        allFp.add(allCbx);
        allFp.addStyleName("borderBottom");
        allFp.setWidth("280px");
        return allFp;
    }

    protected abstract VerticalPanel buildWidget();

    protected abstract void filterAction();

    protected FlowPanel filterPanel(TextBox filterTb) {
        FlowPanel fp = new FlowPanel();
        fp.add(filterTb);
        filterTb.setWidth("265px");

        IContinuation filterCont = new IContinuation() {
            public void doIt() {
                filterAction();
            }
        };

        BikCustomButton filtrKind = new BikCustomButton("", "searchBtn", /* I18N:  */ filterCont);
        GWTUtils.addEnterKeyHandler(filterTb, filterCont);
        fp.add(filtrKind);
        return fp;
    }
}
