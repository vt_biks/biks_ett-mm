/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.wezyrtreegrid;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLTable.Cell;
import com.google.gwt.user.client.ui.HTMLTable.RowFormatter;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.treeandlist.ILazyRowLoader;
import static pl.fovis.foxygwtcommons.wezyrtreegrid.FoxyWezyrTreeGrid.DIAG_MSG_KIND$EXPAND_NODES_BY_IDS;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.LameRuntimeException;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public abstract class WezyrTreeGridProperBase<ROW> extends SimplePanel implements IWezyrTreeGrid<ROW> {

    protected static final String TABLE_ROW_ID_PREFIX = "_WTG_id";
//    public static final String DIAG_KIND_WW_REPEATING_COMMAND = "ww:RepeatingCommand";
//    protected List<String> colNames;
    protected List<Pair<String, Integer>> colNamesAndWidth;
    protected IRowCellValBroker<ROW> rowCellValBroker;
    protected List<IRowSelectionHandler<ROW>> rowSelectionHandlers = new ArrayList<IRowSelectionHandler<ROW>>();
    protected List<IRightClickHandler<ROW>> rightClickHandlers = new ArrayList<IRightClickHandler<ROW>>();
    protected List<IDoubleClickHandler<ROW>> doubleClickHandlers = new ArrayList<IDoubleClickHandler<ROW>>();
    protected List<IExpandCollapseHandler<ROW>> expandCollapseHandlers = new ArrayList<IExpandCollapseHandler<ROW>>();
    protected FlexTable table;
    protected VisualTreeRowProps<ROW> ultimateRootRow = new VisualTreeRowProps<ROW>();
    protected List<VisualTreeRowProps<ROW>> allRows = new ArrayList<VisualTreeRowProps<ROW>>();
    protected ILazyRowLoader<ROW> lazyRowLoader;
    protected ROW selectedRow = null;
    protected Map<ROW, VisualTreeRowProps<ROW>> visualRows = new HashMap<ROW, VisualTreeRowProps<ROW>>();
    protected Element selectedRowElement = null;
    protected int headerRowCnt = 0;
    protected List<Object[]> footerRowValues;
    protected RowFormatter tableRowFormatter;

    protected boolean pendingAddRows;
    protected Integer optRowIdxToSelectAfterAddRowsDone;
    protected Boolean mustFireEventAfterAddRowsDone;

    private final Set<ROW> expandedRows = new HashSet<ROW>();

    //    public WezyrTreeGridProperBase(/*
    //             * ICellBuilder<T> cellBuilder
    //             */IRowCellValBroker<ROW> rowCellValBroker,
    //            /*
    //             * IRowCellValBroker<ROW> rowCellValAdditional,
    //             */
    //            String... colNames) {
    //        this(rowCellValBroker, Arrays.asList(colNames)/*
    //                 * , rowCellValAdditional
    //                 */);
    //    }
    public WezyrTreeGridProperBase(/*
             * ICellBuilder<T> cellBuilder
             */IRowCellValBroker<ROW> rowCellValBroker,
            Collection<Pair<String, Integer>> colNames/*
     * , IRowCellValBroker<ROW> rowCellValAdditional
     */) {
        initTable();

        this.rowCellValBroker = rowCellValBroker;
        this.colNamesAndWidth = new ArrayList<Pair<String, Integer>>(colNames);
        //this.rowCellValAdditional = rowCellValAdditional;

        sinkEvents(Event.ONCONTEXTMENU | Event.ONDBLCLICK);

        initInternalData();
    }

    protected final void initTable() {
        internaInitTable();
    }

    protected void internaInitTable() {
        if (table != null) {
            table.removeFromParent();
        }

        table = new FlexTable();
        tableRowFormatter = table.getRowFormatter();
        table.setCellPadding(0);
        table.setCellSpacing(0);

        this.add(table);

        table.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                tableCellClicked(event);
            }
        });
    }

    protected Integer getGlobalRowIdFromElementId(String eleId) {
        try {
            return Integer.parseInt(eleId.substring(TABLE_ROW_ID_PREFIX.length()));
        } catch (NumberFormatException ex) {
            // something has gone wrong ;-)
            // no welformed id
            return null;
        }
    }

    // fireEvent == null -> fireEvents chyba że stoimy już na tym wierszu
    protected void selectRowByRealRowVisualIdx(Integer realRowIdx, Boolean fireEvent) {
        if (pendingAddRows) {
            optRowIdxToSelectAfterAddRowsDone = realRowIdx;
            mustFireEventAfterAddRowsDone = fireEvent;
            return;
        }

//        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//            ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                    "starting selectRowByRealRowVisualIdx: realRowIdx=" + realRowIdx);
//        }
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding,
//                    "selectRowByRealRowVisualIdx: idx=" + idx + ", fireEvent=" + fireEvent);
//        }
        //System.out.println("realRowIdx = " + realRowIdx + " headerRowCnt = " + headerRowCnt + " table.getRowCount() = " + table.getRowCount() + " BaseUtils.collectionSizeFix(footerRowValues) = " + BaseUtils.collectionSizeFix(footerRowValues));
        if (realRowIdx != null) {
            if (realRowIdx < headerRowCnt || realRowIdx >= table.getRowCount() - BaseUtils.collectionSizeFix(footerRowValues)) {
                //ww: to klik na header lub footer, więc to nie jest wiersz danych
                // zatem -> nic nie rób
                //Blad tutaj gdy liczba wezelow > 40
                return;
            }
        }

        Element newSelectedRowElement = realRowIdx == null ? null : tableRowFormatter.getElement(realRowIdx);

//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding,
//                    "selectRowByRealRowVisualIdx: newSelectedRowElement == selectedRowElement? "
//                    + (newSelectedRowElement == selectedRowElement));
//        }
        if (fireEvent == null) {
            if (newSelectedRowElement == selectedRowElement) {
                return;
            }
            fireEvent = true;
        }

        if (selectedRowElement != null) {
            //table.getRowFormatter().removeStyleName(selectedRowNum, ACTIVE_ROW_STYLE_NAME);
            selectedRowElement.removeClassName(IWezyrTreeGrid.ACTIVE_ROW_STYLE_NAME);
        }

        //int rowNum = c.getRowIndex();
        if (realRowIdx != null) {
            tableRowFormatter.addStyleName(realRowIdx, IWezyrTreeGrid.ACTIVE_ROW_STYLE_NAME);
            selectedRowElement = tableRowFormatter.getElement(realRowIdx);
        } else {
            selectedRowElement = null;
        }
        //selectedRowNum = idx;

        if (realRowIdx != null) {

            int globalRowId = //Integer.parseInt(rowIdStr.substring(TABLE_ROW_ID_PREFIX.length()));
                    getGlobalRowIdFromElementId(tableRowFormatter.getElement(realRowIdx).getId());

            VisualTreeRowProps<ROW> rowProps = allRows.get(globalRowId);
            selectedRow = rowProps.row;
        } else {
            selectedRow = null;
        }

        if (fireEvent) {
            //String rowIdStr = table.getRowFormatter().getElement(rowNum).getId();
            for (IRowSelectionHandler<ROW> handler : rowSelectionHandlers) {
                handler.rowSelected(selectedRow);
            }

            //Window.alert("selected rowNum: " + selectedRowNum);
        }
//        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//            ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                    "finished selectRowByRealRowVisualIdx: realRowIdx=" + realRowIdx);
//        }
    }

    protected void testClickedTarget(ClickEvent event) {
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            Element ele = DOM.eventGetTarget(Event.as(event.getNativeEvent()));
//            String eleTagName = ele == null ? null : DOM.getElementProperty(ele, "tagName");
//            String eleId = ele == null ? null : ele.getId();
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding,
//                    "tableCellClicked on tag=" + eleTagName + ", id=" + eleId);
//        }
    }

    protected void tableCellClicked(ClickEvent event) {
        testClickedTarget(event);

        Cell c = table.getCellForEvent(event);
        if (c == null) {
            return;
        }
        selectRowByRealRowVisualIdx(c.getRowIndex(), null);
    }

    @Override
    public void setLazyRowLoader(ILazyRowLoader<ROW> lazyRowLoader) {
        this.lazyRowLoader = lazyRowLoader;
    }

//    public boolean isLazyLoading() {
//        return lazyRowLoader != null;
//    }
    protected void initInternalDataInner() {
        ultimateRootRow = new VisualTreeRowProps<ROW>();
        ultimateRootRow.level = -1;
        //this.ultimateRootRow.hasChildren = true;
//        ultimateRootRow.setIsExpanded(true);
        setRowPropsIsExpanded(ultimateRootRow, true);
        ultimateRootRow.isVisible = true;

        allRows = new ArrayList<VisualTreeRowProps<ROW>>();
        selectedRowElement = null;
        selectedRow = null;
        visualRows = new HashMap<ROW, VisualTreeRowProps<ROW>>();
        expandedRows.clear();
    }

    private void initInternalData() {
        initInternalDataInner();
    }

    @Override
    public void addRowSelectionHandler(IRowSelectionHandler<ROW> handler) {
        rowSelectionHandlers.add(handler);
    }

    @Override
    public void addRightClickHandler(IRightClickHandler<ROW> handler) {
        rightClickHandlers.add(handler);
    }

    @Override
    public void addDoubleClickHandler(IDoubleClickHandler<ROW> handler) {
        doubleClickHandlers.add(handler);
    }

    @Override
    public void addExpandCollapseHandler(IExpandCollapseHandler<ROW> handler) {
        expandCollapseHandlers.add(handler);
    }

    protected Integer getGlobalRowIdFromElementInsideTable(com.google.gwt.dom.client.Element ele) {
        if (ele == null) {
            return null;
        }

        Integer id = null;
        // uwaga: w źródłach GWT (HTMLTable) używają:
        // DOM.getElementProperty(td, "tagName")
        if (ele.getTagName().equalsIgnoreCase("tr" /* I18N: no */)) {
            id = getGlobalRowIdFromElementId(ele.getId());
        }
        if (id != null) {
            return id;
        }
        return getGlobalRowIdFromElementInsideTable(ele.getParentElement());
    }

    protected void callExpandCollapseHandlers(ROW row) {
        for (IExpandCollapseHandler<ROW> handler : expandCollapseHandlers) {
            handler.rowExpandedCollapsed(row);
        }
    }

    @Override
    public void onBrowserEvent(Event event) {
        int eventType = event.getTypeInt();
        if (eventType == Event.ONCONTEXTMENU || eventType == Event.ONDBLCLICK) {
            Integer globalRowId = getGlobalRowIdFromElementInsideTable(DOM.eventGetTarget(event));
            ROW row;
            if (globalRowId != null) {
                VisualTreeRowProps<ROW> rowProps = allRows.get(globalRowId);
                row = rowProps.row;
                //Window.alert("right click on: " + rowProps.row.getCellVal("name"));
            } else {
                //Window.alert("right click in unknown place");
                row = null;
            }

            if (eventType == Event.ONCONTEXTMENU) {
                if (!rightClickHandlers.isEmpty()) {
                    int clientXFix = event.getClientX() + Window.getScrollLeft();
                    int clientYFix = event.getClientY() + Window.getScrollTop();
                    for (IRightClickHandler<ROW> handler : rightClickHandlers) {
                        handler.rowRightClicked(row, clientXFix, clientYFix);
                    }
                    event.preventDefault();
                }
            } else if (eventType == Event.ONDBLCLICK) {
                if (!doubleClickHandlers.isEmpty()) {
                    for (IDoubleClickHandler<ROW> handler : doubleClickHandlers) {
                        handler.rowDoubleClicked(row);
                    }
                    event.preventDefault();
                }
            }
        }
    }

    protected abstract void expandRow(VisualTreeRowProps<ROW> rowProps);

    @Override
    public void expandRow(ROW row) {
        final VisualTreeRowProps<ROW> visualRow = visualRows.get(row);

        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS, "expandRow: row=" + row + ", visualRow=" + visualRow);
        }

        expandRow(visualRow);
    }

    @Override
    public void removeRow(ROW row) {
        removeRow(visualRows.get(row));
    }

    protected abstract void removeRow(VisualTreeRowProps<ROW> rowProps);

    @Override
    public void markAsCutRow(ROW row) {
        markAsCutOrUncutRow(row, true);
    }

    @Override
    public void markAsUncutRow(ROW row) {
        markAsCutOrUncutRow(row, false);
    }

    protected void markAsCutOrUncutRow(ROW row, boolean isCut) {
        markAsCutOrUncutRow(visualRows.get(row), isCut);
    }

    protected void markAsCutOrUncutRow(VisualTreeRowProps<ROW> rowProps, boolean isCut) {
        int globalIdx = getGlobalIdxOfRow(rowProps, false);
        markAsCutOrUncutRowAndSubTrees(rowProps, isCut, globalIdx);
    }

    protected int globalIdxToRealRowIdx(int globalIdx) {
        return globalIdx + headerRowCnt;
    }

    protected void markAsCutOrUncutRowAndSubTrees(VisualTreeRowProps<ROW> rowProps, boolean isCut, int globalIdx) {
        if (rowProps.isVisible) {
            if (isCut) {
                tableRowFormatter.addStyleName(globalIdxToRealRowIdx(globalIdx), IWezyrTreeGrid.CUT_ROW_STYLE_NAME);
            } else {
                tableRowFormatter.removeStyleName(globalIdxToRealRowIdx(globalIdx), IWezyrTreeGrid.CUT_ROW_STYLE_NAME);
            }
        }
        if (rowProps.isExpanded && rowProps.childRows != null) {
            int i = 0;
            for (VisualTreeRowProps<ROW> childRow : rowProps.childRows) {
                markAsCutOrUncutRowAndSubTrees(childRow, isCut, globalIdx + ++i);
            }
        }
    }

    @Override
    public void pasteRow(ROW row, ROW srcRow, boolean isCut) {
        pasteRow(visualRows.get(row), visualRows.get(srcRow), isCut);
    }

    protected abstract void pasteRow(VisualTreeRowProps<ROW> rowProps, VisualTreeRowProps<ROW> srcRowProps, boolean isCut);

    @Override
    public void collapseRow(ROW row) {
        collapseRow(visualRows.get(row));
    }

    protected abstract void collapseRow(VisualTreeRowProps<ROW> rowProps);

    protected int getExpandedSubTreeCnt(VisualTreeRowProps<ROW> rowProps) {

        int res = 1;

        if (rowProps.isExpanded && rowProps.childRows != null) {
            for (VisualTreeRowProps<ROW> child : rowProps.childRows) {
                if (child.isVisible) {
                    res += getExpandedSubTreeCnt(child);
                }
            }
        }

        return res;
    }

    protected int getGlobalIdxOfRow(VisualTreeRowProps<ROW> rowProps, boolean includeThisRowSubTree/*
     * ROW row
     */) {
        //VisualTreeRowProps<ROW> rowProps = visualRows.get(row);

        if (rowProps == ultimateRootRow) {
            return -1;
        }

        int idx = 0; //rowProps.idxAtParent;

        List<VisualTreeRowProps<ROW>> siblingRows = rowProps.parentRow.childRows;

        for (int i = 0; i < siblingRows.size(); i++) {
            VisualTreeRowProps<ROW> siblingRow = siblingRows.get(i);

            if (siblingRow == rowProps && !includeThisRowSubTree) {
                break;
            }

            idx += getExpandedSubTreeCnt(siblingRow);

            if (siblingRow == rowProps) {
                break;
            }
        }

        if (rowProps.parentRow != ultimateRootRow) {
            idx += getGlobalIdxOfRow(rowProps.parentRow, false) + 1;
        }

        return idx;
    }

    protected int getIndexInSiblings(VisualTreeRowProps<ROW> rowProps, List<VisualTreeRowProps<ROW>> siblings) {
        return siblings.indexOf(rowProps);
//        for (int i = 0; i < siblings.size(); i++) {
//            if (siblings.get(i) == rowProps) {
//                return i;
//            }
//        }
//
//        return -1;
    }

    protected int getIdxAtParent(VisualTreeRowProps<ROW> rowProps) {
        List<VisualTreeRowProps<ROW>> siblings = rowProps.parentRow.childRows;
        return getIndexInSiblings(rowProps, siblings);
    }

    protected boolean isLastChildRow(VisualTreeRowProps<ROW> rowProps) {
        List<VisualTreeRowProps<ROW>> siblings = rowProps.parentRow.childRows;
        return BaseUtils.getLastItem(siblings) == rowProps;
    }

    @Override
    public void addRow(ROW row, ROW prevRow, AddRowMode mode, boolean isLeafForSure) {
        addRows(Collections.singleton(row), prevRow, mode, isLeafForSure);
    }

    @Override
    public abstract void addRows(final Iterable<ROW> rows, ROW prevRow, AddRowMode mode, boolean isLeafForSure);

    protected Object getCellVal(ROW row, String colName) {
//        if (colName.equals("name")) {
//            InlineFlowPanel fp = new InlineFlowPanel();
//            fp.add(new CheckBox(BaseUtils.safeToString(rowCellValBroker.getCellVal(row, colName))));
//            fp.setStyleName("cellInner");
//            return fp;
//        }

        return rowCellValBroker.getCellVal(row, colName);
    }

    protected void updateRowInTableIfVisible(VisualTreeRowProps<ROW> rowProps) {
        if (rowProps.isVisible && rowProps != ultimateRootRow) {
            updateRowInTable(rowProps);
        }
    }

    protected void updateRowInTable(VisualTreeRowProps<ROW> rowProps) {
        int insertAtIdx = getGlobalIdxOfRow(rowProps, false);
        updateRowCellsInTable(rowProps, insertAtIdx);
    }

    protected abstract void updateRowCellsInTable(final VisualTreeRowProps<ROW> rowProps, int rowNum);

    protected void expandAllAncestors(VisualTreeRowProps<ROW> rowProps) {

//        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//            ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                    "starting expandAllAncestors: rowProps=" + rowProps);
//        }
        if (rowProps.isVisible) {
            return; // ancestors already expanded
        }
        VisualTreeRowProps<ROW> parent = rowProps.parentRow;
        if (parent != null) {
            expandAllAncestors(parent);

//            if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//                ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                        "before expandRow(parent)");
//            }
            expandRow(parent);
        }

//        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//            ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                    "finished expandAllAncestors: rowProps=" + rowProps);
//        }
    }

    @Override
    public void selectRow(ROW row, boolean fireEvent) {

//        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//            ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                    "starting selectRow: row=" + row);
//        }
        VisualTreeRowProps<ROW> rowProps = visualRows.get(row);
        if (rowProps != null && !rowProps.isVisible) {
            expandAllAncestors(rowProps);
        }

        Integer realRowIdx;

        if (rowProps != null) {
            int globalRowId = getGlobalIdxOfRow(rowProps, false);
            realRowIdx = globalIdxToRealRowIdx(globalRowId);
        } else {
            realRowIdx = null;
        }

        selectRowByRealRowVisualIdx(realRowIdx, fireEvent);
//        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//            ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                    "finished selectRow: row=" + row);
//        }
    }

    protected List<Object[]> headerRowValues;

    protected void removeAllRowsFromTable() {
        table.removeAllRows();

        if (headerRowValues != null) {
            for (int rowNum = 0; rowNum < headerRowValues.size(); rowNum++) {
                insertHeaderRow(rowNum);
            }
        }
        if (footerRowValues != null) {
            for (int rowNum = 0; rowNum < footerRowValues.size(); rowNum++) {
                insertFooterRow(rowNum);
            }
        }

//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_FILTER_TREE)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_FILTER_TREE, "clearAllRows: after removeAllRowsFromTable(): table.removeAllRows()");
//        }
    }

    @Override
    public void clearAllRows() {
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_FILTER_TREE)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_FILTER_TREE, "clearAllRows: start");
//        }

        removeAllRowsFromTable();

        initInternalDataInner();

//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_FILTER_TREE)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_FILTER_TREE, "clearAllRows: done");
//        }
    }

    @Override
    public ROW getSelectedRow() {
        return selectedRow;
    }

    @Override
    public Set<ROW> getExpandedRows() {
        return new HashSet<ROW>(expandedRows);
    }

    protected void updateExpandedRowsSet(VisualTreeRowProps<ROW> rowProps, boolean isExpanded) {
        if (rowProps == null) {
            return;
        }

        ROW row = rowProps.row;

        if (row != null) {

            if (isExpanded) {
                expandedRows.add(row);
            } else {
                expandedRows.remove(row);
            }
        }
    }

    protected void setRowPropsIsExpanded(VisualTreeRowProps<ROW> rowProps, boolean isExpanded) {
        if (rowProps == null) {
            return;
        }

        updateExpandedRowsSet(rowProps, isExpanded);

        rowProps.setIsExpanded(isExpanded);
    }

    protected void fixIsExpandingIfNeeded(VisualTreeRowProps<ROW> rowProps, boolean isExpanded) {
        if (rowProps.isExpanding) {
//            rowProps.setIsExpanded(isExpanded);
            setRowPropsIsExpanded(rowProps, isExpanded);
            rowProps.isExpanding = false;
        }
    }

    @Override
    public void markNodeAsLeafForSure(ROW row) {
        VisualTreeRowProps<ROW> rowProps = visualRows.get(row);
        rowProps.isLeafForSure = true;

        fixIsExpandingIfNeeded(rowProps, false);

        updateRowInTableIfVisible(rowProps);
    }

    @Override
    public void updateRow(ROW row, IContinuation doUpdate) {
        VisualTreeRowProps<ROW> rowProps = visualRows.remove(row);
        doUpdate.doIt();
        visualRows.put(row, rowProps);

        if (rowProps != null) {
            updateExpandedRowsSet(rowProps, rowProps.isExpanded);
        }

        updateRowInTableIfVisible(rowProps);
    }

    @Override
//    @SuppressWarnings("deprecation")
//    public com.google.gwt.user.client.Element getSelectedRowElement() {
    public Element getSelectedRowElement() {
        return selectedRowElement;
    }

    protected void setIdOfRowInTable(int rowIdxInTable, int globalRowId) {
        final int realRowIdx = globalIdxToRealRowIdx(rowIdxInTable);
        Element rowEle = tableRowFormatter.getElement(realRowIdx);
        rowEle.setId(TABLE_ROW_ID_PREFIX + globalRowId);
        tableRowFormatter.addStyleName(realRowIdx, "nodeRow");
    }

    protected String getDiagInfo(VisualTreeRowProps<ROW> rowProps) {
        if (rowProps == null) {
            return null;
        }

        String res = null;

        ROW row = rowProps.row;
        if (row instanceof Map) {
            @SuppressWarnings("unchecked")
            Map<String, Object> m = (Map<String, Object>) row;
            res = "row-id=" + BaseUtils.safeToString(m.get("id" /* I18N: no */));
        }

        if (res == null) {
            res = "row" /* I18N: no */ + "=" + row;
        }

        return "grId=" + rowProps.globalRowId
                + ",isV=" + rowProps.isVisible
                + ",isE-ed=" + rowProps.isExpanded
                + ",isE-ing=" + rowProps.isExpanding
                + "," + res
                + "," + "props" /* I18N: no */ + "=" + rowProps;
    }
    protected String execPhase = "unknown" /* I18N: no */;

    protected void addRowToTreeTable(int insertAtIdx, VisualTreeRowProps<ROW> newRow) {
        try {
            execPhase = "addRowToTreeTable: start" /* I18N: no */;
            table.insertRow(globalIdxToRealRowIdx(insertAtIdx));
            execPhase = "addRowToTreeTable: before setIdOfRowInTable" /* I18N: no */;
            setIdOfRowInTable(insertAtIdx, newRow.globalRowId);
            execPhase = "addRowToTreeTable: before updateRowCellsInTable" /* I18N: no */;
            updateRowCellsInTable(newRow, insertAtIdx);
            execPhase = "addRowToTreeTable: end" /* I18N: no */;
        } catch (Exception ex) {
//            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding,
//                        "in catch: " + execPhase + ", ex=" + ex + ", row:"
//                        + getDiagInfo(newRow));
//            }
            ClientDiagMsgs.showException("addRowToTreeTable: exception in" /* I18N: no */ + " " + execPhase, ex);
            throw new LameRuntimeException("exception in" /* I18N: no */ + " " + execPhase, ex);
        }
    }

    protected IsWidget getCellValAsWidget(Object val, boolean wrapInline) {
        if (val instanceof IsWidget) {
            return (IsWidget) val;
        } else {
            Widget w;
            if (val instanceof SafeHtml) {
                SafeHtml h = (SafeHtml) val;
                w = wrapInline ? new InlineHTML(h) : new HTML(h);
            } else {
                String strVal = val == null ? "" : val.toString();
                w = wrapInline ? new InlineLabel(strVal) : new Label(strVal);
            }
            w.setStyleName("cellInner");
            return w;
        }
    }

    // ultimateRootRow for row == null
    protected VisualTreeRowProps<ROW> getVisualRowFix(ROW row) {
        return row == null ? ultimateRootRow : visualRows.get(row);
    }

    @Override
    public int getRowChildCount(ROW row) {
        VisualTreeRowProps<ROW> rowProps = getVisualRowFix(row);
        return BaseUtils.collectionSizeFix(rowProps.childRows);
    }

    @Override
    public int getRowIndexInSiblings(ROW row) {
        return getIdxAtParent(visualRows.get(row));
//        VisualTreeRowProps<ROW> rowProps = visualRows.get(row);
//
//        VisualTreeRowProps<ROW> parent = rowProps.parentRow;
//
//        return parent.childRows.indexOf(rowProps);
    }

    @Override
    public boolean isRowExpanded(ROW row) {
        return getVisualRowFix(row).isExpanded;
    }
    //    public boolean isLazyLoading() {
//        return lazyRowLoader != null;
//    }

    protected void insertHeaderRow(int rowNum) {
        Object[] headerRowVals = headerRowValues.get(rowNum);
        table.insertRow(rowNum);
        tableRowFormatter.addStyleName(rowNum, "headerRow");

        int colNum = 0;
        for (Object val : headerRowVals) {
            IsWidget w = getCellValAsWidget(val, true);
            table.setWidget(rowNum, colNum++, w);
        }
    }

    @Override
    public void addHeaderRow(Object... headerRowVals) {
        int rowNum = headerRowCnt++;
        System.out.println(rowNum);
        if (headerRowValues == null) {
            headerRowValues = new ArrayList<Object[]>();
        }
        headerRowValues.add(headerRowVals);
        insertHeaderRow(rowNum);
    }

    @Override
    public void addFooterRow(Object... footerRowVals) {
        if (footerRowValues == null) {
            footerRowValues = new ArrayList<Object[]>();
        }
        int rowNum = footerRowValues.size();
        footerRowValues.add(footerRowVals);
        insertFooterRow(rowNum);
    }

    protected void insertFooterRow(int rowNum) {
        Object[] footerRowVals = footerRowValues.get(rowNum);
        int tRN = table.getRowCount();
        table.insertRow(tRN);
        tableRowFormatter.addStyleName(tRN, "footerRow");

        int colNum = 0;
        for (Object val : footerRowVals) {
            IsWidget w = getCellValAsWidget(val, true);
            table.setWidget(tRN, colNum++, w);
        }
    }
}
