/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import simplelib.NamedPropsBeanGenericCodeGenerator;
import simplelib.NamedPropsBeanGenericCodeGenerator.IMainClass;
import simplelib.NamedPropsBeanGenericCodeGenerator.IPropClass;
import simplelib.logging.LameLoggerMsg;

/**
 *
 * @author pmielanczuk
 */
public class NamedPropsBeanCodeGeneratorByClass {

    static class PropClassByClass implements IPropClass {

        protected Class c;

        public PropClassByClass(Class c) {
            this.c = c;
        }

        @SuppressWarnings("unchecked")
        public boolean isAssignableTo(Class c) {
            return c.isAssignableFrom(this.c);
        }

        @SuppressWarnings("unchecked")
        public boolean isAssignableFrom(Class c) {
            return this.c.isAssignableFrom(c);
        }

        public String getFullName() {
            return c.getCanonicalName();
        }

        public boolean isPrimitiveType() {
            return c.isPrimitive();
        }
    }

    static class MainClassByClass implements IMainClass {

        protected Class c;

        public MainClassByClass(Class c) {
            this.c = c;
        }

        public Iterable<String> getPropNames() {
            return MuppetMerger.getMuppetProps(c, 3);
        }

        public IPropClass getPropClass(String name) {
            return new PropClassByClass(MuppetMerger.getPropertyClass(c, name, true));
        }

        public String getSimpleName() {
            return c.getSimpleName();
        }
    }

    public static String makeMethods(Class c) {
        return NamedPropsBeanGenericCodeGenerator.makeMethods(new MainClassByClass(c));
    }

    public static void main(String[] args) {
        System.out.println(makeMethods(LameLoggerMsg.class));
    }
}
