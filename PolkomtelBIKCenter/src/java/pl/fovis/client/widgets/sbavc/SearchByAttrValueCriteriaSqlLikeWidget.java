/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.widgets.sbavc;

import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import pl.fovis.common.sbavc.SearchByAttrValueCriteriaSqlLike;

/**
 *
 * @author ctran
 */
public class SearchByAttrValueCriteriaSqlLikeWidget extends SearchByAttrValueCriteriaContainsWidget {

    @Override
    public ISearchByAttrValueCriteria provideBean() {
        return (isValidInput() ? new SearchByAttrValueCriteriaSqlLike(getSelectedAttrId(), getSelectedAttrCaption(), containsTbx.getText(), negationChbx.getValue()) : null);
    }

}
