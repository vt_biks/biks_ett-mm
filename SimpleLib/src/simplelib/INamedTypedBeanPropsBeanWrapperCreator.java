/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author pmielanczuk
 * @param <T>
 */
public interface INamedTypedBeanPropsBeanWrapperCreator<T> extends IWrapperCreator<T, INamedTypedPropsBean> {

    public <BC extends T> INamedTypedPropsBeanBroker<BC> getBroker(Class<BC> beanClass);
}
