/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;

/**
 *
 * @author tflorczak
 */
public interface IPumpLogger {
    
    public void pumpLog(String str, Integer proc);
    
    public MssqlConnectionConfig getConnectionConfig();
    
}
