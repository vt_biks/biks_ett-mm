﻿exec sp_check_version '1.3.0.4';
go

update bik_app_prop set is_editable=1 where name='languages';
 
exec sp_update_version '1.3.0.4', '1.3.0.5';
go