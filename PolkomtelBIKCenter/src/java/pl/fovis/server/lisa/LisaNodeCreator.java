/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.lisa;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.BOXIService;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.lisa.LisaConstants;
import pl.fovis.common.lisa.LisaUtils;
import pl.fovis.common.lisa.TeradataException;
import pl.trzy0.foxy.commons.FoxyRuntimeException;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author pku
 */
public class LisaNodeCreator {

    private String newObjId;
    private final static String DESCR_DUMMY = "Opis nadany automatycznie przez BIKS" /* I18N: no */;
    private final static String NAME_ENG_DUMMY = "Name given by BIKS" /* I18N: no */;
    private final static String DESCR_ENG_DUMMY = "English description automatically given by BIKS" /* I18N: no */;
    private AuthorizedTeradataConnection tConnection;
    private BOXIService service;
    private final String schemaName;

    public LisaNodeCreator(AuthorizedTeradataConnection tConnection, BOXIService service, String schemaName) {
        this.tConnection = tConnection;
        this.service = service;
        this.schemaName = schemaName;
    }

    public TreeNodeBean createCategory(final String categoryName, final TreeNodeBean categoryTypeNode, Integer categoryKindId) throws TeradataException {
        final Integer categTypeObjId = LisaUtils.fetchCategoryTypeObjIdAsInt(categoryTypeNode.objId);
        final Integer parentCategId = (!categoryTypeNode.nodeKindCode.equals(BIKConstants.NODE_KIND_LISA_CATEGORIZATION))
                ? LisaUtils.fetchCategoryObjId(categoryTypeNode.objId) : null;

        tConnection.execWithJdbcConnection(new IParametrizedContinuation<Connection>() {
            @Override
            public void doIt(Connection conn) {
                try {
                    String errorMessage = null;
                    String prepareQuery = " call" /* i18n: no */ + " " + schemaName + "add_kateg"
                            + (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES ? LisaTeradataQueryGenerator.VALIDATION_SUFFIX : "")
                            + "(?,?,?,?,?,?,?"
                            + (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES ? ",?" : "")
                            + "); ";
                    CallableStatement cs = conn.prepareCall(prepareQuery);
                    //CallableStatement cs = conn.prepareCall("{" + "call" /* i18n: no */ + " " + schemaName + "add_kateg(?,?,?,?,?,?)}");

                    cs.setInt(1, categTypeObjId);
                    if (parentCategId == null) {
                        cs.setNull(2, java.sql.Types.INTEGER);
                    } else {
                        //cs.setInt(parentCategId, 12);
                        cs.setInt(2, parentCategId);
                    }
                    cs.setString(3, LisaServerUtils.transcodeToLatin1(categoryName));
                    cs.setString(4, DESCR_DUMMY);
                    cs.setString(5, NAME_ENG_DUMMY);
                    cs.setString(6, DESCR_ENG_DUMMY);
                    cs.registerOutParameter(7, java.sql.Types.SMALLINT);
                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
                        cs.registerOutParameter(8, java.sql.Types.VARCHAR);
                    }
                    System.out.println("NAS: CreateCategory query: " + prepareQuery + ", params: " + categTypeObjId + ", " + parentCategId + ", " + LisaServerUtils.transcodeToLatin1(categoryName) + ", " + DESCR_DUMMY + ", " + NAME_ENG_DUMMY + ", " + DESCR_ENG_DUMMY);
                    cs.executeQuery();
                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
                        errorMessage = cs.getString(8);
                        if (errorMessage != null && errorMessage.length() > 0) {
                            throw new SQLException(errorMessage);
                        }
                    }
                    newObjId = categoryTypeNode.objId + LisaConstants.OBJ_ID_SEPARATOR + cs.getInt(7);

//                    cs.setString(2, LisaTeradataQueryGenerator.transcodeToLatin1(categoryName));
//                    cs.setString(3, DESCR_DUMMY);
//                    cs.setString(4, NAME_ENG_DUMMY);
//                    cs.setString(5, DESCR_ENG_DUMMY);
//                    cs.registerOutParameter(6, java.sql.Types.SMALLINT);
//                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
//                        cs.registerOutParameter(7, java.sql.Types.VARCHAR);
//                    }
//                    cs.executeQuery();
//                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
//                        errorMessage = cs.getString(7);
//                        if (errorMessage != null && errorMessage.length() > 0) {
//                            throw new SQLException(errorMessage);
//                        }
//                    }
//                    newObjId = categoryTypeNode.objId + LisaConstants.OBJ_ID_SEPARATOR + cs.getInt(6);
                } catch (SQLException ex) {
                    throw new FoxyRuntimeException(ex.getMessage(), ex);
                }
            }
        });

        Pair<Integer, List<String>> p = service.createTreeNode(categoryTypeNode.id, categoryKindId, categoryName, newObjId, categoryTypeNode.treeId, null);
        return fillTreeNode(p.v1, BIKGWTConstants.NODE_KIND_LISA_CATEGORY, categoryKindId, categoryName, categoryTypeNode);
    }

    public Pair<TreeNodeBean, TreeNodeBean> createCategorization(final String categorizationName, final TreeNodeBean dictionaryParentNode, Integer categorizationKindId, Integer categoryKindId, String unassignedName, final int is_versioned, INamedSqlsDAO<Object> adhocDao) throws TeradataException {
        tConnection.execWithJdbcConnection(new IParametrizedContinuation<Connection>() {
            public void doIt(Connection conn) {
                try {
                    String errorMessage = null;
                    String prepareQuery = " call" /* i18n: no */ + " " + schemaName + "add_kateg_typ"
                            + (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES ? LisaTeradataQueryGenerator.VALIDATION_SUFFIX : "")
                            + "(?,?,?,?"
                            + (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES ? ",?" : "")
                            + "); ";
                    CallableStatement cs = conn.prepareCall(prepareQuery);
                    //CallableStatement cs = conn.prepareCall("{" + "call" /* i18n: no */ + " " + schemaName + "add_kateg_typ(?,?,?,?)}");

                    cs.setString(1, LisaServerUtils.transcodeToLatin1(categorizationName));
                    cs.setString(2, DESCR_DUMMY);
                    cs.setInt(3, is_versioned);
                    cs.registerOutParameter(4, java.sql.Types.SMALLINT);
                    //cs.registerOutParameter(4, java.sql.Types.INTEGER);

                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
                        cs.registerOutParameter(5, java.sql.Types.VARCHAR);
                    }

                    cs.executeQuery();
                    newObjId = dictionaryParentNode.objId + LisaConstants.OBJ_ID_SEPARATOR + cs.getShort(4);
                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
                        errorMessage = cs.getString(5);
                        if (errorMessage != null && errorMessage.length() > 0) {
                            throw new SQLException(errorMessage);
                        }
                    }
                    cs.clearParameters();

                } catch (SQLException ex) {
                    throw new FoxyRuntimeException(ex.getMessage(), ex);
                }
            }
        });

        Pair<Integer, List<String>> p = service.createTreeNode(dictionaryParentNode.id, categorizationKindId, categorizationName, newObjId, dictionaryParentNode.treeId, null);

        TreeNodeBean cTypeNode = fillTreeNode(p.v1, BIKGWTConstants.NODE_KIND_LISA_CATEGORIZATION, categorizationKindId, categorizationName, dictionaryParentNode);

        String unassignedCategObjId = newObjId + LisaConstants.OBJ_ID_SEPARATOR + LisaConstants.UNASSIGNED_CATEGORY_OBJ_ID_SUFFIX;
        Pair<Integer, List<String>> son = service.createTreeNode(cTypeNode.id, categoryKindId, unassignedName, unassignedCategObjId, dictionaryParentNode.treeId, null);
        adhocDao.execNamedCommand("makeNodeBuiltIn", son.v1);
        TreeNodeBean unassignedNode = fillTreeNode(son.v1, BIKGWTConstants.NODE_KIND_LISA_CATEGORY, categoryKindId, unassignedName, cTypeNode);
        return new Pair<TreeNodeBean, TreeNodeBean>(cTypeNode, unassignedNode);

    }

    private TreeNodeBean fillTreeNode(Integer nodeId, String nodeKindeCode, Integer nodeKindId, String nodeName, TreeNodeBean parentNode) {
        TreeNodeBean newNode = new TreeNodeBean();
        newNode.parentNodeId = parentNode.id;
        newNode.nodeKindId = nodeKindId;
        newNode.name = nodeName;
        newNode.objId = newObjId;
        newNode.treeId = parentNode.treeId;
        newNode.linkedNodeId = null;
        newNode.id = nodeId;
        newNode.nodeKindCode = nodeKindeCode;
        newNode.branchIds = parentNode.branchIds + newNode.id + "|";
        newNode.treeCode = parentNode.treeCode;
        newNode.treeName = parentNode.treeName;
        newNode.treeKind = parentNode.treeKind;
        return newNode;
    }
}
