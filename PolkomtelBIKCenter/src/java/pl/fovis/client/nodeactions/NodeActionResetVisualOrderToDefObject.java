/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author bfechner
 */
public class NodeActionResetVisualOrderToDefObject extends NodeActionUpdatingBase {

    @Override
    public boolean isEnabledByDesign() {
        return isMetaBiks() && BIKConstants.NODE_KIND_META_PRINT_ATTRIBUT.equalsIgnoreCase(node.nodeKindCode);
    }

    @Override
    public String getCaption() {
        return I18n.resetuSortowanie.get() /* I18N:  */;
    }

    @Override
    public void execute() {
//        Window.alert(node.parentNodeId + " " + node.treeId);
        getService().resetVisualOrder(node.parentNodeId, node.treeId,
                //       getAntiTreeRefreshingCallback(I18n.resetuSortowanie.get() /* I18N:  */)
                new StandardAsyncCallback<Void>() {
            @Override
            public void onSuccess(Void result) {
                refreshRedisplayDataAndSelectRecord(node.parentNodeId);
            }
        }
        );
    }
}
