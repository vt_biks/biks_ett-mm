/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.user.client.ui.Widget;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryValue;

/**
 *
 * @author ctran
 */
public class LisaFiltrColumnValueSingle extends LisaFiltrColumnValueBase {

    public static enum FILTER_KIND {

        LIKE, EQUAL, GREATER, SMALLER
    };

    public LisaDictionaryValue value;
    public FILTER_KIND kind;

    public LisaFiltrColumnValueSingle() {
    }

    public LisaFiltrColumnValueSingle(LisaDictColumnBean columnBean, Widget w, FILTER_KIND kind) {
        super(columnBean.nameInDb);
        this.value = parseFromWidget(columnBean, w);
        this.kind = kind;
    }

    @Override
    public String toString() {
        return value.getValue().toString() + " ";
    }
}
