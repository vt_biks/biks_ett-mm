/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddAnyNodeDialog;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class NodeActionAddAnyNode extends NodeActionBase {

    protected boolean addAsParent;
    protected Integer treeId;

    public NodeActionAddAnyNode() {
    }

    public NodeActionAddAnyNode(boolean addAsParent, int treeId) {
        this.addAsParent = addAsParent;
        this.treeId = treeId;
    }

    @Override
    public boolean isEnabledByDesign() {
        return isInUltimateEditMode() && !isMetaBiks();
    }

    @Override
    public String getCaption() {
        return I18n.addAnyNode.get() + " (Dev)";
    }

    @Override
    public void execute() {
        if (!isSocialUserLoggedInWithInfo()) {
            return;
        }

        new AddAnyNodeDialog().buildAndShowDialog(new IParametrizedContinuation<TreeNodeBean>() {

            @Override
            public void doIt(TreeNodeBean param) {
                BIKClientSingletons.getService().createTreeNode(addAsParent ? null : node.id, param.nodeKindId, param.name, "#UltimateMode:" + param.name, treeId != null ? treeId : node.treeId, null,
                        new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on createTreeNode") {

                            @Override
                            public void onSuccess(Pair<Integer, List<String>> result) {
                                BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get());
                                refreshRedisplayDataAndSelectRecord(result.v1);
                            }
                        });
            }
        });
    }
}
