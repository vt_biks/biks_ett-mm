/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.PickAuthorBlogDialog;
import pl.fovis.common.BlogAuthorBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author beata
 */
public class AuthorBeanExtractor extends NodeAwareBeanExtractorBase<BlogAuthorBean> {
    /* nie uzywane*/

    protected List<BlogAuthorBean> bab;
    protected int currentNodeId;

    public AuthorBeanExtractor(boolean isAddJoinedButtonEnabled, int currentNodeId, List<BlogAuthorBean> bab) {
        super(isAddJoinedButtonEnabled);
        this.currentNodeId = currentNodeId;
        this.bab = bab;
    }

    public Integer getNodeId(BlogAuthorBean b) {
        return b.userNodeId;
    }

    public String getTreeCode(BlogAuthorBean b) {
        return BIKGWTConstants.TREE_CODE_USER_ROLES;
    }

    @Override
    protected String getName(BlogAuthorBean b) {
        throw new UnsupportedOperationException(I18n.toSieNigdyNieWywola.get() /* I18N:  */);
    }

    @Override
    public String getNameAsHtml(BlogAuthorBean b) {
        return getNameWithOptStarAsHtml(b.userName, BIKClientSingletons.isStarredSystemUser(b.userId));
    }

    protected String getNodeKindCode(BlogAuthorBean b) {
        return null;
    }

    public String getBranchIds(BlogAuthorBean b) {
        return b.branchIds;
    }

    @Override
    public String getIconUrl(BlogAuthorBean b) {
        return BIKClientSingletons.fixAvatarUrl(b.avatar, getIconUrlByNodeKindCode(BIKGWTConstants.NODE_KIND_USERS_GROUP, null));
    }

    @Override
    public int getAddColCnt() {
        return 1; //rola admin
    }

    @Override
    public Object getAddColVal(BlogAuthorBean b, int addColIdx) {
        return b.isAdmin ? I18n.administratorBloga.get() /* I18N:  */ : "";
    }

    @Override
    public boolean isDeleteBtnEnabled(BlogAuthorBean b) {
        return BIKClientSingletons.isUserLoggedIn() && (isActionBtnEnable() || getNodeId(b) == BIKClientSingletons.getLoggedUserId());
    }

    protected boolean isActionBtnEnable() {

        if (bab != null) {
            for (BlogAuthorBean urin : bab) {
                if ((BIKClientSingletons.isUserLoggedIn() && BIKClientSingletons.isBikUserLoggedIn() && urin.userId == BIKClientSingletons.getLoggedUserId()
                        && urin.isAdmin)
                        //                        || BIKClientSingletons.isSysAdminLoggedIn()) {
                        //ww->authors: zmieniam na eksperta (efektywnego) tutaj
                        // nie uzywane nie zmieniam
                        || BIKClientSingletons.isEffectiveExpertLoggedIn()) {
                    return true;//isEditonEnabled();
                }
            }
        }
        return false;
    }

    @Override
    protected void execActDelete(BlogAuthorBean b) {
        final List<BlogAuthorBean> blogA = new ArrayList<BlogAuthorBean>();
        if (BIKClientSingletons.isUserLoggedIn()) {
            for (final BlogAuthorBean ba : bab) {
                if (BIKClientSingletons.isBikUserLoggedIn()
                        && ba.userId != BIKClientSingletons.getLoggedUserId()) {//BIKClientSingletons.getLoggedUserId()) {
                    blogA.add(ba);
                }
            }
        }

        if (b.isAdmin) {

            new PickAuthorBlogDialog().pickOneItem(blogA, null,
                    new IParametrizedContinuation<BlogAuthorBean>() {
                        public void doIt(BlogAuthorBean param) {

                            BIKClientSingletons.getService().updateBikAuthorsBlogToAdmin(currentNodeId, param.userId, getDoNothingCallback());
                            refreshView(I18n.zmienionoAutora.get() /* I18N:  */);

                        }
                    }, false);

        } else {
            BIKClientSingletons.getService().deleteBikAuthorsBlog(currentNodeId, b.userId, getDoNothingCallback());
            deleteBeanFromList(b, I18n.usunietoAutora.get() /* I18N:  */);
            //refreshView("Usunięto autora");
        }

    }
}
