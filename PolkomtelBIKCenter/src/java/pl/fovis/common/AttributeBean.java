/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.util.Date;
import simplelib.BeanWithIntIdBase;

/**
 *
 * @author AMamcarz
 */
public class AttributeBean extends BeanWithIntIdBase {

    public int nodeKindId;
    public int attrDefId;
    public String defaultValue;
    public boolean isRequired;
    public boolean isEmpty;
    public int isVisible;
    //TODO: change isPublic type to boolean
    public int isPublic;
//bik_attribute_linked
    public String atrLinValue;
    public int nodeId;
//bik_attr_def
    public String atrName;
    public String attrDisplayName;
    public int categoryId;
    public String atrCategory;
    public boolean isBuiltIn;
    public String hint;
    public String typeAttr;
    public String valueOpt;
    public boolean displayAsNumber;
    public int visualOrder;
    public boolean usedInDrools;
//bik_joined_obj_attribute
    public String valueOptTo;
//bik_node_kind
    public String kindCaption;
    public String iconName;
    public boolean isFolder;
    public int creatorId;
//?
    public boolean isEditable;
//?
    public String path;
    public String nodeObjId;
    public boolean isAddNodeInToTheSameTree;//nie z bazy, czy dodany został nowy node na hyperlinku do tego samego drzewa
    public boolean isAddNewInChildrenNodes;
    public String templateBranch;
    public Boolean isStatus;
    public Date statusDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof AttributeBean) {
            AttributeBean r = (AttributeBean) o;
            if (atrName.equals(r.atrName)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (this.atrName != null ? this.atrName.hashCode() : 0);
        return hash;
    }

    public String getAtrName() {
        return atrName;
    }

    public String getKindCaption() {
        return kindCaption;
    }

    public String getIconName() {
        return iconName;
    }

    public boolean isFolder() {
        return isFolder;
    }

    public boolean isVisible() {
        return isVisible == 1;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public String getPath() {
        return path;
    }

}
