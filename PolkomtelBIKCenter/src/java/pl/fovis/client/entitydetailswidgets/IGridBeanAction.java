/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.Image;

/**
 *
 * @author beata
 */
public interface IGridBeanAction<T> {

    public enum TypeOfAction {

        Delete, Edit, View, Other, Follow, EditTreeIcon, EditAttribute, ImportLinksAndJoinedObjs, ShowImportLog
    };

    public String getActionIconUrl(T b);

    public String getActionIconHint(T b);

    public void executeAction(T b);

    public boolean isVisible(T b);

    public boolean isEnabled(T b);

    public TypeOfAction getActionType();

    public void setActImage(Image img);

    public Image getActImage();
}
