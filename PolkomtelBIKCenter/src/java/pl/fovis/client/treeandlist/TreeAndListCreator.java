/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeGrid;
import pl.fovis.foxygwtcommons.wezyrtreegrid.FoxyWezyrTreeGrid;

/**
 *
 * @author bfechner
 */
public class TreeAndListCreator {

    public static IFoxyTreeGrid<Integer> createTreeGrid() {
        return new FoxyWezyrTreeGrid<Integer>();
        //ACTreeGridByWW<Integer>();
    }

//    public static IFoxyListGrid<Integer> createListGrid() {
//        return null;//new ACListGridByWW<Integer>(); // jest w GWT Advanced Components
//    }
}
