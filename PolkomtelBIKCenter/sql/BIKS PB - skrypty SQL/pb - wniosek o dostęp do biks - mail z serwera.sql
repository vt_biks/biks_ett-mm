/*
select * from bik_translation

select distinct kind from bik_translation

delete from bik_translation where kind = 'i18n';

insert into bik_translation (code, txt, lang, kind) values
('witaj', 'Spieprzaj dziadu', 'pl', 'i18n')
;

insert into bik_translation (code, txt, lang, kind) values
('witaj', 'Welcome the fuck', 'en', 'i18n')
;



update bik_app_prop set val = 'pl,en' where name = 'languages'

select *
from bik_translation where kind = 'i18n';


insert into bik_translation (code, txt, lang, kind) values
('witajxxx', 'Spieprzaj dziadu', 'pl', 'i18n')
;


merge into bik_translation as t
using (values 
  ('witaj', 'Heja', 'pl', 'i18n'),
  ('witaj', 'Hello', 'en', 'i18n')
) as x (code, txt, lang, kind) on t.code = x.code and t.lang = x.lang and t.kind = x.kind
when not matched by target then
  insert (code, txt, lang, kind)
  values (x.code, x.txt, x.lang, x.kind)
when not matched by source and t.kind = 'i18n' then
  delete
when matched then
  update set t.txt = x.txt;
*/
  
/*

select * from bik_app_prop
where name like 'dum%'


dumMailTitle	Wniosek o dost�p do systemu BIKS dla %{ssoUser}%
dumMailBody	Prosz� o dost�p do nast�puj�cych zak�adek systemu BIKS:    %{optionsN}%    M�j login to: %{ssoUser}%.    
dumFooterMsg	<br/><a href="mailto:Admin@Of@BIKS" class="gwt-PushButton gwt-PushButton-up" id="myCustomPushButton"><div class="html-face">Utw�rz maila z wnioskiem</div></a>, a nast�pnie wy�lij przygotowanego maila.<br/>

*/


update bik_app_prop set val = 'Wniosek o dost�p do systemu BIKS dla %{userName}%%{ssoUserP}%' where name = 'dumMailTitle';
update bik_app_prop set val = 'U�ytkownik <b>%{userName}%%{ssoUserP}%</b> wnioskuje o dost�p do nast�puj�cych zak�adek systemu BIKS:

%{optionsN}%' where name = 'dumMailBody';
update bik_app_prop set val = '<br/><a href="mailto:Admin@Of@BIKS" class="gwt-PushButton gwt-PushButton-up" id="myCustomPushButton"><div class="html-face">Wy�lij wniosek</div></a><br/>'
where name = 'dumFooterMsg';


-- select * from bik_app_prop

merge into bik_app_prop as t
using (values 
  ('useSimpleMsgForUnloggedUser', '0')
) as x (name, val) on t.name = x.name
when not matched by target then
  insert (name, val, is_editable)
  values (x.name, x.val, 0)
when matched then
  update set t.val = x.val;
