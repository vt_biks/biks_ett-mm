/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.common.i18npoc.I18n;

/** 
 *
 * @author mmastalerz
 */
public abstract class BaseActionOrCancelDialogEx extends BaseActionOrCancelDialog {

    protected Label status;
    protected TextBox tbTytul;
    protected TextArea tbDescription;
    private Label lblTitle;
    private Label lblContent;

    @Override
    protected abstract String getDialogCaption();

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        status = new Label();
        main.add(status);
        Grid grid = new Grid(2, 2);
        grid.setCellSpacing(4);

        grid.setWidget(0, 0, lblTitle = new Label(I18n.tytul.get() /* I18N:  */ + ":"));
        grid.setWidget(1, 0, lblContent = new Label(I18n.tresc.get() /* I18N:  */ + ":"));
        grid.setWidget(0, 1, tbTytul = new TextBox());
        grid.setWidget(1, 1, tbDescription = new TextArea());
        lblTitle.setStyleName("addNoteLbl");
        lblContent.setStyleName("addNoteLbl");
        tbTytul.setStyleName("addNote");
        tbDescription.setStyleName("addNote");
        tbTytul.setWidth("500px");
        tbDescription.setWidth("500px");
        tbDescription.setHeight("200px");
        tbTytul.setText(getTitle());
        tbDescription.setValue(getBody());
        tbTytul.addKeyUpHandler(addKeyHandler());
        tbDescription.addKeyUpHandler(addKeyHandler());

        main.add(grid);
    }

    @Override
    protected abstract void doAction();

    protected abstract String getTitle();

    protected abstract String getBody();

    public KeyUpHandler addKeyHandler() {
        return new KeyUpHandler() {
            public void onKeyUp(KeyUpEvent event) {
                if (tbTytul.getText().equals("") || tbDescription.getText().equals("")) {
                    actionBtn.setEnabled(false);
                } else {
                    actionBtn.setEnabled(true);
                }
            }
        };
    }
}
