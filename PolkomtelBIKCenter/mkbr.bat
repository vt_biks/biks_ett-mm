@echo off
@rem %1 - branch dir version, %2 - revision number
@rem example: mkbr.bat 1.3 7503
call prob_cfg.bat

SETLOCAL EnableDelayedExpansion
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  set "DEL=%%a"
)

set svn_branch_ver=%1

if "%1"=="" ( 
 call :ColorText 0C  "1. argument missed, please type a branch version" 
 echo copy_to_branch.bat %1 - branch dir version %2 - revision number
 goto exiterror
)

if "%2"=="" ( 
 call :ColorText 0C "2. argument missed, please type a revision number"
 set svn_src_rev= 
) else (
 set svn_src_rev=-r %2
)

echo Welcome, You chosen:  %1 dir and revision %2

svn info %SVN_LOCALIZATION%branches/%MAIN_PROJECT_NAME%/v%svn_branch_ver% >nul 2>nul
echo %ERRORLEVEL%
if "%ERRORLEVEL%"=="0" (
	echo ERROR - This branch already exists in SVN
	goto exiterror
) else (
	svn mkdir %SVN_LOCALIZATION%branches/%MAIN_PROJECT_NAME%/v%svn_branch_ver% -m "wersja %svn_branch_ver%"
)

for %%p in (%PROJECTS%) do ( 
	echo Project: %%p
	echo svn cp %svn_src_rev% %SVN_LOCALIZATION%%%p  %SVN_LOCALIZATION%branches/%MAIN_PROJECT_NAME%/v%svn_branch_ver%/%%p -m "wersja %svn_branch_ver%"
	svn cp %svn_src_rev% %SVN_LOCALIZATION%%%p  %SVN_LOCALIZATION%branches/%MAIN_PROJECT_NAME%/v%svn_branch_ver%/%%p -m "wersja %svn_branch_ver%"	
)

 echo %date%_%time% Added branch v%svn_branch_ver%>>all_branches.txt
goto exitsuc

:exiterror
 call :ColorText 0C "Copy to branches failed"
 color 0C
 goto exit

:exitsuc
 call :ColorText 0A "Copy to branches successful"
 goto exit
 
:exit
 call :ColorText 0A "To see You soon"
 goto :eof
 
  
:ColorText
echo off
<nul set /p ".=%DEL%" > "%~2"
findstr /v /a:%1 /R "^$" "%~2" nul
del "%~2" > nul 2>&1
goto :eof