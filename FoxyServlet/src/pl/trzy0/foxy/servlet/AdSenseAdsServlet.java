/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import commonlib.LameUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Collection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class AdSenseAdsServlet extends HttpServlet {

    public AdSenseAdsServlet() {
    }
    private String adsHtmlPart1;
    private String adsHtmlPart2;
    private String adsHtmlPart3;

    private String getAdsHtml(HttpServletRequest request) {
        String tagsStr = request.getParameter("tags");

        Collection<String> tags = BaseUtils.splitBySep(tagsStr, ",");

        if (adsHtmlPart1 == null) {
            InputStream is = getServletContext().getResourceAsStream("/adsense_ads.html");
            String content = LameUtils.loadAsString(is, "utf8");
            Pair<String, String> p = BaseUtils.splitString(content, "<!--tags-go-here-->");
            Pair<String, String> p2 = BaseUtils.splitString(p.v1, "put-tags-here");
            adsHtmlPart1 = p2.v1;
            adsHtmlPart2 = p2.v2;
            adsHtmlPart3 = p.v2;
        }

        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();

        sb.append(adsHtmlPart1);

        boolean first = true;
        for (String tag : tags) {
            if (first) {
                first = false;
            } else {
                sb.append(",");
                sb2.append(",");
            }
            String encodedTag = BaseUtils.encodeForHTMLTag(tag);
            sb.append(encodedTag);
            sb2.append("<h1>").append(encodedTag).append("</h1>");
        }

        sb.append(adsHtmlPart2).append(sb2.toString());
        sb.append(adsHtmlPart3);

        return sb.toString();
    }

    @Override
    public void doPost(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        //response.addHeader("Location", "/WebSvr/Servlet1?page=tab7&bizObj=XYZ");
        //response.sendRedirect("/WebSvr/Servlet1?page=tab7&bizObj=XYZ");
        //response.setContentType("text/html");
        request.setCharacterEncoding("utf8");
        response.setContentType("text/html;charset=UTF-8");
        //response.setContentType("application/xhtml+xml");
        response.setCharacterEncoding("utf8");
        PrintWriter out = response.getWriter();

        out.print(getAdsHtml(request));

        out.close();
    }

    @Override
    public void doGet(
            HttpServletRequest req,
            HttpServletResponse res)
            throws ServletException, IOException {
        doPost(req, res);
    }
}
