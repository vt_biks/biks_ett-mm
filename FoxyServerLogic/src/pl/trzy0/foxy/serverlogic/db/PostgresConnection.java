/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import commonlib.connectionpool.ConnectionPoolManagerViaDataSource;
import org.postgresql.jdbc2.optional.PoolingDataSource;
import pl.trzy0.foxy.commons.PostgresConnectionConfig;
import pl.trzy0.foxy.serverlogic.IConnectionDialectHelper;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class PostgresConnection extends PooledJdbcConnectionBase<PostgresConnectionConfig> {

    //private static ILameLogger logger = LameLoggerFactory.getLogger(PostgresConnection.class);
    //private static String driverClassName = "org.postgresql.Driver";
    //private static MiniConnectionPoolManager poolMgr;
    //private IConnectionPoolManager cpm;
    public PostgresConnection(PostgresConnectionConfig config) {
        initConnectionFinal(config);
        //this.dialect = new PostgresConnectionDialectHelper(this);
    }

    public void initConnection(PostgresConnectionConfig config) {
        try {
            //PGConnectionPoolDataSource dataSource = new PGConnectionPoolDataSource();
            PoolingDataSource dataSource = new PoolingDataSource();
            dataSource.setServerName(config.server);
            dataSource.setDatabaseName(config.database);
            dataSource.setUser(config.user);
            dataSource.setPassword(config.password);
            if (config.port != null) {
                dataSource.setPortNumber(config.port);
            }
            //poolMgr = new MiniConnectionPoolManager(dataSource, config.poolSize, config.timeout);
            cpm = //new ConnectionPoolManagerMini(dataSource, config.poolSize, config.timeout);
                    //ConnectionPoolManagerByPooledConnection(dataSource);
                    //new ConnectionPoolManagerByDataSource(dataSource);
                    new ConnectionPoolManagerViaDataSource(dataSource, 30, 30) {
                        {
                            disableConnectionIsValidCheck();
                        }
                    };

        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
    }

    @Override
    protected IConnectionDialectHelper createDialectHelper() {
        return new PostgresConnectionDialectHelper(this);
    }

    /*
     public void destroy() {
     cpm.dispose();
     //        try {
     //            poolMgr.dispose();
     //        } catch (SQLException ex) {
     //            //
     //        }
     }

     @Override
     public void finishWorkUnit(boolean success) {
     //super.finishWorkUnit(success); // <-- unnecessary, cpm will do it for us
     //        prevConn = null;
     cpm.recycleThreadConnection(success);
     }

     protected Connection getConnection() {
     Connection res = cpm.getConnection();

     //        if (prevConn != null && prevConn != res) {
     //            System.out.println("different connection detected, prev="
     //                    + LameUtils.getIdentityHashCode(prevConn)
     //                    + ", curr=" + LameUtils.getIdentityHashCode(res));
     //        }
     //        prevConn = res;

     //        try {
     //            System.out.println("[thread:" + Thread.currentThread() + "] getConnection(): connection=" + LameUtils.getIdentityHashCode(res) + ", autoCommit=" + res.getAutoCommit());
     //        } catch (SQLException ex) {
     //            throw new LameRuntimeException("getConnection(): getAutoCommit failed?", ex);
     //        }
     return res;
     }

     @Override
     protected Statement getStatement() {
     try {
     return getConnection().createStatement();
     } catch (SQLException ex) {
     throw new LameRuntimeException("getStatement failed", ex);
     }
     }

     @Override
     protected void releaseStatement(Statement st) {
     try {
     st.close();
     } catch (SQLException ex) {
     throw new LameRuntimeException("error closing statement", ex);
     }
     }
     */
//    private void initConnection(PostgresConnectionConfig connConfig) {
//        try {
//            Class.forName(driverClassName);
//            String connUrl = "jdbc:postgresql://" + connConfig.server + "/" + connConfig.database;
//            conn = DriverManager.getConnection(connUrl, connConfig.user, connConfig.password);
//            conn.setAutoCommit(true);
//        } catch (Exception ex) {
//            throw new LameRuntimeException(ex);
//        }
//    }
//    private static class ThreadLocalConnection extends ThreadLocal {
//
//        @Override
//        public synchronized Object initialValue() {
//            try {
//                PooledConnection pconn = poolMgr.getConnection();
////                if (logger.isErrorEnabled()) {
////                    logger.error("tlpool init: pconn=" + pconn + " thread=" + Thread.currentThread().getName() + " activeConnections=" + poolMgr.getActiveConnections());
////                }
////                System.out.println("tlpool init: pconn=" + pconn + " thread=" + Thread.currentThread().getName() + " activeConnections=" + poolMgr.getActiveConnections());
//                return pconn;
//            } catch (SQLException ex) {
//                throw new FoxyRuntimeException("getConnection() failed", ex);
//            }
//        }
//    }
//    private static ThreadLocalConnection tlPooledConnection = new ThreadLocalConnection();
//    private static synchronized PooledConnection getPooledConnection() {
//        PooledConnection pconn = (PooledConnection) tlPooledConnection.get();
////        if (logger.isErrorEnabled()) {
////            logger.error("got from tlpool: pconn=" + pconn + " thread=" + Thread.currentThread().getName() + " activeConnections=" + poolMgr.getActiveConnections());
////        }
////        System.out.println("got from tlpool: pconn=" + pconn + " thread=" + Thread.currentThread().getName() + " activeConnections=" + poolMgr.getActiveConnections());
//
//        if (pconn == null) {
//            throw new FoxyRuntimeException("pconn is null");
//        }
//        return pconn;
//    }
//    private static synchronized Connection getConnection() {
//        PooledConnection pconn = getPooledConnection();
//        try {
//            return pconn.getConnection();
//        } catch (SQLException ex) {
//            throw new FoxyRuntimeException("getConnection() failed", ex);
//        }
//    }
//    public static synchronized void recycleThreadConnection() {
//        PooledConnection pconn = getPooledConnection();
////        if (pconn == null) {
////            logger.error("from recycleConn: pconn is null!");
////            return;
////        }
////        if (pconn == null) {
////            System.out.println("from recycleConn: pconn is null!");
////        }
//        tlPooledConnection.remove();
//        poolMgr.recycleConnection(pconn);
////        if (logger.isErrorEnabled()) {
////            logger.error("recycled: pconn=" + pconn + " thread=" + Thread.currentThread().getName() + " activeConnections=" + poolMgr.getActiveConnections());
////        }
////        System.out.println("recycled: pconn=" + pconn + " thread=" + Thread.currentThread().getName() + " activeConnections=" + poolMgr.getActiveConnections());
//    }
//    public int readSequenceNextVal(String seqName) {
//        return execQrySingleValInt("select nextval(" + toSqlString(seqName) + ") as nv", false);
//    }
//
//    public String toSqlString(Object o) {
//        if (o instanceof Date) {
//            return "TIMESTAMP " + SimpleDateUtils.toSingleQuotedCanonicalString((Date) o, true);
//        } else if (o instanceof Enum) {
//            return "\'" + ((Enum) o).name() + "\'";
//        } else if (o instanceof Boolean) {
//            return (Boolean) o ? "1" : "0";
//        }
//        return LameUtils.toSqlString(o, SqlFlavor.Postgres);
//    }
//    //private Connection prevConn;
//    public List<Map<String, Object>> execQry(String qryTxt, boolean maxOneRow, boolean minOneRow) {
//        //System.out.println("execQry: " + qryTxt);
//        if (logger.isDebugEnabled()) {
//            //logger.info("execQry: msg1");
//            logger.debug("execQry: starting for qryTxt=\n" + qryTxt);
//        }
//
//        //if (logger.isInfoEnabled()) {
//        //    logger.info("execQry: msg2");
//        //}
//
//        Date startDate = new Date();
//        //if (logger.isInfoEnabled()) {
//        //    logger.info("execQry: start date set");
//        //}
//
//        List<Map<String, Object>> res = new ArrayList<Map<String, Object>>();
//
//        try {
//            Statement st = getConnection().createStatement();
//            try {
//                if (logger.isDebugEnabled()) {
//                    logger.debug("execQry: before execute");
//                }
//                ResultSet rs = st.executeQuery(qryTxt);
//                if (logger.isDebugEnabled()) {
//                    logger.debug("execQry: before read data");
//                }
//
//                try {
//                    while (rs.next()) {
//                        if (maxOneRow && res.size() > 1) {
//                            logger.errorAndThrowNew("query returned more than 1 row, txt=" + qryTxt);
//                        }
//                        res.add(readOneRow(rs));
//                    }
//                } finally {
//                    rs.close();
//                }
//            } finally {
//                st.close();
//            }
//        } catch (SQLException ex) {
//            logger.errorAndThrowNew("sql error, sql=" + qryTxt, ex);
//        }
//
//        if (minOneRow && res.size() < 1) {
//            logger.errorAndThrowNew("query returned no rows, txt=" + qryTxt);
//        }
//
//        Date endDate = new Date();
//
//        if (logger.isDebugEnabled()) {
//            logger.debug("execQry: done");
//        }
//
//        if (logger.isInfoEnabled()) {
//            String msg = "query executed in " + SimpleDateUtils.getElapsedSecondsWithMilliPrec(startDate, endDate) + " s., qryTxt=\n" + qryTxt;
//            //Date endDate2 = new Date();
//
//            logger.info(msg /*+ ", msg prep. (s): " + (endDate2.getTime() - endDate.getTime()) / 1000d*/);
//        }
//        return res;
//    }
//
//    private Map<String, Object> execOneRowQry(String qryTxt) {
//        return execOneRowQry(qryTxt, false);
//    }
//
//    private Map<String, Object> readOneRow(ResultSet rs) {
//        try {
//            Map<String, Object> res = new HashMap<String, Object>();
//
//            ResultSetMetaData metaData = rs.getMetaData();
//            int colCnt = metaData.getColumnCount();
//
//            for (int i = 1; i <= colCnt; i++) {
//                res.put(metaData.getColumnName(i), rs.getObject(i));
//            }
//
//            return res;
//        } catch (SQLException ex) {
//            throw new FoxyRuntimeException("error reading row", ex);
//        }
//    }
//
//    public Integer execCommand(String cmdTxt) {
//        if (logger.isDebugEnabled()) {
//            logger.debug("execCommand: cmdTxt=\n" + cmdTxt);
//        }
//        Date startDate = new Date();
//        Integer updateCount = null;
//        boolean hasResultSet = false;
//        try {
//            Statement st = getConnection().createStatement();
//
//            try {
//                hasResultSet = st.execute(cmdTxt);
//                updateCount = st.getUpdateCount();
//            } finally {
//                st.close();
//            }
//        } catch (SQLException ex) {
//            logger.errorAndThrowNew("error in command execution, txt=" + cmdTxt, ex);
//            //throw new FoxyRuntimeException(ex);
//        }
//        Date endDate = new Date();
//        if (logger.isDebugEnabled()) {
//            logger.debug("execCommand: done");
//        }
//        if (logger.isInfoEnabled()) {
//            logger.info("command executed in " + SimpleDateUtils.getElapsedSecondsWithMilliPrec(startDate, endDate) + " secs, txt=" + cmdTxt + ", updCnt=" + updateCount + ", hasRsltSet=" + hasResultSet);
//        }
//        return updateCount;
//    }
//
////    public <T extends IJsonNamedPropsBean> int saveBean(T bean) {
////        throw new UnsupportedOperationException("Not supported yet.");
////    }
//    public void setAutoCommit(boolean acOn) {
//        try {
//            getConnection().setAutoCommit(acOn);
//        } catch (Exception ex) {
//            throw new LameRuntimeException("error for setAutoCommit(" + acOn + ")", ex);
//        }
//    }
//
//    public void finalizeTran(boolean success) {
//        Connection conn = getConnection();
//        try {
//            if (success) {
//                conn.commit();
//            } else {
//                conn.rollback();
//            }
//        } catch (Exception ex) {
//            throw new LameRuntimeException("error for finalizeTran(" + success + ")", ex);
//        }
//    }
}
