/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class AddSystemUserGroupDialog extends BaseActionOrCancelDialog {

    protected IParametrizedContinuation<String> saveCont;
    protected String groupName;
    protected String caption;

    @Override
    protected String getDialogCaption() {
        return caption;//I18n.dodajGrupe.get();
    }

    public void buildAndShowDialog(String groupName, String caption, IParametrizedContinuation<String> saveCont) {
        this.saveCont = saveCont;
        this.groupName = groupName;
        this.caption = caption;
        super.buildAndShowDialog();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.save.get();
    }
    TextBox tbTytul;

    @Override
    protected void buildMainWidgets() {
        Label lblTitle;

        Grid grid = new Grid(1, 2);
        grid.setCellSpacing(4);

        grid.setWidget(0, 0, lblTitle = new Label(I18n.nazwaGrupy.get() /* I18N:  */ + ":"));
        grid.setWidget(0, 1, tbTytul = new TextBox());
        tbTytul.setValue(groupName);
        lblTitle.setStyleName("addNoteLbl");
        tbTytul.setStyleName("addNote");
        tbTytul.setWidth("500px");

        main.add(grid);
    }

    @Override
    protected void doAction() {
        groupName = tbTytul.getText();
        if (BaseUtils.isStrEmptyOrWhiteSpace(groupName)) {
            Window.alert(I18n.wprowadzNazweGrupy.get());
        } else {
            saveCont.doIt(groupName);
        }
    }

}
