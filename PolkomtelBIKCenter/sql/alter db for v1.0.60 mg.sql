﻿exec sp_check_version '1.0.60';

declare @UQ_name varchar(100)
set @UQ_name = (select name from sys.indexes where object_id = object_id(N'[dbo].[bik_user]') and name like 'UQ__bik_user_%')
exec ('alter table bik_user drop constraint ' + @UQ_name);
go

alter table bik_user
add old_login_name varchar(250) default null
go

alter table bik_user
add login_name varchar(250)
go

update bik_user set login_name = name
go

alter table bik_user
alter column login_name varchar(250) not null
go

alter table bik_user add unique nonclustered 
(
	[login_name] asc
)
go

update bik_node
set is_deleted = 1
from bik_node n
inner join bik_user u on u.node_id = n.id
where u.activ = 0
go

update bik_node
set name = u.name + ' (' + u.login_name + ')'
from bik_node n
inner join bik_user u on u.node_id = n.id
go

update bik_user
set old_login_name = u.login_name, login_name = '~' + convert(varchar, u.id)
from bik_user u
inner join bik_node n on n.id = u.node_id
where n.is_deleted = 1
go

update bik_user
set activ = 0
from bik_node n
inner join bik_user u on u.node_id = n.id
where n.is_deleted = 1
go
  
exec sp_update_version '1.0.60', '1.0.61';
go
