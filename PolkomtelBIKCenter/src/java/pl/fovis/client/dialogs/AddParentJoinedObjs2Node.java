/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.IGridNodeAwareBeanPropsExtractor;
import pl.fovis.client.entitydetailswidgets.JoinedObjBeanExtractor;
import pl.fovis.common.JoinedObjBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.trzy0.foxy.commons.FoxyCommonConsts;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class AddParentJoinedObjs2Node extends BaseActionOrCancelDialog {

    protected TreeNodeBean node;
    protected IParametrizedContinuation saveCont;
    protected Map<CheckBox, Integer> cb2DstId = new HashMap<CheckBox, Integer>();
    protected ScrollPanel parentJoinedObjsSp;
    protected VerticalPanel joinedObjsCbListVp = new VerticalPanel();
    protected List<JoinedObjBean> parentJoinedObjs, nodeJoinedObjs;
    protected Map<Integer, String> allId2Name;
    protected IGridNodeAwareBeanPropsExtractor extr;
    protected ListBox typeLb;

    @Override
    protected String getDialogCaption() {
        return I18n.powiazaniaZWyzszegoPoziomu.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get();
    }

    protected void refreshJoinedObjsCbListPanel(List<JoinedObjBean> filtredJoinedObjs) {
        cb2DstId.clear();
        joinedObjsCbListVp.clear();
        for (final JoinedObjBean bean : filtredJoinedObjs) {
            HorizontalPanel hp = new HorizontalPanel();
            CheckBox cb = new CheckBox();
            hp.add(cb);
            cb2DstId.put(cb, bean.dstId);
            if (extr.hasIcon()) {
                hp.add(BIKClientSingletons.createIconImg(extr.getIconUrl(bean), "16px", "16px", extr.getOptIconHint(bean)));
            }

            HTML nameWidget = new HTML();
            String ancestorPath = "";
            for (String s : BaseUtils.splitBySep(bean.branchIds, "|")) {
                Integer id = BaseUtils.tryParseInteger(s.trim());
                if (id != null) {
                    ancestorPath += ("".equals(ancestorPath) ? "" : FoxyCommonConsts.RAQUO_STR_SPACED) + allId2Name.get(id);
                }
            }
            String treeCode = extr.getTreeCode(bean);
            String menuPathForTreeCode = BIKClientSingletons.getMenuPathForTreeCode(treeCode) + ancestorPath;
            nameWidget.setTitle(menuPathForTreeCode);
            if (BIKClientSingletons.isShowFullPathInJoinedObjsPane()) {
                nameWidget.setHTML(BIKClientSingletons.getTreeBeanByCode(treeCode).name + FoxyCommonConsts.RAQUO_STR_SPACED + ancestorPath);
            } else {
                nameWidget.setHTML(ancestorPath);
            }
            hp.add(nameWidget);
            joinedObjsCbListVp.add(hp);
        }
        refreshSelectedJoinedObjsCb();
    }

    protected List<JoinedObjBean> filtrSelectedType() {
        List<JoinedObjBean> res = new ArrayList<JoinedObjBean>();
        String selectedType = typeLb.getSelectedItemText();
        if (I18n.wszystkie.get().equals(selectedType)) {
            return parentJoinedObjs;
        }
        for (JoinedObjBean bean : parentJoinedObjs) {
            String type = bean.treeKind + FoxyCommonConsts.RAQUO_STR_SPACED + bean.dstType;
            if (type.equals(selectedType)) {
                res.add(bean);
            }
        }
        return res;
    }

    protected void refreshSelectedJoinedObjsCb() {
        if (nodeJoinedObjs != null) {
            for (CheckBox cb : cb2DstId.keySet()) {
                Integer dstId = cb2DstId.get(cb);
                for (JoinedObjBean bean : nodeJoinedObjs) {
                    if (bean.dstId.equals(dstId)) {
                        cb.setValue(true);
                    }
                }
            }
            AddParentJoinedObjs2Node.this.recenterDialog();
        }
    }

    @Override
    protected void buildMainWidgets() {
        parentJoinedObjsSp = new ScrollPanel();
        typeLb = new ListBox();
        main.add(typeLb);
        main.add(parentJoinedObjsSp);
        BIKClientSingletons.getService().getJoinedObjs(node.parentNodeId, null, new StandardAsyncCallback<List<JoinedObjBean>>() {

            @Override
            public void onSuccess(final List<JoinedObjBean> result) {
                parentJoinedObjs = result;

                parentJoinedObjsSp.add(joinedObjsCbListVp);
                parentJoinedObjsSp.setHeight("300px");
                Set<Integer> idsToFetch = new HashSet<Integer>();
                Set<String> allParentJoinedObjsTypes = new HashSet<String>();

                typeLb.addItem(I18n.wszystkie.get());
                for (JoinedObjBean bean : result) {
                    String type = bean.treeKind + FoxyCommonConsts.RAQUO_STR_SPACED + bean.dstType;
                    if (!allParentJoinedObjsTypes.contains(type)) {
                        allParentJoinedObjsTypes.add(type);
                        typeLb.addItem(type);
                    }
                    for (String s : BaseUtils.splitBySep(bean.branchIds, "|")) {
                        if (!BaseUtils.isStrEmptyOrWhiteSpace(s)) {
                            Integer id = BaseUtils.tryParseInteger(s.trim());
                            if (id != null) {
                                idsToFetch.add(id);
                            }
                        }
                    }
                }
                typeLb.setSelectedIndex(0);
                typeLb.addChangeHandler(new ChangeHandler() {

                    @Override
                    public void onChange(ChangeEvent event) {
                        refreshJoinedObjsCbListPanel(filtrSelectedType());
                    }
                });
                BIKClientSingletons.getService().getNodeNamesByIds(idsToFetch, new StandardAsyncCallback<Map<Integer, String>>() {
                    @Override
                    public void onSuccess(Map<Integer, String> id2Name) {
                        allId2Name = id2Name;
                        refreshJoinedObjsCbListPanel(filtrSelectedType());
                        BIKClientSingletons.getService().getJoinedObjs(node.id, null, new StandardAsyncCallback<List<JoinedObjBean>>() {

                            @Override
                            public void onSuccess(List<JoinedObjBean> result) {
                                nodeJoinedObjs = result;
                                refreshSelectedJoinedObjsCb();
                            }
                        });
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        super.onFailure(caught);
                    }

                });
            }
        });
    }

    @Override
    protected void doAction() {
        Set<Integer> dstId2Add = new HashSet<Integer>();
        Set<Integer> dstId2Delete = new HashSet<Integer>();
        for (CheckBox cb : cb2DstId.keySet()) {
            if (cb.getValue()) {
                dstId2Add.add(cb2DstId.get(cb));
            } else {
                dstId2Delete.add(cb2DstId.get(cb));
            }
        }
        saveCont.doIt(new Pair<Set<Integer>, Set<Integer>>(dstId2Add, dstId2Delete));
    }

    public void buildAndShowDialog(TreeNodeBean node, IParametrizedContinuation<Pair<Set<Integer>, Set<Integer>>> saveCont) {
        this.node = node;
        this.saveCont = saveCont;
        this.extr = new JoinedObjBeanExtractor(false, node.parentNodeId);
        super.buildAndShowDialog();
    }
}
