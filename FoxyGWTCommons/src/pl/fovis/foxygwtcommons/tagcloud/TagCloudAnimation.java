package pl.fovis.foxygwtcommons.tagcloud;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.foxygwtcommons.GWTUtils;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Tuple3;

// i18n-default: no
public class TagCloudAnimation extends Animation {

    public static final double SPEED_DEGRADATION = 0.98;
    private List<Widget> tags;
    private TagSphereCoords coords;
    public double speedX;
    public double speedZ;
    private AbsolutePanel absolutePanel;
    private int radius;
    private IContinuation whenCompleted;
    private double offsetX;
    private double offsetZ;
    public double currentX;
    public double currentZ;
    private double lastProgress;
    private boolean hasMouse;
    private Label lblAnimationStatus;

    public TagCloudAnimation(List<Widget> tags, int radius,
            double offsetX, double offsetZ,
            double speedX, double speedZ, TagSphereCoords coords,
            AbsolutePanel absolutePanel, IContinuation whenCompleted,
            Label lblAnimationStatus, boolean hasMouse) {
        super();
        this.tags = tags;
        this.coords = coords;
        this.offsetX = offsetX;
        this.offsetZ = offsetZ;
        this.currentX = offsetX;
        this.currentZ = offsetZ;
        this.speedX = speedX;
        this.speedZ = speedZ;
        this.radius = radius;
        this.absolutePanel = absolutePanel;
        this.whenCompleted = whenCompleted;
        this.lblAnimationStatus = lblAnimationStatus;
        this.hasMouse = hasMouse;
        //lblAnimationStatus.setText("??");
    }

    public void changeSpeed(double newX, double newZ, boolean hasMouse) {
        offsetX = currentX - lastProgress * newX;
        offsetZ = currentZ - lastProgress * newZ;
        speedX = newX;
        speedZ = newZ;
        this.hasMouse = hasMouse;
    }

    @Override
    protected void onComplete() {
        super.onComplete();
        whenCompleted.doIt();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onUpdate(double progress) {
        if (!hasMouse) {
            changeSpeed(speedX * SPEED_DEGRADATION, speedZ * SPEED_DEGRADATION, hasMouse);
        }
        lastProgress = progress;
        currentX = speedX * progress + offsetX;
        currentZ = speedZ * progress + offsetZ;

        Tuple3<Integer, Integer, Double>[] points = coords.moveTo(currentX, currentZ);

        if (lblAnimationStatus != null) {
            lblAnimationStatus.setText("speedX=" + BaseUtils.doubleToString(speedX, 4)
                    + ", speedZ=" + BaseUtils.doubleToString(speedZ, 4)
                    + ", currentX=" + BaseUtils.doubleToString(currentX, 4)
                    + ", currentZ=" + BaseUtils.doubleToString(currentZ, 4)
                    + ", " + "progress" /* I18N: no */ + "=" + BaseUtils.doubleToString(progress, 4));
        }

        boolean first = true;

        for (int i = 0; i < tags.size(); i++) {
            Tuple3<Integer, Integer, Double> point = points[i];
            if (lblAnimationStatus != null) {
                lblAnimationStatus.setText(lblAnimationStatus.getText()
                        + ", " + "point" /* I18N: no */ + "#" + i + "=" + point);
            }
            Widget w = tags.get(i);
            int procInt = (int) (point.v3 * 100);
            int fontSize = (int) (46 * point.v3);
            Style s = w.getElement().getStyle();
            s.setProperty("fontSize", fontSize + "px");
            s.setProperty("zIndex", Integer.toString(procInt));
            //int colorValue = (int) (255 - point.v3 * 255);
            //String colorPartStr = Integer.toHexString(colorValue);
            //if (colorPartStr.length() < 2) {
            //    colorPartStr = "0" + colorPartStr;
            //}
            //String color = "#" + colorPartStr + colorPartStr + colorPartStr;
            //s.setProperty("color", color);

            GWTUtils.setOpacity(w.getElement(), 40 + procInt / 2);

            absolutePanel.setWidgetPosition(w,
                    absolutePanel.getOffsetWidth() / 2 + point.v1 - w.getOffsetWidth() / 2,
                    absolutePanel.getOffsetHeight() / 2 + point.v2 - w.getOffsetHeight() / 2);

            if (first) {
                first = false;
                if (lblAnimationStatus != null) {
                    lblAnimationStatus.setText(lblAnimationStatus.getText()
                            + ", " + "radius" /* I18N: no */ + "=" + radius + ", point.v1=" + point.v1
                            + ", point.v2=" + point.v2 + ", point.v3=" + point.v3
                            + ", fontSize=" + fontSize + ", " + "opacity" /* I18N: no */ + ": " + procInt
                            + ", " + "widget" /* I18N: no */ + "=" + w);
                }
            }
        }
    }

    @Override
    protected double interpolate(double progress) {
        return progress;
    }
}
