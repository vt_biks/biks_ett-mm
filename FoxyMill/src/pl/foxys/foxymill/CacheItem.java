/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.foxys.foxymill.tags.CacheTag;

/**
 *
 * @author wezyr
 */
public class CacheItem {
    // n+1 items
    public List<String> renderedParts = new ArrayList<String>();
    // n items
    public List<CacheTag> subCacheTags = new ArrayList<CacheTag>();
    // n items
    public List<Map<String, Object>> subCacheVars = new ArrayList<Map<String, Object>>();
}
