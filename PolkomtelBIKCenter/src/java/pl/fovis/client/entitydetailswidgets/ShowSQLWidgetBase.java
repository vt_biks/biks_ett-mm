/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.IContinuation;

/**
 *
 * @author tflorczak
 */
public abstract class ShowSQLWidgetBase extends EntityDetailsWidgetFlatBase {

    protected VerticalPanel gridPanel;
    protected BikCustomButton btnShowSQL;

    @Override
    public boolean hasContent() {
        return getSQLTextFromData() != null;
    }

    @Override
    public void displayData() {
        gridPanel.clear();
        final String bean = getSQLTextFromData();
        if (bean != null) {
            btnShowSQL = new BikCustomButton(I18n.pokazSQL.get() /* I18N:  */, "lookupTrashBtn",
                    new IContinuation() {

                        public void doIt() {
                            getForms().viewSQLDef("SQL" /* I18N: no */, bean);
                        }
                    });
            gridPanel.add(btnShowSQL);
        }
    }

    protected abstract String getSQLTextFromData();

    public Widget buildWidgets() {
        gridPanel = new VerticalPanel();
        return gridPanel;
    }
}
