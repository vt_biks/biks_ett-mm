﻿exec sp_check_version '1.8.4.3';
go

update bik_attr_def set name = 'metaBIKS.Treść pomocy' where name='metaBIKS.Opis'
update bik_attr_def set value_opt = 'hyperlinkInMono,hyperlinkInMulti,hyperlinkOut,data,combobox,checkbox,comboBooleanBox,calculation,javascript,selectOneJoinedObject,ansiiText,longText,shortText' where name in ('metaBIKS.Typ atrybutu', 'metaBIKS.Nadpisany typ atrybutu')

exec sp_update_version '1.8.4.3', '1.8.4.4';
go
