/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.postgresql;

import commonlib.LameUtils;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import pl.bssg.metadatapump.JdbcTestConnectionBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.common.ServerConfigBean;
import static pl.bssg.metadatapump.postgresql.PostgreSQLPump.POSTGRES_DEFAULT_DATABASE_NAME;
import pl.trzy0.foxy.commons.PostgresConnectionConfig;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import pl.trzy0.foxy.serverlogic.db.PostgresConnection;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class PostgreSQLTestConnection extends JdbcTestConnectionBase {

//    protected int id;
    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected ServerConfigBean bean;
    protected Connection conn;

    public PostgreSQLTestConnection(MssqlConnectionConfig mcfg, int id, String optObjsForGrantsTesting) {
        super(mcfg, id, optObjsForGrantsTesting);
//        this.id = id;
    }

    @Override
    protected Pair<Integer, String> testConnectionInner() {
        try {
            bean = adhocDao.createBeanFromNamedQry("getDBConfigForDBAndId", ServerConfigBean.class, true, PumpConstants.SOURCE_NAME_POSTGRESQL, id);
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Invalid login for data loading! Error: " + e.getMessage());
        }
//        Boolean showGrantsInTestConnection = BaseUtils.tryParseBoolean((String) execNamedQuerySingleVal("getAppPropValue", true, "val", "showGrantsInTestConnection"), false);
        String version = "";
        try {
            String database = POSTGRES_DEFAULT_DATABASE_NAME;
            List<String> databasesFilterDatabases = BaseUtils.splitBySep(bean.databaseFilter, ",", true);
            boolean notConnectingWithSystemDB = !databasesFilterDatabases.isEmpty();
            if (notConnectingWithSystemDB) {
                database = databasesFilterDatabases.get(0);
            }
            conn = PumpUtils.getConnection(PostgreSQLPump.DRIVER_CLASS, "jdbc:postgresql://" + bean.server + (bean.port != null ? ":" + bean.port : "") + "/" + database, bean.user, bean.password);
            if (conn == null) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connecting with Postgres DB: " + database);
            }
            if (showGrantsInTestConnection()) {
                bce = new PostgresConnection(new PostgresConnectionConfig(bean.server, database, bean.user, bean.password, bean.port));
            }
            Statement statment = conn.createStatement();
            String query;
//            if (notConnectingWithSystemDB) {
            query = "select version() as version;";
//            } else {
//                query = "select datname from pg_database where datistemplate = false";
//            }
            ResultSet execQry = statment.executeQuery(query);
            if (execQry == null || !execQry.next()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No access to database: " + database);
            }
            version = execQry.getString("version");
            if (showGrantsInTestConnection()) {
                StringBuilder sbGrant = new StringBuilder();
                ResultSet grants = statment.executeQuery("SELECT table_name, privilege_type \n"
                        + "FROM information_schema.role_table_grants \n"
                        + "where grantee = '" + bean.user + "'");
                sbGrant.append(database).append(": \n");
                while (grants.next()) {
                    String grantee = grants.getString("table_name");
                    String type = grants.getString("privilege_type");
                    sbGrant.append("   ").append(grantee).append(" - ").append(type).append("\n");
                }
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, "OK, version: " + version + " \nGrants:\n" + sbGrant.toString());
            }
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error with PostreSQL test connection", e);
            }
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connecting with Postgres DB! Error: " + e.getMessage());
        } finally {
            if (conn != null) {
                PumpUtils.closeConnection(conn);
            }
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, "OK, version: " + version);
    }
}
