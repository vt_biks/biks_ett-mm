/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

/**
 *
 * @author tflorczak
 */
public class DQMConstanst {

    public static final String DQM_EXPORT_PARAM_NAME = "dqmexportresults";
    public static final String DQM_EXPORT_REPORT_PARAM_NAME = "dqmexportreport";
    public static final String DQM_EXPORT_REQUEST_ID_PARAM_NAME = "requestid";
    public static final String DQM_TEMP_TABLE_PREFIX = "dqm_tmp_";

}
