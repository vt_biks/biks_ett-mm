/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.tags;

import pl.trzy0.foxy.commons.FoxyRuntimeException;

/**
 *
 * @author wezyr
 */
public class FoxyTagsRuntimeException extends FoxyRuntimeException {
    public FoxyTagsRuntimeException(Throwable cause) {
        super(cause);
    }

    public FoxyTagsRuntimeException(String errMsg) {
        super(errMsg);
    }

    public FoxyTagsRuntimeException(String errMsg, Throwable cause) {
        super(errMsg, cause);
    }
}
