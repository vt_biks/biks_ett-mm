/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.TreeSelector2ExportDialog;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class NodeActionSaveTree extends NodeActionUpdatingBase {

    @Override
    public boolean isEnabledByDesign() {

        if (treeBean == null) {
            return false;
        }
//        return true;

        return /*(BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_IMPORT_TREE, treeBean.id)
                || BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_TO_HTML, treeBean.id)
                || BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_TO_PDF, treeBean.id))
                && */ !BIKClientSingletons.disableImportExport()
                && !"DictionaryDWH".equalsIgnoreCase(treeBean.code) && !isMetaBiks();

    }

    @Override
    public String getCaption() {
        return I18n.eksportGeneruj.get();
    }

    @Override
    public void execute() {
        TreeSelector2ExportDialog dialog = new TreeSelector2ExportDialog(BIKClientSingletons.getTreeIdOfCurrentTab(), node.branchIds);
        dialog.buildAndShowDialog();
    }

}
