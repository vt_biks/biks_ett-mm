/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.IResourceNamesProvider;

/**
 *
 * @author wezyr
 */
public class FileNamesProvider implements IResourceNamesProvider {

    //ww: inicjaln� �cie�k� bazow� dodajemy jak jest
    // czyli rozr�niamy np. przypadek "c:/" od "c:" - w gr� wchodz� katalogi
    // robocze na dyskach w windows
    private String basePath;

    // �cie�ka bazowa - katalog, mo�e miec / na ko�cu, ale nie musi
    public FileNamesProvider(String basePath) {
        this.basePath = basePath;
    }

    public FileNamesProvider() {
    }

    protected String getFullPath(String filePath) {
        String res = basePath == null ? filePath
                : (BaseUtils.isStrEmpty(filePath) ? basePath : BaseUtils.ensureDirSepPostfix4C(basePath) + filePath);
        //System.out.println("fullPath=" + res);
        return res;
    }

    public Set<String> getNames(String path) {
        path = BaseUtils.dropOptionalSuffix(path, "/");
        //System.out.println("getNames: " + basePath);
        String[] nn = new File(getFullPath(path)).list();

        Set<String> res = new LinkedHashSet<String>();

        for (String n : nn) {
            //System.out.println("getNames: for " + basePath + ", n=" + n);
            res.add(BaseUtils.isStrEmpty(path) ? n : path + "/" + n);
        }

        return res;
    }

    public boolean isDirectory(String name) {
        return new File(getFullPath(name)).isDirectory();
    }
    private static FileNamesProvider instance;

    public static synchronized FileNamesProvider getInstance() {
        if (instance == null) {
            instance = new FileNamesProvider();
        }
        return instance;
    }
}
