﻿exec sp_check_version '1.0.99.8';
go
------------------------------------
------------------------------------
--usunięcie podwójnych hintów dla atrybutów
delete 
from bik_attr_hint
where id not in (

select  min(id)
from bik_attr_hint
group by name
having count(*)>1)  

and name in (select  name
from bik_attr_hint
group by name
having count(*)>1)
go

--dodanie unique dla nazw hintów
alter table bik_attr_hint
	add constraint UQ_bik_attr_hint_name unique(name)
go

------------------------------------
------------------------------------
exec sp_update_version '1.0.99.8', '1.1';
go