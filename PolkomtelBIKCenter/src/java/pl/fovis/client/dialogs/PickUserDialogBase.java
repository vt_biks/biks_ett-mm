/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.foxygwtcommons.BasePickerDialog;
import java.util.Map;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 * 
 */
public abstract class PickUserDialogBase<T> extends BasePickerDialog<T> {

    protected abstract String getName(T b);

    protected abstract Integer getId(T b);

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzUzytkownika.get() /* I18N:  */;
    }

    @Override
    protected void setupListGridRecord(T item, Map<String, Object> record) {
        record.put("name" /* I18N: no */, getName(item)/*item.userName*/);
        record.put("id" /* I18N: no */, Integer.toString(getId(item)/*item.id*/));
    }

    @Override
    protected String[] createFields() {
        return createFields("name=" /* i18n: no */ + I18n.nazwa.get() /* I18N:  */, "id=id" /* I18N: no */);
    }

    @Override
    protected String getIdFieldName() {
        return "id" /* I18N: no */;
    }

    @Override
    protected String getListBoxLabelCaption() {
        return I18n.uzytkownicy.get() /* I18N:  */+ ":";
    }
}
