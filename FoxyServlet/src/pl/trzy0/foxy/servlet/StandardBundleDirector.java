/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import simplelib.MapHelper;

/**
 *
 * @author wezyr
 */
public class StandardBundleDirector implements IBundleDirector {

    protected StandardBundleDerivedLogic createRawLogic() {
        return new StandardBundleDerivedLogic();
    }

    public IBundleDerivedLogic createLogic(MapHelper<String> sysProps
             /*FoxyMillBundleData bundleData*/) {
        StandardBundleDerivedLogic sbdl = createRawLogic();
        sbdl.preInit(sysProps);
        //sbdl.init(bundleData);
        return sbdl;
    }
}
