/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PopupPanel;

/**
 *
 * @author ctran
 */
public class Tooltip extends PopupPanel {

    private static final int DEFAULT_OFFSET_X = 10;
    private static final int DEFAULT_OFFSET_Y = 35;
    private int delay = 1000;
    private HTML contents;

    public Tooltip() {
        super(true);

        this.delay = delay;

        contents = new HTML();
        add(contents);
    }

    public void show() {
        super.show();

        Timer t = new Timer() {

            public void run() {
                Tooltip.this.hide();
            }

        };
        t.schedule(delay);
    }

    public void setText(String text) {
        contents.setHTML(text);
    }
}
