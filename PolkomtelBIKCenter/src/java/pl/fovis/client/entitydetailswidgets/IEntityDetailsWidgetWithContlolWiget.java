/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author pgajda
 */
public interface IEntityDetailsWidgetWithContlolWiget extends IEntityDetailsWidget {
    
    public abstract Widget buildContlolWidgets();
    
}
