/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.tags;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.foxys.foxymill.CacheItem;
import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.exprlang.IParsedExpression;
import pl.foxys.foxymill.IRenderable;
import simplelib.IValueFormatter;
import simplelib.RawString;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import pl.foxys.foxymill.renderctx.RenderCtx;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public class InsertTag extends TagWithExpr {

    private static final Set<String> nonParams = BaseUtils.paramsAsSet("expr", "var", "raw");
    private String var;
    private String raw;
    private boolean asStr;
    private Map<String, IParsedExpression> params = new LinkedHashMap<String, IParsedExpression>();
    private Map<String, IParsedExpression> outVars = new LinkedHashMap<String, IParsedExpression>();
    //private IParsedExpression parsedExpr;

//    public InsertTag(Map<String, String> attrs, List<ITag> subTags) {
//        this.expr = attrs.get("expr");
//        System.out.println("insert expr=" + expr);
//    }
    @Override
    protected void renderWithExprVal(//IRenderContext
            IRenderCtx renderCtx, Object val) {
        //Object val = parsedExpr.eval(renderCtx);

//        Object rm = renderCtx.getRootModel();
//
//        if (rm instanceof IRenderingAbortAwareModel) {
//            IRenderingAbortAwareModel raam = (IRenderingAbortAwareModel)rm;
//            Boolean doAbort = raam.getReturnFromRenderingFlag();
//            if (doAbort != null) {
//                throw new ReturnFromRenderingException(doAbort);
//            }
//        }
//
//        if (rm instanceof IRerenderCapableModel) {
//            IRerenderCapableModel rcm = (IRerenderCapableModel)rm;
//            Object newModel = rcm.getRerenderModel();
//            if (newModel != null) {
//                throw new RerenderWithNewModelException(newModel);
//            }
//        }

        if (val instanceof IRenderable) {
            IRenderable r = (IRenderable) val;

//            SimpleRenderContext newRenderCtx = new SimpleRenderContext(renderCtx//.getEvalContext()
//                    );

            Map<String, Object> newVars = new HashMap<String, Object>();

            for (Entry<String, IParsedExpression> e : params.entrySet()) {
                newVars.put(e.getKey(), e.getValue().eval(renderCtx));
            }

            if (subTags != null) {
                newVars.put("body", FoxyMillUtils.wrapAsRenderable(subTags, renderCtx.getVars()));
            }

            IRenderCtx newRenderCtx = RenderCtx.makeRenderCtx(renderCtx.getRootModel(),
                    newVars, renderCtx.getOutputDevice());
            if (var != null) {

                Object renderVal;

                if (asStr) {
                    renderVal = FoxyMillUtils.renderToString(renderCtx, newVars, r);
                } else {
                    CacheItem cacheItem = FoxyMillUtils.renderToCacheItem(//renderCtx,
                            renderCtx.getRootModel(), newVars,
                            r);
                    renderVal = cacheItem;
                }

                renderCtx.getVars().put(var, renderVal);
                // brakuje obsługi outVars!!!
            } else {
                r.render(newRenderCtx);
            }
            for (Entry<String, IParsedExpression> e : outVars.entrySet()) {
                renderCtx.getVars().put(e.getKey(), e.getValue().eval(newRenderCtx));
            }
            //r.internalRender(newRenderCtx);

        } else if (val instanceof CacheItem) {
            CacheItem cacheItem = (CacheItem) val;
            if (var == null) {
                FoxyMillUtils.renderCacheItem(cacheItem, renderCtx.getRootModel(), renderCtx.getOutputDevice(), true);
            } else {
                renderCtx.getVars().put(var, cacheItem);
            }
        } else {
            IValueFormatter vf;

            Object model = renderCtx.getRootModel();
            if (model instanceof IValueFormatter) {
                vf = (IValueFormatter) model;
            } else {
                vf = renderEnv.getValueFormatter();
            }

            String valStr;

            if (!BaseUtils.safeEquals(raw, "true") && !(val instanceof RawString)) {
                valStr = vf.valToString(val);
            } else {
                valStr = BaseUtils.safeToString(val);
            }

//            String valStr;
//
//            if (val instanceof Date) {
//                valStr = SimpleDateUtils.toISOString((Date) val);
//            } else {
//                valStr = BaseUtils.safeToString(val);
//            }
//
//            if (!BaseUtils.safeEquals(raw, "true") && !(val instanceof RawString)) {
//                valStr = LameUtils.encodeForHTMLTag(valStr);
//            }

            if (var == null) {
                renderCtx.getOutputDevice().append(valStr);
            } else {
                renderCtx.getVars().put(var, new RawString(valStr));
            }
        }
    }

    @Override
    protected void prepareSelf() {
        super.prepareSelf();
        //parsedExpr = attrToExpr("expr"); //exprLang.parseExpression(expr);

        for (Entry<String, String> e : attrs.entrySet()) {
            String paramName = e.getKey();
            String paramValExpr = e.getValue();
            if (!nonParams.contains(paramName)) {
                IParsedExpression parsedParamVal = exprLang.parseExpression(paramValExpr, paramName, tfp);
                //exprLang.parseExpression(paramValExpr);
                String outVarName = BaseUtils.dropPrefixOrGiveNull(paramName, "out:");

                if (outVarName != null) {
                    outVars.put(outVarName, parsedParamVal);
                } else {
                    params.put(paramName, parsedParamVal);
                }
            }
        }
    }

    @Override
    protected void initSelf() {
        this.var = attrs.get("var");
        this.raw = attrs.get("raw");
        this.asStr = BaseUtils.safeEquals(attrs.get("asStr"), "true");
    }
}
