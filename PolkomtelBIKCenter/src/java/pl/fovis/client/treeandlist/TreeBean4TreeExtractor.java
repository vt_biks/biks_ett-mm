/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import pl.fovis.common.TreeNodeBean;

/**
 *
 * @author tflorczak
 */
public class TreeBean4TreeExtractor implements IBean4TreeExtractor<TreeNodeBean, Integer> {

    @Override
    public Integer extractId(TreeNodeBean b) {
        return b.id;
    }

    @Override
    public Integer extractParentId(TreeNodeBean b) {
        return b.parentNodeId;
    }

    @Override
    public boolean extractHasNoChildren(TreeNodeBean b) {
        return b.hasNoChildren;
    }

    @Override
    public String extractName(TreeNodeBean b) {
        return b.name;
    }

    @Override
    public int extractVisualOrder(TreeNodeBean b) {
        return b.visualOrder;
    }

    @Override
    public boolean extractIsFolder(TreeNodeBean b) {
        return b.isFolder;
    }

    @Override
    public String extractBranchIds(TreeNodeBean b) {
        return b.branchIds;
    }
}
