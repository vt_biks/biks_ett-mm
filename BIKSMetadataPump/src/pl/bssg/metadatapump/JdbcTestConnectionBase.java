/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public abstract class JdbcTestConnectionBase extends MetadataTestConnectionBase {

    protected int id;
    protected String optObjsForGrantsTesting;
    protected IBeanConnectionEx<Object> bce = null;

    public JdbcTestConnectionBase(MssqlConnectionConfig mcfg, int id, String optObjsForGrantsTesting) {
        super(mcfg);
        this.id = id;
        this.optObjsForGrantsTesting = optObjsForGrantsTesting;
//        this.showGrantsInTestConnection = showGrantsInTestConnection();
    }

    public JdbcTestConnectionBase() {
    }

    @Override
    public Pair<Integer, String> testConnection() {
        Pair<Integer, String> res = testConnectionInner();

        return new Pair<Integer, String>(res.v1, res.v2 + getPerObjTestingResult());
    }

    protected abstract Pair<Integer, String> testConnectionInner();

    protected String getPerObjTestingResult() {
        return BaseUtils.isStrEmptyOrWhiteSpace(optObjsForGrantsTesting) || !showGrantsInTestConnection() ? "" : "\nPer obj testing: \n" + optTestGrantsPerObjects();
    }

    protected String optTestGrantsPerObjects() {
        return BeanConnectionUtils.optTestGrantsPerObjects(bce, optObjsForGrantsTesting);

//        if (BaseUtils.isStrEmptyOrWhiteSpace(optObjsForGrantsTesting)) {
//            return "";
//        }
//
//        List<String> okObjs = new ArrayList<String>();
//        List<String> errObjs = new ArrayList<String>();
//        String objF = null;
//        StringBuilder sb = new StringBuilder();
//
//        try {
//            sb.append("objs for testing: \"").append(optObjsForGrantsTesting).append("\"\n");
//
//            String o = optObjsForGrantsTesting.trim();
//
//            List<String> objs = BaseUtils.splitBySep(o.substring(1), o.substring(0, 1), true);
//
//            for (String obj : objs) {
//                try {
//                    objF = obj;
//                    bce.execOneRowQry("select * from " + obj + " where 1 = 0", true, IRawConnection.FieldNameConversion.ToLowerCase);
//                    okObjs.add(obj);
//                } catch (Exception ex) {
//                    errObjs.add(obj);
//                    sb.append(obj).append(": ").append(StackTraceUtil.getCustomStackTrace(ex)).append("\n");
//                }
//            }
//
//        } catch (Exception ex) {
//            try {
//                sb.append("\nfatal error while processing objF=").append(objF).append(":\n").append(StackTraceUtil.getCustomStackTrace(ex));
//            } catch (Exception exx) {
//                exx.printStackTrace();
//                ex.printStackTrace();
//                return "\ntotal fatal error in optTestGrantsPerObjects\n";
//            }
//        }
//        return "OkObjs: " + okObjs + "\n" + "ErrObjs: " + errObjs + "\n" + (sb.length() == 0 ? "No grant errors" : "Grant errors: \n" + sb.toString());
    }
    private Boolean showGrantsInTestConnectionx;

    protected boolean showGrantsInTestConnection() {

        if (showGrantsInTestConnectionx == null) {
            try {
                showGrantsInTestConnectionx = BaseUtils.tryParseBoolean((String) execNamedQuerySingleVal("getAppPropValue", true, "val", "showGrantsInTestConnection"), false);
            } catch (Exception ex) {
                //no-op
            }
            if (showGrantsInTestConnectionx == null) {
                showGrantsInTestConnectionx = false;
            }
        }

        return showGrantsInTestConnectionx;
    }
}
