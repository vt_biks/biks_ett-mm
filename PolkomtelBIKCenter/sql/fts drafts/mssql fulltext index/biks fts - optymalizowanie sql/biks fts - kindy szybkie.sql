-- drop table #xxx

-- 56	Tabela pochodna �wiata obiekt�w	646
-- 39	Zapytanie raportowe	750

declare @tab table (node_kind_id int not null)

insert into @tab
    select bn.node_kind_id
    --bn.*, 
    --sr.sum_combined_rank, sr.max_combined_rank, sr.sum_search_weight, sr.max_search_weight
	--into #xxx    
    from
    bik_node bn
    
        inner join
        (select av.node_id        
        from bik_searchable_attr_val av inner join
        containstable(bik_searchable_attr_val, *, '"id" AND "konto"') sr on av.id = sr.[key]        
        group by node_id) sr on sr.node_id = bn.id
    
    where bn.is_deleted = 0 and (bn.linked_node_id is null or bn.disable_linked_subtree <> 0)
    and bn.tree_id in (1,2)
    and  bn.node_kind_id in (39,56)

--------------------------------------------------

    select
    bn.node_kind_id, bnk.caption,
    count(*) as cnt
    from @tab as bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    group by
    bn.node_kind_id, bnk.caption
    order by bnk.caption
