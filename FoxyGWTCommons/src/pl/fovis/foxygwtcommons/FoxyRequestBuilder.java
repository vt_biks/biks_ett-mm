/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import java.util.Date;

/**
 *
 * @author pmielanczuk
 */
public class FoxyRequestBuilder extends RequestBuilder {

    public Date requestSendStart;

    public FoxyRequestBuilder(Method httpMethod, String url) {
        super(httpMethod, url);
    }

    @Override
    public Request sendRequest(String requestData, RequestCallback callback) throws RequestException {
        requestSendStart = new Date();
        return super.sendRequest(requestData, callback);
    }

    @Override
    public Request send() throws RequestException {
        requestSendStart = new Date();
        return super.send();
    }
}
