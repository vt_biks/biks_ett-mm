/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import com.google.gwt.user.client.Window;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.LameRuntimeException;

/**
 *
 * @author ctran
 */
public class NodeActionMetaMenuApply extends NodeActionBase {

    @Override
    public boolean isEnabledByDesign() {
        return nodeKind.code.equalsIgnoreCase(BIKConstants.NODE_KIND_META_MENU_CONF_MAIN_MENU);
    }

    @Override
    public String getCaption() {
        return I18n.zastosujMenu.get() /* I18N:  */;
    }

    @Override
    public void execute() {
        final BIKSProgressInfoDialog dialog = new BIKSProgressInfoDialog();
        dialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaPostepowanie.get(), true);
        BIKClientSingletons.getService().implementMenuConfigTree(new StandardAsyncCallback<Void>() {

            @Override
            public void onSuccess(Void result) {
                dialog.hideDialog();
                BIKClientSingletons.showInfo(I18n.proszeOdswiezycPrzegladarke.get());
            }

            @Override
            public void onFailure(Throwable caught) {
                dialog.hideDialog();
                LameRuntimeException e = (LameRuntimeException) caught;
                Window.alert(e.getMessage());
            }
        });
    }
}
