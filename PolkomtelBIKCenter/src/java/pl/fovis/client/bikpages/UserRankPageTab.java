/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.IPaginatedItemsDisplayer;
import pl.fovis.client.bikwidgets.UserRankPageGrid;
import pl.fovis.common.NodeAuthorBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class UserRankPageTab
        extends MyBIKSTabWithPaginatorBase<NodeAuthorBean> {

    public String getCaption() {
        return I18n.rankingUzytkownikow.get() /* I18N:  */;
    }

    public String getButtonStyle() {
        return "labelLink-rank";
    }

    @Override
    protected int getPageSize() {
        return 20;
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<NodeAuthorBean>> callback) {
        BIKClientSingletons.getService().getRankUsers(callback);
    }

    @Override
    protected IPaginatedItemsDisplayer<NodeAuthorBean> createPaginatedItemsDisplayer() {
        return new UserRankPageGrid();
    }

    @Override
    protected String getCaptionWhenListEmpty() {
        return I18n.brakDanychDoZbudowanRankinguRank.get() /* I18N:  */+ ".";
    }
}
