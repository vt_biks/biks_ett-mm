if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_node'))
drop fulltext index on bik_node
go

if exists(select 1 from sys.fulltext_catalogs where name = 'BIKS_FTS_Catalog')
DROP FULLTEXT CATALOG BIKS_FTS_Catalog;
go

CREATE FULLTEXT CATALOG BIKS_FTS_Catalog AS DEFAULT;
go
declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_node') and is_primary_key = 1)
exec('CREATE FULLTEXT INDEX ON bik_node (name, descr) key index ' + @pk_name)
go
