/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.Label;
import pl.fovis.common.NameDescBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class EditDescriptionDialog extends BaseRichTextAndTextboxDialog {

    protected IParametrizedContinuation<NameDescBean> saveCont;

    @Override
    protected void addLabelsTitle() {
        lblTitle = new Label(I18n.nazwa.get() /* I18N:  */ + ":");
        lblArea = new Label(I18n.opis.get() /* I18N:  */ + ":");
    }

    @Override
    protected String getDialogCaption() {
        return I18n.edytujElement.get() /* I18N:  */;
    }

    @Override
    protected void innerDoAction(String title, String body) {
        NameDescBean nameDesc = new NameDescBean();
        nameDesc.content = /*deleteBadURL(*/ body/*)*/;
        nameDesc.subject = title;
        saveCont.doIt(nameDesc);
    }

    @Override
    protected void buildMainWidgets() {
        super.buildMainWidgets(); //To change body of generated methods, choose Tools | Templates.
        txtTitle.setEnabled(false);
    }

    public void buildAndShowDialog(NameDescBean nameDesc, IParametrizedContinuation<NameDescBean> saveCont) {
        this.saveCont = saveCont;
        super.buildAndShowDialog(nameDesc != null ? nameDesc.subject : null,
                nameDesc != null ? nameDesc.content : null, true);
    }
}
