set nocount on

if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#ref'))
drop table #ref
go

create table #ref (ree_table sysname null, ree_column sysname null, red_table sysname null, red_column sysname null, lvl int not null)

insert into #ref (ree_table, lvl) values ('bik_node', 0)

declare @lvl int = 0

while 1 = 1
begin
  set @lvl = @lvl + 1

  declare @tab table (ree_table sysname null, ree_column sysname null, red_table sysname null, red_column sysname null)
  delete from @tab

  insert into @tab (ree_table, ree_column, red_table, red_column)
  select distinct table_name, name, referencedtablename, ReferencedColumn
  from vw_ww_foreignkeys
  where ReferencedTableName in (select ree_table from #ref)
  and ReferencedTableName not in (select red_table from #ref where red_table is not null)

  if @@rowcount = 0 break  
  
  insert into #ref (ree_table, ree_column, red_table, red_column, lvl)
  select ree_table, ree_column, red_table, red_column, @lvl from @tab
end

select * from #ref
