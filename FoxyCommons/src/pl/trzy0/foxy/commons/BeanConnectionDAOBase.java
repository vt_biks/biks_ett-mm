/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.trzy0.foxy.commons.templates.IParamSignature;
import simplelib.BaseUtils;
import simplelib.CollectionCollectorEx;
import simplelib.FieldNameConversion;
import simplelib.IContinuation;
import simplelib.ILameCollector;
import simplelib.IParametrizedContinuation;
import simplelib.IValueFormatter;
import simplelib.LameRuntimeException;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class BeanConnectionDAOBase<BeanBase> implements INamedSqlsDAO<BeanBase>, IValueFormatter {

    private static final ILameLogger logger = LameLoggerFactory.getLogger(BeanConnectionDAOBase.class);
    protected IBeanConnectionEx<BeanBase> connection;
    protected IParametrizedNamedTextProvider namedSqls;

    public BeanConnectionDAOBase() {
        //ww: innerSetup w podklasie trzeba zawołać zaraz za konstruktorem!
    }

    public BeanConnectionDAOBase(IBeanConnectionEx<BeanBase> connection,
            //IResourceFromProvider<String> sqlTextsRes
            IParametrizedNamedTextProvider namedSqls) {
        innerSetup(connection, namedSqls);
    }

    protected final void innerSetup(IBeanConnectionEx<BeanBase> connection,
            IParametrizedNamedTextProvider namedSqls) {
        this.connection = connection;
        this.namedSqls = //new ParametrizedTextProviderByFoxyMillResource(sqlTextsRes,
                //FoxyMillUtils.makeRenderEnv(true, true, this), this);
                namedSqls;
    }

    public <T extends BeanBase> List<T> createBeansFromQry(String qryTxt, Class<T> beanClass) {
        return connection.createBeansFromQry(qryTxt, beanClass);
    }

    @SuppressWarnings("unchecked")
    public Set<Integer> execIntSetQry(String sqlTxt, boolean maxOneRow, boolean minOneRow) {

        return (Set) connection.execQry(sqlTxt, maxOneRow, minOneRow,
                new CollectionCollectorEx(BaseUtils.anyMapValProjector,
                        new LinkedHashSet()), (FieldNameConversion) null).getCollection();
    }

    public Set<Integer> execIntSetQry(String sqlTxt) {
        return execIntSetQry(sqlTxt, false, false);
    }

    protected String toSqlString(Object obj) {
        return connection.toSqlString(obj);
    }

    protected <T> String toSqlString(Iterable<T> items) {
        return connection.toSqlString(items);
    }

    public Integer execCommand(String commandTxt) {
        return connection.execCommand(commandTxt);
    }

    protected <T extends BeanBase> T createBeanFromQry(String qryTxt, Class<T> beanClass, boolean allowNoResult) {
        return (T) connection.createBeanFromQry(qryTxt, beanClass, allowNoResult);
    }

    @Override
    public String valToString(Object val) {
        if (val instanceof NamedSqlCallParams) {
            return provideNamedSqlText((NamedSqlCallParams) val);
        }

        return toSqlString(val);
    }

    @Override
    public Integer sqlNumberToInt(Object numVal) {
        return connection.sqlNumberToInt(numVal);
    }

    protected Integer execQrySingleValInt(String qryTxt) {
        return connection.execQrySingleValInteger(qryTxt, false);
    }

    public <T extends BeanBase> T createBeanFromQry(String qryTxt, Class<T> beanClass) {
        return connection.createBeanFromQry(qryTxt, beanClass, false);
    }

    public <T extends BeanBase> List<T> createBeansFromNamedQry(String name, Class<T> beanClass,
            Object... args) {

        try {

            //GlobalThreadStopWatch.startProcess("createBeansFromNamedQry");
            try {
                return connection.createBeansFromQry(namedSqls.provide(name, args), beanClass);
            } finally {
                //GlobalThreadStopWatch.endProcess();
            }

        } catch (Exception ex) {
            throw new LameRuntimeException("error for named sql: [" + name + "]", ex);
        }
    }

    public List<Map<String, Object>> execNamedQueryCFN(String name,
            FieldNameConversion optConversion, Object... args) {
        try {
            String sqlTxt = namedSqls.provide(name, args);
            return connection.execQry(sqlTxt, optConversion);
        } catch (Exception ex) {
            throw new LameRuntimeException("error for named sql: [" + name + "]", ex);
        }
    }

    @Deprecated
    public List<Map<String, Object>> execNamedQuery(String name, Object... args) {
        return execNamedQueryCFN(name, null, args);
    }

    public int execNamedCommand(String name, Object... args) {
        try {
            String sqlTxt = namedSqls.provide(name, args);
            return BaseUtils.nullToDef(connection.execCommand(sqlTxt), -1);
        } catch (Exception ex) {
            throw new LameRuntimeException("error for named sql: [" + name + "]", ex);
        }
    }

    public IParamSignature getNamedQuerySignature(String name) {
        return namedSqls.getSignature(name);
    }

    @Deprecated
    public <C extends ILameCollector<Map<String, Object>>> C execNamedQueryEx(String name,
            boolean maxOneRow, boolean minOneRow, C rowCollector,
            Object... args) {
        return execNamedQueryExCFN(name, maxOneRow, minOneRow, rowCollector,
                null, args);
    }

    public <C extends ILameCollector<Map<String, Object>>> C execNamedQueryExCFN(String name,
            boolean maxOneRow, boolean minOneRow, C rowCollector,
            FieldNameConversion optConversion, Object... args) {
        try {
            String sqlTxt = namedSqls.provide(name, args);
            return connection.execQry(sqlTxt, maxOneRow, minOneRow, rowCollector, optConversion);
        } catch (Exception ex) {
            throw new LameRuntimeException("error for named sql: [" + name + "]", ex);
        }
    }

    public <T extends BeanBase> T createBeanFromNamedQry(String name, Class<T> beanClass,
            boolean allowNoResult, Object... args) {
        try {
            String sqlTxt = namedSqls.provide(name, args);
            return connection.createBeanFromQry(sqlTxt, beanClass, allowNoResult);
        } catch (Exception ex) {
            throw new LameRuntimeException("error for named sql: [" + name + "]", ex);
        }
    }

    public Map<String, Object> execNamedQuerySingleRowCFN(String name,
            boolean allowNoResult, FieldNameConversion optConversion, Object... args) {
        List<Map<String, Object>> rows = execNamedQueryCFN(name, optConversion, args);
        if (rows.isEmpty()) {
            if (allowNoResult) {
                return null;
            }
            throw new LameRuntimeException("execNamedQuerySingleRow: no rows for named qry " + name
                    + "(" + toSqlString(args) + ")");
        }
        return rows.get(0);
    }

    @Deprecated
    public Map<String, Object> execNamedQuerySingleRow(String name, boolean allowNoResult, Object... args) {
        return execNamedQuerySingleRowCFN(name, allowNoResult, null, args);
    }

    public void finishWorkUnit(boolean success) {
        connection.finishWorkUnit(success);
    }

    //ww: do debugowania, sprawdzania itp.
    public String provideNamedSqlText(String name, Object... args) {
        return namedSqls.provide(name, args);
    }

    public void execInAutoCommitMode(IContinuation whatToExec) {
        BeanConnectionUtils.execInAutoCommitMode(connection, whatToExec);
    }

    public IBeanConnectionThreadHandle getCurrentThreadHandle() {
        return connection.getCurrentThreadHandle();
    }

    public void setCurrentDatabase(String dbName) {
        connection.setCurrentDatabase(dbName);
    }

    public String getCurrentDatabase() {
        return connection.getCurrentDatabase();
    }

    public IBeanConnectionEx<BeanBase> getConnection() {
        return connection;
    }

    //------------------------------------------------------
    @Override
    public String provideNamedSqlText(NamedSqlCallParams nscp) {
        return provideNamedSqlText(nscp.name, nscp.args);
    }

    @Override
    public <T extends BeanBase> List<T> createBeansFromNamedQry(NamedSqlCallParams nscp, Class<T> beanClass) {
        return createBeansFromNamedQry(nscp.name, beanClass, nscp.args);
    }

    @Override
    public <T extends BeanBase> T createBeanFromNamedQry(NamedSqlCallParams nscp, Class<T> beanClass) {
        return createBeanFromNamedQry(nscp.name, beanClass, nscp.allowNoResult(), nscp.args);
    }

    @Override
    public Map<String, Object> execNamedQuerySingleRowCFN(NamedSqlCallParams nscp, FieldNameConversion optConversion) {
        return execNamedQuerySingleRowCFN(nscp.name, nscp.allowNoResult(), optConversion, nscp.args);
    }

    @Override
    public int execNamedCommand(NamedSqlCallParams nscp) {
        return execNamedCommand(nscp.name, nscp.args);
    }

    @Override
    public List<Map<String, Object>> execNamedQueryCFN(NamedSqlCallParams nscp, FieldNameConversion optConversion) {
        return execNamedQueryCFN(nscp.name, optConversion, nscp.args);
    }

    @Override
    public <C extends ILameCollector<Map<String, Object>>> C execNamedQueryExCFN(NamedSqlCallParams nscp, C rowCollector, FieldNameConversion optConversion) {
        return execNamedQueryExCFN(nscp.name, nscp.maxOneRow, nscp.minOneRow, rowCollector, optConversion, nscp.args);
    }

    @Override
    public void execNamedQueryExCFN(String name,
            IParametrizedContinuation<Iterator<Map<String, Object>>> processCont,
            FieldNameConversion optConversion, Object... args) {
        try {
            String sqlTxt = namedSqls.provide(name, args);
            connection.execQry(sqlTxt, processCont, optConversion);
        } catch (Exception ex) {
            throw new LameRuntimeException("error for named sql: [" + name + "]", ex);
        }
    }

    @Override
    public IParametrizedNamedTextProvider getParametrizedNamedTextProvider() {
        return namedSqls;
    }
}
