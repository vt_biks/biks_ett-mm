/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import simplelib.IValueFormatter;

/**
 *
 * @author wezyr
 */
public interface IConnectionDialectHelper extends IValueFormatter {

    public int readSequenceNextVal(String seqName);
}
