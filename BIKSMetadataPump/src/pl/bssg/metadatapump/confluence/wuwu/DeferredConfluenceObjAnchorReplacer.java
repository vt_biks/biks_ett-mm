/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence.wuwu;

import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.IRegExpMatcher;
import simplelib.IRegExpPattern;
import simplelib.Pair;
import simplelib.deferredstringconverter.IDeferredRegExprReplacer;

/**
 *
 * @author pmielanczuk
 */
public class DeferredConfluenceObjAnchorReplacer implements IDeferredRegExprReplacer<ConfluenceObjAnchorMatcher, LinkedHashMap<String, String>> {

    static {
        LameUtils.initFactories();
    }

    public static enum TagCloseOption {

        Open(""), Closed("[/]"), Any("([/]?)");
        protected String regExprPart;

        TagCloseOption(String regExprPart) {
            this.regExprPart = regExprPart;
        }
    };

    public static String makeRegExprForTagAndAttrs(String tagNameSubRegExpr, TagCloseOption co) {
        return "<" + tagNameSubRegExpr + TAG_ATTRS_REGEXPR + co.regExprPart + ">";
    }
    public static final String ATTR_AND_VAL_REGEXPR = "[ \\t\\n\\r]*([a-z0-9_:-]+)[ \\t\\n\\r]*=[ \\t\\n\\r]*([\"][^\"]*[\"]|['][^']*['])";
    public static final String TAG_ATTRS_REGEXPR = "[ \\t\\n\\r]*(?:[a-z0-9_:-]+[ \\t\\n\\r]*=[ \\t\\n\\r]*(?:[\"][^\"]*[\"]|['][^']*['])[ \\t\\n\\r]*)*";
//    public static final String ANCHOR_REGEXPR = "<a" + TAG_ATTRS_REGEXPR + ">";
    public static final String ANCHOR_REGEXPR = makeRegExprForTagAndAttrs("a", TagCloseOption.Open);
    public static final String IMGHRBR_REGEXPR = makeRegExprForTagAndAttrs("(img|hr|br)", TagCloseOption.Open) + "([ \\t\\n\\r]*)";
    protected static final IRegExpPattern IMGHRBR_PATT = ConfluenceUtils.compileRegExpr(IMGHRBR_REGEXPR);
    protected static final IRegExpPattern ANCHOR_PATT = ConfluenceUtils.compileRegExpr(ANCHOR_REGEXPR);
    protected static final IRegExpPattern ATTR_AND_VAL_PATT = ConfluenceUtils.compileRegExpr(ATTR_AND_VAL_REGEXPR);
    protected String absoluteUrlBase;
    protected IConfluenceObjectImportedChecker importedChecker;
    protected String biksBaseUrl;
    private Integer selectedNodeId;

    public DeferredConfluenceObjAnchorReplacer(
            String biksBaseUrl,
            String confluenceAbsoluteUrlBase,
            IConfluenceObjectImportedChecker importedChecker, Integer optSelectedNodeId) {
        this.biksBaseUrl = biksBaseUrl;
        this.absoluteUrlBase = confluenceAbsoluteUrlBase;
        this.importedChecker = importedChecker;
        this.selectedNodeId = optSelectedNodeId;
    }

    @Override
    public ConfluenceObjAnchorMatcher[] makeMatchers(String s) {
        return new ConfluenceObjAnchorMatcher[]{
            new ConfluenceObjAnchorMatcher(s)
        };
    }

    @Override
    public LinkedHashMap<String, String> getDeferredReplacement(ConfluenceObjAnchorMatcher m) {
        return new LinkedHashMap<String, String>(m.getAttrMap());
    }

//    protected Long extractOptObjIdFromRelativeUrl(String url) {
//        String idStr = BaseUtils.dropPrefixOrGiveNull(url, "page_");
//        if (idStr == null) {
//            return null;
//        }
//        return BaseUtils.tryParseLong(idStr, null);
//    }
    // dostaje url bez ciapków i sprawdza czy jest globalny
    // globalnego nie rusza (pustego też), ale do lokalnego dodaje absoluteUrlBase
    // założenie jest, że pierwszy znak globalnego to '/', a absoluteUrlBase
    // kończy się też na '/'
    protected String fixAsGlobalUrl(String url) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(url) || BaseUtils.isUriGlobal(url)) {
            return url;
        } else {
            return absoluteUrlBase + url.substring(1);
        }
    }

    protected String removeQuotesAndFixAsGlobalUrl(String quotedUrl) {
        if (BaseUtils.strLengthFix(quotedUrl) > 2) {
            return fixAsGlobalUrl(quotedUrl.substring(1, quotedUrl.length() - 1));
        } else {
            return quotedUrl;
        }
    }

    @Override
    public Collection<String> resolveReplacements(Collection<LinkedHashMap<String, String>> replacements) {

        int size = replacements.size();
//        Long[] ids = new Long[size];
        Set<String> idColl = new HashSet<String>();
        Set<String> logins = new HashSet<String>();
//        int idx = 0;
        for (LinkedHashMap<String, String> attrMap : replacements) {

            String href = attrMap.get("href");
            if (href != null && href.length() > 2) {
                char qc = href.charAt(0);
                href = qc + href.substring(1, href.length() - 1).trim() + qc;
                attrMap.put("href", href);
            }

//            Long optId = extractOptObjIdFromRelativeUrl(url);
//            ids[idx] = optId;
//            if (optId != null) {
//            Long optObjId = ConfluenceObjAnchorMatcher.extractOptObjId(attrMap);
//            if (optObjId != null) {
//                idColl.add(optObjId);
//            }
            String dataUsername = attrMap.get("data-username");
            if (dataUsername != null && dataUsername.length() > 2) {
                final String fixedName = dataUsername.substring(1, dataUsername.length() - 1).trim();
                logins.add(fixedName);
//                System.out.println("LOGIN-NAME: " + fixedName);
            } else {
                idColl.add(removeQuotesAndFixAsGlobalUrl(href));
//                System.out.println("no login name!");
            }
//            String href = attrMap.get("href");
//            if (BaseUtils.strLengthFix(href) > 2) {
//                idColl.add(fixAsGlobalUrl(href.substring(1, href.length() - 1)));
//            }
//            }
//            idx++;
        }

        Map<String, String> importedMap = importedChecker.checkConfluenceImportedObjs(idColl, selectedNodeId);

        Map<String, String> loginsMap = importedChecker.checkConfluenceImportedLogins(logins);
//        System.out.println("for logins: " + logins + ", map=" + loginsMap);
        List<String> res = new ArrayList<String>(size);

//        int idx = 0;
        for (LinkedHashMap<String, String> attrMap : replacements) {
            String href = attrMap.get("href");
            String biksUrl = null;

            String dataUsername = attrMap.get("data-username");
            if (dataUsername != null) {
                if (dataUsername.length() > 2) {
                    final String fixedName = dataUsername.substring(1, dataUsername.length() - 1).trim();
                    biksUrl = loginsMap.get(fixedName);
                }
                attrMap.remove("data-username");
//                attrMap.remove("data-user-hover-bound");
//                "url fn confluence-userlink userlink-0"

                String classVal = attrMap.get("class");
                if (classVal != null) {
                    attrMap.put("class", classVal.replace("confluence-userlink", ""));
                }
            } else {
                biksUrl = importedMap.get(removeQuotesAndFixAsGlobalUrl(href));
            }
//            Long optId = ConfluenceObjAnchorMatcher.extractOptObjId(attrMap);
//
//            if (optId != null) {
//                biksUrl = importedMap.get(optId);
//            } else {
//                biksUrl = null;
//            }

            String target = null;

            if (biksUrl == null) {
                String fullUrl = removeQuotesAndFixAsGlobalUrl(href);
                String urlRest = BaseUtils.dropPrefixOrGiveNull(fullUrl, biksBaseUrl);

                if (urlRest != null) {
                    Pair<String, String> p = BaseUtils.splitString(urlRest, "#");
                    if (p.v2 != null) {
                        biksUrl = p.v2;
                    }
                }
            }

            if (biksUrl != null) {
//                href = "\"" + biksUrl + "\"";
                href = "\"#\"";
                target = "'_parent'";
//                Pair<String, String> hashOfUrl = BaseUtils.splitString(biksUrl, "#");
//                if (hashOfUrl.v2 != null) {
//                    attrMap.put("onclick", "'window.parent.location.hash=\"" + hashOfUrl.v2 + "\"; return false;'");
//                }
                attrMap.put("onclick", "'window.parent.location.hash=\"" + biksUrl + "\"; return false;'");
            } else {
                char c = href.charAt(0);
                href = href.substring(1, href.length() - 1);
                if (BaseUtils.isUriGlobal(href) || href.isEmpty()) {
                    href = null;
                } else {
                    href = href.substring(1);
                    href = c + absoluteUrlBase + href + c;
                }
                target = "'_blank'";
            }

            if (href != null) {
                attrMap.put("href", href);
            }
            if (target != null) {
                if (target.isEmpty()) {
                    attrMap.remove("target");
                } else {
                    attrMap.put("target", target);
                }
            }
            StringBuilder sb = new StringBuilder();

            sb.append("<a");

            for (Entry<String, String> e : attrMap.entrySet()) {
                sb.append(" ").append(e.getKey()).append("=").append(e.getValue());
            }
            sb.append(">");
            res.add(sb.toString());

//            idx++;
        }

        return res;
    }

    public static LinkedHashMap<String, String> extractAttrNamesAndVals(String namesAndValsStr,
            boolean dropQuotesFromVals, boolean lowerCaseNames) {
        LinkedHashMap<String, String> res = new LinkedHashMap<String, String>();

        IRegExpMatcher m = ATTR_AND_VAL_PATT.matcher(namesAndValsStr);

        while (m.find()) {
            String name = m.group(1);
            if (lowerCaseNames) {
                name = name.toLowerCase();
            }

            String value = m.group(2);
            if (dropQuotesFromVals) {
                value = value.substring(1, value.length() - 1);
            }

            res.put(name, value);
        }

        return res;
    }

    public static String fixUnclosedImgHrBr(String html) {
        final int htmlLen = html.length();

        IRegExpMatcher m = IMGHRBR_PATT.matcher(html);

        StringBuilder sb = new StringBuilder();

        int pos = 0;
        while (m.find()) {
            String tagName = m.group(1);
            int tagNameLen = tagName.length();

            int gsp = m.start();
            int gep = m.end();

            sb.append(html, pos, gsp);

            boolean needsClose = true;
            final int gepForClose = gep + 2 + tagNameLen;

            // czy tam jeszcze </tag się zmieści?
            if (gepForClose <= htmlLen) {
                if (html.substring(gep, gepForClose).equals("</" + tagName)) {
                    needsClose = false;
                }
            }

            if (needsClose) {
                sb.append(html, gsp, gep - m.group(2).length() - 1);
                sb.append("/>").append(m.group(2));
            } else {
                sb.append(html, gsp, gep);
            }
            pos = gep;
        }

        sb.append(html, pos, htmlLen);
        return sb.toString();
    }

    public static void main(String[] args) {

        System.out.println(fixUnclosedImgHrBr("<img src='aaa'> oo"));
        System.out.println(fixUnclosedImgHrBr("<img src='aaa'>oo"));
        System.out.println(fixUnclosedImgHrBr("<img src='aaa'> </img>oo"));
        System.out.println(fixUnclosedImgHrBr("<br/> <img/>oo"));
        System.out.println(fixUnclosedImgHrBr("<br> <img/>oo<hr><br><br>"));

//        IRegExpMatcher m = ANCHOR_PATT.matcher("aqq <a href='aa' x:link-to-obj='zzz?' title=\"aa > bb\" >qqq");
//        if (m.find()) {
//            String whole = m.group(0);
//            System.out.println("found! " + whole);
//            System.out.println("attrs: " + extractAttrNamesAndVals(whole.substring(2), true, true));
//        } else {
//            System.out.println("not found :-(");
//        }
//        try {
//            throw new RuntimeException("aqq!");
//        } catch (Exception ex) {
//            System.out.println("err!\n" + StackTraceUtil.getCustomStackTrace(ex));
//        }
    }
}
