/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.foxygwtcommons;

/**
 *
 * @author AMamcarz
 */
public class TextWithInherit {
    public String text;
    public boolean inherit;

    public TextWithInherit(String text, boolean inheritCheckBox) {
        this.text = text;
        this.inherit = inheritCheckBox;
    }
}
