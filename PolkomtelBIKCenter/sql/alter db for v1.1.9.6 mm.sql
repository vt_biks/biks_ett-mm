﻿exec sp_check_version '1.1.9.6';
go

create table bik_right_role(
	id int identity(1,1) not null primary key,
	code varchar(255) not null,
	caption varchar(255) not null,
	rank int not null
)

create table bik_user_right(
	user_id int not null references bik_system_user(id),
	right_id int not null references bik_right_role(id)	
)

insert into bik_right_role (code, caption, rank) values ('Administrator', 'Administrator', 100)
insert into bik_right_role (code, caption, rank) values ('Expert', 'Ekspert', 80)


exec sp_update_version '1.1.9.6', '1.1.9.7';
go


