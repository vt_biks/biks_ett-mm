﻿exec sp_check_version '1.1.7';
go

------------------------------------
------------------------------------


update bik_node_name_chunk 
--  select *
set tree_id = n.tree_id
from bik_node_name_chunk av inner join bik_node n on av.node_id = n.id
where av.tree_id <> n.tree_id


------------------------------------
------------------------------------

exec sp_update_version '1.1.7', '1.1.7_A';
go