/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import commonlib.LameUtils.SqlFlavor;
import java.sql.Time;
import java.util.Date;
import pl.trzy0.foxy.serverlogic.BeanConnectionExBase;
import pl.trzy0.foxy.serverlogic.IConnectionDialectHelper;
import pl.trzy0.foxy.serverlogic.SqlValueFormatter;
import simplelib.SimpleDateUtils;

/**
 *
 * @author wezyr
 */
public class PostgresConnectionDialectHelper extends SqlValueFormatter implements IConnectionDialectHelper {

    private BeanConnectionExBase connBase;

    public PostgresConnectionDialectHelper(BeanConnectionExBase connBase) {
        this.connBase = connBase;
    }

    protected String formatDate(Date d) {
        return "TIMESTAMP " + SimpleDateUtils.toSingleQuotedCanonicalString(d, true);
    }

    @Override
    public int readSequenceNextVal(String seqName) {
        return connBase.execQrySingleValInteger("select nextval(" + valToString(seqName) + ") as nv", false);
    }

    @Override
    protected SqlFlavor getSqlFlavor() {
        return SqlFlavor.Postgres;
    }

    @Override
    protected String formatTime(Time t) {
        return SimpleDateUtils.toSingleQuotedCanonicalString(t, true);
    }
}
