/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.TreeNodeBranchBean;
import pl.fovis.foxygwtcommons.GWTUtils;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class TreeSelectorBIAInstanceDialog extends TreeSelectorForRoleRestrictionsDialog {

    protected Map<TreeNodeBranchBean, InlineLabel> nodeToInlineLblMap = new HashMap<TreeNodeBranchBean, InlineLabel>();

    public void buildAndShowDialog(Set<TreeNodeBranchBean> selectedNodes, String code,
            IParametrizedContinuation<Set<TreeNodeBranchBean>> saveCont) {
        this.selectedNodes = BaseUtils.safeNewHashSet(selectedNodes);
        super.buildAndShowDialog(null, BaseUtils.paramsAsSet(code), selectedNodes, saveCont);
        actionBtn.setEnabled(true);
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        BIKClientSingletons.getService().getBikTreeInBIAdmin(getTreeOptExtraMode(), callback);
    }

    @Override
    protected boolean isGrouppingByMenuEnabled() {
        return false;
    }

    @Override
    protected Widget createWidgetForNameColumn(final TreeNodeBean node) {
        Integer nId = node.id;
        int treeId = node.treeId;

        //showCheckboxForTreeCategories
        if (treeId == -1) {
            return new InlineLabel(node.name);
        }
        if (treeId < 0) {
            treeId = -treeId;
            nId = null;
        }
        final TreeNodeBranchBean currNode = new TreeNodeBranchBean(treeId, nId, node.branchIds, node.objId);
        TreeNodeBranchBean parentNode = getParentNode(currNode);
        BaseUtils.addToCollectingMapWithList(childNodeMap, parentNode, currNode);

        if (nodeKindsToCreateWidgetAsCheckBox() == null || nodeKindsToCreateWidgetAsCheckBox().contains(node.nodeKindCode)) {
            final CheckBox cb = new CheckBox(node.name);
            nodeToCheckBoxMap.put(currNode, cb);

            cb.setEnabled(!isAncestorNodeSelected(currNode));
            cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

                @Override
                public void onValueChange(ValueChangeEvent<Boolean> event) {
                    final boolean doAdd = event.getValue();
                    // działaj tylko, gdy faktycznie nastąpiła zmiana wartości checkboxa
                    // w stosunku do zapamiętanego stanu
                    if (selectedNodes.contains(currNode) != doAdd) {
                        BaseUtils.setAddOrRemoveItem(selectedNodes, currNode, doAdd);
                        addOrRemoveNodeToCnts(currNode, doAdd);
                        setEnabledForDescendants(currNode, !doAdd);
                    }
                }
            });

            cb.setValue(selectedNodes.contains(currNode), false);
            setStyleForCheckBox(cb, currNode);
            return cb;
        }

        InlineLabel lbl = new InlineLabel(node.name);
        nodeToInlineLblMap.put(currNode, lbl);
        setStyleForInlainLabelBox(lbl, currNode);
        return lbl;
    }

    @Override
    protected void addOrRemoveNodeToCnts(TreeNodeBranchBean node, boolean doAdd) {
        while (node != null) {
            if (doAdd) {
                selectedDescendantCounts.add(node);
            } else {
                selectedDescendantCounts.remove(node);
            }

            CheckBox cb = nodeToCheckBoxMap.get(node);
            setStyleForCheckBox(cb, node);
            InlineLabel lbl = nodeToInlineLblMap.get(node);
            setStyleForInlainLabelBox(lbl, node);
            node = getParentNode(node);
        }
    }

    protected void setStyleForInlainLabelBox(InlineLabel lbl, TreeNodeBranchBean node) {
        if (lbl != null) {
            GWTUtils.addOrRemoveStyleName(lbl, "inlinleLabelAnch", selectedDescendantCounts.getCount(node) > 0);
        }
    }

    protected Set<String> nodeKindsToCreateWidgetAsCheckBox() {
        return null;
    }
}
