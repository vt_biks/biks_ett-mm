exec sp_check_version '1.0.6';
go

----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

alter table bik_node add is_deleted int not null default 0;
go

----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node]
GO

create procedure [dbo].[sp_insert_into_bik_node](@tree_name varchar(255))
as
	declare @table_folders varchar(255);
	declare @si_id varchar(1000), @si_parentid varchar(1000), @si_kind varchar(max), @si_name varchar(max);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	print cast(sysdatetime() as varchar(23)) + ': faza 1'
	
	select @tree_id = id 
	from bik_tree
	where name=@tree_name;

	--create table #emptyFolders(IID varchar(1000));
	create table #primaryIds(si_id varchar(1000) primary key);
	create table #secondaryIds(si_id varchar(1000) primary key);

	if(@tree_name!='Raporty' and @tree_name!='Teradata')
	begin
		set @table_folders = ' APP_FOLDER where 1=1 ';
		
		if(@tree_name = 'Po��czenia')
		begin
			set @kinds = '''MetaData.DataConnection''';
			exec('insert into #primaryIds
					select convert(varchar,SI_ID) as SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');
			exec('insert into #secondaryIds 
					select convert(varchar,SI_ID) as SI_ID
					from APP_UNIVERSE
					where SI_DATACONNECTION__1 in (select * from #primaryIds)');			
		end;
		if(@tree_name = '�wiaty obiekt�w')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select convert(varchar,SI_ID) as SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_name='Raporty')
	begin
		set @table_folders = ' INFO_FOLDER where si_name != ''~Webintelligence''';
		set @kinds = '''Webi'', ''Flash'', ''CristalReports''';
		
	end;
	
	if(@tree_name!='Teradata')
	begin
		EXECUTE(	'DECLARE Folders CURSOR FOR		select convert(varchar,si_id), case 
														when SI_PARENTID in(select si_id from ' + @table_folders +') 
															then SI_PARENTID
													else 0 end as SI_PARENTID, SI_KIND, SI_NAME 
													from ' + @table_folders);

	set nocount on;
	
	declare @howMany int = 0, @parent varchar(1000);
	
	print cast(sysdatetime() as varchar(23)) + ': faza 2 - przed open folders'

	OPEN Folders

	FETCH NEXT FROM Folders
	into @si_id, @si_parentid, @si_kind, @si_name;

	print cast(sysdatetime() as varchar(23)) + ': faza 3 - po open folders, przed p�tl�'
	
	declare @sql nvarchar(1000);
	
/*	WHILE @@FETCH_STATUS = 0
	BEGIN
		 
		if(@si_id not in (select * from #emptyFolders))
		begin	
			select @parent = @si_id;			
			set @sql = N'select @phowMany  = sum(howMany) 
						 from(	select  COUNT(*) as howMany 
								from' + @table_folders + ' and SI_PARENTID = ' + @si_id + ' and si_id not in(select * from #emptyFolders)
					
								union all
		
								select COUNT(*) as howMany
								from aaa_global_props
								where SI_PARENTID = ' + @si_id + 
									' and SI_KIND in (' + @kinds + ')';--) j';
			if(@tree_name = 'Po��czenia')
				set @sql = @sql + N' union all 
				
									select COUNT(*) as howMany 
									from APP_UNIVERSE
									where SI_DATACONNECTION__1 = ' + @si_id;
			if(@tree_name = '�wiaty obiekt�w' or @tree_name = 'Po��czenia')
				set @sql = @sql + N' union all
				
									select COUNT(*) as howMany
									from INFO_WEBI
									where SI_UNIVERSE__1 = ' + @si_id;
			set @sql = @sql + ') j';
									
			exec sp_executesql @sql, N'@phowMany int OUTPUT', @phowMany = @howMany OUTPUT;
				
		while(@howMany=0 and @parent not in(select * from #emptyFolders))
		begin
				declare @pom int;
				insert into #emptyFolders values(@parent)
				set @sql = 'select @ppom  = SI_PARENTID 
							from ' + @table_folders + ' and si_id = ' + @parent;
				exec sp_executesql @sql, N'@ppom int OUTPUT', @ppom = @pom OUTPUT;
				
				select @parent = @pom;
			
				if(@parent != 0)
				begin
				
				set @sql = N'select @phowMany  = sum(howMany) 
							 from(	select  COUNT(*) as howMany from' + @table_folders + ' and SI_PARENTID = ' + @parent + ' and si_id not in(select * from #emptyFolders)
					
									union all
		
									select COUNT(*) as howMany
									from aaa_global_props
									where SI_PARENTID = ' + @parent + 
										' and SI_KIND in (' + @kinds + ')';
				if(@tree_name = 'Po��czenia')
					set @sql = @sql + N'union all 
					
									select COUNT(*) as howMany 
									from APP_UNIVERSE
									where SI_DATACONNECTION__1 = ' + @parent;
									
				if(@tree_name = '�wiaty obiekt�w' or @tree_name = 'Po��czenia')
					set @sql = @sql + N' union all
					
									select COUNT(*) as howMany
									from INFO_WEBI
									where SI_UNIVERSE__1 = ' + @parent;
									
				set @sql = @sql + ') j';
					
					
				exec sp_executesql @sql, N'@phowMany int OUTPUT', @phowMany = @howMany OUTPUT;
								
				end
				else
					set @howMany=1;
		end;
		end;

	FETCH NEXT FROM Folders
	into @si_id, @si_parentid, @si_kind, @si_name;

	END
*/

	print cast(sysdatetime() as varchar(23)) + ': faza 4 - po p�tli'

CLOSE Folders;
DEALLOCATE Folders;
end


--create table #pomTable (si_parentid varchar(1000), id int unique, si_id varchar(1000) primary key);
		
declare @node_kind_id int;
set @sql = N'';
if(@tree_name = 'Teradata')
begin
	print cast(sysdatetime() as varchar(23)) + ': faza 4.1 - teradata przed sp_teradata_init_branch_names'
	exec sp_teradata_init_branch_names;
	print cast(sysdatetime() as varchar(23)) + ': faza 4.2 - teradata po sp_teradata_init_branch_names'
	set @sql = N'select branch_names as SI_ID, 
																		case 
																		when SUBSTRING(branch_names, 1,len(branch_names) - len(name + ''|'')) =''''
																		then null
																		else SUBSTRING(branch_names, 1,len(branch_names) - len(name + ''|'')) end
																		as SI_PARENTID, 
																		type as SI_KIND, name as SI_NAME
													from aaa_teradata_object
													/*where parent_id is not null or name like ''VD_US_%''*/';
	
end;
else
begin
	
	set @sql = N'select si_id, case 
															when SI_PARENTID in(select si_id from ' + @table_folders +') 
																then SI_PARENTID
															else 0 end as SI_PARENTID, SI_KIND, SI_NAME 
													from ' + @table_folders + 
													
													'union all

													select SI_ID, SI_PARENTID, SI_KIND, SI_NAME
													from aaa_global_props
													where SI_PARENTID in (select si_id from ' + @table_folders + ') 
													and SI_KIND in ('+ @kinds +')';
		if(@tree_name = 'Po��czenia')
			set @sql = @sql + N'union all		  

								select SI_ID, SI_DATACONNECTION__1 as SI_PARENTID, SI_KIND, SI_NAME 
								from APP_UNIVERSE
								where SI_DATACONNECTION__1 in (select * from #primaryIds)
	
								union all
								
								select SI_ID, SI_UNIVERSE__1 as SI_PARENTID, SI_KIND, SI_NAME
								from INFO_WEBI
								where SI_UNIVERSE__1 in (select * from #secondaryIds)';
							
		if(@tree_name = '�wiaty obiekt�w')
			set @sql = @sql + N'union all
			
								select SI_ID, SI_UNIVERSE__1 as SI_PARENTID, SI_KIND,  SI_NAME
								from INFO_WEBI
								where SI_UNIVERSE__1 in (select * from #primaryIds)';
end;					

	print cast(sysdatetime() as varchar(23)) + ': faza 6 - przed zapytaniem'

--print 'zapytanie: ' + @sql;

create table #tmpNodes (si_id varchar(1000) primary key, si_parentid varchar(1000), si_kind varchar(max), si_name varchar(max));

set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name) select si_id, si_parentid, si_kind, si_name from (' + @sql + ') xxx';
EXECUTE(@sql);

	print cast(sysdatetime() as varchar(23)) + ': faza 6.5 - po wrzuceniu do #tmpNodes'

insert into bik_node_kind (code,caption) 
select distinct si_kind, si_kind
from #tmpNodes 
where not exists (select 1 from bik_node_kind where code = si_kind);

update bik_node 
set parent_node_id = null, node_kind_id = bik_node_kind.id, name = #tmpNodes.si_name,
  is_deleted = 0
from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
where bik_node.obj_id = #tmpNodes.si_id and tree_id = @tree_id;

insert into bik_node (node_kind_id, name, tree_id, obj_id)
select bik_node_kind.id, #tmpNodes.si_name, @tree_id, #tmpNodes.si_id
from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
left join bik_node on bik_node.obj_id = #tmpNodes.si_id 
where bik_node.id is null;

/*
insert into #pomTable (si_parentid, id, si_id)
select
  #tmpNodes.si_ParentId, bik_node.id, #tmpNodes.si_id
from #tmpNodes inner join bik_node on #tmpNodes.si_id = bik_node.obj_id and bik_node.tree_id = @tree_id;
*/

	print cast(sysdatetime() as varchar(23)) + ': faza 7.0 - po ci�kiej walce bez kursora?'

/*	
set @sql = 'DECLARE RepoEntity CURSOR FOR		' + @sql;
EXECUTE(@sql);
														
	print cast(sysdatetime() as varchar(23)) + ': faza 7 - po zapytaniu dla kursora'
	
--otwieram kursor
set nocount on;
OPEN RepoEntity;

FETCH NEXT FROM RepoEntity
into @si_id, @si_parentid, @si_kind, @si_name;

	print cast(sysdatetime() as varchar(23)) + ': faza 8 - kursor otwarty, przed p�tl�'
	
WHILE @@FETCH_STATUS = 0
BEGIN
		--if(@si_id not in(select * from #emptyFolders))
		--begin
				if(isnull((	select id
					from bik_node_kind
					where code=@si_kind),'')!='')
				begin
					select @node_kind_id = id
					from bik_node_kind
					where code=@si_kind;
				end
				else
				begin
				--tutaj musze cos zmienic
					insert into bik_node_kind (code,caption) values(@si_kind,@si_kind);
					select @node_kind_id = SCOPE_IDENTITY();
				end
				--nie by�o wcze�niej tego si_Id
				if(not exists(select obj_id from bik_node where tree_id = @tree_id and @si_id=obj_id))
				begin
					insert into bik_node (parent_node_id, node_kind_id, name, tree_id, obj_id)
					values (null, @node_kind_id, @si_name, @tree_id, @si_id);
				
					insert into #pomTable (si_parentid, id, si_id) select @si_parentid, SCOPE_IDENTITY(), @si_id;
				end else
				--by�o to si_id
				--if(exists(select obj_id from bik_node where tree_id = @tree_id and @si_id=obj_id))
				begin
					update bik_node 
					set parent_node_id = null, node_kind_id = @node_kind_id, name = @si_name,
						 tree_id = @tree_id
					where obj_id = @si_id and @tree_id = tree_id;
						
					insert into #pomTable (si_parentid, id, si_id) values (@si_parentid,(select id from bik_node where obj_id=@si_id and tree_id = @tree_id), @si_id)
				end
		--end;
   FETCH NEXT FROM RepoEntity
   into @si_id, @si_parentid, @si_kind, @si_name;
END
	print cast(sysdatetime() as varchar(23)) + ': faza 9 - po p�tli'

CLOSE RepoEntity;
DEALLOCATE RepoEntity;
*/

	print cast(sysdatetime() as varchar(23)) + ': faza 9.5 - po p�tli, zamkni�te kursory, usuwamy pustaki z bik_node'

declare @folder_node_kind_id int

select @folder_node_kind_id = ID from bik_node_kind where code = 'Folder';

--select * from bik_node_kind where code = 'Folder'

declare @rc int = 1

while @rc > 0 begin
  --delete from 
  update bik_node
  set is_deleted = 1
  where tree_id = @tree_id and node_kind_id = @folder_node_kind_id and is_deleted = 0
  and not exists(select 1 from bik_node g where g.parent_node_id = bik_node.id and g.is_deleted = 0)

  set @rc = @@ROWCOUNT
end;


set nocount on;

drop table #primaryIds;
drop table #secondaryIds;
--drop table #emptyFolders;

	print cast(sysdatetime() as varchar(23)) + ': faza 10 - przed uaktualnieniem parent�w'


/*
insert into #pomTable (si_parentid, id, si_id)
select
  #tmpNodes.si_ParentId, bik_node.id, #tmpNodes.si_id
from #tmpNodes inner join bik_node on #tmpNodes.si_id = bik_node.obj_id and bik_node.tree_id = @tree_id;
*/
	
update bik_node 
set parent_node_id= bk.id
from 
  bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id
  inner join bik_node bk on bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id

/*update bik_node 
set parent_node_id= bk.id
from 
  bik_node inner join #pomTable as pt on bik_node.id = pt.id and bik_node.tree_id = @tree_id
  inner join bik_node bk on bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id
*/

	print cast(sysdatetime() as varchar(23)) + ': faza 11 - po uaktualnieniu parent�w'
	
--create index idx_pomTable on #pomTable (si_id);	
	
/*
delete from bik_node
from bik_node left join #pomTable on bik_node.obj_id = #pomTable.si_id
where bik_node.tree_id = @tree_id and #pomTable.id is null;
*/

/*
update --delete from 
bik_node
set is_deleted = 1
from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id
where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id ;
*/
				
--drop table #pomTable;
drop table #tmpNodes

	print cast(sysdatetime() as varchar(23)) + ': faza 12 - po delete zb�dnych'

exec sp_node_init_branch_id @tree_id, null;


	print cast(sysdatetime() as varchar(23)) + ': faza 13 - po sp_node_init_branch_id, finito!'
end
GO

----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_teradata_init_branch_names]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_teradata_init_branch_names]
GO

create procedure sp_teradata_init_branch_names --(@opt_tree_id int, @opt_node_id int)
as
begin  
  set nocount on

	print cast(sysdatetime() as varchar(23)) + ': sp_teradata_init_branch_names #1'
	
  create table #ids (id int not null primary key);
  
  insert into #ids (id) 
  select ID from aaa_teradata_object where parent_id is null
/*  where 
    (@opt_tree_id is null or tree_id = @opt_tree_id)
    and 
    (  @opt_node_id is null and parent_node_id is null 
    or 
       @opt_node_id is not null and id = @opt_node_id
    )
*/
   ;

	print cast(sysdatetime() as varchar(23)) + ': sp_teradata_init_branch_names #2'
    
  create table #child_ids (id int not null primary key);
    
  while exists (select 1 from #ids)
  begin
  
	print cast(sysdatetime() as varchar(23)) + ': sp_teradata_init_branch_names #3.1 - in loop'
  
    update aaa_teradata_object
    set branch_names = coalesce(parent.branch_names, '') + aaa_teradata_object.name + '|'
    from aaa_teradata_object inner join #ids on aaa_teradata_object.id = #ids.id
      left join aaa_teradata_object as parent on parent.id = aaa_teradata_object.parent_id;    
      
	print cast(sysdatetime() as varchar(23)) + ': sp_teradata_init_branch_names #3.2 - after update'

    truncate table #child_ids;
    
    insert into #child_ids (id)
    select ID from aaa_teradata_object where parent_id in (select id from #ids);
    
	print cast(sysdatetime() as varchar(23)) + ': sp_teradata_init_branch_names #3.3 - after insert'

    truncate table #ids;
    
    insert into #ids (id) select ID from #child_ids;

	print cast(sysdatetime() as varchar(23)) + ': sp_teradata_init_branch_names #3.4 - end of loop'
  end
    
  print cast(sysdatetime() as varchar(23)) + ': sp_teradata_init_branch_names #4 - after loop'
  drop table #ids;
  drop table #child_ids;
end
go


----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_node_init_branch_id]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_node_init_branch_id]
GO

create procedure sp_node_init_branch_id (@opt_tree_id int, @opt_node_id int)
as
begin  
  set nocount on
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #1'

  create table #ids (id int not null primary key);
  
  insert into #ids (id) 
  select ID from bik_node 
  where 
    (@opt_tree_id is null or tree_id = @opt_tree_id)
    and 
    (  @opt_node_id is null and parent_node_id is null 
    or 
       @opt_node_id is not null and id = @opt_node_id
    )
   ;
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #2'
    
  create table #child_ids (id int not null primary key);
    
  while exists (select 1 from #ids)
  begin
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #3'
    
    update bik_node
    set branch_ids = coalesce(parent.branch_ids, '') + CAST(bik_node.id as varchar(10)) + '|'
    from bik_node inner join #ids on bik_node.id = #ids.id
      left join bik_node as parent on parent.id = bik_node.parent_node_id;    

	/*
    update bik_node
    set branch_ids = coalesce((select parent.branch_ids from bik_node as parent where parent.id = bik_node.parent_node_id), '')
    + CAST(id as varchar(10)) + '|'
    where bik_node.id in (select id from #ids);
    */
    
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #4'
    truncate table #child_ids;
    
    insert into #child_ids (id)
    select ID from bik_node where parent_node_id in (select id from #ids);
    
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #5'
    truncate table #ids;
    
    insert into #ids (id) select ID from #child_ids;
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #6'    
  end
    
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #7'
  drop table #ids;
  drop table #child_ids;
end
go


----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

create index idx_bik_node_tree_obj_id on bik_node (tree_id, obj_id)
go

if exists (select * from sysobjects where id = OBJECT_ID('uk_bik_node_tree_obj_id'))
alter table bik_node drop constraint uk_bik_node_tree_obj_id
go

--alter table bik_node add constraint uk_bik_node_tree_obj_id unique (tree_id, obj_id)


create index idx_bik_node_parent_node_id on bik_node (parent_node_id, tree_id)
go


--delete from bik_node
--go

----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

exec sp_insert_into_bik_node 'Raporty';

exec sp_insert_into_bik_node '�wiaty obiekt�w';

exec sp_insert_into_bik_node 'Po��czenia';

exec sp_insert_into_bik_node 'Teradata';
go


----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

exec sp_update_version '1.0.6', '1.0.7';
go
