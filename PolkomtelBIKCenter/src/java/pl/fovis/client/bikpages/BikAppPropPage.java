/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.AppPropBean;
import pl.fovis.common.AppPropsBean;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.GWTUtils;
import simplelib.BaseUtils;

/**
 *
 * @author AMamcarz
 */
public class BikAppPropPage extends BikPageMinimalBase {

    protected VerticalPanel mainVp;
    protected FlexTable grid = new FlexTable();
    protected Map<String, TextBox> editableProps = new LinkedHashMap<String, TextBox>();
    protected PushButton saveAppPropsBtn = new PushButton("Save props" /* I18N: no */);
    protected Set<String> changedPropNames = new HashSet<String>();

    @Override
    protected Widget buildWidgets() {
        NewLookUtils.makeCustomPushButton(saveAppPropsBtn);
        mainVp = new VerticalPanel();
        mainVp.add(grid);
        grid.setStyleName("propNameGp");
        saveAppPropsBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                Map<String, String> modifiedProps = new HashMap<String, String>();

                for (Entry<String, TextBox> e : editableProps.entrySet()) {
                    String propName = e.getKey();
                    String val = e.getValue().getText();
                    if (!BaseUtils.safeEquals(val, BIKClientSingletons.getAppPropVal(propName))) {
                        modifiedProps.put(propName, val);
                    }
                }

                if (modifiedProps.isEmpty()) {
                    Window.alert("nothing to set" /* I18N: no */ + "!");
                    return;
                }

                BIKClientSingletons.getService().setEditableAppProps(modifiedProps, new AsyncCallback<List<AppPropBean>>() {
                    public void onFailure(Throwable caught) {
                        Window.alert("error" /* I18N: no */ + "!");
                    }

                    public void onSuccess(List<AppPropBean> result) {
                        Window.alert("saved" /* I18N: no */ + "!");

                        BIKClientSingletons.setupAppProps(result, BIKClientSingletons.isMultiXMode());

                        setupAppPropsGridRows();
                    }
                });
            }
        });

        setupAppPropsGridRows();
        return createMainScrollPanel(mainVp, null);
    }

//    @Override
//    public String getCaptionDeprecated() {
//        return "AppProps";
//    }
    private void setupAppPropsGridRows() {
        int propsRowNum = 0;

        while (grid.getRowCount() > propsRowNum) {
            grid.removeRow(grid.getRowCount() - 1);
        }

        final AppPropsBean apb = BIKClientSingletons.appPropsBean;

        saveAppPropsBtn.setEnabled(false);

        List<String> propNamesSort = new ArrayList<String>(apb.getPropNames());
        Collections.sort(propNamesSort);

        for (String propName : propNamesSort/*apb.getPropNames()*/) {
            Label propNameLbl = new Label(propName);
            propNameLbl.setStyleName("propNameLbl");
            final String properPropVal = BaseUtils.safeToString(apb.getPropValue(propName));

            propNameLbl.setTitle(properPropVal);
            grid.setWidget(propsRowNum, 0, propNameLbl);

            grid.getFlexCellFormatter().setColSpan(propsRowNum, 1, 2);

            final String oldPropVal = BIKClientSingletons.getAppPropVal(propName);

            if (BIKClientSingletons.isAppPropEditable(propName)) {
                final TextBox tb = new TextBox();
                tb.setText(oldPropVal);
                tb.setWidth("550px");
                tb.setStyleName("propNameTb");
                grid.setWidget(propsRowNum, 1, tb);
                editableProps.put(propName, tb);
                final String propNameFinal = propName;

                tb.addKeyUpHandler(new KeyUpHandler() {
                    public void onKeyUp(KeyUpEvent event) {
                        checkValueChanged(tb, propNameFinal, oldPropVal, properPropVal);

                    }
                });
                checkValueChanged(
                        tb, propNameFinal, oldPropVal, properPropVal);
            } else {
                grid.setText(propsRowNum, 1, oldPropVal);
            }

            propsRowNum++;
        }
        grid.getFlexCellFormatter().setColSpan(propsRowNum, 0, 3);
        grid.setWidget(propsRowNum, 0, saveAppPropsBtn);
    }

    private void checkValueChanged(TextBox tb, String propName, String oldPropVal, String properPropVal) {
        String propVal = tb.getText();
        String color;

        boolean valChanged = !BaseUtils.safeEquals(propVal, oldPropVal);

        if (valChanged) {
            color = "green" /* I18N: no */;
        } else if (BaseUtils.safeEquals(propVal, properPropVal)) {
            color = "white" /* I18N: no */;
        } else {
            color = "red" /* i18n: no */;
        }
        GWTUtils.setStyleAttribute(tb, "backgroundColor", color);

        BaseUtils.setAddOrRemoveItem(changedPropNames, propName, valChanged);

        saveAppPropsBtn.setEnabled(!changedPropNames.isEmpty());
    }

    public String getId() {
        return "admin:dict:bikAppProps";
    }

    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        // tu nic nie robimy, bo buildWidgets wszystko buduje i uzupełnia
    }
}
