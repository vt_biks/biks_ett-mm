exec sp_check_version '2.0.2';
GO


UPDATE bik_attribute
SET default_value = 'Tak'
WHERE attr_def_id = (
		SELECT id
		FROM bik_attr_def
		WHERE name = 'metaBiks.Pozwoli� dowi�za�'
		)

exec sp_update_version '2.0.2'
	,'2.0.3';
GO