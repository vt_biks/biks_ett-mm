/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

/**
 *
 * @author wezyr
 */
public interface IBeanAction<T> {
    public void perform(T bean, IParametrizedContinuation<String> whenDone);
}
