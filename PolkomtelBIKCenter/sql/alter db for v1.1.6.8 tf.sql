exec sp_check_version '1.1.6.8';
go

declare @articleTreeId int;
select @articleTreeId=id from bik_tree where code='Articles'

declare @documentsTreeId int;
select @documentsTreeId=id from bik_tree where code='Documents'

declare @folderDocNodeId int;
select @folderDocNodeId=id from bik_node_kind where code='DocumentsFolder'

declare @folderArtNodeId int;
select @folderArtNodeId=id from bik_node_kind where code='ArticleFolder'

update bik_tree
set name='Dokumentacja'
where code='Documents'

update bik_node_kind
set caption = 'Folder dokumentacji'
where code = 'DocumentsFolder'

update bik_node_kind
set tree_kind = 'Documents'
where tree_kind = 'Articles'

update bik_node
set tree_id = @documentsTreeId
where tree_id = @articleTreeId

update bik_node
set node_kind_id = @folderDocNodeId
where node_kind_id = @folderArtNodeId

go

-- dodac jeszcze slowniki

insert into bik_tree(code, name, node_kind_id, tree_kind)
values ('ShortcutsGlossary', 'S�ownik Skr�t�w', (select id from bik_node_kind where code='GlossaryCategory'),'Dictionary')

insert into bik_tree(code, name, node_kind_id, tree_kind)
values ('BusinessGlossary', 'S�ownik Poj�� Biznesowych', (select id from bik_node_kind where code='GlossaryCategory'),'Dictionary')



declare @GlossaryTreeId int;
select @GlossaryTreeId = id from bik_tree where code='Glossary'

declare @GlossaryFolderNodeId int;
select @GlossaryFolderNodeId = id from bik_node_kind where code='GlossaryCategory'

declare @GlossaryShortcutsTreeId int;
select @GlossaryShortcutsTreeId = id from bik_tree where code = 'ShortcutsGlossary'

declare @GlossaryBusinessTreeId int;
select @GlossaryBusinessTreeId = id from bik_tree where code = 'BusinessGlossary'

declare @documentsTreeId int;
select @documentsTreeId=id from bik_tree where code='Documents'

declare @folderDocNodeId int;
select @folderDocNodeId=id from bik_node_kind where code='DocumentsFolder'

declare @GlossaryNodeId int;
select @GlossaryNodeId=id from bik_node_kind where code='Glossary'

declare @GlossaryCategoryNodeId int;
select @GlossaryCategoryNodeId=id from bik_node_kind where code='GlossaryCategory'

declare @GlossaryVersionNodeId int;
select @GlossaryVersionNodeId=id from bik_node_kind where code='GlossaryVersion'

declare @GlossaryNotCorpoNodeId int;
select @GlossaryNotCorpoNodeId=id from bik_node_kind where code='GlossaryNotCorpo'

declare @ArticleNodeId int;
select @ArticleNodeId=id from bik_node_kind where code='Article'


--------- Pion Marketingu do s�ownika skr�t�w
declare @PionBranchId varchar(max);
select @PionBranchId = branch_ids from bik_node 
where name='Pion Marketingu' and parent_node_id is null and is_deleted=0 
and linked_node_id is null and tree_id=@GlossaryTreeId and node_kind_id=@GlossaryFolderNodeId

update bik_node
set tree_id = @GlossaryShortcutsTreeId
where branch_ids like @PionBranchId + '%' and is_deleted = 0 and linked_node_id is null and tree_id=@GlossaryTreeId

--------- Korporacyjny S�ownik Skr�t�w do s�ownika skr�t�w
declare @KorpBranchId varchar(max);
select @KorpBranchId = branch_ids from bik_node 
where name='Korporacyjny S�ownik Skr�t�w' and parent_node_id is null and is_deleted=0 
and linked_node_id is null and tree_id=@GlossaryTreeId and node_kind_id=@GlossaryFolderNodeId

update bik_node
set tree_id = @GlossaryShortcutsTreeId
where branch_ids like @KorpBranchId + '%' and is_deleted = 0 and linked_node_id is null and tree_id=@GlossaryTreeId


--------- DKPiNT do poj�� biznesowych
declare @DKPiNTBranchId varchar(max);
select @DKPiNTBranchId = branch_ids from bik_node 
where name='DKPiNT' and parent_node_id is null and is_deleted=0 
and linked_node_id is null and tree_id=@GlossaryTreeId and node_kind_id=@GlossaryFolderNodeId

update bik_node
set tree_id = @GlossaryBusinessTreeId
where branch_ids like @DKPiNTBranchId + '%' and is_deleted = 0 and linked_node_id is null and tree_id=@GlossaryTreeId


--------- DWH - Kontekst biznesowy, przyk�ady analiz do poj�� biznesowych
declare @DWHBranchId varchar(max);
select @DWHBranchId = branch_ids from bik_node 
where name='DWH - Kontekst biznesowy, przyk�ady analiz' and parent_node_id is null and is_deleted=0 
and linked_node_id is null and tree_id=@GlossaryTreeId and node_kind_id=@GlossaryFolderNodeId

--------- update folder�w
update bik_node
set tree_id = @documentsTreeId, node_kind_id=@folderDocNodeId
where branch_ids like @DWHBranchId + '%' and is_deleted = 0 and linked_node_id is null and tree_id=@GlossaryTreeId
and node_kind_id = @GlossaryCategoryNodeId

--------- update Definicja korporacyjna
update bik_node
set tree_id = @documentsTreeId, node_kind_id=@ArticleNodeId, descr = bm.body
from bik_node join bik_metapedia bm on bm.node_id=bik_node.id
where branch_ids like @DWHBranchId + '%' and is_deleted = 0 and linked_node_id is null and tree_id=@GlossaryTreeId
and node_kind_id = @GlossaryNodeId

--------- update Definicja robocza i inna
update bik_node
set tree_id = @documentsTreeId, node_kind_id = @ArticleNodeId, descr = bm.body, parent_node_id = bn2.parent_node_id
from bik_node join bik_metapedia bm on bm.node_id=bik_node.id
join bik_node bn2 on bik_node.parent_node_id = bn2.id
where bik_node.branch_ids like @DWHBranchId + '%' and bik_node.is_deleted = 0 and bik_node.linked_node_id is null and bik_node.tree_id=@GlossaryTreeId
and bik_node.node_kind_id in (@GlossaryNotCorpoNodeId, @GlossaryVersionNodeId)

-------------------------------------
update bik_searchable_attr_val 
  --select *
  set tree_id = n.tree_id, node_kind_id = n.node_kind_id 
  from bik_searchable_attr_val av inner join bik_node n on av.node_id = n.id
  where av.tree_id <> n.tree_id or av.node_kind_id <> n.node_kind_id
go

------------------------------------------------------------------
------------------------------------------------------------------

-- poprzednia wersja w pliku: "alter db for v1.1.6 mm"
delete from bik_node where tree_id = dbo.fn_tree_id_by_code('TreeOfTrees')
go

exec sp_add_menu_node null, 'M�j BIKS', '#MyBIKS'
exec sp_add_menu_node null, 'Biblioteka', 'metadata'
exec sp_add_menu_node 'metadata', 'SAP BO', 'sapbo'
exec sp_add_menu_node 'sapbo', '@', '$Reports'
exec sp_add_menu_node 'sapbo', '@', '$ObjectUniverses'
exec sp_add_menu_node 'sapbo', '@', '$Connections'
exec sp_add_menu_node 'metadata', '@', '$Teradata'
exec sp_add_menu_node 'metadata', '@', '$DQC'
exec sp_add_menu_node null, '@', '$Glossary'
exec sp_add_menu_node null, 'S�owniki', '@Dictionary'
--exec sp_add_menu_node null, 'Kategoryzacje', '@Taxonomy'
--exec sp_add_menu_node null, 'Dokumenty', '$Documents'
--exec sp_add_menu_node null, 'Blogi', '$Blogs'
exec sp_add_menu_node null, 'Baza Wiedzy', 'knowledge'
exec sp_add_menu_node null, 'Spo�eczno�� BI', 'community'
exec sp_add_menu_node null, 'Admin', '#Admin'
exec sp_add_menu_node null, 'Szukaj', '#Search'


exec sp_add_menu_node '$DQC', '@', '&DQCGroup'
exec sp_add_menu_node '&DQCGroup', '@', '&DQCTestSuccess'
exec sp_add_menu_node '&DQCGroup', '@', '&DQCTestFailed'
exec sp_add_menu_node '&DQCGroup', '@', '&DQCTestInactive'

exec sp_add_menu_node '$Teradata', '@', '&TeradataOwner'
exec sp_add_menu_node '&TeradataOwner', '@', '&TeradataSchema'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataView'
exec sp_add_menu_node '&TeradataView', '@', '&TeradataColumn'

exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataTable'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumn'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumnPK'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumnIDX'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataProcedure'
exec sp_add_menu_node '$Reports', '@', '&ReportFolder'
exec sp_add_menu_node '&ReportFolder', '@', '&Webi'
exec sp_add_menu_node '&ReportFolder', '@', '&FullClient'
exec sp_add_menu_node '&Webi', '@', '&ReportQuery'
exec sp_add_menu_node '&ReportQuery', '@', '&Dimension$Reports'
exec sp_add_menu_node '&ReportQuery', '@', '&Measure'
exec sp_add_menu_node '&ReportQuery', '@', '&Detail'
exec sp_add_menu_node '&ReportFolder', '@', '&Flash'
exec sp_add_menu_node '&ReportFolder', '@', '&Excel'
exec sp_add_menu_node '&ReportFolder', '@', '&Hyperlink'
exec sp_add_menu_node '&ReportFolder', '@', '&Powerpoint'
exec sp_add_menu_node '&ReportFolder', '@', '&Pdf'
exec sp_add_menu_node '&ReportFolder', '@', '&CrystalReport'
exec sp_add_menu_node '$ObjectUniverses', '@', '&UniversesFolder'
exec sp_add_menu_node '&UniversesFolder', '@', '&Universe'

exec sp_add_menu_node '&Universe', '@', '&UniverseDerivedTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseAliasTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseTable'

exec sp_add_menu_node '&Universe', '@', '&UniverseClass'
exec sp_add_menu_node '&UniverseClass', '@', '&Dimension'
exec sp_add_menu_node '&UniverseClass', '@', '&Measure'
exec sp_add_menu_node '&Dimension', '@', '&Detail'
exec sp_add_menu_node '&UniverseClass', '@', '&Filter'

exec sp_add_menu_node '$Connections', '@', '&ConnectionNetworkFolder'
exec sp_add_menu_node '&ConnectionNetworkFolder', '@', '&ConnectionEngineFolder'
exec sp_add_menu_node '&ConnectionEngineFolder', '@', '&DataConnection'

exec sp_add_menu_node 'knowledge', 'Dokumentacja', '$Documents'
exec sp_add_menu_node 'knowledge', 'Kategoryzacje', '@Taxonomy'

exec sp_add_menu_node '@Taxonomy', '@', '&TaxonomyEntity'

exec sp_add_menu_node 'community', 'Blogi', '$Blogs'
exec sp_add_menu_node 'community', 'U�ytkownicy', '$Users'


exec sp_add_menu_node '$Documents', '@', '&DocumentsFolder'
exec sp_add_menu_node '&DocumentsFolder', '@', '&Document'
exec sp_add_menu_node '&DocumentsFolder', '@', '&Article'

exec sp_add_menu_node '$Users', '@', '&UsersGroup'
exec sp_add_menu_node '&UsersGroup', '@', '&User'
exec sp_add_menu_node '$Blogs', '@', '&Blog'

exec sp_add_menu_node '&Blog', '@', '&BlogEntry'

exec sp_add_menu_node '$Glossary', '@', '&GlossaryCategory$Glossary'

exec sp_add_menu_node '&GlossaryCategory$Glossary', '@', '&Glossary'
exec sp_add_menu_node '&GlossaryCategory$Glossary', '@', '&GlossaryNotCorpo'
exec sp_add_menu_node '&GlossaryCategory$Glossary', '@', '&GlossaryVersion'

exec sp_add_menu_node '@Dictionary', '@', '&GlossaryCategory'

exec sp_add_menu_node '&GlossaryCategory', '@', '&Glossary'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryNotCorpo'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryVersion'

go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

--------------------------------------------------------------------
--------------------------------------------------------------------

declare @articleTree varchar(max);
select @articleTree = id from bik_tree where code = 'Articles'

declare @docTree varchar(max);
select @docTree = id from bik_tree where code = 'Documents'

declare @sgloTree varchar(max);
select @sgloTree = id from bik_tree where code = 'ShortcutsGlossary'

declare @bgloTree varchar(max);
select @bgloTree = id from bik_tree where code = 'BusinessGlossary'

declare @gloTree varchar(max);
select @gloTree = id from bik_tree where code = 'Glossary'

declare @tmp varchar(max);
set @tmp = 'n.tree_id in (' + @articleTree + ', ' + @docTree + ', ' + @sgloTree + ', ' + @bgloTree + ', ' + @gloTree + ')'; 

exec sp_verticalize_node_attrs @tmp;
go

exec sp_update_version '1.1.6.8', '1.1.6.9';
go