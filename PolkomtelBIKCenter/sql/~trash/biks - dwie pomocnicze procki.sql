if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_print_and_fix_tran_log]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_print_and_fix_tran_log]
go

create procedure [dbo].[sp_print_and_fix_tran_log] (@step_name varchar(max))
as
begin
  set nocount on

  declare @size float
  select @size = sum(size* 8.0)/1024.0 FROM sys.database_files WHERE type = 1
  
  print cast(sysdatetime() as varchar(23)) + ': ' + @step_name + ': rozmiar log�w ' + cast(@size as varchar(100)) + ' MB przed shrink'

  declare @log_name varchar(max);
  select @log_name=name from sys.database_files where type = 1
  exec('DBCC SHRINKFILE (N''' + @log_name + '''  , 0)')
  
  select @size = sum(size* 8.0)/1024.0 FROM sys.database_files WHERE type = 1

  print cast(sysdatetime() as varchar(23)) + ': ' + @step_name + ': rozmiar log�w ' + cast(@size as varchar(100)) + ' MB po shrink'
end
go


exec sp_print_and_fix_tran_log 'aqq'


-- 36 minut
-- 16 GB




if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_drop_pk_col]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_drop_pk_col]
go

create procedure [dbo].[sp_drop_pk_col] (@tab_name varchar(max), @col_name varchar(max))
as
begin
  set nocount on

declare @idx_name sysname
select @idx_name = i.name
from sys.indexes i
inner join sys.index_columns ic on i.index_id = ic.index_id and ic.object_id = i.object_id
inner join sys.columns c on ic.object_id = c.object_id and ic.column_id = c.column_id
inner join sys.objects o on i.object_id = o.object_id
where o.name = @tab_name and c.name = @col_name

exec ('alter table ' + @tab_name + ' drop constraint ' + @idx_name)

exec ('alter table ' + @tab_name + ' drop column ' + @col_name)
end
go



  select sum(size* 8.0)/1024.0 FROM sys.database_files WHERE type = 1
