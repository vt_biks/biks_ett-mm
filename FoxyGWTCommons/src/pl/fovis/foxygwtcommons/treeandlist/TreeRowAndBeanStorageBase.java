/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.treeandlist;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import pl.fovis.foxygwtcommons.ITreeBroker;

/**
 *
 * @author wezyr
 */
public abstract class TreeRowAndBeanStorageBase<IDT, TR, BEAN> extends SimpleTreeRowStorage<IDT, TR>
        implements ITreeRowAndBeanStorage<IDT, TR, BEAN>, ITreeBroker<TR, IDT> {

    protected Map<IDT, BEAN> beanMap = new HashMap<IDT, BEAN>();

    @SuppressWarnings("LeakingThisInConstructor")
    public TreeRowAndBeanStorageBase() {
        super();
        this.treeBroker = this;
    }

    protected abstract TR convertBeanToRow(BEAN bean);

    public abstract IDT getNodeId(TR n);

    public abstract IDT getParentId(TR n);

    public BEAN getBeanById(IDT nodeId) {
        return beanMap.get(nodeId);
    }

    public Collection<BEAN> getAllBeans() {
        return beanMap.values();
    }

    @Override
    public TR registerAndConvertBeanToRow(BEAN bean) {
        TR row = convertBeanToRow(bean);

        beanMap.put(getNodeId(row), bean);

        addRow(row);

        return row;
    }
}
