/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.simpledblib;

import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class ConnectionFactory {

    public static IConnection createConnection(ConnectionConfig connCfg) {
        IConnection res = null;

        if ("jdbc".equals(connCfg.kind)) {
            res = new ConnectionViaJdbc3(connCfg.props);
        }

        if (res == null) {
            throw new LameRuntimeException("cannot create connection for kind: " + connCfg.kind);
        }

        return res;
    }
}
