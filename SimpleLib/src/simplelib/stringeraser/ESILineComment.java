/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.stringeraser;

import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public class ESILineComment extends ESISqlPrefixSuffixDelimitedString {

    public ESILineComment() {
        // suffix jest zupełnie nieistotny, tak sobie tutaj wrzucony
        super("--", "CR[LF]|LF");
    }

    @Override
    protected int findSuffixPos(String content, int pos) {
//        int len = content.length();
//        while (pos < len) {
//            char c = content.charAt(pos);
//
//            if (c == '\r' || c == '\n') {
//                break;
//            }
//
//            pos++;
//        }
//
//        // albo znaleziony CR lub LF, albo koniec stringa (też możliwe)
//        return pos;

        return BaseUtils.findFirstLineBreak(content, pos, true);
    }

    @Override
    protected int findAfterSuffixPos(String content, int suffixPos) {
//        int len = content.length();
//        // przypadek szczególny - komentarz liniowy do końca stringa, brak końca linii
//        if (suffixPos == len) {
//            return suffixPos;
//        }
//
//        char c = content.charAt(suffixPos);
//        // LF albo CR bez LF za nim - jeden znak nowej linii jest tylko
//        if (c == '\n' || suffixPos + 1 < len && content.charAt(suffixPos + 1) != '\n') {
//            return suffixPos + 1;
//        }
//        // było CRLF
//        return suffixPos + 2;

        return suffixPos; // nic nie robimy, koniec linii tylko ogranicza, ale nie należy do komentarza
    }
}
