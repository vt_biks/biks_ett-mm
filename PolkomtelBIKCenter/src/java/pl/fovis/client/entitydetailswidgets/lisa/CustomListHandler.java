/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import java.util.List;
import java.util.Map;
import pl.fovis.common.lisa.LisaDictionaryValue;

/**
 *
 * @author ctran
 */
public class CustomListHandler extends ListHandler<Map<String, LisaDictionaryValue>> {

    private boolean isInChangeCategoryMode;
    private IDataGrid displayer;

    public CustomListHandler(List<Map<String, LisaDictionaryValue>> list, IDataGrid displayer) {
        super(list);
        this.displayer = displayer;
    }

    public void setIsInChangeCategoryMode(boolean isInChangeCategoryMode) {
        this.isInChangeCategoryMode = isInChangeCategoryMode;
    }

    @Override
    public void onColumnSort(ColumnSortEvent event) {
        Map<String, LisaDictionaryValue> blankRecord = null;
//        if (!isInChangeCategoryMode) {
//            blankRecord = getList().remove(0);
//        }
        displayer.changeDirectionOrder();
        //Window.alert("" + displayer.getPageNum());
        displayer.getData(displayer.getPageNum(), displayer.getPageSize());
        super.onColumnSort(event); //To change body of generated methods, choose Tools | Templates.
//        if (!isInChangeCategoryMode) {
//            getList().add(0, blankRecord);
//        }
        //displayer.updateDataGrid(dataList);
    }
}
