if object_id(N'dbo.fn_compact_white_chars', N'fn') is not null
    drop function dbo.fn_compact_white_chars;
go

/*
   @trim_or_purge_level:
     0 - bez trim i purge
	 1 - ltrim
	 2 - rtrim
	 3 - ltrim, rtrim
	 4 - purge (wywala wszystkie bia�e znaki)
*/
create function dbo.fn_compact_white_chars(@text varchar(max), @trim_or_purge_level int)
returns varchar(max)
with execute as caller
as
begin
  if @text is null return '';

  declare @i int = 0;

  while @i < 32 begin

	set @text = replace(@text, char(@i), ' ');

    set @i += 1;
  end;

  if @trim_or_purge_level = 4 begin
    return replace(@text, ' ', '');
  end;

  if @trim_or_purge_level & 1 <> 0
    set @text = ltrim(@text);

  if @trim_or_purge_level & 2 <> 0
    set @text = rtrim(@text);

  declare @do_more int = 1, @len int;

  while @do_more <> 0 begin
    set @len = len(@text);
	set @text = replace(@text, '  ', ' ');
	set @do_more = case when len(@text) <> @len then 1 else 0 end;
  end;

  return @text;
end;
go



select '[' + dbo.fn_compact_white_chars('   ala  ma 

kota   ', 0) + ']'

select '[' + dbo.fn_compact_white_chars('   ala  ma 

kota   ', 4) + ']'


select 'ala  ma 

kota'









select * from bik_node_kind p full join BIKS_Cyfrowy_2015..bik_node_kind c on p.code = c.code


select * from bik_app_prop p full join BIKS_Cyfrowy_2015..bik_app_prop c on p.name = c.name
where p.id is null or c.id is null or coalesce(p.val, '') <> coalesce(c.val, '')

-- availableConnectors, hideDictionaryRootTab, ignoreRefreshOnOpenForReportSDK, languages, load_user_pwd, searchPageClickTreeNodeToSearch

/*
Polkomtel: Business Objects 3.1, Teradata DBMS, DQC, Active Directory, SAP BW, Business Objects 4.x, Erwin Data Model, Teradata Data Model
Cyfrowy: Business Objects 3.1, MS SQL, Confluence, Active Directory, Business Objects 4.x
*/


select 'polkomtel', * from bik_app_prop where name = 'bik_ver'
union all
select 'cyfrowy', * from BIKS_Cyfrowy_2015..bik_app_prop where name = 'bik_ver'
;



select * from bik_help p full join BIKS_Cyfrowy_2015..bik_help c on p.help_key = c.help_key and p.lang = c.lang 
where (p.is_hidden = 0 or c.is_hidden = 0) and (p.is_hidden <> c.is_hidden or
p.help_key is null or c.help_key is null or coalesce(p.caption, '') <> coalesce(c.caption, '')
or dbo.fn_compact_white_chars(p.text_help, 3) <> dbo.fn_compact_white_chars(c.text_help, 3))


select * from BIKS_Cyfrowy_2015..bik_help where help_key = 'SASCubes'



select * from bik_translation p full join BIKS_Cyfrowy_2015..bik_translation c on p.code = c.code and p.lang = c.lang and p.kind = c.kind
where coalesce(p.txt, '') <> coalesce(c.txt, '') and p.id is not null and c.id is not null


select * from bik_admin p full join BIKS_Cyfrowy_2015..bik_admin c on p.param = c.param
where coalesce(p.value, '') <> coalesce(c.value, '')


select * from bik_attr_category p full join BIKS_Cyfrowy_2015..bik_attr_category c on p.name = c.name
where coalesce(p.value, '') <> coalesce(c.value, '')

select * from BIKS_Cyfrowy_2015..bik_attr_category



select * from (bik_attr_def pd inner join bik_attr_category pc on pd.attr_category_id = pc.id)
  full join (bik_attr_def cd inner join bik_attr_category cc on cd.attr_category_id = cc.id) on pd.name = cd.name and pd.is_built_in = cd.is_built_in
where pd.id is null or cd.id is null or pc.name <> cc.name or coalesce(pd.hint, '') <> coalesce(cd.hint, '') or pd.id <> cd.id





availableConnectors, hideDictionaryRootTab, ignoreRefreshOnOpenForReportSDK, languages, load_user_pwd, searchPageClickTreeNodeToSearch




select * from bik_icon p full join BIKS_Cyfrowy_2015..bik_icon c on p.name = c.name
where p.id is null or c.id is null or p.id <> c.id



select * from bik_data_source_def p full join BIKS_Cyfrowy_2015..bik_data_source_def c on p.description = c.description
where p.id is null or c.id is null or p.id <> c.id


select * from bik_node_kind p full join BIKS_Cyfrowy_2015..bik_node_kind c on p.code = c.code
where p.id is null or c.id is null /*or p.id <> c.id*/ or p.caption <> c.caption or p.icon_name <> c.icon_name or 
p.tree_kind <> c.tree_kind or p.is_folder <> c.is_folder or p.search_rank <> c.search_rank or p.is_leaf_for_statistic <> c.is_leaf_for_statistic



-- s� r�nice w ID
select * from bik_tutorial p full join BIKS_Cyfrowy_2015..bik_tutorial c on p.code = c.code and p.lang = c.lang
where p.id is null or c.id is null or /*p.id <> c.id or*/ p.title <> c.title or coalesce(p.movie, '') <> coalesce(c.movie, '') or p.visual_order <> c.visual_order
  or dbo.fn_compact_white_chars(p.text, 3) <> dbo.fn_compact_white_chars(c.text, 3)


select * from bik_statistic_dict p full join BIKS_Cyfrowy_2015..bik_statistic_dict c on p.counter_name = c.counter_name
where p.id is null or c.id is null /*or p.id <> c.id*/ or p.description <> c.description



select * from bik_tree p full join BIKS_Cyfrowy_2015..bik_tree c on p.code = c.code
where p.id is null or c.id is null /*or p.id <> c.id*/ 


select * from bik_tree p inner join BIKS_Cyfrowy_2015..bik_tree c on p.code = c.code
where p.is_hidden = 0 and c.is_hidden = 0

select * from bik_tree where tree_kind = 'Users'

select * from BIKS_Cyfrowy_2015..bik_tree where tree_kind = 'Users'


select count(*) from sys.objects where type = 'U'


select count(*) from sys.objects where type = 'U' and name like 'bik[_]%'

