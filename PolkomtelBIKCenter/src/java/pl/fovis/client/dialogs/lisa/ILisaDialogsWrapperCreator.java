/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import pl.fovis.foxygwtcommons.dialogs.IHasInputControls;
import pl.trzy0.foxy.commons.annotations.CreatorPackage;
import simplelib.INamedTypedBeanPropsBeanWrapperCreator;

/**
 *
 * @author tbzowka
 */

@CreatorPackage(value = "/pl/fovis/client/dialogs/lisa")
public interface ILisaDialogsWrapperCreator extends INamedTypedBeanPropsBeanWrapperCreator<IHasInputControls>{
    
}
