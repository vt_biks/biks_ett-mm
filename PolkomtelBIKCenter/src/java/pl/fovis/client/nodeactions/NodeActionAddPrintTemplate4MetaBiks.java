/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class NodeActionAddPrintTemplate4MetaBiks extends NodeActionAddNodeBaseOnTreeKindBase/* NodeActionBase*/ {

    @Override
    public boolean isEnabledByDesign() {

        if (!BIKClientSingletons.isEnablePrinting()) {
            return false;
        }

        return isMetaBiks() && ((BIKConstants.NODE_KIND_META_NODE_KIND.equalsIgnoreCase(node.nodeKindCode)
                && data.isTemplatesFolderAdd)
                || BIKConstants.NODE_KIND_META_PRINTS_TEMPLATES.equalsIgnoreCase(node.nodeKindCode));
    }

    @Override
    public String getCaption() {

        return BIKConstants.NODE_KIND_META_PRINTS_TEMPLATES.equalsIgnoreCase(node.nodeKindCode) ? I18n.dodajSzablonWydruku.get() : I18n.dodajGrupeSzablonow.get();
    }

    @Override
    public void execute() {

        final boolean isPrintTemplates = BIKConstants.NODE_KIND_META_PRINTS_TEMPLATES.equalsIgnoreCase(node.nodeKindCode);
        NodeKindBean nk = BIKClientSingletons.getNodeKindBeanByCode(
                isPrintTemplates ? BIKConstants.NODE_KIND_META_PRINT : BIKConstants.NODE_KIND_META_PRINTS_TEMPLATES
        );
//        Window.alert(nk.id + " " + BIKConstants.NODE_KIND_META_PRINT + BIKConstants.NODE_KIND_META_PRINTS_TEMPLATES + node.nodeKindCode);
        nodeKindId = nk.id;
        nodeKindCode = nk.code;

        super.execute();
    }

//    protected Map<String, AttributeBean> getAttributesFromParent() {
//        if (BIKConstants.NODE_KIND_META_PRINTS_TEMPLATES.equalsIgnoreCase(node.nodeKindCode)) {
//            Map<String, AttributeBean> abs = new HashMap<String, AttributeBean>();
//            for (AttributeBean ab : data.attributes) {
//                ab.nodeId = 0;
//                abs.put(ab.atrName, ab);
////                Window.alert(ab.id + " " + ab.atrName);
//            }
//            return abs;
//        } else {
//            return null;
//        }
//    }
}
