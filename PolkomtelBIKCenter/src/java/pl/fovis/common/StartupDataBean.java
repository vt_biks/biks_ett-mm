/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.common.ConnectionParametersJdbcBean;

/**
 *
 * @author mgraczkowski
 */
public class StartupDataBean implements Serializable {

    public boolean isMultiMode = false;
    public boolean disableGuestMode = false;
    public boolean isUserCreatorOfThisDb = false;
    public boolean isNasActive = false;
    public boolean isConfluenceActive = false;
    public boolean userExistButDisabled = false;
    public String ssoUserName;
    public String biksDatabaseName; // dla Bżemga...
//    @Deprecated
//    public String localeLangInfo;
    public String appVersion;
    //public String dbVersion;
//    public UserBean loggedUser;
    public SystemUserBean loggedUser;
    public List<NodeKindBean> nodeKinds;
    public List<TreeBean> trees;
    public List<TreeKindBean> treeKinds;
    public List<TreeIconBean> treeIcons;
    //public boolean developerMode;
    //public String allowedShowDiagKinds;
//    public List<BikRoleForNodeBean> bikRolesForNode;
    public List<AppPropBean> appProps;
    public List<MenuNodeBean> menuNodes;
    public List<AttrHintBean> attrHints;
    public List<AttributeBean> attrDefAndCategory;
    public Map<Integer, List<Integer>> attrsDict;
    public List<AttrSearchableBean> attrsSearchable;
    public List<ConnectionParametersJdbcBean> jdbcConnectionTypes;
    public Map<Integer, List<AttributeBean>> systemAttribute;
     public Map<Integer, List<AttributeBean>>  allAttributesForNodeKindId;
    public Map<Integer, String> openReportLink;
    public Set<String> reportTreeCodes;
//    public BISystemsConnectionsParametersBean biSystems;
    public Set<String> inactiveTabs;
    public ChangedRankingBean changedRanking;
    public String welcomeMsgForUser;
    public String welcomeMsgForVersion;
    public Map<String, RightRoleBean> allRightRolesByCode;
    public Map<String, Map<String, String>> translationsGeneric;
    public Map<String, Map<String, String>> mssqlExProps;
    public String adDomains;
    public List<CustomRightRoleBean> customRightRoles;
    public Map<String, Integer> nodeActionCodeToIdMap;
    public Map<Integer, Set<Integer>> customRightRolesActionsInTreeRoots;
    public String treeSelectorForRegularUserCrr;
    public boolean showGrantsInTestConnection;
    public String developerModeStr;
    //---
    public String defaultSingleBiksDb;
    //---
    public boolean canSendMails;
    //---
    public Map<String, String> i18nTranslations;
    public boolean useSimpleMsgForUnloggedUser;
    public boolean isMultiDomainMode;
    public List<SystemUserGroupBean> allRoles;
}
