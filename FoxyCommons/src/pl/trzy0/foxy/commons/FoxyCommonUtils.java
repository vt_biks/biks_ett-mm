/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.IRegExpMatcher;
import simplelib.IRegExpPattern;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public class FoxyCommonUtils {

    protected static final String htmlUrlPatternPrefix2 = "#[a-zA-Z0-9]*/(";
    protected static final String nodeIdPattern = "[-]?[0-9]+";
    protected static final String htmlUrlPatternSuffix = ")[\"\'][^>]*>([^<]+)</a>";
    //
    protected static final String caption = "[#][^<]+";
    protected static final IRegExpPattern htmlTagPattern
            = BaseUtils.getRegExpMatcherFactory().compile("<([/]?)([^ \t\n\r>/]+)([^>]*)>",
                    true, true);
    protected static final Set<String> doNotClearFormattingForTagNames = BaseUtils.splitUniqueBySep("table,td,tr", ",", true);
    protected static final Map<String, String> clearFormattingTagConvertionMap = new HashMap<String, String>() {

        {
            put("h2", "p");
            put("span", "");
            put("b", "");
            put("strong", "");
            put("i", "");
        }
    };

    public static String replaceText(String text) {
        //te relplace narazie  usuwa tylko  " i ' .
        return BaseUtils.maleLiteryNaPolskawe(text/*.toLowerCase()*/.replace(" ", "").replace("\"", "").replace("\'", "").trim());
    }

//    public static Set<String> extractsContentsFromHtml(String html) {
//        IRegExpPattern htmlUrlPattern =
//                BaseUtils.getRegExpMatcherFactory().compile(spanPatternPrefix + caption + spanPatternSuffix,
//                true, true);
//        IRegExpMatcher htmlUrlMatcher = htmlUrlPattern.matcher(html);
//        Set<String> contents = new LinkedHashSet<String>();
//        while (htmlUrlMatcher.find()) {
//            String grp1 = htmlUrlMatcher.group(1);
//            contents.add(grp1);
//        }
//        return contents;
//    }
    public static String clearFormattingForRichTextArea(String html) {
        IRegExpMatcher matcher = htmlTagPattern.matcher(html);

        StringBuilder sb = new StringBuilder();
        int oldPos = 0;

        while (matcher.find()) {

            int start = matcher.start();
            if (oldPos < start) {
                sb.append(html.substring(oldPos, start));
            }

            String fullMatch = matcher.group(0);
            String tagName = matcher.group(2).toLowerCase();
            // closed tag, e.g. <abc/> is closed tag
            boolean isSelfClosed = fullMatch.charAt(fullMatch.length() - 2) == '/';
            // equals '/' for close tags, e.g. </span> is close tag, empty string for other tags
            String closeTagPrefix = matcher.group(1);

            //System.out.println("tagName=[" + tagName  + "], isClosed=" + isSelfClosed);
            String substitute = BaseUtils.nullToDef(clearFormattingTagConvertionMap.get(tagName), tagName);

            if (doNotClearFormattingForTagNames.contains(tagName)) {
                sb.append(fullMatch);
            } else if (!substitute.isEmpty()) {
                sb.append("<").append(closeTagPrefix).append(substitute);
                if (isSelfClosed) {
                    sb.append("/");
                }
                sb.append(">");
            } else {
                // no-op -> erase tag
            }

            oldPos = matcher.end();
        }

        sb.append(html.substring(oldPos));

        return sb.toString();
    }

    public static Set<Integer> extractNodeIdsFromHtml(String html) {
        IRegExpPattern htmlUrlPattern
                = BaseUtils.getRegExpMatcherFactory().compile(htmlUrlPatternPrefix2 + nodeIdPattern + htmlUrlPatternSuffix,
                        true, true);
        //Matcher htmlUrlMatcher = htmlUrlPattern.matcher(html);
        IRegExpMatcher htmlUrlMatcher = htmlUrlPattern.matcher(html.toLowerCase());
        Set<Integer> listNodeIds = new LinkedHashSet<Integer>();
        while (htmlUrlMatcher.find()) {
            String grp1 = htmlUrlMatcher.group(1);
            listNodeIds.add(BaseUtils.tryParseInteger(//html.substring(grpStart, grpEnd),
                    grp1, -1));
        }
        return listNodeIds;
    }

    public static boolean areAppAndDBVersionsCompatible(String appVer, String dbVer) {
        if (appVer == null || dbVer == null) {
            return false;
        }
        Pair<String, String> appVerMM = BaseUtils.splitString(appVer, FoxyCommonConsts.MAJOR_MINOR_VERSION_SEP);
        Pair<String, String> dbVerMM = BaseUtils.splitString(dbVer, FoxyCommonConsts.MAJOR_MINOR_VERSION_SEP);
//        if (BaseUtils.safeEquals(appVerMM.v1, dbVerMM.v1)) {
//            return true;
//        }
        if (BaseUtils.isSubVersionOfVersion(appVerMM.v1, dbVerMM.v1) || BaseUtils.isSubVersionOfVersion(dbVerMM.v1, appVerMM.v1)) {
            return true;
        }
        return false;
    }
}
