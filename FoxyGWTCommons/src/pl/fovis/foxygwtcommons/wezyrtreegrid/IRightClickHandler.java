/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.foxygwtcommons.wezyrtreegrid;

/**
 *
 * @author wezyr
 */
public interface IRightClickHandler<ROW> {
    public void rowRightClicked(ROW row, int clientX, int clientY);
}
