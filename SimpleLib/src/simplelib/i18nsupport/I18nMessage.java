/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.i18nsupport;

import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author pmielanczuk
 */
public abstract class I18nMessage {

    private static ICurrentLocaleNameProvider localeNameProvider = new ICurrentLocaleNameProvider() {
        @Override
        public String getCurrentLocaleName() {
            return DEFAULT_LOCALE_NAME;
        }
    };

    public static void setCurrentLocaleNameProvider(ICurrentLocaleNameProvider provider) {
        localeNameProvider = provider;
    }

    public static String getCurrentLocaleName() {
        return localeNameProvider.getCurrentLocaleName();
    }
    private String[] msgsInLangs;
    private int langCnt;
    private final Collection<String> localeNames;

    public I18nMessage(String... msgsInLangs) {
        this.msgsInLangs = msgsInLangs;
        this.langCnt = msgsInLangs.length;
        this.localeNames = getLocaleNamesPrivate();
    }

    private Collection<String> getLocaleNamesPrivate() {
        return getLocaleNames();
    }

    protected abstract Collection<String> getLocaleNames();

    private String get(int localeIdx) {
        if (localeIdx >= langCnt) {
            localeIdx = 0;
        }
        return msgsInLangs[localeIdx];
    }

    public final String get() {
        return get(getLocaleIdx());
    }

    public int getLocaleIdx() {
        String currentLocaleName = localeNameProvider.getCurrentLocaleName();
        Iterator<String> it = localeNames.iterator();
        int localeIdx = 0;
        int i = 0;
        while (it.hasNext()) {
            if (currentLocaleName.equals(it.next())) {
                localeIdx = i;
                break;
            }
            i++;
        }
        return localeIdx;
    }

//    @Override
//    public String toString() {
//        return get();
//    }
    public String format(Object... args) {
        return I18nParametrizedMessages.format(this, args);
    }

    public void override(String... msgsInLangs) {
        this.msgsInLangs = msgsInLangs;
        this.langCnt = msgsInLangs.length;
    }
}
