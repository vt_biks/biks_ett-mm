/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.searchengine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.morfologik.MorfologikAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ctran
 */
public class SearcherTest {

    protected final String indexDirectory = "C:\\TEMP\\LuceneTest";
    protected final Analyzer analyzer = new MorfologikAnalyzer();

    public SearcherTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws IOException {
        final FieldType docIdFieldType = new FieldType();
        docIdFieldType.setIndexOptions(IndexOptions.NONE);
        docIdFieldType.setStored(true);

        final FieldType isDeletedFieldType = new FieldType();
        isDeletedFieldType.setIndexOptions(IndexOptions.NONE);
        isDeletedFieldType.setStored(true);

        Indexer indexer = new Indexer(indexDirectory, analyzer, new IDocCrawler() {

            private int cnt = 0;

            @Override
            public List<Document> next() {
                List<Document> res = new ArrayList<Document>();
                if (cnt < 10) {
                    Document doc = new Document();
                    doc.add(new Field(SearchConstant.DOC_ID, Integer.toString(1), docIdFieldType));
                    doc.add(new TextField("bik_node_name", "zadań " + cnt, Field.Store.YES));
                    doc.add(new TextField("bik_node_descr", "łączy się ala", Field.Store.YES));
                    doc.add(new Field(SearchConstant.IS_DELETED, "0", isDeletedFieldType));
                    cnt++;
                    res.add(doc);
                    return res;
                } else {
                    return null;
                }
            }

            @Override
            public void setLastIndexedDate(Date lastIndexedDate) {
            }

            @Override
            public void setActualDate(Date actualDate) {
            }

            @Override
            public void restartDataProviders() {
            }
        });
        indexer.index();
    }

    @After
    public void tearDown() {
    }

    private Query parseQuery(String text, String[] fields) {
        BooleanQuery.Builder builder = new BooleanQuery.Builder();
        for (String field : fields) {
            builder.add(SearchUtils.parse(text, field), BooleanClause.Occur.SHOULD);
        }
        return builder.build();
    }

    /**
     * Test of searchAllField method, of class Searcher.
     */
    @Test
    public void testSearch() throws IOException, ParseException {
        System.out.println("searchAllField");
        String query = "Za";
//        String query = "KRI grupy danych nie jest wyliczane z uwagi na niską istotność";
//        String query = "Za okres 2016-P2";
        String text = "zadania pytania";
        BooleanQuery.Builder builder = new BooleanQuery.Builder();

        List<String> tokens = SearchUtils.tokenizeString(analyzer, text);
        for (String token : tokens) {
            System.out.println(token);
//            builder.add(parseQuery("*" + token + "*", new String[]{"bik_node_name"}), BooleanClause.Occur.SHOULD);
            builder.add(parseQuery(token, new String[]{"bik_node_name"}), BooleanClause.Occur.SHOULD);
        }
        builder.add(parseQuery(text, new String[]{"bik_node_name"}), BooleanClause.Occur.SHOULD);
//        builder.add(parseQuery("\"" + text + "\"", BiksSearchConstant.ALL_TEXT_FIELDS), BooleanClause.Occur.SHOULD);

        Searcher instance = new Searcher(indexDirectory, analyzer);
        List<Document> result = instance.search(builder.build());
        for (Document doc : result) {
            System.out.println(doc.get("bik_node_name"));
        }
    }
}
