/*
select 'update bik_node_kind set search_rank = ' + cast(search_rank as varchar(20)) + ' where code = ''' + code + ''''
from bik_node_kind
order by tree_kind, code



update bik_node_kind set search_rank = 10 where code = 'Glossary'

update bik_node_kind set search_rank = 9 where code = 'GlossaryNotCorpo'

update bik_node_kind set search_rank = 8 where code = 'GlossaryVersion'
go

-- Wyszukiwarka: priorytety wyszukiwania: 1.Glosariusz,
-- 2.Kategoryzacja,
-- 3.Metadane (kolejnosc jak w drzewku), 
-- 4.Dokumenty,Blogi,Użytkownicy,FAQ'
*/

--------------------------------------------------------------
--------------------------------------------------------------

update bik_node_kind set search_rank = 0 where code = 'MenuItem'

update bik_node_kind set search_rank = 20 where code = 'Glossary'
update bik_node_kind set search_rank = 19 where code = 'GlossaryCategory'
update bik_node_kind set search_rank = 19 where code = 'GlossaryNotCorpo'
update bik_node_kind set search_rank = 18 where code = 'GlossaryVersion'
update bik_node_kind set search_rank = 17 where code = 'Keyword'
update bik_node_kind set search_rank = 0 where code = 'KeywordFolder'

update bik_node_kind set search_rank = 17 where code = 'TaxonomyEntity'

update bik_node_kind set search_rank = 17 where code = 'ReportFolder'
update bik_node_kind set search_rank = 16 where code = 'Webi'
update bik_node_kind set search_rank = 15 where code = 'FullClient'
update bik_node_kind set search_rank = 15 where code = 'CrystalReport'
update bik_node_kind set search_rank = 15 where code = 'Word'
update bik_node_kind set search_rank = 15 where code = 'Excel'
update bik_node_kind set search_rank = 15 where code = 'Flash'
update bik_node_kind set search_rank = 15 where code = 'Hyperlink'
update bik_node_kind set search_rank = 15 where code = 'Measure'
update bik_node_kind set search_rank = 15 where code = 'Pdf'
update bik_node_kind set search_rank = 15 where code = 'Txt'
update bik_node_kind set search_rank = 15 where code = 'Rtf'
update bik_node_kind set search_rank = 15 where code = 'Powerpoint'
update bik_node_kind set search_rank = 15 where code = 'ReportQuery'


update bik_node_kind set search_rank = 15 where code = 'UniversesFolder'
update bik_node_kind set search_rank = 14 where code = 'Universe'
update bik_node_kind set search_rank = 13 where code = 'UniverseAliasTable'
update bik_node_kind set search_rank = 13 where code = 'UniverseClass'
update bik_node_kind set search_rank = 13 where code = 'UniverseDerivedTable'
update bik_node_kind set search_rank = 13 where code = 'UniverseTable'
update bik_node_kind set search_rank = 12 where code = 'Detail'
update bik_node_kind set search_rank = 12 where code = 'Dimension'
update bik_node_kind set search_rank = 12 where code = 'Filter'

update bik_node_kind set search_rank = 11 where code = 'ConnectionEngineFolder'
update bik_node_kind set search_rank = 11 where code = 'ConnectionUserFolder'
update bik_node_kind set search_rank = 0 where code = 'ConnectionFolder'
update bik_node_kind set search_rank = 10 where code = 'DataConnection'

update bik_node_kind set search_rank = 09 where code = 'TeradataSchema'
update bik_node_kind set search_rank = 8 where code = 'TeradataView'
update bik_node_kind set search_rank = 8 where code = 'TeradataTable'
update bik_node_kind set search_rank = 7 where code = 'TeradataProcedure'
update bik_node_kind set search_rank = 7 where code = 'TeradataColumn'

update bik_node_kind set search_rank = 6 where code = 'Blog'
update bik_node_kind set search_rank = 5 where code = 'BlogEntry'

update bik_node_kind set search_rank = 6 where code = 'DocumentsFolder'
update bik_node_kind set search_rank = 5 where code = 'Document'

update bik_node_kind set search_rank = 6 where code = 'UsersGroup'
update bik_node_kind set search_rank = 5 where code = 'User'
update bik_node_kind set search_rank = 0 where code = 'AllUsersFolder'

update bik_node_kind set search_rank = 6 where code = 'CategoriesOfQuestions'
update bik_node_kind set search_rank = 5 where code = 'Question'
go
