/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class SapBoExtraDataBean implements Serializable {

    public String author;
    public String owner;
    public String keyword;
    public String filePath;
    public String fileName;
    public Date created;
    public Date modified;
    public String cuid;
    public Date lastRunTime;
    public String filesValue;
    public String size;
    public Integer hasChildren;
    public String guid;
    public Integer instance;
    public Integer owner_id;
    public String progid;
    public Integer obtype;
    public Integer flags;
    public Integer children;
    public String ruid;
    public Integer runnableObject;
    public String contentLocale;
    public Integer isSchedulable;
    public String webiDocProperties;
    public Integer readOnly;
    public String lastSuccessfulInstanceId;
    public Date timestamp;
    public String progidMachine;
    public Date endtime;
    public Date starttime;
    public String scheduleStatus;
    public String recurring;
    public Date nextruntime;
    public String docSender;
    public Integer revisionnum;
    public Integer applicationObject;
    public String shortname;
    public Integer dataconnectionTotal;
    public Integer universeTotal;
    public String statistic;
    
}
