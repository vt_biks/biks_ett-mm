/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.Collection;
import pl.fovis.foxygwtcommons.popupmenu.IPopupMenuItemAction;

/**
 *
 * @author wezyr
 */
public interface INodeAction extends IPopupMenuItemAction<INodeActionContext> {

//    public boolean canBeEnabledByStateForThisNode();
//    public boolean canBeEnabledByStateForThisNodeAndUserLoggedIn();
    public Collection<String> getOptNodeActionCodes();

    public String getOptCurrentNodeActionCode();
}
