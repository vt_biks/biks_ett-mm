﻿exec sp_check_version '1.7.z6.8';
go
------------------------
update bik_attr_def set type_attr = 'shortText' where is_built_in = 1 and type_attr = null;
update bik_searchable_attr set caption = 'Nazwa techniczna' where name in ('obj_technical_id', 'technical_id') 
update bik_searchable_attr set caption = 'Osoba odpowiedzialna' where name in ('sapbw_owner') 
update bik_searchable_attr set caption = 'Ostatnia zmiana' where name in ('update_time') 
update bik_searchable_attr set caption = 'Zmieniony przez' where name in ('last_edit') 
update bik_searchable_attr set caption = 'Formaty' where name in ('format') 
update bik_searchable_attr set caption = 'Parametry' where name in ('parameters') 
update bik_searchable_attr set caption = 'Właściciel' where name in ('schedule_owner') 
update bik_searchable_attr set caption = 'ad_company' where name in ('company')
update bik_attr_def set type_attr = 'data' 
where is_built_in = 1
and name in (
'Create Date', 
'Data aktywacji', 
'Data dezaktywacji', 
'Data modyfikacji', 
'Data ostatniego uruchomienia', 
'Data utworzenia', 
'Endtime', 
'Godzina następnego uruchomienia', 
'Godzina utworzenia', 
'Godzina wygaśnięcia',
'Nextruntime', 
'Starttime', 
'Suspend Date', 
'Timestamp')

------------------
---------------------------------------------------------------------
exec sp_update_version '1.7.z6.8', '1.7.z6.9'
go