/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import commonlib.MuppetMaker;
import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import pl.bssg.metadatapump.ThreadedPumpExecutor;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.common.SchedulePumpBean;
import pl.bssg.metadatapump.confluence.ConfluenceConfigBean;
import pl.bssg.metadatapump.confluence.ConfluenceConnector;
import pl.bssg.metadatapump.confluence.ConfluenceProxy;
import pl.bssg.metadatapump.dqc.DQCTestRequestsReceiver;
import pl.bssg.metadatapump.sapbo.biadmin.BIAConnector;
import pl.bssg.metadatapump.sapbo.biadmin.BIAProxy;
import pl.fovis.common.AppPropBean;
import pl.fovis.common.BIArchiveConfigBean;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.ChangedRankingBean;
import pl.fovis.common.ConnectionParametersADBean;
import pl.fovis.common.ConnectionParametersAuditBean;
import pl.fovis.common.ConnectionParametersBIAdminBean;
import pl.fovis.common.ConnectionParametersConfluenceBean;
import pl.fovis.common.ConnectionParametersDQCBean;
import pl.fovis.common.ConnectionParametersSapBoBean;
import pl.fovis.common.DroolsConfigBean;
import pl.fovis.common.FvsChangeExBean;
import pl.fovis.common.NewsBean;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.ObjectInFvsChangeExBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.UserBean;
import pl.fovis.server.contentsExtractor.CompoundContentsExtractor;
import pl.fovis.server.fts.LuceneSimilarNodesWizard;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.commons.MailServerConfig;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;
import simplelib.IContinuation;
import simplelib.JsonValueHelper;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public class BIKSServiceBaseForSingleBIKS extends BIKSServiceBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    private static final String MUST_RUN_STRIP_HTML_TAGS_FROM_RICHTEXT_FIELDS = "mustRunStripHtmlTagsFromRichTextFields";
    private static final String BIK_ARTIFICIAL_ADMIN = "bik_artificial_admin";
    private static final String BIK_ARTIFICIAL_PASSWORD = "123";
    public static final String HEARTBEAT_MONITOR_URL = "heartbeatMonitorUrl";
    public static final String HEARTBEAT_MONITOR_DELAY_S = "heartbeatMonitorDelayS";
    public static final String SEND_EMAIL_NOTIFICATIONS_PROP_NAME = "sendEmailNotifications";
    public static final String LAME_LOGGERS_CONFIG_PROP_NAME = "lameLoggersConfig";
    protected static AtomicInteger nDroolsInstance = new AtomicInteger(0);
    protected static AtomicInteger lastLoggedUserId = new AtomicInteger(-1);
    protected static AtomicInteger lastChangedId = new AtomicInteger(-1);
    protected static AtomicInteger lastCheckedChangeId = new AtomicInteger(-1);

    //protected MailServerConfig mailServerCfg;
    protected MssqlConnectionConfig mssqlConnCfg;
    protected String dirForUpload;
    protected IContinuation doAfterVerticalizeInPumps = new IContinuation() {

        @Override
        public void doIt() {
//            indexChangedNodes();
        }
    };

    @Override
    @PerformOnDefaultDb
    protected void initWithConfig(BIKCenterConfigObj cfg) {

        super.initWithConfig(cfg);

        if (!isMultiXMode()) {
            setSingleBiksDbForCurrentRequest(defaultDatabaseName);
        }

//        mailServerCfg = cfg.mailServerConfig;
        mssqlConnCfg = cfg.connConfig;
        dirForUpload = cfg.dirForUpload;
    }

    protected void indexChangedNodes() {

        if (logger.isDebugEnabled()) {
            logger.debug("index changed nodes");
        }
        if (getBOXIServiceImplDataForCurrentRequest().similarNodesWizard != null) {
            if (logger.isInfoEnabled()) {
                logger.info("index changed nodes will run nodeDataChanged");
            }
            getBOXIServiceImplDataForCurrentRequest().similarNodesWizard.nodeDataChanged();
        }
    }

//    protected ThreadLocal<Map<Integer, Pair<Long, String>>> getOverwrittenMods() {
//        return sharedServiceObj.overwrittenMods;
//    }
//
//    @Override
//    public void requestStarting(IServiceRequestContext ctx) {
//        getOverwrittenMods().set(new HashMap<Integer, Pair<Long, String>>());
//
//        super.requestStarting(ctx);
//    }
//
//    @Override
//    public void requestFinishing() {
//        super.requestFinishing();
//
//        maybeSetModifiedTreesInfoInResponse();
//
//        getOverwrittenMods().remove();
//    }
    @PerformOnSingleBiksDb
    protected String getAppPropValueEx(String propName, boolean allowSensitive) {
        return getAppPropValueEx(propName, allowSensitive, null);
    }

    @PerformOnSingleBiksDb
    protected void loadAppPropsToCacheDirect(BOXIServiceImplData serviceImplData) {
//        //ww: zabezpieczenie, to się może wywołać poza cyklem życia requestu
//        setDefaultDatabaseAsCurrent();
        List<AppPropBean> appProps = createBeansFromNamedQry("getAppProps", AppPropBean.class);
        serviceImplData.appPropsCache = new LinkedHashMap<String, AppPropBean>();
        serviceImplData.nonSensitiveAppPropsCache = new ArrayList<AppPropBean>();

        for (AppPropBean appProp : appProps) {
            serviceImplData.appPropsCache.put(appProp.name, appProp);
            if (appProp.isEditable >= 0) {
                serviceImplData.nonSensitiveAppPropsCache.add(appProp);
            }
        }
    }

    @PerformOnSingleBiksDb
    protected void loadAppPropsToCacheIfNeeded() {
        BOXIServiceImplData serviceImplData = getBOXIServiceImplDataForCurrentRequest();
        if (serviceImplData.appPropsCache != null) {
            return;
        }

        loadAppPropsToCacheDirect(serviceImplData);
    }

    @PerformOnSingleBiksDb
    public String getAppPropValueEx(String propName, boolean allowSensitive,
            String defVal) {
        loadAppPropsToCacheIfNeeded();
        AppPropBean appProp = getBOXIServiceImplDataForCurrentRequest().appPropsCache.get(propName);
        return appProp == null || (!allowSensitive && appProp.isEditable < 0) ? defVal : appProp.val;
//        Map<String, Object> row = execNamedQuerySingleRowCFN("getAppPropValueEx",
//                true, FieldNameConversion.ToLowerCase, propName, allowSensitive);
//        return BaseUtils.nullToDef((String) BaseUtils.safeMapGet(row, "val"), defVal);
    }

    @PerformOnSingleBiksDb
    public String getAppPropValue(String propName) {
        return getAppPropValueEx(propName, false);
    }

    @PerformOnSingleBiksDb
    public String getSensitiveAppPropValue(String propName) {
        return getAppPropValueEx(propName, true);
    }

    protected boolean parseAppPropValueToBoolean(String propName) {
        String val = getSensitiveAppPropValue(propName);
        return BIKCenterUtils.getAppPropValAsBool(val);
    }

    @PerformOnSingleBiksDb
    protected int getStarredUserCount() {
        return BaseUtils.tryParseInteger(getAppPropValue("starredUserCount"), 10);
    }

    @PerformOnSingleBiksDb
    protected ChangedRankingBean getStarredUsers() {
        ChangedRankingBean starredUser = new ChangedRankingBean();
        starredUser.starredSystemUser = new HashSet<Integer>();
        starredUser.starredBikUser = new HashSet<Integer>();
        starredUser.starredUserNode = new HashSet<Integer>();

        List<Map<String, Object>> starredUsers
                = execNamedQueryCFN("getStarredUser", FieldNameConversion.ToLowerCase, getStarredUserCount());
        for (Map<String, Object> row : starredUsers) {
            starredUser.starredSystemUser.add(sqlNumberInRowToInt(row, "system_user_id"));
            starredUser.starredBikUser.add(sqlNumberInRowToInt(row, "user_id"));
            starredUser.starredUserNode.add(sqlNumberInRowToInt(row, "node_id"));
        }
        starredUser.timeStamp = System.currentTimeMillis();
        return starredUser;
    }

    @PerformOnSingleBiksDb
    protected MssqlConnectionConfig getMssqlConnLoadCfg() {
        MssqlConnectionConfig conn = mssqlConnCfg.clone();
        String load_user = getAppPropValue("load_user_login");
        String load_pwd = getAppPropValue("load_user_pwd");
        if (load_user != null && load_pwd != null) {
            conn.user = load_user;
            conn.password = load_pwd;
        }
//        String dbName = getCurrentDatabase().get();
        String dbName = getSingleBiksDbForCurrentRequest();
        conn.database = dbName;
        return conn;
    }

    @PerformOnSingleBiksDb
    public Set<String> getAvailableConnectors() {
        return BaseUtils.splitUniqueBySep(getAppPropValueEx("availableConnectors", true, ""), ",", true);
    }

    @PerformOnSingleBiksDb
    public Pair<Integer, List<SchedulePumpBean>> getSchedule(int pageNum, int pageSize) {
        Set<String> connectors = getAvailableConnectors();
        Integer rowCnt = execNamedQuerySingleValAsInteger("getScheduleCnt", true, null, connectors);
        if (rowCnt == null || rowCnt == 0) {
            return new Pair<Integer, List<SchedulePumpBean>>(0, new ArrayList<SchedulePumpBean>());
        }
        List<SchedulePumpBean> allConnectors = createBeansFromNamedQry("getScheduleMiniBeans", SchedulePumpBean.class, pageNum * pageSize + 1,
                (pageNum + 1) * pageSize, connectors);
//        List<SchedulePumpBean> availableConnectors = new ArrayList<SchedulePumpBean>();
//        for (SchedulePumpBean bean : allConnectors) {
//            String trimmedSource = bean.source.trim();
//            if (connectors.contains(trimmedSource)) {
//                availableConnectors.add(bean);
//            } else {
//                rowCnt--;
//            }
//        }
        return new Pair<Integer, List<SchedulePumpBean>>(rowCnt, allConnectors);
    }

    public List<SchedulePumpBean> getSchedule() {
        return getSchedule(0, 1000).v2;
    }

    @PerformOnSingleBiksDb
    protected void checkDataLoadingStatusAndUpdateIncorrectLogs() {
        // sprawdza zasilania trwajace i konczy je z bledem
        execNamedCommand("checkDataLoadingStatusAndUpdateIncorrectLogs");
    }

    @PerformOnSingleBiksDb
    protected <B> B readAdminBean(String prefix, Class<B> c) {
        List<Map<String, Object>> list = execNamedQueryCFN("getAdminValue", FieldNameConversion.ToLowerCase, prefix);
        Map<String, Object> vals = new HashMap<String, Object>();
        final String prefixWithDot = prefix + ".";
        for (Map<String, Object> map : list) {
            String key = ((String) map.get("param" /* I18N: no */)).replaceFirst(prefixWithDot, "");
            Object val = map.get("value" /* I18N: no */);
            vals.put(key, val);
        }
        return //BeanMaker.createOneBean(vals, c);
                MuppetMaker.newMuppet(c, vals);
    }

    @PerformOnSingleBiksDb
    protected MailServerConfig mailServerConfig() {
        return new MailServerConfig(getSensitiveAppPropValue("emailSmtpHostName"),
                getSensitiveAppPropValue("emailSmtpPort"),
                getSensitiveAppPropValue("emailUser"),
                getSensitiveAppPropValue("emailPassword"),
                getSensitiveAppPropValue("emailFrom"),
                parseAppPropValueToBoolean("emailSsl"));
    }

    @PerformOnSingleBiksDb
    protected ConnectionParametersDQCBean getDQCConnectionParameters() {
        return readAdminBean("dqc" /* I18N: no */, ConnectionParametersDQCBean.class);
    }

    @PerformOnSingleBiksDb
    protected List<String> getConfluenceTreeCodes() {
        return execNamedQuerySingleColAsList("getConfluenceTreeCodes", "code");
    }

    @PerformOnSingleBiksDb
    protected ConnectionParametersConfluenceBean getConfluenceConnectionParameters() {
        ConnectionParametersConfluenceBean bean = readAdminBean("confluence" /* I18N: no */, ConnectionParametersConfluenceBean.class);
        bean.confluenceTreeCodes = getConfluenceTreeCodes();
        return bean;
    }

    @PerformOnSingleBiksDb
    public Pair<List<ConnectionParametersSapBoBean>, ConnectionParametersBIAdminBean> getBIAdminConnectionParameters() {
        ConnectionParametersBIAdminBean bean = getBIAdminConfig();
        List<ConnectionParametersSapBoBean> servers = createBeansFromNamedQry("getBOConnectionParameters", ConnectionParametersSapBoBean.class, PumpConstants.SOURCE_NAME_SAPBO4, null);
        return new Pair<List<ConnectionParametersSapBoBean>, ConnectionParametersBIAdminBean>(servers, bean);

    }

    //ww: można to wywołać tylko z getBOXIServiceImplDataForCurrentSession!
    @PerformOnSingleBiksDb
    protected BOXIServiceImplData createAndSaveNewBOXIServiceImplData(String instanceDBName) {
        BOXIServiceImplData res = new BOXIServiceImplData();
        loadAppPropsToCacheDirect(res);
        sharedServiceObj.instanceDataMap.put(instanceDBName, res);

        res.inactiveTestNodeKindId = execNamedQuerySingleColAsList("getInactiveTestNodeKindIds", null);//sqlNumberToInt(execNamedQuerySingleVal("getNodeKindIdByCode", false, null, BIKConstants.NODE_KIND_DQC_TEST_INACTIVE));
        res.currentUserRanking = getStarredUsers();
        res.hasCrrLabels = this.<Integer>execNamedQuerySingleVal("hasCrrLabels", false, null) != 0;

        if (instanceDBName == null) {
            try {
                throw new RuntimeException("instanceDBName == null!");
            } catch (Exception ex) {
//                System.out.println("err!\n" + StackTraceUtil.getCustomStackTrace(ex));
            }
            return res;
        }

        res.pumpExecutor = new ThreadedPumpExecutor(urlForPublicAccess, dirForUpload, doAfterVerticalizeInPumps);
        res.pumpExecutor.setConnectionConfig(getMssqlConnLoadCfg());//tConnectionConfig(mssqlConnCfg, getConnection(), getAdhocDao());
        res.pumpExecutor.setScheduler(getSchedule());
        res.pumpThread = new Thread(res.pumpExecutor);
        res.pumpThread.start();
        checkDataLoadingStatusAndUpdateIncorrectLogs();

        ConnectionParametersDQCBean dqcPar = getDQCConnectionParameters();
        if (dqcPar.isActive == 1 && dqcPar.checkTestActivity == 1) {
            res.dqcTestRequestsReceiver = new DQCTestRequestsReceiver(new MssqlConnectionConfig(dqcPar.server, dqcPar.instance, dqcPar.database, dqcPar.user, dqcPar.password));
        }
        ConnectionParametersConfluenceBean confluencePar = getConfluenceConnectionParameters();
        if (confluencePar.isActive == 1) {
            ConfluenceConfigBean configBean = new ConfluenceConfigBean(confluencePar.isActive, confluencePar.url, confluencePar.user, confluencePar.password, confluencePar.checkChildrenCount);
            res.confluenceProxyConnector = ConfluenceProxy.makeProxy(new ConfluenceConnector(configBean));
        }
        Pair<List<ConnectionParametersSapBoBean>, ConnectionParametersBIAdminBean> biaParams = getBIAdminConnectionParameters();
        if (biaParams.v2.isActive == 1) {
            res.biaProxyConnector = BIAProxy.makeProxy(new BIAConnector(biaParams.v2.host, biaParams.v2.user, biaParams.v2.password));
            res.biaArchive = new ThreadedBIAdminArchive(getAdhocDao(), getBOXIServiceImplDataForCurrentRequest().biaProxyConnector, dirForUpload);
            res.biaArchiveThread = new Thread(res.biaArchive);
            res.biaArchiveThread.start();
            res.biaArchive.schedule(getBIAdminConfig(), getBIAdminArchiveConfig());
        }

        String sendEMailNotifications = getAppPropValue(SEND_EMAIL_NOTIFICATIONS_PROP_NAME);
        if (sendEMailNotifications != null && (sendEMailNotifications.equals("true") || sendEMailNotifications.equals("1"))) {
            res.mailSender = new ThreadedMailSender(this, mailServerConfig(), urlForPublicAccess, dirForUpload);
            res.mailSenderThread = new Thread(res.mailSender);
            res.mailSenderThread.start();
            res.mailSender.schedule();
        }

        // update lameLoggerConfig from database
        String lameLoggersValue = getAppPropValue(LAME_LOGGERS_CONFIG_PROP_NAME);
        if (!BaseUtils.isStrEmptyOrWhiteSpace(lameLoggersValue)) {
            final Properties p = new Properties();
            try {
                p.load(new StringReader(lameLoggersValue));
                LameUtils.tryReadAndApplyLameLoggersConfig(p);
            } catch (Exception ex) {
                if (logger.isWarnEnabled()) {
                    logger.warn("Cannot replace/reinit logger config! Error: " + ex.getLocalizedMessage());
                }
            }
        }

//        System.out.println("suggestSimilarNodes=" + getAppPropValue("suggestSimilarNodes"));
        int suggestSimilarMaxResults = getSuggestSimilarMaxResults();
        boolean suggestSimilarResults = suggestSimilarMaxResults > 0;
        res.suggestSimilarResults = suggestSimilarResults;

        int hasFulltextCatalog = execNamedQuerySingleValAsInteger("hasFulltextCatalog", false, null);
        res.hasFulltextCatalog = hasFulltextCatalog != 0;
        boolean useLuceneSearch = !res.hasFulltextCatalog || !JsonValueHelper.getBool(getAppPropValue("useFullTextIndex"));
        res.useLuceneSearch = useLuceneSearch;

        if (suggestSimilarResults || useLuceneSearch) {
            res.similarNodesWizard = new LuceneSimilarNodesWizard(BaseUtils.ensureDirSepPostfix(dirForUpload) + "lucene/" + instanceDBName);

            String plainTextExts = getAppPropValue("fileContentsExtractor.plainTextExts");
            String tikaExts = getAppPropValue("fileContentsExtractor.tikaExts");
            long maxFileSize = BaseUtils.tryParseLong(getAppPropValue("fileContentsExtractor.maxFileSize"), 0L);
            float fileContentsBoost = BaseUtils.tryParseDouble(getAppPropValue("fileContentsExtractor.fileContentsBoost"), 0.0).floatValue();
            CompoundContentsExtractor extractor = maxFileSize == 0 ? null
                    : CompoundContentsExtractor.makeExtractorForPlainTextAndTika(BaseUtils.ensureDirSepPostfix(dirForUpload) + "tika-app-1.11.jar", plainTextExts, tikaExts);

            res.similarNodesWizard.setUp(new NodesDataFetcherForLucene(getAdhocDao(), instanceDBName,
                    extractor, dirForUpload, maxFileSize, fileContentsBoost), !BaseUtils.safeEquals(getAppPropValue("indexing.fullData"), "done"));
        }
        return res;
    }

    @PerformOnSingleBiksDb
    public int getSuggestSimilarMaxResults() {
        return BaseUtils.tryParseInt(getAppPropValue("suggestSimilar.maxResults"));
    }

    @PerformOnSingleBiksDb
    protected void clearAppPropsCache() {
        BOXIServiceImplData serviceImplData = getBOXIServiceImplDataForCurrentRequest();
        serviceImplData.appPropsCache = null;
        serviceImplData.nonSensitiveAppPropsCache = null;
    }

    @PerformOnSingleBiksDb
    protected void setAppPropValue(String appPropName, String val) {
        execNamedCommand("setAppPropValue", appPropName, val);
        //clearCaches();
        clearAppPropsCache();
    }

    @PerformOnSingleBiksDb
    protected void getAllDescrFromExternalTables() {
        List<Map<String, Object>> rows = execNamedQueryCFN("getAllDescrFromExternalTables", FieldNameConversion.ToLowerCase);
        for (Map<String, Object> row : rows) {
            int nodeId = sqlNumberInRowToInt(row, "node_id");
            String value = (String) row.get("descr" /* I18N: no */);
            execNamedCommand("updatePlainDescription", stripHtmlTags(value), nodeId);
        }
    }

    @PerformOnSingleBiksDb
    protected void stripHtmlTagsFromRichTextFields() {
        if (logger.isInfoEnabled()) {
            logger.info("stripHtmlTagsFromRichTextFields: " + "" + "start" /* i18n: no */);
        }

        setAppPropValue(MUST_RUN_STRIP_HTML_TAGS_FROM_RICHTEXT_FIELDS, "-1");

//        stripHtmlTagsFromRichTextFieldsOfOneTable("bik_metapedia", "body", "node_id", "descr");
        getAllDescrFromExternalTables();

        setAppPropValue(MUST_RUN_STRIP_HTML_TAGS_FROM_RICHTEXT_FIELDS, "0");

        if (logger.isInfoEnabled()) {
            logger.info("stripHtmlTagsFromRichTextFields: " + "done" /* I18N: no */);
        }
    }

    @PerformOnSingleBiksDb
    synchronized protected BOXIServiceImplData getBOXIServiceImplDataForCurrentRequest() {

//        checkIfBiksDbObjsAccessibleFromCurrentDb();
        //String dbName = getCurrentDatabase().get();
        String dbName = getSingleBiksDbForCurrentRequest();

        if (dbName == null) {
            throw new LameRuntimeException("not running on SingleBiks");
//            ILameLogger.LameLogLevel lvl = isMultiXMode() ? ILameLogger.LameLogLevel.Error : ILameLogger.LameLogLevel.Debug;
//            if (logger.isEnabledAtLevel(lvl)) {
//                logger.logAtLevel(lvl, "getBOXIServiceImplDataForCurrentRequest: dbName is null, default db=" + defaultDatabaseName
//                        + ", isConnectedToMasterBIKS=" + isMultiXMode());
//            }
//            dbName = defaultDatabaseName;
        }

        BOXIServiceImplData res = sharedServiceObj.instanceDataMap.get(dbName);
        if (res == null) {
            res = createAndSaveNewBOXIServiceImplData(dbName);
            if (!isMultiXMode()) {
                //ww: a nie lepiej użyć Timer.scheduleAtFixedRate lub czegoś podobnego
                // do odpalania zadania co określony czas?
                Integer hbMonitorDelayS = BaseUtils.tryParseInteger(getAppPropValue(HEARTBEAT_MONITOR_DELAY_S));
                String hbMonitorUrl = getAppPropValue(HEARTBEAT_MONITOR_URL);
                if (hbMonitorDelayS != null && !BaseUtils.isStrEmptyOrWhiteSpace(hbMonitorUrl)) {
                    TheadedHeartbeatMonitor heartbeat = new TheadedHeartbeatMonitor();
                    heartbeat.setConnectionData(hbMonitorUrl, hbMonitorDelayS);
                    new Thread(heartbeat).start();
                }
                int mustRunStripHtmlTagsFromRichTextFields
                        = BaseUtils.tryParseInt(getAppPropValueEx(MUST_RUN_STRIP_HTML_TAGS_FROM_RICHTEXT_FIELDS, true, "0"));
                if (mustRunStripHtmlTagsFromRichTextFields == 1) {
                    stripHtmlTagsFromRichTextFields();
                }
            }
        }
        return res;
    }

//    public String getLoggedUserName() {
//        SystemUserBean loggedUser = getLoggedUserBean();
//        return loggedUser != null ? loggedUser.loginName : null;//loggedUser.name : null;
//    }
//    @Override
//    public SystemUserBean getLoggedUserBean() {
//        return super.getLoggedUserBean();
////        BOXISessionData sessionData = getSessionData();
////        //ww: to było złe miejsce na wlogowywanie użytkownika
//////        performAuthenticatedUserLogin(sessionData);
////        return sessionData.loggedUser;
//    }
    @PerformOnSingleBiksDb
    public SystemUserBean getLoggedUserBean() {
        BOXISessionData sessionData = getSessionData();
        if (sessionData == null) {
            return null;
        }
//        if (sessionData.loggedUsers == null) {
//            return null;
//        }
        //ww: to było złe miejsce na wlogowywanie użytkownika
//        performAuthenticatedUserLogin(sessionData);

//        if (isMultiXMode) {
//            return sessionData.loggedUsers.get(getCurrentDatabase().get());
//        } else {
//            return sessionData.loggedUsers.get(defaultDatabaseName);
//        }
//        String dbName = isMultiXMode ? getCurrentDatabase().get() : defaultDatabaseName;
        String dbName = getSingleBiksDbForCurrentRequest();
//        Pair<SystemUserBean, Long> p = sessionData.loggedUsers.get(dbName);
        Pair<SystemUserBean, Long> p = sessionData.getSystemUserBeanAndTimeStampIfLoggedOnDb(dbName);

        if (p == null) {
            return null;
        }

        return fixIfLoggedUserDataInvalidated(p.v1, p.v2);
    }

//    protected SystemUserBean getLoggedUserBean() {
//        BOXISessionData sessionData = getSessionData();
//        //ww: to było złe miejsce na wlogowywanie użytkownika
////        performAuthenticatedUserLogin(sessionData);
//        return sessionData.loggedUser;
//    }
//    protected Integer addBikSystemUserInternal(boolean performSecurityChecks, String loginName, String password,
//            boolean isDisabled, Integer userId, Set<String> userRightRoleCodes, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries) throws ValidationExceptionBiks {
//        //return addBikSystemUserInternal(performSecurityChecks, loginName, password, isDisabled, userId, userRightRoleCodes, customRightRoleUserEntries, getCurrentDatabase().get());
//        return addBikSystemUserInternal(performSecurityChecks, loginName, password, isDisabled, userId, userRightRoleCodes, customRightRoleUserEntries, getSingleBiksDbForCurrentRequest());
//    }
//    protected void updateBikSystemUserInternal(boolean performSecurityChecks, String loginName, String password, Set<String> userRightRoleCodes) throws ValidationExceptionBiks {
//
//        if (performSecurityChecks) {
//            checkRightsToAddOrEditsSysUsers(null);
//        }
//
//        if (performSecurityChecks) {
//            fixSysAdminRightRoleOnAddOrEdit(userRightRoleCodes);
//        }
//        execNamedCommand("updateBikSystemUserEx", loginName, password);
//    }
    @PerformOnSingleBiksDb
    protected void getRightRolesForOneSystemUser(SystemUserBean user) {
        if (user != null) {
            final int userId = user.id;
            user.userRightRoleCodes = getRightRolesForSystemUserByUserId(userId);
            user.userAuthorTreeIDs = execNamedQuerySingleColAsSet("getAuthorTrees", "tree_id", userId, true);
            user.userCreatorTreeIDs = execNamedQuerySingleColAsSet("getCreatorTrees", "tree_id", userId, true);
            user.nonPublicUserTreeIDs = execNamedQuerySingleColAsSet("getNonPublicUserTrees", "tree_id", userId);
            user.userAuthorBranchIds = getAuthorBranchIds(userId);
            user.userCreatorBranchIds = getCreatorBranchIds(userId);
            user.nonPublicUserBranchIds = getNonPublicUserBranchIds(userId);
            user.userAuthorAllTreeIDs = execNamedQuerySingleColAsSet("getAuthorTrees", "tree_id", userId, false);
            user.userCreatorAllTreeIDs = execNamedQuerySingleColAsSet("getCreatorTrees", "tree_id", userId, false);
        }
    }
    
    @PerformOnSingleBiksDb
    protected Set<String> getRightRolesForSystemUserByUserId(int userId) {
        Set<String> res = execNamedQuerySingleColAsSet("getRightRolesForSystemUser", "code" /* I18N: no */, userId);
        //ściągamy kody uprawnień które bieżący użytkownik dostał od grup
        Set<String> customInGroupRightRole = execNamedQuerySingleColAsSet("getCurrentUserCustomRightRoleInGroup", "code" /* I18N: no */, userId);
        Set<String> customRightRole = execNamedQuerySingleColAsSet("getExpertCustomRightRolesForSystemUser", "code" /* I18N: no */, userId);
        res.addAll(customRightRole);
        res.addAll(customInGroupRightRole);
        return res;
    }

    @PerformOnSingleBiksDb
    protected List<UserBean> getUsersToSendEmail() {
        return createBeansFromNamedQry("getUserToSendEmail", UserBean.class);
    }

    @PerformOnSingleBiksDb
    protected List<FvsChangeExBean> getSuggestedElements(int userId) {
        return createBeansFromNamedQry("getSuggestedElements", FvsChangeExBean.class, userId);
    }

    @PerformOnSingleBiksDb
    protected List<FvsChangeExBean> getChangedFavouritesExForUser(int id) {
        return createBeansFromNamedQry("getChangedFavouritesExForUser", FvsChangeExBean.class, id);
    }

    @PerformOnSingleBiksDb
    protected List<NewsBean> getNotReadNewsByUserInner(int id) {
        return createBeansFromNamedQry("getNotReadNewsByUserInner", NewsBean.class, id);
    }

    @PerformOnSingleBiksDb
    protected Pair<String, Map<Integer, Pair<String, String>>> extractInnerLinks(String body, boolean isPublicUrl) {
        Set<Integer> links = BIKCenterUtils.extractNodeIdsFromHtml(body);
        Map<Integer, Pair<String, String>> innerLinks = new LinkedHashMap<Integer, Pair<String, String>>();
        //Set<Integer> deletedNodeIds = new HashSet<Integer>();
        Map<Integer, String> nodeIds = new LinkedHashMap<Integer, String>();
        if (!links.isEmpty()) {
            List<NodeKindBean> nkbs = createBeansFromNamedQry("getInnerLinks", NodeKindBean.class, links);
            for (NodeKindBean nkb : nkbs) {
                int nodeId = nkb.id;
                if (nkb.isDeleted == 0) {
                    innerLinks.put(nodeId, new Pair<String, String>(nkb.caption, nkb.code));
                } else {
                    //deletedNodeIds.add(nodeId);
                }
                nodeIds.put(nodeId, nkb.isDeleted == 0 ? nkb.code : null);
            }
            //body = BIKCenterUtils.removeHrefsFromHtml(body, deletedNodeIds);
            body = BIKCenterUtils.removeOrFixHrefsFromHtml(body, nodeIds, isPublicUrl ? urlForPublicAccess : "");
        }
        return new Pair<String, Map<Integer, Pair<String, String>>>(body, innerLinks);
    }

    @PerformOnSingleBiksDb
    public List<ObjectInFvsChangeExBean> getChangedDescendantsObjectInFvsEx(int nodeId, int userId) {

        List<ObjectInFvsChangeExBean> changedDescendantsObjectInFvsEx = createBeansFromNamedQry("getChangedDescendantsObjectInFvsEx", ObjectInFvsChangeExBean.class, nodeId, userId);
        for (ObjectInFvsChangeExBean oin : changedDescendantsObjectInFvsEx) {
            if (!BaseUtils.isStrEmpty(oin.changedAttrs)) {
                List<String> a = execNamedQuerySingleColAsList("getChangedAttrsNameInFvs", null, oin.changedAttrs);
                oin.changedAttrs = getTranslateAttr(a);
//                oin.changedAttrs = BaseUtils.mergeWithSepEx(a, ", ");
            }
        }
        return changedDescendantsObjectInFvsEx;
    }

    public String getTranslateAttr(List<String> attrNames) {
        final int attrNameCnt = BaseUtils.collectionSizeFix(attrNames);

        List<String> translatedAttrNames = new ArrayList<String>(attrNameCnt);
        if (attrNameCnt > 0) {
            for (String attrName : attrNames) {
                Map<String, String> translatedAttrNames3 = getTranslationsGeneric(BIKConstants.TRANSLATION_KIND_ADEF).get(BIKConstants.TRANSLATION_KIND_ADEF);
                translatedAttrNames.add(BaseUtils.nullToDef(translatedAttrNames3.get(attrName), attrName));
            }
        }

        String longList = BaseUtils.mergeWithSepEx(translatedAttrNames, ", ");
        return longList;

    }

    //ww: wynikiem jest mapa z kindów na mapy tłumaczeń
    protected Map<String, Map<String, String>> getTranslationsGeneric(String... kinds) {
        //ww: bez tego pyszczy NetBeans - wołanie z varargs podwójne by było - a tak - to ma być parametr pojedyńczy!
        Object kindsInner = kinds;
        List<Map<String, Object>> rows = execNamedQueryCFN("getTranslationsGeneric",
                FieldNameConversion.ToJavaPropName, kindsInner);

        Map<String, Map<String, String>> res
                = //new HashMap<String, Map<String, String>>();
                BaseUtils.makeHashMapOfSize(kinds.length);

        //ww: zrobimy puste mapy wewnętrzne, żeby potem nie było zakoczenia nullem
        // gdy brak wartości dla kindu
        for (String kind : kinds) {
            Map<String, String> mapOfKind = new HashMap<String, String>();
            res.put(kind, mapOfKind);
        }

        for (Map<String, Object> row : rows) {
            String kind = (String) row.get("kind" /* i18n: no */);
            String code = (String) row.get("code" /* i18n: no */);
            String txt = (String) row.get("txt" /* i18n: no */);

            //ww: taka mapa jest, bo ją powyżej zakładamy w oddzielnej pętli
            res.get(kind).put(code, txt);
        }

        return res;
    }

//    protected void maybePerformSSOAuthenticatedUserSilentLogin() {
//        BOXISessionData sessionData = getSessionData();
//        final String loginName = sessionData.remoteUser;
//
//        if (!setupLoginAttempt(loginName)) {
//            return;
//        }
//        SystemUserBean user = getOrCreateSystemUserBeanByLogin(loginName);
//
////        if (getSessionData().loggedUsers == null) {
////            getSessionData().loggedUsers = new HashMap<String, SystemUserBean>();
////        }
//        if (!user.isDisabled) {
//            //znaleziony uzytkownik nie jest usunięty
////            systemUsersSubservice.setLoggedUserBeanInSessionData(user);
//            setLoggedUserBeanInSessionData(user);
////
////                sessionData.loggedUser = user;
////                if (user.id != 0) {
////                    insertBIKStatisticsOneTimeOnDay("userLogin", null);
////                }
//        } else {
//            //ww: skoro użytkownik jest wyłączony, to więcej nie będzie
//            // próbował na niego wchodzić po SSO
//            sessionData.ignoreRemoteUser = true;
//            sessionData.userExistButDisabled = true;
//        }
//    }
    //ww: wynik:
    // false - nie da rady, nie może się zalogować ten login
    // true - w zależności od trybu MultiX:
    //     w trybie SingleBIKS: sprawdź hasło w swojej bazie
    //     w trybie MultiX: jest szansa, znamy bazę dla niego, przełączone na tą bazę, sprawdź hasło
    // w trybie SingleBIKS wynik jest zawsze true, bo nic tu nie trzeba robić
    // wystarczy klasycznie sprawdzić hasło w tabelki bik_system_user (albo założyć domenowego)
//    protected boolean setupLoginAttempt(final String login) {
//
//        if (!isMultiXMode()) {
//            return true;
//        }
//
//        //String databaseForLogin = getDefaultDbNameForMultiXLogin(login);
//        String databaseForLogin = performOnMasterBiks(new IContinuationWithReturn<String>() {
//
//            @Override
//            public String doIt() {
//                return multiXLoginsSubservice.getDefaultDbNameForMultiXLogin(login);
//            }
//        });
//
//        //ww->multix/sso: zakomentowałem, chyba było błędnie
////        if (databaseForLogin == null) {
////            databaseForLogin = defaultDatabaseName;
////        }
//        //getSessionData().xInstanceDbName = databaseForLogin;
//        if (databaseForLogin != null) {
//            setCurrentDbForAdhoc(databaseForLogin);
////            getCurrentDatabase().set(databaseForLogin);
//            setSingleBiksDbForCurrentRequest(databaseForLogin);
//        }
//
//        boolean res = isDefaultDbASingeBiksInstance || databaseForLogin != null;
//
//        if (logger.isDebugEnabled()) {
//            dumpCurrentSessionMultiXInfo("performLoginAttempt: db=" + databaseForLogin + ", result=" + res);
//        }
//
//        return res;
//    }
//    protected boolean needsSSOAuthentication() {
//        BOXISessionData sessionData = getSessionData();
//        boolean res = sessionData != null && !isUserLoggedIn() && !sessionData.ignoreRemoteUser
//                && !BaseUtils.isStrEmptyOrWhiteSpace(sessionData.remoteUser);
//
//        if (logger.isDebugEnabled()) {
//            dumpCurrentSessionMultiXInfo("needsSSOAuthentication: " + res);
//        }
//
//        return res;
//    }
    @PerformOnSingleBiksDb
    public void setLoggedUserBeanInSessionData(SystemUserBean user, String dbName) {

        final BOXISessionData sessionData = getSessionData();

//        if (sessionData.loggedUsers == null) {
//            sessionData.loggedUsers = new HashMap<String, Pair<SystemUserBean, Long>>();
//        }
        Pair<SystemUserBean, Long> userPair = new Pair<SystemUserBean, Long>(user, System.currentTimeMillis());

        if (!isMultiXMode()) {
            //-- default key for normal BIKS
//            sessionData.loggedUsers.put(defaultDatabaseName, userPair);
            sessionData.setUserLoggedOnDb(defaultDatabaseName, userPair);
            if (sessionData.lisaTeradataLogins == null) {
                sessionData.lisaTeradataLogins = new HashMap<String, String>();
            }
        } else {
//            sessionData.loggedUsers.put(dbName, userPair);
            sessionData.setUserLoggedOnDb(dbName, userPair);
            addUserNameToLoggedUsers(user.loginName);
//            insertBIKStatisticsOneTimeOnDay("userLogin", null, user.id);
//            insertBIKStatisticsOneTimeOnly("userLoginOnce", null, user.id);
        }
        // tf: teraz statystyki nie tylko dla MULTIX
        insertBIKStatisticsOneTimeOnDay("userLogin", null, user.id);
        insertBIKStatisticsOneTimeOnly("userLoginOnce", null, user.id);
//        execNamedCommand("addOrUpdateSpidHistoryForUser", user.id);
    }

    @PerformOnSingleBiksDb
    protected void addUserNameToLoggedUsers(String loggedUserName) {
        if (loggedUserName != null) {
//            execNamedCommand("addOrUpdateSpidHistoryForUser", getLoggedUserBean().id);

            getBOXIServiceImplDataForCurrentRequest().loggedUsers.put(loggedUserName, new Date());

            Map<String, Date> m = getBOXIServiceImplDataForCurrentRequest().loggedUserAppInstances.get(loggedUserName);
            if (m == null) {
                m = new ConcurrentHashMap<String, Date>();
                getBOXIServiceImplDataForCurrentRequest().loggedUserAppInstances.put(loggedUserName, m);
            }
            m.put(getFoxyAppInstanceId(), new Date());
        }
    }

    @PerformOnSingleBiksDb
    private void insertBIKStatisticsOneTimeOnDay(String counterName, String parametr, Integer userId) {
        execNamedCommand("insertBIKStatisticsOneTimeOnDay", counterName, userId, parametr);
    }

    @PerformOnSingleBiksDb
    public void insertBIKStatisticsOneTimeOnly(String counterName, String parametr, Integer userId) {
        execNamedCommand("insertBIKStatisticsOneTimeOnly", counterName, userId, parametr);
    }

    @PerformOnSingleBiksDb
    public String getAvatarFieldForAd() {
        ConnectionParametersADBean adParam = getADConnectionParameters();
        if (adParam.isActive == 1 && !adParam.avatarField.equals("")) {
            return adParam.avatarField;
        }
        return null;
    }

    @PerformOnSingleBiksDb
    public void setLoggedUserBeanInSessionData(SystemUserBean user) {
        setLoggedUserBeanForCurrentDbInSessionData(user);
    }

    //ww: usunąłem parametry String queryName i String pwd, zamiast tego jest druga metoda
    // bikSystemUserByLoginAndPassword
    @PerformOnSingleBiksDb
    public SystemUserBean bikSystemUserByLogin(String login, String avatarField) {
        SystemUserBean sub;
        //ww: wydaje mi się, że te sprawdzenie jest bez sensu i zbędne
        // zakomentowałem if-a
        //if (!BaseUtils.isStrEmptyOrWhiteSpace(queryName)) {
        //ww: taki warunek jest błędny!
        // BaseUtils.isStrEmptyOrWhiteSpace(pwd)
        // bo gdy ktoś wpisze puste hasło w oknie do logowania to woła się zła metoda
        // i wali błędem!
        sub = createBeanFromNamedQry("bikSystemUserByLogin", SystemUserBean.class, true, login, avatarField);
        getRightRolesForOneSystemUser(sub);

//        System.out.println("bikSystemUserByLogin: login=" + login + ", bean=" + sub);
        return sub;
        //} else {
        //    return null;
        //}
    }

    @PerformOnSingleBiksDb
    protected void setLoggedUserBeanForCurrentDbInSessionData(SystemUserBean user) {
        //setLoggedUserBeanInSessionData(user, getCurrentDatabase().get());
        setLoggedUserBeanInSessionData(user, getSingleBiksDbForCurrentRequest());
    }

    @PerformOnSingleBiksDb
    public ConnectionParametersADBean getADConnectionParameters() {
        return readAdminBean("ad" /* i18n: no */, ConnectionParametersADBean.class);
    }

    @PerformOnSingleBiksDb
    public BIArchiveConfigBean getBIAdminArchiveConfig() {
        return readAdminBean("biadmin.archive", BIArchiveConfigBean.class);
    }

    @PerformOnSingleBiksDb
    public DroolsConfigBean getDroolsConfig() {
        return readAdminBean("drools", DroolsConfigBean.class);
    }

    @PerformOnSingleBiksDb
    public ConnectionParametersBIAdminBean getBIAdminConfig() {
        return readAdminBean("biadmin", ConnectionParametersBIAdminBean.class);
    }

    @PerformOnSingleBiksDb
    public void createSocialUserFromADData(String loginName, String optSsoDomain, int systemUserId) {
        createSocialUserAndLink(loginName, true, optSsoDomain, systemUserId);
    }

    @PerformOnSingleBiksDb
    protected void createSocialUserAndLink(String loginName, boolean addSocialUserAndLink, String optSsoDomain, int systemUserId) {
        if (addSocialUserAndLink && !BaseUtils.isStrEmptyOrWhiteSpace(loginName)) {
            getAdhocDao().execNamedCommand("createSocialUserAndLink", loginName, optSsoDomain, systemUserId);
        }
    }

    protected INamedSqlsDAO<Object> getMetadataPumpAdhocDao() {
        //getConnection().finalizeTran(true);
        getAdhocDao().finishWorkUnit(true);
        return getPumpExecutor().getPumpAdhocDao();
    }

    @PerformOnSingleBiksDb
    protected ThreadedPumpExecutor getPumpExecutor() {
        return getBOXIServiceImplDataForCurrentRequest().pumpExecutor;
    }

    @PerformOnSingleBiksDb
    protected ThreadedMailSender getMailSender() {
        return getBOXIServiceImplDataForCurrentRequest().mailSender;
    }

    @PerformOnSingleBiksDb
    protected SystemUserBean loadUserDataAndStoreInSession(String loginName) {
        String avatarField = getAvatarFieldForAd();
        SystemUserBean user;
        user = bikSystemUserByLogin(loginName, avatarField);

        if (user != null) {
            setLoggedUserBeanForCurrentDbInSessionData(user);
        }
        return user;
    }

    @PerformOnSingleBiksDb
    protected SystemUserBean fixIfLoggedUserDataInvalidated(SystemUserBean user, Long aquiredTimeMillis) {

        if (user == null) {
            return null;
        }

        BOXIServiceImplData implData = getBOXIServiceImplDataForCurrentRequest();

        Long timeMillis = implData.userDataInvalidatedTimeMillis.get(user.loginName);

        if (timeMillis != null && (aquiredTimeMillis == null || aquiredTimeMillis <= timeMillis)) {
            user = loadUserDataAndStoreInSession(user.loginName);
        }

        return user;
    }

    @Override
    protected void optCustomizeServiceMethodCallDefaultRequiredContext(Map<String, Boolean> drc) {
        drc.put("onSingle", true);
    }

    @PerformOnSingleBiksDb
    public ConnectionParametersAuditBean getBIAuditDatabaseConnectionParameters() {
        return readAdminBean("biadmin.audit", ConnectionParametersAuditBean.class);
    }

//    public void addOrUpdateSpidHistoryForCurrentUser() {
//        SystemUserBean loggedUserBean = getLoggedUserBean();
//        if (loggedUserBean != null) {
//            execNamedCommand("addOrUpdateSpidHistoryForUser", getLoggedUserBean().id);
//        }
//    }
    protected void deleteTmpFile(String serverFileName) {
        File file = new File(dirForUpload, serverFileName);
        if (file.exists()) {
            file.delete();
        }
    }

    public static Integer getNDroolsInstance() {
        return nDroolsInstance.get();
    }

    public static int decrementAndGetNDroolsInstance() {
        return nDroolsInstance.decrementAndGet();
    }

    public static int incrementAndGetNDroolsInstance() {
        return nDroolsInstance.incrementAndGet();
    }

    public void setLastLoggecUserId(Integer loggedUserId) {
        lastLoggedUserId.set(loggedUserId);
    }

    public Integer getLastLoggecUserId() {
        return lastLoggedUserId.get();
    }

    public Integer getArtificialAdminId() {
        return execNamedQuerySingleValAsInteger("getArtificialAdminId", true, null);
    }

    @Override
    protected BOXISessionData getSessionData() {
        BOXISessionData sd = super.getSessionData();
        if (sd == null && getNDroolsInstance() > 0) {
            sd = new BOXISessionData();
        }
        return sd;
    }

    private Set<String> getAuthorBranchIds(Integer userId) {
        Set<String> autBranchesOld = execNamedQuerySingleColAsSet("userAuthorBranchIds", "branch_ids", userId);
        Set<String> autBranchesNewRghtSys = execNamedQuerySingleColAsSet("userAuthorBranchNewRightSys", "branch_ids", userId);
        autBranchesOld.addAll(autBranchesNewRghtSys);
        return autBranchesOld;
    }

    private Set<String> getCreatorBranchIds(Integer userId) {
        Set<String> crtBranchesOld = execNamedQuerySingleColAsSet("userCreatorBranchIds", "branch_ids", userId);
        Set<String> crtBranchesNewRghtSys = execNamedQuerySingleColAsSet("userCreatorBranchIdsNewRightSys", "branch_ids", userId);
        crtBranchesOld.addAll(crtBranchesNewRghtSys);
        return crtBranchesOld;
    }

    private Set<String> getNonPublicUserBranchIds(Integer userId) {
        return execNamedQuerySingleColAsSet("nonPublicUserBranchIds", "branch_ids", userId);
    }

    public void addMailLogMsg(String message) {
        execNamedCommand("addMailLogMsg", message);
    }
}
