/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.TableCellElement;
import com.google.gwt.dom.client.TableRowElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IPredicate;
import simplelib.Pair;

/**
 *
 * @author wezyr
 *
 * Przydatne metody, ale nie dotyczące widgetów (niewizualne)
 */
public class GWTUtils {

    static {
        InitFactories4GWT.initFactories();
    }

    public static void runDeferred(final IContinuation cont) {
        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
            @Override
            public void execute() {
                cont.doIt();
            }
        });
    }

    public static IContinuation wrapAsDeferred(final IContinuation cont) {
        return new IContinuation() {
            @Override
            public void doIt() {
                runDeferred(cont);
            }
        };
    }

    //ww: zwraca true gdy był potrzebny dwupoziomowy header
    public static boolean setupTwoLevelTableHeader(FlexTable ht, int initRow, int initCol,
            Collection<String> colCaptions, String levelSep) {

        boolean hasTwoLevels = false;

        for (String colCaption : colCaptions) {
            if (colCaption == null) {
                continue;
            }
            if (colCaption.contains(levelSep)) {
                hasTwoLevels = true;
                break;
            }
        }

        String prevGroupingCaption = null;
        int colNum = 0;
        int sameGrpCnt = 0;
        int grpNo = 0;

        for (String colCaption : colCaptions) {
            if (colCaption == null) {
                continue;
            }

            final int currColIdx = initCol + colNum;
            if (hasTwoLevels) {
                Pair<String, String> p = BaseUtils.splitByLastNthSep(colCaption, levelSep, 1);
                String currGroupingCaption = p.v1;
                String properCaption = p.v2;

                if (BaseUtils.safeEquals(currGroupingCaption, prevGroupingCaption)) {
                    sameGrpCnt++;
                } else {
                    int headerColIdx = initCol + grpNo;
                    if (sameGrpCnt > 0) {
                        ht.setText(initRow, headerColIdx, prevGroupingCaption);
                        if (sameGrpCnt > 1) {
                            ht.getFlexCellFormatter().setColSpan(initRow, headerColIdx, sameGrpCnt);
                        }
                        grpNo++;
                    }
                    prevGroupingCaption = currGroupingCaption;
                    sameGrpCnt = 1;
                }
                ht.setText(initRow + 1, currColIdx, properCaption);
            } else {
                ht.setText(initRow, currColIdx, colCaption);
            }

            colNum++;
        }

        if (hasTwoLevels) {
            int headerColIdx = initCol + grpNo;
            ht.setText(initRow, headerColIdx, prevGroupingCaption);
            if (sameGrpCnt > 1) {
                ht.getFlexCellFormatter().setColSpan(initRow, headerColIdx, sameGrpCnt);
            }
        }

        return hasTwoLevels;
    }

    @SuppressWarnings("deprecation")
    public static Element getElementById(String eleId) {
        Element elem = (com.google.gwt.user.client.Element) Document.get().getElementById(eleId);
        return elem;
    }

    @SuppressWarnings("deprecation")
    public static void setStyleAttribute(Widget w, String attrName, String attrVal) {
        DOM.setStyleAttribute(w.getElement(), attrName, attrVal);
    }

    public static Focusable findFirstFocusableWidget(Widget start, IPredicate<Widget> optCandidatePredicate) {
        if (start == null) {
            return null;
        }
        if (start instanceof Focusable && (optCandidatePredicate == null || optCandidatePredicate.project(start))) {
            return (Focusable) start;
        }

        if (start instanceof HasWidgets) {

            int bestTabIndex = 0;
            Focusable bestFocusable = null;

            HasWidgets hw = (HasWidgets) start;
            for (Widget w : hw) {
                Focusable f = findFirstFocusableWidget(w, optCandidatePredicate);
                if (f != null) {
                    int candidateTabIndex = f.getTabIndex();
                    if (bestFocusable == null || bestTabIndex > candidateTabIndex) {
                        bestFocusable = f;
                        bestTabIndex = candidateTabIndex;
                    }
                }
            }

            return bestFocusable;
        }

        return null;
    }

    public static void tryFocusFirstWidget(Widget start, IPredicate<Widget> optCandidatePredicate) {
        Focusable f = findFirstFocusableWidget(start, optCandidatePredicate);
        if (f != null) {
            f.setFocus(true);
        }
    }

    public static void addOptWidget(HasWidgets container, Widget w) {
        if (w != null) {
            container.add(w);
        }
    }

    public static void addWidgets(HasWidgets container, Widget... widgets) {
        for (Widget w : widgets) {
            addOptWidget(container, w);
        }
    }

    public static void addEnterKeyHandler(Widget w, final IContinuation onEnter) {
        w.addDomHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                NativeEvent ne = event.getNativeEvent();
                if (ne.getKeyCode() == KeyCodes.KEY_ENTER) {
                    onEnter.doIt();
                }
            }
        }, KeyUpEvent.getType());
    }

    public static void addOnClickHandler(Widget w, ClickHandler handler) {
        w.addDomHandler(handler, ClickEvent.getType());
    }

    public static void addOnClickHandler(Widget w, final IContinuation onClick) {
        addOnClickHandler(w, new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                onClick.doIt();
            }
        });
    }

    public static void addOnHoverHandlers(Widget w, MouseOverHandler mouseOverHandler,
            MouseOutHandler mouseOutHandler) {
        w.addDomHandler(mouseOverHandler, MouseOverEvent.getType());
        w.addDomHandler(mouseOutHandler, MouseOutEvent.getType());
    }

    public static void setHTMLTableCellVal(HTMLTable t, int rowIdx, int colIdx, Object val) {
        if (val instanceof IsWidget) {
            t.setWidget(rowIdx, colIdx, (IsWidget) val);
        } else if (val instanceof SafeHtml) {
            t.setHTML(rowIdx, colIdx, (SafeHtml) val);
        } else {
            t.setText(rowIdx, colIdx, BaseUtils.safeToString(val));
        }
    }

    public static Element findAncestorElementByTagName(Node n, String name) {
        return findAncestorElementByTagName(n, name, null);
    }

    public static Element findAncestorElementByTagName(Node n, String name, String optStopName) {
        while (n != null) {
            if (name.equalsIgnoreCase(n.getNodeName())) {
                return Element.as(n);
            }

            if (optStopName != null && name.equalsIgnoreCase(optStopName)) {
                return null;
            }

            n = n.getParentElement();
        }
        return null;
    }

    public static Pair<Integer, Integer> getRowAndColIndicesOfTableCell(TableCellElement tce) {
        if (tce == null) {
            return null;
        }

        Element rowEle = findAncestorElementByTagName(tce, "tr" /* I18N: no */, "table" /* I18N: no */);
        if (rowEle == null) {
            return null;
        }

        TableRowElement tre = TableRowElement.as(rowEle);

        return new Pair<Integer, Integer>(tre.getRowIndex(), tce.getCellIndex());
    }

    public static Pair<Integer, Integer> getRowAndColIndicesOfNodeInsideTableCell(Node n) {
        if (n == null) {
            return null;
        }

        Element cellEle = findAncestorElementByTagName(n, "td" /* I18N: no */, "table" /* I18N: no */);
        if (cellEle == null) {
            return null;
        }
        TableCellElement tce = TableCellElement.as(cellEle);

        return getRowAndColIndicesOfTableCell(tce);
    }

    public static void setOpacity(Element ele, int perc) {
        Style s = ele.getStyle();
        s.setProperty("mozOpacity", BaseUtils.doubleToString(perc / 100.0, 2));
        s.setProperty("opacity" /* I18N: no */, BaseUtils.doubleToString(perc / 100.0, 2));
        s.setProperty("filter" /* I18N: no */, "alpha(opacity" /* I18N: no */ + "=" + perc + ")");
    }

    @SuppressWarnings("deprecation")
    public static void setElementAttribute(Element elem, String attrName, String val) {
        DOM.setElementAttribute((com.google.gwt.user.client.Element) elem.cast(), attrName, val);
    }

    public static void setElementAttribute(UIObject widget, String attrName, String val) {
        setElementAttribute(widget.getElement(), attrName, val);
    }

    @SuppressWarnings("deprecation")
    public static void setStyleAttribute(Element elem, String attr, String value) {
        DOM.setStyleAttribute((com.google.gwt.user.client.Element) elem.cast(), attr, value);
    }

    public static void setStyleAttribute(UIObject widget, String attr, String value) {
        setStyleAttribute(widget.getElement(), attr, value);
    }

    @SuppressWarnings("deprecation")
    public static String getElementProperty(Element elem, String prop) {
        return DOM.getElementProperty((com.google.gwt.user.client.Element) elem.cast(), prop);
    }

    @SuppressWarnings("deprecation")
    public static void setElementPropertyInt(Element elem, String prop, int value) {
        DOM.setElementPropertyInt((com.google.gwt.user.client.Element) elem.cast(), prop, value);
    }

    @SuppressWarnings("deprecation")
    public static void removeElementAttribute(Element elem, String attr) {
        DOM.removeElementAttribute((com.google.gwt.user.client.Element) elem.cast(), attr);
    }

    @SuppressWarnings("deprecation")
    public static void setElementPropertyBoolean(Element elem, String prop,
            boolean value) {
        DOM.setElementPropertyBoolean((com.google.gwt.user.client.Element) elem.cast(), prop, value);
    }

    public static void addOrRemoveStyleName(UIObject widget, String styleName, boolean doAdd) {
        if (doAdd) {
            widget.addStyleName(styleName);
        } else {
            widget.removeStyleName(styleName);
        }
    }

    private static Boolean isBrowserIE = null;

    public static boolean isBrowserIE() {

        if (isBrowserIE == null) {
            String ua = Window.Navigator.getUserAgent();
            if (ua == null) {
                isBrowserIE = false;
            } else {
                isBrowserIE = ua.toLowerCase().contains("msie");
            }
        }

        return isBrowserIE;
    }

    public static void executeAfter(final IContinuation whatToDo, int delayMillis) {
        new Timer() {

            @Override
            public void run() {
                whatToDo.doIt();
            }
        }.schedule(delayMillis);
    }

    public static Widget ensureIsWidgetWithTitle(Object obj, String title) {
        Widget res;

        if (obj instanceof IsWidget) {
            res = ((IsWidget) obj).asWidget();
        } else {
            res = new Label(BaseUtils.safeToString(obj));
        }

        if (title != null) {
            res.setTitle(title);
        }

        return res;
    }
}
