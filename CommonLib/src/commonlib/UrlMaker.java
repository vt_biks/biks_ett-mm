/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class UrlMaker {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected IRequestContext reqCtx;
    //protected String urlBase;
    //protected Map<String, Object> defaults;
    protected Iterable<String> paramsOrder;
    protected Set<String> defParamNames;

    public UrlMaker(IRequestContext reqCtx) {
        this.reqCtx = reqCtx;
    }

    public UrlMaker(IRequestContext reqCtx, //Map<String, Object> defaults,
            Iterable<String> paramsOrder,
            Set<String> defaultValNames) {
        this(reqCtx);
        init(//defaults,
                paramsOrder, defaultValNames);
    }

    public void init(//String urlBase,
            //Map<String, Object> defaults,
            Iterable<String> paramsOrder,
            Set<String> defaultValNames) {
        //this.urlBase = urlBase;
        //this.defaults = defaults;
        this.paramsOrder = paramsOrder;
        this.defParamNames = defaultValNames;
    }

    protected String postProcessUrlAsString(String url) {
        return url;
    }

    protected Iterable<String> getParamsOrder(Map<String, Object> newParams, Map<String, Object> fixing) {
        return paramsOrder;
    }

    protected Iterable<String> getLastParamsOrder(Map<String, Object> newParams, Map<String, Object> fixing) {
        return null;
    }

    protected Iterable<String> getDefaultParamNames(Map<String, Object> newParams,
            Map<String, Object> fixing) {
        return defParamNames;
    }

    protected Object getValWithFixing(Map<String, Object> newParams, Map<String, Object> fixing,
            String paramName) {
        if (fixing != null && fixing.containsKey(paramName)) {
            return fixing.get(paramName);
        }

        if (newParams != null) {
            return newParams.get(paramName);
        }

        return null;
    }

    protected void putAllNew(Map<String, Object> proper, Map<String, Object> src) {
        if (src == null) {
            return;
        }

        for (Entry<String, Object> p : src.entrySet()) {
            String k = p.getKey();
            if (!proper.containsKey(k)) {
                proper.put(k, p.getValue());
            }
        }
    }

    // uwaga: newParams i fixing mog� by� null!
    protected String innerMakeUrl(boolean forRemoteURL,
            Map<String, Object> newParams, Map<String, Object> fixing) {
        fixing = calcParamFixing(newParams, fixing);

        Iterable<String> po = getParamsOrder(newParams, fixing);

        Map<String, Object> proper = new LinkedHashMap<String, Object>();
        if (po != null) {
            for (String pn : po) {
                proper.put(pn, getValWithFixing(newParams, fixing, pn));
            }
        }

        putAllNew(proper, fixing);
        putAllNew(proper, newParams);

        Iterable<String> poLast = getLastParamsOrder(newParams, fixing);
        if (poLast != null) {
            for (String pn : poLast) {
                Object v = proper.remove(pn);
                if (v != null) {
                    proper.put(pn, v);
                }
            }
        }

        Map<String, Object> defVals = getDefaultVals(newParams, fixing);
        if (defVals != null) {
            for (Entry<String, Object> e : defVals.entrySet()) {
                String key = e.getKey();
                Object v = proper.get(key);
                if (BaseUtils.safeEquals(v, e.getValue())) {
                    proper.remove(key);
                }
            }
        }

        boolean forceRemote = forRemoteURL || forceRemoteForFinalParams(proper);

        String subdomain = null;
        
        if (reqCtx.getUsesSubdomains()) {
            subdomain = extractOptionalSubdomain(proper);
            forceRemote = forceRemote || !BaseUtils.isStrEmptyOrWhiteSpace(subdomain);
        }

        String urlBase = reqCtx.getServletUrlBase(forceRemote);

        if (!BaseUtils.isStrEmptyOrWhiteSpace(subdomain)) {
            int pos = urlBase.indexOf("://") + 3;
            urlBase = urlBase.substring(0, pos) + subdomain + "." + urlBase.substring(pos);
        }

        StringBuilder sb = new StringBuilder();
        
        sb.append(urlBase);

        UrlMakingUtils.encodeParamsForUrlWW(sb, proper, "?");

        //System.out.println("innerMakeUrl: remote=" + forRemoteURL + ", sb.toStr()=" + sb.toString());

        String fullUrl = postProcessUrlAsString(sb.toString());

        Pair<String, String> p = BaseUtils.splitGlobalUrl(fullUrl);

        String res = p.v1 + reqCtx.encodeUrl(p.v2);

        if (logger.isDebugEnabled()) {
            logger.debug("fullUrl=" + fullUrl
                    + ", p.v1=" + p.v1 + ", p.v2=" + p.v2
                    + ", final url=" + res
                    + ", proper params for url=" + proper);
        }

        return res;
    }

    protected Map<String, Object> putInFixing(Map<String, Object> fixing, String paramName,
            Object paramValue) {
        if (fixing == null) {
            fixing = new HashMap<String, Object>();
        }
        fixing.put(paramName, paramValue);
        return fixing;
    }

    protected Map<String, Object> calcParamFixingForDefault(Map<String, Object> newParams,
            Map<String, Object> fixing, String paramName) {
        if (fixing == null || !fixing.containsKey(paramName)) {
            if (newParams == null || !newParams.containsKey(paramName)) {
                Map<String, Object> defaults = reqCtx.getParams();
                if (defaults != null && defaults.containsKey(paramName)) {
                    fixing = putInFixing(fixing, paramName, defaults.get(paramName));
                }
            }
        }
        return fixing;
    }

    protected Object getValOfDefaultParam(String paramName) {
        return BaseUtils.safeMapGet(reqCtx.getParams(), paramName);
    }

    protected Map<String, Object> calcParamFixingForDefaults(Map<String, Object> newParams,
            Map<String, Object> fixing, Iterable<String> paramNames) {
        if (paramNames != null) {
            for (String paramName : paramNames) {
                fixing = calcParamFixingForDefault(newParams, fixing, paramName);
            }
        }
        return fixing;
    }

    protected Map<String, Object> calcParamFixingByOverrides(Map<String, Object> newParams,
            Map<String, Object> fixing) {
        return fixing;
    }

    protected Map<String, Object> calcParamFixingByDefaults(Map<String, Object> newParams,
            Map<String, Object> fixing) {
        return calcParamFixingForDefaults(newParams, fixing, getDefaultParamNames(newParams, fixing));
    }

    protected Map<String, Object> calcParamFixing(Map<String, Object> newParams,
            Map<String, Object> fixing) {
        fixing = calcParamFixingByOverrides(newParams, fixing);
        return calcParamFixingByDefaults(newParams, fixing);
    }

    public String getCanonicalUrlBase() {
        return reqCtx.encodeUrl(reqCtx.getServletUrlBase(true));
    }

    protected Map<String, Object> getDefaultVals(Map<String, Object> newParams, Map<String, Object> fixing) {
        return null;
    }

    protected boolean forceRemoteForFinalParams(Map<String, Object> proper) {
        return false;
    }

    protected String extractOptionalSubdomain(Map<String, Object> proper) {
        return null;
    }
}
