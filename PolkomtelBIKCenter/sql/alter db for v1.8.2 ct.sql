﻿exec sp_check_version '1.8.2';
go

if not exists (select * from sys.all_columns where name='is_registry' and object_id=object_id('bik_node_kind'))
alter table bik_node_kind add is_registry int not null default 0
go
update bik_node_kind set is_registry=0 where is_registry is null

if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[sp_add_attr_category]'))
drop procedure [dbo].[sp_add_attr_category]
go

create procedure [dbo].[sp_add_attr_category](@name varchar(max)) as
begin
	if not exists (select 1 from bik_attr_category where name=@name)
	insert into bik_attr_category(name) values (@name)
	else update bik_attr_category set is_deleted=0 where name=@name
end
go

if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[sp_add_attr_def]'))
drop procedure [dbo].[sp_add_attr_def]
go

create procedure [dbo].[sp_add_attr_def](@name varchar(max), @attrCategory int, @typeAttr varchar(max), @valueOpt varchar(max), @displayAsNumber int, @drools int) as
begin
	if not exists (select 1 from bik_attr_def where name=@name and is_built_in=0)
	insert into bik_attr_def(name, attr_category_id, type_attr, value_opt)
	values(@name, @attrCategory, @typeAttr, @valueOpt)
	else
	update bik_attr_def set attr_category_id=@attrCategory, is_deleted=0, type_attr=@typeAttr, value_opt=@valueOpt, display_as_number=@displayAsNumber, in_drools=@drools
	where name=@name and is_built_in=0
end
go

if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[fn_get_bik_icons_as_string]'))
drop function [dbo].[fn_get_bik_icons_as_string]
go

create function [dbo].[fn_get_bik_icons_as_string]() returns varchar(max) as
begin
	declare @icons varchar(max) = ''
	SELECT @icons={fn CONCAT(ISNULL(@icons, ''), ISNULL( coalesce(name+',', ''), ''))} from bik_icon

	--select @icons=concat(@icons, coalesce(name+',', '')) from bik_icon

	if len(@icons) > 0
	set @icons = substring(@icons,0, len(@icons)-1)
	return @icons
end
go

if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[sp_add_attr_2_node_kind]'))
drop procedure [dbo].[sp_add_attr_2_node_kind]
go

create procedure [dbo].[sp_add_attr_2_node_kind](@nodeKindCode varchar(max), @attrName varchar(max), @default varchar(max), @required int, @type varchar(max), @valueOpt varchar(max)) as
begin
	declare @nkId int = (select id from bik_node_kind where code=@nodeKindCode)
	declare @attrDefId int = (select id from bik_attr_def where name=@attrName)
	if (@nkId is null) or (@attrDefId is null) return

	if not exists (select 1 from bik_attribute where node_kind_id=@nkId and attr_def_id=@attrDefId)
	insert into bik_attribute(node_kind_id, attr_def_id, default_value, is_required, type_attr, value_opt)
	select @nkId, @attrDefId, @default, @required, @type, @valueOpt
 	else
	update bik_attribute set is_deleted=0, default_value=@default, is_required=@required, is_visible=1, type_attr=@type, value_opt=@valueOpt
	where node_kind_id=@nkId and attr_def_id=@attrDefId
end
go

if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[sp_add_node_kind_relation]'))
drop procedure [dbo].[sp_add_node_kind_relation]
go

create procedure [dbo].[sp_add_node_kind_relation](@treeKindId int, @srcCode varchar(max), @dstCode varchar(max), @relation varchar(max)) as
begin
	declare @srcNodeKindId int = (select id from bik_node_kind where code=@srcCode)
	declare @dstNodeKindId int = (select id from bik_node_kind where code=@dstCode)
	if (@srcNodeKindId is null) or (@dstNodeKindId is null) return

	if not exists (select 1 from bik_node_kind_4_tree_kind where tree_kind_id=@treeKindId and src_node_kind_id=@srcNodeKindId and dst_node_kind_id=@dstNodeKindId and relation_type=@relation)
	insert into bik_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type)
	select @treeKindId, @srcNodeKindId, @dstNodeKindId, @relation
 	else
	update bik_node_kind_4_tree_kind set is_deleted=0
	where tree_kind_id=@treeKindId and src_node_kind_id=@srcNodeKindId and dst_node_kind_id=@dstNodeKindId and relation_type=@relation
end
go

exec dbo.sp_add_attr_category 'Metadata BIKS'
go

declare @metaCategory int = (select id from bik_attr_category where name='Metadata BIKS')
declare @icons varchar(max) = dbo.fn_get_bik_icons_as_string()

exec sp_add_attr_def 'metaBIKS.Ikona', @metaCategory, 'combobox', @icons, 0, 0
exec sp_add_attr_def 'metaBIKS.Folder', @metaCategory, 'comboBooleanBox', 'Tak,Nie', 0, 0
exec sp_add_attr_def 'metaBIKS.Dzieci w tabeli', @metaCategory, 'shortText', null, 0, 0
exec sp_add_attr_def 'metaBIKS.Atrybuty dzieci w tabeli', @metaCategory, 'shortText', null, 0, 0
exec sp_add_attr_def 'metaBIKS.Typ atrybutu', @metaCategory, 'combobox', 'shortText,longText,ansiiText,hyperlinkIn,hyperlinkOut,data,combobox,checkbox,comboBooleanBox,calculation,javascript,selectOneJoinedObject', 0, 0
exec sp_add_attr_def 'metaBIKS.Nadpisany typ atrybutu', @metaCategory, 'combobox', 'shortText,longText,ansiiText,hyperlinkIn,hyperlinkOut,data,combobox,checkbox,comboBooleanBox,calculation,javascript,selectOneJoinedObject,', 0, 0
exec sp_add_attr_def 'metaBIKS.Wyświetlona nazwa', @metaCategory, 'shortText', null, 0, 0
exec sp_add_attr_def 'metaBIKS.Lista wartości', @metaCategory, 'shortText', null, 0, 0
exec sp_add_attr_def 'metaBIKS.Wyświetlaj jako liczbę', @metaCategory, 'comboBooleanBox', 'Tak,Nie', 0, 0
exec sp_add_attr_def 'metaBIKS.Używaj w systemie zarządzania regułami', @metaCategory, 'comboBooleanBox', 'Tak,Nie', 0, 0
exec sp_add_attr_def 'metaBIKS.Wzorzec', @metaCategory, 'hyperlinkIn', null, 0, 0
exec sp_add_attr_def 'metaBIKS.Wartość domyślna', @metaCategory, 'shortText', null, 0, 0
exec sp_add_attr_def 'metaBIKS.Wymagany', @metaCategory, 'comboBooleanBox', 'Tak,Nie', 0, 0
exec sp_add_attr_def 'metaBIKS.Pozwolić dowiązać', @metaCategory, 'comboBooleanBox', 'Tak,Nie', 0, 0
exec sp_add_attr_def 'metaBIKS.Typ w drzewie', @metaCategory, 'combobox', 'Główny gałąź+Gałąź,Główny gałąź+Liść,Gałąź,Liść', 0, 0
exec sp_add_attr_def 'metaBIKS.Typ relacji', @metaCategory, 'combobox', 'Powiązanie,Dowiązanie,Dziecko,Hyperlink', 0, 0
exec sp_add_attr_def 'metaBIKS.Rejestr', @metaCategory, 'comboBooleanBox', 'Tak,Nie', 0, 0

update bik_attribute set is_deleted=1
where node_kind_id in (
	select id from bik_node_kind
	where code in ('metaBiksAttributeDef', 'metaBiksNodeKind', 'metaBiksAttribute4NodeKind', 'metaBiksTreeKind', 'metaBiksNodeKind4TreeKind', 'metaBiksNodeKindInRelation')
)
exec sp_add_attr_2_node_kind 'metaBiksAttributeDef', 'metaBIKS.Lista wartości', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksAttributeDef', 'metaBIKS.Typ atrybutu', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksAttributeDef', 'metaBIKS.Używaj w systemie zarządzania regułami', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksAttributeDef', 'metaBIKS.Wyświetlaj jako liczbę', null, 1, null, null

exec sp_add_attr_2_node_kind 'metaBiksNodeKind', 'metaBIKS.Atrybuty dzieci w tabeli', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksNodeKind', 'metaBIKS.Dzieci w tabeli', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksNodeKind', 'metaBIKS.Folder', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksNodeKind', 'metaBIKS.Ikona', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksNodeKind', 'metaBIKS.Wyświetlona nazwa', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksNodeKind', 'metaBIKS.Rejestr', null, 1, null, null

exec sp_add_attr_2_node_kind 'metaBiksAttribute4NodeKind', 'metaBIKS.Lista wartości', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksAttribute4NodeKind', 'metaBIKS.Wzorzec', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksAttribute4NodeKind', 'metaBIKS.Nadpisany typ atrybutu', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksAttribute4NodeKind', 'metaBIKS.Wartość domyślna', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksAttribute4NodeKind', 'metaBIKS.Wymagany', null, 1, null, null

exec sp_add_attr_2_node_kind 'metaBiksTreeKind', 'metaBIKS.Pozwolić dowiązać', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksTreeKind', 'metaBIKS.Wyświetlona nazwa', null, 1, null, null

exec sp_add_attr_2_node_kind 'metaBiksNodeKind4TreeKind', 'metaBIKS.Wzorzec', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksNodeKind4TreeKind', 'metaBIKS.Typ w drzewie', null, 1, null, null

exec sp_add_attr_2_node_kind 'metaBiksNodeKindInRelation', 'metaBIKS.Wzorzec', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksNodeKindInRelation', 'metaBIKS.Typ relacji', null, 1, null, null

declare @metaTreeKindId int = (select id from bik_tree_kind where code='metaBIKS')
exec sp_add_node_kind_relation @metaTreeKindId, 'metaBiksAttribute4NodeKind', 'metaBiksAttributeDef', 'Hyperlink'
exec sp_add_node_kind_relation @metaTreeKindId, 'metaBiksNodeKind4TreeKind', 'metaBiksNodeKind', 'Hyperlink'
exec sp_add_node_kind_relation @metaTreeKindId, 'metaBiksNodeKindInRelation', 'metaBiksNodeKind', 'Hyperlink'

exec sp_update_version '1.8.2', '1.8.2.1';
