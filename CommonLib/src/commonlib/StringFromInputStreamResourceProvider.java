/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.io.InputStream;
import simplelib.CompoundGenericResourceProvider;
import simplelib.IGenericResourceProvider;

/**
 *
 * @author wezyr
 */
public class StringFromInputStreamResourceProvider extends CompoundGenericResourceProvider<InputStream, String>
        implements IGenericResourceProvider<String> {

    public StringFromInputStreamResourceProvider(IGenericResourceProvider<InputStream> isResProvider,
            String encoding) {
        super(isResProvider, new InputStreamToStringProjector(encoding));
    }
}
