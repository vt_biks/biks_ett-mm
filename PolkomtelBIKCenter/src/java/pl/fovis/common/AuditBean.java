/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.util.Date;
import simplelib.BeanWithIntegerIdBase;

/**
 *
 * @author bfechner
 */
public class AuditBean extends BeanWithIntegerIdBase{

    public Integer viewCnt;
    public Integer refreshCnt;
    public Date viewLast;
    public Date refreshLast;
    public String name;
    public String cuid;
    public String path;
    public String userRefreshLast;
    public String userViewLast;

    public String getUserRefreshLast() {
        return userRefreshLast;
    }

    public String getUserViewLast() {
        return userViewLast;
    }

    public String getPath() {
        return path;
    }

    public Integer getViewCnt() {
        return viewCnt;
    }

    public Integer getRefreshCnt() {
        return refreshCnt;
    }

    public Date getViewLast() {
        return viewLast;
    }

    public Date getRefreshLast() {
        return refreshLast;
    }

    public String getName() {
        return name;
    }

    public String getCuid() {
        return cuid;
    }
}
