/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class XlsxWriter {

    protected boolean isInitialized = false;

    protected List<String> allColumnNames;
    protected List<String> headerNames;
    protected SXSSFWorkbook wb;
    protected SXSSFSheet mainSheet;

    public XlsxWriter() {
        this(null);
    }

    public XlsxWriter(List<String> headerNames) {
        this.headerNames = headerNames;
    }

    public void writeContentAndCloseWorkBook(List<Map<String, Object>> data, OutputStream output) throws IOException {
        writeContent(data);
        closeWorkbook(output);
    }

    protected void initIfNecessary(List<Map<String, Object>> data) {
        if (!isInitialized) {
            wb = new SXSSFWorkbook();
            mainSheet = (SXSSFSheet) wb.createSheet();
            mainSheet.setAutobreaks(true);
            mainSheet.setDefaultColumnWidth(20); // ?
            createHeaderRow(data);
            isInitialized = true;
        }
    }

    protected void createHeaderRow(List<Map<String, Object>> data) {
        allColumnNames = new ArrayList<String>();
        for (Map<String, Object> row : data) {
            for (String colName : row.keySet()) {
                if (allColumnNames.indexOf(colName) < 0) {
                    allColumnNames.add(colName);
                }
            }
        }
        if (headerNames == null) {
            headerNames = allColumnNames;
        }
        Row rowHead = mainSheet.createRow((short) 0);

        for (int nCol = 0; nCol < headerNames.size(); nCol++) {
            rowHead.createCell(nCol).setCellValue(String.valueOf(headerNames.get(nCol)));
        }
    }

    protected void createRow(int rowNum, Sheet sheet, Map<String, Object> row, boolean isUseAllColumnNames) {
        Row rowHead = sheet.createRow(/*(short) */rowNum);
        for (Map.Entry<String, Object> e : row.entrySet()) {
            String colName = e.getKey();
            if (isUseAllColumnNames) {
                rowHead.createCell(allColumnNames.indexOf(colName)).setCellValue(BaseUtils.safeToStringNotNull(e.getValue()));
            } else {
                rowHead.createCell(headerNames.indexOf(colName)).setCellValue(BaseUtils.safeToStringNotNull(e.getValue()));
            }
        }
    }

    public String safeToStringNum(Object value) {
        if (value != null && value instanceof Number) {
            DecimalFormat df = new DecimalFormat("#");
            return df.format((Number) value);
        } else {
            return BaseUtils.safeToStringNotNull(value);
        }
    }

    public void writeContent(List<Map<String, Object>> data) throws IOException {
//        System.out.println("Total = " + Runtime.getRuntime().totalMemory() / (1024.0 * 1024.0) + "MB");
//        System.out.println("Free  = " + Runtime.getRuntime().freeMemory() / (1024.0 * 1024.0) + "MB");
//        initIfNecessary(data);
//        int rowNum = mainSheet.getLastRowNum() + 1;
////        System.out.println("RowNum = " + rowNum);
//        for (Map<String, Object> row : data) {
//            createRow(rowNum++, mainSheet, row);
//        }
        writeContent(data, false);

    }

    public void writeContent(List<Map<String, Object>> data, boolean isUseAllColumnNames) throws IOException {
//        System.out.println("Total = " + Runtime.getRuntime().totalMemory() / (1024.0 * 1024.0) + "MB");
//        System.out.println("Free  = " + Runtime.getRuntime().freeMemory() / (1024.0 * 1024.0) + "MB");
        initIfNecessary(data);
        int rowNum = mainSheet.getLastRowNum() + 1;
//        System.out.println("RowNum = " + rowNum);
        for (Map<String, Object> row : data) {
            createRow(rowNum++, mainSheet, row, isUseAllColumnNames);
        }
    }

    public void closeWorkbook(OutputStream output) throws IOException {
        if (wb != null) {
            wb.write(output);
            wb.close();
            wb.dispose();
            output.flush();
        }
    }
}
