/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons;

/**
 *
 * @author wezyr
 */
public interface IFileUploadParam {
    public String getFileName();
    public byte[] getFileContent();
    public long getFileSize();
}
