/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.popupmenu;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DecoratedPopupPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.NewLookUtils;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author wezyr
 */
public class PopupMenuDisplayer {

    protected DecoratedPopupPanel deco = new DecoratedPopupPanel(true, true);
    protected VerticalPanel vp = new VerticalPanel();
    protected Widget lastMenuItemForSep = null;
    protected boolean mustAddSep;

    {
        deco.setWidget(vp);
        GWTUtils.setStyleAttribute(deco, "zIndex", "900000");
        NewLookUtils.setupClassAndId(vp, "contexMenu");
    }

    public void clearMenuItems() {
        vp.clear();
        lastMenuItemForSep = null;
    }

    public void addMenuSeparator() {
        if (lastMenuItemForSep != null) {
            mustAddSep = true;
        }
    }

    public void addMenuItem(String caption, final IContinuation execAction, String optCss) {
        if (mustAddSep) {
            lastMenuItemForSep.addStyleName("pop-menuItem-last-in-group");
            lastMenuItemForSep = null;
            mustAddSep = false;
        }

        vp.add(lastMenuItemForSep = new BikCustomButton(caption, BaseUtils.nullToDef(optCss, "no-icon-popup-menu-btn"),
                new IContinuation() {

                    public void doIt() {
                        deco.hide();
                        execAction.doIt();
                    }
                }));
    }

    public void addMenuItem(String caption, final IContinuation execAction) {
        addMenuItem(caption, execAction, null);
    }

    public void hide() {
        deco.hide();
    }

    public void showAtPosition(final int clientX, final int clientY) {
        if (vp.getWidgetCount() > 0) {

            deco.setPopupPositionAndShow(new PopupPanel.PositionCallback() {

                public void setPosition(int offsetWidth, int offsetHeight) {
                    int height = deco.getOffsetHeight();
                    int screenHeight = Window.getClientHeight() + Window.getScrollTop();

                    //Window.alert("screenHeight=" + screenHeight + ", clientY=" + clientY + ", height=" + height);
                    int clientYFix;

                    if (clientY + height >= screenHeight) {
                        clientYFix = screenHeight - height;
                    } else {
                        clientYFix = clientY;
                    }
                    deco.setPopupPosition(clientX, clientYFix);
                }
            });
            //deco.show();
        }
    }
}
