/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;
import pl.trzy0.foxy.commons.FoxyRuntimeException;

/**
 *
 * @author lbiegniewski
 */
public class IntegerLDV implements LisaDictionaryValue, Serializable, IsSerializable {

    Integer value;

    public IntegerLDV() {
    }

    public IntegerLDV(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Object o) {

        if (o instanceof java.lang.Integer) {
            this.value = (java.lang.Integer) o;
        } else {
            throw new FoxyRuntimeException("IntegerLDV: " + "argument is not of 'Integer' type" /* i18n: no */);
        }
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
