/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts.luceneUtils;

import org.apache.lucene.document.Document;

/**
 *
 * @author pmielanczuk
 */
public class SearchWizardBucketManagerOneBucket<T> implements ISearchWizardBucketManager<T> {

    final protected T item;

    public SearchWizardBucketManagerOneBucket(T item) {
        this.item = item;
    }

    @Override
    public int getBucketCount() {
        return 1;
    }

    @Override
    public T assignToBucket(Document doc) {
        return item;
    }
}
