﻿exec sp_check_version '1.0.88.8';
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_sapbo_extradata]
GO

create procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
as
begin
	delete from bik_sapbo_extradata

	insert into bik_sapbo_extradata (node_id,author,owner,keyword,file_path,file_name,created,modified,cuid,files_value,size,has_children,guid,instance,owner_id,progid,obtype,flags,children,ruid,runnable_object,content_locale,is_schedulable,webi_doc_properties,read_only,last_successful_instance_id,last_run_time,timestamp,progid_machine,endtime,starttime,schedule_status,recurring,nextruntime,doc_sender,revisionnum,application_object,shortname,dataconnection_total,universe_total )
    select bik.id as node_id,
    nullif(ltrim(rtrim(p.SI_AUTHOR)), '') as author,
    nullif(ltrim(rtrim(p.SI_OWNER)), '') as owner,
    nullif(ltrim(rtrim(p.SI_KEYWORD)), '') as keyword,
    nullif(ltrim(rtrim(p.SI_FILES__SI_PATH)), '') as file_path,
    nullif(ltrim(rtrim(p.SI_FILES__SI_FILE1)), '') as file_name,
    nullif(ltrim(rtrim(p.SI_CREATION_TIME)), '') as created,
    nullif(ltrim(rtrim(p.SI_UPDATE_TS)), '') as modified,
    nullif(ltrim(rtrim(p.SI_CUID)), '') as cuid,
    nullif(ltrim(rtrim(p.SI_FILES__SI_VALUE1)), '') as files_value,
    nullif(ltrim(rtrim(p.SI_SIZE)), '') as size,
    p.SI_HAS_CHILDREN as has_children,
    nullif(ltrim(rtrim(p.SI_GUID)), '') as guid,
    p.SI_INSTANCE as instance,
    p.SI_OWNERID as owner_id,
    nullif(ltrim(rtrim(p.SI_PROGID)), '') as progid,
    p.SI_OBTYPE as obtype,
    p.SI_FLAGS as flags,
    p.SI_CHILDREN as children,
    nullif(ltrim(rtrim(p.SI_RUID)), '') as ruid,
    p.SI_RUNNABLE_OBJECT as runnable_object,
    nullif(ltrim(rtrim(p.SI_CONTENT_LOCALE)), '') as content_locale,
    p.SI_IS_SCHEDULABLE as is_schedulable,
    nullif(ltrim(rtrim(p.SI_WEBI_DOC_PROPERTIES)), '') as webi_doc_properties,
    p.SI_READ_ONLY as read_only,
    nullif(ltrim(rtrim(iw.SI_LAST_SUCCESSFUL_INSTANCE_ID)), '') as last_successful_instance_id,
    nullif(ltrim(rtrim(iw.SI_LAST_RUN_TIME)), '') as last_run_time,
    nullif(ltrim(rtrim(iw.SI_TIMESTAMP)), '')  as timestamp,
    nullif(ltrim(rtrim(iw.SI_PROGID_MACHINE)), '') as progid_machine,
    nullif(ltrim(rtrim(iw.SI_ENDTIME)), '') as endtime,
    nullif(ltrim(rtrim(iw.SI_STARTTIME)), '') as starttime,
    nullif(ltrim(rtrim(iw.SI_SCHEDULE_STATUS)), '') as schedule_status,
    nullif(ltrim(rtrim(iw.SI_RECURRING)), '') as recurring,
    nullif(ltrim(rtrim(iw.SI_NEXTRUNTIME)), '') as nextruntime,
    nullif(ltrim(rtrim(iw.SI_DOC_SENDER)), '') as doc_sender,
    au.SI_REVISIONNUM as revisionnum,
    au.SI_APPLICATION_OBJECT as application_object,
    nullif(ltrim(rtrim(au.SI_SHORTNAME)), '') as shortname,
    au.SI_DATACONNECTION__SI_TOTAL as dataconnection_total,
    iw.SI_UNIVERSE__SI_TOTAL as universe_total
    from aaa_global_props p
    left join INFO_WEBI iw on p.si_id = iw.si_id
    left join APP_UNIVERSE au on p.si_id = au.si_id
    inner join bik_node bik on convert(varchar,p.si_id) = bik.obj_id
    order by bik.id
end
go

update bik_node set is_built_in=0
where tree_id = (select id from bik_tree where name='Dokumenty')
or linked_node_id is not null
go

update bik_node set is_built_in=1
where (name='Wszystkie dokumenty' and tree_id = (select id from bik_tree where name='Dokumenty'))
or (name='Słowa kluczowe' and tree_id=(select id from bik_tree where name='Glosariusz'))
go

create table bik_sapbo_edited_description(
	id int identity(1,1) not null primary key,
	node_id int not null references bik_node(id),
	descr varchar(max),
	constraint uk_bik_sapbo_edited_description unique (node_id)
);
go

exec sp_update_version '1.0.88.8', '1.0.88.9';
go