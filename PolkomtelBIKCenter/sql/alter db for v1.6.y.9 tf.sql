﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- Konektor do MS SQL - fix na foldery typów
-- SAP BO 4.1 - Report SDK
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_check_version '1.6.y.9';
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_sapbo_server') and name = 'is_bo40')
begin
	alter table bik_sapbo_server add is_bo40 int not null default(0);
end;
go


if not exists(select 1 from bik_admin where param = 'sapbo4.reportPath')
begin
	insert into bik_admin(param, value)
	values('sapbo4.reportPath', 'C:/Temp/report41-pump/BIKSPumpSAPBO41ReportSDK.jar')
end;
go

if not exists(select 1 from bik_node_kind where code = 'MSSQLFolder')
begin
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('MSSQLFolder', 'Folder MSSQL', 'mssqlFolder', 'Metadata', 1, 0, 0)
	
	insert into bik_translation(code, txt, lang, kind)
	values('MSSQLFolder', 'MSSQL folder', 'en', 'kind')
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_confluence_extradata') and name = 'author_full_name')
begin
	alter table bik_confluence_extradata add author_full_name varchar(512) null;
end;
go


exec sp_update_version '1.6.y.9', '1.6.y.10';
go