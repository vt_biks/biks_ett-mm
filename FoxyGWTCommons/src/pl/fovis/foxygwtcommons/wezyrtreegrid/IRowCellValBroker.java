/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.wezyrtreegrid;

/**
 *
 * @author wezyr
 */
public interface IRowCellValBroker<ROW> {

    public Object getCellVal(ROW row, String colName);

    public String getRowIconUrl(ROW row);
    
    public String getRowIconTitle(ROW row);

    public String getAdditionalRowIconUrl(ROW row);
}
