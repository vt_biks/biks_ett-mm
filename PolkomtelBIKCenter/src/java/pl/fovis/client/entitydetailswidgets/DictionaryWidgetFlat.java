/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.JoinedObjBean;
import pl.fovis.common.JoinedObjsTabKind;

/**
 *
 * @author tflorczak
 */
public class DictionaryWidgetFlat extends JoinedObjWidgetFlatBase<JoinedObjBean> {

    @Override
    protected List<JoinedObjBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
        return cddb.jobs.get(JoinedObjsTabKind.Dictionary);
    }

    @Override
    public String getCaption() {
        return BIKGWTConstants.MENU_NAME_GLOSSARY_DICTIONARY;
    }

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_DICTIONARY;
    }

    @Override
    protected Set<String> getAdditionalsTreeKind() {
        Set<String> treeKinds = new HashSet<String>();
        treeKinds.add(BIKGWTConstants.TREE_KIND_DICTIONARY_DWH);
        return treeKinds;
    }
}
