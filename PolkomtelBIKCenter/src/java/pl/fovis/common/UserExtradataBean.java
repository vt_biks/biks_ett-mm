/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class UserExtradataBean implements Serializable {

    public String sAMAccountName;
    public String distinguishedName;
    public String userPrincipalName;
    public String wWWHomePage;
    public String url;
    public String title;
    public String telephoneNumber;
    public String streetAddress;
    public String st;
    public String sn;
    public String postOfficeBox;
    public String postalCode;
    public String physicalDeliveryOfficeName;
    public String pager;
    public String otherTelephone;
    public String otherPager;
    public String otherMobile;
    public String otherIpPhone;
    public String otherFacsimileTelephoneNumber;
    public String mobile;
    public String manager;
    public String managedObjects;
    public String l;
    public String ipPhone;
    public String initials;
    public String homePhone;
    public String givenName;
    public String facsimileTelephoneNumber;
    public String directReports;
    public String department;
    public String displayName;
    public String description;
    public String notes;
    public String co;
    public String c;
    //
    public String company;
    public String mail;

    /// ------------- Avatar ---------------
    public String avatar;

    public String loginAd;
}
