﻿exec sp_check_version '1.1.9.17';
go

----------usuwanie kolumny z defaultem i checkiem z is_admin  w bik_system_user--------------------
declare @table_name sysname = 'bik_system_user', @column_name sysname = 'is_admin'
declare @default_constraint_name_admin sysname
select @default_constraint_name_admin = object_name(cdefault)
from syscolumns
where id = object_id(@table_name)
and name = @column_name
--print @default_constraint_name_admin

if @default_constraint_name_admin is not null
  exec ('alter table ' + @table_name + ' drop constraint ' + @default_constraint_name_admin)
go

declare @table_name sysname = 'bik_system_user', @column_name sysname = 'is_admin'
declare @check_constraint_name_admin sysname
select @check_constraint_name_admin = cc.name
from sys.check_constraints cc 
inner join syscolumns sc on cc.parent_object_id = sc.id and cc.parent_column_id = sc.colid
where cc.parent_object_id = object_id(@table_name)  and sc.name = @column_name
--print @check_constraint_name_admin

if(@check_constraint_name_admin is not null)
	exec ('alter table ' + @table_name + ' drop constraint ' + @check_constraint_name_admin)
go


alter table bik_system_user drop column is_admin
go

----------usuwanie kolumny z defaultem i checkiem z is_ekspert w bik_system_user--------------------

declare @table_name sysname = 'bik_system_user', @column_name sysname = 'is_expert'
declare @default_constraint_name_ekspert sysname
select @default_constraint_name_ekspert = object_name(cdefault)
from syscolumns
where id = object_id(@table_name)
and name = @column_name
--print @default_constraint_name_ekspert

if @default_constraint_name_ekspert is not null
  exec ('alter table ' + @table_name + ' drop constraint ' + @default_constraint_name_ekspert)
go


declare @table_name sysname = 'bik_system_user', @column_name sysname = 'is_expert'
declare @check_constraint_name_expert sysname
select @check_constraint_name_expert = cc.name
from sys.check_constraints cc 
inner join syscolumns sc on cc.parent_object_id = sc.id and cc.parent_column_id = sc.colid
where cc.parent_object_id = object_id(@table_name)  and sc.name = @column_name
--print @check_constraint_name_expert

if(@check_constraint_name_expert is not null)
	exec ('alter table ' + @table_name + ' drop constraint ' + @check_constraint_name_expert)
go

alter table bik_system_user drop column is_expert
go

exec sp_update_version '1.1.9.17', '1.1.9.18';
go

