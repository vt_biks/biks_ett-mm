/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.common.TreeNodeBean;

/**
 *
 * @author wezyr
 */
public class NodeClipboard implements INodeClipboard {

    protected TreeNodeBean node;
    protected boolean isCut;

    public void clear() {
        node = null;
        isCut = false;
    }

    public void copy(TreeNodeBean node) {
        this.node = node;
        this.isCut = false;
    }

    public void cut(TreeNodeBean node) {
        this.node = node;
        this.isCut = true;
    }

    public TreeNodeBean getNode() {
        return node;
    }

    public boolean isCut() {
        return isCut;
    }
}
