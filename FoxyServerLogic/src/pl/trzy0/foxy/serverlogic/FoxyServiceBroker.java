/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.JsonConverterEx;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import simplelib.BaseUtils;
import simplelib.MapHelper;
import simplelib.StopWatch;
import simplelib.logging.ILameLogger;
import simplelib.logging.ILameLogger.LameLogLevel;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class FoxyServiceBroker<T extends IFoxyBizLogic, SCO> {

    private static ILameLogger logger = LameLoggerFactory.getLogger(FoxyServiceBroker.class);
    private Map<String, Method> methods = new HashMap<String, Method>();

    //public <TT extends IFoxyService<? extends T, ? extends SCO>> void init(Class<TT> serviceClass) {
    public void init(Class<? extends IFoxyService<? extends T, SCO>> serviceClass) {
        for (Method m : serviceClass.getMethods()) {
            String name = m.getName();
            if (name.startsWith("get") && name.endsWith("Data")) {
                methods.put(name.substring(3, name.length() - 4), m);
            }
        }
    }

    private String innerExecute(ISessionEnvironment sessionEnv, IFoxyService service, String objName, MapHelper<String> mh) {
        StopWatch sw = new StopWatch(logger, "innerExecute", LameLogLevel.Info);
        //System.out.println("execute!!!");
        //Date start = new Date();

        sw.start("prepare");

        String requestPropsStr = null;
        if (logger.isInfoEnabled()) {
            requestPropsStr = "(" + objName + ", " + mh.helperAsMap() + ")";
            logger.info("execute starts for " + requestPropsStr);
        }

        Method m = methods.get(BaseUtils.capitalize(objName));
        if (m == null) {
            logger.errorAndThrowNew("no method for objName=\"" + objName + "\"");
        }

        sw.start("attach session");
        Object res = null;
        try {
            service.attachSessionEnvironment(sessionEnv);
            try {
                sw.start("exec method");
                res = m.invoke(service, mh);
                sw.start("after method");
            } finally {
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.errorAndThrowNew("error while invoking method " + m.getName(), ex);
        }

        //Date executedTime = new Date();

        sw.start("convert to json");
        String str = null;
        try {
            str = JsonConverterEx.convertObject(res);
        } catch (Exception ex) {
            logger.errorAndThrowNew("error while converting result to string, for method: " + m.getName() +
                    " result of class: " + BaseUtils.safeGetClassName(res), ex);
        }
        sw.stopAndLog();

        if (logger.isInfoEnabled()) {
            logger.info("execute ends for " + requestPropsStr +
                    ", result size: " + str.length());
        }

        return str;
    }

    public /*synchronized*/ String execute(ISessionEnvironment sessionEnv, IFoxyService service, String objName, MapHelper<String> mh) {
        try {
            LameLoggerFactory.startBuffering();
            return innerExecute(sessionEnv, service, objName, mh);
        } finally {
            LameLoggerFactory.endBuffering();
        }
    }
}
