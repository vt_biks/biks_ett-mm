﻿exec sp_check_version '1.6.z3.3';
go


if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_confluence_extradata') and name = 'short_link')
  alter table bik_confluence_extradata add short_link varchar(512) null;
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fn_last_charindex]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [fn_last_charindex]
GO

create function fn_last_charindex(@what_to_search_for varchar(max), @search_in varchar(max), @opt_start_from_pos int) returns int as
begin
  if @what_to_search_for is null return null;
  
  declare @src_len int = datalength(@search_in);
  
  if @opt_start_from_pos is null set @opt_start_from_pos = @src_len;
  
  declare @dst_len int = datalength(@what_to_search_for);
  
  if @dst_len = 0 return @opt_start_from_pos;
  
  if @dst_len > @src_len return 0;
  
  if @dst_len = @src_len
    if @what_to_search_for = @search_in return 1 else return 0;
  
  declare @pos_limit int = @src_len - @dst_len + 1;
  
  if @opt_start_from_pos > @pos_limit set @opt_start_from_pos = @pos_limit;
  
  declare @src_trimmed varchar(max) = substring(@search_in, 1, @opt_start_from_pos + @dst_len - 1)
  
  if @opt_start_from_pos = 1   
    if @what_to_search_for = @src_trimmed return 1 else return 0;
    
  declare @src_rev varchar(max) = reverse(@src_trimmed);
  declare @dst_rev varchar(max) = reverse(@what_to_search_for);
  
  declare @rev_pos int = charindex(@dst_rev, @src_rev);
  
  if @rev_pos = 0 return 0;
  
  declare @src_trimmed_len int = datalength(@src_trimmed);
  
  return @src_trimmed_len - @rev_pos + 1 - @dst_len + 1;
end
go

/*

select len('aqq ')

select datalength('aqq')

select datalength(null)

select dbo.fn_last_charindex('ab', 'ab ab ca ab ', 10)

*/

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fn_confluence_extract_short_link]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [fn_confluence_extract_short_link]
GO

create function fn_confluence_extract_short_link(@html varchar(max)) returns varchar(max) as
begin
  -- <link rel="shortlink" href="http://cyfropedia01-t2/x/NABJAw">

  declare @len int = datalength(@html);
  
  declare @pos int = 1;
  
  while @pos <= @len
  begin
  
    set @pos = charindex('shortlink', @html, @pos);
  
	if @pos = 0 return null;

    declare @old_pos int = @pos;
    
    set @pos = @pos + 9; -- shortlink 9 znaków
    
	declare @left_angular_pos int = dbo.fn_last_charindex('<', @html, @old_pos);
  
    if @left_angular_pos = 0
      --return 'no left <'; --
      continue;
  
    declare @part varchar(100) = substring(@html, @left_angular_pos + 1, 4); -- link 4 znaki  
  
    if @part <> 'link'
      --return 'no link'; --
      continue;
      
    declare @end_pos int = charindex('>', @html, @pos);
    
    if @end_pos = 0
      return --'no end pos'; --
        null;
      
    set @part = substring(@html, @left_angular_pos + 1, @end_pos - @left_angular_pos - 1);
    
    declare @href_pos int = charindex('href', @part);
    
    if @href_pos = 0
      --return 'no href pos'; --
       continue;
    
    set @part = substring(@part, @href_pos + 4, datalength(@part)); -- href 4 znaki
      
    declare @left_apos_pos int = patindex('%[''"]%', @part);
    
    if @left_apos_pos = 0
      --return 'no left apos';-- 
      continue;
      
    declare @right_apos_pos int = charindex(substring(@part, @left_apos_pos, 1), @part, @left_apos_pos + 1);
    
    if @right_apos_pos = 0
      --return 'no right apos: ' + @part + ', left pos=' + cast(@left_apos_pos as varchar(10)) --
      continue;
    
    return ltrim(rtrim(substring(@part, @left_apos_pos + 1, @right_apos_pos - @left_apos_pos - 1)));
  end;
  
  return null;
end
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

update bik_confluence_extradata set short_link = dbo.fn_confluence_extract_short_link(content);
go

exec sp_update_version '1.6.z3.3', '1.6.z3.4';
go