/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.BikItemButtonBig;
import pl.fovis.client.nodeactions.UserGroupPopUpMenu;
import pl.fovis.client.nodeactions.UserRolePopUpMenu;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeOrListGridRightClickHandler;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author Eugeniusz
 */
public class SystemPage extends BikPageMinimalBase implements IBikPageForAppAdmin {

    protected ScrollPanel treeScrollPanel = new ScrollPanel();
    protected ScrollPanel contentScrollPanel = new ScrollPanel();
    protected IPageDataFetchBroker dataFetchBroker = new PageDataFetchBroker();
    protected UserGroupTreeWidget treeWidget;
    protected FlowPanel rightPanel;
    protected TextBox filterBox;

    @Override
    protected Widget buildWidgets() {
        HorizontalPanel main = new HorizontalPanel();

        VerticalPanel left = new VerticalPanel();
        left.setStyleName("EntityDetailsPane");
        GWTUtils.setStyleAttribute(left, "padding" /* I18N: no */, "4px");

        rightPanel = new FlowPanel();
//        rightPanel.setStyleName("EntityDetailPane");
//        GWTUtils.setStyleAttribute(rightPanel, "padding" /* I18N: no */, "4px");

        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(left,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 8);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(rightPanel,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 8);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(contentScrollPanel,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 8);

        main.add(left);
        contentScrollPanel.add(rightPanel);
        main.add(contentScrollPanel);

        contentScrollPanel.setWidth("100%");
        contentScrollPanel.setHeight("100%");
        main.setCellWidth(contentScrollPanel, "100%");
        main.setCellHeight(contentScrollPanel, "100%");
        main.setCellHeight(left, "100%");

        left.add(treeScrollPanel);
        treeScrollPanel.setWidth("330px");

        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(treeScrollPanel,
                BIKClientSingletons.ORIGINAL_TREE_GRID_PANEL_HEIGHT);

        treeWidget = new UserGroupTreeWidget(null, dataFetchBroker, false);
        treeWidget.addSelectionChangedHandler(new IParametrizedContinuation<Map<String, Object>>() {

            @Override
            public void doIt(Map<String, Object> param) {
                BIKClientSingletons.getService().getSystemUsersGroup(treeWidget.getCurrentNodeBean().id, new StandardAsyncCallback<SystemUserGroupBean>("Error in getSystemUsersGroup") {

                    @Override
                    public void onSuccess(SystemUserGroupBean result) {
                        populateRightPanelWithGroup(result);
                    }
                });
            }
        });
        treeWidget.addRightClickHandler(new IFoxyTreeOrListGridRightClickHandler<Map<String, Object>>() {

            @Override
            public void doIt(Map<String, Object> dataRow, int clientX, int clientY, boolean isRowSelected) {
                ITreeRowAndBeanStorage<String, Map<String, Object>, SystemUserGroupBean> storage = treeWidget.getTreeStorage();
                final SystemUserGroupBean sub = storage.getBeanById(storage.getTreeBroker().getNodeId(dataRow));
                if (!sub.isBuiltIn) {
                    if (BIKClientSingletons.newRightsSysByAppProps()) {
                        new UserRolePopUpMenu(sub, clientX, clientY, new IContinuation() {

                            @Override
                            public void doIt() {
                                show();
                            }
                        }).createAndShowPopup();
                    } else {
                        new UserGroupPopUpMenu(sub, clientX, clientY, new IContinuation() {

                            @Override
                            public void doIt() {
                                show();
                            }
                        }).createAndShowPopup();
                    }

                }
            }
        });
        Widget treeGridWidget = treeWidget.getTreeGridWidget();
        treeScrollPanel.add(treeGridWidget);
        treeGridWidget.setWidth("330px");
        // filter box
        HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
        filterBox = new TextBox();
        filterBox.setStyleName("filterBox");
        filterBox.setWidth("164px");
        hPanel.add(filterBox);
        hPanel.setCellWidth(filterBox, "180px");
        hPanel.setStyleName("treeBikFilterBox");
        hPanel.setWidth("100%");

        IContinuation filterCont = new IContinuation() {
            @Override
            public void doIt() {
                doFilter();
            }
        };
        BikItemButtonBig filterButton = new BikItemButtonBig(I18n.fILTRUJ.get() /* I18N:  */, filterCont);
        GWTUtils.addEnterKeyHandler(filterBox, filterCont);
        hPanel.add(filterButton);
        hPanel.setCellHorizontalAlignment(filterButton, HasHorizontalAlignment.ALIGN_LEFT);

        PushButton addButton = new PushButton(new Image("images/plus_large.png" /* I18N: no */), new Image("images/plus_large.png" /* I18N: no */));
        addButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                openAddRoleOrGroupDialog();
            }
        });
        setTitleToAddBtn(addButton);

        addButton.addStyleName("plusLarge");
        hPanel.add(addButton);

        left.add(hPanel);
        show();
        return main;
    }

    protected void doFilter() {
        //jezeli nic nie zostalo wpisane to wroc do pierwotnego wygladu
        if (filterBox.getText().trim().isEmpty()) {
            show();
        } else {
            rightPanel.clear();
            treeWidget.filterTreeEx(filterBox.getText(), null, true, null, null, null);
        }
    }

    protected void setTitleToAddBtn(PushButton addButton){
    }
    
    @Override
    public String getId() {
        return "true";
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        // no op
        show();
    }

    protected void populateRightPanelWithGroup(final SystemUserGroupBean result) {
    }

    protected void show() {
    }

    protected void openAddRoleOrGroupDialog() {
    }
}
