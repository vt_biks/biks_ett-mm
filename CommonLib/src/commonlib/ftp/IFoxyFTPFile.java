/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.ftp;

/**
 *
 * @author pmielanczuk
 */
public interface IFoxyFTPFile {

    public String getName();

    public boolean isDirectory();

    public long getSize();
}
