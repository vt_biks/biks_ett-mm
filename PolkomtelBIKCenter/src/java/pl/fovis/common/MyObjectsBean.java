/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author tflorczak
 */
public class MyObjectsBean implements Serializable {

    public String role;
    public List<ChildrenBean> objects;

    public MyObjectsBean() {
    }

    public MyObjectsBean(String role, List<ChildrenBean> objects) {
        this.role = role;
        this.objects = objects;
    }

}
