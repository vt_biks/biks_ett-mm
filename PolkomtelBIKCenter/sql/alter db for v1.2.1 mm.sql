﻿exec sp_check_version '1.2.1';
go

insert into bik_right_role (code, caption, rank) values ('Author', 'Autor', 60)
go


alter table bik_user_right
add tree_id int null default null references bik_tree (id)
go

exec sp_update_version '1.2.1', '1.2.1.1';
go
