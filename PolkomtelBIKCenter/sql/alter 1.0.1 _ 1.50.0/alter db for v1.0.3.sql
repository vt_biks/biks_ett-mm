exec sp_check_version '1.0.3';
go


----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------


create table bik_biz_def (
id int identity (1,1) primary key not null,
name varchar (255) not null
);
GO

create table bik_biz_def_body (
id int identity (1,1) primary key not null,
biz_def_id int references bik_biz_def (id) not null,
body varchar (max) null,
def_status int not null,
user_id int references bik_user (id) not null,
is_corpo int not null
);
GO

create table bik_department (
id int identity (1,1) primary key not null,
name varchar (255) not null
);
GO

create table bik_def_dependency (
id int identity (1,1) primary key not null,
def_body_id int references bik_biz_def_body (id) not null,
component_biz_def_id int references bik_biz_def (id) not null
constraint uk_bik_def_dependency unique (def_body_id, component_biz_def_id)
);
GO

create table bik_def_department (
id int identity (1,1) primary key not null,
department_id int references bik_department (id) not null,
def_body_id int references bik_biz_def_body (id) not null
constraint uk_bik_def_department unique (department_id, def_body_id)
);
GO


----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

-- po zako�czeniu prac nad wersj� v1.0.4 odkomentowa� poni�sze

exec sp_update_version '1.0.3', '1.0.4';
go
