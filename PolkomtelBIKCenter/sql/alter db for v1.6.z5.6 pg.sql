﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- Zmiany w powiadomieniach o nowych elementach do skategoryzowania
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_check_version '1.6.z5.6';
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_lisa_extradata]') and name = 'dictionary_table')
 begin

	alter table [dbo].[bik_lisa_extradata]
	add dictionary_table [varchar](256) not null constraint ble_dictionary_table_default default '' with values

	alter table [dbo].[bik_lisa_extradata]
	add dictionary_db [varchar](256) constraint ble_dictionary_db_default default '' with values

	alter table [dbo].[bik_lisa_extradata]
	add dictionary_table_physical [varchar](256) constraint ble_dictionary_table_physical_default default '' with values

	alter table [dbo].[bik_lisa_extradata]
	add dictionary_db_physical [varchar](256) constraint ble_dictionary_db_physical_default default '' with values
  end
go

if exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_lisa_extradata]') and name = 'dictionary_table')
 begin
	update ble 
		set
		ble.dictionary_table=(select lower(case when charindex('.',bn.obj_id)>0 then substring(bn.obj_id,charindex('.',bn.obj_id)+1,len(bn.obj_id)) else bn.obj_id end)),
		ble.dictionary_db = (select lower(case when charindex('.',bn.obj_id)>0 then substring(bn.obj_id, 1, charindex('.', bn.obj_id)-1 ) else null end))
	from [dbo].[bik_lisa_extradata] ble
	join bik_node bn on ble.node_id = bn.id 

	if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_lisa_extradata]') and name = N'idx_bik_lisa_extradata_dictionary')
		create nonclustered index [idx_bik_lisa_extradata_dictionary] on [dbo].[bik_lisa_extradata]
			(
				dictionary_table asc, dictionary_db asc
			)
	if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_lisa_extradata]') and name = N'idx_bik_lisa_extradata_dictionary_real')
		create nonclustered index [idx_bik_lisa_extradata_dictionary_real] on [dbo].[bik_lisa_extradata]
			(
				dictionary_table_physical asc, dictionary_db_physical asc
			)		
		
		
end
go

exec sp_update_version '1.6.z5.6', '1.6.z5.7';
go


