/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence.wuwu;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import simplelib.BaseUtils;
import simplelib.IRegExpMatcher;
import simplelib.IRegExpPattern;
import simplelib.Pair;
import simplelib.deferredstringconverter.IDeferredRegExprReplacer;

/**
 *
 * @author pmielanczuk
 */
public class DeferredConfluenceHrefReplacer implements IDeferredRegExprReplacer<IRegExpMatcher, Pair<String, String>> {

    public static final String REL_HREF_QUOTE_REGEXPR = "[ \\t\\n\\r]+(href)[ \\t\\n\\r]*=[ \\t\\n\\r]*[\"][ \\t\\n\\r]*/([^\"]*)[\"]";
    protected static final IRegExpPattern REL_HREF_PATT_Q = ConfluenceUtils.compileRegExpr(REL_HREF_QUOTE_REGEXPR);
    protected static final IRegExpPattern REL_HREF_PATT_A = ConfluenceUtils.compileRegExpr(REL_HREF_QUOTE_REGEXPR.replace("\"", "'"));
    public static final String REL_SRC_QUOTE_REGEXPR = "[ \\t\\n\\r]+(src)[ \\t\\n\\r]*=[ \\t\\n\\r]*[\"][ \\t\\n\\r]*/([^\"]*)[\"]";
    protected static final IRegExpPattern REL_SRC_PATT_Q = ConfluenceUtils.compileRegExpr(REL_SRC_QUOTE_REGEXPR);
    protected static final IRegExpPattern REL_SRC_PATT_A = ConfluenceUtils.compileRegExpr(REL_SRC_QUOTE_REGEXPR.replace("\"", "'"));
    protected static final IRegExpPattern REL_URL_PATT = ConfluenceUtils.compileRegExpr("[:][ \\t\\n\\r]*(url)[ \\t\\n\\r]*[(][ \\t\\n\\r]*/([^)]*)[)]");
    protected String absoluteUrlBase;

    public DeferredConfluenceHrefReplacer(String absoluteUrlBase) {
        this.absoluteUrlBase = absoluteUrlBase;
    }

    @Override
    public IRegExpMatcher[] makeMatchers(String s) {
        return new IRegExpMatcher[]{
            REL_HREF_PATT_Q.matcher(s),
            REL_HREF_PATT_A.matcher(s),
            REL_SRC_PATT_Q.matcher(s),
            REL_SRC_PATT_A.matcher(s),
            REL_URL_PATT.matcher(s)
        };
    }

    @Override
    public Pair<String, String> getDeferredReplacement(IRegExpMatcher m) {
        return new Pair<String, String>(m.group(1), m.group(2));
    }

    @Override
    public Collection<String> resolveReplacements(Collection<Pair<String, String>> replacements) {

        List<String> res = new ArrayList<String>(replacements.size());

        for (Pair<String, String> p : replacements) {
            String kind = p.v1;
            String url = p.v2;

            if (BaseUtils.safeEquals(kind, "url")) {
                res.add(": url(" + absoluteUrlBase + url + ")");
            } else {
                res.add(" " + kind + "=\"" + absoluteUrlBase + url + "\"");
            }
        }

        return res;
    }
}
