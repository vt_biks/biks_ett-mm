/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import java.util.Properties;
import pl.trzy0.foxy.commons.ITextProvider;
import simplelib.IGenericResourceProvider;

/**
 *
 * @author wezyr
 */
public class TextProviderFromProperties implements ITextProvider, IGenericResourceProvider<String> {

    public static enum TextProviderNoKeyDefaultModes {
        ReturnKeyAsText,
        ThrowException,
        ReturnDefValAsText
    }

    private Properties props;
    private TextProviderNoKeyDefaultModes defaultMode;
    private String defaultText;
    
    public TextProviderFromProperties(Properties props) {
        this(props, TextProviderNoKeyDefaultModes.ReturnKeyAsText);
    }

    public TextProviderFromProperties(Properties props, TextProviderNoKeyDefaultModes defaultMode) {
        this(props, defaultMode, null);
    }

    public TextProviderFromProperties(Properties props, String defText) {
        this(props, TextProviderNoKeyDefaultModes.ReturnDefValAsText, defText);
    }

    public TextProviderFromProperties(Properties props, TextProviderNoKeyDefaultModes defaultMode,
            String defText) {
        this.props = props;
        this.defaultMode = defaultMode;
        this.defaultText = defText;
    }

    public String getText(String key) {
        if (defaultMode == TextProviderNoKeyDefaultModes.ThrowException && !props.containsKey(key)) {
            throw new RuntimeException("no text for key: " + key);
        }

        String thisTimeDef = defaultMode == TextProviderNoKeyDefaultModes.ReturnKeyAsText ? key : defaultText;

        return props.getProperty(key, thisTimeDef);
    }

    public String gainResource(String resourceName) {
        return getText(resourceName);
    }

    public long getLastModifiedOfResource(String resourceName) {
        return 0;
    }
}
