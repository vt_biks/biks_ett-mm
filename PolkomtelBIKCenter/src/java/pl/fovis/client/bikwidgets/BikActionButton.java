/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.IContinuation;

/**
 *
 * @author mgraczkowski
 */
public class BikActionButton extends BikCustomButton {

    public BikActionButton(String text, final IContinuation click) {
        super(text, "RelatedObj-linkMediumBtn", click);
    }
}
