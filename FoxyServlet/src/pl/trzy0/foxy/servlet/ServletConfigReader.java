/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import javax.servlet.ServletContext;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;

/**
 *
 * @author wezyr
 */
public class ServletConfigReader extends FoxyAppUtils {

//    public static String getDeploymentLocationCode(ServletContext servletContext) {
//        String path = servletContext.getRealPath("/");
//        //
////        if (true) {
////            return "witzar-localhost";
////        }
//        if (path.startsWith("/usr/local/tomcat/")) {
//            return "nq.pl";
//        }
//        if (path.startsWith("/var/lib/tomcat")) {
//            return "zbyfin.myvnc.com";
//        }
//        if (path.startsWith("C:\\3zero\\") ||
//                path.startsWith("C:\\Program Files\\Apache Software Foundation\\Tomcat 6.0")) {
//            return "wwAtHome";
//        }
//        if (path.startsWith("/home/zbyfin")) {
//            return "hostIgnition";
//        }
//        throw new LameRuntimeException("no dlc for path: \"" + path + "\"");
//    }
    //private static final String configFileName = "/qna-config.cfg";
//    protected static InputStream tryGetResourceAsStream(List<String> paths) {
//
//        System.out.println("*** will try load config from paths: " + paths);
//
//        for (String path : paths) {
//            InputStream is = ServletConfigReader.class.getResourceAsStream(BaseUtils.ensureDirSepPrefix(path));
//
//            if (is != null) {
//                return is;
//            }
//        }
//
//        throw new LameRuntimeException("no file with paths: " + BaseUtils.mergeWithSepEx(paths, ", "));
//    }
//
//    protected static List<String> getConfigPaths(String configFileNameBase, String webAppRealName) {
//
////        InputStream is = ServletConfigReader.class.getResourceAsStream(fullConfigFileName);
////        boolean isReadFromFull = true;
////        if (is == null) {
////            configFileNameBase = BaseUtils.ensureDirSepPrefix(configFileNameBase);
////            is = ServletConfigReader.class.getResourceAsStream(configFileNameBase);
////            isReadFromFull = false;
////        }
////        if (is == null) {
////            throw new LameRuntimeException("error reading configuration file: " + configFileNameBase
////                    + " : " + fullConfigFileName);
////        }
//
//        List<String> paths = new ArrayList<String>();
//        String compName = LameUtils.removeNonCanonicSysChars(LameUtils.getSysComputerName());
//        String userName = LameUtils.removeNonCanonicSysChars(LameUtils.getSysUserName());
//        if (!BaseUtils.isStrEmptyOrWhiteSpace(webAppRealName)) {
//            if (!BaseUtils.isStrEmptyOrWhiteSpace(compName)
//                    && !BaseUtils.isStrEmptyOrWhiteSpace(userName)) {
//                paths.add(compName + "-" + userName + "-" + webAppRealName + "-" + configFileNameBase);
//            }
//            if (!BaseUtils.isStrEmptyOrWhiteSpace(userName)) {
//                paths.add(userName + "-" + webAppRealName + "-" + configFileNameBase);
//            }
//            if (!BaseUtils.isStrEmptyOrWhiteSpace(compName)) {
//                paths.add(compName + "-" + webAppRealName + "-" + configFileNameBase);
//            }
//        }
//        if (!BaseUtils.isStrEmptyOrWhiteSpace(compName)
//                && !BaseUtils.isStrEmptyOrWhiteSpace(userName)) {
//            paths.add(compName + "-" + userName + "-" + configFileNameBase);
//        }
//        if (!BaseUtils.isStrEmptyOrWhiteSpace(userName)) {
//            paths.add(userName + "-" + configFileNameBase);
//        }
//        if (!BaseUtils.isStrEmptyOrWhiteSpace(compName)) {
//            paths.add(compName + "-" + configFileNameBase);
//        }
//
//        if (!BaseUtils.isStrEmptyOrWhiteSpace(webAppRealName)) {
//            paths.add(webAppRealName + "-" + configFileNameBase);
//        }
//
//        paths.add(configFileNameBase);
//
//        return paths;
//    }
//
//    public static <T/* extends ServletConfigObj*/> T readConfigObj(
//            ServletContext servletContext, Class<T> c,
//            String configFileNameBase //String configObjectBasePath
//            ) {
////        String dlc = getDeploymentLocationCode(servletContext);
////        InputStream is = ServletConfigReader.class.getResourceAsStream(configObjectBasePath + dlc + ".cfg");
//
//        //System.out.println("^^^ readConfigObj: classpath=" + System.getProperty("java.class.path") + " ^^^");
//
//        String webAppRealName = FoxyServletUtils.getWebAppRealName(servletContext);
//
//        List<String> paths = getConfigPaths(configFileNameBase, webAppRealName);
//
//        InputStream is = tryGetResourceAsStream(paths);
//
//        //System.out.println("before LameUtils.loadAsString");
//        String confStr = LameUtils.loadAsString(is, "Cp1250");
//
////        System.out.println("^^^ config file read ^^^\n  fullPath=" + fullConfigFileName
////                + "\n  basePath=" + configFileNameBase
////                + "\n  isFromFull=" + isReadFromFull
////                + "\n  webAppRealName=" + webAppRealName
////                + "\n--- contents:\n" + confStr);
//
//        //System.out.println("after LameUtils.loadAsString confStr=" + confStr);
//        Map<String, Object> map = JsonReader.readMap(confStr);
//        return MuppetMaker.newMuppet(c, map);
//    }
    public static <T> T readConfigObj(ServletContext servletContext, Class<T> c, String appName) {
        String webAppRealName = FoxyServletUtils.getWebAppRealName(servletContext);
        return readConfigObj(appName, c, webAppRealName);
    }

    public static void main(String[] args) {
//        String compName = LameUtils.removeNonCanonicSysChars("mońa-pc_ex2?");// LameUtils.getComputerName();
//        compName = LameUtils.deAccentExAndLowerCase(compName);
//        compName = compName.replaceAll("[^A-Za-z0-9\\-._]+", "");
//        System.out.println("compName=" + compName);
//        System.out.println("userName=" + LameUtils.removeNonCanonicSysChars(LameUtils.getSysUserName()));
        System.out.println("paths: " + getConfigPaths("nazwa-aplikacji", "nazwa-web-app"));
    }
}
