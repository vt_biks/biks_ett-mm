package pl.fovis.foxygwtcommons;

import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author pkobik
 * @param <T>
 */
public interface IAdminConfigBaseWidget<T> {
    
    public Widget buildWidget();

    public void populateWidgetWithData(T data);

    public String getSourceName();
}
