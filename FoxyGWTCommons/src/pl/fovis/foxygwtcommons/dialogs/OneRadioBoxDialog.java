/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class OneRadioBoxDialog extends BaseActionOrCancelDialog {

    private IParametrizedContinuation<String> saveCont;
    protected List<RadioButton> buttons = new ArrayList<RadioButton>();
    protected List<String> possibleValues;
    protected String name;

    @Override
    protected String getDialogCaption() {
        return name;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.wybierz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        buttons.clear();
        if (possibleValues == null || possibleValues.isEmpty()) {
            main.add(new Label(I18n.bRAKWARTOSCI.get() /* I18N:  */ + "!"));
        } else {
            int i = 0;
            for (String value : possibleValues) {
                RadioButton radioButton = new RadioButton("GROUP", value);
                radioButton.setValue(i == 0);
                buttons.add(radioButton);
                main.add(radioButton);
                i++;
            }
        }
        main.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
    }

    @Override
    protected void doAction() {
        for (RadioButton btn : buttons) {
            if (btn.getValue()) {
                saveCont.doIt(btn.getText());
                break;
            }
        }
    }

    public void buildAndShowDialog(String name, List<String> initValue, IParametrizedContinuation<String> saveCont) {
        this.name = name;
        this.possibleValues = initValue;
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }
}
