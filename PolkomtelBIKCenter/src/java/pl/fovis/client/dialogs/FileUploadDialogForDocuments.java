/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.ListBox;
import java.util.Arrays;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.FoxyFileUpload;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author administrator
 */
public class FileUploadDialogForDocuments extends BaseActionOrCancelDialog {

    private IParametrizedContinuation<Pair<String, FoxyFileUpload>> saveCont;
    private FoxyFileUpload ffu;
    private ListBox listBox;
    protected List<NodeKindBean> documentNodeKinds;

    public FileUploadDialogForDocuments(List<NodeKindBean> documentNodeKinds) {
        this.documentNodeKinds = documentNodeKinds;
    }

    @Override
    protected String getDialogCaption() {
        return I18n.dodajDokument.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        //return null;
        return I18n.wyslij.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        listBox = new ListBox();
        for (NodeKindBean documentNodeKind : documentNodeKinds) {
            listBox.addItem(documentNodeKind.caption, documentNodeKind.code);
        }
        ffu = new FoxyFileUpload(new IContinuation() {
            public void doIt() {
                if (ffu.isUploadIsAllowed()) {
                    String selectedNodeCode = listBox.getValue(listBox.getSelectedIndex());
                    saveCont.doIt(new Pair<String, FoxyFileUpload>(selectedNodeCode, ffu));
                } else{
                    Window.alert(I18n.wrongFileExtension.get() /* I18N:  */);
                }
                hideDialog();
            }
        }, false, Arrays.asList(BIKClientSingletons.allowedFileExtensionsList()));
        main.add(ffu);
        FlexTable grid = new FlexTable();
        grid.setStyleName("gridJoinedObj");
        grid.setWidth("100%");
        grid.setText(0, 0, I18n.wybierzTypDokumentu.get());
        grid.setWidget(0, 1, listBox);
        main.add(grid);
        grid.setVisible(false);
        if (listBox.getItemCount() > 1) {
            grid.setVisible(true);
        }
    }
    
    public void buildAndShowDialog(IParametrizedContinuation<Pair<String, FoxyFileUpload>> saveCont) {
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }

    @Override
    protected void doAction() {
        //no-op
    }

    @Override
    protected boolean doActionBeforeClose() {
        ffu.performUpload();

        return false;
    }
}
