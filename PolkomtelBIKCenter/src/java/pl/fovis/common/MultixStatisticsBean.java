/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author lbiegniewski
 */
public class MultixStatisticsBean implements Serializable {

    public String dbName;
    public int totalInvited;
    public Date lastActivityDate;
    public Integer totalLogged;
}
