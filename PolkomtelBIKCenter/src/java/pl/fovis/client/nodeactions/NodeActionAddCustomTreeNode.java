/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.TreeNodeDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class NodeActionAddCustomTreeNode extends NodeActionUpdatingBase {

    protected NodeKindBean nodeKindBean;

    public NodeActionAddCustomTreeNode(NodeKindBean nodeKindBean) {
        this.nodeKindBean = nodeKindBean;
    }

//    public NodeActionAddCustomTreeNode() {
//    }
    @Override
    public boolean isEnabledByDesign() {
        return !isMetaBiks() && !isTreeReadOnly() && !isNodeLinkedAnyhow() && isCustomTreeMode();
    }

    @Override
    public String getCaption() {
        return I18n.dodaj.get() + " " + nodeKindBean.caption;
    }

    @Override
    public void execute() {
        if (!isSocialUserLoggedInWithInfo()) {
            return;
        }
        if (nodeKind.treeKind.equals(BIKConstants.TREE_KIND_META_BIKS) || nodeKind.treeKind.equals(BIKConstants.TREE_KIND_META_BIKS_MENU_CONF)) {
            BIKClientSingletons.getService().getRequiredAttrs(nodeKindBean.id, new StandardAsyncCallback<List<AttributeBean>>() {
                @Override
                public void onSuccess(List<AttributeBean> result) {
                    showDialog(result);
                }
            });
        } else {
            BIKClientSingletons.getService().getRequiredAttrsWithVisOrder(nodeKindBean.id, new StandardAsyncCallback<List<AttributeBean>>() {
                @Override
                public void onSuccess(List<AttributeBean> result) {
                    showDialog(result);
                }
            });
        }
//        new OneTextBoxDialog().buildAndShowDialog(I18n.podajNazwe.get(), "", new IParametrizedContinuation<String>() {
//
//            @Override
//            public void doIt(String param) {
//                BIKClientSingletons.getService().createTreeNode(node.id, nodeKindBean.id, param, null, node.treeId, null,
//                        new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on createTreeNode") {
//
//                            @Override
//                            public void onSuccess(Pair<Integer, List<String>> result) {
//                                BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get());
//                                refreshRedisplayDataAndSelectRecord(result.v1);
//                            }
//                        });
//            }
//        });
//        new OneTextBoxDialog().buildAndShowDialog(I18n.podajNazwe.get(), "", new IParametrizedContinuation<String>() {
//
//            @Override
//            public void doIt(String param) {
//                BIKClientSingletons.getService().createTreeNode(node.id, nodeKindBean.id, param, null, node.treeId, null,
//                        new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on createTreeNode") {
//
//                            @Override
//                            public void onSuccess(Pair<Integer, List<String>> result) {
//                                BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get());
//                                refreshRedisplayDataAndSelectRecord(result.v1);
//                            }
//                        });
//            }
//        });
    }

    public void showDialog(List<AttributeBean> result) {
        new TreeNodeDialog().buildAndShowDialog(nodeKindBean.id, nodeKindBean.code, null,
                node, treeBean.id, null, false, result, new IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>>() {
                    @Override
                    public void doIt(final Pair<TreeNodeBean, Map<String, AttributeBean>> param2) {
                        TreeNodeBean param = param2.v1;
                        BIKClientSingletons.getService().createTreeNodeWithAttr(param.parentNodeId,
                                param.nodeKindId, param.name, param.objId, param.treeId, param.linkedNodeId, param.addingJoinedObjs,
                                param2.v2,
                                new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on" /* I18N: no */ + " createTreeNode") {
                                    @Override
                                    public void onSuccess(Pair<Integer, List<String>> result) {
                                        BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get() /* I18N:  */);
                                        refreshRedisplayDataAndSelectRecord(result.v1);
                                    }
                                });
                    }
                });
    }

    @Override
    public String getOptCurrentNodeActionCode() {
        return super.getOptCurrentNodeActionCode();// + ":" + nodeKindBean.code;
    }

}
