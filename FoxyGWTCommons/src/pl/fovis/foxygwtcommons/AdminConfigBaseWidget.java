/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 *
 * @author tflorczak
 */
public abstract class AdminConfigBaseWidget<T> implements IAdminConfigBaseWidget<T> {

    public static final DateTimeFormat DEFAULT_DATE_FORMAT = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_SHORT);

    protected CellPanel main = new VerticalPanel();
    protected List<HasEnabled> widgetList = new ArrayList<HasEnabled>();

    protected void addToWidgetList(FocusWidget... wid) {
        widgetList.addAll(Arrays.asList(wid));
    }

    protected void setWidgetsEnabled(boolean val) {
        for (HasEnabled widget : widgetList) {
            widget.setEnabled(val);
        }
    }

    public static Label createLabel(String name, CellPanel cp) {
        Label label = new Label(name);
        cp.add(label);
        return label;
    }

    protected Label createLabel(String name) {
        return createLabel(name, main);
    }

    protected ListBox createListBoxEx(Set<String> list, CellPanel cp) {
        ListBox lbx = new ListBox();
        if (list != null) {
            for (String element : list) {
                lbx.addItem(element);
            }
            lbx.setSelectedIndex(0);
        }
        cp.add(lbx);
        widgetList.add(lbx);
        return lbx;
    }

    protected ListBox createListBox(Set<String> list) {
        return createListBoxEx(list, main);
    }

    protected TextBox createTextBox() {
        return createTextBox(main);
    }

    protected RadioButton createRadioButton(String groupName, String html) {
        return createRadioButton(groupName, html, main);
    }

    protected TextArea createTextArea() {
        return createTextArea(main);
    }

    protected TextBox createTextBox(CellPanel cp) {
        return createTextBoxEx(false, cp, true);
    }

    protected RadioButton createRadioButton(String groupName, String html, CellPanel cp) {
        return createRadioButtonEx(groupName, html, cp, true);
    }

    protected TextArea createTextArea(CellPanel cp) {
        return createTextAreaEx(false, cp, true);
    }

    protected TextBox createTextBox(CellPanel cp, boolean addToWidgetList) {
        return createTextBoxEx(false, cp, addToWidgetList);
    }

    protected TextBox createPasswordTextBox() {
        return createPasswordTextBox(main);
    }

    protected TextBox createPasswordTextBox(CellPanel cp) {
        return createTextBoxEx(true, cp, true);
    }

    protected TextBox createPasswordTextBox(CellPanel cp, boolean addToWidgetList) {
        return createTextBoxEx(true, cp, addToWidgetList);
    }

    private TextBox createTextBoxEx(boolean isPassword, CellPanel cp, boolean addToWidgetList) {
        TextBox textBox = new TextBox();
        textBox.setWidth("490px");
        if (isPassword) {
            textBox.getElement().setAttribute("type" /* I18N: no */, "password" /* I18N: no */);
        }
        cp.add(textBox);
        if (addToWidgetList) {
            widgetList.add(textBox);
        }
        return textBox;
    }

    private RadioButton createRadioButtonEx(String groupName, String html, CellPanel cp, boolean addToWidgetList) {
        RadioButton bnt = new RadioButton(groupName, html, true);
        bnt.setWidth("490px");
        cp.add(bnt);
        if (addToWidgetList) {
            widgetList.add(bnt);
        }
        return bnt;
    }

    private TextArea createTextAreaEx(boolean isPassword, CellPanel cp, boolean addToWidgetList) {
        TextArea textArea = new TextArea();
        textArea.setWidth("490px");
        textArea.setHeight("300px");
        if (isPassword) {
            textArea.getElement().setAttribute("type" /* I18N: no */, "password" /* I18N: no */);
        }
        cp.add(textArea);
        if (addToWidgetList) {
            widgetList.add(textArea);
        }
        return textArea;
    }

    protected CheckBox createCheckBox(String text, ValueChangeHandler<Boolean> valueChanger, boolean addToWidgetList, CellPanel cp) {
        CheckBox checkBox = new CheckBox(text);
        checkBox.setValue(true);
        if (valueChanger != null) {
            checkBox.addValueChangeHandler(valueChanger);
        }
        cp.add(checkBox);
        if (addToWidgetList) {
            widgetList.add(checkBox);
        }
        return checkBox;
    }

    protected CheckBox createCheckBox(String text, ValueChangeHandler<Boolean> valueChanger, boolean addToWidgetList) {
        return createCheckBox(text, valueChanger, addToWidgetList, main);
    }

    protected CheckBox createAndAddCheckBox(String text, ValueChangeHandler<Boolean> valueChanger) {
        return createCheckBox(text, valueChanger, true);
    }

    protected CheckBox createAndAddCheckBox(String text, ValueChangeHandler<Boolean> valueChanger, CellPanel cp) {
        return createCheckBox(text, valueChanger, true, cp);
    }

    protected CheckBox createAndAddCheckBoxWithOutHandler(String text) {
        return createCheckBox(text, null, true);
    }

    protected CheckBox createAndAddCheckBoxWithOutHandler(String text, CellPanel cp) {
        return createCheckBox(text, null, true, cp);
    }

    protected DateBox createDateBox(CellPanel cp) {
        DateBox dateBox = new DateBox();
//        dateBox.setWidth("490px");
        dateBox.setFormat(new DateBox.DefaultFormat(DEFAULT_DATE_FORMAT));
//        dateBox.getDatePicker().setYearArrowsVisible(true);
        cp.add(dateBox);
        widgetList.add(dateBox);
        return dateBox;
    }

    protected DateBox createDateBox() {
        return createDateBox(main);
    }

    public static PushButton createButton(String name, ClickHandler clickHandler, CellPanel optCp) {
        PushButton button = new PushButton(name);
        NewLookUtils.makeCustomPushButton(button);
        button.addClickHandler(clickHandler);
        if (optCp != null) {
            optCp.add(button);
        }
        return button;
    }

//    protected PushButton createButton(String name, ClickHandler clickHandler, FlexTable table) {
//        PushButton button = new PushButton(name);
//        NewLookUtils.makeCustomPushButton(button);
//        button.addClickHandler(clickHandler);
//        return button;
//    }
    protected PushButton createButton(String name, ClickHandler clickHandler) {
        return createButton(name, clickHandler, main);
    }

    public static void createLine(CellPanel cp) {
        createLine(cp, "500");
    }

    public static void createLine(CellPanel cp, String size) {
        cp.add(new HTML("<HR WIDTH=\"" + size + "\" ALIGN=LEFT>"));
    }

    protected void createLine() {
        createLine(main);
    }

    @Override
    public Widget buildWidget() {
        main.setSpacing(4);
        main.addStyleName("mainConnConf");
        buildInnerWidget();
        return main;
    }

    @Override
    public String getSourceName() {
        return null;//nigdy jest uzywane
    }

    protected abstract void buildInnerWidget();
//    public abstract void populateWidgetWithData(T data);
//    public abstract String getSourceName();
}
