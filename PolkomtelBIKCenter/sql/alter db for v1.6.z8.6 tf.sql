﻿exec sp_check_version '1.6.z8.6';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_tree]') and name = N'uq_bik_tree_code')
begin
	create unique index uq_bik_tree_code on bik_tree (code);
end;
go

-- poprzednia wersja w pliku: alter db for v1.6.z7 tf.sql
-- fix na database filter
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_fix_databases_nodes_after_deactivate]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_fix_databases_nodes_after_deactivate]
go

create procedure [dbo].[sp_fix_databases_nodes_after_deactivate](@dbServerId int)
as
begin
	declare @servName varchar(255) = (select name from bik_db_server where id = @dbServerId);
	declare @sourceId int = (select source_id from bik_db_server where id = @dbServerId);
	declare @treeId int = (select tree_id from bik_db_server where id = @dbServerId);
	declare @activeDBs varchar(255) = '';
	declare @branchsToDelete table (branch varchar(900));

	if(charindex('#', @servName) <> 0)
	begin
		set @servName = substring(@servName, 1, charindex('#', @servName) - 1);
	end;

	select @activeDBs = @activeDBs + ',' + case when database_filter is not null and database_filter <> '' then database_filter when database_list is not null and  database_list <> '' then database_list else '' end
	from bik_db_server as ser
	where source_id = @sourceId and is_active = 1 
	and tree_id = @treeId and (name = @servName or name like @servName + '#%')

	insert into @branchsToDelete(branch)
	select branch_ids 
	from bik_node 
	where parent_node_id in (
		select id from bik_node 
		where tree_id = @treeId 
		and is_deleted = 0 
		and obj_id = @servName + '|')
	and is_deleted = 0
	and name not in (select str 
		from (select * from dbo.fn_split_by_sep(@activeDBs, ',',3 )) x 
		where str <> '')
		
		
	declare @branch varchar(900);	
	declare db_del_cur cursor for select branch from @branchsToDelete
	open db_del_cur
	fetch next from db_del_cur into @branch
	while @@fetch_status = 0
		begin 
		update bik_node 
		set is_deleted = 1
		where tree_id = @treeId
		and branch_ids like @branch + '%'
		fetch next from db_del_cur into @branch
		end
	close db_del_cur;
	deallocate db_del_cur;
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_dqm_schedule]') and name = 'multi_source_id')
begin
	alter table bik_dqm_schedule add multi_source_id int null;
end;
go

if not exists (select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
where CONSTRAINT_TYPE = 'PRIMARY KEY' AND TABLE_NAME = 'bik_dqm_test_multi' 
AND TABLE_SCHEMA ='dbo')
begin
	alter table bik_dqm_test_multi add primary key (id);
end
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_dqm_test_multi]') and name = 'biks_table_name')
begin
	alter table bik_dqm_test_multi add biks_table_name varchar(512) null;
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_dqm_log]') and name = 'multi_source_id')
begin
	alter table bik_dqm_log add multi_source_id int null;
end;
go

if not exists (select * from bik_app_prop where name = 'typeMapping_MSSQL')
begin
	insert into bik_app_prop (name, val) values ('typeMapping_MSSQL', '*=%{typeName}%
	DECIMAL=decimal(%{prec}%, %{scale}%)
	VARCHAR=varchar(max)
	NVARCHAR=varchar(max)
	NCHAR=varchar(max)
	CHAR=varchar(max)');
end;
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

exec sp_update_version '1.6.z8.6', '1.6.z8.7';
go
