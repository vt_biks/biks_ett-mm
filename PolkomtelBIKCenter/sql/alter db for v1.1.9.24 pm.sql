exec sp_check_version '1.1.9.24';
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

-- poprawka dla [TSB-FIX:9]
-- naprawiamy drzewo i typ w�z�a dla wyszukiwarki
update bik_searchable_attr_val
set tree_id = n.tree_id, node_kind_id = n.node_kind_id
from bik_node n
where bik_searchable_attr_val.node_id = n.id
and (bik_searchable_attr_val.tree_id <> n.tree_id or bik_searchable_attr_val.node_kind_id <> n.node_kind_id)
go


------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

-- rozwi�zanie dla problemu [TSB-FIX:10]
-- usuwamy z wyszukiwarki usuni�te w�z�y
delete from bik_searchable_attr_val
from bik_node n
where bik_searchable_attr_val.node_id = n.id and n.is_deleted = 1
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

-- poprawka dla [TSB-FIX:13]
-- usuwamy z tabeli do filtrowania drzewek zb�dne w�z�y (usuni�te)
delete from bik_node_name_chunk
from bik_node n
where bik_node_name_chunk.node_id = n.id and n.is_deleted = 1
go


------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_update_version '1.1.9.24', '1.1.9.25';
go
