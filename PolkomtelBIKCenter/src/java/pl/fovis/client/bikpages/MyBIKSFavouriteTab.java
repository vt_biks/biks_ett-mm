/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.IPaginatedItemsDisplayer;
import pl.fovis.client.bikwidgets.MyBIKSFavouriteGrid;
import pl.fovis.client.bikwidgets.MyBIKSSuggestedElementsGrid;
import pl.fovis.common.FvsChangeExBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.LameBag;

/**
 *
 * @author acichy
 */
public class MyBIKSFavouriteTab extends MyBIKSTabWithButtonBase<FvsChangeExBean>/* implements IMyBIKSTab */ {

    MyBIKSFavouriteGrid paginatorFvs = new MyBIKSFavouriteGrid();

    @Override
    public String getCaption() {
        return I18n.ulubione.get() /* I18N:  */;
    }

    @Override
    public String getButtonStyle() {
        return "labelLink-Fav";
    }

    protected IPaginatedItemsDisplayer<FvsChangeExBean> createPaginatedItemsDisplayer() {
        return paginatorFvs;
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<FvsChangeExBean>> callback) {
        if (BIKClientSingletons.isUserLoggedIn()) {
            BIKClientSingletons.getService().getFavouritesExForUser(callback);
        }
    }

    protected String getOptHeaderBtnCaption() {
        return I18n.oznaczWszystkiElementyJakoPrzecz.get() /* I18N:  */;
    }

    protected String getHeaderBtnStyle() {
        return "Ignore" /* I18N: no */ + "-IgnoreAll";
    }

    @Override
    protected void headerButtonClicked() {
        BIKClientSingletons.getService().deleteAllChangedObjectForUser(new StandardAsyncCallback<Void>() {

            public void onSuccess(Void result) {
                BIKClientSingletons.showInfo(I18n.oznaczonoElementyJakoPrzeczytane.get() /* I18N:  */);
                //myBIKSPage.refreshTabButton(MyBIKSFavouriteTab.this);
                fetchAndDisplayData();
                refreshTabButton();
                //show(rootContainer);
                //getBeansList();
            }
        });
    }

    @Override
    protected boolean isHeaderBtnEnabled(boolean forEmptyFilteredItems) {
        return !forEmptyFilteredItems && BIKClientSingletons.isUserLoggedIn();
    }

    @Override
    protected String getCaptionWhenListEmpty() {
        return I18n.brakUlubionych.get() /* I18N:  */;
    }

    @Override
    protected List<FvsChangeExBean> filterAllItems() {
        List<FvsChangeExBean> res = new ArrayList<FvsChangeExBean>();
        for (FvsChangeExBean nb : allItems) {
            if (paginatorFvs.getObjecChangeCnt(nb) > 0) {
                res.add(nb);
            }
        }
        return res;
    }

    @Override
    protected List<FvsChangeExBean> filteredItemsByKind(List<FvsChangeExBean> items) {
        prepareItemFiltering();
        final List<FvsChangeExBean> fvs = new ArrayList<FvsChangeExBean>();

        for (FvsChangeExBean fc : items) {
            if (shouldShowItem(fc)) {
                fvs.add(fc);
            }
        }
        return fvs;
    }

    protected String getFilterCheckBoxCaption() {
        return I18n.pokazTylkoZmodyfikowaneUlubione.get() /* I18N:  */;
    }

    private HorizontalPanel filters;
    private FlowPanel checkBoxContainer;
    protected List<CheckBox> checkboxes = new ArrayList<CheckBox>();
    protected Set<String> activeKinds;

    protected void addCheckbox(String name, String caption, boolean active, final IContinuation contWhenChanged) {
        final CheckBox cb = new CheckBox(caption, true);
        cb.setStyleName("Filters-Checkbox");
        cb.setValue(active);
        cb.setName(name);
        //tymczasowa szerokość wstawiona tutaj.
        //cb.setWidth("155px");
        cb.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent ce) {
                contWhenChanged.doIt();
            }
        });
        checkboxes.add(cb);
    }

    protected void finalizeCheckboxes() {
        checkBoxContainer.clear();
        for (CheckBox cb : checkboxes) {
            checkBoxContainer.add(cb);
        }
    }

    protected int getFilterCnt() {
        return checkboxes.size();
    }

    protected boolean isFilterOn(int idx) {
        return checkboxes.get(idx).getValue();
    }

    protected String getFilterName(int idx) {
        return checkboxes.get(idx).getName();
    }

    protected void setFilterActive(int idx, boolean active) {
        checkboxes.get(idx).setValue(active);
    }

    protected void clearCheckboxes() {
        checkboxes.clear();
    }

    protected void recreateFilterCheckboxes(List<FvsChangeExBean> allItems) {

        LameBag<String> counts = new LameBag<String>();
        List<FvsChangeExBean> fvs = allItems;//

        if (fvs != null) {
            for (FvsChangeExBean fc : fvs) {
                counts.add(fc.code);
            }
        }
        clearCheckboxes();
        addCheckbox("*", "<b>" + I18n.wszystko.get() /* I18N:  */ + "</b> (" + BaseUtils.collectionSizeFix(fvs) + ")",
                true, new IContinuation() {
                    public void doIt() {
                        boolean check = isFilterOn(0);
                        for (int i = 1; i < getFilterCnt(); i++) {
                            setFilterActive(i, check);
                        }
                        displayData();
                    }
                });

        for (String kind : counts.uniqueSet()) {

            addCheckbox(kind,
                    BIKClientSingletons.getHtmlForNodeKindIconWithText(kind, null, BIKClientSingletons.getNodeKindCaptionByCode(kind) + " (" + counts.getCount(kind) + ")"), true,
                    new IContinuation() {
                        public void doIt() {
                            displayData();
                        }
                    });
        }

        finalizeCheckboxes();
    }

    @Override
    protected void addOptionalFilterByKindWidgets(List<FvsChangeExBean> allItems) {
        optionalFilterByKindWidgets.clear();
        if (BaseUtils.collectionSizeFix(allItems) > 0) {
            VerticalPanel vp = new VerticalPanel();
            filters = new HorizontalPanel();
            filters.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
            HTML lblFilters = new HTML(I18n.pokazTypyObiektow.get() /* I18N:  */ + ":");
            lblFilters.setStyleName("FvsKind");
//            filters.add(lblFilters);
            filters.setWidth("100%");
            GWTUtils.setStyleAttribute(filters, "minHeight", "25px");
//            filters.setCellWidth(lblFilters, "180px");
            checkBoxContainer = new FlowPanel();
            filters.add(lblFilters);
            filters.add(checkBoxContainer);
//            vp.add(lblFilters);
            vp.add(filters);
            vp.setStyleName("Fvs-filters");
            recreateFilterCheckboxes(allItems);
            optionalFilterByKindWidgets.add(vp);
        }
    }

    protected boolean shouldShowItem(FvsChangeExBean e) {
        return activeKinds.contains(e.code);
    }

    protected void prepareItemFiltering() {
        boolean allActive = true;
        activeKinds = new HashSet<String>();
        for (int i = 1; i < getFilterCnt(); i++) {
            if (isFilterOn(i)) {
                activeKinds.add(getFilterName(i));
            } else {
                allActive = false;
            }
        }
        if (getFilterCnt() > 0) {
            setFilterActive(0, allActive);
        }
    }

    MyBIKSSuggestedElementsGrid suggestedElementsGrid;

    @Override
    protected VerticalPanel createOptSuggestedElementsWidget() {

        VerticalPanel seVp = new VerticalPanel();
        seVp.setWidth("100%");

        HTML caption = new HTML(I18n.sugerowaneElementy.get());
        caption.setStyleName("MyBIKS-HTML");

        HorizontalPanel captionHp = new HorizontalPanel();
        captionHp.add(caption);
        captionHp.setStyleName("Suggested-caption");

        HTML suggestedElementHtml = new HTML(I18n.sugerowaneElementyPytanie.get());
        suggestedElementHtml.setStyleName("SuggestedElement");

        suggestedElementsGrid = new MyBIKSSuggestedElementsGrid(new IContinuation() {

            @Override
            public void doIt() {
                fetchAndDisplayData();
                refreshSuggestedElementsGrid();
            }
        });

        seVp.add(captionHp);
        seVp.add(suggestedElementHtml);
        seVp.add(suggestedElementsGrid.buildGrid());
        return seVp;
    }

    protected void refreshSuggestedElementsGrid() {
        suggestedElementsGrid.buildGrid();
    }
}
