﻿exec sp_check_version '1.0.86.2';
go
--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------
 
insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values('CategoriesOfQuestions', 'Kategoria pytania', 'folder', 'Q&A' , 1) 
go
--------------------------------------------------------------
--------------------------------------------------------------
declare @id int;
select @id = id
from bik_node_kind
where code = 'CategoriesOfQuestions'
 
insert into bik_tree(name, node_kind_id, code, tree_kind)
values('Kategorie pytań', @id, 'Q&A', 'Q&A')
go
--------------------------------------------------------------
--------------------------------------------------------------
create table bik_question_and_answer(
	id int identity(1,1) not null primary key,
	question_text varchar(max) not null,
	answer_text varchar(max) not null,
	node_id int not null references bik_node(id)
)
go
--------------------------------------------------------------
--------------------------------------------------------------
declare @node_kind_id int;
declare @tree_id int;

select @node_kind_id = id
from bik_node_kind
where code = 'CategoriesOfQuestions';

select @tree_id = id
from bik_tree
where code = 'Q&A';

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(NULL, @node_kind_id, 'My BIK', @tree_id);

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(NULL, @node_kind_id, 'Glosariusz', @tree_id);

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(NULL, @node_kind_id, 'Metadane', @tree_id);

declare @parent_node_id int;
select @parent_node_id = scope_identity()

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(@parent_node_id, @node_kind_id, 'Raporty', @tree_id);

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(@parent_node_id, @node_kind_id, 'Światy obiektów', @tree_id);

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(@parent_node_id, @node_kind_id, 'Połączenia', @tree_id);

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(NULL, @node_kind_id, 'Baza wiedzy', @tree_id);

select @parent_node_id = scope_identity()

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(@parent_node_id, @node_kind_id, 'Struktura organizacyjna', @tree_id);

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(@parent_node_id, @node_kind_id, 'Systemy IT', @tree_id);

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(NULL, @node_kind_id, 'Dokumenty', @tree_id);

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(NULL, @node_kind_id, 'Blogi', @tree_id);

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(NULL, @node_kind_id, 'Użytkownicy', @tree_id);

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(NULL, @node_kind_id, 'Admin', @tree_id);

select @parent_node_id = scope_identity()

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(@parent_node_id, @node_kind_id, 'Zasilania', @tree_id);

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(@parent_node_id, @node_kind_id, 'Szczegóły połączenia', @tree_id);

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(@parent_node_id, @node_kind_id, 'Słowniki', @tree_id);

insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
values(NULL, @node_kind_id, 'Szukaj', @tree_id);

exec sp_node_init_branch_id @tree_id, null
go
--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------
exec sp_update_version '1.0.86.2', '1.0.86.3';
go
