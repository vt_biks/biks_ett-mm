/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.ArticleBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.Pair;

/**
 *
 * @author beata
 */
public class NodeActionEditDefinition extends NodeActionAddOrPromoteDefBase {

    @Override
    protected String getActionSuccessMsg() {
        return I18n.zmodyfikowanoDefinicje.get() /* I18N:  */;
    }

    @Override
    protected void callService(ArticleBean art, int action, AsyncCallback<Pair<Integer, List<String>>> refreshAfterAction) {
        BIKClientSingletons.getService().createTreeNodeBiz(node, art,
                action, getNodeKindCode(), refreshAfterAction);
    }

    @Override
    public boolean isEnabledByDesign() {
        return (isNodeNotLinkedAnyhowNorReadOnlyTreeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_GLOSSARY)
                || isNodeNotLinkedAnyhowNorReadOnlyTreeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_GLOSSARY_NOT_CORPO)
                || isNodeNotLinkedAnyhowNorReadOnlyTreeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_GLOSSARY_VERSION))
                && isBikUserLoggedIn();

    }

    @Override
    public String getCaption() {
        return I18n.edytujDefinicje.get() /* I18N:  */;
    }

    @Override
    protected String getSubject() {
        return node.name;
    }

    @Override
    protected ArticleBean getBody() {
        return data.thisBizDef;
    }

    @Override
    protected boolean isNewDefinition() {
        return false;
    }
}
