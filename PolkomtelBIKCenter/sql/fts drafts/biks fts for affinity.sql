drop table bik_fts_word;
go

create table bik_fts_word (
  id int not null identity primary key, 
  word varchar(800) not null unique/*,
  origin_flag int not null,
  occurence_cnt int not null,
  obj_cnt int not null,
  max_weight decimal(9,8) not null*/
);
go


/*
  drop table bik_fts_word_in_obj;
  go

  create table bik_fts_word_in_obj (
    id int not null identity primary key, 
    obj_id int not null, 
    word_id int not null, 
    origin_flag int not null,
    occurence_cnt int not null,
    attr_cnt int not null,
    max_weight decimal(9,8) not null,
    unique (obj_id, word_id)
  );
  go
*/

drop table bik_fts_attr;
go

create table bik_fts_attr (
  id int not null identity primary key, 
  name varchar(800) not null unique,
  weight decimal(9,8) not null
);
go


drop table bik_fts_word_in_obj_attr;
go

create table bik_fts_word_in_obj_attr (
  id int not null identity primary key, 
  obj_id int not null, 
  attr_id int not null,
  word_id int not null,
  original_cnt int not null,
  synonym_cnt int not null,
  dictstemmed_cnt int not null,
  algostemmed_cnt int not null,
  accentfree_cnt int not null          
  /*, 
  origin_flag int not null,
  occurence_cnt int not null,
  max_weight decimal(9,8) not null*/,
  unique (obj_id, attr_id, word_id)
);
go


drop table bik_fts_processed_obj;
go

create table bik_fts_processed_obj (id int not null primary key);
go


drop table bik_fts_stemmed_word;
go

create table bik_fts_stemmed_word (
  id int not null identity primary key, 
  word_id int not null,
  word varchar(800) not null,
  origin_flag int not null,
  unique (word_id, word)
);
go

alter table bik_fts_stemmed_word add weight decimal(9,8) not null default 1.0;
go


---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------

update bik_fts_stemmed_word
set weight = 0.2
where word in ('i', 'syst', 'do', 'oraz', 'oraza', 'wg');

update bik_fts_stemmed_word
set weight = 5.0
where word in ('system', 'koszt', 'almonde', 'rezerwa', 'korekta', 'stopa', 'procentowy', 'msr', 'wylicza�', 'bilansowy', 'pozabilansowy', 
'rezerw', 'wyliczania', 's�u�y�', 'pozabilansowych', 'koszt�w', 'efektywny', 's�u�y', 'bilansowych', 'procentowych');


update sw
set sw.weight = x.weight
from
bik_fts_stemmed_word sw inner join
(
select 
  x.word, x.cnt, case when len(x.word) <= 2 then 0.3 else 1 end * case when cnt = 1 then 0.01 else log10(2000.0/sqrt(x.cnt)) end as weight
from
(
select sw.word, count(*) as cnt
from bik_fts_stemmed_word sw inner join bik_fts_word_in_obj_attr wioa on sw.word_id = wioa.word_id
group by sw.word
--order by cnt desc
) x
) x 
on sw.word = x.word


--where x.word in ('profile', 'koma', 'almonde')
--order by weight desc


/*

select * from bik_searchable_attr_val


select * from bik_searchable_attr


    select top 100 node_id
    from bik_searchable_attr_val
    where node_id not in (select obj_id from bik_fts_word_in_obj_attr)
    group by node_id


delete from bik_fts_attr

insert into bik_fts_attr (name, weight) 
output inserted.*
values ('aqq', 1), ('pies', 0.8)


select * from bik_fts_attr


update bik_fts_attr set weight = case when name = 'name' then 1 when name = 'descr' then 0.9 else 0.7 end



if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#tmp_bik_fts_word'))
  drop table #tmp_bik_fts_word;
  
create table #tmp_bik_fts_word (
  word varchar(800) not null primary key,
  origin_flag int not null,
  occurence_cnt int not null,
  max_weight decimal(9,8) not null,
  attr_ids_str varchar(max) not null
);

insert into #tmp_bik_fts_word (word, origin_flag, occurence_cnt, max_weight, attr_ids_str) values 
('ala', 6, 3, 0.9, '1,2,1,1,2'),
('kot', 5, 2, 1, '1');

merge into bik_fts_word w
using #tmp_bik_fts_word nw on w.word = nw.word
when not matched by target then
  insert (word, origin_flag, occurence_cnt, obj_cnt, max_weight)
  values (word, origin_flag, occurence_cnt, 1, max_weight)
when matched then
  update set
    origin_flag |= nw.origin_flag,
    occurence_cnt += nw.occurence_cnt,
    obj_cnt += 1,
    max_weight = case when nw.max_weight > w.max_weight then nw.max_weight else w.max_weight end;



select cast(a.str as int) as attr_id, w.id as word_id, nw.origin_flag, count(*) as occurence_cnt, nw.max_weight  
from #tmp_bik_fts_word nw inner join bik_fts_word w on nw.word = w.word cross apply dbo.fn_split_by_sep(nw.attr_ids_str, ',', 7) a
group by a.str, w.id, nw.origin_flag, nw.max_weight



drop table #tmp_bik_fts_word;



truncate table bik_fts_word_in_obj_attr

truncate table bik_fts_word

truncate table bik_fts_attr


select * 
from bik_fts_word_in_obj_attr wioa inner join bik_fts_word w on w.id = wioa.word_id inner join bik_fts_attr a on a.id = wioa.attr_id


select *--, ascii(word) 
from bik_fts_word
--where word like '%rekomendacja%'
order by word


select * from bik_fts_word where word = 'profile'

select count(distinct obj_id) from bik_fts_word_in_obj_attr where word_id = 1290


select sw.*, w.word
from bik_fts_stemmed_word sw inner join bik_fts_word w on sw.word_id = w.id
order by w.word, sw.origin_flag desc


select * from bik_searchable_attr_val where value like '%si_config__25__si_final%'

select * from bik_node where id in (246937, 269872)

select * from bik_node where id in (122572)

select *, len(value) from bik_searchable_attr_val where node_id = 122572

select * from bik_searchable_attr where id = 989


select * from bik_fts_word where word like 'Mi�dzybank%'
order by word

select count(*) from bik_fts_processed_obj



/*

-- select cast(1.1 as decimal(9,8))

select top 100 * from bik_searchable_attr_val
where attr_id = 1
order by len(value) desc


select * from bik_searchable_attr

*/
*/

