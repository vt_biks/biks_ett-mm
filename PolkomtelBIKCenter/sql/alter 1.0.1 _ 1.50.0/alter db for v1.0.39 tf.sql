exec sp_check_version '1.0.39';

delete from bik_data_load_log
  
alter table bik_data_load_log drop column creation_time
go
  
alter table bik_data_load_log add start_time datetime not null
go
  
alter table bik_data_load_log add finish_time datetime null default null
go
  
exec sp_update_version '1.0.39', '1.0.40';
go
