﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- Confluence etap I - pobieranie treści z Confluence
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_check_version '1.6.y.7';
go

if not exists(select 1 from bik_admin where param like 'confluence%')
begin
	insert into bik_admin(param, value)
	values ('confluence.isActive', '0')
	insert into bik_admin(param, value)
	values ('confluence.url', '')
	insert into bik_admin(param, value)
	values ('confluence.user', '')
	insert into bik_admin(param, value)
	values ('confluence.password', '')
	insert into bik_admin(param, value)
	values ('confluence.checkChildrenCount', '0')
end;
go

if not exists(select 1 from bik_node_kind where code in ('ConfluenceFolder', 'ConfluenceSpace', 'ConfluencePage', 'ConfluenceBlog'))
begin
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('ConfluenceFolder', 'Folder', 'confluenceFolder', 'Confluence', 1, 10, 0)
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('ConfluenceSpace', 'Przestrzeń', 'confluenceSpace', 'Confluence', 1, 12, 0)
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('ConfluencePage', 'Strona', 'confluencePage', 'Confluence', 1, 11, 0)
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('ConfluenceBlog', 'Wpis blogowy', 'confluenceBlog', 'Confluence', 1, 11, 0)
end;
go

if not exists(select 1 from bik_tree where code = 'AtlassianConfluence')
begin
	declare @confluenceFolderId int;
	select @confluenceFolderId = id from bik_node_kind where code = 'ConfluenceFolder'

	insert into bik_tree(name, node_kind_id, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	values ('Confluence', @confluenceFolderId, 'AtlassianConfluence', 'Confluence', 1, 0, 1, 0)

	insert into bik_data_source_def(description)
	values ('Confluence')

	declare @confluenceSourceId int;
	select @confluenceSourceId = id from bik_data_source_def where description = 'Confluence'

	insert into bik_schedule(source_id, hour, minute, interval, is_schedule, priority)
	values(@confluenceSourceId, 20, 0, 1, 0, 11)

	-- translate
	insert into bik_translation(code, txt, lang, kind)
	values('AtlassianConfluence', 'Confluence', 'en', 'tree')
	insert into bik_translation(code, txt, lang, kind)
	values('ConfluenceFolder', 'Folder', 'en', 'kind')
	insert into bik_translation(code, txt, lang, kind)
	values('ConfluenceSpace', 'Space', 'en', 'kind')
	insert into bik_translation(code, txt, lang, kind)
	values('ConfluencePage', 'Page', 'en', 'kind')
	insert into bik_translation(code, txt, lang, kind)
	values('ConfluenceBlog', 'Blog entry', 'en', 'kind')
end;
go

-- modrzew
declare @treeOfTreesId int = dbo.fn_tree_id_by_code('TreeOfTrees');
if not exists(select 1 from bik_node where tree_id = @treeOfTreesId and obj_id = 'Confluence')
begin
	exec sp_add_menu_node null, 'Confluence', 'confluence'
	exec sp_add_menu_node 'confluence', '@', '$AtlassianConfluence'
	exec sp_add_menu_node '$AtlassianConfluence', '@', '&ConfluenceFolder'
	exec sp_add_menu_node '&ConfluenceFolder', '@', '&ConfluenceSpace'
	exec sp_add_menu_node '&ConfluenceSpace', '@', '&ConfluencePage'
	exec sp_add_menu_node '&ConfluenceSpace', '@', '&ConfluenceBlog'
	    
	exec sp_node_init_branch_id @treeOfTreesId, null
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_confluence_extradata') and type in (N'U'))
begin
	create table bik_confluence_extradata (
		id int not null identity(1,1) primary key,
		node_id int not null references bik_node(id) unique,
		obj_id varchar(100) not null,
		parent_id varchar(100) null,
		space_key varchar(20) not null,
		content varchar(max) null,
		kind varchar(100) not null,
		url varchar(512) not null,
		author varchar(128) null,
		publish_date datetime null,
		is_as_root int not null default(1),
		group_id int not null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_confluence_attachment') and type in (N'U'))
begin
	create table bik_confluence_attachment (
		id int not null identity(1,1) primary key,
		node_id int not null references bik_node(id),
		name varchar(512) not null,
		obj_id varchar(100) not null,
		page_id varchar(100) null,
		space_key varchar(20) not null,
		comment varchar(max) null,
		type varchar(100) not null,
		url varchar(512) not null,
		creator varchar(128) null,
		created_date datetime null
	);
end;
go

exec sp_update_version '1.6.y.7', '1.6.y.8';
go
