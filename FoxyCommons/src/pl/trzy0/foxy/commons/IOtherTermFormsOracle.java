package pl.trzy0.foxy.commons;

import java.util.EnumSet;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.Pair;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author wezyr
 */
public interface IOtherTermFormsOracle {

    public static final EnumSet<FTSWordOrigin> allButOriginal = EnumSet.complementOf(BaseUtils.paramsToEnumSet(FTSWordOrigin.Original));
    public static final EnumSet<FTSWordOrigin> allFormKinds = EnumSet.allOf(FTSWordOrigin.class);

    public Set<Pair<String, FTSWordOrigin>> getOtherForms(String term, EnumSet<FTSWordOrigin> kinds);

    public String getNameAndVer();
}
