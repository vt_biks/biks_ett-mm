/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.IMyBIKSPage.ReachableMyBIKSTabCode;
import pl.fovis.client.bikwidgets.IPaginatedItemsDisplayer;
import pl.fovis.client.bikwidgets.MyBIKSTutorialGrid;
import pl.fovis.common.TutorialBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class MyBIKSTutorialTab extends MyBIKSTabWithPaginatorBase<TutorialBean> {

    @Override
    public String getCaption() {
        return I18n.samouczek.get() /* I18N:  */;
    }

    @Override
    public String getButtonStyle() {
        return "labelLink-tutorial";
    }

    @Override
    protected IPaginatedItemsDisplayer<TutorialBean> createPaginatedItemsDisplayer() {
        return new MyBIKSTutorialGrid();
    }

    @Override
    protected int getPageSize() {
        return 12;
    }

    @Override
    public String getHelpCaption() {
        return "Tutorial" /* I18N: no */;
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<TutorialBean>> callback) {
        BIKClientSingletons.getService().getTutorial(callback);
    }

    @Override
    public ReachableMyBIKSTabCode getOptTabCode() {
        return ReachableMyBIKSTabCode.Tutorial;
    }

    public boolean isVisible() {
        return false;
    }
}
