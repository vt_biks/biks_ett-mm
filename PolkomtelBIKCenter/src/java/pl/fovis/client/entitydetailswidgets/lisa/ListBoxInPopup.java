/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DecoratedPopupPanel;
import com.google.gwt.user.client.ui.ListBox;
import java.util.List;
import java.util.logging.Logger;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.lisa.LisaConstants;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class ListBoxInPopup {

    private static final Logger logger = Logger.getLogger("ListBoxInPopup");
    private static final int MAX_ITEM_SHOWN = 6;
    private static final int MAX_FETCHED_ITEMS = 100;
    private static final int START_FETCHING_AT = 0;
    private ListBox lb;

    private DecoratedPopupPanel suggestPopup;
    private LisaDictColumnBean columnBean;
    protected Integer instanceId;

    public ListBoxInPopup(Integer instanceId) {
        this.instanceId = instanceId;
        lb = new ListBox();
        lb.setVisibleItemCount(MAX_ITEM_SHOWN);
        suggestPopup = new DecoratedPopupPanel();
        suggestPopup.setWidget(lb);
        suggestPopup.setAutoHideEnabled(true);
    }

    public void resetListBox(final String value) {
        if (value != null && value.length() < START_FETCHING_AT) {
            return;
        }
        if (columnBean.validateLevel == LisaConstants.VALIDATE_FROM_LIST) {

            BIKClientSingletons.getLisaService().getValidationList(columnBean.id, new AsyncCallback<List<String>>() {

                @Override
                public void onFailure(Throwable caught) {
                    BIKClientSingletons.showError("getValidationList - Error");
                }

                @Override
                public void onSuccess(List<String> result) {
                    showSuggestedList(value, result);
                }

            });
        } else if (columnBean.validateLevel == LisaConstants.VALIDATE_FROM_DICT) {
            BIKClientSingletons.getLisaService().getDistinctColumnValues(instanceId, columnBean, BaseUtils.isStrEmptyOrWhiteSpace(value)/* == null */ ? "" : value, MAX_FETCHED_ITEMS, new StandardAsyncCallback<List<String>>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            BIKClientSingletons.showError("getDistinctColumnValues - Error");
                        }

                        @Override
                        public void onSuccess(List<String> result) {
                            showSuggestedList(value, result);
                        }
                    });
        }
    }

    private void showSuggestedList(String value, List<String> result) {
        lb.clear();
        if (value == null) {
            value = "";
        }
        for (String choice : result) {
            if (choice.contains(value)) {
                lb.addItem(choice);
            }
        }
    }

    public ListBox getLb() {
        return lb;
    }

    public boolean isShowing() {
        return suggestPopup.isShowing();
    }

    public void setPopupPosition(int left, int top) {
        suggestPopup.setPopupPosition(left, top);
    }

    public void hide() {
        suggestPopup.hide();
    }

    public void show() {
        suggestPopup.show();
    }

    public void setColumnBean(LisaDictColumnBean columnBean) {
        this.columnBean = columnBean;
    }

    public boolean isSelected() {
        return lb.getSelectedIndex() >= 0;
    }
}
