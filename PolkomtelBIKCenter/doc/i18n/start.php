<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto; line-height:14.4pt"><p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto; line-height:14.4pt"><span style="font-size:12.0pt;mso-bidi-font-size:14.0pt; font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;; color:#094F8B;mso-fareast-language:PL">You are logged on as: <b>A regular user</b></span>
    <span style="font-family: Tahoma, sans-serif; "><o:p></o:p></span>
</p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto; line-height:14.4pt"><span style="font-size:12.0pt;mso-bidi-font-size:14.0pt; font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;; color:#094F8B;mso-fareast-language:PL">Your privileges allow:</span>
    <span style="font-family: Tahoma, sans-serif; "><o:p></o:p></span>
</p>
<ul type="disc">  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL">Browsing data in the system</span>
        <span style="font-size:10.0pt;      mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:      &quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
    </li>
    <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL">Refreshing data in the system.</span>
        <span style="font-size:10.0pt;      mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:      &quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
    </li>
    <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL">Adding comments and ratings to content in the system.</span>
        <span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
    </li>
    <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL">Searching data in the system.</span>
        <span style="font-size:10.0pt;      mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:      &quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
    </li>
</ul></p>
<!---------------------------------------------------------------------------------------------------------------------->
<p></p>
<p class="MsoNormal"></p>
<p class="MsoNormal"></p>
<p class="MsoNormal" style="background-color: white; background-position: initial initial; background-repeat: initial initial; "></p>
<p class="MsoNormal" style="background-color: white; background-position: initial initial; background-repeat: initial initial; "></p>
<p class="MsoNormal" style="background-color: white; background-position: initial initial; background-repeat: initial initial; "><p class="MsoNormal" style="background-color: white; background-position: initial initial; background-repeat: initial initial; "><b><span style="font-size:12.0pt; mso-bidi-font-size:14.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family: &quot;Times New Roman&quot;;color:#094F8B;mso-fareast-language:PL">You are logged on as: <b>Expert </b></span>
</b><span style="font-family: Tahoma, sans-serif; "><o:p></o:p></span>
</p>
  <p class="MsoNormal" style="background-color: white; background-position: initial initial; background-repeat: initial initial; "><b><span style="font-size:12.0pt; mso-bidi-font-size:14.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family: &quot;Times New Roman&quot;;color:#094F8B;mso-fareast-language:PL">Your privileges allow: </span>
</b><span style="font-family: Tahoma, sans-serif; "><o:p></o:p></span>
</p>
  <ul type="disc">  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Browsing data in the system.</span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Refreshing data in the system.</span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Adding comments and ratings to content in the system.</span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;      font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;      mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold"> Adding news. </span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Searching data in the system.</span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold"> Defining associations between various elements of the system.</span>
<span style="font-size:10.0pt;mso-bidi-font-size:      12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;      mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Adding descriptions and objects to system modules.</span>
<span style="font-size:10.0pt;mso-bidi-font-size:      12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;      mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Deleting elements contained in the system (except for blogging content posted by other users).</span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Editing content in the system (with the exception of blogging content posted by other users).</span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
</li>
 </ul></p>
<ul type="disc"> </ul><p></p>
<ul type="disc"> </ul><p></p>
<ul type="disc"> </ul><p></p>
<p></p>
<p></p>
<p></p>


<!---------------------------------------------------------------------------------------------------------------------->

<h3>Welcome to BI Knowledge System - demo version</h3>
  BI Knowledge System is a tool that allows managing knowledge associated with Business Intelligence systems. This demo version of the application is a fully functional program that allows normal operation of the system and allows users to explore the benefits of its use. Changes saved by a user visiting the BI Knowledge System demo version will be removed at the end of each working day. All data contained in it is exemplary. The content of descriptions of objects may not be true and the correlation of users' names is coincidental.

<!---------------------------------------------------------------------------------------------------------------------->

<h3>Welcome to BI Knowledge System</h3> BI Knowledge System is a tool that allows managing knowledge associated with Business Intelligence systems.  Allows collection of specific knowledge related to the use of BI in the company in one place and in such a way that the lack of access to people with expertise does not cause stopping processes associated with decision-making. BI Knowledge System also helps to build a community of BI users giving them the opportunity of unfettered communication and exchange of knowledge.

<!---------------------------------------------------------------------------------------------------------------------->