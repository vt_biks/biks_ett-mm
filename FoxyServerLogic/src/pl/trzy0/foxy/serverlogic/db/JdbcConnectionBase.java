/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import commonlib.GlobalThreadStopWatch;
import static commonlib.JdbcUtil.generateTargetColNames;
import static commonlib.JdbcUtil.readOneRowFast;
import commonlib.LameUtils;
import commonlib.ThreadActivityCollector;
import commonlib.ThreadCustomVars;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.trzy0.foxy.commons.FoxyRuntimeException;
import pl.trzy0.foxy.commons.IBeanConnectionThreadHandle;
import pl.trzy0.foxy.serverlogic.BeanConnectionExBase;
import pl.trzy0.foxy.serverlogic.FoxySqlCache;
import pl.trzy0.foxy.serverlogic.FoxySqlCacheItem;
import pl.trzy0.foxy.serverlogic.IConnectionDialectHelper;
import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.FieldNameConversion;
import simplelib.IIndexedNamedVals;
import simplelib.ILameCollector;
import simplelib.IParametrizedContinuation;
import simplelib.IParametrizedContinuationWithReturn;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.SimpleDateUtils;
import simplelib.StringEscapeUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public abstract class JdbcConnectionBase<BeanBase> extends BeanConnectionExBase<BeanBase> {

    private static final boolean DumpMemUsageModeOn = false;
    // default: more than that will yield logger.warn
    private static final long DB_STATEMENT_EXECUTION_WARN_THRESHOLD_MILLIS = -1000;
    private static final String JDBC_CONNECTION_BASE_STMT_FOR_THREAD = "JDBC_CONNECTION_BASE_STMT_FOR_THREAD";
    private static final ILameLogger logger = LameUtils.getMyLogger();
    private final FoxySqlCache sqlCache = null;
    protected long dbStatementExecutionWarnThresholdMillis = DB_STATEMENT_EXECUTION_WARN_THRESHOLD_MILLIS;
    protected IConnectionDialectHelper dialect;
    private final boolean useActivityCollector = false;

    protected abstract IConnectionDialectHelper createDialectHelper();

    protected abstract Connection getConnection();

    protected abstract Statement getStatement();

    protected abstract void releaseStatement(Statement st);

    public JdbcConnectionBase() {
        this.dialect = createDialectHelperPrivate();
    }

    private IConnectionDialectHelper createDialectHelperPrivate() {
        return createDialectHelper();
    }

//    @Override
//    public abstract String toSqlString(Object o);
//    @Override
//    public abstract int readSequenceNextVal(String seqName);
//    @Override
//    public abstract void destroy();
    @Override
    public Integer execCommand(String cmdTxt) {
        if (showCmdsForTesting) {
            System.out.println("/* " + SimpleDateUtils.timeToString(new Date(), true) + " */ new ExecCmdStep(" + StringEscapeUtils.escapeJavaScriptAndQuote(cmdTxt) + "),");
        }
        if (isExecutionCanceled()) {
            throw new RuntimeException("execution is canceled");
        }

        boolean doLogging = !getRunWithoutLoggingMode();

        if (doLogging && logger.isDebugEnabled()) {
            logger.debug("execCommand: cmdTxt=\n" + cmdTxt);
        } else {
//            System.out.println("doLogging=" + doLogging
//                    + ", logger.level=" + logger.getFullLoggerName() + ":" + logger.getLoggerLevel());
        }

        if (sqlCache != null) {
            sqlCache.invalidateBySqlTxt(cmdTxt);
        }

        Date startDate = new Date();
        Integer updateCount = null;
        boolean hasResultSet = false;
        try {
            Statement st = //getConnection().createStatement();
                    //getStatement();
                    getNewStatementForThread();

            try {
                hasResultSet = st.execute(cmdTxt);
                updateCount = st.getUpdateCount();
            } finally {
                //st.close();
                //releaseStatement(st);
                releaseStatementForThread(st);
            }
        } catch (SQLException ex) {
            logger.errorAndThrowNew("error in command execution, txt=" + cmdTxt, ex);
            //throw new FoxyRuntimeException(ex);
        }
        Date endDate = new Date();
        if (doLogging && logger.isDebugEnabled()) {
            logger.debug("execCommand: done");
        }
        if (doLogging && logger.isInfoEnabled()) {
            logger.info("command executed in " + SimpleDateUtils.getElapsedSecondsWithMilliPrec(startDate, endDate) + " secs, txt=" + cmdTxt + ", updCnt=" + updateCount + ", hasRsltSet=" + hasResultSet);
        }

        //ThreadActivityCollector.addActivityElapsedTime("db", cmdTxt, SimpleDateUtils.getElapsedMillis(startDate, endDate));
        addToActitivyCollectorAndWarnIfTooLong("cmd", cmdTxt, startDate, endDate);

//        if (cacheLogger.isDebugEnabled()) {
//            cacheLogger.debug("exec command: " + cmdTxt);
//        }
        return updateCount;
    }

    protected void addToActitivyCollectorAndWarnIfTooLong(String kind, String txt, Date startDate, Date endDate) {
        long elaMillis = SimpleDateUtils.getElapsedMillis(startDate, endDate);
        if (useActivityCollector) {
            ThreadActivityCollector.addActivityElapsedTime("db", txt, elaMillis);
        }

        if (logger.isWarnEnabled() && checkStatementTookTooLong(kind, elaMillis)) {
            logger.warn("db statement took too long (" + elaMillis + " millis), " + kind + " txt=" + txt);
        }
    }

    protected boolean checkStatementTookTooLong(String kind, long elaMillis) {
        long threshold = getDbStatementExecutionWarnThresholdMillis(kind);
        return threshold > 0 && elaMillis > threshold;
    }

    protected long getDbStatementExecutionWarnThresholdMillis(String kind) {
        return dbStatementExecutionWarnThresholdMillis;
    }

    protected Statement getNewStatementForThread() {
        Pair<Statement, Boolean> holder = getStatementHolder();

        if (holder.v2) {
            throw new RuntimeException("execution is canceled");
        }

        Statement statement = getStatement();

        holder.v1 = statement;

        return statement;
    }

    protected void releaseStatementForThread(Statement statement) {
//        Pair<Statement, Boolean> holder = getStatementHolder();
//        holder.v1 = null;
        ThreadCustomVars.setVar(JDBC_CONNECTION_BASE_STMT_FOR_THREAD, null);

        releaseStatement(statement);
    }

    protected Pair<Statement, Boolean> getStatementHolder() {
        Pair<Statement, Boolean> holder = getStatementHolderOrNull();
        if (holder == null) {
            holder = new Pair<Statement, Boolean>();
            // musi być ustawienie na false, bo bez tego będzie null!
            holder.v2 = false;
            ThreadCustomVars.setVar(JDBC_CONNECTION_BASE_STMT_FOR_THREAD, holder);
        }
        return holder;
    }

    protected boolean isExecutionCanceled() {
        return getStatementHolder().v2;
    }

    protected boolean hasStatementInUse() {
        return BaseUtils.safeGetV1OfPair(getStatementHolderOrNull()) != null;
    }

    protected Pair<Statement, Boolean> getStatementHolderOrNull() {
        @SuppressWarnings("unchecked")
        Pair<Statement, Boolean> holder = (Pair<Statement, Boolean>) ThreadCustomVars.getVar(JDBC_CONNECTION_BASE_STMT_FOR_THREAD);
        return holder;
    }

    public interface IExecQryRowProcessor {

        public void prepareToProcessFromResultSet(ResultSet rs, FoxySqlCacheItem item);

        public void processCurrentRowFromResultSet() throws SQLException;

        public void processCurrentRowFromCache(Map<String, Object> row);
    }

    public void execQryEx(final String qryTxt,
            final boolean maxOneRow, final boolean minOneRow,
            final Projector<List<String>, Map<String, String>> prepareTargetNamesProj,
            final ILameCollector<IIndexedNamedVals> fastRowCollector,
            final ILameCollector<Map<String, Object>> rowCollectorFromCache) {

        execQry(qryTxt, maxOneRow, minOneRow, new IExecQryRowProcessor() {
            String[] sourceColNames;
            String[] targetColNames;
            ResultSet resultSet;
            FoxySqlCacheItem item;
            IIndexedNamedVals inv;
            int colCnt;

            public void prepareToProcessFromResultSet(ResultSet rs, FoxySqlCacheItem item) {
                this.resultSet = rs;
                this.item = item;
                sourceColNames = generateTargetColNames(rs, null);
                colCnt = sourceColNames.length;
                targetColNames = new String[colCnt];

                List<String> names = Arrays.asList(sourceColNames);

                Map<String, String> convMap = prepareTargetNamesProj.project(names);

//                System.out.println("names=" + names);
//                System.out.println("convMap=" + convMap);
                for (int i = 0; i < colCnt; i++) {
                    String sourceName = sourceColNames[i];
                    String targetName = convMap.get(sourceName);
                    targetColNames[i] = targetName;
//                    System.out.println("#" + i + ": targetName=" + targetName + " for sourceName=" + sourceName);
                }

                inv = new IIndexedNamedVals() {
                    public int getValCnt() {
                        return colCnt;
                    }

                    public String getDstName(int idx) {
                        String targetName = targetColNames[idx];
//                        System.out.println("getDstName(" + idx + "): targetName=" + targetName);
                        return targetName;
                    }

                    public Object getVal(int idx) {
                        try {
                            return resultSet.getObject(idx + 1);
                        } catch (Exception ex) {
                            throw new LameRuntimeException("cannot read val with idx=" + idx, ex);
                        }
                    }
                };
            }

            public void processCurrentRowFromResultSet() throws SQLException {
                fastRowCollector.add(inv);

                if (sqlCache != null) {
                    Map<String, Object> row = readOneRowFast(resultSet, sourceColNames);
                    item.result.add(row);
                }
            }

            public void processCurrentRowFromCache(Map<String, Object> row) {
                rowCollectorFromCache.add(row);
            }
        });
    }

    @Override
    public <C extends ILameCollector<Map<String, Object>>> C execQry(final String qryTxt,
            final boolean maxOneRow, final boolean minOneRow,
            final C rowCollector, final FieldNameConversion optConversion) {

        execQry(qryTxt, maxOneRow, minOneRow, new IExecQryRowProcessor() {
            String[] targetColNames;
            ResultSet rs;
            FoxySqlCacheItem item;

            public void prepareToProcessFromResultSet(ResultSet rs, FoxySqlCacheItem item) {
                this.rs = rs;
                this.item = item;
                targetColNames = generateTargetColNames(rs, optConversion);
            }

            public void processCurrentRowFromResultSet() throws SQLException {
                Map<String, Object> row = readOneRowFast(rs, targetColNames);
                if (sqlCache != null) {
                    item.result.add(row);
                }
                rowCollector.add(row);
            }

            public void processCurrentRowFromCache(Map<String, Object> row) {
                rowCollector.add(row);
            }
        });

        return rowCollector;
    }

    protected void execQry(String qryTxt, boolean maxOneRow, boolean minOneRow,
            IExecQryRowProcessor rowProcessor) {

        if (showCmdsForTesting) {
            System.out.println("/* " + SimpleDateUtils.timeToString(new Date(), true) + " */ new QueryStep(null, " + StringEscapeUtils.escapeJavaScriptAndQuote(qryTxt) + "),");
        }

        if (DumpMemUsageModeOn) {
            LameUtils.dumpMemoryUsage(" >> execQry: start");
        }

        if (isExecutionCanceled()) {
            throw new RuntimeException("execution is canceled");
        }

        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
            GlobalThreadStopWatch.startProcess("execQry");
        }
        try {
            boolean doLogging = !getRunWithoutLoggingMode();

            //if (cacheLogger.isDebugEnabled()) cacheLogger.debug("execQryEx: " + qryTxt);
            if (doLogging && logger.isDebugEnabled()) {
                //logger.info("execQryEx: msg1");
                logger.debug("execQry: starting for qryTxt=\n" + qryTxt);
            } else {
//            System.out.println("doLogging=" + doLogging
//                    + ", logger.level=" + logger.getFullLoggerName() + ":" + logger.getLoggerLevel());
            }

            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.startProcessStage("cache test");
            }
            FoxySqlCacheItem item = null;
            if (sqlCache != null) {
                Set<String> keys = FoxySqlCache.extractCacheKeys(qryTxt);
                item = sqlCache.findInCache(keys, qryTxt);
                if (item != null) {
                    for (Map<String, Object> row : item.result) {
                        rowProcessor.processCurrentRowFromCache(row);
                    }
                    return;
                }

                item = new FoxySqlCacheItem();
                item.sqlTxt = qryTxt;
                item.result = new ArrayList<Map<String, Object>>();
                item.keys = keys;
            }

            //if (logger.isInfoEnabled()) {
            //    logger.info("execQryEx: msg2");
            //}
            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.startProcessStage("before startDate");
            }
            Date startDate = new Date();
            //if (logger.isInfoEnabled()) {
            //    logger.info("execQryEx: start date set");
            //}

            //List<Map<String, Object>> res = new ArrayList<Map<String, Object>>();
            int rowCnt = 0;

            if (doLogging && logger.isDebugEnabled()) {
                logger.debug("execQry: before getStatement");
            }
            try {
                Statement st = //getConnection().createStatement();
                        //getStatement();
                        getNewStatementForThread();
                try {
                    if (doLogging && logger.isDebugEnabled()) {
                        logger.debug("execQry: before execute");
                    }
                    if (DumpMemUsageModeOn) {
                        LameUtils.dumpMemoryUsage(" >> execQry: before exec query");
                    }

                    ResultSet rs = st.executeQuery(qryTxt);
                    if (doLogging && logger.isDebugEnabled()) {
                        logger.debug("execQry: before read data");
                    }
                    if (DumpMemUsageModeOn) {
                        LameUtils.dumpMemoryUsage(" >> execQry: after exec query");
                    }

//                    boolean useReadOneRowFast = true;
//                    String[] targetColNames = null;
//
//                    if (useReadOneRowFast) {
//                        targetColNames = generateTargetColNames(rs, optConversion);
//                    }
                    rowProcessor.prepareToProcessFromResultSet(rs, item);

                    try {
                        while (rs.next()) {
                            if (maxOneRow && rowCnt > 0) {
                                logger.errorAndThrowNew("query returned more than 1 row, txt=" + qryTxt);
                            }

                            rowProcessor.processCurrentRowFromResultSet();
//                            Map<String, Object> row;
//                            if (useReadOneRowFast) {
//                                row = readOneRowFast(rs, targetColNames);
//                            } else {
//                                row = readOneRow(rs, optConversion);
//                            }
//                            if (sqlCache != null) {
//                                item.result.add(row);
//                            }
//                            rowCollector.add(row);

                            rowCnt++;
                        }
                    } finally {
                        rs.close();
                    }

                    if (DumpMemUsageModeOn) {
                        LameUtils.dumpMemoryUsage(" >> execQry: after loop - readOneRow");
                    }

                } finally {
                    //st.close();
                    //releaseStatement(st);
                    releaseStatementForThread(st);
                }
            } catch (Exception ex) { //ww: (2013.02.26) było SQLException
                String causeTxt = "???";

                try {
                    causeTxt = ex.getMessage();
                } catch (Exception exx) {
                    // no-op
                }

                logger.errorAndThrowNew("sql error, sql=" + qryTxt + "\nCause txt=" + causeTxt, ex);
            }

            if (minOneRow && rowCnt < 1) {
                logger.errorAndThrowNew("query returned no rows, txt=" + qryTxt);
            }
//            if (maxOneRow && rowCnt > 1) {
//                logger.errorAndThrowNew("query returned more than 1 row, txt=" + qryTxt);
//            }

            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.startProcessStage("before calc elapsed time");
            }

            Date endDate = new Date();

            if (doLogging && logger.isDebugEnabled()) {
                logger.debug("execQry: done");
            }

            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.startProcessStage("after calc elapsed time");
            }

            if (sqlCache != null) {
                item.duration = (int) SimpleDateUtils.getElapsedMillis(startDate, endDate);
                sqlCache.addToCache(item);
            }

            if (doLogging && logger.isInfoEnabled()) {
                String msg = "*** query executed in " + SimpleDateUtils.getElapsedSecondsWithMilliPrec(startDate, endDate) + " s., qryTxt=\n" + qryTxt;
                //Date endDate2 = new Date();

                logger.info(msg /*+ ", msg prep. (s): " + (endDate2.getTime() - endDate.getTime()) / 1000d*/);
            }

            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.startProcessStage("after logged query executed");
            }

            addToActitivyCollectorAndWarnIfTooLong("qry", qryTxt, startDate, endDate);
//        long elaMillis = SimpleDateUtils.getElapsedMillis(startDate, endDate);
//        ThreadActivityCollector.addActivityElapsedTime("db", qryTxt, elaMillis);
//
//        if (elaMillis > DB_STATEMENT_EXECUTION_WARN_THRESHOLD_MILLIS && logger.isWarnEnabled()) {
//            logger.warn("query took too long: " + elaMillis + " millis, txt=" + qryTxt);
//        }
            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.startProcessStage("just before return");
            }

//            return;
        } finally {
            if (DumpMemUsageModeOn) {
                LameUtils.dumpMemoryUsage(" >> execQry: end");
            }

            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.endProcess();
            }
        }
    }

    public boolean getAutoCommit() {
        try {
            return getConnection().getAutoCommit();
        } catch (Exception ex) {
            throw new LameRuntimeException("error in getAutoCommit()", ex);
        }
    }

    @Override
    public void setAutoCommit(boolean val) {
        try {
            getConnection().setAutoCommit(val);
        } catch (Exception ex) {
            throw new LameRuntimeException("error for setAutoCommit(" + val + ")", ex);
        }
    }

    @Override
    public void finalizeTran(boolean success) {
        if (!hasStatementInUse()) {
            return; //ww: nie ma czego commitować ani rollbackować
        }
        Connection conn = getConnection();
        try {

            if (conn.getAutoCommit()) {
                if (logger.isWarnEnabled()) {
                    logger.warn("finalizeTran(" + success + ") executed in auto-commit mode");
                }
                return;
            }

            if (success) {
                conn.commit();
            } else {
                conn.rollback();
            }
        } catch (Exception ex) {
            throw new LameRuntimeException("error for finalizeTran(" + success + ")", ex);
        }
    }

    @Deprecated
    private Map<String, Object> readOneRow(ResultSet rs, FieldNameConversion optConversion) {
        try {
            ResultSetMetaData metaData = rs.getMetaData();
            int colCnt = metaData.getColumnCount();

            Map<String, Object> res = new LinkedHashMap<String, Object>(
                    BaseUtils.calcHashMapCapacity(colCnt)//colCnt * 4 / 3 + 1
            );

            for (int i = 1; i <= colCnt; i++) {
                String colName = metaData.getColumnName(i);
                if (optConversion == FieldNameConversion.ToUpperCase) {
                    colName = colName.toUpperCase();
                } else if (optConversion == FieldNameConversion.ToLowerCase) {
                    colName = colName.toLowerCase();
                } else if (optConversion == FieldNameConversion.ToJavaPropName) {
                    colName = BaseUtils.tableColNameToBeanFieldName(colName);
                }
                res.put(colName, rs.getObject(i));
            }

//            if (res.containsKey("iz_D") || res.containsKey("izD")) {
//                System.out.println("*** readOneRow: res=" + res);
//            }
            return res;
        } catch (SQLException ex) {
            throw new FoxyRuntimeException("error reading row", ex);
        }
    }

    public void finishWorkUnit(boolean success) {
        finalizeTran(success);
    }

    public void setDbStatementExecutionWarnThresholdMillis(long threshold) {
        this.dbStatementExecutionWarnThresholdMillis = threshold;
    }

    public String valToString(Object val) {
        return dialect.valToString(val);
    }

    public int readSequenceNextVal(String seqName) {
        return dialect.readSequenceNextVal(seqName);
    }

    public IBeanConnectionThreadHandle getCurrentThreadHandle() {
        return new IBeanConnectionThreadHandle() {
            final private Pair<Statement, Boolean> statementHolder = getStatementHolder();

            public void cancelExecution() {
                synchronized (statementHolder) {

                    Statement s = statementHolder.v1;
                    boolean canCancel = s != null && !statementHolder.v2;

                    statementHolder.v2 = true;

                    if (canCancel) {
                        try {
                            statementHolder.v1 = null;
                            s.cancel();
                        } catch (Exception ex) {
                            throw new RuntimeException("error while canceling statement", ex);
                        }
                    }
                }
            }

            public void clearCancelFlag() {
                synchronized (statementHolder) {
                    statementHolder.v2 = false;
                }
            }

            public boolean isExecutionCanceled() {
                synchronized (statementHolder) {
                    return statementHolder.v2;
                }
            }
        };
    }

    public void execWithJdbcConnection(IParametrizedContinuation<Connection> whatToExecCont) {
        whatToExecCont.doIt(getConnection());
    }

    public <T> T execWithJdbcConnection(IParametrizedContinuationWithReturn<Connection, T> whatToExecCont) {
        return whatToExecCont.doIt(getConnection());
    }

    private static final boolean showCmdsForTesting = false;

    public void setCurrentDatabase(String dbName) {
        if (showCmdsForTesting) {
            System.out.println("/* " + SimpleDateUtils.timeToString(new Date(), true) + " */ new SetDbStep(" + StringEscapeUtils.escapeJavaScriptAndQuote(dbName) + "),");
        }
        long ntStart = System.nanoTime();
        Connection c = getConnection();
        long ntGotConn = System.nanoTime();
        try {
            c.setCatalog(dbName);
        } catch (Exception ex) {
            throw new LameRuntimeException("error setting current db to: [" + dbName + "]");
        }
        long ntDone = System.nanoTime();
//        System.out.println("setCurrentDatabase: [" + dbName + "] total time: "
//                + SimpleDateUtils.elapsedNanosToMillis3DPrec(ntStart, ntDone)
//                + " millis, conn: "
//                + SimpleDateUtils.elapsedNanosToMillis3DPrec(ntStart, ntGotConn) + " millis, setCatalog: "
//                + SimpleDateUtils.elapsedNanosToMillis3DPrec(ntGotConn, ntDone) + " millis");
    }

    @Override
    public String getCurrentDatabase() {
        Connection c = getConnection();
        try {
            return c.getCatalog();
        } catch (SQLException ex) {
            throw new LameRuntimeException("error getting current db");
        }
    }

    @Override
    public void execQry(String qryTxt, IParametrizedContinuation<Iterator<Map<String, Object>>> processCont, FieldNameConversion optConversion) {

        if (showCmdsForTesting) {
            System.out.println("/* " + SimpleDateUtils.timeToString(new Date(), true) + " */ new QueryStep(null, " + StringEscapeUtils.escapeJavaScriptAndQuote(qryTxt) + "),");
        }

        if (DumpMemUsageModeOn) {
            LameUtils.dumpMemoryUsage(" >> execQry: start");
        }

//        if (useDefaultStatement && isExecutionCanceled()) {
//            throw new RuntimeException("execution is canceled");
//        }
        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
            GlobalThreadStopWatch.startProcess("execQry");
        }
        try {
            boolean doLogging = !getRunWithoutLoggingMode();

            //if (cacheLogger.isDebugEnabled()) cacheLogger.debug("execQryEx: " + qryTxt);
            if (doLogging && logger.isDebugEnabled()) {
                //logger.info("execQryEx: msg1");
                logger.debug("execQry: starting for qryTxt=\n" + qryTxt);
            } else {
//            System.out.println("doLogging=" + doLogging
//                    + ", logger.level=" + logger.getFullLoggerName() + ":" + logger.getLoggerLevel());
            }

            //if (logger.isInfoEnabled()) {
            //    logger.info("execQryEx: msg2");
            //}
            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.startProcessStage("before startDate");
            }
            Date startDate = new Date();
            //if (logger.isInfoEnabled()) {
            //    logger.info("execQryEx: start date set");
            //}

            //List<Map<String, Object>> res = new ArrayList<Map<String, Object>>();
            if (doLogging && logger.isDebugEnabled()) {
                logger.debug("execQry: before getStatement");
            }
            try {
                Statement st = //getConnection().createStatement();
                        getStatement();
//                        useDefaultStatement ? getNewStatementForThread() : getStatement();
                try {
                    if (doLogging && logger.isDebugEnabled()) {
                        logger.debug("execQry: before execute");
                    }
                    if (DumpMemUsageModeOn) {
                        LameUtils.dumpMemoryUsage(" >> execQry: before exec query");
                    }

                    ResultSet rs = st.executeQuery(qryTxt);
                    if (doLogging && logger.isDebugEnabled()) {
                        logger.debug("execQry: before read data");
                    }
                    if (DumpMemUsageModeOn) {
                        LameUtils.dumpMemoryUsage(" >> execQry: after exec query");
                    }

//                    boolean useReadOneRowFast = true;
//                    String[] targetColNames = null;
//
//                    if (useReadOneRowFast) {
//                        targetColNames = generateTargetColNames(rs, optConversion);
//                    }
                    IteratorForResultSet ifrs = new IteratorForResultSet(rs, optConversion);

                    try {
                        processCont.doIt(ifrs);
                    } finally {
                        rs.close();
                    }

                    if (DumpMemUsageModeOn) {
                        LameUtils.dumpMemoryUsage(" >> execQry: after loop - readOneRow");
                    }

                } finally {
                    st.close();
                    //releaseStatement(st);
//                    if (useDefaultStatement) {
//                        releaseStatementForThread(st);
//                    } else {
//                        st.close();
//                    }
                }
            } catch (Exception ex) { //ww: (2013.02.26) było SQLException
                String causeTxt = "???";

                try {
                    causeTxt = ex.getMessage();
                } catch (Exception exx) {
                    // no-op
                }

                logger.errorAndThrowNew("sql error, sql=" + qryTxt + "\nCause txt=" + causeTxt, ex);
            }

//            if (maxOneRow && rowCnt > 1) {
//                logger.errorAndThrowNew("query returned more than 1 row, txt=" + qryTxt);
//            }
            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.startProcessStage("before calc elapsed time");
            }

            Date endDate = new Date();

            if (doLogging && logger.isDebugEnabled()) {
                logger.debug("execQry: done");
            }

            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.startProcessStage("after calc elapsed time");
            }

            if (doLogging && logger.isInfoEnabled()) {
                String msg = "*** query executed in " + SimpleDateUtils.getElapsedSecondsWithMilliPrec(startDate, endDate) + " s., qryTxt=\n" + qryTxt;
                //Date endDate2 = new Date();

                logger.info(msg /*+ ", msg prep. (s): " + (endDate2.getTime() - endDate.getTime()) / 1000d*/);
            }

            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.startProcessStage("after logged query executed");
            }

            addToActitivyCollectorAndWarnIfTooLong("qry", qryTxt, startDate, endDate);
//        long elaMillis = SimpleDateUtils.getElapsedMillis(startDate, endDate);
//        ThreadActivityCollector.addActivityElapsedTime("db", qryTxt, elaMillis);
//
//        if (elaMillis > DB_STATEMENT_EXECUTION_WARN_THRESHOLD_MILLIS && logger.isWarnEnabled()) {
//            logger.warn("query took too long: " + elaMillis + " millis, txt=" + qryTxt);
//        }
            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.startProcessStage("just before return");
            }

//            return;
        } finally {
            if (DumpMemUsageModeOn) {
                LameUtils.dumpMemoryUsage(" >> execQry: end");
            }

            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.endProcess();
            }
        }
    }
}
