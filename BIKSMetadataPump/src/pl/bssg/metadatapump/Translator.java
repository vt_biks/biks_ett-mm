package pl.bssg.metadatapump;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author pkobik
 */
public class Translator {

    private static final String ESCAPE_CHAR = "\u0001";
    private static final Map<String, String> controlSeq;
    private static final Map<String, String> controlSeqRev;

    static {
        controlSeq = new HashMap<String, String>() {
            {
                put("\\\\", ESCAPE_CHAR + "backslash" + ESCAPE_CHAR);
                put("\\:", ESCAPE_CHAR + "colon" + ESCAPE_CHAR);
                put("\\;", ESCAPE_CHAR + "semicolon" + ESCAPE_CHAR);
                put("\\=", ESCAPE_CHAR + "equal" + ESCAPE_CHAR);
                put("\\|", ESCAPE_CHAR + "pipe" + ESCAPE_CHAR);
            }
        };
        controlSeqRev = new HashMap<String, String>() {
            {
                for (String k : controlSeq.keySet()) {
                    put(controlSeq.get(k), k);
                }
            }
        };
    }

    // string postaci: <property_name>=<display_name>|<property_name>=<display_name>|, ew. tłumaczenia w różnych językach
    // z tłumaczeniami: <property_name>=<[pl|en|...]>:<display_name>|<property_name>=<[pl|en|...]>:<display_name>
    public static Map<String, String> getExPropList(String param, String lang) {
        Map<String, String> props = new HashMap<String, String>();
        if (param == null) {
            return props;
        }
        String encoded = encode(param);
        List<String> listOfProps = BaseUtils.splitBySep(encoded, "|", true);
        for (String prop : listOfProps) {
            Pair<String, String> splitString = BaseUtils.splitString(prop, "=");
            if (splitString.v1 != null && !splitString.v1.isEmpty()) {
                props.put(unescape(splitString.v1), unescape(getTranslateForLang(splitString.v2, lang, splitString.v1)));
            }
        }
        return props;
    }

    private static String getTranslateForLang(String langString, String lang, String property) {
        if (lang == null || lang.length() == 0) {
            return property;
        }
        if (langString == null) {
            return property;
        }

        boolean isLangAware = langString.contains(":");
        List<String> tokens = BaseUtils.splitBySep(langString, ";", true);
        Map<String, String> dictionary = new HashMap<String, String>();
        for (String t : tokens) {
            Pair<String, String> dictEntry = BaseUtils.splitByLastNthSep(t, ":", 1);
            if (!isLangAware) {
                dictEntry.v1 = "*";
            }
            dictionary.put(dictEntry.v1, dictEntry.v2);
        }

        String key = isLangAware ? lang : "*";
        if (dictionary.containsKey(key)) {
            return dictionary.get(key);
        }
        return property;
    }

    private static String encode(String text) {
        for (String k : controlSeq.keySet()) {
            text = text.replace(k, controlSeq.get(k));
        }
        return text;
    }

    private static String decode(String text) {
        for (String k : controlSeqRev.keySet()) {
            text = text.replace(k, controlSeqRev.get(k));
        }
        return text;
    }

    private static String unescape(String text) {
        return decode(text).replace("\\", "");
    }

//    public static void main(String[] args) {
////        String test1 = "start\\=pl\\:stop\\;en\\:run\\|=end";
//        String test1 = "Source Schema=pl:Schemat tabeli źródłowej;en:Source Schema|Source Table=pl:Tabela źródłowa;en:Source Table|Source System Code=pl:System źródłowy;en:Source System Code";
//
//        Map<String, String> props = Translator.getExPropList(test1, "pl");
//        for (String p : props.keySet()) {
//            System.out.println("[" + p + "]->" + props.get(p));
//        }
//        System.out.println(Translator.encode(test1));
//        System.out.println(test1.equals(Translator.decode(Translator.encode(test1))));
//    }
}
