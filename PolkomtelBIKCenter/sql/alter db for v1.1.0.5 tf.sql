﻿exec sp_check_version '1.1.0.5';
go
----------------------------------------
----------------------------------------
----------------------------------------
alter table bik_sapbo_extradata drop column created
go
alter table bik_sapbo_extradata drop column modified
go
alter table bik_sapbo_extradata drop column last_run_time 
go
alter table bik_sapbo_extradata drop column timestamp 
go
alter table bik_sapbo_extradata drop column endtime 
go
alter table bik_sapbo_extradata drop column starttime
go
alter table bik_sapbo_extradata drop column nextruntime
go

alter table bik_sapbo_extradata add created datetime null
go
alter table bik_sapbo_extradata add modified datetime null
go
alter table bik_sapbo_extradata add last_run_time datetime null
go
alter table bik_sapbo_extradata add timestamp datetime null
go
alter table bik_sapbo_extradata add endtime datetime null
go
alter table bik_sapbo_extradata add starttime datetime null
go
alter table bik_sapbo_extradata add nextruntime datetime null
go

delete from bik_sapbo_extradata
go

exec sp_insert_into_bik_node_sapbo_extradata;
go

----------------------------------------
----------------------------------------
----------------------------------------

exec sp_update_version '1.1.0.5', '1.1.0.6';
go
