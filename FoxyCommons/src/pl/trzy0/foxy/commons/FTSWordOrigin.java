package pl.trzy0.foxy.commons;

public enum FTSWordOrigin {

    Original(1), Synonym(0.9), DictStemmed(0.8), AlgoStemmed(0.7), AccentFree(0.6);
    public final double weight;

    FTSWordOrigin(double weight) {
        this.weight = weight;
    }

    public int getOriginStrengthFlag() {
        return 1 << (FTSWordOrigin.values().length - 1 - ordinal());
    }
}
