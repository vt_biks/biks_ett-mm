/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import simplelib.BeanWithIntIdBase;

/**
 *
 * @author AMamcarz
 */
public class SystemUserBean extends BeanWithIntIdBase implements Serializable {

    public String loginName;
    //public String password;//hasło nigdy nie jest ściągane na klienta!!!!!
    public boolean isDisabled;
    public Integer userId; //bf:-->uzywane jeszcze w kilku miejscach gdzie sprawdzamy czy user jest userem spol.
    public String userName;
    public String avatar;
    public String systemUserName;
    public boolean isDomainUser;
    public String ssoDomain;
    public String ssoLogin;
    public boolean isSendMails;
    public boolean doNotFilterHtml;
    //---
    public Set<String> userRightRoleCodes;
    public Set<Integer> userAuthorTreeIDs;
    public Set<Integer> userCreatorTreeIDs;
    public Set<Integer> nonPublicUserTreeIDs;
    public Set<String> userAuthorBranchIds;
    public Set<String> userCreatorBranchIds;
    public Set<String> nonPublicUserBranchIds;
    public boolean userHasRightsInCurrentNode;

    public Set<Integer> userAuthorAllTreeIDs;//też te id gdy użytkownik nie ma uprawnienia do  całego drzewa tylko nodeId
    public Set<Integer> userCreatorAllTreeIDs;//też te id gdy użytkownik nie ma uprawnienia do  całego drzewa tylko nodeId
    //public String defaultDbName; // <-- tego tu nie powinno być - bez sensu tu (sesja to ew. właściwe miejsce)
    //---
//    public Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries;
    public Map<Integer, Set<TreeNodeBranchBean>> customRightRoleUserEntries;
    public Set<Integer> customRightRoleUserEntriesFromGroup;
    public List<Integer> usersGroupsIds;
}
