/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import simplelib.BeanWithIntIdBase;

/**
 *
 * @author wezyr
 */
public class AppPropBean extends BeanWithIntIdBase {

    public String name;
    public String val;
    public int isEditable;

    @Override
    public String toString() {
        return "AppPropBean{" + "name" /* I18N: no */ + "=" + name + "," + "val" /* I18N: no */ + "=" + val + ",isEditable=" + isEditable + '}';
    }
}
