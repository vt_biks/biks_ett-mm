/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.List;
import java.util.Map;
import pl.fovis.common.MultixReturnMessage;
import pl.fovis.common.MultixStatisticsBean;
import pl.fovis.common.StartupDataBean;
import pl.fovis.common.TokenType;
import pl.fovis.common.UserRegistrationDataBean;
import pl.fovis.common.mltx.DatabaseListBoxData;
import pl.fovis.common.mltx.IdName;
import pl.fovis.common.mltx.InvitationResult;
import pl.fovis.common.mltx.MultiRegistrationBean;
import pl.fovis.common.mltx.MultiXDatabaseTemplateBean;
import pl.fovis.common.mltx.MultixStatisticsAuthenticationBean;

/**
 *
 * @author pku
 */
@RemoteServiceRelativePath("boxiservice")
public interface MultiKSService extends RemoteService {

    public StartupDataBean getStartupData();

    public String sendConfirmationEmail(MultiRegistrationBean mrb, String localAccessUrl);

    public String sendInvitationEmail(String email, String role, String localAccessUrl);

    public String verifyTokenAndCreateDB(UserRegistrationDataBean token, Map<Integer, String> map);

    public List<MultiXDatabaseTemplateBean> getDatabaseTemplates();

    public String resetDB(boolean preserveInvited);

    public DatabaseListBoxData getAvailableDatabases();

    public MultixReturnMessage verifyToken(String token, TokenType tokenType);

    public List<MultixStatisticsBean> getMultiXStatistics(MultixStatisticsAuthenticationBean msab);

    public String sendResetPassword(String email, String localAccessUrl);

    public String resetPassword(String token, String password);

    public String verifyInvitationTokenAndCreateAccount(UserRegistrationDataBean registrationData, Map<Integer, String> map);

    public InvitationResult inspectUser(String token);

    public List<IdName> getQuestions();

    public Boolean authenticateUserForMultixStatistics(MultixStatisticsAuthenticationBean msab);
}
