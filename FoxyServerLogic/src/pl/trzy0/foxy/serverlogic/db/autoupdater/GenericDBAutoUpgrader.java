/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db.autoupdater;

import commonlib.LameUtils;
import java.io.File;
import java.io.FileFilter;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.SimpleDateUtils;

/**
 *
 * @author pmielanczuk
 */
public abstract class GenericDBAutoUpgrader {

//    public interface IExecutionLogger {
//
//        public void logMsg(String msg);
//    }
//    protected IExecutionLogger executionLogger;
    protected String appVer;
    //ww: zakładamy, że na końcu jest '/'
    protected String alterSqlsDir;
    protected IBeanConnectionEx<Object> conn;
    protected String prevDBVer;
    protected boolean showLogMessage = true;

    public GenericDBAutoUpgrader(String appVer, String alterSqlsDir, IBeanConnectionEx<Object> conn) {
        this.appVer = appVer;
        this.alterSqlsDir = BaseUtils.ensureDirSepPostfix4C(alterSqlsDir);
        this.conn = conn;
    }

    protected void logExecution(String msg) {
        if (showLogMessage) {
            System.out.println("<log-exec> [" + SimpleDateUtils.toCanonicalStringWithMillis(new Date()) + "]: " + msg);
        }
    }

    protected abstract String getCurrentDBVer();

    protected String getSQLFileNameMaskForDBVer(String dbVer) {
        return "alter db for v" + dbVer + " *.sql";
    }

    public String findProperAlterDBFile(String currentDBVer) {
        String fileMask = getSQLFileNameMaskForDBVer(currentDBVer);
        String regExprMask = LameUtils.fileNameMaskToRegexp(fileMask);

        logExecution("current dbVer=\"" + currentDBVer + "\" looking for sql alter file with mask=\""
                + fileMask + "\", reg-expr mask=\"" + regExprMask + "\"");

        final Pattern patt = Pattern.compile(regExprMask, Pattern.CASE_INSENSITIVE);

        File[] files = new File(alterSqlsDir).listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                if (!pathname.isFile()) {
                    return false;
                }
                String name = pathname.getName();
                return patt.matcher(name).matches();
            }
        });

        if (files == null || files.length == 0) {
            logExecution("no sql files found for alter db, dbVer=\"" + currentDBVer + "\"");
            return null;
        }
        if (files.length > 1) {
            List<String> fileNames = new LinkedList<String>();
            for (File f : files) {
                fileNames.add(f.getName());
            }
            logExecution("many sql files found for alter db, dbVer=\"" + currentDBVer + "\", files=" + fileNames);
            return null;
        }

        return files[0].getName();
    }

    //ww: jeżeli true, należy kontynuować następny przebieg
    protected boolean doOnePass() {
//        System.out.println("Starting onePass - before getCurrentDBVer()");
        String currentDBVer = getCurrentDBVer();
//        System.out.println("Starting onePass - after getCurrentDBVer()");

        if (BaseUtils.safeEquals(appVer, currentDBVer)) {
            //ww: nothing to do
            logExecution("current dbVer=\"" + currentDBVer + "\", db is already upgraded, nothing to do");
            return false;
        }

        if (BaseUtils.safeEquals(currentDBVer, prevDBVer)) {
            logExecution("current dbVer=\"" + currentDBVer + "\" is the same as prevDBVer - no upgrade in script file? ERROR!");
            return false;
        }

        String alterFileName = findProperAlterDBFile(currentDBVer);
        if (alterFileName == null) {
            return false;
        }

        logExecution("found alter-db-file for dbVer=\"" + currentDBVer + "\", fileName=\"" + alterFileName + "\"");

        String fileEncoding = getSqlFileEncoding();
        String alterScript = LameUtils.loadAsString(alterSqlsDir + alterFileName, fileEncoding);

        startAlterScriptForDbVer(currentDBVer);

        executeAlterScript(alterScript);

        logExecution("done alter-db-file for dbVer=\"" + currentDBVer + "\", fileName=\"" + alterFileName + "\"");

        prevDBVer = currentDBVer;

        return true;
    }

    protected void startAlterScriptForDbVer(String dbVer) {
        // no-op
    }

    protected void finishedAlterScriptForDbVer(String newDbVer) {
        // no-op
    }

    public void execute() {

        logExecution("starting for appVer=\"" + appVer + "\", dir=\"" + alterSqlsDir + "\"");

        conn.setAutoCommit(true);
        conn.setDbStatementExecutionWarnThresholdMillis(-1);

        while (doOnePass()) {
//            System.out.println("in exec, after onePass, before next pass");
            finishedAlterScriptForDbVer(getCurrentDBVer());
        }
    }

    protected void executeAlterScriptBatch(String batchText) {
        Exception outerEx = null;
        try {
            conn.execCommand(batchText);
        } catch (Exception ex) {
            outerEx = ex;
        }

        if (!conn.getAutoCommit()) {
            conn.finalizeTran(outerEx == null);
        }

        if (outerEx != null) {
            if (outerEx instanceof RuntimeException) {
                throw (RuntimeException) outerEx;
            }
            throw new LameRuntimeException("error running script batch", outerEx);
        }
    }

    protected void executeAlterScript(String alterScript) {
        executeAlterScriptBatch(alterScript);
    }

    // use LameUtils.getDefaultSystemEncoding() for system default
    protected String getSqlFileEncoding() {
        return "*cp1250";
    }
//    public static void performDBAutoUpgradeIfNeeded(String appVer, String alterSqlsDir,
//            Connection conn) {
//    }
//
//    public static void main(String[] args) {
//        Pattern p = Pattern.compile("ala ma kota");
//        System.out.println(p.matcher("ala ma kota").matches());
//    }
}
