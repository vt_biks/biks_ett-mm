/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs.valuehandlers;

import com.google.gwt.user.datepicker.client.DateBox;
import java.util.Date;

/**
 *
 * @author pmielanczuk
 */
public class DateBoxValueHandler implements IWidgetValueHandler<DateBox> {

    public boolean isWidgetClassHandled(Object widget) {
        return widget instanceof DateBox;
    }

    public Object getValueFromWidget(DateBox widget, Class targetValueClass, GetValueExtraInfo extraInfo) {
        return widget.getValue();
    }

    public void setValueToWidget(DateBox widget, Object value, SetValueExtraInfo extraInfo) {
        widget.setValue((Date)value);
    }
}
