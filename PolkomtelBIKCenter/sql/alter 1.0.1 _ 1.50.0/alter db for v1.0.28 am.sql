exec sp_check_version '1.0.28';
go
----------------------------------------------------------------------
----------------------------------------------------------------------

alter table bik_user
add activ int not null default 1 check (activ in(0,1));
go
----------------------------------------------------------------------
----------------------------------------------------------------------

exec sp_update_version '1.0.28', '1.0.29';
go