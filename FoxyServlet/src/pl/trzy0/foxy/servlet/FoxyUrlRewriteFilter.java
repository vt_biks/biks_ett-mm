/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import commonlib.ILameResourceProvider;
import commonlib.LameLoggersConfigReloader;
import commonlib.LameUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pl.trzy0.foxy.serverlogic.FoxyUrlRewriter;
import simplelib.*;
import simplelib.logging.ILameLogger;
import simplelib.logging.ILameLogger.LameLogLevel;

/**
 *
 * @author wezyr
 */
public class FoxyUrlRewriteFilter implements Filter {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    public static final String URLREWRITE_CONFIG_XML_IN_WEBINF = "/WEB-INF/urlrewrite.xml";
    protected FoxyUrlRewriter fur;
    protected IWatchedResourceFromProvider<InputStream> configRes;
    //protected IWatchedResourceFromProvider<InputStream> lameLoggersConfigResource;

    protected void reinitLameLoggers() {
        if (logger.isDebugEnabled()) {
            logger.debug("reinitLameLoggers!");
        }

        LameLoggersConfigReloader.reloadIfNeeded();
//        if (lameLoggersConfigResource.hasBeenModifiedRecently()) {
//            if (logger.isWarnEnabled()) {
//                logger.warn("reloading lameLoggersConfig");
//            }
//            //System.out.println("reloading lameLoggersConfig");
//            InputStream is = lameLoggersConfigResource.gainResource();
//            LameUtils.tryReadAndApplyLameLoggersConfig(is);
////            Properties lameLoggersConfig = LameUtils.readPropsFileFromInputStream(is,
////                    LAMELOGGERS_CONFIG_PROPERTIES_FILENAME, true);
////            if (lameLoggersConfig != null) {
////                LameLoggerFactory.applyLameLoggersConfig((Map) lameLoggersConfig, false);
////            }
//        } else {
//            if (logger.isDebugEnabled()) {
//                logger.debug("lameLoggersConfig not modified recently");
//            }
//        }
    }

    public void init(FilterConfig fc) throws ServletException {
        //fc.getServletContext()

        FoxyServletUtils.setupLameLoggersConfigReader(fc.getServletContext());
        
        ILameResourceProvider resProvider = new ServletResouceProvider(fc.getServletContext());

        //resProvider = new ServletResouceProvider(getServletContext());
//        lameLoggersConfigResource = new WatchedResourceFromProvider<InputStream>(
//                new ResourceFromProvider<InputStream>(resProvider, LAMELOGGERS_CONFIG_PROPERTIES_FILENAME));
//        LameLoggersConfigReloader.setLameLoggersConfigResource(new WatchedResourceFromProvider<InputStream>(
//                new ResourceFromProvider<InputStream>(resProvider, LAMELOGGERS_CONFIG_PROPERTIES_FILENAME)));
        //reinitLameLoggers();
        
        configRes = new WatchedResourceFromProvider<InputStream>(
                new ResourceFromProvider<InputStream>(resProvider, URLREWRITE_CONFIG_XML_IN_WEBINF));

        fur = new FoxyUrlRewriter(configRes, URLREWRITE_CONFIG_XML_IN_WEBINF);
    }

    private static String getRequestInfo(HttpServletRequest req) {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(Thread.currentThread().getName()).append("] ").append(req.getRequestURL());
        if (!BaseUtils.isStrEmptyOrWhiteSpace(req.getQueryString())) {
            sb.append("?").append(req.getQueryString());
        }
        sb.append(" cliIp:").append(req.getRemoteAddr()).append(", agent:").append(req.getHeader("user-agent"));
        return sb.toString();
    }

    protected boolean doFilterInternalInner(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String urlPath = req.getRequestURL().toString();
        String urlQry = req.getQueryString();
        String url = BaseUtils.isStrEmptyOrWhiteSpace(urlQry) ? urlPath : urlPath + "?" + urlQry;
        HttpSession s = req.getSession(false);
        boolean isNew = s == null ? false : s.isNew();
        boolean isFromCookie = req.isRequestedSessionIdFromCookie();
        String sId = s == null ? null : s.getId();
        if (logger.isDebugEnabled()) {
            logger.debug("%%%%%%%%%%%%%%%%% url=" + url + " for sId=" + sId);
        }
        if (req.isRequestedSessionIdFromURL()) {
            if (s != null) {
                s.invalidate();
            }
            if (logger.isWarnEnabled()) {
                logger.warn("session id=" + sId + " was from URL and invalidated!, "
                        + "isNew=" + isNew + ", isFromCookie=" + isFromCookie + ", url=" + url);
            }
        }

        Pair<String, String> p = BaseUtils.splitGlobalUrl(url);
        String contextPath = req.getContextPath();

        String localNoContextPath = BaseUtils.dropOptionalPrefix(p.v2, contextPath);

        String grpInDomain = null;
        String urlForRewrite;

        int pos = localNoContextPath.lastIndexOf('/');
        if (pos >= 0) {
            if (localNoContextPath.substring(pos + 1).startsWith("domGrp_")) {
                grpInDomain = localNoContextPath.substring(pos + 8);
                urlForRewrite = localNoContextPath.substring(0, pos);
            } else {
                urlForRewrite = localNoContextPath;
            }
        } else {
            urlForRewrite = localNoContextPath;
        }

        String rewrittenUrlPart = fur.rewriteIncomingUrl(urlForRewrite);
        String fixedRewritten;
        if (grpInDomain != null) {
            fixedRewritten = rewrittenUrlPart
                    + (rewrittenUrlPart.contains("?") ? "&" : "?")
                    + "domGrp=" + grpInDomain;
        } else {
            fixedRewritten = rewrittenUrlPart;
        }

        String finalUrl = p.v1 + (contextPath != null ? contextPath : "") + fixedRewritten;

        if (logger.isDebugEnabled()) {
            logger.debug("%%%%%%%%%%%%%%%%% url=" + url
                    + ", fixedRewritten=" + fixedRewritten
                    + ", rewrittenUrlPart=" + rewrittenUrlPart
                    + ", p.v1=" + p.v1 + ", p.v2=" + p.v2 + ", ctx=" + contextPath
                    + ", localNoContextPath=" + localNoContextPath
                    + ", finalUrl=" + finalUrl);
        }

        if (BaseUtils.safeEquals(finalUrl, url)) {
            return false;
        }

        RequestDispatcher rd = req.getRequestDispatcher(//finalUrl
                fixedRewritten);
        rd.forward(req, resp);

        return true;
    }

    public boolean doFilterInternal(ServletRequest sReq, ServletResponse sResp, FilterChain fc) throws IOException, ServletException {
        reinitLameLoggers();

        boolean processed;
        try {
            if (sReq instanceof HttpServletRequest && sResp instanceof HttpServletResponse) {
                processed = doFilterInternalInner((HttpServletRequest) sReq, (HttpServletResponse) sResp);

                if (logger.isDebugEnabled()) {
                    logger.debug("%%%%%%%%%%%%%%%%%% request was " + (processed ? "" : "not ") + "processed by urlRewriter");
                }
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("%%%%%%%%%%%%%%%%%%% sReq.class=" + BaseUtils.safeGetClassName(sReq)
                            + ", sResp.class=" + BaseUtils.safeGetClassName(sResp));
                }
                processed = false;
            }

            if (!processed) {
                fc.doFilter(sReq, sResp);
            }
        } catch (ServletException ex) {
            notifyException(ex);
            throw ex;
        } catch (IOException ex) {
            notifyException(ex);
            throw ex;
        } catch (Throwable ex) {
            notifyException(ex);
            throw new LameRuntimeException("internalError", ex);
        }
        return processed;
    }

    //synchronized
    public void doFilter(ServletRequest sReq, ServletResponse sResp, FilterChain fc) throws IOException, ServletException {
        Date startTime = new Date();
        Boolean processed = null;
        try {
            processed = doFilterInternal(sReq, sResp, fc);
        } finally {
            Date endTime = new Date();
            String ps;
            LameLogLevel lvl;
            if (processed == null) {
                ps = "error filtering";
                lvl = LameLogLevel.Warn;
            } else {
                ps = processed ? "processed" : "passed through";
                lvl = processed ? LameLogLevel.Info : LameLogLevel.Debug;
            }

            if (logger.isEnabledAtLevel(lvl)) {

                String reqInfo;
                if (sReq instanceof HttpServletRequest) {
                    HttpServletRequest r = (HttpServletRequest) sReq;
                    reqInfo = getRequestInfo(r);
                } else {
                    reqInfo = "req of class: " + BaseUtils.safeGetClassName(sReq);
                }
                logger.logAtLevel(lvl, ps + " in " + SimpleDateUtils.getElapsedMillis(startTime, endTime) + " millis. "
                        + reqInfo);
            }
        }
    }

    public void destroy() {
        //
    }

    private void notifyException(Throwable ex) {
        String dateTimeStr = "[" + SimpleDateUtils.toCanonicalString(new Date(), true) + "]: ";
        System.err.println(dateTimeStr + "internalError on err: " + ex);
        //System.err.println("!!! error - error 1 !!!");
        //ex.printStackTrace();
        logger.error("internalError on logger", ex);
        //System.out.println(dateTimeStr + "internalError on out: " + ex);
        //ex.printStackTrace();
        //System.err.println("!!! error - after error !!!");
    }
}
