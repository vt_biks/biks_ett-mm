/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author wezyr
 */
public class LoginDialog extends BaseActionOrCancelDialog {

    private LoginForm form;

    @Override
    protected String getDialogCaption() {
        return I18n.logowanie.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zaloguj.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        form.buildMainWidgets();
        main.add(form);
    }

    @Override
    protected boolean doActionBeforeClose() {
        actionBtn.setEnabled(false);
        form.onLoginClick();
        return false;
    }

    @Override
    protected void doAction() {
        throw new IllegalStateException(I18n.toSieNiePowinnoWywolac.get() /* I18N:  */);
    }

    @Override
    public void buildAndShowDialog(/*IContinuation afterProperLoginCont*/) {
        this.form = new LoginForm(/*afterProperLoginCont,*/new IParametrizedContinuation<Boolean>() {

                    @Override
                    public void doIt(Boolean isSuccess) {
                        if (isSuccess) {
                            hideDialog();
                        } else {
                            actionBtn.setEnabled(true);
                        }
                    }
                });
        super.buildAndShowDialog();
    }

    @Override
    protected void tryFocusFirstWidget() {
        form.getWidgetToFocus().setFocus(true);
    }
}
