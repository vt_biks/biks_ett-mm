/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill;

/**
 *
 * @author wezyr
 */
public interface IPageRedirectionCapableModel {
    public void sendRedirect(String url);
}
