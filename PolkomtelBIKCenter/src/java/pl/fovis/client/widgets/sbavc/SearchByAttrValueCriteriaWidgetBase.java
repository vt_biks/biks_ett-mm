/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.widgets.sbavc;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog.AttributeType;
import pl.fovis.common.AttrSearchableBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public abstract class SearchByAttrValueCriteriaWidgetBase implements ISearchByAttrValueCriteriaWidget {

    protected FlexTable row = new FlexTable();
    protected ListBox attributesListbox = new ListBox();
    protected ListBox criteriaLbx = new ListBox();
    protected CheckBox negationChbx = new CheckBox(I18n.nie3.get());
    protected SearchByAttrValueCriteriaManager manager;
    protected Label attrIdLbl = new Label();
    protected ISearchByAttrValueCriteria bean;

    @Override
    public void setup(final SearchByAttrValueCriteriaManager manager, List<AttrSearchableBean> attrs) {
        this.manager = manager;
        for (AttrSearchableBean oneAttr : attrs) {
            attributesListbox.addItem(oneAttr.caption, String.valueOf(oneAttr.id));
        }
        attributesListbox.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                createOptWidget();
            }
        });
    }

    // wykorzystywane do ustawiania wartosci na widgetach bazowych
    @Override
    public void populateWidgets(int attrId, boolean isNegation, int criteriumIndex) {
        for (int i = 0; i < attributesListbox.getItemCount(); i++) {
            String attrIdAsString = attributesListbox.getValue(i);
            if (attrId == BaseUtils.tryParseInt(attrIdAsString)) {
                attributesListbox.setItemSelected(i, true);
                break;
            }
        }
        negationChbx.setValue(isNegation);
        criteriaLbx.setSelectedIndex(criteriumIndex);
        createOptWidget();
    }

    @Override
    public Widget getWidget() {
        if (row.getRowCount() == 0) {
            row.clear();
            row.insertRow(0);
            createBaseWidget();
            createCriteriaLbx();
            createOptWidget();
            createCloseWidget();
        }
        return row;
    }

    protected void createBaseWidget() {
        row.setWidget(0, 0, attrIdLbl);
        row.setWidget(0, 1, attributesListbox);
        row.setWidget(0, 2, negationChbx);
    }

    /**
     * Funkcja odświeża listę atrybutów, zaznacza atrybut wcześniej wybrany
     * @param attrs - lista dostępnych atrybutów
     * @return true - jeśli wiersz trzeba usunąć, bo atrybut już nie jest dostępy
     *         false - w pp.
     */
    @Override
    public boolean populateAttributeListBox(List<AttrSearchableBean> attrs) {
        Integer oldAttrId = getSelectedAttrId();
        attributesListbox.clear();
        boolean lackOfAttribute = true;
        for (AttrSearchableBean oneAttr : attrs) {
            attributesListbox.addItem(oneAttr.caption, String.valueOf(oneAttr.id));
            if (BaseUtils.safeEquals(oneAttr.id, oldAttrId)) {
                attributesListbox.setItemSelected(attributesListbox.getItemCount() - 1, true);
                lackOfAttribute = false;
            }
        }
        return lackOfAttribute;
    }

    @Override
    public int getSelectedAttrId() {
        int selectedIndex = attributesListbox.getSelectedIndex();
        return BaseUtils.tryParseInt(attributesListbox.getValue(selectedIndex == -1 ? 0 : selectedIndex));
    }

    @Override
    public boolean isNegation() {
        return negationChbx.getValue();
    }

    @Override
    public int getCriteriumIndex() {
        int selectedIndex = criteriaLbx.getSelectedIndex();
        return selectedIndex == -1 ? 0 : selectedIndex;
    }

    protected String getSelectedAttrCaption() {
        int selectedIndex = attributesListbox.getSelectedIndex();
        return attributesListbox.getItemText(selectedIndex == -1 ? 0 : selectedIndex);
    }

    protected abstract void createOptWidget();

    protected void createCloseWidget() {
        int cellCount = row.getCellCount(0);
        Image removeBtn = new Image("images/delete.png");
        removeBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                manager.deleteRow(SearchByAttrValueCriteriaWidgetBase.this);
            }
        });
        row.setWidget(0, cellCount, removeBtn);
    }

    protected void createCriteriaLbx() {
        criteriaLbx.clear();
        Collection<String> availableCriteria = SearchByAttrValueCriteriaManager.getAvailableCriteria();
        for (String criterium : availableCriteria) {
            criteriaLbx.addItem(criterium);
        }
        criteriaLbx.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                manager.changeCriterium(SearchByAttrValueCriteriaWidgetBase.this, criteriaLbx.getSelectedIndex());
            }
        });
        row.setWidget(0, row.getCellCount(0), criteriaLbx);
    }

    @Override
    public void setCriteriaId(int id) {
        attrIdLbl.setText(BaseUtils.createPlaceHolder(id, BIKConstants.OPEN_PLACEHOLDER, BIKConstants.CLOSE_PLACEHOLDER));
    }

    protected Widget createWidgetBox() {
        String typeAttr = manager.getAttrSearchableType(getSelectedAttrId());
        if (AttributeType.data.name().equals(typeAttr)) {
            DateTimeFormat dateFormat = DateTimeFormat.getFormat("yyyy-MM-dd 00:00:00");
            DateBox dateBox = new DateBox();
            dateBox.setFormat(new DateBox.DefaultFormat(dateFormat));
            dateBox.getDatePicker().setYearArrowsVisible(true);
            return dateBox;
        }
        if (AttributeType.combobox.name().equals(typeAttr)
                || AttributeType.comboBooleanBox.name().equals(typeAttr)
                || AttributeType.checkbox.name().equals(typeAttr)) {
            ListBox listBox = new ListBox();
            List<String> values = BaseUtils.splitBySep(manager.getAttrValueOpt(getSelectedAttrId()), ",");
            for (String item : values) {
                listBox.addItem(item);
            }
            listBox.setMultipleSelect(criteriaLbx.getItemText(criteriaLbx.getSelectedIndex()).equals(I18n.singleValueAttributeIn.get())
                    && AttributeType.checkbox.name().equals(typeAttr));
            return listBox;
        }
        return new TextBox();
    }

    protected String getWidgetValueAsString(Widget w) {
        if (w instanceof DateBox) {
            DateBox dateBox = (DateBox) w;
            return dateBox.getTextBox().getText();
        }
        if (w instanceof ListBox) {
            ListBox listBox = (ListBox) w;
            List<String> selectedValues = new ArrayList<String>();
            for (int i = 0, l = listBox.getItemCount(); i < l; i++) {
                if (listBox.isItemSelected(i)) {
                    selectedValues.add(listBox.getValue(i));
                }
            }
            return BaseUtils.mergeWithSepEx(selectedValues, ",");
        }
        TextBox textBox = (TextBox) w;
        return textBox.getText();
    }

    protected boolean isValidInput() {
        return true;
    }
}
