-- nadanie uprawnie� Administratora Systemu
declare @login_name varchar(200) = 'mtararuj+newuser@bssg.pl' -- dla jakiego loginu chcemy nada� uprawnienia ADMIN

declare @sys_user_id int = (select id from bik_system_user where login_name = @login_name)

-- dodanie roli administratora - wa�ne aby mie� ostateczny tryb edycji prawie wszystkiego (UltimateEditMode, UEM)
insert into bik_user_right (user_id, right_id, tree_id) values
(@sys_user_id, (select id from bik_right_role where code = 'Administrator'), null)
go

---------------------------------

-- w��czenie demoMode - konieczne dla UEM
update bik_app_prop set val = '1' where name = 'isDemoVersion'

-- w��czenie developerMode - wa�ne aby mie� UEM
update bik_app_prop set val = '1' where name = 'developerMode'
go

---------------------------------

-- wy��czenie developerMode - wa�ne: nie b�dzie UEM, ale zniknie paj�k krzy�ak ([+]) w prawym rogu paska menu
update bik_app_prop set val = '0' where name = 'developerMode'
go

---------------------------------

-- zmieniamy nazw� (login) u�ytkownika - tymi komendami pozmieniasz nazwy u�ytkownik�w - np. swoj� na kr�tsz� (Demo)
update bik_system_user 
set login_name = 'Demo' -- tu podajemy nowy login, musi by� unikalny
where login_name = 'mtararuj+newuser@bssg.pl'
go

---------------------------------

-- zabranie uprawnie� u�ytkownikowi Demo - wykona� na koniec, gdy ju� wszystko inne zrobione!
declare @login_name varchar(200) = 'Demo' -- dla jakiego loginu chcemy zabra� uprawnienia 

declare @sys_user_id int = (select id from bik_system_user where login_name = @login_name)

delete from bik_user_right where user_id = @sys_user_id
go

---------------------------------

/* 
   -- pomocniczy skrypt (zakomentowany) - nie wykonywa� tego! 
   -- zostawiam tutaj, bo kiedy� mo�e si� przyda� ;-)
   -- s�u�y do wyznaczenia tabel powi�zanych z bik_system_user
select 'update ' + Table_Name + ' set ' + Name + ' = @sys_user_id_to_intercept_objects where ' 
  + name + ' = @sys_user_id_to_delete' as upd_txt, *
from (
select 
cstr.name as [ForeignKey_Name],
fk.constraint_column_id as [ID] ,
tbl.name as [Table_Name] ,
cfk.name as [Name] ,
reftab.name As ReferencedTableName,
crk.name as [ReferencedColumn]
from sys.tables as tbl
inner join sys.foreign_keys as cstr
on cstr.parent_object_id=tbl.object_id
inner join sys.foreign_key_columns as fk
on fk.constraint_object_id=cstr.object_id
inner join sys.columns as cfk
on fk.parent_column_id = cfk.column_id
and fk.parent_object_id = cfk.object_id
inner join sys.columns as crk
on fk.referenced_column_id = crk.column_id
and fk.referenced_object_id = crk.object_id
inner join sys.tables reftab on reftab.object_id = cstr.referenced_object_id
) xxx
where ReferencedTableName = 'bik_system_user'
*/

---------------------------------
-- usuwamy u�ytkownika - to jest d�u�szy skrypt, wiele komend

-- znale�� u�ytkownik�w do usuni�cia mo�na tak:
-- select * from bik_system_user where login_name like 'paw%'
-- select * from bik_system_user where login_name like 'pm%'
declare @login_name_to_delete varchar(200) = 'pawel.nizialek' -- tego chcemy usun��
declare @login_name_to_intercept_objects varchar(200) = 'monika@bssg.pl' -- ten u�ytkownik przejmie cz�� obiekt�w od usuwanego

declare @sys_user_id_to_delete int = (select id from bik_system_user where login_name = @login_name_to_delete)
declare @sys_user_id_to_intercept_objects int = (select id from bik_system_user where login_name = @login_name_to_intercept_objects)

-- cz�� info usuwamy
delete from bik_node_author where user_id = @sys_user_id_to_delete
delete from bik_specialist where proj_id = @sys_user_id_to_delete
  or spon_id = @sys_user_id_to_delete or expe_id = @sys_user_id_to_delete  
delete from bik_lisa_logs where system_user_id = @sys_user_id_to_delete
delete from bik_node_vote where user_id = @sys_user_id_to_delete
delete from bik_authors where user_id = @sys_user_id_to_delete
delete from bik_news_readed_user where user_id = @sys_user_id_to_delete
delete from bik_statistic_ext where user_id = @sys_user_id_to_delete
delete from bik_statistic where user_id = @sys_user_id_to_delete
delete from bik_user_right where user_id = @sys_user_id_to_delete
delete from bik_tutorial_readed_user where user_id = @sys_user_id_to_delete

-- cz�� przepisujemy na innego u�ytkownika
update bik_news set author_id = @sys_user_id_to_intercept_objects where author_id = @sys_user_id_to_delete
update bik_blog set user_id = @sys_user_id_to_intercept_objects where user_id = @sys_user_id_to_delete
update bik_metapedia set user_id = @sys_user_id_to_intercept_objects where user_id = @sys_user_id_to_delete
update bik_note set user_id = @sys_user_id_to_intercept_objects where user_id = @sys_user_id_to_delete
update bik_biz_def_body set user_id = @sys_user_id_to_intercept_objects where user_id = @sys_user_id_to_delete

-- teraz mo�na usun�� u�ytkownika z podanym @login_name_to_delete
delete from bik_system_user where id = @sys_user_id_to_delete
go

-- KONIEC: usuwamy u�ytkownika - to jest d�u�szy skrypt, wiele komend
---------------------------------

