﻿exec sp_check_version '1.7.z5';
go

--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

/*
drop table bik_fts_word;

*/

if not exists(select * from sys.objects where object_id = object_id(N'bik_fts_word') and type in (N'U'))
  create table bik_fts_word (
    id int not null identity primary key, 
    word varchar(800) not null unique/*,
    origin_flag int not null,
    occurence_cnt int not null,
    obj_cnt int not null,
    max_weight decimal(9,8) not null*/
  );
go


/*
  drop table bik_fts_word_in_obj;
  go

  create table bik_fts_word_in_obj (
    id int not null identity primary key, 
    obj_id int not null, 
    word_id int not null, 
    origin_flag int not null,
    occurence_cnt int not null,
    attr_cnt int not null,
    max_weight decimal(9,8) not null,
    unique (obj_id, word_id)
  );
  go
*/

/*
drop table bik_fts_attr;
*/


if not exists(select * from sys.objects where object_id = object_id(N'bik_fts_attr') and type in (N'U'))
  create table bik_fts_attr (
    id int not null identity primary key, 
    name varchar(800) not null unique,
    weight decimal(9,8) not null
  );
go


/*
drop table bik_fts_word_in_obj_attr;
*/

if not exists(select * from sys.objects where object_id = object_id(N'bik_fts_word_in_obj_attr') and type in (N'U'))
  create table bik_fts_word_in_obj_attr (
    id int not null identity primary key, 
    obj_id int not null, 
    attr_id int not null,
    word_id int not null,
    original_cnt int not null,
    synonym_cnt int not null,
    dictstemmed_cnt int not null,
    algostemmed_cnt int not null,
    accentfree_cnt int not null          
    /*, 
    origin_flag int not null,
    occurence_cnt int not null,
    max_weight decimal(9,8) not null*/,
    unique (obj_id, attr_id, word_id)
  );
go


/*
drop table bik_fts_processed_obj;
*/


if not exists(select * from sys.objects where object_id = object_id(N'bik_fts_processed_obj') and type in (N'U'))
  create table bik_fts_processed_obj (id int not null primary key);
go


/*
drop table bik_fts_stemmed_word;
*/


if not exists(select * from sys.objects where object_id = object_id(N'bik_fts_stemmed_word') and type in (N'U'))
  create table bik_fts_stemmed_word (
    id int not null identity primary key, 
    word_id int not null,
    word varchar(800) not null,
    origin_flag int not null,
    weight decimal(9,8) not null default 1.0,
    unique (word_id, word)
  );
go



if not exists(select * from sys.objects where object_id = object_id(N'bik_fts_word_stemming') and type in (N'U'))
  create table bik_fts_word_stemming (
	  [id] [int] identity(1,1) primary key not null,
	  [word_id] [int] not null,
	  [stemmed_word_id] [int] not null,
	  [origin_flag] [int] not null
  );
go



--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------


merge into bik_app_prop ap 
using (values ('suggestSimilarNodes', '0')) as v(name, val) on ap.name = v.name
when matched then update set ap.val = v.val
when not matched by target then insert (name, val) values (v.name, v.val);
go


--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

exec sp_update_version '1.7.z5', '1.7.z5.1';
go
