/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.common.TutorialBean;
import simplelib.IContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author pmielanczuk
 */
public class EditTutorialDialog extends BaseRichTextAndTextboxDialog {

    protected TutorialBean tutorialBean;
    protected IContinuation saveCont;

    @Override
    protected void innerDoAction(String title, String body) {
        tutorialBean.title = title;
        tutorialBean.text = body;
        saveCont.doIt();
    }

    @Override
    protected String getDialogCaption() {
        return I18n.editTutorial.get() /* I18N:  */;
    }

    public void buildAndShowDialog(TutorialBean tutorialBean, IContinuation saveCont) {
        this.tutorialBean = tutorialBean;
        this.saveCont = saveCont;
        super.buildAndShowDialog(tutorialBean.title, tutorialBean.text);
    }
}
