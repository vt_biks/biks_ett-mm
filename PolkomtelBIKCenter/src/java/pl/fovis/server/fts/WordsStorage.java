/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.common.fts.AttributeBean;
import pl.fovis.common.fts.SimilarWordsWithCounts;
import pl.fovis.common.fts.StemmedWordBean;
import pl.fovis.common.fts.WordOccurrencesInOneAttr;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.FieldNameConversion;
import pl.trzy0.foxy.commons.NamedSqlCallParams;
import simplelib.ILameCollector;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 *
 * WordsStorage nie interesuje się odmianami słów, dostaje słowa z wagami
 * (zależnymi od tego, z którego pochodzą źródła - w sensie - odmiany/stemming)
 * i tylko wrzuca je do "bazy".
 *
 * Jednyne co jest ważne tutaj, to z jakiego atrybuty pochodzą słowa, bo od tego
 * zależy ich druga waga. Przykładowo - tagi mogą mieć bardzo dużą wagę w
 * oryginale, ale odmienione już mniejszą. Podobnie nazwa obiektu - jeżeli słowo
 * pojawia się w nazwie, to może znaczyć więcej niż np. w treści (description).
 *
 */
public class WordsStorage {

    protected static final double WEIGHT_FOR_NEW_ATTR = 0.9;
    protected INamedSqlsDAO<Object> adhocDao;
    protected Map<String, AttributeBean> attrByNameMap;
    protected boolean isTempTableCreated;
    protected boolean initStoragePerformed;

    public WordsStorage(INamedSqlsDAO<Object> adhocDao, boolean initStorage) {
        this.adhocDao = adhocDao;
        readAttrs();
        if (initStorage) {
            initStorage();
        }
    }

    public final void initStorage() {
        adhocDao.execNamedCommand("fts_initWordsStorage");
        initStoragePerformed = true;
    }

    private void readAttrs() {
        List<AttributeBean> attrs = adhocDao.createBeansFromNamedQry("fts_readAttrs", AttributeBean.class);
        attrByNameMap = LameUtils.projectBeanCollToMapByName(attrs);
    }

    public void deleteObjectData(int objId) {
        adhocDao.execNamedCommand("fts_deleteObjectData", objId);
    }

//    public List<Integer> getWordIds(Collection<String> words) {
//        List<Integer> res = new ArrayList<>();
//        if (BaseUtils.isCollectionEmpty(words)) {
//            return res;
//        }
//
//        String sqlTxt = "select id from bik_fts_word where word in ("
//                + conn.toSqlString(words) + ")";
//
//        List<Map<String, Object>> idsMap = conn.execQry(sqlTxt);
//
//        for (Map<String, Object> row : idsMap) {
//            res.add(conn.sqlNumberToInt(row.get("id")));
//        }
//
//        return res;
//    }
    public double getAttrWeight(String attrName) {

        AttributeBean ab = attrByNameMap.get(attrName);

        return ab == null ? WEIGHT_FOR_NEW_ATTR : ab.weight;
    }

    public Map<Integer, String> processObject(int objId, Map<String, List<WordOccurrencesInOneAttr>> wordsMap) {
        if (!initStoragePerformed) {
            deleteObjectData(objId);
        }

        return connectObjectWithWords(objId, wordsMap);
    }

    protected String attrNamesToIdsStr(Collection<String> attrNames) {
        StringBuilder sb = new StringBuilder();

        boolean isFirst = true;

        for (String attrName : attrNames) {

            if (isFirst) {
                isFirst = false;
            } else {
                sb.append(",");
            }

            sb.append(attrByNameMap.get(attrName).id);
        }

        return sb.toString();
    }

    protected Map<Integer, String> connectObjectWithWords(int objId, Map<String, List<WordOccurrencesInOneAttr>> wordsMap) {

        final Map<Integer, String> res = new HashMap<Integer, String>();

        if (wordsMap.isEmpty()) {
            return res;
        }

        Set<String> newAttrNames = new HashSet<String>(wordsMap.keySet());

        newAttrNames.removeAll(attrByNameMap.keySet());

        if (!newAttrNames.isEmpty()) {
            List<AttributeBean> newAttrs = adhocDao.createBeansFromNamedQry("fts_addAttrs",
                    AttributeBean.class, newAttrNames, WEIGHT_FOR_NEW_ATTR);

            attrByNameMap.putAll(LameUtils.projectBeanCollToMapByName(newAttrs));
        }

        List<WordOccurrencesInOneAttr> words = new ArrayList<WordOccurrencesInOneAttr>();

        for (Entry<String, List<WordOccurrencesInOneAttr>> e : wordsMap.entrySet()) {

            String attrName = e.getKey();
            int attrId = attrByNameMap.get(attrName).id;

            final List<WordOccurrencesInOneAttr> wc = e.getValue();

            for (WordOccurrencesInOneAttr w : wc) {
                w.attrId = attrId;
            }

            words.addAll(wc);
        }

//        System.out.println("accWords=" + wordsMap);
//        System.out.println("words=" + words);
//        System.out.println("sql to execute:\n"
//                + adhocDao.provideNamedSqlText("updateWordsForObj", objId, words)
//        );
        if (!isTempTableCreated) {
            adhocDao.execNamedCommand("fts_createWordTempTable");
            adhocDao.execNamedCommand("fts_createAddedNewWordTempTable");
            isTempTableCreated = true;
        }

        final int max_words_pack = 1000;

        int wordCnt = words.size();

        if (wordCnt > max_words_pack) {
            List<WordOccurrencesInOneAttr> subPack;

            int i = 0, j = max_words_pack;
            while (j < wordCnt) {
                subPack = words.subList(i, j);
                adhocDao.execNamedCommand("fts_insertWordsIntoTmpTab", subPack);
                i = j;
                j += max_words_pack;
            }

            words = words.subList(i, words.size());
        }

        adhocDao.execNamedQueryExCFN("fts_updateWordsForObj", false, false, new ILameCollector<Map<String, Object>>() {

            @Override
            public void add(Map<String, Object> item) {
                res.put(adhocDao.sqlNumberToInt(item.get("id")), (String) item.get("word"));
            }
        }, FieldNameConversion.ToLowerCase, objId, words);

        return res;
    }

    public void addStemmedWords(List<StemmedWordBean> words) {
        final int max_words_pack = 1000;

        int wordCnt = words.size();

        int i = 0, j = max_words_pack;
        while (i < wordCnt) {

            if (j > wordCnt) {
                j = wordCnt;
            }

            List<StemmedWordBean> subPack = words.subList(i, j);
            adhocDao.execNamedCommand("fts_addStemmedWords", subPack);
            i = j;
            j += max_words_pack;
        }

        adhocDao.execNamedCommand("fts_addStemmedWordPairs");
    }

    public List<Pair<Integer, Double>> findSimilarObjects(NamedSqlCallParams searchWordsAndWeightsClause,
            NamedSqlCallParams filterOutKnownSimilaritiesClause,
            double minRelevance, int maxResults) {

        final List<Pair<Integer, Double>> res = new ArrayList<Pair<Integer, Double>>();

        adhocDao.execNamedQueryExCFN("fts_findSimilarObjects", false, false, new ILameCollector<Map<String, Object>>() {

            @Override
            public void add(Map<String, Object> item) {
                res.add(new Pair<Integer, Double>(adhocDao.sqlNumberToInt(item.get("obj_id")), ((Number) item.get("relevance")).doubleValue()));
            }
        }, FieldNameConversion.ToLowerCase, searchWordsAndWeightsClause,
                filterOutKnownSimilaritiesClause, minRelevance, maxResults);

        return res;
    }

    public List<Pair<Integer, Double>> findSimilarObjects(int objId,
            NamedSqlCallParams filterOutKnownSimilaritiesClause,
            double minRelevance, int maxResults) {

        adhocDao.execNamedCommand("fts_createSearchTempTable");
        adhocDao.execNamedCommand("fts_addSearchWordsForObj", objId);

        return findSimilarObjects(new NamedSqlCallParams("fts_searchWordsAndWeightsClauseByTempTable"),
                filterOutKnownSimilaritiesClause, minRelevance, maxResults);
    }

    public List<SimilarWordsWithCounts> getSimilarWordsWithCounts(NamedSqlCallParams searchWordsAndWeightsClause,
            NamedSqlCallParams filterOutKnownSimilaritiesClause) {

        return adhocDao.createBeansFromNamedQry("fts_getSimilarWordsWithCounts", SimilarWordsWithCounts.class,
                searchWordsAndWeightsClause, filterOutKnownSimilaritiesClause);
    }

    public List<SimilarWordsWithCounts> getSimilarWordsWithCountsOnExistingSearchWords(int objId,
            NamedSqlCallParams filterOutKnownSimilaritiesClause) {

        return getSimilarWordsWithCounts(new NamedSqlCallParams("fts_searchWordsAndWeightsClauseByTempTable"),
                filterOutKnownSimilaritiesClause);
    }

    public List<SimilarWordsWithCounts> getSimilarWordsWithCounts(int objId,
            NamedSqlCallParams filterOutKnownSimilaritiesClause) {
        adhocDao.execNamedCommand("fts_createSearchTempTable");
        adhocDao.execNamedCommand("fts_addSearchWordsForObj", objId);

        return getSimilarWordsWithCountsOnExistingSearchWords(objId, filterOutKnownSimilaritiesClause);
    }

//    public static void main(String[] args) {
//        System.out.println(System.getProperties());
//        System.out.println(System.getenv());
//        System.out.println(LameUtils.getMemoryDiagVals(false));
//    }
    public void changeWordWeight(String word, double weight) {
        adhocDao.execNamedCommand("fts_changeWordWeight", word, weight);
    }
}
