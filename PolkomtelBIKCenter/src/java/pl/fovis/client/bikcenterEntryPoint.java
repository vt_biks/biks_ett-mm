/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.bikpages.ConfluenceTreeBikPage;
import pl.fovis.client.bikpages.IBikPage;
import pl.fovis.client.bikpages.LoginPageBase;
import pl.fovis.client.bikpages.SearchPage;
import pl.fovis.client.bikpages.TestTreeBikPage;
import pl.fovis.client.bikpages.TreeBikPage;
import pl.fovis.client.dialogs.HelpDialog;
import pl.fovis.client.menu.IMenuItem;
import pl.fovis.client.menu.MenuBuilder;
import pl.fovis.client.menu.MenuItemMainWidget;
import pl.fovis.client.menu.MenuItemSubMenu;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKCenterUtils.MenuNodeBeanType;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.ChangedRankingBean;
import pl.fovis.common.HelpBean;
import pl.fovis.common.MenuExtender.MenuBottomLevel;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.StartupDataBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeBrokerForMenuNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.ITreeBottomUpBuilder;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.TreeAlgorithms;
import pl.trzy0.foxy.commons.FoxyCommonUtils;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Pair;

/**
 * Main entry point.
 *
 * @author wezyr
 */
public class bikcenterEntryPoint implements IAutoScrollTopMenu {

//    static {
//        InitFactories4GWT.initFactories();
//    }
    public class TreeBottomUpBuilderForMenuNodeBean extends TreeBrokerForMenuNodeBean implements ITreeBottomUpBuilder<MenuNodeBean, IMenuItem, Integer> {

        protected Map<String, IBikPage<Integer>> staticPageMap = new HashMap<String, IBikPage<Integer>>();

        public TreeBottomUpBuilderForMenuNodeBean() {
            IBikPageCreator biksPageCreator = GWT.create(IBikPageCreator.class);

            for (String pageName : biksPageCreator.getNames()) {
                IBikPage<Integer> page = biksPageCreator.create(pageName);
                staticPageMap.put(page.getId(), page);
            }
        }

        @Override
        public IMenuItem createTreeNode(MenuNodeBean dataNode, List<IMenuItem> childTreeNodes) {
            String code = dataNode.code;
//                System.out.println("createTreeNode: code=[" + code + "]");
//                if (BaseUtils.isStrEmpty(code)) {
//                    return null;
//                }

            MenuNodeBeanType type = BIKCenterUtils.getMenuNodeBeanType(dataNode); //BaseUtils.isStrEmpty(code) ? ' ' : code.charAt(0);
            IBikPage<Integer> page;
            if (type == MenuNodeBeanType.CustomPage) {
                String tabId = code.substring(1);
                page = staticPageMap.get(tabId);
                //ww: zakładamy, że MyBIKS zawsze jest!
                if (BIKGWTConstants.TAB_ID_MYBIKS.equals(tabId)) {
                    defaultTabOnStart = page;
                }
            } else {
                Integer treeId = dataNode.treeId;
                if (treeId != null) {
                    page = buildBikPageForTree(BIKClientSingletons.getTreeBeanById(treeId));
                } else {
                    return new MenuItemSubMenu(dataNode.name, childTreeNodes);
                }
            }
            return new MenuItemMainWidget(dataNode.name, page);
        }
    }

    public bikcenterEntryPoint() {
        Window.addResizeHandler(new ResizeHandler() {

            @Override
            public void onResize(ResizeEvent event) {
                setVisibleDropDownMenu();
            }
        });
    }

    protected boolean badVersionAlertHasBeenShown = false;
    protected boolean showErrorForBadVersion = false;
    protected static final String BAD_VER_MSG = I18n.naSerwerzeZainstalJestInnaWersja.get() /* I18N:  */ + ".";
    protected IBikPage<Integer> defaultTabOnStart;
    protected InlineLabel appVersionInfoLbl;
    private Element menuWrapper = null;
    private Element dropDownMenu = null;
    private Element topMenu = null;
    private MenuBuilder menuBuilder;
    private final FlowPanel dlp = new FlowPanel();
    private List<IMenuItem> rootMenuItems;
    private Map<Element, IMenuItem> itemElementMenuMap = new HashMap<Element, IMenuItem>();
//    protected Map<String, IMenuItem> treeMenu;
//    protected BISystemsConnectionsParametersBean biSystems;

//    @Override
//    protected void onSetLangFailure(Throwable cause) {
//        onInitializationFailure(cause, "setLang" /* i18n: no */);
//    }
    public static void onInitializationFailure(Throwable cause, final String location) {
        BIKClientSingletons.getVersionInfoService().getAppVersion(new StandardAsyncCallback<Pair<String, String>>() {
            protected void onSth(String appVersion, String dbVersion, boolean isOnSuccess) {
                String fullLocation = location + (isOnSuccess ? "" : "+getVersionInfo" /* i18n: no */);
                checkAppAndDBVersions(appVersion, dbVersion, fullLocation);

                //ww: zbędne, bo gdy location != null to już samo checkAppAndDBVersions pokaże błąd
                //BIKClientSingletons.showWarning("Failure on: "/* i18n: no */ + fullLocation + "!");
            }

            @Override
            public void onSuccess(Pair<String, String> result) {
                String paramVal = Window.Location.getParameter("r");

                if (BaseUtils.isStrEmptyOrWhiteSpace(paramVal)) {
                    UrlBuilder ub = Window.Location.createUrlBuilder();
                    ub.setParameter("r", "1");
                    Window.Location.replace(ub.buildString());
                    return;
                }

                onSth(result.v1, result.v2, true);
            }

            @Override
            public void onFailure(Throwable cause) {
                onSth(BIKGWTConstants.APP_VERSION, "???", false);
            }
        });
    }

//    @Override
//    protected void onModuleLoadInit() {
//        super.onModuleLoadInit();
//        showAppAndDBVersions(BIKGWTConstants.APP_VERSION, "???");
//    }
    public void init(StartupDataBean sdb) {
        BIKClientSingletons.showAppAndDBVersions(BIKGWTConstants.APP_VERSION, "???");
//        BIKClientSingletons.setBrowserTitle();
//        BIKClientSingletons.setNotificationLabels(setupInfoErrorLabel("infoPanel"), setupInfoErrorLabel("errorPanel"));

        BIKClientSingletons.setStartupData(sdb);
        BIKClientSingletons.setLogoOverLay();

        // tf: ukrycie paska ekranu z ładowaniem
        hideLoadingView();
        //ww: info o supporcie (tel. i email) widoczne tylko w wersji demo
        Element ele = GWTUtils.getElementById("supportFooterSpan");
        Element ele2 = GWTUtils.getElementById("copyrightSpan");

//        if (ele != null) {
//            UIObject.setVisible(ele, BIKClientSingletons.isDemoVersion());
//        }
//        if (ele2 != null) {
//            UIObject.setVisible(ele2, BIKClientSingletons.isDemoVersion());
//        }
//
        checkAppAndDBVersions(sdb.appVersion, BIKClientSingletons.getDBVersion(), null);

        if (BIKClientSingletons.isDisableGuestMode() && !BIKClientSingletons.isBikUserLoggedIn()) {
            initWidgetsForDisableGuestMode();
        } else {
            initAllWidgets();
        }
        if (sdb.isNasActive && BIKClientSingletons.isUserLoggedIn()) {
            BIKClientSingletons.checkLisaUnassignedCategory();
        }
    }

//    @Override
//    protected void onModuleLoadProper() {
////        GWTCurrentLocaleNameProvider.setupCurrentLocaleOnModuleLoad(null);
//
//        BIKClientSingletons.setBrowserTitle(); //tutaj bo jak ktoś zmieni title w BIKGWTConstants a nie zmieni w welcomeGWT to żeby się jednak podmieniło.
//        //Window.alert("onModuleLoad start");
//        //new BIKCenterUtilsTestInGWT().testIt();
//
////        showAppAndDBVersions(BIKGWTConstants.APP_VERSION, "???");
//        BIKClientSingletons.setNotificationLabels(setupInfoErrorLabel("infoPanel"), setupInfoErrorLabel("errorPanel"));
////        BIKClientSingletons.setErrorLabel(setupInfoErrorLabel("errorPanel"));
//
//        BIKClientSingletons.getService().getStartupData(new AsyncCallback<StartupDataBean>() {
//            public void onSuccess(StartupDataBean result) {
//
//                BIKClientSingletons.setStartupData(result);
//
//                //ww: info o supporcie (tel. i email) widoczne tylko w wersji demo
//                Element ele = GWTUtils.getElementById("supportFooterSpan");
//                if (ele != null) {
//                    UIObject.setVisible(ele, BIKClientSingletons.isDemoVersion());
//                }
//
//                checkAppAndDBVersions(result.appVersion, BIKClientSingletons.getDBVersion(), null);
//
////                // to jest do usunięcia - tylko DEMO localeLang
////                BIKClientSingletons.showInfo("localeLangInfo " +I18n.zSerwera.get() /* I18N:  */+ ": " + result.localeLangInfo);
//
//                initAllWidgets();
//            }
//
//            public void onFailure(Throwable caught) {
//                onInitializationFailure(caught, "getStartupData");
//            }
//        });
//        //Window.alert("onModuleLoad after getStartupData");
//    }
    public static void hideLoadingView() {
        // tf: ukrycie paska ekranu z ładowaniem
        Element loadingDIV = GWTUtils.getElementById("loadingDIV");
        if (loadingDIV != null) {
            UIObject.setVisible(loadingDIV, false);
        }
    }

    protected static void checkAppAndDBVersions(String appVersion, String dbVersion, String optErrorLocation) {
        BIKClientSingletons.showAppAndDBVersions(appVersion, dbVersion);

        boolean hasError = optErrorLocation != null;
        final String errorLocationInfo = hasError
                ? I18n.bladKomunikacjiW.get() /* i18n: no */ + optErrorLocation + "!"
                : null;

        //ww: stary warunek:
        // appVersion == null || dbVersion == null || !(appVersion.startsWith(dbVersion) || dbVersion.startsWith(appVersion))
        if (!FoxyCommonUtils.areAppAndDBVersionsCompatible(appVersion, dbVersion)) {
            Window.alert(
                    I18n.uWAGANiekompaWersjaAplikacjIBazy.get() /* I18N:  */ + appVersion + "\n"
                    + I18n.bazaDanychJestWWersjiV.get() /* I18N:  */ + dbVersion + "\n\n" + I18n.najprawdWymaganyJestSVNUPDATEIUr.get() /* I18N:  */ + "."
                    + (hasError ? "\n\n" + errorLocationInfo : ""));
        } else if (hasError) {
            Window.alert(errorLocationInfo + "\nAppVersion: v" /* i18n: no */ + appVersion
                    + ", dbVersion: v" /* i18n: no */ + dbVersion);
        }
    }

    private void searchButtonClick() {
//        BIKClientSingletons.getTabSelector().submitNewTabSelection(
//                NewSearchBikPage.NEW_SEARCH_TAB_ID, null, true);
        BIKClientSingletons.getTabSelector().submitNewTabSelection(
                SearchPage.SEARCH_PAGE_TAB_ID, null, true);
    }

    public static void helpButtonClick() {
        final Pair<String, Integer> p = BIKClientSingletons.getTabSelector().getCurrentTabIdAndSelectedVal();
        final String tabCaption = BIKClientSingletons.getTabSelector().getCurrentTabCaption();
        final String helpKey = p.v1;
        BIKClientSingletons.getService().getHelpDescr(helpKey, null, new StandardAsyncCallback<HelpBean>() {
            public void onSuccess(HelpBean result) {
                new HelpDialog().buildAndShowDialog(result, helpKey, tabCaption);
            }
        });

    }

//    public static Date autoLogoutAfterTime;
    protected Long lastActivityTimeMillis = System.currentTimeMillis();

//    private boolean showingBadLogginWarning = false;
//    private ConfirmDialog badLoginDialog = null;
    protected void processKeepSessionAliveResponse(String result) {
//                                lastKeepSessionAliveServiceTimeStamp = FoxyRequestCallback.serviceTimeStampForResponse;
//                                Pair<LoginStatusEncodedInVersionAfterLastDotOnKeepSessionAlive, Boolean> ls
//                                = BIKCenterUtils.decodeStatusEncodedInVersionAfterLastDotOnKeepSessionAlive(result);
//        LoggedUserOnClientVsServerStatus luocvss = BIKCenterUtils.decodeStatusEncodedInVersionAfterLastDotOnKeepSessionAlive(result);
//
////                                if (isSmartAutoRefreshMode) {
////                                    if (ls != null && ls.v2) {
////                                        refreshNodesOfCurrentTab();
////                                    }
////                                }
//        if (luocvss == LoggedUserOnClientVsServerStatus.DisabledUser) {
//            return;
//        }
//
//        final boolean userUnlogged = luocvss == LoggedUserOnClientVsServerStatus.UnloggedUser;
//        String heading = userUnlogged
//                ? I18n.jestesWylogowany.get() : I18n.jestesZalogowanyJakoInny.get();
//        final String dialogBody = "<b>" + heading + "</b><br/><br/>" + I18n.abyKontynuowacZalogujSie.get();
//
//        if (badLoginDialog != null) {
//
//            if (luocvss == LoggedUserOnClientVsServerStatus.SameUser) {
//                badLoginDialog.hideDialog();
//                badLoginDialog = null;
//                WaitingForLoginAgainDialog.loginChangedToGood();
//            } else {
//                badLoginDialog.updateBodyTextAndOrTitle(dialogBody, heading);
//            }
//
//            return;
//        }
//
//        if (luocvss != null && luocvss != LoggedUserOnClientVsServerStatus.SameUser) {
//
//            if (BIKClientSingletons.showingWaitingForLoginAgainDialog) {
//                return;
//            }
//
//            badLoginDialog = Dialogs.confirmHtml(dialogBody, heading, I18n.przelogujSieNaAktZakl.get(),
//                    I18n.zalogujSieNaInnejZakl.get(), new IParametrizedContinuation<Boolean>() {
//
//                        @Override
//                        public void doIt(Boolean param) {
//
//                            badLoginDialog = null;
//
//                            boolean dialogsAreOpened = BaseActionOrCancelDialog.isShowingDialog();
//
//                            if (dialogsAreOpened && param) {
//                                param = Window.confirm(I18n.czyNaPewnoMozeszUtracicPrace.get());
//                            }
//
//                            if (param) {
//                                BIKClientSingletons.performUserLogout(BIKClientSingletons.performOnLoginLogoutCont);
//                            } else {
//                                UrlBuilder newUrl = Window.Location.createUrlBuilder();
//                                newUrl.setHash("");
//                                Window.open(newUrl.buildString(), "_blank", "");
//                                new WaitingForLoginAgainDialog().buildAndShowDialog(userUnlogged);
//                            }
//                        }
//                    });
//        }
        checkVersionFromServer(result);
    }

//    private Long lastKeepSessionAliveServiceTimeStamp;
//    private Long prevKeepSessionAliveServiceTimeStamp;
    private void initAllWidgets() {
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(
                BIKClientSingletons.getMainBodyRootPanel(),
                BIKClientSingletons.ORIGINAL_MAINBODY_DIV_HEIGHT);

        //final DockLayoutPanel dlp = new DockLayoutPanel(Unit.PX);
        dlp.setWidth("100%");
        dlp.setStyleName("crazy4beer");

        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(dlp, BIKClientSingletons.ORIGINAL_MAIN_CONTAINER_HEIGHT);

        IContinuation searchForIt = new IContinuation() {
            public void doIt() {
                searchButtonClick();
            }
        };
        RootPanel goFindItWidget = RootPanel.get("goFindIt");

        GWTUtils.addOnClickHandler(goFindItWidget, searchForIt);
        goFindItWidget.setTitle(I18n.szukaj.get() /* I18N:  */);

        GWTUtils.addEnterKeyHandler(BIKClientSingletons.getGlobalSearchTextBox(), searchForIt);

        dlp.setStyleName("mbFloat");

        //menu and sub-menu
        List<MenuNodeBean> menuItems = BIKClientSingletons.getExtendedMenuNodes(
                BIKClientSingletons.getMenuNodes(), BIKClientSingletons.getTrees(),
                BIKClientSingletons.getNodeKindsByCodeMap(),
                true, MenuBottomLevel.BikPages, false, false);

        boolean isNotARegularUser = (BIKClientSingletons.isUserLoggedIn()) && (!BIKClientSingletons.getLoggedUserBean().nonPublicUserBranchIds.isEmpty()
                || !BIKClientSingletons.getLoggedUserBean().nonPublicUserTreeIDs.isEmpty()
                || !BIKClientSingletons.getLoggedUserBean().userAuthorBranchIds.isEmpty()
                || !BIKClientSingletons.getLoggedUserBean().userAuthorTreeIDs.isEmpty()
                || !BIKClientSingletons.getLoggedUserBean().userCreatorBranchIds.isEmpty()
                || !BIKClientSingletons.getLoggedUserBean().userCreatorTreeIDs.isEmpty()
                || BIKClientSingletons.isEffectiveExpertLoggedIn()
                || BIKClientSingletons.isSysAdminLoggedIn());
        List<MenuNodeBean> tabsToHide = new ArrayList<MenuNodeBean>();
        for (MenuNodeBean mnb : menuItems) {
            if (ClientDiagMsgs.isShowDiagEnabled(ClientDiagMsgs.APP_MSG_KIND)) {
                ClientDiagMsgs.showDiagMsg(ClientDiagMsgs.APP_MSG_KIND, mnb.code + ";" + mnb.name + ";" + mnb.nodeKindCode + ";" + mnb.id + ";" + mnb.nodeKindId + ";" + mnb.parentNodeId + ";" + mnb.treeId + ";" + mnb.visualOrder);
            }
            if (!isNotARegularUser && (hideSearchTab(mnb) || hideBlogsTab(mnb) || hideUserRolesTab(mnb) || hideUsersTab(mnb))) {
                tabsToHide.add(mnb);
            }
        }
        if (!tabsToHide.isEmpty()) {
            for (MenuNodeBean mnb : tabsToHide) {
                menuItems.remove(mnb);
            }
        }

        ITreeBottomUpBuilder<MenuNodeBean, IMenuItem, Integer> builder = new TreeBottomUpBuilderForMenuNodeBean();

        menuWrapper = GWTUtils.getElementById("menuWrapper");
        dropDownMenu = GWTUtils.getElementById("dropDownMenu");
        topMenu = GWTUtils.getElementById("topMenuUl");
        menuBuilder = new MenuBuilder(topMenu, GWTUtils.getElementById("sapBOSubmenu"), dlp, this);
        rootMenuItems = TreeAlgorithms.buildTreeBottomUp(menuItems, builder);
        menuBuilder.buildMenu(rootMenuItems);
        NodeList<Element> menuElement = topMenu.getElementsByTagName("li");

        for (int i = 0; i < menuElement.getLength(); i++) {
            Element e = menuElement.getItem(i);
            for (IMenuItem menuItem : rootMenuItems) {
                if (menuItem.getCaption().equals(e.getInnerText())) {
                    itemElementMenuMap.put(e, menuItem);
                }
            }
        }
        setVisibleDropDownMenu();
        BIKClientSingletons.setTabSelector(menuBuilder);

//        menuBuilder.setLoginInfo();
        BIKClientSingletons.getMainBodyRootPanel().add(dlp);

        final int autoLogoutAfterMins = BaseUtils.tryParseInt(BIKClientSingletons.getAppPropVal("autoLogoutAfterMins"));
        if (autoLogoutAfterMins > 0) {
            Event.addNativePreviewHandler(new Event.NativePreviewHandler() {

                @Override
                public void onPreviewNativeEvent(Event.NativePreviewEvent event) {
//                    autoLogoutAfterTime = SimpleDateUtils.addMinutes(new Date(), autoLogoutAfterMins);
                    lastActivityTimeMillis = System.currentTimeMillis();
                }
            });

//            Timer lastActivityTimer = new Timer() {
//
//                @Override
//                public void run() {
////                    BIKClientSingletons.showInfo("Last activity: " + SimpleDateUtils.getShortElapsedTimeStr(autoLogoutAfterTime, new Date(),
////                            "d", "h", "m", "s", 1));
//                    if (BIKClientSingletons.badLoginDialog == null && autoLogoutAfterTime.before(new Date())) {
//                        Window.alert(I18n.wZwiazkuZaAutoLogout.get());
//                        BIKClientSingletons.performUserLogout(BIKClientSingletons.performOnLoginLogoutCont);
//                    }
//                }
//            };
//
//            lastActivityTimer.scheduleRepeating(30 * 1000);
        }

        callKeepSessionAlive();

        Timer keepSessionAliveTimer = new Timer() {
            @Override
            public void run() {
                BIKClientSingletons.setBrowserTitle();

                callKeepSessionAlive();
////                String requestParam = BIKClientSingletons.getLoggedUserLoginName();
//                long currentTimeMillis = System.currentTimeMillis();
//
//                String requestParam = lastActivityTimeMillis == null ? null : ":" + (currentTimeMillis - lastActivityTimeMillis);
//
////                final boolean isSmartAutoRefreshMode = BIKClientSingletons.getTreeTabAutoRefreshLevel() == 3;
////
////                if (isSmartAutoRefreshMode) {
////                    Integer currentTreeId = BIKClientSingletons.getTreeIdOfCurrentTab();
////
////                    if (lastKeepSessionAliveServiceTimeStamp != null && currentTreeId != null) {
////
////                        prevKeepSessionAliveServiceTimeStamp = lastKeepSessionAliveServiceTimeStamp;
////
////                        requestParam = BIKConstants.KEEP_SESSION_ALIVE_REQUEST_PARAM_SEP + lastKeepSessionAliveServiceTimeStamp
////                                + BIKConstants.KEEP_SESSION_ALIVE_REQUEST_PARAM_SEP + currentTreeId
////                                + BIKConstants.KEEP_SESSION_ALIVE_REQUEST_PARAM_SEP + BIKClientSingletons.getLoggedUserLoginName();
////                    }
////                }
//                BIKClientSingletons.getService().keepSessionAlive(requestParam,
//                        new StandardAsyncCallback<String>(I18n.bladPolaczenPrawdopoTrwaInstalac.get() /* I18N:  */) {
//                            @Override
//                            public void onSuccess(String result) {
//                                processKeepSessionAliveResponse(result);
//                            }
//                        }
//                );

                if (!BIKClientSingletons.isDisableRankingPage()) {
                    BIKClientSingletons.getService().getChangedRanking(BIKClientSingletons.getLastRankingChangeTimeStamp(),
                            new StandardAsyncCallback<ChangedRankingBean>(I18n.bladPolaczenPrawdopoTrwaInstalac.get()) {
                        @Override
                        public void onSuccess(ChangedRankingBean result) {
                            if (result != null) {
//                                if (result.timeStamp > BIKClientSingletons.getLastRankingChangeTimeStamp()) {
                                BIKClientSingletons.setChangedRanking(result);

                                ITabSelector<Integer> tabSelector = BIKClientSingletons.getTabSelector();
                                BIKClientSingletons.refreshAndRedisplayDataUsersTree(!BIKGWTConstants.TREE_CODE_USER_ROLES.equals(tabSelector.getCurrentTab().getId()));
                                //bf--> odświeza wszystkie drzewka z uzytkownikami
                                Set<String> treeCode = BIKClientSingletons.getProperTreeCodesWithTreeKind(BIKGWTConstants.TREE_KIND_USERS);
                                for (String tc : treeCode) {
                                    if (!tc.equals(tabSelector.getCurrentTab().getId())) {
                                        // tu specjalnie tak - bo bez invalidowania dla innych instancji
                                        tabSelector.invalidateTabData(tc);
                                    }
                                }
                            }
                        }
                    });
                }
            }
        };

        keepSessionAliveTimer.scheduleRepeating(BIKConstants.KEEP_SESSION_ALIVE_FREQ_MILLIS);

        HistoryManager.setupChangeListenerAndFireCurrentStateToken(defaultTabOnStart);

        BIKClientSingletons.setupRequestCallbackForInitedGui();
    }

    //    protected void refreshNodesOfCurrentTab() {
    //        final Integer currentTreeId = BIKClientSingletons.getTreeIdOfCurrentTab();
    //
    //        if (currentTreeId == null || prevKeepSessionAliveServiceTimeStamp == null) {
    //            return;
    //        }
    //
    //        BIKClientSingletons.getService().getModifiedNodesFromLog(currentTreeId, prevKeepSessionAliveServiceTimeStamp,
    //                new StandardAsyncCallback<List<NodeModificationLogEntryBean>>() {
    //
    //                    @Override
    //                    public void onSuccess(List<NodeModificationLogEntryBean> result) {
    //                        IBikPage<Integer> tab = BIKClientSingletons.getTabSelector().getCurrentTab();
    //                        if (tab == null) {
    //                            return;
    //                        }
    //                        if (!(tab instanceof IBikTreePage)) {
    //                            return;
    //                        }
    //
    //                        IBikTreePage btp = (IBikTreePage) tab;
    //
    //                        if (btp.getTreeBean().id != currentTreeId) {
    //                            return;
    //                        }
    //
    //                        btp.getBikTree().refreshNodes(result);
    //                    }
    //                });
    //    }
    public boolean hideSearchTab(MenuNodeBean mnb) {
        List tabsList = new ArrayList();
        tabsList.add("#SearchPage");
        //czemu nie działa ukrywanie rankingu użytkowników
//        tabsList.add("#Rank");
        return BIKClientSingletons.hideSearchTabByAppProps() && tabsList.contains(mnb.code);
    }

    public boolean hideBlogsTab(MenuNodeBean mnb) {
        List tabsList = new ArrayList();
        tabsList.add("$Blogs");
        return BIKClientSingletons.hideBlogsTabByAppProps() && tabsList.contains(mnb.code);
    }

    public boolean hideUsersTab(MenuNodeBean mnb) {
        List tabsList = new ArrayList();
        tabsList.add("$Users");
        return BIKClientSingletons.hideUsersTabByAppProps() && tabsList.contains(mnb.code);
    }

    public boolean hideUserRolesTab(MenuNodeBean mnb) {
        List tabsList = new ArrayList();
        tabsList.add("$UserRoles");
        return BIKClientSingletons.hideUserRolesTabByAppProps() && tabsList.contains(mnb.code);
    }

    private void initWidgetsForDisableGuestMode() {
//        BIKClientSingletons.performOnLoginLogoutCont = new IContinuation() {
//            public void doIt() {
//                Window.Location.reload();
////                String newUrl = MultiXUrlCreator.createUrl(false);
////                String oldUrl = Window.Location.getHref();
////                Window.alert("oldUrl = " + oldUrl + ", newUrl = " + newUrl);
////                if (BaseUtils.safeEquals(newUrl, oldUrl)) {
////                    Window.Location.reload();
////                } else {
////                    Window.Location.replace(newUrl);
////                }
//            }
//        };
        Widget w = new LoginPageBase();
        BIKClientSingletons.getMainBodyRootPanel().clear();
        BIKClientSingletons.getMainBodyRootPanel().add(w);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(w, BIKClientSingletons.ORIGINAL_MAIN_CONTAINER_HEIGHT);
    }

    protected void checkVersionFromServer(String verFromServer) {
        if (//BaseUtils.safeEquals(verFromServer, BIKGWTConstants.APP_VERSION)
                FoxyCommonUtils.areAppAndDBVersionsCompatible(verFromServer, BIKGWTConstants.APP_VERSION)) {
            return; // ok, no-op
        }

        if (!badVersionAlertHasBeenShown) {
            badVersionAlertHasBeenShown = true;
            Window.alert(BAD_VER_MSG + "\n" + I18n.wersjaNaSerwerze.get() /* I18N:  */ + ": " + verFromServer + "\n" + I18n.wersjaWPrzegladarce.get() /* I18N:  */ + ": " + BIKGWTConstants.APP_VERSION);
        } else {
            if (showErrorForBadVersion) {
                BIKClientSingletons.showWarning(BAD_VER_MSG);
            } else {
                BIKClientSingletons.showInfo(BAD_VER_MSG);
            }
            showErrorForBadVersion = !showErrorForBadVersion;
        }
    }

    protected IBikPage<Integer> getOptCustomizedBikPage(TreeBean tree, String[] defColDefs) {
        if (tree.code.equals(BIKGWTConstants.TREE_CODE_DQC) || tree.code.equals(BIKGWTConstants.TREE_CODE_DQM)) {
            return new TestTreeBikPage(tree);
        }
        if (tree.treeKind.equals(BIKGWTConstants.TREE_KIND_CONFLUENCE)) {
            return new ConfluenceTreeBikPage(tree);
        }
        return null;
    }

    protected IBikPage<Integer> buildBikPageForTree(TreeBean tree) {

        String[] cols = TreeBikPage.TREE_BIK_PAGE_ONE_COL_FOR_TREE;
        /*BaseUtils.safeEqualsAny(tree.treeKind,
         BIKGWTConstants.TREE_KIND_GLOSSARY,
         BIKGWTConstants.TREE_KIND_DOCUMENTS,
         BIKGWTConstants.TREE_KIND_BLOGS) ? TreeBikPage.TREE_BIK_PAGE_ONE_COL_FOR_TREE
         : //ww: taksonomie też są tu obsługiwane
         TreeBikPage.TREE_BIK_PAGE_TWO_COLS_FOR_TREE;*/

        IBikPage<Integer> page = getOptCustomizedBikPage(tree, cols);
        if (page == null) {
            page = new TreeBikPage(tree, cols);
        }

        return page;
    }

//    protected Map<String, IMenuItem> buildTreeMenu(List<TreeBean> trees) {
//        Map<String, IMenuItem> tcm = new HashMap<String, IMenuItem>();
//
//        for (TreeBean tree : trees) {
//            if (BaseUtils.safeEquals(tree.treeKind, BIKGWTConstants.TREE_KIND_TAXONOMY)) {
//                continue; // taksonomie są oddzielnie obsługiwane w innym miejscu
//            }
//
//            String[] cols = BaseUtils.safeEqualsAny(tree.treeKind,
//                    BIKGWTConstants.TREE_KIND_GLOSSARY,
//                    BIKGWTConstants.TREE_KIND_DOCUMENTS,
//                    BIKGWTConstants.TREE_KIND_BLOGS/*,
//                     BIKGWTConstants.TREE_KIND_ARTICLES*/) ? TreeBikPage.TREE_BIK_PAGE_ONE_COL_FOR_TREE
//                    : TreeBikPage.TREE_BIK_PAGE_TWO_COLS_FOR_TREE;
//
//            IMenuItem menuItem = getOptCustomizedBikPage(tree, cols);
//            if (menuItem == null) {
//                menuItem = new TreeBikPage(tree, cols);
//            }
//            tcm.put(tree.code, menuItem);
//
////            if (!BaseUtils.safeEquals(tree.treeKind, BIKGWTConstants.TREE_KIND_GLOSSARY)
////                    && !BaseUtils.safeEquals(tree.treeKind, BIKGWTConstants.TREE_KIND_DOCUMENTS)
////                    && !BaseUtils.safeEquals(tree.treeKind, BIKGWTConstants.TREE_KIND_BLOGS)
////                    && !BaseUtils.safeEquals(tree.treeKind, BIKGWTConstants.TREE_KIND_ARTICLES)) {
////                tcm.put(tree.code, new TreeBikPage(tree,
////                        /*
////                        BaseBIKTreeWidget.NAME_FIELD_NAME + "=300:Nazwa",
////                        BaseBIKTreeWidget.PRETTY_KIND_FIELD_NAME + "=100:Typ"));
////                        } else {
////                        tcm.put(tree.code, new TreeBikPage(tree, BaseBIKTreeWidget.NAME_FIELD_NAME + "=Nazwa"));
////                         *
////                         */
////                        TreeNodeBeanAsMapStorage.NAME_FIELD_NAME + "=300:Nazwa",
////                        TreeNodeBeanAsMapStorage.PRETTY_KIND_FIELD_NAME + "=100:Typ"));
////            } else {
////                tcm.put(tree.code, new TreeBikPage(tree, TreeNodeBeanAsMapStorage.NAME_FIELD_NAME + "=Nazwa"));
////            }
//        }
//        return tcm;
//    }
//
//    private List<IMenuItem> buildTaxonomiesMenu(List<TreeBean> trees) {
//        List<IMenuItem> tcl = new ArrayList<IMenuItem>();
//        for (TreeBean tree : trees) {
//            if (BaseUtils.safeEquals(tree.treeKind, BIKGWTConstants.TREE_KIND_TAXONOMY)) {
//                tcl.add(new TreeBikPage(tree,
//                        TreeNodeBeanAsMapStorage.NAME_FIELD_NAME + "=300:Nazwa,300",
//                        TreeNodeBeanAsMapStorage.PRETTY_KIND_FIELD_NAME + "=100:Typ"));
//            }
//        }
//        return tcl;
//    }
    private void setVisibleDropDownMenu() {
        if (dropDownMenu != null && menuWrapper != null) {
            NodeList<Element> allLiTag = topMenu.getElementsByTagName("li");
            if (tryPlaceAllMenu(allLiTag) == allLiTag.getLength()) {
                dropDownMenu.getStyle().setVisibility(Style.Visibility.HIDDEN);
            } else {
                dropDownMenu.getStyle().setVisibility(Style.Visibility.VISIBLE);
                scrollTopMenu();
            }
        }
    }

    //Spróbować umieścić od leftStart-ego menu
    private int calculateMenuToPlace(int leftStart, NodeList<Element> allLiTag) {
        int i = 0;
        while (i < leftStart) {
            allLiTag.getItem(i++).getStyle().setDisplay(Style.Display.NONE);
        }

        int elemCnt = allLiTag.getLength();
        int maxLeft = dropDownMenu.getAbsoluteLeft();
        //menuWrapper.getStyle().setLeft(leftStart, Style.Unit.PX);
        int grabbedSpace = 0;
        do {
            Element child = allLiTag.getItem(i++);
            //System.out.println(child.getClientWidth());
            if (grabbedSpace + child.getClientWidth() < maxLeft) {
                child.getStyle().setDisplay(Style.Display.INLINE_BLOCK);
            }
            grabbedSpace = grabbedSpace + child.getClientWidth();
        } while (grabbedSpace < maxLeft && i < elemCnt);
        if (grabbedSpace > maxLeft) {
            i--;
        }
        for (int j = i; j < elemCnt; j++) {
            Element child = allLiTag.getItem(j);
            child.getStyle().setDisplay(Style.Display.NONE);
        }
        return i;
    }

    private int tryPlaceAllMenu(NodeList<Element> allLiTag) {
        return calculateMenuToPlace(0, allLiTag);
    }

    private void buildDropDownMenu(NodeList<Element> allLiTag) {
        if (GWTUtils.getElementById("extendedMenu").hasChildNodes()) {
            GWTUtils.getElementById("extendedMenu").removeAllChildren();
        }

        MenuBuilder exMenuBuilder = new MenuBuilder(GWTUtils.getElementById("extendedMenu"),
                GWTUtils.getElementById("sapBOSubmenu"), dlp);
        List<IMenuItem> exMenuItems = new ArrayList<IMenuItem>();
        for (int i = 0; i < allLiTag.getLength(); i++) {
            Element e = allLiTag.getItem(i);
            if (e.getStyle().getDisplay().equalsIgnoreCase(Style.Display.NONE.toString())) {
                exMenuItems.add(itemElementMenuMap.get(e));
            }
        }

        exMenuBuilder.buildMenu(exMenuItems);
    }

    @Override
    public void scrollTopMenu() {
        NodeList<Element> allLiTag = topMenu.getElementsByTagName("li");
        int activeMenuIdx = getActiveMenuElement(allLiTag);
        Element activeMenu = allLiTag.getItem(activeMenuIdx);
        if (activeMenu.getStyle().getDisplay().equalsIgnoreCase(Style.Display.NONE.toString())) {
            int i = 0;
            while (calculateMenuToPlace(i, allLiTag) <= activeMenuIdx) {
                i++;
            }
        }
        buildDropDownMenu(allLiTag);
    }

    private int getActiveMenuElement(NodeList<Element> allLiTag) {
        for (int i = 0; i < allLiTag.getLength(); i++) {
            if (allLiTag.getItem(i).hasClassName("active")) {
                return i;
            }
        }
        return 0;
    }

    protected void callKeepSessionAlive() {
        long currentTimeMillis = System.currentTimeMillis();

        String requestParam = lastActivityTimeMillis == null ? null : ":" + (currentTimeMillis - lastActivityTimeMillis);

//                final boolean isSmartAutoRefreshMode = BIKClientSingletons.getTreeTabAutoRefreshLevel() == 3;
//
//                if (isSmartAutoRefreshMode) {
//                    Integer currentTreeId = BIKClientSingletons.getTreeIdOfCurrentTab();
//
//                    if (lastKeepSessionAliveServiceTimeStamp != null && currentTreeId != null) {
//
//                        prevKeepSessionAliveServiceTimeStamp = lastKeepSessionAliveServiceTimeStamp;
//
//                        requestParam = BIKConstants.KEEP_SESSION_ALIVE_REQUEST_PARAM_SEP + lastKeepSessionAliveServiceTimeStamp
//                                + BIKConstants.KEEP_SESSION_ALIVE_REQUEST_PARAM_SEP + currentTreeId
//                                + BIKConstants.KEEP_SESSION_ALIVE_REQUEST_PARAM_SEP + BIKClientSingletons.getLoggedUserLoginName();
//                    }
//                }
        BIKClientSingletons.getService().keepSessionAlive(requestParam,
                new StandardAsyncCallback<String>(I18n.bladPolaczenPrawdopoTrwaInstalac.get() /* I18N:  */) {
            @Override
            public void onSuccess(String result) {
                processKeepSessionAliveResponse(result);
            }
        }
        );
    }
}
