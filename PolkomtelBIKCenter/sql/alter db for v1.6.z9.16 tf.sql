﻿exec sp_check_version '1.6.z9.16';
go

if not exists(select * from bik_attr_def where is_built_in = 1 and name = 'System')
begin
	declare @categoryId int = (select id from bik_attr_category where name = 'DQM' and is_built_in = 1);

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('System', @categoryId, 1, '', 'shortText')

	insert into bik_translation(code, txt, lang, kind)
	values ('System', 'System', 'en', 'adef')

	insert into bik_translation(code, txt, lang, kind)
	values ('Prefiks nazwy pliku na SFTP', 'Prefiks nazwy pliku na FTP', 'pl', 'adef')
	
	update bik_translation set txt = 'FTP file prefix' where code = 'Prefiks nazwy pliku na SFTP' and lang = 'en' and kind = 'adef'
	
	
	declare @DQMTestInactive int = dbo.fn_node_kind_id_by_code('DQMTestInactive');
    declare @DQMTestSuccess int = dbo.fn_node_kind_id_by_code('DQMTestSuccess');
    declare @DQMTestFailed int = dbo.fn_node_kind_id_by_code('DQMTestFailed');
    declare @DQMTestNotStarted int = dbo.fn_node_kind_id_by_code('DQMTestNotStarted');
    declare @attrTestQuery int = (select id from bik_attr_def where name = 'System' and is_built_in = 1);
    
    insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrTestQuery, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrTestQuery, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrTestQuery, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrTestQuery, 1)
	
end;
go

-- poprzednia wersja w pliku: alter db for v1.6.z9.8 tf.sql
-- dodanie wlasciwosci system
if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_verticalize_node_attrs_dqm_test') and type in (N'P', N'PC'))
drop procedure dbo.sp_verticalize_node_attrs_dqm_test
go

create procedure dbo.sp_verticalize_node_attrs_dqm_test(@optNodeFilter varchar(max))
as
begin
	set nocount on
	
	declare @sqlText varchar(max) = '(select te.test_node_id, te.description, te.long_name, te.benchmark_definition, te.sampling_method, te.additional_information, te.error_threshold, te.create_date, te.modify_date, te.activate_date, te.deactivate_date, def.description as source_name, bn1.name as proc_name, bn2.name as err_proc_name, te.sql_text, te.error_sql_text, db.name as database_name, te.result_file_name, serv.name as serv_name
		from bik_dqm_test as te 
		inner join bik_data_source_def as def on def.id = te.source_id
		left join bik_node as bn1 on bn1.id = te.procedure_node_id
		left join bik_node as bn2 on bn2.id = te.error_procedure_node_id
		left join bik_node as db on te.database_node_id = db.id
		left join bik_node as serv on serv.id = db.parent_node_id
		where te.is_deleted = 0)';
	
	exec sp_verticalize_node_attrs_one_table @sqlText, 'test_node_id', 'description, long_name, benchmark_definition, sampling_method, additional_information, error_threshold, create_date, modify_date, activate_date, deactivate_date, proc_name, err_proc_name, source_name, sql_text, error_sql_text, database_name, result_file_name, serv_name', 'Opis testu, Nazwa biznesowa, Definicja testu, Metoda próbkowania, Informacje dodatkowe, Próg błędu, Data utworzenia, Data modyfikacji, Data aktywacji, Data dezaktywacji, Funkcja testująca, Funkcja zwracająca błędne dane, Źródło, Zapytanie testujące, Zapytanie zwracające błędne dane, Baza danych, Prefiks nazwy pliku na SFTP, System', @optNodeFilter
end
go

exec sp_update_version '1.6.z9.16', '1.6.z9.17';
go

