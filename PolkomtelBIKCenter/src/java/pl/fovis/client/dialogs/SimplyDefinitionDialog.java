/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.common.i18npoc.I18n;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class SimplyDefinitionDialog extends BaseRichTextAndTextboxDialog {

    IParametrizedContinuation<Pair<String, String>> saveCont;

    @Override
    protected void innerDoAction(String title, String body) {
        saveCont.doIt(new Pair<String, String>(title, body));
    }

    @Override
    protected String getDialogCaption() {
        return I18n.dodajArtykul.get();
    }

    public void buildAndShowDialog(Pair<String, String> nameDesc, IParametrizedContinuation<Pair<String, String>> saveCont) {
        this.saveCont = saveCont;
        super.buildAndShowDialog(nameDesc != null ? nameDesc.v1 : null,
                nameDesc != null ? nameDesc.v2 : null, true);
    }

    @Override
    protected boolean showTableOfContent() {
        return false;
    }
}
