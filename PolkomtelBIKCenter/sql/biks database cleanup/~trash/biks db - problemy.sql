select *
from
(
select name, 
  dbo.fn_ww_table_key_cols(name, 0, 0) as pk,
  dbo.fn_ww_table_key_cols(name, 0, 1) as uk,
  dbo.fn_ww_table_key_cols(name, 0, 2) as ux,
  (select top 1 name from sys.columns where object_id = sys.objects.object_id and is_identity = 1) as identity_col,
  case when exists (select 1 from sys.columns where object_id = sys.objects.object_id and name = 'id') then 1 else 0 end as has_id_col
-- , *
from sys.objects
where type = 'U' and name like 'bik[_]%'
) x
where pk like '/' + '*%'



select case when has_id_col = 0 then 'alter table ' + name + ' add id int not null identity primary key'
else 'alter table ' + name + ' add constraint pk_' + name + ' primary key (id)' end as cmd
from
(
select name, 
  dbo.fn_ww_table_key_cols(name, 0, 0) as pk,
  dbo.fn_ww_table_key_cols(name, 0, 1) as uk,
  dbo.fn_ww_table_key_cols(name, 0, 2) as ux,
  (select top 1 name from sys.columns where object_id = sys.objects.object_id and is_identity = 1) as identity_col,
  case when exists (select 1 from sys.columns where object_id = sys.objects.object_id and name = 'id') then 1 else 0 end as has_id_col
-- , *
from sys.objects
where type = 'U' and name like 'bik[_]%'
) x
where pk like '/' + '*%'


alter table bik_active_directory add id int not null identity primary key
alter table bik_app_prop add id int not null identity primary key
alter table bik_data_load_log_details add constraint pk_bik_data_load_log_details primary key (id)
alter table bik_news_readed_user add id int not null identity primary key
alter table bik_tutorial_readed_user add id int not null identity primary key
alter table bik_sapbw_area add id int not null identity primary key
alter table bik_sapbw_provider add id int not null identity primary key
alter table bik_statistic add id int not null identity primary key
alter table bik_sapbw_flow add id int not null identity primary key
alter table bik_sapbw_query add id int not null identity primary key
alter table bik_statistic_dict add id int not null identity primary key
alter table bik_home_page_hint add id int not null identity primary key
alter table bik_sapbw_object add id int not null identity primary key
alter table bik_user_right add id int not null identity primary key
alter table bik_joined_objs add constraint pk_bik_joined_objs primary key (id)
alter table bik_sapbo_report_extradata add id int not null identity primary key
alter table bik_sapbo_user add id int not null identity primary key
alter table bik_sapbo_schedule add id int not null identity primary key
alter table bik_dqc_test_parameters add id int not null identity primary key
alter table bik_specialist add id int not null identity primary key
alter table bik_oracle_index add id int not null identity primary key
alter table bik_sapbo_connection_to_db add id int not null identity primary key
alter table bik_search_hint add id int not null identity primary key
alter table bik_node_name_chunk add id int not null identity primary key
alter table bik_mssql add id int not null identity primary key

