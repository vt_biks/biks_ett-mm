/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.mltx;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasAutoHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.mltx.history.HistoryAction;
import pl.fovis.client.mltx.history.MultiXHistoryToken;
import pl.fovis.common.MultixReturnMessage;
import pl.fovis.common.TokenType;
import pl.fovis.common.UserRegistrationDataBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.mltx.IdName;
import pl.fovis.common.mltx.InvitationConfirmResult;
import pl.fovis.common.mltx.InvitationResult;
import pl.fovis.common.mltx.MultiXDatabaseTemplateBean;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author pku
 */
public class MultixEmailConfirmedPage extends FlowPanel {

    public static final String USER_DIALOG_INPUT_CSS = "userDialogInput";
    public static final String INPUT_WIDTH_258 = "258px";
    //ww:mltx tak było
    //    public static final String INPUT_WIDTH_180 = "180px";
    public static final String FIELD_REQUIRED_CSS = "fieldRequired" + " " + USER_DIALOG_INPUT_CSS;
    public static final String COLON = ":";
    public static final String REQUIRED_MARKER = "* ";
    protected Label info;
    private Integer templateId;
    private static final String DB_OPTION = "dbVariant";
    private Label passwordLbl;
    private Label passwordRepeatLbl;
    private List<MltxRadioButton> dbTemplates;
    private Label companyLbl;
    private Label nameLbl;
    private Label lastNameLbl;
    private Label positionLbl;
    private Label attentionLbl, phoneLbl;
    private PasswordTextBox passwordTb, passwordRepeatTb;
    private TextBox companyTb;
    private TextBox nameTb;
    private TextBox lastNameTb;
    private TextBox positionTb, phoneNumTb;
    private TextArea attentionTa;
    protected FlexTable mltxBackground, infoTable;
    protected VerticalPanel vp;
    private PushButton confirmBtn;
    private FlexTable ft;
    private Map<Integer, TextArea> questions;
    private Map<TextBox, Boolean> fieldsValidity = new HashMap<TextBox, Boolean>();
    private Set<TextBox> fieldsRequired = new HashSet<TextBox>();

    public static HistoryAction getHistoryAction() {
        return new HistoryAction() {
            public Widget performAction(String id) {
                return new MultixEmailConfirmedPage(id, false);
            }
        };
    }

    public static HistoryAction getHistoryActionInvite() {
        return new HistoryAction() {
            public Widget performAction(String id) {
                return new MultixEmailConfirmedPage(id, true);
            }
        };
    }
    private final String token;
    private final boolean invited;

    public MultixEmailConfirmedPage(String token, boolean invited) {
        this.token = token;
        this.invited = invited;
        buildWidgets();
    }

    private void buildWidgets() {

        infoTable = new FlexTable();
        passwordTb = createPasswordBox(128, true);
        passwordRepeatTb = createPasswordBox(128, true);
        companyTb = createTextBox(200, true);
        nameTb = createTextBox(200, true);
        lastNameTb = createTextBox(200, true);
        positionTb = createTextBox(200, true);
        phoneNumTb = createTextBox(200, true);
        attentionTa = new TextArea();

        attentionTa.setStyleName("userTextArea " + USER_DIALOG_INPUT_CSS);
        attentionTa.setWidth(INPUT_WIDTH_258);
//        positionTb.setWidth("100%");

        passwordLbl = createLabel(I18n.wprowadzHaslo.get(), true);
        passwordRepeatLbl = createLabel(I18n.powtorzHaslo2.get(), true);
        companyLbl = createLabel(I18n.firma.get(), true);
        nameLbl = createLabel(I18n.imie.get(), true);
        lastNameLbl = createLabel(I18n.nazwisko.get(), true);
        positionLbl = createLabel(I18n.position.get(), true);
        attentionLbl = createLabel(I18n.additionRemarks.get(), false);
        phoneLbl = createLabel(I18n.numerTelefonu.get(), true);

        confirmBtn = new PushButton(I18n.zatwierdzDane.get());
        NewLookUtils.makeCustomPushButton(confirmBtn);
        confirmBtn.setStyleName("gwt-PushButton");
        confirmBtn.setEnabled(false);
        vp = new VerticalPanel();
        info = new Label();
        info.setStyleName("NewLoginLbl");
        vp.add(info);

        if (invited) {
            BIKClientSingletons.getMultiXService().inspectUser(token, new StandardAsyncCallback<InvitationResult>() {
                public void onSuccess(InvitationResult result) {
                    if (result.result == InvitationConfirmResult.RegistrationNeeded) {
                        createResultPage(true);
                    } else {
                        infoTable = new FlexTable();
                        infoTable.setWidth("520px");
                        if (result.result == InvitationConfirmResult.NoRegistration) {
                            if (result.dbNameToLogIn != null) {
                                BIKClientSingletons.reloadWithSetDb(result.dbNameToLogIn);
                            } else {
                                createSuccessPage(I18n.potwierdzonoZaproszenie.get());
                            }
                        } else if (result.result.equals(InvitationConfirmResult.tokenNotValid)) {
                            createFailurePage(I18n.linkIsNotValid.get());
                        }
                        vp.add(infoTable);
                    }
                }
            });
        } else {
            BIKClientSingletons.getMultiXService().verifyToken(token, TokenType.REGISRTATION, new StandardAsyncCallback<MultixReturnMessage>() {
                public void onSuccess(MultixReturnMessage result) {
                    switch (result.type) {
                        case ok:
                            createResultPage(true);
                            break;
                        case userGotAccount:
                            createResultPage(false);
                            break;
                        default:
                            createFailurePage(result.msg);
                            break;
                    }
                }
            });
        }
        mltxBackground = NewLookUtils.mltxBackground(vp);
        add(mltxBackground);
    }

    private void createResultPage(boolean withData) {
        finishForm(withData);
        displayInfo(I18n.trwaWeryfikacja.get());
    }

    private void finishForm(final boolean withData) {
        BIKClientSingletons.getMultiXService().getDatabaseTemplates(new StandardAsyncCallback<List<MultiXDatabaseTemplateBean>>() {
            public void onSuccess(List<MultiXDatabaseTemplateBean> result) {

                confirmBtn.addClickHandler(new ClickHandler() {
                    public void onClick(ClickEvent event) {
                        doFinalize(withData);
                    }
                });
                dbTemplates = new ArrayList<MltxRadioButton>();
                FlowPanel fpBiksVersions = new FlowPanel();
                for (MultiXDatabaseTemplateBean mdt : result) {
                    MltxRadioButton mrb = new MltxRadioButton(mdt);
                    fpBiksVersions.add(mrb);
                    dbTemplates.add(mrb);
                }

                if (dbTemplates.size() > 0) {
                    MltxRadioButton mrb = dbTemplates.get(0);
                    mrb.setValue(true);
                    if (dbTemplates.size() == 1) {
                        mrb.setVisible(false);
                        Label lbl = new Label(mrb.dbTemplate.caption);
                        lbl.setStyleName("regularInfoText");
                        fpBiksVersions.add(lbl);
                    }
                }
                int row = 0;
                ft = new FlexTable();
                ft.setWidth("100%");
                final HorizontalPanel fpBtns = new HorizontalPanel();
                fpBtns.setWidth("100%");
                fpBtns.add(confirmBtn);

                final int questionRow = row;
                if (withData) {

                    ft.setWidget(row, 0, passwordLbl);
                    ft.setWidget(row++, 1, passwordTb);
                    ft.setWidget(row, 0, passwordRepeatLbl);
                    ft.setWidget(row++, 1, passwordRepeatTb);
                    ft.setWidget(row, 0, nameLbl);
                    ft.setWidget(row++, 1, nameTb);
                    ft.setWidget(row, 0, lastNameLbl);
                    ft.setWidget(row++, 1, lastNameTb);
                    ft.setWidget(row, 0, companyLbl);
                    ft.setWidget(row++, 1, companyTb);
                    ft.setWidget(row, 0, positionLbl);
                    ft.setWidget(row++, 1, positionTb);
                    ft.setWidget(row, 0, phoneLbl);
                    ft.setWidget(row++, 1, phoneNumTb);

                    displayInfo(I18n.podajDaneDoZalożeniaKonta.get());
                    BIKClientSingletons.getMultiXService().getQuestions(new StandardAsyncCallback<List<IdName>>() {
                        public void onSuccess(List<IdName> result) {
                            questions = new HashMap<Integer, TextArea>();
                            int row = questionRow;
                            for (IdName idName : result) {
                                Label questionLbl = new Label(idName.name);
                                ft.getFlexCellFormatter().setColSpan(row, 0, 2);
                                ft.setWidget(row++, 0, questionLbl);
                                questionLbl.setStyleName("userDialogLbl");
                                ft.getFlexCellFormatter().setColSpan(row, 0, 2);
                                TextArea ta = new TextArea();
                                ta.setWidth("100%");
                                ft.setWidget(row++, 0, ta);
                                questions.put(idName.id, ta);
                            }
                            row = ft.getRowCount();
                            row++;
                            ft.setWidget(row, 0, attentionLbl);
                            ft.setWidget(row++, 1, attentionTa);
                            ft.setWidget(row, 0, fpBtns);
                        }
                    });

                } else {
                    displayInfo(I18n.trwaFinalizowanieProcesuRejestracji.get());
                    doFinalize(withData);
                }
                infoTable.clear();
                //ww:mltx usunięte
//                infoTable.setWidth("520px");

                if (withData && !invited) {
                    infoTable.setWidget(0, 0, fpBiksVersions);
                }

                infoTable.setWidget(1, 0, ft);

                vp.add(infoTable);
            }
        });
    }

    private Label createLabel(String txt, boolean isRequired) {
        String lTxt = (isRequired ? REQUIRED_MARKER : "") + txt + COLON;
        Label l = new Label(lTxt);
        l.setStyleName("userDialogLbl");
        l.getElement().getStyle().setMarginRight(10, Style.Unit.PX);
        return l;
    }

    private TextBox createTextBox(Integer maxLength, boolean isRequired) {
        TextBox tb = new TextBox();
        initializeTb(tb, maxLength, isRequired, FIELD_REQUIRED_CSS, INPUT_WIDTH_258);
        return tb;
    }

    private PasswordTextBox createPasswordBox(Integer maxLength, boolean isRequired) {
        PasswordTextBox tb = new PasswordTextBox();
        initializeTb(tb, maxLength, isRequired, FIELD_REQUIRED_CSS, INPUT_WIDTH_258 /* ww:mltx: "258px" */);
        return tb;
    }

    private void initializeTb(TextBox tb, Integer maxLength, boolean isRequired, String style, String width) {
        tb.setStyleName(style);
        tb.setWidth(width);
        if (isRequired) {
            fieldsRequired.add(tb);
            fieldsValidity.put(tb, false);
        }
        tb.addKeyUpHandler(addRequiredValidator(maxLength, tb));
    }

    protected void addLinkToInfo(Widget w) {
        vp.add(w);
    }

    private void createFailurePage(String info) {
        displayInfo(info);
        addLinkToInfo(createLinksPanel());
    }

    private HorizontalPanel createLinksPanel() {
        HorizontalPanel hp = new HorizontalPanel();
        AnchorAsHyperlink login = new AnchorAsHyperlink(I18n.zaloguj.get(), MultiXHistoryToken.LOGIN.hname);
        AnchorAsHyperlink register = new AnchorAsHyperlink(I18n.rejestracja.get(), MultiXHistoryToken.REGISTRATION.hname);
        hp.add(register);
        hp.setHorizontalAlignment(HasAutoHorizontalAlignment.ALIGN_RIGHT);
        hp.add(login);
        hp.setWidth("100%");
        return hp;
    }

    private void createSuccessPage(String msg) {
        if (infoTable != null) {
            infoTable.clear();
        }
        displayInfo(msg);

        addLinkToInfo(createLinksPanel());
    }

    private void displayInfo(String txt) {
        info.setText(txt);
    }

    private boolean isPasswordCorrect() {
        return passwordTb.getText().length() != 0 && passwordTb.getText().equals(passwordRepeatTb.getText());
    }

    private UserRegistrationDataBean getRegistrationData() {
        UserRegistrationDataBean bean = new UserRegistrationDataBean();
        bean.password = passwordTb.getText();
        bean.company = companyTb.getText();
        bean.name = nameTb.getText();
        bean.lastName = lastNameTb.getText();
        bean.token = token;
        bean.phoneNum = BaseUtils.isStrEmptyOrWhiteSpace(phoneNumTb.getText()) ? null : phoneNumTb.getText();
        bean.templateId = templateId;
        bean.attention = BaseUtils.isStrEmptyOrWhiteSpace(attentionTa.getText()) ? null : attentionTa.getText();
        bean.position = positionTb.getText();

        return bean;
    }

    private class MltxRadioButton extends RadioButton {

        protected MultiXDatabaseTemplateBean dbTemplate;

        public MltxRadioButton(MultiXDatabaseTemplateBean dbTemplate) {
            super(DB_OPTION, dbTemplate.caption);
            this.dbTemplate = dbTemplate;
        }

        public Integer getTemplateId() {
            return dbTemplate.id;
        }
    }

    private void doFinalize(boolean withData) {
        Map<Integer, String> answers = new HashMap<Integer, String>();
        if (withData) {
            if (!isPasswordCorrect()) {
                BIKClientSingletons.showError(I18n.bledneHaslo.get());
                return;
            }

            List<Integer> ids = new ArrayList<Integer>(questions.keySet());
            for (Integer id : ids) {
                answers.put(id, questions.get(id).getText());
            }
        }
        infoTable.clear();
        for (MltxRadioButton mrb : dbTemplates) {
            if (mrb.getValue()) {
                templateId = mrb.getTemplateId();
                break;
            }
        }
        displayInfo(I18n.trwaFinalizowanieProcesuRejestracji.get());
        StandardAsyncCallback<String> callback = new StandardAsyncCallback<String>() {
            public void onSuccess(String result) {
                if (result != null) {
                    createFailurePage(result);
                } else {
                    createSuccessPage(I18n.rejestracjaZakonczonaPomyslnie.get());
                }
            }
        };

        if (invited) {
            BIKClientSingletons.getMultiXService().verifyInvitationTokenAndCreateAccount(
                    getRegistrationData(), answers, callback);
        } else {
            BIKClientSingletons.getMultiXService().verifyTokenAndCreateDB(
                    getRegistrationData(), withData ? answers : null, callback);
        }
    }

    private KeyUpHandler addRequiredValidator(final int maxLength, final Widget widget) {
        return new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (widget instanceof TextBox) {
                    TextBox wdgt = (TextBox) widget;
                    if (fieldsRequired.contains(wdgt) && (wdgt.getText().isEmpty() || wdgt.getText().length() > maxLength)) {
                        wdgt.setStyleName(FIELD_REQUIRED_CSS);
                        wdgt.setTitle(I18n.fieldRequired.format(maxLength));
                        fieldsValidity.put(wdgt, false);
                    } else {
                        wdgt.setStyleName(USER_DIALOG_INPUT_CSS);
                        wdgt.setTitle("");
                        fieldsValidity.put(wdgt, true);
                    }
                    confirmBtn.setEnabled(areAllRequiredFieldsValid());
                }
            }
        };
    }

    private boolean areAllRequiredFieldsValid() {
        for (TextBox f : fieldsRequired) {
            if (!fieldsValidity.get(f)) {
                return false;
            }
        }
        return true;
    }
}
