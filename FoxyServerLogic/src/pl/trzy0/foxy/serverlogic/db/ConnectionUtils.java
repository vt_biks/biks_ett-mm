/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author tflorczak
 */
public class ConnectionUtils {

//    public static Connection getConnection(String serverName, String user, String password, String optParams) {
//        return getConnectionByConnectionString("jdbc:teradata://" + serverName, user, password, optParams);
//    }
    public static Connection getConnectionByConnectionString(String driverClassName, String connectionString, String user, String password, String optParams) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(connectionString);
            if (optParams != null && !optParams.isEmpty()) {
                sb.append("/").append(optParams);
            }
            Class.forName(driverClassName);
            return DriverManager.getConnection(sb.toString(), user, password);
        } catch (ClassNotFoundException ex) {
            String errorMsg = "Error in loading driver: ";
            System.out.println(errorMsg + ex);
            throw new RuntimeException(errorMsg, ex);
        } catch (SQLException ex) {
            String errorMsg = "Error in connecting: ";
            System.out.println(errorMsg + ex);
            throw new RuntimeException(errorMsg, ex);
        }
    }
}
