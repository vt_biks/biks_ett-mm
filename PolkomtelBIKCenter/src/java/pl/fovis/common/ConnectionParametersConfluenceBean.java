/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.util.List;

/**
 *
 * @author tflorczak
 */
public class ConnectionParametersConfluenceBean extends ParametersBaseBean {

    public String url;
    public String user;
    public String password;
    public Integer checkChildrenCount;
    public List<String> confluenceTreeCodes;
}
