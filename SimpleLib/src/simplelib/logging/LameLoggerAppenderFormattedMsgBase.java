/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.logging;

import java.util.Collection;
import simplelib.logging.ILameLogger.LameLogLevel;

/**
 *
 * @author wezyr
 */
public abstract class LameLoggerAppenderFormattedMsgBase extends LameLoggerAppenderBase {

    @Deprecated
    public static LameLogLevel calcProperMinLogLevel(LameLogLevel minLogLevel) {
        return LameLoggerAppenderBase.calcProperMinLogLevel(minLogLevel);
    }

    public LameLoggerAppenderFormattedMsgBase(LameLogLevel minLogLevel) {
        super(minLogLevel);
    }

    protected abstract void sendFormatedMsgToOutput(String formattedMsg);

    protected void sendMsgToOutput(LameLoggerMsg msg) {
        sendFormatedMsgToOutput(msg.getFormattedMsg());
    }

    public void sendMsgsToOutput(Collection<LameLoggerMsg> msgs,
            LameLogLevel minLogLevelInMsgs, LameLogLevel maxLogLevelInMsgs) {
        
        if (maxLogLevelInMsgs.isLowerLevelThan(getMinLogLevel())) {
            return;
        }

        for (LameLoggerMsg msg : msgs) {
            if (isMsgLevelInRange(msg)) {
                sendMsgToOutput(msg);
            }
        }
    }
}
