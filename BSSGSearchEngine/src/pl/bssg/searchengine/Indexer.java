/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.searchengine;

import commonlib.LameUtils;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class Indexer {

    protected final String indexDirectory;
    protected final Analyzer analyzer;
    protected final IDocCrawler crawler;
    protected boolean reindexAll;

    public Indexer(String indexDirectory, Analyzer analyzer, IDocCrawler crawler) {
        this.indexDirectory = indexDirectory;
        this.analyzer = analyzer;
        this.crawler = crawler;
    }

    public void setReindexAll(boolean reindexAll) {
        this.reindexAll = reindexAll;
    }

    public synchronized void index() throws IOException {
        Directory indexDirectory = null;
        IndexWriter indexWriter = null;

        try {
            if (reindexAll) {
                LameUtils.killDir(new File(this.indexDirectory));
            }

            indexDirectory = FSDirectory.open(Paths.get(this.indexDirectory));
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
            iwc.setOpenMode(reindexAll ? OpenMode.CREATE : OpenMode.CREATE_OR_APPEND);

            indexWriter = new IndexWriter(indexDirectory, iwc);

            int cnt = 0;
            do {
                List<Document> docPack = crawler.next();
//                if (reindexAll) {
//                    System.out.println("Indexed: " + cnt);
//                }
                if (BaseUtils.isCollectionEmpty(docPack)) {
                    break;
                } else {
                    cnt += docPack.size();
                }
                System.out.println("Indexed: " + cnt);
                for (Document doc : docPack) {
                    String isDeleted = doc.get(SearchConstant.IS_DELETED);
                    Term docId = new Term(SearchConstant.DOC_ID, doc.get(SearchConstant.DOC_ID));
                    if ("1".equals(isDeleted) || "true".equalsIgnoreCase(isDeleted)) {
                        indexWriter.deleteDocuments(docId);
                    } else {
                        indexWriter.updateDocument(docId, doc);
                    }
                }
            } while (true);
        } finally {
            if (indexWriter != null) {
                indexWriter.close();
            } else {
                System.out.println("indexWriter is not created");
            }
            if (indexDirectory != null) {
                indexDirectory.close();
            } else {
                System.out.println("indexDirectory is not created");
            }
        }
    }
}
