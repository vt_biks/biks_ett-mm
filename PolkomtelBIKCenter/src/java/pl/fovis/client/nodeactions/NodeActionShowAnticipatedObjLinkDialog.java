/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AnticipatedObjLinkDialog;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author pmielanczuk
 */
public class NodeActionShowAnticipatedObjLinkDialog extends NodeActionReadOnlyBase {

    @Override
    public boolean isEnabledByDesign() {
        return isAdminOrExpertOrAuthorOrEditor() && BIKClientSingletons.isShowAnticipatedObjLink();
    }

    @Override
    public String getCaption() {
        return I18n.anticipatedObjLinkAct.get();
    }

    @Override
    public void execute() {
        TreeNodeBean tnb = ctx.getCurrentNodeBean();
        String branchIds = tnb == null ? null : tnb.branchIds;
        String ancestorPath = branchIds == null ? null : BIKCenterUtils.buildAncestorPathByStorage(ctx.getCurrentNodeBean().branchIds, ctx.getTreeStorage());
        new AnticipatedObjLinkDialog(ctx.getTreeCode(), ancestorPath, ctx.getSelectedNodeId()).buildAndShowDialog();
    }
}
