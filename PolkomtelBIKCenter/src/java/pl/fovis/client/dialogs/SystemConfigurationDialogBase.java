/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;

/**
 *
 * @author ctran
 */
public abstract class SystemConfigurationDialogBase extends BaseActionOrCancelDialog {

    protected TreeBean treeBean;
    protected Label configTreeLbl;
    protected PushButton changeConfigTreeBtn;
    protected PushButton buildConfigTreeBtn;
    protected PushButton implementConfigTreeBtn;

    @Override
    protected String getDialogCaption() {
        return I18n.konfiguracja.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {
        actionBtn.setVisible(false);
        configTreeLbl = new Label(treeBean.name);
        HorizontalPanel hp = new HorizontalPanel();
        hp.add(new Label(I18n.obecneDrzewoKonfiguracji.get() + ":"));
        hp.add(configTreeLbl);

        Grid grid = new Grid(4, 2);
        grid.setCellPadding(6);
        int row = 0;
//        grid.setWidget(row, 0, createChangeConfigTreeBtn());
//        grid.setWidget(row++, 1, new Label(I18n.uwstaicDrzewoJakoDrzewoKonfiguracji.format(treeBean.name)));
        grid.setWidget(row, 0, createBuildConfigTreeBtn(false));
        grid.setWidget(row++, 1, new Label(I18n.zbudujDrzewoKonfiguracjiZObecnejStrukturySystemuBezKasowania.get()));
        grid.setWidget(row, 0, createBuildConfigTreeBtn(true));
        grid.setWidget(row++, 1, new Label(I18n.zbudujDrzewoKonfiguracjiZObecnejStrukturySystemu.get()));
        grid.setWidget(row, 0, createImplementConfigTreeBtn());
        grid.setWidget(row++, 1, new Label(I18n.wdrozycDrzewoKonfiguracji.get()));
        onConfigurationTreeChange();
        main.add(hp);
        main.add(grid);
    }

    public abstract String getConfigurationAppPropCode();

    public void onConfigurationTreeChange() {
        BIKClientSingletons.getService().getConfigurationTree(getConfigurationAppPropCode(), new StandardAsyncCallback<String>() {

            @Override
            public void onSuccess(String result) {
                configTreeLbl.setText(result);
            }
        });
    }

    public abstract void doImplement();

    public abstract void doBuildConfigTree(boolean deleteOldTree);

    private Widget createImplementConfigTreeBtn() {
        implementConfigTreeBtn = new PushButton(I18n.implementuj.get());
        NewLookUtils.makeCustomPushButton(implementConfigTreeBtn);
        implementConfigTreeBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                doImplement();
//                final BIKSProgressInfoDialog dialog = new BIKSProgressInfoDialog();
//                dialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaPostepowanie.get(), true);
//                BIKClientSingletons.getService().buildTmpConfigTree(new StandardAsyncCallback<Void>() {
//
//                    @Override
//                    public void onSuccess(Void result) {
//                        dialog.hideDialog();
//                    }
//
//                    @Override
//                    public void onFailure(Throwable caught) {
//                        dialog.hideDialog();
//                        BIKClientSingletons.showWarning(caught.getMessage());
//                    }
//                });
            }
        });
        return implementConfigTreeBtn;
    }

    private Widget createBuildConfigTreeBtn(final boolean deleteOldTree) {
        buildConfigTreeBtn = new PushButton(I18n.zbuduj.get());
        NewLookUtils.makeCustomPushButton(buildConfigTreeBtn);
        buildConfigTreeBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                doBuildConfigTree(deleteOldTree);
            }
        });
        return buildConfigTreeBtn;
    }

    @Override
    protected void doAction() {
    }

    public void buildAndShowDialog(TreeBean treeBean) {
        this.treeBean = treeBean;
        super.buildAndShowDialog(); //To change body of generated methods, choose Tools | Templates.
    }
}
