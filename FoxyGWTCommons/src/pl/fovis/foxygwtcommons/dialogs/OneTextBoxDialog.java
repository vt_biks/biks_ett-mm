/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author AMamcarz
 */
public class OneTextBoxDialog extends BaseActionOrCancelDialog {

    private IParametrizedContinuation<String> saveCont;
    protected TextBox tbValue;
    protected String initValue;
    protected String name;

    @Override
    protected String getDialogCaption() {
        return name;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        tbValue = new TextBox();
        tbValue.addStyleName("sendUrlInput");
        tbValue.setTitle(name);
        tbValue.setValue((initValue != null) ? initValue : "");

        main.add(tbValue);
    }

    @Override
    protected void doAction() {
        if (!tbValue.getValue().equals("")) {
            saveCont.doIt(tbValue.getValue());
        }
    }

    public void buildAndShowDialog(String name, String optInitValue, IParametrizedContinuation<String> saveCont) {
        this.name = name;
        this.initValue = optInitValue;
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }
}
