/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.mltx;

import java.io.Serializable;

/**
 *
 * @author pku
 */
public class InvitationResult implements Serializable {

    public InvitationConfirmResult result;
    public String dbNameToLogIn;

    public InvitationResult() {
    }

    public InvitationResult(InvitationConfirmResult result, String dbNameToLogIn) {
        this.result = result;
        this.dbNameToLogIn = dbNameToLogIn;
    }

    public InvitationResult(InvitationConfirmResult result) {
        this.result = result;
    }
}
