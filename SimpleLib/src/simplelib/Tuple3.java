/*
 * Tuple3.java
 *
 * Created on 18 styczeń 2007, 18:11
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package simplelib;

/**
 *
 * @author wezyr
 */
public class Tuple3 <T1, T2, T3> {
    public T1 v1;
    public T2 v2;
    public T3 v3;
    
    public Tuple3(T1 v1, T2 v2, T3 v3) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
    }
    
    @Override
    public String toString() {
        return "(" + v1 + "," + v2 + "," + v3 + ")";
    }
    
    @Override
    public int hashCode() {
        return ((v1 == null ? 0 : v1.hashCode() * 31) + (v2 == null ? 0 : v2.hashCode())) * 31 +
                (v3 == null ? 0 : v3.hashCode());
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj instanceof Tuple3) {
            Tuple3 tuple = (Tuple3)obj;
            return BaseUtils.safeEquals(v1, tuple.v1) && BaseUtils.safeEquals(v2, tuple.v2)
            && BaseUtils.safeEquals(v3, tuple.v3);
        } else
            return false;
    }
}
