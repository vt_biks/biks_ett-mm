/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence;

import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.confluence.wuwu.ConfluenceUtils;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;
import simplelib.LameRuntimeException;
import simplelib.Pair;

/**
 * Pompka do update'u pobranych już obiektów z Confluence
 *
 * @author tflorczak
 */
public class ConfluencePump extends MetadataPumpBase {

//    private static final ILameLogger lameLogger = LameUtils.getMyLogger();
    public final static String endPoint = "/rpc/soap-axis/confluenceservice-v2";

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_CONFLUENCE;
    }

//    public String getPumpGroup() {
//        return PumpConstants.PUMP_GROUP_CONFLUENCE;
//    }
    @Override
    protected Pair<Integer, String> startPump() {
        ConfluenceConfigBean configData = null;
        pumpLogger.pumpLog("Getting confluence config bean", 0);
        try {
            configData = PumpUtils.readAdminBean("confluence", ConfluenceConfigBean.class, adhocDao);
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Invalid login for data loading! Error: " + e.getMessage());
        }
        pumpLogger.pumpLog("Connect to Confluence", 10);
        try {
            pumpLogger.pumpLog("Delete unused content", 20);
            adhocDao.execNamedCommand("confluenceFixDeletedContent");
            pumpLogger.pumpLog("Getting objects to load from DB", 30);
            List<ConfluenceGroupBean> objectsToLoad = getObjectsToLoad();
            pumpLogger.pumpLog("Getting content to load from Confluence", 40);
            IConfluenceConnector conn = new ConfluenceConnector(configData);
            conn.initializeService();
            conn.login();
            if (!conn.isLogged()) {
                throw new LameRuntimeException("Error with login to Confluence.");
            }
            int objCnt = 0;
            for (ConfluenceGroupBean group : objectsToLoad) {
                List<ConfluenceGroupBean> groupAsList = new ArrayList<ConfluenceGroupBean>();
                groupAsList.add(group);
                objCnt++;

                List<List<ConfluenceObjectBean>> contentForObjects = conn.getContentForObjects(groupAsList);
                int percent = 40 + (int) (objCnt * 50.0 / objectsToLoad.size());
                pumpLogger.pumpLog("Getting list object " + objCnt + "/" + objectsToLoad.size(), percent);
                int cnt = 0;
                for (List<ConfluenceObjectBean> objsList : contentForObjects) {
                    if (!objsList.isEmpty()) {
                        cnt++;
                        pumpLogger.pumpLog("Update content with " + objsList.size() + " objects", percent);

                        // wszsytkiie obiekty z listy maja taka sama grupę, więc wybieram pierwszy
                        final int groupId = objsList.get(0).groupId;
                        final Map<String, Integer> objIdToNodeIdMap = new HashMap<String, Integer>();

                        String s = execNamedQuerySingleVal("getAppPropValue", true, null, "confluenceBatchSize");
                        if (BaseUtils.isStrEmptyOrWhiteSpace(s)) {
                            s = execNamedQuerySingleVal("getAppPropValue", true, null, "defaultBatchSize");
                        }
                        if (BaseUtils.isStrEmptyOrWhiteSpace(s)) {
                            s = "64";
                        }

                        Integer batchSize = BaseUtils.tryParseInteger(s);

                        for (int i = 0; i < (objsList.size() - 1) / batchSize + 1; i++) {
                            final List<ConfluenceObjectBean> l = objsList.subList(i * batchSize, Math.min(i * batchSize + batchSize, objsList.size()));
                            pumpLogger.pumpLog("Updated " + (batchSize * i) + "/" + objsList.size() + " objects", percent);

                            List<Map<String, Object>> rows = adhocDao.execNamedQueryCFN("confluenceUpdateOneGroupContent",
                                    FieldNameConversion.ToLowerCase,
                                    l, groupId);

                            for (Map<String, Object> row : rows) {
                                objIdToNodeIdMap.put((String) row.get("obj_id"), (Integer) row.get("node_id"));
                            }
//                        throw new LameRuntimeException("Test Exception");
                        }
                        pumpLogger.pumpLog("confluenceDeletedOneGroupContent: " + objsList.size() + " objects", percent);
                        for (int i = 0; i < (objsList.size() - 1) / batchSize + 1; i++) {
                            final List<ConfluenceObjectBean> l = objsList.subList(i * batchSize, Math.min(i * batchSize + batchSize, objsList.size()));
                            pumpLogger.pumpLog("Delete group content " + (batchSize * i) + "/" + objsList.size() + " objects", percent);

                            adhocDao.execNamedCommand("confluenceDeletedOneGroupContent", objsList, groupId);
                        }
                        Map<Integer, String> nodeIdToDescrMap = new HashMap<Integer, String>();
                        for (ConfluenceObjectBean cob : objsList) {
                            nodeIdToDescrMap.put(objIdToNodeIdMap.get(cob.id), cob.content);
                        }

                        pumpLogger.pumpLog("updatejoinedObjsForLinksToBikNodes: " + nodeIdToDescrMap.size() + " nodes", percent);
                        ConfluenceUtils.updateJoinedObjsForLinksToBikNodes(adhocDao, urlForPublicAccess, nodeIdToDescrMap);
                    }
                }
            }
            conn.logout();
//            adhocDao.execNamedCommand("confluenceUpdateContent", contentForObjects);
            pumpLogger.pumpLog("Init Branch Ids", 90);
            adhocDao.execNamedCommand("confluenceInitBranchIds");
            pumpLogger.pumpLog("Adding associations between Confluence and users", 95);
            adhocDao.execNamedCommand("confluenceJoinContentWithUsers");
            pumpLogger.pumpLog("Adding Confluence authors to ranking", 97);
            adhocDao.execNamedCommand("confluenceAddAuthorsToRanking");
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(LameUtils.printExceptionToString(e));
            throw new LameRuntimeException(e.getMessage());
        }
    }

    protected List<ConfluenceGroupBean> getObjectsToLoad() {
        return adhocDao.createBeansFromNamedQry("getConfluenceObjectsToLoad", ConfluenceGroupBean.class);
    }
}
