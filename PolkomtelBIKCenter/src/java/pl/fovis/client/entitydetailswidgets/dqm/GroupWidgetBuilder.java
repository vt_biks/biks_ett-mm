/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.dqm.DataQualityGroupBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.SimpleDateUtils;

public class GroupWidgetBuilder {

    protected List<DataQualityGroupBean> elems;
    protected boolean useDQMMode; // dla DQM - większe widgety, dodatkowy napis, etc.

    // showNewestSquare - true dla DQM (dla PB)
    public GroupWidgetBuilder(List<DataQualityGroupBean> elems, boolean useDQMMode) {
        this.elems = elems;
        this.useDQMMode = useDQMMode;
    }

    public Widget buildWidget() {
        int listSize = elems.size();
        FlexTable ft = new FlexTable();

        if (listSize == 0) {
            return ft;
        }

        if (useDQMMode) {
            ft.setWidth("330px");
        } else {
            ft.setWidth("230px");
        }
        //            ???????? ft.setHeight("110px");
        ft.setCellPadding(0);
        ft.setCellSpacing(0);
        ft.setStyleName("BizDef-keyowrdsMain superBorderForChuck testWidget");
        //            ft.setBorderWidth(1);
        FlowPanel ftUp = new FlowPanel();
        //            ftUp.setHeight("170px");
        ftUp.addStyleName("testWidgetUp");
//        ftUp.setWidth("100%");
        ftUp.setHeight("45px");
        final DataQualityGroupBean lastElem = elems.get(listSize - 1);
//        Label testTitle = new Label(lastElem.name);
        HTML testTitle = new HTML(
                (lastElem.isError ? "<img src='images/error.png' border='0'/> " : "") + BaseUtils.encodeForHTMLTag(lastElem.name, true, 10));
        testTitle.setTitle(lastElem.testHint);
        testTitle.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getTabSelector().submitNewTabSelection(lastElem.treeCode, lastElem.nodeId, true);
            }
        });
        if (useDQMMode) {
            testTitle.setStyleName("btnPointer leftRightBorder backgroundWhite");
        } else {
            testTitle.setStyleName("btnPointer");
        }
        ftUp.add(testTitle);
        if (lastElem.link != null && !lastElem.link.trim().equals("")) {
            HTML html = new HTML(BIKCenterUtils.createUrlEx(lastElem.link, "<img src='images/dqcOpenReport.png' border='0'/>"));
            html.setStyleName("marginStyle");
            ftUp.add(html);
        }
        //            FlexTable ftDown = new FlexTable();
        //            ftDown.setCellPadding(0);
        //            ftDown.setCellSpacing(0);
        FlowPanel ftDown = new FlowPanel();
        if (useDQMMode) {
            ftDown.addStyleName("testWidgetDown");
        }
        boolean pbStyleSquares = false; //false; //true;
        //                    BaseUtils.safeEquals(getNodeTreeCode(), BIKGWTConstants.TREE_CODE_DQM);
//        boolean showNewestSquare = BaseUtils.safeEquals(getNodeTreeCode(), BIKGWTConstants.TREE_CODE_DQM);
        DataQualityGroupBean[] properOrderElems = new DataQualityGroupBean[listSize];
        for (int i = 0; i < listSize; i++) {
            properOrderElems[i] = elems.get(listSize - 1 - i);
        }
        VerticalPanel vpDown = new VerticalPanel();
        if (useDQMMode && lastElem.requestHint != null) { // dodatkowy tekst - Wykonania testu
            Label testExecutions = new Label(I18n.wykonania.get());
            testExecutions.setStyleName("testLbl");
            vpDown.add(testExecutions);
        }

        for (int i = 0; i < listSize; i++) {
            //                DataQualityGroupBean bean = elems.get(i);
            final DataQualityGroupBean bean = properOrderElems[i];
            //                if (pbStyleSquares || i != listSize - 1) { // pomijamy ostatnie wykonanie, chyba, że PB-style
            if ((useDQMMode || i != 0) && bean.requestHint != null) {
                // pomijamy ostatnie wykonanie, chyba, że PB-style

                Label lbl = new Label();
                //                    InlineHTML lbl = new InlineHTML("&nbsp;");
                lbl.setStyleName("leftRightBorder");
                if (useDQMMode) {
                    lbl.addStyleName("label-inline");
                } else {
                    lbl.addStyleName("label-inline-close");
                }
                lbl.setWidth("15px");
                lbl.setHeight("15px");
                String requestDate = SimpleDateUtils.toCanonicalString(bean.requestHint);
                if (!BaseUtils.isStrEmptyOrWhiteSpace(requestDate)) {
                    lbl.setTitle(I18n.dataWykonaniaTestu.get() + ": " + requestDate);
                }
                //                    ftDown.setWidget(0, listSize - (pbStyleSquares ? 1 : 2) - i, lbl);
                ftDown.add(lbl);
                //                    ftDown.getCellFormatter().setStyleName(0, i, bean.isSuccess ? "backgroundGreen" : "backgroundRed");
                lbl.addStyleName(bean.isSuccess ? "backgroundGreen" : "backgroundRed");
                // głupota 4 bżemeg
                lbl.addStyleName("btnPointer");
                lbl.addClickHandler(new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        BIKClientSingletons.requestId = bean.requestId;
                        BIKClientSingletons.getTabSelector().submitNewTabSelection(lastElem.treeCode, lastElem.nodeId, true);
                    }
                });
            }
            //                if (i == listSize - 1) { // ostatnie wykonanie
            if (!useDQMMode && i == 0) {
                ftUp.setTitle(I18n.ostatnieWykonanieTestu.get() + ": " + SimpleDateUtils.toCanonicalString(bean.requestHint));
            }
        }
        vpDown.add(ftDown);
        ft.setWidget(0, 0, ftUp);
        ft.setWidget(1, 0, useDQMMode ? vpDown : ftDown);
        ft.getCellFormatter().setHeight(0, 0, "65px");
        if (useDQMMode) {
            ft.getCellFormatter().setHeight(1, 0, "75px");
        }
        String style;
        if (lastElem.type.equals(BIKConstants.NODE_KIND_DQC_TEST_SUCCESS) || lastElem.type.equals(BIKConstants.NODE_KIND_DQM_TEST_SUCCESS)) {
            style = "backgroundGreen";
        } else if (lastElem.type.equals(BIKConstants.NODE_KIND_DQC_TEST_INACTIVE) || lastElem.type.equals(BIKConstants.NODE_KIND_DQM_TEST_INACTIVE) || lastElem.type.equals(BIKConstants.NODE_KIND_DQM_TEST_NOT_STARTED)) {
            style = "backgroundGray";
        } else {
            style = "backgroundRed";
        }
        for (int i = 0; i <= 1; i++) {
            if (!pbStyleSquares || i == 0) {
                ft.getCellFormatter().setStyleName(i, 0, style);
            }
            if (i > 0) {
                ft.getCellFormatter().addStyleName(i, 0, "paddingBottom8px");
            }
//            ft.getCellFormatter().setAlignment(i, 0, HasHorizontalAlignment.ALIGN_CENTER, HasVerticalAlignment.ALIGN_MIDDLE);
            ft.getCellFormatter().setAlignment(i, 0, HasHorizontalAlignment.ALIGN_CENTER, HasVerticalAlignment.ALIGN_TOP);
        }
        return ft;
    }

    public static void showManyInContainer(List<DataQualityGroupBean> data, HasWidgets panel, boolean useDQMMode) {
        panel.clear();

//        if (data != null) {
        if (!BaseUtils.isCollectionEmpty(data)) {
            Map<Integer, List<DataQualityGroupBean>> map = new LinkedHashMap<Integer, List<DataQualityGroupBean>>();
            // napełnianie mapy
            for (DataQualityGroupBean bean : data) {
                if (!map.containsKey(bean.nodeId)) {
                    map.put(bean.nodeId, new ArrayList<DataQualityGroupBean>());
                }
                map.get(bean.nodeId).add(bean);
            }

            // dodanie widgetów
            for (Map.Entry<Integer, List<DataQualityGroupBean>> wid : map.entrySet()) {
                panel.add(new GroupWidgetBuilder(wid.getValue(), useDQMMode).buildWidget());
            }
        } else {
            Label h = new Label(I18n.brakAktywnychTestow.get());
            h.addStyleName("testWidgets-noActiveTests");
            panel.add(h);
        }
    }
}
