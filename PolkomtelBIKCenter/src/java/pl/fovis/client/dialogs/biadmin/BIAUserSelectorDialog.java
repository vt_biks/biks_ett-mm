/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.biadmin;

import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import java.util.ArrayList;
import java.util.List;
import pl.bssg.metadatapump.common.SAPBOObjectBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class BIAUserSelectorDialog extends BaseActionOrCancelDialog {

    protected IParametrizedContinuation<SAPBOObjectBean> saveCont;
    protected List<SAPBOObjectBean> users;
    protected ListBox lbx;
    protected TextBox filter;
    protected PushButton filterBtn;

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzUzytkownika.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.wybierz.get();
    }

    @Override
    protected void buildMainWidgets() {
        HorizontalPanel hp = new HorizontalPanel();
        hp.setSpacing(0);
        filter = new TextBox();
        filter.setWidth("250px");
        filter.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                NativeEvent ne = event.getNativeEvent();
                if (ne.getKeyCode() == KeyCodes.KEY_ENTER) {
                    innerOnFilterHandler();
                }
            }
        });
        filterBtn = new PushButton(I18n.filtruj.get());
        NewLookUtils.makeCustomPushButton(filterBtn);
        filterBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                innerOnFilterHandler();
            }
        });
        filterBtn.setWidth("50px");
        lbx = new ListBox();
        lbx.setVisibleItemCount(20);
        lbx.setWidth("300px");
        showData(users);
        hp.add(filter);
        hp.add(filterBtn);
        main.add(hp);
        main.add(lbx);
    }

    protected void showData(List<SAPBOObjectBean> usersToAdd) {
        lbx.clear();
        for (SAPBOObjectBean user : usersToAdd) {
            lbx.addItem(user.name, String.valueOf(user.id));
        }
        if (!users.isEmpty()) {
            lbx.setSelectedIndex(0);
        }
    }

    @Override
    protected boolean doActionBeforeClose() {
        String selectedItemName = lbx.getSelectedItemText();
        if (selectedItemName == null) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.wybierzUzytkownika.get(), null, null);
            return false;
        }
        return true;
    }

    @Override
    protected void doAction() {
        SAPBOObjectBean bean = new SAPBOObjectBean();
        bean.name = lbx.getSelectedItemText();
        bean.id = Integer.valueOf(lbx.getSelectedValue());
        saveCont.doIt(bean);
    }

    public void buildAndShowDialog(List<SAPBOObjectBean> users, IParametrizedContinuation<SAPBOObjectBean> saveCont) {
        this.saveCont = saveCont;
        this.users = users;
        super.buildAndShowDialog();
    }

    protected void innerOnFilterHandler() {
        showData(findUsers(filter.getText()));
    }

    protected List<SAPBOObjectBean> findUsers(String chars) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(chars)) {
            return users;
        }
        List<SAPBOObjectBean> listToReturn = new ArrayList<SAPBOObjectBean>();
        for (SAPBOObjectBean user : users) {
            if (user.name.contains(chars)) {
                listToReturn.add(user);
            }
        }
        return listToReturn;
    }

    @Override
    protected void onEnterPressed() {
        innerOnFilterHandler();
    }
}
