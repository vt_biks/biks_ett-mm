/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.UrlMakingUtils;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class SearchResultsExportManager extends ExportManagerBase {

    protected BOXIServiceImpl service;

    public SearchResultsExportManager(BOXIServiceImpl boxiService) {
        this.service = boxiService;
    }

    @Override
    protected Pair<String, List<Map<String, Object>>> getFileNamePrefixAndContent(HttpServletRequest req) {
        String searchRequestTicket = req.getParameter(BIKConstants.SEARCH_REQUEST_TICKET_PARAM_NAME);
        String fileName = I18n.eksportWynikow.get() + " " + I18n.dla.get() + " " + req.getParameter(BIKConstants.SEARCH_PHRASE_PARAM_NAME);
        List<String> exportOptions = BaseUtils.splitBySep(req.getParameter(BIKConstants.EXPORT_OPTIONS_PARAM_NAME), ",");
        List<Map<String, Object>> content = service.getSearchResultsToExport(searchRequestTicket, exportOptions);
        service.clearSearchResultsCache(searchRequestTicket);
        return new Pair<String, List<Map<String, Object>>>(UrlMakingUtils.encodeStringForUrlWW(fileName).replace("+", "%20"), content);
    }

    @Override
    protected String getFileFormat(HttpServletRequest req) {
        return req.getParameter(BIKConstants.SEARCH_RESULTS_EXPORT_PARAM_NAME);
    }
}
