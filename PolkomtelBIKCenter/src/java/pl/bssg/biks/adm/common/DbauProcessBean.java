/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author pmielanczuk
 */
public class DbauProcessBean implements Serializable {
    /*
     [id] [int] IDENTITY(1,1) NOT NULL,
     [date_added] [datetime] NOT NULL,
     [init_db_ver] [varchar](100) NOT NULL,
     [app_ver] [varchar](100) NOT NULL,
     [spid] [int] NULL,
     [status] [int] NOT NULL,
     [final_db_ver] [varchar](100) NULL,
     */

    public int id;
    public Date dateAdded;
    public String initDbVer;
    public String appVer;
    public Integer spid;
    public int status;
    public String finalDbVer;
    //---
    public Date lastLogDate;
}
