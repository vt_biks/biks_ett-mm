exec sp_check_version '1.5.x.11';
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
--
-- 1. pomocnicze funkcje i widoki - metadane itp.
-- 2. brakuj�ce primary key i id identity na tabelach
-- 3. zdj�cie wymagalno�ci z pola bik_teradata.data_load_log_id
-- 4. usuni�cie �mieciowych procedur, widok�w i funkcji 
--      dla PGE - "czkawka po �miechawce"
-- 5. usuni�cie (drop) tabelek MLTX_
-- 6. dodanie film�w EN do samouczk�w
-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

if OBJECT_ID (N'dbo.fn_ww_table_key_cols', N'FN') is not null
    drop function dbo.fn_ww_table_key_cols;
go

create function dbo.fn_ww_table_key_cols(@table_name sysname, @with_col_types int, @key_level int)
returns varchar(max)
as
begin
  declare @txt varchar(max) = ''

  declare @idx_name sysname = (select top 1 constraint_name from
  (
select distinct i.name as constraint_name,
  case when i.is_primary_key = 1 then 'pk' when i.is_unique_constraint = 1 then 'uk' else 'ux' end as constraint_kind
 from sys.tables t
inner join sys.schemas s on t.schema_id = s.schema_id
inner join sys.indexes i on i.object_id = t.object_id
where i.index_id > 0    
and i.type in (1, 2) -- clustered & nonclustered only
and i.is_disabled = 0
and i.is_hypothetical = 0
and (i.is_primary_key = 1 or @key_level > 0 and i.is_unique_constraint = 1 or @key_level > 1 and i.is_unique = 1)
and t.object_id = object_id(@table_name)
) x order by case constraint_kind when 'pk' then 0 when 'uk' then 1 else 2 end)
    
select 
  @txt = @txt +
  c.column_name + 
  case when @with_col_types <> 0 then
  ' ' + c.DATA_TYPE + case when c.character_maximum_length is null then ''
    when c.character_maximum_length < 0 then '(max)' else '(' + cast(c.CHARACTER_MAXIMUM_LENGTH as varchar(10)) + ')' end + 
    ' ' + case when c.is_nullable = 'NO' then 'not' else '' end + ' null'
  else '' end + ','
from 
  (
select s.name as table_schema, t.name as table_name, i.name as constraint_name, c.name as column_name,
  case when i.is_primary_key = 1 then 'pk' when i.is_unique_constraint = 1 then 'uk' else 'ux' end as constraint_kind,
  ic.key_ordinal as ordinal_position
 from sys.tables t
inner join sys.schemas s on t.schema_id = s.schema_id
inner join sys.indexes i on i.object_id = t.object_id
inner join sys.index_columns ic on ic.object_id = t.object_id and i.index_id = ic.index_id
	inner join sys.columns c on c.object_id = t.object_id and
		ic.column_id = c.column_id
where i.name = @idx_name
and i.index_id > 0    
and i.type in (1, 2) -- clustered & nonclustered only
and i.is_disabled = 0
and i.is_hypothetical = 0
and ic.key_ordinal > 0
and (i.is_primary_key = 1 or @key_level > 0 and i.is_unique_constraint = 1 or @key_level > 1 and i.is_unique = 1)
) tc inner join information_schema.columns c 
    on tc.table_schema = c.TABLE_SCHEMA
      and tc.TABLE_NAME = c.TABLE_NAME and tc.COLUMN_NAME = c.COLUMN_NAME
order by tc.ORDINAL_POSITION

  return case when @txt = '' then 
    '/* no ' + case @key_level when 0 then 'pk' when 1 then 'uk' else 'ux' end + ' on ' + @table_name + ' */'
     else substring(@txt, 1, len(@txt) - 1) end
end
go

/*
 select dbo.fn_ww_primary_key_cols('bik_help', 1)
 select dbo.fn_ww_primary_key_cols('bik_help', 0)
 select dbo.fn_ww_primary_key_cols('bik_node', 1)

 select dbo.fn_ww_table_key_cols('bik_node', 1, 0)
 select dbo.fn_ww_table_key_cols('bik_node', 1, 1)

 select dbo.fn_ww_table_key_cols('bik_joined_objs', 1, 1)
 select dbo.fn_ww_table_key_cols('bik_joined_objs', 1, 0)

 select * from (
 select name, dbo.fn_ww_primary_key_cols(name, 0) as pk_cols
 from sys.objects where name like 'bik[_]%'
 ) x where pk_cols like '/' + '*%'
 
 select * from information_schema.table_constraints
 select distinct constraint_type from information_schema.table_constraints
 
*/


if OBJECT_ID (N'dbo.fn_ww_fk_join_cond', N'FN') is not null
    drop function dbo.fn_ww_fk_join_cond;
go

create function dbo.fn_ww_fk_join_cond(@fk_name sysname, @ree_alias sysname, @red_alias sysname)
returns varchar(max)
as
begin
  declare @txt varchar(max) = ''
  declare @and_sep varchar(max) = ' and '

  select @txt = @txt + @and_sep + @ree_alias + '.' + name + '=' + @red_alias + '.' + ReferencedColumn
  from vw_ww_foreignkeys
  where ForeignKey_Name = @fk_name

  return substring(@txt, len(@and_sep) + 1, len(@txt))
end
go


/*

select dbo.fn_ww_fk_join_cond('FK_bik_joined_objs_dst_node_id', 'ree', 'red')

*/


if exists(select 1 from sys.objects where object_id = object_id('vw_ww_db_table_stats') and type = 'V')
drop view vw_ww_db_table_stats
go

create view vw_ww_db_table_stats as
SELECT 
    t.NAME AS TableName,
    p.rows AS RowCounts,
    SUM(a.total_pages) * 8 AS TotalSpaceKB, 
    SUM(a.used_pages) * 8 AS UsedSpaceKB, 
    (SUM(a.total_pages) - SUM(a.used_pages)) * 8 AS UnusedSpaceKB
FROM 
    sys.tables t
INNER JOIN      
    sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN 
    sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN 
    sys.allocation_units a ON p.partition_id = a.container_id
WHERE 
    t.NAME NOT LIKE 'dt%' 
    AND t.is_ms_shipped = 0
    AND i.OBJECT_ID > 255 
GROUP BY 
    t.Name, p.Rows
go




if OBJECT_ID (N'dbo.fn_ww_self_join_cond', N'FN') is not null
    drop function dbo.fn_ww_self_join_cond;
go

create function dbo.fn_ww_self_join_cond(@cols varchar(max), @ree_alias sysname, @red_alias sysname)
returns varchar(max)
as
begin
  declare @txt varchar(max) = ''
  declare @and_sep varchar(max) = ' and '

  select @txt = @txt + @and_sep + @ree_alias + '.' + str + '=' + @red_alias + '.' + str from dbo.fn_split_by_sep(@cols, ',', 3)
  order by idx
  
  return substring(@txt, len(@and_sep) + 1, len(@txt))
end
go


-- select dbo.fn_ww_self_join_cond('node_id', 'ree', 'tt')
-- select dbo.fn_ww_self_join_cond('node_id,user_id', 'ree', 'tt')

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

/*

-- poni�sze zapytanie generuje brakuj�ce klucze g��wne na tabelach bik_*

select case when has_id_col = 0 then 'alter table ' + name + ' add id int not null identity primary key'
else 'alter table ' + name + ' add constraint pk_' + name + ' primary key (id)' end as cmd
from
(
select name, 
  dbo.fn_ww_table_key_cols(name, 0, 0) as pk,
  dbo.fn_ww_table_key_cols(name, 0, 1) as uk,
  dbo.fn_ww_table_key_cols(name, 0, 2) as ux,
  (select top 1 name from sys.columns where object_id = sys.objects.object_id and is_identity = 1) as identity_col,
  case when exists (select 1 from sys.columns where object_id = sys.objects.object_id and name = 'id') then 1 else 0 end as has_id_col
-- , *
from sys.objects
where type = 'U' and name like 'bik[_]%'
) x
where pk like '/' + '*%'

*/

alter table bik_active_directory add id int not null identity primary key
alter table bik_app_prop add id int not null identity primary key
alter table bik_data_load_log_details add constraint pk_bik_data_load_log_details primary key (id)
alter table bik_news_readed_user add id int not null identity primary key
alter table bik_tutorial_readed_user add id int not null identity primary key
alter table bik_sapbw_area add id int not null identity primary key
alter table bik_sapbw_provider add id int not null identity primary key
alter table bik_statistic add id int not null identity primary key
alter table bik_sapbw_flow add id int not null identity primary key
alter table bik_sapbw_query add id int not null identity primary key
alter table bik_statistic_dict add id int not null identity primary key
alter table bik_home_page_hint add id int not null identity primary key
alter table bik_sapbw_object add id int not null identity primary key
alter table bik_user_right add id int not null identity primary key
alter table bik_joined_objs add constraint pk_bik_joined_objs primary key (id)
alter table bik_sapbo_report_extradata add id int not null identity primary key
alter table bik_sapbo_user add id int not null identity primary key
alter table bik_sapbo_schedule add id int not null identity primary key
alter table bik_dqc_test_parameters add id int not null identity primary key
alter table bik_specialist add id int not null identity primary key
alter table bik_oracle_index add id int not null identity primary key
alter table bik_sapbo_connection_to_db add id int not null identity primary key
alter table bik_search_hint add id int not null identity primary key
alter table bik_node_name_chunk add id int not null identity primary key
alter table bik_mssql add id int not null identity primary key
go


alter table bik_teradata alter column data_load_log_id int null
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

-----------------------------------------------------------------
-----------------------------------------------------------------
-- �mieci z PGE
-----------------------------------------------------------------
-----------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_przygotujDane_Raport3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_przygotujDane_Raport3]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FinalizujDaneRaportow]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FinalizujDaneRaportow]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_dane_raport1]'))
DROP VIEW [dbo].[vw_dane_raport1]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_dane_raport2]'))
DROP VIEW [dbo].[vw_dane_raport2]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_ozn_kontr_agr]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_ozn_kontr_agr]
GO

-----------------------------------------------------
-----------------------------------------------------
-- obiekty mltx - zb�dne
-----------------------------------------------------
-----------------------------------------------------

drop table mltx_login
drop table mltx_registration
drop table mltx_app_prop
drop table mltx_database
drop table mltx_database_template
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

update bik_tutorial set movie = '1Introduction'
where lang = 'en' and code = 'Introduction'

update bik_tutorial set movie = '2MyBIKS'
where lang = 'en' and code = 'MyBIKS'

update bik_tutorial set movie = '3Search'
where lang = 'en' and code = 'Search'

update bik_tutorial set movie = '4Navigation'
where lang = 'en' and code = 'AppNavigation'

update bik_tutorial set movie = '6Usecases'
where lang = 'en' and code = 'UseCases'

update bik_tutorial set movie = '5Analysis'
where lang = 'en' and code = 'Analysis'
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

exec sp_update_version '1.5.x.11', '1.5.z';
go
