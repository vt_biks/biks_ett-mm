/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.AdminConfigBaseWidget;

/**
 *
 * @author ctran
 */
public abstract class BikPageAdminConfigBase<T> extends AdminConfigBaseWidget<T> implements IBikPage<Integer> {

    @Override
    public Integer getDisplayedNodeId() {
        return getSelectedNodeId();
    }

    @Override
    public boolean isPageEnabled() {
        return BIKClientSingletons.isBikPageEnabled(this);
    }

    @Override
    public Integer getSelectedNodeId() {
        return null;
    }

//    protected void setFocusWidgetssEnabled(boolean val) {
//        for (FocusWidget widget : widgetList) {
//            widget.setEnabled(val);
//        }
//    }
//    protected TextBox createTextBox(CellPanel cp, boolean isPassword) {
//        TextBox textBox = new TextBox();
//        textBox.setWidth("290px");
//        if (isPassword) {
//            textBox.getElement().setAttribute("type" /* I18N: no */, "password" /* I18N: no */);
//        }
//        cp.add(textBox);
//        widgetList.add(textBox);
//        return textBox;
//    }
//    protected CheckBox createCheckBox(String text, ValueChangeHandler<Boolean> valueChanger, boolean addToWidgetList, CellPanel cp) {
//        CheckBox checkBox = new CheckBox(text);
//        checkBox.setValue(true);
//        if (valueChanger != null) {
//            checkBox.addValueChangeHandler(valueChanger);
//        }
//        cp.add(checkBox);
//        if (addToWidgetList) {
//            widgetList.add(checkBox);
//        }
//        return checkBox;
//    }
//    protected ListBox createListBox(Set<String> list, CellPanel cp) {
//        ListBox lbx = new ListBox();
//        if (list != null) {
//            for (String element : list) {
//                lbx.addItem(element);
//            }
//            lbx.setSelectedIndex(0);
//        }
//        cp.add(lbx);
//        widgetList.add(lbx);
//        return lbx;
//    }
}
