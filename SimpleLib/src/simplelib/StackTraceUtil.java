/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author pmielanczuk
 */
public final class StackTraceUtil {

    public static String NEW_LINE = "\n"; //System.getProperty("line.separator");

//    public static void throwXXX() {
//        throw new IllegalArgumentException("Aqq");
//    }
//
//    public static void throwLameCoughtSth() {
//        try {
//            throwXXX();
//        } catch (Exception ex) {
//            throw new LameRuntimeException("cought xxx", ex);
//        }
//    }
//
//    public static String getStackTrace(Throwable aThrowable) {
//        final Writer result = new StringWriter();
//        final PrintWriter printWriter = new PrintWriter(result);
//        aThrowable.printStackTrace(printWriter);
//        return result.toString();
//    }
    protected static void printStackTraceAsCause(
            StringBuilder sb,
            Throwable th, StackTraceElement[] causedTrace) {
        // assert Thread.holdsLock(s);

        // Compute number of frames in common between this and caused
        StackTraceElement[] trace = th.getStackTrace();
        int m = trace.length - 1, n = causedTrace.length - 1;
        while (m >= 0 && n >= 0 && trace[m].equals(causedTrace[n])) {
            m--;
            n--;
        }
        int framesInCommon = trace.length - 1 - m;

        sb.append("Caused by: ").append(th).append(NEW_LINE);
        for (int i = 0; i <= m; i++) {
            sb.append("\tat ").append(trace[i]).append(NEW_LINE);
        }
        if (framesInCommon != 0) {
            sb.append("\t... ").append(framesInCommon).append(" more").append(NEW_LINE);
        }

        // Recurse if we have a cause
        Throwable ourCause = th.getCause();
        if (ourCause != null) {
            printStackTraceAsCause(sb, ourCause, trace);
        }
    }

    /**
     * Defines a custom format for the stack trace as String.
     */
    public static String getCustomStackTrace(Throwable th) {

        if (th == null) {
            return null;
        }

        //add the class name and any message passed to constructor
        final StringBuilder sb = new StringBuilder();
        sb.append(th).append(NEW_LINE);

        StackTraceElement[] elems = th.getStackTrace();

        appendStackTrace(elems, sb);

        // Recurse if we have a cause
        Throwable ourCause = th.getCause();
        if (ourCause != null) {
            printStackTraceAsCause(sb, ourCause, th.getStackTrace());
        }

        return sb.toString();
    }
//    /** Demonstrate output.  */
//    public static void main(String... aArguments) {
//
//        Throwable throwable = new IllegalArgumentException("Blah");
//
//        try {
//            throwLameCoughtSth();
//        } catch (Throwable t) {
//            throwable = t;
//        }
//
//        System.out.println("--- standard ---");
//        System.out.println(getStackTrace(throwable));
//        System.out.println("\n\n--- custom ---");
//        System.out.println(getCustomStackTrace(throwable));
//    }

    public static void appendStackTrace(StackTraceElement[] elems, final StringBuilder sb, int elemsToIgnore) {
        //add each element of the stack trace
        for (StackTraceElement element : elems) {
            if (elemsToIgnore > 0) {
                elemsToIgnore--;
            } else {
                sb.append("\tat ").append(element).append(NEW_LINE);
            }
        }
    }

    public static void appendStackTrace(StackTraceElement[] elems, final StringBuilder sb) {
        appendStackTrace(elems, sb, 0);
    }
}
