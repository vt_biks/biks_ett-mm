/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author Eugeniusz
 */
public class NodeActionGrantRightsOldRightsSys extends NodeActionGrantRights {

    @Override
    public boolean isEnabledByDesign() {
        return (BIKClientSingletons.isEffectiveExpertLoggedIn()
                || BIKClientSingletons.isLoggedUserAuthorOfTree(node.treeId)
                || BIKClientSingletons.isLoggedUserCreatorOfTree(node.treeId)
                || BIKClientSingletons.isLoggedUserIsNonPublicUserInTree(node.treeId)
                || BIKClientSingletons.isBranchAuthorLoggedIn(node.branchIds)
                || BIKClientSingletons.isBranchCreatorLoggedIn(node.branchIds)
                || BIKClientSingletons.isBranchNonPublicUserLoggedIn(node.branchIds))
                && !BIKClientSingletons.hideGrantRightsBtnByAppProps()
                && !BIKClientSingletons.newRightsSysByAppProps();
    }

    @Override
    public String getCaption() {
        return I18n.nadajUprawnienia.get();
    }
}
