/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

import java.io.Serializable;

/**
 *
 * @author ctran
 */
public class LisaInstanceInfoBean implements Serializable {

    public Integer id;
    public String name;
    public String url;
    public String port;
    public String dbName;
    public String charset;
    public String tmode;
    public String schemaName;
    public String defaultUnassignedCategoryName;
}
