﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- Indeksy do poprawy wydajności wizualizacji testów
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------


exec sp_check_version '1.6.y.1';
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_dqc_request]') and name = N'idx_bik_dqc_request__deleted_id_request')
begin
	create index idx_bik_dqc_request__deleted_id_request on bik_dqc_request (__deleted, id_request)
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_dqc_request]') and name = N'idx_bik_dqc_request_id_test__deleted_start_timestamp')
begin
	create index idx_bik_dqc_request_id_test__deleted_start_timestamp on bik_dqc_request (id_test, __deleted, start_timestamp)
end;
go


exec sp_update_version '1.6.y.1', '1.6.y.2';
go