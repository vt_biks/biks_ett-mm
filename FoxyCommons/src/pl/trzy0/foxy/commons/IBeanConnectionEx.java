/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import simplelib.BaseUtils.Projector;
import simplelib.FieldNameConversion;
import simplelib.IContinuationWithReturn;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author wezyr
 */
public interface IBeanConnectionEx<BeanBase> extends IRawConnection, IFoxyTransactionalPerformer {
//    public <T extends BeanBase> int saveBean(T bean);

    public List<Map<String, Object>> execQryCFN(String qryTxt, boolean maxOneRow, boolean minOneRow,
            FieldNameConversion optConversion);

    public <K> Map<K, Map<String, Object>> execQry(String qryTxt, boolean maxOneRow, boolean minOneRow,
            Projector<Map<String, Object>, K> proj);

    public Integer execQrySingleValInteger(String qryTxt, boolean allowNoResult);

    public <T extends BeanBase> List<T> createBeansFromQry(String qryTxt, Class<T> beanClass);

    public <K, T extends BeanBase> Map<K, T> createBeansFromQry(String qryTxt,
            Class<T> beanClass, Projector<? super T, K> proj);

    public <T extends BeanBase> T createBeanFromQry(String qryTxt, Class<T> beanClass, boolean allowNoResult);

    // just shorthand for execQry(qryTxt, false, false, null)
    //public <T> List<Map<String, T>> execQry(String qryTxt);
    public List<Map<String, Object>> execQry(String qryTxt);

    // just shorthand for execQry(qryTxt, false, false, optConversion)
    public List<Map<String, Object>> execQry(String qryTxt, FieldNameConversion optConversion);
    //public <T> List<Map<String, T>> execQry(String qryTxt, FieldNameConversion optConversion);

    public Map<String, Object> execOneRowQry(String qryTxt, boolean allowNoResult,
            FieldNameConversion optConversion);
//    public <T> Map<String, T> execOneRowQry(String qryTxt, boolean allowNoResult,
//            FieldNameConversion optConversion);

    //public Map<String, Object> execOneRowQry(String qryTxt);
    public Integer sqlNumberToInt(Object numVal);

    public String toSqlString(Object o);

    public <T> String toSqlString(Iterable<T> items);

    public <T> String toSqlString(T[] items);

    //ww: to siedzi w IFoxyTransactionalPerformer
//    public void finishWorkUnit(boolean success);
    public <T> void extendWithExtraProps(Iterable<T> items, ExtraPropsQuery<?, T>... epqs);

    public <T> T runWithoutLogging(boolean noLogging, IContinuationWithReturn<T> cont);

    public void setDbStatementExecutionWarnThresholdMillis(long threshold);

    public IBeanConnectionThreadHandle getCurrentThreadHandle();

    public void setCurrentDatabase(String dbName);

    public String getCurrentDatabase();

    public void execQry(String qryTxt, IParametrizedContinuation<Iterator<Map<String, Object>>> processCont, FieldNameConversion optConversion);
}
