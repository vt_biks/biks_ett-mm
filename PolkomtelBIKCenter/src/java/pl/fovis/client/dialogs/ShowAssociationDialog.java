/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author ctran
 */
public class ShowAssociationDialog extends ViewSomethingInBIKSDialog {

    protected Integer nodeId;

    @Override
    protected Widget getContentWidget() {
        HTML w = new HTML();
        w.getElement().setId("association-div");
        w.setHeight("600px");
        BIKClientSingletons.getService().getNodeAssociationScript(nodeId, new StandardAsyncCallback<String>() {

            @Override
            public void onSuccess(final String script) {

                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                    @Override
                    public void execute() {
                        ScriptInjector.fromString(script).setWindow(ScriptInjector.TOP_WINDOW).inject();
                    }
                });
            }
        });

        return w;
    }

    @Override
    protected String getDialogCaption() {
        return I18n.powiazania.get();
    }

    public void buildAndShowDialog(int nodeId) {
        this.nodeId = nodeId;
        super.buildAndShowDialog();
    }

}
