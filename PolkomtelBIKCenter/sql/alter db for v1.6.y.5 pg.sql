﻿exec sp_check_version '1.6.y.5';
go

-- kolumna "only_for_user_id" wypełniana jest jeśli jakaś wiadomość przeznaczona
-- jest dla konkretnego użytkonwika systemu. W przypadku wpisania tam ID użytkownika systemowego
-- wiadomość będzie widoczna tylko przez tego użutkownika
if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_news') and name = 'only_for_user_id')
	alter table [dbo].[bik_news]
	add only_for_user_id int


-- hash do unikniecia dubli wiadomości automatycznych, że coś jest do skategoryzowania
if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_news') and name = 'text_hash')
	alter table [dbo].[bik_news] 
	add text_hash as convert(nvarchar(32),hashbytes('md5', substring([text], 1, 8000)),2)

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_news]') and name = N'idx_bik_news_text_hash')
	create nonclustered index [idx_bik_news_text_hash] on [dbo].[bik_news]
	(
		[text_hash] asc
	)

exec sp_update_version '1.6.y.5', '1.6.y.6';
go
