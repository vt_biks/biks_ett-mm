/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase;
import pl.fovis.client.entitydetailswidgets.IMainHeaderSharedTable;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.dqm.DQMCoordinatorDataBean;
import pl.fovis.common.dqm.DQMEvaluationBean;
import pl.fovis.common.dqm.DQMReportBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class DQMEvaluationWidget extends EntityDetailsWidgetFlatBase {

    protected IMainHeaderSharedTable imhst;
    protected VerticalPanel mainVp;

    public DQMEvaluationWidget(IMainHeaderSharedTable imhst) {
        this.imhst = imhst;
    }

    @Override
    public void displayData() {
        mainVp.clear();
        EntityDetailsDataBean data = getData();
        DQMEvaluationBean extradata = data.dqmEvaluationBean;
        Pair<DQMReportBean, List<DQMEvaluationBean>> reportExtradata = data.dqmReportEvaluations;
        Map<String, DQMCoordinatorDataBean> coordinatorData = data.dqmCoordinatorData;
        if (extradata != null) {
            imhst.addTranslatedRow(I18n.okresOd.get(), SimpleDateUtils.dateToString(extradata.dateFrom));
            imhst.addTranslatedRow(I18n.okresDoPelny.get(), SimpleDateUtils.dateToString(extradata.dateTo));
            imhst.addTranslatedRow(I18n.grupaDanych.get(), extradata.groupName);
            imhst.addTranslatedRow(I18n.istotnoscGrupyDanych.get(), extradata.significance);
            imhst.addTranslatedRow(I18n.opiekunGrupyDanych.get(), extradata.leaderName);
            imhst.addTranslatedRow(I18n.dataSporzadzenia.get(), SimpleDateUtils.dateToString(extradata.createdDate));
            imhst.addTranslatedRow(I18n.poprzedniPoziomJakosci.get(), extradata.prevQualityLevel);
            imhst.addTranslatedRow(I18n.aktualnyPoziomJakosci.get(), extradata.actualQualityLevel);
            imhst.addTranslatedRow(I18n.planDzialania.get(), extradata.actionPlan);
        }
        if (reportExtradata != null) {
            mainVp.add(new HTML(I18n.raportNaDzien.get() + " <b>" + SimpleDateUtils.dateToString(reportExtradata.v1.dateTo) + "</b>"));
            FlexTable table = new FlexTable();
            table.setCellSpacing(5);
            if (reportExtradata.v2 != null) {
                setReportHeader(table);
                int row = 1;
                for (DQMEvaluationBean oneRow : reportExtradata.v2) {
                    setReportRow(table, row++, oneRow);
                }
            }
            mainVp.add(table);
        }
        if (coordinatorData != null) {
            FlexTable table = new FlexTable();
            table.setCellSpacing(5);
            table.setWidth("100%");
            createCoordinatorHeader(table);
            int row = 1;
            for (Map.Entry<String, DQMCoordinatorDataBean> entryRow : coordinatorData.entrySet()) {
                setCoordinatorRow(table, row++, entryRow);
            }
            mainVp.add(table);
        }
    }

    @Override
    public Widget buildWidgets() {
        mainVp = new VerticalPanel();
        mainVp.setWidth("100%");
        return mainVp;
    }

    @Override
    public boolean hasContent() {
        EntityDetailsDataBean data = getData();
        return data.dqmEvaluationBean != null || data.dqmReportEvaluations != null || data.dqmCoordinatorData != null;
    }

    protected void setReportHeader(FlexTable table) {
        table.setHTML(0, 0, "<b>" + I18n.nr.get() + "</b>");
        table.setHTML(0, 1, "<b>" + I18n.grupaDanych.get() + "</b>");
        table.setHTML(0, 2, "<b>" + I18n.istotnoscGrupyDanych.get() + "</b>");
        table.setHTML(0, 3, "<b>" + I18n.opiekunGrupyDanych.get() + "</b>");
        table.setHTML(0, 4, "<b>" + I18n.poprzedniPoziomJakosci.get() + "</b>");
        table.setHTML(0, 5, "<b>" + I18n.aktualnyPoziomJakosci.get() + "</b>");
        table.setHTML(0, 6, "<b>" + I18n.planDzialania.get() + "</b>");
    }

    protected void createCoordinatorHeader(FlexTable table) {
        table.setHTML(0, 0, "<b>" + I18n.grupaDanych.get() + "</b>");
        table.setHTML(0, 1, "<b>" + I18n.statusOceny.get() + "</b>");
        table.setHTML(0, 2, "<b>" + I18n.liczbaBledow.get() + "</b>");
    }

    protected void setReportRow(FlexTable table, int row, DQMEvaluationBean oneRow) {
        table.setText(row, 0, String.valueOf(row));
        table.setText(row, 1, oneRow.groupName);
        table.setText(row, 2, oneRow.significance);
        table.setText(row, 3, oneRow.leaderName);
        table.setText(row, 4, oneRow.prevQualityLevel);
        table.setText(row, 5, oneRow.actualQualityLevel);
        table.setText(row, 6, oneRow.actionPlan);
    }

    protected void setCoordinatorRow(FlexTable table, int row, Map.Entry<String, DQMCoordinatorDataBean> entryRow) {
        final DQMCoordinatorDataBean bean = entryRow.getValue();
        List<DQMEvaluationBean> periods = bean.reportData;
        VerticalPanel periodVP = new VerticalPanel();
//        VerticalPanel statusVP = new VerticalPanel();
        for (DQMEvaluationBean period : periods) {
            HorizontalPanel statusPanel = new HorizontalPanel();
            statusPanel.setSpacing(0);
            statusPanel.add(new Label(SimpleDateUtils.dateToString(period.dateFrom) + " - " + SimpleDateUtils.dateToString(period.dateTo)));
            statusPanel.add(getStatusWidget(period.isAccepted));
            periodVP.add(statusPanel);
        }
        table.setText(row, 0, entryRow.getKey());
        table.setWidget(row, 1, periodVP);
        Anchor linkToFolder = new Anchor(bean.numerOpenedDataErrorIssues + "/" + bean.numerAllDataErrorIssues);
        linkToFolder.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getTabSelector().submitNewTabSelection(getNodeTreeCode(), bean.errorRegisterNodeId, true);
            }
        });
        table.setWidget(row, 2, linkToFolder);
        table.getRowFormatter().setStyleName(row, row % 2 != 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
//        table.setWidget(row, 2, statusVP);
    }

    protected Widget getStatusWidget(Integer isAccepted) {
        HorizontalPanel fp = new HorizontalPanel();
        fp.setSpacing(0);
        fp.add(new HTML("&nbsp;&nbsp;"));
        if (isAccepted == null) {
            fp.add(new HTML("<b>" + I18n.brakOceny.get() + "</b>"));
            return fp;
        }
        switch (isAccepted) {
            case -1:
//                return new Label(I18n.nieSkontolowano.get());
                fp.add(BIKClientSingletons.createIconImgEx("images/dqmrate.gif", "16px", "16px", null, null));
                fp.add(new HTML("&nbsp;&nbsp;"));
                fp.add(new Label(I18n.nieSkontolowano.get()));
                break;
            case 0:
//                return new Label(I18n.ocenaOdrzucona.get());
                fp.add(BIKClientSingletons.createIconImgEx("images/dqmraterejected.gif", "16px", "16px", null, null));
                fp.add(new HTML("&nbsp;&nbsp;"));
                fp.add(new Label(I18n.ocenaOdrzucona.get()));
                break;
            case 1:
//                return new Label(I18n.ocenaZaakceptowana.get());
                fp.add(BIKClientSingletons.createIconImgEx("images/dqmrateaccepted.gif", "16px", "16px", null, null));
                fp.add(new HTML("&nbsp;&nbsp;"));
                fp.add(new Label(I18n.ocenaZaakceptowana.get()));
                break;
            default:
                break;
//                return new Label();
        }
        return fp;
    }
}
