/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeNodeBean;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pmielanczuk
 */
public abstract class TreeSelectorForExpliciteTreeCodesDialog extends TreeSelectorDialog {

    protected Set<String> treeCodes;

    @Override
    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        BIKClientSingletons.getDQMService().getBikAllRootTreeNodesForTreeCodes(treeCodes, callback);
    }

    @Override
    protected Collection<Integer> getTreeIdsForFiltering() {
        List<Integer> list = new ArrayList<Integer>();
        for (String treeCode : treeCodes) {
            TreeBean tb = BIKClientSingletons.getTreeBeanByCode(treeCode);
            if (tb != null) {
                list.add(tb.id);
            }
        }
        return list;
    }

    public void buildAndShowDialog(Integer thisNodeId, Set<String> treeCodes, IParametrizedContinuation<TreeNodeBean> saveCont,
            IJoinedObjSelectionManager<Integer> alreadyJoinedManager) {
        this.treeCodes = treeCodes;
        super.buildAndShowDialog(thisNodeId, saveCont, alreadyJoinedManager);
    }
}
