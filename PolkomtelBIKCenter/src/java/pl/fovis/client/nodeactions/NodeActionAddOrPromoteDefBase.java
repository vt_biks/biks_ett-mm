/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.MetapediaDialog;
import pl.fovis.common.ArticleBean;
import pl.fovis.common.NewDefinitionDataBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public abstract class NodeActionAddOrPromoteDefBase extends NodeActionUpdatingBase {

    protected abstract String getActionSuccessMsg();

    protected abstract void callService(ArticleBean art, int /*Set<String>*/ action,
            AsyncCallback<Pair<Integer, List<String>>> refreshAfterAction);

    protected void fillArtProps(ArticleBean artB) {
        // no-op
    }

    @Override
    public void execute() {
        if (!isSocialUserLoggedInWithInfo()) {
            return;
        }

        BIKClientSingletons.getService().getNewDefinitionData(node.treeId, new StandardAsyncCallback<NewDefinitionDataBean>() {
            @Override
            public void onSuccess(NewDefinitionDataBean result) {
                final ArticleBean artB = new ArticleBean();
                artB.official = 1;
                fillArtProps(artB);
                artB.subject = getSubject();
                ArticleBean body = getBody();
                if (body != null) {
                    artB.body = body.body;
                    artB.official = body.official;
                    artB.versions = body.versions;
                    if (body.versions != null) {
                        artB.subject = getSubject().replace("(" + body.versions + ")", "").trim();
                    }
                }
                new MetapediaDialog().buildAndShowDialog(getCaption(), artB,
                        result.defAndVersion, isNewDefinition(), new IParametrizedContinuation<Pair<ArticleBean, Integer /*Set<String>*/>>() {
                            @Override
                            public void doIt(Pair<ArticleBean, Integer> param) {
                                final ArticleBean art = param.v1;
                                int action = param.v2;
                                callService(art, action/* depsToConnect*/, new StandardAsyncCallback<Pair<Integer, List<String>>>() {
                                            @Override
                                            public void onSuccess(Pair<Integer, List<String>> result) {
                                                BIKClientSingletons.showInfo(getActionSuccessMsg());
                                                if (!isNewDefinition() && art.official == artB.official && BaseUtils.safeEquals(art.versions, artB.versions)) { // była modyfikacja i nie zmieniano wersji definicji: nie odświeżamy drzewka, a tylko prawy panel
                                                    redisplayData();
                                                } else {
                                                    refreshRedisplayDataAndSelectRecord(result.v1/*, false*/);
                                                    refreshAndRedisplayDataByTreeCode(result.v2);
                                                }
                                            }
                                        });
                            }
                        });
            }
        });
    }

    protected String getNodeKindCode() {
        return null;
    }

    protected String getSubject() {
        return null;
    }

    protected ArticleBean getBody() {
        return null;
    }

    protected boolean isNewDefinition() {
        return true;
    }
}
