/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.biadmin;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.common.SAPBOObjectBean;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.FilterGridPageBase;
import pl.fovis.client.bikwidgets.HeaderCheckbox;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.client.dialogs.biadmin.BIAChangeDescriptionDialog;
import pl.fovis.client.dialogs.biadmin.BIAChangeNameDialog;
import pl.fovis.client.dialogs.biadmin.BIAUserSelectorDialog;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class BIAObjectManagerPage extends FilterGridPageBase<SAPBOObjectBean> {

    protected final static int MANAGE_TYPE_REPORTS = 1;
    protected final static int MANAGE_TYPE_UNIV = 2;
    protected final static int MANAGE_TYPE_CONN = 3;
    protected final static int MANAGE_TYPE_USER_GROUP = 4;
    protected BikCustomButton exportToExcelPb;
    public final static Map<String, String> KIND_TO_ICON_MAP = new HashMap<String, String>();

    //'Folder', 'Webi', 'CrystalReport', 'Pdf', 'FullClient', 'Rtf', 'Txt', 'Flash', 'Hyperlink', 'Excel', 'Powerpoint', 'Word'
    //'Folder', 'Universe', 'DSL.MetaDataFile'
    //'MetaData.DataConnection', 'CCIS.DataConnection', 'CommonConnection', 'Connection'
    static {
        KIND_TO_ICON_MAP.put("Webi", "webi");
        KIND_TO_ICON_MAP.put("Folder", "folder");
        KIND_TO_ICON_MAP.put("FavoritesFolder", "folder");
        KIND_TO_ICON_MAP.put("CrystalReport", "crystal");
        KIND_TO_ICON_MAP.put("Pdf", "pdf");
        KIND_TO_ICON_MAP.put("FullClient", "client");
        KIND_TO_ICON_MAP.put("Rtf", "report");
        KIND_TO_ICON_MAP.put("Txt", "report");
        KIND_TO_ICON_MAP.put("Flash", "flash");
        KIND_TO_ICON_MAP.put("Hyperlink", "link");
        KIND_TO_ICON_MAP.put("Excel", "excel");
        KIND_TO_ICON_MAP.put("Powerpoint", "powerpoint");
        KIND_TO_ICON_MAP.put("Word", "word");
        KIND_TO_ICON_MAP.put("Publication", "publication");
        KIND_TO_ICON_MAP.put("Universe", "universes");
        KIND_TO_ICON_MAP.put("DSL.MetaDataFile", "universeUNX");
        KIND_TO_ICON_MAP.put("MetaData.DataConnection", "connection");
        KIND_TO_ICON_MAP.put("Connection", "connection");
        KIND_TO_ICON_MAP.put("CCIS.DataConnection", "connectionRel");
        KIND_TO_ICON_MAP.put("CommonConnection", "connectionOLAP");
        KIND_TO_ICON_MAP.put("UserGroup", "users_icon");
        KIND_TO_ICON_MAP.put(null, "note");
    }
    protected SAPBOObjectBean bean;
    //
    protected ListBox manageLbx;
    protected TextBox ownerNameTbx;
    protected TextBox objectNameTbx;
    protected TextBox objIdTbx;
    protected TextBox parentIdTbx;
    protected PushButton refreshBtn;
    protected PushButton clearFiltersBtn;
    protected PushButton stopQueryBtn;
    protected PushButton changeNameBtn;
    protected PushButton changeDescrBtn;
    protected PushButton changeOwnerBtn;
    protected PushButton propertiesBtn;

    @Override
    public String getId() {
        return "admin:biadmin:objectManager";
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        // NO OP
    }

    @Override
    public boolean isPageEnabled() {
        return super.isPageEnabled() && BIKClientSingletons.isBIAdminEnabled();
    }

    @Override
    protected void initTableColumns(ColumnSortEvent.ListHandler<SAPBOObjectBean> sortHandler) {
        // select all
        Column<SAPBOObjectBean, Boolean> checkColumn = new Column<SAPBOObjectBean, Boolean>(new CheckboxCell(true, false)) {
            @Override
            public Boolean getValue(SAPBOObjectBean object) {
                // Get the value from the selection model.
                return selectionModel.isSelected(object);
            }
        };

        checkColumn.setFieldUpdater(new FieldUpdater<SAPBOObjectBean, Boolean>() {

            @Override
            public void update(int index, SAPBOObjectBean object, Boolean value) {
                selectionModel.setSelected(object, true);
                dataProvider.refresh();
            }
        });
        Header<Boolean> selectAllHeader = new Header<Boolean>(new HeaderCheckbox()) {

            @Override
            public Boolean getValue() {

                for (SAPBOObjectBean item : dataGrid.getVisibleItems()) {
                    if (!selectionModel.isSelected(item)) {
                        return false;
                    }
                }
                return dataGrid.getVisibleItems().size() > 0;
            }
        };
        selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {

            @Override
            public void update(Boolean value) {
                for (SAPBOObjectBean object : dataGrid.getVisibleItems()) {

                    selectionModel.setSelected(object, value);
                }
            }
        });

        dataGrid.addColumn(checkColumn, selectAllHeader/*, SafeHtmlUtils.fromSafeConstant("<br/>")*/);
        dataGrid.setColumnWidth(checkColumn, "40px"/*, Unit.PX*/);

        // ID
        Column<SAPBOObjectBean, String> idColumn = new Column<SAPBOObjectBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOObjectBean object) {
                return object.getId().toString();
            }
        };
        idColumn.setSortable(true);
        sortHandler.setComparator(idColumn, new Comparator<SAPBOObjectBean>() {
            @Override
            public int compare(SAPBOObjectBean o1, SAPBOObjectBean o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        dataGrid.addColumn(idColumn, I18n.scheduleID.get());
        dataGrid.setColumnWidth(idColumn, "100px"/*, Unit.PX*/);

        // Nazwa
        Column<SAPBOObjectBean, String> nameColumn = new Column<SAPBOObjectBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOObjectBean object) {
                return object.getName();
            }
        };
        nameColumn.setSortable(true);
        sortHandler.setComparator(nameColumn, new Comparator<SAPBOObjectBean>() {
            @Override
            public int compare(SAPBOObjectBean o1, SAPBOObjectBean o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        dataGrid.addColumn(nameColumn, I18n.scheduleNazwa.get());
        dataGrid.setColumnWidth(nameColumn, "300px"/*, Unit.PX*/);

        // Typ
        Column<SAPBOObjectBean, SafeHtml> typeColumn = new Column<SAPBOObjectBean, SafeHtml>(new SafeHtmlCell()) {
            @Override
            public SafeHtml getValue(SAPBOObjectBean object) {
                return SafeHtmlUtils.fromSafeConstant("<img src='images/" + KIND_TO_ICON_MAP.get(object.type) + ".gif' style='vertical-align: middle'> " + object.type);
            }
        };
        typeColumn.setSortable(true);
        sortHandler.setComparator(typeColumn, new Comparator<SAPBOObjectBean>() {
            @Override
            public int compare(SAPBOObjectBean o1, SAPBOObjectBean o2) {
                return o1.getType().compareTo(o2.getType());
            }
        });
        dataGrid.addColumn(typeColumn, I18n.typObiektu.get());
        dataGrid.setColumnWidth(typeColumn, "200px"/*, Unit.PX*/);

        // Lokalizacja
        Column<SAPBOObjectBean, String> locationColumn = new Column<SAPBOObjectBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOObjectBean object) {
                return object.getLocation();
            }
        };
        locationColumn.setSortable(true);
        sortHandler.setComparator(locationColumn, new Comparator<SAPBOObjectBean>() {
            @Override
            public int compare(SAPBOObjectBean o1, SAPBOObjectBean o2) {
                return o1.getLocation().compareTo(o2.getLocation());
            }
        });
        dataGrid.addColumn(locationColumn, I18n.lokalizacja.get());
        dataGrid.setColumnWidth(locationColumn, "500px"/*, Unit.PX*/);

        // Owner
        Column<SAPBOObjectBean, String> ownerColumn = new Column<SAPBOObjectBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOObjectBean object) {
                return object.getOwner();
            }
        };
        ownerColumn.setSortable(true);
        sortHandler.setComparator(ownerColumn, new Comparator<SAPBOObjectBean>() {
            @Override
            public int compare(SAPBOObjectBean o1, SAPBOObjectBean o2) {
                return o1.getOwner().compareTo(o2.getOwner());
            }
        });
        dataGrid.addColumn(ownerColumn, I18n.scheduleWlasciciel.get());
        dataGrid.setColumnWidth(ownerColumn, "200px"/*, Unit.PX*/);

        // Opis
        Column<SAPBOObjectBean, String> descriptionColumn = new Column<SAPBOObjectBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOObjectBean object) {
                return object.getDescription();
            }
        };
        descriptionColumn.setSortable(true);
        sortHandler.setComparator(descriptionColumn, new Comparator<SAPBOObjectBean>() {
            @Override
            public int compare(SAPBOObjectBean o1, SAPBOObjectBean o2) {
                return o1.getDescription().compareTo(o2.getDescription());
            }
        });
        dataGrid.addColumn(descriptionColumn, I18n.objectOpis.get());
        dataGrid.setColumnWidth(descriptionColumn, "500px"/*, Unit.PX*/);

        // ID parenta
        Column<SAPBOObjectBean, String> parentIdColumn = new Column<SAPBOObjectBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOObjectBean object) {
                return object.getParentId().toString();
            }
        };
        parentIdColumn.setSortable(true);
        sortHandler.setComparator(parentIdColumn, new Comparator<SAPBOObjectBean>() {
            @Override
            public int compare(SAPBOObjectBean o1, SAPBOObjectBean o2) {
                return o1.getParentId().compareTo(o2.getParentId());
            }
        });
        dataGrid.addColumn(parentIdColumn, I18n.scheduleParentID.get());
        dataGrid.setColumnWidth(parentIdColumn, "100px"/*, Unit.PX*/);

        // Utworzono
        Column<SAPBOObjectBean, String> createdColumn = new Column<SAPBOObjectBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOObjectBean object) {
                return SimpleDateUtils.toCanonicalString(object.getCreated());
            }
        };
        createdColumn.setSortable(true);
        sortHandler.setComparator(createdColumn, new Comparator<SAPBOObjectBean>() {
            @Override
            public int compare(SAPBOObjectBean o1, SAPBOObjectBean o2) {
                return SimpleDateUtils.toCanonicalString(o1.getCreated()).compareTo(SimpleDateUtils.toCanonicalString(o2.getCreated()));
            }
        });
        dataGrid.addColumn(createdColumn, I18n.scheduleUtworzono.get());
        dataGrid.setColumnWidth(createdColumn, "200px"/*, Unit.PX*/);

        // Zmodyfikowano
        Column<SAPBOObjectBean, String> updatedColumn = new Column<SAPBOObjectBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOObjectBean object) {
                return SimpleDateUtils.toCanonicalString(object.getModify());
            }
        };
        updatedColumn.setSortable(true);
        sortHandler.setComparator(updatedColumn, new Comparator<SAPBOObjectBean>() {
            @Override
            public int compare(SAPBOObjectBean o1, SAPBOObjectBean o2) {
                return SimpleDateUtils.toCanonicalString(o1.getModify()).compareTo(SimpleDateUtils.toCanonicalString(o2.getModify()));
            }
        });
        dataGrid.addColumn(updatedColumn, I18n.scheduleZmodyfikowano.get());
        dataGrid.setColumnWidth(updatedColumn, "200px"/*, Unit.PX*/);

        // CUID
        Column<SAPBOObjectBean, String> cuidColumn = new Column<SAPBOObjectBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOObjectBean object) {
                return object.getCuid();
            }
        };
        cuidColumn.setSortable(true);
        sortHandler.setComparator(cuidColumn, new Comparator<SAPBOObjectBean>() {
            @Override
            public int compare(SAPBOObjectBean o1, SAPBOObjectBean o2) {
                return o1.getCuid().compareTo(o2.getCuid());
            }
        });
        dataGrid.addColumn(cuidColumn, I18n.scheduleCUID.get());
        dataGrid.setColumnWidth(cuidColumn, "200px"/*, Unit.PX*/);

        // GUID
        Column<SAPBOObjectBean, String> guidColumn = new Column<SAPBOObjectBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOObjectBean object) {
                return object.getGuid();
            }
        };
        guidColumn.setSortable(true);
        sortHandler.setComparator(guidColumn, new Comparator<SAPBOObjectBean>() {
            @Override
            public int compare(SAPBOObjectBean o1, SAPBOObjectBean o2) {
                return o1.getGuid().compareTo(o2.getGuid());
            }
        });
        dataGrid.addColumn(guidColumn, I18n.scheduleGUID.get());
        dataGrid.setColumnWidth(guidColumn, "200px"/*, Unit.PX*/);

        // RUID
        Column<SAPBOObjectBean, String> ruidColumn = new Column<SAPBOObjectBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOObjectBean object) {
                return object.getRuid();
            }
        };
        ruidColumn.setSortable(true);
        sortHandler.setComparator(ruidColumn, new Comparator<SAPBOObjectBean>() {
            @Override
            public int compare(SAPBOObjectBean o1, SAPBOObjectBean o2) {
                return o1.getRuid().compareTo(o2.getRuid());
            }
        });
        dataGrid.addColumn(ruidColumn, I18n.scheduleRUID.get());
        dataGrid.setColumnWidth(ruidColumn, "200px"/*, Unit.PX*/);

        exportToExcelPb = createExportResultsBtn();
        exportToExcelPb.setVisible(false);
        exportToExcelPb.addStyleName("BIAdmin");
    }

    @Override
    protected void refreshActionButtons() {
        Set<SAPBOObjectBean> selectedSet = selectionModel.getSelectedSet();
        boolean rowsSelected = !BaseUtils.isCollectionEmpty(selectedSet);
        changeNameBtn.setVisible(rowsSelected);
        changeDescrBtn.setVisible(rowsSelected);
        changeOwnerBtn.setVisible(rowsSelected);
    }

    @Override
    protected void createFilters(VerticalPanel leftVp) {
        FlexTable filterWidgets = new FlexTable();
        filterWidgets.setCellSpacing(5);
        // zarzadzaj
        filterWidgets.setWidget(0, 0, new Label(I18n.zarzadzaj.get()));
        filterWidgets.setWidget(0, 1, manageLbx = new ListBox());
        manageLbx.addItem(I18n.raporty.get(), String.valueOf(MANAGE_TYPE_REPORTS));
        manageLbx.addItem(I18n.swiatyobiektow.get(), String.valueOf(MANAGE_TYPE_UNIV));
        manageLbx.addItem(I18n.polaczenia.get(), String.valueOf(MANAGE_TYPE_CONN));
        manageLbx.addItem(I18n.grupyUzytkownikow.get(), String.valueOf(MANAGE_TYPE_USER_GROUP));
        manageLbx.setSelectedIndex(0);
        manageLbx.setWidth("160px");

        // nazwa
        filterWidgets.setWidget(1, 0, new Label(I18n.scheduleNazwa.get()));
        filterWidgets.setWidget(1, 1, objectNameTbx = new TextBox());
        objectNameTbx.setText("*");
        objectNameTbx.setWidth("152px");

        // owner
        filterWidgets.setWidget(2, 0, new Label(I18n.scheduleWlasciciel.get()));
        filterWidgets.setWidget(2, 1, ownerNameTbx = new TextBox());
        ownerNameTbx.setText("*");
        ownerNameTbx.setWidth("152px");

        // id obiektu
        filterWidgets.setWidget(3, 0, new Label(I18n.scheduleID.get()));
        filterWidgets.setWidget(3, 1, objIdTbx = new TextBox());
        objIdTbx.setText("*");
        objIdTbx.setWidth("152px");

        // id raportu
        filterWidgets.setWidget(4, 0, new Label(I18n.objectManagerAncestroID.get()));
        filterWidgets.setWidget(4, 1, parentIdTbx = new TextBox());
        parentIdTbx.setText("*");
        parentIdTbx.setWidth("152px");
        // refresh
        refreshBtn = new PushButton(I18n.odswiez.get());
        NewLookUtils.makeCustomPushButton(refreshBtn);
        refreshBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                refreshGrid();
            }
        });

        HorizontalPanel buttonToolsPanel = new HorizontalPanel();
        buttonToolsPanel.setSpacing(0);
        clearFiltersBtn = new PushButton(I18n.deleteFilters.get());
        NewLookUtils.makeCustomPushButton(clearFiltersBtn);
        clearFiltersBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                clearFilters();
            }
        });
        stopQueryBtn = new PushButton(I18n.przerwij.get());
        NewLookUtils.makeCustomPushButton(stopQueryBtn);
        stopQueryBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                stopQueryBtn.setVisible(false);
                BIKClientSingletons.getService().stopBIAQuery(new StandardAsyncCallback<Void>("Error in stopBIAQuery") {

                    @Override
                    public void onSuccess(Void result) {
//                        BIKClientSingletons.showInfo(I18n.);
                    }
                });
            }
        });
        stopQueryBtn.setVisible(false);
        buttonToolsPanel.add(clearFiltersBtn);
        buttonToolsPanel.add(stopQueryBtn);
        filterWidgets.setWidget(5, 0, refreshBtn);
        filterWidgets.setWidget(5, 1, buttonToolsPanel);
        leftVp.add(filterWidgets);
//        leftVp.add(refreshBtn);
    }

    protected void refreshGrid() {
        stopQueryBtn.setVisible(true);
        bean = new SAPBOObjectBean();
        bean.manageType = BaseUtils.tryParseInt(manageLbx.getSelectedValue());
        bean.id = BaseUtils.tryParseInteger(objIdTbx.getText(), -1);
        bean.parentId = BaseUtils.tryParseInteger(parentIdTbx.getText(), -1);
        String ownerText = ownerNameTbx.getText();
        bean.owner = BaseUtils.safeEqualsStr(ownerText, "*", true) ? null : ownerText;
        String nameText = objectNameTbx.getText();
        bean.name = BaseUtils.safeEqualsStr(nameText, "*", true) ? null : nameText;
//        bean.ancestorId = BaseUtils.tryParseInteger(folderId.getText(), -1);
//        int selectedDateFilter = BaseUtils.tryParseInt(dateLbx.getSelectedValue());
//        bean.intervalHours = BaseUtils.tryParseInt(dateLbx.getSelectedValue());
//        if (selectedDateFilter == 0) {
//            bean.started = startDate.getValue();
//            bean.ended = endDate.getValue();
//        }
//        bean.reportsId = reportsObjId;
//        bean.ancestorsId = ancestorsObjId;
//        bean.connectionsNodeId = connectionsNodeId;
        showLoadingWidget();
        refreshBtn.setEnabled(false);
        clearFiltersBtn.setEnabled(false);
        BIKClientSingletons.getService().getBIAObjects(bean, new StandardAsyncCallback<List<SAPBOObjectBean>>("Error in getBIAObjects") {

            @Override
            public void onSuccess(List<SAPBOObjectBean> result) {
                stopQueryBtn.setVisible(false);
                showDataGrid();
                List<SAPBOObjectBean> list = dataProvider.getList();
                list.clear();
                list.addAll(result);
//                if (result.size() >= 1000) {
//                    new SimpleInfoDialog().buildAndShowDialog(I18n.schedulePobranoDuzoRekordow.get(), null, null);
//                }
//                exportToExcelPb.setVisible(!list.isEmpty());
                dataProvider.refresh();
                selectionModel.clear();
                refreshActionButtons();
                dataGrid.redrawHeaders();
                refreshBtn.setEnabled(true);
                clearFiltersBtn.setEnabled(true);
                exportToExcelPb.setVisible(!list.isEmpty());
            }

            @Override
            public void onFailure(Throwable caught) {
                stopQueryBtn.setVisible(false);
                showDataGrid();
                refreshBtn.setEnabled(true);
                clearFiltersBtn.setEnabled(true);
//                new SimpleInfoDialog().buildAndShowDialog("Error: " + caught.getMessage(), null, null);
                super.onFailure(caught);
            }
        });
    }

    @Override
    protected void createActionButtons() {
        changeNameBtn = new PushButton(I18n.objectZmienNazwe.get());
        NewLookUtils.makeCustomPushButton(changeNameBtn);
        changeNameBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new BIAChangeNameDialog().buildAndShowDialog(getSelectedObjects(), new IParametrizedContinuation<Set<SAPBOObjectBean>>() {

                    @Override
                    public void doIt(Set<SAPBOObjectBean> param) {
                        final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                        infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.scheduleTrwaWykonywanieAkcji.get(), true);
                        BIKClientSingletons.getService().changeBIANameAndDescr(param, bean.manageType, new StandardAsyncCallback<Void>("Error in changeNameBIAObjects") {

                            @Override
                            public void onSuccess(Void result) {
                                infoDialog.hideDialog();
                                BIKClientSingletons.showInfo(I18n.objectZmienionoNazwe.get());
                                refreshGrid();
                            }

                            @Override
                            public void onFailure(Throwable caught) {
                                infoDialog.hideDialog();
                                super.onFailure(caught);
                            }
                        });
                    }
                });
            }
        });
        //
        changeDescrBtn = new PushButton(I18n.objectZmienOpis.get());
        NewLookUtils.makeCustomPushButton(changeDescrBtn);
        changeDescrBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new BIAChangeDescriptionDialog().buildAndShowDialog(getSelectedObjects(), new IParametrizedContinuation<Set<SAPBOObjectBean>>() {

                    @Override
                    public void doIt(Set<SAPBOObjectBean> param) {
                        final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                        infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.scheduleTrwaWykonywanieAkcji.get(), true);
                        BIKClientSingletons.getService().changeBIANameAndDescr(param, bean.manageType, new StandardAsyncCallback<Void>("Error in changeDescrBIAObjects") {

                            @Override
                            public void onSuccess(Void result) {
                                infoDialog.hideDialog();
                                BIKClientSingletons.showInfo(I18n.objectZmienionoOpis.get());
                                refreshGrid();
                            }

                            @Override
                            public void onFailure(Throwable caught) {
                                infoDialog.hideDialog();
                                super.onFailure(caught);
                            }
                        });
                    }
                });
            }
        });
        //
        changeOwnerBtn = new PushButton(I18n.objectZmienWlasciciela.get());
        NewLookUtils.makeCustomPushButton(changeOwnerBtn);
        changeOwnerBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().getBIAUsers(new StandardAsyncCallback<List<SAPBOObjectBean>>("Error in getBIAUsers") {

                    @Override
                    public void onSuccess(List<SAPBOObjectBean> result) {
                        if (BaseUtils.isCollectionEmpty(result)) {
                            new SimpleInfoDialog().buildAndShowDialog(I18n.brakWynikow.get(), null, null);
                            return;
                        }
                        new BIAUserSelectorDialog().buildAndShowDialog(result, new IParametrizedContinuation<SAPBOObjectBean>() {

                            @Override
                            public void doIt(SAPBOObjectBean param) {
                                final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                                infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.scheduleTrwaWykonywanieAkcji.get(), true);
                                param.manageType = bean.manageType;
                                BIKClientSingletons.getService().changeOwnerBIAObjects(getSelectedObjectsIds(), param, new StandardAsyncCallback<Void>("Error in changeOwnerBIAObjects") {

                                    @Override
                                    public void onSuccess(Void result) {
                                        infoDialog.hideDialog();
                                        BIKClientSingletons.showInfo(I18n.objectZmienionoWlasciciela.get());
                                        refreshGrid();
                                    }

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        infoDialog.hideDialog();
                                        super.onFailure(caught);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    @SuppressWarnings("unchecked")
    protected void clearFilters() {
        clearTb(objectNameTbx, ownerNameTbx, objIdTbx, parentIdTbx);
        clearListBox(manageLbx);
    }

    @Override
    protected void optAddActionButtonsToPanel(HorizontalPanel actionPanel, HorizontalPanel actionHorizontalPanel) {
        actionPanel.add(changeNameBtn);
        actionPanel.add(changeDescrBtn);
        actionPanel.add(changeOwnerBtn);
        actionHorizontalPanel.add(exportToExcelPb);
    }

    @Override
    protected int getGridHeight() {
        return 2500;
    }

    private BikCustomButton createExportResultsBtn() {
        return new BikCustomButton(I18n.eksportWynikow.get() /* I18N:  */, "importCsv" /* I18N: no */,
                new IContinuation() {

                    @Override
                    public void doIt() {
                        StringBuilder sb = new StringBuilder(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet());
                        sb.append("?").append(BIKConstants.BIA_OBJECT_RESULTS_EXPORT_PARAM_NAME).append("=").append(BIKConstants.EXPORT_FORMAT_XLSX).append("&")
                        .append(BIKConstants.BIA_OBJECT_PHRASE_PARAM_NAME).append("=").append(I18n.objectManager.get())
                        .append("&").append(BIKConstants.BIA_OBJECT_ID).append("=").append(bean.id)
                        .append("&").append(BIKConstants.BIA_OBJECT_PARENT_ID).append("=").append(bean.parentId)
                        .append("&").append(BIKConstants.BIA_OBJECT_MANAGE_TYPE).append("=").append(bean.manageType);

                        if (bean.owner != null) {
                            sb.append("&").append(BIKConstants.BIA_OBJECT_OWNER).append("=").append(bean.owner);
                        }

                        if (bean.name != null) {
                            sb.append("&").append(BIKConstants.BIA_OBJECT_NAME).append("=").append(bean.name);
                        }
                        Window.open(sb.toString(), "_blank", "");
                    }
                });
    }
}
