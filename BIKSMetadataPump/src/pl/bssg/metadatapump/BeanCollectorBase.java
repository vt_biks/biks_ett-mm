/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tflorczak
 */
public abstract class BeanCollectorBase<T> implements IBeanCollector<T> {

    protected List<T> list = new ArrayList<T>();
    protected int bufforSize;
    protected boolean wasSomethingAdded;

    public BeanCollectorBase(int bufforSize) {
        this.bufforSize = bufforSize;
    }

    @Override
    public void add(T bean) {
        list.add(bean);
        if (!wasSomethingAdded) {
            wasSomethingAdded = true;
        }
        if (list.size() >= bufforSize) {
            insertListIntoDatabaseAndClear();
        }
    }

    // wykonujemy na koniec, aby wrzucić resztę listy
    @Override
    public void flush() {
        insertListIntoDatabaseAndClear();
    }

    protected abstract void insertListIntoDatabaseAndClear();

    public List<T> getList() {
        return list;
    }

    public boolean wasSomethingAdded() {
        return wasSomethingAdded;
    }
}
