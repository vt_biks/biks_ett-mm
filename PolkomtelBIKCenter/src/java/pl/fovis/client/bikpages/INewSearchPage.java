/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.TextBox;
import java.util.List;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;

/**
 *
 * @author tflorczak
 */
public interface INewSearchPage {

    public TextBox getSearchPhraseTb();

    public void performSearchAndShow();

    public void setHasNewAttributes(boolean hasNewAttributes);

    public void updateAttributeShowedList(List<ISearchByAttrValueCriteria> sbavcs, String groupingExpr);
}
