/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.javasdk;

import com.crystaldecisions.sdk.framework.CrystalEnterprise;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.framework.ISessionMgr;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class SapBOReportInstancePump {

    protected String sapUser;
    protected String sapPassword;
    protected String sapServer;
    protected String reportSiId;
    protected Integer userId;

    public SapBOReportInstancePump(String reportSiId, Integer userId) {
        this.reportSiId = reportSiId;
        this.userId = userId;
    }

    public Pair<Integer, String> pump() {
        Integer instanceId = null;
        try {
            ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
            IEnterpriseSession enterpriseSession;
            enterpriseSession = sessionMgr.logon(sapUser, sapPassword, sapServer, SapBOPump.AUTHENTICATION_METHOD);
            IInfoStore iStore = (IInfoStore) enterpriseSession.getService("InfoStore");
            if (userId != null) {
                boolean checkRight = ReportAccessChecker.checkRight(iStore, reportSiId, userId);
                if (!checkRight) {
                    return null;
                }
            }
            IInfoObjects results = iStore.query("select SI_LAST_SUCCESSFUL_INSTANCE_ID from CI_INFOOBJECTS where SI_ID = " + reportSiId + " and SI_KIND = 'Webi'");
            if (results == null || results.isEmpty()) {
                return new Pair<Integer, String>(instanceId, "Error in getting object instance!");
            }
            IInfoObject obj = (IInfoObject) results.get(0);
            instanceId = obj.properties().getInt("SI_LAST_SUCCESSFUL_INSTANCE_ID");
            enterpriseSession.logoff();
        } catch (Exception ex) {
            return new Pair<Integer, String>(instanceId, "No connection with BO! Error:" + ex);
        }
        return new Pair<Integer, String>(instanceId, "Success");
    }

    public void setBOConnectionParameters(String server, String user, String password) {
        this.sapServer = server;
        this.sapUser = user;
        this.sapPassword = password;
    }
}
