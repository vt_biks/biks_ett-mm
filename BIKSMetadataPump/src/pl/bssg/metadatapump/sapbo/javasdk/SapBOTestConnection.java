/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.javasdk;

import com.crystaldecisions.sdk.framework.CrystalEnterprise;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.framework.ISessionMgr;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import pl.bssg.metadatapump.MetadataTestConnectionBase;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class SapBOTestConnection extends MetadataTestConnectionBase {

    protected int id;

    public SapBOTestConnection(MssqlConnectionConfig mcfg, int id) {
        super(mcfg);
        this.id = id;
    }

    @Override
    public Pair<Integer, String> testConnection() {
        SapBOBean configBean;
        try {
            configBean = adhocDao.createBeanFromNamedQry("getBOConnectionParamatersById", SapBOBean.class, false, id);
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Invalid login for data loading! Error: " + e.getMessage());
        }

        try {
            ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
            IEnterpriseSession enterpriseSession;
            enterpriseSession = sessionMgr.logon(configBean.userName, configBean.password, configBean.server, SapBOPump.AUTHENTICATION_METHOD);
            if (enterpriseSession == null) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connection with BO");
            }
            IInfoStore iStore = (IInfoStore) enterpriseSession.getService("InfoStore");
            IInfoObjects objects = iStore.query("SELECT COUNT(SI_ID) FROM CI_INFOOBJECTS");
            if (objects == null) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "IStore gets 0 objects from CI_INFOOBJECTS");
            }
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connection with BO! Error: " + e.getMessage());
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, PumpConstants.PUMP_TEST_CONNECTION_OK);
    }
}
