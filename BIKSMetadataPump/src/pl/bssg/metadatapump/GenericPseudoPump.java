/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.LameRuntimeException;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class GenericPseudoPump implements IBIKPumpTaskNew {

    protected IBIKPumpTaskNew properPump;
    protected Class<? extends IBIKPumpTaskNew> classPump;
    protected String optPumpInstanceName;

    public GenericPseudoPump(Class<? extends IBIKPumpTaskNew> classPump) {
        this.classPump = classPump;
        this.optPumpInstanceName = null;
    }

    public GenericPseudoPump(Class<? extends IBIKPumpTaskNew> classPump, String optPumpInstanceName) {
        this.classPump = classPump;
        this.optPumpInstanceName = optPumpInstanceName;
    }

    public Integer setConnectionAndLog(String urlForPublicAccess, String optDirForUpload,
            IBeanConnectionEx<Object> mc, INamedSqlsDAO<Object> adhoDAO, IPumpLogger log) {
        setOptPumpInstanceName(optPumpInstanceName);
        return getPump().setConnectionAndLog(urlForPublicAccess, optDirForUpload, mc, adhoDAO, log);
    }

    public Pair<Integer, String> pump() {
        try {
            return getPump().pump();
        } finally {
            properPump = null;
        }
    }

    public String getPumpGroup() {
        return getPump().getPumpGroup();
    }

    public String getSourceName() {
        return getPump().getSourceName();
    }

    public boolean isActionAfterNeeded() {
        return getPump().isActionAfterNeeded();
    }

    public void setOptPumpInstanceName(String instanceName) {
        getPump().setOptPumpInstanceName(instanceName);
    }

    protected IBIKPumpTaskNew getPump() {
        if (properPump == null) {
            try {
                properPump = classPump.newInstance();
            } catch (Exception ex) {
                throw new LameRuntimeException("Error in newInstance", ex);
            }
        }
        return properPump;
    }
}
