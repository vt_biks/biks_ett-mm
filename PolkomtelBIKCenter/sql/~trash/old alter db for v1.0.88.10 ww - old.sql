exec sp_check_version '1.0.88.10';
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


IF EXISTS (SELECT name FROM sysobjects
      WHERE name = 'trg_bik_node_chunking' AND type = 'TR')
   DROP TRIGGER trg_bik_node_chunking
GO

CREATE TRIGGER trg_bik_node_chunking
ON bik_node
FOR insert, update, delete AS
begin
    -- diag: print cast(sysdatetime() as varchar(23)) + ': start!'

set nocount on

declare @diags_level int = 0;

declare @to_delete table (id int not null primary key);

declare @bik_node_name_chunk table (node_id int not null, tree_id int not null, chunk_txt varchar(3) not null)

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before calc @to_delete'
    end
   
insert into @to_delete (id)    
select d.id from deleted d left join inserted i on d.id = i.id
where i.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 0 and i.is_deleted = 1)
   
   declare @rowcnt int;
   
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before delete by ids'
      set @rowcnt = (select count(*) from @to_delete)
      print cast(sysdatetime() as varchar(23)) + ': before delete by ids, rowcnt=' + cast(@rowcnt as varchar(20))
    end
   
delete from bik_node_name_chunk where node_id in (select id from @to_delete
--select d.id from deleted d left join inserted i on d.id = i.id
--where i.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 0 and i.is_deleted = 1)
);

declare @to_insert table (id int not null, name varchar(max) not null, tree_id int not null);

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before calc @to_insert'
    end
   
insert into @to_insert (id, name, tree_id)
select i.id, i.name, i.tree_id from inserted i left join deleted d on d.id = i.id
where d.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 1 and i.is_deleted = 0);

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before declare cursor'
      set @rowcnt = (select count(*) from @to_insert)
      print cast(sysdatetime() as varchar(23)) + ': before declare cursor, rowcnt=' + cast(@rowcnt as varchar(20))
    end
   
declare nodes cursor for select id, name, tree_id from @to_insert;
/*select id, name, tree_id from bik_node where id in (
select i.id from inserted i left join deleted d on d.id = i.id
where d.name is null or i.name <> d.name or (d.is_deleted = 1 and i.is_deleted = 0)
)*/;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before open cursor'
    end
   
open nodes;

declare @id int, @name varchar(8000), @tree_id int;

  if @diags_level > 0 begin
  -- diag:
    print cast(sysdatetime() as varchar(23)) + ': before first fetch'
  end
 
fetch next from nodes into @id, @name, @tree_id;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before loop'
    end
   
    declare @cnt int = 0;
   
while @@fetch_status = 0
begin
  insert into @bik_node_name_chunk (node_id, chunk_txt, tree_id)
  select distinct @id, chunk_str, @tree_id
  from dbo.fn_split_string(@name, 3);
 
    if @diags_level > 1 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': in loop after insert'
    end
   
  set @cnt = @cnt + 1;
 
  if (@cnt % 1000 = 0) begin
 
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': in loop, before insert into bik_node_name_chunk, cnt:' + cast(@cnt as varchar(10))
    end
   
    insert into bik_node_name_chunk (node_id, chunk_txt, tree_id)
    select node_id, chunk_txt, tree_id
    from @bik_node_name_chunk
   
    delete from @bik_node_name_chunk
  end;   
 
  if @diags_level > 1 begin
     -- diag:
     print cast(sysdatetime() as varchar(23)) + ': in loop before fetch next'
  end
 
  fetch next from nodes into @id, @name, @tree_id;
  if @diags_level > 1 begin
    -- diag:
    print cast(sysdatetime() as varchar(23)) + ': in loop after fetch next'
  end
 
end;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': after loop, before insert into bik_node_name_chunk, @cnt=' + cast(@cnt as varchar(10))
    end
   
    insert into bik_node_name_chunk (node_id, chunk_txt, tree_id)
    select node_id, chunk_txt, tree_id
    from @bik_node_name_chunk
     
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': closing, deallocating cursor'
    end
   
close nodes;

deallocate nodes;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': done, stop!'
    end
end
go


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_attribute') and name = 'attr_type')
  alter table bik_attribute add attr_type int not null default 0;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_attribute_linked') and name = 'fixed_value')
  alter table bik_attribute_linked add fixed_value varchar(max) null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_attribute_linked') and name = 'original_value')
  alter table bik_attribute_linked add original_value varchar(max) null;
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

if OBJECT_ID (N'dbo.fn_normalize_text_for_fts', N'FN') is not null
    drop function dbo.fn_normalize_text_for_fts;
go

create function dbo.fn_normalize_text_for_fts(@txt varchar(max))
returns varchar(max)
as
begin
  if @txt is null return null

  set @txt = ltrim(rtrim(@txt))
  if @txt = '' return null
  
  set @txt = replace(@txt, '_', ' ')
  set @txt = replace(@txt, '(', ' ')
  set @txt = replace(@txt, '/', ' ')
  set @txt = replace(@txt, ')', ' ')
  set @txt = replace(@txt, '\', ' ')
  set @txt = replace(@txt, '[', ' ')
  set @txt = replace(@txt, ']', ' ')
  set @txt = replace(@txt, '*', ' ')
  set @txt = replace(@txt, '"', ' ')
  set @txt = replace(@txt, '''', ' ')

  set @txt = ltrim(rtrim(@txt))
  if @txt = '' set @txt = null
  
  return @txt
end
go

/*

select dbo.fn_normalize_text_for_fts('ala_ma [kota]')

*/

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


if object_id('[dbo].[fn_split_by_sep]') is not null
  drop function [dbo].[fn_split_by_sep]
go

-- @trimOpt = 0 -> do not trim substr
--          = 1 -> do ltrim substr
--          = 2 -> do rtrim substr
--          = 3 -> do full trim substr
--          & 4 <> 0 -> skip empty substr
CREATE FUNCTION [dbo].[fn_split_by_sep] (@str varchar(max), @sep varchar(max), @trimOpt int)
RETURNS @tab TABLE (str varchar(max))
AS
BEGIN
  declare @pos int = 1;
  declare @len int = DATALENGTH(@str);
  declare @seplen int = DATALENGTH(@sep)
  declare @pos2 int
  declare @substr varchar(max)
  
  while @pos <= @len begin
    set @pos2 = charindex(@sep, @str, @pos)
    
    if @pos2 = 0 set @pos2 = @len + 1
    
    set @substr = substring(@str, @pos, @pos2 - @pos)
    
    if @trimOpt & 1 <> 0 set @substr = ltrim(@substr)
    if @trimOpt & 2 <> 0 set @substr = rtrim(@substr)
    
    if @trimOpt & 4 = 0 or DATALENGTH(@substr) <> 0 -- @substr <> '' - to nie zadzia�a dla spacji
      insert into @tab (str) values (@substr)
    
    set @pos = @pos2 + @seplen
  end

  RETURN;
END;
GO


/*

select charindex('a', 'ala ma kota', 2)

select charindex('x', 'ala ma kota', 2)

select len(' ')

select case when len(' ') <> 0 then 'jest znak spacji' else 'brak spacji' end,
  case when datalength(' ') <> 0 then 'jest znak spacji' else 'brak spacji' end,
  case when ' ' <> '' then 'jest znak spacji' else 'brak spacji' end
  
select * from dbo.fn_split_by_sep('ala ama kota', 'a', 7)

select * from dbo.fn_split_by_sep('', ',', 7)

select * from dbo.fn_split_by_sep(null, ',', 7)

*/

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attributes]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attributes
go

create procedure [dbo].[sp_verticalize_node_attributes](@source varchar(max), @nodeIdCol sysname, @normalCols varchar(max), @identCols varchar(max))
as
begin
  declare @attrNamesTab table (name sysname not null primary key, attr_type int not null)
  
  insert into @attrNamesTab (name, attr_type)
  select str, 1 from dbo.fn_split_by_sep(@normalCols, ',', 7)

  insert into @attrNamesTab (name, attr_type)
  select str, 2 from dbo.fn_split_by_sep(@identCols, ',', 7)

  -- mamy ju� nazwy atrybut�w i typy w tabelce, trzeba sprawdzi� teraz o jakie node_kindy chodzi
  
  declare @attrNodeKindsTab table (node_kind_id int not null)
  
  declare @attrNodeKindsSql varchar(max) = 'select distinct n.node_kind_id from bik_node as n inner join ' + @source + ' as x on n.id = x.' + @nodeIdCol

  insert into @attrNodeKindsTab (node_kind_id)
  exec (@attrNodeKindsSql)

  -- znamy nazwy atrybut�w i node_kindy, teraz uzupe�niamy bik_attribute, ew. poprawiamy tam typy
  
  update bik_attribute 
  set attr_type = ant.attr_type
  from @attrNamesTab ant, @attrNodeKindsTab ankt
  where bik_attribute.name = ant.name and bik_attribute.node_kind_id = ankt.node_kind_id
  
  insert into bik_attribute (name, node_kind_id, attr_type)
  select ant.name, ankt.node_kind_id, ant.attr_type
  from @attrNamesTab ant cross join @attrNodeKindsTab ankt left join bik_attribute on 
    bik_attribute.name = ant.name and bik_attribute.node_kind_id = ankt.node_kind_id
  where bik_attribute.id is null
  
  -- wrzucamy warto�ci atrybut�w do tabeli pomocniczej
  
  declare @attrValsTab table (attribute_id int not null, node_id int not null, value varchar(max), fixed_value varchar(max))
  
  declare attrCur cursor for select name, attr_type from @attrNamesTab
  
  open attrCur
  
  declare @attrName sysname, @attrType int
  declare @attrValSql varchar(max)
  
  fetch next from attrCur into @attrName, @attrType
  
  while @@fetch_status = 0
  begin
    set @attrValSql = 'select a.id, x.' + @nodeIdCol + ', x.' + @attrName + ', ' +
      case when @attrType = 2 then 'null' else 'dbo.fn_normalize_text_for_fts(' + @attrName + ')' end +
      ' from ' + @source + ' as x inner join bik_node n on n.id = x.' + @nodeIdCol +
      ' inner join bik_attribute a on n.node_kind_id = a.node_kind_id and a.name = ''' + @attrName + '''' +
      ' where x.' + @attrName + ' is not null'
  
    print 'sql for attr=' + @attrName + ' is:
    ' + @attrValSql
  
    insert into @attrValsTab (attribute_id, node_id, value, fixed_value)
    exec (@attrValSql)
  
    fetch next from attrCur into @attrName, @attrType
  end
    
  close attrCur
  
  deallocate attrCur
  
  -- zak�adamy, �e warto�ci atrybut�w z typem <> 0 (automatyczne) mog� teraz istnie� 
  -- tylko w przypadku, gdy maj� nadpisan� warto�� (lub s� dopiero co skonwertowane we 
  -- wcze�niejszym kroku)
  
  update bik_attribute_linked
  set original_value = avt.value
  from @attrValsTab avt
  where avt.attribute_id = bik_attribute_linked.attribute_id and avt.node_id = bik_attribute_linked.node_id
  
  insert into bik_attribute_linked (node_id, attribute_id, value, fixed_value)
  select avt.node_id, avt.attribute_id, avt.value, avt.fixed_value
  from @attrValsTab avt left join bik_attribute_linked on
  avt.attribute_id = bik_attribute_linked.attribute_id and avt.node_id = bik_attribute_linked.node_id
  where bik_attribute_linked.id is null
  
  -- select * from @attrValsTab
  
  --declare @sql varchar(max) = 'select ' + @nodeIdCol + ', ' + @normalCols + ', ' + @identCols + ' from ' + @source
  --print @sql
  
  --select * from @attrNamesTab
  --select * from @attrNodeKindsTab ankt inner join bik_node_kind nk on ankt.node_kind_id = nk.id
end
go


/*

exec [sp_verticalize_node_attributes] 'bik_sapbo_extradata', 'node_id', 'author, owner', 'guid'

exec [sp_verticalize_node_attributes] 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null


select * from bik_attribute

-----------------------
-----------------------
-----------------------

select * from bik_sapbo_extradata
select * from bik_sapbo_universe_table
select * from bik_sapbo_universe_connection
select * from bik_sapbo_universe_object
select * from bik_sapbo_query
select * from bik_sapbo_universe_column

select count(*) from bik_sapbo_extradata
select count(*) from bik_sapbo_universe_table
select count(*) from bik_sapbo_universe_connection
select count(*) from bik_sapbo_universe_object
select count(*) from bik_sapbo_query
select count(*) from bik_sapbo_universe_column

select * from bik_sapbo_universe_table
where coalesce(sql_of_derived_table, '') <> coalesce(sql_of_derived_table_with_alias, '')


select * from bik_sapbo_query


*/

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

--exec sp_update_version '1.0.88.8', '1.0.88.9';
--go
