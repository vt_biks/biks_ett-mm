/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import simplelib.logging.ILameLogger;

/**
 *
 * @author ctran
 */
public class LocalArchiveFileHelper implements IArchiveFileHelper {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    public LocalArchiveFileHelper() {
    }

    @Override
    public void makeSureFolderExists(String dirPath) {
        File dir = new File(dirPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
    }

    @Override
    public void writeTempFile2SavedFolder(String tempFilePath, String destFilePath) throws IOException {
        File destFile = new File(destFilePath);
        destFile.createNewFile();
        LameUtils.copy2File(new FileOutputStream(new File(destFilePath)), new FileInputStream(new File(tempFilePath)));
    }

    @Override
    public boolean checkFileExists(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }
}
