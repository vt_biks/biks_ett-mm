/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package html2pdf;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import java.io.File;

/**
 *
 * @author ctran
 */
public class Html2pdf {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Throwable {
        convert("C:\\TEMP\\BIKS\\upload\\treeexport\\Systemy.html", "C:\\TEMP\\BIKS\\upload\\treeexport\\Systemy.pdf");
    }

    public static final void convert(String htmlFilePath, String pdfFilePath) throws Throwable {

        ConverterProperties properties = new ConverterProperties();
        properties.setFontProvider(new DefaultFontProvider(true, true, true));
        HtmlConverter.convertToPdf(new File(htmlFilePath), new File(pdfFilePath), properties);
    }
}
