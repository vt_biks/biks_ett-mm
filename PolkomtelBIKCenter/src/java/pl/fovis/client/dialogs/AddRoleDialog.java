/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.bikwidgets.AttrLongText;
import pl.fovis.client.bikwidgets.AttrWigetBase;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author Eugeniusz
 */
public class AddRoleDialog extends BaseActionOrCancelDialog {

    protected IParametrizedContinuation <List<String>> saveCont;
    protected String roleName;
    protected String caption;
    protected TextBox tbTytul;
    protected AttrWigetBase attributeWidget;

    @Override
    protected String getDialogCaption() {
        return caption;//I18n.dodajGrupe.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.save.get();
    }

    public void buildAndShowDialog(String groupName, String caption, IParametrizedContinuation  <List<String>> saveCont) {
        this.saveCont = saveCont;
        this.roleName = groupName;
        this.caption = caption;
        super.buildAndShowDialog();
    }

    @Override
    protected void buildMainWidgets() {
        Label lblTitle;
        Label description;
        FlexTable ft = new FlexTable();
        ft.setCellSpacing(4);

        ft.setWidget(0, 0, lblTitle = new Label(I18n.nazwaRoli.get() /* I18N:  */ + ":"));
        ft.setWidget(0, 1, tbTytul = new TextBox());
        ft.setWidget(1, 0, description = new Label(I18n.opis.get()));

        attributeWidget = new AttrLongText("");
        ft.setWidget(1, 1, attributeWidget.getWidget());

        tbTytul.setValue(roleName);
        lblTitle.setStyleName("addNoteLbl");
        description.setStyleName("addNoteLbl");
        tbTytul.setStyleName("addNote");
        tbTytul.setWidth("500px");

        main.add(ft);
    }

    @Override
    protected void doAction() {
        roleName = tbTytul.getText();
        if (BaseUtils.isStrEmptyOrWhiteSpace(roleName)||BaseUtils.isStrEmptyOrWhiteSpace(attributeWidget.getValue())) {
            Window.alert(I18n.wprowadzNazweRoliOrazOpis.get());
        } else {
            List ret = new ArrayList();
            ret.add(roleName);
            ret.add(attributeWidget.getValue());
            saveCont.doIt(ret);
        }
    }

}
