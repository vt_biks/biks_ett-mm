﻿exec sp_check_version '1.0.56';

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_sapbo_extradata]
GO

create procedure sp_insert_into_bik_node_sapbo_extradata
as
begin
insert into bik_sapbo_extradata  
    select bik.id as node_id, 
    nullif(ltrim(rtrim(p.SI_AUTHOR)), '') as author, 
    nullif(ltrim(rtrim(p.SI_OWNER)), '') as owner,
    null as subject, 
    null as comments, 
    nullif(ltrim(rtrim(p.SI_KEYWORD)), '') as keyword,
    au.SI_NAME as universe_name, 
    amdc.si_name as connection_name, 
    uni.id as universe_node_id, conn.id as connection_node_id    
    from aaa_global_props p left join INFO_WEBI iw on p.si_id = iw.si_id
    left join APP_UNIVERSE au on iw.SI_UNIVERSE__1 = au.si_id
    left join APP_METADATA__DATACONNECTION amdc on au.SI_DATACONNECTION__1 = amdc.si_id
    inner join bik_node bik on convert(varchar,p.si_id) = bik.obj_id
    left join bik_node uni on convert(varchar,au.si_id) = uni.obj_id and uni.tree_id=(select id from bik_tree where name='Światy obiektów')
    left join bik_node conn on convert(varchar, amdc.si_id) = conn.obj_id and conn.tree_id=(select id from bik_tree where name='Połączenia')
    left join bik_sapbo_extradata spe on bik.id =spe.node_id
    where spe.node_id is null 
    order by bik.id 
end
go

  
exec sp_update_version '1.0.56', '1.0.57';
go