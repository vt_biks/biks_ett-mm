/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class DQMReportBean implements Serializable {

    public int id;
    public int nodeId;
    public Date dateFrom;
    public Date dateTo;

}
