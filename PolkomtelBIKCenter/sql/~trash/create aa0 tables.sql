create table aa0_node_kind (
  id int not null identity primary key,
  code varchar(255) not null unique,
  caption varchar(255) not null
);

create table aa0_node (
  id int not null identity primary key,
  node_kind_id int not null references aa0_node_kind (id),
  name varchar(4000) not null
);

create table aa0_node_connection (
  id int not null identity primary key,
  node_id int not null references aa0_node (id),
  parent_node_id int not null references aa0_node (id),
  unique (node_id, parent_node_id)
);

create table aa0_node_attr_def (
  id int not null identity primary key,
  name varchar(255) not null unique,
  node_kind_id int not null references aa0_node_kind,
  
);

create table aa0_node_attr_val (
  id int not null identity primary key,
  attr_def_id int not null,
  node_kind_id int not null references aa0_node_kind,
  
);
