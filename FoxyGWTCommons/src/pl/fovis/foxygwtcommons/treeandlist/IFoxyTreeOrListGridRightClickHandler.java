/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.treeandlist;

import java.util.Map;

/**
 *
 * @author młydkowski
 */
public interface IFoxyTreeOrListGridRightClickHandler<T> {

    public void doIt(Map<String, Object> dataRow, int clientX, int clientY, boolean isRowSelected);
}
