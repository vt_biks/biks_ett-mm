﻿exec sp_check_version '1.6.z6.6';
go


if object_id (N'dbo.fn_get_branch_path', N'FN') is not null
    drop function dbo.fn_get_branch_path;
go

create function dbo.fn_get_branch_path (@nodeId int)
returns varchar(max)
as
begin
    --declare @nodeId int = 2233418;
	declare @branchIds varchar(max) = (select branch_ids from bik_node where id = @nodeId);
	declare @treeCaption varchar(max) = (select bt.name from bik_tree bt inner join bik_node bn on bn.tree_id = bt.id where bn.id = @nodeId)

	declare @tmp varchar(max) = '';
	select @tmp = case when @tmp = '' then @tmp + bn.name else @tmp + ' » ' + bn.name end 
	from dbo.fn_split_by_sep(@branchIds, '|', 0) bra
	inner join bik_node bn on bra.str = bn.id
	order by bra.idx;

	set @tmp = @treeCaption + ' » ' + @tmp;
	
    return(@tmp);
end;
go


exec sp_update_version '1.6.z6.6', '1.6.z6.7';
go