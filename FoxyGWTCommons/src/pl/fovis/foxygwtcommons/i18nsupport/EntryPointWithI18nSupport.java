/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.i18nsupport;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.InitFactories4GWT;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pmielanczuk
 */
public abstract class EntryPointWithI18nSupport implements EntryPoint {

    static {
        InitFactories4GWT.initFactories();
    }

    protected abstract void onModuleLoadProper();

    protected void onModuleLoadInit() {
        // no-op
    }

    protected void onSetLangFailure(Throwable cause) {
//        StandardAsyncCallback.showOnFailureError("failure in setupCurrentLocaleOnModuleLoad", cause);
        // tf: lepsze logowanie - ze szczegółami
        FoxyClientSingletons.showError("failure in setupCurrentLocaleOnModuleLoad, message: " + cause.getMessage());
    }

    @Override
    public void onModuleLoad() {
        GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {

            @Override
            public void onUncaughtException(Throwable e) {
                ClientDiagMsgs.showException("UncaughtException", e);
//                if (ClientDiagMsgs.isShowDiagEnabled(FoxyCommonConsts.DIAG_MSG_KIND$GLOBAL_UNCAUGHT_EXCEPTIONS)) {
//                    ClientDiagMsgs.showDiagMsg("globalUncaughtExceptions", "exception:\n" + StackTraceUtil.getCustomStackTrace(e));
//                }
            }
        });

        Scheduler.get().scheduleDeferred(new ScheduledCommand() {
            @Override
            public void execute() {
                onModuleLoadInit();
                callServiceToSetLang();
            }
        });
    }

    protected String getOptForcedLang() {
        return null;
    }

    protected void callServiceToSetLang() {
        GWTCurrentLocaleNameProvider.setupCurrentLocaleOnModuleLoad(null,
                new IContinuation() {
                    public void doIt() {
                        onModuleLoadProper();
                    }
                },
                new IParametrizedContinuation<Throwable>() {
                    public void doIt(Throwable param) {
                        onSetLangFailure(param);
                    }
                }, getOptForcedLang());
    }
}
