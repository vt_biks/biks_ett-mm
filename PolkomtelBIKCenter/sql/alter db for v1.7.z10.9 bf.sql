exec sp_check_version '1.7.z10.9';
go 


/* ***********
	1.Prze��cznik w app_props czy stare role maj� by� widoczne
***********/
	merge into bik_app_prop ap 
	using (values ('disableBuiltInRoles', '0')) as v(name, val) on ap.name = v.name
	when matched then update set ap.val = v.val
	when not matched by target then insert (name, val) values (v.name, v.val);
	go
	
/* w��cz dla wszystkich poza Polkomtelem */	
	if ((select val from bik_app_prop where name = 'biks_id') = '' or (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA')  begin
	  
	  update bik_app_prop set val='1' where name='disableBuiltInRoles';


		insert into bik_node_action (code)
		select x.code
		from
		  (values ('#admin:dqm:logs')
		  ) x(code) left join bik_node_action na on x.code = na.code
		where na.code is null;

				insert into bik_node_action (code)
		select x.code
		from
		  (values ('#admin:dqm:schedule')
		  ) x(code) left join bik_node_action na on x.code = na.code
		where na.code is null;

/* ************
	3.Nowe role 
*************/
--- RegularUser 

		merge into bik_custom_right_role ap 
		using (select 'RegularUser',coalesce(max(visual_order),0)+1,'',1 from bik_custom_right_role) as v(code,visual_order,tree_selector,selector_mode) on ap.code = v.code
		when not matched by target then insert (code,visual_order,tree_selector,selector_mode) values (v.code,v.visual_order,v.tree_selector,v.selector_mode);
		
		
		if not exists (select * from bik_translation where code='RegularUser' and kind='crr')
		begin
			insert into bik_translation (code, txt, lang,kind) values('RegularUser','Zwyk�y u�ytkownik','pl','crr');
			insert into bik_translation (code, txt, lang,kind) values('RegularUser','User','en','crr');
		end;
		
--- Administrator
		merge into bik_custom_right_role ap 
		using (select 'Administrator',coalesce(max(visual_order),0)+1,'',0 from bik_custom_right_role) as v(code,visual_order,tree_selector,selector_mode) on ap.code = v.code
		when not matched by target then insert (code,visual_order,tree_selector,selector_mode) values (v.code,v.visual_order,v.tree_selector,v.selector_mode);
		

		if not exists (select * from bik_translation where code='Administrator' and kind='crr')
		begin
			insert into bik_translation (code, txt, lang,kind) values('Administrator','Administrator','pl','crr');
			insert into bik_translation (code, txt, lang,kind) values('Administrator','Admin','en','crr');
		end		
		
--- Admin Aplikacji
		merge into bik_custom_right_role ap 
		using (select 'AppAdmin',coalesce(max(visual_order),0)+1,'',0 from bik_custom_right_role) as v(code,visual_order,tree_selector,selector_mode) on ap.code = v.code
		when not matched by target then insert (code,visual_order,tree_selector,selector_mode) values (v.code,v.visual_order,v.tree_selector,v.selector_mode);
		
		
		if not exists (select * from bik_translation where code='AppAdmin' and kind='crr')
		begin
			insert into bik_translation (code, txt, lang,kind) values('AppAdmin','Administrator aplikacji','pl','crr');
			insert into bik_translation (code, txt, lang,kind) values('AppAdmin','Content Administrator','en','crr');
		end;

--- Ekspert

		merge into bik_custom_right_role ap 
		using (select 'Expert',coalesce(max(visual_order),0)+1,'',0 from bik_custom_right_role) as v(code,visual_order,tree_selector,selector_mode) on ap.code = v.code
		when not matched by target then insert (code,visual_order,tree_selector,selector_mode) values (v.code,v.visual_order,v.tree_selector,v.selector_mode);

		if not exists (select * from bik_translation where code='Expert' and kind='crr')
		begin
			insert into bik_translation (code, txt, lang,kind) values('Expert','Ekspert','pl','crr');
			insert into bik_translation (code, txt, lang,kind) values('Expert','Expert','en','crr');
		end;
--- Autor		
		merge into bik_custom_right_role ap 
		using (select 'Author',coalesce(max(visual_order),0)+1,'',1 from bik_custom_right_role) as v(code,visual_order,tree_selector,selector_mode) on ap.code = v.code
		when not matched by target then insert (code,visual_order,tree_selector,selector_mode) values (v.code,v.visual_order,v.tree_selector,v.selector_mode);
		
		
			if not exists (select * from bik_translation where code='Author' and kind='crr')
		begin
			insert into bik_translation (code, txt, lang,kind) values('Author','Autor','pl','crr');
			insert into bik_translation (code, txt, lang,kind) values('Author','Author','en','crr');
		end;
		
	update bik_custom_right_role set visual_order= 1 where code='Administrator'
	update bik_custom_right_role set visual_order= 2 where code='AppAdmin'
	update bik_custom_right_role set visual_order= 3 where code='Expert'
	update bik_custom_right_role set visual_order= 4 where code='Author'
	update bik_custom_right_role set visual_order= 5 where code='DqmUser'
	update bik_custom_right_role set visual_order= 6 where code='DqmEditor'
	update bik_custom_right_role set visual_order= 7 where code='DqmAdmin'	
	end;
	go
---------------------------------------
/* *****
	4.AKTUALIZACJA procedury uzupelniajacej customRightRolesTreeSelector i treeSelectory dla r�l Administrator, Admin Aplikacji i Autor
	wyzej
	*****/		

		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors]') AND type in (N'P', N'PC'))
		DROP PROCEDURE [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors]
		GO

		create procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors] as
		begin
		 /* if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' 
			or (select val from bik_app_prop where name = 'biks_id') = 'B424EC5D-2D08-45AE-874D-A4E95FA61880' 
			begin
			exec sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL
			end else 
			*/
			if ((select val from bik_app_prop where name = 'biks_id') = '' 
			or (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA')  
			begin
	 
			/*PB */
			  exec sp_recalculate_Custom_Right_Role_Tree_Selectors_PB
			/*inne*/
			  declare @regUserCrrId int = (select id from bik_custom_right_role where code = 'RegularUser');
		  
			  if @regUserCrrId is null return;
			  
			  declare @tree_selector_codes_all varchar(max) = '';
			  declare @tree_selector_codes_ru varchar(max) = '';
			  
			  select 
				@tree_selector_codes_all = case when @tree_selector_codes_all = '' then '' else @tree_selector_codes_all + ',' end + code,
				@tree_selector_codes_ru =  case when @tree_selector_codes_ru = '' then '' else @tree_selector_codes_ru + ',' end + code  
			  from bik_tree where code not in ( 'TreeOfTrees') and is_hidden = 0;
			  
			  update bik_app_prop set val = @tree_selector_codes_all+','+ 'TreeOfTrees' where name = 'customRightRolesTreeSelector';
			  
			  update bik_custom_right_role set tree_selector = @tree_selector_codes_all+','+ 'TreeOfTrees' where code in('AppAdmin','Administrator','Author','Expert');
			  
			  update bik_custom_right_role set tree_selector = @tree_selector_codes_ru where id = @regUserCrrId;
			end
			else
			begin
			exec sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL
			end
			
			
		end;
		/*end; */
		go		
		/* tymczasowa procedura */
		if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_upgrade_role]') and type in (N'P', N'PC'))
			drop procedure [dbo].sp_upgrade_role
		go

		create procedure [dbo].[sp_upgrade_role](@cusromRoleCode  varchar(50),@oldRoleCode  varchar(50))
		as
		begin
			declare @cRoleId int;
			declare @oRoleId int;
			select @cRoleId=id from bik_custom_right_role where code=@cusromRoleCode;
			select @oRoleId=id from bik_right_role where code=@oldRoleCode;
			--delete from bik_custom_right_role_user_entry where role_id=@cRoleId
			insert into bik_custom_right_role_user_entry(user_id,role_id,tree_id)
			select user_id,@cRoleId,null  from bik_user_right where right_id=@oRoleId
			and not exists (select * from bik_custom_right_role_user_entry where  role_id=@cRoleId) ;
		end;
		go
--------------------------------------------------------------------------------------------
	
	if ((select val from bik_app_prop where name = 'biks_id') = '' or (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA')  begin
/* *****
	5. Wykonanie procedury 
*****/	
		exec sp_recalculate_Custom_Right_Role_Tree_Selectors

/* *****
	6. Aktualizacja tree_selector dla Ekspert�w
*****/
		update bik_custom_right_role set tree_selector = (select val from bik_app_prop where name = 'customRightRolesTreeSelector')
		where code in('Expert');

/* *****
	7. Podpi�cie akcji do r�l
*****/	

		declare @roleId int;
--- Administrator
		select @roleId=id from  bik_custom_right_role where code='Administrator';
		insert into bik_node_action_in_custom_right_role(action_id,role_id)
		select id,@roleId from bik_node_action  bna where not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=bna.id  and role_id=@roleId) )
		
		if (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA'
		begin
			delete from bik_node_action_in_custom_right_role where action_id in (select id from bik_node_action where code like '#admin:biadmin:%')
		end 
		
--- Admin Aplikacji
		select @roleId=id from  bik_custom_right_role where code='AppAdmin';
		insert into bik_node_action_in_custom_right_role(action_id,role_id)
		select id,@roleId from bik_node_action bna
		where code not in('#admin:load:logs', '#admin:load:runManual','#admin:load:schedule', '#admin:load:cfg','#admin:dict:statistics','#admin:dict:userstatistics','admin:dqm:logs'
		,'#admin:lisa:metadata','#admin:lisa:connection','#admin:lisa:logs','#OtherActionsSysAdmin','#admin:dict:attrUsage')
		and code not like '%admin:biadmin%'
		and not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=bna.id  and role_id=@roleId) );		
--- Ekspert
	
		select @roleId=id from  bik_custom_right_role where code='Expert';
		insert into bik_node_action_in_custom_right_role (action_id,role_id)
		select id,@roleId from bik_node_action bna where code not like '#%' and not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=bna.id  and role_id=@roleId) )
--- Autor
		select @roleId=id from  bik_custom_right_role where code='Author';
		insert into bik_node_action_in_custom_right_role (action_id,role_id)
		select id,@roleId from bik_node_action bna where code not like '#%' and not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=bna.id  and role_id=@roleId) )

----------------------
/* *****
	8. Przepinanie starych r�l na nowe: 
	(Nie usuwam starych!)
*****/	
--- Administrator
		exec sp_upgrade_role 'Administrator','Administrator';
--- Admin Aplikacji
		exec sp_upgrade_role 'AppAdmin','AppAdmin';

--- Ekspert 
			exec sp_upgrade_role 'Expert','Expert';

--- Autor
		declare @customRoleId int;
		select @customRoleId=id from bik_custom_right_role where code='Author';
		declare @odlRoleId int;
		select @odlRoleId=id from bik_right_role where code='Author';
		delete from bik_custom_right_role_user_entry where role_id=@customRoleId
		insert into bik_custom_right_role_user_entry(user_id,role_id,tree_id)
		select user_id,@customRoleId,tree_id  from bik_user_right where right_id=@odlRoleId 
	end
	
	/*usuni�cie tymczasowe procedury */
	
	if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_upgrade_role]') and type in (N'P', N'PC'))
			drop procedure [dbo].sp_upgrade_role

	--exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
	go	
	
	if ((select val from bik_app_prop where name = 'biks_id') = '')  begin
	/* dodanie wszystkich drzew do szablonu zwyklego uzytkownika */
	insert into bik_custom_right_role_rut_entry (tree_id,node_id,is_deleted)
	select bt.id,null,0 from bik_tree bt where is_hidden=0 and not exists(select * from bik_custom_right_role_rut_entry where bt.id=tree_id)
	exec sp_recalculate_Custom_Right_Role_Ruts;
	end
/* dodanie brakuj�cych akcji */
	if ((select val from bik_app_prop where name = 'biks_id') = '' or (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA')  begin	
		insert into bik_node_action (code)
		select x.code
		from
		  (values ('EditOwnComment')
		  ) x(code) left join bik_node_action na on x.code = na.code
		where na.code is null;
		
		
		declare @actionId int;
		select @actionId=id from  bik_node_action where code='EditOwnComment';
		

		insert into bik_node_action_in_custom_right_role(action_id,role_id)
		select @actionId,id from  bik_custom_right_role bcr
		where not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=@actionId  and role_id=bcr.id) );

		declare @roleId int;
		select @roleId=id from  bik_custom_right_role where code='RegularUser';
		insert into bik_node_action_in_custom_right_role(action_id,role_id)
		select id,@roleId from bik_node_action  bna where not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=bna.id  and role_id=@roleId) ) and code in('AddComment','VoteForUsefulness','ShowInTree','DownloadDocument','EditOwnComment');
	
		
		exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
	end
go

exec sp_update_version '1.7.z10.9', '1.7.z10.10';
go