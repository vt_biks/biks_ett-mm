/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import java.util.LinkedHashMap;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.HelpBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import simplelib.i18nsupport.I18nMessage;

/**
 *
 * @author mmastalerz
 */
public class HelpDialog extends ViewHTMLInFullScreenDialog {

    private HelpBean hbRecord;

    @Override
    protected String getActionButtonCaption() {
        if (BIKClientSingletons.isAppAdminLoggedIn(BIKConstants.ACTION_HELP_EDITOR)) {
            return I18n.edytuj.get() /* I18N:  */;
        } else {
            return null;
        }
    }

    public void buildAndShowDialog(HelpBean hb, String helpKey, String caption) {
        if (hb != null) {
            this.hbRecord = hb;
        } else {
            this.hbRecord = new HelpBean();
            this.hbRecord.helpKey = helpKey;
        }
        this.hbRecord.caption = BaseUtils.coalesce(caption, this.hbRecord.caption, "");
        super.buildAndShowDialog(I18n.pomoc.get() /* I18N:  */ + ": " + hbRecord.caption, null,
                (hbRecord.textHelp != null ? hbRecord.textHelp : I18n.brakOpisu.get() /* I18N:  */ + ". "),
                new LinkedHashMap<Integer, Pair<String, String>>());
    }

    @Override
    protected void doAction() {
        new EditHelpDescriptionDialog().buildAndShowDialog(hbRecord, new IParametrizedContinuation<HelpBean>() {
            public void doIt(HelpBean param) {
                param.lang = I18nMessage.getCurrentLocaleName();
                BIKClientSingletons.getService().addOrUpdateBikHelp(param, new StandardAsyncCallback<Void>() {
                    public void onSuccess(Void result) {
                        BIKClientSingletons.showInfo(I18n.zaktualizowanoPomoc.get() /* I18N:  */);
                    }
                });
            }
        });
    }
}
