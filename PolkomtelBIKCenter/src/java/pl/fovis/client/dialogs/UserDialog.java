/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.TextBox;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.UserBean;
import pl.fovis.common.UserExtBean;
import pl.fovis.common.UserExtradataBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author mgraczkowski
 */
public class UserDialog extends BaseActionOrCancelDialog {

    protected Label status;
    protected IParametrizedContinuation<Pair<Integer, List<String>>/*Integer*/> saveCont;
    protected TreeNodeBean node;
    protected UserBean user;
    protected TextBox title;
    protected TextBox email;
    protected TextBox phone;
    protected SuggestBox loginAD;
    protected Grid grid;
//    protected RadioButton loadFromAd;
//    protected RadioButton otherSource;

    @Override
    protected String getDialogCaption() {
        return I18n.uzytkownik.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        status = new Label();
        status.setStyleName("status" /* i18n: no:css-class */);
        main.add(status);
        main.setWidth("340px");
        grid = new Grid(5, 2);
        grid.setCellSpacing(4);
        Label nameLbl = new Label(I18n.nazwa.get() /* I18N:  */ + "(*):");
        nameLbl.addStyleName("userDialogLbl");
        Label emailLbl = new Label("Email" /* I18N: no */ + "(*):");
        emailLbl.addStyleName("userDialogLbl");
        Label phoneLbl = new Label(I18n.telefon.get() /* I18N:  */ + ":");
        phoneLbl.addStyleName("userDialogLbl");
        Label addsLbl = new Label("------------");
        addsLbl.addStyleName("userDialogLbl");
        Label adLbl = new Label(I18n.loginZAD.get() /* I18N:  */ + ":");
        adLbl.addStyleName("userDialogLbl");
        grid.setWidget(0, 0, nameLbl);
        grid.setWidget(0, 1, title = new TextBox());
        title.addStyleName("userDialogInput");
        grid.setWidget(1, 0, emailLbl);
        grid.setWidget(1, 1, email = new TextBox());
        email.addStyleName("userDialogInput");
        grid.setWidget(2, 0, phoneLbl);
        grid.setWidget(2, 1, phone = new TextBox());
        phone.addStyleName("userDialogInput");
        grid.setWidget(3, 0, addsLbl);
        grid.setWidget(4, 0, adLbl);

        HorizontalPanel hp = new HorizontalPanel();
        hp.add(loginAD = createSuggestBox());
        hp.add(updateInfoFromADBtn());
        grid.setWidget(4, 1, hp);
        loginAD.addStyleName("userDialogInput");
        title.setWidth("258px");
        email.setWidth("258px");
        phone.setWidth("258px");
        loginAD.setWidth("258px");
        if (user != null) {
            title.setValue(BaseUtils.safeToString(user.name));
            email.setValue(BaseUtils.safeToString(user.email));
            phone.setValue(BaseUtils.safeToString(user.phoneNum));
            loginAD.setValue(BaseUtils.safeToString(user.loginNameForAd));
        }
        main.add(grid);
//        Grid grid = new Grid(3, 2);
//
//        Label nameLbl = new Label(I18n.nazwa.get() + "*");
//        nameLbl.addStyleName("userDialogLbl");
//        grid.setWidget(0, 0, nameLbl);
//        title = new TextBox();
//        title.addStyleName("userDialogInput");
//        grid.setWidget(0, 1, title);
//
//        Label emailLbl = new Label(I18n.email.get() + "*");
//        emailLbl.addStyleName("userDialogLbl");
//        grid.setWidget(1, 0, emailLbl);
//        email = new TextBox();
//        email.addStyleName("userDialogInput");
//        grid.setWidget(1, 1, email);
//
//        Label phoneLbl = new Label(I18n.telefon.get());
//        phoneLbl.addStyleName("userDialogLbl");
//        grid.setWidget(2, 0, phoneLbl);
//        phone = new TextBox();
//        phone.addStyleName("userDialogInput");
//        grid.setWidget(2, 1, phone);
//
//        main.add(grid);
//
//        HorizontalPanel radioBtnWrapper1 = new HorizontalPanel();
//        radioBtnWrapper1.setSpacing(6);
//        loadFromAd = new RadioButton("GROUP", I18n.loginZAD.get());
//        loadFromAd.addClickHandler(new ClickHandler() {
//
//            @Override
//            public void onClick(ClickEvent event) {
//                onChangeRadioBtnState();
//            }
//        });
//        PushButton btn = new PushButton(I18n.wybierz.get());
//        NewLookUtils.makeCustomPushButton(btn);
//        radioBtnWrapper1.add(loadFromAd);
//        radioBtnWrapper1.add(btn);
//
//        main.add(radioBtnWrapper1);
//
//        otherSource = new RadioButton("GROUP", I18n.inne.get());
//        otherSource.addClickHandler(new ClickHandler() {
//
//            @Override
//            public void onClick(ClickEvent event) {
//                onChangeRadioBtnState();
//            }
//
//        });
//        otherSource.getElement().getStyle().setMarginLeft(6, Style.Unit.PX);
//        main.add(otherSource);
    }

//    private void onChangeRadioBtnState() {
//        boolean enabled = otherSource.getValue();
//        title.setEnabled(enabled);
//        email.setEnabled(enabled);
//        phone.setEnabled(enabled);
//    }
    protected boolean willExecuteServiceMethod;
    protected int qqCnt;

    @SuppressWarnings("unchecked")
    @Override
    protected boolean doActionBeforeClose() {

        if (willExecuteServiceMethod) {
            //Window.alert("doActionBeforeClose: willExecuteServiceMethod! repeated call detected. exiting");
            //willExecuteServiceMethod = false;
            return false;
        }
        if (BaseUtils.isStrEmptyOrWhiteSpace(title.getText()) || BaseUtils.isStrEmptyOrWhiteSpace(email.getText())) {
//        if (title.getText().equals("") || email.getText().equals("")) {
            status.setText(I18n.polaNazwaIEmailSaWymagane.get() /* I18N:  */);//"Pola login, nazwa, email są wymagane");
            return false;
        } else {
            willExecuteServiceMethod = true;
            final UserExtBean usr = new UserExtBean();
            if (user != null) {
                usr.id = user.id;
            }
            usr.name = title.getValue();
            usr.email = email.getValue();
            usr.phoneNum = phone.getValue();
            usr.loginNameForAd = (!loginAD.getValue().trim().equals("") ? loginAD.getValue() : null);
            if (user == null) {
                BIKClientSingletons.getService().addBikTreeNodeUser(node, usr, new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on" /* I18N: no */ + " addBikTreeNodeUser") {

                            public void onSuccess(Pair<Integer, List<String>> result) {
                                //willExecuteServiceMethod = false;
                                if (result != null) {
                                    BIKClientSingletons.showInfo(I18n.dodanoUzytkownika.get() /* I18N:  */);
                                    hideDialog();
                                    saveCont.doIt(result);
                                } else {
                                    willExecuteServiceMethod = false;
                                    status.setText(I18n.uzytkownikONazwie.get() /* I18N:  */ + " " + usr.name + " " + I18n.juzIstnieje.get() /* I18N:  */ + ".");
                                }
                            }
                        });
            } else {
                BIKClientSingletons.getService().updateBikUser(user.id, user.loginName, usr.loginName, usr.name, usr.password, usr.email, usr.phoneNum, usr.loginNameForAd,
                        new StandardAsyncCallback<List<String>>("Failure on" /* I18N: no */ + " updateBikUser") {

                            public void onSuccess(List<String> result) {
                                //willExecuteServiceMethod = false;
                                if (result != null) {
                                    BIKClientSingletons.showInfo(I18n.zmodyfikowanoUzytkownika.get() /* I18N:  */);
                                    hideDialog();
                                    saveCont.doIt(new Pair<Integer, List<String>>(node.id, result));
                                } else {
                                    willExecuteServiceMethod = false;
                                    status.setText(I18n.uzytkownikOLoginie.get() /* I18N:  */ + " " + usr.loginName + " " + I18n.juzIstnieje.get() /* I18N:  */ + ".");
                                }
                            }
                        });
            }
            return false;
        }
    }

    @Override
    protected void doAction() {
        throw new IllegalStateException(I18n.toSieNiePowinnoWywolac.get() /* I18N:  */);
    }

    public void buildAndShowDialog(TreeNodeBean node, UserBean user, IParametrizedContinuation<Pair<Integer, List<String>>/*Integer*/> saveCont) {
        this.node = node;
        this.saveCont = saveCont;
        this.user = user;
        super.buildAndShowDialog();
    }

    private SuggestBox createSuggestBox() {
        SuggestOracle oracle = new SuggestOracle() {

            @Override
            public void requestSuggestions(final SuggestOracle.Request request, final SuggestOracle.Callback callback) {
                String query = request.getQuery();
                final List<SuggestOracle.Suggestion> suggestions = new ArrayList<SuggestOracle.Suggestion>();
                BIKClientSingletons.getService().getADLogin(query, new StandardAsyncCallback<List<UserExtradataBean>>() {

                    @Override
                    public void onSuccess(List<UserExtradataBean> result) {
                        for (final UserExtradataBean adUser : result) {
                            suggestions.add(new SuggestOracle.Suggestion() {

                                @Override
                                public String getDisplayString() {
                                    return adUser.loginAd + " - " + adUser.displayName;
                                }

                                @Override
                                public String getReplacementString() {
                                    return adUser.loginAd;
                                }
                            });
                        }
                        SuggestOracle.Response response = new SuggestOracle.Response(suggestions);
                        callback.onSuggestionsReady(request, response);
                    }
                });
            }
        };
        SuggestBox box = new SuggestBox(oracle);

        return box;
    }

    private PushButton updateInfoFromADBtn() {
        PushButton btn = new PushButton(I18n.uaktualnij.get());
        NewLookUtils.makeCustomPushButton(btn);
        btn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().getInfoFromADByLogin(loginAD.getText(), new StandardAsyncCallback<UserExtradataBean>() {

                    @Override
                    public void onSuccess(UserExtradataBean result) {
                        if (result == null) {
                            BIKClientSingletons.showWarning(I18n.loginADNieIstnieje.get());
                        } else {
                            title.setText(result.displayName);
                            email.setText(result.mail);
                        }
                    }
                });
            }
        });
        btn.getElement().getStyle().setMarginLeft(6, Style.Unit.PX);
        return btn;
    }
}
