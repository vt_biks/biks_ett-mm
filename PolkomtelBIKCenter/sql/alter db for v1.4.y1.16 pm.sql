exec sp_check_version '1.4.y1.16';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
--
-- 1. usuwamy t�umaczenia atrybut�w dodatkowych, zostaj� tylko wbudowane
--
-- 2. dodane t�umaczenie nazw kategorii atrybut�w wbudowanych
--
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

-- usuwamy t�umaczenia dla atrybut�w innych ni� wbudowane - s� zb�dne
--   a dodatkowo przeszkadzaj�
delete
--select * 
from bik_translation
where code not in (select name from bik_attr_def where is_built_in = 1)
and kind = 'adef'
go

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

-- dodajemy t�umaczenia dla kategorii atrybut�w wbudowanych
--select * from dbo.bik_attr_category where is_built_in = 1

insert into bik_translation (code, lang, kind, txt) values ('SAP BO', 'en', 'adcat', 'SAP BO')
insert into bik_translation (code, lang, kind, txt) values ('DQC', 'en', 'adcat', 'DQC')
insert into bik_translation (code, lang, kind, txt) values ('U�ytkownicy', 'en', 'adcat', 'Users')
insert into bik_translation (code, lang, kind, txt) values ('SAP BW', 'en', 'adcat', 'SAP BW')
go

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

exec sp_update_version '1.4.y1.16', '1.4.y1.17';
go
