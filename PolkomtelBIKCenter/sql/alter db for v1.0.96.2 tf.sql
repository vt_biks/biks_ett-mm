﻿exec sp_check_version '1.0.96.2';
go


insert into bik_admin(param,value) values ('dqc.server','ZF-SAMSUNG')
insert into bik_admin(param,value) values ('dqc.instance','')
insert into bik_admin(param,value) values ('dqc.database','DQC')
insert into bik_admin(param,value) values ('dqc.user','sa')
insert into bik_admin(param,value) values ('dqc.password','sa123')

insert into bik_data_source_def(description) values ('DQC')
go
------------------------------------------
------------------------------------------


/* testy */
CREATE TABLE bik_dqc_test (
  id_test INTEGER NOT NULL,
  id_template_test INTEGER NULL,
  id_test_stage INTEGER NOT NULL,
  id_importance_level INTEGER NOT NULL,
  id_person INTEGER NOT NULL,
  name VARCHAR(32) NOT NULL,
  long_name VARCHAR(128) NOT NULL,
  description TEXT NOT NULL,
  sampling_method TEXT NULL,
  verified_attributes TEXT NULL,
  expected_result TEXT NULL,
  logging_details TEXT NULL,
  benchmark_definition TEXT NULL,
  error_threshold DECIMAL(18,8) NULL,
  results_object VARCHAR(128) NULL,
  additional_information TEXT NULL,
  create_date DATETIME NOT NULL,
  modify_date DATETIME NOT NULL,
  suspend_date DATETIME NULL,
  __deleted BIT NOT NULL DEFAULT 0,
  __obj_id varchar(max) not null,
  PRIMARY KEY(id_test)
);
go


/* powiązania testów i grup */
CREATE TABLE bik_dqc_group_test (
  id_group INTEGER NOT NULL,
  id_test INTEGER NOT NULL,
  __deleted BIT NOT NULL DEFAULT 0,
  PRIMARY KEY(id_group, id_test)
);
go


/* grupy testów */
CREATE TABLE bik_dqc_group (
  id_group INTEGER NOT NULL,
  name VARCHAR(32) NOT NULL,
  description VARCHAR(128) NOT NULL,
  __deleted BIT NOT NULL DEFAULT 0,
  __obj_id varchar(max) not null,
  PRIMARY KEY(id_group)
);


/* wykonania testów */
CREATE TABLE bik_dqc_request (
  id_request INTEGER NOT NULL,
  id_alias_sql INTEGER NOT NULL,
  id_alias_mdx INTEGER NOT NULL,
  id_test INTEGER NOT NULL,
  id_person INTEGER NOT NULL,
  id_request_state INTEGER NOT NULL,
  case_count BIGINT NULL,
  error_count BIGINT NULL,
  error_percentage DECIMAL(18,8) NULL,
  error_level_exceeded BIT NULL,
  comments TEXT NULL,
  passed BIT NULL,
  log TEXT NULL,
  start_timestamp DATETIME NOT NULL,
  end_timestamp DATETIME NULL,
  last_trial_timestamp DATETIME NULL,
  last_trial_count INTEGER NULL,
  is_manual BIT NOT NULL,
  finished_steps INTEGER NULL DEFAULT 0,
  number_of_steps INTEGER NULL,
  __deleted BIT NOT NULL DEFAULT 0,
  test_obj_id varchar(max) not null,
  PRIMARY KEY(id_request)
);
go
------------------------------------------
------------------------------------------

insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values('DQCGroup', 'Grupa testów', 'dqcgroup', 'Metadata', 1, 25)

insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values('DQCTest', 'Test', 'dqctest', 'Metadata', 1, 25)

insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values('DQCAllTestsFolder', 'Folder testów', 'dqcalltests', 'Metadata', 1, 25)

insert into bik_tree(name, node_kind_id,code,tree_kind)
values ('DQC', null,'DQC','Metadata')

declare @tree_id int;
select @tree_id = id from bik_tree where code='DQC'

insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id,is_built_in)
values (null, (select id from bik_node_kind where code='DQCAllTestsFolder'), 'Wszystkie testy', null, @tree_id,1)

exec sp_node_init_branch_id @tree_id,null
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_dqc]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_node_dqc
go


create procedure [dbo].[sp_insert_into_bik_node_dqc]
as
begin

declare @dqc_group int;
select @dqc_group=id from bik_node_kind where code='DQCGroup'
declare @dqc_test int;
select @dqc_test=id from bik_node_kind where code='DQCTest'
declare @dqc_alltests int;
select @dqc_alltests=id from bik_node_kind where code='DQCAllTestsFolder'
declare @dqc_tree_id int;
select @dqc_tree_id=id from bik_tree where code='DQC'
declare @alltestsid int;
select @alltestsid = id from bik_node where is_built_in=1 and is_deleted=0 and linked_node_id is null and node_kind_id=@dqc_alltests

-- update grup
update bik_node 
set parent_node_id = null, node_kind_id = @dqc_group, name = dqc.name,
	is_deleted = 0, descr = dqc.description, tree_id = @dqc_tree_id
from bik_dqc_group dqc
where bik_node.obj_id = dqc.__obj_id 
and bik_node.linked_node_id is null
and dqc.__deleted=0

-- dodanie nowych grup
insert into bik_node (parent_node_id, node_kind_id, name, descr, tree_id, obj_id)
	select null,@dqc_group, dqc.name, dqc.description, @dqc_tree_id, dqc.__obj_id
	from bik_dqc_group dqc 
	left join bik_node bn on bn.obj_id=dqc.__obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null 
	and bn.node_kind_id=@dqc_group 
	and bn.tree_id=@dqc_tree_id
	where bn.id is null 
	and dqc.__deleted=0

-- usuniecie niepotrzebnych grup
update bik_node
set is_deleted=1
where node_kind_id=@dqc_group
and obj_id not in (select __obj_id from bik_dqc_group where __deleted=0)
and linked_node_id is null
and is_deleted=0

-- update testów
update bik_node 
set parent_node_id = @alltestsid, node_kind_id = @dqc_test, name = dqc.name,
	is_deleted = 0, descr = dqc.description, tree_id = @dqc_tree_id
from bik_dqc_test dqc
where bik_node.obj_id = dqc.__obj_id 
and bik_node.linked_node_id is null
and dqc.__deleted=0

-- dodanie nowych testów
insert into bik_node (parent_node_id, node_kind_id, name, descr, tree_id, obj_id)
	select @alltestsid,@dqc_test, dqc.name, dqc.description, @dqc_tree_id, dqc.__obj_id
	from bik_dqc_test dqc 
	left join bik_node bn on bn.obj_id=dqc.__obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null 
	and bn.node_kind_id=@dqc_test 
	and bn.tree_id=@dqc_tree_id
	where bn.id is null 
	and dqc.__deleted=0

-- usuniecie niepotrzebnych testów
update bik_node
set is_deleted=1
where node_kind_id=@dqc_test
and obj_id not in (select __obj_id from bik_dqc_test where __deleted=0)
and linked_node_id is null
and is_deleted=0

create table #tmp_test_group (test_node_id int,group_node_id int, test_name varchar(max), obj_id varchar(max));

insert into #tmp_test_group(test_node_id,group_node_id,test_name,obj_id)
	select bn2.id, bn.id, bn2.name, bn2.obj_id from bik_dqc_group_test dgt
	join bik_dqc_group dg on dg.id_group=dgt.id_group
	join bik_dqc_test dt on dt.id_test=dgt.id_test
	join bik_node bn on bn.obj_id=dg.__obj_id
	and bn.is_deleted = 0 and bn.linked_node_id is null
	and bn.node_kind_id = @dqc_group and bn.parent_node_id is null
	join bik_node bn2 on bn2.obj_id=dt.__obj_id
	and bn2.is_deleted = 0 and bn2.linked_node_id is null
	and bn2.node_kind_id = @dqc_test and bn2.parent_node_id=@alltestsid
	where dgt.__deleted = 0 and dt.__deleted = 0 and dg.__deleted = 0

-- update testów podlinkowanych
update bik_node 
set parent_node_id = dqc.group_node_id, node_kind_id = @dqc_test, name = dqc.test_name,
	is_deleted = 0, tree_id = @dqc_tree_id, linked_node_id = dqc.test_node_id
from #tmp_test_group dqc
where bik_node.node_kind_id=@dqc_test
and bik_node.linked_node_id is not null
and bik_node.tree_id = @dqc_tree_id
and bik_node.linked_node_id=dqc.test_node_id
and bik_node.parent_node_id=dqc.group_node_id


-- dodanie nowych testów podlinkowanych
insert into bik_node (parent_node_id, node_kind_id, name, tree_id, obj_id, linked_node_id)
	select dqc.group_node_id,@dqc_test, dqc.test_name, @dqc_tree_id, dqc.obj_id, dqc.test_node_id
	from #tmp_test_group dqc 
	left join bik_node bn on bn.obj_id=dqc.obj_id 
	and bn.parent_node_id=dqc.group_node_id
	and bn.linked_node_id = dqc.test_node_id
	and bn.is_deleted=0
	and bn.linked_node_id is not null 
	and bn.node_kind_id=@dqc_test 
	and bn.tree_id=@dqc_tree_id
	where bn.id is null 

-- usuniecie niepotrzebnych testów podlinkowanych
update bik_node
set is_deleted=1
where node_kind_id=@dqc_test
and obj_id not in (select obj_id from #tmp_test_group)
and linked_node_id is not null
and is_deleted=0

drop table #tmp_test_group;

exec sp_node_init_branch_id @dqc_tree_id,null

end;
go


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
exec sp_update_version '1.0.96.2', '1.0.96.3';
go
