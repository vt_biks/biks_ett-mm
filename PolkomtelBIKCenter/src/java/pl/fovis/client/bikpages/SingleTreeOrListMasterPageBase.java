/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import java.util.Map;
import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeOrListGrid;

/**
 *
 * @author wezyr
 */
public abstract class SingleTreeOrListMasterPageBase<DATA> extends BikPageStandardBase<DATA> {

    protected boolean isFirstSelected;

    @Override
    protected void refreshMainWidgetState() {
        getTreeOrListGrid().refreshWidgetsState();
    }

    protected String getIdFieldName() {
        return getTreeOrListGrid().getIdFieldName();
    }

    @Override
    protected void innerSelectEntityById(Integer optId) {
        String idFieldName = getIdFieldName();

        if (idFieldName == null || optId == null) {
            getTreeOrListGrid().selectFirstRecord();
            isFirstSelected = true;
        } else {
            //Window.alert("idFieldName = " + idFieldName + " optId = " + optId);
            getTreeOrListGrid().selectRecord(optId);
            isFirstSelected = false;
        }

    }

    @Override
    public Integer getSelectedNodeId() {
        String idFieldName = getIdFieldName();

        if (idFieldName == null) {
            return null;
        }

        Map<String, Object> row = getTreeOrListGrid().getSelectedRecord();

        if (row == null) {
            return null;
        }

        Object idVal = row.get(idFieldName);

        return (Integer) idVal;
    }

    protected abstract IFoxyTreeOrListGrid<Integer> getTreeOrListGrid();
}
