/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.common;

import java.util.Map;
import java.util.Set;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public class BIKSAdmUtils {

    public enum NeedToUpgrade {

        AlreadyUpgraded,
        UpgradeInProgress,
        NeedsUpgrade,
        NeedsManualFixing,
        MissingUpgradeScript
    }

    public static NeedToUpgrade calcNeedToUpgrade(InstanceInfoBean info, AppInfoBean appInfoBean,
            Map<String, Set<String>> availableScriptsForDbVers) {
        if (BaseUtils.safeEquals(info.dbVer, appInfoBean.appVer)) {
            return NeedToUpgrade.AlreadyUpgraded;
        }

        DbauProcessBean lpi = info.lastProcessInfo;

        if (lpi != null && lpi.status != 1 && lpi.status != 2) {
            if (lpi.status == 0) {
                return NeedToUpgrade.UpgradeInProgress;
            }

            return NeedToUpgrade.NeedsManualFixing;
        }

        Set<String> upgradeableDbVers = BaseUtils.safeMapGet(availableScriptsForDbVers, appInfoBean.appCode);
        if (!BaseUtils.collectionContainsFix(upgradeableDbVers, info.dbVer)) {
            return NeedToUpgrade.MissingUpgradeScript;
        }

        return NeedToUpgrade.NeedsUpgrade;
    }
}
