/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.client.menu;

import pl.fovis.client.bikpages.IBikPage;

/**
 *
 * @author bfechner
 */
public interface IMenuItemMainWidget extends IMenuItem {
    public IBikPage<Integer> getTab();
}
