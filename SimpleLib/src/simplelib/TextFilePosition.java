/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author wezyr
 */
public class TextFilePosition {

    private String fileName;
    private int pos;
    private int row;
    private int col;

    protected TextFilePosition(String fileName, int pos, int row, int col) {
        this.fileName = fileName;
        this.pos = pos;
        this.row = row;
        this.col = col;
    }

    @Override
    public String toString() {
        return "{fileName=" + (fileName == null ? "<unknown>" : "\"" + fileName + "\"")
                + ", row=" + (row + 1) + ", col=" + (col + 1)
                + /* ", byte pos=" + pos +*/ "}";
    }

    public static TextFilePosition initialPos(String fileName) {
        return new TextFilePosition(fileName, 0, 0, 0);
    }

    public static TextFilePosition moveTo(TextFilePosition prevPos, String txt, int newPos) {
        int newRow = prevPos.row, newCol = prevPos.col;

        int pos = prevPos.pos;

        while (pos < newPos && pos < txt.length()) {
            if (txt.charAt(pos) == '\n') {
                newCol = 0;
                newRow++;
            } else {
                newCol++;
            }
            pos++;
        }

        return new TextFilePosition(prevPos.fileName, newPos, newRow, newCol);
    }

    public int getCol() {
        return col;
    }

    public String getFileName() {
        return fileName;
    }

    public int getPos() {
        return pos;
    }

    public int getRow() {
        return row;
    }

    public Pair<Integer, Integer> getRowAndCol() {
        return new Pair<Integer, Integer>(row, col);
    }
}
