﻿exec sp_check_version '1.2.1.4';
go

declare @table_name sysname = 'bik_user_in_node', @column_name sysname = 'is_deleted'
declare @default_constraint_name sysname
select @default_constraint_name = object_name(cdefault)
from syscolumns
where id = object_id(@table_name)
and name = @column_name
--print @default_constraint_name
if @default_constraint_name is not null
  exec ('alter table ' + @table_name + ' drop constraint ' + @default_constraint_name)
if exists(select 1 from syscolumns sc where id = object_id(@table_name) and name = @column_name)
  exec ('alter table ' + @table_name + ' drop column ' + @column_name)
go

declare @table_name sysname = 'bik_user_in_node', @column_name sysname = 'date_added'
declare @default_constraint_name sysname 
select @default_constraint_name = object_name(cdefault)
from syscolumns
where id = object_id(@table_name)
and name = @column_name
--print @default_constraint_name
if @default_constraint_name is not null
  exec ('alter table ' + @table_name + ' drop constraint ' + @default_constraint_name)
if exists(select 1 from syscolumns sc where id = object_id(@table_name) and name = @column_name)
  exec ('alter table ' + @table_name + ' drop column ' + @column_name)
go

create table bik_object_author (
	node_id int not null primary key references bik_node(id),
	user_id int not null references bik_user(id),
	date_added datetime not null default sysdatetime()
)
go


exec sp_update_version '1.2.1.4', '1.2.1.5';
go
