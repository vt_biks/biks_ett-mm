declare @parentNodeId int = 117617;
declare @loggedUserId int = 101;
declare @branchIds varchar(max) =  (select branch_ids from bik_node where id=@parentNodeId);
declare @parentObjId varchar(max) =  (select obj_id from bik_node where id=@parentNodeId);
declare @nodeKindId int = (select id from bik_node_kind where code='RISK0_KlasaRyzyka');

declare @treeId int = (select tree_id from bik_node where id=@parentNodeId);
declare @name varchar(max) = 'test';

if exists (select 1 from bik_spid_source where spid = @@spid)
	update bik_spid_source set user_id = @loggedUserId, data_source_def_id = null where spid = @@spid;
else
	insert into bik_spid_source(user_id) values (@loggedUserId);
    

insert into bik_node(parent_node_id, node_kind_id, name, obj_id, tree_id, linked_node_id)
values(@parentNodeId, @nodeKindId, @name, @parentObjId+'|'+@name, @treeId, null);
declare @add_id int = scope_identity();
update bik_node set branch_ids = @branchIds + convert(varchar(20), @add_id) + '|', visual_order = dbo.fn_get_visual_order_for_new_node(parent_node_id, tree_id)
where id = @add_id;


select * from bik_node_kind where caption='Klasa ryzyka'

select * from bik_node where parent_node_id=117618