/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.lisa;

import com.google.gwt.user.client.rpc.AsyncCallback;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.entitydetailswidgets.lisa.LisaAuthorizedRequest;
import pl.fovis.client.nodeactions.NodeActionUpdatingBase;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.OneTextBoxDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pku
 */
public class NodeActionAddLisaCategory extends NodeActionUpdatingBase {

    public NodeActionAddLisaCategory() {
    }

    @Override
    public boolean isEnabledByDesign() {
        return isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_LISA_CATEGORIZATION, BIKGWTConstants.NODE_KIND_LISA_CATEGORY) && BIKClientSingletons.isBikUserLoggedIn();
    }

    @Override
    public String getCaption() {
        return I18n.dodajKategorie2.get() /* I18N:  */;
    }

    @Override
    public void execute() {
        OneTextBoxDialog dialog = new OneTextBoxDialog();
        dialog.buildAndShowDialog(I18n.podajNazwe.get() /* I18N:  */, "", new IParametrizedContinuation<String>() {

                    @Override
                    public void doIt(String param) {
                        saveNewCategory(param);
                    }
                });
    }

    private void saveNewCategory(final String categoryName) {
        //TO-DO
        BIKClientSingletons.getLisaService().getLisaInstanceByNodeId(node.id, new StandardAsyncCallback<LisaInstanceInfoBean>() {

            @Override
            public void onSuccess(final LisaInstanceInfoBean result) {
                new LisaAuthorizedRequest<TreeNodeBean>(result.id, result.name) {

                    @Override
                    protected void performServerAction(AsyncCallback<TreeNodeBean> callback) {
                        BIKClientSingletons.getLisaService().saveNewCategory(result.id, categoryName, data.node.id, callback);
                    }

                    @Override
                    public void onActionSuccess(TreeNodeBean result) {
                        BIKClientSingletons.showInfo(I18n.kategoria.get() /* I18N:  */ + " \"" + categoryName + "\" " + I18n.zostalaDodana.get() /* I18N:  */);
//                 refreshRedisplayDataAndSelectRecord(getSelectedNodeId());
                        refreshRedisplayDataAndSelectRecord(result.id);
//                ctx.addTreeGridNode(result);
                    }
                }.go();

            }
        });
    }
}
