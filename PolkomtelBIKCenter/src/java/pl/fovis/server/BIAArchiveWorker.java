/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import pl.bssg.metadatapump.common.SAPBOObjectBean;
import pl.bssg.metadatapump.sapbo.biadmin.IBIAConnector;
import pl.fovis.common.BIArchiveConfigBean;
import pl.fovis.common.ConnectionParametersBIAdminBean;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.IFoxyTransactionalPerformer;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.logging.ILameLogger;

/**
 *
 * @author ctran
 */
public class BIAArchiveWorker implements IFoxyTransactionalPerformer {

    private static final int BACK_UP_LIMIT = Integer.MAX_VALUE;
    private static final int MB = 1024 * 1024;
    public static final String BACKUP_SUCCESS_STATUS = "EXIT_SUCCESS";
    public static final String BACKUP_FILE_EXT = ".lcmbiar";
    public static final String BACKUP_KIND_REPORTS = "Reports";
    public static final String BACKUP_KIND_CONNECTIONS = "Connections";
    public static final String BACKUP_KIND_UNIVERSES = "Universes";
    public static final SimpleDateFormat DATE_DEFAULT_FORMAT = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected INamedSqlsDAO<Object> adhocDao;
    protected IBIAConnector biaProxyConnector;
    protected String dirForUpload;
    protected ConnectionParametersBIAdminBean biServerConfig;
    protected BIArchiveConfigBean archiveConfig;
    protected Map<Integer, SAPBOObjectBean> oldArchivedObjs;
    protected int backupCnt;
    protected Integer total2Backup;
    protected Integer backupLogId;
    protected IArchiveFileHelper localFileHelper;
    protected IArchiveFileHelper smbFileHelper;

    protected boolean isCancelled = false;

    public BIAArchiveWorker(INamedSqlsDAO<Object> adhocDao, IBIAConnector biaProxyConnector, String dirForUpload, ConnectionParametersBIAdminBean biServerConfig, BIArchiveConfigBean archiveConfig) {
        this.adhocDao = adhocDao;
        this.biaProxyConnector = biaProxyConnector;
        this.dirForUpload = dirForUpload;
        this.biServerConfig = biServerConfig;
        this.archiveConfig = archiveConfig;
    }

    public void cancel() {
        isCancelled = true;
    }

    public void doit() {
        adhocDao.execInAutoCommitMode(new IContinuation() {

            @Override
            public void doIt() {
                localFileHelper = new LocalArchiveFileHelper();
                smbFileHelper = (archiveConfig.isRemoteSaving == 1
                        ? new SmbArchiveFileHelper(archiveConfig.domain, archiveConfig.user, archiveConfig.pw)
                        : null);
                backupLogId = createProcessBackupLog(/*archiveConfig.jobCuid*/);
                LameUtils.killDir(new File(dirForUpload + "/LCMBIAR/" /*+ archiveConfig.jobCuid*/));
                if (BaseUtils.tryParseInteger(archiveConfig.limit) != null) {
                    total2Backup = BaseUtils.tryParseInteger(archiveConfig.limit);
                } else {
                    if (!BaseUtils.isStrEmptyOrWhiteSpace(archiveConfig.limit)) {
                        total2Backup = -1;
                    } else {
                        total2Backup = null;
                    }
                }

                oldArchivedObjs = getArchivedObjs(/*archiveConfig.jobCuid*/);
                backupCnt = 0;
                if (!isCancelled) {
                    doBackup(BACKUP_KIND_REPORTS, biaProxyConnector.getBIAReports());
                }
                if (!isCancelled) {
                    doBackup(BACKUP_KIND_UNIVERSES, biaProxyConnector.getBIAUniverses());
                }
                if (!isCancelled) {
                    doBackup(BACKUP_KIND_CONNECTIONS, biaProxyConnector.getBIAConnections());
                }

                if (total2Backup == null || total2Backup == -1 || backupCnt >= total2Backup) {
                    updateBackupLog(backupLogId, new Date(), "Success");
                } else {
                    updateBackupLog(backupLogId, new Date(), "Error");
                }
            }
        });
    }

    private void doBackup(String procesStepName, List<SAPBOObjectBean> objList) {
        Set<SAPBOObjectBean> obj2Backup = new HashSet<SAPBOObjectBean>();
        for (SAPBOObjectBean bean : objList) {
//            if (bean.id == 35454) {
//                System.out.println("oldArchivedObjs.containsKey = " + oldArchivedObjs.containsKey(bean.id));
//            }
//            if (!oldArchivedObjs.containsKey(bean.id) || oldArchivedObjs.get(bean.id).before(bean.modify)) {
            if (!fileWasArchived(procesStepName, bean) || fileWasOld(bean)) {
                obj2Backup.add(bean);
            }
//            }
        }
//        total2Backup += obj2Backup.size();
        updateBackupLog(backupLogId, null, procesStepName + " to archive: " + obj2Backup.size());
        for (SAPBOObjectBean bean : obj2Backup) {
            if (total2Backup != null && backupCnt >= total2Backup) {
                cancel();
            }
            if (!isCancelled) {
                backupOneObject(procesStepName, bean);
            }
        }
        // commit jeśli korzystamy z GITa
        if (archiveConfig.useGit == 1) {
            try {
//                File workingTreePath = new File(archiveConfig.savedFolder);
//                DotGit dotGit = DotGit.getInstance(workingTreePath);
//                dotGit.init();
//                WorkingTree wt = dotGit.getWorkingTree();
//                wt.addAndCommitAll("BIKS BIAdmin commit");
                File repo = new File(archiveConfig.savedFolder);
                Git git = Git.open(repo);
                git.add().addFilepattern(".").call();
                git.commit().setMessage("BIKS BIAdmin commit").call();
                git.push()
                        .setCredentialsProvider(new UsernamePasswordCredentialsProvider(archiveConfig.gitUsername, archiveConfig.gitPw))
                        .call();
                git.close();
            } catch (Exception e) {
                System.out.println("Error in GIT commit! Msg: " + e.getMessage());
                updateBackupLog(backupLogId, null, procesStepName + " " + e.getMessage());
                return;
            }
        }
        updateBackupLog(backupLogId, null, procesStepName + " backup: done");
    }

    private void backupOneObject(String procesStepName, SAPBOObjectBean sapboObj) {
        Date startTime = new Date();
        String tempFilePath = generateFilePath(true, BaseUtils.ensureDirSepPostfix(dirForUpload) + "LCMBIAR/" /*+ archiveConfig.jobCuid + "/" */ + procesStepName, sapboObj);
        String destFilePath = generateFilePath((archiveConfig.isRemoteSaving == 0), BaseUtils.ensureDirSepPostfix(archiveConfig.savedFolder) + procesStepName, sapboObj);
//        System.out.println("tempFilePath = " + tempFilePath);
//        System.out.println("destFilePath = " + destFilePath);
        try {
//            System.out.println("backupCnt = " + backupCnt);
            updateBackupLog(backupLogId, null, procesStepName + ": SI_ID = " + sapboObj.id);
            Runtime rt = Runtime.getRuntime();

            StringBuilder cmd = new StringBuilder();
            cmd.append("cmd /c java -Dtracelog.configscript=\"sap_trace_level=trace_none;sap_log_level=log_none;\" -cp \"").append(archiveConfig.lcmCliPath).append("\"")
                    .append(" com.businessobjects.lcm.cli.LCMCLI")
                    .append(" -action export")
                    .append(" -exportLocation ").append("\"").append(tempFilePath).append("\"")
                    .append(" -lcm_cms ").append(biServerConfig.host)
                    .append(" -lcm_username ").append(biServerConfig.user)
                    .append(" -lcm_password ").append(biServerConfig.password)
                    .append(" -lcm_authentication secEnterprise")
                    .append(" -source_cms ").append(biServerConfig.host)
                    .append(" -source_username ").append(biServerConfig.user)
                    .append(" -source_password ").append(biServerConfig.password)
                    .append(" -souce_authentication secEnterprise")
                    .append(" -includesecurity false ")
                    .append(" -exportdependencies false ")
                    .append(" -exportquery1 ").append(getExportQuery(procesStepName, sapboObj.id))
                    .append(" -exportqueriestotal 1")
                    .append(" -lcmbiarpassword")
                    .append(" -consolelog true")
                    .append(" -stacktrace false");
//                    .append(" -JOB_CUID ").append(archiveConfig.jobCuid);

            Process proc = rt.exec(cmd.toString());
            BufferedReader input = new BufferedReader(new InputStreamReader(proc.getInputStream()));

            String line = null;
            StringBuilder backupLog = new StringBuilder();
            while ((line = input.readLine()) != null) {
                backupLog.append("\n").append(line);
//                System.out.println(line);
            }
            updateBackupLog(backupLogId, null, procesStepName + ": SI_ID = " + sapboObj.id + " -> " + line);
//            System.out.println((System.currentTimeMillis() - startTime.getTime()) / 1000.0);
            proc.waitFor();
            proc.destroy();
            if (backupLog.toString().endsWith(BACKUP_SUCCESS_STATUS)) {
                backupCnt++;
                long fileSize = LameUtils.getFileSize(tempFilePath);
                if (Math.abs(archiveConfig.fileSize) < 1e-10 || fileSize < archiveConfig.fileSize * MB) {
                    writeTempFile2SavedFolder(tempFilePath, destFilePath);
                    createFileBackupLog(backupLogId, startTime, backupLog.toString(), destFilePath, sapboObj, 1, fileSize);
                } else {
                    createFileBackupLog(backupLogId, startTime, "Plik jest za duży", destFilePath, sapboObj, 0, fileSize);
                }
            } else {
                createFileBackupLog(backupLogId, startTime, backupLog.toString(), destFilePath, sapboObj);
            }
            if (LameUtils.fileExists(tempFilePath)) {
                LameUtils.deleteFile(tempFilePath);
            }
            biaProxyConnector.deleteBIAJobs("Query_" + biServerConfig.user);
        } catch (Exception ex) {
            logger.error("error: backupOneObject SI_ID = " + sapboObj.id + ",tempFilePath = " + tempFilePath + ", destFilePath = " + destFilePath + ", msg = " + ex.getMessage());
            createFileBackupLog(backupLogId, startTime, ex.getMessage(), destFilePath, sapboObj);
            // TF > nie rzucamy wyjątku i nie przerywamy zasilania przy jednym błędzie
//            throw new FoxyRuntimeException(ex);
        }
    }

    private String generateFilePath(boolean isLocal, String rootDirPath, SAPBOObjectBean sapboObj) {
        StringBuilder filePath = new StringBuilder();
        String folerPath = rootDirPath + sapboObj.location;
        folerPath = folerPath.replace('\\', '/');
        makeSureFolderExists(isLocal, folerPath);
        filePath.append(BaseUtils.ensureDirSepPostfix(folerPath)).append(sapboObj.name.replace(" ", "_").replace("\"", "").replace("'", ""));
        if (archiveConfig.useGit == 1) { // jeśli korzystamy z GIT to tworzymy tylko 1 plik i go aktualizujemy!
            filePath.append(BACKUP_FILE_EXT);
        } else {
            filePath.append("_").append(DATE_DEFAULT_FORMAT.format(sapboObj.modify)).append(BACKUP_FILE_EXT);
        }
        return filePath.toString();
    }

    private String getExportQuery(String procesStepName, Integer objId) {
        StringBuilder sb = new StringBuilder();
        sb.append("\"SELECT * FROM ");
        if (BACKUP_KIND_REPORTS.equals(procesStepName)) {
            sb.append("CI_INFOOBJECTS");
//        } else if (BACKUP_KIND_UNIVERSES.equals(procesStepName)) {
        } else {
            sb.append("CI_APPOBJECTS");
//            sb.append("CI_SYSTEMOBJECTS");
        }
        sb.append(" WHERE SI_ID = ").append(BaseUtils.safeToString(objId)).append("\"");
        return sb.toString();
//        return "\"SELECT * FROM CI_INFOOBJECTS WHERE SI_ID = " + BaseUtils.safeToString(objId) + "\"";
    }

    private void writeTempFile2SavedFolder(String tempFilePath, String destFilePath) throws IOException {
        if (archiveConfig.isRemoteSaving == 1) {
            smbFileHelper.writeTempFile2SavedFolder(tempFilePath, destFilePath);
        } else {
            localFileHelper.writeTempFile2SavedFolder(tempFilePath, destFilePath);
        }
    }

    private void makeSureFolderExists(boolean isLocal, String dirPath) {
        if (isLocal) {
            localFileHelper.makeSureFolderExists(dirPath);
        } else if (smbFileHelper != null) {
            smbFileHelper.makeSureFolderExists(dirPath);
        }
    }

    public Integer createProcessBackupLog() {
        return BeanConnectionUtils.<Integer>execNamedQuerySingleVal(adhocDao, "createProcessBackupLog", true, "id" /* I18N: no */);
    }

    public void updateBackupLog(Integer backupLogId, Date endTime, String status) {
        adhocDao.execNamedCommand("updateBackupLog", backupLogId, endTime, status);
    }

    public Map<Integer, SAPBOObjectBean> getArchivedObjs() {
        List<SAPBOObjectBean> listArchivedObjs = adhocDao.createBeansFromNamedQry("getArchivedObjs", SAPBOObjectBean.class);
        Map<Integer, SAPBOObjectBean> ret = new HashMap<Integer, SAPBOObjectBean>();
        for (SAPBOObjectBean bean : listArchivedObjs) {
            ret.put(bean.id, bean);
        }
        return ret;
    }

    @Override
    public void finishWorkUnit(boolean success) {
        adhocDao.finishWorkUnit(success);
    }

    private boolean fileWasArchived(String procesStepName, SAPBOObjectBean bean) {
        if (oldArchivedObjs.containsKey(bean.id)) {
            String destFilePath = generateFilePath((archiveConfig.isRemoteSaving == 0), archiveConfig.savedFolder + "/" + procesStepName, bean);
            return (archiveConfig.isRemoteSaving == 1 ? smbFileHelper.checkFileExists(destFilePath) : localFileHelper.checkFileExists(destFilePath));
        }
        return false;
    }

    private boolean fileWasOld(SAPBOObjectBean newObj) {
        SAPBOObjectBean oldObj = oldArchivedObjs.get(newObj.id);
        return (newObj.modify.getTime() / 1000) < (oldObj.modify.getTime() / 1000);
    }

    private void createFileBackupLog(Integer backupLogId, Date startTime, String status, String fileName, SAPBOObjectBean sapboObj, int isSuccess, long fileSize) {
        adhocDao.execNamedCommand("createFileBackupLog", backupLogId, startTime, status, fileName, sapboObj, isSuccess, fileSize);
    }

    public void createFileBackupLog(Integer backupLogId, Date startTime, String status, String fileName, SAPBOObjectBean sapboObj) {
        createFileBackupLog(backupLogId, startTime, status, fileName, sapboObj, 0, 0);
    }
}
