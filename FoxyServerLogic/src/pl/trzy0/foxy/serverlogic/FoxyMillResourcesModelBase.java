/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.LameUtils;
import java.util.Map;
import pl.foxys.foxymill.IRenderable;
import pl.trzy0.foxy.commons.templates.INamedTemplateProvider;
import pl.trzy0.foxy.glass.Glass;
import pl.trzy0.foxy.glass.GlassToText;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public abstract class FoxyMillResourcesModelBase extends FoxyMillModelBaseEx implements IFoxyMillResourcesModel {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    public static final String GETGLOBAL_VERSION_SEPARATOR = "$";
    private IApplicationEnvironment appEnv;

    public FoxyMillResourcesModelBase(IApplicationEnvironment appEnv) {
        super(appEnv.getSysProps());
        this.appEnv = appEnv;
    }

    public abstract String getClientLang();

    public String getText(String key) {
        return nullToDef(appEnv.getTextProps(getClientLang()).get(key), key);
    }

    public String glassAsText(Glass g) {
        if (logger.isDebugEnabled()) {
            logger.debug("glassAsText()...");
        }
        INamedTemplateProvider tmplProvider = appEnv.getTemplateProvider(getClientLang());
        String result = GlassToText.asText(tmplProvider, g, getSysProp("glass.resourceKeyPrefix"));
        if (logger.isDebugEnabled()) {
            logger.debug("glassAsText() = " + result);
        }
        return result;
    }

    // params can be null
    protected Map<String, Object> extendParamsWithDefaults(Map<String, Object> params) {
        return params; // no-op
    }

    protected String encodeSessionIDInUrl(String url) {
        return url; // no-op
    }

//    protected String innerMakeUrl(String urlBase, String pageName, Map<String, Object> params) {
//        params = extendParamsWithDefaults(params);
//
//        return encodeSessionIDInUrl(
//                urlBase + "?page=" + LameUtils.encodeForURLNoExcWW(pageName)
//                + (BaseUtils.isMapEmpty(params) ? ""
//                : "&" + LameUtils.encodeParamsForUrlWW(params)));
//    }
//
//    // url to canonical address (wrt canonical base url e.g. naqnaq.com),
//    public String makeCanonicalUrl(String pageName, Map<String, Object> params) {
//        return innerMakeUrl(appEnv.getCanonicalAppUrlBase(), pageName, params);
//    }

    public IRenderable getPage(String name) {
        return appEnv.getPage(name);
    }

    // empty string <==> null, null is returned for empty or whitespace string
    public String getVersionFromRequest() {
        return null;
    }

    public Object getGlobal(String name) {
        Object res = null;

        String v = getVersionFromRequest();

        if (v != null) {
            res = appEnv.getGlobal(v + GETGLOBAL_VERSION_SEPARATOR + name);
        }
        if (res == null) {
            res = appEnv.getGlobal(name);
        }
        return res;
    }
}
