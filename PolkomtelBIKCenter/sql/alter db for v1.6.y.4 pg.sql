﻿exec sp_check_version '1.6.y.4';
go

-- procedura konwertująca string(listę) jakiś wartości do tablicy
-- np  wywołanie 
--
--  select cast(item as int) from dbo.list2table('123|1234|4567', '|'); 
--
-- daje na wyjściu tablicę integerów

if  exists (select * from sys.objects where object_id = object_id(N'[dbo].[list2table]') and type in (N'fn', N'if', N'tf', N'fs', N'ft'))
	drop function [dbo].[list2table]
go

create function dbo.list2table
(
	@list varchar(max),
	@delim char
)
returns
	@parsedlist table
	(
		item varchar(max)
	)
	as
	begin
		declare @item varchar(max), @pos int
		set @list = ltrim(rtrim(@list))+ @delim
		set @pos = charindex(@delim, @list, 1)
		
		while @pos > 0
			begin
				set @item = ltrim(rtrim(left(@list, @pos - 1)))
				if @item <> ''
				  begin
				   insert into @parsedlist (item)
				   values (cast(@item as varchar(max)))
				  end
				set @list = right(@list, len(@list) - @pos)
				set @pos = charindex(@delim, @list, 1)
			end
		return
	end
go

exec sp_update_version '1.6.y.4', '1.6.y.5';
go
