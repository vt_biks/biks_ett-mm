/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.dqm;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author tflorczak
 */
public class NodeActionDQMDeactivateTest extends NodeActionDQMBase {

    @Override
    public boolean isEnabledByDesign() {
        return node != null && isInDQMAdminMode() && isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_DQM_TEST_FAILED, BIKGWTConstants.NODE_KIND_DQM_TEST_NOT_STARTED, BIKGWTConstants.NODE_KIND_DQM_TEST_SUCCESS);
    }

    @Override
    public String getCaption() {
        return I18n.dezaktywujTest.get();
    }

    @Override
    public void execute() {
        final int nodeId = node.linkedNodeId != null ? node.linkedNodeId : node.id;
        BIKClientSingletons.getDQMService().changeDQMTestActivity(nodeId, true, new StandardAsyncCallback<Void>("Error in changeDQMTestActivity") {

            @Override
            public void onSuccess(Void result) {
                BIKClientSingletons.showInfo(I18n.dezaktywwowanoTest.get());
                refreshRedisplayDataAndSelectRecord(node.id);
            }
        });
    }
}
