/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author pgajda
 */
public class ErwinTableBean implements Serializable {

    public String tableId;//longID
    public String name;
    public String schemaName;
    public Integer tableType;
    public String shapeID;
    public String tableComment;
    public Integer nodeId;
    public String objId;
}
