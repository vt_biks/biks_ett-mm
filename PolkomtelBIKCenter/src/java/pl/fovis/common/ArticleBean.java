/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author tflorczak
 */
public class ArticleBean implements Serializable {

    public int id;
    public String subject;
    public String body;
    public int official;
    public Set<String> tags;
    public Integer nodeId;
//    public Map<Integer, Pair<String, String>> innerLinks;
    public Set<Integer> deleteNodeId;
    public String dstCode;
    public int srcId;
    public String versions;
}
