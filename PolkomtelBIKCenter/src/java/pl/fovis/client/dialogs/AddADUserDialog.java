/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class AddADUserDialog extends BaseActionOrCancelDialog {

    private IParametrizedContinuation<SystemUserBean> saveCont;
    protected Map<String, SystemUserBean> users = new LinkedHashMap<String, SystemUserBean>();
    protected ListBox listBox = new ListBox();
    protected TextBox filter;
    protected PushButton filterBtn;
    protected String filterString = null;

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzUzytkownika.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.wybierz.get();
    }

    @Override
    protected void buildMainWidgets() {
        listBox.setMultipleSelect(false);
        listBox.setVisibleItemCount(20);
        listBox.setWidth("230px");
        HorizontalPanel hp = new HorizontalPanel();
        hp.setSpacing(0);
        filter = new TextBox();
        filter.setWidth("170px");
        filter.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                NativeEvent ne = event.getNativeEvent();
                if (ne.getKeyCode() == KeyCodes.KEY_ENTER) {
                    innerOnFilterHandler();
                }
            }
        });
        filterBtn = new PushButton(I18n.filtruj.get());
        NewLookUtils.makeCustomPushButton(filterBtn);
        filterBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                innerOnFilterHandler();
            }
        });
        filterBtn.setWidth("50px");
        hp.add(filter);
        hp.add(filterBtn);
        main.setSpacing(0);
        main.add(hp);
        main.add(listBox);
        getData();
    }

    protected void innerOnFilterHandler() {
        filterString = filter.getValue();
        getData();
    }

    @Override
    protected void doAction() {
        saveCont.doIt(users.get(listBox.getItemText(listBox.getSelectedIndex())));
    }

    public void buildAndShowDialog(IParametrizedContinuation<SystemUserBean> saveCont) {
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }

    protected void getData() {
        BIKClientSingletons.getService().getADUsers(filterString, new StandardAsyncCallback<List<SystemUserBean>>("Error in getADUsers") {

            @Override
            public void onSuccess(List<SystemUserBean> result) {
                listBox.clear();
                users.clear();
                for (SystemUserBean res : result) {
                    listBox.addItem(res.loginName);
                    users.put(res.loginName, res);
                }
            }
        });
    }

    @Override
    protected void onEnterPressed() {
        innerOnFilterHandler();
    }
}
