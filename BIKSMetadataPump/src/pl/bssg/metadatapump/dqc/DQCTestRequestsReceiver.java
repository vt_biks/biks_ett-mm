/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.dqc;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;

/**
 *
 * @author tflorczak
 */
public class DQCTestRequestsReceiver {

    protected IBeanConnectionEx<Object> conn;
    protected INamedSqlsDAO<Object> adhocDao;

    public DQCTestRequestsReceiver(MssqlConnectionConfig cfg) {
        conn = new MssqlConnection(cfg);
        conn.setDbStatementExecutionWarnThresholdMillis(-1);
        adhocDao = FoxyAppUtils.createNamedSqlsDAOFromJar(conn, MetadataPumpBase.ADHOC_SQLS_PATH);
    }

    public Set<Integer> getDailyTests() {
        List<DQCTestBean> tests = adhocDao.createBeansFromNamedQry("getDaylyTestsFromDQC", DQCTestBean.class);
        Set<Integer> uniqueTests = new HashSet<Integer>();
        for (DQCTestBean dQCTestBean : tests) {
            uniqueTests.add(dQCTestBean.idTest);
        }
        return uniqueTests;
    }

    public List<DQCRequestBean> getRequestsForTest(int testId) {
        return adhocDao.createBeansFromNamedQry("getRequestsForTest", DQCRequestBean.class, testId);
    }

    public void finishWorkUnit(boolean isSoccess) {
        conn.finishWorkUnit(isSoccess); // ew false
    }
}
