IF OBJECT_ID('tempdb..#xxx') IS NOT NULL 
drop table #xxx
go

declare @rapTreeId int = (select id from bik_tree where code = 'Reports')
declare @rapWebiKindId int = (select dbo.fn_node_kind_id_by_code('Webi'))
declare @univTreeId int = (select id from bik_tree where code = 'ObjectUniverses')
declare @univKindId int = (select dbo.fn_node_kind_id_by_code('Universe'))
declare @connKindId int = (select dbo.fn_node_kind_id_by_code('DataConnection'))
declare @connTreeId int = (select id from bik_tree where code = 'Connections')

select
		cast('' as varchar(max)) as conn_parent_path, 
		conn.name as conn_name,
		extConn.connetion_networklayer_name as conn_network_layer,
		extConn.database_engine as conn_database_engine,
		bn.name as joined_object_name,
		bnk.caption as joined_object_kind,
		conn.parent_node_id as conn_ancestor_id
 into #xxx
from
		
		bik_node conn
		left join bik_sapbo_universe_connection extConn on extConn.node_id = conn.id
		left join (bik_joined_objs objs inner join bik_node bn on objs.dst_node_id=bn.id		
		inner join bik_node_kind bnk on bn.node_kind_id=bnk.id) on objs.src_node_id=conn.id and objs.type=1
		and bn.is_deleted = 0 and bn.linked_node_id is null and bn.tree_id in (@rapTreeId,@univTreeId) and bn.node_kind_id in (@rapWebiKindId,@univKindId)
		
where 
		conn.is_deleted = 0 and conn.linked_node_id is null and conn.tree_id = @connTreeId and conn.node_kind_id = @connKindId
go




while 1 = 1 begin
  declare @rc int = null

  update #xxx
  set conn_parent_path = bn.name + '\' + conn_parent_path, conn_ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #xxx.conn_ancestor_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go

--select * from #xxx
select conn_parent_path,conn_name,conn_network_layer,conn_database_engine,joined_object_name,joined_object_kind 
from #xxx
order by conn_parent_path,conn_name,joined_object_kind,joined_object_name