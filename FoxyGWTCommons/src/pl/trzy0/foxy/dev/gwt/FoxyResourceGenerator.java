/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.dev.gwt;

import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.TreeLogger.Type;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JMethod;
import com.google.gwt.core.ext.typeinfo.JPackage;
import com.google.gwt.dev.resource.impl.PathPrefix;
import com.google.gwt.dev.resource.impl.PathPrefixSet;
import com.google.gwt.dev.resource.impl.ResourceFilter;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.foxygwtcommons.IFoxyResourceBoundle;
import pl.fovis.foxygwtcommons.IFoxyResourceFiles;
import pl.trzy0.foxy.commons.annotations.ResourceDir;
import pl.trzy0.foxy.commons.annotations.ResourceFile;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.StringEscapeUtils;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public class FoxyResourceGenerator extends FoxyGeneratorBase {

    private static final TreeLogger.Type LOGGER_LEVEL = TreeLogger.WARN;

    private void generateForResourceBoundle() {
        Map<String, String> resources = getResources();
        log("found resources" /* I18N: no */ + ": " + resources.keySet());
        println("private static Map<String, String> resources = new LinkedHashMap<String, String" /* I18N: no */ + ">();");
        println("static" /* I18N: no */ + " {");
        for (Entry<String, String> e : resources.entrySet()) {
            println("  " + "resources.put" /* I18N: no */ + "(\"" + e.getKey() + "\", \""
                    + StringEscapeUtils.escapeJavaScript(e.getValue()) + "\");");
        }
        println("}");
        println("public String get(String name" /* I18N: no */ + ") {");
        println("  " + "String res = resources.get(name" /* I18N: no */ + ");");
        println("  " + "if (res == null" /* I18N: no */ + ") " + getThrowStmt("no resource with name" /* I18N: no */ + " \"", "name" /* I18N: no */, "\""));
        println("  " + "return res" /* I18N: no */ + ";");
        println("}");
        println("public Set<String> getNames" /* I18N: no */ + "() {");
        println("  " + "return resources.keySet" /* I18N: no */ + "();");
        println("}");
    }

    private void generateForResourceFiles() throws UnableToCompleteException {
        String basePath = "/" + BaseUtils.dropLastItems(genClassName.replace(".", "/"), "/", 1);
        String defExt = null;
        String defEnc = null;
        if (genClassType.isAnnotationPresent(ResourceDir.class)) {
            ResourceDir rd = genClassType.getAnnotation(ResourceDir.class);
            if (!BaseUtils.isStrEmpty(rd.value())) {
                basePath = BaseUtils.mergePaths(basePath, rd.value(), "/", "..");
            }
            defEnc = rd.encoding();
            defExt = rd.ext();
        }
        JMethod[] methods = genClassType.getMethods();
        for (JMethod method : methods) {
            if (!method.isAnnotationPresent(ResourceFile.class)) {
                continue;
            }
            ResourceFile rf = method.getAnnotation(ResourceFile.class);
            String enc = BaseUtils.emptyStrToDef(rf.encoding(), defEnc);
            String ext = BaseUtils.emptyStrToDef(rf.ext(), defExt);
            String fileName = BaseUtils.emptyStrToDef(rf.value(), method.getName());
            if (!BaseUtils.isStrEmpty(ext)) {
                fileName = fileName + "." + ext;
            }
            generateMethod(method.getName(), fileName, enc, basePath);
        }
    }

    private Map<String, String> getResources() {
        ResourceDir rd = genClassType.getAnnotation(ResourceDir.class);
        String relativeDir = "";
        String extStr = rd == null ? null : rd.ext();

        Set<String> ext0 = null;
        if (!BaseUtils.isStrEmpty(extStr)) {
            ext0 = new LinkedHashSet<String>();
            List<String> preExt = BaseUtils.splitBySep(extStr, ";");
            for (String preExtSingle : preExt) {
                ext0.add("." + preExtSingle);
            }
        }

        final Set<String> ext = ext0;
        String enc = null;

        if (rd != null) {
            relativeDir = rd.value();
            enc = rd.encoding();
        } else {
            log("no ResourceDir annotation for" /* I18N: no */ + " " + genClassName);
        }

        JPackage pkg = genClassType.getPackage();

        String pkgPath = pkg.getName().replace(".", "/");

        final String path = BaseUtils.mergePaths(pkgPath, relativeDir, "/", "..");
        log("package path" /* I18N: no */ + "=\"" + pkgPath
                + "\", relativeDir=\"" + relativeDir
                + "\", " + "merged path" /* I18N: no */ + "=\"" + path + "\"");

        PathPrefixSet pathPrefixSet = new PathPrefixSet();
        pathPrefixSet.add(new PathPrefix(path, ext == null ? null : new ResourceFilter() {

            public boolean allows(String path) {
                String fileExt = BaseUtils.extractFileExtFromFullPath(path);
                boolean allow = ext.contains(fileExt);
                //log("path=\"" + path +
                //        "\", fileExt=\"" + fileExt + "\", allow=" + allow);
                return allow;
            }
        }));

        Collection<String> paths = getResourcePathNames(pathPrefixSet);
        log("paths" /* I18N: no */ + "=" + paths);

        Map<String, String> res = new LinkedHashMap<String, String>();
        String pathWithSep = path + "/";

        for (String filePath : paths) {
            String fileName = BaseUtils.dropOptionalPrefix(filePath, pathWithSep);
            if (ext != null) {
                fileName = BaseUtils.dropFileExtFromFullPath(fileName);
            }
            String content = loadFileContent("/" + filePath, enc);
            res.put(fileName, content);
        }

        return res;
    }

    @Override
    protected void addImports() {
        addImport(Set.class);
        addImport(Map.class);
        addImport(LinkedHashMap.class);
        addImport(LameRuntimeException.class);
    }

    protected void generateClassBody() throws UnableToCompleteException {
        JClassType filesIntf = getClassType(IFoxyResourceFiles.class);
        JClassType boundleIntf = getClassType(IFoxyResourceBoundle.class);

        if (genClassType.isAssignableTo(filesIntf)) {
            generateForResourceFiles();
        }

        if (genClassType.isAssignableTo(boundleIntf)) {
            generateForResourceBoundle();
        }
    }

    private void generateMethod(String methodName, String fileName, String encoding, String basePath) throws UnableToCompleteException {

        String fullResourcePath = BaseUtils.mergePaths(basePath, fileName, "/", "..");
        try {
            String content = loadFileContent(fullResourcePath, encoding);
            println("public String" /* I18N: no */ + " " + methodName + "() { " + "return" /* I18N: no */ + " \"" + StringEscapeUtils.escapeJavaScript(content) + "\"; }");
        } catch (Exception ex) {
            throwUnableToComplete("error for getResourceAsStream, basePath" /* I18N: no */ + "=\"" + basePath + "\", fileName=\"" + fileName + "\", fullResourcePath=\"" + fullResourcePath + "\"", ex);
        }
    }

    @Override
    protected Type getDefaultLogLevel() {
        return LOGGER_LEVEL;
    }
}
