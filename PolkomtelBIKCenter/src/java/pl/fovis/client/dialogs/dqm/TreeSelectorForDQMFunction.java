/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.TreeSelectorForExpliciteTreeCodesDialog;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class TreeSelectorForDQMFunction extends TreeSelectorForExpliciteTreeCodesDialog {

    @Override
    protected boolean isInheritToDescendantsCheckBoxVisible() {
        return false;
    }

    @Override
    protected boolean isActionButtonEnabled() {
        return selectedNode != null && (selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_MSSQL_TABLE_FUNCTION)
                || selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_ORACLE_FUNCTION)
                || selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_POSTGRES_FUNCTION));
    }

    @Override
    protected BikNodeTreeOptMode getTreeOptExtraMode() {
        return BikNodeTreeOptMode.DQMDBFunction;
    }

    public void buildAndShowDialog(Integer thisNodeId, String treeCode, IParametrizedContinuation<TreeNodeBean> saveCont) {
        super.buildAndShowDialog(thisNodeId, BaseUtils.paramsAsSet(treeCode), saveCont, null);
    }

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzFunkcje.get() /* I18N:  */;
    }

    @Override
    protected boolean isHeaderVisible() {
        return false;
    }
}
