exec sp_check_version '1.1.4.1';
go

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

alter table bik_node add visual_order int not null default 0
go


declare @nk_id int = (select id from bik_node_kind where code = 'UniverseTablesFolder')

update bik_node set visual_order = -1 where node_kind_id = @nk_id
go

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_fix_nodes_visual_order]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_fix_nodes_visual_order
go

create procedure sp_fix_nodes_visual_order(@parent_id int, @tree_id int) as
begin
  set nocount on
  --declare @parent_id int = 869338, @tree_id int = 16

  declare @vofix table (id int not null primary key, visual_order_fix int not null)

  insert into @vofix (id, visual_order_fix)
  select id, (ROW_NUMBER() OVER (ORDER BY visual_order, name)) - 1 AS visual_order_fix --, * 
  from 
  bik_node
  where tree_id = @tree_id and is_deleted = 0 and (@parent_id is null and parent_node_id is null or @parent_id is not null and parent_node_id = @parent_id)

  update 
  bik_node
  set visual_order = visual_order_fix
  from @vofix v
  where v.id = bik_node.id
  and bik_node.tree_id = @tree_id and bik_node.is_deleted = 0 and (@parent_id is null and bik_node.parent_node_id is null or @parent_id is not null and bik_node.parent_node_id = @parent_id)
end
go


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_set_node_visual_order]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_set_node_visual_order
go

create procedure sp_set_node_visual_order(@node_id int, @visual_order int) as
begin
  set nocount on

  declare @parent_id int, @tree_id int

  select @parent_id = parent_node_id, @tree_id = tree_id from bik_node where id = @node_id

  exec sp_fix_nodes_visual_order @parent_id, @tree_id
  
  update bik_node
  set visual_order = visual_order + 1
  where tree_id = @tree_id and is_deleted = 0 and (@parent_id is null and parent_node_id is null or @parent_id is not null and parent_node_id = @parent_id)
    and visual_order >= @visual_order
    
  update bik_node set visual_order = @visual_order where id = @node_id  
end
go


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_reset_visual_order_for_nodes]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_reset_visual_order_for_nodes
go

create procedure sp_reset_visual_order_for_nodes(@parent_id int, @tree_id int) as
begin
  set nocount on

  update bik_node
  set visual_order = 0
  where tree_id = @tree_id and is_deleted = 0 and (@parent_id is null and parent_node_id is null or @parent_id is not null and parent_node_id = @parent_id)
end
go

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

/*

update bik_node set visual_order = 1 where id = 841782

update bik_node set visual_order = -1 where id = 841780


select * from bik_tree

declare @parent_id int = 869338, @tree_id int = 16

  select visual_order, * 
  from 
  bik_node
  where tree_id = @tree_id and is_deleted = 0 and (@parent_id is null and parent_node_id is null or @parent_id is not null and parent_node_id = @parent_id)
  order by bik_node.visual_order, bik_node.name
  
Cykl �ycia klienta
Macierz migracji
Prognoza
Ranking
Raport bazy abonenckiej
Rozw�j bazy produktowej
Sta�e
Szeregi czasowe
Analiza prze�ycia


  exec sp_set_node_visual_order 841777, -1
  
  exec sp_set_node_visual_order 841777, 1
  
  exec sp_fix_nodes_visual_order 869338, 16
  
  exec sp_reset_visual_order_for_nodes 869338, 16
  
*/

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

exec sp_update_version '1.1.4.1', '1.1.4.2';
go
