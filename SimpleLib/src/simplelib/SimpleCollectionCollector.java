/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.Collection;

/**
 *
 * @author wezyr
 */
public class SimpleCollectionCollector<T> extends CollectionCollectorEx<T, T> {

    public SimpleCollectionCollector(Collection<T> coll) {
        super(null, coll);
    }

    public SimpleCollectionCollector() {
        super(null);
    }

    @Override
    protected T convertItem(T item) {
        return item;
    }
}
