/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.ObjectInHistoryBeanExtractor;
import pl.fovis.client.entitydetailswidgets.IGridBeanAction;
import pl.fovis.common.ObjectInHistoryBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class MyBIKSHistoryGrid extends MyBIKSGridSingleRowBase<ObjectInHistoryBean> implements IPaginatedItemsDisplayer<ObjectInHistoryBean> {

    protected Map<String, Widget> postponedAncestorPaths = new HashMap<String, Widget>();

    @Override
    protected String getIconUrl(ObjectInHistoryBean n) {
        return null;
    }
//    @Override
//    protected void addRowToItem(int row, ObjectInHistoryBean n) {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }

//do przezucenia wyżej
    public void createRowFromBean(int gridIdx, final ObjectInHistoryBean item/*FlexTable gp, IGridNodeAwareBeanPropsExtractor<T> extr*/) {

        ObjectInHistoryBeanExtractor extr = new ObjectInHistoryBeanExtractor();
        int col = 0;
        if (extr.hasIcon()) {
            gp.getColumnFormatter().setWidth(col, "26px");
            gp.setWidget(gridIdx, col++, BIKClientSingletons.createIconImg(extr.getIconUrl(item), "16px", "16px",
                    extr.getOptIconHint(item)));
        }

        String itemName = extr.getNameAsHtml(item);
        if (itemName != null) {
            Integer itemNodeId = extr.getNodeId(item);
            if (itemNodeId != null) {
                Widget nameWidget = BIKClientSingletons.createHtmlLabelLookupButton(itemName, extr.getTreeCode(item), itemNodeId, null, true, null);
                gp.setWidget(gridIdx, col++, nameWidget);
                postponedAncestorPaths.put(extr.getBranchIds(item), nameWidget);
            } else {
                gp.setHTML(gridIdx, col++, itemName);
            }
        }
//        else {
//            gp.setText(gridIdx, col++, ""); //bez tego nie umieszcza stylu na komorce
//        }
        for (int i = 0; i < extr.getAddColCnt(); i++) {

            Object addColVal = extr.getAddColVal(item, i);
            IsWidget val = getCellValAsWidget(addColVal);
            if (val instanceof Image) {
                gp.getColumnFormatter().setWidth(col, "26px");
            }
            gp.setWidget(gridIdx, col++, val);
        }
        for (int i = 0; i < extr.getActionCnt(); i++) {
            final IGridBeanAction<ObjectInHistoryBean> act = extr.getActionByIdx(i);

            String actionIconUrl = act.getActionIconUrl(item);

            if (actionIconUrl != null && act.isVisible(item)) {
                gp.getColumnFormatter().setWidth(col, "26px");
                gp.setWidget(gridIdx, col++, createActionBtn(act, actionIconUrl, item));
            } else {
                gp.setText(gridIdx, col++, ""); //bez tego komorki się zle czasem ukladaja.
            }
        }
    }

    public static IsWidget getCellValAsWidget(Object val) {
        if (val instanceof IsWidget) {
            return (IsWidget) val;
        } else {
            Widget w;
            if (val instanceof SafeHtml) {
                SafeHtml h = (SafeHtml) val;
                w = new InlineHTML(h);
            } else {
                String strVal = val == null ? "" : val.toString();
                w = new InlineLabel(strVal);
            }
            return w;
        }
    }

    @Override
    protected void setUpGridColumnsWidths(FlexTable gp) {
        gp.setWidth("100%");
        gp.getColumnFormatter().setWidth(0, "16px");
        gp.getColumnFormatter().setWidth(2, "100px");
        gp.getColumnFormatter().setWidth(3, "150px");
        gp.getColumnFormatter().setWidth(4, "26px");
        gp.getColumnFormatter().setWidth(5, "26px");
    }

    @Override
    protected void addOrRefreshTitles(int row, ObjectInHistoryBean n) {
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void addHeaderRow() {
        int row = 0;
        columnsCnt = 0;
        gp.setWidget(row, columnsCnt++, new Label(""));
        gp.setWidget(row, columnsCnt++, new Label(I18n.nazwa.get() /* I18N:  */));
        gp.setWidget(row, columnsCnt++, new Label(I18n.odwiedzin.get() /* I18N:  */));
        gp.setWidget(row, columnsCnt++, new Label(I18n.ostatnieOdwiedziny.get() /* I18N:  */));
        gp.setWidget(row, columnsCnt++, new Label(""));
        gp.setWidget(row, columnsCnt++, new Label(""));
        gp.setWidth("100%");
        gp.getRowFormatter().setStyleName(0, "Rank-Users-Gp");
    }

    @Override
    protected boolean isHeaderRowVisible() {
        return true;
    }
}
