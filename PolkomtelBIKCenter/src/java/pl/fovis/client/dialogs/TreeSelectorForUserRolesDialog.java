/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import java.util.Collection;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.JoinTypeRoles;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.UseInNodeSelectionBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author beata
 * z poziomu uzytkownika
 */
public class TreeSelectorForUserRolesDialog extends TreeSelectorForJoinedObjectsDialog/*
 * TreeSelectorDialogBase
 */ {

    protected int roleId;
    protected JoinTypeRoles roleType;
    protected boolean isAlreadyJoinedReplace;

    public void buildAndShowDialog(int thisNodeId, Set<String> treeKind,
            IParametrizedContinuation<TreeNodeBean> saveCont,
            IJoinedObjSelectionManager<Integer> alreadyJoinedManager, Pair<UseInNodeSelectionBean, Boolean> param) {
        this.treeKinds = treeKind;
        this.roleId = param.v1.roleForNodeId;
        this.roleType = param.v1.joinTypeRole;
        this.isAlreadyJoinedReplace = param.v2;
        //this.alreadyJoinedObjIds = alreadyJoinedObjIds;
        super.buildAndShowDialog(thisNodeId, saveCont, alreadyJoinedManager);
    }

    @Override
    protected void doAction() {
        if (isInMultiSelectMode()) {
            BIKClientSingletons.showInfo(I18n.zapisywaniePowiazanProszeCzekac.get() /* I18N:  */ + "...");
            Collection<Integer> alreadyJoinedObjIds = alreadyJoinedManager.getJoinedObjIds();
            for (int dO : directOverride.keySet()) {
                if (alreadyJoinedObjIds.contains(dO)) {
                    alreadyJoinedObjIds.remove(dO);
                }
            }
            getService().updateUserRolesForNode(nodeId, mustHideLinkedNodes(),
                    directOverride, subNodesOverride, roleId, treeKinds, roleType == JoinTypeRoles.Auxiliary,
                    roleType, isAlreadyJoinedReplace ? alreadyJoinedObjIds : null, new StandardAsyncCallback<Boolean>() {
                public void onSuccess(Boolean result) {
                    BIKClientSingletons.showInfo(I18n.powiazaniaZapisaneOdswiezanie.get() /* I18N:  */);
                    BIKClientSingletons.refreshAndRedisplayDataUsersTree(result);
                    alreadyJoinedManager.refreshJoinedNodes(nodeId);
//                    if (result) {
//                        ITabSelector<Integer> tabSelector = BIKClientSingletons.getTabSelector();
//                        String treeCode = BIKConstants.TREE_CODE_USERS_ROLES;
////                        if (!treeCode.equals(tabSelector.getCurrentTab().getId())) {
//                            tabSelector.invalidateTabData(treeCode);
//
////                        }
//                    }

                }
            });
        } else {
            saveCont.doIt(selectedNode);
        }
    }
}
