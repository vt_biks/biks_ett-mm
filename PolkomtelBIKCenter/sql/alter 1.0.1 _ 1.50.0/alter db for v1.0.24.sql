exec sp_check_version '1.0.24';
go


if not exists (select * from bik_node_kind where code = 'Keyword') 
insert into bik_node_kind(code,caption) values('Keyword','S�owo kluczowe')
go

update bik_tree
set node_kind_id = (select id from bik_node_kind where code = 'Folder')
where name = 'Tezaurus'
go

exec sp_update_version '1.0.24', '1.0.25';
go
