/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import pl.fovis.client.nodeactions.INodeClipboard;
import pl.fovis.common.TreeBean;

/**
 *
 * @author pmielanczuk
 */
public interface IBikTreePage extends IBikPage<Integer> {

    // zawsze zwróci not null
    public INodeClipboard getNodeClipboard();

    // zawsze zwróci not null
    public TreeBean getTreeBean();

    public BikTreeWidget getBikTree();
}
