﻿exec sp_check_version '1.1.4.7';
go
------------------------------------
------------------------------------


if OBJECT_ID (N'dbo.fn_get_visual_order_for_new_node', N'FN') is not null
    drop function dbo.fn_get_visual_order_for_new_node;
go

create function dbo.fn_get_visual_order_for_new_node(@parent_id int)
returns int
as
begin
  declare @res int = (select max(visual_order) from bik_node where parent_node_id = @parent_id and is_deleted = 0)
  return case when coalesce(@res, 0) = 0 then 0 else @res + 1 end
end
go


------------------------------------
------------------------------------

exec sp_update_version '1.1.4.7', '1.1.4.8';
go
