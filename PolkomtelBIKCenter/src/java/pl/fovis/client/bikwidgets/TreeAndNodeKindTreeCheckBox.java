/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.MenuNodeBean;
import simplelib.BaseUtils;

/**
 *
 * @author beata
 */
public abstract class TreeAndNodeKindTreeCheckBox extends TreeAndNodeKindTreeBase {

    protected Map<Integer, CheckBox> checkBoxMap = new HashMap<Integer, CheckBox>();
    protected int disableFireCheckBoxChange;

    protected abstract boolean calcCheckBoxValueForTreeIdNode(Map<String, Object> row);

    @Override
    protected Map<String, Object> convertBeanToRow(final MenuNodeBean mnb) {

//        CheckBox cb = createValueForNameColumn(mnb);
//        return new HashMap<String, Object>() {
//            {
//                put("id" /* I18N: no */, mnb.id);
//                put("parentId", mnb.parentNodeId);
//                put("name" /* I18N: no */, cb);
//                put("icon" /* I18N: no */, null);
//            }
//        };
        Object ncv = createValueForNameColumn(mnb);

        Map<String, Object> res = new HashMap<String, Object>();
        res.put("id" /* I18N: no */, mnb.id);
        res.put("parentId", mnb.parentNodeId);
        res.put("name" /* I18N: no */, ncv);
        res.put("icon" /* I18N: no */, null);

        return res;
    }

    protected Object createValueForNameColumn(final MenuNodeBean mnb) {
        final CheckBox cb = new CheckBox();
        cb.setValue((!isExpandAllNodes()) || isInitiallyChecked(mnb), false);
        if (mnb.nodeKindId == null) {
            cb.setHTML("<b>" + BaseUtils.encodeForHTMLTag(mnb.name) + "</b>");
        } else {
            cb.setHTML(BIKClientSingletons.getHtmlForNodeKindIconWithText(mnb.nodeKindCode, mnb.treeId != null ? BIKClientSingletons.getTreeCodeById(mnb.treeId) : null, mnb.name));
        }
        cb.setStyleName("cellInner");
        cb.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (disableFireCheckBoxChange == 0) {
                    recalcParentAndChildrenCheckBoxes(mnb);
                    //                    tu...mnb.nodeKindId;
                    //                    Window.alert(cb.getName()+" "+mnb.code+" "+mnb.nodeKindId);
                    selectionChanged();
                    //performSearchAndShow();
                }
            }
        });
        checkBoxMap.put(mnb.id, cb);
        return cb;
    }

    protected void safeSetValueInCheckBoxMap(Integer id, Boolean val) {
        CheckBox cb = checkBoxMap.get(id);
        if (cb == null) {
            return;
        }

        cb.setValue(val);
    }

    protected void recalcParentAndChildrenCheckBoxes(MenuNodeBean mnb) {
        disableFireCheckBoxChange++;
        try {
            int id = mnb.id;
            Map<String, Object> row = storage.getRowById(id);

            if (mnb.nodeKindId == null) {
                boolean val = getCheckBoxValue(id);
                recalcChildrenCheckBoxes(row, val);
                fixAncestorParentTabCheckboxes(row);
            } else {
                Integer parentId = (Integer) row.get("parentId");
                MenuNodeBean ancestor;

                // znajdź zakładkę - ancestora
                while (parentId != null) {
                    ancestor = storage.getBeanById(parentId);
                    if (ancestor.treeId != null) {
                        break;
                    }
                    parentId = ancestor.parentNodeId;
                }

                if (parentId == null) {
                    //Window.alert("parentId is null :-(");
//                    if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_warn)) {
//                        ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_warn, "NewSearchBikPage: parentId is null :-(");
//                    }
                    return;
                }

                row = storage.getRowById(parentId);
                //to?
//                checkBoxMap.get(parentId).setValue(calcCheckBoxValueForTreeIdNode(row));
                safeSetValueInCheckBoxMap(parentId, calcCheckBoxValueForTreeIdNode(row));
                //
                fixAncestorParentTabCheckboxes(row);
            }
        } finally {
            disableFireCheckBoxChange--;
        }
    }

    protected boolean getCheckBoxValue(int id) {
        CheckBox cb = checkBoxMap.get(id);
        return cb == null ? false : cb.getValue();
    }

    protected void recalcChildrenCheckBoxes(Map<String, Object> row, boolean val) {
        Collection<Map<String, Object>> children = storage.getChildRows(row);
        if (children != null) {
            for (Map<String, Object> child : children) {
                int childId = (Integer) child.get("id" /* I18N: no */);
//                checkBoxMap.get(childId).setValue(val);
                safeSetValueInCheckBoxMap(childId, val);
                recalcChildrenCheckBoxes(child, val);
            }
        }
    }

    protected boolean areAllDescendantsOneValEx(Map<String, Object> row, boolean oneVal,
            boolean checkChildrenOnly) {
        Collection<Map<String, Object>> children = storage.getChildRows(row);
        if (children != null) {
            for (Map<String, Object> child : children) {
                int childId = (Integer) child.get("id" /* I18N: no */);
                if (getCheckBoxValue(childId) != oneVal) {
                    return false;
                }
                if (!checkChildrenOnly) {
                    boolean subDescendantsOK = areAllDescendantsOneVal(child, oneVal);
                    if (!subDescendantsOK) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    protected boolean areAllDescendantsOneVal(Map<String, Object> row, boolean oneVal) {
        return areAllDescendantsOneValEx(row, oneVal, false);
    }

    protected void fixAncestorParentTabCheckboxes(Map<String, Object> row) {
        while (true) {
            Integer parentId = (Integer) row.get("parentId");
            if (parentId == null) {
                break;
            }
            row = storage.getRowById(parentId);
            boolean currVal = getCheckBoxValue(parentId);
            boolean newVal = areAllChildrenChecked(row);
            if (currVal == newVal) {
                break;
            }
//            checkBoxMap.get(parentId).setValue(newVal);
            safeSetValueInCheckBoxMap(parentId, newVal);
        }
    }

    protected boolean areAllChildrenChecked(Map<String, Object> row) {
        return areAllChildrenOneVal(row, true);
    }

    protected boolean areAllChildrenOneVal(Map<String, Object> row, boolean oneVal) {
        return areAllDescendantsOneValEx(row, oneVal, true);

    }

    public Collection<Integer> getCheckedTreeIds() {
        Set<Integer> res = new LinkedHashSet<Integer>();

        for (Map<String, Object> row : storage.getAllRows()) {
            int id = storage.getTreeBroker().getNodeId(row);
            MenuNodeBean mnb = storage.getBeanById(id);
            Integer treeId = mnb.treeId;
            if (treeId != null) {
                if (getCheckBoxValue(id)) {
                    res.add(treeId);
                }
            }
        }

        return res;
    }

    public Set<Integer> getCheckedNodeKindIdsOnTree() {
        Set<Integer> res = new LinkedHashSet<Integer>();

        for (Map<String, Object> row : storage.getAllRows()) {
            int id = storage.getTreeBroker().getNodeId(row);
            MenuNodeBean mnb = storage.getBeanById(id);
            Integer nodeKindId = mnb.nodeKindId;
            if (nodeKindId != null) {
                if (getCheckBoxValue(id)) {
                    res.add(nodeKindId);
                }
            }
        }

        return res;
    }

    protected boolean isInitiallyChecked(MenuNodeBean mnb) {
        return false;
    }
}
