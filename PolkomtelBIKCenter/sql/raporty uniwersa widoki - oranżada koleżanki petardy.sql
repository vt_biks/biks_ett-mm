-- http://www.youtube.com/watch?v=sM2D3BkoRM0
-- s�owa mia�d��... wiele z nich zgadza si� nawet :D

-- 10:13

select * from bik_node_kind



select distinct objkind.code 
from bik_node rep inner join bik_node_kind repkind on rep.node_kind_id = repkind.id
left join (bik_joined_objs repjoobj
inner join bik_node objs on repjoobj.dst_node_id = objs.id inner join bik_node_kind objkind on objs.node_kind_id = objkind.id) on rep.id = repjoobj.src_node_id
where repkind.code = 'Webi'


select distinct code from
(
select --distinct 
tabkind.code 
from bik_node obj inner join bik_node_kind objkind on obj.node_kind_id = objkind.id
inner join bik_joined_objs objjoins on obj.id = objjoins.src_node_id
inner join bik_node tab on objjoins.dst_node_id = tab.id inner join bik_node_kind tabkind on tab.node_kind_id = tabkind.id
where objkind.code in ('Measure', 'Dimension', 'Detail')
) x



declare @codes table (code varchar(255) not null)

insert into @codes (code)
select --distinct 
tabkind.code 
from 
bik_node objlinked inner join
bik_node obj on objlinked.linked_node_id = obj.id inner join bik_node_kind objkind on obj.node_kind_id = objkind.id
inner join bik_joined_objs objjoins on obj.id = objjoins.src_node_id
inner join bik_node tab on objjoins.dst_node_id = tab.id inner join bik_node_kind tabkind on tab.node_kind_id = tabkind.id
where objkind.code in ('Measure', 'Dimension', 'Detail')

select distinct code from @codes


-- 163730
select count(*) from bik_node where disable_linked_subtree = 1


-- 163730
select 
count(*)
from 
bik_node rap inner join bik_node qry on qry.parent_node_id = rap.id inner join
bik_node objlinked on objlinked.parent_node_id = qry.id
where objlinked.disable_linked_subtree = 1


select 
--count(distinct qry.id), count(*)
qry.id, count(distinct univ.id), min(univ.id), max(univ.id)
from 
bik_node rap inner join bik_node qry on qry.parent_node_id = rap.id inner join
bik_node objlinked on objlinked.parent_node_id = qry.id inner join
bik_node obj on objlinked.linked_node_id = obj.id inner join bik_node_kind objkind on obj.node_kind_id = objkind.id
inner join bik_joined_objs objjoins on obj.id = objjoins.src_node_id
inner join bik_node tab on objjoins.dst_node_id = tab.id inner join bik_node_kind tabkind on tab.node_kind_id = tabkind.id
inner join bik_joined_objs qryjoins on qry.id = qryjoins.src_node_id
inner join bik_node univ on qryjoins.dst_node_id = univ.id
inner join bik_node_kind univkind on univkind.id = univ.node_kind_id
where objlinked.disable_linked_subtree = 1
and objlinked.is_deleted = 0 and rap.is_deleted = 0 and qry.is_deleted = 0
and tab.is_deleted = 0
and tabkind.code in ('TeradataView', 'TeradataTable')
and univkind.code = 'Universe'
group by qry.id
having count(distinct univ.id) > 1


select * from bik_node n inner join bik_node_kind k on n.node_kind_id = k.id
where n.id = 34938



select
qry.id, qry.name, count(*) as cnt
from bik_node qry inner join bik_node_kind qrykind on qrykind.id = qry.node_kind_id
inner join bik_joined_objs qryjoins on qry.id = qryjoins.src_node_id
inner join bik_node univ on univ.id = qryjoins.dst_node_id
inner join bik_node_kind univkind on univkind.id = univ.node_kind_id
where qrykind.code = 'ReportQuery'
and univkind.code = 'Universe'
group by qry.id, qry.name
having count(*) > 1


select 
--distinct 
rap.name as report_name, obj.name as obj_name, univ.name as univ_name, schm.name + '.' + tab.name as schema_table, 
schm.name as schema_name, tab.name as tab_name,
count(*) as usage_cnt
--*
from 
bik_node rap inner join bik_node qry on qry.parent_node_id = rap.id inner join
bik_node objlinked on objlinked.parent_node_id = qry.id inner join
bik_node obj on objlinked.linked_node_id = obj.id inner join bik_node_kind objkind on obj.node_kind_id = objkind.id
inner join bik_joined_objs objjoins on obj.id = objjoins.src_node_id
inner join bik_joined_objs qryjoins on qry.id = qryjoins.src_node_id
inner join bik_node univ on qryjoins.dst_node_id = univ.id
inner join bik_node_kind univkind on univkind.id = univ.node_kind_id
inner join bik_node tab on objjoins.dst_node_id = tab.id inner join bik_node_kind tabkind on tab.node_kind_id = tabkind.id
inner join bik_node schm on tab.parent_node_id = schm.id
where objkind.code in ('Measure', 'Dimension', 'Detail')
and univkind.code = 'Universe'
and objlinked.is_deleted = 0 and rap.is_deleted = 0 and qry.is_deleted = 0 and tab.is_deleted = 0 and schm.is_deleted = 0 and univ.is_deleted = 0
and tabkind.code in ('TeradataView', 'TeradataTable')
group by
rap.name, obj.name, univ.name, schm.name, tab.name
--having count(*) > 1


-- 1974
select bn.id, bn.name, bnk.code as kind_code, bnk.caption as kind_caption, unin.name as universe_name,
    tab.name as viewName
into #repo_ania
from bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id left join
    (bik_joined_objs jo
    inner join bik_node unin on unin.id = jo.dst_node_id and unin.is_deleted = 0
    inner join bik_node_kind unink on unin.node_kind_id = unink.id and unink.code = 'Universe'
    ) on bn.id = jo.src_node_id
    left join (bik_joined_objs jo2
    inner join bik_node tab on tab.id = jo2.dst_node_id and tab.is_deleted = 0
    inner join bik_node_kind tabk on tab.node_kind_id = tabk.id and (tabk.code = 'TeradataTable' or tabk.code ='TeradataView')
    ) on bn.id = jo2.src_node_id
where bn.is_deleted = 0 and bn.linked_node_id is null and
    bnk.code in ('Webi') 
and tab.id is not null



-- 1664
select 
--distinct 
rap.name as report_name, --obj.name as obj_name, 
univ.name as univ_name, schm.name + '.' + tab.name as schema_table, 
schm.name as schema_name, tab.name as tab_name,
count(*) as usage_cnt
--*
into #repo_ww
from 
bik_node rap inner join bik_node qry on qry.parent_node_id = rap.id inner join
bik_node objlinked on objlinked.parent_node_id = qry.id inner join
bik_node obj on objlinked.linked_node_id = obj.id inner join bik_node_kind objkind on obj.node_kind_id = objkind.id
inner join bik_joined_objs objjoins on obj.id = objjoins.src_node_id
inner join bik_joined_objs qryjoins on qry.id = qryjoins.src_node_id
inner join bik_node univ on qryjoins.dst_node_id = univ.id
inner join bik_node_kind univkind on univkind.id = univ.node_kind_id
inner join bik_node tab on objjoins.dst_node_id = tab.id inner join bik_node_kind tabkind on tab.node_kind_id = tabkind.id
inner join bik_node schm on tab.parent_node_id = schm.id
where objkind.code in ('Measure', 'Dimension', 'Detail')
and univkind.code = 'Universe'
and objlinked.is_deleted = 0 and rap.is_deleted = 0 and qry.is_deleted = 0 and tab.is_deleted = 0 and schm.is_deleted = 0 and univ.is_deleted = 0
and tabkind.code in ('TeradataView', 'TeradataTable')
group by
rap.name, --obj.name, 
univ.name, schm.name, tab.name
--having count(*) > 1



select * from #repo_ww
where report_name = 'dwh_plusc_Inter_MVNO_SP_Service_Provider _przychody i koszty'
and tab_name = 'DM_MVNO_DATA'

select * 
from #repo_ania a 
full join #repo_ww w on a.name = w.report_name and 
a.universe_name = w.univ_name and a.viewName = w.tab_name
where 
--w.report_name is null
--and 
--a.universe_name = 'DWH_PlusC_MVNO_wer_5'
--and a.name = 'dwh_plusc_Inter_MVNO_SP_Service_Provider _przychody i koszty'
w.report_name = 'dwh_plusc_Inter_MVNO_SP_Service_Provider _przychody i koszty'
and w.tab_name = 'DM_ICIW_VOICE'




select 
rap.name as report_name, --
obj.name as obj_name, 
univ.name as univ_name, schm.name + '.' + tab.name as schema_table, 
schm.name as schema_name, tab.name as tab_name
--, count(*) as usage_cnt
from 
bik_node rap inner join bik_node qry on qry.parent_node_id = rap.id inner join
bik_node objlinked on objlinked.parent_node_id = qry.id inner join
bik_node obj on objlinked.linked_node_id = obj.id inner join bik_node_kind objkind on obj.node_kind_id = objkind.id
inner join bik_joined_objs objjoins on obj.id = objjoins.src_node_id
inner join bik_joined_objs qryjoins on qry.id = qryjoins.src_node_id
inner join bik_node univ on qryjoins.dst_node_id = univ.id
inner join bik_node_kind univkind on univkind.id = univ.node_kind_id
inner join bik_node tab on objjoins.dst_node_id = tab.id inner join bik_node_kind tabkind on tab.node_kind_id = tabkind.id
inner join bik_node schm on tab.parent_node_id = schm.id
where objkind.code in ('Measure', 'Dimension', 'Detail')
and univkind.code = 'Universe'
and objlinked.is_deleted = 0 and rap.is_deleted = 0 and qry.is_deleted = 0 and tab.is_deleted = 0 and schm.is_deleted = 0 and univ.is_deleted = 0
and tabkind.code in ('TeradataView', 'TeradataTable')
and obj.linked_node_id is null and tab.linked_node_id is null and univ.linked_node_id is null
group by
rap.name, --
obj.name, 
univ.name, schm.name, tab.name
