/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.filesystem;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import simplelib.LameRuntimeException;

/**
 *
 * @author ctran
 */
public class SmbFileDelegator implements IFileDelegator {

    public SmbFile smbFile;

    public SmbFileDelegator(String path, NtlmPasswordAuthentication auth) throws MalformedURLException {
        this(new SmbFile(path, auth));
    }

    public SmbFileDelegator(SmbFile smbFile) {
        this.smbFile = smbFile;
    }

    @Override
    public boolean isDirectory() {
        try {
            return smbFile.isDirectory();
        } catch (SmbException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    @Override
    public Integer getFileCount() {
        try {
            return smbFile.list().length;
        } catch (SmbException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    @Override
    public String getFolderName() {
        return smbFile.getName().substring(0, smbFile.getName().length() - 1);
    }

    @Override
    public IFileDelegator[] listFiles() {
        try {
            return convert2Array(smbFile.listFiles());
        } catch (SmbException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    @Override
    public String getPath() {
        return smbFile.getPath();
    }

    @Override
    public IFileDelegator[] listFiles(String wildcard) {
        try {
            return convert2Array(smbFile.listFiles(wildcard));
        } catch (SmbException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    @Override
    public long lastModified() {
        try {
            return smbFile.lastModified();
        } catch (SmbException ex) {
            throw new LameRuntimeException(ex);

        }
    }

    @Override
    public String getName() {
        return smbFile.getName();
    }

    @Override
    public long length() {
        try {
            return smbFile.length();
        } catch (SmbException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    @Override
    public InputStream getInputStream() {
        try {
            return smbFile.getInputStream();
        } catch (IOException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    private IFileDelegator[] convert2Array(SmbFile[] listFiles) {
        IFileDelegator[] ret = new IFileDelegator[listFiles.length];
        for (int i = 0; i < listFiles.length; i++) {
            ret[i] = new SmbFileDelegator(listFiles[i]);
        }
        return ret;
    }
}
