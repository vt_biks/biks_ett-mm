/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import java.sql.Time;
import java.util.Date;
import pl.trzy0.foxy.serverlogic.IConnectionDialectHelper;
import pl.trzy0.foxy.serverlogic.SqlValueFormatter;
import simplelib.SimpleDateUtils;

/**
 *
 * @author wezyr
 */
public class SqlValueFormatterCurlyTimeStamp extends SqlValueFormatter implements IConnectionDialectHelper {

    @Override
    protected String formatDate(Date d) {
        if (d instanceof java.sql.Date) {
            Date d2 = new Date(d.getTime());
            //return "{ts " + SimpleDateUtils.dateToSingleQuotedCanonicalString(d) + "}";
            d = d2;
        }
        return "{ts " + SimpleDateUtils.toSingleQuotedCanonicalString(d, true) + "}";
    }

    public int readSequenceNextVal(String seqName) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected String formatTime(Time t) {
        return "{t " + SimpleDateUtils.toSingleQuotedCanonicalString(t, true) + "}";
    }
}
