/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author LKI
 */
public enum TokenType {

    REGISRTATION("register", I18n.linkRejestracyjnyWygasl.get()),
    RESET_PASSWORD("reset_pass", ""),
    INVITATION("invite", "");
    public String actionType;
    public String errorMsg;

    TokenType(String string, String errorMsg) {
        this.actionType = string;
        this.errorMsg = errorMsg;
    }
}
