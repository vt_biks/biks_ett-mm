﻿

exec sp_check_version '1.6.y.2';
go

update bik_node set is_deleted=1  where obj_id='Sandbox';

exec sp_add_menu_node @parent_obj_id = '@Dictionary', @name = '@', @obj_id = '$DictionaryDWH';
declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

exec sp_update_version '1.6.y.2', '1.6.y.3';
go