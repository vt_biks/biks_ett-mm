/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

import com.google.gwt.i18n.client.DateTimeFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import pl.fovis.client.dialogs.lisa.LisaDataType;
import simplelib.BaseUtils;

/**
 *
 * @author pku
 */
public class LisaUtils {

    public static String fetchCategoryTypeObjIdAsString(String obj) {
        obj = deleteInstanceId(obj);
        String[] a = obj.split(":");
        return a[1];
    }

    public static Integer fetchCategoryTypeObjIdAsInt(String obj) {
        return BaseUtils.tryParseInteger(fetchCategoryTypeObjIdAsString(obj));
    }

    public static Integer fetchCategoryObjId(String obj) {
        obj = deleteInstanceId(obj);
        String[] a = obj.split(LisaConstants.OBJ_ID_SEPARATOR);
        int lastInd = a.length > 0 ? (a.length - 1) : 0;

        if (LisaConstants.UNASSIGNED_CATEGORY_OBJ_ID_SUFFIX.equals(a[lastInd])) {
            return 0;
        } else {
            return BaseUtils.tryParseInteger(a[lastInd]);
        }
    }

    public static String getTableNameWithoutDB(String fullTableName) {
        fullTableName = deleteInstanceId(fullTableName);
        return BaseUtils.getLastItems(fullTableName != null ? fullTableName : "", ".", 1);
    }

    public static String getDbWithoutNameTable(String fullTableName, String defaultDB) {
        fullTableName = deleteInstanceId(fullTableName);
        if (fullTableName == null || fullTableName.indexOf(".") == -1) {
            return defaultDB;
        }
        return BaseUtils.getFirstItems(fullTableName, ".", 1) + ".";
    }

    public static String getTableNameWithDBFromObjId(String objId) {
        if (objId == null) {
            return null;
        }
        return BaseUtils.getFirstItems(objId, ":", 1);
    }

    public static void createOrUpdateValueForRecord(Map<String, LisaDictionaryValue> record, LisaDictColumnBean columnInfoBean, Object value) {
        switch (LisaDataType.getValueFor(columnInfoBean.dataType)) {
            case DA:
                Date convertedDate = null;
                if (value != null) {
                    if (value instanceof String) {
                        DateTimeFormat df = DateTimeFormat.getFormat("yyyy-MM-dd");
                        convertedDate = df.parse((String) value);
                    } else {
                        convertedDate = (Date) value;
                    }
                }
                record.put(columnInfoBean.nameInDb, convertedDate == null ? new DateLDV(null) : new DateLDV(convertedDate));
                break;
            case I1:
            case I2:
            case I8:
            case I:
                record.put(columnInfoBean.nameInDb, value == null ? new IntegerLDV(null) : new IntegerLDV(BaseUtils.tryParseInt(BaseUtils.safeToString(value))));
                break;
            default:
                record.put(columnInfoBean.nameInDb, value == null ? new StringLDV(null) : new StringLDV((String) value));
        }
    }

    public static List<LisaDictColumnBean> extractPkColumns(List<LisaDictColumnBean> columnsList) {
        List<LisaDictColumnBean> pkColumnsList = new ArrayList<LisaDictColumnBean>();
        for (LisaDictColumnBean column : columnsList) {
            if (column.isMainKey) {
                pkColumnsList.add(column);
            }
        }
        return pkColumnsList;
    }

    public static List<LisaDictColumnBean> extractNonPkColumns(List<LisaDictColumnBean> columnsList) {
        List<LisaDictColumnBean> nonPkColumnsList = new ArrayList<LisaDictColumnBean>();
        for (LisaDictColumnBean column : columnsList) {
            if (!column.isMainKey) {
                nonPkColumnsList.add(column);
            }
        }
        return nonPkColumnsList;
    }

    public static String mergeToString(List<String> list) {
        return BaseUtils.mergeWithSepEx(list, LisaConstants.PK_VALUE_SEPERATOR, new BaseUtils.Projector<String, String>() {
            @Override
            public String project(String val) {
                return BaseUtils.isStrEmptyOrWhiteSpace(val) ? "" : val.trim();
            }
        });
    }

    public static boolean hasDateColumns(List<LisaDictColumnBean> columnList) {
        boolean hasDateFromColumn = false;
        for (LisaDictColumnBean column : columnList) {
            if (column.nameInDb.equals("data_od")) {
                hasDateFromColumn = true;
            }
        }
        return (hasDateFromColumn);
    }

    public static String deleteInstanceId(String obj) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(obj)) {
            return obj;
        }
        if (obj.contains(".") && BaseUtils.tryParseInteger(obj.substring(0, obj.indexOf("."))) != null) {
            return obj.substring(obj.indexOf(".") + 1);
        } else {
            return obj;
        }
    }
}
