/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.client.dialogs.UserDialog;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.UserBean;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
public class NodeActionEditUser extends NodeActionUpdatingBase {

    public String getCaption() {
        return I18n.edytuj.get() /* I18N:  */;
    }

//    @Override
//    protected boolean isEnabledWhenNotLinked() {
//        return node.nodeKindCode.equals("User");
//    }
    public void execute() {
        BIKClientSingletons.getService().bikUserByNodeId(node.id, new StandardAsyncCallback<UserBean>("Failure on" /* I18N: no */ + " bikUserByNodeId") {
            public void onSuccess(final UserBean param) {
                new UserDialog().buildAndShowDialog(node, param,/* userKinds,*/ new IParametrizedContinuation<Pair<Integer, List<String>>>() {
                    public void doIt(Pair<Integer, List<String>> result) {
                        refreshRedisplayDataAndSelectRecord(result.v1/*, false*/);
                        refreshAndRedisplayDataByTreeCode(result.v2);
                    }
                });
            }
        });
    }

    @Override
    public boolean isEnabledByDesign() {
        if (!isAdminOrExpertOrAuthorOrEditor() && !isNodeAuthorOrEditor()) {
            return false;
        }

        return isNodeNotLinkedAnyhowNorReadOnlyNodeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_USER);
    }
}
