exec sp_check_version '1.0.42';

create table bik_admin(
	id int not null IDENTITY(1,1) primary key,
	param varchar(256) not null,
	value varchar(1024) not null,
	constraint uk_bik_admins unique (param)
);
go

insert into bik_admin values('teradataDriverClass', 'com.teradata.jdbc.TeraDriver')
insert into bik_admin values('teradataURL', 'jdbc:teradata://192.168.167.21')
insert into bik_admin values('teradataUser', 'dbc')
insert into bik_admin values('teradataPassword', 'dbc')
insert into bik_admin values('teradataSelect', 'SELECT DatabaseName FROM DBC.DATABASES where DBKind=''D''')


exec sp_update_version '1.0.42', '1.0.43';
go
