if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_delete_bik_joined_objs_by_kinds_fast]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_delete_bik_joined_objs_by_kinds_fast
go

create procedure sp_delete_bik_joined_objs_by_kinds_fast @kinds_one_str varchar(max), @kinds_two_str varchar(max) as
begin
  set nocount on

  declare @timestamp_start_total datetime = current_timestamp
  declare @diags_level int = 1 -- warto�� 0 oznacza brak logowania, 1 i wi�cej - jest logowanie
  declare @rc int
  declare @timestamp_start datetime 
  declare @timestamp_end datetime
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: start' +
      ', @kinds_one_str = ' + @kinds_one_str + 
      ', @kinds_two_str = ' + @kinds_two_str
  end

  -- declare @kinds_one varchar(max) = 'Universe', @kinds_two varchar(max) = 'TeradataSchema'
  declare @codes_one table (code varchar(255) not null primary key)
  declare @codes_two table (code varchar(255) not null primary key)

  insert into @codes_one (code)
  select distinct str
  from dbo.fn_split_by_sep(@kinds_one_str, ',', 7)

  insert into @codes_two (code)
  select distinct str
  from dbo.fn_split_by_sep(@kinds_two_str, ',', 7)

  declare @kind_one_id table (id int not null primary key)
  declare @kind_two_id table (id int not null primary key)

  insert into @kind_one_id (id)
  select distinct k.id
  from @codes_one x inner join bik_node_kind k on x.code = k.code
  where substring(x.code, 1, 1) <> '@'
  
  insert into @kind_two_id (id)
  select distinct k.id
  from @codes_two x inner join bik_node_kind k on x.code = k.code
  where substring(x.code, 1, 1) <> '@'

  declare @tree_one_id table (id int not null primary key)
  declare @tree_two_id table (id int not null primary key)

  delete from @codes_one where substring(code, 1, 1) <> '@'
  delete from @codes_two where substring(code, 1, 1) <> '@'

  update @codes_one set code = substring(code, 2, len(code))
  update @codes_two set code = substring(code, 2, len(code))

  insert into @tree_one_id (id)
  select distinct t.id
  from @codes_one x inner join bik_tree t on x.code = t.code

  insert into @tree_two_id (id)
  select distinct t.id
  from @codes_two x inner join bik_tree t on x.code = t.code

  declare @tree_id_to_kind_id table (tree_id int not null, node_kind_id int not null)
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before insert into @tree_id_to_kind_id'
  end

  set @timestamp_start = current_timestamp
  
  insert into @tree_id_to_kind_id (tree_id, node_kind_id)
  select distinct tree_id, node_kind_id
  from bik_node where is_deleted = 0 and linked_node_id is null

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after insert into @tree_id_to_kind_id, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end

  insert into @kind_one_id (id)
  select t2k.node_kind_id
  from @tree_one_id t inner join @tree_id_to_kind_id t2k on t.id = t2k.tree_id
  left join @kind_one_id k on k.id = t2k.node_kind_id
  where k.id is null

  insert into @kind_two_id (id)
  select t2k.node_kind_id
  from @tree_two_id t inner join @tree_id_to_kind_id t2k on t.id = t2k.tree_id
  left join @kind_two_id k on k.id = t2k.node_kind_id
  where k.id is null

  declare @joined_ids_to_del table (id int not null primary key)

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before insert into @joined_ids_to_del'
  end

  set @timestamp_start = current_timestamp

  insert into @joined_ids_to_del (id)
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.src_node_id = n_one.id
  inner join bik_node n_two on jo.dst_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1
  union -- bez all, bo mog� by� duplikaty, a chcemy je w�a�nie wyeliminowa�
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.dst_node_id = n_one.id
  inner join bik_node n_two on jo.src_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after insert into @joined_ids_to_del, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end
  
  --select count(*) from @joined_ids_to_del

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before final delete'
  end

  set @timestamp_start = current_timestamp
  
  delete from bik_joined_objs where id in (select id from @joined_ids_to_del)
  
  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  declare @timestamp_end_total datetime = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after final delete, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
      + ', TOTAL TIME TAKEN: ' + cast(datediff(ms, @timestamp_start_total, @timestamp_end_total) / 1000.0 as varchar(20)) + ' s'
  end
end
go

/*

exec sp_delete_bik_joined_objs_by_kinds_fast 'TeradataView,TeradataTable,TeradataSchema,Webi,ReportQuery', 'Universe,DataConnection,Measure,Dimension,Detail,Filter,Webi,ReportQuery,UniverseAliasTable,UniverseDerivedTable,UniverseTable'


2012-08-24 16:54:09.845: sp_delete_bik_joined_objs_by_kinds_fast: start, @kinds_one_str = TeradataView,TeradataTable,TeradataSchema,Webi,ReportQuery, @kinds_two_str = Universe,DataConnection,Measure,Dimension,Detail,Filter,Webi,ReportQuery,UniverseAliasTable,UniverseDerivedTable,UniverseTable
2012-08-24 16:54:09.849: sp_delete_bik_joined_objs_by_kinds_fast: before insert into @tree_id_to_kind_id
2012-08-24 16:54:10.079: sp_delete_bik_joined_objs_by_kinds_fast: after insert into @tree_id_to_kind_id, @rc=57, time taken: 0.230000 s
2012-08-24 16:54:10.079: sp_delete_bik_joined_objs_by_kinds_fast: before insert into @joined_ids_to_del
2012-08-24 16:54:16.131: sp_delete_bik_joined_objs_by_kinds_fast: after insert into @joined_ids_to_del, @rc=212483, time taken: 6.053000 s
2012-08-24 16:54:16.131: sp_delete_bik_joined_objs_by_kinds_fast: before final delete
2012-08-24 16:54:22.554: sp_delete_bik_joined_objs_by_kinds_fast: after final delete, @rc=212483, time taken: 6.423000 s, TOTAL TIME TAKEN: 12.710000 s

*/
