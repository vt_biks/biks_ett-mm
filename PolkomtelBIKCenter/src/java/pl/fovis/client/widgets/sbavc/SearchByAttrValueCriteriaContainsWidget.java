/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.widgets.sbavc;

import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import pl.fovis.common.sbavc.SearchByAttrValueCriteriaContains;

/**
 *
 * @author tflorczak
 */
public class SearchByAttrValueCriteriaContainsWidget extends SearchByAttrValueCriteriaWidgetBase {

    protected TextBox containsTbx = new TextBox();

    @Override
    protected void createOptWidget() {
        row.setWidget(0, 4, containsTbx);
    }

    @Override
    public ISearchByAttrValueCriteria provideBean() {
        return (isValidInput() ? new SearchByAttrValueCriteriaContains(getSelectedAttrId(), getSelectedAttrCaption(), containsTbx.getText(), negationChbx.getValue()) : null);
    }
}
