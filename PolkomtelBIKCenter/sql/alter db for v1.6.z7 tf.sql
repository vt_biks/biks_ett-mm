﻿-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-----------------   Poprawki do konektora do Oracle  --------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
exec sp_check_version '1.6.z7';
go

if exists(select 1 from syscolumns sc where id = OBJECT_ID('[aaa_oracle_index]') and name = 'unique_flag')
begin
	delete from aaa_oracle_index;
	alter table aaa_oracle_index drop column unique_flag;
	alter table aaa_oracle_index add is_unique int null;
	alter table aaa_oracle_index add is_primary_key int not null;
	alter table aaa_oracle_index add id int not null identity(1,1) primary key;
end
go

if exists(select * from sys.objects where object_id = object_id(N'aaa_oracle') and type in (N'U'))
begin
	drop table aaa_oracle;
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_oracle_definition') and type in (N'U'))
begin
	create table aaa_oracle_definition (
		id int not null identity(1,1) primary key,
		server varchar(500) null,
		owner varchar(500) not null,
		name varchar(500) not null,
		definition varchar(max) not null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_oracle_column_extradata') and type in (N'U'))
begin
	create table aaa_oracle_column_extradata (
		id int not null identity(1,1) primary key,
		name varchar(500) not null,
		owner varchar(500) not null,
		table_name varchar(500) not null,
		server_name varchar(500) not null,
		column_id int not null,
		description varchar(max) null,
		is_nullable int not null,
		type varchar(100) not null,
		default_field varchar(512) null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_oracle_foreign_key') and type in (N'U'))
begin
	create table aaa_oracle_foreign_key (
		id int not null identity(1,1) primary key,
		name varchar(500) not null,
		description varchar(max) null,
		server varchar(500) null,
		fk_table varchar(500) not null,
		fk_schema varchar(500) not null,
		fk_column varchar(500) not null,
		pk_table varchar(500) not null,
		pk_schema varchar(500) not null,
		pk_column varchar(500) not null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_oracle_dependency') and type in (N'U'))
begin
	create table aaa_oracle_dependency (
		id int not null identity(1,1) primary key,
		server varchar(500) null,
		ref_name varchar(500) not null,
		ref_owner varchar(500) not null,
		dep_name varchar(500) not null,
		dep_owner varchar(500) not null
	);
end;
go

update bik_node_kind set code = 'OracleColumnFK', caption = 'Kolumna Oracle FK' where code = 'OracleColumnIDX'
update bik_translation set code = 'OracleColumnFK', txt = 'Oracle FK column' where code = 'OracleColumnIDX' and kind = 'kind' and lang = 'en'

declare @treeOfTreesId int = (select id from bik_tree where code = 'TreeOfTrees');
update bik_node set obj_id = '&OracleColumnFK' where tree_id = @treeOfTreesId and obj_id = '&OracleColumnIDX'

if not exists (select 1 from bik_node_kind where code = 'OracleFolder')
begin
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic, children_kinds)
	values ('OracleFolder', 'Folder Oracle', 'oracleFolder', 'Metadata', 0, 0, 0, 'OracleTable|OracleView|OracleProcedure|OracleFunction');
	
	insert into bik_translation(code, txt, lang, kind)
	values  ('OracleFolder', 'Oracle folder', 'en', 'kind');
	
	insert into bik_icon(name)
	values  ('OracleFolder')
end;
go

update bik_node_kind set children_kinds = 'OracleOwner' where code = 'OracleServer'
update bik_node_kind set children_kinds = 'OracleFolder' where code = 'OracleOwner'

if exists(select * from sys.objects where object_id = object_id(N'bik_oracle_extradata') and type in (N'U'))
begin
	drop table bik_oracle_extradata;
end;
go

if exists(select * from sys.objects where object_id = object_id(N'bik_oracle_index') and type in (N'U'))
begin
	drop table bik_oracle_index;
end;
go

alter table bik_db_column_extradata alter column database_name varchar(512) null;
go

alter table bik_db_definition alter column database_name varchar(512) null;
go

alter table bik_db_dependency alter column database_name varchar(512) null;
go

alter table bik_db_foreign_key alter column database_name varchar(512) null;
go

alter table bik_db_index alter column database_name varchar(512) null;
go

alter table bik_db_index alter column type varchar(512) null;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_db_column_extradata]') and name = N'idx_bik_db_column_extradata_table_node_id_column_node_id_column_id')
begin
	create index idx_bik_db_column_extradata_table_node_id_column_node_id_column_id on bik_db_column_extradata (table_node_id, column_node_id, column_id);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_db_column_extradata]') and name = N'idx_bik_db_column_extradata_column_node_id_table_node_id_column_id')
begin
	create index idx_bik_db_column_extradata_column_node_id_table_node_id_column_id on bik_db_column_extradata (column_node_id, table_node_id, column_id);
end;
go

--
if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_db_index]') and name = N'idx_bik_db_column_extradata_table_node_id_column_node_id')
begin
	create index idx_bik_db_column_extradata_table_node_id_column_node_id on bik_db_index (table_node_id, column_node_id);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_db_index]') and name = N'idx_bik_db_column_extradata_column_node_id_table_node_id')
begin
	create index idx_bik_db_column_extradata_column_node_id_table_node_id on bik_db_index (column_node_id, table_node_id);
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_db_index]') and name = 'descend')
begin
  alter table bik_db_index add descend varchar(50) null;
end
go

if not exists(select 1 from bik_node_kind where code = 'OracleFunction')
begin
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('OracleFunction', 'Funkcja Oracle', 'oracleFunction', 'Metadata', 0, 8, 0);
	
	exec sp_add_menu_node '&OracleOwner', '@', '&OracleFunction';
	
	insert into bik_translation(code, txt, lang, kind)
	values  ('OracleFunction', 'Oracle function', 'en', 'kind');
	
	insert into bik_icon(name)
	values ('oracleFunction');
end;

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

-- poprzednia wersja w pliku: alter db for v1.6.z6.7 tf.sql
-- dodanie update typu węzła
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_generic_insert_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_generic_insert_into_bik_node]
go

create procedure [dbo].[sp_generic_insert_into_bik_node](@treeCode varchar(max), @objId varchar(900))
as
begin
	-- założenia:
	-- branch_ids są uzupełnione w bik_node i amg_node (@objId is not null)
	-- jeśli @objId is not null wtedy musi istniec ten obiekt w bik_node (inny mechanizm go wstawil wczesniej)
	-- 
	declare @treeId int = (select id from bik_tree where code = @treeCode);
	declare @parentNodeId int = (select id from bik_node where tree_id = @treeId and obj_id = @objId);
	declare @branchIds varchar(900) = (select branch_ids from bik_node where tree_id = @treeId and obj_id = @objId);
	declare @amgBranchIds varchar(900) = (select branch_ids from amg_node where obj_id = @objId);

	-- update
	update bik_node 
	set is_deleted = 0, descr = gen.descr, visual_order = gen.visual_order, node_kind_id = bnk.id
	from amg_node as gen
	inner join bik_node_kind as bnk on gen.node_kind_code = bnk.code
	where bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	and bik_node.obj_id = gen.obj_id
	and bik_node.linked_node_id is null

	-- insert nowych
	insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id, visual_order)
	select @parentNodeId, bnk.id, gen.name, gen.descr, @treeId, gen.obj_id, gen.visual_order
	from amg_node as gen
	inner join bik_node_kind as bnk on gen.node_kind_code = bnk.code
	left join (bik_node as bn inner join bik_tree as bt on bn.tree_id = bt.id and bn.tree_id = @treeId and (@branchIds is null or bn.branch_ids like @branchIds + '%')) on bn.obj_id = gen.obj_id
	where bn.id is null
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	order by gen.obj_id
	
	exec sp_node_init_branch_id @treeId, @parentNodeId;

	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join amg_node as gen on bik_node.obj_id = gen.obj_id
	inner join bik_node as bk on bk.obj_id = gen.parent_obj_id
	where bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.tree_id = @treeId
	and bk.is_deleted = 0
	and bk.linked_node_id is null

	-- usunięcie zbędnych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join amg_node as gen on bik_node.obj_id = gen.obj_id 
		and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	where gen.name is null
	and bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node as bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @treeId
	and (@branchIds is null or bn2.branch_ids like @branchIds + '%')
	and bn2.is_deleted = 1

	exec sp_node_init_branch_id @treeId, null
end;
go


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_fix_databases_nodes_after_deactivate]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_fix_databases_nodes_after_deactivate]
go

create procedure [dbo].[sp_fix_databases_nodes_after_deactivate](@dbServerId int)
as
begin
	declare @servName varchar(255) = (select name from bik_db_server where id = @dbServerId);
	declare @sourceId int = (select source_id from bik_db_server where id = @dbServerId);
	declare @treeId int = (select tree_id from bik_db_server where id = @dbServerId);
	declare @activeDBs varchar(255) = '';
	declare @branchsToDelete table (branch varchar(900));

	if(charindex('#', @servName) <> 0)
	begin
		set @servName = substring(@servName, 1, charindex('#', @servName) - 1);
	end;

	select @activeDBs = @activeDBs + ',' + coalesce(database_list,'') 
	from bik_db_server as ser
	where source_id = @sourceId and is_active = 1 
	and tree_id = @treeId and (name = @servName or name like @servName + '#%')

	insert into @branchsToDelete(branch)
	select branch_ids 
	from bik_node 
	where parent_node_id in (
		select id from bik_node 
		where tree_id = @treeId 
		and is_deleted = 0 
		and obj_id = @servName + '|')
	and is_deleted = 0
	and name not in (select str 
		from (select * from dbo.fn_split_by_sep(@activeDBs, ',',3 )) x 
		where str <> '')
		
		
	declare @branch varchar(900);	
	declare db_del_cur cursor for select branch from @branchsToDelete
	open db_del_cur
	fetch next from db_del_cur into @branch
	while @@fetch_status = 0
		begin 
		update bik_node 
		set is_deleted = 1
		where tree_id = @treeId
		and branch_ids like @branch + '%'
		fetch next from db_del_cur into @branch
		end
	close db_del_cur;
	deallocate db_del_cur;
end;
go

exec sp_update_version '1.6.z7', '1.6.z7.1';
go
