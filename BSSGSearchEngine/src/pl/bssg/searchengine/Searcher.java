/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.searchengine;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.TotalHitCountCollector;
import org.apache.lucene.store.FSDirectory;

/**
 *
 * @author ctran
 */
public class Searcher {

    public static final Integer MAX_RETURNED_RESULT = 10;
    protected final String indexDirectory;
    protected final Analyzer analyzer;
    protected IndexSearcher indexSearcher;

    public Searcher(String indexDirectory, Analyzer analyzer) {
        this.indexDirectory = indexDirectory;
        this.analyzer = analyzer;
    }

    public List<Document> search(Query query) throws IOException, ParseException {
        List<Document> res = new ArrayList<Document>();
        DirectoryReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(indexDirectory)));

        IndexSearcher searcher = new IndexSearcher(reader);

        int startIndex = 0;

        Integer total = countResult(query);
        if (total > 0) {
            TopScoreDocCollector collector = TopScoreDocCollector.create(startIndex + total);
            searcher.search(query, collector);

            TopDocs topDocs = collector.topDocs(startIndex, total);

            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                Document doc = searcher.doc(scoreDoc.doc);
                res.add(doc);
            }
        }
        return res;
    }

    public Integer countResult(Query query) throws IOException, ParseException {
        DirectoryReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(indexDirectory)));

        IndexSearcher searcher = new IndexSearcher(reader);
        TotalHitCountCollector collector = new TotalHitCountCollector();
        searcher.search(query, collector);

        return collector.getTotalHits();
    }
}
