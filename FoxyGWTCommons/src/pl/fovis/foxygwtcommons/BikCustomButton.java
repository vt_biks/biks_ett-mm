/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasEnabled;
import simplelib.IContinuation;

/**
 *
 * @author mgraczkowski
 */
public class BikCustomButton extends HTML implements HasEnabled {

    public static final String DISABLED_STYLE_NAME = "BikButton-disabled";
    private boolean isEnabled = true;

    public BikCustomButton(String text, String style, final IContinuation click) {
        super(text);
        super.setStyleName(style);

        if (click != null) {
            addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    if (isEnabled) {
                        click.doIt();
                    }
                }
            });
        }
    }

    public final void setEnabled(boolean val) {
        this.isEnabled = val;

        if (isEnabled) {
            removeStyleName(DISABLED_STYLE_NAME);
        } else {
            addStyleName(DISABLED_STYLE_NAME);
        }
    }

    public boolean isEnabled() {
        return isEnabled;
    }
}
