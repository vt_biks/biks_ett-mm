/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.postgresql;

/**
 *
 * @author tflorczak
 */
public class PostgresDefinitionBean {

    public String server;
    public String databaseName;
    public String owner;
    public String name;
    public String definition;

    public PostgresDefinitionBean(String server, String databaseName, String owner, String name, String definition) {
        this.server = server;
        this.databaseName = databaseName;
        this.owner = owner;
        this.name = name;
        this.definition = definition;
    }
}
