/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import java.util.Date;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class ScheduleWidgetBean /*implements Serializable*/ {

    private static final DateTimeFormat dateFormat = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_SHORT);
    private static final DateBox.DefaultFormat DEFAULT_FORMAT = new DateBox.DefaultFormat(dateFormat);

    public int id;
    public String name;
    public String instance;
    public TextBox time;
//    public ListBox interval;
    public TextBox interval;
    private DateBox startTime;
    public CheckBox isSchedule;
    protected Grid grid;
    protected int startTimeRow;
    protected Integer startTimeCol;
    public TextBox startTimeFake;
    private Date startTimeDate;

    public ScheduleWidgetBean(Grid grid, int startTimeRow, Integer startTimeCol) {
        this.grid = grid;
        this.startTimeRow = startTimeRow;
        this.startTimeCol = startTimeCol;
    }

    private DateBox getStartTime() {
        if (startTime == null) {
            DatePicker dp = new DatePicker();
            startTime = new DateBox(dp, startTimeDate, DEFAULT_FORMAT);
            startTime.setWidth("100px");
            if (startTimeCol != null) {
                grid.setWidget(startTimeRow, startTimeCol, startTime);
            }
        }
        return startTime;
    }

    public void setStartTimeEnabled(boolean val) {
        if (val || startTime != null) {
            getStartTime().setEnabled(val);
            startTimeFake = null;
        } else {
            startTimeFake.setEnabled(val);
        }
    }

    public void setStartTimeValue(Date d) {
        startTimeDate = d;
        if (startTime != null) {
            startTime.setValue(d);
        } else {
            startTimeFake.setValue(SimpleDateUtils.dateToString(d, "-"));
        }
    }

    public Date getStartTimeValue() {
        return startTime != null ? startTime.getValue() : startTimeDate;
    }
}
