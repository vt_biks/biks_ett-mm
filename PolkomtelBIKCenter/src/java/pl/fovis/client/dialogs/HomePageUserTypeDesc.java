/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.*;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author mmastalerz
 */
public class HomePageUserTypeDesc extends BaseActionOrCancelDialog {

    protected String text;

    @Override
    protected String getDialogCaption() {
        return I18n.opisUprawnienUzytkownika.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return null;
    }

    @Override
    protected String getCancelButtonCaption() {
        return I18n.zamknij.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        HorizontalPanel caption = new HorizontalPanel();
        caption.add(new HTML(text));
        caption.setStyleName("HomePage-bizDef");
        main.add(caption);
    }

    @Override
    protected void doAction() {
        //no-op
    }

    public void buildAndShowDialog(String text) {
        this.text = text;
        super.buildAndShowDialog();
    }
}
