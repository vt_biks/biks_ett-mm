/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.mltx;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.mltx.history.HistoryAction;
import pl.fovis.client.mltx.history.MultiXHistoryToken;
import pl.fovis.client.mltx.history.MultiXUrlCreator;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.GWTUtils;
import simplelib.IContinuation;

/**
 *
 * @author LKI
 */
public class MultixResePasswordSendPage extends SimplePanel {

    protected VerticalPanel vp;
    protected FlexTable ft;
    protected TextBox tbEmail;
    protected Label lblEmail;
    protected Label info;
    private PushButton confirmBtn;
    private Label infoEmailSend;

    public static HistoryAction getHistoryAction() {
        return new HistoryAction() {
            public Widget performAction(String id) {
                return new MultixResePasswordSendPage();
            }
        };
    }

    public MultixResePasswordSendPage() {
        buildWidgets();
    }

    private void buildWidgets() {
        vp = new VerticalPanel();
        info = new Label();
        info.setStyleName("NewLoginLbl");
        displayInfo(I18n.resetingPassword.get());
        vp.add(info);
        vp.add(buildRessetFt());
        FlexTable MltxBackground = NewLookUtils.mltxBackground(vp);
        this.add(MltxBackground);
    }

    protected FlexTable buildRessetFt() {
        ft = new FlexTable();
        ft.setCellSpacing(2);
        HorizontalPanel emailPanel = new HorizontalPanel();
        emailPanel.setWidth("100%");
        emailPanel.add(lblEmail = new Label(I18n.enterEmail.get()));
        emailPanel.add(tbEmail = new TextBox());
        ft.setWidget(0, 0, emailPanel);
        infoEmailSend = new Label(I18n.naPodanyPrzezCiebieAdresReset.get());
        infoEmailSend.setStyleName("miniInfoText");
        tbEmail.setStyleName("userDialogInput");
        tbEmail.setWidth("258px");
        lblEmail.setStyleName("userDialogLbl");
        addEnterKeyHandler(tbEmail);

        confirmBtn = new PushButton(I18n.reset.get());
        NewLookUtils.makeCustomPushButton(confirmBtn);
        confirmBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                doAction();
            }
        });

        HorizontalPanel fpBtns = new HorizontalPanel();
        fpBtns.setWidth("100%");
        fpBtns.add(confirmBtn);
        AnchorAsHyperlink loginHL = new AnchorAsHyperlink(I18n.logowanie.get(), MultiXHistoryToken.LOGIN.hname);
        fpBtns.add(loginHL);
        fpBtns.setCellHorizontalAlignment(loginHL, HasHorizontalAlignment.ALIGN_RIGHT);
        ft.setWidget(1, 0, fpBtns);
        ft.setWidget(2, 0, infoEmailSend);


        confirmBtn.setStyleName("gwt-PushButton");

        return ft;
    }

    protected void addEnterKeyHandler(TextBox tb) {
        GWTUtils.addEnterKeyHandler(tb, new IContinuation() {
            @Override
            public void doIt() {
                doAction();
            }
        });
    }

    private void doAction() {
        ft.clear();
        displayInfo(I18n.sendingResetEmail.get());
        String propUrl = MultiXUrlCreator.createUrl();
        BIKClientSingletons.getMultiXService().sendResetPassword(tbEmail.getText(), propUrl, new StandardAsyncCallback<String>() {
            public void onSuccess(String result) {
                createPageAfterReset(result != null ? result : I18n.linkZostalWyslany.get());
            }
        });
    }

    private void createPageAfterReset(String info) {
        displayInfo(info);
        AnchorAsHyperlink link = new AnchorAsHyperlink(I18n.logowanie.get(), MultiXHistoryToken.LOGIN.hname);
        vp.add(link);
    }

    private void displayInfo(String txt) {
        info.setText(txt);
    }
}
