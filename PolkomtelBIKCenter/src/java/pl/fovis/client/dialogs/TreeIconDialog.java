/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class TreeIconDialog extends BaseActionOrCancelDialog {

    protected IParametrizedContinuation<List<NodeKindBean>> con;
    protected List<NodeKindBean> nodeKinds;
    protected Map<Integer, Pair<String, PushButton>> nodeKindIdToIcon;

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzIkonyDlaDrzewa.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {
        FlexTable grid = new FlexTable();
        nodeKindIdToIcon = new HashMap<Integer, Pair<String, PushButton>>();
        grid.setCellSpacing(4);
        populateGrid(grid, nodeKinds);
        main.add(grid);
    }

    @Override
    protected void doAction() {
        for (NodeKindBean nkb : nodeKinds) {
            nkb.iconName = nodeKindIdToIcon.get(nkb.id).v1;
        }
        con.doIt(nodeKinds);
    }

    public void buildAndShowDialog(List<NodeKindBean> nodeKinds, IParametrizedContinuation<List<NodeKindBean>> con) {
        this.nodeKinds = nodeKinds;
        this.con = con;
        super.buildAndShowDialog();
    }

    protected void populateGrid(FlexTable grid, List<NodeKindBean> nodeKinds) {
        int i = 0;
        for (NodeKindBean nodeKindBean : nodeKinds) {
            final String iconName = nodeKindBean.iconName;
            final int nodeKindId = nodeKindBean.id;
            Image img = new Image(BIKGWTConstants.ICON_DIR_PREFIX + iconName + BIKGWTConstants.ICON_DIR_POSTFIX);
            PushButton btn = new PushButton(img, new ClickHandler() {

                public void onClick(ClickEvent event) {
                    callServiceToGetData(new StandardAsyncCallback<Set<String>>("Error in" /* I18N: no */ + " getIcons") {

                                public void onSuccess(Set<String> result) {
                                    new PickIconDialog().buildAndShowDialog(iconName, result, new IParametrizedContinuation<String>() {

                                        public void doIt(String param) {
                                            Pair<String, PushButton> res = nodeKindIdToIcon.get(nodeKindId);
                                            res.v1 = param;
                                            res.v2.getUpFace().setImage(new Image(BIKGWTConstants.ICON_DIR_PREFIX + param + BIKGWTConstants.ICON_DIR_POSTFIX));
                                        }
                                    });
                                }
                            });
                }
            });
            btn.setStyleName("followBtn");
            btn.setTitle(I18n.zmienIkone.get() /* I18N:  */);
            nodeKindIdToIcon.put(nodeKindBean.id, new Pair<String, PushButton>(iconName, btn));
            // do grida
            grid.setWidget(i, 0, new Label(nodeKindBean.caption));
            grid.setWidget(i, 1, btn);
            grid.setWidget(i++, 2, new Label(nodeKindBean.code));
        }
    }

    protected Request callServiceToGetData(AsyncCallback<Set<String>> callback) {
        return BIKClientSingletons.getService().getIcons(callback);
    }
}
