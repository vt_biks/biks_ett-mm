/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public interface VersionInfoServiceAsync {

    public void getAppVersion(AsyncCallback<Pair<String, String>> asyncCallback);
}
