/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.FlexTable;
import java.util.List;
import simplelib.IContinuation;

/**
 *
 * @author mmastalerz
 */
public interface IGridGenericBeanPropsExtractor<T> {

    public void setDataChangedHandler(IContinuation handler);

    public String getNameAsHtml(T b);

    public boolean hasIcon();

    public String getIconUrl(T b);

    public String getOptIconHint(T b);

    public int getAddColCnt();

    public Object getAddColVal(T b, int addColIdx);

    public int getActionCnt();

    public IGridBeanAction<T> getActionByIdx(int idx);

    public boolean createGridColumnNames(FlexTable gp);

    public void setAllBeansList(List<T> allBeans);

    public String getOptAddColTitle(T b, int addColIdx);
    
    public String getTreeCode(T b);
}
