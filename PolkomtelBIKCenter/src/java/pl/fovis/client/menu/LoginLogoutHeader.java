/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.menu;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.IMyBIKSPage;
import pl.fovis.client.dialogs.LoginDialog;
import pl.fovis.client.mltx.history.MultiXHistoryToken;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.mltx.DatabaseListBoxData;
import pl.fovis.common.mltx.DatabaseListboxElem;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.i18nsupport.SupportedLocaleNameEnumerator;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pku
 */
public class LoginLogoutHeader {

    protected static Map<String, Anchor> otherLangAnchors = new HashMap<String, Anchor>();

    public static void refreshOtherLangAnchors() {
        if (SupportedLocaleNameEnumerator.isMultiLanguage()) {

            UrlBuilder urlBuilder = Window.Location.createUrlBuilder();

            for (Entry<String, Anchor> e : otherLangAnchors.entrySet()) {
                String localeName = e.getKey();
                Anchor a = e.getValue();

                a.setHref(SupportedLocaleNameEnumerator.getUrlForLocale(urlBuilder, localeName));
            }
        }
    }

    public static void reRender(final boolean isMulti) {
        final RootPanel loginLogoutPanel = RootPanel.get("loginButton");

        boolean isLoggedIn = BIKClientSingletons.isUserLoggedIn();
//        if (loginInfoForLoggedIn != null && isLoggedIn == loginInfoForLoggedIn) {
//            return;
//        }

        loginLogoutPanel.clear();
        otherLangAnchors.clear();

        if (SupportedLocaleNameEnumerator.isMultiLanguage()) {
            SupportedLocaleNameEnumerator.enumerate(SupportedLocaleNameEnumerator.EnumerationMode.CurrentAsLast, new IParametrizedContinuation<SupportedLocaleNameEnumerator>() {
                @Override
                public void doIt(final SupportedLocaleNameEnumerator param) {
                    boolean isCurrent = param.isCurrent();
                    String localeName = param.getLocaleName();
                    Anchor anc = null;
                    Widget w = isCurrent ? new InlineLabel(localeName)
                            : (anc = new Anchor(localeName, param.getUrlForLocale()));

                    if (anc != null) {
                        otherLangAnchors.put(localeName, anc);
                    }
                    w.setStyleName("localeLangSelector");
                    w.setStyleName((isCurrent ? "" : "in" /* I18N: no */) + "active" /*i18n: no*/, true);
                    w.setStyleName("localeInfo_" + localeName, true);
                    w.setTitle(isCurrent ? I18n.aktualnieWybranyJezyk.get() /* I18N:  */ : I18n.zmienJezyk.get() /* I18N:  */);
                    loginLogoutPanel.add(w);
                }
            });

            refreshOtherLangAnchors();
        }

        if (isLoggedIn) {
            if (isMulti && BIKClientSingletons.isUserLoggedIn()) {
                FlowPanel container = new FlowPanel();
                addDbChooseListIfNeeded(container);
                loginLogoutPanel.add(container);

            }
            loginLogoutPanel.add(new InlineHTML(I18n.witaj.get() /* I18N:  */ + ", "));//<a href=\"#myBIK\"><b>" + BIKClientSingletons.getLoggedUserName() + "</b></a>"));
            Anchor userNameAnc = new Anchor(BIKClientSingletons.getLoggedUserName());
            userNameAnc.setStyleName("welcomeUserLogin");
            userNameAnc.addClickHandler(BIKClientSingletons.createClickHandlerForShowMyBIKSTab(IMyBIKSPage.ReachableMyBIKSTabCode.EditUser));
            loginLogoutPanel.add(userNameAnc);
            BikCustomButton logoutBtn = new BikCustomButton(I18n.wyloguj.get() /* I18N:  */, "labelLink",
                    new IContinuation() {
                        public void doIt() {
                            BIKClientSingletons.performUserLogout(/*BIKClientSingletons.performOnLoginLogoutCont*/);
                        }
                    });
            //logoutBtn.addStyleName("labelLink");
            loginLogoutPanel.add(logoutBtn);
        } else {
            if (!isMulti && !BIKClientSingletons.isDisableGuestMode()) {
                loginLogoutPanel.add(new InlineHTML(I18n.witajNieznajomy.get() /* I18N:  */));
            }
            BikCustomButton loginBtn = new BikCustomButton(I18n.zaloguj.get() /* I18N:  */, "labelLink",
                    new IContinuation() {
                        public void doIt() {
                            showLoginForm(isMulti);
                        }
                    });
            //loginBtn.addStyleName("labelLink");
            loginLogoutPanel.add(loginBtn);
        }
//        loginInfoForLoggedIn = isLoggedIn;
    }

    private static void showLoginForm(boolean isMulti) {
        if (isMulti) {
            History.newItem(MultiXHistoryToken.LOGIN.hname);
        } else if (BIKClientSingletons.isDisableGuestMode()) {
            // DO NOTHING - login form is already visable
            return;
        } else {
            new LoginDialog().buildAndShowDialog(/*BIKClientSingletons.performOnLoginLogoutCont*/);
        }
    }

    private static void addDbChooseListIfNeeded(final Panel container) {
        BIKClientSingletons.getMultiXService().getAvailableDatabases(new StandardAsyncCallback<DatabaseListBoxData>() {
            public void onSuccess(DatabaseListBoxData result) {
                container.setStyleName("inline");
                if (result.elems.size() < 2) {
                    return;
                }
                Label label = new Label(I18n.changeContext.get());
                label.setStyleName("inline");
                final ListBox lb = new ListBox();
                lb.addStyleName("contextLb");
                for (DatabaseListboxElem el : result.elems) {
                    lb.addItem(el.description, el.id);
                }
                lb.setSelectedIndex(result.selectedIndex);
                lb.addChangeHandler(new ChangeHandler() {
                    public void onChange(ChangeEvent event) {
                        String listBoxValue = lb.getValue(lb.getSelectedIndex());
//                        String newUrl = UrlParser.addParamToUrl(Window.Location.getHref(), "dbName", listBoxValue);
                        String newUrl = BIKClientSingletons.makeUrlToDatabase(listBoxValue, true);
                        Window.Location.replace(newUrl);

                    }
                });

                container.add(label);
                container.add(lb);
            }
        });
    }
}
