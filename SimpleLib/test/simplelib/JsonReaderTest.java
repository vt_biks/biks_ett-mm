/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wezyr
 */
public class JsonReaderTest {

    public JsonReaderTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testReadMap1() {
        System.out.println("readMap");
        String jsonString = "{x:20,'z':'aqq'}";
        Map<String, Object> result = JsonReader.readMap(jsonString);
        assertEquals(2, result.size());
    }

    @Test
    public void testReadMap2() {
        System.out.println("readMap");
        String jsonString = "{x:20.1,'z':'a\"qq'}";
        Map<String, Object> result = JsonReader.readMap(jsonString);
        assertEquals(2, result.size());
    }

    @Test
    public void testReadMap3() {
        System.out.println("readMap");
        String jsonString = "{x:20.1,'z':{\"u\":'a\"qq', \"null\":null}}";
        Map<String, Object> result = JsonReader.readMap(jsonString);
        assertEquals(2, result.size());
    }

    @Test
    public void testReadObject1() {
        System.out.println("readObject");
        String jsonString = "{x:20.1,'z':{\"u\":'a\"qq'}}";
        Map<String, Object> result = (Map<String, Object>)JsonReader.readObject(jsonString);
        assertEquals(2, result.size());
    }

    @Test
    public void testReadObject2() {
        System.out.println("readObject");
        String jsonString = "[{x:20.1,'z':{\"u\":'a\"qq', z:[], x: { } }}]";
        List<Object> result = (List<Object>)JsonReader.readObject(jsonString);
        assertEquals(1, result.size());
        Map<String, Object> map = (Map<String, Object>)result.get(0);
        assertEquals(2, map.size());
    }

    @Test
    public void testReadBoolean() {
        System.out.println("readBoolean");
        String jsonString = "[{x:true,'z':{\"u\":null, z:[], x: false }}]";
        List<Object> result = (List<Object>)JsonReader.readObject(jsonString);
        assertEquals(1, result.size());
        Map<String, Object> map = (Map<String, Object>)result.get(0);
        assertEquals(2, map.size());
        assertEquals(true, map.get("x"));
        Map<String, Object> innerMap = (Map)map.get("z");
        assertEquals(null, innerMap.get("u"));
        assertEquals(false, innerMap.get("x"));
    }

    @Test
    public void testReadDate() {
        System.out.println("readDate");
        String jsonString = "new Date(2005,11, 15)";
        Date result = (Date)JsonReader.readObject(jsonString);
        assertEquals(SimpleDateUtils.newDate(2005, 11, 15), result);
    }

    @Test
    public void testReadDate2() {
        System.out.println("readDate");
        String jsonString = "new Date(2005,11, 15, 10, 30)";
        Date result = (Date)JsonReader.readObject(jsonString);
        assertEquals(SimpleDateUtils.newDate(2005, 11, 15, 10, 30, 0), result);
    }

    @Test
    public void testReadDate3() {
        System.out.println("readDate");
        String jsonString = "new Date(2009,11,15, 10, 30, 0, 555)";
        Date result = (Date)JsonReader.readObject(jsonString);
        assertEquals(SimpleDateUtils.newDateWithMillis(2009, 11, 15, 10, 30, 0, 555), result);
    }

    @Test
    public void testReadDate4() {
        System.out.println("readDate");
        String jsonString = "{x: new Date(2009,11,15, 10, 30, 0, 555), y:[new Date(2009,1,1), new Date(1999,12,31, 13,55)]}";
        Date result = (Date)JsonReader.readMap(jsonString).get("x");
        assertEquals(SimpleDateUtils.newDateWithMillis(2009, 11, 15, 10, 30, 0, 555), result);
    }
}