/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.util.Collection;
import java.util.Map;
import pl.fovis.common.SearchFullResult;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;

/**
 *
 * @author wezyr
 */
public interface IBikFullTextSearch {

    //public void addNode(int nodeId);
    //public void updateNode(int nodeId);
    public boolean updateNodeAttributes(int nodeId, Map<String, String> attrVals);

    public void deleteNodes(Collection<Integer> nodeIds);

    //public void deleteNode(int nodeId);
    public void deleteTree(String treeCode);

    // offset liczony od 0
    public SearchFullResult search(String text, int offset, int limit, Collection<Integer> optTreeIds,
            Collection<Integer> optKindIdsToSearch, Collection<Integer> optKindIdsToExclude,
            boolean calcKindCounts, Collection<ISearchByAttrValueCriteria> optSBAVCs, String opEpr,
            boolean useLucene);

    //public void indexStart();
    public void updateTree(int treeId);

    public int unindexedPendingCount();

//    public void setSearchWizard(ISimilarNodesWizard wizard);
}
