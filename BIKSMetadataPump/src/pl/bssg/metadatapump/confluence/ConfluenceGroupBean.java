/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence;

/**
 *
 * @author tflorczak
 */
public class ConfluenceGroupBean {

    public String id;
    public String kind;
    public Integer groupId;
    public Integer treeId;

    public ConfluenceGroupBean() {
    }

    public ConfluenceGroupBean(String id, String kind, Integer groupId) {
        this.id = id;
        this.kind = kind;
        this.groupId = groupId;
    }
}
