﻿exec sp_check_version '1.1.1.3';
go
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

update bik_system_user
set bik_system_user.user_id = null
from bik_system_user join bik_user on bik_system_user.user_id = bik_user.id
join bik_node on bik_user.node_id = bik_node.id
where bik_node.is_deleted = 1
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
exec sp_update_version '1.1.1.3', '1.1.1.4';
go