/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

/**
 *
 * @author wezyr
 */
@SuppressWarnings("deprecation")
public class OrderedListNew extends GenericHtmlElementContainerWidget {

    public static OrderedListNew wrap(com.google.gwt.user.client.Element ele) {
        return wrap(ele, new OrderedListNew(null));
    }

    public OrderedListNew(com.google.gwt.user.client.Element ele) {
        super(ele);
    }

    public OrderedListNew() {
        super();
    }

    @Override
    protected String getElementName() {
        return "OL" /* I18N: no */;
    }
}
