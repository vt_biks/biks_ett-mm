package pl.fovis.foxygwtcommons.richtexteditor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptException;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.TableCellElement;
import com.google.gwt.dom.client.TableElement;
import com.google.gwt.dom.client.TableRowElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.i18n.client.Constants;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.ButtonBase;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.RichTextArea.Formatter;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pl.fovis.foxygwtcommons.AddTableDialog;
import pl.fovis.foxygwtcommons.FileUploadDialogBase;
import pl.fovis.foxygwtcommons.FileUploadDialogForPhoto;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.FoxyFileUpload;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.InitFactories4GWT;
import pl.fovis.foxygwtcommons.TableColWidth;
import pl.fovis.foxygwtcommons.i18n.I18n;
import pl.fovis.foxygwtcommons.popupmenu.PopupMenuDisplayer;
import pl.fovis.foxygwtcommons.selection.Range;
import pl.fovis.foxygwtcommons.selection.RangeEndPoint;
import pl.fovis.foxygwtcommons.selection.Selection;
import pl.trzy0.foxy.commons.FoxyCommonUtils;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 * A sample toolbar for use with {@link RichTextArea}. It provides a simple UI
 * for all rich text formatting, dynamically displayed only for the available
 * functionality.
 */
@SuppressWarnings("deprecation")
public class RichTextAreaFoxyToolbar extends Composite {

    static {
        InitFactories4GWT.initFactories();
    }

    /**
     * This {@link ClientBundle} is used for all the button icons. Using a
     * bundle allows all of these images to be packed into a single image, which
     * saves a lot of HTTP requests, drastically improving startup time.
     */
    public interface Images extends ClientBundle {

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/bold.gif")
        ImageResource bold();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/createLink.gif")
        ImageResource createLink();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/hr.gif")
        ImageResource hr();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/indent.gif")
        ImageResource indent();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/insertImage.gif")
        ImageResource insertImage();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/italic.gif")
        ImageResource italic();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/justifyCenter.gif")
        ImageResource justifyCenter();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/justifyLeft.gif")
        ImageResource justifyLeft();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/justifyRight.gif")
        ImageResource justifyRight();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/ol.gif")
        ImageResource ol();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/outdent.gif")
        ImageResource outdent();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/removeFormat.gif")
        ImageResource removeFormat();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/removeLink.gif")
        ImageResource removeLink();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/strikeThrough.gif")
        ImageResource strikeThrough();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/subscript.gif")
        ImageResource subscript();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/superscript.gif")
        ImageResource superscript();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/ul.gif")
        ImageResource ul();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/underline.gif")
        ImageResource underline();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/table.gif")
        ImageResource insertTable();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/code.gif")
        ImageResource code();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/caption.gif")
        ImageResource caption();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/tableCaption.gif")
        ImageResource tableCaption();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/createObjLink.gif")
        ImageResource createObjLink();

        @Source("pl/fovis/foxygwtcommons/richtexteditor/images/insertMyImage.gif")
        ImageResource insertMyImage();
    }

    /**
     * This {@link Constants} interface is used to make the toolbar's strings
     * internationalizable.
     */
    public interface Strings extends Constants {

        String black();

        String blue();

        String bold();

        String color();

        String createLink();

        String font();

        String green();

        String hr();

        String indent();

        String insertImage();

        String italic();

        String justifyCenter();

        String justifyLeft();

        String justifyRight();

        String large();

        String medium();

        String normal();

        String ol();

        String outdent();

        String red();

        String removeFormat();

        String removeLink();

        String size();

        String small();

        String strikeThrough();

        String subscript();

        String superscript();

        String ul();

        String underline();

        String white();

        String xlarge();

        String xsmall();

        String xxlarge();

        String xxsmall();

        String yellow();

        String insertTable();

        String code();

        String caption();

        String tableCaption();

        String createObjLink();

        String insertMyImage();
    }

    /**
     * We use an inner EventHandler class to avoid exposing event methods on the
     * RichTextToolbar itself.
     */
    private class EventHandler implements ClickHandler, ChangeHandler, KeyUpHandler {

        @Override
        public void onChange(ChangeEvent event) {
            Widget sender = (Widget) event.getSource();

            if (sender == backColors) {
                formatter.setBackColor(backColors.getValue(backColors.getSelectedIndex()));
                backColors.setSelectedIndex(0);
            } else if (sender == foreColors) {
                formatter.setForeColor(foreColors.getValue(foreColors.getSelectedIndex()));
                foreColors.setSelectedIndex(0);
            } else if (sender == fonts) {
                formatter.setFontName(fonts.getValue(fonts.getSelectedIndex()));
                fonts.setSelectedIndex(0);
            } else if (sender == fontSizes) {
                formatter.setFontSize(fontSizesConstants[fontSizes.getSelectedIndex() - 1]);
                fontSizes.setSelectedIndex(0);
            } else if (sender == viewMode) {
                if ("HTML".equalsIgnoreCase(viewMode.getSelectedItemText())) {
                    changeButtonsStatuses(true);
                    String body = richText.getText();
                    richText.setText("");
                    richText.setHTML(body);
                } else if (I18n.programista.get().equalsIgnoreCase(viewMode.getSelectedItemText())) {
                    changeButtonsStatuses(false);
                    String body = richText.getHTML();
                    richText.setHTML("");
                    richText.setText(body);
                } else {
                    handlerAdditionalViewMode(event);
                }
            }
        }

        @Override
        public void onClick(ClickEvent event) {
            Widget sender = (Widget) event.getSource();

            if (sender == bold) {
                formatter.toggleBold();
            } else if (sender == italic) {
                formatter.toggleItalic();
            } else if (sender == underline) {
                formatter.toggleUnderline();
            } else if (sender == subscript) {
                formatter.toggleSubscript();
            } else if (sender == superscript) {
                formatter.toggleSuperscript();
            } else if (sender == strikethrough) {
                formatter.toggleStrikethrough();
            } else if (sender == indent) {
                formatter.rightIndent();
            } else if (sender == outdent) {
                formatter.leftIndent();
            } else if (sender == justifyLeft) {
                formatter.setJustification(RichTextArea.Justification.LEFT);
            } else if (sender == justifyCenter) {
                formatter.setJustification(RichTextArea.Justification.CENTER);
            } else if (sender == justifyRight) {
                formatter.setJustification(RichTextArea.Justification.RIGHT);
            } else if (sender == insertImage) {
                String url = Window.prompt(I18n.podajAdresURLDlaObrazka.get() /* I18N:  */ + ":", "http" /* I18N: no! */ + "://");
                if (url != null) {
                    formatter.insertImage(url);
                }
            } else if (sender == createLink) {
                String url = Window.prompt(I18n.podajAdresURL.get() /* I18N:  */, "http" /* I18N: no */ + "://");
                if (url != null) {
                    formatter.createLink(url);
                    /*
                     // parse HTML for the URL and add target="_blank" to the link
                     // code below contains a problem: if the same exact URL is entered more than once, it will attach target="_blank"
                     // to each URL (doesn't check if target="_blank" is already there)
                     String currentHtml = richText.getHTML();
                     int linkLocation = currentHtml.indexOf("href=\"" + url + "\"");
                     int oldLinkLocation = 0;
                     while (linkLocation != -1) {
                     if (currentHtml.substring(oldLinkLocation, linkLocation).indexOf("target=\"blank\"") == -1) {
                     currentHtml = currentHtml.substring(0, linkLocation) + "target=\"_blank\" " + currentHtml.substring(linkLocation, currentHtml.length());
                     }

                     oldLinkLocation = linkLocation + ("href=\"" + url + "\"").length();
                     linkLocation = currentHtml.indexOf("href=\"" + url + "\"", oldLinkLocation);
                     }
                     richText.setHTML(currentHtml);*/
                }
            } else if (sender == removeLink) {
                formatter.removeLink();
            } else if (sender == hr) {
                formatter.insertHorizontalRule();
            } else if (sender == ol) {
                formatter.insertOrderedList();
            } else if (sender == ul) {
                formatter.insertUnorderedList();
            } else if (sender == tableCaption) {
                insertTableOfContents();
            } else if (sender == removeFormat) {
                //extended.removeFormat();
                removeFormatting();
            } else if (sender == insertTable) {
                showAddTableDialog();
            } else if (sender == caption) {
                insertHeading();
            } else if (sender == code) {
                formatAsCode();
            } else if (sender == insertMyImage) {
                showImageUploadDialog();
            } else if (sender == showHtml) {
                Window.alert(richText.getHTMLNoFixFullPro());
            } else if (sender == richText) {
                // We use the RichTextArea's onKeyUp event to update the toolbar status.
                // This will catch any cases where the user moves the cursur using the
                // keyboard, or uses one of the browser's built-in keyboard shortcuts.
                updateStatus();
            }
        }

        public void onKeyUp(KeyUpEvent event) {
            Widget sender = (Widget) event.getSource();
            if (sender == richText) {
                // We use the RichTextArea's onKeyUp event to update the toolbar status.
                // This will catch any cases where the user moves the cursur using the
                // keyboard, or uses one of the browser's built-in keyboard shortcuts.
                updateStatus();
            }
        }

    }

    protected void handlerAdditionalViewMode(ChangeEvent event) {
    }

    protected void changeButtonsStatuses(boolean isEnable) {
        for (ButtonBase btn : allButtons) {
            btn.setEnabled(isEnable);
        }
//        showHtml.setEnabled(true);
        backColors.setEnabled(isEnable);
    }
    private Set<ButtonBase> allButtons = new HashSet<ButtonBase>();
    private static final RichTextArea.FontSize[] fontSizesConstants = new RichTextArea.FontSize[]{
        RichTextArea.FontSize.XX_SMALL, RichTextArea.FontSize.X_SMALL,
        RichTextArea.FontSize.SMALL, RichTextArea.FontSize.MEDIUM,
        RichTextArea.FontSize.LARGE, RichTextArea.FontSize.X_LARGE,
        RichTextArea.FontSize.XX_LARGE};
    private Images images = (Images) GWT.create(Images.class);
    private Strings strings = (Strings) GWT.create(Strings.class);
    private EventHandler handler;
    public FoxyRichTextArea richText;
    protected Formatter formatter;
    private VerticalPanel outer = new VerticalPanel();
    private HorizontalPanel topPanel = new HorizontalPanel();
    private HorizontalPanel bottomPanel = new HorizontalPanel();
    // ---
    private ToggleButton bold;
    private ToggleButton italic;
    private ToggleButton underline;
    private ToggleButton subscript;
    private ToggleButton superscript;
    private ToggleButton strikethrough;
    private PushButton indent;
    private PushButton outdent;
    private PushButton justifyLeft;
    private PushButton justifyCenter;
    private PushButton justifyRight;
    private PushButton hr;
    private PushButton ol;
    private PushButton ul;
    private PushButton insertImage;
    protected PushButton insertMyImage;
    private PushButton createLink;
    protected PushButton createObjLink;
    private PushButton removeLink;
    private PushButton removeFormat;
    protected PushButton insertTable;
    protected PushButton code;
    protected PushButton caption;
    protected PushButton tableCaption;
    protected PushButton showHtml;
    //---
    private ListBox backColors;
    private ListBox viewMode;
    private ListBox foreColors;
    private ListBox fonts;
    private ListBox fontSizes;
    protected IContinuation onStatusChangeCont;
    protected Range actualRange;
    protected boolean statusChanged = true;
    protected Pair<Integer, Integer> rowAndColIdxesInTable;
    private List<String> allowedExst;

    /**
     * Creates a new toolbar that drives the given rich text area.
     *
     * @param richText the rich text area to be controlled
     */
    public RichTextAreaFoxyToolbar(FoxyRichTextArea richText, List<String> allowedExst) {
        this.handler = new EventHandler();

        this.richText = richText;
        this.formatter = richText.getFormatter();
        this.richText.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                popupMenu.hide();
            }
        });
        this.allowedExst = allowedExst;

        outer.add(topPanel);
        outer.add(bottomPanel);
        topPanel.setWidth("100%");
        bottomPanel.setWidth("100%");

        initWidget(outer);
        setStyleName("gwt-RichTextToolbar");

        richText.addStyleName("hasRichTextToolbar");

        if (formatter != null) {
            topPanel.add(viewMode = createViewModeList());
            topPanel.add(bold = createToggleButton(images.bold(), I18n.bold.get()));
            topPanel.add(italic = createToggleButton(images.italic(), I18n.italic.get()));
            topPanel.add(underline = createToggleButton(images.underline(), I18n.underline.get()));
            topPanel.add(subscript = createToggleButton(images.subscript(), I18n.subscript.get()));
            topPanel.add(superscript = createToggleButton(images.superscript(), I18n.superscript.get()));
            topPanel.add(justifyLeft = createPushButton(images.justifyLeft(), I18n.justifyLeft.get()));
            topPanel.add(justifyCenter = createPushButton(images.justifyCenter(), I18n.justifyCenter.get()));
            topPanel.add(justifyRight = createPushButton(images.justifyRight(), I18n.justifyRight.get()));
            topPanel.add(strikethrough = createToggleButton(images.strikeThrough(), I18n.strikeThrough.get()));
            topPanel.add(indent = createPushButton(images.indent(), I18n.indent.get()));
            topPanel.add(outdent = createPushButton(images.outdent(), I18n.outdent.get()));
            topPanel.add(hr = createPushButton(images.hr(), I18n.hr.get()));
            topPanel.add(ol = createPushButton(images.ol(), I18n.ol.get()));
            topPanel.add(ul = createPushButton(images.ul(), I18n.ul.get()));
            topPanel.add(insertImage = createPushButton(images.insertImage(), I18n.insertImage.get()));
            topPanel.add(insertMyImage = createPushButton(images.insertMyImage(), I18n.insertMyImage.get()));
            topPanel.add(createLink = createPushButton(images.createLink(), I18n.createLink.get()));
            topPanel.add(createObjLink = createPushButton(images.createObjLink(), I18n.createObjLink.get()));
            topPanel.add(removeLink = createPushButton(images.removeLink(), I18n.removeLink.get()));
            topPanel.add(removeFormat = createPushButton(images.removeFormat(), I18n.removeFormat.get()));
            topPanel.add(insertTable = createPushButton(images.insertTable(), I18n.insertTable.get()));
            topPanel.add(code = createPushButton(images.code(), I18n.code.get()));
            topPanel.add(caption = createPushButton(images.caption(), I18n.caption.get()));
            topPanel.add(tableCaption = createPushButton(images.tableCaption(), I18n.tableCaption.get()));
            topPanel.add(showHtml = createPushButton("HTML" /* I18N: no */, "" /* I18N:  */));
//            topPanel.add(showHtml = createPushButton("HTML" /* I18N: no */, "" /* I18N:  */));
            showHtml.setVisible(false);

            topPanel.add(backColors = createColorList("Background" /* I18N: no */));
//            bottomPanel.add(backColors = createColorList("Background"));
//            bottomPanel.add(foreColors = createColorList("Foreground"));
//            bottomPanel.add(fonts = createFontList());
//            bottomPanel.add(fontSizes = createFontSizes());

            // We only use these handlers for updating status, so don't hook them up
            // unless at least basic editing is supported.
            richText.addKeyUpHandler(handler);
            richText.addClickHandler(handler);
        }
    }

    protected void recalcStatus() {
        Node n = Range.getNodeInRange(richText);

        rowAndColIdxesInTable = GWTUtils.getRowAndColIndicesOfNodeInsideTableCell(n);
    }

    protected void recalcStatusIfNeeded() {
        if (statusChanged) {
            recalcStatus();
            statusChanged = false;
        }
    }

    public boolean isInTable() {
        recalcStatusIfNeeded();

        return rowAndColIdxesInTable != null;
    }

    public int getRowIdxInTable() {
        return Pair.getV1(rowAndColIdxesInTable, -1);
    }

    public int getColIdxInTable() {
        return Pair.getV2(rowAndColIdxesInTable, -1);
    }

    protected void formatBlock(String withTag) {
        execCommand("FormatBlock", "<" + withTag + ">");
    }

    protected void removeFormatting() {
        formatBlock("p" /* I18N: no */);
        formatter.removeFormat();
    }

    protected List<Pair<String, String>> fixAndExtractTableOfContentsItemsInner(List<Pair<String, String>> items, Element e) {
        String tagName = e.getTagName().toLowerCase();
        if (("h" /* I18N: no */ + "2").equals(tagName)) {
            String tokenForId = BaseUtils.generateRandomToken(20).toLowerCase();
            e.setId(tokenForId);
            e.setAttribute("name" /* I18N: no */, "naglowek" /* I18N: no *//*tokenForId*/);
            items.add(new Pair<String, String>(e.getInnerText(), tokenForId));
        } else {
            Element child = e.getFirstChildElement();
            while (child != null) {
                fixAndExtractTableOfContentsItemsInner(items, child);
                child = child.getNextSiblingElement();
            }
        }
        return items;
    }

    // pair<caption, id>
    protected final List<Pair<String, String>> fixAndExtractTableOfContentsItems() {
        return fixAndExtractTableOfContentsItemsInner(new ArrayList<Pair<String, String>>(), getEditorContainerElement());
    }

    protected Element getEditorContainerElement() {
        return getRichTextBodyElement();
    }

    public void insertTableOfContents() {
        List<Pair<String, String>> tocItems = fixAndExtractTableOfContentsItems();
        boolean noSeeAlso = FoxyCommonUtils.extractNodeIdsFromHtml(richText.getHTML()).isEmpty();

        if (tocItems.isEmpty() && noSeeAlso) {
            Window.alert(I18n.dodajNaglowkiH1PojawiaSieOneWSpi.get() /* I18N:  */);
            Element e = getElementById("toc" /* I18N: no */);
            if (e != null) {
                //gdy Nie znaleziono żadnych pozycji spisu treści.
                StringBuilder sb = new StringBuilder();
                sb.append("<div id=\"toctitle\"><h3>").append(I18n.spisTresci.get()).append("</h3>"
                        + "</div><ul style=\"display: block;\">");
                sb.append(" <li class=\"toclevel-1 tocsection-2\"> <span class=\"toctext\">").append(I18n.nieZnalezioZadnychPozycjiSpisuTr.get()).append(".</span></li>");
                sb.append("</ul>");
                TableElement table = TableElement.as(e);
                table.getRows().getItem(0).getCells().getItem(0).setInnerHTML(sb.toString());
            }
        } else {
//            restoreSavedRange();
//            formatter.insertHTML(createTableOfContents(tocItems, noSeeAlso));
            searchAndCreateOrUpdateToc(tocItems, noSeeAlso);
        }
    }

    protected void searchAndCreateOrUpdateToc(List<Pair<String, String>> tocItems, boolean noSeeAlso) {
        Element e = getElementById("toc");
        if (e != null) { // jest już spis
            TableElement table = TableElement.as(e);
            table.getRows().getItem(0).getCells().getItem(0).setInnerHTML(createToCString(tocItems, noSeeAlso));
//            e.setInnerHTML(createToCString(tocItems, noSeeAlso));
        } else { // nie ma spisu, tworzę nowy
            String tocHTML = createTableOfContents(tocItems, noSeeAlso);
            formatter.insertHTML(tocHTML);
        }
    }

    protected Element getElementById(String id) {
        return getElementById(getEditorContainerElement(), id);
    }

    protected Element getElementById(Element ele, String id) {
        if (ele.getId().equals(id)) {
            return ele;
        }

        Element child = ele.getFirstChildElement();
        while (child != null) {
            Element elementFound = getElementById(child, id);
            if (elementFound != null) {
                return elementFound;
            }
            child = child.getNextSiblingElement();
        }
        return null;
    }

//    protected void removeTableOfContents(Element e) {
//        String tagName = e.getTagName().toLowerCase();
//        if ("table".equals(tagName) && e.getId().equals("toc")) {
//            e.removeFromParent();
//        } else {
//            Element child = e.getFirstChildElement();
//            while (child != null) {
//                removeTableOfContents(child);
//                child = child.getNextSiblingElement();
//            }
//        }
//    }
    protected final String createTableOfContents(List<Pair<String, String>> tocItems, boolean noSeeAlso) {
        StringBuilder sb = new StringBuilder();
        sb.append("<table id=\"toc\"><tbody><tr><td>");
        ///////////////////////
        //sb.append(createToCString(tocItems, noSeeAlso));
        createToCString(sb, tocItems, noSeeAlso);
        /////////////////////
        sb.append("</td> </tr> </tbody> </table>");

        return sb.toString();
    }

    protected String createToCString(List<Pair<String, String>> tocItems, boolean noSeeAlso) {
        return createToCString(new StringBuilder(), tocItems, noSeeAlso).toString();
    }

    protected StringBuilder createToCString(StringBuilder sb, List<Pair<String, String>> tocItems, boolean noSeeAlso) {
        sb.append("<div id=\"toctitle\"><h3>").append(I18n.spisTresci.get()).append("</h3>"
                + "</div><ul style=\"display: block;\">");

        for (Pair<String, String> tocItem : tocItems) {
            sb.append("<li class=\"toclevel-1 tocsection-1\"> <a href=\"#").append(tocItem.v2).append("\" style=\"text-decoration:none;color: #0000AA;\"><span class=\"toctext\">").
                    append(tocItem.v1).append("</span></a> </li>");
        }

        if (!noSeeAlso) {
            sb.append(" <li class=\"toclevel-1 tocsection-2\"> <a href=\"#zobacz\" style=\"text-decoration:none;color: #0000AA;\">"
                    + "  <span class=\"toctext\">").append(I18n.zobaczTezSpanA_noMaybeIdent.get()).append("</span> </a> </li>");
        }
        sb.append("</ul>");

        return sb;
    }

    protected String getSelectedText() {
        String res = null;
        Range rng = Range.getRange(richText);
        if (rng != null) {
            if (!rng.isCursor()) {
                res = rng.getHtmlText();
            }
        }
        return BaseUtils.nullToDef(res, "");
    }

    protected void surroundSelectedText(String prefix, String suffix) {
        formatter.insertHTML(prefix + getSelectedText() + suffix);
    }

    protected void formatAsCode() {
//        formatter.insertHTML("<table class=\"def-code\"><tr><td>&nbsp;</td></tr></table>");
        formatter.insertHTML("<table class=\"def-code\"><tr><td>" + getSelectedText() + "&" + "nbsp" /* I18N: no */ + ";</td></tr></table>");
        //ww: poniższe sposoby nie działają poprawnie z różnych powodów
//        formatBlock("<div class='def-code'>");
//        surroundSelectedText("<div class=\"def-code\">", "</div>");
//        formatBlock("<pre>");
//        surroundSelectedText("<pre class=\"def-code\">", "</pre>");
    }

    protected void insertHeading() {
        formatBlock("h" /* I18N: no */ + "2");
    }

    private ListBox createColorList(String caption) {
        ListBox lb = new ListBox();
        lb.addChangeHandler(handler);
        lb.setVisibleItemCount(1);

        lb.addItem(caption);
        //i18n-default: no
        lb.addItem(I18n.white.get(), "white" /* I18N: no */);
        lb.addItem(I18n.black.get(), "black" /* I18N: no */);
        lb.addItem(I18n.red.get(), "red" /* I18N: no */);
        lb.addItem(I18n.green.get(), "green" /* I18N: no */);
        lb.addItem(I18n.yellow.get(), "yellow" /* I18N: no */);
        lb.addItem(I18n.blue.get(), "blue" /* I18N: no */);
        //i18n-default:
        return lb;
    }

    private ListBox createViewModeList() {
        ListBox lb = new ListBox();
        lb.addChangeHandler(handler);
        lb.setVisibleItemCount(1);

        lb.addItem("HTML");
        addAdditionalViewMode(lb);

        return lb;
    }

    protected void addAdditionalViewMode(ListBox lb) {
        lb.addItem(I18n.programista.get());
    }

    private ListBox createFontList() {
        ListBox lb = new ListBox();
        lb.addChangeHandler(handler);
        lb.setVisibleItemCount(1);

        lb.addItem(I18n.font.get(), "");
        lb.addItem(I18n.normal.get(), "");
        //i18n-default: no
        lb.addItem("Times New Roman" /* I18N: no */, "Times New Roman" /* I18N: no */);
        lb.addItem("Arial" /* I18N: no */, "Arial" /* I18N: no */);
        lb.addItem("Courier New" /* I18N: no */, "Courier New" /* I18N: no */);
        lb.addItem("Georgia" /* I18N: no */, "Georgia" /* I18N: no */);
        lb.addItem("Trebuchet" /* I18N: no */, "Trebuchet" /* I18N: no */);
        lb.addItem("Verdana" /* I18N: no */, "Verdana" /* I18N: no */);
        //i18n-default:
        return lb;
    }

    protected Selection getSelection() {
        return Selection.getSelection(FoxyRichTextArea.getWindow(richText));
    }

    protected void saveCurrentRange() {
        //pod ff i chromem już samo Range.getRange(richText) nie działa (nie zwraca nawet null)
        if (richText.getHTML().equals("<br>") || richText.getHTML().isEmpty()) {
            formatter.insertHTML("&" + "nbsp" /* I18N: no */ + ";");
        }
        actualRange = Range.getRange(richText);
        if (actualRange == null) {
            formatter.insertHTML("&" + "nbsp" /* I18N: no */ + ";");
            actualRange = Range.getRange(richText);
        }
    }

    protected void restoreSavedRange() {
        if (actualRange == null) {
            return;
        }
        Selection sel = getSelection();
        if (sel != null) {
            sel.setRange(actualRange);
        }
    }
    protected PopupMenuDisplayer popupMenu = new PopupMenuDisplayer();

    protected void showAddTableDialog() {
        saveCurrentRange();
        popupMenu.clearMenuItems();

        if (getOptTableNode() != null) {
            popupMenu.addMenuItem(I18n.dodajWierszPonizej.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            addRow(true);//true poniżej false powyżej
                        }
                    });
            popupMenu.addMenuItem(I18n.dodajWierszPowyzej.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            addRow(false);
                        }
                    });

            popupMenu.addMenuItem(I18n.dodajKolumneZLewej.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            addColumn(true);//true z lewej false z prawej
                        }
                    });
            popupMenu.addMenuItem(I18n.dodajKolumneZPrawej.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            addColumn(false);
                        }
                    });
            popupMenu.addMenuItem(I18n.usunWiersz.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            deleteRow();
                        }
                    });

            popupMenu.addMenuItem(I18n.usunKolumne.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            deleteColumn();
                        }
                    });

            popupMenu.addMenuItem(I18n.ustawSzerokosc.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            setWidthColumn();
                        }
                    });

//            popupMenu.addMenuItem("Ustaw wysokość", new IContinuation() {
//
//                public void doIt() {
//                    setHeightRow();
//                }
//            });
//            saveCurrentRange();
            popupMenu.showAtPosition(insertTable.getAbsoluteLeft(), insertTable.getAbsoluteTop());

        } else {
            AddTableDialog atd = new AddTableDialog();
            atd.buildAndShowDialog(new IParametrizedContinuation<String>() {
                public void doIt(String param) {
                    restoreSavedRange();
                    formatter.insertHTML(param);
                }
            });
        }
    }

    private ListBox createFontSizes() {
        ListBox lb = new ListBox();
        lb.addChangeHandler(handler);
        lb.setVisibleItemCount(1);

        lb.addItem(I18n.size.get());
        lb.addItem(I18n.xxsmall.get());
        lb.addItem(I18n.xsmall.get());
        lb.addItem(I18n.small.get());
        lb.addItem(I18n.medium.get());
        lb.addItem(I18n.large.get());
        lb.addItem(I18n.xlarge.get());
        lb.addItem(I18n.xxlarge.get());
        return lb;
    }

    protected <B extends ButtonBase> B setUpButton(B btn, String tip) {
        allButtons.add(btn);
        btn.addClickHandler(handler);
        btn.setTitle(tip);
        return btn;
    }

    private PushButton createPushButton(String btnTxt, String tip) {
        return setUpButton(new PushButton(btnTxt), tip);
    }

    private PushButton createPushButton(ImageResource img, String tip) {
        return setUpButton(new PushButton(new Image(img)), tip);
    }

    private ToggleButton createToggleButton(ImageResource img, String tip) {
        return setUpButton(new ToggleButton(new Image(img)), tip);
    }

    private void updateStatus() {
        statusChanged = true;

        if (formatter != null) {
            bold.setDown(formatter.isBold());
            italic.setDown(formatter.isItalic());
            underline.setDown(formatter.isUnderlined());
            subscript.setDown(formatter.isSubscript());
            superscript.setDown(formatter.isSuperscript());
            strikethrough.setDown(formatter.isStrikethrough());
        }

        if (onStatusChangeCont != null) {
            onStatusChangeCont.doIt();
        }
    }

    public void setOnStatusChange(IContinuation onStatusChangeCont) {
        this.onStatusChangeCont = onStatusChangeCont;
    }

    private void showImageUploadDialog() {
        saveCurrentRange();

        FileUploadDialogBase fud = new FileUploadDialogForPhoto();
        fud.buildAndShowDialog(new IParametrizedContinuation<FoxyFileUpload>() { // to do
            public void doIt(final FoxyFileUpload param) {
                restoreSavedRange();
                formatter.insertImage(//GWT.getHostPageBaseURL() + "fsb?get=" + param.getServerFileName()
                        FoxyClientSingletons.getFullFileUploadUrl(param.getServerFileName()));
            }
        }, allowedExst);
    }

    protected void execCommand(String cmd, String param) {
        // When executing a command, focus the iframe first, since some commands
        // don't take properly when it's not focused.
        richText.setFocus(true);
        try {
            execCommandAssumingFocus(cmd, param);
        } catch (JavaScriptException e) {
            // In mozilla, editing throws a JS exception if the iframe is
            // *hidden, but attached*.
        }
    }

    protected void execCommandAssumingFocus(String cmd, String param) {
        execCommandAssumingFocusInner(richText.getElement(), cmd, param);
    }

    public static native void execCommandAssumingFocusInner(Element elem, String cmd, String param) /*-{
     elem.contentWindow.document.execCommand(cmd, false, param);
     }-*/;

    protected Element getRichTextBodyElement() {
        return Element.as(getRichTextBodyElement(richText.getElement()));
    }

    protected static native JavaScriptObject getRichTextBodyElement(Element richTextElement) /*-{
     return richTextElement.contentWindow.document.body;
     }-*/;

    protected TableElement getOptTableNode() {
//        saveCurrentRange();
        Element ele = getOptSelectedElementByName("table" /* I18N: no */);
        if (ele == null || ele.getClassName().equals("def-code") || ele.getId().equals("toc" /* I18N: no */)) {
            return null;
        }
        return TableElement.as(ele);
    }

    protected TableCellElement getOptTableCellNode() {
        Element ele = getOptSelectedElementByName("td" /* I18N: no */);
        return ele == null ? null : TableCellElement.as(ele);
    }

    protected TableRowElement getOptTableRowNode() {
        Element ele = getOptSelectedElementByName("tr" /* I18N: no */);
        return ele == null ? null : TableRowElement.as(ele);
    }

    protected Element getOptSelectedElementByName(String name) {
//        saveCurrentRange();
        Range rng = richText.getRange();//Range.getRange(richText);/* getRange();*/ //sel.getRange();

        Set<RangeEndPoint> reps = BaseUtils.paramsAsSet(rng.getStartPoint(),
                rng.getCursor(), rng.getEndPoint());

        for (RangeEndPoint rep : reps) {
            if (rep != null) {
                Node n = rep.getNode();
                if (n != null) {
                    Element tc = GWTUtils.findAncestorElementByTagName(n, name);

                    if (tc != null) {
                        return tc;
                    }
                }
            }
        }
        return null;
    }

    protected void addRow(boolean up) {
        TableElement t = getOptTableNode();
        if (t != null) {
            int rowCount = t.getRows().getLength();
            int cellCount = rowCount > 0 ? t.getRows().getItem(0).getCells().getLength() : 1;
            TableRowElement row = t.insertRow(getOptTableRowNode().getRowIndex() + (up ? 1 : 0));

            for (int cellNo = 0; cellNo < cellCount; cellNo++) {
//                row.insertCell(cellNo).setInnerHTML("&nbsp;");
                row.insertCell(cellNo).setAttribute("width" /* I18N: no */, "40px");
                row.getCells().getItem(cellNo).setInnerHTML("&" + "nbsp" /* I18N: no */ + ";");

            }

        } else {
            Window.alert(I18n.brakTabeli.get() /* I18N:  */ + "!");
        }
    }

    protected void addColumn(boolean before) {
        TableElement t = getOptTableNode();
        if (t != null) {
            int rowCount = t.getRows().getLength();
            int cellIndex = getOptTableCellNode().getCellIndex();
            for (int i = 0; i < rowCount; i++) {
//                t.getRows().getItem(i).insertCell(cellIndex + (before ? 0 : 1)).setInnerHTML("&nbsp;");
                t.getRows().getItem(i).insertCell(cellIndex + (before ? 0 : 1)).setAttribute("width" /* I18N: no */, "40px");
                t.getRows().getItem(i).getCells().getItem(cellIndex + (before ? 0 : 1)).setInnerHTML("&" + "nbsp" /* I18N: no */ + ";");
            }
        } else {
            Window.alert(I18n.brakTabeli.get() /* I18N:  */ + "!");
        }
    }

    protected void deleteRow() {
        TableElement t = getOptTableNode();
        if (t != null) {
            restoreSavedRange();
            t.deleteRow(getOptTableRowNode().getSectionRowIndex());//getRowIndex());
        } else {
            Window.alert(I18n.brakTabeli.get() /* I18N:  */ + "!");
        }
    }

    protected void deleteColumn() {
        TableElement t = getOptTableNode();
        if (t != null) {
            int rowCount = t.getRows().getLength();
            int cellIndex = getOptTableCellNode().getCellIndex();
            for (int i = 0; i < rowCount; i++) {
                t.getRows().getItem(i).deleteCell(cellIndex);
            }
        } else {
            Window.alert(I18n.brakTabeli.get() /* I18N:  */ + "!");
        }
    }

    protected TableElement getOptDefCode() {
        saveCurrentRange();
        Element ele = getOptSelectedElementByName("table" /* I18N: no */);
        if (ele == null || ele.getClassName().equals("def" /* I18N: no */) || ele.getId().equals("def-code")) {
            return null;
        }
        return TableElement.as(ele);
    }

    protected void setWidthColumn() {

        final TableElement t = getOptTableNode();

        if (t != null) {
            final int rowCount = t.getRows().getLength();
            final int cellIndex = getOptTableCellNode().getCellIndex();
            new TableColWidth().buildAndShowDialog(new IParametrizedContinuation<String>() {
                public void doIt(String param) {
                    for (int i = 0; i < rowCount; i++) {
                        t.getRows().getItem(i).getCells().getItem(cellIndex).setAttribute("width" /* I18N: no */, param + "px");
                    }
                }
            }, I18n.szerokosc.get() /* I18N:  */);
        } else {
            Window.alert(I18n.brakTabeli.get() /* I18N:  */ + "!");
        }
    }
//    protected void setHeightRow() {
//
//        final TableElement t = getOptTableNode();
//
//        if (t != null) {
//            final int rowCount = getOptTableRowNode().getSectionRowIndex();
////            final int cellIndex = getOptTableCellNode().getCellIndex();
//            new TableColWidth().buildAndShowDialog(new IParametrizedContinuation<String>() {
//
//                public void doIt(String param) {
//                    t.getRows().getItem(rowCount).setAttribute("height", param + "px");
//                }
//            }, "Wysokość");
//        } else {
//            Window.alert("no table!");
//        }
//    }

    public boolean isHtmlMode() {
        return "HTML".equalsIgnoreCase(viewMode.getSelectedItemText());
    }
}
