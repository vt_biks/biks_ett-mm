/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.tags;

import commonlib.LameUtils;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import pl.foxys.foxymill.FoxyMillRenderingFallThroughException;
import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.IExceptionHandlingModel;
import pl.foxys.foxymill.IRenderingAbortAwareModel;
import pl.foxys.foxymill.IRerenderCapableModel;
import pl.foxys.foxymill.RenderEnvironment;
import pl.foxys.foxymill.RerenderWithNewModelException;
import pl.foxys.foxymill.ReturnFromRenderingException;
import pl.foxys.foxymill.exprlang.IExpressionLanguage;
import pl.foxys.foxymill.exprlang.IParsedExpression;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import simplelib.LameRuntimeException;
import simplelib.TextFilePosition;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public abstract class TagBase implements ITag {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected Map<String, String> attrs;
    protected List<ITag> subTags;
    protected ITag prevTag;
    private String exprLangName;
    protected TextFilePosition tfp;
    protected IExpressionLanguage exprLang;
    protected RenderEnvironment renderEnv;

    protected void initSelf() {
        //no-op
    }

    protected abstract void internalRender(//IRenderContext
            IRenderCtx renderCtx);

    protected void handleModelDrivenEvents(IRenderCtx renderCtx) {
        Object rm = renderCtx.getRootModel();

        if (rm instanceof IRenderingAbortAwareModel) {
            IRenderingAbortAwareModel raam = (IRenderingAbortAwareModel) rm;
            Boolean doAbort = raam.getReturnFromRenderingFlag();
            if (doAbort != null) {
                throw new ReturnFromRenderingException(doAbort);
            }
        }

        if (rm instanceof IRerenderCapableModel) {
            IRerenderCapableModel rcm = (IRerenderCapableModel) rm;
            Object newModel = rcm.getRerenderModel();
            if (newModel != null) {
                throw new RerenderWithNewModelException(newModel);
            }
        }
    }

    public void render(//IRenderContext
            IRenderCtx renderCtx) {
        try {
            internalRender(renderCtx);
        } catch (Throwable ex) {
            if (ex instanceof FoxyMillRenderingFallThroughException) {
                throw (FoxyMillRenderingFallThroughException) ex;
            }
            if (!tryHandleExceptionByModel(renderCtx, ex)) {
                throw new LameRuntimeException("error in internal render", ex);
            }
        }
        handleModelDrivenEvents(renderCtx);
    }

    protected void prepareSelf() {
        //no-op
    }

//    @Override
    public final void init(Map<String, String> attrs, List<ITag> subTags, ITag prevTag,
            TextFilePosition filePos) {
        this.attrs = attrs;
        this.subTags = subTags;
        this.prevTag = prevTag;
        this.tfp = filePos;
        this.exprLangName = this.attrs.remove("exprLang");

        initSelf();
    }

    protected String getReqAttrVal(String attrName) {
        String res = attrs.get(attrName);
        if (res == null) {
            raiseError("attr named '" + attrName + "' is required, supplied attrs: " + attrs);
        }
        return res;
    }

    protected IParsedExpression attrToReqExpr(String attrName) {
        IParsedExpression res = attrToExpr(attrName);
        if (res == null) {
            raiseError("attr named '" + attrName + "' is required");
        }
        return res;
    }

    protected IParsedExpression attrToExpr(String attrName) {
        String attrVal = attrs.get(attrName);
        if (attrVal == null) {
            return null;
        }
        return exprLang.parseExpression(attrVal, attrName, tfp);
    }

    @Override
    public void prepare(RenderEnvironment renderEnv) {
        this.renderEnv = renderEnv;
        exprLang = renderEnv.exprLangs.get(exprLangName);
        if (exprLang == null) {
            throw new LameRuntimeException("no exprLang: " + exprLangName + ", defined expr langs=" + renderEnv.exprLangs.keySet());
        }
        prepareSelf();
        FoxyMillUtils.prepareTags(subTags, renderEnv);
    }

    protected void fixUpSelf(Map<String, Object> globals) {
        // no-op
    }

    public void fixUp(Map<String, Object> globals) {
        fixUpSelf(globals);

        FoxyMillUtils.fixupTags(subTags, globals);
    }

    public void raiseError(String msg, Throwable cause) {
        throw new LameRuntimeException(msg + "\nat: " + tfp, cause);
    }

    public void raiseError(String msg) {
        raiseError(msg, null);
    }

    public TextFilePosition getTextFilePosition() {
        return tfp;
    }

    protected boolean tryHandleExceptionByModel(IRenderCtx renderCtx, Throwable ex) {
        Object rm = renderCtx.getRootModel();

        if (rm instanceof IExceptionHandlingModel) {
            IExceptionHandlingModel ehm = (IExceptionHandlingModel) rm;
            Object newModel = ehm.getRerenderOnExceptionModel();
            if (newModel == null) {
                return false;
            }
            if (logger.isErrorEnabled()) {
                logger.error("caught exception for handling by model", ex);
                //ex.printStackTrace();
            }
            throw new RerenderWithNewModelException(newModel);
        }

        return false;
    }
}
