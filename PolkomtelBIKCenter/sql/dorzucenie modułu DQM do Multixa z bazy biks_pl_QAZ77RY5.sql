﻿---------------------------------------------------
-- skrypt NIE jest wielokrotnego wykonania
-- zalozenie ze node_kindy maja takie same ID
-- pobranie DQM z bazy przemyslaw.kowalski@plusbank.pl - biks_pl_QAZ77RY5
---------------------------------------------------

update bik_app_prop set val = val + ',DQC' where name = 'availableConnectors'

update bik_tree set name = 'DQM', is_hidden = 0 where code = 'DQC'

declare @treeOfTreesId int = (select id from bik_tree where code = 'TreeOfTrees')

update bik_node set parent_node_id = null where tree_id = @treeOfTreesId and obj_id = '$DQC'

declare @dqcTreeId int = (select id from bik_tree where code = 'DQC')

insert into bik_node(node_kind_id, name, tree_id, obj_id, is_built_in)
select node_kind_id, name, @dqcTreeId, obj_id, is_built_in from [biks_pl_QAZ77RY5].[dbo].[bik_node] where tree_id = (select id from [biks_pl_QAZ77RY5].[dbo].[bik_tree] where code = 'DQC') and parent_node_id is null
go

-- Wszystkie testy
declare @dqcTreeId int = (select id from bik_tree where code = 'DQC')
insert into bik_node(parent_node_id, node_kind_id, name, tree_id, obj_id, descr)
select (select id from bik_node where tree_id = @dqcTreeId and name = 'Wszystkie testy' and is_built_in = 1),
node_kind_id, name, @dqcTreeId, obj_id, descr 
from [biks_pl_QAZ77RY5].[dbo].[bik_node] 
where tree_id = (select id from [biks_pl_QAZ77RY5].[dbo].[bik_tree] where code = 'DQC') 
and parent_node_id = (select id from [biks_pl_QAZ77RY5].[dbo].[bik_node] where tree_id = (select id from [biks_pl_QAZ77RY5].[dbo].[bik_tree] where code = 'DQC') and name = 'Wszystkie testy' and is_built_in = 1) 
and is_deleted = 0
go

-- Testy podstawowe
declare @dqcTreeId int = (select id from bik_tree where code = 'DQC')
insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id, obj_id, descr)
select (select id from bik_node where tree_id = @dqcTreeId and name = 'Testy podstawowe'), 
node_kind_id, name, @dqcTreeId
, (select id from bik_node bn where bn.tree_id = @dqcTreeId and bn.obj_id = bno.obj_id and bn.parent_node_id = (select id from bik_node where tree_id = @dqcTreeId and name = 'Wszystkie testy' and is_built_in = 1))
, obj_id, descr 
from [biks_pl_QAZ77RY5].[dbo].[bik_node] bno where tree_id = @dqcTreeId 
and parent_node_id = (select id from [biks_pl_QAZ77RY5].[dbo].[bik_node] where tree_id = (select id from [biks_pl_QAZ77RY5].[dbo].[bik_tree] where code = 'DQC') and name = 'Testy podstawowe') 
and is_deleted = 0
go

-- Testy przekrojowe
declare @dqcTreeId int = (select id from bik_tree where code = 'DQC')
insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id, obj_id, descr)
select (select id from bik_node where tree_id = @dqcTreeId and name = 'Testy przekrojowe'), 
node_kind_id, name, @dqcTreeId
, (select id from bik_node bn where bn.tree_id = @dqcTreeId and bn.obj_id = bno.obj_id and bn.parent_node_id = (select id from bik_node where tree_id = @dqcTreeId and name = 'Wszystkie testy' and is_built_in = 1))
, obj_id, descr 
from [biks_pl_QAZ77RY5].[dbo].[bik_node] bno where tree_id = @dqcTreeId 
and parent_node_id = (select id from [biks_pl_QAZ77RY5].[dbo].[bik_node] where tree_id = (select id from [biks_pl_QAZ77RY5].[dbo].[bik_tree] where code = 'DQC') and name = 'Testy przekrojowe') 
and is_deleted = 0
go

insert into bik_dqc_group
select * from [biks_pl_QAZ77RY5].[dbo].[bik_dqc_group]

insert into bik_dqc_test
select * from [biks_pl_QAZ77RY5].[dbo].[bik_dqc_test]

insert into bik_dqc_request
select * from [biks_pl_QAZ77RY5].[dbo].[bik_dqc_request]


go

declare @dqcTreeId int = (select id from bik_tree where code = 'DQC')
exec sp_node_init_branch_id @dqcTreeId, null