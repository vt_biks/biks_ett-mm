﻿exec sp_check_version '1.0.95';
go
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

create table bik_system_user(
	id int identity(1,1) not null,
	login_name varchar(250) NOT NULL unique,
	password varchar(250) NULL,
	is_activ int not null check (is_activ in (0,1)) default 1,
	is_admin int not null check(is_admin in (0,1)) default 0,
	is_expert int not null check(is_expert in (0,1)) default 0,
	user_id int  references bik_user(id)
)
go

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
insert into bik_system_user(login_name, password, is_activ, user_id, is_admin, is_expert)
select bik_user.login_name, bik_user.password,  bik_user.activ, bik_user.id,
case	
	when bik_user_kind.name = 'Administrator' then 1
	else 0 end,
case 
	when bik_user_kind.name = 'Ekspert' then 1
	else 0 end
from bik_user inner join bik_user_kind on bik_user.user_kind_id = bik_user_kind.id
go
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

update bik_node
set name = bik_user.name
from bik_node inner join bik_user on bik_node.id = bik_user.node_id
go

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
exec sp_update_version '1.0.95', '1.0.95.1';
go