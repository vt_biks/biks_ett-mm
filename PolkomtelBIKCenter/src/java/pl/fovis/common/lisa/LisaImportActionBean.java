/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

import java.io.Serializable;

/**
 *
 * @author ctran
 */
public class LisaImportActionBean implements Serializable {

    public String serverFileName;
    public int lineNr;
    public String pkValue;
    public String actionCode;

    public LisaImportActionBean() {
    }

    public LisaImportActionBean(String serverFileName, int lineNr, String pkValue, String actionCode) {
        this.serverFileName = serverFileName;
        this.lineNr = lineNr;
        this.pkValue = pkValue;
        this.actionCode = actionCode;
    }
}
