/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.mltx;

import java.io.Serializable;
import java.util.Set;
import simplelib.BeanWithIntIdBase;

/**
 *
 * @author AMamcarz
 */
public class InternalUserBean extends BeanWithIntIdBase implements Serializable {

    public String loginName;
    public String password;
    public boolean isDisabled;
    public Integer userId;
    public String userName;
    public String systemUserName;
    public boolean flag;
    public Set<String> userRightRoleCodes;
    public Set<Integer> userAuthorTreeIDs;
    public String phoneNum;
}
