/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.Map;
import java.util.Map.Entry;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.ListItem;
import pl.fovis.foxygwtcommons.UnorderedList;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Pair;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author mgraczkowski wiem ze tu pokopiowane.. ale to pozniej uporządkuje
 */
public class InnerLinksWidgetFlat extends EntityDetailsWidgetFlatBase {

//    protected List<ArticleBean> arts;
//    protected List<ArticleBean> artsAll;
//    protected List<BlogBean> blogAll;
//    protected HTML lblZeroNote;
//    protected HTML lblNotes;
    protected VerticalPanel oneBizHl;
//    protected PushButton actionBt;
//    protected HorizontalPanel hp;
//    protected BlogBean blogEntry;
//    protected ArticleBean thisBizDef;
    protected IContinuation con;
    protected VerticalPanel innerLinksVl;
    protected UnorderedList ul;

    @Override
    public Widget buildWidgets() {
        HorizontalPanel hl = new HorizontalPanel();
        VerticalPanel vpl = new VerticalPanel();

        innerLinksVl = new VerticalPanel();
        innerLinksVl.setWidth("100%");
        oneBizHl = new VerticalPanel();
        oneBizHl.setWidth("100%");
        oneBizHl.setSpacing(3);

        vpl.setWidth("100%");
        vpl.add(innerLinksVl);
        hl.add(vpl);

        return hl;
    }

    @Override
    public void displayData() {

        fetchAndShowLinksEx(getData().innerLinks);

    }

    public void setParamToClose(IContinuation con) {
        this.con = con;
    }

    public void fetchAndShowLinksEx(Map<Integer, Pair<String, String>> innerLinks) {
        ul = new UnorderedList();
        innerLinksVl.clear();
        if (!BaseUtils.isMapEmpty(innerLinks)) {
            for (Entry<Integer, Pair<String, String>> entry : innerLinks.entrySet()) {
                final Pair<String, String> url = entry.getValue();
                ul.add(makeLiInnerLinks(url.v1, url.v2, entry.getKey()));
            }
            innerLinksVl.add(ul);
        } else {
            innerLinksVl.add(zeroMsg(I18n.brakOdsylaczy.get() /* I18N:  */));
        }
    }

    public boolean isEnabledDeprecated(String code, int count) {
        return true;
    }

    public ListItem makeLiInnerLinks(String caption, final String tabId, final int nodeId) {
        ListItem li = new ListItem(caption);
        li.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
                getTabSelector().submitNewTabSelection(tabId, (Integer) nodeId, true);
                BaseActionOrCancelDialog.hideAllDialogs();
            }
        });
        li.setStyleName("artLink relatedBizDefMain");
        return li;
    }
}
