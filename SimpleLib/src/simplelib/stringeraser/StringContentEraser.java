/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.stringeraser;

import java.util.ArrayList;
import java.util.List;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public class StringContentEraser {

    protected List<IErasableSyntaxItem> esis = new ArrayList<IErasableSyntaxItem>();
    protected boolean isErasingWithDelims = false;
    protected boolean isPreservingWhiteChars = true;

    // gdy nie znajdzie - zwraca null
    protected EsiPos findFirstESI(String content, int pos) {

        int bestEsiStartPos = content.length();
        EsiPos bestEsiPos = null;

        for (IErasableSyntaxItem esi : esis) {
            EsiPos esiPos = esi.findStartPos(content, pos, bestEsiStartPos);
            if (esiPos == null) {
                continue;
            }

            int esiStartPos = esiPos.startPos;

            if (esiStartPos < bestEsiStartPos) {
                bestEsiStartPos = esiStartPos;
                bestEsiPos = esiPos;
            }
        }

        return bestEsiPos;
    }

    public String erase(String content) {
        StringBuilder sb = new StringBuilder();

        int pos = 0;

        while (true) {
            EsiPos esiPos = findFirstESI(content, pos);
            if (esiPos == null) {
                break;
            }

            sb.append(content.substring(pos, esiPos.startPos));
            pos = appendEsiPosErasure(sb, content, esiPos);
        }

        sb.append(content.substring(pos));

        return sb.toString();
    }

    protected void appendErasedContent(StringBuilder sb, String content, int startPos, int endPos, boolean preserveWhiteChars) {
        while (startPos < endPos) {
            char c = content.charAt(startPos);

            if (!preserveWhiteChars || c > ' ') {
                c = ' ';
            }

            sb.append(c);

            startPos++;
        }
    }

    protected int appendEsiPosErasure(StringBuilder sb, String content, EsiPos esiPos) {
        int startPos, endPos;

        Pair<Integer, Integer> esiEndPos = esiPos.calcEndPos();

        // esi-Inner/Outer-Start/End-Pos --> e[IO][SE]P
        int eOSP = esiPos.startPos;
        int eISP = esiPos.innerStartPos;
        int eIEP = esiEndPos.v1;
        int eOEP = esiEndPos.v2;

        if (isErasingWithDelims) {
            startPos = eOSP;
            endPos = eOEP;
        } else {
            sb.append(content.substring(eOSP, eISP));
            startPos = eISP;
            endPos = eIEP;
        }

        appendErasedContent(sb, content, startPos, endPos, isPreservingWhiteChars);

        if (!isErasingWithDelims) {
            sb.append(content.substring(eIEP, eOEP));
        }

        return eOEP;
    }

    public static String eraseMSSQLScript(String content, boolean isErasingWithDelims, boolean isPreservingWhiteChars) {
        StringContentEraser sce = new StringContentEraser();
        sce.esis.add(new ESISqlQuotedString('\''));
        sce.esis.add(new ESISqlQuotedString('"'));
        sce.esis.add(new ESISqlPrefixSuffixDelimitedString("[", "]"));
        sce.esis.add(new EsiBlockComment());
        sce.esis.add(new ESILineComment());
        sce.isErasingWithDelims = isErasingWithDelims;
        sce.isPreservingWhiteChars = isPreservingWhiteChars;
        return sce.erase(content);
    }

    public interface IScriptBatchCollector {

        public void initialInfo(int batchCount, int lineCount);

        // numery linii liczone od 0, endLine - linia za ostatnią linią w batchu
        // czyli to jest linia z GO w przypadku MSSQLa
        public void collect(String batch, int startLine, int endLine);
    }

    public static void splitMSSQLScript(String script, IScriptBatchCollector sbc) {
        String erased = StringContentEraser.eraseMSSQLScript(script, true, true);

//        String lineBreakRegExp = "\\r?\\n|\\r";
//        String[] lines = script.split(lineBreakRegExp);
//        String[] erasedLines = erased.split(lineBreakRegExp);
//
//        if (lines.length != erasedLines.length) {
//            throw new LameRuntimeException("error while parsing file! lines count=" + lines.length + ", erased lines cnt=" + erasedLines.length);
//        }

        int scriptLen = script.length();

        if (scriptLen != erased.length()) {
            throw new LameRuntimeException("error while parsing file! script size=" + scriptLen
                    + ", erased size=" + erased.length());
        }

        List<Integer> batchSepLines = new ArrayList<Integer>();
        List<Integer> batchStartPoses = new ArrayList<Integer>();
        List<Integer> batchLinePoses = new ArrayList<Integer>();

        int lineNum = 0;
        int batchStartPos = 0;
        int lbPos = batchStartPos;

        while (lbPos < scriptLen) {
            int nextLbPos = BaseUtils.findFirstLineBreak(erased, lbPos, true);
            int nextLbPosAfter = BaseUtils.eatLineBreak(erased, nextLbPos);

            String line = erased.substring(lbPos, nextLbPos).trim();
            if (line.equalsIgnoreCase("GO")) {
                batchSepLines.add(lineNum);
                batchStartPoses.add(batchStartPos);
                batchLinePoses.add(lbPos);
                batchStartPos = nextLbPosAfter;
            }

            lineNum++;
            lbPos = nextLbPosAfter;
        }

        if (batchStartPos < scriptLen) {
            batchSepLines.add(lineNum);
            batchStartPoses.add(batchStartPos);
            batchLinePoses.add(lbPos);
        }

        int batchCnt = batchSepLines.size();
        int firstLine = 0;

        sbc.initialInfo(batchCnt, lineNum);

        for (int batchNum = 0; batchNum < batchCnt; batchNum++) {
            int batchSepLine = batchSepLines.get(batchNum);
            sbc.collect(script.substring(batchStartPoses.get(batchNum), batchLinePoses.get(batchNum)),
                    firstLine, batchSepLine);
            firstLine = batchSepLine + 1;
        }
    }
}
