/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PushButton;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.treeandlist.ConfluenceBean4TreeExtractor;
import pl.fovis.client.treeandlist.ConfluenceLazyRowLoader;
import pl.fovis.client.treeandlist.ConfluenceObjectBeanAsMapStorage;
import pl.fovis.client.treeandlist.IBean4TreeExtractor;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.foxygwtcommons.ITreeBroker;
import pl.fovis.foxygwtcommons.treeandlist.ILazyRowLoaderEx;
import pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase;

/**
 *
 * @author tflorczak
 */
public class BikConfluenceTreeWidget extends AbstractBIKTreeWidget<ConfluenceObjectBean, String> {

    protected Map<String, String> selectedRows = new HashMap<String, String>();
    protected PushButton actionBtn;

    public BikConfluenceTreeWidget(boolean noLinkedNodes, String firstColStyleName,
            IPageDataFetchBroker dataFetchBroker, boolean isWidgetRunFromDialog, String... fields) {
        super(firstColStyleName, dataFetchBroker, isWidgetRunFromDialog, fields);
    }

    public BikConfluenceTreeWidget(boolean noLinkedNodes, Integer treeGridHeight,
            String firstColStyleName, IPageDataFetchBroker dataFetchBroker,
            boolean calcHasNoChildren, String... fields) {
        super(treeGridHeight, firstColStyleName, dataFetchBroker, fields);
    }

    public BikConfluenceTreeWidget(PushButton actionBtn, IPageDataFetchBroker dataFetchBroker, String... fields) {
        super(dataFetchBroker, fields);
        this.actionBtn = actionBtn;
    }

    @Override
    protected Request getBikFilteredNodes(String val, final Integer optSubTreeRootNodeId,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String filterRequestTicket, AsyncCallback<List<ConfluenceObjectBean>> asyncCallback) {
        return BIKClientSingletons.getService().getConfluenceFiltredObjects(val, filterRequestTicket, asyncCallback);
    }

    @Override
    protected Request getBikFilteredNodesWithCondidtion(String val, final Integer optSubTreeRootNodeId,
            Collection<Integer> treeIds, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String filterRequestTicket, AsyncCallback<List<ConfluenceObjectBean>> asyncCallback) {
        return BIKClientSingletons.getService().getConfluenceFiltredObjects(val, filterRequestTicket, asyncCallback);
    }

    @Override
    public boolean isSubTreeFilteringCapable() {
        return false;
    }

    @Override
    protected MapTreeRowAndBeanStorageBase<String, ConfluenceObjectBean> createStorage() {
        return new ConfluenceObjectBeanAsMapStorage(cellWidgetBuilder, isInMultiSelectMode()) {
            @Override
            protected void selectionChanged(String id, String kind, boolean isChecked) {
                if (isChecked) {
                    selectedRows.put(id, kind);
                } else {
                    selectedRows.remove(id);
                }
                updateStateActionBtn();
            }
        };
    }

    private void updateStateActionBtn() {
        actionBtn.setEnabled(!selectedRows.isEmpty());
    }

    @Override
    protected ITreeBroker<ConfluenceObjectBean, String> createTreeBroker() {
        return new ITreeBroker<ConfluenceObjectBean, String>() {
            public String getNodeId(ConfluenceObjectBean n) {
                return n.id;
            }

            public String getParentId(ConfluenceObjectBean n) {
                return n.parentId;
            }
        };
    }

    @Override
    protected ILazyRowLoaderEx<Map<String, Object>, String> createLazyRowLoader(BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode) {
        return new ConfluenceLazyRowLoader(treeGrid, storage, optExtraMode, dataFetchBroker, true);
    }

    @Override
    public IBean4TreeExtractor<ConfluenceObjectBean, String> createBeanExtractor() {
        return new ConfluenceBean4TreeExtractor();
    }

    public Map<String, String> getSelectedRows() {
        return selectedRows;
    }

    public boolean isInMultiSelectMode() {
        return true;
    }
}
