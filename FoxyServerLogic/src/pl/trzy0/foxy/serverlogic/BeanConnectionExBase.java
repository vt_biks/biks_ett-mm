/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.GlobalThreadStopWatch;
import commonlib.LameUtils;
import commonlib.NamedPropsBeanEx;
import commonlib.ThreadedLameLoggingEnvironment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.ExtraPropsQuery;
import pl.trzy0.foxy.commons.FoxyRuntimeException;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.FieldNameConversion;
import simplelib.IContinuationWithReturn;
import simplelib.IIndexedNamedVals;
import simplelib.ILameCollector;
import simplelib.LameRuntimeException;
import simplelib.ListCollector;
import simplelib.MapCollector;
import simplelib.MapCollectorEx;
import simplelib.Pair;
import simplelib.TableColNameToPropNameRenameStrategy;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public abstract class BeanConnectionExBase<BeanBase> implements IBeanConnectionEx<BeanBase> {

    static {
        ThreadedLameLoggingEnvironment.initLameLoggingEnvironment();
    }
    private static final ILameLogger logger = LameUtils.getMyLogger();
    //private static final String RUN_WITHOUT_LOGGING_VARNAME = "RUN_WITHOUT_LOGGING_VARNAME";

    public abstract Integer execCommand(String sqlTxt);

    //public abstract List<Map<String, Object>> execQryEx(String qryTxt, boolean maxOneRow, boolean minOneRow);
    public abstract String valToString(Object val);

    public abstract int readSequenceNextVal(String seqName);

    public abstract void setAutoCommit(boolean val);

    public abstract void finalizeTran(boolean success);

    public String toSqlString(Object o) {
        return valToString(o);
    }

    public Object execQrySingleVal(String qryTxt, boolean allowNoResult) {
        List<Map<String, Object>> resRows = execQryCFN(qryTxt, true, !allowNoResult, null);

        Object res = resRows.isEmpty() ? null : resRows.get(0).values().iterator().next();

//        boolean error = false;
//        Object res = null;
//        try {
//            Statement st = conn.createStatement();
//
//            try {
//                ResultSet rs = st.executeQuery(qryTxt);
//                try {
//                    if (!rs.next()) {
//                        if (!allowNoResult) {
//                            error = true;
//                        }
//                    } else {
//                        res = rs.getObject(1);
//
//                        if (rs.next()) {
//                            error = true;
//                        }
//                    }
//
//                } finally {
//                    rs.close();
//                }
//
//            } finally {
//                st.close();
//            }
//        } catch (SQLException ex) {
//            throw new FoxyRuntimeException("error executing qry or reading result, txt: " + qryTxt, ex);
//        }
//
//        if (error) {
//            throw new FoxyRuntimeException("qry returned less or more than 1 row, txt: " + qryTxt);
//        }
        return res;
    }

//    private Integer valAsInt(Object val) {
//        if (val == null) {
//            return null;
//        }
//        if (val instanceof Integer) {
//            return (Integer) val;
//        }
//        if (val instanceof Long) {
//            return ((Long) val).intValue();
//        }
//        throw new FoxyRuntimeException("cannot convert value " + val + " of class: " + val.getClass().getCanonicalName());
//    }
    public Object execQrySingleVal(String qryTxt) {
        return execQrySingleVal(qryTxt, false);
    }

    public Integer execQrySingleValInteger(String qryTxt, boolean allowNoResult) {
        return sqlNumberToInt(execQrySingleVal(qryTxt, allowNoResult));
    }

    public <T extends BeanBase> T createBeanFromQry(String qryTxt, Class<T> beanClass, boolean allowNoResult) {
        List<Map<String, Object>> rows = execQry(qryTxt);

        int rowCnt = rows.size();

        if (rowCnt > 1 || !allowNoResult && rowCnt == 0) {
            throw new FoxyRuntimeException("single row query returned " + rows.size() + " rows");
        }

        return rowCnt == 0 ? null : BeanMaker.createOneBean(rows.get(0), beanClass);
    }

    public <T extends BeanBase> List<T> createBeansFromQry(String qryTxt,
            final Class<T> beanClass) {
        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
            GlobalThreadStopWatch.startProcess("createBeansFromQry");
        }
        try {

//            List<Map<String, Object>> rows = execQryEx(qryTxt);
//            return BeanMaker.createBeansWithColsAsInFirstOne(rows, beanClass);
//            ListCollectorEx<Map<String, Object>, T> collector = new ListCollectorEx<Map<String, Object>, T>(
//                    new Projector<Map<String, Object>, T>() {
//                        BeanMaker<T> bm;
//
//                        public T project(Map<String, Object> val) {
//
//                            if (bm == null) {
//                                bm = new BeanMaker(val.keySet(), beanClass);
//                            }
//
//                            return bm.createBean(val);
////                            return BeanMaker.createOneBean(val, beanClass);
//                        }
//                    });
//
//            execQry(qryTxt, false, false, collector, null);
//            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
//                GlobalThreadStopWatch.startProcessStage("after execQry, before getList");
//            }
//            return collector.getList();
            final List<T> res = new ArrayList<T>();

            execQryEx(qryTxt, false, false,
                    new Projector<List<String>, Map<String, String>>() {
                        public Map<String, String> project(List<String> val) {
                            return BeanMaker.prepareStandardColumnToPropNameMapping(val, beanClass);
                        }
                    },
                    new ILameCollector<IIndexedNamedVals>() {
                        BeanMaker<T> bm = new BeanMaker<T>(beanClass);

                        public void add(IIndexedNamedVals item) {
                            res.add(bm.createBean(item));
                        }
                    }, new ILameCollector<Map<String, Object>>() {
                        BeanMaker<T> bm;

                        public void add(Map<String, Object> item) {
                            if (bm == null) {
                                bm = new BeanMaker<T>(item.keySet(), beanClass);
                            }
                            res.add(bm.createBean(item));
                        }
                    });

            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.startProcessStage("after execQry, before getList");
            }

            return res;
        } finally {
            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.endProcess();
            }
        }
    }

    //ww: (2013.02.26) jest to deprecated bo nie jest zoptymalizowane jak powyższe
    @Deprecated
    public <K, T extends BeanBase> Map<K, T> createBeansFromQry(String qryTxt,
            final Class<T> beanClass,
            final Projector<? super T, K> proj) {
//        Map<K, Map<String, Object>> rows = execQryEx(qryTxt, false, false, proj);
//        return BeanMaker.createBeansWithColsAsInFirstOne(rows, beanClass);
        MapCollectorEx<K, Map<String, Object>, T> collector = new MapCollectorEx<K, Map<String, Object>, T>(
                null,
                new Projector<Map<String, Object>, T>() {
                    BeanMaker<T> bm;

                    public T project(Map<String, Object> val) {
                        if (bm == null) {
                            bm = new BeanMaker<T>(val.keySet(), beanClass);
                        }

                        return bm.createBean(val);
//                        return BeanMaker.createOneBean(val, beanClass);
                    }
                }, proj);

        execQry(qryTxt, false, false, collector, null);

        return collector.getMap();
    }

    public Integer sqlNumberToInt(Object numVal) {
        if (numVal == null) {
            return null;
        }
        if (numVal instanceof Number) {
            return ((Number) numVal).intValue();
        }
        throw new LameRuntimeException("sqlNumberToInt: cannot convert value of class"
                + numVal.getClass().getName() + " to Integer");
    }

    public List<Map<String, Object>> execQry(String qryTxt) {
        return execQryCFN(qryTxt, false, false, null);
    }

    public List<Map<String, Object>> execQry(String qryTxt, FieldNameConversion optConversion) {
        return execQryCFN(qryTxt, false, false, optConversion);
    }

    public <T> String toSqlString(Iterable<T> items) {
        return BeanConnectionUtils.mergeAsSqlStrings(this, items);
    }

    public String toSqlString(Object... items) {
        return BeanConnectionUtils.mergeAsSqlStrings(this, items);
    }

//    public void finishWorkUnit() {
//        finishWorkUnit(true);
//    }
    public Map<String, Object> execOneRowQry(String qryTxt, boolean allowNoResult,
            FieldNameConversion optConversion) {
        List<Map<String, Object>> list = execQryCFN(qryTxt, true, !allowNoResult, optConversion);

//        if (list.size() > 1) {
//            throw new FoxyRuntimeException("one row qry [" + qryTxt + "] returned more than 1 row (rows: " + list.size() + ")");
//        }
//        if (!allowNoResult && list.size() == 0) {
//            throw new FoxyRuntimeException("one row qry [" + qryTxt + "] returned no rows");
//        }
        return list.isEmpty() ? null : list.get(0);
    }

    public <K> Map<K, Map<String, Object>> execQry(String qryTxt, boolean maxOneRow, boolean minOneRow,
            Projector<Map<String, Object>, K> proj) {
        MapCollector<K, Map<String, Object>> collector = new MapCollector<K, Map<String, Object>>(proj);
        execQry(qryTxt, maxOneRow, minOneRow, collector, null);
        return collector.getMap();
    }

    public List<Map<String, Object>> execQryCFN(String qryTxt, boolean maxOneRow, boolean minOneRow,
            FieldNameConversion optConversion) {
        ListCollector<Map<String, Object>> collector = new ListCollector<Map<String, Object>>();
        execQry(qryTxt, maxOneRow, minOneRow, collector, optConversion);
        return collector.getList();
    }

    @SuppressWarnings("unchecked")
    public <T> void extendWithExtraProps(Iterable<T> items, ExtraPropsQuery<?, T>... epqs) {
        Map<Pair<Projector, Boolean>, String> fks = new HashMap<Pair<Projector, Boolean>, String>();

        List<Map<Object, Map<String, Object>>> extraRows = new ArrayList<Map<Object, Map<String, Object>>>();
        for (ExtraPropsQuery<?, T> epq : epqs) {
            Pair<Projector, Boolean> pbp = new Pair<Projector, Boolean>(epq.fkProj, epq.pkNullable);
            String fk = fks.get(pbp);
            if (fk == null) {
                Set fkRawVals = BaseUtils.projectToSet(items, epq.fkProj, !epq.pkNullable);
                fk = toSqlString(fkRawVals);
                if (logger.isDebugEnabled()) {
                    logger.debug("~~~~~~~ fk=" + fk);
                }
                fks.put(pbp, fk);
            }

            String qryTxt = epq.qryTxt.replace(epq.keysSubstr, fk);
            Projector toStrPkProj = BaseUtils.makeCompoundToStringProjector(epq.pkProj);
            Map<Object, Map<String, Object>> qRes = execQry(qryTxt, false, false, toStrPkProj);
            if (logger.isDebugEnabled()) {
                logger.debug("~~~~~~~ qryTxt=" + qryTxt);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("~~~~~~~ qRes=" + qRes);
            }
            extraRows.add(qRes);
        }

        for (T item : items) {
            for (int i = 0; i < epqs.length; i++) {
                ExtraPropsQuery epq = epqs[i];
                Map<Object, Map<String, Object>> extraR = extraRows.get(i);
                Object fkKeyVal = BaseUtils.safeToString(epq.fkProj.project(item));
                Map<String, Object> rr = extraR.get(fkKeyVal);
                if (logger.isDebugEnabled()) {
                    logger.debug("fkKeyVal=" + fkKeyVal + ", rr=" + rr);
                }
                NamedPropsBeanEx.copyProps(rr, item,
                        epq.propsRenameStrategy == null
                                ? TableColNameToPropNameRenameStrategy.getInstance()
                                : epq.propsRenameStrategy);
            }
        }
    }

    public boolean getRunWithoutLoggingMode() {
        return LameLoggerFactory.getRunWithoutLoggingMode();
    }

    public <T> T runWithoutLogging(boolean noLogging, IContinuationWithReturn<T> cont) {
        return LameLoggerFactory.runWithoutLogging(noLogging, cont);
    }
//    public boolean getRunWithoutLoggingMode() {
//        return ThreadCustomVars.getVars().getBool(RUN_WITHOUT_LOGGING_VARNAME);
//    }
//
//    public <T> T runWithoutLogging(boolean noLogging, IContinuationWithReturn<T> cont) {
//        Object oldVal = ThreadCustomVars.setVar(RUN_WITHOUT_LOGGING_VARNAME, noLogging);
//        try {
//            return cont.doIt();
//        } finally {
//            ThreadCustomVars.setVar(RUN_WITHOUT_LOGGING_VARNAME, oldVal);
//        }
//    }
}
