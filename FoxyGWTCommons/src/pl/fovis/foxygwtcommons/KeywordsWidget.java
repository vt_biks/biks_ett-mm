/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import java.util.HashSet;
import java.util.Set;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class KeywordsWidget extends Composite implements IKeywordCollection {

    private Set<OneKeywordWidget> list;
    private FlowPanel main;
    private PushButton btnAdd;
    private IParametrizedContinuation<IKeywordCollection> newTagSelector;
    private boolean showPlusButton;
    private boolean isAdd;

    public KeywordsWidget(boolean showPlusButton, Set<String> tags, IParametrizedContinuation<IKeywordCollection> newTagSelector) {
        this.showPlusButton = showPlusButton;
        this.newTagSelector = newTagSelector;
        buildWidgets(tags);
        initWidget(main);
    }

    private void buildWidgets(Set<String> tags) {
        main = new FlowPanel();
        list = new HashSet<OneKeywordWidget>();
        if (showPlusButton) {
            btnAdd = new PushButton(new Image("images/plus.png" /* I18N: no */));
            Style style = btnAdd.getElement().getStyle();
            style.setMargin(0, Unit.PX);
            style.setPadding(0, Unit.PX);
            GWTUtils.setStyleAttribute(btnAdd.getElement(), "background" /* I18N: no */, "none" /* I18N: no */);
            style.setBorderWidth(0, Unit.PX);
            btnAdd.addClickHandler(new ClickHandler() {

                public void onClick(ClickEvent event) {
                    isAdd = true;
                    newTagSelector.doIt(KeywordsWidget.this);
                }
            });
        }

        if (tags != null) {
            for (String string : tags) {
                OneKeywordWidget w = new OneKeywordWidget(this, string);

                list.add(w);
            }
        }
        update();
    }

    public void addTag(OneKeywordWidget tag) {
        list.add(tag);
        update();
    }

    private void update() {
        main.clear();

        for (OneKeywordWidget oneTag : list) {
            main.add(oneTag);
        }
        if (showPlusButton) {
            main.add(btnAdd);
        }
    }

    public void deleteOneTag(String name) {
        for (OneKeywordWidget oneTag : list) {
            if (oneTag.getName().equals(name)) {
                list.remove(oneTag);
                break;
            }
        }
        update();
        if (showPlusButton == false) {
            isAdd = false;
            newTagSelector.doIt(KeywordsWidget.this);
        }
    }

    public Set<String> getKeywords() {
        Set<String> tmp = new HashSet<String>();
        for (OneKeywordWidget tag : list) {
            tmp.add(tag.getName());
        }
        return tmp;
    }

    public void addKeyword(String tag) {
        HashSet<String> listTmp = new HashSet<String>();
        for (OneKeywordWidget oneTag : list) {
            listTmp.add(oneTag.getName());
        }
        if (!listTmp.contains(tag)) {
            list.add(new OneKeywordWidget(this, tag));
            update();
        }
    }

    public void addEvent() {
        isAdd = true;
        newTagSelector.doIt(KeywordsWidget.this);
    }

    public boolean getType() {
        return isAdd;
    }
}
