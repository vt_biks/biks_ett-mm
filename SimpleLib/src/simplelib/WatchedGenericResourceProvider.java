/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author pmielanczuk
 */
public class WatchedGenericResourceProvider<T> implements IWatchedGenericResourceProvider<T> {

    protected IGenericResourceProvider<T> baseProvider;
    protected Map<String, IWatchedResourceFromProvider<T>> watchedResources = new HashMap<String, IWatchedResourceFromProvider<T>>();

    public WatchedGenericResourceProvider(IGenericResourceProvider<T> baseProvider) {
        this.baseProvider = baseProvider;
    }

    public boolean hasBeenModifiedRecently() {
        for (Entry<String, IWatchedResourceFromProvider<T>> watchedResource : watchedResources.entrySet()) {
            if (watchedResource.getValue().hasBeenModifiedRecently()) {
//                System.out.println("hasBeenModifiedRecently! fileName=" + watchedResource.getKey());
//                System.out.println("hasBeenModifiedRecently! className=" + watchedResource.getValue().getClass());                
                return true;
            }
        }
        return false;
    }

    protected IWatchedResourceFromProvider<T> getWatchedResource(String resourceName) {
        IWatchedResourceFromProvider<T> res = watchedResources.get(resourceName);
        if (res == null) {
            res = new WatchedResourceFromProvider<T>(new ResourceFromProvider<T>(baseProvider, resourceName));
            watchedResources.put(resourceName, res);
        }
        return res;
    }

    public boolean hasBeenModifiedRecently(String resourceName) {
        return getWatchedResource(resourceName).hasBeenModifiedRecently();
    }

    public T gainResource(String resourceName) {
        return getWatchedResource(resourceName).gainResource();
    }

    public long getLastModifiedOfResource(String resourceName) {
        return getWatchedResource(resourceName).getLastModifiedOfResource();
    }
}
