/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.StatisticsBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author mmastalerz
 */
public class StatisticsPage extends StatisticsPageBase {

    protected String getCounterName(StatisticsBean s) {
        return s.description;
    }

    public void createGridColumnNames() {
        grid.setWidget(0, 0, getHtmlAndStyle("Statistics" /* i18n: no */ + "-tableTitleName", I18n.nazwa.get() /* I18N:  */));
        grid.setWidget(0, 1, getHtmlAndStyle("Statistics" /* i18n: no */ + "-tableTitle", I18n.ostatniDzien.get() /* I18N:  */));
        grid.setWidget(0, 2, getHtmlAndStyle("Statistics" /* i18n: no */ + "-tableTitle", I18n.ostatniTydzien.get() /* I18N:  */));
        grid.setWidget(0, 3, getHtmlAndStyle("Statistics" /* i18n: no */ + "-tableTitle", I18n.ostatnie30Dni.get() /* I18N:  */));
        grid.setWidget(0, 4, getHtmlAndStyle("Statistics" /* i18n: no */ + "-tableTitle", "Total" /* I18N: no */));
        grid.setWidget(0, 5, getHtmlAndStyle("Statistics" /* i18n: no */ + "-tableTitle", I18n.najczesciejUzywanyParametr.get() /* I18N:  */));
        grid.getRowFormatter().setStyleName(0, "Statistics" /* i18n: no */ + "-titleRow");
    }

    @Override
    public void show(VerticalPanel vp) {

        vp.clear();
        grid.clear();
        createGridColumnNames();

        BIKClientSingletons.getService().getStatisticsDict(new StandardAsyncCallback<List<StatisticsBean>>() {
            @Override
            public void onSuccess(List<StatisticsBean> result) {
                int row = 1;

                for (StatisticsBean sb : result) {
                    sb.treePath = BIKClientSingletons.getMenuPathForTreeCode(sb.counterName);
                }

                Collections.sort(result, new Comparator<StatisticsBean>() {
                    @Override
                    public int compare(StatisticsBean o1, StatisticsBean o2) {
                        String p1 = BaseUtils.safeToStringDef(o1.treePath, "");
                        String p2 = BaseUtils.safeToStringDef(o2.treePath, "");
                        return p1.compareTo(p2);
                    }
                });
                //drzewa na zakładce
                Map<String, List<StatisticsBean>> treeInTabMap = new HashMap<String, List<StatisticsBean>>();
                for (StatisticsBean sb : result) {
                    String tabCode = getTabCodeForTree(sb.counterName);
                    if (!treeInTabMap.containsKey(tabCode)) {
                        treeInTabMap.put(tabCode, new ArrayList<StatisticsBean>());
                    }
                    treeInTabMap.get(tabCode).add(sb);
                }
                //Kolejność zakładek
                for (MenuNodeBean mnb : BIKClientSingletons.getMenuNodes()) {

                    if (mnb.parentNodeId == null) {

                        List<StatisticsBean> sbs = treeInTabMap.get(mnb.code);
                        if (!BaseUtils.isCollectionEmpty(sbs)) {
                            for (StatisticsBean sb : sbs) {
                                addRow(row, sb);
                                row++;
                                result.remove(sb);
                            }
                        }
                    }
                }
                //Wyszukiwarka,Logowanie i liczba uzytkownikow
                for (StatisticsBean sb : result) {
                    addRow(row, sb);
                    row++;
                }
            }
        }
        );
        vp.add(grid);

    }

    protected void addRow(int row, StatisticsBean sb) {
        int col = 0;
        grid.setWidget(row, col++, getHtmlAndStyle("Statistics-title", BaseUtils.isStrEmpty(sb.treePath) ? getCounterName(sb) : BaseUtils.dropOptionalSuffix(sb.treePath.trim(), "»")));
        grid.setWidget(row, col++, getHtmlAndStyle(sb.dayCnt + ""));
        grid.setWidget(row, col++, getHtmlAndStyle(sb.weekCnt + ""));
        grid.setWidget(row, col++, getHtmlAndStyle(sb.thirtyCnt + ""));
        grid.setWidget(row, col++, getHtmlAndStyle((sb.bestParameter == null ? "" : sb.allCnt + "/") + sb.total));
        grid.setWidget(row, col++, getHtmlAndStyle(sb.bestParameter == null ? " " : sb.bestParameter));
        grid.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
    }

//    public String getCaptionDeprecated() {
//        return I18n.statystyki.get() /* I18N:  */;
//    }
    @Override
    public String getId() {
        return "admin:dict:statistics";
    }

    protected String getTabCodeForTree(String code) {
        List<MenuNodeBean> mnbs = BIKClientSingletons.getMenuNodes();
        Map<String, MenuNodeBean> menuByCodeMap;
        Map<Integer, MenuNodeBean> menuByIdMap;
        menuByCodeMap = BaseUtils.projectToMap(mnbs, new BaseUtils.Projector<MenuNodeBean, String>() {

            @Override
            public String project(MenuNodeBean val) {
                return val.code;
            }
        });
        menuByIdMap = BaseUtils.makeBeanMap(mnbs);
        MenuNodeBean mnb = menuByCodeMap.get("$" + code);

        if (mnb == null) {
            return "(" + I18n.brak.get() /* I18N:  */ + ")";
        }

        String tabCode = "";
        while (true) {
            if (mnb.parentNodeId == null) {
                return mnb.code;
            }
            mnb = menuByIdMap.get(mnb.parentNodeId);
            if (mnb == null) {
                break;
            }
            tabCode = mnb.code;
        }
        return tabCode;
    }
}
