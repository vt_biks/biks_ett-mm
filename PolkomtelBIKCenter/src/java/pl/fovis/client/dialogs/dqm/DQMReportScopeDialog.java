/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.Date;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class DQMReportScopeDialog extends BaseActionOrCancelDialog {

    protected IParametrizedContinuation<Pair<Date, Date>> saveCont;
//    protected DateBox dpFromDate;
    protected DateBox dpToDate;

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzZakresRaportu.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.wybierz.get();
    }

    @Override
    protected void buildMainWidgets() {
        HorizontalPanel mainPanel = new HorizontalPanel();
        mainPanel.setSpacing(5);
        DateTimeFormat dateFormat = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_SHORT);
//        mainPanel.add(new HTML("&nbsp;" + I18n.okresOd.get() + "&nbsp;&nbsp;"));
//        mainPanel.add(dpFromDate = new DateBox());
//        dpFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
//        dpFromDate.setWidth("100px");
//        mainPanel.add(new HTML("&nbsp;&nbsp;" + I18n.okresDo.get() + "&nbsp;&nbsp;"));
        mainPanel.add(new HTML(I18n.raportNaDzien.get()));
        mainPanel.add(dpToDate = new DateBox());
        dpToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
        dpToDate.setWidth("100px");
        main.add(mainPanel);
    }

    @Override
    protected void doAction() {
        saveCont.doIt(new Pair<Date, Date>(SimpleDateUtils.newDate(2000, 1, 1), dpToDate.getValue()));
    }

    @Override
    protected boolean doActionBeforeClose() {
//        Date periodFrom = dpFromDate.getValue();
        Date periodTo = dpToDate.getValue();
//        if (periodFrom == null) {
//            new SimpleInfoDialog().buildAndShowDialog(I18n.okresOdNieMozeBycPusty.get(), I18n.blad.get(), null);
//            return false;
//        }
        if (periodTo == null) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.okresDoNieMozeBycPusty.get(), I18n.blad.get(), null);
            return false;
        }
//        if (periodFrom.after(periodTo)) {
//            new SimpleInfoDialog().buildAndShowDialog(I18n.bladWOkresachObowiazywania.get(), I18n.blad.get(), null);
//            return false;
//        }
        return true;
    }

    public void buildAndShowDialog(IParametrizedContinuation<Pair<Date, Date>> saveCont) {
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }
}
