/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.biadmin;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.Comparator;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.FilterGridPageBase;
import pl.fovis.common.AuditBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.SimpleDateUtils;

/**
 *
 * @author bfechner
 */
public class BIAAuditPage extends FilterGridPageBase<AuditBean> {

    protected VerticalPanel mainVp;
    protected PushButton refreshBtn;
    protected BikCustomButton exportToExcelPb;

    @Override
    protected int getGridHeight() {
        return 1540;
    }

    @Override
    protected void initTableColumns(ColumnSortEvent.ListHandler<AuditBean> sortHandler) {
        /* Lokalziacja */
        Column<AuditBean, String> pathColumn
                = new Column<AuditBean, String>(new TextCell()) {
                    @Override
                    public String getValue(AuditBean object) {
                        return object.getPath();
                    }
                };
        pathColumn.setSortable(true);
        sortHandler.setComparator(pathColumn, new Comparator<AuditBean>() {
            @Override
            public int compare(AuditBean o1, AuditBean o2) {
                return o1.getPath().compareTo(o2.getPath()
                );
            }
        });
        dataGrid.addColumn(pathColumn, I18n.lokalizacja.get());
        dataGrid.setColumnWidth(pathColumn, "300px"/*, Unit.PX*/);

        /* Nazwa */
        Column<AuditBean, String> nameColumn
                = new Column<AuditBean, String>(new TextCell()) {
                    @Override
                    public String getValue(AuditBean object) {
                        return object.getName();
                    }
                };
        nameColumn.setSortable(true);
        sortHandler.setComparator(nameColumn, new Comparator<AuditBean>() {
            @Override
            public int compare(AuditBean o1, AuditBean o2) {
                return o1.getName().compareTo(o2.getName()
                );
            }
        });
        dataGrid.addColumn(nameColumn, I18n.nazwa.get());
        dataGrid.setColumnWidth(nameColumn, "180px"/*, Unit.PX*/);

        /* Cuid */
        Column<AuditBean, String> cuidColumn
                = new Column<AuditBean, String>(new TextCell()) {
                    @Override
                    public String getValue(AuditBean object) {
                        return object.getCuid();
                    }
                };
        cuidColumn.setSortable(true);
        sortHandler.setComparator(cuidColumn, new Comparator<AuditBean>() {
            @Override
            public int compare(AuditBean o1, AuditBean o2) {
                return o1.getCuid().compareTo(o2.getCuid());
            }
        });
        dataGrid.addColumn(cuidColumn, I18n.scheduleCUID.get());
        dataGrid.setColumnWidth(cuidColumn, "200px"/*, Unit.PX*/);

        /*Ilość odswiezen w meisiacu  */
        Column<AuditBean, String> refreshCntColumn
                = new Column<AuditBean, String>(new TextCell()) {
                    @Override
                    public String getValue(AuditBean object) {
                        return BaseUtils.safeToStringDef(object.getRefreshCnt(), "0");
                    }
                };
        refreshCntColumn.setSortable(true);
        sortHandler.setComparator(refreshCntColumn, new Comparator<AuditBean>() {
            @Override
            public int compare(AuditBean o1, AuditBean o2) {
                return BaseUtils.safeToStringDef(o1.getRefreshCnt(), "0").compareTo(BaseUtils.safeToStringDef(o2.getRefreshCnt(), "0")
                );
            }
        });

        SafeHtml auditRefreshNameWithTitle = new SafeHtmlBuilder().appendHtmlConstant("<span title='"
                + new SafeHtmlBuilder().appendEscaped(I18n.auditRefreshCnt30day.get()).toSafeHtml().asString() + "'>" + I18n.auditRefreshCnt.get() + "</span>").toSafeHtml();

        dataGrid.addColumn(refreshCntColumn, auditRefreshNameWithTitle);
        dataGrid.setColumnWidth(refreshCntColumn, "150px"/*, Unit.PX*/);
        /*ilość otwarć w miesiącu*/
        Column<AuditBean, String> viewCntColumn
                = new Column<AuditBean, String>(new TextCell()) {
                    @Override
                    public String getValue(AuditBean object) {
                        return BaseUtils.safeToStringDef(object.getViewCnt(), "0");
                    }
                };
        viewCntColumn.setSortable(true);
        sortHandler.setComparator(viewCntColumn, new Comparator<AuditBean>() {
            @Override
            public int compare(AuditBean o1, AuditBean o2) {

                return BaseUtils.safeToStringDef(o1.getViewCnt(), "0").compareTo(BaseUtils.safeToStringDef(o2.getViewCnt(), "0")
                );
            }
        });
        SafeHtml auditViewCntNameWithTitle = new SafeHtmlBuilder().appendHtmlConstant("<span title='"
                + new SafeHtmlBuilder().appendEscaped(I18n.auditViewCnt30day.get()).toSafeHtml().asString() + "'>" + I18n.auditViewCnt.get() + "</span>").toSafeHtml();

        dataGrid.addColumn(viewCntColumn, auditViewCntNameWithTitle);
        dataGrid.setColumnWidth(viewCntColumn, "150px"/*, Unit.PX*/);

        /*ostatnie odswiezenie*/
        Column<AuditBean, String> lastRefreshColumn = new Column<AuditBean, String>(new TextCell()) {
            @Override
            public String getValue(AuditBean object) {
                return SimpleDateUtils.toCanonicalString(object.getRefreshLast());
            }
        };
        lastRefreshColumn.setSortable(true);
        sortHandler.setComparator(lastRefreshColumn, new Comparator<AuditBean>() {
            @Override
            public int compare(AuditBean o1, AuditBean o2) {
                if (o1.getRefreshLast() == null) {
                    if (o2.getRefreshLast() == null) {
                        return 0; //equal
                    } else {
                        return -1; // null is before other strings
                    }
                } else // this.member != null
                if (o2.getRefreshLast() == null) {
                    return 1;  // all other strings are after null
                } else {
                    return o1.getRefreshLast().compareTo(o2.getRefreshLast());
                }
            }
        });
        dataGrid.addColumn(lastRefreshColumn, I18n.auditLastRefresh.get());
        dataGrid.setColumnWidth(lastRefreshColumn, "200px"/*, Unit.PX*/);

//        /*ostatnie otworzenie*/
        Column<AuditBean, String> lastViewCntColumn = new Column<AuditBean, String>(new TextCell()) {
            @Override
            public String getValue(AuditBean object) {
                return SimpleDateUtils.toCanonicalString(object.getViewLast());
            }
        };
        lastViewCntColumn.setSortable(true);
        sortHandler.setComparator(lastViewCntColumn, new Comparator<AuditBean>() {
            @Override
            public int compare(AuditBean o1, AuditBean o2) {
                if (o1.getViewLast() == null) {
                    if (o2.getViewLast() == null) {
                        return 0; //equal
                    } else {
                        return -1; // null is before other strings
                    }
                } else // this.member != null
                if (o2.getViewLast() == null) {
                    return 1;  // all other strings are after null
                } else {
                    return o1.getViewLast().compareTo(o2.getViewLast());
                }
            }
        });
        dataGrid.addColumn(lastViewCntColumn, I18n.auditLastView.get());
        dataGrid.setColumnWidth(lastViewCntColumn, "200px"/*, Unit.PX*/);

        /* Ostatnio odswiezony przez */
        Column<AuditBean, String> userLastRefreshColumn
                = new Column<AuditBean, String>(new TextCell()) {
                    @Override
                    public String getValue(AuditBean object) {
                        return object.getUserRefreshLast();
                    }
                };
        userLastRefreshColumn.setSortable(true);
        sortHandler.setComparator(userLastRefreshColumn, new Comparator<AuditBean>() {
            @Override
            public int compare(AuditBean o1, AuditBean o2) {
                return BaseUtils.coalesce(o1.getUserRefreshLast(), "").compareTo(BaseUtils.coalesce(o2.getUserRefreshLast(), "")
                );
            }
        });
        dataGrid.addColumn(userLastRefreshColumn, I18n.auditUserLastRefresh.get());
        dataGrid.setColumnWidth(userLastRefreshColumn, "180px"/*, Unit.PX*/);

        /* Ostatnio otworzony przez */
        Column<AuditBean, String> userLastViewColumn
                = new Column<AuditBean, String>(new TextCell()) {
                    @Override
                    public String getValue(AuditBean object) {
                        return object.getUserViewLast();
                    }
                };
        userLastViewColumn.setSortable(true);
        sortHandler.setComparator(userLastViewColumn, new Comparator<AuditBean>() {
            @Override
            public int compare(AuditBean o1, AuditBean o2) {
                return BaseUtils.coalesce(o1.getUserViewLast(), "").compareTo(BaseUtils.coalesce(o2.getUserViewLast(), "")
                );
            }
        });
        dataGrid.addColumn(userLastViewColumn, I18n.auditUserLastView.get());
        dataGrid.setColumnWidth(userLastViewColumn, "180px"/*, Unit.PX*/);

        exportToExcelPb = createExportResultsBtn();
        exportToExcelPb.setVisible(false);
        exportToExcelPb.addStyleName("BIAdmin");

    }

    @Override
    protected void refreshActionButtons() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Widget buildWidgets() {
        mainVp = new VerticalPanel();
        mainVp.setWidth("100%");
        rightVp = new VerticalPanel();
        rightVp.setWidth("100%");
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(rightVp, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 130);
        initDataGrid();
        createActionButtons();
        addGridWIthFooterToRightPanel();
        optAddActionButtonsToPanel(actionPanel, actionHorizontalPanel);
        refreshActionButtons();
        mainVp.add(rightVp);
        return mainVp;
    }

    @Override
    protected void createFilters(final VerticalPanel leftVp) {
        //no-op
    }

    @Override
    protected void createActionButtons() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void optAddActionButtonsToPanel(HorizontalPanel actionPanel, HorizontalPanel actionHorizontalPanel) {
        refreshBtn = new PushButton(I18n.odswiez.get());
        NewLookUtils.makeCustomPushButton(refreshBtn);
        refreshBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                refreshGrid();
            }
        });
        actionPanel.add(refreshBtn);
        actionHorizontalPanel.add(exportToExcelPb);
    }

    @Override
    public String getId() {
        return "admin:biadmin:audyt";
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    protected void refreshGrid() {
        showLoadingWidget();
        refreshBtn.setEnabled(false);
        BIKClientSingletons.getService().getBIAAuditInformation(new StandardAsyncCallback<List<AuditBean>>() {

            @Override
            public void onSuccess(List<AuditBean> result) {
                showDataGrid();
                List<AuditBean> list = dataProvider.getList();
                list.clear();
                list.addAll(result);
//                   if (result.size() >= 1000) {
//                    new SimpleInfoDialog().buildAndShowDialog(I18n.schedulePobranoDuzoRekordow.get(), null, null);
//                }

                dataProvider.refresh();
                selectionModel.clear();
                refreshActionButtons();
                dataGrid.redrawHeaders();
                refreshBtn.setEnabled(true);
                exportToExcelPb.setVisible(!list.isEmpty());
            }

            @Override
            public void onFailure(Throwable caught) {
                showDataGrid();
                refreshBtn.setEnabled(true);
                super.onFailure(caught);
            }

        });

    }

    @Override
    protected void showDataGrid() {
        rightVp.clear();
        rightVp.add(actionHorizontalPanel);
        rightVp.add(dataGrid);
    }

    private BikCustomButton createExportResultsBtn() {
        return new BikCustomButton(I18n.eksportWynikow.get() /* I18N:  */, "importCsv" /* I18N: no */,
                new IContinuation() {

                    @Override
                    public void doIt() {

                        StringBuilder sb = new StringBuilder(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet());
                        sb.append("?").append(BIKConstants.BIA_AUDIT_RESULTS_EXPORT_PARAM_NAME).append("=").append(BIKConstants.EXPORT_FORMAT_XLSX).append("&")
                        .append(BIKConstants.BIA_AUDIT_PHRASE_PARAM_NAME).append("=").append(I18n.audit.get());
                        Window.open(sb.toString(), "_blank", "");
                    }
                });
    }
}
