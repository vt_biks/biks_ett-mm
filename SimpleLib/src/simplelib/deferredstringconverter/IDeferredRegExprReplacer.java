/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.deferredstringconverter;

import java.util.Collection;
import simplelib.IStringMatcher;

/**
 *
 * @author pmielanczuk
 */
public interface IDeferredRegExprReplacer<SM extends IStringMatcher, T> {

    public SM[] makeMatchers(String s);

    public T getDeferredReplacement(SM m);

    public Collection<String> resolveReplacements(Collection<T> replacements);
}
