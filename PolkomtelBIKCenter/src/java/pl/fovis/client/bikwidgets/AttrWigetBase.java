/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author bfechner
 */
public abstract class AttrWigetBase {

    public abstract Widget getWidget();
//

    public abstract String getValue();

    protected ScrollPanel createScrolledPanel(VerticalPanel vp, int objsCount) {
        ScrollPanel scollPanel = new ScrollPanel(vp);
        if (objsCount > 20) {
            scollPanel.setHeight("380px");
        }
        return scollPanel;
    }

    public abstract int getWidgetHeight();

    public void setEnable(boolean isEnable, String attrName) {

    }

}
