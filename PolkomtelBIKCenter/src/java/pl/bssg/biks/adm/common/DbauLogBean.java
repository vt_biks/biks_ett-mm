/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author pmielanczuk
 */
public class DbauLogBean implements Serializable {
    /*
     id int identity not null primary key,
     date_added datetime not null default getdate(),
     proc_id int not null references dbau_process (id),
     msg varchar(max) not null
     */

    public int id;
    public Date dateAdded;
    public int procId;
    public String msg;
}
