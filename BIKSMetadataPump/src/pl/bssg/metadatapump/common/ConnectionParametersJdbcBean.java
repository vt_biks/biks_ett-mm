/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

/**
 *
 * @author ctran
 */
public class ConnectionParametersJdbcBean extends ConnectionParametersDBServerBean {

    public String jdbcUrl;
    public Integer jdbcSourceId;
    public String jdbcSourceName;
    public String jdbcDriverName;
    public String jdbcSourceTemplate;
    public String query;
    public String queryJoined;
    public String treeCode;
    public String folderPath;
    public String adhocSql;

}
