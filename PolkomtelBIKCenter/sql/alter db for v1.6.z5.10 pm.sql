exec sp_check_version '1.6.z5.10';
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
--
-- 1. zmiana typ�w kolumn seed_value, increment_value
--    w tabelach bik_mssql_column_extradata i
--    aaa_mssql_column_extradata z int na bigint
-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

alter table aaa_mssql_column_extradata alter column seed_value bigint;
go
alter table aaa_mssql_column_extradata alter column increment_value bigint;
go

alter table bik_mssql_column_extradata alter column seed_value bigint;
go
alter table bik_mssql_column_extradata alter column increment_value bigint;
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

exec sp_update_version '1.6.z5.10', '1.6.z5.11';
go
