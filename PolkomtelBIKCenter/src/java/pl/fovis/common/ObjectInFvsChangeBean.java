/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.List;

/**
 *
 * @author bfechner
 */
public class ObjectInFvsChangeBean extends ObjectInFvsBean implements IsSerializable {

    public int cntNew;
    public int cntUpdate;
    public int cntDelete;
    public int cntCut;
    public int cntPast;
    public int actionType;
    public boolean isDeleted;
    public String branchIds;
    public List<String> ancestorNodePath;
    public String treeCode;
    public Double similarityScore;
}
