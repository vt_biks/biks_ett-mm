﻿exec sp_check_version '1.3.0.5';
go

exec sp_add_menu_node @parent_obj_id = '@Dictionary', @name = '@', @obj_id = '$DictionaryDWH';
go

insert into bik_tree (name,node_kind_id,code,tree_kind,is_hidden,is_in_ranking,branch_level_for_statistics,is_built_in)values ('Słowniki DWH',null,'DictionaryDWH','DictionaryDWH',0,0,0,1)
go

insert into bik_node_kind (code,caption,icon_name,tree_kind,is_folder,search_rank,is_leaf_for_statistic)
values ('DWHDictionaries','Słowniki DWH','dwhDictionaries','DictionaryDWH',1,17,0)
go

insert into bik_node_kind (code,caption,icon_name,tree_kind,is_folder,search_rank,is_leaf_for_statistic)
values ('DWHDictionary','Słownik DWH','dwhDictionary','DictionaryDWH',1,17,0)
go

insert into bik_node_kind (code,caption,icon_name,tree_kind,is_folder,search_rank,is_leaf_for_statistic)
values ('DWHCategorization','Kategoryzacja DWH','dwhCategorization','DictionaryDWH',1,17,0)
go

insert into bik_node_kind (code,caption,icon_name,tree_kind,is_folder,search_rank,is_leaf_for_statistic)
values ('DWHCategory','Kategoria DWH','dwhCategory','DictionaryDWH',1,17,0)
go

exec sp_add_menu_node @parent_obj_id = '$DictionaryDWH', @name = '@', @obj_id = '&DWHDictionary';
exec sp_add_menu_node @parent_obj_id = '&DWHDictionary', @name = '@', @obj_id = '&DWHCategorization';
exec sp_add_menu_node @parent_obj_id = '&DWHCategorization', @name = '@', @obj_id = '&DWHCategory';
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

exec sp_update_version '1.3.0.5', '1.3.0.6';
go