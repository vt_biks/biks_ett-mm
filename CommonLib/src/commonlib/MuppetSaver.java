/*
 * MuppetMaker.java
 *
 * Created on 28 grudzie� 2006, 09:57
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package commonlib;

import java.util.Set;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class MuppetSaver {

    static ILameLogger logger = LameLoggerFactory.getLogger(MuppetSaver.class);
    //LameLogger.getClassLogger();
    private StringBuilder buf;

    private MuppetSaver() {
        buf = new StringBuilder();
    }

    public static StringBuilder getMuppetAsXML(Object muppet) {
        MuppetSaver muppetSaver = new MuppetSaver();
        muppetSaver.saveMuppet(muppet);
        return muppetSaver.buf;
    }

    /**
     * Daje zbi�r publicznych property klasy
     */
    private Set<String> getMuppetFields(Object muppet) {
        return MuppetMerger.getMuppetProps(muppet, 4);
    }

    private void appendXMLValue(String name, Object val) {
        buf.append(name + "=\"" + val + "\"");
    }

    private void appendXMLTag(String name, boolean isClosing) {
        buf.append("<" + (isClosing ? "/" : "") + name + ">\n");
    }

    private void saveMuppet(Object obj) {
        buf.append("<?xml version=\"1.0\" encoding=\"windows-1250\"?>\n");
        appendMuppetTags(obj, 0);
    }

    private void appendMuppetTags(Object obj, int level) {
        String space = LameUtils.replicateStr(" ", level * 2);
        String mainTag = obj.getClass().getSimpleName();
        buf.append(space + "<" + mainTag);
        for (String field : getMuppetFields(obj)) {
            Object object = MuppetMerger.getProperty(obj, field);
            if (object != null) {
                if (!object.getClass().isArray()) {
                    buf.append(" ");
                    appendXMLValue(field, object);
                }
            }
        }

        boolean first = true;

        for (String field : getMuppetFields(obj)) {
            Object object = MuppetMerger.getProperty(obj, field);
            if (object != null) {
                if (object.getClass().isArray() && ((Object[]) object).length != 0) {
                    if (first) {
                        buf.append(">\n");
                        first = false;
                    }
                    String arrayTag = field;
                    buf.append(space + " ");
                    appendXMLTag(arrayTag, false);
                    for (Object arrayObj : (Object[]) object) {
                        if (arrayObj != null) {
                            appendMuppetTags(arrayObj, level + 1);
                        }
                    }
                    buf.append(space + " ");
                    appendXMLTag(arrayTag, true);
                }
            }
        }
        if (first) {
            buf.append("/>\n");
        } else {
            buf.append(space + "</" + mainTag + ">\n");
        }
    }
}
