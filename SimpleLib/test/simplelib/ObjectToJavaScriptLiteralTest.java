/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wezyr
 */
public class ObjectToJavaScriptLiteralTest {

    public ObjectToJavaScriptLiteralTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testConvertObjectMap1() {
        Map<String, Integer> m = new LinkedHashMap<String, Integer>();
        m.put("ala", 10);
        m.put("kot", 20);
        m.put(null, -666);
        String res = JsonConverter.convertObject(m);
        assertEquals("{\"ala\":10,\"kot\":20,null:-666}", res);
    }

    @Test
    public void testConvertObjectMap2() {
        Map<String, Double> m = new LinkedHashMap<String, Double>();
        m.put("ala", 10.0);
        m.put("kot", 20.0);
        m.put(null, -666.1);
        String res = JsonConverter.convertObject(m);
        assertEquals("{\"ala\":10.0,\"kot\":20.0,null:-666.1}", res);
    }

    @Test
    public void testConvertObjectMap3() {
        Map<String, Object> m = new LinkedHashMap<String, Object>();
        m.put("ala", 10);
        m.put("kot", 20.0);
        m.put(null, new ArrayList<String>());
        List<String> list = new ArrayList<String>();
        list.add("marchew");
        list.add(null);
        list.add("kapusta");
        m.put("pies", list);
        String res = JsonConverter.convertObject(m);
        assertEquals("{\"ala\":10,\"kot\":20.0,null:[],\"pies\":[\"marchew\",null,\"kapusta\"]}", res);
    }
}