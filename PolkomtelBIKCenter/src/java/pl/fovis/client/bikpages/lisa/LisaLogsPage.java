/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.lisa;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.Date;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.BikPageMinimalBase;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaConstants;
import pl.fovis.common.lisa.LisaGridParamBean;
import pl.fovis.common.lisa.LisaLogBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.grid.BeanGrid;
import pl.fovis.foxygwtcommons.grid.BeanGridPager;
import pl.fovis.foxygwtcommons.grid.IBeanDisplay;
import pl.fovis.foxygwtcommons.grid.IBeanProvider;
import simplelib.INamedTypedPropsBeanBroker;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tbzowka
 */
public class LisaLogsPage extends BikPageMinimalBase {

    protected BeanGrid<LisaLogBean> logsGrid;
    protected VerticalPanel vp;

    @Override
    protected Widget buildWidgets() {

        vp = new VerticalPanel();
        getLogs();
        ScrollPanel scPanel = new ScrollPanel(vp);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(scPanel, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT);

        return scPanel;
    }

    public void getLogs() {
        final LisaLogDisplay<LisaLogBean> lisaLogDisplay = new LisaLogDisplay<LisaLogBean>();

        logsGrid = new BeanGrid<LisaLogBean>(new IBeanProvider<LisaLogBean>() {
            public void getBeans(final int offset, final int limit, Iterable<String> sortCols, Iterable<String> descendingCols, final IParametrizedContinuation<Pair<? extends Iterable<LisaLogBean>, Integer>> showBeansCont) {

                LisaGridParamBean params = new LisaGridParamBean(offset, limit, null, null);
                BIKClientSingletons.getLisaService().getLisaLogs(params, new StandardAsyncCallback<List<LisaLogBean>>() {
                    public void onSuccess(List<LisaLogBean> result) {
                        Pair<List<LisaLogBean>, Integer> p = new Pair<List<LisaLogBean>, Integer>();
                        p.v1 = result;
                        p.v2 = result.size();

                        showBeansCont.doIt(p);
                    }
                });
            }
        }, lisaLogDisplay, LisaConstants.GRID_PAGE_SIZE, LisaLogDisplay.COLUMNS_TIME_ADDED, LisaLogDisplay.COLUMNS_TERADATA_LOGIN, LisaLogDisplay.COLUMNS_DICTIONARY_NAME, LisaLogDisplay.COLUMNS_INFO, LisaLogDisplay.COLUMNS_SYSTEM_USER_NAME);

        logsGrid.setHeaderStyleName("lisaDictHeader");
        BeanGridPager<LisaLogBean> bgp = new BeanGridPager<LisaLogBean>(logsGrid);
        bgp.setUknownPageCountMode(true);
        vp.add(bgp);
        vp.add(logsGrid);
    }

    public String getId() {
        return "admin:lisa:logs";
    }

    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
    }

    private class LisaLogDisplay<T extends LisaLogBean> implements IBeanDisplay<T> {

        public static final String COLUMNS_DICTIONARY_NAME = "dictionaryName";
        public static final String COLUMNS_INFO = "info" /* i18n: no:column-name */;
        public static final String COLUMNS_SYSTEM_USER_NAME = "systemUserName";
        public static final String COLUMNS_TERADATA_LOGIN = "teradataLogin";
        public static final String COLUMNS_TIME_ADDED = "timeAdded";
        private final String DATE_FORMAT = "yyyy-MM-dd HH:mm";
        private final DateTimeFormat dtf = DateTimeFormat.getFormat(DATE_FORMAT);
        private final INamedTypedPropsBeanBroker<LisaLogBean> beanPropsBroker = BIKClientSingletons.beanCreator.getBroker(LisaLogBean.class);

        public Object getHeader(String colName) {
            if (colName.equals(COLUMNS_DICTIONARY_NAME)) {
                return I18n.nazwaSlownika.get() /* i18n:  */;
            } else if (colName.equals(COLUMNS_INFO)) {
                return I18n.podjetaAkcja.get() /* i18n:  */;
            } else if (colName.equals(COLUMNS_SYSTEM_USER_NAME)) {
                return I18n.uzytkownikBIKS.get() /* i18n:  */;
            } else if (colName.equals(COLUMNS_TERADATA_LOGIN)) {
                return I18n.uzytkownikTERADATA.get() /* i18n:  */;
            } else if (colName.equals(COLUMNS_TIME_ADDED)) {
                return I18n.dataAkcji.get() /* i18n:  */;
            } else {
                return colName;
            }
        }

        public Object getValue(T bean, String colName) {
            Object val = beanPropsBroker.getPropValue(bean, colName);
            if (colName.equals(COLUMNS_TIME_ADDED)) {
                return dtf.format((Date) val);
            } else {
                return val;
            }
        }
    }
}
