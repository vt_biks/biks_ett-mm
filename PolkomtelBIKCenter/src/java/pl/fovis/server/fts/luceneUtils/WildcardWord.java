/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts.luceneUtils;

import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.List;
import simplelib.BaseUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public class WildcardWord {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    final public String pattern;
    final public String prefix;
    final public List<String> infixes;
    final public String suffix;
    final public int minLength;

    protected WildcardWord(String pattern, String prefix, List<String> infixes, String suffix) {
        this.pattern = pattern;
        this.prefix = prefix;
        this.infixes = infixes;
        this.suffix = suffix;

        int length = 0;

        if (prefix != null) {
            length += prefix.length();
        }

        for (String infix : infixes) {
            length += infix.length();
        }

        if (suffix != null) {
            length += suffix.length();
        }

        minLength = length;
    }

    public int countWildcards() {
        return infixes.isEmpty() ? 1 : infixes.size() + 1;
    }

    @Override
    public String toString() {

        String wildcardMarker = "[]";

        StringBuilder sb = new StringBuilder(minLength + countWildcards() * wildcardMarker.length());

        if (prefix != null) {
            sb.append(prefix);
        }

        sb.append(wildcardMarker);

        for (String infix : infixes) {
            sb.append(infix).append(wildcardMarker);
        }

        if (suffix != null) {
            sb.append(suffix);
        }

        String calculatedPattern = sb.toString();

        if (logger.isDebugEnabled()) {
            return "{" + "pattern=" + pattern + ", calculatedPattern=" + calculatedPattern + ", prefix=" + prefix + ", infixes=" + infixes + ", suffix=" + suffix + '}';
        }
        return "{" + "pattern=" + pattern + ", calculatedPattern=" + calculatedPattern + ", ...}";
    }

    public boolean matches(String word) {
        int pos = 0;

        if (prefix != null) {

            if (!word.startsWith(prefix)) {
                return false;
            }

            pos = prefix.length();
        }

        for (String infix : infixes) {
            pos = word.indexOf(infix, pos);
            if (pos < 0) {
                return false;
            }
            pos += infix.length();
        }

        if (suffix != null) {
            if (pos > word.length() - suffix.length() || !word.endsWith(suffix)) {
                return false;
            }
        }

        return true;
    }

    public static WildcardWord splitWildcardWord(String word) {
        List<String> parts = BaseUtils.splitBySep(word, "*", false);
        int partCnt = parts.size();
        if (partCnt == 1) {
            return null;
        }
        String wcwPrefix = null;
        final String prefix = parts.get(0);
        if (!prefix.isEmpty()) {
            wcwPrefix = prefix;
        }
        int suffixIdx = partCnt - 1;
        String suffix = parts.get(suffixIdx);
        String wcwSuffix = null;
        if (!suffix.isEmpty()) {
            wcwSuffix = suffix;
        }
        List<String> wcwInfixes = new ArrayList<String>(partCnt - 2);
        for (int i = 1; i < suffixIdx; i++) {
            String part = parts.get(i);
            if (!part.isEmpty()) {
                wcwInfixes.add(part);
            }
        }

        return new WildcardWord(word, wcwPrefix, wcwInfixes, wcwSuffix);
    }
}
