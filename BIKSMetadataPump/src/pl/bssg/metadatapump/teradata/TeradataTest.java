/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.teradata;

import pl.bssg.metadatapump.IPumpLogger;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;

/**
 *
 * @author pgajda
 */
public class TeradataTest {

    protected TeradataPump tp;

    public static void main(String[] args) {

        final MssqlConnectionConfig msconf = new MssqlConnectionConfig();
        msconf.database = "pl_2";
        msconf.instance = "SQLEXPRESS";
        msconf.server = "bssg-dell11";
        msconf.poolSize = 30;
        msconf.timeout = 10;
        msconf.user = "sa";
        msconf.password = "sasasa";

        IBeanConnectionEx<Object> mc = new MssqlConnection(msconf);
        mc.setAutoCommit(true);
        mc.setDbStatementExecutionWarnThresholdMillis(-1);
        INamedSqlsDAO<Object> adhocDao = createAdhocDao(mc);

        IPumpLogger pLogger = new IPumpLogger() {
            @Override
            public void pumpLog(String str, Integer proc) {
                //ThreadedPumpExecutor.this.pumpLog(str, proc);
                System.out.println("Proc: " + proc + " " + str);
            }

            @Override
            public MssqlConnectionConfig getConnectionConfig() {
                return msconf;
            }
        };

        TeradataDataModel tp = new TeradataDataModel();
        tp.setConnectionAndLog("", null, mc, adhocDao, pLogger);
        tp.startPump();

    }

    protected static INamedSqlsDAO<Object> createAdhocDao(IBeanConnectionEx<Object> mc) {
        //return FoxyAppUtils.createNamedSqlsDAOFromJar(mc, "/pl/bssg/metadatapump/metadataAdhocSqls.html");
        //String path = LameUtils.getPackagePathOfClass(TeradataTest.class) + "metadataAdhocSqls.html";
        //return FoxyAppUtils.createNamedSqlsDAOFromJar(mc, path);
        return FoxyAppUtils.createNamedSqlsDAOFromFile(mc, "c:\\projekty\\svn\\BIKSMetadataPump\\src\\pl\\bssg\\metadatapump\\metadataAdhocSqls.html");
    }
}
