/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

/**
 *
 * @author wezyr
 */
public interface IResourceFromProvider<T> {

    public T gainResource();

    public long getLastModifiedOfResource();
}
