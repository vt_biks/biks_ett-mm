/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author msmoktunowicz
 */
public class CryptoUtils {

    public static String getDigest(String pass, String algorithm) {
        return bytesToHex(getDigest(pass.getBytes(), "stdkey".getBytes(), algorithm));
    }

    public static String getDigest(String pass, String key, String algorithm) {
        return bytesToHex(getDigest(pass.getBytes(), key.getBytes(), algorithm));
    }

    private static byte[] getDigest(byte[] buffer, byte[] key, String algorithm) {
        try {
            MessageDigest md5 = MessageDigest.getInstance(algorithm);
            md5.update(buffer);
            return md5.digest(key);
        } catch (NoSuchAlgorithmException e) {
        }
        return null;
    }

    private static String byteToHex(byte b) {
        String res = "";
        int n = (int) b & 0xFF;
        if (n < 0x10) {
            res = "0";
        }
        return res + Integer.toHexString(n);
    }

    public static String bytesToHex(byte[] digest) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < digest.length; i++) {
            sb.append(byteToHex(digest[i]));
        }
        return sb.toString();
    }
}
