/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.client.dialogs.BlogAddOrEditDialog;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.BlogBean;
import simplelib.IParametrizedContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class NodeActionEditBlogEntry extends NodeActionUpdatingBase {

    @Override
    public boolean isEnabledByDesign() {
        if (isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_BLOG_ENTRY/*"BlogEntry"*/)) {
            return isNodeNotLinkedAnyhowNorReadOnlyNodeAndBlogEntryNodeAndUserInProperAuthor();
        }
        return false;
    }

    public String getCaption() {
        return I18n.edytujWpisBlogowy.get() /* I18N:  */;
    }

    public void execute() {
        final BlogBean blogEntry = data.blogEntry;
        new BlogAddOrEditDialog().buildAndShowDialog(blogEntry, new IParametrizedContinuation<BlogBean>() {

            public void doIt(final BlogBean param) {
                BIKClientSingletons.getService().editBikBlogEntry(param.id, param.content, param.subject/*, param.dateAdded*/, param.nodeId/*, node.branchIds*/, new StandardAsyncCallback<List<String>>("Failure on" /* I18N: no */+ " editBlogEntry") {

                    public void onSuccess(List<String> result) {
                        BIKClientSingletons.showInfo(I18n.edytowanoWpis.get() /* I18N:  */);
//                        BIKClientSingletons.getTabSelector().invalidateNodeData(node.id);
                        refreshRedisplayDataAndSelectRecord(node.id/*, false*/);
                        refreshAndRedisplayDataByTreeCode(result);
                    }
                });
            }
        });
    }
}
