﻿exec sp_check_version '1.1.8.12';
go

update bik_app_prop 
set val = 'Business Objects,Teradata DBMS,DQC,Active Directory'
where name = 'availableConnectors'


exec sp_update_version '1.1.8.12', '1.1.9';
go