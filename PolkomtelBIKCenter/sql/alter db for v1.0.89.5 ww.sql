﻿exec sp_check_version '1.0.89.5';
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

delete from bik_node where tree_id = dbo.fn_tree_id_by_code('TreeOfTrees')
go

exec sp_add_menu_node null, 'My BIKS', '#MyBIKS'
exec sp_add_menu_node null, 'Metadane', 'metadata'
exec sp_add_menu_node 'metadata', 'SAP BO', 'sapbo'
exec sp_add_menu_node 'sapbo', '@', '$Reports'
exec sp_add_menu_node 'sapbo', '@', '$ObjectUniverses'
exec sp_add_menu_node 'sapbo', '@', '$Connections'
exec sp_add_menu_node 'metadata', '@', '$Teradata'
exec sp_add_menu_node null, '@', '$Glossary'
exec sp_add_menu_node null, 'Kategoryzacja', '@Taxonomy'
exec sp_add_menu_node null, 'Dokumenty', '$Documents'
exec sp_add_menu_node null, 'Blogi', '$Blogs'
exec sp_add_menu_node null, 'Użytkownicy', '$Users'
exec sp_add_menu_node null, 'FAQ', '$FAQ'
exec sp_add_menu_node null, 'Admin', '#Admin'
exec sp_add_menu_node null, 'Szukaj', '#Search'

exec sp_add_menu_node '$Teradata', '@', '&TeradataSchema'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataView'
exec sp_add_menu_node '&TeradataView', '@', '&TeradataColumn'
exec sp_add_menu_node '@Taxonomy', '@', '&TaxonomyEntity'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataTable'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumn'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataProcedure'
exec sp_add_menu_node '$Reports', '@', '&ReportFolder'
exec sp_add_menu_node '&ReportFolder', '@', '&Webi'
exec sp_add_menu_node '&ReportFolder', '@', '&FullClient'
exec sp_add_menu_node '&Webi', '@', '&ReportQuery'
exec sp_add_menu_node '&ReportQuery', '@', '&Dimension$Reports'
exec sp_add_menu_node '&ReportQuery', '@', '&Measure'
exec sp_add_menu_node '&ReportQuery', '@', '&Detail'
exec sp_add_menu_node '&ReportFolder', '@', '&Flash'
exec sp_add_menu_node '&ReportFolder', '@', '&Excel'
exec sp_add_menu_node '&ReportFolder', '@', '&Hyperlink'
exec sp_add_menu_node '&ReportFolder', '@', '&Powerpoint'
exec sp_add_menu_node '&ReportFolder', '@', '&Pdf'
exec sp_add_menu_node '&ReportFolder', '@', '&CrystalReport'
exec sp_add_menu_node '$ObjectUniverses', '@', '&UniversesFolder'
exec sp_add_menu_node '&UniversesFolder', '@', '&Universe'

exec sp_add_menu_node '&Universe', '@', '&UniverseDerivedTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseAliasTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseTable'

exec sp_add_menu_node '&Universe', '@', '&UniverseClass'
exec sp_add_menu_node '&UniverseClass', '@', '&Dimension'
exec sp_add_menu_node '&UniverseClass', '@', '&Measure'
exec sp_add_menu_node '&Dimension', '@', '&Detail'
exec sp_add_menu_node '&UniverseClass', '@', '&Filter'

exec sp_add_menu_node '$Connections', '@', '&ConnectionEngineFolder'
exec sp_add_menu_node '&ConnectionEngineFolder', '@', '&ConnectionUserFolder'

exec sp_add_menu_node '&ConnectionUserFolder', '@', '&DataConnection'

exec sp_add_menu_node '$Documents', '@', '&DocumentsFolder'
exec sp_add_menu_node '&DocumentsFolder', '@', '&Document'

exec sp_add_menu_node '$Users', '@', '&UsersGroup'
exec sp_add_menu_node '&UsersGroup', '@', '&User'
exec sp_add_menu_node '$Blogs', '@', '&Blog'

exec sp_add_menu_node '$FAQ', '@', '&CategoriesOfQuestions'
exec sp_add_menu_node '&CategoriesOfQuestions', '@', '&Question'

exec sp_add_menu_node '$Glossary', '@', '&Keyword'

exec sp_add_menu_node '$Glossary', '@', '&GlossaryCategory'

exec sp_add_menu_node '&GlossaryCategory', '@', '&Glossary'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryNotCorpo'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryVersion'

exec sp_add_menu_node '&Blog', '@', '&BlogEntry'
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

exec sp_update_version '1.0.89.5', '1.0.89.6';
go
