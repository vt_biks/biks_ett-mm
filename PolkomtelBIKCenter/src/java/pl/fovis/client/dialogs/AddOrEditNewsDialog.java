/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.common.NewsBean;
import simplelib.IParametrizedContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class AddOrEditNewsDialog extends BaseRichTextAndTextboxDialog {

    protected IParametrizedContinuation<NewsBean> saveCont;
    protected NewsBean news;

    @Override
    protected void innerDoAction(String title, String body) {
        news.title = title;
        news.text = body;
        saveCont.doIt(news);
    }

    @Override
    protected String getDialogCaption() {
        return news.title == null ? I18n.dodajAktualnosc.get() /* I18N:  */ : I18n.edytujAktualnosc.get() /* I18N:  */;
    }

    public void buildAndShowDialog(NewsBean news, IParametrizedContinuation<NewsBean> saveCont) {
        this.saveCont = saveCont;
        this.news = news;
        super.buildAndShowDialog(news.title, news.text);
    }
}
