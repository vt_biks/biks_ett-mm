/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.oracle;

/**
 *
 * @author tflorczak
 */
public class OracleDefinitionBean {

    public String server;
    public String owner;
    public String name;
    public String definition;

    public OracleDefinitionBean(String server, String owner, String name, String definition) {
        this.server = server;
        this.owner = owner;
        this.name = name;
        this.definition = definition;
    }
}
