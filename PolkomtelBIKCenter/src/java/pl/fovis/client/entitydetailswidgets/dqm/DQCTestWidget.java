/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase;
import pl.fovis.client.entitydetailswidgets.IMainHeaderSharedTable;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.dqm.DataQualityTestBean;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class DQCTestWidget extends EntityDetailsWidgetFlatBase {

    protected IMainHeaderSharedTable imhst;

    public DQCTestWidget(IMainHeaderSharedTable imhst) {
        this.imhst = imhst;
    }

    @Override
    public void displayData() {
        DataQualityTestBean dqcTestExtradata = getData().dataQualityTestExtradata;
        if (dqcTestExtradata != null) {
            imhst.addTranslatedRow("Long name" /* I18N: no */, dqcTestExtradata.longName);
            imhst.addTranslatedRow("Benchmark definition" /* I18N: no */, dqcTestExtradata.benchmarkDefinition);
            imhst.addTranslatedRow("Error Threshold" /* I18N: no */, dqcTestExtradata.errorThreshold + "");
            imhst.addTranslatedRow("Expected Result" /* I18N: no */, dqcTestExtradata.expectedResult);
            imhst.addTranslatedRow("Sampling Method" /* i18n: no */, dqcTestExtradata.samplingMethod);
            imhst.addTranslatedRow("Additional Information" /* I18N: no */, dqcTestExtradata.additionalInformation);
            imhst.addTranslatedRow("Create Date" /* I18N: no */, SimpleDateUtils.toCanonicalString(dqcTestExtradata.createDate));
            imhst.addTranslatedRow("Modify Date" /* I18N: no */, SimpleDateUtils.toCanonicalString(dqcTestExtradata.modifyDate));
            imhst.addTranslatedRow("Suspend Date" /* I18N: no */, SimpleDateUtils.toCanonicalString(dqcTestExtradata.suspendDate));
        }
    }

    @Override
    public Widget buildWidgets() {
        return new VerticalPanel();
    }

    @Override
    public boolean hasContent() {
        EntityDetailsDataBean data = getData();
        return data.dataQualityTestExtradata != null && data.node.treeCode.equals(BIKConstants.TREE_CODE_DQC);
    }
}
