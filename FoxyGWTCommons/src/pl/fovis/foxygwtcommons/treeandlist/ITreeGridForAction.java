/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.treeandlist;

import java.util.Comparator;
import java.util.Map;

/**
 *
 * @author pmielanczuk
 */
public interface ITreeGridForAction<IDT> {

    public void setNodeName(IDT nodeId, String name);

    public void deleteNode(IDT nodeId);

    public void markAsCutNode(IDT nodeId);

    public void markAsUncutNode(IDT nodeId);

    public void pasteNode(IDT nodeId, IDT srcNodeId, boolean isCut);

    public int getNodeChildCount(IDT nodeId);

    public int getNodeIndexInSiblings(IDT nodeId);

    public void setNodeIndexInSiblings(IDT nodeId, int idx);

    public void sortChildNodes(IDT nodeId, Comparator<Map<String, Object>> cmp);

    public IDT getNodeIdAtIndexInSiblings(IDT parentId, int idx);
}
