/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.Map;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.foxygwtcommons.treeandlist.ITreeGridForAction;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;

/**
 *
 * @author wezyr
 */
public interface INodeActionContext {

    //ww: storage jest dostępny dopiero po wywołaniu initWithFullData!
    public ITreeRowAndBeanStorage<Integer, Map<String, Object>, TreeNodeBean> getTreeStorage();

    public EntityDetailsDataBean getNodeData();

    public int getTreeId();

    public String getTreeCode();

    public Integer getSelectedNodeId();

    // can be null for root actions
    public TreeNodeBean getNode();

    public TreeNodeBean getCurrentNodeBean();

    public INodeClipboard getClipboard();

    public void refreshAndRedisplayData();

    public void redisplayData();

    public void refreshRedisplayDataAndSelectRecord(Integer nodeId/*
     * , boolean expand
     */);

    public ITreeGridForAction<Integer> getTreeGrid();

    public void addTreeGridNode(TreeNodeBean node);
//
//    public void setTreeGridNodeName(int nodeId, String name);
//
//    public void deleteTreeGridNode(int nodeId);
//
//    public void markAsCutTreeGridNode(int nodeId);
//
//    public void markAsUncutTreeGridNode(int nodeId);
//
//    public void pasteTreeGridNode(int nodeId, int srcNodeId, boolean isCut);
//
//    // nodeId == null -> get tree roots count
//    public int getChildCount(Integer nodeId);
//
//    public int getIndexInSiblings(int nodeId);
}
