exec sp_check_version '1.0.87.2';
go
-- select * from bik_app_prop

----------------------
----------------------
----------------------

if OBJECT_ID (N'dbo.fn_node_kind_id_by_code', N'FN') is not null
    drop function dbo.fn_node_kind_id_by_code;
go

create function dbo.fn_node_kind_id_by_code(@code varchar(256))
returns int
as
begin
  return (select id from bik_node_kind where code = @code)
end
go

-- select * from bik_node_kind

-- select dbo.fn_node_kind_id_by_code('Glossary')



if OBJECT_ID (N'dbo.fn_tree_id_by_code', N'FN') is not null
    drop function dbo.fn_tree_id_by_code;
go

create function dbo.fn_tree_id_by_code(@code varchar(256))
returns int
as
begin
  return (select id from bik_tree where code = @code)
end
go


-- select * from bik_tree

-- select dbo.fn_tree_id_by_code('Glossary')


if OBJECT_ID (N'dbo.fn_node_id_by_tree_code_and_obj_id', N'FN') is not null
    drop function dbo.fn_node_id_by_tree_code_and_obj_id;
go

create function dbo.fn_node_id_by_tree_code_and_obj_id(@tree_code varchar(256), @obj_id varchar(255))
returns int
as
begin
  return (select id from bik_node where tree_id = dbo.fn_tree_id_by_code(@tree_code) and obj_id = @obj_id)
end
go


----------------------------------------
----------------------------------------

insert into bik_tree (name, node_kind_id, code, tree_kind)
values ('Drzewo drzew (tzw. modrzew)', null, 'TreeOfTrees', 'larch')
go

-- select dbo.fn_tree_id_by_code('TreeOfTrees')

-- select * from bik_node_kind
insert into bik_node_kind (code, caption, icon_name, tree_kind, is_folder)
values ('MenuItem', 'Pozycja menu', 'folder', 'aqq', 1)
go

-- select dbo.fn_node_kind_id_by_code('MenuItem')

-- select * from bik_node

--------------------
--------------------
--------------------

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_add_menu_node]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_add_menu_node]
GO

create procedure [dbo].[sp_add_menu_node](@parent_obj_id varchar(255), @name varchar(255), @obj_id varchar(255))
as
begin
  insert into bik_node (parent_node_id, node_kind_id, tree_id, name, obj_id )
  values (dbo.fn_node_id_by_tree_code_and_obj_id('TreeOfTrees', @parent_obj_id), 
    dbo.fn_node_kind_id_by_code('MenuItem'), 
    dbo.fn_tree_id_by_code('TreeOfTrees'), @name, @obj_id)
end
go

--------------------
--------------------
--------------------
create index idx_bik_node_tree_id_is_deleted on bik_node (tree_id, is_deleted)
go

--------------------
--------------------
--------------------

-- select * from bik_tree
delete from bik_node where tree_id = dbo.fn_tree_id_by_code('TreeOfTrees')
go

exec sp_add_menu_node null, 'My BIKS', '#MyBIKS'
exec sp_add_menu_node null, '@', '$Glossary'
exec sp_add_menu_node null, 'Metadane', 'metadata'
exec sp_add_menu_node 'metadata', 'SAP BO', 'sapbo'
exec sp_add_menu_node 'sapbo', '@', '$Reports'
exec sp_add_menu_node 'sapbo', '@', '$ObjectUniverses'
exec sp_add_menu_node 'sapbo', '@', '$Connections'
exec sp_add_menu_node 'metadata', '@', '$Teradata'
exec sp_add_menu_node null, 'Baza wiedzy', '@Taxonomy'
exec sp_add_menu_node null, 'Dokumenty', '$Documents'
exec sp_add_menu_node null, 'Blogi', '$Blogs'
exec sp_add_menu_node null, 'Użytkownicy', '$Users'
exec sp_add_menu_node null, 'FAQ', '$FAQ'
exec sp_add_menu_node null, 'Admin', '#Admin'
exec sp_add_menu_node null, 'Szukaj', '#Search'
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

/*
declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
select * from bik_node where is_deleted = 0 and tree_id = --dbo.fn_tree_id_by_code('TreeOfTrees') -- 
@tree_id
order by branch_ids
*/

--------------------
--------------------
--------------------

/*

select node_kind_id, count(*)
from bik_node

*/

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_set_app_prop]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_set_app_prop]
GO

create procedure [dbo].[sp_set_app_prop](@name varchar(255), @val varchar(255))
as
begin
  if @val is null
    delete from bik_app_prop where name = @name
  else
  begin
    update bik_app_prop set val = @val where name = @name
    if @@rowcount = 0
      insert into bik_app_prop (name, val, is_editable) values (@name, @val, 1);
  end;
end
go

--------------------
--------------------
--------------------

-- select * from bik_app_prop

exec sp_set_app_prop 'search_order_by_exprs', 'sr.[rank] desc'
go

--------------------
--------------------
--------------------

exec sp_update_version '1.0.87.2', '1.0.87.3';
go
