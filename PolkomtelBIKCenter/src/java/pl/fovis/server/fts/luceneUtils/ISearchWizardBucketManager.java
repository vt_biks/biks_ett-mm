/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts.luceneUtils;

import org.apache.lucene.document.Document;

/**
 *
 * @author pmielanczuk
 */
public interface ISearchWizardBucketManager<T> {

    int getBucketCount();

    T assignToBucket(Document doc);
}
