exec sp_check_version '1.2.1.1';
go

	declare @node_kind_id int;
	declare @tree_id int;
	declare @add_id int;
	
	select @tree_id = id, @node_kind_id = node_kind_id from bik_tree where code = 'UserRoles'

    insert into bik_node( name, node_kind_id, tree_id, is_built_in) values ('Autor', @node_kind_id, @tree_id, 1);
	
	select @add_id = scope_identity()
	update bik_node set branch_ids = convert(varchar(20),@add_id)+'|' where  id = @add_id;

    insert into bik_node( name, node_kind_id, tree_id, is_built_in) values ('Redaktor', @node_kind_id, @tree_id, 1);
	
	select @add_id = scope_identity()
	update bik_node set branch_ids = convert(varchar(20),@add_id)+'|' where  id = @add_id;

exec sp_update_version '1.2.1.1', '1.2.1.2';
go
