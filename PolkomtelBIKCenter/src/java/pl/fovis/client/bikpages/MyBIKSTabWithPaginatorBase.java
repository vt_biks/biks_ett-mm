/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.IGridItemsProvider;
import pl.fovis.client.bikwidgets.IPaginatedItemsDisplayer;
import pl.fovis.client.bikwidgets.WidgetWithPagination;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author bfechner
 */
public abstract class MyBIKSTabWithPaginatorBase<T> extends MyBIKSTabBase {

    protected VerticalPanel activeTab = new VerticalPanel();
    protected List<T> allItems;
    protected WidgetWithPagination<T> pagin;
    protected Widget paginationWidget;
    protected VerticalPanel optionalFilterByKindWidgets = new VerticalPanel();
    protected IGridItemsProvider<T> dataPaginator = new IGridItemsProvider<T>() {
        public void itemsChanged() {
            refreshTabButton();
        }

        public void refetchItems() {
            refreshTabButton();
            fetchAndDisplayData();
        }
    };

    protected void displayData() {

        activeTab.clear();
        activeTab.add(optionalFilterByKindWidgets);
        List<T> allItemsToDisplay = prepareAllItemsToDisplay();
        if (BaseUtils.isCollectionEmpty(allItemsToDisplay)) {
            String cap = getCaptionWhenListEmpty();
            if (cap != null) {
                Label captionWhenListEmpty = new Label(cap);
                captionWhenListEmpty.setStyleName("MyBIK");
                activeTab.add(captionWhenListEmpty/*new Label(cap)*/);
            }
        } else {
            if (paginationWidget == null) {
                pagin = new WidgetWithPagination<T>(getPageSize(),
                        dataPaginator,
                        createPaginatedItemsDisplayer());
                paginationWidget = pagin.buildWidgets();
            }
            activeTab.add(paginationWidget);
            pagin.displayData(allItemsToDisplay, allItems);
        }
    }

    protected void fetchAndDisplayData() {
        callServiceToGetData(new StandardAsyncCallback<List<T>>() {
            public void onSuccess(List<T> result) {
                allItems = result;
                addOptionalFilterByKindWidgets(allItems);
                displayData();
            }
        });
    }

    @Override
    public void show(VerticalPanel vp) {
        vp.clear();
        activeTab.setWidth("100%");
        vp.setWidth("100%");
        GWTUtils.addOptWidget(vp, createOptHeaderWidget());
//        vp.add(addOptionalFilterWidgets());
        vp.add(activeTab);
        if (BIKClientSingletons.showSuggestedElements()) {
            GWTUtils.addOptWidget(vp, createOptSuggestedElementsWidget());
        }
        fetchAndDisplayData();
    }

    public void refreshTabButton() {
        if (myBIKSPage != null) {
            myBIKSPage.refreshTabButton(this);
        }
    }

    protected abstract void callServiceToGetData(AsyncCallback<List<T>> callback);

    protected HorizontalPanel createOptHeaderWidget() {
        return null;
    }

    protected abstract IPaginatedItemsDisplayer<T> createPaginatedItemsDisplayer();

    protected int getPageSize() {
        return 10;
    }

    // null - nic się nie pokaże
    // ta metoda nie jest abstrakcyjna, bo są paginowane zakładki, które z założenia
    // nie będą nigdy puste (tutorial), więc nie ma sensu na nich ten tekst
    protected String getCaptionWhenListEmpty() {
        return null;
    }

    protected List<T> prepareAllItemsToDisplay() {
        return allItems;
    }

    protected void addOptionalFilterByKindWidgets(List<T> allItems) {
    }

    protected VerticalPanel createOptSuggestedElementsWidget() {
        return null;
    }
}
