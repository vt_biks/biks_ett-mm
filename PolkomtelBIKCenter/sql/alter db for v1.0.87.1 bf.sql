﻿exec sp_check_version '1.0.87.1';
go

--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------


update bik_metapedia set official=1


------------------------------------

declare @parent_node_id int
select @parent_node_id=bn.id from bik_node bn join  bik_tree bt on bn.tree_id= bt.id
where bn.name='Artykuły'  and bt.code='Glossary'and bn.parent_node_id in
(select bn.id from bik_node bn join  bik_tree bt on bn.tree_id= bt.id
where bn.name='RODOS'  and bt.code='Glossary')
declare @wprowadzenie varchar(50)
select @wprowadzenie=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Wprowadzenie'and bn.parent_node_id=@parent_node_id


select @parent_node_id=bn.id from bik_node bn join  bik_tree bt on bn.tree_id= bt.id
where bn.name='Miary'  and bt.code='Glossary' and bn.parent_node_id in
(select bn.id from bik_node bn join  bik_tree bt on bn.tree_id= bt.id
where bn.name='RODOS'  and bt.code='Glossary')
declare @liczba_zmian varchar(50)
declare @liczba_migracji varchar(50)
declare @liczba_reaktywacji varchar(50)
declare @churn_netto varchar(50)
declare @przyrost_brutto varchar(50)
declare @przyrost_netto varchar(20)
declare @liczba_aktywacji varchar(50)
declare @liczba_deaktywacji varchar(50)
declare @liczba_zawieszen varchar(50)
declare @baza_abonencka varchar(50)
declare @liczba_odwieszen varchar(50)

select @liczba_migracji=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Liczba migracji'and bn.parent_node_id=@parent_node_id
select @liczba_reaktywacji=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Liczba reaktywacji'and bn.parent_node_id=@parent_node_id
select @churn_netto=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Churn netto'and bn.parent_node_id=@parent_node_id
select @przyrost_brutto=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Przyrost brutto'and bn.parent_node_id=@parent_node_id
select @liczba_aktywacji=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Liczba aktywacji'and bn.parent_node_id=@parent_node_id
select @liczba_deaktywacji=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Liczba deaktywacji (churn brutto)'and bn.parent_node_id=@parent_node_id
select @liczba_zawieszen=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Liczba zawieszeń'and bn.parent_node_id=@parent_node_id
select @baza_abonencka=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Baza abonencka'and bn.parent_node_id=@parent_node_id
select @liczba_odwieszen=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Liczba odwieszeń'and bn.parent_node_id=@parent_node_id
select @liczba_zmian=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Liczba zmian'and bn.parent_node_id=@parent_node_id
select @przyrost_netto=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Przyrost netto'and bn.parent_node_id=@parent_node_id
--declare @parent_node_id int
select @parent_node_id=bn.id from bik_node bn join  bik_tree bt on bn.tree_id= bt.id
where bn.name='Wymiary'  and bt.code='Glossary' and bn.parent_node_id in
(select bn.id from bik_node bn join  bik_tree bt on bn.tree_id= bt.id
where bn.name='RODOS'  and bt.code='Glossary')
declare @czas varchar(50)
declare @status varchar(50)
declare @linia_produktowa varchar(50)
declare @komercyjny varchar(50)
declare @typ_klienta varchar(50)
declare @baza_pr varchar(50)
declare @sprzet varchar(50)
declare @opiekun varchar(50)
declare @plan_cenowy varchar(50)
declare @sprzedaz varchar(50)

select @czas =bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Czas' and bn.parent_node_id=@parent_node_id
select @status=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Status' and bn.parent_node_id=@parent_node_id
select @linia_produktowa=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Linia produktowa' and bn.parent_node_id=@parent_node_id
select @komercyjny=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Komercyjny' and bn.parent_node_id=@parent_node_id
select @typ_klienta=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Typ klienta' and bn.parent_node_id=@parent_node_id
select @baza_pr=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Baza produktowa' and bn.parent_node_id=@parent_node_id
select @sprzet=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Sprzęt' and bn.parent_node_id=@parent_node_id
select @opiekun=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Opiekun' and bn.parent_node_id=@parent_node_id
select @plan_cenowy=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Plan cenowy' and bn.parent_node_id=@parent_node_id
select @sprzedaz=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Sprzedaż' and bn.parent_node_id=@parent_node_id

--declare @parent_node_id int
select @parent_node_id=bn.id from bik_node bn join  bik_tree bt on bn.tree_id= bt.id
where bn.name='Zdarzenia'  and bt.code='Glossary'and bn.parent_node_id in
(select bn.id from bik_node bn join  bik_tree bt on bn.tree_id= bt.id
where bn.name='RODOS'  and bt.code='Glossary')
declare @aktywacja varchar(50)
declare @dekatywacja varchar(50)
declare @reaktywacja varchar(50)
declare @zawieszenie varchar(50)
declare @odwieszenie varchar(50)
declare @migracja varchar(50)
declare @zmiana varchar(50)

select @aktywacja=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Aktywacja' and bn.parent_node_id=@parent_node_id
select @dekatywacja=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Deaktywacja'and bn.parent_node_id=@parent_node_id 
select @reaktywacja=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Reaktywacja' and bn.parent_node_id=@parent_node_id
select @zawieszenie=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Zawieszenie' and bn.parent_node_id=@parent_node_id
select @odwieszenie=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Odwieszenie' and bn.parent_node_id=@parent_node_id
select @migracja=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Migracja' and bn.parent_node_id=@parent_node_id
select @zmiana=bn.id from bik_metapedia bm join bik_node bn on bm.node_id=bn.id where bn.name='Zmiana' and bn.parent_node_id=@parent_node_id


insert into bik_joined_objs values (@baza_abonencka,@wprowadzenie,0)
insert into bik_joined_objs values (@dekatywacja,@aktywacja,0)
insert into bik_joined_objs values (@reaktywacja,@aktywacja,0)
insert into bik_joined_objs values (@zmiana,@aktywacja,0)
insert into bik_joined_objs values (@liczba_aktywacji,@aktywacja,0)
insert into bik_joined_objs values (@status,@aktywacja,0)
insert into bik_joined_objs values (@aktywacja,@dekatywacja,0)
insert into bik_joined_objs values (@reaktywacja,@dekatywacja,0)
insert into bik_joined_objs values (@zmiana,@dekatywacja,0)
insert into bik_joined_objs values (@liczba_deaktywacji,@dekatywacja,0)
insert into bik_joined_objs values (@status,@dekatywacja,0)
insert into bik_joined_objs values (@aktywacja,@reaktywacja,0)
insert into bik_joined_objs values (@dekatywacja,@reaktywacja,0)
insert into bik_joined_objs values (@zmiana,@reaktywacja,0)
insert into bik_joined_objs values (@liczba_reaktywacji,@reaktywacja,0)
insert into bik_joined_objs values (@status,@reaktywacja,0)
insert into bik_joined_objs values (@odwieszenie,@zawieszenie,0)
insert into bik_joined_objs values (@zmiana,@zawieszenie,0)
insert into bik_joined_objs values (@liczba_zawieszen,@zawieszenie,0)
insert into bik_joined_objs values (@zawieszenie,@odwieszenie,0)
insert into bik_joined_objs values (@zmiana,@odwieszenie,0)
insert into bik_joined_objs values (@Liczba_odwieszen,@odwieszenie,0)
insert into bik_joined_objs values (@status,@odwieszenie,0)
insert into bik_joined_objs values (@zmiana,@migracja,0)
insert into bik_joined_objs values (@liczba_migracji,@migracja,0)
insert into bik_joined_objs values (@baza_pr,@migracja,0)
insert into bik_joined_objs values (@aktywacja,@zmiana,0)
insert into bik_joined_objs values (@dekatywacja,@zmiana,0)
insert into bik_joined_objs values (@reaktywacja,@zmiana,0)
insert into bik_joined_objs values (@zawieszenie,@zmiana,0)
insert into bik_joined_objs values (@odwieszenie,@zmiana,0)
insert into bik_joined_objs values (@migracja,@zmiana,0)
insert into bik_joined_objs values (@liczba_zmian,@zmiana,0)
insert into bik_joined_objs values (@wprowadzenie,@baza_abonencka,0)
insert into bik_joined_objs values (@liczba_aktywacji,@baza_abonencka,0)
insert into bik_joined_objs values (@liczba_deaktywacji,@baza_abonencka,0)
insert into bik_joined_objs values (@liczba_reaktywacji,@baza_abonencka,0)
insert into bik_joined_objs values (@liczba_migracji,@baza_abonencka,0)
insert into bik_joined_objs values (@przyrost_brutto,@baza_abonencka,0)
insert into bik_joined_objs values (@churn_netto,@baza_abonencka,0)
insert into bik_joined_objs values (@przyrost_netto,@baza_abonencka,0)
insert into bik_joined_objs values (@aktywacja,@liczba_aktywacji,0)
insert into bik_joined_objs values (@baza_abonencka,@liczba_aktywacji,0)
insert into bik_joined_objs values (@liczba_deaktywacji,@liczba_aktywacji,0)
insert into bik_joined_objs values (@liczba_reaktywacji,@liczba_aktywacji,0)
insert into bik_joined_objs values (@liczba_migracji,@liczba_aktywacji,0)
insert into bik_joined_objs values (@przyrost_brutto,@liczba_aktywacji,0)
insert into bik_joined_objs values (@przyrost_netto,@liczba_aktywacji,0)
insert into bik_joined_objs values (@dekatywacja,@liczba_deaktywacji,0)
insert into bik_joined_objs values (@baza_abonencka,@liczba_deaktywacji,0)
insert into bik_joined_objs values (@liczba_aktywacji,@liczba_deaktywacji,0)
insert into bik_joined_objs values (@liczba_reaktywacji,@liczba_deaktywacji,0)
insert into bik_joined_objs values (@liczba_migracji,@liczba_deaktywacji,0)
insert into bik_joined_objs values (@churn_netto,@liczba_deaktywacji,0)
insert into bik_joined_objs values (@reaktywacja,@liczba_reaktywacji,0)
insert into bik_joined_objs values (@baza_abonencka,@liczba_reaktywacji,0)
insert into bik_joined_objs values (@liczba_aktywacji,@liczba_reaktywacji,0)	
insert into bik_joined_objs values (@liczba_deaktywacji,@liczba_reaktywacji,0)
insert into bik_joined_objs values (@liczba_migracji,@liczba_reaktywacji,0)
insert into bik_joined_objs values (@churn_netto,@liczba_reaktywacji	,0)
insert into bik_joined_objs values (@zawieszenie,@liczba_zawieszen,0)
insert into bik_joined_objs values (@Liczba_odwieszen,@liczba_zawieszen,0)
insert into bik_joined_objs values (@odwieszenie,@Liczba_odwieszen,0)
insert into bik_joined_objs values (@liczba_zawieszen,@Liczba_odwieszen,0)
insert into bik_joined_objs values (@migracja,@liczba_migracji,0)
insert into bik_joined_objs values (@baza_abonencka,@liczba_migracji,0)
insert into bik_joined_objs values (@liczba_aktywacji,@liczba_migracji,0)
insert into bik_joined_objs values (@liczba_deaktywacji,@liczba_migracji,0)
insert into bik_joined_objs values (@liczba_reaktywacji,@liczba_migracji,0)
insert into bik_joined_objs values (@przyrost_brutto,@liczba_migracji,0)
insert into bik_joined_objs values (@przyrost_netto,@liczba_migracji,0)
insert into bik_joined_objs values (@zmiana,@liczba_zmian,0)
insert into bik_joined_objs values (@baza_abonencka,@przyrost_brutto,0)
insert into bik_joined_objs values (@liczba_aktywacji,@przyrost_brutto,0)
insert into bik_joined_objs values (@liczba_migracji,@przyrost_brutto,0)
insert into bik_joined_objs values (@przyrost_netto,@przyrost_brutto,0)
insert into bik_joined_objs values (@baza_abonencka,@churn_netto,0)
insert into bik_joined_objs values (@liczba_deaktywacji,@churn_netto,0)
insert into bik_joined_objs values (@liczba_reaktywacji,@churn_netto,0)
insert into bik_joined_objs values (@przyrost_netto,@churn_netto,0)
insert into bik_joined_objs values (@baza_abonencka,@przyrost_netto,0)
insert into bik_joined_objs values (@liczba_aktywacji,@przyrost_netto,0)
insert into bik_joined_objs values (@liczba_migracji,@przyrost_netto,0)
insert into bik_joined_objs values (@przyrost_brutto,@przyrost_netto,0)
insert into bik_joined_objs values (@churn_netto,@przyrost_netto,0)
insert into bik_joined_objs values (@aktywacja,@status,0)
insert into bik_joined_objs values (@dekatywacja,@status,0)
insert into bik_joined_objs values (@reaktywacja,@status,0)
insert into bik_joined_objs values (@odwieszenie,@status,0)
insert into bik_joined_objs values (@migracja,@baza_pr,0)
--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

exec sp_update_version '1.0.87.1', '1.0.87.2';
go
