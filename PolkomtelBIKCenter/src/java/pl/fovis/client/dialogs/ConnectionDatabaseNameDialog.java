/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.*;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author tflorczak
 */
public class ConnectionDatabaseNameDialog extends BaseActionOrCancelDialog {

    protected ListBox lbServer;
    protected ListBox lbDB;
    protected TextBox tbSchema;
    protected Label status;
    protected Label lblServer;
    protected Label lblDB;
    protected Label lblSchema;
    protected Map<String, List<String>> serversMap;
    protected int nodeId;
    protected IContinuation continuation;

    @Override
    protected String getDialogCaption() {
        return I18n.ustawBazeDanychDlaPolaczenia.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.ustaw.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {

        status = new Label();
        status.setStyleName("status" /* i18n: no:css-class */);
        main.add(status);
        main.setWidth("415px");

        Grid grid = new Grid(3, 2);
        grid.setCellSpacing(4);
        grid.setWidget(0, 0, lblServer = new Label("Server name" /* I18N: no */ + ":"));
        grid.setWidget(1, 0, lblDB = new Label("Database name" /* I18N: no */ + ":"));
        grid.setWidget(2, 0, lblSchema = new Label("Default schema name" /* I18N: no */ + ":"));
        grid.setWidget(0, 1, lbServer = new ListBox());
        grid.setWidget(1, 1, lbDB = new ListBox());
        grid.setWidget(2, 1, tbSchema = new TextBox());
        lblServer.setStyleName("loginLbl");
        lblDB.setStyleName("loginLbl");
        lblSchema.setStyleName("loginLbl");
        lbServer.setStyleName("loginAndPass");
        lbDB.setStyleName("loginAndPass");
        tbSchema.setStyleName("loginAndPass");
        lbServer.setWidth("258px");
        lbDB.setWidth("258px");
        tbSchema.setWidth("248px");

        main.add(grid);
        for (String server : serversMap.keySet()) {
            lbServer.addItem(server);
        }
        lbServer.addChangeHandler(new ChangeHandler() {

            public void onChange(ChangeEvent event) {
                addDbsForSelectedRecord();
            }
        });
        if (lbServer.getItemCount() > 0) {
            addDbsForSelectedRecord();
        }
    }

    @Override
    protected boolean doActionBeforeClose() {
        status.setText(null);
        if (lbServer.getItemCount() == 0 || lbDB.getItemCount() == 0) {
            status.setText(I18n.wybierzSerwerBazeDanychOrazDomys.get() /* I18N:  */);
            return false;
        } else {
            hideDialog();
        }
        return true;
    }

    @Override
    protected void doAction() {
        BIKClientSingletons.getService().setMSSQLServerAndDatabasesForBOConnection(nodeId, lbServer.getItemText(lbServer.getSelectedIndex()), lbDB.getItemText(lbDB.getSelectedIndex()), tbSchema.getText(), new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " setMSSQLServerAndDatabasesForBOConnection") {

            public void onSuccess(Void result) {
                BIKClientSingletons.showInfo(I18n.zapisanoBazeDanychDlaPolaczenia.get() /* I18N:  */);
                continuation.doIt();
            }
        });
    }

    public void buildAndShowDialog(Map<String, List<String>> servers, int nodeId, IContinuation continuation) {
        this.serversMap = servers;
        this.nodeId = nodeId;
        this.continuation = continuation;
        super.buildAndShowDialog();
    }

    private void addDbsForSelectedRecord() {
        List<String> dbs = serversMap.get(lbServer.getItemText(lbServer.getSelectedIndex()));
        lbDB.clear();
        for (String db : dbs) {
            lbDB.addItem(db);
        }
    }
}
