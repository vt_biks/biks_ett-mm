/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.teradata;

/**
 *
 * @author tflorczak
 */
public class TeradataConfigBean {

    public Integer isActive;
    public String url;
    public String user;
    public String password;
    public String instanceId;
    public String select;
    public String maxErrorCount;
    public String driverClass;
}
