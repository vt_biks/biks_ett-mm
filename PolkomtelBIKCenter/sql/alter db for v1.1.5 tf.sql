﻿exec sp_check_version '1.1.5';
go

------------------------------------
------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_shrink_file]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_shrink_file]
go


CREATE PROC [dbo].[sp_shrink_file]
WITH EXECUTE AS OWNER
AS
BEGIN
	declare @log_name varchar(max);
    select @log_name=name from sys.database_files where type = 1

    exec('DBCC SHRINKFILE (N''' + @log_name + '''  , 0)')
END;
GO

-- PONIŻSZE JEST ZBĘDNE
--GRANT EXECUTE ON [dbo].[sp_shrink_file] TO biks_load;
--GO


insert into bik_admin(param,value)
select 'mssql.user', val as value from bik_app_prop where name='load_user_login'

insert into bik_admin(param,value)
select 'mssql.password', val as value from bik_app_prop where name='load_user_pwd'


------------------------------------
------------------------------------

exec sp_update_version '1.1.5', '1.1.5.1';
go
