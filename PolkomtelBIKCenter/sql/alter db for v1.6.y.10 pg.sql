﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- Poprawki funkcji hash-ującej md5,
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_check_version '1.6.y.10';
go

if exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_news') and name = 'text_hash')
 	begin
 		if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_news]') and name = N'idx_bik_news_text_hash')
		  drop index [idx_bik_news_text_hash] on [dbo].[bik_news]
		  
 		alter table bik_news drop column text_hash
end
	
if  exists (select * from sys.objects where object_id = object_id(N'[dbo].[fn_md5_hash_varcharmax]') and type in (N'fn', N'if', N'tf', N'fs', N'ft'))
			drop function [dbo].[fn_md5_hash_varcharmax]
go 

create function [dbo].[fn_md5_hash_varcharmax] ( @inputDataString varchar(max))
		returns varbinary(32)
		with schemabinding
		as
		begin
		declare
			@index int,
			@inputDataLength int,
			@returnMD5Hash varbinary(32),
			@inputDataStringTmp varchar(8000)

		set @returnMD5Hash = 0
		set @index = 1
		set @inputDataLength = len(@InputDataString)

		while @index <= @inputDataLength
		begin
			set @inputDataStringTmp = substring(@InputDataString, @index, 7968)+(case when @index>1 then convert(nvarchar(32), @returnMD5Hash,2) else '' end)
			set @returnMD5Hash = hashbytes('md5', @inputDataStringTmp)
			set @index = @index + 7968
		end

		return @returnMD5Hash
end
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_news') and name = 'text_hash')
begin
	alter table [dbo].[bik_news] 
		add text_hash as convert(nvarchar(32),[dbo].[fn_md5_hash_varcharmax]([text]),2)

	create nonclustered index [idx_bik_news_text_hash] on [dbo].[bik_news]
		(
			[text_hash] asc
		)

end 
go

exec sp_update_version '1.6.y.10', '1.6.y.11';
go