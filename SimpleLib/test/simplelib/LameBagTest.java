/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.Iterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wezyr
 */
public class LameBagTest {

    public LameBagTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getMillis method, of class SimpleDateUtils.
     */
    @Test
    public void testGetMillis() {
        LameBag<String> b = new LameBag<String>();
        b.add("aaa");
        b.add("bb");
        System.out.println("b={" + b.asString(": ", ", ") + "}");
        assertEquals(2, b.size());
        assertEquals(1, b.getCount("aaa"));
        assertEquals(1, b.getCount("bb"));
        assertEquals(0, b.getCount("zzz"));
        b.clear();
        System.out.println("b={" + b.asString(": ", ", ") + "}");
        assertEquals(0, b.size());
        assertEquals(0, b.getCount("aaa"));
        assertEquals(0, b.getCount("bzzz"));
        b.add("aaa");
        b.add("aaa");
        b.add("bb");
        b.add("bb");
        System.out.println("b={" + b.asString(": ", ", ") + "}");
        assertEquals(4, b.size());
        assertEquals(2, b.getCount("aaa"));
        assertEquals(0, b.getCount("bzzz"));
        assertEquals(2, b.getCount("bb"));
        b.add("bb2");
        System.out.println("b={" + b.asString(": ", ", ") + "}");
        assertEquals(5, b.size());
        assertEquals(2, b.getCount("aaa"));
        assertEquals(0, b.getCount("bzzz"));
        assertEquals(1, b.getCount("bb2"));
        assertEquals(2, b.getCount("bb"));
        b.clear();
        System.out.println("b={" + b.asString(": ", ", ") + "}");
        assertEquals(0, b.size());
        assertEquals(0, b.getCount("aaa"));
        assertEquals(0, b.getCount("bzzz"));
        b.clear();
        assertEquals(0, b.size());
        assertEquals(0, b.getCount("aaa"));
        assertEquals(0, b.getCount("bzzz"));
        System.out.println("b={" + b.asString(": ", ", ") + "}");

        b.add("aaa");
        b.add("zz");
        b.add("aaa");
        b.add("bb");
        b.add("bb");

        boolean x = false;
        Iterator<String> iter = b.iterator();
        while (iter.hasNext()) {
            String item = iter.next();
            if (x) {
                iter.remove();
            }
            x = !x;
        }
        System.out.println("b={" + b.asString(": ", ", ") + "}");
        assertEquals(3, b.size());
        b.clear();
        assertEquals(0, b.size());
    }
}
