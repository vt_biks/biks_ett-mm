/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.teradata;

import commonlib.LameUtils;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import static pl.bssg.metadatapump.teradata.TeradataPump.STOP_SHOW_TABLE_CALL_AFTER_ERROR_CNT;
import pl.trzy0.foxy.serverlogic.DriverNameConstants;
import pl.trzy0.foxy.serverlogic.db.ConnectionUtils;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pgajda
 */
public class TeradataDataModel extends TeradataPump {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected Set<String> areasMap = new HashSet<String>();
    protected List<TeradataBeanDataModel> teradataList = new LinkedList<TeradataBeanDataModel>();
    protected List<TeradataBeanProcesRel> procesToObjectRefList = new ArrayList<TeradataBeanProcesRel>();

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_TERADATA_DATAMODEL;
    }

    @Override
    protected Pair<Integer, String> startPump() {
        if (fakeModePump) {
            suspiciousNames.addAll(BaseUtils.splitUniqueBySep(
                    getTeradataConfigProp("teradata.suspiciousNamesForDebug", "S66_B0020_AGREEMENT"), ",", true));
            execNamedCommandWithCommit("prepareForFakeMode");
        }
        TeradataConfigBean configData = PumpUtils.readAdminBean("teradata", TeradataConfigBean.class, adhocDao);
        if (configData == null) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Error with loading config data!");
        }

        int stopShowTableCallAfterErrorCnt = BaseUtils.tryParseInteger(configData.maxErrorCount, STOP_SHOW_TABLE_CALL_AFTER_ERROR_CNT);

        try {
            tc = ConnectionUtils.getConnectionByConnectionString(DriverNameConstants.TERADATA, configData.url, configData.user, configData.password, "tmode=ANSI,charset=UTF8");
            if (tc == null) {
                return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "No connection with Teradata!");
            }
        } catch (Exception ex) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "No connection with Teradata! " + ex.getMessage());
        }

        try {
            if (configData.select == null) {
                return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Bad select query");
            }
            Statement createStatement = tc.createStatement();
            if (!fakeModePump) {
                execNamedCommandWithCommit("deleteTeradataDataModelOldTables");
            }
            /*
             int indexStarted = configData.select.indexOf("FROM", 0);
             String whereString = configData.select.substring(indexStarted);
             String ownerStatement = "select distinct(OwnerName) " + whereString;
             // get owners
             pumpLogger.pumpLog("Insert into temporary table: owners", 15);
             ResultSet ownerQuery = createStatement.executeQuery(ownerStatement);
             teradataList.clear();
             while (ownerQuery.next()) {
             String ownerName = ownerQuery.getString("OwnerName");
             teradataList.add(new TeradataBean(insertedLogId, "TeradataOwner", ownerName.trim(), null, "owner:" + ownerName.trim() + "|", null, null));
             }
             if (isWarnLoggingInFakeModeEnabled()) {
             logger.warn("fake: got owners, all beans now: " + teradataList.size());
             }
             */
            teradataList.add(new TeradataBeanDataModel(insertedLogId, "TeradataServer", "DWH", null, "DWH" + "|", null, null, null, null));
            // get obszar
            String getAreasQuery
                    = //"select * from VD_US_META_PROC.obszar"; //polkomtel
                    "select distinct ob.nazwa, ob.opis "
                    + "FROM DBC.Tables t "
                    + "join "
                    + "VD_US_META_PROC.V00160_tablica tab on tab.nazwa_tablica = t.TableName and tab.nazwa_baza = t.DatabaseName "
                    + "join VD_US_META_PROC.obszar ob on tab.id_obszar = ob.id_obszar";
//                    "select distinct ob.nazwa, ob.opis "
//                    + "FROM "
//                    + "VD_US_META_PROC.V00160_tablica tab "
//                    + "join VD_US_META_PROC.obszar ob on tab.id_obszar = ob.id_obszar";

            //String getAreasQuery = "select * from DB00_META.T0045_obszar"; //u mnie
            pumpLogger.pumpLog("Insert into temporary table: schemas", 30);
            //ResultSet executeQuery = createStatement.executeQuery(configData.select);
            ResultSet executeQuery = createStatement.executeQuery(getAreasQuery);
            while (executeQuery.next()) {
                String areaName = executeQuery.getString("nazwa");
                //schemasMap.add("'" + databaseName + "'");
                areasMap.add("'" + areaName + "'");
                String description = executeQuery.getString("opis").trim();
                //String ownerName = executeQuery.getString("OwnerName").trim();
                //teradataList.add(new TeradataBean(insertedLogId, "TeradataSchema", databaseName.trim(), executeQuery.getString("CommentString"), databaseName.trim() + "|", "owner:" + ownerName + "|", null));
                teradataList.add(new TeradataBeanDataModel(insertedLogId, "TeradataArea", areaName.trim(), description, "DWH|" + areaName.trim() + "|", "DWH|", null, null, null));
            }
            if (isWarnLoggingInFakeModeEnabled()) {
                logger.warn("fake: got schemas, all beans now: " + teradataList.size());
            }
            // get tables/views
            pumpLogger.pumpLog("Insert into temporary table: tables/views/procedure", 45);
            //ResultSet executeQueryTables = createStatement.executeQuery("SELECT DatabaseName,TableName,TableKind,CommentString,RequestText FROM DBC.Tables Where DatabaseName in (" + BaseUtils.mergeWithSepEx(schemasMap, ",") + ") AND (TableKind='T' OR TableKind='V' OR TableKind='P' OR TableKind='E')");
            ResultSet executeQueryTables = createStatement.executeQuery(
                    "SELECT DatabaseName,TableName,TableKind,CommentString,RequestText,ob.nazwa as area_name "
                    + "FROM DBC.Tables t "
                    + "join "
                    + "VD_US_META_PROC.V00160_tablica tab on tab.nazwa_tablica = t.TableName and tab.nazwa_baza = t.DatabaseName "
                    + "join VD_US_META_PROC.obszar ob on tab.id_obszar = ob.id_obszar");
            //+ "DB00_META.T00160_tablica tab on tab.nazwa_tablica = t.TableName and tab.nazwa_baza = t.DatabaseName "
            //+ "join DB00_META.T0045_obszar ob on tab.id_obszar = ob.id_obszar");

            List<Pair<String, TeradataBean>> forShowTableCalls = new ArrayList<Pair<String, TeradataBean>>();

            int tableCnt = 0;

            while (executeQueryTables.next()) {
                String tableName = executeQueryTables.getString("TableName");
                String databaseName = executeQueryTables.getString("DatabaseName");
                String tableKind = executeQueryTables.getString("TableKind").trim();
                String comment = executeQueryTables.getString("CommentString");
                String areaName = executeQueryTables.getString("area_name");
                String requestText = null;

                boolean addToShowTable = false;

                if (!tableKind.equals("P") && !tableKind.equals("E")) {
                    tablesMap.add(databaseName + tableName);
                    requestText = executeQueryTables.getString("RequestText");
                    addToShowTable = true;
                }
                //TeradataBean tb = new TeradataBean(insertedLogId, tableKindMap.get(tableKind), tableName.trim(), comment, "DWH|"+databaseName.trim() + "|" + tableName.trim() + "|", "DWH|"+databaseName.trim() + "|", requestText);

                String tableNameForObjId = modifyTableNameForObjId(tableName.trim());
                TeradataBeanDataModel tb = new TeradataBeanDataModel(insertedLogId, tableKindMap.get(tableKind), tableName.trim(), comment, "DWH|" + areaName + "|" + tableNameForObjId + "|", "DWH|" + areaName + "|", requestText, databaseName, tableName);
                if (isNameSuspiciousForFakeMode(tableName)) {
                    logger.warn("fake: adding bean with suspicious name to teradataList: " + tb);
                }
                teradataList.add(tb);
                tableCnt++;
                if (addToShowTable) {
                    forShowTableCalls.add(new Pair<String, TeradataBean>(tableKind, tb));
                }
            }
            if (isWarnLoggingInFakeModeEnabled()) {
                logger.warn("fake: added tables cnt=" + tableCnt + ", all beans now: " + teradataList.size());
            }
            if (isWarnLoggingInFakeModeEnabled()) {
                logger.warn("fake: tables for SHOW TABLE cnt=" + forShowTableCalls.size());
            }

            int showTableErrorCnt = 0;
            for (Pair<String, TeradataBean> fstp : forShowTableCalls) {
                String fullTableName = BaseUtils.dropOptionalSuffix(fstp.v2.branchNames, "|").replace("|", ".");
                String tableKind = fstp.v1;
                String requestText = fstp.v2.requestText;

                if (stopShowTableCallAfterErrorCnt > 0 && showTableErrorCnt >= stopShowTableCallAfterErrorCnt) {
                    requestText = "/* not calling SHOW TABLE due to " + showTableErrorCnt + " prev calls with no privilidges, showing last DDL statement on object */ \n\n" + requestText;
                } else {
                    try {
                        ResultSet executeTables = createStatement.executeQuery("SHOW " + tableKindMapForCreateSQL.get(tableKind) + " " + fullTableName);
                        while (executeTables.next()) {
                            requestText = executeTables.getString(1);
                        }
                    } catch (Exception e) {
                        showTableErrorCnt++;
                        if (isWarnLoggingInFakeModeEnabled()) {
                            logger.warn("fake: catched error (errCnt=" + showTableErrorCnt + ") for table: " + fullTableName + "\n"
                                    + e);
                        }
                        requestText = "/* error in SHOW TABLE: no privilidges, showing last DDL statement on object */ \n\n" + requestText;
                    }
                }
                fstp.v2.requestText = requestText;
            }

            // get indieces
            pumpLogger.pumpLog("Insert into temporary table: indieces", 70);
            //ResultSet executeQueryIndex = createStatement.executeQuery("SELECT DatabaseName, TableName, IndexNumber, IndexType, UniqueFlag, IndexName, ColumnName, ColumnPosition FROM DBC.Indices Where DatabaseName in (" + BaseUtils.mergeWithSepEx(schemasMap, ",") + ")");
            ResultSet executeQueryIndex = createStatement.executeQuery(
                    "SELECT DatabaseName, TableName, IndexNumber, IndexType,"
                    + "UniqueFlag, IndexName, ColumnName, ColumnPosition, ob.nazwa as area_name "
                    + "FROM DBC.Indices ind "
                    + "join "
                    + "VD_US_META_PROC.V00160_tablica tab on tab.nazwa_tablica = ind.TableName and tab.nazwa_baza = ind.DatabaseName "
                    + "join VD_US_META_PROC.obszar ob on tab.id_obszar = ob.id_obszar");
            while (executeQueryIndex.next()) {
                String tableName = executeQueryIndex.getString("TableName");
                String databaseName = executeQueryIndex.getString("DatabaseName");
                if (tablesMap.contains(databaseName + tableName)) {
                    String columnName = executeQueryIndex.getString("ColumnName");
                    String indexName = executeQueryIndex.getString("IndexName");
                    String indexType = executeQueryIndex.getString("IndexType").trim();
                    String areaName = executeQueryIndex.getString("area_name");
                    String idxTmp;
                    if ((idxTmp = indexNameAndTypeMap.get(databaseName + tableName + columnName)) != null) {
                        if (!idxTmp.contains("Primary")) {
                            indexNameAndTypeMap.put(databaseName + tableName + columnName, tableIndexTypeMap.get(indexType));
                        }
                    } else {
                        indexNameAndTypeMap.put(databaseName + tableName + columnName, tableIndexTypeMap.get(indexType));
                    }
                    //execNamedCommandWithCommit("insertTeradataIndexes", databaseName.trim() + "|" + tableName.trim() + "|", indexName == null ? "(#" + executeQueryIndex.getString("IndexNumber") + ")" : indexName, tableIndexTypeMap.get(indexType), tableIndexUniqueMap.get(executeQueryIndex.getString("UniqueFlag").trim()), columnName.trim(), executeQueryIndex.getInt("ColumnPosition"), fakeModePump);
                    String tableNameForObjId = modifyTableNameForObjId(tableName.trim());
                    execNamedCommandWithCommit("insertTeradataIndexes", "DWH|" + areaName + "|" + tableNameForObjId + "|", indexName == null ? "(#" + executeQueryIndex.getString("IndexNumber") + ")" : indexName, tableIndexTypeMap.get(indexType), tableIndexUniqueMap.get(executeQueryIndex.getString("UniqueFlag").trim()), columnName.trim(), executeQueryIndex.getInt("ColumnPosition"), fakeModePump);
                }
            }
            if (isWarnLoggingInFakeModeEnabled()) {
                logger.warn("fake: got indices, all beans now: " + teradataList.size());
            }
            // get columns
            pumpLogger.pumpLog("Insert into temporary table: columns", 85);
            //ResultSet executeQueryColumns = createStatement.executeQuery("SELECT DatabaseName, TableName, ColumnName, ColumnType, ColumnLength, DecimalFractionalDigits, DecimalTotalDigits, Nullable FROM DBC.COLUMNS Where DatabaseName in (" + BaseUtils.mergeWithSepEx(schemasMap, ",") + ")");
            ResultSet executeQueryColumns = createStatement.executeQuery(
                    "SELECT DatabaseName, TableName, ColumnName, ColumnType, ColumnLength,"
                    + "DecimalFractionalDigits, DecimalTotalDigits, Nullable, ob.nazwa as area_name  "
                    + "FROM DBC.COLUMNS col "
                    + "join  "
                    + "VD_US_META_PROC.V00160_tablica tab on tab.nazwa_tablica = col.TableName and tab.nazwa_baza = col.DatabaseName "
                    + "join VD_US_META_PROC.obszar ob on tab.id_obszar = ob.id_obszar");

            while (executeQueryColumns.next()) {
                String columnName = executeQueryColumns.getString("ColumnName");
                String columnType = columnTypeMap.get(executeQueryColumns.getString("ColumnType") != null ? executeQueryColumns.getString("ColumnType").trim() : null);
                String columnTypeToInsert;
                String databaseName = executeQueryColumns.getString("DatabaseName");
                String tableName = executeQueryColumns.getString("TableName");
                String nullable = executeQueryColumns.getString("Nullable");
                String areaName = executeQueryColumns.getString("area_name");
                if (tablesMap.contains(databaseName + tableName)) {
                    if (nullable == null) {
                        nullable = " NULL";
                    }
                    if (executeQueryColumns.getString("ColumnType") != null) {
                        if (executeQueryColumns.getString("ColumnType").equals("D")) {
                            columnTypeToInsert = columnType + "(" + executeQueryColumns.getString("DecimalFractionalDigits") + "," + executeQueryColumns.getString("DecimalTotalDigits") + ")";
                        } else if (typesWithLength.contains(executeQueryColumns.getString("ColumnType"))) {
                            columnTypeToInsert = columnType + "(" + executeQueryColumns.getString("ColumnLength") + ")";
                        } else {
                            columnTypeToInsert = columnType;
                        }
                    } else {
                        columnTypeToInsert = columnType;
                    }
                    String columnTypeIdx;
                    if ((columnTypeIdx = indexNameAndTypeMap.get(databaseName + tableName + columnName)) == null) {
                        columnTypeIdx = "TeradataColumn";
                    } else {
                        if (!columnTypeIdx.contains("Primary")) {
                            columnTypeIdx = "TeradataColumnIDX";
                        } else {
                            columnTypeIdx = "TeradataColumnPK";
                        }
                    }

                    //teradataList.add(new TeradataBean(insertedLogId, columnTypeIdx, columnName.trim(), (columnTypeToInsert == null ? "" : columnTypeToInsert + (nullable.equals("N") ? " NOT NULL" : " NULL")),"DWH|"+ databaseName.trim() + "|" + tableName.trim() + "|" + columnName.trim() + "|", "DWH|"+databaseName.trim() + "|" + tableName.trim() + "|", null));
                    String tableNameForObjId = modifyTableNameForObjId(tableName.trim());
                    teradataList.add(new TeradataBeanDataModel(insertedLogId, columnTypeIdx, columnName.trim(), (columnTypeToInsert == null ? "" : columnTypeToInsert + (nullable.equals("N") ? " NOT NULL" : " NULL")), "DWH|" + areaName + "|" + tableNameForObjId.trim() + "|" + columnName.trim() + "|", "DWH|" + areaName + "|" + tableNameForObjId.trim() + "|", null, databaseName, tableName));
                }
            }
            if (isWarnLoggingInFakeModeEnabled()) {
                logger.warn("fake: got columns, all beans now: " + teradataList.size());
            }

            pumpLogger.pumpLog("Insert into temporary table: ProcesToObject", 90);
            //powiązania tabel, obszarów do procesów
            ResultSet executeProcesToObject = createStatement.executeQuery(
                    "select * from ("
                    + "select "
                    + "	ptr.symb_proc,"
                    + "	ptr.id_tablica as id_obj,"
                    + "	ptr.id_typ_proces_tablica as id_typ_proces,"
                    + "	't' as obj_type,"
                    + "	'DWH|'||o.nazwa||'|' as bik_node_obj_id,"
                    + " o.nazwa as obszar,"
                    + "	t.nazwa_baza, "
                    + " t.nazwa_tablica "
                    + "from VD_US_META_PROC.proces_tablica_rel ptr "
                    + "join VD_US_META_PROC.V00160_tablica t on ptr.id_tablica=t.id_tablica "
                    + "join VD_US_META_PROC.obszar o on o.id_obszar = t.id_obszar "
                    + "union "
                    + "select "
                    + "	por.symb_proc,"
                    + "	por.id_obszar,"
                    + "	por.id_proces_obszar_rel_typ,"
                    + "	'o',"
                    + "	'DWH|'||o.nazwa||'|' as bik_node_obj_id,"
                    + " o.nazwa as obszar, "
                    + " cast(null AS varchar(32) ), "
                    + " cast(null AS varchar(32) ) "
                    + "FROM VD_US_META_PROC.Proces_obszar_rel por "
                    + " join "
                    + " VD_US_META_PROC.obszar o on o.id_obszar=por.id_obszar"
                    + ") as rel "
                    + "order by obj_type, symb_proc");

            while (executeProcesToObject.next()) {
                String symbProc = executeProcesToObject.getString("symb_proc");
                String idObj = executeProcesToObject.getString("id_obj");
                String idTypProces = executeProcesToObject.getString("id_typ_proces");
                String objType = executeProcesToObject.getString("obj_type");
                String bikNodeObjId = executeProcesToObject.getString("bik_node_obj_id");
                String area = executeProcesToObject.getString("obszar");
                String dbName = executeProcesToObject.getString("nazwa_baza");
                String nazwaTablica = executeProcesToObject.getString("nazwa_tablica");
                if (nazwaTablica != null && !nazwaTablica.isEmpty()) {
                    bikNodeObjId += modifyTableNameForObjId(nazwaTablica) + "|";
                }
                procesToObjectRefList.add(new TeradataBeanProcesRel(symbProc, idObj, idTypProces, objType, bikNodeObjId, area, dbName));
            }

            insertListToDBInPackages(procesToObjectRefList, "insertProcesToObjectRefList");

            // move data to bik_teradata
            if (isWarnLoggingInFakeModeEnabled()) {
                logger.warn("teradataList.size() = " + teradataList.size());

                for (int i = 0; i < teradataList.size(); i++) {
                    TeradataBean tb = teradataList.get(i);
                    if (/*
                             * BaseUtils.safeEquals("S66_B0020_AGREEMENT", tb.name)
                             */isNameSuspiciousForFakeMode(tb.name)) { //S66_B0020_AGREEMENT AlertControl
                        logger.warn("suspicious bean @idx=" + i + ": " + tb);
                    }
                }
            }
            if (!fakeModePump) {
                insertListToDBInPackages(teradataList, "insertTeradataMetadataDataModel");
            }

            if (logger.isInfoEnabled()) {
                logger.info("Update BIKS structure!");
            }
            pumpLogger.pumpLog("Update BIKS structure" + (fakeModePump ? "(fake mode)" : ""), 100);
            if (!fakeModePump) {
                execNamedCommandWithCommit("moveToBikTeradataDataModel");
            }

            if (!fakeModePump) {
                execNamedCommandWithCommit("moveTeradataDataModelMetadataIntoBikNode");
                execNamedCommandWithCommit("insertDataModelConnections");
                execNamedCommandWithCommit("addTeradataTree2Instance", "TeradataDataModel", BaseUtils.tryParseInteger(configData.instanceId));
            }
            if (logger.isInfoEnabled()) {
                logger.info("All right. Success!");
            }
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, "Success");
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("" + ex, ex);
            }
            throw new LameRuntimeException(ex.getMessage(), ex);
        } finally {
            if (tc != null) {
                closeConnection(tc);
            }
        }
    }

    protected String modifyTableNameForObjId(String tableName) {
        Pattern pattern = Pattern.compile("^[tT]([0-9]+)_.*");

        String tableNameObjId = tableName;
        if (tableNameObjId != null) {
            Matcher matcher = pattern.matcher(tableNameObjId);
            if (matcher.matches()) {
                tableNameObjId = tableNameObjId.substring(0, tableNameObjId.indexOf('_'));
            }
        }
        return tableNameObjId;
    }
}
