/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author tflorczak
 */
public class ConfluenceObjectBean implements Serializable {

    public String id;
    public String parentId;
    public String name;
    public String kind;
    public String content;
    public String url;
    public String author;
    public Date publishDate;
    public String branchIds;
    public String space;
    public boolean hasNoChildren;
    public boolean addAsRoot;
    public List<ConfluenceObjectBean> attachments;
    public Integer groupId;
    public String authorFullName;
    public Integer treeId;

    public ConfluenceObjectBean() {
    }

    public ConfluenceObjectBean(String id, String parentId, String name, String kind, String content, String url, String author, Date publishDate, String branchIds, String space, boolean hasNoChildren, boolean addAsRoot) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;
        this.kind = kind;
        this.content = content;
        this.url = url;
        this.author = author;
        this.publishDate = publishDate;
        this.branchIds = branchIds;
        this.space = space;
        this.hasNoChildren = hasNoChildren;
        this.addAsRoot = addAsRoot;
    }

    public ConfluenceObjectBean(String id, String parentId, String name, String kind, String content, String url, String author, Date publishDate, String branchIds, String space, boolean hasNoChildren, boolean addAsRoot, List<ConfluenceObjectBean> attachments, Integer groupId, String authorFullName, Integer treeId) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;
        this.kind = kind;
        this.content = content;
        this.url = url;
        this.author = author;
        this.publishDate = publishDate;
        this.branchIds = branchIds;
        this.space = space;
        this.hasNoChildren = hasNoChildren;
        this.addAsRoot = addAsRoot;
        this.attachments = attachments;
        this.groupId = groupId;
        this.authorFullName = authorFullName;
        this.treeId = treeId;
    }

    public void setAttachments(List<ConfluenceObjectBean> attachments) {
        this.attachments = attachments;
    }

    @Override
    public String toString() {
        return "ConfluenceObjectBean{" + "id=" + id + ", parentId=" + parentId + ", name=" + name + ", kind=" + kind + ", url=" + url + ", author=" + author + ", publishDate=" + publishDate + ", branchIds=" + branchIds + ", space=" + space + ", hasNoChildren=" + hasNoChildren + ", addAsRoot=" + addAsRoot + '}';
    }
}
