/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import commonlib.LameUtils;
import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class SQLServerTestExecutor extends AbstractDatabaseTestExecutor {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    public SQLServerTestExecutor(IBIKSDQMSubservice service) {
        super(service);
    }

    @Override
    public boolean initConnection(DQMTestBean test) {
        this.test = test;
        return initConnectionInner(test.dbServerId, test.connectionId, useFunctionInsteadSQLQuery ? test.functionDatabase : test.queryDatabase);
    }

    protected boolean initConnectionInner(Integer dbServerId, Integer connectionId, String databaseName) {
        try {
            this.mc = new MssqlConnection(getConnectionConfig(dbServerId, connectionId, databaseName));
            mc.setDbStatementExecutionWarnThresholdMillis(-1);
            mc.setAutoCommit(true);
            mc.execQrySingleValInteger("SELECT 1", false);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in initConnection", e);
            }
            return false;
        }
        return true;
    }

    protected MssqlConnectionConfig getConnectionConfig(Integer dbServerId, Integer connectionId, String databaseName) {
        if (dbServerId != null) {
            ConnectionParametersDBServerBean serverConfig = getDatabaseConfig(dbServerId, connectionId);
            return new MssqlConnectionConfig(serverConfig.server, serverConfig.instance, serverConfig.databaseName != null ? serverConfig.databaseName : databaseName, serverConfig.user, serverConfig.password);
        } else { // w przypadku testów wieloźródłowych, wykonujemy zapytanie na bazie BIKSa
            return service.getBIKSDBParameters();
        }
    }

    @Override
    protected String getCallTestFunctionQuery(String functionName) {
        functionName = functionName.endsWith(")") ? functionName : functionName + "()";
        return "select * from " + functionName;
    }

    @Override
    public boolean initConnection(DQMMultiSourceBean source) {
        this.source = source;
        return initConnectionInner(source.dbServerId, source.connectionId, source.databaseName);
    }

    @Override
    public String getSourceName() {
        return "MSSQL";
    }
}
