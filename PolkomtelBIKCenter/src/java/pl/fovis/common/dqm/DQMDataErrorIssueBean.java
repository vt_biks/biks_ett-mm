/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

import java.util.Date;
import simplelib.BeanWithIntIdBase;

/**
 *
 * @author ctran
 */
public class DQMDataErrorIssueBean extends BeanWithIntIdBase {

    public Integer lp;
    public int nodeId;
    public String name;
    public int groupNodeId;
    public String groupName;
    public int leaderNodeId;
    public String leaderName;
    public Integer errorStatus;
    public Date createdDate;
    public String actionPlan;
}
