/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.postgresql;

/**
 *
 * @author tflorczak
 */
public class PostgresColumnBean {
//table_schema, table_name, column_name, ordinal_position, column_default, is_nullable, column_type

    public String name;
    public String owner;
    public String tableName;
    public String databaseName;
    public String serverName;
    public int columnId;
    public String description;
    public boolean isNullable;
    public String type;
    public String defaultField;

    public PostgresColumnBean(String name, String owner, String tableName, String databaseName, String serverName, int columnId, String description, boolean isNullable, String type, String defaultField) {
        this.name = name;
        this.owner = owner;
        this.tableName = tableName;
        this.databaseName = databaseName;
        this.serverName = serverName;
        this.columnId = columnId;
        this.description = description;
        this.isNullable = isNullable;
        this.type = type;
        this.defaultField = defaultField;
    }

}
