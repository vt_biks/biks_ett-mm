-- zarejestrowani userzy - tak na rozgrzewk�
SELECT d.id as database_id, first_name, last_name, d.database_name
FROM [MasterBIKS].[dbo].[mltx_registered_user] u inner join [MasterBIKS].[dbo].[mltx_database] d on u.id = d.creator_id

-- loginy (e-maile) + bazy do kt�rych maj� dost�p + tw�rcy tych baz (e-mail tw�rcy itp.)
select * from [MasterBIKS].[dbo].[mltx_login_database] ld inner join [MasterBIKS].[dbo].[mltx_database] d on ld.database_id = d.id
inner join [MasterBIKS].[dbo].[mltx_registered_user] u on d.creator_id = u.id
where ld.login like 'dstepnik%@bssg.pl' -- tu jak raz mnie interesowa�y bazy, do kt�rych dost�p ma Damian
order by login

-- interesuj� mnie dwie bazy z powy�szych, "nazw� bazy" zmienia si� w polach first_name + last_name tw�rcy (zarejestrowanego u�ytkownika)

-- zmiana nazwy bazy 1
update [MasterBIKS].[dbo].[mltx_registered_user]
set first_name = 'ZUS', last_name = '' where id = 7078

-- zmiana nazwy bazy 2
update [MasterBIKS].[dbo].[mltx_registered_user]
set first_name = 'mBank', last_name = '' 
where email = 'dstepnik+mbank@bssg.pl'


-- sprawdzamy u�ytkownik�w na tych bazach
select * from biks_empty_pl_PZLXQZEW..bik_system_user

select * from biks_empty_pl_PF90Q72M..bik_system_user

-- dodatkowo - jakie role ma jeden z nich na jednej z tych baz
select * from biks_empty_pl_PF90Q72M..bik_user_right ur inner join biks_empty_pl_PF90Q72M..bik_right_role rr on ur.right_id = rr.id
where ur.user_id = 102

-- chcemy temu loginowi doda� dost�p do drugiej bazy (biks_empty_pl_PZLXQZEW)
-- tmierzwa@valuetank.com
-- tjm001

-- czym si� r�ni� app_propsy na tych dw�ch bazach?

select * from biks_empty_pl_PZLXQZEW..bik_app_prop apnew full join biks_empty_pl_PF90Q72M..bik_app_prop apold
on apold.name = apnew.name
where coalesce(apold.val, '?w">we?>') <> coalesce(apnew.val, '?wxxwe?>')


-- wszystko ju� mamy, to czas na zmiany!!!

-- app_propsy - r�cznie, ale mo�na zrobi� skrypt...
update biks_empty_pl_PZLXQZEW..bik_app_prop
set val = '1' where name = 'developerMode'

update biks_empty_pl_PZLXQZEW..bik_app_prop
set val = 'true' where name = 'showFullPathInJoinedObjsPane'


-- dodajemy login do bazy na MasterBIKS

insert into [MasterBIKS].[dbo].[mltx_login_database] (login, database_id, is_disabled)
values ('tmierzwa@valuetank.com', 7082, 0)

-- dodajemy usera w bazie

insert into biks_empty_pl_PZLXQZEW..bik_system_user (login_name, is_disabled, name)
values ('tmierzwa@valuetank.com', 0, 'tmierzwa@valuetank.com')

select scope_identity() -- id dodanego bik_system_usera... za��my, �e wysz�o 103

-- select * from biks_empty_pl_PZLXQZEW..bik_system_user

-- dodajemy admina dla naszego nowego usera (jego id wysz�o z zapytania o scope_identity())
insert into biks_empty_pl_PZLXQZEW..bik_user_right (user_id, right_id)
values (103, 1)
