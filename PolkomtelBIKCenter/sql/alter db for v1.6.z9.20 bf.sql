﻿exec sp_check_version '1.6.z9.20';
go

if not exists (select * from bik_translation where code='userLoginOnce' and kind='stat')
begin
	insert into bik_translation (code,txt,lang,kind) values ('userLoginOnce', 'Liczba użytkowników w systemie', 'pl', 'stat')
	insert into bik_translation (code,txt,lang,kind) values ('userLoginOnce', 'Number of users in system', 'en', 'stat')
end
if not exists (select * from bik_translation where code='userLogin' and kind='stat')
begin
	insert into bik_translation (code,txt,lang,kind) values ('userLogin', 'Logowanie do systemu', 'pl', 'stat')
	insert into bik_translation (code,txt,lang,kind) values ('userLogin', 'Number of login', 'en', 'stat')
end
if not exists (select * from bik_translation where code='newSearch' and kind='stat')
begin
	insert into bik_translation (code,txt,lang,kind) values ('newSearch', 'Wyszukiwarka', 'pl', 'stat')
	insert into bik_translation (code,txt,lang,kind) values ('newSearch', 'Search', 'en', 'stat')
end


-- poprzednia wersja w pliku: alter db for v1.2.3.7 pg.sql
-- dodanie tłumaczenia
if  exists (select * from sys.objects where object_id = object_id('[dbo].[sp_get_all_statistics_ext_dict]') and type in ('p', 'pc'))
drop procedure [dbo].[sp_get_all_statistics_ext_dict]
go
 
create procedure [dbo].[sp_get_all_statistics_ext_dict](@lang varchar(3)) as
begin
	 set nocount on

    create table #tmp
    (
            counter_name varchar(255),
            description varchar(255),
            best_parameter varchar(255),
            day_cnt int,
            week_cnt int,
            thirty_cnt int,
            all_cnt int,
            total int
    )

    declare @treeId int
    declare @counterName varchar(255)
    declare @description varchar(255)
    declare @bestParameter varchar(255)
    declare @cnt1 int, @cnt7 int, @cnt30 int, @cntAll int, @total int 

    declare tree_to_statistics_curs cursor for 
            select bt.id, bsd.counter_name, bsd.description
                    from bik_statistic_dict bsd
                    left join bik_tree bt on bsd.counter_name = bt.code
                    where 
                     bt.is_hidden = 0

    open tree_to_statistics_curs
    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description

    while @@fetch_status = 0
            begin
					select @bestParameter = '', @cnt1 = 0, @cnt7 = 0, @cnt30 = 0, @cntAll = 0, @total = 0;
                    with table_stat
                    as
                    (
                            select bse.grouping_node_id, bn.name, count(*) as all_cnt, 
                            sum(case when datediff(d, bse.event_date, sysdatetime()) <= 1 then 1 else 0 end) as day_cnt, 
                            sum(case when datediff(d, bse.event_date, sysdatetime())  <= 7 then 1 else 0 end) as week_cnt,
                            sum(case when datediff(d, bse.event_date, sysdatetime())  <= 30 then 1 else 0 end) as thirty_cnt
                            from bik_statistic_ext bse
                            left join bik_node bn on bn.id = bse.grouping_node_id
                            where bse.tree_id = @treeId
                            group by bse.tree_id, bse.grouping_node_id, bn.name
                    )
                    select top 1 @bestParameter=name, @cntAll=all_cnt, @cnt1=statGroup.day_cnt, 
                    @cnt7=statGroup.week_cnt, @cnt30=statGroup.thirty_cnt, @total=statGroup.total
                    from table_stat,
                    (select sum(day_cnt) day_cnt, sum(week_cnt) week_cnt, sum(thirty_cnt) thirty_cnt , sum( all_cnt) total from table_stat) as statGroup
                    order by table_stat.all_cnt desc
                    --select top 1 @bestParameter=name, @cnt1=day_cnt, @cnt7=week_cnt, @cnt30=thirty_cnt, @cntAll=all_cnt 
                    --,@total=(select sum( all_cnt) from table_stat) 
                    --from table_stat order by all_cnt desc			

                    insert into #tmp(counter_name, description, best_parameter, day_cnt, week_cnt, thirty_cnt, all_cnt, total)
                            values(@counterName, @description, @bestParameter, @cnt1, @cnt7, @cnt30, @cntAll, @total )

                    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description
            end	

    close tree_to_statistics_curs;
    deallocate tree_to_statistics_curs


	declare tree_to_statistics_curs cursor for 
            select bt.id, bsd.counter_name, bsd.description
                    from bik_statistic_dict bsd
                    left join bik_tree bt on bsd.counter_name = bt.code
                    where 
                     bt.id is null


    open tree_to_statistics_curs
    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description

    while @@fetch_status = 0
            begin
					select @bestParameter = '', @cnt1 = 0, @cnt7 = 0, @cnt30 = 0, @cntAll = 0, @total = 0;
                    with table_stat
                    as
                    (
							select bs.parametr as name,
							count(*) as all_cnt,
							sum(case when datediff(D, bs.event_date, sysdatetime()) <= 1 then 1 else 0 end) as day_cnt, 
							sum(case when datediff(D, bs.event_date, sysdatetime())  <= 7 then 1 else 0 end) as week_cnt,
							sum(case when datediff(D, bs.event_date, sysdatetime())  <= 30 then 1 else 0 end) as thirty_cnt
							from 
							bik_statistic bs 
							left join bik_tree bt on bs.counter_name = bt.code  
							where 
							bs.counter_name = @counterName and (bt.id is null or bt.is_hidden = 0)
							group by bs.parametr 
                            
                    )
                    select top 1 @bestParameter=name, @cntAll=all_cnt, @cnt1=statGroup.day_cnt, 
                    @cnt7=statGroup.week_cnt, @cnt30=statGroup.thirty_cnt, @total=statGroup.total
                    from table_stat,
                    (select sum(day_cnt) day_cnt, sum(week_cnt) week_cnt, sum(thirty_cnt) thirty_cnt , sum( all_cnt) total from table_stat) as statGroup
                    order by table_stat.all_cnt desc

                    insert into #tmp(counter_name, description, best_parameter, day_cnt, week_cnt, thirty_cnt, all_cnt, total)
                            values(@counterName, @description, @bestParameter, @cnt1, @cnt7, @cnt30, @cntAll, @total )

                    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description
            end	

    close tree_to_statistics_curs;
    deallocate tree_to_statistics_curs
    
    select counter_name,coalesce(txt, description) as description, best_parameter, day_cnt, week_cnt, thirty_cnt, all_cnt, total
	from #tmp left join bik_translation bt on (#tmp.counter_name=bt.code and lang=@lang and (kind='tree' or kind='stat'))
	
end
go
exec sp_update_version '1.6.z9.20', '1.6.z9.21';
go