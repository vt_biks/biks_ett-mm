exec sp_check_version '1.0.86.7';
go


alter table bik_metapedia add versions varchar(500);
GO

if not exists (select * from bik_node_kind where code='GlossaryVersion')
insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder)
 values ('GlossaryVersion','Wersja definicji','glossary_ver','Glossary',0)

if not exists (select * from bik_node_kind where code='GlossaryNotCorpo')
insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder) values ('GlossaryNotCorpo','Glosariusz','glossary_ncorp','Glossary',0) 

exec sp_update_version '1.0.86.7', '1.0.86.8';
go
