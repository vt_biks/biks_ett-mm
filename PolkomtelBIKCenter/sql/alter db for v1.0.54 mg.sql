﻿exec sp_check_version '1.0.54';

alter table bik_node
add is_built_in int default 0
go

update bik_node set is_built_in = 0
go

alter table bik_node
alter column is_built_in int not null
go

update bik_node
set is_built_in = 1
from bik_node n
inner join bik_tree t on t.id = n.tree_id
inner join bik_node_kind nk on nk.id = n.node_kind_id
where nk.code = 'DocumentsDefaultFolder' or (t.tree_kind = 'Tezaurus' and nk.code = 'TezaurusCategory' and n.name = 'Słowa kluczowe')
  
exec sp_update_version '1.0.54', '1.0.55';
go
