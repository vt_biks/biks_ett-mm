﻿exec sp_check_version '1.3.0.9';
go

alter table bik_mona_dict_column add data_type varchar(15);
go

alter table bik_mona_dict_column add is_main_key integer;
go

exec sp_add_menu_node 'Admin', 'Lisa słowniki', 'admin:lisa'
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
declare @menuNodeId int = (select id from bik_node where tree_id = @tree_id and obj_id = 'admin:lisa')        
exec sp_node_init_branch_id @tree_id, @menuNodeId
go

exec sp_add_menu_node 'admin:lisa', 'Edycja metadanych', '#admin:lisa:metadata'
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
declare @menuNodeId int = (select id from bik_node where tree_id = @tree_id and obj_id = '#admin:lisa:metadata')        
exec sp_node_init_branch_id @tree_id, @menuNodeId
go

alter table bik_mona_dict_column drop column dictionary_id;
go
 
alter table bik_mona_dict_column add dictionary_name varchar(30);
go

sp_rename 'bik_mona_dict_column', 'bik_lisa_dict_column';
go

exec sp_update_version '1.3.0.9', '1.3.0.10';
go

