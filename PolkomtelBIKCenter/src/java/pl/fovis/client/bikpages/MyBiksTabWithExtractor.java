/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.HashMap;
import java.util.List;
import pl.fovis.client.entitydetailswidgets.IGridNodeAwareBeanPropsExtractor;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanGridWidget;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author beata
 */
public abstract class MyBiksTabWithExtractor<T> extends MyBIKSTabBase {

    protected abstract IGridNodeAwareBeanPropsExtractor<T> getExtractor();

    protected abstract Request callServiceToGetData(AsyncCallback<List<T>> callback);

    protected abstract String setTextWhenListEmpty();

    @Override
    public void show(final VerticalPanel vp) {

        callServiceToGetData(new StandardAsyncCallback<List<T>>() {
            public void onSuccess(List<T> result) {
                vp.clear();
                if (!BaseUtils.isCollectionEmpty(result)) {

                    NodeAwareBeanGridWidget<T> gw = new NodeAwareBeanGridWidget<T>(true, getExtractor(), new HashMap<Integer, String>());
                    vp.add(gw.buildGrid(result, result, 0, result.size()));
                } else {
                    Label textWhenListEmpty = new Label(setTextWhenListEmpty());
                    textWhenListEmpty.setStyleName("MyBIK");
                    vp.add(textWhenListEmpty);
                }
            }
        });
    }
}
