/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import org.apache.lucene.analysis.Analyzer;

/**
 *
 * @author pmielanczuk
 */
public class MySimpleAnalyzer extends Analyzer {

    @Override
    protected Analyzer.TokenStreamComponents createComponents(final String fieldName) {
        return new Analyzer.TokenStreamComponents(new MyLowerCaseTokenizer());
    }
}
