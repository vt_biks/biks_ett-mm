/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import pl.bssg.metadatapump.ThreadedPumpExecutor;
import pl.bssg.metadatapump.confluence.IConfluenceConnector;
import pl.bssg.metadatapump.dqc.DQCTestRequestsReceiver;
import pl.bssg.metadatapump.sapbo.biadmin.IBIAConnector;
import pl.fovis.common.AppPropBean;
import pl.fovis.common.ChangedRankingBean;
import pl.fovis.common.NodeModificationLogEntryBean;
import pl.fovis.server.dqm.DQMTestManager;
import pl.fovis.server.drools.ThreadedDroolsEngine;
import pl.fovis.server.fts.ISimilarNodesWizard;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public class BOXIServiceImplData {

    protected Map<String, AppPropBean> appPropsCache;
    protected List<AppPropBean> nonSensitiveAppPropsCache;
//    protected HashSet<String> logonUsers = new HashSet<String>();
//    protected boolean checkLogonUsers = false;
    final protected Map<String, Date> loggedUsers = new ConcurrentHashMap<String, Date>();
    // userName -> instanceId -> access time
    final protected Map<String, Map<String, Date>> loggedUserAppInstances = new ConcurrentHashMap<String, Map<String, Date>>();
    final protected Set<String> instanceIdsForFullRefresh = new HashSet<String>();
    final protected Set<String> disabledUsers = new HashSet<String>();
    final protected Map<String, Long> userDataInvalidatedTimeMillis = new ConcurrentHashMap<String, Long>();
    //protected Map<String, Integer> nodeKindIdByCode;
    protected List<Integer> inactiveTestNodeKindId;
    //protected IResourceFromProvider<String> namedSqlsTextResource;
    protected Thread pumpThread;
    protected Thread mailSenderThread;
    protected Thread biaArchiveThread;
    protected ThreadedMailSender mailSender;
    protected ThreadedPumpExecutor pumpExecutor;
    protected DQCTestRequestsReceiver dqcTestRequestsReceiver;
    protected ThreadedBIAdminArchive biaArchive;
    protected IConfluenceConnector confluenceProxyConnector;
    protected IBIAConnector biaProxyConnector;
//    protected HeartbeatMonitorTask heartbeat;
//    protected long lastRankingChangedTimeStamp;
    protected ChangedRankingBean currentUserRanking;// = new ChangedRankingBean();
    final protected Map<Integer, Map<Long, NodeModificationLogEntryBean>> nodeModificationLog = new HashMap<Integer, Map<Long, NodeModificationLogEntryBean>>();
    final protected Map<Integer, Pair<Long, String>> treeIdToModificationTimeStampMap = new HashMap<Integer, Pair<Long, String>>();
    //---
    //ww:dqmVsMltx to musi być tu, a nie globalnie w serwisie DQMServiceImpl
    protected DQMTestManager dqmTestExecutor;
    protected boolean hasCrrLabels;
    protected boolean suggestSimilarResults;
    protected boolean useLuceneSearch;
    public boolean hasFulltextCatalog;
    protected ISimilarNodesWizard similarNodesWizard;
    protected ThreadedDroolsEngine droolsEngine;
    protected Thread droolsEngineThread;

    public void destroy() {
        if (pumpExecutor != null) {
            pumpExecutor.destroy();
        }
        if (mailSender != null) {
            mailSender.destroy();
        }

        if (mailSenderThread != null) {
            mailSenderThread.interrupt();
        }

        if (biaArchive != null) {
            biaArchive.destroy();
        }
        if (biaArchiveThread != null) {
            biaArchiveThread.interrupt();
        }
        if (pumpThread != null) {
//            System.out.println("BOXIServiceImplData.destroy(): pumpThread.interrupt() start");
            pumpThread.interrupt();
//            System.out.println("BOXIServiceImplData.destroy(): pumpThread.interrupt() done");
        } else {
//            System.out.println("BOXIServiceImplData.destroy(): pumpThread == null");
        }

        if (similarNodesWizard != null) {
            similarNodesWizard.destroy();
        }
    }
}
