﻿exec sp_check_version '1.8.3.1';
go

if not exists (select 1 from bik_app_prop where name='showTitleInEntityHeader')
insert into bik_app_prop(name, val) values ('showTitleInEntityHeader', 'true')
else update bik_app_prop set val='true' where name='showTitleInEntityHeader' 

exec sp_update_version '1.8.3.1', '1.8.3.2';
go
