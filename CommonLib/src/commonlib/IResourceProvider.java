/*
 * IResourceProvider.java
 *
 * Created on 10 październik 2007, 11:44
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib;

/**
 *
 * @author wezyr
 */
public interface IResourceProvider {
    public long getLastModified(String resourceName);
    public byte[] loadAsBytes(String resourceName);
}
