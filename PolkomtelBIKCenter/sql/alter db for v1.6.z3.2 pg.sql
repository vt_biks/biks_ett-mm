exec sp_check_version '1.6.z3.2';
go


if not exists( select * from dbo.bik_node_kind where code = 'TeradataArea')
	insert into dbo.bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
		values('TeradataArea' ,'Obszar danych', 'tablearea', 'Metadata', 1, 8, 1 )

if not exists( select * from dbo.bik_node_kind where code = 'TeradataServer')
	insert into dbo.bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
		values('TeradataServer' ,'Teradata Server', 'mssqlServer', 'Metadata', 0, 10, 0 )

if not exists( select * from dbo.bik_icon where name = 'tablearea')
	insert into dbo.bik_icon(name) values('tablearea')


if not exists( select * from bik_tree where code = 'TeradataDataModel')
  begin 
		insert into bik_tree(name, code, tree_kind, branch_level_for_statistics, is_hidden)
		values('Model danych', 'TeradataDataModel', 'Metadata', 2, 1)

		exec sp_add_menu_node 'metadata', 'Logiczny model danych', 'logicdatamodel'
		exec sp_add_menu_node 'logicdatamodel', '@', '$TeradataDataModel'
   end
   
declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

--
--wstawienie g��wnego elementu Modelu Danych

declare @treeId int
select @treeId = id from bik_tree where code ='TeradataDataModel'
declare @nodeKindId int
select @nodeKindId = id from bik_node_kind where code='TeradataServer'

if not exists (select top 1 id from bik_node where tree_id = @treeId and obj_id = 'TeradataServerNode' and is_deleted = 0 )
insert into bik_node (parent_node_id,node_kind_id,name,tree_id,obj_id) values (null, @nodeKindId,'DWH',@treeId,'TeradataServerNode');
go


--lista dostepnych konektorow
/*if not exists(select * from bik_app_prop where name = 'availableConnectors' and val like '%Teradata Data Model%')
	update bik_app_prop
		set val = val + ',Teradata Data Model'
		where name = 'availableConnectors'
*/
--scheduler
if not exists( select * from bik_data_source_def where description = 'Teradata Data Model' )  
  begin
	insert into bik_data_source_def(description)
	values ('Teradata Data Model')    
	
    declare @sourceId int, @priority int;
    select @sourceId = id from bik_data_source_def where description = 'Teradata Data Model'

	insert into bik_schedule(source_id, hour, minute, interval, is_schedule, priority)
	values(@sourceId, 0, 0, 1, 0, 11)
  end
	
	

if not exists(select * from sys.objects where object_id = object_id(N'[bik_teradata_data_model]') and type in (N'U'))
begin 
	create table [dbo].[bik_teradata_data_model](
		[id] [int] identity(1,1) not null,
		[data_load_log_id] [int] null,
		[type] [varchar](100) not null,
		[name] [varchar](100) not null,
		[extra_info] [varchar](max) null,
		[branch_names] [varchar](1000) null,
		[parent_branch_names] [varchar](max) null,
		[request_text] [varchar](max) null,
		[database_name] [varchar](100) NULL,
		[table_name] [varchar](100) NULL,
	primary key clustered 
	(
		[id] asc
	)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
	) on [primary]

	alter table [dbo].[bik_teradata_data_model]  with check add foreign key([data_load_log_id])
	references [dbo].[bik_data_load_log] ([id])
end
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model]') and name = N'idx_bik_teradata_data_model_name')
	create nonclustered index [idx_bik_teradata_data_model_name] on [dbo].[bik_teradata_data_model]
	(
		name asc
	)

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model]') and name = N'idx_bik_teradata_data_model_database_name')
	create nonclustered index [idx_bik_teradata_data_model_database_name] on [dbo].[bik_teradata_data_model]
	(
		database_name asc
	)

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model]') and name = N'idx_bik_teradata_data_model_table_name')
	create nonclustered index [idx_bik_teradata_data_model_table_name] on [dbo].[bik_teradata_data_model]
	(
		table_name asc
	)

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model]') and name = N'idx_bik_teradata_data_model_branch_names')
	create nonclustered index [idx_bik_teradata_data_model_branch_names] on [dbo].[bik_teradata_data_model]
	(
		branch_names asc
	)


if not exists(select * from sys.objects where object_id = object_id(N'[bik_teradata_data_model_index]') and type in (N'U'))
begin
	create table [dbo].[bik_teradata_data_model_index](
		[id] [int] identity(1,1) not null,
		[table_branch_names] [varchar](max) not null,
		[name] [varchar](max) null,
		[type] [varchar](255) not null,
		[unique_flag] [varchar](255) not null,
		[column_name] [varchar](max) not null,
		[column_position] [int] not null,
	primary key clustered 
	(
		[id] asc
	)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
	) on [primary]
end
go	
	
if not exists(select * from sys.objects where object_id = object_id(N'[aaa_teradata_data_model_object]') and type in (N'U'))
begin
	CREATE TABLE [dbo].[aaa_teradata_data_model_object](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[data_load_log_id] [int] NOT NULL,
		[type] [varchar](100) NOT NULL,
		[name] [varchar](100) NOT NULL,
		[extra_info] [varchar](max) NULL,
		[branch_names] [varchar](1000) NULL,
		[parent_branch_names] [varchar](max) NULL,
		[request_text] [varchar](max) NULL,
		[database_name] [varchar](100) NULL,
		[table_name] [varchar](100) NULL
	PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]	
end
go
	
	
if not exists(select * from sys.objects where object_id = object_id(N'[aaa_teradata_data_model_process_object_rel]') and type in (N'U'))
begin
	CREATE TABLE [dbo].[aaa_teradata_data_model_process_object_rel](
		[symb_proc] [varchar](80) NOT NULL,
		[id_obj] [int] NOT NULL,
		[obj_type] [char](1) NOT NULL,
		[id_typ_process] [varchar](3) NOT NULL,
		[bik_node_obj_id] [varchar](800) NOT NULL,
		[area] [varchar](80) NOT NULL,
		[database_name] [varchar](100) NULL
	)	
end
go
	

if not exists(select * from sys.objects where object_id = object_id(N'[bik_teradata_data_model_process_object_rel]') and type in (N'U'))
begin
	CREATE TABLE [dbo].[bik_teradata_data_model_process_object_rel](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[symb_proc] [varchar](80) NOT NULL,
		[id_obj] [int] NOT NULL,
		[obj_type] [char](1) NOT NULL,
		[id_typ_process] [varchar](3) NOT NULL,
		[bik_node_obj_id] [varchar](800) NOT NULL,
		[area] [varchar](80) NOT NULL,
		[database_name] [varchar](100) NULL
	PRIMARY KEY CLUSTERED 
	(
		[id]  ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]	
end
go	

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model_process_object_rel]') and name = N'idx_tdmpor_bik_node_obj_id')
	create nonclustered index [idx_tdmpor_bik_node_obj_id] on [dbo].[bik_teradata_data_model_process_object_rel]
	(
		[bik_node_obj_id] asc
	)
	
	
--zmiana procedury wstawiajace dane do bik_node
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node]
GO

create procedure [dbo].[sp_insert_into_bik_node](@tree_code varchar(255))

as
	declare @table_folders varchar(255);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	declare @diag_level int = 0;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'

	if(@tree_code = 'Connections')
	begin
		set @kinds = '''MetaData.DataConnection'', ''CCIS.DataConnection'', ''CommonConnection''';		
	end;

	if(@tree_code = 'ObjectUniverses')
	begin
		set @kinds = '''Universe'', ''DSL.MetaDataFile''';
	end;		
	
	if(@tree_code='Reports')
	begin
		-- wybranie folderw raport�w bez raport�w u�ytkownik�w
		set @table_folders = ' INFO_FOLDER where si_name not like ''~Webintelligence%''';
		--set @table_folders = ' INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 is null or SI_PATH__SI_FOLDER_NAME2 != ''User Folders''';
		set @kinds = '''Webi'', ''Flash'', ''CrystalReport'',''Excel'',''FullClient'',''Pdf'',''Hyperlink''
		,''Rtf'',''Txt'',''Powerpoint'',''Word''';
	end;
	
    declare @sql nvarchar(max);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		select @tree_id = id from bik_tree where code = @tree_code;
		set @sql = N'select branch_names as SI_ID, parent_branch_names	as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata';
	end;
	if(@tree_code = 'TeradataDataModel')
	begin
		select @tree_id = id from bik_tree where code = @tree_code;
		set @sql = N'select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, DESCR
					from(
						select 
						branch_names as SI_ID,
						row_number () over ( partition by branch_names order by branch_names  ) as kolejnosc
							, parent_branch_names as SI_PARENTID, type as SI_KIND, name as SI_NAME, extra_info as DESCR
						from bik_teradata_data_model
					) as tab									 
					where 
					 kolejnosc =1';
	end;		
	if(@tree_code = 'Reports')
	begin
		set @tree_id = dbo.fn_get_bo_actual_report_tree_id();
		
		declare @rootFolderName varchar(max) = null, @userFolderName varchar(max) = null;
		select @rootFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$RootFolder' and is_deleted = 0
		select @userFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$UserFolder' and is_deleted = 0
		
		-- sztuczny podzial na root folder i user folders dla raport�w
		set @sql = N'select distinct convert(varchar(30),SI_PARENT_FOLDER) as SI_ID, ''#BO$UserFolder'' as SI_PARENTID, SI_KIND, SI_PATH__SI_FOLDER_NAME1 as SI_NAME, NULL as DESCR
				 from INFO_FOLDER where SI_PATH__SI_FOLDER_ID2 in (select si_id from INFO_FOLDER where SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c'') 
				 and SI_NAME not like ''~Webintelligence%'' 
				
				 union all
				
				 select case when SI_CUID = ''ASHnC0S_Pw5LhKFbZ.iA_j4'' then ''#BO$RootFolder'' when SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c'' then ''#BO$UserFolder'' else convert(varchar(30),si_id) end as si_id, case
									when SI_PARENTID = 0 and SI_CUID <> ''AWigQI18AAZJoXfRHLzWJ2c''
										then ''#BO$RootFolder''
									when si_id in (select si_id from INFO_FOLDER where SI_PATH__SI_FOLDER_ID2 in (select si_id from INFO_FOLDER where SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c''))
										then convert(varchar(30),SI_PARENTID)
									when SI_PARENTID in(select si_id from INFO_FOLDER)
										then convert(varchar(30),SI_PARENTID)
									else ''0'' end as SI_PARENTID, SI_KIND, case
																			when SI_CUID = ''ASHnC0S_Pw5LhKFbZ.iA_j4''
																				then coalesce(' + coalesce(QUOTENAME(substring(@rootFolderName,1,128),''''),'null') + ',SI_NAME)
																			when SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c''
																				then coalesce(' + coalesce(QUOTENAME(substring(@userFolderName,1,128), ''''),'null') + ',SI_NAME)
																			else SI_NAME end, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + '
				
				 union all
				
				 select convert(varchar(30),SI_ID), convert(varchar(30),SI_PARENTID), SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') and SI_INSTANCE = 0
				
				union all
				
				select convert(varchar(30),SI_ID), convert(varchar(30),SI_PARENTID), SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR
				from aaa_global_props where si_parentid in (select si_parentid from INFO_FOLDER where SI_PATH__SI_FOLDER_ID2 in (select si_id from INFO_FOLDER where SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c'') and SI_NAME <> ''~Webintelligence'') and si_kind <> ''Folder''
				
				union all
				
				select convert(varchar(30),id), convert(varchar(30),parent_id), ''ReportSchedule'', name, null
				from aaa_report_schedule sch
				inner join aaa_global_props gp on gp.si_id = sch.parent_id';
	end;
	if(@tree_code = 'Connections')
	begin
		set @tree_id = dbo.fn_get_bo_actual_connection_tree_id();
		
		declare @conenctionFolderName varchar(max) = null;
		select @conenctionFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$ConnectionFolder' and is_deleted = 0
		
		set @sql = N'select ''#BO$ConnectionFolder'' as si_id, ''0'' as si_parentid, si_kind, coalesce(' + coalesce(QUOTENAME(substring(@conenctionFolderName,1,128), ''''),'null') + ',SI_NAME) as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR from app_folder where SI_CUID = ''AZVXOgKIBEdOkiXzUuybja4''
		
			 union all
			 
			 select ''#BO$ConnectionOLAPFolder'', ''#BO$ConnectionFolder'', ''Folder'', ''OLAP'', null
			 
			 union all
			 
			 select convert(varchar(30),SI_ID), case when SI_KIND = ''CommonConnection'' then ''#BO$ConnectionOLAPFolder'' else ''#BO$ConnectionFolder'' end, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
			 from aaa_global_props
			 where SI_KIND in ('+ @kinds +') and SI_OBJECT_IS_CONTAINER = 0';
	end;
	if(@tree_code = 'ObjectUniverses')
	begin
		set @tree_id = dbo.fn_get_bo_actual_universe_tree_id();
		
		declare @universeFolderName varchar(max) = null;
		select @universeFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$UniverseFolder' and is_deleted = 0
		
		set @sql = N'select case when SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'' then ''#BO$UniverseFolder'' else convert(varchar(30),si_id) end as si_id, case 
								when SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk''
									then ''0''
								when SI_PARENTID in (select si_id from app_folder where SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'') 
									then ''#BO$UniverseFolder''
								when SI_PARENTID in(select si_id from APP_FOLDER) 
									then convert(varchar(30),SI_PARENTID)
								else ''0'' end as SI_PARENTID, SI_KIND, case when SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'' then coalesce(' + coalesce(QUOTENAME(substring(@universeFolderName,1,128), ''''),'null') + ',SI_NAME) else SI_NAME end as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
			 from APP_FOLDER
			 
			 union all
			 
			 select convert(varchar(30),SI_ID), case when SI_PARENTID in (select si_id from app_folder where SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'') then ''#BO$UniverseFolder'' else convert(varchar(30),SI_PARENTID) end, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
			 from aaa_global_props
			 where SI_PARENTID in (select si_id from APP_FOLDER) 
					and SI_KIND in ('+ @kinds +')';
	end;					

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed zapytaniem'
	print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(900) collate DATABASE_DEFAULT primary key, si_parentid varchar(900) collate DATABASE_DEFAULT, si_kind varchar(max) collate DATABASE_DEFAULT, si_name varchar(max) collate DATABASE_DEFAULT, descr varchar(max) collate DATABASE_DEFAULT);
	if(@tree_code = 'Reports')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ReportFolder'' 
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	if(@tree_code = 'Connections')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ConnectionFolder''
										when si_kind=''MetaData.DataConnection'' then ''DataConnection''
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	if(@tree_code = 'ObjectUniverses')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''UniversesFolder'' else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	if(@tree_code = 'Teradata' or @tree_code = 'TeradataDataModel')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, si_kind, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po stworzeniu drugiego @sql'
			   
	EXECUTE(@sql);

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);
	
	-- fix na foldery g��wne
	if(@tree_code = 'Reports' or @tree_code = 'ObjectUniverses' or @tree_code = 'Connections')
	begin
		delete from #tmpNodes
		where si_parentid = '0' and si_id not in ('#BO$RootFolder', '#BO$UserFolder', '#BO$UniverseFolder', '#BO$ConnectionFolder')
		
		declare @rc1 int = 1
		while @rc1 > 0 begin
			delete from #tmpNodes
			where si_parentid <> '0' and not exists(select 1 from #tmpNodes g where g.si_id = #tmpNodes.si_parentid)
			
			set @rc1 = @@ROWCOUNT
		end;
	end;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po fixie root�w dla #tmpNodes'
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/

	declare @rc int = 1

	while @rc > 0 begin
	
	delete from #tmpNodes
	where (si_kind = 'UniversesFolder' or si_kind = 'ConnectionFolder' or si_kind = 'ReportFolder') and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/
	
	declare @report_tree_id int = dbo.fn_get_bo_actual_report_tree_id();

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updatetami'

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.descr, tree_id = @tree_id
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.tree_id = @tree_id and bik_node.obj_id = #tmpNodes.si_id and bik_node.linked_node_id is null
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed update podlinkowanych'
	--update podlinkowanych
	update bik_node 
	set /*parent_node_id = null, */node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		 descr = #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and (tree_id = @report_tree_id or tree_id in (select id from bik_tree where tree_kind in (select code from bik_tree_kind where allow_linking = 1))) and bik_node.linked_node_id is not null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed insertami'
	
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id
	where bik_node.id is null;

	set nocount on;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updateami parentow'
	

	update bik_node 
	set parent_node_id = bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id
	inner join bik_node bk on bk.tree_id = @tree_id and bk.obj_id = pt.si_parentid where bik_node.linked_node_id is null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed usuwaniem'
	
	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in 
	('Measure', 'Dimension', 'Detail', 'UniverseClass', 'ReportQuery', 'Filter', 'UniverseColumn', 'UniverseTable', 'UniverseAliasTable', 'UniverseDerivedTable', 'UniverseTablesFolder', 'ConnectionEngineFolder', 'ConnectionNetworkFolder')
				
	drop table #tmpNodes
	
	-- fix na is_built_in
	update bik_node
	set is_built_in = 1
	where tree_id = @tree_id
	and obj_id in ('#BO$RootFolder', '#BO$UserFolder', '#BO$UniverseFolder', '#BO$ConnectionFolder', '#BO$ConnectionOLAPFolder')
	and is_built_in = 0
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po usuwaniu i po dropie'

	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': koniec'
end;

GO

--koniec procedury wstawiajace dane do bik_node
exec sp_update_version '1.6.z3.2', '1.6.z3.3';
go