/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.erwin;

import java.util.HashMap;

/**
 *
 * @author pgajda
 */
public class ErwinTable extends ErwinXMLElementString {

    public ErwinTable() {
        propertiesMap = new HashMap<String, String>() {
            {
                put("Name", null);
                put("Long_Id", null);
                put("Type", null);
                put("Schema_Name", null);
                put("Physical_Name", null);
                put("Shapes_Ref", null);
                put("Comment", null);
            }
        };
    }

}
