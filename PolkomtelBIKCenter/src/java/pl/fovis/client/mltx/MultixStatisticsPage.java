/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.mltx;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.mltx.history.HistoryAction;
import pl.fovis.common.MultixStatisticsBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.mltx.MultixIcons;
import pl.fovis.common.mltx.MultixStatisticsAuthenticationBean;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.ShowablePasswordTextBox;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author mmastalerz
 */
public class MultixStatisticsPage {

    private static final String NBSP = "&nbsp;";
    protected VerticalPanel mainVp;
    protected HorizontalPanel mainHp;
    protected VerticalPanel dataGridVp, statisticsVp;
    protected FlexTable dataGrid;
    protected FlexTable loginGrid;
    //--
    private boolean isAuthenticated;
    private TextBox tbDbLogin = new TextBox();
    private ShowablePasswordTextBox tbDbPass = new ShowablePasswordTextBox();
    //--

    protected Widget buildWidgets() {
        tbDbPass.asWidget().setWidth("18em");
        mainHp = new HorizontalPanel();
        mainHp.setWidth("100%");
        mainHp.setHeight("100%");
        dataGrid = new FlexTable();
        dataGrid.setWidth("100%");
        dataGrid.setStyleName("gridJoinedObj");
        loginGrid = new FlexTable();
        loginGrid.setWidth("35%");
        loginGrid.setStyleName("gridJoinedObj");
        mainVp = new VerticalPanel();
        mainVp.setWidth("100%");
        dataGridVp = new VerticalPanel();
        dataGridVp.setWidth("100%");
        statisticsVp = new VerticalPanel();
        statisticsVp.setWidth("100%");
        mainHp.add(mainVp);
        mainVp.add(loginGrid);
        mainVp.add(statisticsVp);
        preparePageContent();
        return mainHp;
    }

    public static HistoryAction getHistoryAction() {
        return new HistoryAction() {
            @Override
            public Widget performAction(String id) {
                return new MultixStatisticsPage().buildWidgets();
            }
        };
    }

    public void createGridColumnNames() {
        dataGrid.setWidget(0, 0, getHtmlAndStyle("Statistics" /* i18n: no */ + "-tableTitle", I18n.nazwaBazyDanychMultix.get() /* I18N:  */));
        dataGrid.setWidget(0, 1, getHtmlAndStyle("Statistics" /* i18n: no */ + "-tableTitle", I18n.ostatniaAktywnosc.get() /* I18N:  */));
        dataGrid.setWidget(0, 2, getHtmlAndStyle("Statistics" /* i18n: no */ + "-tableTitle", I18n.logowaloSie.get() /* I18N:  */));
        dataGrid.setWidget(0, 3, getHtmlAndStyle("Statistics" /* i18n: no */ + "-tableTitle", I18n.zaproszonych.get() /* I18N:  */));
    }

    public void show(final VerticalPanel vp) {
        vp.clear();
        vp.add(dataGrid);
        dataGrid.clear();
        dataGrid.setWidget(0, 0, new Image(MultixIcons.SPINNER));

        BIKClientSingletons.getMultiXService().getMultiXStatistics(getAuthenticationData(), new StandardAsyncCallback<List<MultixStatisticsBean>>() {
            @Override
            public void onSuccess(List<MultixStatisticsBean> result) {

                if (result == null) {
                    statisticsVp.clear();
                    FoxyClientSingletons.showWarning(I18n.bladLogowaniaSprobujPonownie.get());
                } else {
                    dataGrid.clear();
                    int row = 1;
                    createGridColumnNames();
                    for (MultixStatisticsBean msb : result) {
                        int col = 0;
                        dataGrid.setWidget(row, col++, getHtmlAndStyle("Statistics-title", msb.dbName));
                        dataGrid.setWidget(row, col++, getHtmlAndStyle(BaseUtils.safeToStringDef(msb.lastActivityDate, "-")));
                        dataGrid.setWidget(row, col++, getHtmlAndStyle(BaseUtils.safeToStringDef(msb.totalLogged, "")));
                        dataGrid.setWidget(row, col++, getHtmlAndStyle(msb.totalInvited + ""));
                        row++;
                    }
                }
            }
        });
    }

    protected HTML getHtmlAndStyle(String text) {
        HTML ht = new HTML(text);
        ht.setStyleName("Statistics-cnt");
        return ht;
    }

    protected HTML getHtmlAndStyle(String style, String text) {
        HTML ht = new HTML(text);
        ht.setStyleName(style);
        return ht;
    }

    private void createAndShowLoginForm() {
        loginGrid.clear();
        PushButton loginBtn = new PushButton(I18n.zaloguj.get());
        loginBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                performLogin();
            }
        });
        loginBtn.setStyleName("gwt-PushButton centerAll");
        loginBtn.setWidth("5em");
        Label titleLbl = new HTML(NBSP + I18n.loginToMultixStatisticsPage.get());
        titleLbl.setStyleName("userDialogLbl");

        loginGrid.clear();
        loginGrid.getFlexCellFormatter().setColSpan(0, 0, 5);
        loginGrid.setWidget(0, 0, titleLbl);
        loginGrid.setWidget(1, 0, new HTML(NBSP + I18n.login.get()));
        loginGrid.setWidget(1, 1, tbDbLogin);
        loginGrid.setWidget(1, 2, new HTML(NBSP + I18n.haslo.get()));
        loginGrid.setWidget(1, 3, tbDbPass);
        loginGrid.setWidget(1, 5, loginBtn);
    }

    private void performLogin() {
        BIKClientSingletons.getMultiXService().authenticateUserForMultixStatistics(getAuthenticationData(), new StandardAsyncCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                isAuthenticated = result;
                if (isAuthenticated) {
                    createAndShowStatistics();
                } else {
                    statisticsVp.clear();
                    FoxyClientSingletons.showWarning(I18n.bladLogowaniaSprobujPonownie.get());
                }
            }
        });
    }

    private MultixStatisticsAuthenticationBean getAuthenticationData() {
        return new MultixStatisticsAuthenticationBean(tbDbLogin.getText(), tbDbPass.getText());
    }

    private void preparePageContent() {
        if (!isAuthenticated) {
            createAndShowLoginForm();
            return;
        }
        createAndShowStatistics();
    }

    private void createAndShowStatistics() {
        statisticsVp.clear();
        PushButton refreshStatsBtn = new PushButton(I18n.refreshMultixStatistics.get());
        refreshStatsBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                show(dataGridVp);
            }
        });
        refreshStatsBtn.setStyleName("gwt-PushButton centerAll");
        show(dataGridVp);
        ScrollPanel scPanelFvsm = new ScrollPanel(dataGridVp);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(scPanelFvsm, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 18);
        scPanelFvsm.setStyleName("EntityDetailsPane");
        statisticsVp.add(refreshStatsBtn);
        statisticsVp.add(scPanelFvsm);
    }
}
