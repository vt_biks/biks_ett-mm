package pl.trzy0.foxy.commons;

public class PostgresConnectionConfig extends StandardDBConnectionConfig {

    public int poolSize = 30;
    public int timeout = 10;
    public Integer port;

    public PostgresConnectionConfig() {
    }

    public PostgresConnectionConfig(String server, String database, String user, String password) {
        this.server = server;
        this.database = database;
        this.user = user;
        this.password = password;
    }

    public PostgresConnectionConfig(String server, String database, String user, String password, Integer port) {
        this(server, database, user, password);
        this.port = port;
    }
}
