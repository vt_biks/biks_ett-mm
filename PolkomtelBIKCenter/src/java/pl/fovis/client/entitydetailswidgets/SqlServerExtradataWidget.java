/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.DBColumnBean;
import pl.fovis.common.DBDependencyBean;
import pl.fovis.common.DBDependentObjectBean;
import pl.fovis.common.DBForeignKeyBean;
import pl.fovis.common.DBIndexBean;
import pl.fovis.common.DBPartitionBean;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.MssqlExPropBean;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class SqlServerExtradataWidget extends DatabaseExtradataWidget {

    public SqlServerExtradataWidget(IMainHeaderSharedTable imhst) {
        this.imhst = imhst;
    }

    protected String getTranslateNameForProp(String prop) {
        return BIKClientSingletons.getTranslatedMssqlExProp(getData().node.nodeKindCode, prop);
    }

    @Override
    protected void addAdditionaryRow() {
        List<MssqlExPropBean> exProps = getData().mssqlExProps;
        if (!BaseUtils.isCollectionEmpty(exProps)) {
//            mainVp.add(createHeader(I18n.atrybutyDodatkowe.get())); // zle jest dla pokaz SQL
            for (MssqlExPropBean mssqlExPropBean : exProps) {
                imhst.addRow(getTranslateNameForProp(mssqlExPropBean.name),true, mssqlExPropBean.value);
            }
        }
    }

    @Override
    public boolean hasContent() {
        EntityDetailsDataBean data = getData();
        return data.mssqlColumnExtradata != null || data.mssqlForeignKeyExtradata != null || data.mssqlPartitionExtradata != null || data.mssqlDependencyExtradata != null || data.mssqlIndices != null ||/* data.mssqlChildrenExtradata != null ||*/ data.mssqlExProps != null;
    }

    @Override
    protected List<DBColumnBean> getColumnExtradata() {
        return getData().mssqlColumnExtradata;
    }

    @Override
    protected List<DBForeignKeyBean> getForeignKeyExtradata() {
        return getData().mssqlForeignKeyExtradata;
    }

    @Override
    protected List<DBPartitionBean> getPartitionExtradata() {
        return getData().mssqlPartitionExtradata;
    }

    @Override
    protected List<DBDependencyBean> getDependencyExtradata() {
        return getData().mssqlDependencyExtradata;
    }

    @Override
    protected List<DBDependentObjectBean> getDependentObjectExtradata() {
        return getData().mssqlDependentObjectsExtradata;
    }

    @Override
    protected List<DBIndexBean> getIndexExtradata() {
        return getData().mssqlIndices;
    }

    @Override
    protected boolean isTableOrView() {
        return getData().node.nodeKindCode.equals(BIKConstants.NODE_KIND_MSSQL_TABLE) || getData().node.nodeKindCode.equals(BIKConstants.NODE_KIND_MSSQL_VIEW);
    }

    @Override
    protected boolean treeIdFilter() {
        return getData().node.treeCode.equals(BIKConstants.TREE_CODE_MSSQL);
    }
}
