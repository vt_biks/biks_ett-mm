IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_update_version]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_update_version]
GO
create procedure sp_update_version(@ver_current varchar(255), @ver_upper varchar(255))
as
	declare @version varchar(255)
begin
	
	select @version = val from bik_app_prop where name='bik_ver';
	if(@version != @ver_current)
	begin
		raiserror(N'Wrong number of version.Current version is %s',--Message text
				20,
				-1,
				@version) with log;
				print N'Wrong number of version.Current version is %s';
	end;
	
	update bik_app_prop set val=@ver_upper where name='bik_ver';
	
end

