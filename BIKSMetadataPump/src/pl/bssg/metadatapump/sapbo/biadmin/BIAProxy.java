/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.biadmin;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import simplelib.LameRuntimeException;

/**
 * Proxy class for IBIAConnector. It trying login again to SAPBO, when the exception occures (session timeout).
 * @author tflorczak
 */
public class BIAProxy implements InvocationHandler {

    protected final static int NUMBER_OF_FOR_ITERATION = 2;
    protected IBIAConnector target;

    public static IBIAConnector makeProxy(IBIAConnector target) {
        BIAProxy myProxy = new BIAProxy();
        myProxy.target = target;
        return (IBIAConnector) Proxy.newProxyInstance(target.getClass().getClassLoader(), new Class[]{IBIAConnector.class}, myProxy);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (!target.isServiceInit()) {
            target.initAndLogin();
        }
        Object result = null;
        Exception caught = null;

        for (int i = 0; i < NUMBER_OF_FOR_ITERATION; i++) {
            try {
                result = method.invoke(target, args);
                caught = null;
                break;
            } catch (Exception e) {
                caught = e;
                target.initAndLogin();
            }
        }
        if (caught != null) {
            throw new LameRuntimeException(caught.getMessage(), caught);
        }
        return result;
    }
}
