exec sp_check_version '1.5.x.2';
go

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------
--    nowa tabelka do rejestracji MULTIXa
----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

create table mltx_registration (
id int identity(1,1) not null,
token varchar(128) not null,
email varchar(128) not null,
password varchar(128) not null,
db_option varchar(64) not null,
is_valid int not null default(1),
created_at datetime not null default(CURRENT_TIMESTAMP),
expires_at datetime not null, 
status int not null,
constraint pk_mltx_token_id primary key clustered (	id asc ) ,
constraint uq_mltx_email_token unique nonclustered ( email,token ) 
)
go

exec sp_update_version '1.5.x.2', '1.5.x.3';
go
