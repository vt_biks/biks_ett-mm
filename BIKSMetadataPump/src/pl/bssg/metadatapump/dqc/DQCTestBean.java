/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.dqc;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class DQCTestBean implements Serializable {

    public int idTest;
    public Integer idTemplateTest;
    public int idTestStage;
    public int idImportanceLevel;
    public int idPerson;
    public String name;
    public String longName;
    public String description;
    public String samplingMethod;
    public String verifiedAttributes;
    public String expected_result;
    public String loggingDetails;
    public String benchmarkDefinition;
    public Double errorThreshold; // DECIMAL(18,8) NULL,
    public String resultsObject;
    public String additionalInformation;
    public Date createDate;
    public Date modifyDate;
    public Date suspendDate;
    public boolean deleted;
    public String objId;
    public Integer typeTest;
    
}
