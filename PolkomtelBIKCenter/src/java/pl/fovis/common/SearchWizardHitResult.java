/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 *
 * @author pmielanczuk
 */
public class SearchWizardHitResult implements Serializable {

    public int nodeId;
    public float score;
    public Set<String> occurenceFields;
    public List<String> occurenceCaptions;
    public int bestAttrWeight;

    public SearchWizardHitResult() {
    }

    public SearchWizardHitResult(int nodeId, float score, Set<String> occurenceFields) {
        this.nodeId = nodeId;
        this.score = score;
        this.occurenceFields = occurenceFields;
    }
}
