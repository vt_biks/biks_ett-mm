/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.wezyrtreegrid;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.IsWidget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.RepeatingCommandFix;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.StackTraceUtil;

/**
 *
 * @author wezyr
 */
public class WezyrTreeGridFasterAndClean<ROW> extends WezyrTreeGridProperBase<ROW> implements IWezyrTreeGrid<ROW> {

    // ADD_ROWS_IN_PACKS_SIZE < 0 ---> all at once
    // ADD_ROWS_IN_PACKS_SIZE == 0 ---> do not add data (just add empty <TR>s)
    //ww: nie jest final aby można było nadpisać w projekcie i np. ustawić na -1
    public static int ADD_ROWS_IN_PACKS_SIZE = 40;
//    public static final String MODULE_BASE_FOR_STATIC_FILES = GWT.getModuleBaseForStaticFiles();
    public static final String MODULE_BASE_FOR_STATIC_FILES = GWT.getModuleBaseURL();
    protected String firstColStyleName = "wwNewTree";
//    protected static Map<Integer, WezyrTreeGridFasterAndClean> treeByIdMap = new HashMap<Integer, WezyrTreeGridFasterAndClean>();
    //protected static Queue<Integer> treeIdsForReuse = new LinkedList<Integer>();
    protected int treeIdForOnClick = globalTreeIdForOnClick++;
    protected static int globalTreeIdForOnClick = 0;

//    public WezyrTreeGridFasterAndClean(/*
    //             * ICellBuilder<T> cellBuilder
    //             */IRowCellValBroker<ROW> rowCellValBroker,
    //            /*
    //             * IRowCellValBroker<ROW> rowCellValAdditional,
    //             */
    //            String... colNames) {
    //        super(rowCellValBroker, colNames);
    //    }
    public WezyrTreeGridFasterAndClean(/*
             * ICellBuilder<T> cellBuilder
             */IRowCellValBroker<ROW> rowCellValBroker,
            Collection<Pair<String, Integer>> colNames/*
     * , IRowCellValBroker<ROW> rowCellValAdditional
     */) {
        super(rowCellValBroker, colNames);
    }

    public void setFirstColStyleName(String styleName) {
        this.firstColStyleName = styleName;
    }

//    protected void testDebugMsg(String msg) {
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "testDebugMsg: " + msg);
//        }
//    }
    // 'L' - last in parent, != 'L' - middle
    protected char[] calcConnectors2(VisualTreeRowProps<ROW> rowProps) {
        int lvl = rowProps.level;
        char[] res = new char[lvl + 1];

        while (lvl >= 0) {
            res[lvl] = isLastChildRow(rowProps) ? 'L' : 'M';
            rowProps = rowProps.parentRow;
            lvl--;
        }

        return res;
    }

//    protected void insertNewRow(int insertAtIdx, VisualTreeRowProps<ROW> newRow) {
//        table.insertRow(insertAtIdx);
//        Element rowEle = table.getRowFormatter().getElement(insertAtIdx);
//        rowEle.setId(TABLE_ROW_ID_PREFIX + newRow.globalRowId);
//    }
    protected String createRow(final VisualTreeRowProps<ROW> rowProps) {
        ROW row = rowProps.row;
        char[] connectors = calcConnectors2(rowProps);

        StringBuilder sb = new StringBuilder();

        //sb.append("<div class=\"wwNewTree\">");
        //sb.append("<span>");
        //testDebugMsg("#2 before loop");
        for (int i = 0; i <= rowProps.level; i++) {

            boolean attachExpandCollapse = false;
            boolean isMiddle = connectors[i] != 'L';
            String imgName;

            if (i < rowProps.level) {
                if (!isMiddle) {
                    imgName = "blank" /* I18N: no */;
                } else {
                    imgName = "connector_ancestor";
                }
            } else {
                boolean hasNowNoChildren = rowProps.childRows == null || rowProps.childRows.isEmpty();
                boolean isLeafForSure;

                if (rowProps.childRows != null && rowProps.childRows.isEmpty()) {
                    isLeafForSure = true; //hasNowNoChildren; //true
                } else if (lazyRowLoader != null && !rowProps.isLeafForSure && hasNowNoChildren) {
                    isLeafForSure = lazyRowLoader.isLeafForSure(row);
                    rowProps.isLeafForSure = isLeafForSure;
                } else {
                    isLeafForSure = hasNowNoChildren;
                }

                if (isLeafForSure) {
                    imgName = isMiddle ? "connector_middle" : "connector_end";
                } else {
                    attachExpandCollapse = true;
                    if (rowProps.isExpanded) {
                        imgName = isMiddle ? "connector_opened_middle" : "connector_opened_end";
                    } else {
                        imgName = isMiddle ? "connector_closed_middle" : "connector_closed_end";
                    }
                }
            }

            sb.append("<" + "img src='").append(MODULE_BASE_FOR_STATIC_FILES).append("/images/wezyrtreegrid/").append(imgName).append("." + "gif' style=\"width: 22px; height: 22px;\" border=\"0\" class=\"gwt-Image" /* I18N: no */ + "\" ");

            if (attachExpandCollapse) {
                sb.append("id='plusMinusBtn" /* I18N: no */ + "_").append(treeIdForOnClick).append("__").append(rowProps.globalRowId).append("' ");
//                sb.append("onClick='wezyrTreeGridExpandCollapse(").append(treeIdForOnClick).append(",").
//                        append(rowProps.globalRowId).append(")' ");
            }
            sb.append("/>");
        }

        //imgsPanel.setSize((22 * connectors.length) + "px", "22px");
        //testDebugMsg("#3 after loop");
        String rowIconUrl = rowCellValBroker.getRowIconUrl(row);
        String rowIconTitle = rowCellValBroker.getRowIconTitle(row);
        String rowIconAdditionalUrl = //rowCellValAdditional.getRowIconUrl(row);
                rowCellValBroker.getAdditionalRowIconUrl(row);

        if (rowIconUrl != null) {
            sb.append("<" + "img src" /* I18N: no */ + "='").append(rowIconUrl).append("' " + "style=\"width: 16px; height: 16px" /* i18n: no */ + ";\"");
            if (rowIconTitle != null) {
                sb.append(" " + "title" /* I18N: no */ + "=\"").append(rowIconTitle).append("\"");
            }
            sb.append(" " + "border=\"0\" class=\"treeIcon" /* I18N: no */ + "\"/>");

            if (rowIconAdditionalUrl != null) {
                sb.append("<" + "img src" /* I18N: no */ + "='").append(rowIconAdditionalUrl).append("' " + "style=\"width: 9px; height: 9px;\" border=\"0\" class=\"treeIconP" /* I18N: no */ + "\"/>");
            }
        }
        return sb.toString();
    }

    protected void updateRowCellsInTable(final VisualTreeRowProps<ROW> rowProps, int rowNum) {
        if (rowProps == ultimateRootRow) {
            return;
        }

        //testDebugMsg("#1");
        ROW row = rowProps.row;
        char[] connectors = calcConnectors2(rowProps);

        FlowPanel fp = new FlowPanel();
        fp.setStyleName(firstColStyleName);

        String rowHtml = createRow(rowProps);

        fp.add(new InlineHTML(rowHtml));

        //sb.append("</span>");
        //testDebugMsg("#4 before get ele");
        //testDebugMsg("#5 after get ele");
        int cellNum = 0;

        for (Pair<String, Integer> colNameAndWidth : colNamesAndWidth) {
            Object cellVal = //row.getCellVal(colName);
                    getCellVal(row, colNameAndWidth.v1);

//            String properHtml = "<span class='cellInner'>" + BaseUtils.encodeForHTMLTag(cellVal.toString()) + "</span>";
//
//            if (cellNum == 0) {
//                sb.append(properHtml).append("</div>");
//                properHtml = sb.toString();
//            }
//
//            table.setHTML(rowNum, cellNum, properHtml);
            IsWidget w = getCellValAsWidget(cellVal, cellNum == 0);

            if (cellNum == 0) {
                fp.add(w);
                w = fp;
            } else {
                w.asWidget().addStyleName("wwNewTreeExtraCol");
            }

            int realRowNum = globalIdxToRealRowIdx(rowNum);

            table.setWidget(realRowNum, cellNum, w);
            if (colNameAndWidth.v2 != null) {
                table.getCellFormatter().setWidth(realRowNum, cellNum, colNameAndWidth.v2 + "px");
            }

            cellNum++;
        }

        //testDebugMsg("#7 inner html is set, done");
    }

    protected void maybeExpandCollapseClicked(ClickEvent event) {
        com.google.gwt.dom.client.Element ele = DOM.eventGetTarget(Event.as(event.getNativeEvent()));

        if (ele == null) {
            return;
        }

        String eleTagName = GWTUtils.getElementProperty(ele, "tagName");

        if (eleTagName == null || !eleTagName.equalsIgnoreCase("IMG" /* I18N: no */)) {
            return;
        }

        String eleId = ele.getId();

        if (eleId == null) {
            return;
        }

        int pos = eleId.indexOf("__");

        if (pos < 0) {
            return;
        }

        Integer globalRowId = BaseUtils.tryParseInteger(eleId.substring(pos + 2));
        if (globalRowId == null) {
            return;
        }

        expandOrCollapse(globalRowId);
    }

    @Override
    protected void tableCellClicked(ClickEvent event) {
        super.tableCellClicked(event);

        maybeExpandCollapseClicked(event);
    }

//    @SuppressWarnings("unchanged")
//    public static void staticExpandOrCollapse2(int treeIdForOnClick, int globalRowId) {
//        //Window.alert("#0: aaaaa!");
//
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "staticExpandOrCollapse2: start, treeIdForOnClick=" + treeIdForOnClick + ", globalRowId=" + globalRowId);
//        }
//
//        WezyrTreeGridFasterAndClean treeGrid = treeByIdMap.get(treeIdForOnClick);
//
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "staticExpandOrCollapse2: before treeGrid.expandOrCollapse");
//        }
//
//        treeGrid.expandOrCollapse(globalRowId);
//
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "staticExpandOrCollapse2: end");
//        }
//    }
//
//    public static <ROW> void staticExpandOrCollapse(WezyrTreeGridFasterAndClean<ROW> treeGrid, int globalRowId) {
//        //Window.alert("#0: aaaaa!");
//        treeGrid.expandOrCollapse(globalRowId);
//    }
//
//    @Override
//    protected void onAttach() {
//        super.onAttach();
//        setupWezyrTreeGridOnClickFunction(/*this*/);
//    }
//
//    @Override
//    protected void onDetach() {
//        super.onDetach();
//        //clearWezyrTreeGridOnClickFunction();
//        unsetupWezyrTreeGridOnClickFunction();
//    }
//
//    private void setupWezyrTreeGridOnClickFunction() {
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "setupWezyrTreeGridOnClickFunction: start, treeIdForOnClick=" + treeIdForOnClick
//                    + ", treeByIdMap.size()=" + treeByIdMap.size());
//        }
//
//        if (treeByIdMap.isEmpty()) {
//            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "setupWezyrTreeGridOnClickFunction: do setup");
//            }
//            setupWezyrTreeGridOnClickFunctionInner();
//        }
//
//        treeByIdMap.put(treeIdForOnClick, this);
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "setupWezyrTreeGridOnClickFunction: end");
//        }
//    }
//
//    protected void unsetupWezyrTreeGridOnClickFunction() {
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "unsetupWezyrTreeGridOnClickFunction: start, treeIdForOnClick=" + treeIdForOnClick
//                    + ", treeByIdMap.size()=" + treeByIdMap.size());
//        }
//        treeByIdMap.remove(treeIdForOnClick);
//        if (treeByIdMap.isEmpty()) {
//            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "unsetupWezyrTreeGridOnClickFunction: clear");
//            }
//            clearWezyrTreeGridOnClickFunction();
//        }
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "unsetupWezyrTreeGridOnClickFunction: end");
//        }
//    }
//
//    private native void setupWezyrTreeGridOnClickFunctionInner() /*-{
//    $wnd.wezyrTreeGridExpandCollapse = function (treeIdForOnClick, globalRowIdx) {
//    //$wnd.alert('globalRowIdx=' + globalRowIdx);
//    @pl.fovis.foxygwtcommons.wezyrtreegrid.WezyrTreeGridFasterAndClean::staticExpandOrCollapse2(II)(treeIdForOnClick, globalRowIdx);
//    //$wnd.alert('#2:globalRowIdx=' + globalRowIdx);
//    };
//    }-*/;
//
////    private native void setupWezyrTreeGridOnClickFunctionInner(/*WezyrTreeGridFasterAndClean treeGrid*/) /*-{
////    $wnd.wezyrTreeGridExpandCollapse = function (globalRowIdx) {
////    //$wnd.alert('globalRowIdx=' + globalRowIdx);
////    @pl.fovis.foxygwtcommons.wezyrtreegrid.WezyrTreeGridFasterAndClean::staticExpandOrCollapse(Lpl/fovis/foxygwtcommons/wezyrtreegrid/WezyrTreeGridFasterAndClean;I)(treeGrid, globalRowIdx);
////    //$wnd.alert('#2:globalRowIdx=' + globalRowIdx);
////    };
////    }-*/;
//    private native void clearWezyrTreeGridOnClickFunction() /*-{
//    $wnd.wezyrTreeGridExpandCollapse = null;
//    }-*/;
    protected void expandOrCollapse(int globalRowId) {
        //Window.alert("aaaaa!");
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "expandOrCollapse: start, globalRowId=" + globalRowId);
//        }
        VisualTreeRowProps<ROW> rowProps = allRows.get(globalRowId);
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "expandOrCollapse: gotRowProps, rowProps=" + getDiagInfo(rowProps));
//        }
        if (rowProps.isExpanded) {
//            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "expandOrCollapse: before colapseRow");
//            }
            collapseRow(rowProps);
        } else {
//            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "expandOrCollapse: before expandRow");
//            }
            expandRow(rowProps);
        }
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "expandOrCollapse: end");
//        }
    }

    @Override
    public void addRows(final Iterable<ROW> rows, ROW prevRow, AddRowMode mode, boolean isLeafForSure) {
        VisualTreeRowProps<ROW> parentRowProps;
        List<VisualTreeRowProps<ROW>> siblings;
        int siblingIdx;
        //String nameVal = (String) getCellVal(row, colNames.get(0));

        parentRowProps = getVisualRowFix(prevRow);
        if (parentRowProps == null) {
            return;
        }
//        if (prevRow == null) {
////            if (mode == AddRowMode.AsFirstChild) {
////                insertAtIdx = 0;
////            } else {
////                insertAtIdx = table.getRowCount();
////            }
//
//            parentRowProps = ultimateRootRow;
//        } else {
//            parentRowProps = visualRows.get(prevRow);
//        }

        VisualTreeRowProps<ROW> insertAtRowProps;
        boolean includeThisRowSubTree;
        int moveToNextVal; // 0 or 1

        if (mode == AddRowMode.AsLastChild && parentRowProps.childRows != null
                && parentRowProps.childRows.size() > 0) {
            insertAtRowProps = parentRowProps.childRows.get(parentRowProps.childRows.size() - 1);
            includeThisRowSubTree = true;
            moveToNextVal = 0;
        } else if (mode == AddRowMode.AsSibling && parentRowProps.childRows != null
                && parentRowProps.childRows.size() > 0) {
            insertAtRowProps = parentRowProps;
            includeThisRowSubTree = true;
            moveToNextVal = 0;
        } else {
            insertAtRowProps = parentRowProps;
            includeThisRowSubTree = false;
            moveToNextVal = 1;
        }

        if (mode == AddRowMode.AsFirstChild || mode == AddRowMode.AsLastChild) {
            siblings = parentRowProps.childRows;
            if (siblings == null) {
                siblings = new ArrayList<VisualTreeRowProps<ROW>>();
                parentRowProps.childRows = siblings;
            }

            if (mode == AddRowMode.AsFirstChild) {
                siblingIdx = 0;
            } else {
                siblingIdx = siblings.size();
            }
        } else {
            VisualTreeRowProps<ROW> siblingRowProps = parentRowProps;
            parentRowProps = siblingRowProps.parentRow;
            siblings = parentRowProps.childRows;
            siblingIdx = getIndexInSiblings(siblingRowProps, siblings) + 1;
        }

        boolean hasRows = rows.iterator().hasNext();
        fixIsExpandingIfNeeded(parentRowProps, hasRows);

        boolean isParentVisibleAndExpanded = parentRowProps.isExpanded && parentRowProps.isVisible;

        int insertAtIdx = -1;
        int startInsertAtIdx = -1;

        for (ROW row : rows) {
//            hasRows = true;
            VisualTreeRowProps<ROW> newRow = new VisualTreeRowProps<ROW>();
            newRow.parentRow = parentRowProps;
            newRow.row = row;
            newRow.isLeafForSure = isLeafForSure;
            newRow.level = parentRowProps.level + 1;
            newRow.isVisible = false; //isParentVisibleAndExpanded;
            newRow.globalRowId = allRows.size();
            //newRow.isExpanded = true;

            allRows.add(newRow);

            boolean newLastSibling = siblingIdx == siblings.size() && !siblings.isEmpty();
            boolean isFirstChild = siblings.isEmpty();
            siblings.add(siblingIdx, newRow);
            visualRows.put(row, newRow);

            if (isParentVisibleAndExpanded) {
                if (insertAtIdx == -1) {
                    insertAtIdx = getGlobalIdxOfRow(insertAtRowProps, includeThisRowSubTree) + moveToNextVal;
                    startInsertAtIdx = insertAtIdx;
                }
//                addRowToTreeTable(insertAtIdx, newRow);
                if (newLastSibling) {
//                    if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
//                        ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "why i am here?");
//                    }

                    VisualTreeRowProps<ROW> siblingRow = siblings.get(siblingIdx - 1);
                    int siblingVisualIdx = getGlobalIdxOfRow(siblingRow, false);
                    updateVisibleSubTree(siblingRow, siblingVisualIdx);
                }
            }

            if (isFirstChild) {
                //insertAtIdx = getGlobalIdxOfRow(parentRowProps, false);
                //updateRowCellsInTable(parentRowProps, insertAtIdx);
                updateRowInTableIfVisible(parentRowProps);
            }

            insertAtIdx++;
            siblingIdx++;
        }

//        if (!hasRows) {
//            fixIsExpandingIfNeeded(parentRowProps, hasRows);
//        }
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand,
//                    "after main loop (addRowToTreeTable), ADD_ROWS_IN_PACKS_SIZE=" + ADD_ROWS_IN_PACKS_SIZE
//                    + ", parent=" + getDiagInfo(parentRowProps));
//        }
        if (ADD_ROWS_IN_PACKS_SIZE != 0 && startInsertAtIdx >= 0) {
            final int siai = startInsertAtIdx;

            final VisualTreeRowProps<ROW> parentRowPropsFinal = parentRowProps;

//            if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//                ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                        "before create repeating command");
//            }
            RepeatingCommand rc = new RepeatingCommandFix() {

                Iterator<ROW> iter = rows.iterator();
                int iai = siai;
                int packIdx = 0;
                VisualTreeRowProps<ROW> lastAdded = null;

                @Override
                public boolean doExecute() {
//                    if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//                        ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                                "starting doExecute for packIdx: " + packIdx);
//                    }

                    String loopPhase = "init" /* I18N: no */;
                    int cnt = 0;
                    boolean willContinue = true;

                    Exception exx = null;

                    try {
//                        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
//                            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "RepeatingCommand.execute() starts, packIdx=" + packIdx
//                                    + ", parent=" + getDiagInfo(parentRowPropsFinal));
//                        }

                        if (!parentRowPropsFinal.isVisible || !parentRowPropsFinal.isExpanded) {
//                            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
//                                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "RepeatingCommand.execute(), packIdx=" + packIdx
//                                        + ", break, return");
//                            }
                            loopPhase = "after loop" /* I18N: no */;
                            return false;
                        }

//                        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
//                            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "RepeatingCommand.execute() after check breaking, packIdx=" + packIdx
//                                    + ", parent=" + getDiagInfo(parentRowPropsFinal));
//                        }
                        if (lastAdded != null) {
//                            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
//                                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "RepeatingCommand.execute() in if  before getGlobalIdxOfRow, packIdx=" + packIdx
//                                        + ", parent=" + getDiagInfo(parentRowPropsFinal));
//                            }
                            iai = getGlobalIdxOfRow(lastAdded, true);
                        }

//                        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
//                            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "RepeatingCommand.execute() after if-getGlobalIdxOfRow, packIdx=" + packIdx
//                                    + ", parent=" + getDiagInfo(parentRowPropsFinal));
//                        }
                        for (; (ADD_ROWS_IN_PACKS_SIZE < 0 || cnt < ADD_ROWS_IN_PACKS_SIZE) && iter.hasNext(); cnt++) {
//                        if (packIdx == 0) {
//                            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
//                                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "RepeatingCommand.execute(), packIdx=" + packIdx + ", row#=" + cnt);
//                            }
//                        }
                            loopPhase = "loop starts" /* I18N: no */;
                            ROW r = iter.next();
                            loopPhase = "after iter.next" /* I18N: no */ + "()";
                            lastAdded = visualRows.get(r);
                            loopPhase = "after lastAdded assigned" /* I18N: no */;

                            //insertNewRow(iai, vtrp);
                            //updateRowCellsInTable(vtrp, iai++);
                            lastAdded.isVisible = true;
                            loopPhase = "after lastAdded.isVisible = true" /* I18N: no */;
                            addRowToTreeTable(iai++, lastAdded);
                            loopPhase = "after addRowToTreeTable, loop iteration ends" /* I18N: no */;
                        }
                        loopPhase = "after loop" /* I18N: no */;
                        if (true || packIdx == 0) {
//                            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
//                                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "RepeatingCommand.execute(), packIdx=" + packIdx + ", after loop"
//                                        + ", parent=" + getDiagInfo(parentRowPropsFinal));
//                            }
                        }
                        packIdx++;

                        willContinue = iter.hasNext();
                        pendingAddRows = willContinue;

//                        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//                            ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                                    "finishing doExecute (before callECH) for packIdx: " + packIdx + ", will continue:" + willContinue);
//                        }
                        if (!willContinue) {
                            callExpandCollapseHandlers(parentRowPropsFinal.row);

//                            if (optRowIdxToSelectAfterAddRowsDone != null) {
//                                selectRowByRealRowVisualIdx(optRowIdxToSelectAfterAddRowsDone, mustFireEventAfterAddRowsDone);
//                                optRowIdxToSelectAfterAddRowsDone = null;
//                            }
                            if (!BaseUtils.safeEquals(mustFireEventAfterAddRowsDone, false)) {
                                selectRowByRealRowVisualIdx(optRowIdxToSelectAfterAddRowsDone, mustFireEventAfterAddRowsDone);
                                optRowIdxToSelectAfterAddRowsDone = null;
                                mustFireEventAfterAddRowsDone = false;
                            }
                        }

                        return willContinue;
                    } catch (Exception ex) {
                        exx = ex;
                        return false;
                    } finally {
//                        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
//                            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "RepeatingCommand.execute() in last finally, after return: end, "
//                                    + "loopPhase=" + loopPhase + ", cnt=" + cnt + ", iai=" + iai + ", packIdx=" + packIdx
//                                    + ", parent=" + getDiagInfo(parentRowPropsFinal));
//                        }
//                        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//                            ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                                    "finished doExecute for packIdx: " + packIdx);
//                        }

                        if (!"after loop" /* I18N: no */.equals(loopPhase)) {
                            throw new LameRuntimeException("something went wrong, exec not finished properly (loopPhase=" /* I18N: no */
                                    + loopPhase + ")!"
                                    + (exx == null ? "" : "\nException in exec:\n"
                                            + StackTraceUtil.getCustomStackTrace(exx)));
                        }
                    }
                }
            };

//            if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//                ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                        "before rc.execute()");
//            }
            if (rc.execute()) {
//                if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//                    ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                            "before scheduleIncremental()");
//                }

                Scheduler.get().scheduleIncremental(rc);
//                if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//                    ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                            "after scheduleIncremental()");
//                }
            }
//            if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//                ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                        "after rc.execute()");
//            }
        }
    }

    protected int addExpandedChildrenSubTreesOfRow(VisualTreeRowProps<ROW> rowProps, int atGlobalIdx) {
        int res = 0;

        if (rowProps.isExpanded && rowProps.childRows != null) {
            for (int i = 0; i < rowProps.childRows.size(); i++) {
                int subRowCnt = addExpandedSubTreeOfRow(rowProps.childRows.get(i), atGlobalIdx);
                atGlobalIdx += subRowCnt;
                res += subRowCnt;
            }
        }

        return res;
    }

    protected int addExpandedSubTreeOfRow(VisualTreeRowProps<ROW> rowProps, int atGlobalIdx) {
        rowProps.isVisible = true;
        addRowToTreeTable(atGlobalIdx++, rowProps);

        return addExpandedChildrenSubTreesOfRow(rowProps, atGlobalIdx) + 1;
    }

    @Override
    public boolean hasUnloadedChildren(ROW row) {
        if (lazyRowLoader == null || row == null) {
            return false;
        }

        VisualTreeRowProps<ROW> rowProps = getVisualRowFix(row); // visualRows.get(row);

        if (rowProps.childRows != null) {
            return false;
        }

        if (rowProps.isExpanding) {
            return true;
        }

        if (rowProps.isLeafForSure || lazyRowLoader.isLeafForSure(row)) {
            return false;
        }

        return true;
    }

    @Override
    protected void expandRow(VisualTreeRowProps<ROW> rowProps) {

//        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_KIND_WW_REPEATING_COMMAND)) {
//            ClientDiagMsgs.showDiagMsg(DIAG_KIND_WW_REPEATING_COMMAND,
//                    "expandRow: rowProps=" + rowProps);
//        }
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "expandRow: start, rowProps=" + rowProps);
//        }
        if (rowProps == null || rowProps.isExpanding) {
            return;
        }

        expandAllAncestors(rowProps);

        //ww: jak na 100% jest liściem, to finito - nie ma sensu wołać wczytywania dzieci!
        if (rowProps.isExpanded || rowProps.isLeafForSure) {
//            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "expandRow: rowProps.isExpanded, return");
//            }
            return;
        }

        if (rowProps.childRows == null && lazyRowLoader != null) {
            //Window.alert("aaaaaaaa!");
            rowProps.isExpanding = true;
//            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "expandRow: before lazyRowLoader.loadSubRowsOnExpand");
//            }
            lazyRowLoader.loadSubRowsOnExpand(rowProps.row);
//            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "expandRow: after lazyRowLoader.loadSubRowsOnExpand, done");
//            }
            return;
        }

//        rowProps.setIsExpanded(true);
        setRowPropsIsExpanded(rowProps, true);

//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "expandRow");
//        }
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "expandRow: before getGlobalIdxOfRow");
//        }
        int globalIdx = getGlobalIdxOfRow(rowProps, false);

//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "expandRow: before addExpandedChildrenSubTreesOfRow, globalIdx=" + globalIdx);
//        }
        int expandedDescendantCnt = addExpandedChildrenSubTreesOfRow(rowProps, globalIdx + 1);

//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "expandRow: before updateRowCellsInTable, expandedDescendantCnt=" + expandedDescendantCnt);
//        }
        updateRowCellsInTable(rowProps, globalIdx);

        callExpandCollapseHandlers(rowProps.row);

//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_nodeExpanding, "expandRow: end");
//        }
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_loadSubRowsOnExpand, "expandRow - done, expandedDescendantCnt=" + expandedDescendantCnt);
//        }
    }

    protected void updateVisibleSubTreeInTable(VisualTreeRowProps<ROW> rowProps) {
        updateVisibleSubTree(rowProps, getGlobalIdxOfRow(rowProps, false));
    }

    @Override
    protected void removeRow(VisualTreeRowProps<ROW> rowProps) {
        VisualTreeRowProps<ROW> parentRowProps = rowProps.parentRow;
        int rowIdx = getIndexInSiblings(rowProps, parentRowProps.childRows);
        boolean isSelected = getSelectedRow() == rowProps.row;
        int globalIdx = getGlobalIdxOfRow(rowProps, false);
        int childrenCount = parentRowProps.childRows.size();
        boolean isLast = rowIdx + 1 >= childrenCount;

        removeRowAndSubTrees(rowProps, globalIdx);
        parentRowProps.childRows.remove(rowProps);

        if (isLast && childrenCount >= 2) {
            //updateRowInTableIfVisible(parentRowProps.childRows.get(childrenCount - 2));
            updateVisibleSubTreeInTable(parentRowProps.childRows.get(childrenCount - 2));
        } else if (childrenCount == 1 && parentRowProps != ultimateRootRow) {
            updateRowInTableIfVisible(parentRowProps);
        }
        if (isSelected) {
            if (rowIdx >= childrenCount - 1) {
                rowIdx--;
            }
            if (rowIdx >= 0) {
                selectRow(parentRowProps.childRows.get(rowIdx).row, true);
            } else {
                //pg: poniższy warunek de facto oznacza, że całe drzewo jest puste
                //ww: puste drzewo, to też drzewo ;-) trzeba wybrać nulla
//                if (parentRowProps.row != null) {
                selectRow(parentRowProps.row, true);
//                }
            }
        }
    }

    protected void removeRowAndSubTrees(VisualTreeRowProps<ROW> rowProps, int globalIdx) {
        visualRows.remove(rowProps.row);
        allRows.set(rowProps.globalRowId, null);

        updateExpandedRowsSet(rowProps, false);

        if (rowProps.isVisible) {
            table.removeRow(globalIdxToRealRowIdx(globalIdx));
        }
        if (rowProps.childRows != null) {
            for (VisualTreeRowProps<ROW> childRow : rowProps.childRows) {
                removeRowAndSubTrees(childRow, globalIdx);
                //operacja zbędna - całe rowProps zostaje usunięte z allRows powyżej
                //rowProps.childRows.remove(i);
            }
        }
    }

    protected void pasteRow(VisualTreeRowProps<ROW> rowProps, VisualTreeRowProps<ROW> srcRowProps, boolean isCut) {
        if (isCut) {
            cutRow(srcRowProps);
        } else {
            markAsCutOrUncutRow(srcRowProps, false);
        }
//        pasteCutRow(rowProps, srcRowProps); // źle działa gdy wklejamy do pustego / wcześniej nie otwieranego folderu
        pasteCutRowBySamoth(rowProps, srcRowProps);
    }

    protected void cutRow(VisualTreeRowProps<ROW> rowProps) {
        VisualTreeRowProps<ROW> parentRowProps = rowProps.parentRow;
        int rowIdx = getIndexInSiblings(rowProps, parentRowProps.childRows);
        int globalIdx = getGlobalIdxOfRow(rowProps, false);
        int childrenCount = parentRowProps.childRows.size();
        boolean isLast = rowIdx + 1 >= childrenCount;

        cutRowAndSubTrees(rowProps, globalIdx);
        parentRowProps.childRows.remove(rowProps);

        if (isLast && childrenCount >= 2) {
            updateRowInTableIfVisible(parentRowProps.childRows.get(childrenCount - 2));
        } else if (childrenCount == 1 && parentRowProps != ultimateRootRow) {
            updateRowInTableIfVisible(parentRowProps);
        }
    }

    protected void cutRowAndSubTrees(VisualTreeRowProps<ROW> rowProps, int globalIdx) {
        if (rowProps.isVisible) {
            table.removeRow(globalIdxToRealRowIdx(globalIdx));
        }
        if (rowProps.isExpanded && rowProps.childRows != null) {
            for (VisualTreeRowProps<ROW> childRow : rowProps.childRows) {
                cutRowAndSubTrees(childRow, globalIdx);
            }
        }
    }

//    public void replaceInVisualRows(ROW oldRow, ROW row) {
//        VisualTreeRowProps<ROW> rowProps = visualRows.get(oldRow);
//        rowProps.row = row;
//        visualRows.remove(oldRow);
//        visualRows.put(row, rowProps);
//    }
    // dziala źle gdy chcemy wkleić do folderu bez dzieci, albo folderu wcześniej nie otwieranego: duplikuje węzeł
    protected void pasteCutRow(VisualTreeRowProps<ROW> rowProps, VisualTreeRowProps<ROW> srcRowProps) {
        if (!rowProps.isExpanded) {
            expandRow(rowProps);
        }
        if (rowProps.childRows == null) {
            rowProps.childRows = new ArrayList<VisualTreeRowProps<ROW>>();
        }
        rowProps.childRows.add(0, srcRowProps);
        srcRowProps.parentRow = rowProps;
        pasteCutRowAndSubTrees(srcRowProps, getGlobalIdxOfRow(rowProps, false) + 1);
        if (rowProps.childRows.size() == 1) {
            updateRowInTableIfVisible(rowProps);
        } else {
            updateRowInTableIfVisible(rowProps.childRows.get(0));
        }
        updateRowInTableIfVisible(srcRowProps);
    }

    protected void pasteCutRowBySamoth(VisualTreeRowProps<ROW> rowProps, VisualTreeRowProps<ROW> srcRowProps) {
        if (!rowProps.isExpanded) {
            expandRow(rowProps);
            if (rowProps.childRows != null) {
                visualAddRowWhenPaste(rowProps, srcRowProps);
            }
        } else {
            if (rowProps.childRows == null) {
                rowProps.childRows = new ArrayList<VisualTreeRowProps<ROW>>();
            }
            visualAddRowWhenPaste(rowProps, srcRowProps);
        }
    }

    protected void visualAddRowWhenPaste(VisualTreeRowProps<ROW> rowProps, VisualTreeRowProps<ROW> srcRowProps) {
        rowProps.childRows.add(0, srcRowProps);
        srcRowProps.parentRow = rowProps;
        pasteCutRowAndSubTrees(srcRowProps, getGlobalIdxOfRow(rowProps, false) + 1);
        if (rowProps.childRows.size() == 1) {
            updateRowInTableIfVisible(rowProps);
        } else {
            updateRowInTableIfVisible(rowProps.childRows.get(0));
        }
        updateRowInTableIfVisible(srcRowProps);
    }

    protected int pasteCutRowAndSubTrees(VisualTreeRowProps<ROW> rowProps, int targetIdx) {
        rowProps.level = rowProps.parentRow.level + 1;
        if (rowProps.isVisible) {
            addRowToTreeTable(targetIdx++, rowProps);
        }
        if (rowProps.isExpanded && rowProps.childRows != null) {
            for (int i = 0; i < rowProps.childRows.size(); i++) {
                targetIdx = pasteCutRowAndSubTrees(rowProps.childRows.get(i), targetIdx);
            }
        }
        return targetIdx;
    }

    protected int removeVisibleChildrenSubTreesOfRow(VisualTreeRowProps<ROW> rowProps, int globalIdx) {
        int res = 0;

        if (rowProps.isExpanded && rowProps.childRows != null) {
            for (int i = 0; i < rowProps.childRows.size(); i++) {
                int subRowCnt = removeVisibleSubTreeOfRow(rowProps.childRows.get(i), globalIdx);
                //globalIdx += subRowCnt;
                res += subRowCnt;
            }
        }

        return res;
    }

    protected int removeVisibleSubTreeOfRow(VisualTreeRowProps<ROW> rowProps, int atGlobalIdx) {
        if (!rowProps.isVisible) {
            return 0;
        }

        rowProps.isVisible = false;
        table.removeRow(globalIdxToRealRowIdx(atGlobalIdx));
        if (rowProps.row == selectedRow) {
            selectedRow = null;
            selectedRowElement = null;
        }

        return removeVisibleChildrenSubTreesOfRow(rowProps, atGlobalIdx);
    }

    @Override
    protected void collapseRow(VisualTreeRowProps<ROW> rowProps) {
        if (!rowProps.isExpanded) {
            return;
        }

        int globalIdx = getGlobalIdxOfRow(rowProps, false);

        removeVisibleChildrenSubTreesOfRow(rowProps, globalIdx + 1);

//        rowProps.setIsExpanded(false);
        setRowPropsIsExpanded(rowProps, false);

        updateRowCellsInTable(rowProps, globalIdx);

        callExpandCollapseHandlers(rowProps.row);
    }

    protected int updateVisibleSubTree(VisualTreeRowProps<ROW> row, int visualIdx) {
        if (!row.isVisible) {
            return 0;
        }
        int res = 1;
        updateRowCellsInTable(row, visualIdx++);

        if (row.isExpanded && row.childRows != null) {
            for (VisualTreeRowProps<ROW> childRow : row.childRows) {
                int childUpdCnt = updateVisibleSubTree(childRow, visualIdx);
                visualIdx += childUpdCnt;
                res += childUpdCnt;
            }
        }
        return res;
    }

    @Override
    protected void removeAllRowsFromTable() {
        initTable();

//        if (table.getRowCount() == 0) {
//            return; // nothing to do, table is clear already
//        }
//
//        Element tr = table.getRowFormatter().getElement(0);
//        Element tBody = tr.getParentElement();
//
//        for (int i = tBody.getChildCount() - 1; i >= 0; i--) {
//            tBody.removeChild(tBody.getChild(i));
//        }
//
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_FILTER_TREE)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_FILTER_TREE, "clearAllRows: after removeAllRowsFromTable(): remove child nodes");
//        }
    }

    protected void changeSubRowsAndRedisplay(VisualTreeRowProps<ROW> parent, IContinuation changeCont) {

        ROW oldSelected = selectedRow;

        collapseRow(parent);

        changeCont.doIt();

        expandRow(parent);

        if (selectedRow == null && oldSelected != null) {
            selectRow(oldSelected, false);
        }
    }

    @Override
    public void setRowIndexInSiblings(final ROW row, final int idx) {

        final VisualTreeRowProps<ROW> props = visualRows.get(row);
        final VisualTreeRowProps<ROW> parent = props.parentRow;
        changeSubRowsAndRedisplay(parent, new IContinuation() {

            @Override
            public void doIt() {

                List<VisualTreeRowProps<ROW>> siblings = parent.childRows;

                int currIdx = getIndexInSiblings(props, siblings);

                int newIdx = idx;

                if (currIdx < newIdx) {
                    newIdx--;
                }

                siblings.remove(currIdx);

                if (newIdx > siblings.size()) {
                    newIdx = siblings.size();
                }

                if (newIdx < 0) {
                    newIdx = 0;
                }

                siblings.add(newIdx, props);
            }
        });
    }

    public void sortChildRows(ROW row, final Comparator<ROW> cmp) {
        final VisualTreeRowProps<ROW> props = getVisualRowFix(row);

        changeSubRowsAndRedisplay(props, new IContinuation() {

            public void doIt() {

                List<VisualTreeRowProps<ROW>> children = props.childRows;

                Collections.sort(children, new Comparator<VisualTreeRowProps<ROW>>() {

                    public int compare(VisualTreeRowProps<ROW> o1, VisualTreeRowProps<ROW> o2) {
                        return cmp.compare(o1.row, o2.row);
                    }
                });
            }
        });
    }

    public ROW getRowAtIndexInSiblings(ROW parentRow, int idx) {

        final VisualTreeRowProps<ROW> props = getVisualRowFix(parentRow);

        List<VisualTreeRowProps<ROW>> childRows = props.childRows;

        if (childRows == null) {
            throw new LameRuntimeException("row has no children" /* i18n: no */);
        }

        if (childRows.size() <= idx || idx < 0) {
            throw new LameRuntimeException("child row idx" /* I18N: no */ + " (" + idx + ") " + "out of range" /* I18N: no */ + " [0.." + childRows.size() + ")");
        }

        return childRows.get(idx).row;
    }
}
