/*
 * JarResourceProvider.java
 *
 * Created on 10 październik 2007, 11:45
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;

/**
 *
 * @author wezyr
 */
public class JarResourceProvider extends BaseUrlResourceProvider {
    
    protected ClassLoader classLoader;
    private URL[] urls;
    
    
    public JarResourceProvider(URL[] urls, Class baseClassForResources) {
        this.urls = urls;
        this.classLoader = new URLClassLoader(urls,
                (baseClassForResources == null ? getClass() : baseClassForResources).getClassLoader());
    }
    
    @Override
    protected String noUrlExplanation(String resourceName) {
        return super.noUrlExplanation(resourceName) + ", urls: " + Arrays.toString(urls);
    }
    
    protected URL getUrlForResource(String resourceName) {
        resourceName = LameUtils.cutPrefix(resourceName, "/");
        //InputStream is = classLoader.getResourceAsStream(resourceName);
        URL res = classLoader.getResource(resourceName);
        System.out.println("getUrlForResource: resourceName=" + resourceName + ", url=" + res);
        return res;
    }
    
/*
 protected InputStream getInputStreamForResource(String resourceName) {
        //testGetStringFromResource();
        resourceName = LameUtils.cutPrefix(resourceName, "/");
 
        InputStream is = classLoader.getResourceAsStream(resourceName);
        return is;
    }
 */
}
