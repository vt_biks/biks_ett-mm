/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.client.dialogs.TreeSelectorForUsersToCopyRole;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.TreeNodeBean;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class NodeActionCopyJojnedObj extends NodeActionUpdatingBase {

    @Override
    public boolean isEnabledByDesign() {
        return isNodeNotLinkedAnyhowNorReadOnlyNodeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_USER/*"User"*/); //dla podlinkowanych nie?
    }

    public String getCaption() {
        return I18n.kopiujPowiazania.get() /* I18N:  */;
    }

    public void execute() {
        final TreeSelectorForUsersToCopyRole tsfjod = new TreeSelectorForUsersToCopyRole();

        tsfjod.buildAndShowDialog(0, "Users" /* I18N: no */, new IParametrizedContinuation< Pair<TreeNodeBean, List<Boolean>>>() {

            public void doIt(Pair<TreeNodeBean, List<Boolean>> param) {
                TreeNodeBean tb = param.v1;
                BIKClientSingletons.getService().copyAllRolesUser(BaseUtils.nullToDef(node.linkedNodeId, node.id),
                        BaseUtils.nullToDef(tb.linkedNodeId, tb.id), param.v2.get(0), param.v2.get(1), new StandardAsyncCallback<Void>() {

                    public void onSuccess(Void result) {
                        refreshRedisplayDataAndSelectRecord(node.id);
                    }
                });
            }
        }, null, false, node.id, node.name);
    }
}
