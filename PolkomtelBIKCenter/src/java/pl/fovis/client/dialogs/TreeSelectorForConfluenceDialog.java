/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.bikpages.AbstractBIKTreeWidget;
import pl.fovis.client.bikpages.BikConfluenceTreeWidget;
import pl.fovis.client.treeandlist.ConfluenceBean4TreeExtractor;
import pl.fovis.client.treeandlist.ConfluenceObjectBeanAsMapStorage;
import pl.fovis.client.treeandlist.IBean4TreeExtractor;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class TreeSelectorForConfluenceDialog extends AbstractSelectorDialog<ConfluenceObjectBean, String> {

    protected BikConfluenceTreeWidget confluenceTreeWidget;
    protected IParametrizedContinuation<Map<String, String>> saveContForConfluence;

    @Override
    protected boolean isInheritToDescendantsCheckBoxVisible() {
        return false;
    }

    @Override
    protected boolean isInMultiSelectMode() {
        return true;
    }

    @Override
    protected boolean isNodesIdFromExternalData() {
        return true;
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<ConfluenceObjectBean>> callback) {
        BIKClientSingletons.getService().getConfluenceRootObjects(callback);
    }

    @Override
    protected Collection<Integer> getTreeIdsForFiltering() {
        return BIKClientSingletons.getProperTreeIdsWithTreeKind(BIKGWTConstants.TREE_KIND_CONFLUENCE);
    }

    @Override
    protected Collection<String> splitBranchIdsIntoColl(String branchIds, boolean withOutSelf, Collection<String> coll) {
        return BIKCenterUtils.splitBranchIdsAsStringIntoColl(branchIds, withOutSelf, coll);
    }

    @Override
    public void optGetNodePathAndCenter(final IContinuation con) {
        if (nodeId == null) {
            headLbl.setText(I18n.obiektZostanieDodanyJakoRoot.get());
            con.doIt();
        } else {
            getService().getAncestorNodePathByNodeId(Integer.parseInt(nodeId), new StandardAsyncCallback<List<String>>() {
                public void onSuccess(List<String> result) {
                    headLbl.setText(getHeadLblPrefixMsg() + BaseUtils.mergeWithSepEx(result, BIKConstants.RAQUO_STR_SPACED));
                    con.doIt();
                }
            });
        }
    }

    @Override
    protected AbstractBIKTreeWidget<ConfluenceObjectBean, String> getTreeWidget() {
        confluenceTreeWidget = new BikConfluenceTreeWidget(actionBtn, dataFetchBroker,
                ConfluenceObjectBeanAsMapStorage.NAME_FIELD_NAME + "=300:Nazwa",
                ConfluenceObjectBeanAsMapStorage.PRETTY_KIND_FIELD_NAME + "=100:Typ") {
                    @Override
                    protected void afterInitWithNoData() {
                        super.afterInitWithNoData();
                        treeHasNoData();
                    }

                    @Override
                    public boolean isInMultiSelectMode() {
                        return TreeSelectorForConfluenceDialog.this.isInMultiSelectMode();
                    }
                };
        return confluenceTreeWidget;
    }

    @Override
    protected void doAction() {
//        BIKClientSingletons.showInfo(I18n.zapisywaniePowiazanProszeCzekac.get() /* I18N:  */ + "...");
        saveContForConfluence.doIt(confluenceTreeWidget.getSelectedRows());
    }

    public void buildAndShowDialog(IParametrizedContinuation<Map<String, String>> saveCont) {
        this.saveContForConfluence = saveCont;

        super.buildAndShowDialog(null, null, null);
    }

    @Override
    public IBean4TreeExtractor<ConfluenceObjectBean, String> createBeanExtractor() {
        return new ConfluenceBean4TreeExtractor();
    }

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzObiektyDoDodania.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.wybierz.get() /* I18N:  */;
    }

    @Override
    protected String getHeadLblPrefixMsg() {
        return I18n.obiektZostanieDodanyWBIKSPod.get();
    }
}
