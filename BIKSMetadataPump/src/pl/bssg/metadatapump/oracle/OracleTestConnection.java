/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.oracle;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import pl.bssg.metadatapump.JdbcTestConnectionBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.common.ServerConfigBean;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import pl.trzy0.foxy.serverlogic.db.OracleConnection;
import pl.trzy0.foxy.serverlogic.db.OracleConnectionConfig;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class OracleTestConnection extends JdbcTestConnectionBase {

//    protected int id;
    protected ServerConfigBean bean;
    protected Connection conn;

    public OracleTestConnection(MssqlConnectionConfig mcfg, int id, String optObjsForGrantsTesting) {
        super(mcfg, id, optObjsForGrantsTesting);
//        this.id = id;
    }

    @Override
    protected Pair<Integer, String> testConnectionInner() {
        try {
            bean = adhocDao.createBeanFromNamedQry("getDBConfigForDBAndId", ServerConfigBean.class, true, PumpConstants.SOURCE_NAME_ORACLE, id);
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Invalid login for data loading! Error: " + e.getMessage());
        }
//        Boolean showGrantsInTestConnection = BaseUtils.tryParseBoolean((String) execNamedQuerySingleVal("getAppPropValue", true, "val", "showGrantsInTestConnection"), false);

//        String version = "";
        try {
            StringBuilder url = new StringBuilder();
            url.append("jdbc:oracle:thin:@//").append(bean.server).append(":").append(bean.port).append("/").append(bean.instance);
//            conn = PumpUtils.getConnection(OraclePump.DRIVER_CLASS, url.toString(), bean.user, bean.password);
            Class.forName(OraclePump.DRIVER_CLASS);
            conn = DriverManager.getConnection(url.toString(), bean.user, bean.password);
            if (conn == null) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No conencting with Oracle DB!");
            }
            if (showGrantsInTestConnection()) {
                bce = new OracleConnection(new OracleConnectionConfig(bean.server, bean.instance, bean.user, bean.password, bean.port));
            }
            Statement statment = conn.createStatement();
//            ResultSet execQry = statment.executeQuery("select banner as version from v$version where banner like 'Oracle%'");
            ResultSet execQry = statment.executeQuery("select * from all_tables where rownum = 1");

            if (execQry == null || !execQry.next()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No access to system tables: all_tables");
            }
//            version = execQry.getString("version");
//            if (showGrantsInTestConnection) {
//                StringBuilder sbGrant = new StringBuilder();
//                ResultSet grants = statment.executeQuery("select * from all_tab_privs_recd where grantee = '" + bean.user + "'");
////                sbGrant.append(database).append(": \n");
//                while (grants.next()) {
//                    String grantee = grants.getString("table_name");
//                    String type = grants.getString("privilege_type");
//                    sbGrant.append("   ").append(grantee).append(" - ").append(type).append("\n");
//                }
//                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, "OK\n" + /* + version +*/ " \nGrants:\n" + sbGrant.toString());
//            }
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connecting with Oracle DB! Error: " + e.getMessage());
        } finally {
            if (conn != null) {
                PumpUtils.closeConnection(conn);
            }
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, PumpConstants.PUMP_TEST_CONNECTION_OK);
    }
}
