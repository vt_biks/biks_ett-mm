/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

import java.util.Date;
import pl.bssg.metadatapump.common.SchedulePumpBean;

/**
 *
 * @author tflorczak
 */
public class DQMScheduleBean extends SchedulePumpBean {

    public int testId;
    public int testNodeId;
    public String name;
    public String testTreeCode;
    public String icon;
    public String databaseName;
    public String serverName;
    public String testInstanceName;
    public String fileName;
    public Integer multiSourceId;
    public Integer sourceCount;
    public Date scheduleDateWithTime;
    public String lastLog;
    public int statusFlag;
    public String connectionName;

    @Override
    public String toString() {
        return "DQMScheduleBean{" + "testId=" + testId + ", testNodeId=" + testNodeId + ", name=" + name + ", testTreeCode=" + testTreeCode + ", icon=" + icon + ", databaseName=" + databaseName + ", serverName=" + serverName + ", testInstanceName=" + testInstanceName + ", fileName=" + fileName + ", multiSourceId=" + multiSourceId + ", sourceCount=" + sourceCount + ", scheduleDateWithTime=" + scheduleDateWithTime + ", lastLog=" + lastLog + '}';
    }

}
