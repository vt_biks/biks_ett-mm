/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author mgraczkowski
 */
public class UserRoleInNodeBean implements Serializable {

    public int nodeId;
    public int roleForNodeId;
    public String roleCaption;
    public String nodeName;
    public String nodeTreeCode;
    public String nodeKindCode;
    public String branchIds;
    public String treeKind;
    public boolean inheritToDescendants;
    public boolean isAutomaticJoined; // połączenie jest automatyczne (autor obiektu) - wynika to z metadanych
    public boolean isAuxiliary;
    public boolean dstHasChildren;
    public String roleBranchIds;
    public int roleNodeId;
    public String descrPlain;
    public boolean isFromHyperlink;
}
