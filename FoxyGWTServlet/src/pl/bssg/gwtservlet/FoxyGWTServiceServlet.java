package pl.bssg.gwtservlet;

import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.server.rpc.RPC;
import com.google.gwt.user.server.rpc.RPCRequest;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import commonlib.ArrayToString;
import commonlib.LameLoggersConfigReloader;
import commonlib.LameUtils;
import commonlib.MuppetMerger;
import commonlib.UrlMakingUtils;
import commonlib.i18nsupport.JavaCurrentLocaleNameProvider;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import pl.fovis.foxygwtcommons.ServiceMethodCallException;
import pl.fovis.foxygwtcommons.i18nsupport.ServerSideI18nSupportingService;
import pl.trzy0.foxy.commons.FoxyCommonConsts;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.IFoxyTransactionalPerformer;
import pl.trzy0.foxy.commons.IHasFoxyTransactionalPerformers;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.IDaoAwareNamedSqlsModel;
import pl.trzy0.foxy.serverlogic.IFoxyServiceOnServer;
import pl.trzy0.foxy.serverlogic.IFoxySessionDataProvider;
import pl.trzy0.foxy.serverlogic.IServerSupportedLocaleNameRetriever;
import pl.trzy0.foxy.servlet.FoxyServletUtils;
import pl.trzy0.foxy.servlet.ServletConfigReader;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IGenericResourceProvider;
import simplelib.IResourceFromProvider;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.SimpleDateUtils;
import simplelib.StackTraceUtil;
import simplelib.Tuple3;
import simplelib.logging.ILameLogger;
import simplelib.logging.ILameLogger.LameLogLevel;

/**
 * Base abstract GWT Service servlet class
 *
 * @author wezyr
 * @param <CFG> - config class
 * @param <SD> - session data class
 */
public abstract class FoxyGWTServiceServlet<CFG, SD, SO> extends RemoteServiceServlet implements IFoxySessionDataProvider<SD> {

    public static final String WAFFLE_REMOTE_USER_ATTR_KEY = "WAFFLE_REMOTE_USER";
    private static final String FOXY_REQUEST_TEMP_STORAGE = "FoxyRequestTempStorage";
    private static final ILameLogger logger = LameUtils.getMyLogger();
    private static final String SESSION_DATA_KEY_PREFIX = "FOXY_SESSION_DATA:";
    public static final String FOXY_LOCALE_LANG_NAME_ATTR_KEY = "_foxy_locale_lang_name_";
    public static final String DEFAULT_LOCALE_NAME = "en";
    //protected IFoxyServiceOnServer<CFG, SD> impl;
    protected IRequestLoggingFoxyService reqLoggingImpl;
    protected ServerSideI18nSupportingService i18nSupporingService;
    protected IServerSupportedLocaleNameRetriever serverSupportedLocaleNameRetriever;
    protected String appName;
    protected INamedSqlsDAO<Object> adhocDao;
    protected AtomicInteger concurrentRequests = new AtomicInteger(0);
    protected String sessionDataKeyName;
    protected IFoxyServiceConfigurator<CFG, SD, SO> serviceConfigurator;
    protected List<Object> serviceImpls = new ArrayList<Object>();
    protected Map<Class, Object> intfClassToImplObjMap = new HashMap<Class, Object>();
    protected String dirForUpload;
    protected CFG cfg;
    private static final Map<String, String> fileExtToContentTypeMap = new HashMap<String, String>() {
        {
            put(".doc", "application/msword");
            put(".docx", "application/vnd.openxmlformats"); //lub application/vnd.openxmlformats-officedocument.wordprocessingml.document
            put(".xls", "application/excel");
            put(".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            put(".ppt", "application/powerpoint");
            put(".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
            put(".rtf", "application/rtf");
            put(".csv", "text/csv");
            //open office
            put(".odt", "application/vnd.oasis.opendocument.text");
            put(".ods", "application/vnd.oasis.opendocument.spreadsheet");
            put(".odp", "application/vnd.oasis.opendocument.presentation");
            put(".odg", "application/vnd.oasis.opendocument.graphics");
            put(".odc", "application/vnd.oasis.opendocument.chart");
            put(".odf", "application/vnd.oasis.opendocument.formula");
            put(".odi", "application/vnd.oasis.opendocument.image");
            put(".odm", "application/vnd.oasis.opendocument.text-master");
        }
    };

    private final Map<Thread, Pair<String, Long>> threadNameToServiceMethodAndNanosMap = new ConcurrentHashMap<Thread, Pair<String, Long>>();
    protected static final ThreadLocal<Long> serviceMethodInvokeCurrentMillis = new ThreadLocal<Long>();
    private static final long ONE_SECOND_IN_NANOS = 1000L * 1000L * 1000L;
    private static final long LONG_RUNNING_REQUEST_NANOS = 3L * ONE_SECOND_IN_NANOS;
    private static final LameLogLevel THREADS_SUSPICIOUS_BEHAVIOUR_LOGLEVEL = LameLogLevel.Stage;

    protected abstract IFoxyServiceConfigurator<CFG, SD, SO> getServiceConfigurator(ServletContext servletContext);

    protected String getDirForUpload() {

        if (cfg instanceof BaseConfigWithDirForUpload) {
            BaseConfigWithDirForUpload bcwdfu = (BaseConfigWithDirForUpload) cfg;
            return bcwdfu.dirForUpload;
        }

        if (cfg instanceof IConfigWithDirForUpload) {
            IConfigWithDirForUpload cwdfu = (IConfigWithDirForUpload) cfg;
            return cwdfu.getDirForUpload();
        }

        if (MuppetMerger.hasPropAssignableTo(cfg.getClass(), "dirForUpload", String.class)) {
            return (String) MuppetMerger.getProperty(cfg, "dirForUpload");
        }

        return null;
    }

    protected void registerAdditionalServiceImpls() {
        // no-op
    }

    protected String getConfigFileName(ServletContext servletContext) {
        return appName + "-config";
    }

    protected CFG readConfigBean(ServletContext servletContext) {
        return ServletConfigReader.readConfigObj(servletContext, serviceConfigurator.getConfigBeanClass(),
                getConfigFileName(servletContext));
    }

    protected IResourceFromProvider<String> createNamedSqlsResource(ServletContext servletContext, CFG cfg) {
        return FoxyServletUtils.getResourceFromServlet(servletContext, serviceConfigurator.getAdhocSqlsWebPath());
    }

    protected String extractShortServiceNameFromImplClassName(String serviceImplClassName) {
        serviceImplClassName = BaseUtils.dropOptionalSuffix(serviceImplClassName, "Impl", false);
        serviceImplClassName = BaseUtils.dropOptionalSuffix(serviceImplClassName, "Service", false);
        serviceImplClassName = BaseUtils.dropOptionalPrefix(serviceImplClassName, appName, true);
        return serviceImplClassName;
    }

    protected String makeFullPathToAdhocSqlsByName(String adhocSqlsWebPath, String adhocSqlsName) {
        return adhocSqlsWebPath + adhocSqlsName;
    }

    protected String getDefaultAdhocSqlsNameBySimpleName(String serviceSimpleName, Class serviceClass) {
        return "adhocSqls" + serviceSimpleName + ".xml";
    }

    protected String makeFullPathToAdhocSqlsOfService(String adhocSqlsWebPath, String serviceSimpleName,
            Class serviceClass) {
        return adhocSqlsWebPath + getDefaultAdhocSqlsNameBySimpleName(serviceSimpleName, serviceClass);
    }

    protected String getAdhocSqlsForServiceImpl(String adhocSqlsWebPath, Object serviceImpl) {
        Class<?> serviceClass = serviceImpl.getClass();
        boolean isPresent = serviceClass.isAnnotationPresent(WithoutAdhocSqls.class);

        if (logger.isDebugEnabled()) {
            logger.debug("+-+-+ getAdhocSqlsForServiceImpl: " + adhocSqlsWebPath + ", class=" + serviceClass
                    + ", isAnnotationPresent=" + isPresent + ", annotations: " + Arrays.asList(serviceClass.getAnnotations()));
        }

        if (isPresent) {
            return null;
        }

        AdhocSqlsName asn = serviceClass.getAnnotation(AdhocSqlsName.class);
        if (asn != null) {
            String adhocSqlsNameVal = asn.value();
            return adhocSqlsNameVal == null ? null : makeFullPathToAdhocSqlsByName(adhocSqlsWebPath, adhocSqlsNameVal);
        }

        if (!(serviceImpl instanceof IFoxyServiceOnServer)) {
            return null; // no default for custom services
        }

        String simpleServiceName = extractShortServiceNameFromImplClassName(serviceClass.getSimpleName());
        return makeFullPathToAdhocSqlsOfService(adhocSqlsWebPath, simpleServiceName, serviceClass);
    }

    protected Collection<String> getAdhocSqlWebPaths() {
        //ww: to się powinno zawołać tylko raz na starcie, a woła się chyba kilka razy?!? WTF?
        String path = serviceConfigurator.getAdhocSqlsWebPath();

        if (!BaseUtils.strHasSuffix(path, "/", false)) {
            return Collections.singleton(path);
        }

        Set<String> paths = new LinkedHashSet<String>();
        for (Object impl : serviceImpls) {
            String onePath = getAdhocSqlsForServiceImpl(path, impl);
            if (!BaseUtils.isStrEmptyOrWhiteSpace(onePath)) {
                paths.add(onePath);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getAdhocSqlWebPaths: paths=" + paths);
        }

        return paths;
    }

    protected IDaoAwareNamedSqlsModel<Object> getOptNamedSqlsModel() {
        return null;
    }

    protected INamedSqlsDAO<Object> createAdhocDao(ServletContext servletContext, CFG cfg) {
        Collection<String> paths = getAdhocSqlWebPaths();
        IGenericResourceProvider<String> resProvider = FoxyServletUtils.getStringResourceProviderFromServlet(servletContext);
        IBeanConnectionEx<Object> connection = serviceConfigurator.createDBConnection(cfg);
        //ww: do wywalenia, testy!!!
//        DaoSingleton.conn = connection;
        return FoxyAppUtils.createNamedSqlsDAOFromResources(connection, resProvider, paths,
                getOptNamedSqlsModel(),
                serverSupportedLocaleNameRetriever);
    }

//    protected INamedSqlsDAO<Object> createAdhocDao(ServletContext servletContext, CFG cfg) {
//        IResourceFromProvider<String> namedSqlsTextResource = createNamedSqlsResource(servletContext, cfg);
//        IBeanConnectionEx<Object> connection = serviceConfigurator.createDBConnection(cfg);
//        return FoxyAppUtils.createNamedSqlsDAOFromResource(connection, namedSqlsTextResource);
//    }
    protected String getSessionDataKeyName(ServletContext servletContext) {
        return SESSION_DATA_KEY_PREFIX + appName;
    }
    private static int initCallNum;
    private boolean instanceAlreadyInitialized;

    protected abstract SO createSharedSessionObj();

    /**
     * Base method to init servlet
     *
     * @param config {@link javax.servlet.ServletConfig}
     * @throws ServletException
     */
    @Override
    public synchronized void init(ServletConfig config) throws ServletException {

        try {

            ServletContext servletContext;
            try {
                servletContext = config.getServletContext();
                FoxyServletUtils.setupLameLoggersConfigReader(servletContext);
            } catch (Exception ex) {
                System.out.println("Exception in FoxyGWTServiceServlet.init()");
                System.out.println(StackTraceUtil.getCustomStackTrace(ex));
                throw new LameRuntimeException("Exception in FoxyGWTServiceServlet.init()", ex);
            }

//        System.out.println("init &&&&&&&&&&***&&&&&&&");
            final boolean isSubsequentInitCall = initCallNum > 0;

            LameLogLevel logLvl = isSubsequentInitCall ? LameLogLevel.Warn : LameLogLevel.Debug;

//        System.out.println("logLvl=" + logLvl);
            final boolean isLoggingEnabledAtLvl = logger.isEnabledAtLevel(logLvl);
//        System.out.println("isLoggingEnabledAtLvl=" + isLoggingEnabledAtLvl);

            if (isLoggingEnabledAtLvl) {
//            System.out.println("in if");
                final String logMsg = "init():start: " + (isSubsequentInitCall ? "subsequent" : "first") + " call: initCallNum=" + initCallNum
                        + ", instance addr: " + System.identityHashCode(this)
                        + ", class: " + getClass().getName() + ", stack trace:\n"
                        + LameUtils.getCurrentStackTrace();
//            System.out.println("msg: " + logMsg);
                logger.logAtLevel(logLvl, logMsg);
//            System.out.println("if ends");
            }
//        System.out.println("after if");
//        if (initCallNum > 0 && logger.isWarnEnabled()) {
//            logger.warn("init():start: initCallNum=" + initCallNum);
//        } else if (logger.isStageEnabled()) {
//            logger.stage("^^^^^ init():start: initCallNum=" + initCallNum);
//        }

            initCallNum++;

            super.init(config);

//        System.out.println("----> uruchomienie!");
            if (instanceAlreadyInitialized) {
//            System.out.println("############ double uruchomienie!");
                if (logger.isWarnEnabled()) {
                    logger.warn("servlet instance " + System.identityHashCode(this) + " of class "
                            + getClass().getName() + " is already initialized, init() ends");
                }
                return;
            }

            instanceAlreadyInitialized = true;

            serviceConfigurator = getServiceConfigurator(servletContext);

            appName = serviceConfigurator.getAppName();

            sessionDataKeyName = getSessionDataKeyName(servletContext);

            cfg = readConfigBean(servletContext);

            registerAllServiceImpls();

            if (i18nSupporingService == null) {
                i18nSupporingService = new ServerSideI18nSupportingServiceImplDef(this, serverSupportedLocaleNameRetriever);
                registerServiceImpl(i18nSupporingService);
            }

            adhocDao = createAdhocDao(servletContext, cfg);

            SO sharedSessionObj = createSharedSessionObj();

            boolean hasException = true;
            try {

                for (Object serviceImpl : serviceImpls) {
                    initServiceImpl(serviceImpl, sharedSessionObj);
                }

                hasException = false;
            } finally {
                finishWorkUnitOnConnections(hasException);
            }

            dirForUpload = getDirForUpload();

            if (isLoggingEnabledAtLvl) {
                logger.logAtLevel(logLvl, "init():end: initCallNum=" + (initCallNum - 1));
            }
//        if (logger.isDebugEnabled()) {
//            logger.debug("^^^^^ init():end: initCallNum=" + (initCallNum - 1));
//        }

        } catch (Throwable ex) {
            if (logger.isErrorEnabled()) {
                logger.error("exception in init", ex);
            }
            if (ex instanceof Error) {
                throw (Error) ex;
            }
            throw new LameRuntimeException("exception in init", ex);
        }
    }

    @Override
    public void destroy() {
        if (logger.isInfoEnabled()) {
            logger.info("destroy(): instance addr=" + System.identityHashCode(this) + ", class="
                    + getClass().getName());
        }

        for (Object serviceImpl : serviceImpls) {
            if (serviceImpl instanceof IFoxyServiceOnServer) {
                ((IFoxyServiceOnServer) serviceImpl).destroy();
            }
        }

        super.destroy();
    }

    protected void registerAllServiceImpls() {
        IFoxyServiceOnServer<CFG, SD, SO> mainImpl = serviceConfigurator.getServiceImpl();
        if (mainImpl != null) {
            registerServiceImpl(mainImpl);
        }

        registerAdditionalServiceImpls();
    }

    protected void registerServiceImpl(Object serviceImpl) {
        serviceImpls.add(serviceImpl);
        if (reqLoggingImpl == null && serviceImpl instanceof IRequestLoggingFoxyService) {
            reqLoggingImpl = (IRequestLoggingFoxyService) serviceImpl;
        }
        if (i18nSupporingService == null && serviceImpl instanceof ServerSideI18nSupportingService) {
            i18nSupporingService = (ServerSideI18nSupportingService) serviceImpl;
        }
        if (serverSupportedLocaleNameRetriever == null && serviceImpl instanceof IServerSupportedLocaleNameRetriever) {
            serverSupportedLocaleNameRetriever = (IServerSupportedLocaleNameRetriever) serviceImpl;
        }
    }

//    protected void addServiceImplWithInit(IFoxyServiceOnServer<CFG, SD> serviceImpl) {
//        serviceImpl.init(cfg, this, adhocDao);
//        registerServiceImpl(serviceImpl);
//    }
    protected SD getSessionData(HttpSession session) {
        if (session == null) {
            return null;
        }

        @SuppressWarnings("unchecked")
        SD sessionData = (SD) session.getAttribute(sessionDataKeyName);
        if (sessionData == null) {
            try {
                sessionData = serviceConfigurator.getSessionDataClass().newInstance();
            } catch (Exception ex) {
                throw new LameRuntimeException("error while creating session bean", ex);
            }
            // zbędne - inicjalnie będzie false
            //sessionData.ignoreRemoteUser = false;
            session.setAttribute(sessionDataKeyName, sessionData);
        }

        if (sessionData instanceof IFoxySessionDataRemoteUserAware) {
            IFoxySessionDataRemoteUserAware remoteAware = (IFoxySessionDataRemoteUserAware) sessionData;
            HttpServletRequest request = this.getThreadLocalRequest();
            String remoteUser = request.getRemoteUser();
            if (remoteUser == null) {
                remoteUser = (String) session.getAttribute(WAFFLE_REMOTE_USER_ATTR_KEY);
            }

            if (logger.isDebugEnabled()) {
                logger.debug("getSessionData: remoteUser=[" + remoteUser + "]");
            }

            if (remoteUser != null) {
                // nie odrzucamy już nazwy domeny
//                remoteUser = remoteUser.replaceFirst(".*\\\\", "");
//                if (logger.isDebugEnabled()) {
//                    logger.debug("getSessionData: remoteUser after domain dropping=[" + remoteUser + "]");
//                }
                remoteAware.setRemoteUserName(remoteUser);
                session.removeAttribute(WAFFLE_REMOTE_USER_ATTR_KEY);
            }
        }

        return sessionData;
    }

    /**
     * Base method to get session data from servlet
     *
     * @return session data
     */
    @Override
    public SD getSessionData() {
        HttpSession session = getThreadLocalSession();
        return getSessionData(session);
    }

    @Override
    public Map<String, Object> getRequestTempStorage() {
        HttpServletRequest req = getThreadLocalRequest();
        @SuppressWarnings("unchecked")
        Map<String, Object> res = (Map) req.getAttribute(FOXY_REQUEST_TEMP_STORAGE);
        if (res == null) {
            res = new HashMap<String, Object>();
            req.setAttribute(FOXY_REQUEST_TEMP_STORAGE, res);
        }
        return res;
    }

    protected Pair<String, String> getMethodAndParamsAsStrings(Method mtc, Object[] mtcp) {
        return new Pair<String, String>(mtc.getName(), ArrayToString.arrayToString(mtcp, false));
    }

    protected boolean isRequestLoggingEnabled() {
        return reqLoggingImpl != null && reqLoggingImpl.isServletRequestLoggingEnabled();
    }

    protected String getCurrentLocaleNameFromRequest(Object serviceImpl, HttpServletRequest request) {
        // słabo jest, trzeba użyć tej metody!!! http://stackoverflow.com/a/10912078/209507
//        String localeName = request.getParameter(REQUEST_PARAM_NAME_FOR_LOCALE_NAME);
        String localeName = request.getHeader(FoxyCommonConsts.REQ_EXTRA_PARAM_PREFIX
                + FoxyCommonConsts.REQUEST_PARAM_NAME_FOR_LOCALE_NAME);

        Set<String> localeNames;
        if (serviceImpl instanceof IServiceImplWithLocaleNamesOverride) {
            IServiceImplWithLocaleNamesOverride siwlno = (IServiceImplWithLocaleNamesOverride) serviceImpl;
            localeNames = siwlno.getSupportedLocaleNames();
        } else {
            localeNames = getServerSupportedLocaleNames();
        }

        if (BaseUtils.isCollectionEmpty(localeNames)) {
            return DEFAULT_LOCALE_NAME;
        }

        if (localeNames.size() == 1) {
            return BaseUtils.safeGetFirstItem(localeNames, DEFAULT_LOCALE_NAME);
        }

        if (localeNames.contains(localeName)) {
            return localeName;
        }

        HttpSession session = request.getSession();
        localeName = (String) session.getAttribute(FOXY_LOCALE_LANG_NAME_ATTR_KEY);

        if (localeName == null) {
            // brak w sesji
            String acceptLanguage = BaseUtils.safeToLowerCase(getThreadLocalRequest().getHeader("Accept-Language"));
            localeName = BaseUtils.getBestAcceptedAndSupportedLanguage(acceptLanguage,
                    localeNames, DEFAULT_LOCALE_NAME);
            session.setAttribute(FOXY_LOCALE_LANG_NAME_ATTR_KEY, localeName);
        }

//                        if (logger.isErrorEnabled() && BaseUtils.isStrEmptyOrWhiteSpace(localeName)) {
//                            logger.error("localeName is empty for session id=" + session.getId());
//                        }
        return localeName;
    }

    @Deprecated
    public static void sleepInRequestForThreadsSuspiciousBehaviour(int inOneOutOfNCalls) {
        if (BaseUtils.nextRandomInt(inOneOutOfNCalls) == 0) {
            final long sleepForMillis = LONG_RUNNING_REQUEST_NANOS / 1000000L * 3;
            logger.fatal("sleepInRequestForThreadsSuspiciousBehaviour: sleeping for " + sleepForMillis + " ms starts");
            LameUtils.threadSleep(sleepForMillis);
            logger.fatal("sleepInRequestForThreadsSuspiciousBehaviour: done sleeping for " + sleepForMillis + " ms");
        }
    }

    private String getOptThreadsSuspiciousBehaviourDescr() {

        long longest = -1;
        int longRunning = 0;

        long currentNanos = System.nanoTime();

        StringBuilder sb = null;

        for (Entry<Thread, Pair<String, Long>> e : threadNameToServiceMethodAndNanosMap.entrySet()) {
            Thread thread = e.getKey();
            String threadName = thread.getName();
            final Pair<String, Long> v = e.getValue();
            String methodDescr = v.v1;
            Long startNanos = v.v2;
            final long runningForNanos = currentNanos - startNanos;

            if (runningForNanos >= LONG_RUNNING_REQUEST_NANOS) {
                if (sb == null) {
                    sb = new StringBuilder();
                }

                longRunning++;

                sb.append("  [").append(threadName).append("]: ").append(methodDescr).append(" running for: ").append(runningForNanos / ONE_SECOND_IN_NANOS).append(" s., call stack:\n");
                StackTraceUtil.appendStackTrace(thread.getStackTrace(), sb);

                if (runningForNanos > longest) {
                    longest = runningForNanos;
                }
            }
        }

        if (sb != null) {
            sb.append("long running: ").append(longRunning).append(", longest running for: ").append(longest / ONE_SECOND_IN_NANOS).append(" s.");
            return sb.toString();
        }

        return null;
    }

    @Override
    public String processCall(String payload) throws SerializationException {

        if (logger.isDebugEnabled()) {
            logger.debug("starting processCall() for payload: " + payload);
        }

        HttpServletResponse response = getThreadLocalResponse();

        response.setHeader("_foxy_request_timing_start_", SimpleDateUtils.toCanonicalStringWithMillis(new Date()));

//        System.out.println("payload: " + payload);
        int otherReqCnt = concurrentRequests.getAndIncrement();
        try {
            LameLoggersConfigReloader.reloadIfNeeded();

            boolean isServletRequestLoggingEnabled = false;

            try {
                isServletRequestLoggingEnabled = isRequestLoggingEnabled();
            } catch (Exception ex) {
                if (logger.isErrorEnabled()) {
                    logger.error("error in isServletRequestLoggingEnabled - ignored", ex);
                }
                ex.printStackTrace();
                // no re-throw!
            }

            //Object delegate = impl;
            // First, check for possible XSRF situation
            checkPermutationStrongName();

            Integer reqId = null;
            String elapsedMillisStr = null;
            Exception caughtEx = null;
            boolean needsMethodCallDescr = isServletRequestLoggingEnabled;
            boolean needsThreadsSuspiciousBehaviourLogging = logger.isEnabledAtLevel(THREADS_SUSPICIOUS_BEHAVIOUR_LOGLEVEL);

            try {
                RPCRequest rpcRequest = RPC.decodeRequest(payload, null /*delegate.getClass()*/, this);

                final Method mtc = rpcRequest.getMethod();
                boolean methodCallLoggingEnabled = logger.isInfoEnabled()
                        && !mtc.isAnnotationPresent(DisableCallLoggingForServiceImplMethod.class);
                needsMethodCallDescr = needsMethodCallDescr || methodCallLoggingEnabled || needsThreadsSuspiciousBehaviourLogging;

                final String methodName = mtc.getName();
                final Class<?> intfClass = mtc.getDeclaringClass();
//                if (logger.isDebugEnabled()) logger.debug("method name=" + methodName + ", declaring class=" + intfClass);

                Object[] mtcp = rpcRequest.getParameters();
//                Object properImpl =

                Object delegate = getCachedServiceImplOfClass(intfClass);
                if (delegate == null) {
                    throw new LameRuntimeException("no impl created for method: " + methodName + " of class: " + intfClass);
                }

                Method implMethodX = null;
                try {
                    implMethodX = delegate.getClass().getMethod(methodName, mtc.getParameterTypes());
                } catch (Exception ex) {
                    throw new LameRuntimeException("problem with fetching impl method " + methodName + " from implementing class", ex);
                }

                final Method implMethod = implMethodX;

                onAfterRequestDeserialized(rpcRequest);

                boolean hasException = false;

                IRequestLifecycleAwareService rlas = null;
                IServiceRequestContext serviceRequestContext = null;

                try {
                    try {
                        final HttpServletRequest request = this.getThreadLocalRequest();

                        if (delegate instanceof IRequestLifecycleAwareService) {
                            rlas = (IRequestLifecycleAwareService) delegate;
//                            String referer = this.getThreadLocalRequest().getHeader("referer");
//                            rlas.requestStarting(referer);

//                            System.out.println("requestStarting on " + rlas.getClass().getName());
                            serviceRequestContext = new IServiceRequestContext() {
                                @Override
                                public String getContextParamValue(String paramName) {
                                    return request.getHeader(FoxyCommonConsts.REQ_EXTRA_PARAM_PREFIX + paramName);
                                }

                                @Override
                                public String getServiceMethodName() {
                                    return methodName;
                                }

                                @Override
                                public Method getServiceImplMethod() {
                                    return implMethod;
                                }

                                @Override
                                public Method getServiceIntfMethod() {
                                    return mtc;
                                }
                            };
                            rlas.requestStarting(serviceRequestContext);
                        }

                        Pair<String, String> callDescr;
                        String callDescrMethodAndParams;

                        if (needsMethodCallDescr) {
                            callDescr = getMethodAndParamsAsStrings(mtc, mtcp);
                            callDescrMethodAndParams = callDescr.v1 + "(" + callDescr.v2 + ")";
                        } else {
                            callDescr = null;
                            callDescrMethodAndParams = null;
                        }
                        HttpSession session = request.getSession();

                        if (isServletRequestLoggingEnabled) {
                            try {
                                reqId = reqLoggingImpl.insertServiceRequest(callDescr.v1,
                                        callDescr.v2, request.getRemoteAddr(), session.getId(),
                                        "otherReqCnt: " + otherReqCnt);

                            } catch (Exception ex) {
                                if (logger.isErrorEnabled()) {
                                    logger.error("error in insertServiceRequest - ignored", ex);
                                }
                                ex.printStackTrace();
                                // no re-throw!
                            }
                        }

//                        SD sessionData = getSessionData(session);
//                        if (sessionData instanceof IFoxySessionDataLocaleAware) {
//                            IFoxySessionDataLocaleAware locAware = (IFoxySessionDataLocaleAware) sessionData;
//                            String localeName = locAware.getCurrentLocaleName();
//                            JavaCurrentLocaleNameProvider.setCurrentLocaleName(localeName);
//                        }
//                        String localeName = (String) session.getAttribute(FOXY_LOCALE_LANG_NAME_ATTR_KEY);
//
//                        if (logger.isErrorEnabled() && BaseUtils.isStrEmptyOrWhiteSpace(localeName)) {
//                            logger.error("localeName is empty for session id=" + session.getId());
//                        }
                        String localeName = getCurrentLocaleNameFromRequest(delegate, request);

                        JavaCurrentLocaleNameProvider.setCurrentLocaleName(localeName);

                        long reqMethodStartNanos = System.nanoTime();

                        String callResult = null;

                        Boolean shouldInvokeAndEncodeResponse = true;

                        try {
                            if (delegate instanceof IRestrictedAccessService) {
                                IRestrictedAccessService ras = (IRestrictedAccessService) delegate;
                                if (ras.userNotLoggedInError(implMethod)) {
                                    callResult = RPC.encodeResponseForFailure(null, ras.getExceptionForNotLoggedUser());
                                    shouldInvokeAndEncodeResponse = false;
                                } else if (!ras.canAccess(implMethod)) {
                                    throw new Exception("Access denied to method " + methodName);
                                }
                            }
                            if (shouldInvokeAndEncodeResponse) {

                                if (needsThreadsSuspiciousBehaviourLogging) {

                                    String otsbd = getOptThreadsSuspiciousBehaviourDescr();
                                    if (otsbd != null) {
                                        logger.logAtLevel(THREADS_SUSPICIOUS_BEHAVIOUR_LOGLEVEL, "THREADS_SUSPICIOUS_BEHAVIOUR:\n" + otsbd);
                                    }

//                                    reqMethodStartNanos = System.nanoTime();
                                    threadNameToServiceMethodAndNanosMap.put(Thread.currentThread(), new Pair<String, Long>(callDescrMethodAndParams, reqMethodStartNanos));
                                }
                                try {
                                    serviceMethodInvokeCurrentMillis.set(System.currentTimeMillis());
                                    try {
                                        callResult = RPC.invokeAndEncodeResponse(delegate, mtc,
                                                mtcp, rpcRequest.getSerializationPolicy(),
                                                rpcRequest.getFlags());
                                    } finally {
                                        serviceMethodInvokeCurrentMillis.remove();
                                    }
                                } finally {
                                    if (needsThreadsSuspiciousBehaviourLogging) {
                                        threadNameToServiceMethodAndNanosMap.remove(Thread.currentThread());
                                    }
                                }
                            }
                        } finally {
                            if (needsMethodCallDescr) {
                                long reqMethodEndNanos = System.nanoTime();
//                                elapsedMillisStr = BaseUtils.doubleToString((reqMethodEndNanos - reqMethodStartNanos) / 1000000.0, 3) + " millis";
                                elapsedMillisStr = SimpleDateUtils.elapsedNanosToMillis3DPrec(reqMethodStartNanos, reqMethodEndNanos) + " millis";
                            }
                        }

                        if (methodCallLoggingEnabled) {
                            logger.info("call (in " + elapsedMillisStr + ") to: " + callDescrMethodAndParams);
                        }

                        if (BaseUtils.strHasPrefix(callResult, "//EX", true)) {
                            hasException = true;
                            String exMsg = "expected (declared) exception in service method, result: " + callResult;
                            caughtEx = new LameRuntimeException(exMsg);
                            if (logger.isErrorEnabled()) {
                                logger.error(exMsg);
                            }
                        }

                        return callResult;
                    } catch (IncompatibleRemoteServiceException ex) {
                        hasException = true;
                        caughtEx = ex;
                        //ex.printStackTrace();
                        throw ex;
                    } catch (Exception ex) {
                        hasException = true;
                        caughtEx = ex;
                        //ex.printStackTrace();
                        throw new LameRuntimeException("exception in processCall: payload=" + payload, ex);
                    }
                } finally {
                    try {
                        if (hasException) {
                            //System.out.println("hasException: entering finally");
                            if (logger.isErrorEnabled()) {
                                logger.error("exception in processCall for payload: " + payload, caughtEx);
                            }
                        }

                        JavaCurrentLocaleNameProvider.setCurrentLocaleName(null);

                        finishWorkUnitOnConnections(hasException);
                        response.setHeader(FoxyCommonConsts.FOXY_REQUEST_TIMING_AFTER_COMMIT_TIMESTAMP_HEADER_NAME, Long.toString(System.currentTimeMillis()));

//                        if (hasException) {
//                            System.out.println("hasException: before if (rlas != null), rlas=" + rlas);
//                        }
                        if (rlas != null) {
                            // to może znów korzystać z połączeń (connections), więc trzeba je oddać do puli
                            boolean he3 = true;
                            try {
//                                if (hasException) {
//                                    System.out.println("hasException: before rlas.requestFinishing");
//                                }
                                rlas.requestFinishing(serviceRequestContext, caughtEx);
//                                if (hasException) {
//                                    System.out.println("hasException: after rlas.requestFinishing");
//                                }
                                he3 = false;
                            } finally {
                                finishWorkUnitOnConnections(he3);
                            }
                        }

                        if (isServletRequestLoggingEnabled && reqId != null) {
                            boolean he3 = true;

                            try {
                                reqLoggingImpl.updateServiceRequest(reqId,
                                        "timeInMethod: " + elapsedMillisStr,
                                        BaseUtils.safeToString(caughtEx));
                                he3 = false;
                            } catch (Exception ex) {
                                if (logger.isErrorEnabled()) {
                                    logger.error("error in updateServiceRequest - ignored", ex);
                                }
                                ex.printStackTrace();
                                // no re-throw!
                            } finally {
                                try {
                                    // try to rollback tran
                                    //adhocDao.finishWorkUnit(false);
                                    finishWorkUnitOnConnections(he3);
                                } catch (Exception ex2) {
                                    // no-op
                                }
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

            } catch (IncompatibleRemoteServiceException ex) {
                if (logger.isErrorEnabled()) {
                    logger.error("An IncompatibleRemoteServiceException was thrown while processing this call", ex);
                }
                log("An IncompatibleRemoteServiceException was thrown while processing this call.", ex);
                return RPC.encodeResponseForFailure(null, ex);
            } catch (Exception ex) {

                ex = new ServiceMethodCallException("error in processCall, caused by:\n" + StackTraceUtil.getCustomStackTrace(ex));

                String responsePayload = RPC.encodeResponseForFailure(null, ex);
                if (logger.isErrorEnabled()) {
                    logger.error("encoded response for failure: " + responsePayload);
                }
                return responsePayload;
            }
        } finally {
            concurrentRequests.decrementAndGet();
            response.setHeader("_foxy_request_timing_end_", SimpleDateUtils.toCanonicalStringWithMillis(new Date()));
        }
    }

    protected Object findProperImplForIntfClass(Class intfClass) {

        for (Object oneImpl : serviceImpls) {
            if (intfClass.isInstance(oneImpl)) {
                return oneImpl;
            }
        }

        return null;
    }

//    @Override
//    protected void onAfterResponseSerialized(String serializedResponse) {
//        super.onAfterResponseSerialized(serializedResponse);
//
//        if (BaseUtils.strHasPrefix(serializedResponse, "//EX", true)) {
//            if (logger.isErrorEnabled()) {
//                logger.error("serializedResponse of an exception: " + serializedResponse);
//            }
//        }
//    }
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String methodName = req.getMethod();
        if (methodName.equals("POST") || methodName.equals("GET")) {
            if (processFileHandlingRequest(req, resp)) {
                return;
            }
        }

        super.service(req, resp);

//        try {
//            super.service(req, resp);
//        } catch (Exception ex) {
//            if (logger.isErrorEnabled()) {
//                logger.error("exception in super.service()", ex);
//            }
//            if (ex instanceof RuntimeException) {
//                throw (RuntimeException) ex;
//            } else if (ex instanceof ServletException) {
//                throw (ServletException) ex;
//            } else if (ex instanceof IOException) {
//                throw (IOException) ex;
//            } else {
//                throw new LameRuntimeException("exception in super.service()", ex);
//            }
//        }
    }

    //ww: c&p z AbstractRemoteServiceServlet bo tam private
    private void validateThreadLocalData() {
        if (perThreadRequest == null) {
            perThreadRequest = new ThreadLocal<HttpServletRequest>();
        }
        if (perThreadResponse == null) {
            perThreadResponse = new ThreadLocal<HttpServletResponse>();
        }
    }

    protected void setPerThreadVars(HttpServletRequest request, HttpServletResponse response) {
        synchronized (this) {
            validateThreadLocalData();
            if (request != null) {
                perThreadRequest.set(request);
            } else {
                perThreadRequest.remove();
            }
            if (response != null) {
                perThreadResponse.set(response);
            } else {
                perThreadResponse.remove();
            }
        }
    }

    protected void runCustomSessionAwareCode(HttpServletRequest req, HttpServletResponse resp, IContinuation codeCont) {
        setPerThreadVars(req, resp);
        try {
            codeCont.doIt();
        } finally {
            clearPerThreadVars();
        }
    }

    protected void clearPerThreadVars() {
        setPerThreadVars(null, null);
    }

    protected boolean processFileHandlingRequest(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        if (dirForUpload == null) {
            return false;
        }

        boolean isServeFileReq = !BaseUtils.isStrEmptyOrWhiteSpace(req.getParameter("get"));
        boolean isUploadFileReq = !isServeFileReq && ServletFileUpload.isMultipartContent(req);

        if (!isServeFileReq && !isUploadFileReq) {
            return false;
        }

        req.setCharacterEncoding("utf8");
        if (isServeFileReq) {
            req.setCharacterEncoding("UTF-8");
            String serverFileName = req.getParameter("get");
            String clientFileName = BaseUtils.emptyStringOrWhiteSpaceToDef(
                    req.getParameter(FoxyCommonConsts.CLIENT_FILE_NAME_SERVLET_PARAM_NAME), serverFileName);
            serveFile(req, serverFileName, clientFileName, false, resp);
        } else {
            processUploadRequest(req, resp);
        }
        return true;
    }

    protected int getUploadFileSizeMax() {
        return -1;
    }

    public static boolean isIeBrowser(HttpServletRequest req) {
        String userAgent = req.getHeader("user-agent");
        return userAgent != null && userAgent.contains("MSIE");
    }

//    protected void serveFile(String serverFileName, HttpServletResponse response) {
//        serveFile(serverFileName, serverFileName, response);
//    }
    protected void serveFile(HttpServletRequest req, String serverFileName, String clientFileName, boolean fileAsAttachment,
            HttpServletResponse response) {
        String filePath = dirForUpload;

        // Check if file name is supplied to the request.
        if (serverFileName != null) {
            // Strip slashes (avoid directory sniffing by hackers!).
            serverFileName = serverFileName.replaceAll("\\\\|/", "");
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("serveFile: no file");
            }
            return;
        }
        // Prepare file object.
        File file = new File(filePath, serverFileName);

        // Check if file actually exists in filesystem.
        if (!file.exists()) {
            if (logger.isDebugEnabled()) {
                logger.debug("serveFile: file \"" + serverFileName + "\" does not exist");
            }
            return;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("serveFile: file exists");
        }
        // Get content type by filename.
        String contentType = URLConnection.guessContentTypeFromName(serverFileName);
        if (contentType == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("extractFileExt: \"" + BaseUtils.extractFileExt(serverFileName) + "\"");
            }
            contentType = fileExtToContentTypeMap.get(BaseUtils.extractFileExt(serverFileName));
        }
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        if (logger.isDebugEnabled()) {
            logger.debug("serveFile: content type: " + contentType);
        }

        // Prepare streams.
        BufferedInputStream input = null;
        BufferedOutputStream output = null;
        try {
            // Open image file.
            input = new BufferedInputStream(new FileInputStream(file));

            int contentLength = input.available();

            // Init servlet response.
            response.setContentLength(contentLength);
            response.setContentType(contentType);
            String encodedClientFileName = UrlMakingUtils.encodeStringForUrlWW(clientFileName).replace("+", "%20");
            if (logger.isDebugEnabled()) {
                logger.debug("serveFile: clientFileName=" + clientFileName);
                logger.debug("serveFile: encodedClientFileName=" + encodedClientFileName);
            }
//            System.out.println("encodedClientFileName = " + encodedClientFileName);
            String contentDispositionVal = (fileAsAttachment ? "attachment" : "inline") + "; filename";
            boolean isBrowserInternetExplorer = isIeBrowser(req);
            if (isBrowserInternetExplorer) {
                contentDispositionVal += "=\"" + encodedClientFileName + "\"";
            } else {
                contentDispositionVal += "*=UTF-8\'\'" + encodedClientFileName;
            }
            //tf -> ww: zakomentowałem sysouta bo niepotrzebnie wypisywal informacje w BIKSie na konsoli...
//            System.out.println("isBrowserInternetExplorer=" + isBrowserInternetExplorer + ", serverFileName=" + serverFileName + ", clientFileName=\"" + clientFileName + "\""
//                    + ", contentDispositionVal=" + contentDispositionVal);
            if (logger.isDebugEnabled()) {
                logger.debug("serveFile: contentDispositionVal=" + contentDispositionVal);
            }
            response.setHeader("Content-disposition", contentDispositionVal); //attachment inline

//            response.setHeader("Content-disposition", (fileAsAttachment ? "attachment" : "inline") + "; filename*=\""
//                    + UrlMakingUtils.encodeStringForUrlWW(clientFileName) + "\""); //attachment inline
            output = new BufferedOutputStream(response.getOutputStream());

            // Write file contents to response.
            while (contentLength-- > 0) {
                output.write(input.read());
            }
            // Finalize task.
            output.flush();
        } catch (IOException e) {
            if (logger.isErrorEnabled()) {
                logger.error("serveFile: something went wrong while serving file: " + serverFileName);
            }
            e.printStackTrace();
        } finally {
            // Gently close streams.
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    if (logger.isErrorEnabled()) {
                        logger.error("serveFile: something went wrong while closing file: " + serverFileName);
                    }
                    e.printStackTrace();
                }
            }
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    if (logger.isErrorEnabled()) {
                        logger.error("serveFile: something went wrong while closing output for file: " + serverFileName);
                    }
                    e.printStackTrace();
                }
            }
        }
    }

    protected byte[] preprocessUploadedFile(byte[] fileContent, String fileName) {
        return fileContent;
    }

    protected void processUploadRequest(HttpServletRequest request,
            HttpServletResponse response) throws IOException {

//        if (logger.isDebugEnabled()) {
//            logger.debug("Upload file. Set Content Type");
//        }
        response.setContentType("text/html;charset=UTF-8");
        //if (logger.isDebugEnabled()) logger.debug("----------------- UPLOAD ----------------");

        Map<String, Object> res = new HashMap<String, Object>();

        // Create a factory for disk-based file items
//        if (logger.isDebugEnabled()) {
//            logger.debug("Create a factory for disk-based file items");
//        }
        FileItemFactory factory = new DiskFileItemFactory();

        // Create a new file upload handler
//        if (logger.isDebugEnabled()) {
//            logger.debug("Create a new file upload handler");
//        }
        ServletFileUpload upload = new ServletFileUpload(factory);
        try {
            int fileSizeMax = getUploadFileSizeMax();
            if (fileSizeMax >= 0) {
                upload.setFileSizeMax(fileSizeMax);
            }
            List /* FileItem */ items = upload.parseRequest(request);

            // Process the uploaded items
            Iterator iter = items.iterator();
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();

                if (item.isFormField()) {
                    // zwykłe pole
                    String name = item.getFieldName();
                    String value;
                    try {
                        value = item.getString("UTF-8");
                    } catch (UnsupportedEncodingException ex) {
                        throw new LameRuntimeException(ex);
                    }

                    res.put(name, value);

                } else {
                    // plik
                    String fieldName = item.getFieldName();
                    String fileName = item.getName();
                    long sizeInBytes = item.getSize();

                    res.put(fieldName, new Tuple3<String, byte[], Long>(fileName, item.get(), sizeInBytes));
                }
            }

        } catch (FileUploadException ex) {
            //ex.printStackTrace();
            throw new LameRuntimeException(ex);
        }
        @SuppressWarnings("unchecked")
        Tuple3<String, byte[], Long> t3 = (Tuple3) res.get("file");
        String fileName = t3.v1;

//        if (logger.isDebugEnabled()) {
//            logger.debug("Before preprocessUploadedFile");
//        }
        byte[] fileBytes = preprocessUploadedFile(t3.v2, fileName);

        String ext = BaseUtils.extractFileExtFromFullPath(fileName);
        //LameUtils.extractFileExtNoDot(fileName);
        //if (logger.isDebugEnabled()) logger.debug("------- dir for upload: \"" + configObj.dirForUpload + "\" -------");
        if (checkExtensionByMimeType(fileBytes, fileName)) {
            File dir = new File(dirForUpload);

//        if (logger.isDebugEnabled()) {
//            logger.debug("Before File.createTempFile. ext = " + ext + ", dir = " + dir);
//        }
            File file = File.createTempFile("file_", ext, dir);

//        if (logger.isDebugEnabled()) {
//            logger.debug("Before write bytes.");
//        }
            FileOutputStream fileOut = new FileOutputStream(file);
            fileOut.write(fileBytes);
            fileOut.close();

            PrintWriter out = response.getWriter();
            out.print(file.getName());
            out.close();
//        if (logger.isDebugEnabled()) {
//            logger.debug("Upload success!");
//        }
        }
    }

    protected boolean checkExtensionByMimeType(byte[] fileBytes, String fileName) {
        boolean allowDwnld = false;
        String extension;
        String ext = BaseUtils.extractFileExtFromFullPath(fileName);
        InputStream input = new ByteArrayInputStream(fileBytes);
        try {
            TikaConfig tika = new TikaConfig();

            Metadata metadata = new Metadata();
            metadata.set(Metadata.RESOURCE_NAME_KEY, fileName);
            MediaType mediaType;
            mediaType = tika.getDetector().detect(input, metadata);
            MimeType mimeType = tika.getMimeRepository().forName(mediaType.toString());
            extension = mimeType.getExtension();

            if (extension.equals(ext.toLowerCase()) || ext.toLowerCase().equals(".metadata")) {
                allowDwnld = true;
            }
            input.close();
        } catch (IOException ex) {
            logger.error(FoxyGWTServiceServlet.class.getName() + " IoException ", ex);
        } catch (MimeTypeException ex) {
            logger.error(FoxyGWTServiceServlet.class.getName() + " MimeTypeException ", ex);
        } catch (TikaException ex) {
            logger.error(FoxyGWTServiceServlet.class.getName() + " TikaException ", ex);
        }
        return allowDwnld;
    }

    protected void initServiceImpl(Object serviceImpl, SO sharedSessionObj) {
        if (serviceImpl instanceof IFoxyServiceOnServer) {
            @SuppressWarnings("unchecked")
            IFoxyServiceOnServer<CFG, SD, SO> fsos = (IFoxyServiceOnServer) serviceImpl;
            try {
                fsos.init(cfg, this, adhocDao, sharedSessionObj);
            } catch (Exception ex) {
                if (logger.isErrorEnabled()) {
                    logger.error("cannot init service", ex);
                }
            }
        }
    }

    protected Object getCachedServiceImplOfClass(final Class<?> intfClass) {
        Object properImpl = intfClassToImplObjMap.get(intfClass);
        if (properImpl == null) {

            properImpl = findProperImplForIntfClass(intfClass);

            if (properImpl != null) {
                intfClassToImplObjMap.put(intfClass, properImpl);
            }
        }

        return properImpl;
    }

    protected HttpSession getThreadLocalSession() {
        HttpServletRequest request = this.getThreadLocalRequest();
        if (request == null) {
            return null;
        }
        HttpSession session = request.getSession();
        return session;
    }

    private void tryFinishWorkUnitOnOneConnetion(boolean success,
            Object transPerfCandidate,
            Set<IFoxyTransactionalPerformer> finished,
            List<Exception> exceptions) {

        Collection<? extends IFoxyTransactionalPerformer> transPerfs = null;
        if (transPerfCandidate instanceof IHasFoxyTransactionalPerformers) {
            IHasFoxyTransactionalPerformers htp = (IHasFoxyTransactionalPerformers) transPerfCandidate;
            transPerfs = htp.getTransactionalPerformers();
        } else if (transPerfCandidate instanceof IFoxyTransactionalPerformer) {
            transPerfs = Collections.singleton((IFoxyTransactionalPerformer) transPerfCandidate);
        }

        if (transPerfs == null) {
//            System.out.println("tryFinishWorkUnitOnOneConnetion: no transPerfs");
            return;
        }

        for (IFoxyTransactionalPerformer transPerf : transPerfs) {
            try {
                if (transPerf != null && finished.add(transPerf)) {
//                    System.out.println("tryFinishWorkUnitOnOneConnetion: finishWorkUnit(" + success + ") on " + transPerf.getClass().getName());
                    transPerf.finishWorkUnit(success);
                }
            } catch (Exception ex) {
                exceptions.add(ex);
                if (logger.isErrorEnabled()) {
                    logger.error("tryFinishWorkUnitOnOneConnetion: error on finishWorkUnit(" + success + ") for transPerf of class"
                            + BaseUtils.safeGetClassName(transPerf) + ": ex=" + ex, ex);
                }
            }
        }
    }

    protected void finishWorkUnitOnConnections(boolean hasException) {

        Set<IFoxyTransactionalPerformer> finished = new HashSet<IFoxyTransactionalPerformer>();
        List<Exception> exceptions = new ArrayList<Exception>();
        boolean success = !hasException;

//        IFoxyTransactionalPerformer tp = adhocDao;
        tryFinishWorkUnitOnOneConnetion(success, adhocDao, finished, exceptions);
        for (Object serviceImpl : serviceImpls) {
            tryFinishWorkUnitOnOneConnetion(success, serviceImpl, finished, exceptions);
        }

        if (!exceptions.isEmpty()) {
            Exception ex = exceptions.get(0);
            throw new LameRuntimeException("error finishing work unit on connections, exception cnt: " + exceptions.size()
                    + ", first exception: " + ex, ex);
        }
    }

    protected Set<String> getServerSupportedLocaleNames() {
        return i18nSupporingService.getServerSupportedLocaleNames();
//        Set<String> serverSupportedLocaleNames;
//        if (serverSupportedLocaleNameRetriever != null) {
//            serverSupportedLocaleNames = serverSupportedLocaleNameRetriever.getServerSupportedLocaleNames();
//        } else {
//            serverSupportedLocaleNames = null;
//        }
//        return serverSupportedLocaleNames;
    }

//    @Override
//    public void setServiceTimeStampForResponse(long timeStamp) {
//        getThreadLocalResponse().setHeader("_Service_TimeStamp_For_Response_", Long.toString(timeStamp));
//    }
    @Override
    public void setServiceExtraResponseData(String data) {
        getThreadLocalResponse().setHeader(FoxyCommonConsts.SERVICE_EXTRA_RESPONSE_DATA_HEADER_NAME, data);
    }

    @Override
    public String getFoxyAppInstanceId() {
        try {
            HttpServletRequest threadLocalRequest = getThreadLocalRequest();
            if (threadLocalRequest == null) {
                return BaseUtils.generateRandomToken(20);
            } else {
                return threadLocalRequest.getHeader(FoxyCommonConsts.FOXY_APP_INSTANCE_ID_HEADER_NAME);
            }
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("exception in getFoxyAppInstanceId", ex);
            }
            return null;
        }
    }

    @Override
    public String getClientLoggedUserName() {
        try {
            return getThreadLocalRequest().getHeader(FoxyCommonConsts.FOXY_LOGGED_USER_NAME_HEADER_NAME);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("exception in getClientLoggedUserName", ex);
            }
            return null;
        }
    }

    @Override
    public long getServiceMethodInvokeCurrentMillis() {

        Long res = serviceMethodInvokeCurrentMillis.get();
        return res == null ? System.currentTimeMillis() : res;
    }

    @Override
    public String getRequestHeader(String name) {
        return getThreadLocalRequest().getHeader(name);
    }

    @Override
    public void setSessionAttribute(String name, Object val) {
        getThreadLocalSession().setAttribute(name, val);
    }

    @Override
    public String getSessionId() {
        return getThreadLocalSession().getId();
    }
}
