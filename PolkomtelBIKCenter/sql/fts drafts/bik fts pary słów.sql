insert into bik_fts_word_stemming (word_id, stemmed_word_id, origin_flag)
select word_id, (select min(isw.id) from bik_fts_stemmed_word isw where isw.word = sw.word) as stemmed_word_id, origin_flag
from bik_fts_stemmed_word sw
order by word;

delete sw 
from bik_fts_stemmed_word sw
where id <> (select min(isw.id) from bik_fts_stemmed_word isw where isw.word = sw.word)
;



select * from bik_fts_word
where word not like '%[_]%' and len(word) >= 5


select
  word_id, count(*) as cnt
from
  bik_fts_word_stemming
group by word_id
order by cnt desc



where word in ('system', 'informatyczny')


select
  w.word_id, sw.word + ' ' + sw2.word as word, case when ws.origin_flag < ws2.origin_flag then ws.origin_flag else ws2.origin_flag end as origin_flag,
    (sw.weight + sw2.weight) / 1.2 as weight
from
  (values (1, 2508, 58986), (2, 9527, 25371)) as w (word_id, w1_id, w2_id)
  inner join 
  (
    bik_fts_word_stemming ws inner join bik_fts_stemmed_word sw on ws.stemmed_word_id = sw.id
    cross join 
    bik_fts_word_stemming ws2 inner join bik_fts_stemmed_word sw2 on ws2.stemmed_word_id = sw2.id
  ) on ws.word_id = w.w1_id and ws2.word_id = w.w2_id
order by word;


--------------------------------------------------
--------------------------------------------------
--------------------------------------------------
--------------------------------------------------


declare @res table (id int not null, word varchar(800));

insert into @res (id, word) values
(10, 'pies kot'),
(20, 'zrobi� system'),
(30, 'maj� dzia�a�'),
(31, 'dzia�a� sprawnie'),
(32, 'sprawnie na'),
(33, 'pomi�dzy pozosta�ymi'),
(34, 'zapewniaj�ca dost�pem'),
(35, 'pozosta�ymi zapewniaj�ca'),
(36, 'zapewniaj�ca funkcjonowanie'),
(37, 'funkcjonowanie systemu'),
(38, 'szyna dzia�aj�ca')
;

select
  w.word_id, sw.word + ' ' + sw2.word as word, case when ws.origin_flag < ws2.origin_flag then ws.origin_flag else ws2.origin_flag end as origin_flag,
    (sw.weight + sw2.weight) / 1.8 as weight
from
  (select * from
    (
    select 
      id as word_id, 
      (select id from bik_fts_word w where w.word = substring(r.word, 1, charindex(' ', r.word) - 1)) as w1_id,
      (select id from bik_fts_word w where w.word = substring(r.word, charindex(' ', r.word) + 1, len(r.word))) as w2_id
    from @res r
    ) x
    where w1_id is not null and w2_id is not null
    ) w
  inner join 
  (
    bik_fts_word_stemming ws inner join bik_fts_stemmed_word sw on ws.stemmed_word_id = sw.id
    cross join 
    bik_fts_word_stemming ws2 inner join bik_fts_stemmed_word sw2 on ws2.stemmed_word_id = sw2.id
  ) on ws.word_id = w.w1_id and ws2.word_id = w.w2_id
order by word_id;



select * from bik_fts_word where word like '% %'
order by word

select * from bik_fts_stemmed_word where word like '% %'
order by word



select * from bik_fts_stemmed_word sw inner join bik_fts_word_stemming ws on sw.id = ws.stemmed_word_id
where word_id = 63288;
