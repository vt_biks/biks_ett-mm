/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.Date;
import java.util.List;
import pl.fovis.common.SimilarNodeBean;
import pl.fovis.common.fts.SimilarWordsWithCounts;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
@RemoteServiceRelativePath("boxiservice")
public interface FtsService extends RemoteService {

    public List<SimilarNodeBean> findSimilarObjects(int objId, double minRelevance, int maxResults);

    public List<SimilarNodeBean> findSimilarObjectsByLucene(int objId, double minRelevance, int maxResults);

    public Pair<List<SimilarNodeBean>, List<SimilarWordsWithCounts>> findSimilarObjectsAndWords(int objId, double minRelevance, int maxResults);

    public List<SimilarWordsWithCounts> getSimilarWordsWithCountsForObjPair(int objId1, int objId2);

    public void changeWordWeight(String word, double weight);

    public String getDateDiagInfo(Date d);
}
