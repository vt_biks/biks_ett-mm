exec sp_check_version '1.0.9';
go
----------------------------------------------------------------------
----------------------------------------------------------------------

alter table bik_joined_objs
 drop column dst_name
go

----------------------------------------------------------------------
----------------------------------------------------------------------
declare @name varchar(255);

select @name = sys.objects.name
from sys.objects join sys.objects o2 on o2.object_id = sys.objects.parent_object_id
where o2.name = 'bik_joined_objs'

exec ('alter table bik_joined_objs
	drop constraint ' + @name);
go

alter table bik_joined_objs
 drop column dst_kind_id
go
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------


exec sp_update_version '1.0.9', '1.0.10';
go

