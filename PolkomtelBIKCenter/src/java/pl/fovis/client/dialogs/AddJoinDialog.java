/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.IPaginatedItemsDisplayer;
import pl.fovis.client.bikwidgets.NodesToJoinGrid;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class AddJoinDialog extends AddNodesDialogBase {

    protected IJoinedObjSelectionManager<Integer> alreadyJoinedManager;
    protected int nodeId;

    public void buildAndShowDialog(int nodeKindId, List<TreeNodeBean> nodes, Integer nodeId, IJoinedObjSelectionManager<Integer> alreadyJoinedManager, IParametrizedContinuation<TreeNodeBean> saveCont) {
        this.nodeKindId = nodeKindId;
        this.allItems = nodes;
        this.nodeId = nodeId;
        this.saveCont = saveCont;
        this.alreadyJoinedManager = alreadyJoinedManager;
        super.buildAndShowDialog();
    }

    @Override
    protected IPaginatedItemsDisplayer<TreeNodeBean> createPaginatedItemsDisplayer() {
        paginatorFvs = new NodesToJoinGrid(alreadyJoinedManager);
//        return new NodesToJoinGrid();
        return paginatorFvs;
    }

    protected void doAction() {
        BIKClientSingletons.showInfo(I18n.zapisywaniePowiazanProszeCzekac.get() /* I18N:  */ + "...");

        BIKClientSingletons.getService().updateJoinedObjsForNode(nodeId, false/*mustHideLinkedNodes()*/,
                paginatorFvs.directOverride, paginatorFvs.subNodesOverride, new StandardAsyncCallback<Void>() {
                    public void onSuccess(Void result) {
                        BIKClientSingletons.showInfo(I18n.powiazaniaZapisaneOdswiezanie.get() /* I18N:  */);
                        alreadyJoinedManager.refreshJoinedNodes(nodeId);
                    }
                });
    }

    @Override
    protected String getRelationType() {
        return BIKConstants.ASSOCIATION;
    }

    protected String getTextWhenKindsListEmpty() {
        return I18n.brakObiektowDoPowiazania.get();
    }
    
    protected String getDialogCaption() {
        return I18n.dodajPowiazanie.get() /* I18N:  */;
    }

}
