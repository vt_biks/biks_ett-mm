/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author beata
 */
public enum JoinTypeRoles {
    Primary(I18n.glowny.get() /* I18N:  */),
    PrimaryOldDelete(I18n.usunW.get() /* I18N:  */),
    //ww->prim2: nie podobają mi się spacje na końcu, a w szczególności jak 
    // są dwie spacje na końcu...
    PrimaryOldReplace(I18n.zamienNaPomocniczeW.get() /* I18N:  */),
    PrimaryOldNotReplace(I18n.pozostawAktualneUstawienWObiekta.get() /* I18N:  */),
    SelectedPrimaryOldReplace(I18n.zamienNaPomocnicze.get() /* I18N:  */),
    Auxiliary(I18n.pomocniczy.get() /* I18N:  */);//,
    //ww->prim2: tej wartości, albo Primary nie powinno chyba być w tym Enumie?
    // sprawdziłem USAGES i tylko raz caption jest potrzebny z tej wartości
    // nic więcej chyba - więc chyba może to nie być w enumie?
//    PrimaryAct("Główny, a aktualnie ustawione na główne z tą rolą i w");
    public String caption;

    JoinTypeRoles(String caption) {
        this.caption = caption;
    }
}
