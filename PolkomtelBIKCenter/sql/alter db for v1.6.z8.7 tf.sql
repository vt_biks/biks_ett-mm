﻿exec sp_check_version '1.6.z8.7';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

if not exists (select * from bik_app_prop where name = 'typeMapping_Oracle')
begin
	insert into bik_app_prop (name, val) values ('typeMapping_Oracle', '
	VARCHAR2=varchar(%{prec}%)
	NVARCHAR2=nvarchar(%{prec}%)
	NUMBER=<f:if cond="prec==0 || scale < 0">float</f:if><f:else>numeric(%{prec}%, %{scale}%)</f:else>
	DATE=date
	CHAR=char(%{prec}%)
	NCHAR=nchar(%{prec}%)
	LONG=VARCHAR(MAX)
	RAW=VARBINARY(%{prec}%)
	BLOB=VARBINARY(MAX)
	CLOB=VARCHAR(MAX)
	FLOAT=FLOAT
	REAL=FLOAT
	INT=NUMERIC(38)
	BINARY_FLOAT=FLOAT
	BINARY_DOUBLE=FLOAT(53)
	BFILE=VARBINARY(MAX)
	DATE=DATETIME
	INTERVAL=DATETIME
	LONG\u0020RAW=IMAGE
	ROWID=CHAR(18)
	TIMESTAMP=DATETIME
	UROWID=CHAR(18)
	');
end;
go

if not exists (select * from bik_app_prop where name = 'typeMapping_PostgreSQL')
begin
	insert into bik_app_prop (name, val) values ('typeMapping_PostgreSQL', '
	BPCHAR=char(%{prec}%)
	BOOL=tinyint
	TEXT=text
	VARCHAR=VARCHAR(%{prec > 8000 ? "MAX" : ("" + prec)}%)
	TIMESTAMP=datetime
	DATE=date
	TIME=time
	INT2=smallint
	INT4=int
	INT8=bigint
	NUMERIC=%{prec == 0 || prec > 38 ? "float" : "decimal(" + prec + "," + scale + ")"}%
	');
end;
go

if not exists (select * from bik_app_prop where name = 'typeMapping_MSSQL')
begin
	insert into bik_app_prop (name, val) values ('typeMapping_MSSQL', '*=%{typeName}%
	DECIMAL=decimal(%{prec}%, %{scale}%)
	VARCHAR=varchar(max)
	NVARCHAR=varchar(max)
	NCHAR=varchar(max)
	CHAR=varchar(max)');
end;
go

alter table bik_dqm_request_error alter column value varchar(max) null;
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

exec sp_update_version '1.6.z8.7', '1.6.z9';
go
