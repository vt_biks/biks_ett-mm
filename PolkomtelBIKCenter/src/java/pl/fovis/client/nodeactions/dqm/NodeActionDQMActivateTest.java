/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.dqm;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author tflorczak
 */
public class NodeActionDQMActivateTest extends NodeActionDQMBase {

    @Override
    public boolean isEnabledByDesign() {
        return node != null && isInDQMAdminMode() && isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_DQM_TEST_INACTIVE);
    }

    @Override
    public String getCaption() {
        return I18n.aktywujTest.get();
    }

    @Override
    public void execute() {
        final int nodeId = node.linkedNodeId != null ? node.linkedNodeId : node.id;
        BIKClientSingletons.getDQMService().changeDQMTestActivity(nodeId, false, new StandardAsyncCallback<Void>("Error in changeDQMTestActivity") {

            @Override
            public void onSuccess(Void result) {
                BIKClientSingletons.showInfo(I18n.aktywwowanoTest.get());
                refreshRedisplayDataAndSelectRecord(node.id);
            }
        });
    }

}
