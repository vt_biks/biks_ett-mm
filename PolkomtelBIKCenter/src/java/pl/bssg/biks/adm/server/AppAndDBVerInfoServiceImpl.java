/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.server;

import commonlib.GlobalThreadStopWatch;
import commonlib.LameUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import pl.bssg.biks.adm.client.AppAndDBVerInfoService;
import pl.bssg.biks.adm.common.AppInfoBean;
import pl.bssg.biks.adm.common.AuthenticationDataBean;
import pl.bssg.biks.adm.common.DbLoginBean;
import pl.bssg.biks.adm.common.DbauLogBean;
import pl.bssg.biks.adm.common.DbauProcessBean;
import pl.bssg.biks.adm.common.InstanceInfoBean;
import pl.bssg.biks.adm.common.MultiInfoBean;
import pl.bssg.biks.adm.common.UnauthorizedServiceMethodAccessException;
import pl.bssg.gwtservlet.AdhocSqlsName;
import pl.bssg.gwtservlet.IServiceImplWithLocaleNamesOverride;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.mltx.MltxDatabaseBean;
import pl.fovis.common.mltx.MultixConstants;
import pl.fovis.server.BIKCenterConfigObj;
import pl.fovis.server.BIKSServiceBase;
import simplelib.FieldNameConversion;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import pl.trzy0.foxy.serverlogic.db.autoupdater.MSSQLDBAutoUpgrader;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.StackTraceUtil;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
@AdhocSqlsName("adhocSqlsDbAutoUpgrader.xml")
public class AppAndDBVerInfoServiceImpl extends BIKSServiceBase implements AppAndDBVerInfoService,
        IServiceImplWithLocaleNamesOverride {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected static final Set<String> supportedLocaleNames = Collections.singleton("en");
    public static final String BIKS_VER_PROP_NAME = "bik_ver";
    public static final String MLTX_VER_PROP_NAME = "multiXDbVer";
    protected static final Set<String> BIKS_APP_PROP_NAMES = BaseUtils.paramsAsSet(BIKS_VER_PROP_NAME, "load_user_login", "load_user_pwd");
    protected static final Set<String> MLTX_APP_PROP_NAMES = BaseUtils.paramsAsSet(MLTX_VER_PROP_NAME);
    protected String accAppLogin;
    protected String accAppPwd;
    protected BIKCenterConfigObj configBean;

    @Override
    protected void initWithConfig(BIKCenterConfigObj configBean) {
        super.initWithConfig(configBean);
        accAppLogin = configBean.connConfig.user;
        accAppPwd = configBean.connConfig.password;
        this.configBean = configBean;
    }

    protected Pair<InstanceInfoBean, List<DbLoginBean>> getInfoForInstance(String dbInstanceName,
            boolean isBIKSInstance) {

        final String dbInstanceNameQ = MssqlConnection.quoteIdent(dbInstanceName);

        if (GlobalThreadStopWatch.isProcInfoLoggingEnabled()) {
            GlobalThreadStopWatch.startProcess("getInfoForInstance: " + dbInstanceName);
        }
        try {

//        boolean isBIKSInstance = isBIKSInstance(dbInstanceName);
            String tableNamePrfx = isBIKSInstance ? "bik" : "mltx";
//        String appCode = isBIKSInstance ? "BIKS" : "MultiX";
            Set<String> appPropNames = isBIKSInstance ? BIKS_APP_PROP_NAMES : MLTX_APP_PROP_NAMES;
            String dbVerPropName = isBIKSInstance ? BIKS_VER_PROP_NAME : MLTX_VER_PROP_NAME;

            //ww:scdb: getAdhocDao().setCurrentDbForAdhoc(dbInstanceName);
            if (GlobalThreadStopWatch.isProcInfoLoggingEnabled()) {
                GlobalThreadStopWatch.startProcessStage("after setCurrentDatabase, dbauCreateTables");
            }
            execNamedCommand("dbauCreateTables", dbInstanceNameQ);

            if (GlobalThreadStopWatch.isProcInfoLoggingEnabled()) {
                GlobalThreadStopWatch.startProcessStage("dbauGetAppProps");
            }

            List<Map<String, Object>> rows = execNamedQueryCFN("dbauGetAppProps",
                    FieldNameConversion.ToLowerCase, dbInstanceNameQ, tableNamePrfx, appPropNames);

            if (GlobalThreadStopWatch.isProcInfoLoggingEnabled()) {
                GlobalThreadStopWatch.startProcessStage("collecting results");
            }

            @SuppressWarnings("unchecked")
            Map<String, Object> map = BaseUtils.projectCollOfMapToMapVals((Iterable) rows, "name", "val");

            InstanceInfoBean instance = new InstanceInfoBean();
//        res.appCode = appCode;
//        res.appVer = getAppVer(isBIKSInstance);
            instance.dbVer = (String) map.get(dbVerPropName);
            instance.dbInstanceName = dbInstanceName;
            instance.lastProcessInfo = getLastProcessInfo(dbInstanceName);

            List<DbLoginBean> logins = new ArrayList<DbLoginBean>();

            logins.add(new DbLoginBean("cfg", accAppLogin, accAppPwd));

            if (isBIKSInstance) {
                String load_user_login = (String) map.get("load_user_login");
                String load_user_pwd = (String) map.get("load_user_pwd");

                if (!BaseUtils.isStrEmptyOrWhiteSpace(load_user_login)) {
                    logins.add(new DbLoginBean("load", load_user_login, load_user_pwd));
                }
            }

            return new Pair<InstanceInfoBean, List<DbLoginBean>>(instance, logins);
        } finally {
            if (GlobalThreadStopWatch.isProcInfoLoggingEnabled()) {
                GlobalThreadStopWatch.endProcess();
            }
        }
    }

    protected void setMsgsMaybe(MultiInfoBean mib,
            InstanceInfoBean instance, Integer minLogId) {
        if (BaseUtils.safeEquals(instance.dbInstanceName, mib.msgsInstanceName)
                && instance.lastProcessInfo != null) {
            mib.msgs = getLogMsgsInner(instance.dbInstanceName, instance.lastProcessInfo.id, minLogId);
        }
    }

    protected void collectDbLogins(Map<String, Pair<String, String>> loginNameToLoginData,
            Map<Pair<String, String>, String> loginDataToLoginName,
            List<DbLoginBean> logins) {

        for (DbLoginBean dlb : logins) {
            Pair<String, String> loginData = new Pair<String, String>(dlb.login, dlb.pwd);

            String usedName = loginDataToLoginName.get(loginData);
            if (usedName != null) {
                continue;
            }

            int num = 1;
            String loginName = dlb.name;
            while (true) {
                Pair<String, String> usedLoginData = loginNameToLoginData.get(loginName);
                if (usedLoginData == null) {
                    loginDataToLoginName.put(loginData, loginName);
                    loginNameToLoginData.put(loginName, loginData);
                    break;
                }
                num++;
                loginName = dlb.name + num;
            }
        }
    }

    protected MultiInfoBean getInfoInner(String optDBInstanceNameForLogs,
            Integer minLogId, Map<String, String> sqlFilesDirsToCheck) {

        GlobalThreadStopWatch.startProcess("getInfoInner");
        try {
            MultiInfoBean res = new MultiInfoBean();
            res.apps = new LinkedList<AppInfoBean>();
            res.msgsInstanceName = optDBInstanceNameForLogs;

            AppInfoBean aib1 = new AppInfoBean();
            res.apps.add(aib1);
            boolean isMultiXMode = isMultiXMode();
            boolean isBIKSInstance = !isMultiXMode;

            Map<String, Pair<String, String>> loginNameToLoginData = new LinkedHashMap<String, Pair<String, String>>();
            Map<Pair<String, String>, String> loginDataToLoginName = new LinkedHashMap<Pair<String, String>, String>();

            aib1.appCode = isMultiXMode ? "MultiX" : "BIKS";
            aib1.appVer = getAppVer(isBIKSInstance);
            aib1.instances = new LinkedList<InstanceInfoBean>();
            final Pair<InstanceInfoBean, List<DbLoginBean>> instancePair = getInfoForInstance(defaultDatabaseName, isBIKSInstance);

            GlobalThreadStopWatch.startProcessStage("got info for main instance");

            aib1.instances.add(instancePair.v1);
            collectDbLogins(loginNameToLoginData, loginDataToLoginName, instancePair.v2);
            setMsgsMaybe(res, aib1.instances.get(0), minLogId);

            if (isMultiXMode()) {
                GlobalThreadStopWatch.startProcessStage("aditional instances");
                AppInfoBean aib2 = new AppInfoBean();
                res.apps.add(aib2);
                aib2.instances = new LinkedList<InstanceInfoBean>();
                aib2.appCode = "BIKS";
                aib2.appVer = getAppVer(true);

                List<MltxDatabaseBean> mdbs = createBeansFromNamedQry("mltxGetDatabases", MltxDatabaseBean.class);

                GlobalThreadStopWatch.startProcessStage("got databases, looping");

                for (MltxDatabaseBean mdb : mdbs) {
                    final Pair<InstanceInfoBean, List<DbLoginBean>> auxInstancePair = getInfoForInstance(mdb.databaseName, true);
                    InstanceInfoBean instance = auxInstancePair.v1;
                    collectDbLogins(loginNameToLoginData, loginDataToLoginName, auxInstancePair.v2);
                    aib2.instances.add(instance);
                    setMsgsMaybe(res, instance, minLogId);
                }
            }

            GlobalThreadStopWatch.startProcessStage("final props");
            res.uploadFilesDir = configBean.dirForUpload;
            res.dbLogins = loginNameToLoginData;

            if (sqlFilesDirsToCheck != null) {
                res.availableScriptsForDbVers = checkSqlFilesDirInner(res, sqlFilesDirsToCheck);
                res.checkedSqlFilesDirs = sqlFilesDirsToCheck;
            }

            return res;

        } finally {
            GlobalThreadStopWatch.endProcess();
        }
    }
    int addRunCnt = 0;

    public MultiInfoBean getInfo(AuthenticationDataBean adb,
            String optDBInstanceNameForLogs, Integer minLogId, Map<String, String> sqlFilesDirsToCheck) {
        checkAuthenticationData(adb);
        GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled = true;
        GlobalThreadStopWatch.startProcess("getInfo", true);
        try {
//            GlobalThreadStopWatch.startProcessStage("initial testing");
////            System.out.println("INITIAL TESTING...");
            if (addRunCnt > 0) {
                pl.trzy0.foxy.db.setdbperformance.Tester.performTestingSingle();
                addRunCnt--;
            }
////            System.out.println("INITIAL TESTING DONE...");

//            MultiInfoBean mmm = getInfoInner(optDBInstanceNameForLogs, minLogId, sqlFilesDirsToCheck);
            GlobalThreadStopWatch.startProcessStage("getInfoInner");
            return getInfoInner(optDBInstanceNameForLogs, minLogId, sqlFilesDirsToCheck);
        } finally {
//            GlobalThreadStopWatch.startProcessStage("final testing");
//            pl.trzy0.foxy.db.setdbperformance.Tester.performTestingSingle();
            GlobalThreadStopWatch.endProcess();
        }
    }

    protected String getDbVer(String dbInstanceName, boolean isBIKSInstance) {
//        System.out.println("before getDbVer");
        final String dbInstanceNameQ = MssqlConnection.quoteIdent(dbInstanceName);
        String dbVer = execNamedQuerySingleVal("dbauGetAppProps", false, "val",
                dbInstanceNameQ, isBIKSInstance ? "bik" : "mltx", isBIKSInstance ? BIKS_VER_PROP_NAME : MLTX_VER_PROP_NAME);
//        System.out.println("after getDBVer res=" + dbVer);
        return dbVer;
    }

    protected void checkAuthenticationData(AuthenticationDataBean adb) throws UnauthorizedServiceMethodAccessException {
        if (!performLogin(adb)) {
            throw new UnauthorizedServiceMethodAccessException("bad auth-data");
        }
    }

    protected boolean performLogin(AuthenticationDataBean adb) {
        return adb != null && BaseUtils.safeEquals(adb.login, "au") && BaseUtils.safeEquals(adb.pass, "au");
    }

    public int runDbUpgradeProcess(AuthenticationDataBean adb, final String dbInstanceName, String dbLogin, String dbPass, String sqlFilesDir)
            throws LameRuntimeException {

        final String dbInstanceNameQ = MssqlConnection.quoteIdent(dbInstanceName);

        checkAuthenticationData(adb);
        final boolean isBIKSInstance = isBIKSInstance(dbInstanceName);
        final String sqlFilesDirFin = sqlFilesDir.replace("\\", "/");
        if (!new File(sqlFilesDirFin).isDirectory()) {
            throw new LameRuntimeException("no such dir: \"" + sqlFilesDirFin + "\"");
        }

        MssqlConnectionConfig connConfig = configBean.connConfig;

        MssqlConnectionConfig cfg = new MssqlConnectionConfig(
                connConfig.server, connConfig.instance, dbInstanceName, dbLogin, dbPass);
        final MssqlConnection conn = new MssqlConnection(cfg);

        final LinkedBlockingQueue<Integer> lbq = new LinkedBlockingQueue<Integer>();

//        finishWorkUnit(true);
        Runnable r = new Runnable() {
            protected int procId;
            protected MSSQLDBAutoUpgrader dbau;
//                    new MSSQLDBAutoUpgrader(getAppVer(true), sqlFilesDirFin, conn) {
//                @Override
//                protected void startAlterScriptForDbVer(String dbVer) {
//                    execNamedCommand("dbauUpdateProcess", procId, 0);
//                }
//
//                @Override
//                protected void finishedAlterScriptForDbVer(String newDbVer) {
//                    updateProcessFinalDbVer(procId, newDbVer);
//                }
//
//                @Override
//                protected String getCurrentDBVer() {
//                    return getDbVer(isBIKSInstance);
//                }
//
//                @Override
//                protected void logExecution(String msg) {
//                    super.logExecution(msg);
//                    execNamedCommand("dbauLogMsg", procId, msg);
//                }
//            };

            public void run() {
                try {
                    getAdhocDao().execInAutoCommitMode(new IContinuation() {
                        public void doIt() {

                            //ww:scdb: getAdhocDao().setCurrentDbForAdhoc(dbInstanceName);
                            execNamedCommand("dbauCreateTables", dbInstanceNameQ);

                            procId = execNamedQuerySingleValAsInteger("dbauStartProcess",
                                    false, null, dbInstanceNameQ, getDbVer(dbInstanceName, isBIKSInstance), getAppVer(isBIKSInstance));

                            dbau = new BIKSDBAutoUpgrader(procId,
                                    new IAppAndDBVerInfoServiceForUpgrader() {
                                        @Override
                                        public int execNamedCommand(String name, Object... args) {

                                            Object[] argsFix = new Object[args.length + 1];
                                            argsFix[0] = dbInstanceNameQ;
                                            System.arraycopy(args, 0, argsFix, 1, args.length);

                                            return AppAndDBVerInfoServiceImpl.this.execNamedCommand(name, argsFix);
                                        }

                                        @Override
                                        public void updateProcessFinalDbVer(int procId, String dbVer) {
                                            AppAndDBVerInfoServiceImpl.this.updateProcessFinalDbVer(dbInstanceName, procId, dbVer);
                                        }

                                        @Override
                                        public String getDbVer() {
                                            return AppAndDBVerInfoServiceImpl.this.getDbVer(dbInstanceName, isBIKSInstance);
                                        }
                                    },
                                    getAppVer(isBIKSInstance), sqlFilesDirFin, conn);

                            lbq.add(procId);

                            int status;

                            try {
                                dbau.execute();
//                            System.out.println("after dbau.exec");
                                status = 1;
                            } catch (Exception ex) {
                                if (logger.isErrorEnabled()) {
                                    logger.error("error in dbau.execute()", ex);
                                }
                                status = -1;
                                String errorMsg = StackTraceUtil.getCustomStackTrace(ex);
                                execNamedCommand("dbauLogMsg", dbInstanceNameQ, procId, "Exception: " + ex + "\n" + errorMsg);
                            }

//                        System.out.println("before dbauUpdateProcess");
                            execNamedCommand("dbauUpdateProcess", dbInstanceNameQ, procId, status);
//                        System.out.println("after dbauUpdateProcess");
                        }
                    });
                } catch (Exception ex) {
                    logger.error("error in thread for upgrade", ex);
                    lbq.add(null);
                    throw new LameRuntimeException(ex);
                } finally {
                    getAdhocDao().finishWorkUnit(true);
                }
            }
        };

        new Thread(r).start();

        int procId;

        try {
            procId = lbq.take();
        } catch (Exception ex) {
            throw new LameRuntimeException("error in take", ex);
        }

        return procId;
    }

    protected String getAppVer(boolean isBIKSInstance) {
        return isBIKSInstance ? BIKConstants.APP_VERSION_MAJOR : MultixConstants.MLTX_APP_VERSION_MAJOR;
    }

    protected List<DbauLogBean> getLogMsgsInner(String dbInstanceName, int procId, Integer minLogId) {

        final String dbInstanceNameQ = MssqlConnection.quoteIdent(dbInstanceName);

        GlobalThreadStopWatch.startProcess("getLogMsgsInner");
        try {
            List<DbauLogBean> res = createBeansFromNamedQry("dbauGetLogMsgs", DbauLogBean.class,
                    dbInstanceNameQ, procId, minLogId);
            return res;
        } finally {
            GlobalThreadStopWatch.endProcess();
        }
    }

//    public List<DbauLogBean> getLogMsgs(AuthenticationDataBean adb, int procId, Integer minLogId) {
//        checkAuthenticationData(adb);
//        return getLogMsgsInner(procId, minLogId);
//    }
    public void keepSessionAlive() {
        // no-op
    }

    protected void updateProcessFinalDbVer(String dbInstanceName, int procId, String dbVer) {
        final String dbInstanceNameQ = MssqlConnection.quoteIdent(dbInstanceName);
        execNamedCommand("dbauUpdateProcessFinalDbVer", dbInstanceNameQ, procId, dbVer);
    }

    protected DbauProcessBean getLastProcessInfo(String dbInstanceName) {
        final String dbInstanceNameQ = MssqlConnection.quoteIdent(dbInstanceName);

        return createBeanFromNamedQry("dbauGetLastProcessInfo", DbauProcessBean.class, true, dbInstanceNameQ);
    }

    protected boolean isSqlFilesDirProperForDbVer(final String dir, final String dbVer) {

        String fileName = null;
        try {
            if (dir == null || !new File(dir).isDirectory()) {
                return false;
            }

            //ww: zaślepka, tylko w celu wywołania logiki z metody
            // GenericDBAutoUpgrader.findProperAlterDBFile
            MSSQLDBAutoUpgrader u = new MSSQLDBAutoUpgrader(null, dir, null) {
                @Override
                protected String getCurrentDBVer() {
                    return dbVer;
                }

                @Override
                protected void logExecution(String msg) {
                    // no-op
                }
            };

            fileName = u.findProperAlterDBFile(dbVer);
        } catch (Exception ex) {
            //ww: no-op
        }

        return fileName != null;
    }

//    public Map<String, Set<String>> checkSqlFilesDir(Map<String, String> dirs) {
//        //ww: zaślepkowe wywołanie, aby mieć MultiInfoBean
//        MultiInfoBean m = getInfoInner(null, null, null);
//
//        return checkSqlFilesDirInner(m, dirs);
//    }
    protected Map<String, Set<String>> checkSqlFilesDirInner(MultiInfoBean m, Map<String, String> dirs) {

        GlobalThreadStopWatch.startProcess("checkSqlFilesDirInner");

        try {

            Map<String, AppInfoBean> apps = new HashMap<String, AppInfoBean>();
            final Map<String, String> instanceToDbVerOf = new HashMap<String, String>();
            final Map<String, String> instanceToAppCode = new HashMap<String, String>();

            Map<String, Set<String>> res = new HashMap<String, Set<String>>();

            for (AppInfoBean app : m.apps) {
                final String appCode = app.appCode;
                final String dirName = dirs.get(appCode);
                if (dirName == null || !new File(dirName).isDirectory()) {
                    continue;
                }

                apps.put(appCode, app);
                res.put(appCode, new LinkedHashSet<String>());

                for (InstanceInfoBean instance : app.instances) {
                    final String dbInstanceName = instance.dbInstanceName;
                    instanceToDbVerOf.put(dbInstanceName, instance.dbVer);
                    instanceToAppCode.put(dbInstanceName, appCode);
                }
            }

            Set<Pair<String, String>> okPairs = new HashSet<Pair<String, String>>();
            Set<Pair<String, String>> badPairs = new HashSet<Pair<String, String>>();

            for (String instanceName : instanceToDbVerOf.keySet()) {

                final String instanceNameFinal = instanceName;
                String appCode = instanceToAppCode.get(instanceName);
                final String dir = dirs.get(appCode);

//            if (dir == null) {
//                continue;
//            }
//
//            final String dbVer = instanceToDbVerOf.get(instanceNameFinal);
//
//            MSSQLDBAutoUpgrader u = new MSSQLDBAutoUpgrader(null, dir, null) {
//                @Override
//                protected String getCurrentDBVer() {
//                    return dbVer;
//                }
//
//                @Override
//                protected void logExecution(String msg) {
//                    // no-op
//                }
//            };
//
//            String fileName = u.findProperAlterDBFile(dbVer);
                final String dbVer = instanceToDbVerOf.get(instanceNameFinal);

                Pair<String, String> p = new Pair<String, String>(dir, dbVer);

                boolean isSqlFilesFirProperForDbVer;

                if (okPairs.contains(p)) {
                    isSqlFilesFirProperForDbVer = true;
                } else if (badPairs.contains(p)) {
                    isSqlFilesFirProperForDbVer = false;
                } else {
                    isSqlFilesFirProperForDbVer = isSqlFilesDirProperForDbVer(dir, dbVer);
                    if (isSqlFilesFirProperForDbVer) {
                        okPairs.add(p);
                    } else {
                        badPairs.add(p);
                    }
                }

                if (isSqlFilesFirProperForDbVer) {
                    BaseUtils.addToCollectingMapWithLinkedSet(res, appCode, dbVer);
                }
            }

            return res;
        } finally {
            GlobalThreadStopWatch.endProcess();
        }
    }

    public Set<String> getSupportedLocaleNames() {
        return supportedLocaleNames;
    }
}
