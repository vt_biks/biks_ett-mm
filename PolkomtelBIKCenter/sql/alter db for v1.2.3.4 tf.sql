﻿exec sp_check_version '1.2.3.4';
go


create index idx_bik_sapbo_universe_object_node_id on bik_sapbo_universe_object (node_id)
go

create index idx_bik_sapbo_universe_connection_node_id on bik_sapbo_universe_connection (node_id)
go

create unique index idx_bik_active_directory_s_a_m_account_name on bik_active_directory (s_a_m_account_name)
go


exec sp_update_version '1.2.3.4', '1.2.3.5';
go