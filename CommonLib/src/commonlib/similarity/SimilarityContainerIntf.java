/*
 * SimilarityContainerIntf.java
 *
 * Created on 10 wrzesie� 2007, 18:39
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib.similarity;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public interface SimilarityContainerIntf {
    Map<String, Double> findSimilar(String word);
    void add(String word);
}
