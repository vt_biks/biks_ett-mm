/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import pl.fovis.client.UniTreeNode;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.foxygwtcommons.ITreeBuilder;

/**
 *
 * @author mgraczkowski
 */
public class UniTreeBuilder extends TreeNodeBroker implements ITreeBuilder<TreeNodeBean, UniTreeNode, Integer> {

    private int treeNodeCopyId = -1;
    protected List<TreeNodeBean> rootDataNodes = new ArrayList<TreeNodeBean>();
    protected Map<Integer, UniTreeNode> uniNodesMap = new LinkedHashMap<Integer, UniTreeNode>();

    public UniTreeNode createNode(TreeNodeBean n, UniTreeNode p) {
        int id = getNodeId(n);
        UniTreeNode res = new UniTreeNode(n);
        if (p != null) {
            p.addChild(res);
        } else {
            rootDataNodes.add(n);
        }
        uniNodesMap.put(id, res);
        return res;
    }

    public void copyLinkedSubtrees(int treeId) {
        for (TreeNodeBean tnb : rootDataNodes) {
            if (tnb.treeId == treeId) {
                copySubtreesLinkedToTree(treeId, uniNodesMap.get(tnb.id));
            }
        }
        removeOtherTrees(treeId);
    }

    private void copySubtreesLinkedToTree(int treeId, UniTreeNode utn) {
        if (utn.children != null) {
            for (Entry e : utn.children.entrySet()) {
                UniTreeNode child = (UniTreeNode) e.getValue();
                if (child.tnb.treeId == treeId) {
                    copySubtreesLinkedToTree(treeId, child);
                }
            }
        }
        if (utn.tnb.linkedNodeId != null) {
            copySubtreeLinkedToTree(utn, uniNodesMap.get(utn.tnb.linkedNodeId));
        }
    }

    private void copySubtreeLinkedToTree(UniTreeNode utn, UniTreeNode sub) {
        if (utn.children == null && sub.children != null) {
            utn.children = new LinkedHashMap<Integer, UniTreeNode>();
            for (Entry e : sub.children.entrySet()) {
                UniTreeNode child = (UniTreeNode) e.getValue();
                TreeNodeBean tnbCopy = new TreeNodeBean(
                        treeNodeCopyId--,
                        utn.tnb.id,
                        child.tnb.nodeKindId,
                        child.tnb.descr,
                        child.tnb.nodeKindCaption,
                        child.tnb.nodeKindCode,
                        child.tnb.name,
                        child.tnb.objId,
                        child.tnb.treeId,
                        child.tnb.id, //child.tnb.linkedNodeId,
                        child.tnb.branchIds,
                        child.tnb.treeCode,
                        child.tnb.treeName,
                        child.tnb.treeKind);

//                tnbCopy.id = treeNodeCopyId--;
//                tnbCopy.parentNodeId = utn.tnb.id;
//
//                tnbCopy.linkedNodeId = child.tnb.id; //null;
//                tnbCopy.name = child.tnb.name;
//                tnbCopy.nodeKindCaption = child.tnb.nodeKindCaption;
//                tnbCopy.nodeKindId = child.tnb.nodeKindId;
//                tnbCopy.objId = child.tnb.objId;
//                tnbCopy.treeId = child.tnb.treeId;

                UniTreeNode subCopy = new UniTreeNode(tnbCopy);
                copySubtreeLinkedToTree(subCopy, child);
                utn.children.put(subCopy.tnb.id, subCopy);
                uniNodesMap.put(subCopy.tnb.id, subCopy);
            }
        }
    }

    private void removeOtherTrees(int treeId) {
        Iterator<TreeNodeBean> iter = rootDataNodes.iterator();
        while (iter.hasNext()) {
            TreeNodeBean rootNode = iter.next();
            if (rootNode.treeId != treeId) {
                removeSubTree(rootNode.id);
                iter.remove();
            }
        }
    }

    protected void removeSubTree(int nodeId) {
        UniTreeNode utn = uniNodesMap.get(nodeId);
        if (utn.children != null) {
            for (Entry e : utn.children.entrySet()) {
                removeSubTree(((UniTreeNode) e.getValue()).tnb.id);
            }
        }
        uniNodesMap.remove(nodeId);
    }

    public Map<Integer, UniTreeNode> getUniNodesMap() {
        return uniNodesMap;
    }

    public List<TreeNodeBean> getRootDataNodes() {
        return rootDataNodes;
    }
}
