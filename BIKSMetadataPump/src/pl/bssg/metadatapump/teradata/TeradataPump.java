/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.teradata;

import commonlib.LameUtils;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.DriverNameConstants;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.ConnectionUtils;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class TeradataPump extends MetadataPumpBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected static final int STOP_SHOW_TABLE_CALL_AFTER_ERROR_CNT = 0; //20;
    protected static final Map<String, String> columnTypeMap = new HashMap<String, String>() {
        {
            put("I", "INTEGER");
            put("I1", "BYTEINT");
            put("I2", "SMALLINT");
            put("I8", "BYTEINTEGER");
            put("CV", "VARCHAR");
            put("CF", "CHAR");
            put("BF", "BYTE");
            put("BV", "VARBYTE");
            put("F", "FLOAT");
            put("DA", "DATE");
            put("D", "DECIMAL"); //prec
            put("TS", "TIMESTAMP");
            put("AT", "DATETIME");
            put("GF", "GRAPHIC");
            put("GV", "VARGRAPHIC");
            put("BO", "BLOB");
            put("CO", "CLOB");
            put("UT", "UDT TYPE");
            put("DH", "INTERVAL DAY TO HOUR");
            put("DM", "INTERVAL DAY TO MINUTE");
            put("DS", "INTERVAL DAY TO SECOND");
            put("DY", "INTERVAL DAY");
            put("HM", "INTERVAL HOUR TO MINUTE");
            put("HR", "INTERVAL HOUR");
            put("HS", "INTERVAL HOUR TO SECOND");
            put("MI", "INTERVAL MINUTE");
            put("MO", "INTERVAL MONTH");
            put("MS", "INTERVAL MINUTE TO SECOND");
            put("SC", "INTERVAL SECOND");
            put("SZ", "TIMESTAMP WITH TIME ZONE");
            put("TZ", "TIME WITH TIME ZONE");
            put("YM", "INTERVAL YEAR TO MONTH");
            put("YR", "INTERVAL YEAR");
        }
    };
    protected static final Map<String, String> tableIndexTypeMap = new HashMap<String, String>() {
        {
            put("P", "Nonpartitioned Primary");
            put("Q", "Partitioned Primary");
            put("S", "Secondary");
            put("J", "Join index");
            put("N", "Hash index");
            put("K", "Primary key");
            put("U", "Unique constraint");
            put("V", "Value ordered secondary");
            put("H", "Hash ordered ALL covering secondary");
            put("O", "Valued ordered ALL covering secondary");
            put("I", "Ordering column of a composite secondary index");
            put("M", "Multi-column statistics");
            put("D", "Derived column partition statistics");
            put("1", "Field1 column of a join or hash index");
            put("2", "Field2 column of a join or hash index");

        }
    };
    protected static final Map<String, String> tableIndexUniqueMap = new HashMap<String, String>() {
        {
            put("Y", "unique");
            put("N", "non unique");
        }
    };
    protected static final Map<String, String> tableKindMap = new HashMap<String, String>() {
        {
            put("T", "TeradataTable");
            put("V", "TeradataView");
            put("P", "TeradataProcedure");
            put("E", "TeradataProcedure");

        }
    };
    protected static final Map<String, String> tableKindMapForCreateSQL = new HashMap<String, String>() {
        {
            put("T", "TABLE");
            put("V", "VIEW");
            put("P", "PROCEDURE");
            put("E", "PROCEDURE");

        }
    };
    protected static final Set<String> typesWithLength = new HashSet<String>() {
        {
            addAll(BaseUtils.splitUniqueBySep("CV,CF,BV,BF,GV,GF", ","));
        }
    };
    protected Map<String, String> indexNameAndTypeMap = new HashMap<String, String>();
    protected Set<String> schemasMap = new HashSet<String>();
    protected Set<String> tablesMap = new HashSet<String>();
    protected Set<String> suspiciousNames = new HashSet<String>();
    protected List<TeradataBean> teradataList = new LinkedList<TeradataBean>();
    protected Connection tc = null;
    protected int insertedLogId = 0;
    protected boolean fakeModePump = false;

    public static INamedSqlsDAO<Object> createAdhocDao(IBeanConnectionEx<Object> mc) {
        return FoxyAppUtils.createNamedSqlsDAOFromJar(mc, ADHOC_SQLS_PATH);
    }

    protected boolean isWarnLoggingInFakeModeEnabled() {
        return fakeModePump && logger.isWarnEnabled();
    }

    protected boolean isNameSuspiciousForFakeMode(String name) {
        if (!fakeModePump) {
            return false;
        }
        if (name == null) {
            return true;
        }
        name = name.trim();
        return suspiciousNames.contains(name);
    }

    protected String getTeradataConfigProp(String propName) {
        return getTeradataConfigPropEx(propName, false, null);
    }

    protected String getTeradataConfigProp(String propName, String defVal) {
        return getTeradataConfigPropEx(propName, true, defVal);
    }

    protected String getTeradataConfigPropEx(String propName, boolean allowNoResult, String defVal) {
        String res = execNamedQuerySingleVal("getConfigValue", allowNoResult, "value", propName);
        return BaseUtils.nullToDef(res, defVal);
    }

//    public String getPumpGroup() {
//        return PumpConstants.PUMP_GROUP_TERADATA;
//    }
    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_TERADATA;
    }

    @Override
    protected Integer insertMainLog() {
        insertedLogId = insertStartLog(getSourceName(), null);
        return insertedLogId;
    }

    @Override
    protected Pair<Integer, String> startPump() {
        if (fakeModePump) {
            suspiciousNames.addAll(BaseUtils.splitUniqueBySep(
                    getTeradataConfigProp("teradata.suspiciousNamesForDebug", "S66_B0020_AGREEMENT"), ",", true));
            execNamedCommandWithCommit("prepareForFakeMode");
        }
        TeradataConfigBean configData = PumpUtils.readAdminBean("teradata", TeradataConfigBean.class, adhocDao);
        if (configData == null) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Error with loading config data!");
        }

        int stopShowTableCallAfterErrorCnt = BaseUtils.tryParseInteger(configData.maxErrorCount, STOP_SHOW_TABLE_CALL_AFTER_ERROR_CNT);

        try {
            tc = ConnectionUtils.getConnectionByConnectionString(DriverNameConstants.TERADATA, configData.url, configData.user, configData.password, "tmode=ANSI,charset=UTF8");
            if (tc == null) {
                return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "No connection with Teradata!");
            }
        } catch (Exception ex) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "No connection with Teradata! " + ex.getMessage());
        }

        try {
            if (configData.select == null) {
                return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Bad select query");
            }
            Statement createStatement = tc.createStatement();
            if (!fakeModePump) {
                execNamedCommandWithCommit("deleteTeradataOldTables");
            }
            int indexStarted = configData.select.indexOf("FROM", 0);
            String whereString = configData.select.substring(indexStarted);
            String ownerStatement = "select distinct(OwnerName) " + whereString;
            // get owners
            pumpLogger.pumpLog("Insert into temporary table: owners", 15);
            ResultSet ownerQuery = createStatement.executeQuery(ownerStatement);
            teradataList.clear();
            while (ownerQuery.next()) {
                String ownerName = ownerQuery.getString("OwnerName");
                teradataList.add(new TeradataBean(insertedLogId, "TeradataOwner", ownerName.trim(), null, "owner:" + ownerName.trim() + "|", null, null));
            }
            if (isWarnLoggingInFakeModeEnabled()) {
                logger.warn("fake: got owners, all beans now: " + teradataList.size());
            }
            // get schemas
            pumpLogger.pumpLog("Insert into temporary table: schemas", 30);
            ResultSet executeQuery = createStatement.executeQuery(configData.select);
            while (executeQuery.next()) {
                String databaseName = executeQuery.getString("DatabaseName");
                schemasMap.add("'" + databaseName + "'");
                String ownerName = executeQuery.getString("OwnerName").trim();
                teradataList.add(new TeradataBean(insertedLogId, "TeradataSchema", databaseName.trim(), executeQuery.getString("CommentString"), databaseName.trim() + "|", "owner:" + ownerName + "|", null));
            }
            if (isWarnLoggingInFakeModeEnabled()) {
                logger.warn("fake: got schemas, all beans now: " + teradataList.size());
            }
            // get tables/views
            pumpLogger.pumpLog("Insert into temporary table: tables/views/procedures", 45);
            ResultSet executeQueryTables = createStatement.executeQuery("SELECT DatabaseName,TableName,TableKind,CommentString,RequestText FROM DBC.TablesV Where DatabaseName in (" + BaseUtils.mergeWithSepEx(schemasMap, ",") + ") AND (TableKind='T' OR TableKind='V' OR TableKind='P' OR TableKind='E')");

            List<Pair<String, TeradataBean>> forShowTableCalls = new ArrayList<Pair<String, TeradataBean>>();

            int tableCnt = 0;
            while (executeQueryTables.next()) {
                String tableName = BaseUtils.trimRight(executeQueryTables.getString("TableName"));
                String databaseName = executeQueryTables.getString("DatabaseName");
                String tableKind = executeQueryTables.getString("TableKind").trim();
                String comment = executeQueryTables.getString("CommentString");
                String requestText = null;
                boolean addToShowTable = false;

                if (!tableKind.equals("P") && !tableKind.equals("E")) {
                    tablesMap.add(databaseName + tableName);
                    requestText = executeQueryTables.getString("RequestText");
                    addToShowTable = true;
                }
                TeradataBean tb = new TeradataBean(insertedLogId, tableKindMap.get(tableKind), tableName, comment, databaseName.trim() + "|" + tableName + "|", databaseName.trim() + "|", requestText);
                if (isNameSuspiciousForFakeMode(tableName)) {
                    logger.warn("fake: adding bean with suspicious name to teradataList: " + tb);
                }
                teradataList.add(tb);
                tableCnt++;
                if (addToShowTable) {
                    forShowTableCalls.add(new Pair<String, TeradataBean>(tableKind, tb));
                }
            }
            if (isWarnLoggingInFakeModeEnabled()) {
                logger.warn("fake: added tables cnt=" + tableCnt + ", all beans now: " + teradataList.size());
            }
            if (isWarnLoggingInFakeModeEnabled()) {
                logger.warn("fake: tables for SHOW TABLE cnt=" + forShowTableCalls.size());
            }

            int showTableErrorCnt = 0;
            for (Pair<String, TeradataBean> fstp : forShowTableCalls) {
                String fullTableName = BaseUtils.dropOptionalSuffix(fstp.v2.branchNames, "|").replace("|", ".");
                String tableKind = fstp.v1;
                String requestText = fstp.v2.requestText;

                if (stopShowTableCallAfterErrorCnt > 0 && showTableErrorCnt >= stopShowTableCallAfterErrorCnt) {
                    requestText = "/* not calling SHOW TABLE due to " + showTableErrorCnt + " prev calls with no privilidges, showing last DDL statement on object */ \n\n" + requestText;
                } else {
                    try {
                        ResultSet executeTables = createStatement.executeQuery("SHOW " + tableKindMapForCreateSQL.get(tableKind) + " " + fullTableName);
                        while (executeTables.next()) {
                            requestText = executeTables.getString(1);
                        }
                    } catch (Exception e) {
                        showTableErrorCnt++;
                        if (isWarnLoggingInFakeModeEnabled()) {
                            logger.warn("fake: catched error (errCnt=" + showTableErrorCnt + ") for table: " + fullTableName + "\n"
                                    + e);
                        }
                        requestText = "/* error in SHOW TABLE: no privilidges, showing last DDL statement on object */ \n\n" + requestText;
                    }
                }
                fstp.v2.requestText = requestText;
            }

            // get indieces
            pumpLogger.pumpLog("Insert into temporary table: indieces", 70);
            ResultSet executeQueryIndex = createStatement.executeQuery("SELECT DatabaseName, TableName, IndexNumber, IndexType, UniqueFlag, IndexName, ColumnName, ColumnPosition FROM DBC.IndicesV Where DatabaseName in (" + BaseUtils.mergeWithSepEx(schemasMap, ",") + ")");
            while (executeQueryIndex.next()) {
                String tableName = BaseUtils.trimRight(executeQueryIndex.getString("TableName"));
                String databaseName = executeQueryIndex.getString("DatabaseName");
                if (tablesMap.contains(databaseName + tableName)) {
                    String columnName = BaseUtils.trimRight(executeQueryIndex.getString("ColumnName"));
                    String indexName = executeQueryIndex.getString("IndexName");
                    String indexType = executeQueryIndex.getString("IndexType").trim();
                    String idxTmp;
                    if ((idxTmp = indexNameAndTypeMap.get(databaseName + tableName + columnName)) != null) {
                        if (!idxTmp.contains("Primary")) {
                            indexNameAndTypeMap.put(databaseName + tableName + columnName, tableIndexTypeMap.get(indexType));
                        }
                    } else {
                        indexNameAndTypeMap.put(databaseName + tableName + columnName, tableIndexTypeMap.get(indexType));
                    }
                    String type = tableIndexTypeMap.get(indexType);
                    String uniqueFlag = tableIndexUniqueMap.get(executeQueryIndex.getString("UniqueFlag"));
                    Integer colPos = executeQueryIndex.getInt("ColumnPosition");
                    if (type != null && uniqueFlag != null && columnName != null && colPos != null) {
                        execNamedCommandWithCommit("insertTeradataIndexes", databaseName.trim() + "|" + tableName + "|", indexName == null ? "(#" + executeQueryIndex.getString("IndexNumber") + ")" : indexName, tableIndexTypeMap.get(indexType), tableIndexUniqueMap.get(executeQueryIndex.getString("UniqueFlag").trim()), columnName, executeQueryIndex.getInt("ColumnPosition"), fakeModePump);
                    }
                }
            }
            if (isWarnLoggingInFakeModeEnabled()) {
                logger.warn("fake: got indices, all beans now: " + teradataList.size());
            }
            // get columns
            pumpLogger.pumpLog("Insert into temporary table: columns", 85);
            ResultSet executeQueryColumns = createStatement.executeQuery("SELECT DatabaseName, TableName, ColumnName, ColumnType, ColumnLength, DecimalFractionalDigits, DecimalTotalDigits, Nullable FROM DBC.COLUMNSV Where DatabaseName in (" + BaseUtils.mergeWithSepEx(schemasMap, ",") + ")");
            while (executeQueryColumns.next()) {
                String columnName = BaseUtils.trimRight(executeQueryColumns.getString("ColumnName"));
                String columnType = columnTypeMap.get(executeQueryColumns.getString("ColumnType") != null ? executeQueryColumns.getString("ColumnType").trim() : null);
                String columnTypeToInsert;
                String databaseName = executeQueryColumns.getString("DatabaseName");
                String tableName = BaseUtils.trimRight(executeQueryColumns.getString("TableName"));
                String nullable = executeQueryColumns.getString("Nullable");
                if (tablesMap.contains(databaseName + tableName)) {
                    if (nullable == null) {
                        nullable = " NULL";
                    }
                    if (executeQueryColumns.getString("ColumnType") != null) {
                        if (executeQueryColumns.getString("ColumnType").equals("D")) {
                            columnTypeToInsert = columnType + "(" + executeQueryColumns.getString("DecimalFractionalDigits") + "," + executeQueryColumns.getString("DecimalTotalDigits") + ")";
                        } else if (typesWithLength.contains(executeQueryColumns.getString("ColumnType"))) {
                            columnTypeToInsert = columnType + "(" + executeQueryColumns.getString("ColumnLength") + ")";
                        } else {
                            columnTypeToInsert = columnType;
                        }
                    } else {
                        columnTypeToInsert = columnType;
                    }
                    String columnTypeIdx;
                    if ((columnTypeIdx = indexNameAndTypeMap.get(databaseName + tableName + columnName)) == null) {
                        columnTypeIdx = "TeradataColumn";
                    } else {
                        if (!columnTypeIdx.contains("Primary")) {
                            columnTypeIdx = "TeradataColumnIDX";
                        } else {
                            columnTypeIdx = "TeradataColumnPK";
                        }
                    }

                    teradataList.add(new TeradataBean(insertedLogId, columnTypeIdx, columnName, (columnTypeToInsert == null ? "" : columnTypeToInsert + (nullable.equals("N") ? " NOT NULL" : " NULL")), databaseName.trim() + "|" + tableName + "|" + columnName + "|", databaseName.trim() + "|" + tableName + "|", null));
                }
            }
            if (isWarnLoggingInFakeModeEnabled()) {
                logger.warn("fake: got columns, all beans now: " + teradataList.size());
            }

            // move data to bik_teradata
            if (isWarnLoggingInFakeModeEnabled()) {
                logger.warn("teradataList.size() = " + teradataList.size());

                for (int i = 0; i < teradataList.size(); i++) {
                    TeradataBean tb = teradataList.get(i);
                    if (/*
                             * BaseUtils.safeEquals("S66_B0020_AGREEMENT", tb.name)
                             */isNameSuspiciousForFakeMode(tb.name)) { //S66_B0020_AGREEMENT AlertControl
                        logger.warn("suspicious bean @idx=" + i + ": " + tb);
                    }
                }
            }
            if (!fakeModePump) {
                insertListToDBInPackagesEx(teradataList, "insertTeradataMetadata", 10);
            }
            if (logger.isInfoEnabled()) {
                logger.info("Update BIKS structure!");
            }
            pumpLogger.pumpLog("Update BIKS structure" + (fakeModePump ? "(fake mode)" : ""), 90);
            if (!fakeModePump) {
                execNamedCommandWithCommit("moveToBikTeradata");
            }
            if (!fakeModePump) {
                execNamedCommandWithCommit("moveTeradataMetadataIntoBikNode");
                execNamedCommandWithCommit("addTeradataTree2Instance", "Teradata", BaseUtils.tryParseInteger(configData.instanceId));
            }
            if (logger.isInfoEnabled()) {
                logger.info("All right. Success!");
            }
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("" + ex, ex);
            }
            throw new LameRuntimeException(ex.getMessage(), ex);
        } finally {
            if (tc != null) {
                closeConnection(tc);
            }
        }
    }
}
