/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.NodeKind4TreeKind;
import pl.fovis.common.TreeKindBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author wezyr
 */
public class NodeActionPaste extends NodeActionUpdatingBase {

    @Override
    public String getCaption() {
        return I18n.wklej.get() /* I18N:  */;
    }

    @Override
    public void execute() {
        final TreeNodeBean srcNode = clipboard.getNode();

        if (clipboard.isCut()) {
            srcNode.parentNodeId = node.id;
            BIKClientSingletons.getService().updateTreeNodeAndChangeFvs(srcNode, new StandardAsyncCallback<List<String>>("Failure on" /* I18N: no */ + " updateTreeNode") {
                        @Override
                        public void onSuccess(List<String> result) {
                            BIKClientSingletons.showInfo(I18n.wklejonoElement.get() /* I18N:  */);
                            ctx.getTreeGrid().pasteNode(node.id, srcNode.id, clipboard.isCut());
                            clipboard.clear();
//                    refreshRedisplayDataAndSelectRecord(srcNode.parentNodeId, true); // byl zwykly refresh
//                    refreshRedisplayDataAndSelectRecord(srcNode.id, true);
                            result.add(srcNode.treeCode);
//                    refreshAndRedisplayData();
//                    refreshRedisplayDataAndSelectRecord(srcNode.id);
                            refreshAndRedisplayDataByTreeCode(result);
                        }
                    });
        } else { // copy
//            BIKClientSingletons.getService().createTreeNode(node.id, srcNode.nodeKindId,
//                    srcNode.name, srcNode.objId, srcNode.treeId, srcNode.linkedNodeId,
//                    new StandardAsyncCallback<Integer>("Failure on createTreeNode") {
//
//                        public void onSuccess(Integer result) {
//                            BIKClientSingletons.showInfo("Wklejono nowy element");
//                            refreshAndRedisplayData();
//                        }
//                    });
        }
    }

    @Override
    public boolean isEnabledByDesign() {
        if (clipboard.getNode() == null) {
            return false;
        }
        if(!treeBean.treeKind.equals(BIKConstants.TREE_KIND_META_BIKS)){
            return false;
        }
        if (isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_BLOG)) {
            // 1. węzeł w którym wklejamy nie może być podlinkowany,
            // 2. zalogowany użytkownik musi być autorem tego bloga,
            // 3. węzeł w schowku musi być wpisem blogowym
            return !isNodeLinkedAnyhow() && isLoggedUserInBlogAuthors(false)
                    && (clipboard.getNode() != null && clipboard.getNode().nodeKindCode.equals(BIKGWTConstants.NODE_KIND_BLOG_ENTRY));
        } else if (isNodeOneOfTheKind(true, BIKConstants.NODE_KIND_LISA_FOLDER, BIKConstants.NODE_KIND_LISA_DICTIONARIES)) {
            return clipboard.getNode() != null
                    && (clipboard.getNode().nodeKindCode.equals(BIKConstants.NODE_KIND_LISA_DICTIONARY)
                    || clipboard.getNode().nodeKindCode.equals(BIKConstants.NODE_KIND_LISA_FOLDER));
        } else if (isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_ALL_USERS_FOLDER)) {
            return false;
        } else if (isInDQMAdminMode()) {
            return !isNodeBuiltIn() && clipboard.getNode() != null && !isNodeTheSameOrDescendantOf(clipboard.getNode());
            //&& isNodeFolder() nie sprawdzamy czy folder bo chyba nie ma potrzeby.
        } else {
            return isNodeNotLinkedAnyhowNorReadOnlyTreeNorTheSameNorDescendantOfClipboarNodeAndNodeIsFolder() && isCompatible();
        }
    }

    protected boolean isCompatible() {
        if (clipboard.getNode() == null) {
            return false;
        }
        TreeKindBean tk = BIKClientSingletons.getTreeKindBeanByCode(node.treeKind);
        for (NodeKind4TreeKind nk4tk : tk.additionalNodeKindsId) {
            if (ClientDiagMsgs.isShowDiagEnabled(ClientDiagMsgs.APP_MSG_KIND)) {
                ClientDiagMsgs.showDiagMsg(ClientDiagMsgs.APP_MSG_KIND, nk4tk.srcNodeKindId + ";" + nk4tk.dstNodeKindId + ";" + node.nodeKindId + ";" + clipboard.getNode().nodeKindId);
            }
            if ((BIKConstants.CHILD.equals(nk4tk.relationType))
                    && (nk4tk.srcNodeKindId != null) && (nk4tk.srcNodeKindId.equals(node.nodeKindId))
                    && (nk4tk.dstNodeKindId != null) && (nk4tk.dstNodeKindId.equals(clipboard.getNode().nodeKindId))) {
                return true;
            }
        }
        return false;
    }
}
