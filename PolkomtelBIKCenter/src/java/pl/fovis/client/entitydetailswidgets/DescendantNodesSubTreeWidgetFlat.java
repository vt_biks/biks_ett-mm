/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.Widget;
import java.util.Collections;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.bikpages.BikTreeWidget;
import pl.fovis.client.bikpages.PageDataFetchBroker;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
public class DescendantNodesSubTreeWidgetFlat extends EntityDetailsTabBase {

    protected BikTreeWidget bbTree;

    @Override
    public void displayData() {
        bbTree.initWithFullData(Collections.singletonList(getData().node), true, null, null);
    }

    @Override
    public Widget buildWidgets() {
        bbTree = new BikTreeWidget(false,
                BIKClientSingletons.ORIGINAL_EDP_BODY, "wwNewTreePopup wwNewTree",
                new PageDataFetchBroker() {
                    {
                        isFullDataFetched = true;
                    }
                }, true,
                "name=" /* i18n: no */ + I18n.nazwa.get() /* I18N:  */);
        return bbTree.getTreeGridWidget();
    }

//    public boolean deprecatedIsEnabled(String code, int count) {
//        return true;
//    }
    @Override
    public boolean hasEditableControls() {
        return false;
    }

    @Override
    public String getCaption() {
        return BIKGWTConstants.MENU_NAME_DESCENDANT_NODES;
    }

    @Override
    public Integer getItemCount() {
        return null;
    }

    @Override
    public String getTreeKindForSelected() {
        return null;
    }

    @Override
    public boolean isEnabled() {
        return hasContent();
    }
}
