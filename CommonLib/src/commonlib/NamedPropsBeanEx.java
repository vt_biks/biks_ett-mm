/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.util.Map;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.INamedPropsBean;
import simplelib.INamedPropsBeanEx;
import simplelib.INamedPropsBeanWriter;
import simplelib.INamedPropsRenameStrategy;
import simplelib.NamedPropsRenameStrategy;

/**
 *
 * @author wezyr
 */
public class NamedPropsBeanEx implements INamedPropsBeanEx {

    public enum Mode {

        ReadOnly(1), WriteOnly(2), ReadWrite(3);
        private int v;

        Mode(int v) {
            this.v = v;
        }
    };
    private Object bean;
    private Mode mode;

    public NamedPropsBeanEx(Object bean, Mode mode) {
        this.bean = bean;
        this.mode = mode;
    }

    public NamedPropsBeanEx(Object bean) {
        this(bean, Mode.ReadWrite);
    }

    public Object getPropValue(String propName) {
        return MuppetMerger.getProperty(bean, propName);
    }

    public Set<String> getPropNames() {
        return MuppetMerger.getMuppetProps(bean, mode.v);
    }

    public void setPropValue(String propName, Object value) {
        MuppetMerger.setProperty(bean, propName, value);
    }

//    public static final Projector<?, INamedPropsBean> readableProjector =
//            new Projector<Object, INamedPropsBean>() {
//
//                public INamedPropsBean project(Object val) {
//                    return wrapAsNamedPropsBean(val);
//                }
//            };
//    public static final Projector<?, INamedPropsBeanWriter> writableProjector =
//            new Projector<Object, INamedPropsBeanWriter>() {
//
//                public INamedPropsBeanWriter project(Object val) {
//                    return wrapAsNamedPropsBeanWriter(val);
//                }
//            };
    
    public static INamedPropsBean wrapAsNamedPropsBean(Object bean) {
        INamedPropsBean res = BaseUtils.wrapAsNamedPropsBean(bean, false);

        if (res != null) {
            return res;
        }

        return new NamedPropsBeanEx(bean);
    }

    public static INamedPropsBeanWriter wrapAsNamedPropsBeanWriter(Object bean) {
        INamedPropsBeanWriter res = BaseUtils.wrapAsNamedPropsBeanWriter(bean, false);

        if (res != null) {
            return res;
        }

        return new NamedPropsBeanEx(bean);
    }

    public static void copyProps(Object src, Object dst,
            INamedPropsRenameStrategy renameStrategy) {
        if (src == null) {
            return;
        }
        NamedPropsRenameStrategy.copyProps(wrapAsNamedPropsBean(src),
                wrapAsNamedPropsBeanWriter(dst), renameStrategy);
    }

    public static <T1, T2, K> void extendWithExtraProps(Iterable<T1> items, Map<K, T2> extraProps,
            Projector<T1, K> keyProjector, INamedPropsRenameStrategy propsRenameStrategy) {
        NamedPropsRenameStrategy.extendWithExtraProps(items, extraProps, keyProjector,
                new Projector<T1, INamedPropsBeanWriter>() {

                    public INamedPropsBeanWriter project(T1 val) {
                        return wrapAsNamedPropsBeanWriter(val);
                    }
                },
                new Projector<T2, INamedPropsBean>() {

                    public INamedPropsBean project(T2 val) {
                        return wrapAsNamedPropsBean(val);
                    }
                }, propsRenameStrategy);
    }
}
