exec sp_check_version '1.0.20';
go
----------------------------------------------------------------------
----------------------------------------------------------------------
insert into bik_node_kind(code, caption)
values('DefaultFolder', 'Folder domy�lny')
go
----------------------------------------------------------------------
----------------------------------------------------------------------
update bik_node
set node_kind_id = (select id from bik_node_kind where code = 'DefaultFolder')
from bik_node bn
join bik_tree bt on bn.tree_id = bt.id
where bt.name = 'Dokumenty' and bn.name = 'Wszystkie dokumenty'
go
----------------------------------------------------------------------
----------------------------------------------------------------------
exec sp_update_version '1.0.20', '1.0.21';
go

