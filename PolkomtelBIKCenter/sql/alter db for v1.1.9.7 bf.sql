﻿exec sp_check_version '1.1.9.7';
go

insert into bik_data_source_def(description) values('SAS')
go

if not exists( select * from bik_app_prop where name='availableConnectors' and val like'%Business Objects%')
update bik_admin set value='0' where param='sapbo.isActive'
go
if not exists( select * from bik_app_prop where name='availableConnectors' and val like'%DQC%')
update bik_admin set value='0' where param='dqc.isActive'
go
if not exists( select * from bik_app_prop where name='availableConnectors' and val like'%Teradata DBMS%')
update bik_admin set value='0' where param='teradata.isActive'
go

if not exists( select * from bik_app_prop where name='availableConnectors' and val like'%SAS%')
update bik_admin set value='0' where param='sas.isActive'
go

if not exists(select * from bik_app_prop where name='availableConnectors' and val like'%MS SQL%')
update bik_db_server set is_active=0 where source_id =(select id from bik_data_source_def where description='MS SQL')
go

update bik_tree set is_hidden=(select (case when value= 1 then 0 else 1 end) from bik_admin where param='sapbo.isActive') where code in('Reports','ObjectUniverses','Connections')
update bik_tree set is_hidden=(select (case when value= 1 then 0 else 1 end) from bik_admin where param='dqc.isActive') where code='DQC'
update bik_tree set is_hidden=(select (case when value= 1 then 0 else 1 end) from bik_admin where param='teradata.isActive') where code='Teradata'
update bik_tree set is_hidden=(select (case when value= 1 then 0 else 1 end) from bik_admin where param='sas.isActive') where code in('SASReports','SASInformationMaps','SASLibraries','SASCubes')
update bik_tree set is_hidden=(case when ( select  top 1 is_active from bik_db_server order by is_active desc) = 1 then 0 else 1 end) where code ='MS SQL'          
go


exec sp_update_version '1.1.9.7', '1.1.9.8';
go