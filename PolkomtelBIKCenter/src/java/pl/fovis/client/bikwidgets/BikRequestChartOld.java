/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.googlecode.gchart.client.GChart;
import java.util.List;
import pl.fovis.common.dqm.DataQualityRequestBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author tflorczak
 */
public class BikRequestChartOld extends GChart {

    public BikRequestChartOld(Double errorThreshold, List<DataQualityRequestBean> requests) {
        setChartTitle("<b>" + I18n.wykresWykonanTestu.get() /* I18N:  */ + "</b>");
        setChartSize(400, 140);
        getXAxis().setAxisLabel(I18n.wykonania.get() /* I18N:  */);
        getXAxis().setTickCount(requests.size());
        getYAxis().setAxisLabel(I18n.blad.get() /* I18N:  */ + " (%)");

        // requests for test
        addCurve();
        getCurve().getSymbol().setSymbolType(SymbolType.LINE);
        getCurve().getSymbol().setFillThickness(2);
        getCurve().getSymbol().setFillSpacing(1);
        getCurve().getSymbol().setBackgroundColor("blue" /* I18N: no */);
        int j = requests.size();
        for (int i = 0; i < requests.size(); i++) {
            if (requests.get(i).errorCount != null && requests.get(i).caseCount != null) {
                getCurve().addPoint(j--, 100 * (double) requests.get(i).errorCount / (double) requests.get(i).caseCount);
            }
        }
        getCurve().setLegendLabel(I18n.errorCountDivCaseCountShort.get() /*"Error count / Case count"*/ /* I18N: no */);

        // error treshold
        addCurve();
        getCurve().getSymbol().setSymbolType(SymbolType.LINE);
        getCurve().getSymbol().setFillThickness(2);
        getCurve().getSymbol().setFillSpacing(1);
        getCurve().getSymbol().setBackgroundColor(
                getCurve().getSymbol().getBorderColor());
        for (int i = 1; i <= requests.size(); i++) {
            getCurve().addPoint(i, errorThreshold * 100);
        }
        getCurve().setLegendLabel(I18n.progBledu.get()/*"Error Threshold"*/ /* I18N: no */);
    }
}
