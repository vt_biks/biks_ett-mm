/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.ui.HTML;
import pl.fovis.foxygwtcommons.dialogs.OneTextBoxDialog;
import pl.fovis.foxygwtcommons.i18n.I18n;

/**
 *
 * @author AMamcarz
 */
public class ViewURLLink extends OneTextBoxDialog {

//    TextBoxBase tbb;
    @Override
    protected String getActionButtonCaption() {
        return null;
    }

    @Override
    protected void buildMainWidgets() {
        HTML infoText = new HTML(I18n.wcisnijCtrlCAbySkopiowaLinkDoSch.get() /* I18N:  */ + ".");
        infoText.addStyleName("sendUrlInfoText");
        main.add(infoText);
        super.buildMainWidgets();
//        tbValue.setWidth("100%");
        tbValue.setWidth("600px");
        tbValue.setReadOnly(true);
    }

    @Override
    protected void firstShowCentered() {
        super.firstShowCentered();

        Scheduler.get().scheduleFixedDelay(new RepeatingCommandFix() {

            boolean first = true;

            @Override
            public boolean doExecute() {
                tbValue.setFocus(true);
                tbValue.selectAll();

                return false;
            }
        }, 600);
    }
}
