create table aaa_linked_resource (
id int not null identity primary key,
kind int not null, -- 1 artyku�, 2 za��cznik
caption varchar(max) not null,
href varchar(max) not null,
si_id int not null
);
go

-- drop table aaa_keyword

create table aaa_keyword (
id int not null identity primary key,
body varchar(800) not null unique
);
go

create table aaa_entity_keyword (
id int not null identity primary key,
keyword_id int not null references aaa_keyword (id),
si_id int not null,
constraint uk_aaa_entity_keyword unique (keyword_id, si_id)
);
go

-- drop table aaa_tree_node
create table aaa_tree_node (
id int not null identity primary key,
item_si_id int null,
parent_id int null references aaa_tree_node (id),
id_path varchar(800) not null unique, -- 10 digits max per id + 1 for separator -> 72 levels min
caption varchar(max) not null,
tree_num int not null
);
go

-- drop table aaa_entity_in_tree_node
create table aaa_entity_in_tree_node (
id int not null identity primary key,
si_id int null,
tree_node_id int not null references aaa_tree_node (id),
constraint uk_aaa_entity_in_tree_node unique (si_id, tree_node_id)
);
go


create table aaa_user (
id int not null identity primary key,
name varchar(250) not null unique,
email varchar(250) null,
phone_num varchar(200) null,
kind int not null, -- 1 expert, 2 user, 3 bik center admin
short_descr varchar(max) null
);
go

-- drop table aaa_note
create table aaa_note (
id int not null identity primary key,
user_id int not null references aaa_user(id),
title varchar(140) not null,
body varchar(max) null,
date_added datetime not null default getdate(),
si_id int not null,
kind int not null -- 1 description, 2 question
);
go

/*
insert into aaa_user (name, kind) values ('wezyr', 3);

insert into aaa_note (si_id, user_id, title, kind) values (8841, dbo.fn_User_id('wezyr'), 'To jest moja profesjonalna notatka nr 1', 1);
insert into aaa_note (si_id, user_id, title, kind) values (8841, dbo.fn_User_id('wezyr'), 'Druga notatka - nakr�ci�em si�!', 1);

insert into aaa_note (si_id, user_id, title, kind) values (8842, dbo.fn_User_id('wezyr'), 'Notka #3', 1);



*/

select * 
from aaa_entity_keyword ekw inner join aaa_keyword kw on ekw.keyword_id = kw.id
where ekw.si_id = 1000

insert into aaa_keyword (body) values ('BI')
insert into aaa_keyword (body) values ('Business Objects')
insert into aaa_keyword (body) values ('Reklamacje')
insert into aaa_keyword (body) values ('Panteon')
insert into aaa_keyword (body) values ('Pankracy')
insert into aaa_keyword (body) values ('DEV')
insert into aaa_keyword (body) values ('Energetyka')

insert into aaa_entity_keyword (si_id, keyword_id) values (8840, dbo.fn_KEYWORD_ID('BI'));
insert into aaa_entity_keyword (si_id, keyword_id) values (8840, dbo.fn_KEYWORD_ID('Panteon'));
insert into aaa_entity_keyword (si_id, keyword_id) values (8930, dbo.fn_KEYWORD_ID('DEV'));
insert into aaa_entity_keyword (si_id, keyword_id) values (8839, dbo.fn_KEYWORD_ID('DEV'));
insert into aaa_entity_keyword (si_id, keyword_id) values (8839, dbo.fn_KEYWORD_ID('Energetyka'));
insert into aaa_entity_keyword (si_id, keyword_id) values (7361, dbo.fn_KEYWORD_ID('Energetyka'));

select si_id, * from aaa_global_props where SI_NAME = 'DEV/ST'
select si_id, * from aaa_global_props where SI_NAME = 'PGE'
select si_id, * from aaa_global_props where SI_NAME = 'PGE Univ'
