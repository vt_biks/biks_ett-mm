/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.drools;

import commonlib.LameUtils;
import java.util.Collection;
import java.util.List;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.DroolsConfigBean;
import pl.fovis.common.DroolsEnvironment;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.server.BOXIServiceImpl;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.IFoxyTransactionalPerformer;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class DroolsEngineWorker implements IFoxyTransactionalPerformer {

    protected BOXIServiceImpl service;
    protected INamedSqlsDAO<Object> adhocDao;
    protected IRulesManagementEngine engine;
    protected DroolsConfigBean config;
    protected List<EntityDetailsDataBean> eddbs;
    protected boolean isCancelled = false;
    protected AttributeBean ab = null;
    protected TreeNodeBean tnb = null;
    //    protected Integer lastCheckedChangeId;
//    protected Integer lastChangedId;

    public DroolsEngineWorker(BOXIServiceImpl service, INamedSqlsDAO<Object> adhocDao, IRulesManagementEngine engine, DroolsConfigBean config) {
        this.service = service;
        this.adhocDao = adhocDao;
        this.engine = engine;
        this.config = config;
    }

    protected Integer sqlNumberToInt(Object numVal) {
        return adhocDao.sqlNumberToInt(numVal);
    }

    protected <V> V execNamedQuerySingleVal(String qryName, boolean allowNoResult,
            String optColumnName, Object... args) {
        return BeanConnectionUtils.<V>execNamedQuerySingleVal(adhocDao, qryName, allowNoResult, optColumnName,
                args);
    }

    protected Integer execNamedQuerySingleValAsInteger(String qryName, boolean allowNoResult,
            String optColumnName, Object... args) {
        return sqlNumberToInt(execNamedQuerySingleVal(qryName, allowNoResult, optColumnName,
                args));
    }

    @Override
    public void finishWorkUnit(boolean success) {
        adhocDao.finishWorkUnit(success);
    }

    public void doit() {
        try {
            service.incrementAndGetNDroolsInstance();
            addLog("Starting...");
            adhocDao.execInAutoCommitMode(new IContinuation() {

                @Override
                public void doIt() {
                    Pair<EngineStatus, String> reset = engine.reset(config.drlFolder);
                    if (reset.v1 == EngineStatus.FAILED) {
                        System.out.println(reset.v2);
                        addLog("Drools' compiling : FAILED");
                        addLog(reset.v2);
                        return;
                    }
                    addLog("Drools' compiling : OK");

                    DroolsEnvironment env = null;
                    Collection<? extends Object> objs = engine.getObjects();
                    if (!BaseUtils.isCollectionEmpty(objs)) {
                        for (Object obj : objs) {
                            if (obj instanceof DroolsEnvironment) {
                                env = (DroolsEnvironment) obj;
                            }
                        }
                    }
                    if (env == null) {
                        env = new DroolsEnvironment();
                        env.hasCreated = false;
                        engine.addAll(env);
                    }

                    if (ab != null) {
                        engine.addAll(ab);
                    }
                    if (tnb != null) {
                        engine.addAll(tnb);
                    }
                    if (ab != null) {
                        engine.addAll(ab);
                    }
                    if (tnb != null) {
                        engine.addAll(tnb);
                    }
                    engine.fireAllRules();

                    addLog("Done work in folder: " + config.drlFolder);
                }
            });
            finishWorkUnit(true);
        } catch (Throwable ex) {
            ex.printStackTrace();
            addLog(LameUtils.printExceptionAsString((Exception) ex));
            finishWorkUnit(false);
        } finally {
            service.decrementAndGetNDroolsInstance();
        }
    }

    public void setAb(AttributeBean ab) {
        this.ab = ab;
    }

    public void setTnb(TreeNodeBean tnb) {
        this.tnb = tnb;
    }

    public void cancel() {
        isCancelled = true;
    }

    public String tryCompile() {
        Pair<EngineStatus, String> result = engine.reset(config.drlFolder, true);
        return result.v2;
    }

    private void addLog(String msg) {
        adhocDao.execNamedCommand("addDroolsLogMsg", msg);
    }
}
