/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

/**
 *
 * @author tflorczak
 */
public class GenericNodeBean {

    public String objId;
    public String parentObjId;
    public String nodeKindCode;
    public String name;
    public String descr;
    public int visualOrder;

    public GenericNodeBean() {
    }

    public GenericNodeBean(String objId, String parentObjId, String nodeKindCode, String name, String descr, int visualOrder) {
        this.objId = objId;
        this.parentObjId = parentObjId;
        this.nodeKindCode = nodeKindCode;
        this.name = name;
        this.descr = descr;
        this.visualOrder = visualOrder;
    }

    public GenericNodeBean(String objId, String parentObjId, String nodeKindCode, String name, String descr) {
        this.objId = objId;
        this.parentObjId = parentObjId;
        this.nodeKindCode = nodeKindCode;
        this.name = name;
        this.descr = descr;
    }
}
