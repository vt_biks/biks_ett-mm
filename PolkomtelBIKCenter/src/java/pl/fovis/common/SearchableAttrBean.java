/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author pmielanczuk
 */
public class SearchableAttrBean implements Serializable {

    public int id;
    public String name;
    public int searchWeight;
    public String caption;
}
