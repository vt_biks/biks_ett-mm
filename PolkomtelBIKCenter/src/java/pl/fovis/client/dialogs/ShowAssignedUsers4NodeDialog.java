package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.List;
import java.util.Map;
import pl.fovis.common.RightRoleBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;

/**
 *
 * @author Eugeniusz
 */
public class ShowAssignedUsers4NodeDialog extends BaseActionOrCancelDialog {

    protected Map<RightRoleBean, List<SystemUserBean>> users;

    public ShowAssignedUsers4NodeDialog(Map<RightRoleBean, List<SystemUserBean>> users) {
        this.users = users;
    }

    @Override
    protected String getDialogCaption() {
        return I18n.usersBtn.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return "action button nie jest potrzebny";
    }

    @Override
    protected void buildMainWidgets() {
        VerticalPanel vp = new VerticalPanel();
        ScrollPanel sp = new ScrollPanel(vp);
        sp.setHeight("300px");

        FlexTable topFt = new FlexTable();
        Label roleLbl = new Label(I18n.roleUzytkownikow.get());
        roleLbl.setWidth("125px");
        Label usersLbl = new Label(I18n.userNames.get());

        topFt.setWidget(0, 0, roleLbl);
        topFt.setWidget(0, 1, usersLbl);
        vp.add(topFt);

        int numbOfLines = users.keySet().size();
        for (RightRoleBean rrb : users.keySet()) {
            if (!rrb.code.equals("Administrator")) {
                numbOfLines--;

                FlexTable ft = new FlexTable();
                VerticalPanel hpRole = new VerticalPanel();
                VerticalPanel hpUser = new VerticalPanel();
                hpRole.setSpacing(2);
                hpRole.setWidth("120px");

                hpUser.setSpacing(2);
                hpUser.setStyleName("komunikat");

                ft.setWidget(0, 0, hpRole);
                ft.setWidget(0, 1, hpUser);

                hpRole.add(new Label(rrb.code));

                for (SystemUserBean sub : users.get(rrb)) {
                    hpUser.add(new Label(sub.loginName));
                }
                vp.add(ft);
                if (numbOfLines != 0) {
                    vp.add(new HTML("<hr>"));
                }
            }
        }
        main.add(sp);
        actionBtn.setVisible(false);
    }

    @Override
    protected void doAction() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
