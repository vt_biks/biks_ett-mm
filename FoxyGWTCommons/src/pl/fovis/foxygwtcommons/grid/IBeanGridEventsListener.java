/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.grid;

/**
 *
 * @author pmielanczuk
 */
public interface IBeanGridEventsListener<T> {

    public void pageChanged();

    public void oneBeanChanged(T bean);

    public void beansChanged();
}
