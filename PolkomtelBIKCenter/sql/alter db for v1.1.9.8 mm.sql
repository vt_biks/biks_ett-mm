﻿exec sp_check_version '1.1.9.8';
go

alter table bik_right_role add constraint uk_bik_right_role_code unique (code)
go

alter table bik_user_right add constraint uk_bik_user_right_user_right unique (user_id, right_id)
go

exec sp_update_version '1.1.9.8', '1.1.9.9';
go

