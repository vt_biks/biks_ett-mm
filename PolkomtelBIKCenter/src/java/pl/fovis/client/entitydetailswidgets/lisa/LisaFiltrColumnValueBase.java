/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import java.io.Serializable;
import pl.fovis.client.dialogs.lisa.LisaDataType;
import static pl.fovis.client.dialogs.lisa.LisaDataType.DA;
import static pl.fovis.client.dialogs.lisa.LisaDataType.I;
import static pl.fovis.client.dialogs.lisa.LisaDataType.I1;
import static pl.fovis.client.dialogs.lisa.LisaDataType.I2;
import static pl.fovis.client.dialogs.lisa.LisaDataType.I8;
import pl.fovis.common.lisa.DateLDV;
import pl.fovis.common.lisa.IntegerLDV;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryValue;
import pl.fovis.common.lisa.StringLDV;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class LisaFiltrColumnValueBase implements Serializable {

    public String nameInDb;

    public LisaFiltrColumnValueBase() {
    }

    public LisaFiltrColumnValueBase(String nameInDb) {
        this.nameInDb = nameInDb;
    }

    public LisaDictionaryValue parseFromWidget(LisaDictColumnBean columnBean, Widget w) {
        LisaDictionaryValue value;
        LisaDataType dataType = LisaDataType.getValueFor(columnBean.dataType);
        String txtValue;
        //System.out.println(dataType);
        switch (dataType) {
            case DA:
                value = new DateLDV(((DateBox) w).getValue());
                break;
            case I1:
            case I2:
            case I8:
            case I:
                txtValue = ((TextBox) w).getValue();
                value = new IntegerLDV(BaseUtils.tryParseInteger(txtValue));
                break;
            default:
                txtValue = ((TextBox) w).getValue();
                value = new StringLDV(txtValue);
        }

        return value;
    }
}
