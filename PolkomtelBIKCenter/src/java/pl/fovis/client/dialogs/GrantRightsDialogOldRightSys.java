/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.RightRoleBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author ctran
 */
public class GrantRightsDialogOldRightSys extends BaseActionOrCancelDialog {

    protected TreeNodeBean node;
    protected List<SystemUserBean> users;
    protected Map<CheckBox, SystemUserBean> userBeanMap = new HashMap<CheckBox, SystemUserBean>();
    protected Map<CheckBox, RightRoleBean> roleBeanMap = new HashMap<CheckBox, RightRoleBean>();
    protected Map<CheckBox, SystemUserGroupBean> groupBeanMap = new HashMap<CheckBox, SystemUserGroupBean>();
    protected ArrayList<SystemUserBean> resultUsersList = new ArrayList<SystemUserBean>();
    protected ArrayList<RightRoleBean> resultRoleList = new ArrayList<RightRoleBean>();
    protected ArrayList<SystemUserGroupBean> resultGroupList = new ArrayList<SystemUserGroupBean>();
    protected IParametrizedContinuation<Map<RightRoleBean, List<SystemUserGroupBean>>> saveContGroups;
    protected IParametrizedContinuation<Map<SystemUserBean, List<RightRoleBean>>> saveContUsers;
    protected List<RightRoleBean> userRoles;
    protected Map<SystemUserBean, List<RightRoleBean>> usersMappedToRole;
    protected SystemUserBean currUser = BIKClientSingletons.getLoggedUserBean();
    protected List<SystemUserGroupBean> groupsBeans;
    protected VerticalPanel vpUsersNgroups = new VerticalPanel();
    protected VerticalPanel usersCheckBoxPanel = new VerticalPanel();
    protected VerticalPanel groupsCheckBoxPanel = new VerticalPanel();
    protected TextBox searchTxtBx = new TextBox();
    protected Label userNamesLbl;

    public GrantRightsDialogOldRightSys(TreeNodeBean node, Map<SystemUserBean, List<RightRoleBean>> usersMappedToRole,
            List<RightRoleBean> userRoles, List<SystemUserGroupBean> groupsBeans,
            IParametrizedContinuation<Map<SystemUserBean, List<RightRoleBean>>> saveContUsers,
            IParametrizedContinuation<Map<RightRoleBean, List<SystemUserGroupBean>>> saveContGroups) {
        this.node = node;
        this.usersMappedToRole = usersMappedToRole;
        this.userRoles = userRoles;
        this.groupsBeans = groupsBeans;
        this.saveContUsers = saveContUsers;
        this.saveContGroups = saveContGroups;
    }

    @Override
    protected String getDialogCaption() {
        return I18n.nadajUprawnienia.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {
        FlexTable ft = new FlexTable();
        VerticalPanel vpRoles = new VerticalPanel();
        vpRoles.setWidth("100px");

        ScrollPanel sp = new ScrollPanel(vpRoles);

        ft.setWidget(0, 0, new Label(I18n.roleUzytkownikow.get() + ":"));
        FlexTable ftLblAndSearch = new FlexTable();
        FlexTable ftSearch = new FlexTable();
        ftLblAndSearch.setWidget(0, 1, ftSearch);
        ft.setWidget(0, 1, ftLblAndSearch);

        userNamesLbl = new Label(I18n.userNames.get() + ":");
        userNamesLbl.setWidth("160px");
        ftLblAndSearch.setWidget(0, 0, userNamesLbl);

        searchTxtBx.addValueChangeHandler(new ValueChangeHandler<String>() {

            @Override
            public void onValueChange(ValueChangeEvent<String> event) {
                seekForACoincidences();
            }
        });
        ftSearch.setWidget(0, 0, searchTxtBx);

        PushButton searchBtn = new PushButton(new Image("images/searchBtn1.png"));
        searchBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                seekForACoincidences();
            }
        });

        ftSearch.setWidget(0, 1, searchBtn);

        ft.setWidget(1, 0, sp);
        ScrollPanel sp2 = new ScrollPanel(vpUsersNgroups);
        sp2.addScrollHandler(new ScrollHandler() {

            @Override
            public void onScroll(ScrollEvent event) {
                int groupsMinusUserNameLbl = groupsCheckBoxPanel.getElement().getAbsoluteTop() - userNamesLbl.getElement().getAbsoluteTop();
                if (groupsMinusUserNameLbl - 30 < 0) {
                    userNamesLbl.setText(I18n.grupyUzytkownikow.get() + ":");
                } else {
                    userNamesLbl.setText(I18n.userNames.get() + ":");
                }
            }
        });

        ft.setWidget(1, 1, sp2);

        sp.setHeight("300px");
        sp2.setHeight("300px");
        ft.setWidth("320px");

        displayRoles(userRoles, vpRoles);

        displayUsers(usersMappedToRole);

        vpUsersNgroups.setSpacing(2);
        displayGroups(groupsBeans);

        checkCurrentUserRights();
        main.add(ft);
    }

    protected void seekForACoincidences() {
        if (searchTxtBx.getValue().isEmpty()) {
            displayUsers(usersMappedToRole);
            displayGroups(groupsBeans);
        } else {
            Map<SystemUserBean, List<RightRoleBean>> tempSearchUserMap = new HashMap<SystemUserBean, List<RightRoleBean>>();
            List<SystemUserGroupBean> tempSearchGroupsMap = new ArrayList<SystemUserGroupBean>();
            String enteredString = searchTxtBx.getValue();
            enteredString = enteredString.toLowerCase();

            for (Map.Entry<SystemUserBean, List<RightRoleBean>> pair : usersMappedToRole.entrySet()) {
                if (pair.getKey() != null) {
                    String userName = (pair.getKey().userName != null ? pair.getKey().userName : pair.getKey().loginName).toLowerCase();

                    if (userName.contains(enteredString)) {
                        tempSearchUserMap.put(pair.getKey(), pair.getValue());
                    }
                }
            }

            for (SystemUserGroupBean groupBean : groupsBeans) {
                String groupName = groupBean.name.toLowerCase();

                if (groupName.contains(enteredString)) {
                    tempSearchGroupsMap.add(groupBean);
                }
            }

            displayUsers(tempSearchUserMap);
            displayGroups(tempSearchGroupsMap);
        }
        checkCurrentUserRights();
    }

    protected void displayUsers(Map<SystemUserBean, List<RightRoleBean>> usersMappedToRole) {
        vpUsersNgroups.clear();
        usersCheckBoxPanel.clear();
        if (!usersMappedToRole.isEmpty()) {
            for (Map.Entry<SystemUserBean, List<RightRoleBean>> pair : usersMappedToRole.entrySet()) {
                boolean userHasHigherRights = false;
                for (RightRoleBean rrb : pair.getValue()) {
                    if (rrb != null && rrb.rank > 60) {
                        userHasHigherRights = true;
                        break;
                    }
                }
                if (pair.getKey() != null) {
                    if (currUser.id != pair.getKey().id && !userHasHigherRights) {
                        final CheckBox cbUser = new CheckBox(pair.getKey().userName == null ? pair.getKey().loginName : pair.getKey().userName);
                        userBeanMap.put(cbUser, pair.getKey());
                        cbUser.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

                            @Override
                            public void onValueChange(ValueChangeEvent<Boolean> event) {
                                SystemUserBean bean = userBeanMap.get(cbUser);
                                if (event.getValue()) {
//                            //TODO: check if the nonPublicUserTreeIDs array is empty and so on
                                    if (!BaseUtils.isCollectionEmpty(bean.nonPublicUserBranchIds)) {
                                        if (!bean.nonPublicUserBranchIds.contains(String.valueOf(node.id))) {
                                            resultUsersList.add(bean);
                                        } else {
                                            BIKClientSingletons.showInfo(bean.userName + I18n.uzytkownikMaUprawnienia.get() /* I18N:  */);

                                            //relocate before actual creating checkbox
                                            cbUser.setValue(false);
                                        }
                                    } else {
                                        resultUsersList.add(bean);
                                    }
                                } else {
                                    if (resultUsersList.contains(bean)) {
                                        resultUsersList.remove(bean);
                                    }
                                }
                            }
                        });
                        cbUser.setEnabled(false);
                        usersCheckBoxPanel.add(cbUser);
                    }
                }
            }
        } else {
            usersCheckBoxPanel.add(new Label(I18n.brakUzytkownikow.get()));
        }
        vpUsersNgroups.add(usersCheckBoxPanel);
    }

    protected void displayGroups(List<SystemUserGroupBean> groupsBeans) {
        groupsCheckBoxPanel.clear();
        vpUsersNgroups.add(new HTML("<br></br>"));
        vpUsersNgroups.add(new Label(I18n.grupyUzytkownikow.get() + ":"));
        if (!groupsBeans.isEmpty()) {
            for (SystemUserGroupBean groupBean : groupsBeans) {
                final CheckBox cbGroup = new CheckBox(groupBean.name);
                groupBeanMap.put(cbGroup, groupBean);

                cbGroup.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

                    @Override
                    public void onValueChange(ValueChangeEvent<Boolean> event) {
                        SystemUserGroupBean bean = groupBeanMap.get(cbGroup);
                        if (event.getValue()) {
                            resultGroupList.add(bean);
                        } else {
                            if (resultGroupList.contains(bean)) {
                                resultGroupList.remove(bean);
                            }
                        }
                    }
                });
                groupsCheckBoxPanel.add(cbGroup);
            }
        } else {
            groupsCheckBoxPanel.add(new HTML("<br></br>"));
            groupsCheckBoxPanel.add(new Label(I18n.brakGrupUzytkownikow.get()));
        }
        vpUsersNgroups.add(groupsCheckBoxPanel);
    }

    protected void displayRoles(List<RightRoleBean> userRoles, VerticalPanel vpRoles) {
        for (RightRoleBean role : userRoles) {
            if (!(role.code.equals("Administrator") || role.code.equals("AppAdmin") || role.code.equals("Expert"))) {
                final CheckBox cbRole = new CheckBox(role.code);
                roleBeanMap.put(cbRole, role);
                cbRole.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

                    @Override
                    public void onValueChange(ValueChangeEvent<Boolean> event) {
                        RightRoleBean role = roleBeanMap.get(cbRole);
                        if (event.getValue()) {
                            resultRoleList.add(role);
                        } else {
                            if (resultRoleList.contains(role)) {
                                resultRoleList.remove(role);
                            }
                        }
                    }
                });
                cbRole.setEnabled(false);
                vpRoles.add(cbRole);
            }
        }
    }

    protected void checkCurrentUserRights() {
        List<RightRoleBean> currUserRoles;
        for (Map.Entry<SystemUserBean, List<RightRoleBean>> pair : usersMappedToRole.entrySet()) {
            if (pair.getKey() != null && pair.getKey().id == currUser.id) {
                currUserRoles = usersMappedToRole.get(pair.getKey());
                for (RightRoleBean currUserRole : currUserRoles) {
                    //hierarchia uprawnień author>creator>nonPublicUser>regular user
                    enableElemInCheckBoxRolePair(currUserRole);
                }
            }
        }
    }

    protected void enableElemInCheckBoxRolePair(RightRoleBean currUserRole) {
        for (RightRoleBean existingRole : userRoles) {
            if (existingRole.rank > 60) {
                for (Map.Entry<CheckBox, RightRoleBean> checkBoxRolePair : roleBeanMap.entrySet()) {
                    checkBoxRolePair.getKey().setEnabled(true);
                }
            }
            if (existingRole.code.equals(currUserRole.code) && (checkUsersRights(currUserRole.rank))) {
                for (Map.Entry<CheckBox, RightRoleBean> checkBoxRolePair : roleBeanMap.entrySet()) {
                    if (checkBoxRolePair.getValue().id == existingRole.id) {
                        checkBoxRolePair.getKey().setEnabled(true);
                    }
                }
            }
        }
        for (Map.Entry<CheckBox, SystemUserBean> checkBoxUserPair : userBeanMap.entrySet()) {
            checkBoxUserPair.getKey().setEnabled(true);
        }
    }

    protected boolean checkUsersRights(int currUserRoleRank) {
        if (currUserRoleRank == 60) {
            return BIKClientSingletons.isBranchAuthorLoggedIn(node.branchIds) || BIKClientSingletons.isLoggedUserAuthorOfTree(node.treeId);
        } else if (currUserRoleRank == 55) {
            return BIKClientSingletons.isBranchCreatorLoggedIn(node.branchIds) || BIKClientSingletons.isLoggedUserCreatorOfTree(node.treeId);
        } else if (currUserRoleRank == 30) {
            return BIKClientSingletons.isBranchNonPublicUserLoggedIn(node.branchIds) || BIKClientSingletons.isLoggedUserIsNonPublicUserInTree(node.treeId);
        }
        return false;
    }

    @Override
    protected void doAction() {
        if (resultRoleList.isEmpty()) {
            BIKClientSingletons.showWarning(I18n.zaznaczRole.get());
        } else {
            if (resultUsersList.isEmpty() && resultGroupList.isEmpty()) {
                BIKClientSingletons.showWarning(I18n.zaznaczUzytkownikow.get());
            } else {
                if (!resultUsersList.isEmpty()) {
                    Map<SystemUserBean, List<RightRoleBean>> resultMapUsers = new HashMap<SystemUserBean, List<RightRoleBean>>();
                    for (SystemUserBean sub : resultUsersList) {
                        resultMapUsers.put(sub, resultRoleList);
                    }
                    saveContUsers.doIt(resultMapUsers);
                }
                if (!resultGroupList.isEmpty()) {
                    Map<RightRoleBean, List<SystemUserGroupBean>> resultMapGroups = new HashMap<RightRoleBean, List<SystemUserGroupBean>>();
                    for (RightRoleBean rrb : resultRoleList) {
                        resultMapGroups.put(rrb, resultGroupList);
                    }
                    saveContGroups.doIt(resultMapGroups);
                }
            }
        }
    }
}
