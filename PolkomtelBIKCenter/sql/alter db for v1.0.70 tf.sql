﻿exec sp_check_version '1.0.70';
go

delete from bik_data_load_log where status = 0 and data_source_type_id=(select id from bik_data_source_def where description='Business Objects' or description='BO')

create table bik_data_load_log_details (
  id int IDENTITY(1,1) not null,
  log_id int not null references bik_data_load_log(id),
  description varchar(500) not null,
  start_time datetime not null 
);
go

update bik_data_source_def set description='Business Objects' where description='BO'
update bik_data_source_def set description='Teradata DBMS' where description='TERADATA DBMS'


exec sp_update_version '1.0.70', '1.0.71';
go
