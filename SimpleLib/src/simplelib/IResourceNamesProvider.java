/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import java.util.Set;

/**
 *
 * @author wezyr
 */
public interface IResourceNamesProvider {
    public Set<String> getNames(String basePath);
    public boolean isDirectory(String name);
}
