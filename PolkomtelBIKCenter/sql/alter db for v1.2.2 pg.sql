﻿exec sp_check_version '1.2.2';
go


declare @user_role_bik_tree_id int
declare @user_node_kind_id int
declare @all_users_node_id int

--dane początkowe
select @user_role_bik_tree_id=id from bik_tree where code = 'UserRoles'
select @user_node_kind_id=id from bik_node_kind where code = 'User'
select @all_users_node_id=id from bik_node where node_kind_id =
	(select id from bik_node_kind where code = 'AllUsersFolder')
	and is_deleted = 0
	and tree_id = @user_role_bik_tree_id

--ETAP I - dodanie użytkownika społecznościowego - ponieważ w poprzednim podejściu blogi mógł tworzyć
--użytkownik systemowy, który może nie mieć społecznościowego. Jeśli trafi się przypadek, gdy
--nie ma społecznościowego to dodaję automatycznie społecznościowego z tą samą nazwą co systemowy

--zmienne używane na pierwszym cursorze
declare @system_user_id int
declare @system_user_name varchar(250)
declare @is_admin int
declare @siocial_user_id int
declare @added_user_bik_node_id int

declare no_social_users_but_blog_autors_curs cursor for 
	select distinct id, name, is_admin from
	(
		select bsu.id, bsu.name, ba.is_admin  from bik_blog bb
		join bik_node bn on bb.node_id = bn.id
		join bik_node bn2 on bn.parent_node_id = bn2.id
		join bik_authors ba on bn2.id = ba.node_id
		left join bik_system_user bsu on bsu.id = bb.user_id
		where 
		bn.is_deleted = 0 and bn2.is_deleted = 0 and
		bsu.user_id is null
		union
		select bsu.id, bsu.name, ba.is_admin 
		from bik_node bn
		join bik_authors ba on ba.node_id = bn.id
		left join bik_system_user bsu on bsu.id = ba.user_id
		where 
		tree_id = (select id from bik_tree where code = 'Blogs' )
		and 
		node_kind_id = (select id from bik_node_kind where code = 'Blog')
		and is_deleted =0 and bsu.user_id is null
	) tunion

open no_social_users_but_blog_autors_curs

	fetch next from no_social_users_but_blog_autors_curs into @system_user_id, @system_user_name, @is_admin

	while @@fetch_status = 0
		begin
			--select @system_user_id, @system_user_name, @is_admin
			--dodać do bik_user
			insert into bik_user(name) values (@system_user_name)
			select @siocial_user_id = scope_identity()
			
			--uaktualnic bik system user - dodac id spolecznościowego
			update bik_system_user set user_id = @siocial_user_id where id = @system_user_id
			
			--następnie dodać do bik_node usera, do drzewa użytkownicy
			insert into bik_node(parent_node_id, node_kind_id, name, tree_id)
				values(@all_users_node_id, @user_node_kind_id, @system_user_name, @user_role_bik_tree_id)
			select @added_user_bik_node_id = scope_identity()
			
			update bik_node set 
				branch_ids = convert( varchar(1000), @all_users_node_id ) +'|' + convert( varchar(1000), @added_user_bik_node_id ) +'|'
				where 
				id = @added_user_bik_node_id
			
			--muszę jeszcze użytkownika społecznościowego zaktualizować
			update bik_user set node_id = @added_user_bik_node_id where id = @siocial_user_id
		
			fetch next from no_social_users_but_blog_autors_curs into @system_user_id, @system_user_name, @is_admin
		end

close no_social_users_but_blog_autors_curs;
deallocate no_social_users_but_blog_autors_curs;


--ETAP II wstawiam autorów blogów jako redaktorzy główni lub pomocniczy
--zmienne używane na drugim kursorze
declare @editor_role_node_id int
declare @role_editor_id int
declare @node_id int
declare @is_auxiliary int
declare @user_node int
declare @user_name varchar(250)
declare @tmp int

select @editor_role_node_id = node_id from bik_role_for_node where code = 'Editor'
select @role_editor_id=id from bik_role_for_node where code = 'Editor'


--ponownie uzywam kursora do wstawienia wszystkich autorów blogów jaki redaktorzy
declare add_blog_autors_curs cursor for 
	select  
		bsu.id, bn.id, case when ba.is_admin=1 then 0 else 1 end is_auxiliary, @role_editor_id
	from bik_blog bb
	join bik_node bn on bb.node_id = bn.id
	join bik_node bn2 on bn.parent_node_id = bn2.id
	join bik_authors ba on bn2.id = ba.node_id
	left join bik_system_user bsu on bsu.id = bb.user_id
	where bn.is_deleted = 0 and bn2.is_deleted = 0
	union
	select bsu.id, bn.id, case when ba.is_admin=1 then 0 else 1 end is_auxiliary, @role_editor_id 
	from bik_node bn
	join bik_authors ba on ba.node_id = bn.id
	left join bik_system_user bsu on bsu.id = ba.user_id
	where 
	tree_id = (select id from bik_tree where code = 'Blogs' )
	and 
	node_kind_id = (select id from bik_node_kind where code = 'Blog')
	and is_deleted =0	
	except
	select bsu.id, buin.node_id, buin.is_auxiliary, buin.role_for_node_id from bik_user_in_node buin
	 join bik_system_user bsu on bsu.user_id = buin.user_id

	open add_blog_autors_curs

	fetch next from add_blog_autors_curs into @system_user_id, @node_id, @is_auxiliary, @tmp

	while @@fetch_status = 0
		begin
	
		select @siocial_user_id=user_id from bik_system_user where id = @system_user_id

		insert into bik_object_author(node_id, user_id, date_added)
			values(@node_id, @siocial_user_id,'2012-12-12 12:12' )

		insert into bik_user_in_node(user_id, node_id, role_for_node_id, is_auxiliary)
			values(@siocial_user_id, @node_id, @role_editor_id, @is_auxiliary) 
		
		select @user_node = node_id, @user_name = name from bik_user where id = @siocial_user_id
		
		if not exists (select * from bik_node where parent_node_id = @editor_role_node_id and linked_node_id = @user_node and is_deleted = 0 )
		begin
			insert into bik_node (parent_node_id, node_kind_id, name, tree_id, linked_node_id)
			values (@editor_role_node_id, @user_node_kind_id , @user_name , @user_role_bik_tree_id, @user_node )
			
			select @added_user_bik_node_id = scope_identity()
			
			update bik_node
			set branch_ids = convert( varchar(1000), @editor_role_node_id ) +'|' + convert( varchar(1000), @added_user_bik_node_id ) +'|',
			visual_order = dbo.fn_get_visual_order_for_new_node(parent_node_id, tree_id)
			where id = @added_user_bik_node_id		
		end		
		
		fetch next from add_blog_autors_curs into @system_user_id, @node_id, @is_auxiliary, @tmp
		end
		
close add_blog_autors_curs;
deallocate add_blog_autors_curs;


exec sp_update_version '1.2.2', '1.2.2.1';
go
