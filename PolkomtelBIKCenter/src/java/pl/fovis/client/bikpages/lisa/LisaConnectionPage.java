/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.lisa;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.BikPageMinimalBase;
import pl.fovis.client.dialogs.lisa.LisaAddOrEditInstanceDialog;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import pl.fovis.common.lisa.TeradataException;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author tbzowka
 */
public class LisaConnectionPage extends BikPageMinimalBase {

//    private final static String RADIO_GROUP = "LCPRadioGroup" /* i18n: no */;
//    public TextBox serverNameTb, portTb, dbNameTb, charsetTb, defaultUnassignedCategoryNameTb, schemaTb;
//    public RadioButton ansiButton, teradataButton;
//    protected Label serverNameLbl, portLbl, dbNameLbl, charsetLbl, transactionTypeLbl, defaultUnassignedCategoryNameLbl;
    protected ListBox teradataInstanceLb;
    protected PushButton addInstanceBtn, editInstanceBtn, deleteInstanceBtn;
    protected Map<Integer, LisaInstanceInfoBean> id2InstanceMap;
    protected IContinuation refreshPage = new IContinuation() {

        @Override
        public void doIt() {
            populateInstanceList();
        }
    };

    @Override
    protected Widget buildWidgets() {
        VerticalPanel vp = new VerticalPanel();
        HorizontalPanel hp = new HorizontalPanel();
        teradataInstanceLb = new ListBox();
        hp.add(new Label(I18n.listaInstancji.get() + ": "));
        hp.add(teradataInstanceLb);
        hp.add(addInstanceBtn = createAddInstanceBtn());
        hp.add(editInstanceBtn = createEditInstanceBtn());
        hp.add(deleteInstanceBtn = createDeleteInstanceBtn());
        hp.setSpacing(6);
        vp.add(hp);
        vp.add(createDeleteAllSessionsButton());
        vp.setSpacing(6);
        populateInstanceList();
        return vp;

//        serverNameLbl = new Label(I18n.nazwaSerwera.get() /* i18n:  */);
//        portLbl = new Label("Port" /* i18n: no */);
//        dbNameLbl = new Label(I18n.nazwaBazyDanych.get());
//        //schemaNameLbl = new Label(I18n.schemat.get() /* i18n:  */);
//        charsetLbl = new Label("Charset" /* i18n: no */);
//        transactionTypeLbl = new Label(I18n.typTransakcji.get() /* i18n:  */);
//        defaultUnassignedCategoryNameLbl = new Label(I18n.domyslnaNazwaKategDlaNieprzypisanych.get());
//
//        serverNameTb = new TextBox();
//        portTb = new TextBox();
//        dbNameTb = new TextBox();
//        schemaTb = new TextBox();
//        charsetTb = new TextBox();
//        defaultUnassignedCategoryNameTb = new TextBox();
//
//        ansiButton = new RadioButton(RADIO_GROUP, "ANSI" /* i18n: no */);
//        teradataButton = new RadioButton(RADIO_GROUP, "TERA" /* i18n: no */);
//        VerticalPanel transactionTypePanel = new VerticalPanel();
//        transactionTypePanel.add(ansiButton);
//        transactionTypePanel.add(teradataButton);
//
//        int row = 0;
//        Grid grid = new Grid(7, 2);
//        grid.setWidget(row, 0, serverNameLbl);
//        grid.setWidget(row++, 1, serverNameTb);
//        grid.setWidget(row, 0, portLbl);
//        grid.setWidget(row++, 1, portTb);
//        grid.setWidget(row, 0, dbNameLbl);
//        grid.setWidget(row++, 1, dbNameTb);
//        //grid.setWidget(row, 0, schemaNameLbl);
//        //grid.setWidget(row++, 1, schemaTb);
//        grid.setWidget(row, 0, charsetLbl);
//        grid.setWidget(row++, 1, charsetTb);
//        grid.setWidget(row, 0, defaultUnassignedCategoryNameLbl);
//        grid.setWidget(row++, 1, defaultUnassignedCategoryNameTb);
//        grid.setWidget(row, 0, transactionTypeLbl);
//        grid.setWidget(row++, 1, transactionTypePanel);
//
//        populateWidgetWithData();
//
//        VerticalPanel vp = new VerticalPanel();
//        vp.add(grid);
//        vp.add(createSaveButton());
//        vp.add(createDeleteAllSessionsBytton());
//        return createMainScrollPanel(vp, null);
    }

//    private void populateWidgetWithData() {
//        BIKClientSingletons.getLisaService().getLisaConnectionData(new StandardAsyncCallback<LisaTeradataConnectionBean>() {
//            @Override
//            public void onSuccess(LisaTeradataConnectionBean data) {
//                serverNameTb.setText(data.serverName);
//                portTb.setText(data.port);
////                dbNameTb.setText(data.dbName);
//                dbNameTb.setText(data.schemaName);
//                charsetTb.setText(data.charset);
//                defaultUnassignedCategoryNameTb.setText(data.defaultUnassignedCategoryName);
//                if (data.transactionType.equals("ANSI" /* i18n: no */)) {
//                    ansiButton.setValue(true);
//                } else {
//                    teradataButton.setValue(true);
//                }
//                schemaTb.setText(data.schemaName);
//            }
//        });
//    }
//    private PushButton createSaveButton() {
//        PushButton saveBtn = new PushButton(I18n.zapisz.get() /* I18N:  */);
//        NewLookUtils.makeCustomPushButton(saveBtn);
//        saveBtn.addClickHandler(new ClickHandler() {
//            public void onClick(ClickEvent event) {
//                LisaTeradataConnectionBean data = new LisaTeradataConnectionBean();
//                data.serverName = serverNameTb.getText();
//                data.port = portTb.getText();
//                data.dbName = dbNameTb.getText();
//                data.charset = charsetTb.getText();
////                data.schemaName = schemaTb.getText();
//                data.schemaName = dbNameTb.getText();
//                data.defaultUnassignedCategoryName = defaultUnassignedCategoryNameTb.getText();
//
//                if (ansiButton.getValue()) {
//                    data.transactionType = "ANSI" /* i18n: no */;
//                } else {
//                    data.transactionType = "TERA" /* i18n: no */;
//                }
//
//                BIKClientSingletons.getLisaService().updateLisaConnectionData(data, new StandardAsyncCallback<Void>() {
//                    @Override
//                    public void onSuccess(Void result) {
//                        BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get() /* I18N:  */);
//                    }
//                });
//            }
//        });
//        return saveBtn;
//    }
    private PushButton createDeleteAllSessionsButton() {
        PushButton deleteBtn = new PushButton(I18n.usunWszystkieSesieTeradaty.get() /* I18N:  */);
        NewLookUtils.makeCustomPushButton(deleteBtn);
        deleteBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

                BIKClientSingletons.getLisaService().deleteAllTeredataSessions((new StandardAsyncCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean result) {
                        BIKClientSingletons.showInfo(I18n.sesieZostalyUsuniete.get() /* I18N:  */);
                    }
                }));
            }
        });
        return deleteBtn;
    }

    public String getId() {
        return "admin:lisa:connection" /* i18n: no */;
    }

    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
    }

    private PushButton createAddInstanceBtn() {
        return createBtn(I18n.dodaj.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new LisaAddOrEditInstanceDialog().buildAndShowDialog(new LisaInstanceInfoBean(), false, refreshPage);
            }
        });
    }

    private PushButton createEditInstanceBtn() {
        return createBtn(I18n.edytuj.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                Integer selectedId = BaseUtils.tryParseInteger(teradataInstanceLb.getSelectedValue(), null);
                if (selectedId != null) {
                    new LisaAddOrEditInstanceDialog().buildAndShowDialog(id2InstanceMap.get(selectedId), true, refreshPage);
                }
            }
        });
    }

    private PushButton createDeleteInstanceBtn() {
        return createBtn(I18n.usun.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new ConfirmDialog().buildAndShowDialog(I18n.czyChceszUsunac.get() + ": " + teradataInstanceLb.getSelectedItemText() + " ?", new IContinuation() {

                    @Override
                    public void doIt() {
                        Integer selectedId = BaseUtils.tryParseInteger(teradataInstanceLb.getSelectedValue(), null);
                        if (selectedId != null) {
                            BIKClientSingletons.getLisaService().deleteLisaInstance(selectedId, new StandardAsyncCallback<Void>() {

                                @Override
                                public void onSuccess(Void result) {
                                    populateInstanceList();
                                }

                                @Override
                                public void onFailure(Throwable caught) {
                                    if (caught instanceof TeradataException) {
                                        TeradataException tdException = (TeradataException) caught;
                                        showOnFailureError(tdException.getMessage(), caught);
                                    } else {
                                        super.onFailure(caught);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    private void populateInstanceList() {
        BIKClientSingletons.getLisaService().getLisaInstanceList(new StandardAsyncCallback<List<LisaInstanceInfoBean>>() {

            @Override
            public void onSuccess(List<LisaInstanceInfoBean> instanceList) {
                id2InstanceMap = new HashMap<Integer, LisaInstanceInfoBean>();
                addInstanceBtn.setEnabled(true);
                teradataInstanceLb.clear();
                if (instanceList.size() > 0) {
                    editInstanceBtn.setEnabled(true);
                    deleteInstanceBtn.setEnabled(true);
                    for (LisaInstanceInfoBean instance : instanceList) {
                        id2InstanceMap.put(instance.id, instance);
                        teradataInstanceLb.addItem(instance.name);
                        teradataInstanceLb.setValue(teradataInstanceLb.getItemCount() - 1, String.valueOf(instance.id));
                    }
                }
            }
        });
    }

    private PushButton createBtn(String txt, ClickHandler clickHandler) {
        PushButton btn = new PushButton(txt);
        NewLookUtils.makeCustomPushButton(btn);
        btn.setEnabled(false);
        btn.addClickHandler(clickHandler);
        return btn;
    }
}
