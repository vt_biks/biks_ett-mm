/*
 * TestCodeUtils.java
 *
 * Created on 18 maj 2007, 16:00
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author wezyr
 */
public class TestCodeUtils {
    
    private TestCodeUtils() {}
    
    static public String escapeForJavaCode(String s) {
        return s == null ? "null" : "\"" + s + "\"";
    }
    
    static public void dumpCreationCode(StringBuilder sb, String varName, Set<String> cols) {
        sb.append("        Set<String> ").append(varName).append(" = new LinkedHashSet<String>();\n");
        for (String col : cols) {
            sb.append("        ").append(varName).append(".add(" + escapeForJavaCode(col) + ");\n");
        }
    }
    
    static public void dumpCreationCode(StringBuilder sb, String varName, Map<String, Boolean> forcedCols) {
        sb.append("        Map<String, Boolean> ").append(varName).append(" = new LinkedHashMap<String, Boolean>();\n");
        for (Entry<String, Boolean> e : forcedCols.entrySet()) {
            sb.append("        ").append(varName).append(".put(" + escapeForJavaCode(e.getKey()) +
                    ", " + e.getValue() + ");\n");
        }
    }
    
    static public void dumpTestResultCode(StringBuilder sb, String setVarName, Set<String> set) {
        sb.append("        assertEquals(")
        .append(set.size())
        .append(", ")
        .append(setVarName)
        .append(".size());\n");
        for (String s : set) {
            sb.append("        assertTrue(")
            .append(setVarName)
            .append(".contains(")
            .append(escapeForJavaCode(s))
            .append("));\n");
        }
    }
}
