/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.user.client.rpc.RpcRequestBuilder;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import pl.trzy0.foxy.commons.FoxyCommonConsts;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public class FoxyRpcRequestBuilder extends RpcRequestBuilder {

    static {
        InitFactories4GWT.initFactories();
    }

    private static final FoxyRpcRequestBuilder instance = new FoxyRpcRequestBuilder();
    private static final String foxyAppInstanceId = BaseUtils.generateRandomToken(20);
    private static String loggerUserName;

    protected static FoxyRpcRequestBuilder getInstance() {
        return instance;
    }
    protected static Map<String, String> reqExtraParams = new LinkedHashMap<String, String>();

    public static void setReqExtraParam(String name, String val) {
        reqExtraParams.put(name, val);
    }

    public static String getReqExtraParam(String name) {
        return reqExtraParams.get(name);
    }

    // specjalnie protected aby nie tworzyć dodatkowych instancji nadaremno
    protected FoxyRpcRequestBuilder() {
        super();
    }

    @Override
    protected RequestBuilder doCreate(String serviceEntryPoint) {
        FoxyRequestBuilder frb = new FoxyRequestBuilder(RequestBuilder.POST, serviceEntryPoint);        
        return frb; //new FoxyRequestBuilder(RequestBuilder.POST, serviceEntryPoint);
//        return super.doCreate(serviceEntryPoint);
    }

    @Override
    protected void doSetCallback(RequestBuilder rb, RequestCallback callback) {

        if (callback instanceof FoxyRequestCallback || !(rb instanceof FoxyRequestBuilder)) {
            super.doSetCallback(rb, callback);
            return;
        }

        FoxyRequestBuilder frb = (FoxyRequestBuilder) rb;

        super.doSetCallback(rb, new FoxyRequestCallback(callback, frb));
    }

    @Override
    protected void doFinish(RequestBuilder rb) {
        super.doFinish(rb);

        for (Entry<String, String> e : reqExtraParams.entrySet()) {
            final String val = e.getValue();
            if (val != null) {
                rb.setHeader(FoxyCommonConsts.REQ_EXTRA_PARAM_PREFIX + e.getKey(), val);
            }
        }

        rb.setHeader(FoxyCommonConsts.FOXY_APP_INSTANCE_ID_HEADER_NAME, foxyAppInstanceId);
        if (!BaseUtils.isStrEmptyOrWhiteSpace(loggerUserName)) {
            rb.setHeader(FoxyCommonConsts.FOXY_LOGGED_USER_NAME_HEADER_NAME, loggerUserName);
        }
        rb.setHeader("Strict-Transport-Security","max-age=31536000 ; includeSubDomains");
        rb.setHeader("X-Frame-Options","sameorigin");
        rb.setHeader("X-XSS-Protection","1; mode=block");
        rb.setHeader("X-Content-Type-Options","nosniff");
        rb.setHeader("Content-Security-Policy","script-src 'self'; img-src 'self'");
        rb.setHeader("Referrer-Policy", "origin");
        
    }

    public static <T> T fixAsyncService(T asyncService) {
        ((ServiceDefTarget) asyncService).setRpcRequestBuilder(getInstance());
        return asyncService;
    }

    public static void setLoggedUserName(String userName) {
        loggerUserName = userName;
    }

    public static String getOptLoggedUserNameOnClient() {
        return loggerUserName;
    }
}
