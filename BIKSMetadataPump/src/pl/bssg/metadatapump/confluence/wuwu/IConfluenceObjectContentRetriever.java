/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence.wuwu;

/**
 *
 * @author pmielanczuk
 */
public interface IConfluenceObjectContentRetriever {

    public String getHtml(long objId);

    public String getHtmlFull(long objId);

    // zwraca URL bazowy razem z / na końcu
    public String getAbsoluteUrlBase();
}
