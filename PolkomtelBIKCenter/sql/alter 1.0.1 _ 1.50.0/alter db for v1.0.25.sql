exec sp_check_version '1.0.25';
go


create table bik_user_kind(
	id int identity(1,1) not null primary key,
	name [varchar](250) not null,
	constraint uk_bik_user_kind unique(name)
)
go


insert bik_user_kind values ('Administrator');
insert bik_user_kind values ('Sponsor');
insert bik_user_kind values ('Expert');
insert bik_user_kind values ('Użytkownik');

go

update bik_user
set kind = 1
where kind = 0 or kind>4
go

ALTER TABLE [dbo].[bik_user]  WITH CHECK ADD FOREIGN KEY([kind])
REFERENCES [dbo].[bik_user_kind] ([id])

exec sp_update_version '1.0.25', '1.0.26';


