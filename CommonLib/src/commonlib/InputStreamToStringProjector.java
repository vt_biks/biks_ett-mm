/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.io.InputStream;
import simplelib.BaseUtils.Projector;

/**
 *
 * @author wezyr
 */
public class InputStreamToStringProjector implements Projector<InputStream, String> {

    private String encoding;

    public InputStreamToStringProjector(String encoding) {
        this.encoding = encoding;
    }

    public String project(InputStream val) {
        return LameUtils.loadAsString(val, encoding);
    }
}
