-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-- 
-- custom right roles
--
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
exec sp_check_version '1.6.z8.2';
go


if not exists(select * from sys.objects where object_id = object_id(N'bik_node_action') and type in (N'U'))
  create table bik_node_action (
    id int identity not null primary key,
    code varchar(255) not null unique
  );
go



if not exists(select * from sys.objects where object_id = object_id(N'bik_custom_right_role') and type in (N'U'))
create table bik_custom_right_role (
  id int not null identity primary key,
  code varchar(255) not null unique,
  visual_order tinyint null,
  tree_selector varchar(8000) null,
  selector_mode tinyint not null default 0
);
go


if not exists(select * from sys.objects where object_id = object_id(N'bik_node_action_in_custom_right_role') and type in (N'U'))
create table bik_node_action_in_custom_right_role (
  id int not null identity primary key,
  action_id int not null references bik_node_action (id),
  role_id int not null references bik_custom_right_role (id),
  unique (action_id, role_id)
);


/*

insert into bik_custom_right_role (code, visual_order, tree_selector, selector_mode) values
('DqmUser', 1, 'Documents, Reports,Teradata', 1),
('DqmEditor', 2, 'Documents,Reports,Teradata', 1),
('DqmAdmin', 3, 'Documents,Reports,Teradata', 0);

update bik_custom_right_role set tree_selector = 'Documents,Reports,Teradata' where code = 'DqmAdmin'

insert into bik_translation (code, txt, lang, kind) values
('DqmUser', 'DQM User', 'en', 'crr'),
('DqmUser', 'Użytkownik DQM', 'pl', 'crr'),
('DqmEditor', 'DQM Editor', 'en', 'crr'),
('DqmEditor', 'Redaktor DQM', 'pl', 'crr'),
('DqmAdmin', 'DQM Admin', 'en', 'crr'),
('DqmAdmin', 'Administrator DQM', 'pl', 'crr');

*/



if not exists(select * from sys.objects where object_id = object_id(N'bik_custom_right_role_user_entry') and type in (N'U'))

create table bik_custom_right_role_user_entry (
  id int identity not null primary key,
  user_id int not null references bik_system_user (id),
  role_id int not null references bik_custom_right_role (id),
  tree_id int null references bik_tree (id),
  node_id int null references bik_node (id),
  unique (user_id, role_id, tree_id, node_id)
);
go



if not exists(select * from sys.objects where object_id = object_id(N'bik_custom_right_role_view_branch_grant') and type in (N'U'))
create table bik_custom_right_role_view_branch_grant (
  tree_id int not null references bik_tree (id),
  user_id int not null references bik_system_user (id),
  branch_ids varchar(1000) not null,
  unique (user_id, tree_id, branch_ids)
);
go


if not exists (select * from bik_app_prop where name = 'customRightRolesTreeSelector')
insert into bik_app_prop (name, val, is_editable) values ('customRightRolesTreeSelector', '', 1);
go

if not exists (select * from bik_app_prop where name = 'registerNewNodeActions')
insert into bik_app_prop (name, val, is_editable) values ('registerNewNodeActions', '0', 1);
go


insert into bik_node_action (code)
select x.code
from
  (values ('ShowAnticipatedObjLinkDialog'),
('AddAnyNode'),
('JumpToLinkedNode'),
('AddRole'),
('Rename'),
('RenameRole'),
('EditDescription'),
('NewNestableParent'),
('AddDocument'),
('AddLisaCategory'),
('DelLisaCategory'),
('MoveLisaCategory'),
('AddLisaCategorization'),
('AddObjectFromConfluence'),
('AddObjectToConfluence'),
('DQMAddTest'),
('DQMEditTest'),
('DQMDeleteTest'),
('AddDefinition'),
('AddVersionDefinition'),
('EditDefinition'),
('AddQuestionAndAnswer'),
('Copy'),
('Cut'),
('Paste'),
('Delete'),
('DeleteRole'),
('MoveInTree'),
('ResetVisualOrder'),
('AddBlogEntry'),
('EditBlogEntry'),
('LinkNode'),
('ChangeToLeaf'),
('ChangeToBranch'),
('AddBranch'),
('AddLeaf'),
('AddCustomTreeNode:dtk_MechanizmKontrolny'),
('AddCustomTreeNode:dtk_Regula'),
('AddUser'),
('LinkUser'),
('EditUser'),
('CopyJojnedObj'),
('LinkUserToRole'),
('LinkUserToRoleForBlogs'),
('EditAnyComment'),
('AddComment')
) x(code) left join bik_node_action na on x.code = na.code
where na.code is null;
go



/*

insert into bik_app_prop (name, val, is_editable) values ('customRightRolesTreeSelector', 'DQM, @Taxonomy', 1);

update bik_app_prop set val = 'Documents, Reports,Teradata' where name = 'customRightRolesTreeSelector';

delete from bik_app_prop where name = 'customRightRolesTreeSelector';

*/


exec sp_update_version '1.6.z8.2', '1.6.z8.3';
go
