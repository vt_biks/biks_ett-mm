/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author tflorczak
 */
public class SimpleHtmlSanitizer {

    public static final boolean USE_MULTILINE = true;
    public static final IRegExpPattern forbiddenTags = BaseUtils.getRegExpMatcherFactory().compile("^(|script|object|embed|link|style|form|input)$", true, USE_MULTILINE);
    public static final IRegExpPattern allowedTags = BaseUtils.getRegExpMatcherFactory().compile("^(|b|p|i|s|a|img|table|thead|tbody|tfoot|tr|th|td|dd|dl|dt|em|h1|h2|h3|h4|h5|h6|li|ul|ol|span|div|strike|strong|"
            + "sub|sup|pre|del|code|blockquote|strike|kbd|br|hr|area|map|object|embed|param|link|form|small|big)$", true, USE_MULTILINE);
    private static final IRegExpPattern commentPattern = BaseUtils.getRegExpMatcherFactory().compile("<!--.*", true, USE_MULTILINE);  // <!--.........>
    //ww: było <(?i)
    private static final IRegExpPattern tagStartPattern = BaseUtils.getRegExpMatcherFactory().compile("<[?]?(\\w+\\b)\\s*(.*)/?>$", true, USE_MULTILINE);  // <tag ....props.....>
    //ww: było </(?i)
//    private static final IRegExpPattern tagClosePattern = BaseUtils.getRegExpMatcherFactory().compile("<\\/(\\w+\\b)\\s*>$", true, USE_MULTILINE);  // </tag .........>
    //tf: poprawka na myślnik, np: </ac:no-link>
    private static final IRegExpPattern tagClosePattern = BaseUtils.getRegExpMatcherFactory().compile("<\\/(\\w+\\b)\\s*:?[a-zA-Z0-9 \\-]*>$", true, USE_MULTILINE);  // </tag .........>
    private static final IRegExpPattern standAloneTags = BaseUtils.getRegExpMatcherFactory().compile("^(img|br|hr)$", true, USE_MULTILINE);
    private static final IRegExpPattern selfClosed = BaseUtils.getRegExpMatcherFactory().compile("<.+/>", true, USE_MULTILINE);
    private static final IRegExpPattern attributesPattern = BaseUtils.getRegExpMatcherFactory().compile("(\\w*)\\s*=\\s*\"([^\"]*)\"", true, USE_MULTILINE);  // prop="...."
    private static final IRegExpPattern attributes2Pattern = BaseUtils.getRegExpMatcherFactory().compile("(\\w*)\\s*=\\s*'([^']*)'", true, USE_MULTILINE);  // prop='....'
    private static final IRegExpPattern stylePattern = BaseUtils.getRegExpMatcherFactory().compile("([^\\s^:]+)\\s*:\\s*([^;]+);?", true, USE_MULTILINE);  // color:red;
    //ww: było (?i)
    private static final IRegExpPattern urlStylePattern = BaseUtils.getRegExpMatcherFactory().compile(".*\\b\\s*url\\s*\\(['\"]([^)]*)['\"]\\)", true, USE_MULTILINE);  // url('....')"
    public static final IRegExpPattern forbiddenStylePattern = BaseUtils.getRegExpMatcherFactory().compile("(?:(expression|eval|javascript))\\s*\\(", true, USE_MULTILINE);  // expression(....)"   thanks to Ben Summer

    /**
     *  This method should be used to test input.
     *
     * @param html
     * @return true if the input is "valid"
     */
    public static boolean isSanitized(String html) {
        return sanitizer(html).isValid;
    }

    /**
     * Used to clean every html before to output it in any html page
     *
     * @param html
     * @return sanitized html
     */
    public static String sanitize(String html) {
        return sanitizer(html).html;
    }

    /**
     * Used to get the text,  tags removed or encoded
     *
     * @param html
     * @return sanitized text
     */
    public static String getText(String html) {
        return sanitizer(html).text;
    }

    /**
     * This is the main method of sanitizing. It will be used both for validation and cleaning
     *
     * @param html
     * @return a SanitizeResult object
     */
    public static SanitizeResult sanitizer(String html) {
        return sanitizer(html, allowedTags, forbiddenTags);
    }

    public static SanitizeResult sanitizer(String html, IRegExpPattern allowedTags, IRegExpPattern forbiddenTags) {
        SanitizeResult ret = new SanitizeResult();
        Stack<String> openTags = new Stack<String>();

        List<String> tokens = tokenize(html);

        // -------------------   LOOP for every token --------------------------
        for (String token : tokens) {
            boolean isAcceptedToken = false;

            IRegExpMatcher startMatcher = tagStartPattern.matcher(token);
            IRegExpMatcher endMatcher = tagClosePattern.matcher(token);

//            System.out.println("1");
            //--------------------------------------------------------------------------------  COMMENT    <!-- ......... -->
            if (commentPattern.matcher(token).find()) {
//            System.out.println("2");
                ret.val = ret.val + token + (token.endsWith("-->") ? "" : "-->");
                ret.invalidTags.add(token + (token.endsWith("-->") ? "" : "-->"));
                continue;

                //--------------------------------------------------------------------------------  OPEN TAG    <tag .........>
            } else if (startMatcher.find()) {
//            System.out.println("3");

                //tag name extraction
                String tag = startMatcher.group(1).toLowerCase();

                //-----------------------------------------------------  FORBIDDEN TAG   <script .........>
                if (forbiddenTags.matcher(tag).find()) {
                    ret.invalidTags.add("<" + tag + ">");
                    continue;

                    // --------------------------------------------------  WELL KNOWN TAG
                } else if (allowedTags.matcher(tag).find()) {

                    String cleanToken = "<" + tag;
                    String tokenBody = startMatcher.group(2);

                    //first test table consistency
                    //table tbody tfoot thead th tr td
                    if ("thead".equals(tag) || "tbody".equals(tag) || "tfoot".equals(tag) || "tr".equals(tag)) {
                        if (openTags.search("table") < 1) {
                            ret.invalidTags.add("<" + tag + ">");
                            continue;
                        }
                    } else if ("td".equals(tag) || "th".equals(tag)) {
                        if (openTags.search("tr") < 1) {
                            ret.invalidTags.add("<" + tag + ">");
                            continue;
                        }
                    }

                    // then test properties
                    IRegExpMatcher attributes = attributesPattern.matcher(tokenBody);
                    IRegExpMatcher attributes2 = attributes2Pattern.matcher(tokenBody);

                    boolean foundURL = false; // URL flag
                    boolean isRawImage = false;
                    boolean hasMoreAttributes1 = true;
                    while (true) {

                        String attr = null;
                        String val = null;
                        if (hasMoreAttributes1 && attributes.find()) {
                            attr = attributes.group(1).toLowerCase();
                            val = attributes.group(2);
                        } else {
                            hasMoreAttributes1 = false;
                        }

                        if (!hasMoreAttributes1) {
                            if (!attributes2.find()) {
                                break;
                            }
                            attr = attributes2.group(1).toLowerCase();
                            val = attributes2.group(2);
                        }

//                        System.out.println("*** tag=" + tag + ", attr=" + attr + ", val=" + val);
                        // we will accept href in case of <A>
                        if ("a".equals(tag) && "href".equals(attr)) {    // <a href="......">
                            String[] customSchemes = {"http", "https"};
                            if (isUrlValid(customSchemes, val)) {
                                foundURL = true;
                            } else {
                                // may be it is a mailto?
                                //  case <a href="mailto:pippo@pippo.com?subject=...."
                                if (val.toLowerCase().startsWith("mailto:") && val.indexOf("@") >= 0) {
                                    String val1 = "http://www." + val.substring(val.indexOf("@") + 1);
                                    if (isUrlValid(customSchemes, val1)) {
                                        foundURL = true;
                                    } else {
                                        ret.invalidTags.add(attr + " " + val);
                                        val = "";
                                    }
                                } else {
                                    ret.invalidTags.add(attr + " " + val);
                                    val = "";
                                }
                            }

                        } else if ("a".equals(tag) && "name".equals(attr)) {    // <a name="......">
                            foundURL = true;
                        } else if (tag.matches("img|embed") && "src".equals(attr)) { // <img src="......">
                            String[] customSchemes = {"http", "https"};
                            if (isUrlValid(customSchemes, val)) {
                                foundURL = true;
                            } else if (val.indexOf("data:image") == 0) {
                                isRawImage = true;
                            } else {
                                ret.invalidTags.add(attr + " " + val);
                                val = "";
                            }

                        } else if ("href".equals(attr) || "src".equals(attr)) { // <tag src/href="......">   skipped
                            ret.invalidTags.add(tag + " " + attr + " " + val);
                            continue;

                        } else if (attr.matches("width|height")) { // <tag width/height="......">
                            if (!val.toLowerCase().matches("\\d+%|\\d+$")) { // test numeric values
                                ret.invalidTags.add(tag + " " + attr + " " + val);
                                continue;
                            }

                        } else if ("style".equals(attr)) { // <tag style="......">

                            // then test properties
                            IRegExpMatcher styles = stylePattern.matcher(val);
                            String cleanStyle = "";

                            while (styles.find()) {
                                String styleName = styles.group(1).toLowerCase();
                                String styleValue = styles.group(2);

                                // suppress invalid styles values
                                if (forbiddenStylePattern.matcher(styleValue).find()) {
                                    ret.invalidTags.add(tag + " " + attr + " " + styleValue);
                                    continue;
                                }

                                // check if valid url
                                IRegExpMatcher urlStyleMatcher = urlStylePattern.matcher(styleValue);
                                if (urlStyleMatcher.find()) {
                                    String[] customSchemes = {"http", "https"};
                                    String url = urlStyleMatcher.group(1);
                                    if (!isUrlValid(customSchemes, url)) {
                                        ret.invalidTags.add(tag + " " + attr + " " + styleValue);
                                        continue;
                                    }
                                }

                                cleanStyle = cleanStyle + styleName + ":" + encode(styleValue) + ";";

                            }
                            val = cleanStyle;

                        } else if (attr.startsWith("on")) {  // skip all javascript events
                            ret.invalidTags.add(tag + " " + attr + " " + val);
                            continue;

                        } else {  // by default encode all properies
                            val = encode(val);
                        }

                        cleanToken = cleanToken + " " + attr + (hasMoreAttributes1 ? "=\"" + val + "\""
                                : "='" + val + "'");
                    }
                    cleanToken = cleanToken + ">";

                    isAcceptedToken = true;

                    // for <img> and <a>
                    if (tag.matches("a|img|embed") && !foundURL && !isRawImage) {
                        isAcceptedToken = false;
                        cleanToken = "";
                    }

                    token = cleanToken;

                    // push the tag if require closure and it is accepted (otherwirse is encoded)
                    if (isAcceptedToken && !(standAloneTags.matcher(tag).find() || selfClosed.matcher(tag).find())) {
                        openTags.push(tag);
                    }

                    // --------------------------------------------------------------------------------  UNKNOWN TAG
                } else {
                    ret.invalidTags.add(token);
                    ret.val = ret.val + token;
                    continue;

                }

                // --------------------------------------------------------------------------------  CLOSE TAG </tag>
            } else if (endMatcher.find()) {
//                System.out.println("4");
                String tag = endMatcher.group(1).toLowerCase();

                //is self closing
                if (selfClosed.matcher(tag).find()) {
                    ret.invalidTags.add(token);
                    continue;
                }
                if (forbiddenTags.matcher(tag).find()) {
                    ret.invalidTags.add("/" + tag);
                    continue;
                }
                if (!allowedTags.matcher(tag).find()) {
                    ret.invalidTags.add(token);
                    ret.val = ret.val + token;
                    continue;
                } else {

                    String cleanToken = "";

                    // check tag position in the stack
                    int pos = openTags.search(tag);
                    // if found on top ok
                    for (int i = 1; i <= pos; i++) {
                        //pop all elements before tag and close it
                        String poppedTag = openTags.pop();
                        cleanToken = cleanToken + "</" + poppedTag + ">";
                        isAcceptedToken = true;
                    }

                    token = cleanToken;
                }

            }
//            System.out.println("5");
            ret.val = ret.val + token;

            if (isAcceptedToken) {
                ret.html = ret.html + token;
                //ret.text = ret.text + " ";
            } else {
                String sanToken = htmlEncodeApexesAndTags(token);
                ret.html = ret.html + sanToken;
                ret.text = ret.text + htmlEncodeApexesAndTags(removeLineFeed(token));
            }

        }

        // must close remaining tags
        while (openTags.size() > 0) {
            //pop all elements before tag and close it
            String poppedTag = openTags.pop();
            ret.html = ret.html + "</" + poppedTag + ">";
            ret.val = ret.val + "</" + poppedTag + ">";
        }

        //set boolean value
        ret.isValid = ret.invalidTags.isEmpty();

        return ret;
    }

    /**
     * Splits html tag and tag content <......>.
     *
     * @param html
     * @return a list of token
     */
    private static List<String> tokenize(String html) {
        ArrayList<String> tokens = new ArrayList<String>();
        int pos = 0;
        String token = "";
        int len = html.length();
        while (pos < len) {
            char c = html.charAt(pos);

            String ahead = html.substring(pos, pos > len - 4 ? len : pos + 4);

            //a comment is starting
            if ("<!--".equals(ahead)) {
                //store the current token
                if (token.length() > 0) {
                    tokens.add(token);
                }

                //clear the token
                token = "";

                // serch the end of <......>
                int end = moveToMarkerEnd(pos, "-->", html);
                tokens.add(html.substring(pos, end));
                pos = end;

                // a new "<" token is starting
            } else if ('<' == c) {

                //store the current token
                if (token.length() > 0) {
                    tokens.add(token);
                }

                //clear the token
                token = "";

                // serch the end of <......>
                int end = moveToMarkerEnd(pos, ">", html);
                tokens.add(html.substring(pos, end));
                pos = end;

            } else {
                token = token + c;
                pos++;
            }

        }

        //store the last token
        if (token.length() > 0) {
            tokens.add(token);
        }

        return tokens;
    }

    private static int moveToMarkerEnd(int pos, String marker, String s) {
        int i = s.indexOf(marker, pos);
        if (i > -1) {
            pos = i + marker.length();
        } else {
            pos = s.length();
        }
        return pos;
    }

    private static boolean isUrlValid(String[] customSchemes, String val) {
        // tak to było w oryginale
        //return new UrlValidator(customSchemes).isValid(val);
        if (val == null || val.isEmpty()) {
            return false;
        }

        if ((val.startsWith("fsb?get=") || val.startsWith("#")) && val.indexOf(" ") == -1) {
            return true;
        }

        for (String scheme : customSchemes) {
//            tak było...
//            if (val.startsWith(scheme + "://") && val.indexOf(" ") == -1) {
            if (val.startsWith(scheme + "://") && val.indexOf(" ") == -1) {
                return true;
            }
        }
//        System.out.println("url: " + val + " is invalid");
        return false;
    }

    /**
     * Contains the sanitizing results.
     * html is the sanitized html encoded  ready to be printed. Unaccepted tag are encode, text inside tag is always encoded    MUST BE USED WHEN PRINTING HTML
     * text is the text inside valid tags. Contains invalid tags encoded                                                        SHOULD BE USED TO PRINT EXCERPTS
     * val  is the html source cleaned from unaccepted tags. It is not encoded:                                                 SHOULD BE USED IN SAVE ACTIONS
     * isValid is true when every tag is accepted without forcing encoding
     * invalidTags is the list of encoded-killed tags
     */
    public static class SanitizeResult {

        public String html = "";
        public String text = "";
        public String val = "";
        public boolean isValid = true;
        public List<String> invalidTags = new ArrayList<String>();
    }

    public static String encode(String s) {
        return convertLineFeedToBR(htmlEncodeApexesAndTags(s == null ? "" : s));
    }

    public static final String htmlEncodeApexesAndTags(String source) {
        return htmlEncodeTag(htmlEncodeApexes(source));
    }

    public static final String htmlEncodeApexes(String source) {
        if (source != null) {
            String result = replaceAllNoRegex(source, new String[]{"\"", "'"}, new String[]{"&quot;", "&#39;"});
            return result;
        } else {
            return null;
        }
    }

    public static final String htmlEncodeTag(String source) {
        if (source != null) {
            String result = replaceAllNoRegex(source, new String[]{"<", ">"}, new String[]{"&lt;", "&gt;"});
            return result;
        } else {
            return null;
        }
    }

    public static String convertLineFeedToBR(String text) {
        if (text != null) {
            return replaceAllNoRegex(text, new String[]{"\n", "\f", "\r"}, new String[]{"<br>", "<br>", " "});
        } else {
            return null;
        }
    }

    public static String removeLineFeed(String text) {

        if (text != null) {
            return replaceAllNoRegex(text, new String[]{"\n", "\f", "\r"}, new String[]{" ", " ", " "});
        } else {
            return null;
        }
    }

    public static final String replaceAllNoRegex(String source, String searches[], String replaces[]) {
        int k;
        String tmp = source;
        for (k = 0; k < searches.length; k++) {
            tmp = replaceAllNoRegex(tmp, searches[k], replaces[k]);
        }
        return tmp;
    }

    public static final String replaceAllNoRegex(String source, String search, String replace) {
        StringBuffer buffer = new StringBuffer();
        if (source != null) {
            if (search.length() == 0) {
                return source;
            }
            int oldPos, pos;
            for (oldPos = 0, pos = source.indexOf(search, oldPos); pos != -1; oldPos = pos + search.length(), pos = source.indexOf(search, oldPos)) {
                buffer.append(source.substring(oldPos, pos));
                buffer.append(replace);
            }
            if (oldPos < source.length()) {
                buffer.append(source.substring(oldPos));
            }
        }
        return new String(buffer);
    }
}
