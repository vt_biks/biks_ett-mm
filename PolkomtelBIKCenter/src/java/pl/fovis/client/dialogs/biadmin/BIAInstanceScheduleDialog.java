/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.biadmin;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import pl.bssg.metadatapump.common.SAPBOScheduleBean;
import pl.fovis.common.i18npoc.I18n;
import static pl.fovis.foxygwtcommons.AdminConfigBaseWidget.createLine;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class BIAInstanceScheduleDialog extends BaseActionOrCancelDialog {

    IParametrizedContinuation<SAPBOScheduleBean> saveCont;
    protected VerticalPanel parametersVp;
    protected FlexTable emailFt;
    protected FlexTable filesFt;
    protected ListBox scheduleLbx;
    protected ListBox formatsLbx;
    protected ListBox destinationLbx;
    protected TextBox dayTb;
    protected TextBox monthTb;
    protected DateBox startDate;
    protected DateBox endDate;
    protected TextBox emailFrom;
    protected TextBox emailTo;
    protected TextBox emailSubject;
    protected TextArea emailBody;
    protected TextBox filesUserName;
    protected TextBox filesPassword;
    protected TextBox filesCatalog;
    protected FlexTable scheduleFt;
    protected RadioButton rbOldParams;
    protected RadioButton rbNewParams;

    public void buildAndShowDialog(IParametrizedContinuation<SAPBOScheduleBean> saveCont) {
        this.saveCont = saveCont;
        super.buildAndShowDialog();

    }

    @Override
    protected String getDialogCaption() {
        return I18n.scheduleHarmonogramujPonownie.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.scheduleHarmonogramuj.get();

    }

    @Override
    protected void buildMainWidgets() {

        rbOldParams = new RadioButton("r1", I18n.scheduleHarmonogramujZTymiSamymiParametrami.get());
        rbNewParams = new RadioButton("r1", I18n.scheduleHarmonogramujZInnymiParametrami.get());
        rbOldParams.setValue(true);
        parametersVp = new VerticalPanel();
//        parametersVp.setHeight("100%");
        parametersVp.setWidth("330px");
        main.setWidth("280px");
//        main.setHeight("500px");
        main.add(rbOldParams);
        main.add(rbNewParams);
        parametersPanel();
        ScrollPanel sc = new ScrollPanel();
        sc.add(parametersVp);
        sc.setWidth("340px");

        main.add(sc);

        rbOldParams.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                boolean isEnabled = false;
                scheduleLbx.setEnabled(isEnabled);
                formatsLbx.setEnabled(isEnabled);
                destinationLbx.setEnabled(isEnabled);
                scheduleLbx.setSelectedIndex(0);
                destinationLbx.setSelectedIndex(0);
                emailFt.setVisible(false);
                filesFt.setVisible(false);
                setScheduleParameters();
            }
        });

        rbNewParams.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                boolean isEnabled = true;
                scheduleLbx.setEnabled(isEnabled);
                formatsLbx.setEnabled(isEnabled);
                destinationLbx.setEnabled(isEnabled);
            }
        });

    }

    protected void parametersPanel() {
        parametersVp.clear();
        dayTb = new TextBox();
        dayTb.setValue("1");
        monthTb = new TextBox();
        monthTb.setValue("1");
        DateTimeFormat dateFormat = DateTimeFormat.getFormat("yyyy-MM-dd hh:mm:ss");
        startDate = new DateBox();
        startDate.setFormat(new DateBox.DefaultFormat(dateFormat));
        endDate = new DateBox();
        endDate.setFormat(new DateBox.DefaultFormat(dateFormat));
        FlexTable filterWidgets = new FlexTable();
        filterWidgets.setStyleName("biAdminSchedule");
        filterWidgets.setCellSpacing(5);
        // Cykl
        int row = 0;
        Label schedule = new Label(I18n.scheduleCykl.get());
//        schedule.setWidth("130px");
        schedule.setStyleName("biAdminScheduleBold");
        filterWidgets.setWidget(row, 0, schedule);
        filterWidgets.setWidget(row, 1, scheduleLbx = new ListBox());
        for (Schedule e : Schedule.values()) {
            scheduleLbx.addItem(e.name, String.valueOf(e.code));
        }
        scheduleLbx.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                setScheduleParameters();
            }
        });

        //Formaty
        FlexTable formatWidgets = new FlexTable();
        formatWidgets.setStyleName("biAdminSchedule");
        formatWidgets.setCellSpacing(5);
        row = 0;
        Label format = new Label(I18n.scheduleFormaty.get());
        format.setStyleName("biAdminScheduleBold");
        formatWidgets.setWidget(row, 0, format);
        formatsLbx = new ListBox();
        for (Formats e : Formats.values()) {
            formatsLbx.addItem(e.value);
        }
        formatWidgets.setWidget(row, 1, formatsLbx);
        scheduleFt = new FlexTable();
        scheduleFt.setStyleName("biAdminSchedule");
        formatWidgets.setCellSpacing(5);
        setScheduleParameters();

        //Miejsce docelowe
        FlexTable destinationWidgets = new FlexTable();
        destinationWidgets.setStyleName("biAdminSchedule");
        destinationWidgets.setCellSpacing(5);
        row = 0;
        Label destinationLbl = new Label(I18n.scheduleMiejsceDocelowe.get());
        destinationLbl.setStyleName("biAdminScheduleBold");
        destinationWidgets.setWidget(row, 0, destinationLbl);
        destinationLbx = new ListBox();
        for (Destination e : Destination.values()) {
            destinationLbx.addItem(e.value, e.code);
        }
        destinationWidgets.setWidget(row, 1, destinationLbx);
        createLine(parametersVp, "300");
//        parametersVp.add(new HTML("<hr style=\"width:100%;\" />"));
        parametersVp.add(filterWidgets);
        parametersVp.add(scheduleFt);
        createLine(parametersVp, "300");
//        parametersVp.add(new HTML("<hr style=\"width:100%;\" />"));
        parametersVp.add(formatWidgets);
        createLine(parametersVp, "300");
//        parametersVp.add(new HTML("<hr style=\"width:100%;\" />"));
        parametersVp.add(destinationWidgets);
        emailFt = emailWidget();

        filesFt = filesWidget();
        destinationLbx.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                int idx = destinationLbx.getSelectedIndex();
                emailFt.setVisible(idx == 1);
                filesFt.setVisible(idx == 2);
                recenterDialog();
            }
        });

        parametersVp.add(filesFt);
        parametersVp.add(emailFt);

        boolean isEnabled = false;
        scheduleLbx.setEnabled(isEnabled);
        formatsLbx.setEnabled(isEnabled);
        destinationLbx.setEnabled(isEnabled);
    }

    protected void setScheduleParameters() {

        scheduleFt.clear();
        int idx = scheduleLbx.getSelectedIndex();

        if (idx != 0) {
            int row = 0;

            if (idx == 2) {
                scheduleFt.setWidget(row, 0, new Label(I18n.scheduleDni.get()));
                scheduleFt.setWidget(row, 1, dayTb);
                row++;
            }
            if (idx == 3) {
                row = 0;
                scheduleFt.setWidget(row, 0, new Label(I18n.scheduleMiesiac.get()));
                scheduleFt.setWidget(row, 1, monthTb);
                row++;
            }
            scheduleFt.setWidget(row, 0, new Label(I18n.scheduleDataRozpoczecia.get()));
            scheduleFt.setWidget(row, 1, startDate);

            Label scheduleEndDate = new Label(I18n.scheduleDataZakonczenia.get());
            scheduleEndDate.setWidth("130px");
            scheduleFt.setWidget(++row, 0, scheduleEndDate);
            scheduleFt.setWidget(row, 1, endDate);

        }
    }

    protected FlexTable emailWidget() {

        emailFt = new FlexTable();
        emailFt.setStyleName("biAdminSchedule");
        emailFt.setCellSpacing(5);
        int row = 0;
        Label from = new Label(I18n.scheduleEmailOd.get());
        from.setWidth("130px");
        emailFt.setWidget(row, 0, from);
        emailFt.setWidget(row, 1, emailFrom = new TextBox());
        emailFt.setWidget(++row, 0, new Label(I18n.scheduleEmailDo.get()));
        emailFt.setWidget(row, 1, emailTo = new TextBox());
        emailFt.setWidget(++row, 0, new Label(I18n.scheduleEmailTemat.get()));
        emailFt.setWidget(row, 1, emailSubject = new TextBox());
        emailFt.setWidget(++row, 0, new Label(I18n.scheduleEmailKomunikat.get()));
        emailFt.setWidget(row, 1, emailBody = new TextArea());
        emailBody.setWidth("170px");
        emailBody.setHeight("120px");
        emailFt.setVisible(false);
        return emailFt;
    }

    protected FlexTable filesWidget() {
        int row = 0;
        filesFt = new FlexTable();
        filesFt.setStyleName("biAdminSchedule");
        filesFt.setCellSpacing(5);
        Label user = new Label(I18n.scheduleFilesNazwaUzytkownika.get());
        user.setWidth("130px");
        filesFt.setWidget(row, 0, user);
        filesFt.setWidget(row, 1, filesUserName = new TextBox());
        filesFt.setWidget(++row, 0, new Label(I18n.scheduleFilesHaslo.get()));
        filesFt.setWidget(row, 1, filesPassword = new TextBox());
        filesFt.setWidget(++row, 0, new Label(I18n.scheduleFilesKatalog.get()));
        filesFt.setWidget(row, 1, filesCatalog = new TextBox());
        filesFt.setVisible(false);
        return filesFt;
    }

    @Override
    protected void onEnterPressed() {
        // NO OP - żeby w treści mailu enter przechodizł do następnej linii zamiast klikać OK :)
    }

    @Override
    protected void doAction() {
        if (rbOldParams.getValue()) {
            saveCont.doIt(null);
        } else { // z parametrami
            SAPBOScheduleBean bean = new SAPBOScheduleBean();
            bean.type = BaseUtils.tryParseInt(scheduleLbx.getSelectedValue());
            bean.started = startDate.getValue();
            bean.ended = endDate.getValue();
            bean.intervalDays = BaseUtils.tryParseInt(dayTb.getValue());
            bean.intervalMonths = BaseUtils.tryParseInt(monthTb.getValue());
            bean.format = formatsLbx.getSelectedItemText();
            bean.destination = destinationLbx.getSelectedValue();
            // email
            bean.emailFromAddresses = emailFrom.getValue();
            bean.emailAddresses = emailTo.getValue();
            bean.emailTitle = emailSubject.getValue();
            bean.emailMessage = emailBody.getValue();
            // location
            bean.destinationUser = filesUserName.getValue();
            bean.destinationPw = filesPassword.getValue();
            bean.destinationFolder = filesCatalog.getValue();
            saveCont.doIt(bean);
        }
    }
}

enum Schedule {

    NOW(I18n.scheduleNow.get(), -1),
    ONCE(I18n.scheduleOnce.get(), 0),
    DAILY(I18n.scheduleDaily.get(), 2),
    MONTHLY(I18n.scheduleMonthly.get(), 4);
    public String name;
    public int code;

    Schedule(String value, int code) {
        this.name = value;
        this.code = code; // CeScheduleType.ONCE
    }
}

enum Formats {

    WEBI(I18n.scheduleWebi.get()),
    EXCEL(I18n.scheduleExcel.get()),
    PDF(I18n.schedulePdf.get());
    public String value;

    Formats(String value) {
        this.value = value;
    }
}

enum Destination {

    DEFAULT(I18n.scheduleDefault.get(), "Default"),
    EMAIL(I18n.scheduleEmail.get(), "Smtp"),
    FILES(I18n.scheduleFiles.get(), "DiskUnmanaged");
    public String value;
    public String code;

    Destination(String value, String code) {
        this.value = value;
        this.code = code;
    }
}
