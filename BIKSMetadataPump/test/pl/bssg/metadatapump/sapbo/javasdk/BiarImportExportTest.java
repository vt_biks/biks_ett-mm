/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.javasdk;

import com.businessobjects.sdk.biar.BIARException;
import com.businessobjects.sdk.biar.BIARFactory;
import com.businessobjects.sdk.biar.IExportCallback;
import com.businessobjects.sdk.biar.IExportOptions;
import com.businessobjects.sdk.biar.IImportCallback;
import com.businessobjects.sdk.biar.IImportOptions;
import com.businessobjects.sdk.biar.om.IManagedObjectIterator;
import com.businessobjects.sdk.biar.om.IObjectManager;
import com.businessobjects.sdk.biar.om.OMException;
import com.crystaldecisions.sdk.framework.CrystalEnterprise;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.framework.ISessionMgr;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;

/**
 *
 * @author tflorczak
 */
public class BiarImportExportTest {

    public static void main(String[] args) throws OMException {
        BiarImportExportTest test = new BiarImportExportTest();
        test.exportTOBIAR();
//        test.importFromBIAR();
    }

    public void importFromBIAR() throws OMException {
        IEnterpriseSession enterpriseSession = null;
        IManagedObjectIterator managedObjectIter = null;
        IObjectManager objectManager = null;
        try {
            ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
            enterpriseSession = sessionMgr.logon("Administrator", "Andrzej123BSSG4win", "192.168.168.102", "secEnterprise");
//            IInfoStore iStore = (IInfoStore) enterpriseSession.getService("InfoStore");
            BIARFactory biarFactory = BIARFactory.getFactory();

            objectManager = biarFactory.createOM(enterpriseSession);

            IImportOptions importOptions = biarFactory.createImportOptions();
            importOptions.setIncludeSecurity(true);
            importOptions.setFailUnresolvedCUIDs(true);

            importOptions.setCallback(new MyCallback());

            objectManager.importArchive("C:/temp/mybiar.biar", importOptions);
            objectManager.commit();

            managedObjectIter = objectManager.readAll();

            while (managedObjectIter.hasNext()) {
                IInfoObject infoObject = managedObjectIter.next();
                System.out.println("SI_CUID:" + infoObject.getCUID() + ", SI_NAME:" + infoObject.getTitle() + ",SI_KIND:" + infoObject.getKind());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            managedObjectIter.close();
            System.out.println(objectManager.size() + "objects processed.");
            objectManager.dispose();
        }
    }

    public void exportTOBIAR() {
        IEnterpriseSession enterpriseSession = null;
        try {
            ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
            enterpriseSession = sessionMgr.logon("Administrator", "Andrzej123BSSG4win", "192.168.168.102", "secEnterprise");
            IInfoStore iStore = (IInfoStore) enterpriseSession.getService("InfoStore");

            String whereQuery = "SI_ID in (5091, 36278, 44122)";

            IInfoObjects results = iStore.query("select top 100 * from ci_infoobjects, ci_appobjects where " + whereQuery);
            BIARFactory biarFactory = BIARFactory.getFactory();
            IObjectManager objectManager = biarFactory.createOM(enterpriseSession);

            objectManager.insertAll(results.iterator());

            IExportOptions exportOptions = biarFactory.createExportOptions();
            exportOptions.setIncludeSecurity(true);
            exportOptions.setFailUnresolvedIDs(true);

            exportOptions.setCallback(new MyCallback());

            try {
                objectManager.exportToArchive("C:/temp/mybiar3.biar", exportOptions);
            } catch (BIARException e) {
                e.printStackTrace();
            } finally {
                System.out.println(objectManager.size() + "objects processed.");
                objectManager.dispose();
            }
        } catch (Exception ex) {
            System.out.println("Błąd: " + ex.getMessage());
        } finally {
            if (enterpriseSession != null) {
                enterpriseSession.logoff();
            }
        }
    }

    class MyCallback implements IExportCallback, IImportCallback {

        @Override
        public void onSuccess(int id) {
            System.out.println("Object with ID " + id
                    + " was exported successfully.");
        }

        @Override
        public void onFailure(int id, BIARException cause) {
            System.out.println("Object with ID " + id + " failed to export: "
                    + cause.getMessage());
        }

        @Override
        public void onProgressUpdate(float f) {
            System.out.println("On progress: " + f);
        }
    }
}
