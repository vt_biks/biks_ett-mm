/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.dqm.DQMDataErrorIssueBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;
import simplelib.SimpleDateUtils;

/**
 *
 * @author ctran
 */
public class AddOrEditDQMDataErrorIssueDialog extends BaseActionOrCancelDialog {

    protected DQMDataErrorIssueBean issueBean;
    protected IParametrizedContinuation<DQMDataErrorIssueBean> saveCont;
    protected Label lblDataGroup;
    protected ListBox lbStatus;
    protected Label lblResposiblePerson;
    protected Label lblDate;
    protected TextBox tbAction;
    protected FlexTable grid;

    @Override
    protected String getDialogCaption() {
        return (issueBean.id == 0 ? I18n.dodajZgloszenie.get() : I18n.edytujZgloszenie.get());
    }

    @Override
    protected String getActionButtonCaption() {
        return (issueBean.id == 0 ? I18n.dodaj.get() : I18n.zapisz.get());
    }

    @Override
    protected void buildMainWidgets() {
        int row = 0;
        actionBtn.setEnabled(issueBean.leaderNodeId != 0);
        grid = new FlexTable();

        grid.setWidget(row, 0, new Label(I18n.status.get()));
        grid.setWidget(row++, 1, lbStatus = new ListBox());
        lbStatus.addItem(I18n.otwarty.get());
        lbStatus.addItem(I18n.zamkniety.get());
        lbStatus.setEnabled(issueBean.id != 0);
        lbStatus.setSelectedIndex(issueBean.errorStatus);

        grid.setWidget(row, 0, new Label(I18n.grupaDanych.get()));
        grid.setWidget(row++, 1, lblDataGroup = new Label(issueBean.groupName));

        HorizontalPanel hpLeader = new HorizontalPanel();
        final PushButton selectLeaderBtn;
        final Label lblLeader;
        hpLeader.add(lblLeader = new Label(issueBean.leaderName == null ? "" : issueBean.leaderName));

        selectLeaderBtn = new PushButton(new Image("images/edit_yellow.png" /* I18N: no */), new Image("images/edit_yellow.png" /* I18N: no */));
        selectLeaderBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new TreeSelectorForDQMLeader().buildAndShowDialog(null, new IParametrizedContinuation<TreeNodeBean>() {

                    @Override
                    public void doIt(TreeNodeBean param) {
                        lblLeader.setText(param.name);
                        issueBean.leaderNodeId = param.linkedNodeId != null ? param.linkedNodeId : param.id;
                        actionBtn.setEnabled(true);
                        selectLeaderBtn.setVisible(false);
                    }
                });
            }
        });
        selectLeaderBtn.setStyleName("admin-btn");
        selectLeaderBtn.setWidth("16px");
        selectLeaderBtn.setTitle(I18n.wybierzUzytkownika.get() /* I18N:  */);
        selectLeaderBtn.setVisible(issueBean.leaderNodeId == 0);

        hpLeader.add(selectLeaderBtn);
        grid.setWidget(row, 0, new Label(I18n.opiekunGrupyDanych.get()));
        grid.setWidget(row++, 1, hpLeader);

        grid.setWidget(row, 0, new Label(I18n.data.get()));
        grid.setWidget(row++, 1, lblDate = new Label(SimpleDateUtils.dateTimeToString(issueBean.createdDate, "-", false)));

        grid.setWidget(row, 0, new Label(I18n.akcje.get()));
        grid.setWidget(row++, 1, tbAction = new TextBox());
        tbAction.setText(issueBean.actionPlan);
        main.add(grid);
    }

    @Override
    protected void doAction() {
        issueBean.actionPlan = tbAction.getText();
        issueBean.errorStatus = lbStatus.getSelectedIndex();
        saveCont.doIt(issueBean);
    }

    public void buildAndShowDialog(DQMDataErrorIssueBean bean, IParametrizedContinuation<DQMDataErrorIssueBean> saveCont) {
        issueBean = bean;
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }
}
