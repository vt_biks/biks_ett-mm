﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- nowe generyczne funkcjonalności :)
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_check_version '1.6.z6.2';
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_node_kind]') and name = 'children_kinds')
begin
	alter table bik_node_kind add children_kinds varchar(900) null;
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_node_kind]') and name = 'children_attr_names')
begin
	alter table bik_node_kind add children_attr_names varchar(max) null;
end;
go

if not exists(select 1 from bik_app_prop where name = 'sendEmailNotifications')
begin
	insert into bik_app_prop(name,val, is_editable)
	values('sendEmailNotifications', 'false', 1)

	insert into bik_app_prop(name,val, is_editable)
	values('emailFrom', '', 1)

	insert into bik_app_prop(name,val, is_editable)
	values('emailPassword', '', 1)

	insert into bik_app_prop(name,val, is_editable)
	values('emailSmtpHostName', '', 1)

	insert into bik_app_prop(name,val, is_editable)
	values('emailSmtpPort', '', 1)

	insert into bik_app_prop(name,val, is_editable)
	values('emailSsl', '', 1)

	insert into bik_app_prop(name,val, is_editable)
	values('emailUser', '', 1)
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_attribute]') and name = 'default_value')
begin
	alter table bik_attribute add default_value varchar(max) null;
end;
go

-- poprzednia wersja w "alter db for v1.6.z4 tf.sql"
-- fix na 'ex_prorp'
if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_verticalize_node_attrs_add_attrs') and type in (N'P', N'PC'))
drop procedure dbo.sp_verticalize_node_attrs_add_attrs
go

create procedure dbo.sp_verticalize_node_attrs_add_attrs(@optNodeFilter varchar(max))
as
begin
  set nocount on

  declare @diags_level int = 0 -- 0 oznacza brak logowania

  --declare @attr_names table (attr_id int not null unique, name varchar(255) not null unique, is_deleted int not null)
  
  --declare @attr_names_sql varchar(max) = 'select ad.id, ad.name, ad.is_deleted from bik_attr_def ad where is_built_in=0 /*where ad.is_deleted = 0*/'
  
  /*
  if (@optNodeFilter is not null) begin
    set @attr_names_sql = @attr_names_sql + ' and ad.id in (select a.attr_def_id from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id
    inner join bik_node n on al.node_id = n.id where ' + @optNodeFilter + ')'
  end
  */
  
  --insert into @attr_names (attr_id, name, is_deleted)
  --exec(@attr_names_sql)
  
  --select * from @attr_names
  
  --declare attr_names_cur cursor for select attr_id, name, is_deleted from @attr_names
  declare attr_names_cur cursor for select ad.id, ad.name, ad.is_deleted from bik_attr_def ad where is_built_in = 0 and is_deleted = 0 and exists (select 1 from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id where a.attr_def_id = ad.id and al.is_deleted = 0 and a.is_deleted = 0)
  
  declare @attr_id int, @name varchar(255), @is_deleted int
  
  open attr_names_cur
  
  fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  
  --return
  
  while @@fetch_status = 0 begin
    declare @attr_source_sql varchar(max) = '(select node_id, value from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id 
    where ' + case when @is_deleted = 0 then '' else '1 = 0 and ' end + 'al.is_deleted = 0 and a.is_deleted = 0 and a.attr_def_id = ' + cast(@attr_id as varchar(20)) + ')'
    
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': before verticalize one, @name=' + @name + ', source_sql=' + @attr_source_sql

	--set @name = '$exprop_' + @name;
    exec sp_verticalize_node_attrs_one_table @attr_source_sql, 'node_id', 'value', @name, @optNodeFilter    
  
    fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  end
  
  close attr_names_cur
  
  deallocate attr_names_cur
end
go

-- fix na $exprop_
delete from bik_searchable_attr_val where attr_id in (select id from bik_searchable_attr where name like '$exprop_%')

exec sp_verticalize_node_attrs_add_attrs null

exec sp_verticalize_node_attrs_ex_props null
-- end fix


if not exists(select 1 from bik_app_prop where name = 'myObjectsTree')
begin
	insert into bik_app_prop(name,val, is_editable)
	values('myObjectsTree', '', 1)

	insert into bik_app_prop(name,val, is_editable)
	values('myObjectsNodeKinds', '', 1)
	
	insert into bik_app_prop(name,val, is_editable)
	values('myObjectsInRole', '', 1)
	
	insert into bik_app_prop(name,val, is_editable)
	values('myObjectsAttributes', '', 1)
end;
go

--------------------------------------------
--------------------------------------------
--------------------------------------------

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_node_kind]') and name = 'attr_calculating_types')
begin
	alter table bik_node_kind add attr_calculating_types varchar(max) null;
end
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_node_kind]') and name = 'attr_calculating_expressions')
begin
	alter table bik_node_kind add attr_calculating_expressions varchar(max) null;
end
go

--------------------------------------------
--------------------------------------------
--------------------------------------------

if OBJECT_ID('sp_calc_formula_on_attr_vals') is not null
  drop proc sp_calc_formula_on_attr_vals;
go

create procedure sp_calc_formula_on_attr_vals(@node_id int) as
begin
  set nocount on;

  --, @formula varchar(max)

  declare @node_kind_id int = (select node_kind_id from bik_node where id = @node_id);

  --declare @sep_pos int = charindex(':::', @formula);
  
  -- declare @cols_def varchar(max) = rtrim(ltrim(substring(@formula, 1, @sep_pos - 1)));
  declare @cols_def varchar(max), @formula_expr varchar(max);
  
  select @cols_def = rtrim(ltrim(attr_calculating_types)), @formula_expr = rtrim(ltrim(attr_calculating_expressions))
  from bik_node_kind where id = @node_kind_id;

  if @formula_expr is null or @formula_expr = ''
    return;
  
  declare @temp_tab_name_base sysname = '##biks_calc_formula_on_attr_vals__' + replace(convert(varchar(36),NEWID()), '-', '_');
  declare @temp_tab_name_src sysname = case when @cols_def is null or @cols_def = '' then null else @temp_tab_name_base + '_src' end;
  
  if @temp_tab_name_src is not null
  begin
	  
	  -- declare @formula_expr varchar(max) = ltrim(rtrim(substring(@formula, @sep_pos + 3, len(@formula))));
	  -- declare @formula_expr varchar(max) = ltrim(rtrim((select attr_calculating_expressions from bik_node_kind where id = @node_kind_id)));
	   
	  declare @create_temp_tab_sql varchar(max) = 'create table ' + @temp_tab_name_src + ' (node_id int, ' + @cols_def + ')';
	  
	  --print @create_temp_tab_sql;
	  exec(@create_temp_tab_sql);

	  declare @insert_node_id_sql varchar(max) = 'insert into ' + @temp_tab_name_src + ' (node_id) values (' + cast(@node_id as varchar(20)) + ')';
	  exec(@insert_node_id_sql)
	 
	  declare @temp_tab_id int = object_id('tempdb..' + @temp_tab_name_src);
	 
	  declare @tempdb_schema_name sysname = (select ss.name from tempdb.sys.objects so inner join tempdb.sys.schemas ss on so.schema_id = ss.schema_id
		where so.object_id = @temp_tab_id);

	  --declare @cnt int = 0;
	 
	  declare @col_name sysname, @type_name varchar(max);  
	 
	  declare cur cursor for 
	  SELECT 
			column_name, 
			data_type + case data_type
				when 'sql_variant' then ''
				when 'text' then ''
				when 'ntext' then ''
				when 'xml' then ''
				when 'decimal' then '(' + cast(numeric_precision as varchar) + ', ' + cast(numeric_scale as varchar) + ')'
				else coalesce('('+case when character_maximum_length = -1 then 'max' else cast(character_maximum_length as varchar) end +')','') end        
		   from tempdb.information_schema.columns where table_name = @temp_tab_name_src and table_schema = @tempdb_schema_name
		   order by ordinal_position;

	  open cur;

	  fetch next from cur into @col_name, @type_name;

	  while @@fetch_status = 0
	  begin    
		declare @col_name_quoted varchar(max) = quotename(@col_name);
		declare @val_expr varchar(max) = 'cast(value as ' + @type_name + ')';
	    
		-- tu można zmienić @val_expr na inną konwersję w zależności od @type_name
		
		--print @col_name_quoted + ' ' + @type_name;

		declare @conv_one_val_sql varchar(max);
	    
		declare @attribute_id int = (select a.id from bik_attr_def ad inner join bik_attribute a on ad.id = a.attr_def_id where ad.name = @col_name and a.node_kind_id = @node_kind_id);

		declare @select_val_conv_sql varchar(max) = 'select ' + @val_expr + ' from bik_attribute_linked where node_id = ' + cast(@node_id as varchar(10)) + ' and attribute_id = ' + cast(@attribute_id as varchar(10));
	    
		set @conv_one_val_sql = 'update ' + @temp_tab_name_src + ' set ' + @col_name_quoted + ' = (' + @select_val_conv_sql + ')';
	    
		--print @conv_one_val_sql;
	    
		exec(@conv_one_val_sql);
	      
		--set @cnt = @cnt + 1;
		fetch next from cur into @col_name, @type_name;
	  end;
	 
	  close cur;
	  deallocate cur;
  end;
	 
  declare @temp_tab_name_dst sysname = @temp_tab_name_base + '_dst';
  
  declare @select_from_temp_sql varchar(max) = 'select ' + @formula_expr + ' into ' + @temp_tab_name_dst + case when @temp_tab_name_src is null then '' else ' from ' + @temp_tab_name_src end;
  --print @select_from_temp_sql;
  exec(@select_from_temp_sql);
  
  declare @temp_tab_id_dst int = object_id('tempdb..' + @temp_tab_name_dst);
 
  declare @tempdb_schema_name_dst sysname = (select ss.name from tempdb.sys.objects so inner join tempdb.sys.schemas ss on so.schema_id = ss.schema_id
	where so.object_id = @temp_tab_id_dst);

  --declare @cnt int = 0;
 
  declare @col_name_dst sysname, @type_name_dst varchar(max);  
 
  declare cur_dst cursor for 
  SELECT 
		column_name, 
		data_type
	   from tempdb.information_schema.columns where table_name = @temp_tab_name_dst and table_schema = @tempdb_schema_name_dst
	   order by ordinal_position;

  open cur_dst;

  fetch next from cur_dst into @col_name_dst, @type_name_dst;

  while @@fetch_status = 0
  begin
	declare @convert_dst_expr nvarchar(max) = 'cast(' + quotename(@col_name_dst) + ' as varchar(max))';
	
	-- tu opcjonalnie inne konwersje zależnie od typu @type_name_dst
	
	declare @ssql nvarchar(max) = 'select @retvalout=' + @convert_dst_expr + ' from ' + @temp_tab_name_dst;
		
	declare @retval varchar(max);   
	declare @param_def nvarchar(500) = '@retvalout varchar(max) output';

	exec sp_executesql @ssql, @param_def, @retvalout=@retval output;

    declare @attr_def_id_dst int = (select id from bik_attr_def where name = @col_name_dst);

    if @attr_def_id_dst is not null
    begin
	  declare @attribute_id_dst int = (select id from bik_attribute where node_kind_id = @node_kind_id and attr_def_id = @attr_def_id_dst);     
    
      if @retval is null or @retval = ''
      begin
        delete from bik_attribute_linked 
		where attribute_id = @attribute_id_dst and node_id = @node_id;
      end
      else
      begin
        update bik_attribute_linked 
		set value = @retval, is_deleted = 0
		where attribute_id = @attribute_id_dst and node_id = @node_id;
	      
		if @@rowcount = 0
		begin
		  insert into bik_attribute_linked (attribute_id, node_id, value, is_deleted)
		  values (@attribute_id_dst, @node_id, @retval, 0);
		end;
      end;
            
      --select top 10 * from bik_attribute_linked
    end;
    
    fetch next from cur_dst into @col_name_dst, @type_name_dst;
  end;
  
  close cur_dst;
  deallocate cur_dst;
  
  if @temp_tab_name_src is not null
  begin
    declare @drop_dyn_tab_sql varchar(max) = 'drop table ' + @temp_tab_name_src;
    exec(@drop_dyn_tab_sql);
  end;

  declare @drop_dyn_tab_sql_dst varchar(max) = 'drop table ' + @temp_tab_name_dst;
  exec(@drop_dyn_tab_sql_dst);
end;
go

--------------------------------------------
--------------------------------------------
--------------------------------------------

if not exists(select 1 from bik_app_prop where name = 'additionalDocumentKinds')
begin
	insert into bik_app_prop(name,val, is_editable)
	values('additionalDocumentKinds', '', 1)
end;
go

if not exists (select 1 from sys.tables where name = 'bik_node_kind_4_tree_kind')
begin
	create table bik_node_kind_4_tree_kind (
		id int identity(1,1) not null primary key,
		tree_kind_id int not null references bik_tree_kind(id),
		node_id int not null references bik_node_kind(id),
		is_branch int not null default 0,
		is_leaf int not null default 0
	);
end;
go


exec sp_update_version '1.6.z6.2', '1.6.z6.3';
go