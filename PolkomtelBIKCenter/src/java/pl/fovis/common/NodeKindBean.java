/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.util.List;
import simplelib.BeanWithIntIdBase;

/**
 *
 * @author wezyr
 */
public class NodeKindBean extends BeanWithIntIdBase {

    public String code;
    public String caption;
    public String nodeKindCaption;
    public String iconName;
    public String treeKind;
    public boolean isFolder;
    public boolean isBranch;
    public boolean isLeaf;
    public int searchRank;
    public boolean isLeafforStatistic;
    public String childrenKinds;
    public String childrenAtrNames;
    public String atrCalculatingTypes;
    public String atrCalculatingExpresions;
    public String atrDependentNodesSql;
//    public String uploadableChhildrenKinds;
    public String genericNodeSelect;
    public String sql;
    public String jasperReports;
    public int isDeleted;
    public boolean isRegistry;
    public String attrControlNodeName;
    public String defaultDesc;
    public List<SystemUserGroupBean> associatedRoles;
}
