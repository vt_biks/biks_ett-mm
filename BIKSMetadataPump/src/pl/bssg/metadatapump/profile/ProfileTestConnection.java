/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.profile;

import commonlib.LameUtils;
import commonlib.ftp.FTP4JFoxyFTPClient;
import commonlib.ftp.IFoxyFTPClient;
import pl.bssg.metadatapump.MetadataTestConnectionBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class ProfileTestConnection extends MetadataTestConnectionBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected String path;

    public ProfileTestConnection(MssqlConnectionConfig mcfg) {
        super(mcfg);
    }

    @Override
    public Pair<Integer, String> testConnection() {
        try {
            ProfileConfigBean config = PumpUtils.readAdminBean("profile", ProfileConfigBean.class, adhocDao);
            IFoxyFTPClient client = new FTP4JFoxyFTPClient();//SFTP4JFoxyFTPClient();
            client.connectAndLogin(config.host, BaseUtils.tryParseInteger(config.port, 21), config.user, config.password);
            client.changeDirectory(config.path);
            client.disconnect();
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in Profile test connection", e);
            }
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Error with connecting with FTP: " + e.getMessage());
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, PumpConstants.PUMP_TEST_CONNECTION_OK);
    }
}
