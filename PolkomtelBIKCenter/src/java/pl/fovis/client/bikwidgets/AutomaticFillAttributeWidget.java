/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author bfechner
 */
public class AutomaticFillAttributeWidget {

    public Widget buildWidgets() {
        TreeSelectorForAutomaticFillAttributeWidget m = new TreeSelectorForAutomaticFillAttributeWidget();
        VerticalPanel f=m.buildMainWidgets();
        FlowPanel fp = new FlowPanel();
        fp.add(f);
        return fp;
    }
}
