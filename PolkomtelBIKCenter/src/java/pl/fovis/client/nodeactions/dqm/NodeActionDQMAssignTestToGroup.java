/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.dqm;

import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.dqm.TreeSelectorForDQMTests;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class NodeActionDQMAssignTestToGroup extends NodeActionDQMBase {

    @Override
    public boolean isEnabledByDesign() {
        return node != null && isInDQMAdminMode() && isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_DQM_GROUP);
    }

    @Override
    public String getCaption() {
        return I18n.przypiszTest.get();
    }

    @Override
    public void execute() {
        BIKClientSingletons.getDQMService().getAlreadyAssignedDQMTest(node.id, new StandardAsyncCallback<Set<Integer>>("Error in getAlreadyAssignedDQMTest") {

            @Override
            public void onSuccess(Set<Integer> result) {
                new TreeSelectorForDQMTests().buildAndShowDialog(node.id, result, new IParametrizedContinuation<TreeNodeBean>() {

                    @Override
                    public void doIt(TreeNodeBean param) {
                        BIKClientSingletons.getDQMService().assignDQMTestToGroup(param.linkedNodeId != null ? param.linkedNodeId : param.id, node.id, param.name, new StandardAsyncCallback<Integer>("Error in assignDQMTestToGroup") {

                            @Override
                            public void onSuccess(Integer result) {
                                BIKClientSingletons.showInfo(I18n.przypisanoTestDoGrupy.get());
                                refreshRedisplayDataAndSelectRecord(result);
                            }
                        });
                    }
                });
            }
        });
    }
}
