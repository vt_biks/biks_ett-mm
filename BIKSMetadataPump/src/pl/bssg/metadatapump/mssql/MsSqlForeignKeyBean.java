/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.mssql;

/**
 *
 * @author tflorczak
 */
public class MsSqlForeignKeyBean {

    public String name;
    public String description;
    public String server;
    public String databaseName;
    public String fkTable;
    public String fkSchema;
    public String fkColumn;
    public String pkTable;
    public String pkSchema;
    public String pkColumn;

    public MsSqlForeignKeyBean() {
    }
}
