/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.common;

import java.io.Serializable;

/**
 *
 * @author pmielanczuk
 */
public class DbLoginBean implements Serializable {

    public String name;
    public String login;
    public String pwd;

    public DbLoginBean() {
    }

    public DbLoginBean(String name, String login, String pwd) {
        this.name = name;
        this.login = login;
        this.pwd = pwd;
    }
}
