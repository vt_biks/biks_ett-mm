/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.ConnectionParametersProfileBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class AdminConfigProfileWidget extends AdminConfigBIKBaseWidget<ConnectionParametersProfileBean> {

    protected CheckBox chbIsActive;
    protected PushButton btnSave;
    protected TextBox tbxPath;
    protected TextBox tbxTestPath;
    protected TextBox tbxHost;
    protected TextBox tbxPort;
    protected TextBox tbxUser;
    protected TextBox tbxPassword;
//    protected CheckBox chbPassword;

    @Override
    protected void buildInnerWidget() {
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
            }
        }, false);
        createLine();
        createLabel(I18n.ftpMetadataFolder.get() + ":");
        tbxPath = createTextBox();
        createLabel(I18n.ftpTestFolder.get() + ":");
        tbxTestPath = createTextBox();
        createLabel(I18n.ftpHost.get() + ":");
        tbxHost = createTextBox();
        createLabel(I18n.ftpPort.get() + ":");
        tbxPort = createTextBox();
        createLabel(I18n.ftpUser.get() + ":");
        tbxUser = createTextBox();
        createLabel(I18n.ftpPass.get() + ":");
        tbxPassword = createPasswordTextBox();
//        chbPassword = createAndAddCheckBox(I18n.ukryjHaslo.get(), new ValueChangeHandler<Boolean>() {
//            @Override
//            public void onValueChange(ValueChangeEvent<Boolean> event) {
//                tbxPassword.getElement().setAttribute("type" /* I18N: no */, chbPassword.getValue() ? "password" /* I18N: no */ : "text" /* I18N: no */);
//            }
//        });
        createLine();
        btnSave = createButton(I18n.zapisz.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ConnectionParametersProfileBean bean = new ConnectionParametersProfileBean();
                bean.isActive = chbIsActive.getValue() == true ? 1 : 0;
                bean.path = tbxPath.getText();
                bean.testPath = tbxTestPath.getText();
                bean.host = tbxHost.getText();
                bean.port = tbxPort.getText();
                bean.user = tbxUser.getText();
                bean.password = tbxPassword.getText();
                BIKClientSingletons.getService().setProfileConnectionParameters(bean, new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " setProfileConnectionParameters") {
                            @Override
                            public void onSuccess(Void result) {
                                BIKClientSingletons.showInfo(I18n.zmienionParametrDoPolaczenZProfile.get() /* I18N:  */);
                            }
                        });
            }
        });
        createTestConnectionWidget(I18n.testujPolaczenie.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().runProfileTestConnection(new StandardAsyncCallback<Pair<Integer, String>>("Error in" /* I18N: no */ + " runProfileTestConnection") {
                            @Override
                            public void onSuccess(Pair<Integer, String> result) {
                                setResultTestConnection(result);
                            }
                        });
            }
        });
    }

    @Override
    public void populateWidgetWithData(ConnectionParametersProfileBean data) {
        tbxPath.setText(data.path);
        tbxTestPath.setText(data.testPath);
        tbxHost.setText(data.host);
        tbxPort.setText(data.port);
        tbxUser.setText(data.user);
        tbxPassword.setText(data.password);
        chbIsActive.setValue(data.isActive == 1);
        setWidgetsEnabled(chbIsActive.getValue());
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_PROFILE;
    }
}
