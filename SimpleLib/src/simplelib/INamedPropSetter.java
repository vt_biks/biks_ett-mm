/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

/**
 *
 * @author wezyr
 */
public interface INamedPropSetter<Bean> {
    public void setValue(Bean o, Object val);
}
