/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import pl.fovis.common.TreeNodeBean;
import pl.fovis.foxygwtcommons.ITreeBroker;

/**
 *
 * @author wezyr
 */
public class TreeNodeBroker implements ITreeBroker<TreeNodeBean, Integer> {

    public Integer getNodeId(TreeNodeBean n) {
        return n.id;
    }

    public Integer getParentId(TreeNodeBean n) {
        return n.parentNodeId;
    }
}
