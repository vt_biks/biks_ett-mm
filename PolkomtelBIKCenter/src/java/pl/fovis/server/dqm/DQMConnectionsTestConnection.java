/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import commonlib.LameUtils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.MetadataTestConnectionBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.mssql.IMsSqlQueryNameProvider;
import pl.bssg.metadatapump.mssql.MsSql2000QueryNameProvider;
import pl.bssg.metadatapump.mssql.MsSql2005QueryNameProvider;
import pl.bssg.metadatapump.mssql.MsSql2008QueryNameProvider;
import pl.bssg.metadatapump.mssql.MsSqlBean;
import pl.bssg.metadatapump.oracle.OraclePump;
import pl.bssg.metadatapump.postgresql.PostgreSQLPump;
import static pl.bssg.metadatapump.postgresql.PostgreSQLPump.POSTGRES_DEFAULT_DATABASE_NAME;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.commons.PostgresConnectionConfig;
import pl.trzy0.foxy.serverlogic.DriverNameConstants;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.ConnectionUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import pl.trzy0.foxy.serverlogic.db.OracleConnection;
import pl.trzy0.foxy.serverlogic.db.OracleConnectionConfig;
import pl.trzy0.foxy.serverlogic.db.PostgresConnection;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author bfechner
 */
public class DQMConnectionsTestConnection extends MetadataTestConnectionBase {

    protected ConnectionParametersDQMConnectionsBean bean;

    protected int id;
    protected String database;
    protected INamedSqlsDAO<Object> adhocDaoMSSQL;
    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected Connection tc = null;
    protected MssqlConnection bce;
    protected Connection oc;
    protected Boolean showGrantsInTestConnection;

    public DQMConnectionsTestConnection(MssqlConnectionConfig mcfg, int id, String optObjsForGrantsTesting) {
        super(mcfg);
        this.id = id;

    }

    @Override
    public Pair<Integer, String> testConnection() {
        try {
            bean = adhocDao.createBeanFromNamedQry("getDQMConnectionParametersById", ConnectionParametersDQMConnectionsBean.class, true,/* PumpConstants.SOURCE_NAME_MSSQL,*/ id);
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Invalid login for data loading! Error: " + e.getMessage());
        }
        showGrantsInTestConnection = BaseUtils.tryParseBoolean((String) execNamedQuerySingleVal("getAppPropValue", true, "val", "showGrantsInTestConnection"), false);

        String dataSource = bean.sourceType.trim();
        if (dataSource.equalsIgnoreCase(PumpConstants.SOURCE_NAME_MSSQL)) {
            return mssqlTestConnection();
        } else if (dataSource.equalsIgnoreCase(PumpConstants.SOURCE_NAME_ORACLE)) {
            return oracleTestConnection();
        } else if (dataSource.equalsIgnoreCase(PumpConstants.SOURCE_NAME_POSTGRESQL)) {
            return postgreSqlTestConnection();
        } else if (dataSource.equalsIgnoreCase(PumpConstants.SOURCE_NAME_TERADATA)) {
            return teradataTestConnection();
        }

        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "");
    }

    protected Pair<Integer, String> mssqlTestConnection() {
        String serverVersion = "";
        try {

            database = "master";
            List<String> databasesFilterDatabases = BaseUtils.splitBySep(bean.databaseName, ",", true);
            boolean notConnectingWithSystemDB = !databasesFilterDatabases.isEmpty();
            if (notConnectingWithSystemDB) {
                database = databasesFilterDatabases.get(0);
            }
            bce = new MssqlConnection(
                    new MssqlConnectionConfig(bean.server, bean.instance, database, bean.user, bean.password));
            if (bce == null) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connecting with MSSQL DB: " + database);
            }
            List<Map<String, Object>> execQry = bce.execQry("select cast(serverproperty('productversion') as char) as version");
            if (execQry == null || execQry.isEmpty()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No access to database: " + database);
            }
            // sprawdzam skladnie
            adhocDaoMSSQL = FoxyAppUtils.createNamedSqlsDAOFromJar(bce, ADHOC_SQLS_PATH);
            serverVersion = BeanConnectionUtils.execNamedQuerySingleVal(adhocDaoMSSQL, "getMSSQLServerVersion", false, "version");
            IMsSqlQueryNameProvider queryNameProvider;
            if (serverVersion.startsWith("8.")) { // 2000
                queryNameProvider = new MsSql2000QueryNameProvider();
            } else if (serverVersion.startsWith("9.")) { // 2005
                queryNameProvider = new MsSql2005QueryNameProvider();
            } else if (serverVersion.startsWith("10.")) { // 2008 & 2008 R2
                queryNameProvider = new MsSql2008QueryNameProvider();
            } else if (serverVersion.startsWith("11.")) { // 2012
                queryNameProvider = new MsSql2008QueryNameProvider();
            } else {
                queryNameProvider = new MsSql2008QueryNameProvider();
            }
            List<MsSqlBean> databases;
            if (notConnectingWithSystemDB) {
                databases = new ArrayList<MsSqlBean>();
                for (String dbF : databasesFilterDatabases) {
                    MsSqlBean msBean = new MsSqlBean();
                    msBean.name = dbF;
                    databases.add(msBean);
                }
            } else { // pobierz z bazy master
                databases = adhocDaoMSSQL.createBeansFromNamedQry(queryNameProvider.getDatabases(), MsSqlBean.class, BaseUtils.splitBySep(""/*bean.databaseFilter*/, ","));
            }

            if (databases == null || databases.isEmpty()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Lack of database!");
            }
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in MS SQL Test Connection", e);
            }
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connecting with MSSQL DB! Error: " + e.getMessage());
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, "OK, version: " + serverVersion);
    }

    protected Pair<Integer, String> oracleTestConnection() {
        try {
            StringBuilder url = new StringBuilder();
            url.append("jdbc:oracle:thin:@//").append(bean.server).append(":").append(bean.port).append("/").append(bean.instance);
//            conn = PumpUtils.getConnection(OraclePump.DRIVER_CLASS, url.toString(), bean.user, bean.password);
            Class.forName(OraclePump.DRIVER_CLASS);
            oc = DriverManager.getConnection(url.toString(), bean.user, bean.password);
            if (oc == null) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No conencting with Oracle DB!");
            }
            if (showGrantsInTestConnection) {
                OracleConnection oc = new OracleConnection(new OracleConnectionConfig(bean.server, bean.instance, bean.user, bean.password, bean.port));
            }
            Statement statment = oc.createStatement();
//            ResultSet execQry = statment.executeQuery("select banner as version from v$version where banner like 'Oracle%'");
            ResultSet execQry = statment.executeQuery("select * from all_tables where rownum = 1");

            if (execQry == null || !execQry.next()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No access to system tables: all_tables");
            }
//            String version = execQry.getString("version");
            if (showGrantsInTestConnection) {
                StringBuilder sbGrant = new StringBuilder();
                ResultSet grants = statment.executeQuery("select * from all_tab_privs_recd where grantee = '" + bean.user + "'");
//                sbGrant.append(database).append(": \n");
                while (grants.next()) {
                    String grantee = grants.getString("table_name");
                    String type = grants.getString("privilege_type");
                    sbGrant.append("   ").append(grantee).append(" - ").append(type).append("\n");
                }
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, "OK\n" + /* + version +*/ " \nGrants:\n" + sbGrant.toString());
            }
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connecting with Oracle DB! Error: " + e.getMessage());
        } finally {
            if (oc != null) {
                PumpUtils.closeConnection(oc);
            }
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, PumpConstants.PUMP_TEST_CONNECTION_OK);
    }

    public Pair<Integer, String> postgreSqlTestConnection() {
        String version = "";
        try {
            String database = POSTGRES_DEFAULT_DATABASE_NAME;
            List<String> databasesFilterDatabases = BaseUtils.splitBySep(bean.databaseName, ",", true);
            boolean notConnectingWithSystemDB = !databasesFilterDatabases.isEmpty();
            if (notConnectingWithSystemDB) {
                database = databasesFilterDatabases.get(0);
            }
            oc = PumpUtils.getConnection(PostgreSQLPump.DRIVER_CLASS, "jdbc:postgresql://" + bean.server + (bean.port != null ? ":" + bean.port : "") + "/" + database, bean.user, bean.password);
            if (oc == null) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connecting with Postgres DB: " + database);
            }
            if (showGrantsInTestConnection) {
                PostgresConnection pc = new PostgresConnection(new PostgresConnectionConfig(bean.server, database, bean.user, bean.password, bean.port));
            }
            Statement statment = oc.createStatement();
            String query;
//            if (notConnectingWithSystemDB) {
            query = "select version() as version;";
//            } else {
//                query = "select datname from pg_database where datistemplate = false";
//            }
            ResultSet execQry = statment.executeQuery(query);
            if (execQry == null || !execQry.next()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No access to database: " + database);
            }
            version = execQry.getString("version");
            if (showGrantsInTestConnection) {
                StringBuilder sbGrant = new StringBuilder();
                ResultSet grants = statment.executeQuery("SELECT table_name, privilege_type \n"
                        + "FROM information_schema.role_table_grants \n"
                        + "where grantee = '" + bean.user + "'");
                sbGrant.append(database).append(": \n");
                while (grants.next()) {
                    String grantee = grants.getString("table_name");
                    String type = grants.getString("privilege_type");
                    sbGrant.append("   ").append(grantee).append(" - ").append(type).append("\n");
                }
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, "OK, version: " + version + " \nGrants:\n" + sbGrant.toString());
            }
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error with PostreSQL test connection", e);
            }
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connecting with Postgres DB! Error: " + e.getMessage());
        } finally {
            if (oc != null) {
                PumpUtils.closeConnection(oc);
            }
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, "OK, version: " + version);

    }

    protected Pair<Integer, String> teradataTestConnection() {

        try {
//            tc = PumpUtils.getConnection(teradataDriverClass, teradataUrl + "/tmode=ANSI,charset=UTF8", teradataUser, teradataPassword);
//            Class.forName(teradataDriverClass);
            String url = "jdbc:teradata://" + bean.server;
            tc = ConnectionUtils.getConnectionByConnectionString(DriverNameConstants.TERADATA, url, bean.user, bean.password, "tmode=ANSI,charset=UTF8");
            if (tc == null) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connection with Teradata!");
            }
            Statement createStatement = tc.createStatement();
            ResultSet executeQuery = createStatement.executeQuery("select top 1 * from DBC.TablesV");
            if (executeQuery == null || !executeQuery.next()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Wrong select query or no access");
            }
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connection with Teradata! Error: " + e.getMessage());
        } finally {
            if (tc != null) {
                PumpUtils.closeConnection(tc);
            }
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, PumpConstants.PUMP_TEST_CONNECTION_OK);
    }
}
