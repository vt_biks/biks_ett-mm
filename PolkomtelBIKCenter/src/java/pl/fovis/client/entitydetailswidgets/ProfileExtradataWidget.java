/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.bssg.metadatapump.common.ProfileBean;
import pl.bssg.metadatapump.common.ProfileFileBean;
import pl.fovis.common.EntityDetailsDataBean;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class ProfileExtradataWidget extends EntityDetailsWidgetFlatBase {

    protected IMainHeaderSharedTable imhst;

    public ProfileExtradataWidget(IMainHeaderSharedTable imhst) {
        this.imhst = imhst;
    }

    @Override
    public Widget buildWidgets() {
        return new VerticalPanel();
    }

    @Override
    public void displayData() {
        ProfileBean itemExtradata = getData().profileItemExtradata;
        ProfileFileBean fileExtradata = getData().profileFileExtradata;
        if (itemExtradata != null) {
            imhst.addTranslatedRow("libs" /* I18N: no */, itemExtradata.libs);
            imhst.addTranslatedRow("fid" /* I18N: no */, itemExtradata.fid);
            imhst.addTranslatedRow("di" /* I18N: no */, itemExtradata.di);
            imhst.addTranslatedRow("nod" /* I18N: no */, itemExtradata.nod);
            imhst.addTranslatedRow("len" /* I18N: no */, itemExtradata.len);
            imhst.addTranslatedRow("dft" /* I18N: no */, itemExtradata.dft);
            imhst.addTranslatedRow("dom" /* I18N: no */, itemExtradata.dom);
            imhst.addTranslatedRow("tbl" /* I18N: no */, itemExtradata.tbl);
            imhst.addTranslatedRow("ptn" /* I18N: no */, itemExtradata.ptn);
            imhst.addTranslatedRow("xpo" /* I18N: no */, itemExtradata.xpo);
            imhst.addTranslatedRow("xpr" /* I18N: no */, itemExtradata.xpr);
            imhst.addTranslatedRow("typ1" /* I18N: no */, itemExtradata.typ);
            imhst.addTranslatedRow("des" /* I18N: no */, itemExtradata.des);
            imhst.addTranslatedRow("itp" /* I18N: no */, itemExtradata.itp);
            imhst.addTranslatedRow("min" /* I18N: no */, itemExtradata.min);
            imhst.addTranslatedRow("max" /* I18N: no */, itemExtradata.max);
            imhst.addTranslatedRow("dec" /* I18N: no */, itemExtradata.dec);
            imhst.addTranslatedRow("req" /* I18N: no */, itemExtradata.req);
            imhst.addTranslatedRow("cmp" /* I18N: no */, itemExtradata.cmp);
            imhst.addTranslatedRow("sfd" /* I18N: no */, itemExtradata.sfd);
            imhst.addTranslatedRow("sfd1" /* I18N: no */, itemExtradata.sfd1);
            imhst.addTranslatedRow("sfd2" /* I18N: no */, itemExtradata.sfd2);
            imhst.addTranslatedRow("sfp" /* I18N: no */, itemExtradata.sfp);
            imhst.addTranslatedRow("sft" /* I18N: no */, itemExtradata.sft);
            imhst.addTranslatedRow("siz" /* I18N: no */, itemExtradata.siz);
            imhst.addTranslatedRow("del" /* I18N: no */, itemExtradata.del);
            imhst.addTranslatedRow("pos" /* I18N: no */, itemExtradata.pos);
            imhst.addTranslatedRow("rhd" /* I18N: no */, itemExtradata.rhd);
            imhst.addTranslatedRow("srl" /* I18N: no */, itemExtradata.srl);
            imhst.addTranslatedRow("cnv" /* I18N: no */, itemExtradata.cnv);
            imhst.addTranslatedRow("ltd" /* I18N: no */, SimpleDateUtils.toCanonicalString(itemExtradata.ltd));
            imhst.addTranslatedRow("usr" /* I18N: no */, itemExtradata.usr);
            imhst.addTranslatedRow("mdd" /* I18N: no */, itemExtradata.mdd);
            imhst.addTranslatedRow("val4ext" /* I18N: no */, itemExtradata.val4ext);
            imhst.addTranslatedRow("deprep" /* I18N: no */, itemExtradata.deprep);
            imhst.addTranslatedRow("depostp" /* I18N: no */, itemExtradata.depostp);
            imhst.addTranslatedRow("nullind" /* I18N: no */, itemExtradata.nullind);
            imhst.addTranslatedRow("mddfid" /* I18N: no */, itemExtradata.mddfid);
            imhst.addTranslatedRow("validcmp" /* I18N: no */, itemExtradata.validcmp);
        }
        if (fileExtradata != null) {
            imhst.addTranslatedRow("libs" /* I18N: no */, fileExtradata.libs);
            imhst.addTranslatedRow("fid" /* I18N: no */, fileExtradata.fid);
            imhst.addTranslatedRow("global" /* I18N: no */, fileExtradata.global);
            imhst.addTranslatedRow("akey1" /* I18N: no */, fileExtradata.akey1);
            imhst.addTranslatedRow("akey2" /* I18N: no */, fileExtradata.akey2);
            imhst.addTranslatedRow("akey3" /* I18N: no */, fileExtradata.akey3);
            imhst.addTranslatedRow("akey4" /* I18N: no */, fileExtradata.akey4);
            imhst.addTranslatedRow("akey5" /* I18N: no */, fileExtradata.akey5);
            imhst.addTranslatedRow("akey6" /* I18N: no */, fileExtradata.akey6);
            imhst.addTranslatedRow("akey7" /* I18N: no */, fileExtradata.akey7);
            imhst.addTranslatedRow("del" /* I18N: no */, fileExtradata.del);
            imhst.addTranslatedRow("syssn" /* I18N: no */, fileExtradata.syssn);
            imhst.addTranslatedRow("netloc" /* I18N: no */, fileExtradata.netloc);
            imhst.addTranslatedRow("parfid" /* I18N: no */, fileExtradata.parfid);
            imhst.addTranslatedRow("mplctdd" /* I18N: no */, fileExtradata.mplctdd);
            imhst.addTranslatedRow("dftdes" /* I18N: no */, fileExtradata.dftdes);
            imhst.addTranslatedRow("dftord" /* I18N: no */, fileExtradata.dftord);
            imhst.addTranslatedRow("dfthdr" /* I18N: no */, fileExtradata.dfthdr);
            imhst.addTranslatedRow("dftdes1" /* I18N: no */, fileExtradata.dftdes1);
            imhst.addTranslatedRow("ltd" /* I18N: no */, SimpleDateUtils.toCanonicalString(fileExtradata.ltd));
            imhst.addTranslatedRow("usr" /* I18N: no */, fileExtradata.usr);
            imhst.addTranslatedRow("filetyp" /* I18N: no */, fileExtradata.filetyp);
            imhst.addTranslatedRow("exist" /* I18N: no */, fileExtradata.exist);
            imhst.addTranslatedRow("extendlength" /* I18N: no */, fileExtradata.extendlength);
            imhst.addTranslatedRow("fsn" /* I18N: no */, fileExtradata.fsn);
            imhst.addTranslatedRow("fdoc" /* I18N: no */, fileExtradata.fdoc);
            imhst.addTranslatedRow("qid1" /* I18N: no */, fileExtradata.qid1);
            imhst.addTranslatedRow("acckeys" /* I18N: no */, fileExtradata.acckeys);
            imhst.addTranslatedRow("val4ext" /* I18N: no */, fileExtradata.val4ext);
            imhst.addTranslatedRow("predaen" /* I18N: no */, fileExtradata.predaen);
            imhst.addTranslatedRow("screen" /* I18N: no */, fileExtradata.screen);
            imhst.addTranslatedRow("rflag" /* I18N: no */, fileExtradata.rflag);
            imhst.addTranslatedRow("dflag" /* I18N: no */, fileExtradata.dflag);
            imhst.addTranslatedRow("udacc" /* I18N: no */, fileExtradata.udacc);
            imhst.addTranslatedRow("udfile" /* I18N: no */, fileExtradata.udfile);
            imhst.addTranslatedRow("fpn" /* I18N: no */, fileExtradata.fpn);
            imhst.addTranslatedRow("udpre" /* I18N: no */, fileExtradata.udpre);
            imhst.addTranslatedRow("udpost" /* I18N: no */, fileExtradata.udpost);
            imhst.addTranslatedRow("publish" /* I18N: no */, fileExtradata.publish);
            imhst.addTranslatedRow("zrsfile" /* I18N: no */, fileExtradata.zrsfile);
            imhst.addTranslatedRow("glref" /* I18N: no */, fileExtradata.glref);
            imhst.addTranslatedRow("rectyp" /* I18N: no */, fileExtradata.rectyp);
            imhst.addTranslatedRow("ptruser" /* I18N: no */, fileExtradata.ptruser);
            imhst.addTranslatedRow("ptrtld" /* I18N: no */, fileExtradata.ptrtld);
            imhst.addTranslatedRow("log" /* I18N: no */, fileExtradata.log);
            imhst.addTranslatedRow("archfiles" /* I18N: no */, fileExtradata.archfiles);
            imhst.addTranslatedRow("archkey" /* I18N: no */, fileExtradata.archkey);
            imhst.addTranslatedRow("ptrtim" /* I18N: no */, fileExtradata.ptrtim);
            imhst.addTranslatedRow("ptruseru" /* I18N: no */, fileExtradata.ptruseru);
            imhst.addTranslatedRow("ptrtldu" /* I18N: no */, fileExtradata.ptrtldu);
            imhst.addTranslatedRow("ptrtimu" /* I18N: no */, fileExtradata.ptrtimu);
            imhst.addTranslatedRow("listdft" /* I18N: no */, fileExtradata.listdft);
            imhst.addTranslatedRow("listreq" /* I18N: no */, fileExtradata.listreq);
            imhst.addTranslatedRow("des" /* I18N: no */, fileExtradata.des);
        }
    }

    @Override
    public boolean hasContent() {
        EntityDetailsDataBean data = getData();
        return data.profileItemExtradata != null || data.profileFileExtradata != null;
    }
}
