﻿exec sp_check_version '1.1.6.4';
go


EXEC sp_rename 'bik_help.tab_code', 'help_key', 'COLUMN'
go

EXEC sp_rename 'bik_help.tab_name', 'caption', 'COLUMN'
go

alter table bik_help 
add is_hidden int not null default 0
go



if not exists(select * from bik_help where help_key ='myBIK')
begin
	update bik_help set help_key = 'New' + help_key where help_key = 'Search'
end
else 
	update bik_help set is_hidden = 1 where help_key = 'Search'
	
if not exists(select * from bik_help where help_key ='myBIK')
begin
	update bik_help set help_key = 'myBIK' where help_key = 'MyBIKS'
end
else 
	update bik_help set is_hidden = 1 where help_key = 'MyBIKS'

 
-- przerzucenie Innych Podpowiedzi do bik_help
insert into bik_help 
    select id as help_key, name as caption, hint as text_help, null as text_help_plain, 0 as is_hidden 
	from bik_attr_hint
	
	
exec sp_update_version '1.1.6.4', '1.1.6.5';
go