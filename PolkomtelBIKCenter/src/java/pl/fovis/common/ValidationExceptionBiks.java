/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author pku
 */
public class ValidationExceptionBiks extends RuntimeException implements Serializable {

    public ValidationExceptionBiks() {
    }

    public ValidationExceptionBiks(String msg) {
        super(msg);
    }
}
