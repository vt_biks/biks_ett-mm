/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

import simplelib.FieldNameConversion;
import java.util.List;
import java.util.Map;
import simplelib.BaseUtils.Projector;
import simplelib.IIndexedNamedVals;
import simplelib.ILameCollector;
import simplelib.IValueFormatter;

/**
 *
 * @author wezyr
 */
public interface IRawConnection extends IValueFormatter {


    // returns update count if available
    public Integer execCommand(String sqlTxt);

    public <C extends ILameCollector<Map<String, Object>>> C execQry(String qryTxt, boolean maxOneRow, boolean minOneRow,
            C rowCollector, FieldNameConversion optConversion);

    public void execQryEx(final String qryTxt,
            final boolean maxOneRow, final boolean minOneRow,
            final Projector<List<String>, Map<String, String>> prepareTargetNamesProj,
            final ILameCollector<IIndexedNamedVals> fastRowCollector,
            final ILameCollector<Map<String, Object>> rowCollectorFromCache);

    public int readSequenceNextVal(String seqName);

    public boolean getAutoCommit();

    public void setAutoCommit(boolean val);

    public void finalizeTran(boolean success);

//    public void finishWorkUnit();
    public void destroy();
}
