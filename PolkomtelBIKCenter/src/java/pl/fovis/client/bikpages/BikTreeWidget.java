/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.treeandlist.BikNodeLazyRowLoader;
import pl.fovis.client.treeandlist.IBean4TreeExtractor;
import pl.fovis.client.treeandlist.TreeBean4TreeExtractor;
import pl.fovis.client.treeandlist.TreeNodeBeanAsMapStorage;
import pl.fovis.client.treeandlist.TreeNodeBroker;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.NodeModificationLogEntryBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.foxygwtcommons.ITreeBroker;
import pl.fovis.foxygwtcommons.treeandlist.ILazyRowLoaderEx;
import pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class BikTreeWidget extends AbstractBIKTreeWidget<TreeNodeBean, Integer> {

    protected Integer treeId;
    protected boolean noLinkedNode;
    protected boolean calcHasNoChildren = true;
    protected Integer nodeKindId;
    protected String relationType;
    protected String attributeName;
    protected Integer outerTreeId;// TreeId zewnetrznego drzewa , czyli jak dodajemy dowiązanie,powiązanie,atrybut to treeId drzewa do ktorego dodajemy

    public BikTreeWidget(boolean noLinkedNodes, String firstColStyleName,
            IPageDataFetchBroker dataFetchBroker, boolean isWidgetRunFromDialog, String... fields) {
        super(firstColStyleName, dataFetchBroker, isWidgetRunFromDialog, fields);
        this.noLinkedNode = noLinkedNodes;
    }

    public BikTreeWidget(boolean noLinkedNodes, Integer treeGridHeight,
            String firstColStyleName, IPageDataFetchBroker dataFetchBroker,
            boolean calcHasNoChildren, String... fields) {
        super(treeGridHeight, firstColStyleName, dataFetchBroker, fields);
        this.noLinkedNode = noLinkedNodes;
        this.calcHasNoChildren = calcHasNoChildren;
    }

    public BikTreeWidget(boolean noLinkedNodes, IPageDataFetchBroker dataFetchBroker, boolean calcHasNoChildren, String... fields) {
        super(dataFetchBroker, fields);
        this.noLinkedNode = noLinkedNodes;
        this.calcHasNoChildren = calcHasNoChildren;
    }

    public BikTreeWidget(boolean noLinkedNodes, IPageDataFetchBroker dataFetchBroker, boolean calcHasNoChildren, String relationType,String attributeName, Integer nodeKindId,Integer outerTreeId, String... fields) {
        super(dataFetchBroker, fields);
        this.noLinkedNode = noLinkedNodes;
        this.calcHasNoChildren = calcHasNoChildren;
        this.nodeKindId = nodeKindId;
        this.relationType = relationType;
        this.attributeName=attributeName;
        this.outerTreeId=outerTreeId;
    }

    public BikTreeWidget(Integer treeId, IPageDataFetchBroker dataFetchBroker, String... fields) {
        super(dataFetchBroker, fields);
        this.treeId = treeId;
    }

    public BikTreeWidget(Integer treeGridHeight,
            String firstColStyleName, IPageDataFetchBroker dataFetchBroker,
            String... fields) {
        super(treeGridHeight, firstColStyleName, dataFetchBroker, fields);

    }

    @Override
    protected Request getBikFilteredNodes(String val, final Integer optSubTreeRootNodeId,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String filterRequestTicket, AsyncCallback<List<TreeNodeBean>> asyncCallback) {
        // Window.alert("2 "+relationType+" +"+nodeKindId+attributeName);
        return BIKClientSingletons.getService().getBikFilteredNodes(treeId, val, optSubTreeRootNodeId,
                optExtraMode, optFilteringNodeActionCode, filterRequestTicket, nodeKindId,relationType,attributeName,asyncCallback);
    }

    @Override
    protected Request getBikFilteredNodesWithCondidtion(String val, final Integer optSubTreeRootNodeId,
            Collection<Integer> treeIds, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String filterRequestTicket, AsyncCallback<List<TreeNodeBean>> asyncCallback) {
      //  Window.alert(relationType+" +"+nodeKindId+ " "+attributeName);
        return BIKClientSingletons.getService().getBikFilteredNodesWithCondidtion(val, optSubTreeRootNodeId,
                treeIds, noLinkedNode, optExtraMode, optFilteringNodeActionCode, filterRequestTicket, nodeKindId,relationType,attributeName, asyncCallback);
    }

    @Override
    public boolean isSubTreeFilteringCapable() {
        return true;
    }

    @Override
    protected MapTreeRowAndBeanStorageBase<Integer, TreeNodeBean> createStorage() {
        return new TreeNodeBeanAsMapStorage(cellWidgetBuilder, isWidgetRunFromDialog);
    }

    @Override
    protected ITreeBroker<TreeNodeBean, Integer> createTreeBroker() {
        return new TreeNodeBroker();
    }

    protected void optFixBeanPropsAfterLoad(TreeNodeBean tnb) {
        //no-op
    }

    @Override
    protected ILazyRowLoaderEx<Map<String, Object>, Integer> createLazyRowLoader(BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode) {
        return new BikNodeLazyRowLoader(treeGrid, storage, noLinkedNode, optExtraMode, optFilteringNodeActionCode, dataFetchBroker, calcHasNoChildren, nodeKindId,relationType,attributeName,outerTreeId) {
            @Override
            public void optFixBeanPropsAfterLoad(TreeNodeBean tb, TreeNodeBean ancestorBean) {
                super.optFixBeanPropsAfterLoad(tb, ancestorBean);
                BikTreeWidget.this.optFixBeanPropsAfterLoad(tb);
            }
        };
    }

    @Override
    public IBean4TreeExtractor<TreeNodeBean, Integer> createBeanExtractor() {
        return new TreeBean4TreeExtractor();
    }

    public void refreshNodes(List<NodeModificationLogEntryBean> nodeModifications) {

        // 4. nowy węzeł
//        IFoxyTreeGrid<Integer> treeGrid = getTreeGrid();
        TreeNodeBean selectedTnb = getCurrentNodeBean();
        Integer selectedNodeId = selectedTnb != null ? selectedTnb.id : null;
        boolean mustReselectNode = false;

        Set<Integer> maybeSortChildrenOfNodeIds = new HashSet<Integer>();

        for (NodeModificationLogEntryBean nmleb : nodeModifications) {
            TreeNodeBean tnb = storage.getBeanById(nmleb.id);
            if (nmleb.isDeleted) {

                // 1. usunięcie węzła
                if (tnb != null) {
                    treeGrid.deleteNode(nmleb.id);
                    if (selectedNodeId == tnb.id) {
                        mustReselectNode = true;
                        selectedNodeId = tnb.parentNodeId;
                    }
                }
            } else if (tnb != null) {

                if (!BaseUtils.safeEquals(tnb.name, nmleb.name)) {
                    // 2. zmiana nazwy
                    treeGrid.setNodeName(nmleb.id, nmleb.name);
                    maybeSortChildrenOfNodeIds.add(tnb.parentNodeId);
                }

                String lastItems = BaseUtils.getLastItems(nmleb.branchIds, "|", 3);
                String parentIdStr = BaseUtils.getFirstItems(lastItems, "|", 1);
                Integer parentId = BaseUtils.tryParseInteger(parentIdStr);

                if (!BaseUtils.safeEquals(parentId, tnb.parentNodeId)) {
                    // 3. przepięcie pod innego parenta
                    TreeNodeBean newParentTnb = storage.getBeanById(parentId);
                    if (newParentTnb != null) {
                        tnb.parentNodeId = parentId;
                        treeGrid.pasteNode(parentId, tnb.id, true);

                        maybeSortChildrenOfNodeIds.add(parentId);
                        if (selectedNodeId == tnb.id) {
                            mustReselectNode = true;
                            selectedNodeId = tnb.id;
                        }
                    } else {
                        treeGrid.deleteNode(tnb.id);
                        if (selectedNodeId == tnb.id) {
                            mustReselectNode = true;
                            selectedNodeId = tnb.parentNodeId;
                        }
                    }
                }
            }
        }

        for (Integer parentNodeId : maybeSortChildrenOfNodeIds) {
            BIKClientSingletons.maybeSortChildrenWithDefaultOrder(storage, treeGrid, parentNodeId);
        }

        if (mustReselectNode) {
            treeGrid.selectRecord(selectedNodeId);
        }
    }

}
