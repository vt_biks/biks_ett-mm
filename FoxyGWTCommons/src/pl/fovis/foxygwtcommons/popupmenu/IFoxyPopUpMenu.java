/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.popupmenu;

/**
 *
 * @author pmielanczuk
 */
public interface IFoxyPopUpMenu<CTX> {

    public void addAction(final IPopupMenuItemAction<CTX> act);

    public void addSeparator();

    public CTX getContextObject();
}
