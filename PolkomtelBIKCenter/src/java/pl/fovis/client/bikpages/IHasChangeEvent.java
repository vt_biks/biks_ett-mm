/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import simplelib.IContinuation;

/**
 *
 * @author beata
 */
public interface IHasChangeEvent {

    public void setOnChange(IContinuation onChangeCont);
}
