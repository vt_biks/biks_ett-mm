/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author bfechner
 */
public class ChangedRankingBean implements Serializable, IsSerializable {

    public Set<Integer> starredSystemUser;
    public Set<Integer> starredBikUser;
    public Set<Integer> starredUserNode;
    public long timeStamp;

    public ChangedRankingBean() {
    }

}
