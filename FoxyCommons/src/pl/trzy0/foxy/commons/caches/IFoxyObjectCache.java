/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.caches;

/**
 *
 * @author wezyr
 */
public interface IFoxyObjectCache<K, V> {

    public V getFromCacheByKey(K key);

    public V getByKey(K key);

    public V getByAlternativeKey(String keyName, Object keyVal);

    public FoxyObjectUpdateData getByKeyForUpdate(
            K key, boolean allowSupplying);
}
