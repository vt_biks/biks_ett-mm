/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.Date;
import pl.fovis.common.BIKCenterUtils;
import simplelib.SimpleDateUtils;

/**
 *
 * @author bfechner
 */
public class AttrDateTime extends AttrWigetBase {

//    UTCTimeBox dp;
    protected DateBox datePicker = new DateBox();
    ;
    protected String valueOpt;
    boolean isEditAttr;
    protected String atrLinValue;

    public AttrDateTime(String atrLinValue, String valueOpt, boolean isEditAttr) {
        this.atrLinValue = atrLinValue;
        this.valueOpt = valueOpt;
        this.isEditAttr = isEditAttr;
    }

    @Override
    public Widget getWidget() {
//        dp = new TextBox();
//        datePicker = new DateBox();
        BIKCenterUtils.setupDateTimeBoxWithLimit(datePicker, valueOpt, "yyyy-MM-dd HH:mm:ss");
        if (isEditAttr) {
            Date atrLinValueDate = SimpleDateUtils.parseFromCanonicalString(atrLinValue);
            if (atrLinValueDate != null && SimpleDateUtils.getYear(atrLinValueDate) > 1900) {
                datePicker.setValue(atrLinValueDate);
                datePicker.getDatePicker().setCurrentMonth(atrLinValueDate);
            }
        }
        datePicker.setStyleName("addAttr");
        datePicker.setWidth(/*"800px"*/"100%");
        return datePicker;
    }

    @Override
    public String getValue() {
//        Window.alert(SimpleDateUtils.dateTimeToStringNoSep(datePicker.getValue()));
        return SimpleDateUtils.toCanonicalString(datePicker.getValue());
//        return datePicker.getValue().toString();
    }

    @Override
    public int getWidgetHeight() {
        return 20;
    }

}
