﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- dodanie wizualizacji wykonań testów DQC
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_check_version '1.6.y';
go

if not exists(select * from bik_attr_def where name = 'Wizualizacja' and attr_category_id = (select id from bik_attr_category where name = 'DQC' and is_built_in = 1) and is_built_in = 0)
begin
	insert into bik_attr_def(name,attr_category_id,is_deleted, is_built_in,type_attr)
	values ('Wizualizacja', (select id from bik_attr_category where name = 'DQC' and is_built_in = 1), 0, 0, 'hyperlinkOut')

	insert into bik_translation(code, txt, lang, kind)
	values ('Wizualizacja', 'Visualization', 'en', 'adef')

	insert into bik_attribute(node_kind_id, attr_def_id, is_deleted)
	values ((select id from bik_node_kind where code = 'DQCTestSuccess'), (select id from bik_attr_def where name = 'Wizualizacja' and attr_category_id = (select id from bik_attr_category where name = 'DQC')), 0)

	insert into bik_attribute(node_kind_id, attr_def_id, is_deleted)
	values ((select id from bik_node_kind where code = 'DQCTestFailed'), (select id from bik_attr_def where name = 'Wizualizacja' and attr_category_id = (select id from bik_attr_category where name = 'DQC')), 0)

	insert into bik_attribute(node_kind_id, attr_def_id, is_deleted)
	values ((select id from bik_node_kind where code = 'DQCTestInactive'), (select id from bik_attr_def where name = 'Wizualizacja' and attr_category_id = (select id from bik_attr_category where name = 'DQC')), 0)
end;

exec sp_update_version '1.6.y', '1.6.y.1';
go