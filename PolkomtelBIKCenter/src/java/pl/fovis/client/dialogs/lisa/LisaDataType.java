/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import pl.fovis.client.entitydetailswidgets.lisa.WidgetWithValue;

/**
 *
 * @author tbzowka
 */
public enum LisaDataType {

    AT(new TextBoxCreator()), //TIME
    BF(new TextBoxCreator()), //BYTE
    BV(new TextBoxCreator()), //VARBYTE
    CF(new TextBoxCreator()), //CHAR
    CV(new TextBoxCreator()), //VARCHAR
    D(new TextBoxCreator()), //DECIMAL
    DA(new DateBoxCreator()), //DATE
    F(new TextBoxCreator()), //FLOAT
    I1(new IntegerBoxCreator()), //BYTEINT
    I2(new IntegerBoxCreator()), //SMALLINT
    I8(new IntegerBoxCreator()), //BIGINT
    I(new IntegerBoxCreator()), //INTEGER
    TS(new TextBoxCreator()); //TIMESTAMP

    public static LisaDataType getValueFor(String dataType) {
        try {
            if (dataType.trim() != null) {
                return valueOf(dataType.trim());
            } else {
                return CV;
            }
        } catch (IllegalArgumentException e) {
            return CV;
        }
    }

    private WidgetCreator creator;

    LisaDataType(WidgetCreator creator) {
        this.creator = creator;
    }

    public WidgetWithValue createWidget() {
        return (creator.createWidget());
    }

    @Override
    public String toString() {
        return name();
    }
}
