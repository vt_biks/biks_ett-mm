exec sp_check_version '1.4.y1.18';
go

----------------------------------------------------
----------------------------------------------------
--     zmiana zawartosci pol 'obj_id' w drzewie slownikow LISY
--     Dotychczasowa zawartość (bezpośrednie identyfikatory z Teradaty)
--      nie zapewnialy unikalnosci.
----------------------------------------------------
----------------------------------------------------
 if exists (
                       select  * from tempdb.dbo.sysobjects o
                       where o.xtype in ('U')  
                   and o.id = object_id(N'tempdb..#temporalCategorizations')
               )
drop table #temporalCategorizations         
go

declare @treeId int = dbo.fn_tree_id_by_code('DictionaryDWH')

update bik_node set is_built_in = 1, obj_id = 0 where tree_id =@treeId and obj_id like 'UNASSIGNED%'
and is_deleted = 0



select id into #temporalCategorizations from bik_node where parent_node_id in (
       select id from bik_node where parent_node_id = (
               select id from bik_node where tree_id=@treeId and obj_id = 'DWHDictionaryRoot' and is_deleted = 0)
               and is_deleted = 0) 
       and is_deleted = 0
       

update ctype set obj_id = dict.obj_id+':'+ctype.obj_id
from bik_node ctype left join bik_node dict on ctype.parent_node_id = dict.id
where ctype.id in (select  id from #temporalCategorizations)

update categ set obj_id = ctype.obj_id+':'+categ.obj_id
from bik_node categ left join bik_node ctype on categ.parent_node_id = ctype.id
where ctype.id in (select  id from #temporalCategorizations)
and categ.is_built_in = 0

update categ set obj_id = ctype.obj_id+':UNASSIGNED'
from bik_node categ left join bik_node ctype on categ.parent_node_id = ctype.id
where ctype.id in (select  id from #temporalCategorizations)
and categ.is_built_in = 1


exec sp_update_version '1.4.y1.18', '1.4.y1.19';
go