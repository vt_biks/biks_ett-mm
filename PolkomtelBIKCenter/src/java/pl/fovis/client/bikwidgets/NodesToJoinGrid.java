/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.dialogs.IJoinedObjSelectionManager;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.JoinedObjBasicGenericInfoBean;
import pl.fovis.common.JoinedObjMode;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class NodesToJoinGrid extends NodesGridBase {

    protected IJoinedObjSelectionManager<Integer> alreadyJoinedManager;

     public NodesToJoinGrid() {
//        this.alreadyJoinedManager = alreadyJoinedManager;
    }
    
    public NodesToJoinGrid(IJoinedObjSelectionManager<Integer> alreadyJoinedManager) {
        this.alreadyJoinedManager = alreadyJoinedManager;
    }

    public Map<TreeNodeBean, JoinedObjMode> joinedMap = new HashMap<TreeNodeBean, JoinedObjMode>();

    protected Widget getSelectedWidget(final TreeNodeBean n) {

//        JoinedObjMode selectedVal =n!=null? getNodeSelectedValue(n): JoinedObjMode.NotJoined;
        JoinedObjMode selectedVal = JoinedObjMode.NotJoined;
        joinedMap.put(n, selectedVal);
        tnb = n;
        final boolean isFixed = false;

        final InlineHTML imgHtml = new InlineHTML(generateHtmlForJoinedObjImg(selectedVal, n.isNodeSelected));

        if (!isFixed) {
            imgHtml.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    JoinedObjMode nextVal = joinedMap.get(n).getNextVal(true);
                    joinedMap.put(n, joinedMap.get(n).getNextVal(true));
                    directOverride.put(n.id, nextVal);
                    imgHtml.setHTML(generateHtmlForJoinedObjImg(nextVal, isFixed));

                }
            });
        }
        return imgHtml;
    }

    protected String generateHtmlForJoinedObjImg(JoinedObjMode selectedVal, boolean isFixed) {
        return "<" + "img src='images" /* I18N: no */ + "/joined_"
                + selectedVal.imgNamePart + (isFixed ? "_fixed" : "")
                + "." + "png' style=\"width: 16px; height: 16px;\" border=\"0\" class" /* I18N: no */ + "=\"treeIcon\"/>";
    }

    protected JoinedObjMode getNodeSelectedValue(TreeNodeBean node) {
//        Window.alert(node != null ? node.id + " " : "null");
//         return  JoinedObjMode.NotJoined;
        return getNodeSelectedValueEx(node, false);
    }

    protected JoinedObjMode getNodeSelectedValueEx(TreeNodeBean node, boolean noOverride) {
        Integer nodeId = tnb.id;
        JoinedObjBasicGenericInfoBean jobi = getJoinedObjBasicInfo(tnb.id);

        if (!isJobiFixed(jobi, nodeId)) {
            
            JoinedObjMode jobiOverride = directOverride.get(nodeId);
            if (jobiOverride != null) {
                if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
                    ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "getNodeSelectedValueEx (true) for id=" + nodeId + ", nodeId=" + nodeId + ", from override: " + jobiOverride);
                }
                return jobiOverride;
            }
        
            List<Integer> ancestorNodeIds = (List<Integer>) splitBranchIdsIntoColl(tnb.branchIds, true, new ArrayList<Integer>());
            JoinedObjMode fromAncestor = null;
            boolean isParent = true;
        
            for (int i = ancestorNodeIds.size() - 1; i >= 0; i--) {
                Integer ancestorNodeId = ancestorNodeIds.get(i);
                Pair<JoinedObjMode, Boolean> ancestorOverride = subNodesOverride.get(ancestorNodeId);
                if (ancestorOverride != null && (ancestorOverride.v2 || isParent)) {
                    fromAncestor = ancestorOverride.v1;
                    break;
                }
                isParent = false;
            }
   
            if (fromAncestor != null) {
                return fromAncestor;
            }
        }

        if (jobi == null) {
            if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
                ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "getNodeSelectedValueEx (true) for id=" + nodeId + ", nodeId=" + nodeId + ", no jobi");
            }
            return JoinedObjMode.NotJoined;
        }

        if (jobi.inheritToDescendants || !BaseUtils.safeEquals(jobi.srcId, nodeId)) {
            if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
                ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "getNodeSelectedValueEx (true) for id=" + nodeId + ", nodeId=" + nodeId + ", jobi=" + jobi + ", with inheritance");
            }
            return JoinedObjMode.WithInheritance;
        }

        if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
            ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "getNodeSelectedValueEx (true) for id=" + nodeId + ", nodeId=" + nodeId + ", jobi=" + jobi + ", single");
        }
        return JoinedObjMode.Single;
    }

    protected JoinedObjBasicGenericInfoBean getJoinedObjBasicInfo(Integer dstNodeId) {
        if (alreadyJoinedManager == null /*|| !alreadyJoinedObjIds.contains(dstNodeId)*/) {
            return null;
        }

        JoinedObjBasicGenericInfoBean jobi = alreadyJoinedManager.getInfoForJoinedNodeById(dstNodeId);

        if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
            ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "jobi for id=" + dstNodeId + ": " + jobi);
        }

        return jobi;
    }

    protected boolean isJobiFixed(JoinedObjBasicGenericInfoBean jobi, int nodeId) {
        boolean res = jobi != null && (jobi.type || !BaseUtils.safeEquals(jobi.srcId, nodeId));

        if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
            ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "isJobiFixed jobi=" + jobi + ", nodeId=" + nodeId + ", result: " + res);
        }

        return res;
    }

    protected Collection<Integer> splitBranchIdsIntoColl(String branchIds, boolean withOutSelf, Collection<Integer> coll) {
        return BIKCenterUtils.splitBranchIdsIntoColl(branchIds, withOutSelf, coll);
    }
}
