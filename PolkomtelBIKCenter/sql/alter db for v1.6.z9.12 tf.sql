﻿exec sp_check_version '1.6.z9.12';
go


if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_node_kind]') and name = 'generic_node_select')
begin
	alter table bik_node_kind add generic_node_select varchar(max) null;
end;
go


exec sp_update_version '1.6.z9.12', '1.6.z9.13';
go
