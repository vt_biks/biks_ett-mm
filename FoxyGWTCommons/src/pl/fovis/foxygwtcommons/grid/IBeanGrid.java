/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.grid;

/**
 *
 * @author pmielanczuk
 */
public interface IBeanGrid<T> {

    public int getBeanCount();

    public int getCurrPage();

    public int getPageSize();
    
    public void setPageNum(int pageNum);
}
