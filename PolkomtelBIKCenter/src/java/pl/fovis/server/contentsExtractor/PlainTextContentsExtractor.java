/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.contentsExtractor;

import commonlib.LameUtils;

/**
 *
 * @author pmielanczuk
 */
public class PlainTextContentsExtractor extends BaseContentExtractor {

    public PlainTextContentsExtractor(String fileExtensions) {
        super(fileExtensions);
    }

    @Override
    public String extractContents(String fullFilePath) {
        return LameUtils.loadAsString(fullFilePath, "*Cp1250,ISO-8859-2,UTF-8");
    }
}
