/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;

/**
 *
 * @author ctran
 */
public abstract class SelectExportOptionsDialog extends BaseActionOrCancelDialog {

    protected ListBox selectFormatLb = new ListBox();

    @Override
    protected String getDialogCaption() {
        return I18n.wybierOpcjiDoEksportu.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.eksportuj.get();
    }

    @Override
    protected void buildMainWidgets() {
        HorizontalPanel hp = new HorizontalPanel();
        hp.add(new Label(I18n.formatPliku.get()));

        selectFormatLb.addItem(BIKConstants.EXPORT_FORMAT_XLSX);
        selectFormatLb.addItem(BIKConstants.EXPORT_FORMAT_CSV);
        hp.add(selectFormatLb);
        main.add(hp);

        buildAdditionalOptions();
    }

    public abstract void buildAdditionalOptions();

    public abstract String generateDownloadLink();
}
