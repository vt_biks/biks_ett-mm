/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

/**
 *
 * @author tflorczak
 */
public interface IBeanCollector<T> {

    public void add(T bean);

    public void flush();
}
