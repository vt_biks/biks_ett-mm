/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.UrlMakingUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import pl.bssg.metadatapump.common.SAPBOObjectBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author bfechner
 */
public class BIAObjectManagerExportManager extends ExportManagerBase {

    protected BOXIServiceImpl service;

    public BIAObjectManagerExportManager(BOXIServiceImpl boxiService) {
        this.service = boxiService;
    }

    @Override
    protected Pair<String, List<Map<String, Object>>> getFileNamePrefixAndContent(HttpServletRequest req) {

        String fileName = I18n.objectManager.get();
        SAPBOObjectBean ob = new SAPBOObjectBean();
        ob.manageType = BaseUtils.tryParseInt(req.getParameter(BIKConstants.BIA_OBJECT_MANAGE_TYPE));
        ob.id = BaseUtils.tryParseInt(req.getParameter(BIKConstants.BIA_OBJECT_ID));
        ob.name = req.getParameter(BIKConstants.BIA_OBJECT_NAME);
        ob.owner = req.getParameter(BIKConstants.BIA_OBJECT_OWNER);
        ob.parentId = BaseUtils.tryParseInt(req.getParameter(BIKConstants.BIA_OBJECT_PARENT_ID));
        List<Map<String, Object>> content = getBIAObjectManager(ob);
        return new Pair<String, List<Map<String, Object>>>(UrlMakingUtils.encodeStringForUrlWW(fileName).replace("+", "%20"), content);

    }

    public List<Map<String, Object>> getBIAObjectManager(SAPBOObjectBean filter) {
        List<Map<String, Object>> content = new ArrayList<Map<String, Object>>();
        List<SAPBOObjectBean> instances = service.getBIAObjects(filter);
        for (SAPBOObjectBean au : instances) {
            Map<String, Object> item = new LinkedHashMap<String, Object>();
            item.put(I18n.id.get(), au.id);
            item.put(I18n.nazwa.get(), au.name);
            item.put(I18n.typObiektu.get(), au.type);
            item.put(I18n.lokalizacja.get(), au.location);
            item.put(I18n.scheduleWlasciciel.get(), au.owner);
            item.put(I18n.objectOpis.get(), au.description);
            item.put(I18n.scheduleParentID.get(), au.parentId);
            item.put(I18n.scheduleUtworzono.get(), SimpleDateUtils.toCanonicalString(au.created));
            item.put(I18n.scheduleZmodyfikowano.get(), SimpleDateUtils.toCanonicalString(au.modify));
            item.put(I18n.scheduleCUID.get(), au.cuid);
            item.put(I18n.scheduleGUID.get(), au.guid);
            item.put(I18n.scheduleRUID.get(), au.ruid);
            content.add(item);
        }
        return content;
    }

    @Override
    protected String getFileFormat(HttpServletRequest req) {
        return req.getParameter(BIKConstants.BIA_OBJECT_RESULTS_EXPORT_PARAM_NAME);
    }

}
