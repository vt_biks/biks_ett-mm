select sc.name, *
  from sys.indexes si 
          join sys.tables st 
             on si.object_id = st.object_id
          join sys.index_columns sic 
             on sic.object_id = si.object_id and 
                sic.index_id = si.index_id
          join sys.columns sc 
             on sic.object_id = sc.object_id and 
                sic.column_id = sc.column_id
  where st.name = 'bik_node' and 
        si.is_primary_key = 1
  order by sic.index_column_id 
  

CREATE FULLTEXT CATALOG BIKS_FTS_Catalog AS DEFAULT;
go
declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_node') and is_primary_key = 1)
exec('CREATE FULLTEXT INDEX ON bik_node (name, descr) key index ' + @pk_name)
go


DROP FULLTEXT CATALOG BIKS_FTS_Catalog;


if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_node'))
drop fulltext index on bik_node
go

if exists(select 1 from sys.fulltext_catalogs where name = 'BIKS_FTS_Catalog')
DROP FULLTEXT CATALOG BIKS_FTS_Catalog;
go


select object_name(id), * from sysindexes
where object_name(id) = 'bik_node'


CREATE FULLTEXT INDEX ON bik_node (name, descr) key index PK__bik_node__3213E83F7DD4055A;
go


select *
from bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
where contains(bn.*, 'baza')
and is_deleted = 0 and linked_node_id is not null
order by --name --parent_node_id
bn.search_rank + bnk.search_rank desc

update bik_node set search_rank = 1 where id = 640743



select * from bik_node where is_deleted = 0
and descr is not null


BACKUP DATABASE boxi TO DISK='d:\temp\bak\boxi.bak'



select SERVERPROPERTY('IsFullTextInstalled')

select * from bik_node_kind


select * from bik_sapbo_extradata
where webi_doc_properties is not null


alter table bik_node add search_rank int not null default 0;

alter table bik_node_kind add search_rank int not null default 0;


    select distinct bn.node_kind_id
    from
      bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
      inner join bik_tree bt on bn.tree_id = bt.id
    where bn.is_deleted = 0 and bn.linked_node_id is null and contains(bn.*, 'baza')


    select count(*)
    from
      bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
      inner join bik_tree bt on bn.tree_id = bt.id
    where bn.is_deleted = 0 and bn.linked_node_id is null and contains(bn.*, 'baza')


select * from bik_app_prop

insert into bik_app_prop (name, val, is_editable) values ('useFullTextIndex', 'false', 1)

select * from bik_node where id = 620738


select * from bik_sapbo_extradata
where node_id = 620738


select * from bik_node_kind 

select * from bik_admin


select node_kind_id, min(tree_id), max(tree_id)
from bik_node
where is_deleted = 0 and linked_node_id is null
group by node_kind_id
having min(tree_id) <> max(tree_id)




select 
    bnk.id as node_kind_id,
    bnk.code as node_kind_code,
    bnk.caption as node_kind_caption,
    bt.id as tree_id,
    bt.name as tree_name,
    bt.code as tree_code,
    count(*) as cnt
    from
      bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
      inner join bik_tree bt on bn.tree_id = bt.id
    where bn.is_deleted = 0 and bn.linked_node_id is null and contains(bn.*, 'biling')
    group by
    bnk.id,
    bnk.code,
    bnk.caption,
    bt.id,
    bt.name,
    bt.code 
    
    
select 
    *
    from
      bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
      inner join bik_tree bt on bn.tree_id = bt.id
    where bn.is_deleted = 0 and bn.linked_node_id is null and contains(bn.*, 'biling')
 
 
 select bn.*, sr.[rank] as fts_rank, 
 case when freetext(name, 'baza') then 1 else 0 end as found_in_name,
 case when freetext(descr, 'baza') then 1 else 0 end as found_in_descr
    from
    bik_node bn
    inner join freetexttable(bik_node, *, 'baza') sr on bn.id = sr.[key]
    where bn.is_deleted = 0 and bn.linked_node_id is null
    
    
select * from bik_node where name = 'sr_daiwn_DWN_efektywnosc_konsult_monthly'

sr_daiwn_DWN_Churn_weekly


create table #x (id int not null, abc numeric(5,0) null)
