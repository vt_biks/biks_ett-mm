exec sp_check_version '1.0.5';
go

insert into dbo.bik_blog(content, subject, user_id, date_added)  select content, subject, user_id, date_added from dbo.aaa_blog_entry;
go
insert into dbo.bik_blog_linked(blog_id,node_id) values (1, 291);
insert into dbo.bik_blog_linked(blog_id,node_id) values (1, 1285);
insert into dbo.bik_blog_linked(blog_id,node_id) values (1, 2245);
go
insert into dbo.bik_specialist(proj_id, spon_id, expe_id, node_id) select dbo.aaa_specialist.proj_id ,dbo.aaa_specialist.spon_id, 
dbo.aaa_specialist.expe_id,dbo.bik_node.id
from dbo.bik_node join dbo.aaa_specialist on dbo.bik_node.obj_id = convert(varchar,dbo.aaa_specialist.si_id)
go

SELECT * INTO bik_object_in_fvs FROM aaa_object_in_fvs;
go
SELECT * INTO bik_object_in_history FROM aaa_object_in_history;
go

INSERT INTO bik_data_load_log (data_source_type_id,creation_time,status,status_description)
SELECT data_source_type_id,creation_time,status,status_description FROM aaa_data_load_log
go

alter table bik_node
add descr varchar(max) null;
go

update bik_node 
set descr = aaa_additional_descr.descr
from bik_node inner join aaa_teradata_object on bik_node.obj_id = aaa_teradata_object.branch_names
  inner join aaa_additional_descr on aaa_additional_descr.siId = aaa_teradata_object.id

update bik_node set descr = (select descr from aaa_additional_descr where siId=16149) where obj_id=convert(varchar,16149)
go
update aaa_attribute set atr_kind='Webi' where atr_name='Numer Rap'
insert into bik_attribute(name,node_kind_id) select a.atr_name,b.id from aaa_attribute a join bik_node_kind b on a.atr_kind=b.code
go

insert into bik_attachment(href,caption,node_id) 
select a.href,a.caption,b.id from aaa_linked_resource a join bik_node b on convert(varchar,a.si_id) = b.obj_id
go

--insert into bik_keyword (body) values ('aktywacja');
--insert into bik_keyword (body) values ('rezygnacja');
--insert into bik_keyword (body) values ('us�uga');
delete from bik_keyword;
insert into bik_keyword (body) select body from aaa_keyword

go

insert into bik_keyword_linked (keyword_id, node_id)
select
  bk.id, bn.id
from
  bik_keyword bk inner join (aaa_entity_keyword aek inner join aaa_keyword ak on aek.keyword_id = ak.id)
  on bk.body = ak.body inner join bik_node bn on bn.obj_id = cast(aek.si_id as varchar(100))
go  


--insert into bik_keyword_linked (keyword_id,node_id) values (1,14963);
--insert into bik_keyword_linked (keyword_id,node_id) values (2,949);
--insert into bik_keyword_linked (keyword_id,node_id) values (3,14963);
go



CREATE TABLE bik_joined_objs(
	id int IDENTITY(1,1) NOT NULL,
	src_node_id int NOT NULL,
	dst_node_id int NOT NULL,
	dst_kind_id int references bik_node_kind (id) not null,
	dst_name varchar (2000) NOT NULL)
GO

-- delete from bik_joined_objs

insert into bik_joined_objs (src_node_id,dst_node_id,dst_kind_id,dst_name) 
select c.id,d.id,b.id,a.dst_name  from aaa_joined_objs a join bik_node_kind b on a.dst_type=b.code 
join bik_node c on convert(varchar,a.src_id)=c.obj_id  join bik_node d on convert(varchar,a.dst_id)=d.obj_id
go


insert into bik_joined_objs (src_node_id,dst_node_id,dst_kind_id,dst_name) 
select c.id,d.id, d.node_kind_id,a.dst_name  
--*
from aaa_joined_objs a --join bik_node_kind b on a.dst_type=b.code 
join (bik_node c inner join aaa_teradata_object td on c.obj_id = td.branch_names) on a.src_id = td.id 
join (bik_node d inner join aaa_teradata_object td2 on d.obj_id = td2.branch_names) on a.dst_id = td2.id
go


insert into bik_joined_objs (src_node_id,dst_node_id,dst_kind_id,dst_name) 
select c.id,d.id, d.node_kind_id,a.dst_name  
--*
from aaa_joined_objs a --join bik_node_kind b on a.dst_type=b.code 
join (bik_node c inner join aaa_teradata_object td on c.obj_id = td.branch_names) on a.src_id = td.id 
join bik_node d on convert(varchar,a.dst_id)=d.obj_id
go


insert into bik_joined_objs (src_node_id,dst_node_id,dst_kind_id,dst_name) 
select c.id,d.id, d.node_kind_id,a.dst_name  
--*
from aaa_joined_objs a --join bik_node_kind b on a.dst_type=b.code 
join bik_node c on convert(varchar,a.src_id)=c.obj_id
join (bik_node d inner join aaa_teradata_object td2 on d.obj_id = td2.branch_names) on a.dst_id = td2.id
go


-- select * from bik_node where id = 11905

exec sp_update_version '1.0.5', '1.0.6';
go
