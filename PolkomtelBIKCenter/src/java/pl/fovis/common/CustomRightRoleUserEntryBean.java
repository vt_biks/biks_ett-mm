/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import simplelib.BeanWithIntIdBase;

/**
 *
 * @author pmielanczuk
 */
public class CustomRightRoleUserEntryBean extends BeanWithIntIdBase {

    public int roleId;
    public int userId;
    public Integer treeId;
    public Integer nodeId;
    public int groupId;
    //---
    public String branchIds;
}
