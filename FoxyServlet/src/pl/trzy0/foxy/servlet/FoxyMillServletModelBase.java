/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import pl.trzy0.foxy.serverlogic.FoxyUrlMaker;
import commonlib.IRequestContext;
import pl.trzy0.foxy.serverlogic.FoxyMillModelBaseEx;
import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.foxys.foxymill.CacheStorage;
import pl.foxys.foxymill.IDynamicCustomTagsModel;
import pl.foxys.foxymill.IRenderable;
import pl.trzy0.foxy.commons.templates.INamedTemplateProvider;
import pl.trzy0.foxy.glass.Glass;
import pl.trzy0.foxy.glass.GlassToText;
import pl.trzy0.foxy.serverlogic.FoxyUrlRewriter;
import pl.trzy0.foxy.serverlogic.IApplicationEnvironment;
import pl.trzy0.foxy.serverlogic.IFoxyService;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.MapHelper;
import simplelib.Pair;
import simplelib.SimpleDateUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class FoxyMillServletModelBase extends FoxyMillModelBaseEx implements IDynamicCustomTagsModel {

    public static final String GETGLOBAL_VERSION_SEPARATOR = "$";
    public static final String PREV_PARAMS_SESSION_KEY_NAME = "_prevParams_";
    public static final String VER_EXTENDS_PREFIX = "ver_";
    public static final String VER_EXTENDS_SUFFIX = "_extends";
    private static final ILameLogger logger = LameUtils.getMyLogger();
    //private static final Set<String> defParamNames = BaseUtils.splitUniqueBySep("_ver,lang", ",");
    public HttpServletRequest request;
    public HttpServletResponse response;
    private Map<String, IRenderable> boxes;
    public IFoxyService service;
    public String pageName;
    public MapHelper<String> params;
    public MapHelper<String> fullParams;
    // Witku, po co ta inicjalizacja tutaj, skoro na init jest to wyciągane z sesji???
    public MapHelper<String> prevParams; // tak było --> = new MapHelper<String>(new HashMap<String, Object>());
    private Map<String, Object> globals;
    private CacheStorage cacheStorage;
    private Map<String, Properties> textsProps;
    private String clientLang;
    private String contentLang;
    private Map<String, INamedTemplateProvider> tmplProviders;
    private String ver;
    private boolean isVerSet;
    protected StandardBundleDerivedLogic bundleLogic;
    //protected Map<String, Object> paramFixes;
    public FoxyUrlMaker urlMaker;
    protected Map<String, Object> defaultParamsVals;
    protected FoxyUrlRewriter urlRewriter;
    protected IApplicationEnvironment appEnv;
    protected IRequestContext reqCtx;

    protected void customPreInit() {
        // no-op
    }

    protected void customInit() {
        // no-op
    }

    protected void customFinalInit() {
        // no-op
    }

    public void init(HttpServletRequest request, HttpServletResponse response,
            IFoxyService service,
            String pageName, MapHelper<String> params,
            StandardBundleDerivedLogic bundleLogic,
            FoxyUrlRewriter urlRewriter,
            IApplicationEnvironment appEnv //            Map<String, IRenderable> boxes,
            //            Map<String, Object> globals,
            //            //Properties props,
            //            MapHelper<String> sysProps,
            //            Map<String, Properties> textsProps,
            //            CacheStorage cacheStorage,
            //            Map<String, INamedTemplateProvider> tmplProviders
            ) {

        this.bundleLogic = bundleLogic;
        FoxyMillBundleData bundleData = bundleLogic.bundleData;
        super.init(bundleData.sysProps);
        this.request = request;
        this.response = response;
        this.service = service;
        this.pageName = isStrEmptyOrWhiteSpace(pageName) ? getMainPageName() : pageName;
        this.params = params;
        this.urlRewriter = urlRewriter;
        this.appEnv = appEnv;

        this.textsProps = bundleData.textsProps;
        this.boxes = bundleData.boxes;
        this.globals = bundleData.globals;
        this.cacheStorage = bundleData.renderEnv.cacheStorage;
        this.tmplProviders = bundleData.tmplProviders;

        // Witku, właśne m. in. dlatego chcę mieć wgląd w twój kod - wtedy kiedy
        // go robisz. Tu mi śmierdzi olejem z rybek na kilometr, niby spoko,
        // ale wiadomo, że kto potem będzie na FoxyMillu pamiętał, aby sprawdzić
        // czy aby prevParams nie są nullem? No właśnie - Ty sam - autor kodu
        // o tym nie pamiętasz i piszesz:
        //   value="%{prevParams.getString('login')}%"
        // no przecież NullPointer się kłania...
        //prevParams = (MapHelper<String>) getSessionProp(PREV_PARAMS_SESSION_KEY_NAME);
        @SuppressWarnings("unchecked")
        MapHelper<String> ppAux = (MapHelper<String>) getSessionProp(PREV_PARAMS_SESSION_KEY_NAME,
                MapHelper.emptyImmutable());
        this.prevParams = ppAux;

        defaultParamsVals = new HashMap<String, Object>();

        customPreInit();

        initLang();

        customInit();

        defaultParamsVals.put("_ver", getDefaultSiteVerName());

        saveParamsAndCreateUrlMaker();

        customFinalInit();
    }

    protected void setUrlMaker() {
        this.reqCtx = createRequestContext();
        setResponseInReqCtx();
        this.urlMaker = createUrlMaker();
    }

    protected void saveParamsAndCreateUrlMaker() {
        this.fullParams = new MapHelper<String>(this.params.helperAsMap()).put("page", this.pageName);

        setUrlMaker();

        setSessionProp(PREV_PARAMS_SESSION_KEY_NAME, this.fullParams);
    }

    protected void setContentLangValue(String contentLangValCandidate) {
        String realCL = BaseUtils.safeToLowerCase(contentLangValCandidate);
        contentLang = realCL;

        Set<String> supportedLangs = getSupportedContentLangs();

        if (BaseUtils.isStrEmptyOrWhiteSpace(contentLang) || !supportedLangs.contains(contentLang)) {
            contentLang = suggestLangByRequestHeader();
        }

        clientLang = bundleLogic.getClientLangForContentLang(contentLang);

//        String urlLang = getLangFromRequest();
//        if (!BaseUtils.safeEquals(urlLang, contentLang)) {
//            System.out.println(" *** fix lang = " + contentLang);
        addParamFix("lang", contentLang);
//        }

        setSessionProp("_langInSession_", contentLang);

//        System.out.println(" *** setContentLangValue contentLang=" + contentLang
//                + ", clientLang=" + clientLang);
    }

    protected void initLang() {
        setContentLangValue(getLangFromRequest());
    }

    protected void addParamFix(String paramName, Object paramVal) {
        params.helperAsMap().put(paramName, paramVal);
//        if (paramFixes == null) {
//            paramFixes = new HashMap<String, Object>();
//        }
//        paramFixes.put(paramName, paramVal);
    }

    protected IRequestContext createRequestContext() {
        return new FoxyHttpModelRequestContext(request, //response,
                fullParams,
                urlRewriter, appEnv.getUsesSubdomains());

//                IRequestContext() {
//
//            private Map<String, Object> paramsAsMap = fullParams.helperAsMap();
//            private String subLocal = FoxyServletUtils.getLocalServletUrl(request);
//            private String subRemote = FoxyServletUtils.getRemoteServletUrl(request);
//            private String rubLocal = FoxyServletUtils.getLocalWebAppUrl(request);
//            private String rubRemote = FoxyServletUtils.getRemoteWebAppUrl(request);
//            private String method = request.getMethod();
//
//            public String getRequestMethod() {
//                return //request.getMethod();
//                        method;
//            }
//
//            public String encodeUrl(String url) {
//                String res = response.encodeURL(url);
//                System.out.println("$$$$$$$$$$$$$$$$ encode url: old=" + url + ", new=" + res);
//                return res;
//            }
//
//            public String getServletUrlBase(boolean forRemoteURL) {
//                return forRemoteURL ? //FoxyServletUtils.getRemoteWebAppUrl(request)
//                        //request.getRequestURL().toString()
//                        //FoxyServletUtils.getRemoteServletUrl(request)
//                        subRemote
//                        : //request.getRequestURI();
//                        //FoxyServletUtils.getLocalServletUrl(request)
//                        subLocal;
//            }
//
//            public Map<String, Object> getParams() {
//                return paramsAsMap;
//            }
//
//            public String getResourceUrlBase(boolean forRemoteURL) {
//                return forRemoteURL
//                        ? //FoxyServletUtils.getRemoteWebAppUrl(request)
//                        rubRemote
//                        : //FoxyServletUtils.getLocalWebAppUrl(request);
//                        rubLocal;
//            }
//        };
    }

    protected FoxyUrlMaker createUrlMaker() {
        return new FoxyUrlMaker(reqCtx,//, params.helperAsMap()
                defaultParamsVals);
    }

    public String makeResourceUrl(String resource, boolean forceForRemote) {
        return urlMaker.makeResourceUrl(resource, forceForRemote);
        //return FoxyServletUtils.getRemoteWebAppUrl(request) + (resource.startsWith("/") ? "" : "/") + resource;
    }

    public String makeResourceUrl(String resource) {
        return urlMaker.makeResourceUrl(resource, false);
        //return FoxyServletUtils.getRemoteWebAppUrl(request) + (resource.startsWith("/") ? "" : "/") + resource;
    }

    public String makeUrl(boolean forRemoteUrl, String pageName, Map<String, Object> newParams) {
        return urlMaker.makeUrl(forRemoteUrl, pageName, newParams);
    }

    public String makeUrl(String pageName, Map<String, Object> newParams) {
        return //urlMaker.makeUrl(pageName, newParams);
                makeUrl(false, pageName, newParams);
    }

    public String makeRemoteUrl(String pageName, Map<String, Object> newParams) {
        return //urlMaker.makeRemoteUrl(pageName, newParams);
                makeUrl(true, pageName, newParams);
//        newParams = extendParamsWithDefaults(newParams);
//        return response.encodeUrl(
//                request.getRequestURL() + "?page=" + UrlMakingUtils.encodeForURLNoExcWWOldLame(pageName)
//                + (BaseUtils.isMapEmpty(newParams) ? ""
//                : "&" + UrlMakingUtils.encodeParamsForUrlWW(newParams)));
    }

    protected Map<String, Object> makeParamsForSelfUrl(Map<String, Object> newParams) {
        Map<String, Object> newParamsFull = new LinkedHashMap<String, Object>(this.params.helperAsMap());
        newParamsFull.putAll(newParams);
        return newParamsFull;
    }

    public String makeSelfUrl(Map<String, Object> newParams) {
        // Wezyrze, nie tak programujemy! brzydkie c&p!
//        Map<String, Object> newParamsFull = new LinkedHashMap<String, Object>(this.params.helperAsMap());
//        newParamsFull.putAll(newParams);
//        return makeUrl(pageName, newParamsFull);
        return makeUrl(pageName, makeParamsForSelfUrl(newParams));
    }

    public String makeSelfRemoteUrl(Map<String, Object> newParams) {
        // Wezyrze, nie tak programujemy! brzydkie c&p!
//        Map<String, Object> newParamsFull = new LinkedHashMap<String, Object>(this.params.helperAsMap());
//        newParamsFull.putAll(newParams);
//        return makeRemoteUrl(pageName, newParamsFull);
        return makeRemoteUrl(pageName, makeParamsForSelfUrl(newParams));
    }
//    public IRenderable getBox(String name) {
//        return boxes.get("mainBox/" + name);
//    }
//

    protected IRenderable innerGetPage(String name) {
        return boxes.get(name);
    }

    public IRenderable getPage(String name) {
        IRenderable res = null;

        String v = getVersionFromRequest();

        while (res == null && v != null) {
            res = innerGetPage(name + "_" + v);
            if (res == null) {
                v = getSysProp(VER_EXTENDS_PREFIX + v + VER_EXTENDS_SUFFIX);
            }
        }

        if (res == null) {
            res = innerGetPage(name);
        }

        return res;
    }

    protected Object innerGetGlobal(String name) {
        return globals.get(name);
    }

    public void sendRedirect(String url) {
        //url = response.encodeRedirectURL(url);
//        String rquri = (String) request.getParameter("rquri");
        //System.out.println("sendRedirect() - url=" + url + " rquri=" + rquri);
        try {
            response.sendRedirect(url);
        } catch (Exception ex) {
            throw new LameRuntimeException("sendRedirect: " + url, ex);
        }
        abortRendering();
    }

    protected List<Pair<String, Boolean>> getCreatedMsgs(boolean resetNewCopy) {
        @SuppressWarnings("unchecked")
        Map<String, Object> customSessionData = service.getCustomSessionData();
        @SuppressWarnings("unchecked")
        List<Pair<String, Boolean>> msgs = (List<Pair<String, Boolean>>) customSessionData.get("messages");
        if (msgs == null) {
            msgs = new ArrayList<Pair<String, Boolean>>();
            customSessionData.put("messages", msgs);
        }
        if (resetNewCopy) {
            customSessionData.put("messages", new ArrayList<Pair<String, Boolean>>());
        }
        return msgs;
    }

    public void addMsg(String msg, boolean isError) {
        List<Pair<String, Boolean>> msgs = getCreatedMsgs(false);
        msgs.add(new Pair<String, Boolean>(msg, isError));
    }

    public void addErrorMsg(String msg) {
        addMsg(msg, true);
    }

    public void addInfoMsg(String msg) {
        addMsg(msg, false);
    }

    public Collection<Pair<String, Boolean>> getMsgsAndClear() {
        return getCreatedMsgs(true);
    }

    public void setSessionProp(String name, Object val) {
        @SuppressWarnings("unchecked")
        Map<String, Object> customSessionData = service.getCustomSessionData();
        customSessionData.put(name, val);
    }

    public Object getSessionProp(String name, Object defVal) {
        return nullToDef(getSessionProp(name), defVal);
    }

    public Object getSessionProp(String name) {
        @SuppressWarnings("unchecked")
        Map<String, Object> customSessionData = service.getCustomSessionData();
        return customSessionData.get(name);
    }
    private PerformanceTestBean ptb;

    public PerformanceTestBean getPtb() {
        if (ptb == null) {
            ptb = new PerformanceTestBean();
            ptb.repeats = params.getInt("cnt", 10000);
        }
        return ptb;
    }

    public void invalidateCacheItem(Object key) {
        if (cacheStorage != null) {
            cacheStorage.invalidateItem(key);
        }
    }

    public void setCacheSubKeys(Object key, Collection<Object> subKeys) {
        if (cacheStorage != null) {
            cacheStorage.setCacheSubKeys(key, subKeys);
        }
    }

    public void invalidateSubItem(Object subKey) {
        if (cacheStorage != null) {
            cacheStorage.invalidateSubItem(subKey);
        }
    }

    public String getContentLang() {
        return contentLang;
    }

    public Set<String> getSupportedContentLangs() {
        return bundleLogic.getSupportedClientLangs();
    }

    public String getDefaultContentLang() {
        return bundleLogic.getDefaultClientLang();
    }

    protected String suggestLangByRequestHeader() {
        String accLng = ""; //request.getHeader("Accept-Language");
        String defLng = getDefaultContentLang();

        String res = BaseUtils.getBestAcceptedAndSupportedLanguage(
                accLng,
                getSupportedContentLangs(), defLng);

        return res;
    }

    protected String getLangFromRequest() {
        String res = params.getString("lang");

        if (BaseUtils.isStrEmptyOrWhiteSpace(res)) {
            res = (String) getSessionProp("_langInSession_");
        }

        return res;
    }

    public String getClientLang() {
        return clientLang;
    }

    public String getText(String key) {
        return getText(key, null /*key*/);
    }

    public String getText(String key, String defVal) {
        String cL = getClientLang();

        if (logger.isDebugEnabled()) {
            logger.debug("key=" + key + ", clientLang=" + cL + ", props lang keys=" + textsProps.keySet());
        }

        Pair<String, String> p = BaseUtils.splitString(key, "=");
        if (p.v2 != null) {
            defVal = p.v2;
        }

        String res = textsProps.get(cL).getProperty(p.v1, defVal);
        if (res == null) {
            String defCL = getDefaultContentLang();
            if (BaseUtils.safeEquals(cL, defCL)) {
                res = p.v1;
            } else {
                res = textsProps.get(defCL).getProperty(p.v1, p.v1);
            }
        }
        return res;
    }

    public String getShortTimeElapsed(Date pastDate) {
        if (pastDate == null) {
            return getText("shortTimeAgo.never");
        }
        return SimpleDateUtils.getShortElapsedTimeStr(pastDate, new Date(),
                getText("shortTimeAgo.days"),
                getText("shortTimeAgo.hours"),
                getText("shortTimeAgo.mins"),
                getText("shortTimeAgo.secs"),
                sysProps.getInt("shortTimeAgo.compactFactor", 2));
    }

    public String getShortTimeAgo(Date pastDate) {
        if (pastDate == null) {
            return getText("shortTimeAgo.never");
        }
        return getShortTimeElapsed(pastDate) + " " + getText("shortTimeAgo.ago");
    }

    public String glassAsText(Glass g) {
        if (logger.isDebugEnabled()) {
            logger.debug("glassAsText()...");
        }
        INamedTemplateProvider tmplProvider = tmplProviders.get(getClientLang());
        String result = GlassToText.asTextEx(tmplProvider, g, getSysProp("glass.resourceKeyPrefix"), this);
        if (logger.isDebugEnabled()) {
            logger.debug("glassAsText() = " + result);
        }
        return result;
    }

    public Glass makeGlass(Object key) {
        return new Glass(key);
    }

    public Glass makeGlass(Object key, Object... args) {
        return new Glass(key, args);
    }

    public Object getHtmlFromGlass(String key) {
        return raw(glassAsText(makeGlass(key)));
    }

    public String getTextFromGlass(String key, Object... args) {
        return glassAsText(makeGlass(key, args));
    }

    protected String getDefaultSiteVerName() {
        return getSysProp("defaultSiteVerName");
    }

    protected String innerGetVersionFromRequest() {
        return params.getString("_ver", getDefaultSiteVerName());
    }

    // empty string <==> null, null is returned for empty or whitespace string
    public String getVersionFromRequest() {
        if (!isVerSet) {
            ver = innerGetVersionFromRequest();
            if (BaseUtils.isStrEmptyOrWhiteSpace(ver)) {
                ver = null;
            }
            isVerSet = true;
        }

        return ver;
    }

    public boolean isLanguageFixedInRequest() {
        return false;
    }

    public Object getGlobal(String name) {
        Object res = null;

        String v = getVersionFromRequest();

        while (res == null && v != null) {
            res = innerGetGlobal(v + GETGLOBAL_VERSION_SEPARATOR + name);
            if (res == null) {
                v = getSysProp(VER_EXTENDS_PREFIX + v + VER_EXTENDS_SUFFIX);
            }
        }

        if (res == null) {
            res = innerGetGlobal(name);
        }
        return res;
    }

    public Object getCustomTagVal(String tagName) {
        return getGlobal(tagName);
    }

    public boolean isUrlMalformed() {
//        String lang = params.getString("lang");
//        return !BaseUtils.collectionContainsFix(getSupportedClientLangs(), lang);
        return false;
    }

    public String getHardRedirectionUrl() {
        return null; // no redir
    }

    public String getSoftRedirectionUrl() {
        return null; // no redir
    }

    public boolean isOnLanguageSensitivePage() {
        return bundleLogic.isLanguageSensitivePage(pageName);
    }

    public String getMainPageName() {
        return bundleLogic.getMainPageName();
    }

    public String makeChangeLangUrl(String newContentLang) {
        Map<String, Object> newParams = new HashMap<String, Object>();
        newParams.put("lang", newContentLang);
        return isOnLanguageSensitivePage()
                ? makeUrl(bundleLogic.getMainPageName(), newParams)
                : makeSelfUrl(newParams);
    }

    // to tylko alias dla isStrEmptyOrWhiteSpace
    public boolean isValNullOrEmpty(String val) {
        return isStrEmptyOrWhiteSpace(val);
        //return BaseUtils.isStrEmptyOrWhiteSpace(val);
    }

    public boolean isParamNullOrEmpty(String paramName) {
        return isValNullOrEmpty(params.getString(paramName));
    }

    // returns true -> was for error
    public boolean addMsgFromGlass(Glass g) {
        boolean isError = g != null && g.isForError(true);
        if (g != null) {
            addMsg(glassAsText(g), isError);
        }
        return isError;
    }

    public void addMsgAndSendRedirect(Glass g, String successPageName, Map<String, Object> successParams,
            String errorPageName, Map<String, Object> errorParams) {
//        boolean isError = g != null && g.isForError(true);
//        if (g != null) {
//            addMsg(glassAsText(g), isError);
//        }
        boolean isError = addMsgFromGlass(g);
        sendRedirect(makeUrl(isError ? errorPageName : successPageName,
                isError ? errorParams : successParams));
    }

    public void addMsgAndSendRedirect(Glass g, String newPageName, Map<String, Object> newParams) {
        addMsgAndSendRedirect(g, newPageName, newParams, newPageName, newParams);
    }

    public void fixContentLang(String fixedCL) {
        setContentLangValue(fixedCL);
        saveParamsAndCreateUrlMaker();
    }

    public void fixVersionOfRequest(String newVer, boolean recalcUrlMaker) {
        isVerSet = false;
        params.helperAsMap().put("_ver", newVer);
        defaultParamsVals.put("_ver", newVer);
        if (recalcUrlMaker) {
            saveParamsAndCreateUrlMaker();
        }
    }

    public void rememberThisRequestAndRedirectToLogon() {
        setSessionProp("_actionRequestModel", this);
        sendRedirect(makeUrl("login", null));
    }

    protected void resetResponse(HttpServletResponse response) {
        this.response = response;
        setResponseInReqCtx();
    }

    public void proceedWithActionRequestModel(FoxyMillServletModelBase proceedModel) {
        //proceedModel.response = this.response;
        proceedModel.resetResponse(response);
        proceedModel.returnFromRenderingFlag = null;
        proceedModel.rerenderModel = null;
        //throw new RerenderWithNewModelException(proceedModel);
        rerenderModel = proceedModel;
    }

    protected void setResponseInReqCtx() {
        if (reqCtx instanceof IHttpResponseAware) {
            ((IHttpResponseAware) reqCtx).setHttpResponse(response);
        }
    }

    public void setResponseContentType(String contentType) {
        response.setContentType(contentType);
    }

    public void declareAjaxResposeType() {
        setResponseContentType("text/javascript; charset=UTF-8");
    }
}
//    protected Object getFixedParamVal(String paramName) {
//        if (paramFixes != null) {
//            Object val = paramFixes.get(paramName);
//            if (val != null) {
//                return val;
//            }
//        }
//        return this.params.get(paramName);
//    }
//    protected Map<String, Object> setNewParamVal(Map<String, Object> newParams,
//            String paramName, Object paramVal) {
//        if (newParams == null) {
//            newParams = new HashMap<String, Object>();
//        }
//        newParams.put(paramName, paramVal);
//        return newParams;
//    }
//
//    protected Map<String, Object> extendParamsWithDefault(Map<String, Object> newParams, String paramName) {
//        if (!BaseUtils.isStrEmpty(this.params.getString(paramName))) {
//
//            if (newParams == null || !newParams.containsKey(paramName)) {
//                newParams = setNewParamVal(newParams, paramName, this.params.getString(paramName));
//            }
//
//        }
//        return newParams;
//    }
//    protected Map<String, Object> extendParamsWithDefaultParams(Map<String, Object> newParams,
//            Collection<String> defParamNames) {
//        for (String paramName : defParamNames) {
//            newParams = extendParamsWithDefault(newParams, paramName);
//        }
//        return newParams;
//    }
//    protected Map<String, Object> extendParamsWithDefaults(Map<String, Object> newParams) {
//        return extendParamsWithDefaultParams(newParams, defParamNames);
//    }
//    private String paramSeparator(String url) {
//        return url.indexOf('?') == -1 ? "?" : "&";
//    }
//
//    private String cleanRquri(String rquri) {
//        String cleanUrl = rquri.replaceAll("[&?]nu=.*", "");
////        System.out.println("cleanRquri() - cleanUrl=" + cleanUrl);
//        return cleanUrl;
//    }
//    protected Set<String> getDefaultParamNames() {
//        return defParamNames;
//    }
//
//
//    protected Set<String> getParamsOrder() {
//        return getDefaultParamNames();
//    }
//
//    private String alterUrlForCms(String url, String rquri) {
////        String r2;
////        try {
////            // Witku, what the fuck? przecież w tej klasie widać, że nie
////            // korzystamy z URLEncoder tylko z LameUtils.encodeForURLNoExcWWOldLame
////            // ZAWSZE pisząc nowy kod należy zastanowić się jaki jest już
////            // zastany zwyczaj programowania, używane metody itp. i używać
////            // tych samych, chyba że są konkretne powody aby tak nie robić
////            // ale wtedy należy je wpisać w komentarzu lub najlepiej omówić
////            // ze mną (WW)
////            r2 = URLEncoder.encode(url, "UTF-8");
////        } catch (UnsupportedEncodingException ex) {
////            ex.printStackTrace();
////            throw new RuntimeException(ex);
////        }
//        // Witku, taka wersja jest właściwa dla Foxys, bo używana już
//        // wiele razy w innym miejscach, w tym tutaj (w tej klasie)
//        String r2 = UrlMakingUtils.encodeStringForUrlWW(url);
//        String cleanHostUrl = cleanRquri(rquri);
//        String ps = paramSeparator(cleanHostUrl);
//        String alteredUrl = cleanHostUrl + ps + "nu=" + r2;
////        System.out.println("alterUrlForCms() - alteredUrl=" + alteredUrl);
//        return alteredUrl;
//    }
//    @Deprecated
//    public String makeUrlOld(String pageName, Map<String, Object> newParams) {
//        String currentPage = request.getParameter("page");
//
//        String rquri = (String) request.getParameter("rquri");
//        if ((rquri != null && "POST".equals(request.getMethod()))
//                || (currentPage != null && currentPage.startsWith("action_"))) {
//            newParams = setNewParamVal(newParams, "rquri", rquri);
//        }
//        newParams = extendParamsWithDefaults(newParams);
//        String result =
//                "?page=" + UrlMakingUtils.encodeStringForUrlWW(pageName)
//                + (BaseUtils.isMapEmpty(newParams) ? ""
//                : "&" + UrlMakingUtils.encodeParamsForUrlWW(newParams));
//
//        // --------------------------------------------------------------------
//        // Witku, to jest lepsza wersja kodu, jaka spełnia standardy Foxys
//        // co do stylu itp. starą wersję znajdziesz zakomentowaną poniżej
//        // --------------------------------------------------------------------
//        result = request.getRequestURI() + result;
//        result = response.encodeUrl(result);
//
//        String aCase;
//        if (rquri == null) {
//            aCase = "rquri==null";
//        } else if ("POST".equals(request.getMethod())) {
//            aCase = "POST";
//        } else if (currentPage != null && currentPage.startsWith("action_")) {
//            aCase = "action_";
//        } else {
//            aCase = "GET";
//            result = alterUrlForCms(result, rquri);
//        }
//
//        // --------------------------------------------------------------------
//        // Witku, tak nie programujemy, nie chcę czegoś takiego więcej widzieć!
//        // wersja zgodna ze stylem Foxys jest powyżej
//        // --------------------------------------------------------------------
////        String aCase = "unknown";
////        if (rquri == null) {
////            aCase = "rquri==null";
////            result = request.getRequestURI() + result;
////            result = response.encodeUrl(result);
////        } else if ("POST".equals(request.getMethod())) {
////            aCase = "POST";
////            result = request.getRequestURL() + result;
////            result = response.encodeUrl(result);
////        } else if (currentPage != null && currentPage.startsWith("action_")) {
////            aCase = "action_";
////            result = request.getRequestURL() + result;
////            result = response.encodeUrl(result);
////        } else {
////            aCase = "GET";
////            result = request.getRequestURL() + result;
////            result = response.encodeUrl(result);
////            result = alterUrlForCms(result, rquri);
////        }
//        //System.out.println("[" + aCase + "] currentPage=" + currentPage + " pageName=" + pageName + " result=" + result);
//        return result;
//    }

