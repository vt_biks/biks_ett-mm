/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IContinuation;

/**
 *
 * @author mmastalerz
 */
public class GenericBeanGridWidget<T> {

    protected IGridGenericBeanPropsExtractor<T> extr;
    protected boolean widgetsActive;
    protected boolean isWidgetRunFromDialog;
    protected List<T> beansToDisplay;
    protected int pageNum;
    protected int packSize;
    protected IContinuation dataChangedHandler;
    protected FlexTable ft = new FlexTable();
    protected String rowStyleNameEven;
    protected String rowStyleNameOdd;

    public GenericBeanGridWidget(boolean widgetsActive, IGridGenericBeanPropsExtractor<T> extr,
            boolean paddedCells) {
        this.widgetsActive = widgetsActive;
        this.extr = extr;
        //ww: zwykłego Ex nie używamy, bo chcemy nadać taki styl (paddingi)
        // który zepsuje część gridów NodeAware (one używają sufiksu Ex)
        String styleOptExSuffix = paddedCells ? " paddedCells" : "";
        String stylePrefix = "RelatedObj-oneObj-";
        this.rowStyleNameEven = stylePrefix + "grey" /* I18N: no */ + styleOptExSuffix;
        this.rowStyleNameOdd = stylePrefix + "white" /* I18N: no */ + styleOptExSuffix;

        extr.setDataChangedHandler(new IContinuation() {
            public void doIt() {
                dataChanged();
            }
        });
    }

    public GenericBeanGridWidget(boolean widgetsActive, IGridGenericBeanPropsExtractor<T> extr) {
        this(widgetsActive, extr, false);
    }

    public void rebuildGrid() {
        ft.removeAllRows();
        innerBuildGrid();
    }

    protected void dataChanged() {
        if (dataChangedHandler != null) {
            dataChangedHandler.doIt();
        } else {
            rebuildGrid();
        }
    }

    protected void innerBuildGrid() {
        ft.setStyleName("gridJoinedObj");
        ft.setWidth("100%");
        addBeansToGrid(ft, pageNum * packSize, beansToDisplay, 0, packSize);
    }

    public Widget buildGrid(final List<T> allBeans,
            final List<T> beansToDisplay,
            final int pageNum, int packSize) {
        this.beansToDisplay = beansToDisplay;
        this.pageNum = pageNum;
        this.packSize = packSize;
        extr.setAllBeansList(allBeans);
        innerBuildGrid();
        return ft;
    }

    public boolean createGridColumnNames(FlexTable gp) {
        return false;
    }

    protected void addBeansToGrid(final FlexTable gp, int listIdx, List<T> beansToBeAddedToGrid,
            int gridIdx, int packSize) {

        int listSize = beansToBeAddedToGrid.size();
        if (extr.createGridColumnNames(gp)) {
            gridIdx++;
        }

        for (int i = 0; i < packSize && listIdx < listSize; i++) {
            T e = beansToBeAddedToGrid.get(listIdx++);
            createRowFromBean(e, gridIdx, gp, extr);
            gp.getRowFormatter().setStyleName(gridIdx,
                    gridIdx % 2 == 0 ? rowStyleNameEven : rowStyleNameOdd);
            gridIdx++;
        }
    }

    protected void setColWidth(int row, int col, Integer widthInPxOrNullFor100Percent) {
        ft.getCellFormatter().setWidth(row, col, widthInPxOrNullFor100Percent == null
                ? "10000px" : widthInPxOrNullFor100Percent + "px");
    }

    public void createRowFromBean(final T item, int gridIdx, FlexTable gp, IGridGenericBeanPropsExtractor<T> extr) {
        int col = 0;
        if (extr.hasIcon()) {
            //gp.getColumnFormatter().setWidth(col, "26px");
            setColWidth(gridIdx, col, 26);
            gp.setWidget(gridIdx, col++, BIKClientSingletons.createIconImg(extr.getIconUrl(item), "16px", "16px",
                    extr.getOptIconHint(item)));
        }

        String itemName = extr.getNameAsHtml(item);
        if (itemName != null) {
            gp.setHTML(gridIdx, col, itemName);
            col++;
        }

        for (int i = 0; i < extr.getAddColCnt(); i++) {
            IsWidget val = getCellValAsWidget(extr.getAddColVal(item, i));

            Widget wdg = val == null ? null : val.asWidget();

            if (wdg instanceof Image) {
                setColWidth(gridIdx, col, 32);
            }

            String optTitle = extr.getOptAddColTitle(item, i);
            if (optTitle != null && wdg != null) {
                wdg.setTitle(itemName);
            }

            gp.setWidget(gridIdx, col++, wdg);
        }

        for (int i = 0; i < extr.getActionCnt(); i++) {
            final IGridBeanAction<T> act = extr.getActionByIdx(i);

            String actionIconUrl = act.getActionIconUrl(item);

            if (actionIconUrl != null && act.isVisible(item)) {
                //gp.getColumnFormatter().setWidth(col, "26px");
                gp.setWidget(gridIdx, col++, createActionBtn(act, actionIconUrl, item));
            } else {
                gp.setText(gridIdx, col++, ""); //bez tego komorki się zle czasem ukladaja.
            }
            setColWidth(gridIdx, col - 1, 32);
        }
    }

    protected PushButton createActionBtn(final IGridBeanAction<T> act, String actionIconUrl, final T item) {
        Image img = new Image(actionIconUrl);
//        if (item instanceof TreeBean && act.getActionType() == IGridBeanAction.TypeOfAction.ShowImportLog) {
//            TreeBean b = (TreeBean) item;
//            if (b.importStatus == TreeImportLogStatus.SAW.getCode()) {
//                img.getElement().setAttribute("style", "");
//            } else if (b.importStatus == TreeImportLogStatus.RUNNING.getCode()) {
//                img.getElement().setAttribute("style", "background: orange;");
//            } else if (b.importStatus == TreeImportLogStatus.SUCCESS.getCode()) {
//                img.getElement().setAttribute("style", "background: blue;");
//            } else if (b.importStatus == TreeImportLogStatus.ERROR.getCode()) {
//                img.getElement().setAttribute("style", "background: red;");
//            }
//        }
        PushButton btn = new PushButton(img);
        btn.addStyleName("followBtn");
        btn.setTitle(act.getActionIconHint(item));
        btn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (isWidgetRunFromDialog) {
                    BaseActionOrCancelDialog.hideAllDialogs();
                }

                act.executeAction(item);
            }
        });

        //btn.setEnabled(widgetsActive && act.isEnabled(item));
        if (isWidgetRunFromDialog) {
            btn.setVisible((act.getActionType() == IGridBeanAction.TypeOfAction.Follow || act.getActionType() == IGridBeanAction.TypeOfAction.View));
        } else {
            btn.setVisible(widgetsActive && act.isEnabled(item));
        }

        return btn;
    }

    public static IsWidget getCellValAsWidget(Object val) {
        if (val instanceof IsWidget) {
            return (IsWidget) val;
        } else {
            Widget w;
            if (val instanceof SafeHtml) {
                SafeHtml h = (SafeHtml) val;
                w = new InlineHTML(h);
            } else {
                String strVal = val == null ? "" : val.toString();
                w = new InlineLabel(strVal);
            }
            return w;
        }
    }

    public void setDataChangedHandler(IContinuation handler) {
        this.dataChangedHandler = handler;
    }
}
