exec sp_check_version '1.0.86.6';
go

--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_check_version]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_check_version]
GO

create procedure [dbo].[sp_check_version] (@ver varchar(255)) as
begin	
	declare @version varchar(255)
	select @version = val from bik_app_prop where name = 'bik_ver';
	if(@version != @ver)
	begin
	    declare @txt nvarchar(4000) = N'!!! Wrong version number: ' + @ver + 
	      '. Current version is ' + @version + '. !!!'
	    print @txt
	    
		raiserror(@txt,
				20,
				-1) with log;
	end	
end
GO


--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_drop_column_if_exists_with_opt_default]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_drop_column_if_exists_with_opt_default]
GO

create procedure [dbo].[sp_drop_column_if_exists_with_opt_default](@table_name sysname, @column_name sysname) as
begin
  --declare @table_name sysname = 'bik_node', @column_name sysname = 'disable_linked_subtree'
  DECLARE @default_constraint_name sysname
  SELECT @default_constraint_name = object_name(cdefault)
  FROM syscolumns
  WHERE id = object_id(@table_name)
  AND name = @column_name
  --select @default_constraint_name
  if @default_constraint_name is not null
    EXEC ('ALTER TABLE ' + @table_name + ' DROP CONSTRAINT ' + @default_constraint_name)
  if exists(select 1 from syscolumns sc where id = OBJECT_ID(@table_name) and name = @column_name)
    EXEC ('ALTER TABLE ' + @table_name + ' DROP column ' + @column_name)
end
go


exec sp_drop_column_if_exists_with_opt_default 'bik_node', 'disable_linked_subtree'
go


alter table bik_node add disable_linked_subtree int not null default 0;
go


update bik_node set disable_linked_subtree = 1
where tree_id = (select id from bik_tree where code = 'Reports')
and linked_node_id is not null and 
(select code from bik_node_kind where id = node_kind_id) in ('Detail', 'Dimension', 'Measure')
go


--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_filter_bik_nodes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_filter_bik_nodes]
GO

create procedure [dbo].[sp_filter_bik_nodes] (@txt varchar(max), @optTreeId int, @optExtraFilterCondition varchar(max))
as
begin
  set nocount on
  
  declare @diag_level int = 0
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': start!'
  
declare @sql varchar(8000);

set @sql = 'select id from bik_node where id in (' + dbo.fn_bik_node_name_chunk_search_sql(@txt, @optTreeId) 
  + ') and name like ''%' + replace(@txt, '''', '''''') + '%'' and is_deleted = 0' +
  case when @optExtraFilterCondition is null then '' else ' and (' + @optExtraFilterCondition + ')' end

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': have sql: sql=' + @sql
  
-- ostatnio wrzucone
declare @t table (id int not null primary key, parent_node_id int null);
-- parent_node_ids ostatnio wrzuconych - te b�dziemy wrzuca� teraz
declare @p table (id int not null primary key);
-- wszystkie
declare @a table (id int not null primary key);

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': before exec sql'

  insert into @p (id)
  exec(@sql)

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': sql executed'
  
  while exists (select 1 from @p) begin
    insert into @a select id from @p;

    delete from @t;

    insert into @t (id, parent_node_id) select id, parent_node_id from bik_node where id in (select id from @p);

    delete from @p;

    insert into @p 
    select distinct parent_node_id 
    from @t 
    where parent_node_id is not null and parent_node_id not in (select id from @a);
  end;

  --select * from @a

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': after loop'

select bn.*,
    bnk.caption as node_kind_caption,
    bnk.code as node_kind_code,
    bnk.is_folder as is_folder,
    bt.name as tree_name, bt.tree_kind, bt.code as tree_code
    , case when disable_linked_subtree = 0 and exists 
        (select 1 from bik_node where is_deleted = 0 and parent_node_id = coalesce(bn.linked_node_id, bn.id)) then 0 else 1 end as has_no_children
    from bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    inner join bik_tree bt on bn.tree_id = bt.id
    where bn.id in (select id from @a)
    order by bn.name

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': after final select'
end;
go


--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

exec sp_update_version '1.0.86.6', '1.0.86.7';
go
