/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class DBDependentObjectBean implements Serializable {

    public String sourceName;
    public String sourceOwner;
    public int type; // 0 - zależność, 1 - klucz obcy
    public int sourceNodeId;
    public String treeCode;
    public String dstName;
    public String dstOwner;
    public String sourceIcon;
    public String dstColumn;
    public String sourceColumn;
    public Integer sourceColumnNodeId;
}
