/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.i18nsupport;

import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.Window;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pmielanczuk
 */
public class SupportedLocaleNameEnumerator {

    public enum EnumerationMode {

        Normal, CurrentAsFirst, CurrentAsLast, WithoutCurrent
    };
    protected static String localeQueryParamName = LocaleInfo.getLocaleQueryParam();
    protected Set<String> localeNames = GWTCurrentLocaleNameProvider.getCommonSupportedLocaleNames();
    protected String currentLocaleName = GWTCurrentLocaleNameProvider.getCurrentLocaleNameStatic();
    protected UrlBuilder urlBuilder = Window.Location.createUrlBuilder();
    protected EnumerationMode enumMode;
    // dynamic props - every iteration pass changes these props
    protected int idx = 0;
//    protected boolean isFirst = true;
    protected boolean isCurrent;
    protected IParametrizedContinuation<SupportedLocaleNameEnumerator> forOneLocaleCont;
    protected String localeName;

    public String getCurrentLocaleName() {
        return currentLocaleName;
    }

    public int getIdx() {
        return idx;
    }

    public String getLocaleName() {
        return localeName;
    }

    public boolean isFirst() {
        return idx == 0;
    }

    public boolean isLast() {
        return idx + 1 == localeNames.size();
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public void callGetOneCont(String cln, boolean skipCurrent) {
        localeName = cln;
        isCurrent = BaseUtils.safeEquals(currentLocaleName, localeName);
        if (isCurrent && skipCurrent) {
            return;
        }
        forOneLocaleCont.doIt(this);
        idx++;
    }

    public String getUrlForLocale() {
        return getUrlForLocale(urlBuilder, localeName);
    }

    public static String getUrlForLocale(UrlBuilder urlBuilder, String localeName) {
        return urlBuilder.setParameter(localeQueryParamName, localeName).buildString();
    }

    protected void enumerate() {
        if (enumMode == EnumerationMode.CurrentAsFirst) {
            callGetOneCont(currentLocaleName, false);
        }

        for (String cln : localeNames) {
            callGetOneCont(cln, enumMode != EnumerationMode.Normal);
        }

        if (enumMode == EnumerationMode.CurrentAsLast) {
            callGetOneCont(currentLocaleName, false);
        }
    }

    public static boolean isMultiLanguage() {
        return GWTCurrentLocaleNameProvider.getCommonSupportedLocaleNames().size() > 1;
    }

    public static void enumerate(EnumerationMode enumMode, IParametrizedContinuation<SupportedLocaleNameEnumerator> forOneLocaleCont) {
        SupportedLocaleNameEnumerator enumerator = new SupportedLocaleNameEnumerator();
        enumerator.enumMode = enumMode;
        enumerator.forOneLocaleCont = forOneLocaleCont;
        enumerator.enumerate();
    }
}
