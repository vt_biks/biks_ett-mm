/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

/**
 *
 * @author wezyr
 */
public class NodeRefreshingCallback<T> extends BaseNodeRefreshingCallback<T> {

    boolean isAddNodeInToTheSameTree = false;

    public NodeRefreshingCallback(int nodeId, String msgOnSuccess, String errorMsg) {
        super(nodeId, msgOnSuccess, errorMsg);
    }

    public NodeRefreshingCallback(int nodeId, String msgOnSuccess) {
        this(nodeId, msgOnSuccess, null);
    }

    public NodeRefreshingCallback(int nodeId, String msgOnSuccess,  boolean isAddNodeInToTheSameTree) {
        super(nodeId, msgOnSuccess, null);
        this.isAddNodeInToTheSameTree = isAddNodeInToTheSameTree;
    }

    @Override
    public void onSuccess(T result) {
        super.onSuccess(result);
        if (isAddNodeInToTheSameTree) {
            BIKClientSingletons.invalidateTabData(BIKClientSingletons.getTreeCodeById(BIKClientSingletons.getTreeIdOfCurrentTab()));
        }
        refreshNode();
    }
}
