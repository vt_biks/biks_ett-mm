/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.biadmin;

import com.crystaldecisions.sdk.exception.SDKException;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import com.crystaldecisions.sdk.properties.IProperties;
import java.util.Date;
import pl.bssg.metadatapump.common.SAPBOObjectBean;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class ObjectInfo {

    public static SAPBOObjectBean getInfoForObject(IInfoObject iInfoObject, IInfoStore iStore) {
        IProperties properties = iInfoObject.properties();

        SAPBOObjectBean bean = new SAPBOObjectBean();
        bean.id = getId(properties);
        bean.parentId = getParentId(properties);
        bean.name = getName(iInfoObject);
        bean.type = getKind(iInfoObject);
        bean.cuid = getCUID(iInfoObject);
        bean.guid = getGUID(iInfoObject);
        bean.ruid = getRUID(iInfoObject);
        bean.description = getDescription(iInfoObject);
        bean.created = getCreationTime(properties);
        bean.modify = getModifyTime(properties);
        bean.owner = getOwner(properties);
        bean.children = getChildren(properties);
        return bean;
    }

    public static SAPBOObjectBean getBaseInfoForObject(IInfoObject iInfoObject, IInfoStore iStore) {
        IProperties properties = iInfoObject.properties();
        SAPBOObjectBean bean = new SAPBOObjectBean();
        bean.id = getId(properties);
        bean.parentId = getParentId(properties);
        bean.name = getName(iInfoObject);
        bean.type = getKind(iInfoObject);
        bean.cuid = getCUID(iInfoObject);
        bean.modify = getModifyTime(properties);
        return bean;
    }

    private static Integer getId(IProperties properties) {
        return (Integer) properties.getProperty("SI_ID").getValue();
    }

    private static Integer getParentId(IProperties properties) {
        return (Integer) properties.getProperty("SI_PARENTID").getValue();
    }

    private static String getName(IInfoObject iInfoObject) {
        return iInfoObject.getTitle();
    }

    private static String getDescription(IInfoObject iInfoObject) {
        return iInfoObject.getDescription();
    }

    private static Date getCreationTime(IProperties properties) {
        return properties.getDate("SI_CREATION_TIME");
    }

    private static Date getModifyTime(IProperties properties) {
        return properties.getDate("SI_UPDATE_TS");
    }

    private static String getOwner(IProperties properties) {
        return BaseUtils.safeToString(properties.getProperty("SI_OWNER"));
    }

    private static int getChildren(IProperties properties) {
        return properties.getInt("SI_CHILDREN");
    }

    private static String getCUID(IInfoObject iInfoObject) {
        try {
            return iInfoObject.getCUID();
        } catch (SDKException ex) {
            return null;
        }
    }

    private static String getGUID(IInfoObject iInfoObject) {
        try {
            return iInfoObject.getGUID();
        } catch (SDKException ex) {
            return null;
        }
    }

    private static String getRUID(IInfoObject iInfoObject) {
        try {
            return iInfoObject.getRUID();
        } catch (SDKException ex) {
            return null;
        }
    }

    private static String getKind(IInfoObject iInfoObject) {
        try {
            return iInfoObject.getKind();
        } catch (SDKException ex) {
            return null;
        }
    }
}
