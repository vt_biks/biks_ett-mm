/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.UserBean;
import pl.fovis.common.UserExtradataBean;

/**
 *
 * @author tflorczak
 */
public class UserExtradataWidget extends EntityDetailsWidgetFlatBase {

    protected VerticalPanel mainVp;
    protected IMainHeaderSharedTable imhst;

    public UserExtradataWidget(IMainHeaderSharedTable imhst) {
        this.imhst = imhst;
    }

    @Override
    public void displayData() {

        UserExtradataBean userExtradata = getData().userExtradata;
        UserBean user = getData().user;

        String avatarUrl = null;
        if (userExtradata != null && userExtradata.avatar != null) {
            avatarUrl = userExtradata.avatar;
        }
        if (user != null && user.avatar != null) {
            avatarUrl = user.avatar;
        }
        if (avatarUrl != null) {
            imhst.addTranslatedRow("Zdjęcie" /* I18N: no */, null, BIKClientSingletons.createAvatarImage(avatarUrl, 200, 200, null, "MyBIKAvatarImg", null));
        }
        if (user != null) {
            imhst.addTranslatedRow("E-mail" /* i18n: no */, user.email);
            imhst.addTranslatedRow("Telefon" /* i18n: no */, user.phoneNum);
        }
        if (userExtradata != null) {
            // i18n-default: no
            // Kolejność wg PB ala pżemek
            imhst.addTranslatedRow("Imię" /* I18N:  */, userExtradata.givenName);
            imhst.addTranslatedRow("Nazwisko" /* I18N:  */, userExtradata.sn);
            imhst.addTranslatedRow("Stanowisko" /* i18n:  */, userExtradata.title);
            imhst.addTranslatedRow("Pion" /* i18n:  */, userExtradata.department);
            imhst.addTranslatedRow("Firma" /* I18N:  */, userExtradata.physicalDeliveryOfficeName);
            imhst.addTranslatedRow("ad_company" /* I18N:  */, userExtradata.company);
            imhst.addTranslatedRow("Numer telefonu" /* i18n:  */, userExtradata.telephoneNumber);
            imhst.addTranslatedRow("Telefon komórkowy" /* i18n:  */, userExtradata.mobile);
            imhst.addTranslatedRow("ad_mail" /* I18N:  */, userExtradata.mail);
            imhst.addTranslatedRow("Ulica" /* I18N:  */, userExtradata.streetAddress);
            imhst.addTranslatedRow("Kod pocztowy" /* i18n:  */, userExtradata.postalCode);
            imhst.addTranslatedRow("Miasto" /* I18N:  */, userExtradata.l);
            // reszta
            imhst.addTranslatedRow("Account Name" /* I18N: no */, userExtradata.sAMAccountName);
            imhst.addTranslatedRow("Inicjały" /* I18N:  */, userExtradata.initials);
            imhst.addTranslatedRow("Wyświetlana nazwa" /* I18N:  */, userExtradata.displayName);
            imhst.addTranslatedRow("Opis" /* I18N:  */, userExtradata.description);
            imhst.addTranslatedRow("Distinguished Name" /* I18N: no */, userExtradata.distinguishedName);
            imhst.addTranslatedRow("Nazwa logowania" /* I18N:  */, userExtradata.userPrincipalName);
            imhst.addTranslatedRow("Manager" /* i18n:  */, getNameFromAdString(userExtradata.manager));
            imhst.addTranslatedRow("Podwładny" /* i18n:  */, getNameFromAdString(userExtradata.directReports));
            imhst.addTranslatedRow("Zarządza" /* i18n:  */, userExtradata.managedObjects);
            imhst.addTranslatedRow("Strona domowa" /* I18N:  */, userExtradata.wWWHomePage);
            imhst.addTranslatedRow("Strona wwww" /* I18N:  */, userExtradata.url);
            imhst.addTranslatedRow("Telefon domowy" /* i18n:  */, userExtradata.homePhone);
            imhst.addTranslatedRow("Telefon fax" /* i18n:  */, userExtradata.facsimileTelephoneNumber);
            imhst.addTranslatedRow("Telefon pager" /* i18n:  */, userExtradata.pager);
            imhst.addTranslatedRow("Inny telefon" /* i18n:  */, userExtradata.otherTelephone);
            imhst.addTranslatedRow("Inny pager" /* i18n:  */, userExtradata.otherPager);
            imhst.addTranslatedRow("Inny komórkowy" /* i18n:  */, userExtradata.otherMobile);
            imhst.addTranslatedRow("Inny telefon IP" /* i18n:  */, userExtradata.otherIpPhone);
            imhst.addTranslatedRow("Inny fax" /* i18n:  */, userExtradata.otherFacsimileTelephoneNumber);
            imhst.addTranslatedRow("Telefon IP" /* i18n:  */, userExtradata.ipPhone);
            imhst.addTranslatedRow("Skrytka pocztowa" /* i18n:  */, userExtradata.postOfficeBox);
            imhst.addTranslatedRow("Województwo" /* I18N:  */, userExtradata.st);
            imhst.addTranslatedRow("Kraj/region" /* i18n:  */, userExtradata.co);
            imhst.addTranslatedRow("Kraj ISO" /* i18n:  */, userExtradata.c);
            imhst.addTranslatedRow("Uwagi" /* i18n:  */, userExtradata.notes);
            // i18n-default:
        }
    }

    public Widget buildWidgets() {
        mainVp = new VerticalPanel();
        return mainVp;
    }

    @Override
    public boolean hasContent() {
        return (getData().userExtradata != null) || (getData().user != null);
    }

    protected String getNameFromAdString(String adString) {
        if (adString != null) {
            int startIndex = adString.indexOf("CN" /* I18N: no */ + "=");
            int endIndex = adString.indexOf(",", startIndex + 3);
            if (startIndex != -1 && endIndex != -1) {
                return new String(adString.substring(startIndex + 3, endIndex));
            }
        }
        return adString;
    }
}
