/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import simplelib.BaseUtils;
import simplelib.IContinuationWithReturn;
import simplelib.logging.ILameLoggingEnvironment;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class ThreadedLameLoggingEnvironment implements ILameLoggingEnvironment {

    private static final String RUN_WITHOUT_LOGGING_VARNAME = "RUN_WITHOUT_LOGGING_VARNAME";
    private static boolean logEnvSet = false;

    synchronized public static void initLameLoggingEnvironment() {
        if (logEnvSet) {
            return;
        }
        logEnvSet = true;
        LameLoggerFactory.setLoggingEnvironment(new ThreadedLameLoggingEnvironment());
    }

    static {
        initLameLoggingEnvironment();
    }

    public boolean getRunWithoutLoggingMode() {
        if (ThreadCustomVars.isInterrupted()) {
            return true;
        }
        return BaseUtils.nullToDef((Boolean) ThreadCustomVars.getVar(RUN_WITHOUT_LOGGING_VARNAME), false);
    }

    public <T> T runWithoutLogging(boolean noLogging, IContinuationWithReturn<T> cont) {
        Object oldVal = ThreadCustomVars.setVar(RUN_WITHOUT_LOGGING_VARNAME, noLogging);
        try {
            return cont.doIt();
        } finally {
            if (!ThreadCustomVars.isInterrupted()) {
                ThreadCustomVars.setVar(RUN_WITHOUT_LOGGING_VARNAME, oldVal);
            }
        }
    }

    public String getCurrentRuningContextInfo() {
        return Thread.currentThread().getName();
    }
}
