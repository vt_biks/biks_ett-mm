/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author pmielanczuk
 * @param <S>
 * @param <W>
 */
public interface IWrapperCreator<S, W> {

    public W create(S src);
}
