/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author wezyr
 */
public class ConstantResourceFromProvider<T> implements IResourceFromProvider<T> {

    private T val;

    public ConstantResourceFromProvider(T val) {
        this.val = val;

    }

    public T gainResource() {
        return val;
    }

    public long getLastModifiedOfResource() {
        return 0;
    }
}
