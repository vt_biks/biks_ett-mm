/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import java.util.Map;
import pl.fovis.client.bikwidgets.BikIcon;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class GenericNodeObjectsWidget extends EntityDetailsWidgetFlatBase {

    protected final static String ICON_NAME = "icon";
    protected VerticalPanel mainVp;

    @Override
    public void displayData() {
        mainVp.clear();
        List<Map<String, String>> objects = getData().genericNodeObjects;
        if (!BaseUtils.isCollectionEmpty(objects)) {
            FlexTable table = new FlexTable();
            boolean isHeaderCreated = false;
            int row = 1;
            for (Map<String, String> object : objects) {
                int col = 0;
                if (!isHeaderCreated) {
                    createHeader(table, object);
                    isHeaderCreated = true;
                }
                for (Map.Entry<String, String> entrySet : object.entrySet()) {
                    String key = entrySet.getKey();
                    String value = entrySet.getValue();
                    if (ICON_NAME.equals(key)) {
                        table.setWidget(row, col, BikIcon.createIcon("images/" + value + ".gif" /* I18N: no */, "RelatedObj-ObjIcon"));
                    } else {
                        table.setWidget(row, col, new Label(BaseUtils.tryParseDouble(value) != null ? BaseUtils.chunkStringWithSepSpace(value) : value));
                    }
                    col++;
                }
                table.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
                row++;
            }
            table.setWidth("100%");
            mainVp.add(table);
        }
    }

    @Override
    public Widget buildWidgets() {
        mainVp = new VerticalPanel();
        mainVp.setWidth("100%");
        return mainVp;
    }

    @Override
    public boolean hasContent() {
        return getData().genericNodeObjects != null && !getData().genericNodeObjects.isEmpty();
    }

    protected void createHeader(FlexTable table, Map<String, String> object) {
        int col = 0;
        for (Map.Entry<String, String> entrySet : object.entrySet()) {
            String key = entrySet.getKey();
            if (ICON_NAME.equals(key)) {
                table.setWidget(0, col, new HTML(""));
                table.getColumnFormatter().setWidth(col, "2%");
            } else {
                table.setWidget(0, col, new HTML("<b>" + key + "</b>"));
            }
            col++;
        }
    }
}
