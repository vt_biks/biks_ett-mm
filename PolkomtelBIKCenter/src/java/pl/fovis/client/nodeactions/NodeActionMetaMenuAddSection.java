/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.OneTextBoxDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author ctran
 */
public class NodeActionMetaMenuAddSection extends NodeActionBase {

    @Override
    public boolean isEnabledByDesign() {
        return isNodeOneOfTheKind(false, BIKConstants.NODE_KIND_META_MENU_CONF_MAIN_MENU,
                BIKConstants.NODE_KIND_META_MENU_GROUP);
    }

    @Override
    public String getCaption() {
        return I18n.dodajZakladke.get();
    }

    @Override
    public void execute() {
        OneTextBoxDialog dialog = new OneTextBoxDialog();
        dialog.buildAndShowDialog(getCaption(), null, new IParametrizedContinuation<String>() {

            @Override
            public void doIt(String param) {
                if (!BaseUtils.isStrEmptyOrWhiteSpace(param)) {
                    BIKClientSingletons.getService().addSubmenu(node.id, param, new StandardAsyncCallback<Void>() {

                        @Override
                        public void onSuccess(Void result) {
                            BIKClientSingletons.showInfo(I18n.proszeOdswiezycPrzegladarke.get());
                        }
                    });
                } else {
                    BIKClientSingletons.showWarning(I18n.bladWeryfikacji.get());
                }
            }
        });
    }
}
