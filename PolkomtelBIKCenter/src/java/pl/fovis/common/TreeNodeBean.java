/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.List;
import java.util.Set;

/**
 *
 * @author mgraczkowski
 */
public class TreeNodeBean implements IsSerializable {

    public int id;
    public Integer parentNodeId;
    public int nodeKindId;
    public String descr;
    public String descrPlain;
    public String nodeKindCaption;
    public String nodeKindCode;
    public String nodeKindChildrenKinds;
    public String nodeKindChildrenAttrs;
    public String nodeKindUploadableChildrenKinds;
    public boolean isFolder;
    public String name;
    public String objId;
    public String parentObjId;
    public int treeId;
    public Integer linkedNodeId;
    public String branchIds;
    public String treeCode;
    public String treeName;
    public String treeKind;
    public String parentTreeName;
    public Integer parentTreeId;
    public boolean isBuiltIn;
    public boolean isFromLinkedSubtree;
    public int visualOrder;
    public String nodePath;
    public String iconName;
    public List<AttributeBean> requiredAttrs;
    public boolean isFixed;
    //---
    public boolean hasNoChildren;
    //---
    public boolean hasProperGrantForNodeFilteringAction;//= true;
    //---
    public boolean isNodeSelected;
    public Set<Integer> addingJoinedObjs;
    public boolean isRegistry;
    public String attributes;
//    public String defaultTemplate;
    public Integer templateNodeId;
    public Integer maxPrintLevel;

    public TreeNodeBean() {
        // pusty
    }

    public TreeNodeBean(Integer id, Integer parentNodeId, Integer nodeKindId, String name, String objId, Integer treeId, Integer linkedNodeId) {
        this.id = id;
        this.parentNodeId = parentNodeId;
        this.nodeKindId = nodeKindId;
        this.name = name;
        this.objId = objId;
        this.treeId = treeId;
        this.linkedNodeId = linkedNodeId;
    }

    public TreeNodeBean(int id, Integer parentNodeId, int nodeKindId, String descr, String nodeKindCaption, String nodeKindCode, String name, String objId, int treeId, Integer linkedNodeId, String branchIds, String treeCode, String treeName, String treeKind) {
        this.id = id;
        this.parentNodeId = parentNodeId;
        this.nodeKindId = nodeKindId;
        this.descr = descr;
        this.nodeKindCaption = nodeKindCaption;
        this.nodeKindCode = nodeKindCode;
        this.name = name;
        this.objId = objId;
        this.treeId = treeId;
        this.linkedNodeId = linkedNodeId;
        this.branchIds = branchIds;
        this.treeCode = treeCode;
        this.treeName = treeName;
        this.treeKind = treeKind;
    }

    @Override
    public String toString() {
        return "TreeNodeBean{" + "id=" + id + ", parentNodeId=" + parentNodeId + ", nodeKindId=" + nodeKindId + ", descr=" + descr + ", nodeKindCaption=" + nodeKindCaption + ", nodeKindCode=" + nodeKindCode + ", nodeKindChildrenKinds=" + nodeKindChildrenKinds + ", nodeKindChildrenAttrs=" + nodeKindChildrenAttrs + ", isFolder=" + isFolder + ", name=" + name + ", objId=" + objId + ", treeId=" + treeId + ", linkedNodeId=" + linkedNodeId + ", branchIds=" + branchIds + ", treeCode=" + treeCode + ", treeName=" + treeName + ", treeKind=" + treeKind + ", parentTreeName=" + parentTreeName + ", parentTreeId=" + parentTreeId + ", isBuiltIn=" + isBuiltIn + ", isFromLinkedSubtree=" + isFromLinkedSubtree + ", visualOrder=" + visualOrder + ", hasNoChildren=" + hasNoChildren + ", hasProperGrantForNodeFilteringAction=" + hasProperGrantForNodeFilteringAction + ", isNodeSelected=" + isNodeSelected + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
