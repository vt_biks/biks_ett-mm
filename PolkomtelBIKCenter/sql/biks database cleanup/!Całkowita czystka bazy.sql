﻿--------------------------------------------------------
--------------------------------------------------------
-- PONIŻSZY SKRYPT CZYŚCI BAZE DANYCH DO ZERA, 
-- DOPROWADZAJĄC JĄ DO PIERWOTNEGO STANU
-- ZOSTAWIA TYLKO PUSTE DRZEWKA: GLOSARIUSZ, 
-- DOKUMENTACJA, BLOGI ORAZ UŻYTKOWNICY
-- WSZYSTKIE KONEKTORY SĄ WYŁĄCZONE
--
-- DOSTĘPNY JEST JEDEN UZYTKOWNIK Z HASŁEM:
-- Admin // 123
--
-- SKRYPT MOŻE WYKONYWAĆ SIĘ DŁUGO!
--------------------------------------------------------
--------------------------------------------------------


-----------------------------------------------------
-----------------------------------------------------
-- ukrywanie drzewek
-----------------------------------------------------
-----------------------------------------------------
print '-----> ukrywanie drzewek';
update bik_tree set is_hidden = 1
update bik_tree set is_hidden = 0 where code in ('TreeOfTrees')


-----------------------------------------------------
-----------------------------------------------------
-- czyszczenie tablic - do zera
-----------------------------------------------------
-----------------------------------------------------

print '-----> czyszczenie tablic - do zera, part1';
truncate table bik_service_request
truncate table bik_data_load_log_details
delete from bik_teradata 
delete from bik_data_load_log
delete from bik_obj_in_body_node
delete from bik_attachment
delete from bik_blog_linked
delete from bik_blog
delete from bik_joined_objs
delete from bik_keyword
delete from bik_metapedia_keyword
delete from bik_metapedia_linked
delete from bik_metapedia
delete from bik_news_readed_user
delete from bik_news
delete from bik_note
delete from bik_tutorial_readed_user
delete from bik_metapedia_linked
delete from bik_mssql
delete from bik_teradata_index
delete from bik_sapbo_object_table
delete from bik_sapbo_extradata
delete from bik_sapbo_universe_table
delete from bik_sapbo_query
delete from bik_lisa_dict_column
delete from bik_monitor
delete from bik_sapbo_universe_connection
delete from bik_keyword
delete from bik_dqc_test_parameters
delete from bik_dqc_request
delete from bik_dqc_group_test
delete from bik_dqc_group
delete from bik_dqc_test
delete from bik_node_vote
delete from bik_sapbo_universe_column
go
print '-----> czyszczenie tablic - do zera, part2';
delete from bik_attribute_linked
delete from bik_attribute
delete from bik_attr_def where is_built_in = 0
delete from bik_attr_category where is_built_in = 0
delete from bik_searchable_attr where name not in ('name', 'descr')
truncate table bik_searchable_attr_val
delete from bik_object_in_history
delete from bik_object_in_fvs_change
delete from bik_object_in_fvs
delete from bik_statistic
delete from bik_statistic_ext
delete from bik_user_right
delete from bik_authors
delete from bik_lisa_logs
delete from bik_node_author
delete from bik_system_user
delete from bik_object_author
delete from bik_user_in_node
delete from bik_user
go

-----------------------------------------------------
-----------------------------------------------------
-- czyszczenie tabelek ami
-----------------------------------------------------
-----------------------------------------------------
print '-----> czyszczenie tabelek ami';
truncate table ami_app_universe
truncate table ami_universe
truncate table ami_universe_class
truncate table ami_universe_column
truncate table ami_universe_connection
truncate table ami_universe_connection_networklayer
truncate table ami_universe_delta
truncate table ami_universe_filter
truncate table ami_universe_obj
truncate table ami_universe_obj_tables
truncate table ami_universe_table
truncate table ami_query
truncate table ami_report_object
go

-----------------------------------------------------
-----------------------------------------------------
-- czystka userów i założenie admina: Admin/123
-----------------------------------------------------
-----------------------------------------------------
print '-----> czystka userów i założenie admina';
insert into bik_system_user(login_name, is_disabled, password, name)
values('Admin', 0, '123', 'Administrator')

declare @adminId int;
select @adminId = scope_identity();

insert into bik_user_right(user_id, right_id)
select @adminId, id from bik_right_role where code in ('Administrator', 'Expert', 'AppAdmin')

update bik_app_prop set val = '' where name in ('availableConnectors', 'heartbeatMonitorUrl', 'load_user_login', 'load_user_pwd')
update bik_app_prop set val = '0' where name = 'isDemoVersion'
update bik_admin set value = '' where param like '%.url' or param like '%.pass%' or param like '%.user%' or param like 'lisateradata.%'
update bik_schedule set is_schedule = 0
-- wylaczenie NAS
declare @NASID varchar(50) = (select branch_ids from bik_node where obj_id = 'admin:lisa')
update bik_node set is_deleted = 1 where branch_ids like '' + @NASID + '%'



----- SKRYPT 1. cleanup any biks db.sql
print '-----> SKRYPT 1. cleanup any biks db.sql';
-----------------------------------------------------------------
-----------------------------------------------------------------
-- śmieci z PGE
-----------------------------------------------------------------
-----------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_przygotujDane_Raport3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_przygotujDane_Raport3]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FinalizujDaneRaportow]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FinalizujDaneRaportow]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_dane_raport1]'))
DROP VIEW [dbo].[vw_dane_raport1]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_dane_raport2]'))
DROP VIEW [dbo].[vw_dane_raport2]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_ozn_kontr_agr]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_ozn_kontr_agr]
GO

set nocount on

if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#table_to_trunc'))
drop table #table_to_trunc
go

create table #table_to_trunc (name sysname not null primary key, lvl int not null default 0)

insert into #table_to_trunc (name) 
select t.name
from sys.objects t
  left join sys.extended_properties as ep on ep.class = 1 and
  ep.major_id = t.object_id and ep.minor_id = 0 and ep.name = 'microsoft_database_tools_support'
where t.is_ms_shipped = 0 and ep.major_id is null
  and t.type = 'U'
  and not (t.name like 'bik[_]%' or t.name like 'ami[_]%' or t.name like 'mltx[_]%')

if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#table_rel'))
drop table #table_rel
go

create table #table_rel (referee_name sysname not null, referenced_name sysname not null,
  unique (referee_name, referenced_name),
  unique (referenced_name, referee_name))

insert into #table_rel (referee_name, referenced_name)
select distinct ree.name, red.name
from vw_ww_foreignkeys fk inner join #table_to_trunc ree on fk.Table_Name = ree.name collate DATABASE_DEFAULT
  inner join #table_to_trunc red on fk.referencedtablename = red.name collate DATABASE_DEFAULT
where ree.name <> red.name
  
declare @pass_num int = 0

while exists(select * from #table_to_trunc)
begin

  update #table_to_trunc
  set lvl = 1
  where name in (select referenced_name from #table_rel)

  declare @sql_txt varchar(max) = ''

  declare @single_cmd varchar(max) = case when @pass_num = 0 then 'truncate table' else 'delete from' end

  select @sql_txt = @sql_txt + @single_cmd + ' ' + name + '
'
  --t.* 
  from #table_to_trunc ttt
  where lvl = 0

  print '
-----------
pass #' + cast(@pass_num as varchar(10)) + ', executing commands:
' + @sql_txt
  --select len(@sql_txt) as length, @sql_txt

  exec(@sql_txt)

  delete from #table_rel where referee_name in (select name from #table_to_trunc where lvl = 0)

  delete from #table_to_trunc
  where lvl = 0

  update #table_to_trunc
  set lvl = 0

  set @pass_num = @pass_num + 1

  -- select * from #table_to_trunc
  -- select * from #table_rel
end
go



----- SKRYPT 2. delete invisible bik_nodes
print '-----> SKRYPT 2. delete invisible bik_nodes';

set nocount on


if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#has_new_to_del'))
drop table #has_new_to_del
go

create table #has_new_to_del (table_name sysname not null primary key)

insert into #has_new_to_del (table_name) values ('bik_node')

if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##ww_td_bik_node'))
drop table ##ww_td_bik_node
go

create table ##ww_td_bik_node (id int not null primary key)
insert into ##ww_td_bik_node (id) select id from bik_node where is_deleted = 1 and is_built_in = 0

insert into ##ww_td_bik_node (id) select id from bik_node
where is_deleted = 0 and tree_id in (select id from bik_tree where is_hidden = 1) and is_built_in = 0


if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#table_to_del_rows'))
drop table #table_to_del_rows
go

create table #table_to_del_rows (table_name sysname not null primary key)

declare @drop_tmp_txt varchar(max) = ''

select @drop_tmp_txt = @drop_tmp_txt + 'drop table ' + name + '
'
-- object_id('tempdb..' + name), * 
from tempdb.sys.objects
where object_id('tempdb..' + name) is not null and name like '##ww[_]td[_]%'
and object_id('tempdb..' + name) not in (
  select tmp_tab_id from (select object_id('tempdb..##ww_td_' + table_name) as tmp_tab_id from #has_new_to_del) x
  where tmp_tab_id is not null)

--select @drop_tmp_txt
exec(@drop_tmp_txt)

declare @step int = 0

while exists(select 1 from #has_new_to_del)
begin
  declare @table_name sysname = (select top 1 table_name from #has_new_to_del)
  
  print '-- step #' + cast(@step as varchar(10)) + ', table name: ' + @table_name
  
  if not exists(select 1 from #table_to_del_rows where table_name = @table_name)
  insert into #table_to_del_rows (table_name) values (@table_name)
  
  delete from #has_new_to_del where table_name = @table_name
  
  --set @lvl = @lvl + 1

  declare @sql_txt varchar(max) = ''

  select 
  @sql_txt = @sql_txt + '
' +
  'if not exists(select 1 from tempdb.sys.objects where object_id = object_id(''tempdb..##ww_td_' + Table_Name + ''')) exec(''create table ' + tmp_tab_name + ' (' + pk_typed + ', primary key (' + pk_untyped + '))'')
' +  
    'insert into ' + tmp_tab_name + ' (' + pk_untyped +     
    ') select ' + 'ree.' + replace(pk_untyped, ',', ',ree.') +
    ' from ' + table_name + ' ree inner join ##ww_td_' + ReferencedTableName + ' red on ' + join_cond + ' left join ' + tmp_tab_name + ' tt on ' +
    dbo.fn_ww_self_join_cond(pk_untyped, 'ree', 'tt') + ' where ' + replace('tt.' + pk_untyped, ',', ' is null and tt.') + ' is null
' + 
'if @@rowcount <> 0 and not exists(select 1 from #has_new_to_del where table_name = ''' + table_name + ''') insert into #has_new_to_del (table_name) values (''' + table_name + ''')'
  from (
  select
  table_name, dbo.fn_ww_table_key_cols(table_name, 1, 2) as pk_typed,
  dbo.fn_ww_table_key_cols(table_name, 0, 2) as pk_untyped,
  dbo.fn_ww_fk_join_cond(ForeignKey_Name, 'ree', 'red') as join_cond,
  case when exists(select 1 from tempdb.sys.objects where object_id = object_id('tempdb..##ww_td_' + Table_Name)) then 1 else 0 end
    as has_tmp_tab,
  '##ww_td_' + Table_Name as tmp_tab_name, ReferencedTableName
  from (
  select distinct foreignkey_name, Table_Name, ReferencedTableName
  from vw_ww_foreignkeys
  where ReferencedTableName = --'bik_node' --
    @table_name
  ) x
  ) y  
  
  --print @sql_txt
  
  exec(@sql_txt)
  
  set @step = @step + 1
end

declare @nocheck_constraints varchar(max) = ''
declare @check_constraints varchar(max) = ''
declare @delete_rows varchar(max) = ''

select @nocheck_constraints = @nocheck_constraints +
'alter table ' + table_name + ' nocheck constraint all
',
@check_constraints = @check_constraints +
'alter table ' + table_name + ' check constraint all
',
@delete_rows = @delete_rows +
'delete from ' + table_name + ' from ##ww_td_' + table_name + ' as td where ' +
   dbo.fn_ww_self_join_cond(dbo.fn_ww_table_key_cols(table_name, 0, 2), table_name, 'td') + '
'
from #table_to_del_rows

print '
-- nocheck constraints'
--print @nocheck_constraints
exec(@nocheck_constraints)

print '
-- delete rows'
--print @delete_rows
exec(@delete_rows)

print '
-- check constraints'
--print @check_constraints
exec(@check_constraints)



-----------------------------------------------------
-----------------------------------------------------
-- update danych pod wyszukiwanie
-----------------------------------------------------
-----------------------------------------------------
print '-----> update danych pod wyszukiwanie';
update bik_searchable_attr set search_weight = 100, caption = 'Opis/Treść' where name = 'descr'
update bik_searchable_attr set search_weight = 10000, caption = 'Nazwa' where name = 'name'
go

exec dbo.sp_verticalize_node_attrs null
go


-----------------------------------------------------
-----------------------------------------------------
-- przycinanie logów i plików bazy
-----------------------------------------------------
-----------------------------------------------------
print '-----> przycinanie logów i plików bazy';
declare @log_name varchar(max);
select @log_name=name from sys.database_files where type = 1
exec('DBCC SHRINKFILE (N''' + @log_name + '''  , 0)')
go

declare @cmd_txt varchar(max) = 'DBCC SHRINKDATABASE (''' + db_name() + ''' ,0, NOTRUNCATE)'
exec(@cmd_txt)
go

declare @cmd_txt varchar(max) = 'DBCC SHRINKDATABASE (''' + db_name() + ''' ,0, TRUNCATEONLY)'
exec(@cmd_txt)
go

declare @cmd_txt varchar(max) = 'DBCC SHRINKDATABASE (''' + db_name() + ''' ,0)'
exec(@cmd_txt)
go

-----------------------------------------------------
-----------------------------------------------------
-- przywrócenie widoczności drzewek
-----------------------------------------------------
-----------------------------------------------------
print '-----> przywrócenie widoczności drzewek';
update bik_tree set is_hidden = 0 where code in ('Glossary', 'Documents', 'Blogs', 'TreeOfTrees', 'UserRoles')

print '-----> KONIEC';