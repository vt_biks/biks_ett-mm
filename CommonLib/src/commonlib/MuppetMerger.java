/*
 * MuppetMerger.java
 *
 * Created on 5 luty 2007, 14:58
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package commonlib;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.PropNotInTable;
import simplelib.PropsNotInSuperClass;
import simplelib.StackTraceUtil;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class MuppetMerger {

    static ILameLogger logger = LameLoggerFactory.getLogger(MuppetMerger.class);
    //LameLogger.getClassLogger();

    /*
     static public boolean isPropGetterMethod(Method met) {
     if (met.isAnnotationPresent(UnmergableGetterSetter.class))
     return false;
     String propName = LameUtils.getFieldForGetter(met.getName());
     boolean nameEquals = propName != null;
     boolean paramOk = nameEquals && met.getParameterTypes().length == 0;
     boolean returnOk = paramOk && met.getReturnType() != Void.TYPE;
     return returnOk;
     }
     */
    static public String getPropNameForGetterMethod(Method met) {
        return getPropNameForGetterMethod(met, null);
    }

    protected static boolean isAccessibleObjectExcluded(AccessibleObject ao, Class<? extends Annotation> optAnnotationToIgnore) {
        return (optAnnotationToIgnore != null && ao.isAnnotationPresent(optAnnotationToIgnore))
                || ao.isAnnotationPresent(UnmergableGetterSetter.class);
    }

    static public String getPropNameForGetterMethod(Method met, Class<? extends Annotation> optAnnotationToIgnore) {
        if (isAccessibleObjectExcluded(met, optAnnotationToIgnore)) {
            return null;
        }
        boolean paramOk = met.getParameterTypes().length == 0;
        boolean returnOk = paramOk && met.getReturnType() != Void.TYPE;
        return returnOk ? LameUtils.getFieldForGetter(met.getName()) : null;
    }

    static public String getPropNameForSetterMethod(Method met) {
        return getPropNameForSetterMethod(met, null);
    }

    static public String getPropNameForSetterMethod(Method met, Class<? extends Annotation> optAnnotationToIgnore) {
        if (isAccessibleObjectExcluded(met, optAnnotationToIgnore)) {
            return null;
        }
        boolean paramOk = met.getParameterTypes().length == 1;
        boolean returnOk = paramOk && met.getReturnType() == Void.TYPE;
        return returnOk ? LameUtils.getFieldForSetter(met.getName()) : null;
    }

    static public Set<String> getMuppetProps(Object muppet, int requiredOps) {
        return getMuppetProps(muppet, requiredOps, null);
    }

    static public Set<String> getMuppetProps(Object muppet, int requiredOps, Class<? extends Annotation> optAnnotationToIgnore) {
        return getMuppetProps(muppet.getClass(), requiredOps, optAnnotationToIgnore);
    }
    // requiredOps:
    //   0 - any
    //   1 - readable
    //   2 - writable
    //   3 - both

    public static Set<String> getMuppetProps(Class clazz, int requiredOps) {
        return getMuppetProps(clazz, requiredOps, null);
    }

    public static Set<String> getMuppetProps(Class<?> clazz, int requiredOps, Class<? extends Annotation> optAnnotationToIgnore) {
        Set<String> res = new LinkedHashSet<String>();

        boolean disableSuperClassProps = optAnnotationToIgnore != null && clazz.isAnnotationPresent(PropsNotInSuperClass.class);

        for (Field fld : disableSuperClassProps ? clazz.getDeclaredFields() : clazz.getFields()) {
            int modifiers = fld.getModifiers();
            if ((modifiers & Modifier.PUBLIC) != 0
                    && (modifiers & (Modifier.STATIC + Modifier.FINAL)) == 0
                    && !isAccessibleObjectExcluded(fld, optAnnotationToIgnore)) {
                res.add(fld.getName());
            }
        }

        Set<String> readablePropNames = null;
        Set<String> writablePropNames = null;

        if ((requiredOps & 1) == 1 || requiredOps == 0) {
            readablePropNames = new HashSet<String>();
        }
        if ((requiredOps & 2) == 2 || requiredOps == 0) {
            writablePropNames = new HashSet<String>();
        }

        for (Method met : disableSuperClassProps ? clazz.getDeclaredMethods() : clazz.getMethods()) {
            if ((met.getModifiers() & Modifier.PUBLIC) == 0) {
                continue;
            }

            String propName; // = null;

            // czy musi by� odczytywalna
            if ((requiredOps & 1) == 1 || requiredOps == 0) {
                propName = getPropNameForGetterMethod(met, optAnnotationToIgnore);
                if (propName != null) {
                    readablePropNames.add(propName);
                }
            }

            // ma by� zapisywalna
            if ((requiredOps & 2) == 2 || requiredOps == 0) {
                propName = getPropNameForSetterMethod(met, optAnnotationToIgnore);
                if (propName != null) {
                    writablePropNames.add(propName);
                }
            }
        }

        if (requiredOps == 3) {
            readablePropNames.retainAll(writablePropNames);
            res.addAll(readablePropNames);
        } else {
            if ((requiredOps & 1) == 1 || requiredOps == 0) {
                res.addAll(readablePropNames);
            }
            if ((requiredOps & 2) == 2 || requiredOps == 0) {
                res.addAll(writablePropNames);
            }
        }

        return res;
    }

    static public Method findGetterSetter(Class c, String propName, boolean setterMode) {
        //String methodName = setterMode ? LameUtils.getSetter(propName) : LameUtils.getGetter(propName);

        for (Method met : c.getMethods()) {
            String methodPropName;
            if (setterMode) {
                methodPropName = getPropNameForSetterMethod(met);
            } else {
                methodPropName = getPropNameForGetterMethod(met);
            }

            if (methodPropName != null && methodPropName.equals(propName)) {
                return met;
            }
        }
        return null;
    }
    static Map<Pair<Class, String>, AccessibleObject> getterPropCache = new HashMap<Pair<Class, String>, AccessibleObject>();
    static Map<Pair<Class, String>, AccessibleObject> setterPropCache = new HashMap<Pair<Class, String>, AccessibleObject>();

//    private static int tempCnt_getPropObjViaCache = 0;
    static AccessibleObject getPropObjViaCache(Map<Pair<Class, String>, AccessibleObject> propCache, Class c, String propName, boolean isForSetter, boolean allowNoSuchProp) {
        Pair<Class, String> cacheKey = new Pair<Class, String>(c, propName);

        AccessibleObject propObj = propCache.get(cacheKey);
        boolean isInCache = propObj != null || propCache.containsKey(cacheKey);
        Exception exx = null;

        if (propObj == null && !isInCache) {
            try {
                propObj = findGetterSetter(c, propName, isForSetter);
                if (propObj == null) {
                    propObj = c.getField(propName);
                }
            } catch (Exception ex) {
                exx = ex;
//                throw new LameRuntimeException("Error getting propObj for property: " + propName + " for object of class: " + c.getName());
                //ww: no-op
            }
            propCache.put(cacheKey, propObj);
        }

//        tempCnt_getPropObjViaCache++;
//        if (tempCnt_getPropObjViaCache > 10000 && Math.random() > 0.999) {
//            propObj = null;
//            exx = new RuntimeException("aqq!");
//        }
        if (propObj == null && !allowNoSuchProp) {
            // wyplujemy co wiemy
            StringBuilder sb = new StringBuilder();

            sb.append("fields: {");

            boolean isFirst = true;

            for (Field f : c.getFields()) {
                if (isFirst) {
                    isFirst = false;
                } else {
                    sb.append(", ");
                }
                sb.append(f.getName());
            }

            sb.append("}, methods: {");

            isFirst = true;

            for (Method m : c.getMethods()) {
                if (isFirst) {
                    isFirst = false;
                } else {
                    sb.append(", ");
                }
                sb.append(m.getName());
            }

            sb.append("})");

            StringBuilder sb2 = new StringBuilder();

            sb2.append("isInCache: ").append(isInCache).append("\n");

            if (exx != null) {
                sb2.append("exception when trying to acquire propObj: ").append(StackTraceUtil.getCustomStackTrace(exx)).append("\n");
            }

            int propsInCacheCnt = 0;
            final String className = c.getName();

            sb2.append("props of class ").append(className).append(" (").append(System.identityHashCode(c)).append(") in cache:");

            for (Entry<Pair<Class, String>, AccessibleObject> e : propCache.entrySet()) {
                final Pair<Class, String> cacheClassAndPropNamePair = e.getKey();
                final Class cacheClass = cacheClassAndPropNamePair.v1;
                final String cacheClassName = cacheClass == null ? "null" : cacheClass.getName();
                if (BaseUtils.safeEquals(cacheClassName, className)) {
                    sb2.append("\n  ").append(cacheClassName).append(" (").append(System.identityHashCode(cacheClass)).append(").").append(cacheClassAndPropNamePair.v2);
                    sb2.append(": ").append(e.getValue() == null ? "null" : e.getValue().getClass().getName());
                    propsInCacheCnt++;
                }
            }

            if (propsInCacheCnt == 0) {
                sb2.append(" <none>");
            }

            throw new LameRuntimeException("no getter of property: " + propName + " for object of class: " + className
                    + ", " + sb2 + ",\nbut it has " + sb);
        }

        return propObj;
    }

    static AccessibleObject getPropObjViaCache(Map<Pair<Class, String>, AccessibleObject> propCache, Class c, String propName, boolean isForSetter) {
        return getPropObjViaCache(propCache, c, propName, isForSetter, false);
    }

    public static Type getPropertyGenericClass(Class c, String propName, boolean isForSetter) {
        AccessibleObject propObj = getPropObjViaCache(getterPropCache, c, propName, isForSetter);
        if (propObj instanceof Field) {
            Field fld = (Field) propObj;
            return fld.getGenericType();
        }
        if (propObj instanceof Method) {
            Method met = (Method) propObj;
            return met.getGenericParameterTypes()[0];
        }
        throw new LameRuntimeException("property \"" + propName + "\" of class "
                + c.getCanonicalName() + " has no prop object of proper class (found prop object class: "
                + LameUtils.safeGetClassName(propObj) + ")");
    }

    public static Class getPropertyClassEx(Class c, String propName, boolean isForSetter, boolean allowNoSuchProp) {
        AccessibleObject propObj = getPropObjViaCache(getterPropCache, c, propName, isForSetter, allowNoSuchProp);
        if (propObj instanceof Field) {
            Field fld = (Field) propObj;
            return fld.getType();
        }
        if (propObj instanceof Method) {
            Method met = (Method) propObj;
            return met.getParameterTypes()[0];
        }
        if (allowNoSuchProp) {
            return null;
        }
        throw new LameRuntimeException("property \"" + propName + "\" of class "
                + c.getCanonicalName() + " has no prop object of proper class (found prop object class: "
                + LameUtils.safeGetClassName(propObj) + ")");
    }

    public static Class getPropertyClass(Class c, String propName, boolean isForSetter) {
        return getPropertyClassEx(c, propName, isForSetter, false);
    }

    @SuppressWarnings("unchecked")
    public static boolean hasPropAssignableTo(Class beanClass, String propName, Class targetClass) {
        Class propClass = getPropertyClassEx(beanClass, propName, false, true);
        return propClass != null && targetClass.isAssignableFrom(propClass);
    }

    static public Object getProperty(Object obj, String propName) {
        Object propObj = getPropObjViaCache(getterPropCache, obj.getClass(), propName, false);

        try {
            if (propObj instanceof Field) {
                Field fld = (Field) propObj;
                return fld.get(obj);
            } else {
                Method met = (Method) propObj;
                return met.invoke(obj);
            }
        } catch (Exception ex) {
            throw new LameRuntimeException("Error getting value of property: " + propName + " via " + (propObj instanceof Method ? "method" : "field"), ex);
        }

//        Method getter = findGetterSetter(obj, propName, false);
//        try {
//            if (getter != null) {
//                return getter.invoke(obj);
//            } else {
//                return obj.getClass().getField(propName).get(obj);
//            }
//        } catch (Exception ex) {
//            throw new LameRuntimeException("Error getting value of property: " + propName + " via " + (getter != null ? "method" : "field"), ex);
//        }
    }

    @SuppressWarnings("unchecked")
    static public void setProperty(Object obj, String propName, Object propVal) {
        Object propObj = getPropObjViaCache(setterPropCache, obj.getClass(), propName, true);

        try {
            if (propObj instanceof Field) {
                Field fld = (Field) propObj;

                Class propType = fld.getType();

//                if (BaseUtils.safeEquals(BaseUtils.safeGetSimpleClassName(obj), "JoinedObjBean")
//                        && BaseUtils.safeEquals(propName, "dstId")) {
//                    System.out.println("setProperty: obj class=" + BaseUtils.safeGetSimpleClassName(obj) + ", propName=" + propName
//                            + ", propVal class=" + BaseUtils.safeGetClassName(propVal) + ", propType=" + propType.getName());
//                }
                if (propVal != null) {
                    if (propType.isAssignableFrom(int.class)
                            || propType.isAssignableFrom(Integer.class)) {
                        if (propVal instanceof Long) {
                            propVal = ((Long) propVal).intValue();
                        } else if (propVal instanceof Number) {
                            propVal = ((Number) propVal).intValue();
                        } else if (propVal instanceof String && propType.isAssignableFrom(int.class)) {
                            propVal = BaseUtils.tryParseInt((String) propVal);
                        } else if (propVal instanceof String && propType.isAssignableFrom(Integer.class)) {
                            propVal = BaseUtils.tryParseInteger((String) propVal);
                        }
                    } else if (propType.isAssignableFrom(long.class)
                            || propType.isAssignableFrom(Long.class)) {
                        if (propVal instanceof Number) {
                            propVal = ((Number) propVal).longValue();
                        } else if (propVal instanceof String && propType.isAssignableFrom(long.class)) {
                            propVal = BaseUtils.tryParseLong((String) propVal, 0L);
                        } else if (propVal instanceof String && propType.isAssignableFrom(Long.class)) {
                            propVal = BaseUtils.tryParseLong((String) propVal, null);
                        }
                    } else if (propType.isAssignableFrom(Double.class) || propType.isAssignableFrom(double.class)) {
                        if (propVal instanceof BigDecimal) {
                            propVal = ((BigDecimal) propVal).doubleValue();
                        }
                    } else if ((propType.isAssignableFrom(boolean.class)
                            || propType.isAssignableFrom(Boolean.class))
                            && !(propVal instanceof Boolean)) {
                        String propValStr = propVal.toString().trim().toLowerCase();
                        propVal = !("0".equals(propValStr) || "false".equals(propValStr));
                    } else if (propType.isEnum() && propVal instanceof String) {
                        propVal = Enum.valueOf(propType, (String) propVal);
                    }
                } else {
                    if (int.class.equals(propType)) {
                        propVal = 0;
                    } else if (double.class.equals(propType)) {
                        propVal = 0d;
                    } else if (long.class.equals(propType)) {
                        propVal = 0L;
                    }
                }

                fld.set(obj, propVal);
            } else {
                Method met = (Method) propObj;
                met.invoke(obj, propVal);
            }
        } catch (Exception ex) {
            throw new LameRuntimeException("Error setting value of property: " + propName
                    + " via " + (propObj instanceof Method ? "method" : "field")
                    + ", value class: " + LameUtils.safeGetClassName(propVal), ex);
        }

//        Method setter = findGetterSetter(obj, propName, true);
//        try {
//            if (setter != null) {
//                setter.invoke(obj, propVal);
//            } else {
//                obj.getClass().getField(propName).set(obj, propVal);
//            }
//        } catch (Exception ex) {
//            throw new LameRuntimeException("Error setting value of property: " + propName, ex);
//        }
    }

    /*
     static public void mergeLists(List mainList, List defList) {
     Map<String, Pair<Integer, Object>> mainMap = new HashMap<String, Pair<Integer, Object>>();
     for (int i = 0; i < mainList.size(); i++) {
     Object o = mainList.get(i);
     mainMap.put((String)getProperty(o, "name"), new Pair<Integer, Object>(i, o));
     }
     for (Object o : defList) {
     String name = (String)getProperty(o, "name");
     Pair<Integer, Object> pair = mainMap.get(name);
     Object valToSet;
     if (pair != null) {
     if (pair.v2 != null)
     valToSet = mergeMuppets(pair.v2, o, false);
     else
     valToSet = o;
     if (valToSet != null)
     mainList.set(pair.v1, valToSet);
     } else if (o != null)
     mainList.add(o);
     }
     }
     */
    static public <T> void checkArrayForNullItem(T[] arr, String msg, Object... args) {
        if (arr == null) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == null) {
                throw new LameRuntimeException(String.format(msg, args) + " at index: " + i);
            }
        }
    }
    static public final Projector namePropProjector = new PropertyProjector("name");

    // idzie od strony defList - kolejnosc wzieta stad
    @SuppressWarnings("unchecked")
    static public void mergeLists2(List mainList, List defList) {
        if (logger.isDebugEnabled()) {
            logger.debug("mergeLists2: START");
        }

        if (mainList == null) {
            throw new LameRuntimeException("mergeLists2: mainList == null, cannot merge in place");
        }

        if (defList == null) {
            return;
        }

        Map listDups = LameUtils.findDuplicates(mainList, namePropProjector);
        if (listDups.size() > 0) {
            throw new LameRuntimeException("mainList has duplicates: " + listDups);
        }

        listDups = LameUtils.findDuplicates(defList, namePropProjector);
        if (listDups.size() > 0) {
            throw new LameRuntimeException("defList has duplicates: " + listDups);
        }

        Map<String, Pair<Integer, Object>> defMap = new LinkedHashMap<String, Pair<Integer, Object>>();
        for (int i = 0; i < defList.size(); i++) {
            Object o = defList.get(i);
            //Object oldVal =
            defMap.put((String) getProperty(o, "name"), new Pair<Integer, Object>(i, o));
            //if (oldVal != null)
            //    throw new LameRuntimeException("defList has duplicates, example: " + oldVal);
        }

        List mainListCopy = new ArrayList(mainList);
        mainList.clear();
        mainList.addAll(defList);

        for (Object o : mainListCopy) {
            if (o == null) {
                throw new LameRuntimeException("Item in mainListCopy is null");
            }
            String name = (String) getProperty(o, "name");

            if (logger.isDebugEnabled()) {
                logger.debug("mergeLists2: item name: " + name);
            }

            Pair<Integer, Object> pair = defMap.get(name);
            Object valToSet;

            if (pair != null) {
                if (pair.v2 != null) {
                    valToSet = mergeMuppets(o, pair.v2, false);
                    if (valToSet == null) {
                        valToSet = o;
                    }
                } else {
                    valToSet = o;
                }
                if (valToSet != null) {
                    mainList.set(pair.v1, valToSet);
                }
            } else if (o != null) {
                mainList.add(o);
            }
        }

        for (Object o : mainList) {
            if (o == null) {
                throw new LameRuntimeException("Item in mainList is null");
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("mergeLists2: END");
        }
    }

    static public <T> void mergeComplexMuppets(T mainMuppet, T defMuppet) {
        if (logger.isDebugEnabled()) {
            logger.debug("mergeComplexMuppets: START for class: " + mainMuppet.getClass().getName());
        }

        Set<String> propNames = getMuppetProps(mainMuppet, 3);

        if (propNames.isEmpty()) {
            throw new LameRuntimeException("Cannot merge object with no readable/writable properties, class: "
                    + mainMuppet.getClass().getName());
        }

        for (String propName : propNames) {
            if (logger.isDebugEnabled()) {
                logger.debug("mergeComplexMuppets: class: " + mainMuppet.getClass().getName() + ", prop: " + propName);
            }

            Object propVal = getProperty(mainMuppet, propName);
            Object defPropVal = getProperty(defMuppet, propName);
            if (propVal == null) {
                if (defPropVal != null) {
                    setProperty(mainMuppet, propName, defPropVal);
                }
            } else {
                try {
                    Object newVal = mergeMuppets(propVal, defPropVal, false);
                    if (newVal != null) {
                        setProperty(mainMuppet, propName, newVal);
                    }
                    Object testVal = getProperty(mainMuppet, propName);
                    if (testVal != null && testVal.getClass().isArray()) {
                        checkArrayForNullItem((Object[]) testVal, "property %s", propName);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    throw new LameRuntimeException("Error while merging/setting prop: "
                            + propName + " for muppet of class: " + mainMuppet.getClass().getName(), ex);
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("mergeComplexMuppets: END for class: " + mainMuppet.getClass().getName());
        }
    }

// primitive wrappers are immutable
    static public boolean isImmutableObject(Object obj) {
        return obj instanceof Integer
                || obj instanceof String
                || obj instanceof Boolean
                || obj.getClass().isEnum()
                || obj.getClass().isArray();
    }

    static public Object[] mergeArrays(Object[] mainArray, Object[] defArray) {
        if (defArray == null) {
            return mainArray;
        }
        if (mainArray == null) {
            return defArray;
        }

        int mainLen = mainArray.length;
        List<Object> mainList = LameUtils.arrayToArrayList(mainArray);
        List defList = Arrays.asList(defArray);
        int mainList0size = mainList.size();
        mergeLists2(mainList, defList);
        int mainList1size = mainList.size();
        int nullIdx = mainList.indexOf(null);
        if (nullIdx >= 0) {
            throw new LameRuntimeException("mergeList2 returned list with null item at idx: " + nullIdx);
        }
        Object[] res = mainList.toArray(mainArray);
        int resLen = res.length;
        checkArrayForNullItem(res, "converting list to array failed" + (" mailLen=" + mainLen + " mainList0size=" + mainList0size
                + " mainList1size=" + mainList1size + " resLen=" + resLen));
        return res;
    }

    static public void mergeMaps(Map<Object, Object> mainMap, Map<Object, Object> defMap) {
        if (defMap == null) {
            return;
        }
        for (Entry<Object, Object> e : defMap.entrySet()) {
            Object key = e.getKey();
            if (logger.isDebugEnabled()) {
                logger.debug("mergeMaps: prop: " + key);
            }
            Object defVal = e.getValue();
            Object v = mainMap.get(key);
            if (v == null) {
                mainMap.put(key, defVal);
            } else {
                Object newVal = mergeMuppets(v, defVal, false);
                if (newVal != null) {
                    mainMap.put(key, newVal);
                }
            }

        }
    }

    // merged in place -> result == null
    @SuppressWarnings("unchecked")
    static public <T> T mergeMuppets(T mainMuppet, T defMuppet, boolean forceInPlace) {
        if (mainMuppet == null) {
            if (forceInPlace) {
                throw new LameRuntimeException("mainMuppet is null, but should merged in place");
            } else {
                return defMuppet;
            }
        }

        if (mainMuppet.getClass().isArray()) {
            if (forceInPlace) {
                throw new LameRuntimeException("Cannot merge immutable array of class " + mainMuppet.getClass().getComponentType().getName() + " in place");
            }
            checkArrayForNullItem((Object[]) mainMuppet, "mainMuppet is array with null item");
            checkArrayForNullItem((Object[]) defMuppet, "defMuppet is array with null item");
            return (T) mergeArrays((Object[]) mainMuppet, (Object[]) defMuppet);
        }

        boolean isImmutable = isImmutableObject(mainMuppet);
        if (isImmutable) {
            if (forceInPlace) {
                throw new LameRuntimeException("Cannot merge immutable object of class " + mainMuppet.getClass().getName() + " in place");
            }
            return mainMuppet == null ? defMuppet : mainMuppet;
        }

        if (mainMuppet instanceof Map) {
            mergeMaps((Map) mainMuppet, (Map) defMuppet);
        } else if (mainMuppet instanceof List) {
            mergeLists2((List) mainMuppet, (List) defMuppet);
        } else {
            mergeComplexMuppets(mainMuppet, defMuppet);
        }

        return null;
    }

    static public Map<String, Object> muppetToMap(Object muppet, Set<String> propNames) {
        Map<String, Object> res = new LinkedHashMap<String, Object>();
        for (String propName : propNames) {
            Object propVal = getProperty(muppet, propName);
            if (propVal != null) {
                res.put(propName, propVal);
            }
        }
        return res;
    }

    static public Map<String, Object> muppetToMap(Object muppet, Class asClass) {
        return muppetToMap(muppet, getMuppetProps(asClass, 3));
    }

    static public Map<String, Object> muppetToMap(Object muppet) {
        return muppetToMap(muppet, getMuppetProps(muppet, 3));
//        Set<String> propNames = getMuppetProps(muppet, 3);
//        Map<String, Object> res = new HashMap<String, Object>();
//        for (String propName : propNames) {
//            Object propVal = getProperty(muppet, propName);
//            if (propVal != null) {
//                res.put(propName, propVal);
//            }
//        }
//        return res;
    }

    @SuppressWarnings("unchecked")
    public static <T> T cloneMuppet(T muppet) {
        Class<T> c = (Class<T>) muppet.getClass();
        T res = null;
        try {
            res = c.newInstance();
        } catch (Exception ex) {
            logger.errorAndThrowNew("newInstance failed for muppet class: " + c.getSimpleName(), ex);
        }
        res = LameUtils.nullToDef(mergeMuppets(res, muppet, false), res);
        return res;
    }

    //ww: zwracana mapa ma nazwy props�w przekonwertowane do zapisu
    // bazodanowego z UPPERCASE, np. property w beanie o nazwie firstName to b�dzie
    // FIRST_NAME w mapie wynikowej!
    public static Map<String, Object> newMapFromBean(Object muppet) {
        return newMapFromBean(muppet, null);
    }

    //ww: specjalna wersja - pomija pola z adnotacj� PropNotInTable
    public static Map<String, Object> newMapFromBeanToStore(Object muppet) {
        return newMapFromBean(muppet, PropNotInTable.class);
    }

    public static Map<String, Object> newMapFromBean(Object muppet, Class<? extends Annotation> optAnnotationToIgnore) {
        Map<String, Object> res = new HashMap<String, Object>();
        Set<String> propNames = getMuppetProps(muppet, 3, optAnnotationToIgnore);
//        System.out.println("newMapFromBean: propNames=" + propNames + ", optAnnotationToIgnore=" + optAnnotationToIgnore);
        for (String prop : propNames) { // both
            Object obj = getProperty(muppet, prop);
            res.put(LameUtils.beanFieldNameToTableColName(prop).toUpperCase(), obj);
        }
        return res;
    }

    public static List<Map<String, ? extends Object>> muppetsToListOfMap(List muppets) {
        List<Map<String, ? extends Object>> res = new ArrayList<Map<String, ? extends Object>>();
        for (Object muppet : muppets) {
            res.add(muppetToMap(muppet));
        }
        return res;
    }
//    static class TestMuppet {
//
//        TestMuppet(List<String> ala, Integer kot, boolean ma, int i) {
//            this.ala = ala;
//            this.kot = kot;
//            this.ma = ma;
//            this.i = i;
//        }
//        public List<String> ala;
//        public Integer kot;
//        public boolean ma;
//        public int i;
//    }
//
//    public static void main(String... args) {
//        String[] arr = {"aa"};
//        Object arrAsObj = arr;
//        Object[] a = (Object[]) arrAsObj;
//
//        TestMuppet mainM = new TestMuppet(Arrays.asList("uu"), null, false, 10);
//        TestMuppet defM = new TestMuppet(Arrays.asList("bbb", "xx"), 100, true, 1);
//
//        mergeMuppets(mainM, defM, true);
//
//        System.out.println(muppetToMap(mainM));
//    }
}
