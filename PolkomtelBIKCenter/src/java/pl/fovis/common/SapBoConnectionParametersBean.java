/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class SapBoConnectionParametersBean implements Serializable{

    public String server;
    public String type;
    public String user;
    public String password;
    public String path;
    public int withUsers;
}
