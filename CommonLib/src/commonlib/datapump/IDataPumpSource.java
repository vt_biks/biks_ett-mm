/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.datapump;

/**
 *
 * @author pmielanczuk
 */
public interface IDataPumpSource {

    public abstract ColumnMetadata[] openSource() throws Exception;

    public abstract void closeSource() throws Exception;

    public abstract boolean hasSourceData() throws Exception;

    public abstract Object[] readDataRow() throws Exception;
}
