declare @tab table (
	[id] [int] NOT NULL,
	[node_kind_id] [int] NOT NULL,
	[name] [varchar](4000) NOT NULL,
	[tree_id] [int] NOT NULL,
	[search_rank] [int] NOT NULL,
	[vote_sum] [int] NOT NULL,
	[vote_cnt] [int] NOT NULL,
	[vote_val] [float] NOT NULL,
	sum_combined_rank float,
	max_combined_rank float,
	sum_search_weight float,
	max_search_weight float
)

	insert into @tab (
	[id],
	[node_kind_id],
	[name],
	[tree_id],
	[search_rank],
	[vote_sum],
	[vote_cnt],
	[vote_val],
    sr.sum_combined_rank, sr.max_combined_rank, sr.sum_search_weight, sr.max_search_weight
	)
    select --bn.*,
	bn.[id],
	bn.[node_kind_id],
	bn.[name],
	bn.[tree_id],
	bn.[search_rank],
	bn.[vote_sum],
	bn.[vote_cnt],
	bn.[vote_val],
    sr.sum_combined_rank, sr.max_combined_rank, sr.sum_search_weight, sr.max_search_weight
	--into #xxx    
    from
    bik_node bn    
        inner join
        (select node_id,
        sum(sr.[rank] * coalesce(ap.search_weight, 1)) as sum_combined_rank,
        max(sr.[rank] * coalesce(ap.search_weight, 1)) as max_combined_rank,
        sum(coalesce(ap.search_weight, 1)) as sum_search_weight,
        max(coalesce(ap.search_weight, 1)) as max_search_weight
        from bik_searchable_attr_val av inner join
        containstable(bik_searchable_attr_val, *, '"id" AND "konto"') sr on av.id = sr.[key]
        inner join bik_searchable_attr a on av.attr_id = a.id
        left join bik_searchable_attr_props ap on a.name = ap.name
        group by node_id) sr on sr.node_id = bn.id
    
    where bn.is_deleted = 0 and (bn.linked_node_id is null or bn.disable_linked_subtree <> 0)
    and bn.tree_id in (1,2)
    and  bn.node_kind_id in (56,39)



    select top 20 bn.id as node_id,
    bn.name,
    bnk.code as node_kind_code,
    bnk.caption as node_kind_caption,
    bt.name as tree_name,
    bt.code as tree_code,
    bn.vote_sum,
    bn.vote_cnt
    from @tab as bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    inner join bik_tree bt on bn.tree_id = bt.id
    order by bn.max_search_weight desc, bn.search_rank + bnk.search_rank desc, bn.vote_sum desc, sum_combined_rank desc 
