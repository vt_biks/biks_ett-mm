/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.ArrayList;
import java.net.URI;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wezyr
 */
public class BaseUtilsTest {

    public BaseUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testTrimLeft() {
        assertEquals("ala  ", BaseUtils.trimLeft(" ala  "));
        assertEquals("", BaseUtils.trimLeft("    "));
        assertEquals("x", BaseUtils.trimLeft("   x"));
    }

    @Test
    public void testTrimRight() {
        assertEquals(" ala", BaseUtils.trimRight(" ala  "));
        assertEquals("", BaseUtils.trimRight("   "));
    }

    @Test
    public void testDropLastItems() {
        String s = "ala ma kota";
        assertEquals("ala", BaseUtils.dropLastItems(s, " ", 2));
        assertEquals("ala ma", BaseUtils.dropLastItems(s, " ", 1));
        assertEquals("", BaseUtils.dropLastItems(s, " ", 3));

        s = "ala  ma  kota";
        assertEquals("ala  ma", BaseUtils.dropLastItems(s, " ", 2));
        assertEquals("ala  ma ", BaseUtils.dropLastItems(s, " ", 1));
        assertEquals("ala ", BaseUtils.dropLastItems(s, " ", 3));
        assertEquals("ala", BaseUtils.dropLastItems(s, " ", 4));

        assertEquals("ala", BaseUtils.dropLastItems(s, "  ", 2));
        assertEquals("ala  ma", BaseUtils.dropLastItems(s, "  ", 1));
        assertEquals("", BaseUtils.dropLastItems(s, "  ", 3));

        s = "ala  ma  kota ";
        assertEquals("ala  ma  kota", BaseUtils.dropLastItems(s, " ", 1));
        assertEquals("ala  ma", BaseUtils.dropLastItems(s, "  ", 1));
    }

    @Test
    public void testDropLastItems2() {
        assertEquals("2.65", BaseUtils.doubleToString(2.654, 2));
        assertEquals("-2.65", BaseUtils.doubleToString(-2.654, 2));
        assertEquals("0.00", BaseUtils.doubleToString(0, 2));
        assertEquals("3", BaseUtils.doubleToString(3.1, 0));
        assertEquals("-2.611", BaseUtils.doubleToString(-2.611, 3));
        assertEquals("-0.99", BaseUtils.doubleToString(-0.999, 2));
        assertEquals("-0.01", BaseUtils.doubleToString(-0.011, 2));
        assertEquals("0.01", BaseUtils.doubleToString(0.019, 2));
    }

    @Test
    public void testGetLastItems() {
        assertEquals("kota", BaseUtils.getLastItems("ala ma kota", " ", 1));
        assertEquals("", BaseUtils.getLastItems("ala ma kota ", " ", 1));
        assertEquals("kota ", BaseUtils.getLastItems("ala ma kota ", " ", 2));
        assertEquals("ma--kota", BaseUtils.getLastItems("ala--ma--kota", "--", 2));
        assertEquals("ala--ma--kota", BaseUtils.getLastItems("ala--ma--kota", "--", 3));
        assertEquals("ala--ma--kota", BaseUtils.getLastItems("ala--ma--kota", "--", 7));
        assertEquals("kota", BaseUtils.getLastItems("kota", " ", 2));
        assertEquals("kota", BaseUtils.getLastItems("kota", "--", 2));
        assertEquals("", BaseUtils.getLastItems("-----", "--", 1));
        assertEquals("", BaseUtils.getLastItems("--", "--", 1));
        assertEquals("--", BaseUtils.getLastItems("--", "--", 2));
        assertEquals("", BaseUtils.getLastItems("ala??ma??kota", "??", 0));
    }

    @Test
    public void testDropFileExtFromFullPath() {
        assertEquals("pytania", BaseUtils.dropFileExtFromFullPath("pytania.html"));
    }

    @Test
    public void testSplitByLastNthSep() {
        Pair<String, String> p = BaseUtils.splitByLastNthSep("pytania.html", "/", 1);

        assertNull(p.v1);
        assertEquals("pytania.html", p.v2);
    }

    private static class NPBInner implements INamedPropsBean {

        public Set<String> getPropNames() {
            return Collections.singleton("b");
        }

        public Object getPropValue(String propName) {
            return null;
        }
    }

    private static class NPBOuter implements INamedPropsBean {

        private NPBInner a = new NPBInner();

        public Set<String> getPropNames() {
            return Collections.singleton("a");
        }

        public Object getPropValue(String propName) {
            return a;
        }
    }

    @Test
    public void testNamedPropsBeanUtils() {
        NPBOuter npb1 = new NPBOuter();
        System.out.println("npb1 as map: " + BaseUtils.namedPropsBeanToMap(npb1, true));
        NPBOuter npb2 = new NPBOuter();
        System.out.println("npb2 as map: " + BaseUtils.namedPropsBeanToMap(npb2, true));
        System.out.println("changed props: " + BaseUtils.extractChangedEntries(BaseUtils.namedPropsBeanToMap(npb1, true),
                BaseUtils.namedPropsBeanToMap(npb2, true)));
        LinkedHashMap m1 = new LinkedHashMap();
        LinkedHashMap m2 = new LinkedHashMap();
        assertEquals(m1, m2);
        m1.put("x", null);
        m2.put("x", null);
        assertEquals(m1, m2);
        m1.put("y", new LinkedHashMap());
        m2.put("y", new LinkedHashMap());
        assertEquals(m1, m2);
    }

    @Test
    public void testFindItemInStr() {
        assertEquals(0, BaseUtils.findItemInStr("ala ma kota", "ala", " "));
        assertEquals(1, BaseUtils.findItemInStr("ala ma kota", "ma", " "));
        assertEquals(-1, BaseUtils.findItemInStr("ala ma kota", "ma", ","));
        assertEquals(-1, BaseUtils.findItemInStr("", "ma", " "));
        assertEquals(-1, BaseUtils.findItemInStr("ala ma kota", "kot", " "));
        assertEquals(2, BaseUtils.findItemInStr("ala ma kota", "kota", " "));
        assertEquals(true, BaseUtils.strHasItem("ala ma kota", "kota", " "));
        assertEquals(true, BaseUtils.strHasItem("ala, ma, kota", "ala", ", "));
        assertEquals(false, BaseUtils.strHasItem("ala,ma, kota", "ala", ", "));
        assertEquals(true, BaseUtils.strHasItem("ala,ma, kota", "ala,ma", ", "));
        assertEquals(true, BaseUtils.strHasItem("ala,ma, kota", "kota", ", "));
        assertEquals(true, BaseUtils.strHasItem("ala,ma, kota, ", "", ", "));
        assertEquals(true, BaseUtils.strHasItem(", ala,ma, kota", "", ", "));
        assertEquals(true, BaseUtils.strHasItem("ala,ma, , kota", "", ", "));
    }

    @Test
    public void testStrReplaceOrAddItem() {
        assertEquals("ala miała kota", BaseUtils.strReplaceOrAddItem("ala ma kota", "ma", "miała", " "));
        assertEquals("ala ma psa", BaseUtils.strReplaceOrAddItem("ala ma kota", "kota", "psa", " "));
        assertEquals("ala ma kota i psa", BaseUtils.strReplaceOrAddItem("ala ma kota", "chomika", "i psa", " "));
        assertEquals("ala i helga ma kota", BaseUtils.strReplaceOrAddItem("ala ma kota", "ala", "ala i helga", " "));
    }

    @Test
    public void testSplitByCapitals() {
        List<String> res;
        res = BaseUtils.splitByCapitals("alaMaKota");
        assertEquals(3, res.size());
        assertEquals("ala", res.get(0));
        assertEquals("Ma", res.get(1));
        assertEquals("Kota", res.get(2));
        res = BaseUtils.splitByCapitals("AlaMaKota");
        assertEquals(3, res.size());
        assertEquals("Ala", res.get(0));
        assertEquals("Ma", res.get(1));
        assertEquals("Kota", res.get(2));
        res = BaseUtils.splitByCapitals("ABBa");
        assertEquals(3, res.size());
        assertEquals("A", res.get(0));
        assertEquals("B", res.get(1));
        assertEquals("Ba", res.get(2));
        res = BaseUtils.splitByCapitals("kura");
        assertEquals(1, res.size());
        assertEquals("kura", res.get(0));
        res = BaseUtils.splitByCapitals("kuraX");
        assertEquals(2, res.size());
        assertEquals("kura", res.get(0));
        assertEquals("X", res.get(1));
        res = BaseUtils.splitByCapitals("");
        assertEquals(0, res.size());
    }

    @Test
    public void testGetBestAcceptedAndSupportedLanguage() {
        assertEquals("pl",
                BaseUtils.getBestAcceptedAndSupportedLanguage("pl;q=0.9,en;q=0.3",
                BaseUtils.paramsAsSet("en", "de", "pl"), "??"));
        assertEquals("pl",
                BaseUtils.getBestAcceptedAndSupportedLanguage("pl,en;q=0.7,en-us;q=0.3",
                BaseUtils.paramsAsSet("en", "de", "pl"), "??"));
        assertEquals("en",
                BaseUtils.getBestAcceptedAndSupportedLanguage("en;q=0.7,en-us;q=0.3,pl;q=0.1,ch",
                BaseUtils.paramsAsSet("en", "de", "pl"), "??"));
        assertEquals("??",
                BaseUtils.getBestAcceptedAndSupportedLanguage("gr;q=0.7,gr-us;q=0.3,fr;q=0.1,ch",
                BaseUtils.paramsAsSet("en", "de", "pl"), "??"));
    }

    @Test
    public void testFirstPos() {
        assertEquals(5, BaseUtils.firstPos(5, 6));
        assertEquals(5, BaseUtils.firstPos(5, -1));
        assertEquals(5, BaseUtils.firstPos(-1, 5));
        assertEquals(0, BaseUtils.firstPos(-1, 0));
    }

    @Test
    public void testReplaceNewLinesToRowSep() {
        assertEquals("", BaseUtils.replaceNewLinesToRowSep("", "<br />"));
        assertEquals("z", BaseUtils.replaceNewLinesToRowSep("z", "<br />"));
        assertEquals("ala ", BaseUtils.replaceNewLinesToRowSep("ala ", "<br />"));
        assertEquals("ala<br />ma<br />kota<br />i psa<br />haha!", BaseUtils.replaceNewLinesToRowSep("ala\nma\rkota\r\ni psa\nhaha!", "<br />"));
    }

    public static Pair<String, String> splitGlobalUrlByURI(String url) {
        URI u;
        try {
            u = new URI(url);
        } catch (Exception ex) {
            throw new RuntimeException("malformed url: " + url, ex);
        }
        StringBuilder p1 = new StringBuilder();
        if (u.getScheme() != null) {
            p1.append(u.getScheme()).append("://").append(u.getHost());
        }
        if (u.getPort() != -1) {
            p1.append(":").append(u.getPort());
        }
        StringBuilder p2 = new StringBuilder();
        p2.append(u.getPath());
        if (u.getQuery() != null) {
            p2.append("?").append(u.getQuery());
        }
        return new Pair<String, String>(p1.toString(), p2.toString());
    }

    protected String splitGlobalUrlMerged(String url) {
        Pair<String, String> p = BaseUtils.splitGlobalUrl(url);
        assertEquals(p.v1 + p.v2, url);

        Pair<String, String> p2 = splitGlobalUrlByURI(url);
        assertEquals(p, p2);

        return p.v1 + "|" + p.v2;
    }

    @Test
    public void testDropHttpProtocol() {
        assertEquals(null, BaseUtils.dropHttpProtocol(null));
        assertEquals("ala", BaseUtils.dropHttpProtocol("http://ala"));
        assertEquals("ala", BaseUtils.dropHttpProtocol("https://ala"));
        assertEquals("ala.pl/abc?xxx", BaseUtils.dropHttpProtocol("https://ala.pl/abc?xxx"));
        assertEquals("ala.pl/abc?xxx", BaseUtils.dropHttpProtocol("http://ala.pl/abc?xxx"));
    }

    @Test
    public void testGetDomainFromGlobalUrl() {
        assertEquals(null, BaseUtils.getDomainFromGlobalUrl(null));
        assertEquals("ala", BaseUtils.getDomainFromGlobalUrl("http://ala"));
        assertEquals("ala", BaseUtils.getDomainFromGlobalUrl("https://ala"));
        assertEquals("ala.pl", BaseUtils.getDomainFromGlobalUrl("https://ala.pl/abc?xxx"));
        assertEquals("ala.pl", BaseUtils.getDomainFromGlobalUrl("http://ala.pl/abc?xxx"));
    }

    @Test
    public void testSplitGlobalUrl() {
        //assertEquals("|", splitGlobalUrlMerged(""));
        assertEquals("http://ala.ma.com|/ufo", splitGlobalUrlMerged("http://ala.ma.com/ufo"));
        assertEquals("http://ala.ma.com|/", splitGlobalUrlMerged("http://ala.ma.com/"));
        assertEquals("http://ala.ma.com|", splitGlobalUrlMerged("http://ala.ma.com"));
        assertEquals("http://ala.ma.com:8080|", splitGlobalUrlMerged("http://ala.ma.com:8080"));
        assertEquals("http://ala.ma.com:777|/ufo?x=ą", splitGlobalUrlMerged("http://ala.ma.com:777/ufo?x=ą"));

        assertEquals("https://ala.ma.com|/ufo", splitGlobalUrlMerged("https://ala.ma.com/ufo"));
        assertEquals("https://ala.ma.com|/", splitGlobalUrlMerged("https://ala.ma.com/"));
        assertEquals("https://ala.ma.com|", splitGlobalUrlMerged("https://ala.ma.com"));
        assertEquals("https://ala.ma.com:8080|", splitGlobalUrlMerged("https://ala.ma.com:8080"));
        assertEquals("https://ala.ma.com:777|/ufo", splitGlobalUrlMerged("https://ala.ma.com:777/ufo"));

        assertEquals("|https//ala.ma.com/ufo", splitGlobalUrlMerged("https//ala.ma.com/ufo"));
        assertEquals("|/", splitGlobalUrlMerged("/"));
        assertEquals("|a", splitGlobalUrlMerged("a"));
        assertEquals("|/aasdas/asdsad/asd", splitGlobalUrlMerged("/aasdas/asdsad/asd"));
    }

    @Test
    public void testEscapeAndMerge() {
        assertEquals("ala", BaseUtils.escapeAndMerge(BaseUtils.splitBySep("ala", ","), "|"));
        assertEquals("ala|ma", BaseUtils.escapeAndMerge(BaseUtils.splitBySep("ala,ma", ","), "|"));
        assertEquals("ala|m||a", BaseUtils.escapeAndMerge(BaseUtils.splitBySep("ala,m|a", ","), "|"));
        assertEquals("ala|m||||a", BaseUtils.escapeAndMerge(BaseUtils.splitBySep("ala,m||a", ","), "|"));
        assertEquals("ala|||m||||a", BaseUtils.escapeAndMerge(BaseUtils.splitBySep("ala|,m||a", ","), "|"));
    }

    @Test
    public void testSplitAndUnescape() {
        assertEquals("ala", BaseUtils.mergeWithSepEx(BaseUtils.splitAndUnescape("ala", "|"), ","));
        assertEquals("ala|c", BaseUtils.mergeWithSepEx(BaseUtils.splitAndUnescape("ala||c", "|"), ","));
        assertEquals("ala|c,d", BaseUtils.mergeWithSepEx(BaseUtils.splitAndUnescape("ala||c|d", "|"), ","));
        assertEquals(",ala|c,d", BaseUtils.mergeWithSepEx(BaseUtils.splitAndUnescape("|ala||c|d", "|"), ","));
        assertEquals(",ala|c,d|", BaseUtils.mergeWithSepEx(BaseUtils.splitAndUnescape("|ala||c|d||", "|"), ","));
        assertEquals("|", BaseUtils.mergeWithSepEx(BaseUtils.splitAndUnescape("||", "|"), ","));
        assertEquals("||", BaseUtils.mergeWithSepEx(BaseUtils.splitAndUnescape("||||", "|"), ","));
        assertEquals("|, ,", BaseUtils.mergeWithSepEx(BaseUtils.splitAndUnescape("||| |", "|"), ","));
    }

    @Test
    public void testRemoveManyFromArrayList() {
        List<String> items = new ArrayList<String>(BaseUtils.arrayToArrayList("ala", "kot", "pies", "ala", "ala", "hela"));

        List<String> workCopy = new ArrayList<String>(items);
        BaseUtils.removeManyFromArrayList(workCopy, new IPredicate<String>() {

            public Boolean project(String val) {
                return BaseUtils.safeEquals(val, "ala");
            }
        }, true);

        assertEquals(3, workCopy.size());
        assertEquals("kot", workCopy.get(0));
        assertEquals("pies", workCopy.get(1));
        assertEquals("hela", workCopy.get(2));

        workCopy = new ArrayList<String>(items);
        BaseUtils.removeManyFromArrayList(workCopy, new IPredicate<String>() {

            public Boolean project(String val) {
                return false;
            }
        }, true);

        assertTrue(BaseUtils.equalByElements(workCopy, items));

        workCopy = new LinkedList<String>(items);
        IPredicate<String> removeAll = new IPredicate<String>() {

            public Boolean project(String val) {
                return true;
            }
        };
        BaseUtils.removeManyFromArrayList(workCopy, removeAll, false);

        assertEquals(0, workCopy.size());

        boolean thrownEx = false;
        try {
            BaseUtils.removeManyFromArrayList(new LinkedList<String>(items), removeAll, true);
        } catch (Exception ex) {
            thrownEx = true;
            //ex.printStackTrace();
        }
        assertTrue(thrownEx);
    }

    @Test
    public void testRemoveManyFromList() {
        List<String> items = new LinkedList<String>(BaseUtils.arrayToArrayList("ala", "kot", "pies", "ala", "ala", "hela"));

        List<String> workCopy = new ArrayList<String>(items);
        BaseUtils.removeManyFromList(workCopy, new IPredicate<String>() {

            public Boolean project(String val) {
                return BaseUtils.safeEquals(val, "ala");
            }
        });

        assertEquals(3, workCopy.size());
        assertEquals("kot", workCopy.get(0));
        assertEquals("pies", workCopy.get(1));
        assertEquals("hela", workCopy.get(2));

        workCopy = new ArrayList<String>(items);
        BaseUtils.removeManyFromList(workCopy, new IPredicate<String>() {

            public Boolean project(String val) {
                return false;
            }
        });

        assertTrue(BaseUtils.equalByElements(workCopy, items));

        workCopy = new LinkedList<String>(items);
        IPredicate<String> removeAll = new IPredicate<String>() {

            public Boolean project(String val) {
                return true;
            }
        };
        BaseUtils.removeManyFromArrayList(workCopy, removeAll, false);

        assertEquals(0, workCopy.size());
    }
}
