/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

/**
 *
 * @author pmielanczuk
 */
public class LinkToBikNodeBean {

    public int srcNodeId;
    public String dstTreeCode;
    public Integer dstNodeId;
    public String optDstSubNodeName;

    public LinkToBikNodeBean() {
    }

    public LinkToBikNodeBean(int srcNodeId, String dstTreeCode, Integer dstNodeId, String optDstSubNodeName) {
        this.srcNodeId = srcNodeId;
        this.dstTreeCode = dstTreeCode;
        this.dstNodeId = dstNodeId;
        this.optDstSubNodeName = optDstSubNodeName;
    }

    @Override
    public String toString() {
        return "LinkToBikNodeBean{" + "srcNodeId=" + srcNodeId + ", dstTreeCode=" + dstTreeCode + ", dstNodeId=" + dstNodeId + ", optDstSubNodeName=" + optDstSubNodeName + '}';
    }
}
