exec sp_check_version '1.0.35';
go


create table bik_role_for_node (
  id int identity not null primary key,
  caption varchar(255) not null,
);
go

alter table bik_role_for_node add constraint uk_bik_role_for_node_caption unique (caption);
go

create table bik_role_in_node_kind (
  id int identity not null primary key,
  node_kind_id int not null foreign key references bik_node_kind (id),
  role_id int not null foreign key references bik_role_for_node (id)
);
go

insert into bik_role_for_node (caption)
values ('Specjalista IT')
insert into bik_role_for_node (caption)
values ('Sponsor')
insert into bik_role_for_node (caption)
values ('Ekspert BI')
go


-- select * from bik_specialist

insert into bik_role_in_node_kind (role_id, node_kind_id)
select role_id, node_kind_id
from 
(
select distinct nk.id as node_kind_id, 
  (select id from bik_role_for_node where caption = 'Specjalista IT') as role_id
from bik_specialist s inner join bik_node n on s.node_id = n.id
inner join bik_node_kind nk on n.node_kind_id = nk.id
where s.proj_id is not null
union all
select distinct nk.id as node_kind_id, 
  (select id from bik_role_for_node where caption = 'Sponsor') as role_id
from bik_specialist s inner join bik_node n on s.node_id = n.id
inner join bik_node_kind nk on n.node_kind_id = nk.id
where s.spon_id is not null
union all
select distinct nk.id as node_kind_id, 
  (select id from bik_role_for_node where caption = 'Ekspert BI') as role_id
from bik_specialist s inner join bik_node n on s.node_id = n.id
inner join bik_node_kind nk on n.node_kind_id = nk.id
where s.expe_id is not null
) x
go

-- select * from bik_node_kind_user_role

-- drop table bik_user_in_node 

create table bik_user_in_node 
( id int identity not null primary key,
  user_id int not null foreign key references bik_user (id),
  role_in_kind_id int not null foreign key references bik_role_in_node_kind (id),
  node_id int not null foreign key references bik_node (id)
)
go


insert into bik_user_in_node (user_id, node_id, role_in_kind_id)
select proj_id as user_id, node_id,
  (select rnk.id from bik_role_in_node_kind rnk inner join bik_role_for_node rn on rnk.role_id = rn.id
  where rn.caption = 'Specjalista IT' and rnk.node_kind_id = nk.id) as role_in_kind_id
from bik_specialist s inner join bik_node n on s.node_id = n.id
inner join bik_node_kind nk on n.node_kind_id = nk.id
where s.proj_id is not null and n.linked_node_id is null
union all
select spon_id as user_id, node_id,
  (select rnk.id from bik_role_in_node_kind rnk inner join bik_role_for_node rn on rnk.role_id = rn.id
  where rn.caption = 'Sponsor' and rnk.node_kind_id = nk.id) as role_in_kind_id
from bik_specialist s inner join bik_node n on s.node_id = n.id
inner join bik_node_kind nk on n.node_kind_id = nk.id
where s.spon_id is not null and n.linked_node_id is null
union all
select expe_id as user_id, node_id,
  (select rnk.id from bik_role_in_node_kind rnk inner join bik_role_for_node rn on rnk.role_id = rn.id
  where rn.caption = 'Ekspert BI' and rnk.node_kind_id = nk.id) as role_in_kind_id
from bik_specialist s inner join bik_node n on s.node_id = n.id
inner join bik_node_kind nk on n.node_kind_id = nk.id
where s.expe_id is not null and n.linked_node_id is null
go


-- select id from 
update bik_user
set activ = 0
where len(rtrim(ltrim(replace(coalesce(name, ''), '_', '')))) < 1
go

-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------

create table bik_node_vote(
id int identity(1,1) not null primary key,
node_id int not null references bik_node(id),
user_id int not null references bik_user(id),
value int not null
constraint uk_bik_node_vote unique (node_id, user_id)
)
go


-----------------------------------------------------------------

exec sp_update_version '1.0.35', '1.0.36';
go
