/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author wezyr
 */
public class RawString {

    public String str;

    public RawString(String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return str;
    }
}
