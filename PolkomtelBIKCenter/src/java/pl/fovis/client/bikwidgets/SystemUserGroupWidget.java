/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.SystemUserDialog;
import pl.fovis.common.CustomRightRoleBean;
import pl.fovis.common.TreeNodeBranchBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class SystemUserGroupWidget extends SystemGroupWidget {

    @Override
    protected void buildMainWidgets() {
        main = new VerticalPanel();
        main.setSpacing(10);
        main.setWidth("100%");
        main.add(new HTML("<h1>" + userGroup.name + "</h1></br>"));
        usersInGroupVp(I18n.userInGroup.get(), userGroup.usersName);
        grupInOrInGrupVp(I18n.groupIn.get(), userGroup.inGroup);
//        grupInOrInGrupVp(I18n.inGroup.get(), userGroup.groupIn);
        main.add(createLabelWithStyle(I18n.uprawnienia.get()));
        grid = new FlexTable();
        grid.addStyleName("sysUserDialog");
        grid.setWidth("300px");
        addRightRolesToGrid(0);
        main.add(grid);
        saveBtn = new PushButton(I18n.zapisz.get());
        NewLookUtils.makeCustomPushButton(saveBtn);
        saveBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                Map<Integer, Set<Pair<Integer, Integer>>> flatCrrEntries = makeFlatCrrEntries();
                BIKClientSingletons.getService().updateSystemUserGroup(userGroup.id, flatCrrEntries, new StandardAsyncCallback<Void>() {

                    @Override
                    public void onSuccess(Void result) {
                        BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get());
                    }
                });
            }
        });
        main.add(saveBtn);
    }

    protected int addRightRolesToGrid(int row) {
        List<CustomRightRoleBean> crrbs = BIKClientSingletons.getCustomRightRoles();
        for (CustomRightRoleBean crr : crrbs) {
            final CustomRightRoleBean crrF = crr;
            final CheckBox cb = createCheckBoxWithStyle();
            cb.setValue(!BaseUtils.isCollectionEmpty(customRightRoleUserEntries.get(crr.id)));
            FlowPanel flowPanel = new FlowPanel();
            flowPanel.add(cb);
            if (!BaseUtils.isStrEmptyOrWhiteSpace(crr.treeSelector) && crr.selectorMode != 0) {
            cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

                @Override
                public void onValueChange(ValueChangeEvent<Boolean> event) {
                    if (event.getValue()) {
                            showEditRoleRestrictionsSelectorDialog(cb, crrF);
                        } else {
                            customRightRoleUserEntries.remove(crrF.id);
                            }
                        }
                });
                flowPanel.add(createEditRoleRestrictionsButton(new IContinuation() {

                    @Override
                    public void doIt() {
                        showEditRoleRestrictionsSelectorDialog(cb, crrF);
                    }
                }));
                    } else {
                cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

                    @Override
                    public void onValueChange(ValueChangeEvent<Boolean> event) {
                        if (event.getValue()) {
                            customRightRoleUserEntries.put(crrF.id, SystemUserDialog.MARKER_FOR_CUSTOM_ROLES_WITHOUT_SELECTOR);
                        } else {
                        customRightRoleUserEntries.remove(crrF.id);
                        }
                    }
                });
                }
            flowPanel.setWidth("60px");
            boolean isRegularUserCrr = BaseUtils.safeEquals(crr.code, "RegularUser");
            cb.setVisible(!isRegularUserCrr);
            addRowToGrid(crr.caption, flowPanel, row++, isRegularUserCrr ? null : "customRightRole");
        }
        return row;
    }

    protected Map<Integer, Set<Pair<Integer, Integer>>> makeFlatCrrEntries() {
        Map<Integer, Set<Pair<Integer, Integer>>> res = new HashMap<Integer, Set<Pair<Integer, Integer>>>();
        for (Map.Entry<Integer, Set<TreeNodeBranchBean>> e : customRightRoleUserEntries.entrySet()) {
            Set<Pair<Integer, Integer>> ents = new HashSet<Pair<Integer, Integer>>();
                for (TreeNodeBranchBean tnbb : e.getValue()) {
                    ents.add(new Pair<Integer, Integer>(tnbb.treeId, tnbb.nodeId));
                }
                res.put(e.getKey(), ents);
            }
        return res;
    }
}
