/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.ConnectionParametersConfluenceBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class AdminConfigConfluenceWidget extends AdminConfigBIKBaseWidget<ConnectionParametersConfluenceBean> {

    protected TextBox tbxURL;
    protected TextBox tbxUser;
    protected TextBox tbxPassword;
    protected PushButton btnSave;
    protected CheckBox chbIsActive;
//    protected CheckBox chbPw;
    protected CheckBox chbCheckChildrenCount;

    @Override
    protected void buildInnerWidget() {
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
            }
        }, false);
        createLine();
        createLabel(I18n.serwer.get() + ":");
        tbxURL = createTextBox();
        createLabel(I18n.uzytkownik.get() + ":");
        tbxUser = createTextBox();
        createLabel(I18n.haslo.get() + ":");
        tbxPassword = createPasswordTextBox();
//        chbPw = createAndAddCheckBox(I18n.ukryjHaslo.get(), new ValueChangeHandler<Boolean>() {
//            @Override
//            public void onValueChange(ValueChangeEvent<Boolean> event) {
//                tbxPassword.getElement().setAttribute("type" /* I18N: no */, chbPw.getValue() ? "password" /* I18N: no */ : "text" /* I18N: no */);
//            }
//        });
        chbCheckChildrenCount = createAndAddCheckBoxWithOutHandler(I18n.sprawdzajLiczbePotomkow.get());
        createLine();
        btnSave = createButton(I18n.zapisz.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ConnectionParametersConfluenceBean bean = new ConnectionParametersConfluenceBean();
                bean.url = tbxURL.getText();
                bean.user = tbxUser.getText();
                bean.password = tbxPassword.getText();
                bean.isActive = chbIsActive.getValue() ? 1 : 0;
                bean.checkChildrenCount = chbCheckChildrenCount.getValue() ? 1 : 0;
                BIKClientSingletons.getService().setConfluenceConnectionParameters(bean, new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " setConfluenceConnectionParameters") {
                            @Override
                            public void onSuccess(Void result) {
                                BIKClientSingletons.showInfo(I18n.zmienionParametrDoPolaczenZConfluence.get() /* I18N:  */);
                            }
                        });
            }
        });
        createTestConnectionWidget(I18n.testujPolaczenie.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().runConfluenceTestConnection(new StandardAsyncCallback<Pair<Integer, String>>("Error in" /* I18N: no */ + " runConfluenceTestConnection") {
                            @Override
                            public void onSuccess(Pair<Integer, String> result) {
                                setResultTestConnection(result);
                            }
                        });
            }
        });
    }

    @Override
    public void populateWidgetWithData(ConnectionParametersConfluenceBean data) {
        tbxURL.setText(data.url);
        tbxUser.setText(data.user);
        tbxPassword.setText(data.password);
        chbCheckChildrenCount.setValue(data.checkChildrenCount == 1);
        chbIsActive.setValue(data.isActive == 1);
        setWidgetsEnabled(chbIsActive.getValue());
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_CONFLUENCE;
    }
}
