/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author mgraczkowski
 */
public class BIKGWTConstants extends BIKConstants {

    //ww: poniższe nie są używane, bo choć captiony BIKPages idą z modrzewia,
    // to jednak w XxxWidgetFlat są używane te stałe do captionów
    public static Integer DEFERRED_ID = 0;
    public static final String MENU_NAME_METADATA = I18n.metadane.get() /* I18N:  */;
    public static final String MENU_NAME_GLOSSARY = I18n.glosariusz.get() /* I18N:  */;
    public static final String MENU_NAME_GLOSSARY_DICTIONARY = I18n.slowniki.get() /* I18N:  */;
    public static final String MENU_NAME_TAXONOMY = I18n.kategoryzacje.get() /* I18N:  */;
    public static final String MENU_NAME_DOCUMENTS = I18n.dokumentacja.get() /* I18N:  */;
    public static final String MENU_NAME_BLOGS = I18n.blogi.get() /* I18N:  */;
    public static final String MENU_NAME_AUTHORS = I18n.autorzy.get() /* I18N:  */;
    public static final String MENU_NAME_COMMENTS = I18n.komentarze.get() /* I18N:  */;
    public static final String MENU_NAME_USER_COMMENTS = I18n.komentarzeUzytkownika.get() /* I18N:  */;
    public static final String MENU_NAME_USERS = I18n.spolecznosc.get() /* I18N:  */;
    public static final String MENU_NAME_USERS2 = I18n.uzytkownicy.get() /* I18N:  */;
    public static final String MENU_NAME_DQC = "DQC" /* I18N: no */;
    public static final String MENU_NAME_KNOWLEDGE = I18n.bazaWiedzy.get() /* I18N:  */;
    public static final String MENU_NAME_COLUMNS = I18n.kolumny.get() /* I18N:  */;
    public static final String MENU_NAME_INDICIES = I18n.indexy.get() /* I18N:  */;
    public static final String MENU_NAME_REQUESTS = I18n.wykonania.get() /* I18N:  */;
    public static final String MENU_NAME_CHART = I18n.wykresWykonan.get() /* I18N:  */;
    public static final String MENU_NAME_DATA_QUALITY = I18n.miary.get() /* I18N:  */;
    public static final String MENU_NAME_DESCENDANT_NODES = I18n.obiektyPodrzedne.get() /* I18N:  */;
    public static final String MENU_NAME_CONFLUENCE = I18n.confluenceName.get() /* I18N:  */;
    //---
    //ww: poniższe nie są używane, bo captiony BIKPages idą z modrzewia
//    public static final String MENU_NAME_SHORTCUTS_GLOSSARY = I18n.slownikSkrotow.get() /* I18N:  */;
//    public static final String MENU_NAME_BUSINESS_GLOSSARY = I18n.slownikPojecBiznesowych.get() /* I18N:  */;
//    public static final String MENU_NAME_MY_BIKS = I18n.mojBIKS.get() /* I18N:  */;
//    public static final String MENU_NAME_MONITOR = "Monitor" /* I18N: no */;
//    public static final String MENU_NAME_DATABASES = I18n.bazyDanych.get() /* I18N:  */;
//    public static final String MENU_NAME_SAP_BO = "SAP BO" /* i18n: no */;
//    public static final String MENU_NAME_TERADATA = "Teradata" /* I18N: no */;
//    public static final String MENU_NAME_MSSQL = "MS SQL" /* I18N: no */;
////    public static final String MENU_NAME_ARTICLES = "Artykuły";
//    public static final String MENU_NAME_ADMIN = "Admin" /* I18N: no */;
//    public static final String MENU_NAME_LOAD = I18n.zasilania.get() /* I18N:  */;
//    public static final String MENU_NAME_DICTIONARY = I18n.slowniki.get() /* I18N:  */;
//    public static final String MENU_NAME_STATISTICS = I18n.statystyki.get() /* I18N:  */;
//    public static final String MENU_NAME_SAS = "SAS" /* i18n: no */;
//    public static final String MENU_NAME_SAPBW = "SAP BW" /* i18n: no */;
    //---
    // i18n-default: no
    public static final String MENU_CODE_METADATA = "metadata";
//    public static final String TREE_CODE_REPORTS = "Reports";
//    public static final String TREE_CODE_OBJECT_UNIVERSES = "ObjectUniverses";
//    public static final String TREE_CODE_CONNECTIONS = "Connections";
    //---
    // i18n-default:
    public static final String USER_KIND_NORMAL_USER = I18n.uzytkownik.get() /* I18N:  */;
    public static final String USER_KIND_ADMIN = "Administrator" /* I18N: no */;
    //---
    // i18n-default: no
    public static final String TAB_ID_MYBIKS = "MyBIKS";
    public static final String BROWSER_TITLE = "BIKS";
    //---
//    public static final int LOG_LOAD_STATUS_DONE = 1;
//    public static final int LOG_LOAD_STATUS_ERROR = 2;
//    public static final int LOG_LOAD_STATUS_STARTED = 0;
    //---
    public static final String DIAG_MSG_KIND$WW_FILTER_TREE = "ww:filterTree";
    public static final String DIAG_MSG_KIND$WW_INIT_TREE_GRID = "ww:initTreeGrid";
    public static final String DIAG_MSG_KIND$WW_loadSubRowsOnExpand = "ww:loadSubRowsOnExpand";
    public static final String DIAG_MSG_KIND$WW_lazyLoad = "ww:lazyLoad";
    public static final String DIAG_MSG_KIND$WW_joinedObjs = "ww:joinedObjs";
    public static final String DIAG_MSG_KIND$WW_FoxyWezyrTreeGrid = "ww:FoxyWezyrTreeGrid";
    public static final String DIAG_MSG_KIND$WW_warn = "ww:warn";
    public static final String DIAG_MSG_KIND$WW_nodeExpanding = "ww:nodeExpanding";
    public static final String DIAG_MSG_KIND$error = "error";
    public static final String DIAG_MSG_KIND$buildDialog = "buildDialog";
    public static final String DIAG_MSG_KIND$SchedulePageBase = "SchedulePageBase";
    //---
//    public static final String SOURCE_NAME_SABBO = "Business Objects";
//    public static final String SOURCE_NAME_TERADATA = "Teradata DBMS";
//    public static final String SOURCE_NAME_DQC = "DQC";
    //---
    public static final String USER_KIND_CODE_AUTHOR = "Author";
    public static final String ROLE_CODE_EDITOR = "Editor";//redaktor
    public static final String ICON_DIR_PREFIX = "images/";
    public static final String ICON_DIR_POSTFIX = ".gif";
    public static final String DEFAULT_NODE_KIND_ICON_NAME = "bfolder";
    public static final String DEFAULT_NODE_KIND_ICON_lOCATION = ICON_DIR_PREFIX + DEFAULT_NODE_KIND_ICON_NAME + ICON_DIR_POSTFIX;
    public static String LANG_PL = "pl";
}
