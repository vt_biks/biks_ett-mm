/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import commonlib.LameUtils;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import pl.fovis.common.fts.SimilarWordsWithCounts;
import pl.trzy0.foxy.commons.IFoxyTransactionalPerformer;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.commons.NamedSqlCallParams;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public class FtsExecutor implements IFoxyTransactionalPerformer {

//    static {
//        LameUtils.tryReadAndApplyLameLoggersConfig("/lameLoggersConfig.properties");
//    }
    private static final boolean TRACK_PROCESSED_OBJS = false;
    protected static final String ADHOC_SQLS_PATH = LameUtils.getPackagePathOfClass(FtsExecutor.class) + "adhocSqlsFts.xml";
    private static final ILameLogger logger = LameUtils.getMyLogger();

    // workers
    protected WordsStorage storage;
//    protected TextSlicer slicer;
    protected ISlicer slicer;
    protected ObjectDataProvider dataProvider;
    protected INamedSqlsDAO<Object> adhocDao;

    public FtsExecutor(INamedSqlsDAO<Object> adhocDao) {
        initWorkers(adhocDao);
    }

    public FtsExecutor(MssqlConnection conn) {
        initWorkers(conn);
    }

    public FtsExecutor(MssqlConnectionConfig connCfg) {
        initWorkers(connCfg);
    }

    private void initWorkers(MssqlConnectionConfig connCfg) {
        initWorkers(new MssqlConnection(connCfg));
    }

    private void initWorkers(MssqlConnection conn) {
//        conn.setAutoCommit(true);
        initWorkers(FoxyAppUtils.createNamedSqlsDAOFromJar(conn, ADHOC_SQLS_PATH));
    }

    private void initWorkers(INamedSqlsDAO<Object> adhocDao) {
        this.adhocDao = adhocDao;
        storage = new WordsStorage(adhocDao, false);
//        slicer = new TextSlicer(storage);
        slicer = new FtsByLucene();
        dataProvider = new ObjectDataProvider(adhocDao);
    }

    public void processObjects(final boolean initStorage) {
        adhocDao.execInAutoCommitMode(new IContinuation() {

            @Override
            public void doIt() {
                if (logger.isStageEnabled()) {
                    logger.stage("processObjects: start (initStorage=" + initStorage + ")");
                }

                slicer.init();

                if (initStorage) {
                    storage.initStorage();
                    dataProvider.removeAllObjsFromProcessed();
                }

                if (logger.isStageEnabled()) {
                    logger.stage("processObjects: before getUnprocessedDataPack");
                }

                Map<Integer, Map<String, Object>> data = dataProvider.getUnprocessedDataPack(-1);
//                Map<Integer, Map<String, Object>> data = dataProvider.getUnprocessedDataPack(100000);

                final int objCntToProcess = data.size();

                if (logger.isStageEnabled()) {
                    logger.stage("processObjects: process data pack starts, full pack size=" + objCntToProcess);
                }

                int processedCnt = 0;
                long nanoStart = System.nanoTime();

                for (Entry<Integer, Map<String, Object>> e : data.entrySet()) {
                    int objId = e.getKey();
                    Map<String, Object> attrVals = e.getValue();
                    if (TRACK_PROCESSED_OBJS) {
                        dataProvider.startProcessingObj(objId);
                    }
                    slicer.addOrUpdateObject(objId, attrVals);
                    if (TRACK_PROCESSED_OBJS) {
                        dataProvider.markObjectAsProcessed(objId);
                    }
                    processedCnt++;

                    if (logger.isStageEnabled()) {
                        if (processedCnt % 500 == 0) {
                            long nanoCurrent = System.nanoTime();
                            double elapsedSecs = (nanoCurrent - nanoStart) / 1000000000.0;
                            final double speed = processedCnt / elapsedSecs;
                            logger.stage("processObjects: processed " + processedCnt + " objects, time taken: " + BaseUtils.doubleToString(elapsedSecs, 3) + " s, "
                                    + "speed: " + BaseUtils.doubleToString(speed, 3) + " obj / s, time remaining: "
                                    + BaseUtils.doubleToString(objCntToProcess / speed - elapsedSecs, 3) + " s, "
                                    + "unique chars used: " + TextSlicer.charUsage.getUniqueCount() + ", max char: " + (int) TextSlicer.maxChar);
                        }
                    }
                }

                slicer.close();

                if (logger.isStageEnabled()) {
                    logger.stage("processObjects: finished");
                }
            }
        });
    }

    public List<Pair<Integer, Double>> findSimilarObjects(int objId, double minRelevance, int maxResults) {
        return storage.findSimilarObjects(objId, new NamedSqlCallParams("fts_filterOutKnownSimilaritiesForObjClause", objId), minRelevance, maxResults);
    }

    public List<SimilarWordsWithCounts> getSimilarWordsWithCounts(int objId) {
        return storage.getSimilarWordsWithCounts(objId, new NamedSqlCallParams("fts_filterOutKnownSimilaritiesForObjClause", objId));
    }

    public List<SimilarWordsWithCounts> getSimilarWordsWithCountsForObjPair(int objId1, int objId2) {
        return storage.getSimilarWordsWithCounts(objId1, new NamedSqlCallParams("fts_filterSingleObjClause", objId2));
    }

    public Pair<List<Pair<Integer, Double>>, List<SimilarWordsWithCounts>> findSimilarObjectsAndWords(int objId, double minRelevance, int maxResults) {
        List<Pair<Integer, Double>> v1 = findSimilarObjects(objId, minRelevance, maxResults);
        List<SimilarWordsWithCounts> v2 = storage.getSimilarWordsWithCountsOnExistingSearchWords(objId, new NamedSqlCallParams("fts_filterOutKnownSimilaritiesForObjClause", objId));
        return new Pair<List<Pair<Integer, Double>>, List<SimilarWordsWithCounts>>(v1, v2);
    }

    public void changeWordWeight(String word, double weight) {
        storage.changeWordWeight(word, weight);
    }

    @Override
    public void finishWorkUnit(boolean success) {
        adhocDao.finishWorkUnit(success);
    }

    public static void main(String[] args) {

        LameUtils.tryReadAndApplyLameLoggersConfig(LameUtils.getPackagePathOfClass(FtsExecutor.class) + "lameLoggersConfig.properties");
//        LameUtils.tryReadAndApplyLameLoggersConfig("/lameLoggersConfig.properties");

//        MssqlConnectionConfig connCfg = new MssqlConnectionConfig("localhost", null, "PB-BIKS-TEST", "sa", "sasasa");
        MssqlConnectionConfig connCfg = new MssqlConnectionConfig("localhost", null, "BIKS_Polkomtel_2015", "sa", "sasasa");

        FtsExecutor exe = new FtsExecutor(connCfg);

        exe.processObjects(true);
//        System.out.println(exe.findSimilarObjects(113170, 10, 100));
    }
}
