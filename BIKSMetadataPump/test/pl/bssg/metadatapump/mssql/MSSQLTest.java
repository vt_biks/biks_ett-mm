/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.mssql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import pl.bssg.metadatapump.PumpUtils;
import static pl.bssg.metadatapump.PumpUtils.getConnection;

/**
 *
 * @author tflorczak
 */
public class MSSQLTest {

    public static void main(String[] args) throws SQLException {
        Connection conn = null;
        conn = getConnection("com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver://localhost:1433;databaseName=BIKS", "sa", "sasasa");
        Statement statment = conn.createStatement();
        ResultSet results = statment.executeQuery("select * from dbo.fn_split_by_sep('123,32,231', ',', 4)");
        int columnCount = results.getMetaData().getColumnCount();
        while (results.next()) {
            for (int i = 1; i <= columnCount; i++) {
                System.out.println(results.getMetaData().getColumnName(i) + " = " + results.getString(i));
            }
        }
        PumpUtils.closeConnection(conn);
    }
}
