/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class ThreadedJobExecutor implements Runnable {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    private BlockingQueue<Runnable> pendingJobs = new LinkedBlockingQueue<Runnable>();
    private AtomicInteger pendingJobCount = new AtomicInteger(0);
    private AtomicBoolean finishedRunning = new AtomicBoolean(false);

    public void addJob(Runnable job) {
        pendingJobCount.incrementAndGet();
        pendingJobs.add(job);
    }

    public void run() {
        try {
            while (true) {
                try {
                    Runnable job = pendingJobs.take();
                    try {
                        job.run();
                    } catch (Exception ex) {
                        if (ex instanceof InterruptedException) {
                            throw (InterruptedException) ex;
                        }

                        if (logger.isErrorEnabled()) {
                            logger.error("Error running job: " + job, ex);
                        }
                    }
                    pendingJobCount.decrementAndGet();

                    if (Thread.interrupted()) {
                        throw new InterruptedException("self checked");
                    }
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    break;
                }
            }
        } finally {
            finishedRunning.set(true);
        }
    }

    public int getPendingJobCount() {
        return pendingJobCount.get();
    }

    public boolean finishedRunning() {
        return finishedRunning.get();
    }

    public boolean isIdleOrFinished() {
        return getPendingJobCount() == 0 || finishedRunning();
    }
}
