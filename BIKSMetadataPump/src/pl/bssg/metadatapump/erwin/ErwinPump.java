/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.erwin;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.common.PumpConstants;
import simplelib.Pair;

/**
 *
 * @author pgajda
 */
public class ErwinPump extends MetadataPumpBase {

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_ERWIN_DATAMODEL;
    }

    @Override
    protected Pair<Integer, String> startPump() {

        //String fileName = "c:\\moje_dokumenty\\polkomtel\\Erwin\\modele\\DWH_DM_EWin7.3.11_v13.04_20130705_1_standard.xml";
        //String fileName = "c:\\moje_dokumenty\\polkomtel\\Erwin\\Moje_Testy\\xml\\xml_01.xml";
        //String fileName = "c:\\instalki\\Erwin\\testy\\xml_01_komentarz.xml";
        String fileName = execNamedQuerySingleVal("getConfigValue", false, "value", "erwin.path");

        Date beg_date = new Date();
        ErwinStaXParser esp = new ErwinStaXParser(fileName);
        esp.readObject();
        Date beg1_date = new Date();
        System.out.println("CZAS PARSOWANIA:" + (beg1_date.getTime() - beg_date.getTime()) / 1000.0 + " s");

        execNamedCommandWithCommit("deleteErwinStructures");

        List<ErwinTable> erwinTableList = esp.getErwinTableList();
        modifyErwinTableList(erwinTableList);

        insertListToDBInPackages(erwinTableList, "insertErwinTables");

        ArrayList<HashMap<String, String>> filterList = new ArrayList<HashMap<String, String>>();
        filterList.add(new HashMap<String, String>() {
            {
                put("Owner_Path", ".Fizyczny");
            }
        }
        );

        insertListToDBInPackages(filterList(esp.getErwinShapeList(), "Anchor_Point", filterList), "insertErwinShapes");
        insertListToDBInPackages(esp.getErwinTableColumnList(), "insertErwinTableColumns");
//        insertListToDBInPackages(filterList(esp.getErwinShapeList(), "Parent_Connector_Point"), "insertErwinRelationShapes");

        insertListToDBInPackages(esp.getErwinRelationshipList(), "insertErwinRelationships");
        insertListToDBInPackages(esp.getErwinKeyGroupList(), "insertErwinKeyGroups");
        insertListToDBInPackages(esp.getErwinKeyGroupMemberList(), "insertErwinKeyGroupMembers");

        List<ErwinSubjectArea> erwinSubjectAreaList = esp.getErwinSubjectAreaList();
        erwinSubjectAreaList = erwinSubjectAreaList.subList(1, erwinSubjectAreaList.size());
        modifySubjectAreaList(erwinSubjectAreaList);
        if (erwinSubjectAreaList != null && erwinSubjectAreaList.size() > 1) {
            insertListToDBInPackages(erwinSubjectAreaList, "insertErwinSubjectAreaList");
            insertListToDBInPackages(erwinSubjectAreaList, "insertErwinSubjectAreaObjectRef");
        }

        return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
    }

    public <T extends ErwinXMLElement> List<T> filterList(List<T> list, String dropNullKey, List<HashMap<String, String>> dropKeyWithPattern) {
        List<T> tmpList = new ArrayList<T>();
        //List<ErwinShape>
        for (T el : list) {
            if (el.getPropertieValue(dropNullKey) == null) {
                continue;
            }

            boolean addElement = true;
            if (dropKeyWithPattern != null) {
                Iterator<HashMap<String, String>> it = dropKeyWithPattern.iterator();
                while (it.hasNext() && addElement) {
                    Set<Entry<String, String>> set = it.next().entrySet();

                    Iterator<Entry<String, String>> itInner = set.iterator();
                    while (itInner.hasNext() && addElement) {
                        Entry<String, String> entry = itInner.next();

                        Object val = el.getPropertieValue(entry.getKey());
                        if (val != null && val instanceof String) {
                            //System.out.println("val: "+val);
                            if (((String) val).contains(entry.getValue())) {
                                addElement = false;
                                break;
                            }
                        }
                    }
                }
            }

            if (addElement) {
                tmpList.add(el);
            }
        }

        return tmpList;
    }

    protected void modifySubjectAreaList(List<ErwinSubjectArea> list) {

        for (ErwinSubjectArea el : list) {
            ArrayList<String> names = el.getPropertieValue("Name");
            ArrayList<String> namesTmp = new ArrayList<String>();
            for (String name : names) {
                if (name != null) {
                    name = name.replaceFirst("^([0-9]+_)", "");
                    namesTmp.add(name);
                }
            }
            el.setPropertieValues("Name", namesTmp);
        }
    }

    protected void modifyErwinTableList(List<ErwinTable> list) {

        Pattern pattern = Pattern.compile("^([0-9]+)_.*");
        Matcher matcher = null;

        for (ErwinTable el : list) {
            String name = el.getPropertieValue("Name");
            if (name != null) {
                matcher = pattern.matcher(name);
                if (matcher.matches()) {
                    name = "T" + name;
                }
                name = name.replace(' ', '_');
            }

            el.setPropertieValues("Name", name);
        }
    }

//    public String getPumpGroup() {
//        return PumpConstants.PUMP_GROUP_ERWIN;
//    }
}
