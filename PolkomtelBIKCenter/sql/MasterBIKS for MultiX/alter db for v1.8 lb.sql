------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- 1. updt procki zbierajacej dane, usunieto nieaktualne tabele
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

------------------------------------------------------

exec sp_mltx_check_version '1.8'
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getMultixStatisticsData]') AND type in (N'P'))
drop procedure [dbo].[sp_getMultixStatisticsData];
go

create procedure [dbo].[sp_getMultixStatisticsData] as
begin
  set nocount on
  declare mltxdbs cursor for select id, database_name from mltx_database;
  open mltxdbs;
  declare @dbname varchar(512);
  declare @dbid int;
  declare @sql nvarchar(max);
  
  -- constant event / action token types
  declare @evtUserLogin varchar(32); 
  declare @evtLastActivity varchar(32);
  declare @evtTotalInvited varchar(32);
  declare @tknInvited varchar(32);
  set @evtUserLogin = '''userLogin''';
  set @evtLastActivity = '''lastActivity''';
  set @evtTotalInvited = '''totalInvited''';
  set @tknInvited = '''invite''';
  -- constant event / action token types - end
  
  fetch next from mltxdbs into @dbid, @dbname
	while @@fetch_status = 0 begin
		set @sql = 'insert into mltx_statistic (db_id, event_datetime, action_count, event_type) ';
		
		-- usuwamy stare dane statystyczne
		delete from mltx_statistic where db_id = @dbid;
		
			   set @sql = @sql + 'select x.dbid, x.event_datetime, x.action_count, x.event_type from ('  
			   set @sql = @sql + 'select ' + cast(@dbid as varchar(100)) + ' as dbid, ';
			   set @sql = @sql + 's.* from  (';
				   -- zalogowanych
				   set @sql = @sql + 'select MAX(event_datetime) as event_datetime, count(*) as action_count, '+@evtUserLogin+' as event_type from '+@dbname+'.dbo.bik_statistic nolock where id in (select max(id) from '+@dbname+'.dbo.bik_statistic where counter_name = '+@evtUserLogin+' group by user_id)'; 
				   set @sql = @sql + ' union '; 	
				   -- ostatnia aktywnosc
				   set @sql = @sql + 'select MAX(event_datetime) as event_datetime, null as action_count, '+@evtLastActivity+' as event_type from '+@dbname+'.dbo.bik_statistic nolock'; 
				   set @sql = @sql + ' union '; 	
				   -- zaproszonych
				   set @sql = @sql + 'select max(t.created_at) as event_datetime, count(*) as action_count, '+@evtTotalInvited+' as event_type from (select tkn.created_at, l.database_id from mltx_action_token tkn left join mltx_invitation inv on tkn.id = inv.token_id left join mltx_login_database l on l.login = tkn.email where tkn.action_type = '+@tknInvited+' and l.database_id = '+cast(@dbid as varchar(100))+') as t group by t.database_id';
			   set @sql = @sql + ') as s) as x;';
	    EXECUTE sp_executesql @sql; 
		-- kontrolne wypisywanie sqli dla danej bazy
	    --select @sql;
	    fetch next from mltxdbs into @dbid,@dbname
	    end;
		close mltxdbs;
		deallocate mltxdbs;
	end
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_mltx_update_version '1.8', '1.9'
go