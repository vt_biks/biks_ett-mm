/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class DBForeignKeyBean implements Serializable {

    public String name;
    public String description;
    public String fkTable;
    public String fkOwner;
    public String fkColumn;
    public String pkTable;
    public String pkSchema;
    public String pkColumn;
    public String pkTreeCode;
    public int pkTableNodeId;
    public int pkColumnNodeId;

}
