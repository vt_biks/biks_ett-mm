/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.reportsdk;

/**
 *
 * @author tflorczak
 */
public class ReportQueryBean {

    public int id;
    public String name;
    public String idText;
    public String filtrText;
    public String SQLText;
    public String universeCUID;
    public String universeName;
    public int reportNodeId;
    public int reportSiId;

    public ReportQueryBean(int id, String name, String idText, String filtrText, String SQLText, String universeCUID, String universeName, int reportNodeId, int reportSiId) {
        this.id = id;
        this.name = name;
        this.idText = idText;
        this.filtrText = filtrText;
        this.SQLText = SQLText;
        this.universeCUID = universeCUID;
        this.universeName = universeName;
        this.reportNodeId = reportNodeId;
        this.reportSiId = reportSiId;
    }
}
