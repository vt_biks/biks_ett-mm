/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public class StringMapHelper extends MapHelper<String> implements INamedPropsBean {

    public StringMapHelper(Map<String, Object> map) {
        super(map);
    }

    public StringMapHelper(IMapHelperWrapper<String> wrapper) {
        super(wrapper);
    }

    public Object getPropValue(String propName) {
        return get(propName);
    }
}
