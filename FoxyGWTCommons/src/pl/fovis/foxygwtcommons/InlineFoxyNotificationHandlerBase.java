/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

/**
 *
 * @author pmielanczuk
 */
public abstract class InlineFoxyNotificationHandlerBase implements IFoxyNotificationHandler {

    protected abstract void showMsg(boolean isWarning, String msg);

    public void showInfo(String msg) {
        showMsg(false, msg);
    }

    public void showWarning(String err) {
        showMsg(true, err);
    }

    public boolean isInfoModal() {
        return false;
    }

    public boolean isWarningModal() {
        return false;
    }
}
