/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.serverlogic;

import pl.trzy0.foxy.serverlogic.db.PostgresConnection;
import simplelib.IBeanAction;
import simplelib.INamedPropsBean;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author wezyr
 */
public class DatabaseBeanSaver implements IBeanAction<INamedPropsBean> {

    private PostgresConnection connection;

    public DatabaseBeanSaver(PostgresConnection connection) {
        this.connection = connection;
    }

    public void perform(INamedPropsBean bean, IParametrizedContinuation<String> whenDone) {
        //...
    }
}
