/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import pl.trzy0.foxy.commons.StandardDBConnectionConfig;

/**
 *
 * @author tflorczak
 */
public class OracleConnectionConfig extends StandardDBConnectionConfig {

    public static final int DEFAULT_PORT = 1521;
    public String instance;
    public Integer port;
    public int poolSize = 30;
    public int timeout = 10;

    public OracleConnectionConfig() {
    }

    public OracleConnectionConfig(String server, String instance, String user, String password, Integer port) {
        this.server = server;
        this.instance = instance;
        this.user = user;
        this.password = password;
        this.port = port;
    }

    public OracleConnectionConfig(String server, String instance, String user, String password, Integer port, String defaultDatabase) {
        this(server, instance, user, password, port);
        this.database = defaultDatabase;
    }
}
