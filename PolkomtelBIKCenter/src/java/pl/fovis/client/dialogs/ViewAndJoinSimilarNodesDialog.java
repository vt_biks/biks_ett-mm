/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable.RowFormatter;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.fts.SimilarWordsWithCounts;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.EntityDetailsPaneForDialog;
import pl.fovis.client.bikwidgets.FtsSimilarWordsContainer;
import pl.fovis.client.bikwidgets.NodeNamePathsAsynchFetcher;
import pl.fovis.common.JoinedObjMode;
import pl.fovis.common.SimilarNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.VisualMetricsUtils;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import static pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog.isShowDiagDialogSizingEnabled;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public class ViewAndJoinSimilarNodesDialog extends BaseActionOrCancelDialog {

    public static final String DIAG_MSG_KIND$SIMILAR_NODES = "SimilarNodes";

    protected static final int TOP_SIMILAR_WORDS_BOX_HEIGHT = 100;
    protected static final int OUTSIZE_INNER_CONTENT_HEIGHT = 100;

//    protected final FlowPanel fpWords = new FlowPanel();
//    protected final FlowPanel fpNodeWords = new FlowPanel();
    protected final IContinuation wordWeightChangedCont = new IContinuation() {

        @Override
        public void doIt() {
            fetchAndShowData();
        }
    };
    protected final IContinuation wordListChangedCont = new IContinuation() {

        @Override
        public void doIt() {
            resizeAfterWordListChanged();
        }
    };
    protected final FtsSimilarWordsContainer fpWords = new FtsSimilarWordsContainer(wordWeightChangedCont, wordListChangedCont);
    protected final FtsSimilarWordsContainer fpNodeWords = new FtsSimilarWordsContainer(wordWeightChangedCont, wordListChangedCont);
    protected final HorizontalPanel hpMain = new HorizontalPanel();
    protected final FlexTable ftSimilarNodes = new FlexTable();
    protected final RowFormatter similarNodesRowFormatter = ftSimilarNodes.getRowFormatter();
    protected final ScrollPanel spLeft = new ScrollPanel(ftSimilarNodes);
    protected final VerticalPanel vpRight = new VerticalPanel();
    protected final EntityDetailsPaneForDialog edp4d = new EntityDetailsPaneForDialog(null, null, null);
    protected IContinuation cont;
    protected int objId;
    protected String nodeName;
    protected String nodeKindCaption;
    protected Integer selectedNodeId;
    protected Map<Integer, Integer> nodeIdToRowIdxMap = new HashMap<Integer, Integer>();
    protected final NodeNamePathsAsynchFetcher nnpaf = new NodeNamePathsAsynchFetcher(null);
    protected int mainHeight;
    protected final Set<Integer> selectedNodeIds = new HashSet<Integer>();

    @Override
    protected String getDialogCaption() {
        return I18n.podobneObiektyDla.get() + " [" + nodeKindCaption + ": " + nodeName + "]";
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("buildMainWidgets: start");
        }

        Pair<Integer, Integer> sizes = getSizeForScrollPanel();

        mainHeight = sizes.v2 - 30;

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("buildMainWidgets: Before setting main sized, requested height=" + mainHeight);
        }

        //setScrollPanelSize(main, mainHeight /*???? OUTSIZE_INNER_CONTENT_HEIGHT*/);
        main.setSize(sizes.v1 + "px", mainHeight + "px");

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("buildMainWidgets: after setting main sizes");
        }

        ftSimilarNodes.setStyleName("fts_similarNodes");
//        ftSimilarNodes.setSize("100%", "100%");
        ftSimilarNodes.setWidth("100%");

        spLeft.setStyleName("fts_spLeft");
        //spLeft.setSize("400px", (getSimilarWordsBoxHeight() + OUTSIZE_INNER_CONTENT_HEIGHT) + "px");
        //spLeft.setSize("100%", "100%");
        //final int innerBodyHeight = getSizeForScrollPanel().v2 - getSimilarWordsBoxHeight() - OUTSIZE_INNER_CONTENT_HEIGHT;

//        hpMain.setSize("100%", innerBodyHeight + "px");
//        hpMain.setSize("100%", "100%");
        hpMain.setWidth("100%");

        hpMain.add(spLeft);

//        vpRight.setSize("100%", "100%");
        vpRight.setWidth("100%");

//        edp4d.setWidgetHeight(innerBodyHeight);
//        edp4d.setWidgetHeight("400px");
        edp4d.setWidgetHeight(200); // inicjalnie tyle, żeby nam nie rozjechało na wstępie wysokości

        vpRight.add(edp4d.getWidget());
        vpRight.add(fpNodeWords);
        hpMain.add(vpRight);

        hpMain.setCellWidth(spLeft, "400px");

        main.add(fpWords);
        main.add(hpMain);

//        main.setCellHeight(hpMain, "100%");
        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("buildMainWidgets: end");
        }

        actionBtn.setEnabled(false);
    }

    @Override
    protected void doAction() {

        BIKClientSingletons.showInfo(I18n.zapisywaniePowiazanProszeCzekac.get());

        Map<Integer, JoinedObjMode> idsToJoin = BaseUtils.makeHashMapOfSize(selectedNodeIds.size());

        for (Integer id : selectedNodeIds) {
            idsToJoin.put(id, JoinedObjMode.Single);
        }

        BIKClientSingletons.getService().updateJoinedObjsForNode(objId, false, idsToJoin, new HashMap<Integer, Pair<JoinedObjMode, Boolean>>(), new StandardAsyncCallback<Void>() {

            @Override
            public void onSuccess(Void result) {

                BIKClientSingletons.showInfo(I18n.powiazaniaZapisaneOdswiezanie.get());

                if (cont != null) {
                    cont.doIt();
                }
            }
        });
    }

    protected void setNodeIdToCompareTo(int nodeId) {

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("setNodeIdToCompareTo: start");
        }

        Integer rowIdx = nodeIdToRowIdxMap.get(selectedNodeId);
        if (rowIdx != null) {
            similarNodesRowFormatter.removeStyleName(rowIdx, "fts_selectedNode");
        }

        selectedNodeId = nodeId;

        rowIdx = nodeIdToRowIdxMap.get(selectedNodeId);
        if (rowIdx != null) {
            similarNodesRowFormatter.addStyleName(rowIdx, "fts_selectedNode");
        }

//        vpRight.clear();
//        vpRight.add(edp4d.getWidget());
//        vpRight.add(fpNodeWords);
        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("setNodeIdToCompareTo: before edp4d.setNodeId");
        }
        edp4d.setNodeIdAndFetchData(nodeId, null);
        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("setNodeIdToCompareTo: after edp4d.setNodeId");
        }

        BIKClientSingletons.getFtsService().getSimilarWordsWithCountsForObjPair(objId, nodeId, new StandardAsyncCallback<List<SimilarWordsWithCounts>>() {

            @Override
            public void onSuccess(List<SimilarWordsWithCounts> result) {
                //addCommonWords(fpNodeWords, result);
                if (isShowDiagDialogSizingEnabled()) {
                    showDiagDialogSizingMsg("setNodeIdToCompareTo: before fpNodeWords.setData");
                }

                fpNodeWords.setData(result);

                setProperEdpHeight();
            }
        });

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("setNodeIdToCompareTo: end");
        }
    }

    protected void setProperEdpHeight() {
        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("setProperEdpHeight: start");
        }
        int hpMainHeight = calcHpMainHeight();
        final int fpNodeWordsHeight = VisualMetricsUtils.getHeight(fpNodeWords.asWidget());

        final int edp4dHeight = hpMainHeight - fpNodeWordsHeight - 6; // 6 magiczne

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("setNodeIdToCompareTo: edp4dHeight=" + edp4dHeight
                    + ", hpMainHeight=" + hpMainHeight
                    + ", fpNodeWordsHeight=" + fpNodeWordsHeight);
        }

        edp4d.setWidgetHeight(edp4dHeight + "px");
        edp4d.getWidget().setHeight(edp4dHeight + "px");
        edp4d.setHeightBodyEdp();

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("setProperEdpHeight: end");
        }
    }

    public static void diagMsg(String msg) {
        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SIMILAR_NODES)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SIMILAR_NODES, msg);
        }
    }

    protected int calcHpMainHeight() {
        int fpWordsHeight = VisualMetricsUtils.getHeight(fpWords.asWidget());

        final int res = mainHeight - 30 - fpWordsHeight - 8; // 8 jest magiczne (paddingi i te sprawy)

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("calcHpMainHeight: res=" + res);
        }

        return res;
    }

    protected void showData(Pair<List<SimilarNodeBean>, List<SimilarWordsWithCounts>> data) {

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("showData: start");
        }

        //addCommonWords(fpWords, data.v2);
        fpWords.setData(data.v2);

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("showData: after fpWords.setData");
        }

        int fpWordsHeight = VisualMetricsUtils.getHeight(fpWords.asWidget());
//        int mainHeight = VisualMetricsUtils.getHeight(main);
//        spLeft.setHeight((mainHeight - 30 - fpWordsHeight) + "px");

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("showData: before setting spLeft.height, fpWordsHeight: " + fpWordsHeight + ", measured mainHeight: " + mainHeight);
        }

//        final String hpMainHeightStr = (mainHeight - 30 - fpWordsHeight) + "px";
        setProperHpMainHeight();

        ftSimilarNodes.removeAllRows();

        List<SimilarNodeBean> snbs = data.v1;

//        boolean clearSelectedNodeId = true;
        nodeIdToRowIdxMap.clear();

        Integer nodeIdToSelect = selectedNodeId;
        selectedNodeId = null;

        for (int i = 0; i < snbs.size(); i++) {
            final SimilarNodeBean snb = snbs.get(i);

            final Label lblName = new Label(snb.name);

            final int snbId = snb.id;

            nnpaf.addIdWithName(snbId, snb.name);

            nnpaf.setOrCollect(snb.branchIds, new IParametrizedContinuation<Pair<String, String>>() {

                @Override
                public void doIt(Pair<String, String> param) {
                    lblName.setTitle(BIKClientSingletons.getMenuPathForTreeId(snb.treeId) + param.v2);
                }
            });

//            if (i % 20 == 0) {
//                nnpaf.fetchAndSetCollected();
//            }
            nodeIdToRowIdxMap.put(snbId, i);

            lblName.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    setNodeIdToCompareTo(snbId);
                }
            });

            int col = 0;
            final CheckBox cbNode = new CheckBox();
            cbNode.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    BaseUtils.setAddOrRemoveItem(selectedNodeIds, snbId, cbNode.getValue());
                    actionBtn.setEnabled(!selectedNodeIds.isEmpty());
                }
            });

            ftSimilarNodes.setWidget(i, col++, cbNode);

            final String nodeKindCode = BIKClientSingletons.getNodeKindCodeById(snb.nodeKindId);

            String iconName = "images/" + BIKClientSingletons.getNodeKindIconNameByCode(nodeKindCode,
                    BIKClientSingletons.getTreeCodeById(snb.treeId)) + ".gif";
            ftSimilarNodes.getCellFormatter().setWidth(i, col, "20px");
            ftSimilarNodes.setWidget(i, col++, BIKClientSingletons.createIconImg(iconName, "16px", "16px",
                    BIKClientSingletons.getNodeKindCaptionByCode(nodeKindCode)));
            ftSimilarNodes.setWidget(i, col++, lblName);
            ftSimilarNodes.setText(i, col, BaseUtils.doubleToString(snb.relevance, 2));
            ftSimilarNodes.getCellFormatter().setHorizontalAlignment(i, col++, HasHorizontalAlignment.ALIGN_RIGHT);

            similarNodesRowFormatter.setStyleName(i, "fts_similarNodes_row_" + (i % 2 == 0 ? "even" : "odd"));
        }

        nnpaf.fetchAndSetCollected();

        if (!nodeIdToRowIdxMap.containsKey(nodeIdToSelect)) {
            nodeIdToSelect = null;
        }

        if (nodeIdToSelect == null && !snbs.isEmpty()) {
            nodeIdToSelect = snbs.get(0).id;
        }

        if (nodeIdToSelect != null) {
            setNodeIdToCompareTo(nodeIdToSelect);
        }

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("showData: end");
        }
    }

    protected void fetchAndShowData() {
        diagMsg("fetchAndShowData");

        BIKClientSingletons.getFtsService().findSimilarObjectsAndWords(objId, 0.1, 50, new StandardAsyncCallback<Pair<List<SimilarNodeBean>, List<SimilarWordsWithCounts>>>() {

            @Override
            public void onSuccess(Pair<List<SimilarNodeBean>, List<SimilarWordsWithCounts>> result) {
                diagMsg("before show data");
                showData(result);
                diagMsg("after show data");
            }
        });

        diagMsg("fetchAndShowData request submitted");
    }

    public void buildAndShowDialog(int objId, String nodeName, String nodeKindCaption, IContinuation cont) {
        this.cont = cont;
        this.objId = objId;
        this.nodeName = nodeName;
        this.nodeKindCaption = nodeKindCaption;

        fetchAndShowData();

        super.buildAndShowDialog();
    }

    @Override
    protected void firstShowCentered() {
        super.firstShowCentered(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void afterBuildAllWidgetsBeforeShow() {
        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("afterBuildAllWidgetsBeforeShow: start");
        }

        super.afterBuildAllWidgetsBeforeShow();

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("afterBuildAllWidgetsBeforeShow: end");
        }
    }

    protected void resizeAfterWordListChanged() {

        setProperHpMainHeight();
        setProperEdpHeight();
    }

    protected void setProperHpMainHeight() {
        final String hpMainHeightStr = calcHpMainHeight() + "px";

        hpMain.setHeight(hpMainHeightStr);
        spLeft.setHeight(hpMainHeightStr);

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsg("setProperHpMainHeight: after setting spLeft.height, prev mainHeight: " + mainHeight + ", hpMainHeightStr: " + hpMainHeightStr);
        }
    }
}
