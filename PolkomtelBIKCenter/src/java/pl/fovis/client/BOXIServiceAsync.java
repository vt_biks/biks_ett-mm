/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;
import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import pl.bssg.metadatapump.common.ConnectionParametersDynamicAxBean;
import pl.bssg.metadatapump.common.ConnectionParametersFileSystemBean;
import pl.bssg.metadatapump.common.ConnectionParametersJdbcBean;
import pl.bssg.metadatapump.common.ConnectionParametersPlainFileBean;
import pl.bssg.metadatapump.common.MsSqlExPropConfigBean;
import pl.bssg.metadatapump.common.SAPBOObjectBean;
import pl.bssg.metadatapump.common.SAPBOScheduleBean;
import pl.bssg.metadatapump.common.SchedulePumpBean;
import pl.fovis.client.dialogs.ImportTreeLogDialog.TreeImportLogStatus;
import pl.fovis.common.AppPropBean;
import pl.fovis.common.ArticleBean;
import pl.fovis.common.AttachmentBean;
import pl.fovis.common.AttrHintBean;
import pl.fovis.common.AttrSearchableBean;
import pl.fovis.common.AttributeAndTheirKindsBean;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.AttributeValueInNodeBean;
import pl.fovis.common.AuditBean;
import pl.fovis.common.BIAArchiveLogBean;
import pl.fovis.common.BIArchiveConfigBean;
import pl.fovis.common.BISystemsConnectionsParametersBean;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.BikRoleForNodeBean;
import pl.fovis.common.BiksSearchResult;
import pl.fovis.common.BlogBean;
import pl.fovis.common.ChangedRankingBean;
import pl.fovis.common.ConnectionParametersADBean;
import pl.fovis.common.ConnectionParametersAuditBean;
import pl.fovis.common.ConnectionParametersBIAdminBean;
import pl.fovis.common.ConnectionParametersConfluenceBean;
import pl.fovis.common.ConnectionParametersDQCBean;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.fovis.common.ConnectionParametersErwinBean;
import pl.fovis.common.ConnectionParametersProfileBean;
import pl.fovis.common.ConnectionParametersSASBean;
import pl.fovis.common.ConnectionParametersSapBWBean;
import pl.fovis.common.ConnectionParametersSapBoBean;
import pl.fovis.common.ConnectionParametersTeradataBean;
import pl.fovis.common.DataSourceStatusBean;
import pl.fovis.common.DroolsConfigBean;
import pl.fovis.common.DroolsLogBean;
import pl.fovis.common.DynamicTreesBigBean;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.ErwinDataModelProcessObjectsRel;
import pl.fovis.common.ErwinDiagramDataBean;
import pl.fovis.common.FvsChangeExBean;
import pl.fovis.common.HelpBean;
import pl.fovis.common.HomePageBean;
import pl.fovis.common.JoinTypeRoles;
import pl.fovis.common.JoinedObjAttributeLinkedBean;
import pl.fovis.common.JoinedObjBean;
import pl.fovis.common.JoinedObjMode;
import pl.fovis.common.MailLogBean;
import pl.fovis.common.MyObjectsBean;
import pl.fovis.common.NameDescBean;
import pl.fovis.common.NewDefinitionDataBean;
import pl.fovis.common.NewsBean;
import pl.fovis.common.NodeAuthorBean;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.NodeModificationLogEntryBean;
import pl.fovis.common.NoteBean;
import pl.fovis.common.ObjectInFvsChangeBean;
import pl.fovis.common.ObjectInFvsChangeExBean;
import pl.fovis.common.ObjectInHistoryBean;
import pl.fovis.common.ParametersAnySqlBean;
import pl.fovis.common.PrintTemplateBean;
import pl.fovis.common.RightRoleBean;
import pl.fovis.common.RoleForNodeBean;
import pl.fovis.common.SapBoUniverseColumnBean;
import pl.fovis.common.SearchFullResult;
import pl.fovis.common.SearchFullResult.IndexCrawlStatus;
import pl.fovis.common.StatisticsBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeKindBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.TreeNodeBranchBean;
import pl.fovis.common.TutorialBean;
import pl.fovis.common.UserBean;
import pl.fovis.common.UserExtBean;
import pl.fovis.common.UserExtradataBean;
import pl.fovis.common.UserInNodeBean;
import pl.fovis.common.UserObjectBean;
import pl.fovis.common.VoteBean;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import simplelib.BeanWithIntIdAndNameBase;
import simplelib.LameRuntimeException;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public interface BOXIServiceAsync {

    public void getBikEntityDetailsData(int nodeId, Integer linkingParentId, boolean fetchExtraData, AsyncCallback<EntityDetailsDataBean> AsyncCallback);

    public void performUserLogout(AsyncCallback<Void> AsyncCallback);

    public void performUserLogin(String login, String password, String domain, AsyncCallback<Pair<String, Boolean>> AsyncCallback);

//    public void performBikUserLogin(String login, String pwd, AsyncCallback<SystemUserBean> AsyncCallback);
//
//    public void performUserLoginBySSO(AsyncCallback<Void> AsyncCallback);
//
//    public void performUserLoginBySSOLoginAndDomain(String login, String password, String domain, AsyncCallback<SystemUserBean> AsyncCallback);
//    public void getLoggedUserName(AsyncCallback<String> AsyncCallback);
//    public void getLoggedUserBean(AsyncCallback<SystemUserBean> AsyncCallback);
    public void addToFvs(List<Integer> nodeId, AsyncCallback<Boolean> AsyncCallback);

    public void removeFromFvs(int nodeId, AsyncCallback<Boolean> AsyncCallback);

    public Request getFavouritesForUser(AsyncCallback<List<ObjectInFvsChangeBean>> AsyncCallback);

    public void addToHist(int nodeId, AsyncCallback<Void> AsyncCallback);

    public Request getFromHistory(AsyncCallback<List<ObjectInHistoryBean>> AsyncCallback);

    public void createTreeNode(Integer parentNodeId, int nodeKindId, String name, String objId, int treeId, Integer linkedNodeId, AsyncCallback<Pair<Integer, List<String>>> asyncCallback);

    public void createTreeNodeWithAttr(Integer parentNodeId, int nodeKindId, String name, String objId, int treeId, Integer linkedNodeId, Set<Integer> addingJoinedObjs, Map<String, AttributeBean> requiredAttrs, AsyncCallback<Pair<Integer, List<String>>> standardAsyncCallback);

    public void updateTreeNode(TreeNodeBean node, AsyncCallback<List<String>> AsyncCallback);

    public void updateTreeNodeAndChangeFvs(final TreeNodeBean node, AsyncCallback<List<String>> asyncCallback);

    public Request getTrimmedTree(int treeId, AsyncCallback<List<TreeNodeBean>> AsyncCallback);

    public void getTreeNodes(int treeId, AsyncCallback<List<TreeNodeBean>> AsyncCallback);

    public void deleteTreeNode(TreeNodeBean node, AsyncCallback<List<String>> AsyncCallback) throws LameRuntimeException;

    public void addBikEntityAttribute(String name, String value, int siId, AsyncCallback<Void> AsyncCallback);

    public void addBikEntityAttrAssociateRoleWithAnObj(String name, String value, int siId, AsyncCallback<Void> AsyncCallback);

    public void getBikAllUsers(AsyncCallback<List<UserBean>> asyncCallback);

    public void addBikUser(String name, String mail, String phone, String desc, int nodeId, String loginAD, AsyncCallback<Void> AsyncCallback);

    public void addBikEntityNote(String title, String body, int nodeId, AsyncCallback<Void> AsyncCallback);

    public void updateBikUser(int id, String oldLoginName, String loginName, String name, String password, String mail, String phone, String desc, AsyncCallback<List<String>> AsyncCallback);

    public void editBikBlogEntry(int id, String content, String subject, int nodeId, AsyncCallback<List<String>> AsyncCallback);

    public void addBikEntry(int cuId, int id, boolean inheritToDescendants, AsyncCallback<Void> AsyncCallback);

    public void deleteBikLinkedEntry(int cuId, int id, AsyncCallback<Void> AsyncCallback);

    public void getBikBizDefForNodeId(int siId, AsyncCallback<ArticleBean> AsyncCallback);

//    public void getBikArticle(int siId, AsyncCallback<List<JoinedObjBean>> AsyncCallback);
    public void addBikArticle(String title, String body, Set<String> tags, int official, int node, String version, AsyncCallback<Void> AsyncCallback);

    public void deleteBikArticle(int id, int nodeId, AsyncCallback<List<String>> AsyncCallback);

    public void deleteBikNote(int id, AsyncCallback<Void> AsyncCallback);

    public void editBikEntityNote(int id, String title, String body, AsyncCallback<Void> AsyncCallback);

    public void deleteBikLinkedMeta(Integer cuId, int id, AsyncCallback<Void> AsyncCallback);

    public void deleteBikLinkedAtr(Integer cuId, int id, String value, AsyncCallback<Void> AsyncCallback);

    public void editBikEntityAttribute(int id, int cuId, String atrLinValue, AsyncCallback<Void> AsyncCallback);

    public void editBikEntityAttrAssociateRoleWithAnObj(int id, int cuId, String atrLinValue, List<String> rightRolesBefore, AsyncCallback<List<String>> AsyncCallback);

    public Request getBikDataSourceStatuses(int pageNum, int pageSize, AsyncCallback<Pair<Integer, List<DataSourceStatusBean>>> AsyncCallback);

    public void getBikAllTreeNodes(AsyncCallback<List<TreeNodeBean>> AsyncCallback);

    public void getBikAllTreeNodesExcludingOneTree(int treeId, AsyncCallback<List<TreeNodeBean>> AsyncCallback);

    public void getBikAllRootTreeNodesExcludingOneTree(int treeId, Integer nodeKindId, String relationType, String attributeName, List<Integer> parentNodeIdsDirectOverride, /*boolean isHideDictionaryRootTab -tymczasowe do ukrycia słowników,*/ AsyncCallback<List<TreeNodeBean>> AsyncCallback);

    public void addBikJoinedObject(int srcNodeId, int dstNodeId,
            boolean inheritToDescendants, AsyncCallback<Void> AsyncCallback);

    public void deleteBikJoinedObjs(int srcNodeId, int dstNodeId, AsyncCallback<Void> asyncCallback);

    //   public void getStartupData(AsyncCallback<StartupDataBean> asyncCallback);
    public void createTreeNodeBiz(TreeNodeBean repo, ArticleBean param, int action, String nodeKindCode, AsyncCallback<Pair<Integer, List<String>>> asyncCallback);

    public void addBikDocument(int parentNodeId, String caption, String href, String nodeKindCode, AsyncCallback<Pair<Integer, List<String>>> asyncCallback);

    public void addBikDocumentAdHoc(String caption, String href, String nodeKindCode, AsyncCallback<java.lang.Integer> asyncCallback);

    public void getBikDocumentTreeNodes(AsyncCallback<List<TreeNodeBean>> AsyncCallback);

    public void getBikRolesTreeNodes(boolean onlyTopRoles, AsyncCallback<List<TreeNodeBean>> AsyncCallback);

    public void getBikDocumentRootTreeNodes(AsyncCallback<List<TreeNodeBean>> AsyncCallback);

    public void getBikDocumentForSiId(int siId, AsyncCallback<AttachmentBean> asyncCallback);

    public Request getBikTreeForData(AsyncCallback<DynamicTreesBigBean> callback);

    public void getBikAllArts(int treeId, AsyncCallback<List<String>> asyncCallback);

    public void updateBikTree(int id, Map<String, String> langToName, String parentMenuCode, int visualOrder, int isAutoObjId,
            Map<String, String> optMenuSubGroup, String treeKind,
            String optServerTreeFileName, AsyncCallback<Void> AsyncCallback);

    public void addBikTree(String name, int nodeKindId, AsyncCallback<Void> AsyncCallback);

    public void deleteTree(Integer parseInt, AsyncCallback<Void> AsyncCallback);

    public void createTree(Map<String, String> langToName, String treeKind, String parentMenuCode, Integer parentId, Integer visualOrder, Integer isAutoObjId,
            Map<String, String> optMenuSubGroup, String optServerTreeFileName, AsyncCallback<Void> asyncCallback);

    public void addOrUpdateTreeType(List<TreeKindBean> tkbs, Integer kindId, AsyncCallback<TreeKindBean> asyncCallback);

    public void sendMail(String subject, String body, String[] recipients, AsyncCallback<Void> asyncCallback);

    public void sendMailWithFiles(String subject, String body, Map<String, String> embeddedFiles, String[] recipients, AsyncCallback<Void> asyncCallback);

    public void sendMailWithCCAndFiles(String subject, String body, Map<String, String> embeddedFiles, String[] recipients, String[] cc, AsyncCallback<Void> asyncCallback);

    public void deleteJoinedObjectWithRoleFromUser(int nodeId, int roleInKindId, int inheritToDescendants, boolean isAuxiliary, int userNodeId, AsyncCallback<Integer> asyncCallback);

    public void addUserInNode(int nodeId, Integer userId, int roleForNodeId, int inheritToDescendants, boolean isAuxiliary, JoinTypeRoles joinTypeRoles, AsyncCallback<Boolean> asyncCallback);

    public void addVote(int nodeId, int value, AsyncCallback<Void> asyncCallback);

    public void deleteLoggedUserVoteForNode(int nodeId, AsyncCallback<Void> asyncCallback);

    public void getBikAllJoinableTreeNodes(AsyncCallback<List<TreeNodeBean>> asyncCallback);

    public void getBikAllJoinableRootTreeNodes(Set<String> treeKind, boolean noLinkedNodes, Integer nodeKindId, String relationType, Integer treeId, AsyncCallback<List<TreeNodeBean>> asyncCallback);

    public void bikUserByNodeId(int nodeId, AsyncCallback<UserBean> asyncCallback);

    public void bikUserByLogin(String loginName, AsyncCallback<UserBean> asyncCallback);

    public void addBikTreeNodeUser(TreeNodeBean repo, UserExtBean user, AsyncCallback<Pair<Integer, List<String>>> standardAsyncCallback);

    public void getBikRolesForNode(AsyncCallback<List<BikRoleForNodeBean>> asyncCallback);

    public void updateMetapediaArt(int id, String subject, String body, int official, int nodeId, int action, AsyncCallback<java.lang.Integer> asyncCallback);

    public void getBikEntryByNodeId(int nodeId, AsyncCallback<BlogBean> asyncCallback);

    public void addBikBlogEntry(int parentNodeId, String content, String subject, AsyncCallback<Pair<Integer, List<String>>> asyncCallback);

    public void addBikAuthorsBlog(int nodeId, int userId, boolean isAdmin, AsyncCallback<Void> asyncCallback);

    public void deleteBikAuthorsBlog(int nodeId, int userId, AsyncCallback<Void> asyncCallback);

    public void getNewDefinitionData(int treeId, AsyncCallback<NewDefinitionDataBean> asyncCallback);

    public void runTeradataPump(AsyncCallback<Void> asyncCallback);

    public void runTeradataDataModelPump(AsyncCallback<Void> asyncCallback);

    public void runErwinDataModelPump(AsyncCallback<Void> asyncCallback);

    public void runOraclePump(String instanceName, AsyncCallback<Void> asyncCallback);

    public void runPostgresPump(String instanceName, AsyncCallback<Void> asyncCallback);

    public void runProfilePump(AsyncCallback<Void> asyncCallback);

    public void runMSSQLPump(String instanceName, AsyncCallback<Void> asyncCallback);

    public void runJdbcPump(String instanceName, AsyncCallback<Void> asyncCallback);

    public void runFileSystemPump(String instanceName, AsyncCallback<Void> asyncCallback);

    public void runDynamicAxPump(String instanceName, AsyncCallback<Void> asyncCallback);

    public void runDQCPump(AsyncCallback<Void> asyncCallback);

    public void runADPump(AsyncCallback<Void> asyncCallback);

    public void runSAPBWPump(AsyncCallback<Void> asyncCallback);

    public void runConfluencePump(AsyncCallback<Void> asyncCallback);

    public void runSapBOPump(String instanceName, String source, AsyncCallback<Void> asyncCallback);

    public void runSapBOReportPump(String instanceName, String source, AsyncCallback<Void> asyncCallback);

    public void runSapBODesignerPump(String instanceName, String source, AsyncCallback<Void> asyncCallback);

    public void runSapBOIDTPump(String instanceName, AsyncCallback<Void> asyncCallback);

    public void runSapBOPumpAll(String instanceName, String source, AsyncCallback<Void> asyncCallback);

    public void runPermissionsPump(String sqlName, AsyncCallback<Void> asyncCallback);

    public void runAll(AsyncCallback<Void> asyncCallback);

    public void runSapBoTestConnection(int id, AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void runTeradataTestConnection(AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void runDQCTestConnection(AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void runADTestConnection(AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void runSASTestConnection(AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void runSapBWTestConnection(AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void runConfluenceTestConnection(AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void runMsSqlTestConnection(int id, String optObjsForGrantsTesting, AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void runOracleTestConnection(int id, String optObjsForGrantsTesting, AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void runPostgresTestConnection(int id, String optObjsForGrantsTesting, AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void runJdbcTestConnection(int id, AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void runFileSystemTestConnection(int id, AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void runProfileTestConnection(AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void getActiveSystemInstance(String source, AsyncCallback<List<String>> asyncCallback);

    public void updateBikAuthorsBlogToAdmin(int nodeId, int userId, AsyncCallback<Void> asyncCallback);

    public void setUserAvatar(String avatar, AsyncCallback<String> asyncCallback);

    public void promoteMetapediaArt(TreeNodeBean repo, int id, String subject, String body, int official, int nodeId, int action, String versions, AsyncCallback<Pair<Integer, List<String>>> asyncCallback);

    public void setSapBoConnectionParameters(ConnectionParametersSapBoBean bean, AsyncCallback<Void> asyncCallback);

    public void setTeradataConnectionParameters(ConnectionParametersTeradataBean bean, AsyncCallback<Void> asyncCallback);

    public void setDQCConnectionParameters(ConnectionParametersDQCBean bean, AsyncCallback<Void> asyncCallback);

    public void setADConnectionParameters(ConnectionParametersADBean bean, AsyncCallback<Void> asyncCallback);

    public void setOracleConnectionParameters(ConnectionParametersDBServerBean bean, AsyncCallback<Void> asyncCallback);

    public void setPostgresConnectionParameters(ConnectionParametersDBServerBean bean, AsyncCallback<Void> asyncCallback);

    public void setProfileConnectionParameters(ConnectionParametersProfileBean bean, AsyncCallback<Void> asyncCallback);

    public void setMsSqlConnectionParameters(ConnectionParametersDBServerBean bean, AsyncCallback<Void> asyncCallback);

    public void setMsSqlExPropParameters(MsSqlExPropConfigBean bean, AsyncCallback<Void> asyncCallback);

    public void setSASConnectionParameters(ConnectionParametersSASBean bean, AsyncCallback<Void> asyncCallback);

    public void setSapBWConnectionParameters(ConnectionParametersSapBWBean bean, AsyncCallback<Void> asyncCallback);

    public void setConfluenceConnectionParameters(ConnectionParametersConfluenceBean bean, AsyncCallback<Void> asyncCallback);

    public void addBOConfig(String sourceName, String configName, AsyncCallback<ConnectionParametersSapBoBean> asyncCallback);

    public void addDBConfig(String sourceName, String configName, boolean needSeparateSchedule, String treeCode, AsyncCallback<ConnectionParametersDBServerBean> asyncCallback);

    public void deleteBOConfig(int configId, AsyncCallback<Void> asyncCallback);

    public void deleteDBConfig(int configId, String sourceName, String serverName, AsyncCallback<Void> asyncCallback);

    public void getConfluenceRootObjects(AsyncCallback<List<ConfluenceObjectBean>> asyncCallback);

    public Request getConfluenceChildrenObjects(String parentId, String parentKind, AsyncCallback<List<ConfluenceObjectBean>> asyncCallback);

    public Request getConfluenceFiltredObjects(final String val, String ticket, AsyncCallback<List<ConfluenceObjectBean>> asyncCallback);

    public Request getGroupRootObjects(AsyncCallback<List<SystemUserGroupBean>> asyncCallback);

    public Request getGroupChildrenObjects(String objId, int id, AsyncCallback<List<SystemUserGroupBean>> asyncCallback);

    public void refreshConfluenceObject(int nodeId, AsyncCallback<Void> asyncCallback);

    public void saveConfluenceObjects(Map<String, String> objectsIds, Integer parentId, int treeId, AsyncCallback<Void> asyncCallback);

    public void publishArticleToConfluence(String biksUrlBase, String articleName, String articleBody, String confluenceLogin, String confluencePassword, String parentObjId, String space, Integer parentNodeId, int treeId, AsyncCallback<Void> asyncCallback);

    public void getMSSQLServerAndDatabasesForBOConnection(AsyncCallback<Map<String, List<String>>> asyncCallback);

    public void getOracleServerForBOConnection(AsyncCallback<Set<String>> asyncCallback);

    public void setMSSQLServerAndDatabasesForBOConnection(int nodeId, String serverName, String databaseName, String schemaName, AsyncCallback<Void> asyncCallback);

    public void setOracleServerForBOConnection(int nodeId, String serverName, AsyncCallback<Void> asyncCallback);

    public void getLastSourceStatus(String source, AsyncCallback<java.lang.Integer> asyncCallback);

    public Request getOneSubtreeLevelOfTree(int treeId, Integer parentNodeId, boolean withLinkedNodes,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, boolean calcHasNoChildren, Integer nodeKindId, String relationType, String attributeName, Integer outerTreeId, AsyncCallback<List<TreeNodeBean>> asyncCallback);

    public Request getBikNodeAncestorIdsMulti(Collection<Integer> nodeIds, AsyncCallback<Map<Integer, List<Integer>>> asyncCallback);

    public Request getBikNodeAncestorIds(int nodeId, AsyncCallback<List<Integer>> asyncCallback);

    public Request getSubtreeLevelsByParentIds(Collection<Integer> parentNodeIds,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String optRelationType, String optAttributeName, Integer nodeKindId, AsyncCallback<List<TreeNodeBean>> asyncCallback);

    public Request getBikFilteredNodes(int treeId, String val, final Integer optSubTreeRootNodeId, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String ticket, Integer optNodeKindId, String optRelationType, String optAttributeName, AsyncCallback<List<TreeNodeBean>> asyncCallback);

    public Request getUserGroupFilteredNodes(String val, String ticket, AsyncCallback<List<SystemUserGroupBean>> asyncCallback);

    public void getAllAboutTable(int tableId, AsyncCallback<List<SapBoUniverseColumnBean>> asyncCallback);

    public void addFrequentlyAskedQuestions(int nodeId, String question, String answer, AsyncCallback<Integer> asyncCallback);

    public void updateFrequentlyAskedQuestions(int id, String question, String answer, AsyncCallback<Void> asyncCallback);

    public void deleteFrequentlyAskedQuestions(int id, AsyncCallback<Void> asyncCallback);

    public void setEditableAppProps(Map<String, String> modifiedOrNewProps, AsyncCallback<List<AppPropBean>> asyncCallback);

    public Request newSearch(String text, int offset, int limit,
            Collection<Integer> optTreeIds,
            Collection<Integer> optKindIdsToSearch,
            Collection<Integer> optKindIdsToExclude,
            boolean calcKindAndCounts,
            Collection<ISearchByAttrValueCriteria> optSBAVCs,
            String ticket, String opEpr,
            boolean useLucene,
            AsyncCallback<SearchFullResult> asyncCallback);

    public Request getBikAttributeAndTheirKinds(AsyncCallback<List<AttributeAndTheirKindsBean>> callback);

    public void getNodeKinds(AsyncCallback<List<NodeKindBean>> callback);

    public Request getNotesByUser(AsyncCallback<List<NoteBean>> asyncCallback);

    public void addNewRole(String caption, int attrCategoryId, AsyncCallback<Void> asyncCallback);

    public void updateRole(int id, String caption, int attrCategoryId, AsyncCallback<Void> asyncCallback);

    public void deleteRole(int id, AsyncCallback<Void> asyncCallback);

    public void updateDescription(NameDescBean nameDesc, TreeNodeBean node, AsyncCallback<Void> asyncCallback);

    public void updateDescriptionForMetadata(NameDescBean nameDesc, TreeNodeBean node, AsyncCallback<Void> asyncCallback);

    public void addLinkUser(int linkedId, String name, int parentNodeId, int treeId, AsyncCallback< Pair<Integer, Boolean>> asyncCallback);

    public Request getAttributes(AsyncCallback<List<AttrHintBean>> callback);

    public void updateAttrHint(String name, String hint, int isBuiltIn, AsyncCallback<Void> asyncCallback);

    public void changeInheritance(int idBikJoinedObj, int inherit, AsyncCallback<Void> asyncCallback);

    public void getCountOfLinkedElements(int idAtr, AsyncCallback<Integer> asyncCallback);

    public Request getBISystemsConnectionsParameters(AsyncCallback<BISystemsConnectionsParametersBean> asyncCallback);

    public void addBikSystemUser(String loginName, String password, boolean isDisabled, Integer userId, Set<String> userRightRoleCodes, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries, List<Integer> usersGroupsIds, boolean doNotFilterHtml, AsyncCallback<Integer> asyncCallback);

//    public Request getAllBikSystemUser(AsyncCallback<List<SystemUserBean>> asyncCallback);
    public void updateBikSystemUser(int id, String loginName, String password, boolean isDisabled, Integer userId, String systemUserName,
            Set<String> userRightRoleCodes, Set<Integer> treeIds, boolean isSendMails, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries, List<Integer> usersGroupsIds, boolean doNotFilterHtml, AsyncCallback<Void> asyncCallback);

    public void deleteSystemUser(Integer systemUserId, AsyncCallback<Void> asyncCallback);

    public void updateBikSystemUser(int id, String loginName, String password, boolean isSendMails, AsyncCallback<Void> asyncCallback);

    public void getBikUserTreeNodes(Integer optUpSelectedNodeId, AsyncCallback<List<TreeNodeBean>> AsyncCallback);

    public void getNotConnectedUsers(AsyncCallback<List<TreeNodeBean>> AsyncCallback);

    public void getBikUserRootTreeNodes(Integer optUpSelectedNodeId, AsyncCallback<List<TreeNodeBean>> AsyncCallback);

    public void getAttributeDict(AsyncCallback<Map<Integer, List<Integer>>> AsyncCallback);

    public void getRoleForNode(AsyncCallback<List<RoleForNodeBean>> AsyncCallback);

    public void getAncestorNodePathByNodeId(int nodeId, AsyncCallback<List<String>> asyncCallback);

    public void updateItemInCategory(int itemId, int categoryId, String name, String attrType, String valueOpt, boolean displayAsNumber, boolean usedInDrools, AsyncCallback<Void> asyncCallback);

    public void addOrUpdateCategory(Integer id, String name, AsyncCallback<Integer> asyncCallback);

    public void addAttributeDict(String itemName, int categoryId, String typeAttr, String attrValueList, boolean displayAsNumber, boolean usedInDrools, AsyncCallback<Integer> asyncCallback);

    public void getAttrDefAndCategory(AsyncCallback<List<AttributeBean>> asyncCallback);

    public void getAttrDefBuiltAndCategory(AsyncCallback<List<AttributeBean>> asyncCallback);

    public void addOrDeleteAttributeDictForKind(Map<Integer, Integer> atrIdAndCnt, Set<Integer> selectedNodeKindIds, AsyncCallback<Void> asyncCallback);

    public void modifySystemAttrVisibleForKind(Map<Integer, Integer> atrIdAndCnt, Set<Integer> selectedNodeKindIds, AsyncCallback<Void> asyncCallback);

    public void deleteAttributeDict(int atrId, AsyncCallback<Void> asyncCallback);

    public void updateJoinedObjsForNode(int nodeId, boolean noLinkedNodes, Map<Integer, JoinedObjMode> directOverride, Map<Integer, Pair<JoinedObjMode, Boolean>> subNodesOverride, AsyncCallback<Void> asyncCallback);

    public void getNodeNamesByIds(Collection<Integer> nodeIds, AsyncCallback<Map<Integer, String>> asyncCallback);

    public Request getBikFilteredNodesWithCondidtion(String val, final Integer optSubTreeRootNodeId, Collection<Integer> treeIds, boolean noLinkedNodes,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String ticket, Integer optNodeKindId, String optRelationType, String optAttributeName, AsyncCallback<List<TreeNodeBean>> asyncCallback);

    public void getFullTextCrawlCompleteStatus(boolean useLucene, AsyncCallback<IndexCrawlStatus> asyncCallback);

    public void cancelDBJob(String ticket, AsyncCallback<java.lang.Boolean> asyncCallback);

    public void getAlreadyJoinedUserIds(AsyncCallback<Set<Integer>> asyncCallback);

//    public void getAdminOrExpertBikSystemUser(AsyncCallback<List<SystemUserBean>> asyncCallback);
    public Request getVotesByUser(AsyncCallback<List<VoteBean>> asyncCallback);

    public void deleteCategory(int id, AsyncCallback<Void> asyncCallback);

    public void isOldPasswordTrue(String password, int id, AsyncCallback<Boolean> asyncCallback);

    public void setNodeVisualOrder(int nodeId, int visualOrder, AsyncCallback<Void> asyncCallback);

    public void updateUserRolesForNode(int nodeId, boolean noLinkedNodes, Map<Integer, JoinedObjMode> directOverride, Map<Integer, Pair<JoinedObjMode, Boolean>> subNodesOverride, int roleId, Set<String> treeKind, boolean isAuxiliary, JoinTypeRoles joinTypeRole, Collection<Integer> alreadyJoinedNode, AsyncCallback<Boolean> asyncCallback);

    public void resetVisualOrderForNodes(Integer parentId, int treeId, AsyncCallback<Void> asyncCallback);

    public void keepSessionAlive(String dummy, AsyncCallback<String> asyncCallback);

    public void getChangedRanking(long timeStamp, AsyncCallback<ChangedRankingBean> asyncCallback);

    public void getAppVersion(AsyncCallback<Pair<String, String>> asyncCallback);

    public void getBikAttributesNameAndType(Integer nodeId, Integer nodeKindId, String relationType, Integer treeId, Map<String, List<Integer>> usedLink, AsyncCallback<Pair<Map<String, AttributeBean>, Map<String, List<TreeNodeBean>>>> asyncCallback);

    public void getHelpDescr(String helpKey, String caption, AsyncCallback<HelpBean> asyncCallback);

    public void getMultipleHelpDescr(String helpKey, String caption, String lang, AsyncCallback<HelpBean> asyncCallback);

    public void addOrUpdateBikHelp(HelpBean hb, AsyncCallback<Void> asyncCallback);

    public Request getHelpHints(AsyncCallback<List<HelpBean>> asyncCallback);

    public void updateBikHelpHint(HelpBean hb, AsyncCallback<Void> asyncCallback);

    public void getDescendantsCntInMainRoles(int nodeId, AsyncCallback<Map<Integer, Integer>> asyncCallback);

    public void deleteUserInNode(int nodeId, Integer userId, int roleInKindId, AsyncCallback<Boolean> asyncCallback);

    public void getDescendantsCntInMainRoleByUserRoleAndTreeKind(int userId, int roleForNodeId, Set<String> treeKindForSelected, AsyncCallback<Integer> asyncCallback);

    public Request getNewsByUser(AsyncCallback<List<NewsBean>> asyncCallback);

    public void markNewsAsReadAndGetNew(int newsId, AsyncCallback<NewsBean> asyncCallback);

    public void addNews(NewsBean news, AsyncCallback<NewsBean> asyncCallback);

//    public void getNewsToEdit(int id, AsyncCallback<NewsBean> asyncCallback);
    public void updateNews(NewsBean news, AsyncCallback<Void> asyncCallback);

    public void deleteNews(int newsId, AsyncCallback<Void> asyncCallback);

    public void insertBIKStatisticsEveryTimeOneDay(String counterName, String parameter, AsyncCallback<Void> asyncCallback);

    public void getChangedDescendantsObjectInFvs(int nodeId, int userId, AsyncCallback<List<ObjectInFvsChangeBean>> asyncCallback);

    public void insertBIKStatisticsTabAndNode(String caption, String description, String parametr, AsyncCallback<Void> asyncCallback);

    public void insertBIKStatisticsExtTabAndNode(int nodeId, String branchIds, AsyncCallback<Void> asyncCallback);

    public void getStatisticsDict(AsyncCallback<List<StatisticsBean>> asyncCallback);

    public void deleteAllChangedObjectInFvsInFvs(int nodeId, AsyncCallback<Void> asyncCallback);

    public void deleteChangedObjectInFvs(int nodeId, AsyncCallback<Void> asyncCallback);

    public void deleteAllChangedObjectForUser(AsyncCallback<Void> asyncCallback);

    public void copyAllRolesUser(int userIdFrom, int userIdTo, Boolean isMoveMain, Boolean isCopyAuxiliary, AsyncCallback<Void> asyncCallback);

    public void getAllUserLogged(AsyncCallback<List<String>> asyncCallback);

    public void getTutorial(AsyncCallback<List<TutorialBean>> AsyncCallback);

    public void markTutorialAsRead(int tutorialId, AsyncCallback<Void> AsyncCallback);

    public Request getRankUsers(AsyncCallback<List<NodeAuthorBean>> AsyncCallback);

    public void getHomePageHints(AsyncCallback<List<HomePageBean>> asyncCallback);

    public void getShortCodesForMyBIKSTabsWithNewItems(AsyncCallback<Set<String>> asyncCallback);

    public void getSchedule(int pageNum, int pageSize, AsyncCallback<Pair<Integer, List<SchedulePumpBean>>> asyncCallback);

    public void setSchedule(List<SchedulePumpBean> bean, AsyncCallback<Void> asyncCallback);

    public void getLastInstanceForRaport(String reportId, int nodeId, AsyncCallback<Pair<Integer, String>> asyncCallback);

    public void getSearchHints(AsyncCallback<List<String>> asyncCallback);

    public Request getAllBikSystemUserWithFilter(String ticket, String textOfFilter, Boolean isDisabled,
            Boolean hasCommunityUser, RightRoleBean rightRole, Set<Integer> selectedAuthorTreeIDs, Integer optCustomRightRoleId, boolean showUserInRoleFromGroup,
            AsyncCallback<List<SystemUserBean>> asyncCallback);

    public void getChildrenNodeBeansByParentId(int parentNodeId, AsyncCallback<List<TreeNodeBean>> asyncCallback);

    public void deleteTreeNodeWithTheUserRole(int id, String branchIds, AsyncCallback<List<String>> AsyncCallback);

    public void updateTreeNodeWithTheUserRole(int id, String name, AsyncCallback<List<String>> AsyncCallback);

    public void createTreeNodeWithTheUserRole(Integer parentNodeId, int nodeKindId, String name, String objId, int treeId, AsyncCallback<Pair<Pair<Integer, Integer>, List<String>>> AsyncCallback);

    public void getBikUserRolesTreeNodes(Integer optUpSelectedNodeId, String showOnlyRoleByCode, boolean useLazyLoading, AsyncCallback<List<TreeNodeBean>> asyncCallback);

    public void deleteBadLinkedUsersUnderRole(AsyncCallback<Boolean> asyncCallback);

    public void resetPumpThread(AsyncCallback<Void> asyncCallback);

    public void getAvailableConnectors(AsyncCallback<Set<String>> asyncCallback);

    public Request getIcons(AsyncCallback<Set<String>> asyncCallback);

    public void changeToBranchOrLeaf(int nodeId, boolean changeBranchToleaf, AsyncCallback<Void> asyncCallback);

    public void setBODesignerPath(String path, String source, AsyncCallback<Void> asyncCallback);

    public void setReportSDKPath(String path, AsyncCallback<Void> asyncCallback);

    public void setPath(String path, String pathName, AsyncCallback<Void> asyncCallback);

    public void getTranslatedTreeKinds(int treeKindId, AsyncCallback<Map<String, TreeKindBean>> asyncCallback);

    public void getTranslatedTrees(int treeId, AsyncCallback<Map<String, String>> asyncCallback);

    public void updateTutorial(int id, String title, String text, AsyncCallback<Void> asyncCallback);

    public void getErwinListTables(String areaId, AsyncCallback<ErwinDiagramDataBean> asyncCallback);

    public void getDataModelProcessObjectsRel(int nodeId, String areaName, AsyncCallback<List<ErwinDataModelProcessObjectsRel>> asyncCallback);

    public void setConnectionParametersErwinBean(ConnectionParametersErwinBean bean, AsyncCallback<Void> asyncCallback);

    public void setPermissionsConnectionParameters(ParametersAnySqlBean bean, AsyncCallback<Void> asyncCallback);

    public void getNodeId(String objId, String treeCode, AsyncCallback<Integer> asyncCallback);

    public void getNodeKindsIconsForTree(int treeId, String treeKind, AsyncCallback<List<NodeKindBean>> asyncCallback);

    public void setNodeKindsIconsForTree(int treeId, List<NodeKindBean> icons, AsyncCallback<Void> asyncCallback);

    public void findAnticipatedNodeOfTree(int treeId, Integer optAncestorNodeId, String anticipatedNodeName, AsyncCallback<Integer> asyncCallback);

    public void getAttributesBySelectedTreeNode(Collection<Integer> treeIds, Set<Integer> kindsToSearchFor, AsyncCallback<List<AttrSearchableBean>> asyncCallback);

    public void updateConfluenceNode(TreeNodeBean node, AsyncCallback<Integer> asyncCallback);

    public void getMyObjects(AsyncCallback<List<MyObjectsBean>> asyncCallback);

    public void getUsersObjects(AsyncCallback<List<UserObjectBean>> asyncCallback);

    public void registerNodeActionCodes(Set<String> codes, AsyncCallback<Void> asyncCallback);

    public void getRutEntries(AsyncCallback<Set<TreeNodeBranchBean>> asyncCallback);

    public void updateRutEntries(Set<Pair<Integer, Integer>> newRutEntries, AsyncCallback<Void> asyncCallback);

    public void getLoggedUserName(AsyncCallback<String> asyncCallback);

    public void getModifiedNodesFromLog(int treeId, long timeStamp, AsyncCallback<List<NodeModificationLogEntryBean>> asyncCallback);

    public void registerChangeInTrees(Long serverTimeStamp, Set<Integer> treeIds, AsyncCallback<Void> asyncCallback);

    public void importCSVDataToTree(final String treeCode, final String filePath, AsyncCallback<Void> asyncCallback);

    public void forSerializationOnly(AsyncCallback<Void> asyncCallback);

    public void getSystemUsersGroup(int groupId, AsyncCallback<SystemUserGroupBean> asyncCallback);

    public void updateSystemUserGroup(int groupId, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries, AsyncCallback<Void> asyncCallback);

    public void updateProfileInRole(int groupId, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries, AsyncCallback<Void> asyncCallback);

    public void addJdbcConfig(String sourceName, String serverName, AsyncCallback<ConnectionParametersJdbcBean> asyncCallback);

    public void setJdbcConnectionParameters(ConnectionParametersJdbcBean bean, AsyncCallback<Void> asyncCallback);

    public void deleteJdbcConfig(String sourceName, int id, String serverName, AsyncCallback<Void> asyncCallback);

    public void addFileSystemConfig(String serverName, AsyncCallback<ConnectionParametersFileSystemBean> asyncCallback);

    public void setFileSystemConnectionParameters(ConnectionParametersFileSystemBean bean, AsyncCallback<Void> asyncCallback);

    public void deleteFileSystemConfig(String sourceName, int id, String serverName, AsyncCallback<Void> asyncCallback);

//    public void getRootTreeNodes(Set<String> treeCodes, AsyncCallback<List<TreeNodeBean>> asyncCallback);
//    public void getAttributeUsages(AsyncCallback<List<AttributeUsageBean>> asyncCallback);
    public void calculateAttributeUsages(AsyncCallback<Void> asyncCallback);

    public void getAttributeUsageTreeNodes(AsyncCallback<List<TreeNodeBean>> callback);

    public void getAttributeUsingNodes(TreeNodeBean nodeBean, AsyncCallback<List<AttributeValueInNodeBean>> callback);

    public void getRootTreeNodes4ApplyForExtendedAccess(Set<String> treeCodes, AsyncCallback<List<TreeNodeBean>> asyncCallback);

    public void setCrrLabelsOnNode(Set<String> labels, int nodeId, AsyncCallback<Void> asyncCallback);

    public void getCrrLabelCodes(AsyncCallback<Set<String>> asyncCallback);

    public void performFullSearch(String query, int allCnt, Set<Integer> treeIds,
            Set<Integer> kindsToSearchFor, Set<Integer> kindIdsToExclude,
            boolean calcKindCounts, List<ISearchByAttrValueCriteria> sbavcs,
            String ticket, String opExpr, boolean useLucene, AsyncCallback<String> callback);

    public void clearSearchResultsCache(String ticketForFullSearch, AsyncCallback<Void> callback);

    public void getADUsers(String optfilter, AsyncCallback<List<SystemUserBean>> callback);

    public void getOrCreateSystemUserBeanByLogin(String loginName, String optSsoDomain, AsyncCallback<SystemUserBean> callback);

    public void getFavouritesExForUser(AsyncCallback<List<FvsChangeExBean>> callback);

    public void getChangedDescendantsObjectInFvsEx(int nodeId, int userId, AsyncCallback<List<ObjectInFvsChangeExBean>> callback);

    public void getBIAdminConnectionParameters(AsyncCallback<Pair<List<ConnectionParametersSapBoBean>, ConnectionParametersBIAdminBean>> callback);

    public void setBIAdminConnectionParameters(ConnectionParametersBIAdminBean bean, AsyncCallback<Void> callback);

    public void getBIASchedules(SAPBOScheduleBean filter, AsyncCallback<List<SAPBOScheduleBean>> callback);

    public void pauseBIAInstances(List<Integer> instancesToPause, AsyncCallback<Void> callback);

    public void resumeBIAInstances(List<Integer> instancesToResume, AsyncCallback<Void> callback);

    public void deleteBIAInstances(List<Integer> instancesToDelete, AsyncCallback<Void> callback);

    public void rescheduleBIAInstances(List<Integer> instancesToReschedule, SAPBOScheduleBean params, AsyncCallback<Void> callback);

    public void getSuggestedElementsForLoggedUser(AsyncCallback<List<FvsChangeExBean>> callback);

    public void getBikTreeInBIAdmin(BikNodeTreeOptMode extraMode, AsyncCallback<List<TreeNodeBean>> callback);

    public void getBIAObjects(SAPBOObjectBean filter, AsyncCallback<List<SAPBOObjectBean>> callback);

    public void getBIAUsers(AsyncCallback<List<SAPBOObjectBean>> callback);

    public void setBIAdminArchiveConfig(BIArchiveConfigBean bean, AsyncCallback<Void> callback);

    public void getBIAdminArchiveConfig(AsyncCallback<BIArchiveConfigBean> callback);

    public void stopBIAQuery(AsyncCallback<Void> callback);

    public void runBIArchiveTestConnection(AsyncCallback<String> callback);

    public void changeBIANameAndDescr(Set<SAPBOObjectBean> objs, Integer manageType, AsyncCallback<Void> callback);

    public void changeOwnerBIAObjects(List<Integer> objs, SAPBOObjectBean newOwner, AsyncCallback<Void> callback);

    public void getBIAArchiveLogs(int pageNum, int pageSize, AsyncCallback<Pair<Integer, List<BIAArchiveLogBean>>> callback);

    public void getSimilarFavouriteWithSimilarityScore(int nodeId, AsyncCallback<List<ObjectInFvsChangeBean>> callback);

    public void getBIAuditDatabaseConnectionParameters(AsyncCallback<ConnectionParametersAuditBean> callback);

    public void setBIAuditDatabaseConnectionParameters(ConnectionParametersAuditBean bean, AsyncCallback<Void> callback);

    public void getBIAAuditInformation(AsyncCallback<List<AuditBean>> callback);

    public void getUsersInNode(int nodeId, String branchIds, AsyncCallback<Pair<List<UserInNodeBean>, Map<Integer, String>>> callback);

    public void getAttachments(int nodeId, Integer linkingParentId, AsyncCallback<List<AttachmentBean>> callback);

    public void getBlogs(int nodeId, Integer linkingParentId, AsyncCallback<List<BlogBean>> callback);

    public void getUserBlogs(int nodeId, AsyncCallback<List<BlogBean>> callback);

    public void getNotes(int nodeId, AsyncCallback<List<NoteBean>> callback);

    public void getJoinedObjs(int nodeId, Integer linkingParentId, AsyncCallback<List<JoinedObjBean>> callback);

    public void runBIAAuditTestConnection(AsyncCallback<Pair<Integer, String>> callback);

    public void getBIAInstanceTreeCode(String typeName, AsyncCallback<String> callback);

    public void addDQMConnections(String name, AsyncCallback<ConnectionParametersDQMConnectionsBean> callback);

    public void deleteDQMConnections(int id, AsyncCallback<Void> callback);

    public void updateDQMConnections(ConnectionParametersDQMConnectionsBean cpDQM, AsyncCallback<Void> callback);

    public void runDQMTestConnection(int id, String optObjsForGrantsTesting, AsyncCallback<Pair<Integer, String>> callback);

    public void addJoinedObjAttribute(String name, String type, String valueOptList, String valueOptTo, boolean usedInDrools, AsyncCallback<Integer> callback);

    public void getJoinedObjAttribute(AsyncCallback<List<AttributeBean>> callback);

    public void deleteJoinedObjAttribute(int attrId, AsyncCallback<Void> callback);

    public void updateJoinedObjAttribute(int itemId, String name, String type, String valueOptList, String valueOptTo, boolean usedInDrools, AsyncCallback<Void> callback);

    public void getJoinedAttributesLinked(int joinedObjId, AsyncCallback<List<JoinedObjAttributeLinkedBean>> callback);

    public void getJoinedAttributesLinkedAndoinedObjAttributes(Integer joinedObjId, AsyncCallback<Pair<List<JoinedObjAttributeLinkedBean>, Map<String, AttributeBean>>> callback);

    public void addJoinedAttributeLinked(int joinedObjId, List<JoinedObjAttributeLinkedBean> joal, AsyncCallback<Void> callback);

    public void addDynamicAxConfig(String serverName, AsyncCallback<ConnectionParametersDynamicAxBean> asyncCallback);

    public void runPlainFilePump(String instanceName, AsyncCallback<Void> callback);

    public void addPlainFileConfig(String serverName, AsyncCallback<ConnectionParametersPlainFileBean> callback);

    public void setPlainFileConnectionParameters(ConnectionParametersPlainFileBean bean, AsyncCallback<Void> callback);

    public void runPlainFileTestConnection(int id, AsyncCallback<Pair<Integer, String>> callback);

    public void deletePlainFileConfig(String sourceName, int id, String serverName, AsyncCallback<Void> callback);

//    public void getBindedTreeId(Integer currentTreeId, AsyncCallback<List<Integer>> callback);
    public void deleteTmpFiles(Collection<String> tmpFileName2Delete, AsyncCallback<Void> callback);

    public void getDroolsConfig(AsyncCallback<DroolsConfigBean> callback);

    public void setDroolsConfig(DroolsConfigBean bean, AsyncCallback<Void> callback);

    public void resetDrools(AsyncCallback<String> callback);

    public void getDroolsLogs(int pageNum, int pageSize, AsyncCallback<Pair<Integer, List<DroolsLogBean>>> callback);

    public void checkTreeAttributesCompatibilitiy(String treeKind, String serverFileName, AsyncCallback<Boolean> callback);

    public void checkJoinedObjsAttributesCompatibilitiy(Map<Integer, String> bindingTreeIdWithFile, AsyncCallback<Boolean> callback);

    public void importLinksAndJoinedObjs2Tree(Integer treeId, Map<Integer, String> bindingTreeIdWithFile, AsyncCallback<Void> callback);

    public void calculateObjIds(Integer treeId, Set<Integer> allTreeIds, AsyncCallback<Void> callback);

    public void getImportTreeLog(Integer treeId, AsyncCallback<List<String>> asyncCallback);

    public void setTreeImportStatus(int treeId, TreeImportLogStatus status, AsyncCallback<Void> callback);

    public void addSystemUserGroup(String name, Integer parentId, AsyncCallback<Integer> callback);

    public void addSystemRole(String name, String desc, Integer parentId, AsyncCallback<Integer> callback);

    public void addSystemUserToGroup(String groupName, List<Integer> userIds, AsyncCallback<Void> callback);

    public void renameUserGroup(SystemUserGroupBean sub, AsyncCallback<Integer> callback);

    public void deleteUserGroup(int id, AsyncCallback<Void> callback);

    public void getUsersGroups(int userId, AsyncCallback<List<Integer>> callback);

    public void copyPermissionFromUser(int userId, int groupId, AsyncCallback<Void> callback);

    public void getImportableTrees(AsyncCallback<List<TreeBean>> callback);

    public void getManagmentAttrubutes(AsyncCallback<List<AttributeBean>> callback);

    public void updateAttributes(AttributeBean ab, AsyncCallback<Void> callback);

    public void getADLogin(String query, AsyncCallback<List<UserExtradataBean>> callback);

    public void getInfoFromADByLogin(String text, AsyncCallback<UserExtradataBean> callback);

    public void checkAutoObj4Tree(Set<Integer> allTreeIds, AsyncCallback<Set<Integer>> callback);

    public void importTreeWithLinkedAndJoinedObjs(int treeId, Integer isIncremenral, String serverFileName2ImportTree, String serverMetadataFileName, Map<Integer, String> bindingTreeIdWithFile, AsyncCallback<Void> callback);

    public void search(String searchMode, Integer searchId, String text, Set<Integer> selectedTreeIds, Set<String> selectedAttributeName, List<String> selectedOtherFilter, Set<Integer> nodeKindIds, int pageNum, int pageSize, AsyncCallback<BiksSearchResult> callback) throws LameRuntimeException;

    public void automaticFillAttributesForNode(Set<TreeNodeBranchBean> tnb, AttributeBean attrs, boolean addAttribute, AsyncCallback<Void> callback);

    public void automaticUpdateJoinedObjsForNodeInner(int srcId, Set<TreeNodeBranchBean> tnbs, AsyncCallback<Void> callback);

    public void automatiAddJoinedAttributeLinked(int nodeId, Set<TreeNodeBranchBean> tnbs, List<JoinedObjAttributeLinkedBean> joal, AsyncCallback<Void> callback);

    public void automaticDeleteAttributesForNode(Set<TreeNodeBranchBean> tnb, AttributeBean atr, AsyncCallback<Void> callback);

    public void getRequiredAttrs(int nodeKindId, AsyncCallback< List<AttributeBean>> callback);

    public void getRequiredAttrsWithVisOrder(int nodeKindId, AsyncCallback< List<AttributeBean>> callback);

    public void compileJasper(Integer nodeId, String reportName, String params, AsyncCallback<String> callback);

    public void getNodeAssociationScript(Integer nodeId, AsyncCallback<String> callback);

    public void updateJoinedObjsFromParent(Integer nodeId, Set<Integer> dstId2Add, Set<Integer> dstId2Delete, AsyncCallback<Void> callback);

    public void getConfigurationTree(String appProp, AsyncCallback<String> callback);

    public void changeConfigurationTree(String appProp, String treeCode, AsyncCallback<Void> callback) throws LameRuntimeException;

    public void implementConfigTree(Map<Integer, Integer> attrDictConfMap, Set<Integer> attrDictIds2Delete, Map<Integer, Integer> attrDefConfMap, Set<Integer> attrDefIds2Delete, /*Map<Integer, Integer> attrConfMap, Set<Integer> attrIds2Delete, */ Map<Integer, Integer> nodeKindConfMap, Set<Integer> nodeKindIds2Delete, Map<Integer, Integer> treeKindConfMap, Set<Integer> treeKindIds2Delete, AsyncCallback<Void> callback) throws LameRuntimeException;

    public void buildConfigTree(boolean deleteOldTree, AsyncCallback<Void> callback) throws LameRuntimeException;

    public void getAttributeDictOverView(AsyncCallback< Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>>> callback) throws LameRuntimeException;

    public void getAttributeDefOverView(AsyncCallback<Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>>> callback) throws LameRuntimeException;

    public void getNodeKindsOverView(AsyncCallback<Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>>> callback) throws LameRuntimeException;

    public void getTreeKindsOverView(AsyncCallback<Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>>> callback) throws LameRuntimeException;

    public void getNodesByLinkedKind(int nodeKindId, String link, int treeId, AsyncCallback<List<TreeNodeBean>> callback);

    public void compileReport(int nodeId, String reportTag, AsyncCallback<String> callback) throws LameRuntimeException;

    public void getBikAllJoinableNodeListByLinkedKind(int srcNodeKindId, int treeId, String relationType, AsyncCallback<List<TreeNodeBean>> callback);

    public void getKindsForLinkNodeKind(Integer srcNodeKindId, int treeId, String relationType, AsyncCallback<List<TreeNodeBean>> callback);

    public void getInnerJoinKindsForLinkNodeKindAndHyperlink(Integer nodeKindIdOuter, Integer nodeKindIdInner, int reeIdOuter, int treeIdInner, Pair<String, String> relationTypes, String optAttributeName, AsyncCallback<List<TreeNodeBean>> callback);
//    public void buildTmpConfigTree(AsyncCallback<Void> callback);

    public void getAttribute4NodeKindOverView(AsyncCallback<Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>>> callback);

//    public void changeMenuConfigTree(String treeCode, AsyncCallback<Void> callback);
//    public void getMenuConfigurationTree(AsyncCallback<String> callback);
    public void buildMenuConfigTree(boolean deleteOldTree, AsyncCallback<Void> callback) throws LameRuntimeException;

    public void grantRightsFromContextForUser(Map<SystemUserBean, List<RightRoleBean>> map, int treeId, Integer nodeId, String rightsProc, AsyncCallback<Void> callback);

    public void grantRightsFromContextForUserNewRightsSys(List<SystemUserBean> usersList, int nodeId, int roleId, String rightsProc, AsyncCallback<Void> callback);

    public void getAssignedToNodeUsers(int treeId, Integer nodeId, AsyncCallback<Map<RightRoleBean, List<SystemUserBean>>> asyncCallback);

    public void getAllUserRoles(AsyncCallback<List<RightRoleBean>> asyncCallback);

    public void getUsersMappedToRoles(String textOfFilter, Boolean isDisabled,
            Boolean hasCommunityUser, RightRoleBean rightRole, Set<Integer> selectedAuthorTreeIDs, Integer optCustomRightRoleId, boolean showUserInRoleFromGroup,
            AsyncCallback<Map<SystemUserBean, List<RightRoleBean>>> asyncCallback);

    public void getAllUserGroups(AsyncCallback<List<SystemUserGroupBean>> asyncCallback);

    public void getRolesAssociatedWithObj(String nodeKindCode, AsyncCallback<List<SystemUserGroupBean>> asyncCallback);

    public void grantRightsFromContextForGroup(Map<RightRoleBean, List<SystemUserGroupBean>> rightToGroupMap, int treeId, Integer nodeId, String rightsProc, AsyncCallback<Void> callback);

    public void getCurrentUserCustomRightRoleInGroup(int userId, AsyncCallback< Set<String>> callback);

    public void isDeletedNode(Integer nodeId, AsyncCallback<Integer> callback);

    public void implementConfigTree(AsyncCallback<Void> callback) throws LameRuntimeException;

    public void getAllTreeNodes(Integer treeId, AsyncCallback<List<TreeNodeBean>> callback);

    public void getAllTreeNodesToExport(Integer treeId, String branchId, AsyncCallback<List<TreeNodeBean>> callback);

    public void implementMenuConfigTree(AsyncCallback<Void> callback) throws LameRuntimeException;

    public void addSubmenu(Integer parentMenuId, String name, AsyncCallback<Void> callback);

    public void runScript(Integer nodeId, String script, AsyncCallback<Void> callback);

    public void addAnySql(String sourceName, String serverName, AsyncCallback<ParametersAnySqlBean> asyncCallback);

    public void setAnySqlParameters(ParametersAnySqlBean bean, AsyncCallback<Void> asyncCallback);

    public void deleteAnySql(String sourceName, int id, String serverName, AsyncCallback<Void> asyncCallback);

    public void getMailLogs(int pageNum, int pageSize, AsyncCallback<Pair<Integer, List<MailLogBean>>> asyncCallback);

    public void updateRoleDescription(NameDescBean nameDesc, Integer id, AsyncCallback<Void> asyncCallback);

    public void getRoleDescr(Integer id, AsyncCallback<String> asyncCallback);

    public void getAllUsersWithRightsInCurrentNode(int node_id, int group_id, AsyncCallback<List<SystemUserBean>> asyncCallback);

    public void getAttributeOrKindObjectToConfigPrintTemplate(Integer nodeId, Integer nodeIdObjectKind, String nodeKindCode, String nodeKindCodeFromAttrLis, String parentName, AsyncCallback< List<TreeNodeBean>> asyncCallback);

    public void createTreeNodesAttribute(Integer parentNodeId, List<Integer> directOverride, int nodeKindId, AsyncCallback<Integer> asyncCallback);

    public void getPrintTemplates(String nodeKindCode, AsyncCallback<Map<String, PrintTemplateBean>> asyncCallback);

    public void resetVisualOrder(Integer parentId, int treeId, AsyncCallback<Void> asyncCallback);

    public void setStatus(int nodeId, Map<String, AttributeBean> statusAttrs, AsyncCallback<Void> asyncCallback);

}
