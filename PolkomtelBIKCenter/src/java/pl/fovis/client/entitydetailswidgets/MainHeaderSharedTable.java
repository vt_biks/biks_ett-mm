/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public class MainHeaderSharedTable implements IMainHeaderSharedTable, IsWidget {

    protected FlexTable ft = new FlexTable();
    protected int row = 0;
    protected int columnNumb = 0;
    protected MainHeaderWidget edwfb;
//    protected int nodeKindId;

    public MainHeaderSharedTable(MainHeaderWidget edwfb/*, int nodeKindId*/) {
        this.edwfb = edwfb;
//        this.nodeKindId = nodeKindId;

        ft.setWidth("100%");
        ft.setStyleName("gridJoinedObj");
        setColumnWidth();
    }

    protected void setColumnWidth() {
        if (isShowAttributeButtonsOnTheLeft()) {
            ft.getColumnFormatter().setWidth(0, "1px");
            ft.getColumnFormatter().setWidth(1, "1px");
           // ft.getColumnFormatter().setWidth(2, "16px");
            ft.getColumnFormatter().setWidth(2, "8px");
            //add a column in between 3rd and 4th to make indent
            ft.getColumnFormatter().setWidth(3, "250px");
//            ft.getColumnFormatter().setWidth(4, "16px");
        } else {
            ft.getColumnFormatter().setWidth(0, "250px");
            ft.getColumnFormatter().setWidth(2, "16px");
            ft.getColumnFormatter().setWidth(3, "16px");
            ft.getColumnFormatter().setWidth(4, "16px");
            ft.getColumnFormatter().setWidth(5, "16px");
            
        }
    }

    public void setColumnWidthForNonActiveWidget() {
        ft.getColumnFormatter().setWidth(2, "160px");         
    }

    @Override   
    public void addTranslatedRow(String nameId, Object value, Widget... actWidget) {
        String nameToDisplay = BIKClientSingletons.getTranslatedAttributeNameOrNullForHidden(edwfb.getData().node.nodeKindId, nameId);
        addTranslatedRow(nameToDisplay, true, value, nameId != null && nameToDisplay == null, actWidget);
    }

    //wrzuacamy (s,s) lub(s,s,w,w) lub(s,null,w) lub (null,null,w)
    @Override
    public void addRow(String nameToDisplay, boolean isVisible, Object value, Widget... actWidget) {
        addRow(nameToDisplay, isVisible, value, false, actWidget);
    }

    protected void setAttributeDescriptionPosition(boolean isVisible, boolean hideWidgets, boolean isEnabled, Widget... actWidget) {
        if (isEnabled) {
            if (actWidget.length > 0 && !hideWidgets) {
                for (Widget w : actWidget) {
                    ft.getCellFormatter().setStyleName(row, columnNumb, row % 2 == 0 ? "RelatedObj-oneObj-white-top" : "RelatedObj-oneObj-grey-top");
                    ft.setWidget(row, columnNumb++, w);
                }
            }
            ft.getRowFormatter().setVisible(row, isVisible);
            if (columnNumb > 0) {
                while (columnNumb < 3) {
                    ft.setWidget(row, columnNumb++, new Label());
                }
                ft.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
                ft.getCellFormatter().setStyleName(row, 0, row % 2 == 0 ? "RelatedObj-oneObj-white-top" : "RelatedObj-oneObj-grey-top");
                if (!isShowAttributeButtonsOnTheLeft()) {
                    row++;
                }
            }
        }
    }

    protected boolean isShowAttributeButtonsOnTheLeft() {
        if (edwfb != null && edwfb.treeBean != null) {
            String treeCode = edwfb.treeBean.code;
            boolean isDQMTree = treeCode.equals(BIKConstants.TREE_CODE_DQM);
            boolean isShowAttributeButtonsOnTheLeft = isDQMTree ? false : BIKClientSingletons.isShowAttributeButtonsOnTheLeft();
            return isShowAttributeButtonsOnTheLeft;
        }
        return BIKClientSingletons.isShowAttributeButtonsOnTheLeft();
    }

    protected void addRow(String nameToDisplay, boolean isVisible, Object value, boolean hideWidgets, Widget... actWidget) {
//        String nameToDisplay = BIKClientSingletons.getTranslatedAtribute(getData().node.nodeKindId, nameId);
        columnNumb = 0;
        setAttributeDescriptionPosition(isVisible, hideWidgets, isShowAttributeButtonsOnTheLeft(), actWidget);
        if (nameToDisplay != null) {
            String valueStr = BaseUtils.safeToString(value);
            if (!BaseUtils.isStrEmpty(nameToDisplay)) {
                if (!BaseUtils.isStrEmpty(valueStr)) {

                    Widget valuelbl;
                    if (value instanceof Widget) {
                        valuelbl = (Widget) value;
                    } else {
                        valuelbl = new Label(valueStr);
                        valuelbl.setStyleName("gp-html");
                    }

                    ft.setWidget(row, columnNumb++, edwfb.createHintedHTML(nameToDisplay, null));
                    ft.setWidget(row, columnNumb++, valuelbl);
                } else {
                    if (actWidget.length > 0) {
                        ft.setWidget(row, columnNumb++, edwfb.createHintedHTML(nameToDisplay, null));
                    }
                }
            }
        }

        setAttributeDescriptionPosition(isVisible, hideWidgets, !isShowAttributeButtonsOnTheLeft(), actWidget);
        if (isShowAttributeButtonsOnTheLeft()) {
            row++;
        }
    }
    
    protected void addTranslatedRow(String nameToDisplay, boolean isVisible, Object value, boolean hideWidgets, Widget... actWidget) {
//        String nameToDisplay = BIKClientSingletons.getTranslatedAtribute(getData().node.nodeKindId, nameId);
        columnNumb = 0;
        if (nameToDisplay != null) {
            String valueStr = BaseUtils.safeToString(value);
            if (!BaseUtils.isStrEmpty(nameToDisplay)) {
                if (!BaseUtils.isStrEmpty(valueStr)) {

                    Widget valuelbl;
                    if (value instanceof Widget) {
                        valuelbl = (Widget) value;
                    } else {
                        valuelbl = new Label(valueStr);
                        valuelbl.setStyleName("gp-html");
                    }

                    ft.setWidget(row, columnNumb++, edwfb.createHintedHTML(nameToDisplay, null));
                    ft.setWidget(row, columnNumb++, valuelbl);
                } else {
                    if (actWidget.length > 0) {
                        ft.setWidget(row, columnNumb++, edwfb.createHintedHTML(nameToDisplay, null));
                    }
                }
            }
        }

        setAttributeDescriptionPosition(isVisible, hideWidgets, true, actWidget);
        if (isShowAttributeButtonsOnTheLeft()) {
            row++;
        }
    }

    @Override
    public Widget asWidget() {
        return ft;
    }

    @Override
    public void clear() {
        row = 0;
        ft.clear();
    }
}
