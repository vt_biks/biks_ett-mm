/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public interface IDaoAwareNamedSqlsModel<DAO> {

    public void setDao(DAO dao, IServerSupportedLocaleNameRetriever optServerSupportedLocaleNameRetriever);

    public void setGlobals(Map<String, Object> globals);
}
