/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class ProfileBean implements Serializable {

    //"%LIBS", FID, DI, NOD, LEN, DFT, DOM, TBL, PTN, XPO, XPR, TYP, DES, ITP, MIN, MAX, DEC, REQ, CMP, SFD, SFD1, SFD2, SFP, SFT, SIZ, DEL, POS, RHD, SRL, CNV, LTD, USER, MDD, VAL4EXT, DEPREP, DEPOSTP, NULLIND, MDDFID, VALIDCMP
    public String libs; // Library Name                             12  T
    public String fid; // File Name                                256  U
    public String di; // Data Item Name                           256  U
    public String nod; // Subscript Key                            26  T
    public Integer len; // Maximum Field Length                      5  N
    public String dft; // Default Value                            58  T
    public String dom; // User-Defined Data Type                   20  U
    public String tbl; // Look-Up Table Name                       255  T
    public String ptn; // MUMPS Pattern Match                      60  T
    public String xpo; // Post Processor expression                58  T
    public String xpr; // Pre Processor Expression                 58  T
    public String typ; // Data Type                                 1  U
    public String des; // Description                              40  T
    public String itp; // Internal Data Type                        1  T
    public String min; // Minimum Value                            25  T
    public String max; // Maximum Value                            25  T
    public Integer dec; // Decimal Precision                         2  N
    public String req; // Required Indicator                        1  L
    public String cmp; // Computed Expression                      255  T
    public String sfd; // Sub Field Definition                     20  T
    public Integer sfd1; // Sub-Field Delimiter (Tag Prefix)          3  N
    public Integer sfd2; // Sub-Field Delimiter (Tag Suffix)          3  N
    public Integer sfp; // Sub-Field Position                        2  N
    public String sft; // Sub-Field Tag                            12  U
    public Integer siz; // Field Display Size                        3  N
    public Integer del; // Delimeter                                 3  N
    public Integer pos; // Field Position                            2  N
    public String rhd; // Report Header                            40  T
    public String srl; // Serial Value                              1  L
    public Integer cnv; // Conversion Flag                           2  N
    public Date ltd; // Last Updated                             10  D
    public String usr; // User ID                                  20  T
    public String mdd; // Master Dictionary Reference              12  U
    public String val4ext; // Valid for Extraction                      1  L
    public String deprep; // Data Entry Pre-Processor                 255  T
    public String depostp; // Data Entry Post-Processor                255  T
    public String nullind; // Null Vs. Zero Indicator                   1  L
    public String mddfid; // Master Dictionary File Name              12  T
    public String validcmp; // Validate the CMP field syntax             1  L

    public ProfileBean() {
    }

    public ProfileBean(String libs, String fid, String di, String nod, Integer len, String dft, String dom, String tbl, String ptn, String xpo, String xpr, String typ, String des, String itp, String min, String max, Integer dec, String req, String cmp, String sfd, Integer sfd1, Integer sfd2, Integer sfp, String sft, Integer siz, Integer del, Integer pos, String rhd, String srl, Integer cnv, Date ltd, String user, String mdd, String val4ext, String deprep, String depostp, String nullind, String mddfid, String validcmp) {
        this.libs = libs;
        this.fid = fid;
        this.di = di;
        this.nod = nod;
        this.len = len;
        this.dft = dft;
        this.dom = dom;
        this.tbl = tbl;
        this.ptn = ptn;
        this.xpo = xpo;
        this.xpr = xpr;
        this.typ = typ;
        this.des = des;
        this.itp = itp;
        this.min = min;
        this.max = max;
        this.dec = dec;
        this.req = req;
        this.cmp = cmp;
        this.sfd = sfd;
        this.sfd1 = sfd1;
        this.sfd2 = sfd2;
        this.sfp = sfp;
        this.sft = sft;
        this.siz = siz;
        this.del = del;
        this.pos = pos;
        this.rhd = rhd;
        this.srl = srl;
        this.cnv = cnv;
        this.ltd = ltd;
        this.usr = user;
        this.mdd = mdd;
        this.val4ext = val4ext;
        this.deprep = deprep;
        this.depostp = depostp;
        this.nullind = nullind;
        this.mddfid = mddfid;
        this.validcmp = validcmp;
    }

    @Override
    public String toString() {
        return "ProfileBean{" + "libs=" + libs + ", fid=" + fid + ", di=" + di + ", nod=" + nod + ", len=" + len + ", dft=" + dft + ", dom=" + dom + ", tbl=" + tbl + ", ptn=" + ptn + ", xpo=" + xpo + ", xpr=" + xpr + ", typ=" + typ + ", des=" + des + ", itp=" + itp + ", min=" + min + ", max=" + max + ", dec=" + dec + ", req=" + req + ", cmp=" + cmp + ", sfd=" + sfd + ", sfd1=" + sfd1 + ", sfd2=" + sfd2 + ", sfp=" + sfp + ", sft=" + sft + ", siz=" + siz + ", del=" + del + ", pos=" + pos + ", rhd=" + rhd + ", srl=" + srl + ", cnv=" + cnv + ", ltd=" + ltd + ", user=" + usr + ", mdd=" + mdd + ", val4ext=" + val4ext + ", deprep=" + deprep + ", depostp=" + depostp + ", nullind=" + nullind + ", mddfid=" + mddfid + ", validcmp=" + validcmp + '}';
    }
}
