/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

/**
 *
 * @author ctran
 */
public class BIArchiveConfigBean extends ParametersBaseBean {

    public String frequency;
    public String startDate;
    public String startHour;
    public String user;
    public String pw;
    public String domain;
    public String savedFolder;
    public String lcmCliPath;
    public String jobCuid;
    public String limit;
    public int isRemoteSaving;
    public int useGit;
    public String gitUsername;
    public String gitPw;
    public String gitRepository;
    public Double fileSize;
}
