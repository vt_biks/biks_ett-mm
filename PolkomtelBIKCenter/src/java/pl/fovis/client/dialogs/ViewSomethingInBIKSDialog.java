/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.ViewSomethingInFullScreenDialog;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public abstract class ViewSomethingInBIKSDialog extends ViewSomethingInFullScreenDialog {

    protected void setScrollPanelSize(Widget scPanel, int height) {

        Pair<Integer, Integer> weightHight = BIKClientSingletons.customDialogPercentSize();
        if (weightHight == null) {
            super.setScrollPanelSize(scPanel, height);
        } else {
            scPanel.setSize(Window.getClientWidth() * weightHight.v1 / 100 + "px", Window.getClientHeight() * weightHight.v2 / 100 - height + "px");
        }
    }

}
