/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence;

import pl.bssg.metadatapump.MetadataTestConnectionBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class ConfluenceTestConnection extends MetadataTestConnectionBase {

    public ConfluenceTestConnection(MssqlConnectionConfig mcfg) {
        super(mcfg);
    }

    @Override
    public Pair<Integer, String> testConnection() {
        ConfluenceConfigBean configData = null;
        try {
            configData = PumpUtils.readAdminBean("confluence", ConfluenceConfigBean.class, adhocDao);
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Invalid login for data loading! Error: " + e.getMessage());
        }
        ConfluenceConnector conn = null;
        try {
            conn = new ConfluenceConnector(configData);
            conn.initializeService();
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Error in initializa Service - check IP and port. Error message: " + e.getMessage());
        }
        try {
            conn.login();
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Error in login - check user and password. Error message: " + e.getMessage());
        }
        String version = null;
        try {
            version = conn.testConnection();
            conn.logout();
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Error in get version. Error message: " + e.getMessage());
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, "OK! Confluence version: " + version);
    }
}
