/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

/**
 *
 * @author pmielanczuk
 */
public class NamedSqlCallParams {

    public String name;
    public Object[] args;
    public boolean maxOneRow;
    // allowNoResult = !minOneRow
    public boolean minOneRow;

    public NamedSqlCallParams(String name, Object... args) {
        this(name, false, false, args);
    }

    public NamedSqlCallParams(String name, boolean maxOneRow, boolean minOneRow, Object[] args) {
        this.name = name;
        this.args = args;
        this.maxOneRow = maxOneRow;
        this.minOneRow = minOneRow;
    }

    public NamedSqlCallParams setMaxOneRow(boolean maxOneRow) {
        this.maxOneRow = maxOneRow;
        return this;
    }

    public NamedSqlCallParams setMinOneRow(boolean minOneRow) {
        this.minOneRow = minOneRow;
        return this;
    }

    public boolean allowNoResult() {
        return !minOneRow;
    }
}
