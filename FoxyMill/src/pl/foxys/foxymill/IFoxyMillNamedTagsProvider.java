/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill;

import java.util.List;
import pl.foxys.foxymill.tags.ITag;

/**
 *
 * @author wezyr
 */
public interface IFoxyMillNamedTagsProvider {
    public List<ITag> getParsedTemplate(String templateName);
}
