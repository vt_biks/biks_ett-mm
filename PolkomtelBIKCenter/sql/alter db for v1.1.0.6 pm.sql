exec sp_check_version '1.1.0.6';
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- poprzednia wersja w pliku: "alter db for v1.0.99.6 pm.sql"
-- wyrzucamy @valueImportance!
-- SELECT referencing_schema_name + '.' + referencing_entity_name FROM sys.dm_sql_referencing_entities ('sp_verticalize_node_attrs_one_table', 'OBJECT');
/*
dbo.sp_update_searchable_attr_val
dbo.sp_verticalize_node_attrs_add_attrs
dbo.sp_verticalize_node_attrs_inner
dbo.sp_verticalize_node_attrs_one_metadata_table
*/
-- w @optNodeFilter s� mo�liwe odwo�ania do bik_node pod aliasem "n", tzn. np.: n.id = 100 lub n.tree_id = 99 itp.
-- @source to nazwa tabeli albo podzapytanie zapakowane w nawiasy, np. (select 10 as node_id, 'aqq' as val)

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_one_table]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_one_table
go

create procedure [dbo].[sp_verticalize_node_attrs_one_table](@source varchar(max), @nodeIdCol sysname, @cols varchar(max),
  @properNames varchar(max), @optNodeFilter varchar(max))
as
begin
  set nocount on
  
  declare @diags_level int = 0 -- 0 oznacza brak logowania

  -- diag
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': start, @source=' + @source

  --------------
  -- 1. rozbijamy nazwy atrybut�w - nazwy kolumn w �r�dle i nazwy w�a�ciwe (opcjonalne)
  --------------

  declare @attrPropNamesTab table (idx int primary key, name sysname not null)
  
  insert into @attrPropNamesTab (idx, name)
  select idx, str from dbo.fn_split_by_sep(@properNames, ',', 7)
  
  declare @attrNamesTab table (name sysname not null primary key, 
    proper_name varchar(255) not null, search_weight int not null,
    attr_id int null
  )
  
  insert into @attrNamesTab (name, proper_name, search_weight,
    attr_id)
  select aaa.str, coalesce(apnt.name, aaa.str), coalesce(a.search_weight, 1),
    a.id
  from dbo.fn_split_by_sep(@cols, ',', 7) aaa left join @attrPropNamesTab apnt on aaa.idx = apnt.idx
    left join 
      bik_searchable_attr a on a.name = coalesce(apnt.name, aaa.str)

  --------------
  -- 2. znamy nazwy atrybut�w i node_kindy, teraz uzupe�niamy bik_attribute, ew. poprawiamy tam typy
  --------------
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed uzupe�nieniem searchable attrs'
  
  insert into bik_searchable_attr (name, caption, search_weight)
  select ant.proper_name, ant.proper_name, 1
  from @attrNamesTab ant
  where ant.attr_id is null  
  
  update @attrNamesTab set attr_id = a.id
  from @attrNamesTab aaa inner join bik_searchable_attr a on aaa.attr_id is null and a.name = aaa.proper_name
  
  --------------
  -- 3. wrzucamy warto�ci atrybut�w do tabeli pomocniczej
  --------------

  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przygotowanie do wrzucania do @attrValsTab'
  
  declare @attrValsTab table (attr_id int not null, node_id int not null, value varchar(max) null, 
    --type int not null, 
    search_weight int not null, tree_id int not null, node_kind_id int not null, unique (attr_id, node_id))
  
  declare attrCur cursor for select name, --attr_type, 
  proper_name, search_weight, attr_id from @attrNamesTab
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed open attrCur'

  if @diags_level > 0 begin
    select name, --attr_type, 
    proper_name, search_weight, attr_id from @attrNamesTab
  end

  open attrCur
  
  declare @attrName sysname, @attrType int, @attrProperName varchar(255), @searchWeight int, @attr_id int
  declare @attrValSql varchar(max)
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed pierwszym fetch next from attrCur'

  fetch next from attrCur into @attrName, --@attrType, 
    @attrProperName, @searchWeight, @attr_id
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed p�tl�'

  while @@fetch_status = 0
  begin
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': start p�tli dla pola: ' + @attrName + ' (' + @attrProperName + ')'
    
    set @attrValSql = 
    'select attr_id, node_id, /*case when is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''' then null else value end*/ value, search_weight,' +
    ' tree_id, node_kind_id from (' +
    'select ' + cast(@attr_id as varchar(20)) + ' as attr_id, x.' + @nodeIdCol + ' as node_id, x.' + @attrName + ' as value, ' + --', a.type, ' + 
    --cast(@searchWeight as varchar(20)) + ' * (n.search_rank + nk.search_rank) * 100 + n.vote_sum as search_weight, ' +
    'dbo.fn_calc_searchable_attr_val_weight(' + cast(@searchWeight as varchar(20)) + ', n.search_rank, nk.search_rank, n.vote_sum) as search_weight, ' + 
    + 'case when n.is_deleted = 0 and (n.linked_node_id is null or n.disable_linked_subtree <> 0) then 0 else 1 end is_deleted,' + 
      ' n.tree_id, n.node_kind_id' +
      ' from ' + @source + ' as x inner join bik_node n on n.id = x.' + @nodeIdCol +
      ' inner join bik_node_kind nk on n.node_kind_id = nk.id' +
      --' inner join bik_searchable_attr a on n.node_kind_id = a.node_kind_id and a.name = ''' + @attrProperName + '''' +
      --' where x.' + @attrName + ' is not null' +
      case when @optNodeFilter is null then '' else ' where ' + @optNodeFilter end +
      ' ) x where not (is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''')'
    if @diags_level > 0 print 'sql for attr=' + @attrName + ' is: 
    ' + @attrValSql
  
    insert into @attrValsTab (attr_id, node_id, value, --type, 
    search_weight, tree_id, node_kind_id)
    exec (@attrValSql)
  
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed ko�cem iteracji p�tli dla pola: ' + @attrName
    
    fetch next from attrCur into @attrName, --@attrType, 
      @attrProperName, @searchWeight, @attr_id
  end
    
  close attrCur
  
  deallocate attrCur
  
  --------------
  -- 5. aktualizacja bik_searchable_attr_val - na podstawie wyci�gni�tych warto�ci
  --------------
  
  if @diags_level > 0 begin
    declare @avtCnt int = (select count(*) from @attrValsTab)
    print cast(sysdatetime() as varchar(23)) + ': za p�tl�, kursor zamkni�ty, liczba warto�ci razem: ' +
      cast(@avtCnt as varchar(20)) + ', przed delete'
  end
  
  declare @attrIdsStr varchar(max) = ''
  update @attrNamesTab set @attrIdsStr = @attrIdsStr + ',' + cast(attr_id as varchar(20))
  set @attrIdsStr = substring(@attrIdsStr, 2, len(@attrIdsStr))
  
  declare @attrValsInRangeSql varchar(max) = 'select av.id, av.node_id, av.attr_id 
  from bik_searchable_attr_val av' + case when @optNodeFilter is null then '' else ' inner join bik_node n on n.id = av.node_id and ' + @optNodeFilter end +
  ' where av.attr_id in (' + @attrIdsStr + ')'
  
  declare @attrValsInRange table (id int not null primary key, node_id int not null, attr_id int not null, unique (node_id, attr_id))
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': przed insert into @attrValsInRange, sql=' + @attrValsInRangeSql
  end 
  
  insert into @attrValsInRange (id, node_id, attr_id)
  exec(@attrValsInRangeSql)
  
  declare @delCnt int
/*
  delete from bik_searchable_attr_val
  from @attrValsTab avt 
  where avt.value is null and bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id
*/
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': przed w�a�ciwym delete'
  end 
  
  delete from bik_searchable_attr_val
  from @attrValsInRange avir left join @attrValsTab av on avir.attr_id = av.attr_id and avir.node_id = av.node_id
  where bik_searchable_attr_val.id = avir.id and av.node_id is null
  set @delCnt = @@rowcount
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po delete, rowcnt=' + cast(@delCnt as varchar(20)) + ', przed update'
  end

  update bik_searchable_attr_val
  set value = avt.value, 
    fixed_value = case when bik_searchable_attr_val.value <> avt.value then dbo.fn_normalize_text_for_fts(avt.value) else fixed_value end,
    search_weight = avt.search_weight
  from @attrValsTab avt
  where avt.value is not null and bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id and
     (bik_searchable_attr_val.value <> avt.value or bik_searchable_attr_val.search_weight <> avt.search_weight)
  
  declare @updRowCnt int = @@rowcount
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po update, row count: ' +
      cast(@updRowCnt as varchar(20)) + ', przed insert'
  end
  
  --------------
  -- 6. dorzucenie nowych warto�ci
  --------------
  
  insert into bik_searchable_attr_val (node_id, attr_id, value, fixed_value, search_weight, tree_id, node_kind_id)
  select avt.node_id, avt.attr_id, avt.value, dbo.fn_normalize_text_for_fts(avt.value), avt.search_weight, avt.tree_id, avt.node_kind_id
  from @attrValsTab avt left join bik_searchable_attr_val av on
  avt.attr_id = av.attr_id and avt.node_id = av.node_id
  where av.id is null and avt.value is not null

  declare @insRowCnt int = @@rowcount

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po insert, row count: ' +
      cast(@insRowCnt as varchar(20)) + ', KONIEC!'
  end
  
  -- select * from @attrValsTab
  
  --declare @sql varchar(max) = 'select ' + @nodeIdCol + ', ' + @normalCols + ', ' + @identCols + ' from ' + @source
  --print @sql
  
  --select * from @attrNamesTab
  --select * from @attrNodeKindsTab ankt inner join bik_node_kind nk on ankt.node_kind_id = nk.id
end
go


---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- poprzednia wersja w "alter db for v1.0.88.12 ww.sql"
-- usuwamy zb�dny parametr: @valueImportance
/*
SELECT referencing_schema_name, referencing_entity_name, referencing_id, referencing_class_desc, is_caller_dependent
FROM sys.dm_sql_referencing_entities ('sp_update_searchable_attr_val', 'OBJECT');
-- pusto
*/

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_update_searchable_attr_val]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_update_searchable_attr_val
go

create procedure [dbo].sp_update_searchable_attr_val(@nodeId int, @properAttrName varchar(255), @value varchar(max))
as
begin
  declare @source varchar(max) = '(select ' + cast(@nodeId as varchar(20)) + ' as node_id, 
    ''' + replace(@value, '''', '''''') + ''' as val)'
  
  declare @nodeFilter varchar(max) = 'n.id = ' + cast(@nodeId as varchar(20))
    
  exec sp_verticalize_node_attrs_one_table @source, 'node_id', 'val', @properAttrName, @nodeFilter
end
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- poprzednia wersja w "alter db for v1.0.88.12 ww.sql"
-- usuwamy warto�� parametru @valueImportance = 0 w wywo�aniu sp_verticalize_node_attrs_one_table
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_one_metadata_table]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_one_metadata_table
go

create procedure [dbo].sp_verticalize_node_attrs_one_metadata_table(@source varchar(max), @nodeIdCol sysname, 
  @cols varchar(max), @properNames varchar(max), @optNodeFilter varchar(max))
as
begin
  exec sp_verticalize_node_attrs_one_table @source, @nodeIdCol, @cols, @properNames, @optNodeFilter
end
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- nowa procedura!

if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_verticalize_node_attrs_add_attrs') and type in (N'P', N'PC'))
drop procedure dbo.sp_verticalize_node_attrs_add_attrs
go

create procedure dbo.sp_verticalize_node_attrs_add_attrs(@optNodeFilter varchar(max))
as
begin
  set nocount on

  declare @diags_level int = 0 -- 0 oznacza brak logowania

  declare @attr_names table (attr_id int not null unique, name varchar(255) not null unique, is_deleted int not null)
  
  declare @attr_names_sql varchar(max) = 'select ad.id, ad.name, ad.is_deleted from bik_attr_def ad /*where ad.is_deleted = 0*/'
  
  /*
  if (@optNodeFilter is not null) begin
    set @attr_names_sql = @attr_names_sql + ' and ad.id in (select a.attr_def_id from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id
    inner join bik_node n on al.node_id = n.id where ' + @optNodeFilter + ')'
  end
  */
  
  insert into @attr_names (attr_id, name, is_deleted)
  exec(@attr_names_sql)
  
  --select * from @attr_names
  
  declare attr_names_cur cursor for select attr_id, name, is_deleted from @attr_names
  
  declare @attr_id int, @name varchar(255), @is_deleted int
  
  open attr_names_cur
  
  fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  
  --return
  
  while @@fetch_status = 0 begin
    declare @attr_source_sql varchar(max) = '(select node_id, value from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id 
    where ' + case when @is_deleted = 0 then '' else '1 = 0 and ' end + 'al.is_deleted = 0 and a.is_deleted = 0 and a.attr_def_id = ' + cast(@attr_id as varchar(20)) + ')'
    
    --if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': before verticalize one, @name=' + @name + ', source_sql=' + @attr_source_sql

    exec sp_verticalize_node_attrs_one_table @attr_source_sql, 'node_id', 'value', @name, @optNodeFilter    
  
    fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  end
  
  close attr_names_cur
  
  deallocate attr_names_cur
end
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

-- nowa procedura!

if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_verticalize_node_attrs_delete_attr') and type in (N'P', N'PC'))
drop procedure dbo.sp_verticalize_node_attrs_delete_attr
go

create procedure dbo.sp_verticalize_node_attrs_delete_attr(@attribute_id int)
as
begin
  set nocount on
  
  declare @node_kind_id int = (select node_kind_id from bik_attribute where id = @attribute_id)
  
  declare @attr_def_id int = (select attr_def_id from bik_attribute where id = @attribute_id)
  
  declare @attr_name varchar(255) = (select name from bik_attr_def where id = @attr_def_id)
  
  declare @searchable_attr_id int = (select id from bik_searchable_attr where name = @attr_name)
  
  delete from bik_searchable_attr_val where attr_id = @searchable_attr_id and node_id in (select id from bik_node where node_kind_id = @node_kind_id)
end
go

-- select * from bik_searchable_attr

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- poprzednia wersja z pliku "alter db for v1.0.98.2 pm.sql"
-- dodajemy wywo�anie sp_verticalize_node_attrs_add_attrs oraz wykorzystanie pola descr_plain z bik_node

if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_verticalize_node_attrs_inner') and type in (N'P', N'PC'))
drop procedure dbo.sp_verticalize_node_attrs_inner
go

create procedure dbo.sp_verticalize_node_attrs_inner(@optNodeFilter varchar(max))
as
begin
  exec sp_verticalize_node_attrs_one_table '(select id, name /*+ ''uqaz''*/ as name, coalesce(descr_plain, descr) as descr from bik_node)', 'id', 'name, descr', null, @optNodeFilter

  exec sp_verticalize_node_attrs_add_attrs @optNodeFilter

  exec sp_verticalize_node_attrs_metadata @optNodeFilter
end
go


---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- poprzednia wersja z pliku "alter db for v1.0.98.2 pm.sql"
-- tu zmieniamy podej�cie - zamiast kasowa� indeks b�dziemy go prze��cza� na r�czne 
-- aktualizacje, a na koniec - zn�w na automatyczne

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs
go

create procedure [dbo].[sp_verticalize_node_attrs](@optNodeFilter varchar(max))
as
begin
  set nocount on
  
  if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) begin
  --exec('drop fulltext index on bik_searchable_attr_val')
    exec('ALTER FULLTEXT INDEX ON bik_searchable_attr_val SET CHANGE_TRACKING MANUAL')
  end
  
  exec sp_verticalize_node_attrs_inner @optNodeFilter

  if not exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) begin
    declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_searchable_attr_val') and is_primary_key = 1)
    exec('CREATE FULLTEXT INDEX ON bik_searchable_attr_val (value, fixed_value) key index ' + @pk_name + ' with stoplist = off')
  end
  
  if (select change_tracking_state from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) <> 'A' begin
    exec('ALTER FULLTEXT INDEX ON bik_searchable_attr_val SET CHANGE_TRACKING AUTO')
  end
end
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- poni�sze mo�e trwa� w sumie kilkana�cie minut...

if (select val from bik_app_prop where name = 'bik_ver') = '1.1.0.6' begin

  -- w tym biznesie to proste, albo ty przycinasz loga... (cdn).
  declare @log_name varchar(max);
  select @log_name=name from sys.database_files where type = 1
  exec('DBCC SHRINKFILE (N''' + @log_name + '''  , 0)')

  -- wy��czamy indeks - o ile istnieje
  if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) begin
    exec('drop fulltext index on bik_searchable_attr_val')
    --exec('ALTER FULLTEXT INDEX ON bik_searchable_attr_val SET CHANGE_TRACKING MANUAL')
  end

  -- czy�cimy tabelk� z wyszukiwalnymi atrybutami
  truncate table bik_searchable_attr_val

  -- wo�amy pe�ne przetworzenie atrybut�w i zaindeksowanie
  exec sp_verticalize_node_attrs null

  -- (cd) ...albo log przycina ciebie!
  -- warto przyci�� loga po takich masowych operacjach
  exec('DBCC SHRINKFILE (N''' + @log_name + '''  , 0)')
end
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

exec sp_update_version '1.1.0.6', '1.1.0.7';
go
