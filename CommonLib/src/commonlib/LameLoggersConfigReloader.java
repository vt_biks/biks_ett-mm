/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.io.InputStream;
import java.util.Collections;
import simplelib.IResourceFromProvider;
import simplelib.IWatchedResourceFromProvider;
import simplelib.WatchedResourceFromProvider;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author pmielanczuk
 */
public class LameLoggersConfigReloader {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    private static IWatchedResourceFromProvider<InputStream> cfgRes;
    private static boolean nullCfgResWarned;

    public static void setLameLoggersConfigResource(IResourceFromProvider<InputStream> res) {
        if (logger.isStageEnabled()) {
            logger.stage("setLameLoggersConfigResource: setting config resource " + (cfgRes == null ? "for the first time" : "again"));
        }

        cfgRes = res == null ? null : new WatchedResourceFromProvider<InputStream>(res);
        nullCfgResWarned = false;
        reloadIfNeeded();
    }

    public static void reloadIfNeeded() {
        if (logger.isDebugEnabled()) {
            logger.debug("reloadIfNeeded!");
        }

        if (cfgRes == null) {
            if (!nullCfgResWarned) {
                if (logger.isStageEnabled()) {
                    logger.stage("reloadIfNeeded: cfgRes is null (one-time warning)");
                }
                nullCfgResWarned = true;
                LameLoggerFactory.applyLameLoggersConfig(Collections.<String, String>emptyMap(), false);
            }
            return;
        }

        if (cfgRes.hasBeenModifiedRecently()) {
            if (logger.isStageEnabled()) {
                logger.stage("reloading lameLoggersConfig");
            }
            //System.out.println("reloading lameLoggersConfig");
            InputStream is = cfgRes.gainResource();
            LameUtils.tryReadAndApplyLameLoggersConfig(is);
//            Properties lameLoggersConfig = LameUtils.readPropsFileFromInputStream(is,
//                    LAMELOGGERS_CONFIG_PROPERTIES_FILENAME, true);
//            if (lameLoggersConfig != null) {
//                LameLoggerFactory.applyLameLoggersConfig((Map) lameLoggersConfig, false);
//            }
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("lameLoggersConfig not modified recently");
            }
        }
    }
}
