/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.util.Set;
import pl.bssg.gwtservlet.ServerSideI18nSupportingServiceImplBase;
import pl.bssg.gwtservlet.WithoutAdhocSqls;
import pl.fovis.foxygwtcommons.i18nsupport.LocaleNegotiationResultBean;
import pl.fovis.foxygwtcommons.i18nsupport.ServerSideI18nSupportingService;

/**
 *
 * @author pmielanczuk
 */
@WithoutAdhocSqls
public class ServerSideI18nSupportingServiceImpl extends BIKSServiceBase implements ServerSideI18nSupportingService {

    protected ServerSideI18nSupportingService delegate;

    @Override
    protected void initWithConfig(BIKCenterConfigObj configBean) {
        super.initWithConfig(configBean);

        delegate = new ServerSideI18nSupportingServiceImplBase(getSessionDataProvider()) {

            @Override
            public Set<String> getServerSupportedLocaleNames() {
                return systemUsersSubservice.getServerSupportedLocaleNames();
            }
        };
    }

    @Override
    @PerformOnAnyDb
    public LocaleNegotiationResultBean setSessionLocaleName(String localeName, Set<String> clientSupportedLocaleNames) {
        return delegate.setSessionLocaleName(localeName, clientSupportedLocaleNames);
    }

    @Override
    public Set<String> getServerSupportedLocaleNames() {
        return delegate.getServerSupportedLocaleNames();
    }
}
