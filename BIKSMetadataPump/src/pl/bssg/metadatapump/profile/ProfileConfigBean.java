/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.profile;

/**
 *
 * @author tflorczak
 */
public class ProfileConfigBean {

    public Integer isActive;
    public String path;
    public String testPath;
    public String host;
    public String port;
    public String user;
    public String password;

    public ProfileConfigBean() {
    }

    public ProfileConfigBean(Integer isActive, String path, String testPath, String host, String port, String user, String password) {
        this.isActive = isActive;
        this.path = path;
        this.testPath = testPath;
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
    }
}
