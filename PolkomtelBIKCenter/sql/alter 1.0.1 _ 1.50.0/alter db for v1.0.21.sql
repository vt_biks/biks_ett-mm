exec sp_check_version '1.0.21';
go

create table bik_obj_in_body_node(
	id int identity(1,1) not null primary key,
	linked_node_id int not null references bik_node (id),
	blog_id int not null references bik_blog (id),
	constraint uk_bik_obj_in_body_node_id_node_id unique(linked_node_id, blog_id)
)
go

exec sp_update_version '1.0.21', '1.0.22';
go