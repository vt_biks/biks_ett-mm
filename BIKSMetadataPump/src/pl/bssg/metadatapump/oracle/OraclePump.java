/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.oracle;

import commonlib.LameUtils;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.AdhocPartialBeanCollector;
import pl.bssg.metadatapump.GenericNodeBean;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.common.ServerConfigBean;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class OraclePump extends MetadataPumpBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    public static final int DEFINITION_PACKAGE_SIZE = 50;
    public final static String NODE_KIND_SERVER = "OracleServer";
    public final static String NODE_KIND_OWNER = "OracleOwner";
    public final static String NODE_KIND_TABLE = "OracleTable";
    public final static String NODE_KIND_VIEW = "OracleView";
    public final static String NODE_KIND_PROCEDURE = "OracleProcedure";
    public final static String NODE_KIND_FUNCTION = "OracleFunction";
    public final static String NODE_KIND_PACKAGE = "OraclePackage";
    public final static String NODE_KIND_PACKAGE_METHOD = "OraclePackageMethod";
    public final static String NODE_KIND_COLUMN = "OracleColumn";
    public final static String NODE_KIND_COLUMN_PK = "OracleColumnPK";
    public final static String NODE_KIND_COLUMN_IDX = "OracleColumnIDX";
    public final static String NODE_KIND_FOLDER = "OracleFolder";
    //    public final static String OWNER_QUERY = "select distinct owner from all_objects";
    public final static String OWNER_QUERY = "select distinct owner from dba_segments where owner in (select username from dba_users where default_tablespace not in ('SYSTEM','SYSAUX'))";
//    public final static String OWNER_QUERY = "select distinct owner from all_objects where temporary = 'N' and owner not in ('SYSTEM', 'SYS', 'ORDSYS', 'OLAPSYS', 'AUDSYS', 'ORDPLUGINS', 'GSMADMIN_INTERNAL', 'OJVMSYS', 'ORDDATA', 'WMSYS', 'LBACSYS','DVSYS', 'CTXSYS', 'MDSYS', 'FLOWS_FILES', 'XDB', 'APEX_040200', 'APEX_LISTENER', 'ORACLE_OCM', 'APPQOSSYS', 'SI_INFORMTN_SCHEMA')";
//    public final static String OWNER_QUERY = "select username as owner from dba_users where username not in ('SYSTEM', 'SYS', 'SYSBACKUP', 'SYSDG', 'ORDSYS', 'SYSKM', 'OLAPSYS', 'XS$NULL', 'AUDSYS', 'ORDPLUGINS', 'GSMADMIN_INTERNAL', 'OJVMSYS', 'ORDDATA', 'WMSYS', 'LBACSYS','DVSYS', 'CTXSYS', 'MDSYS', 'FLOWS_FILES', 'XDB', 'APEX_040200', 'APEX_LISTENER', 'ORACLE_OCM', 'APPQOSSYS', 'SI_INFORMTN_SCHEMA')";
//    public final static String TABLE_QUERY = "select owner, table_name, iot_type from all_tables where owner in (select username from dba_users where default_tablespace not in ('SYSTEM','SYSAUX')) and temporary = 'N'";
    public final static String TABLE_QUERY = "select tab.owner, tab.table_name, tab.iot_type, comm.comments\n"
            + "from dba_tables tab left join dba_tab_comments comm on tab.owner = comm.owner and tab.table_name = comm.table_name and comm.comments is not null\n"
            + "where tab.temporary = 'N'";
//    public final static String VIEW_QUERY = "select owner, view_name, text from all_views where owner in (select username from dba_users where default_tablespace not in ('SYSTEM','SYSAUX'))";
    public final static String PACKAGE_QUERY = "select b.name, b.owner, b.type, b.line, b.text from dba_procedures a inner join dba_source b on a.object_name = b.name where b.type = 'PACKAGE' and a.owner = %{OWNER}% and b.owner = %{OWNER}% and a.procedure_name is null order by b.owner, b.name, b.type, b.line";
    public final static String PACKAGE_BODY_QUERY = "select b.name, b.line, b.text from dba_procedures a inner join dba_source b on a.object_name = b.name where b.type = 'PACKAGE BODY' and a.procedure_name is not null and a.owner = %{OWNER}% and b.owner = %{OWNER}% group by b.name, b.line, b.text order by b.name, b.line, b.text";
//    public final static String PACKAGE_METHOD_NAME_QUERY = "select object_name, procedure_name from dba_procedures where object_type = 'PACKAGE' and procedure_name is not null and owner = %{OWNER}% group by object_name, procedure_name order by object_name, procedure_name";
    public final static String PACKAGE_METHOD_NAME_QUERY = "select a.object_name, a.procedure_name from dba_procedures a inner join dba_source b on a.object_name = b.name where b.type = 'PACKAGE' and a.procedure_name is not null and a.owner = %{OWNER}% group by a.object_name, a.procedure_name order by a.object_name, a.procedure_name";
    public final static String VIEW_QUERY = "select vw.owner, vw.view_name, comm.comments, vw.text \n"
            + "from dba_views vw left join dba_tab_comments comm on vw.owner = comm.owner and vw.view_name = comm.table_name and comm.comments is not null";
    public final static String PROCEDURE_QUERY = "select b.name, b.owner, b.type, b.line, b.text from dba_procedures a inner join dba_source b on a.object_name = b.name where b.type in ('PROCEDURE', 'FUNCTION') order by b.name, b.owner, b.line";
//    public final static String COLUMN_QUERY = "select a.owner, a.table_name, a.column_name, a.data_type, a.data_length, a.data_precision, a.data_scale, a.nullable, a.char_used, a.column_id from ALL_TAB_COLUMNS a left join all_tables b on a.owner = b.owner and a.table_name = b.table_name where a.owner in (select username from dba_users where default_tablespace not in ('SYSTEM','SYSAUX')) and (b.temporary is null or b.temporary = 'N')";
    public final static String COLUMN_QUERY = "select a.owner, a.table_name, a.column_name, a.column_id, a.nullable, a.data_default, comm.comments,\n"
            + "case when a.data_type in ('CHAR','VARCHAR2','NCHAR','NVARCHAR2') then a.data_type || '(' || a.data_length || ' ' || case a.char_used when 'B' then 'BYTE' else 'CHAR' end || ')' else a.data_type end as type\n"
            + "from dba_tab_columns a\n"
            + "left join dba_tables b on a.owner = b.owner and a.table_name = b.table_name\n"
            + "left join dba_col_comments comm on a.owner = comm.owner and a.table_name = comm.table_name and a.column_name = comm.column_name and comm.comments is not null\n"
            + "where a.owner = %{OWNER}% and (b.temporary is null or b.temporary = 'N')";
    public final static String INDEX_QUERY = "select distinct b.index_name as name, b.table_owner as owner, b.table_name, b.column_name, b.column_position, b.descend, a.uniqueness, c.constraint_type from dba_indexes a inner join dba_ind_columns b on a.index_name = b.index_name left join dba_constraints c on a.index_name = c.index_name";
    public final static String DEPENDENCY_QUERY = "select owner, name, referenced_owner, referenced_name from dba_dependencies where type in ('VIEW', 'PROCEDURE', 'TABLE') \n"
            + "and referenced_type in ('TABLE', 'VIEW', 'PROCEDURE', 'FUNCTION')";
    public final static String FOREIGN_KEY_QUERY = "select a.constraint_name as name, c.owner as from_schema, a.table_name as from_table, a.column_name as from_column, \n"
            + "c.r_owner as to_schema, c_pk.table_name as to_table, re.column_name as to_column\n"
            + "from dba_cons_columns a\n"
            + "inner join dba_constraints c on a.owner = c.owner and a.constraint_name = c.constraint_name\n"
            + "inner join dba_constraints c_pk on c.r_owner = c_pk.owner and c.r_constraint_name = c_pk.constraint_name\n"
            + "inner join dba_cons_columns re on re.owner = c_pk.owner and re.constraint_name = c_pk.constraint_name and re.position = a.position\n"
            + "where c.constraint_type = 'R' order by a.constraint_name";
    public final static Integer DEFAULT_VISUAL_ORDER = 0;
    public final static Integer TABLE_VISUAL_ORDER = 1;
    public final static Integer VIEW_VISUAL_ORDER = 2;
    public final static Integer PROCEDURE_VISUAL_ORDER = 3;
    public final static Set<String> STRING_TYPES_SET = BaseUtils.splitUniqueBySep("CHAR,VARCHAR2,NCHAR,NVARCHAR2", ",");
    public final static String DRIVER_CLASS = "oracle.jdbc.driver.OracleDriver";
    protected static final Map<String, String> constraintTypeMap = new HashMap<String, String>() {

        {
            put("C", "check constraint");
//            put("P", "primary key"); // wyświetlamy już czy jest primary key - zbędne
//            put("U", "unique key"); // wyświetlamy już czy jest unique - zbędne
            put("R", "referential integrity");
            put("V", "with check option");
            put("O", "with read only");

        }
    };
    public final static Map<String, String> typeFolderNameMap = new HashMap<String, String>();
    public final static Map<String, Integer> typeFolderVisualOrderMap = new HashMap<String, Integer>();

    static {
        typeFolderNameMap.put(NODE_KIND_TABLE, "Tabele");
        typeFolderNameMap.put(NODE_KIND_VIEW, "Widoki");
        typeFolderNameMap.put(NODE_KIND_PROCEDURE, "Procedury");
        typeFolderNameMap.put(NODE_KIND_FUNCTION, "Funkcje");
        typeFolderNameMap.put(NODE_KIND_PACKAGE, "Pakiety");
        typeFolderVisualOrderMap.put(NODE_KIND_TABLE, 1);
        typeFolderVisualOrderMap.put(NODE_KIND_VIEW, 10);
        typeFolderVisualOrderMap.put(NODE_KIND_PROCEDURE, 20);
        typeFolderVisualOrderMap.put(NODE_KIND_FUNCTION, 30);
        typeFolderVisualOrderMap.put(NODE_KIND_PACKAGE, 40);
    }
//    protected List<OracleObjectBean> objectList = new LinkedList<OracleObjectBean>();
    protected List<OracleProcedureBean> procedureList = new ArrayList<OracleProcedureBean>();
//    protected List<OracleIndexBean> indexList = new LinkedList<OracleIndexBean>();
//    protected Set<String> primaryKeySet = new HashSet<String>();
//    protected Set<String> indexedColumnSet = new HashSet<String>();
//    protected Connection conn = null;
    protected AdhocPartialBeanCollector<GenericNodeBean> beanCollector;
    protected AdhocPartialBeanCollector<OracleIndexBean> beanIndexCollector;
    protected AdhocPartialBeanCollector<OracleColumnBean> beanColumnsCollector;
    protected AdhocPartialBeanCollector<OracleDefinitionBean> beanDefinitionCollector;
    protected AdhocPartialBeanCollector<OracleForeignKeyBean> beanForeignKeyCollector;
    protected AdhocPartialBeanCollector<OracleDependencyBean> beanDependencyCollector;
    protected Set<String> typeSet = new HashSet<String>();
    protected Set<String> objectLCSet = new HashSet<String>();
    protected List<String> duplicateObjectList = new ArrayList<String>();
    protected Map<String, Set<String>> serverDatabases = new HashMap<String, Set<String>>();
    protected List<Integer> successServerLoadList = new ArrayList<Integer>();

    protected Integer serversStepCount;
    protected Integer miniStep;
    protected Integer actualServerNumber;
    protected Integer actualStep;
    protected String serverNameWithSuffix;
    protected String serverName;
    protected String serverBranchName;
    protected Set<String> databases;
    protected Connection conn;
    protected Statement statement;
    protected List<String> databasesToFilter;
    protected List<String> packageNames = new ArrayList<String>();

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_ORACLE;
    }

    @Override
    protected Pair<Integer, String> startPump() {
        createCollectors();
        getMetadataFromServers();

        if (!beanCollector.wasSomethingAdded()) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "No connecting with Oracle server or lack of databases!");
        }

        execNamedCommandWithCommit("updateOracleKeys"); // update PK, FK
        execNamedCommandWithCommit("initBranchInAmgNode"); // init branch names
//        execNamedCommandWithCommit("deleteOracleOldTables");
//        pumpLogger.pumpLog("Moving data to temporary tables", 50);
//        insertListToDBInPackages(indexList, "insertOracleIndexMetadata");
        if (logger.isInfoEnabled()) {
            logger.info("Update BIKS structure!");
        }
        pumpLogger.pumpLog("Update BIKS structure", 80);
        execNamedCommandWithCommit("insertOracleIntoBikNode", serverDatabases);
        pumpLogger.pumpLog("Insert Oracle extradata", 90);
        execNamedCommandWithCommit("moveOracleMetadataExtradata", serverDatabases);
        execNamedCommandWithCommit("fixDatabasesNodesAfterDeactivate", successServerLoadList);
        if (logger.isInfoEnabled()) {
            logger.info("All right. Success!");
        }
        return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
    }

    protected void getMetadataFromServers() {
        // get connection properties
        typeSet.clear();
        serverDatabases.clear();
        successServerLoadList.clear();
        List<ServerConfigBean> servers = adhocDao.createBeansFromNamedQry("getDBConfigForDB", ServerConfigBean.class, PumpConstants.SOURCE_NAME_ORACLE, instanceName);
        if (servers == null || servers.isEmpty()) {
            System.out.println("No servers found for source: " + PumpConstants.SOURCE_NAME_ORACLE + " and instance: " + instanceName);
        }
        Boolean useBasicModeInDBConnectors = BaseUtils.tryParseBoolean((String) execNamedQuerySingleVal("getAppPropValue", true, "val", "useBasicModeInDBConnectors"), false);
        // clear old values
        serversStepCount = 80 / servers.size();
        miniStep = serversStepCount / 10; // 8 = {owner, table, view, procedure, columns, Indieces, Foreign Keys, dependency, package, package's method}
        actualServerNumber = 0;
        actualStep = 0;
        execNamedCommandWithCommit("deleteOracleTempTables");
        for (ServerConfigBean serv : servers) {
            actualStep = 0;
            serverNameWithSuffix = serv.name;
            serverName = PumpUtils.getServerNameWithoutSuffix(serverNameWithSuffix);
            serverBranchName = serverName + "|";
            conn = null;
            databases = new HashSet<String>();
            try {
//                jdbc:oracle:thin:@[HOST][:PORT]:SID               // Old syntax
//                jdbc:oracle:thin:@//[HOST][:PORT]/SERVICE         // New syntax
//                String url = "jdbc:oracle:thin:@//192.168.167.56:1521/XE";
                StringBuilder url = new StringBuilder();
                url.append("jdbc:oracle:thin:@//").append(serv.server).append(":").append(serv.port).append("/").append(serv.instance);
                conn = getConnection(DRIVER_CLASS, url.toString(), serv.user, serv.password);
                if (conn == null) {
                    pumpLogger.pumpLog("No connecting with server: " + serv.server, null);
                    continue;
                }
                statement = conn.createStatement();
                execNamedCommandWithCommit("addServerIfNotExists", serverName, NODE_KIND_SERVER);
                databasesToFilter = BaseUtils.splitBySep(serv.databaseFilter, ",", true);
                objectLCSet.clear();
                duplicateObjectList.clear();
//                long start = System.currentTimeMillis();
                getOwners(serv);
//                System.out.println("Get owners: " + (System.currentTimeMillis() - start) / 1000.0 + "s");
//                start = System.currentTimeMillis();
                getTables(serv);
//                System.out.println("Get tables: " + (System.currentTimeMillis() - start) / 1000.0 + "s");
//                start = System.currentTimeMillis();
                getViews(serv);
//                System.out.println("Get views: " + (System.currentTimeMillis() - start) / 1000.0 + "s");
//                start = System.currentTimeMillis();
                getProcedures(serv);
//                System.out.println("Get procedures: " + (System.currentTimeMillis() - start) / 1000.0 + "s");
//                start = System.currentTimeMillis();
                // basic mode - kończymy pobieranie
                if (useBasicModeInDBConnectors) {
//                    System.out.println("Basic mode ORACLE! Instance: " + instanceName);
                    addToSuccessDBServer(serv, serverName, databases);
                    continue;
                }
                getPackages(serv);
//                System.out.println("Get packages: " + (System.currentTimeMillis() - start) / 1000.0 + "s");
//                start = System.currentTimeMillis();
                getPackageMethods(serv);
//                System.out.println("Get package's methods: " + (System.currentTimeMillis() - start) / 1000.0 + "s");
//                start = System.currentTimeMillis();
                getColumns(serv);
//                System.out.println("Get columns: " + (System.currentTimeMillis() - start) / 1000.0 + "s");
//                start = System.currentTimeMillis();
                getIndices(serv);
//                System.out.println("Get indices: " + (System.currentTimeMillis() - start) / 1000.0 + "s");
//                start = System.currentTimeMillis();
                getForeignKeys(serv);
//                System.out.println("Get foreign keys: " + (System.currentTimeMillis() - start) / 1000.0 + "s");
//                start = System.currentTimeMillis();
                getDependencies(serv);
//                System.out.println("Get dependencies: " + (System.currentTimeMillis() - start) / 1000.0 + "s");
//                start = System.currentTimeMillis();

                addToSuccessDBServer(serv, serverName, databases);
            } catch (Exception e) {
                if (logger.isErrorEnabled()) {
                    logger.error("Error for Oracle server: " + serverNameWithSuffix, e);
                }
                pumpLogger.pumpLog(serverNameWithSuffix + " - error: " + e.getMessage(), null);
            } finally {
                if (conn != null) {
                    closeConnection(conn);
                    conn = null;
                }
            }
            actualServerNumber++;
            flushCollectors();
        }
    }

    protected void addToSuccessDBServer(ServerConfigBean serv, String serverName, Set<String> databases) {
        successServerLoadList.add(serv.id);
        Set<String> setOnMap = serverDatabases.get(serverName);
        if (setOnMap == null) {
            setOnMap = new HashSet<String>();
        }
        setOnMap.addAll(databases);
        serverDatabases.put(serverName, setOnMap);
    }

    protected void addProcedureOrFunction(StringBuilder sb, String serverBranchName, OracleProcedureBean procedure, String serverName, String type) {
        String procedureDefinition = sb.toString();
        String parentKey = serverBranchName + procedure.owner + "|$" + type + "|";
        String objectBranch = serverBranchName + procedure.owner + "|" + procedure.name + "|";
        String objectBranchLC = objectBranch.toLowerCase();
        if (!objectLCSet.contains(objectBranchLC)) {
            objectLCSet.add(objectBranchLC);
            optAddTypeFolderIfNeeded(serverName, procedure.owner, type, parentKey);
            beanCollector.add(new GenericNodeBean(objectBranch, parentKey, type, procedure.name, null));
            if (!BaseUtils.isStrEmptyOrWhiteSpace(procedureDefinition)) {
                beanDefinitionCollector.add(new OracleDefinitionBean(serverName, procedure.owner, procedure.name, procedureDefinition));
            }
        } else {
            duplicateObjectList.add(objectBranch);
        }
    }

//    @Override
//    public String getPumpGroup() {
//        return PumpConstants.PUMP_GROUP_ORACLE;
//    }
    protected void createCollectors() {
        beanCollector = new AdhocPartialBeanCollector<GenericNodeBean>(PACKAGE_SIZE, adhocDao, "insertIntoGenericTable");
        beanDefinitionCollector = new AdhocPartialBeanCollector<OracleDefinitionBean>(DEFINITION_PACKAGE_SIZE, adhocDao, "insertOracleDefinitionMetadata");
        beanIndexCollector = new AdhocPartialBeanCollector<OracleIndexBean>(PACKAGE_SIZE, adhocDao, "insertOracleIndexMetadata");
        beanColumnsCollector = new AdhocPartialBeanCollector<OracleColumnBean>(PACKAGE_SIZE, adhocDao, "insertOracleColumnMetadata");
        beanForeignKeyCollector = new AdhocPartialBeanCollector<OracleForeignKeyBean>(PACKAGE_SIZE, adhocDao, "insertOracleForeignKeyMetadata");
        beanDependencyCollector = new AdhocPartialBeanCollector<OracleDependencyBean>(PACKAGE_SIZE, adhocDao, "insertOracleDependencyMetadata");
    }

    protected void flushCollectors() {
        beanCollector.flush();
        beanIndexCollector.flush();
        beanColumnsCollector.flush();
        beanDefinitionCollector.flush();
        beanForeignKeyCollector.flush();
        beanDependencyCollector.flush();
    }

    protected void optAddTypeFolderIfNeeded(String servName, String owner, String objectType, String folderKey) {
        if (!typeSet.contains(folderKey)) {
            beanCollector.add(new GenericNodeBean(folderKey, servName + "|" + owner + "|", NODE_KIND_FOLDER, typeFolderNameMap.get(objectType), null, typeFolderVisualOrderMap.get(objectType)));
            typeSet.add(folderKey);
        }
    }

    protected void getOwners(ServerConfigBean serv) throws SQLException {
// get owners
        pumpLogger.pumpLog(serverNameWithSuffix + ": loading: owners", actualServerNumber * serversStepCount + miniStep * (actualStep++));
        ResultSet owners = statement.executeQuery(OWNER_QUERY);
        while (owners.next()) {
            String owner = owners.getString("owner");
            if (databasesToFilter.isEmpty()) { // nie ma filtru na bazy danych
                databases.add(owner);
            } else {
                for (String dbF : databasesToFilter) {
                    if (dbF.contains(owner)) {
                        databases.add(owner);
                        break;
                    }
                }
            }
        }
        for (String database : databases) {
            beanCollector.add(new GenericNodeBean(serverBranchName + database + "|", serverBranchName, NODE_KIND_OWNER, database, null));
        }
        // set databases for server
        execNamedCommandWithCommit("setDatabasesForServer", serv.id, BaseUtils.mergeWithSepEx(databases, ","));
    }

    protected void getTables(ServerConfigBean serv) throws SQLException {
        // get tables
        pumpLogger.pumpLog(serverNameWithSuffix + ": loading: tables", actualServerNumber * serversStepCount + miniStep * (actualStep++));
        ResultSet tables = statement.executeQuery(TABLE_QUERY);
        while (tables.next()) {
            String owner = tables.getString("owner");
            if (!databases.contains(owner)) {
                continue;
            }
            String table = tables.getString("table_name");
            String comment = tables.getString("comments");
//                    String indexOrganized = tables.getString("iot_type");
            String parentKey = serverBranchName + owner + "|$" + NODE_KIND_TABLE + "|";
            String objectBranch = serverBranchName + owner + "|" + table + "|";
            String objectBranchLC = objectBranch.toLowerCase();
            if (!objectLCSet.contains(objectBranchLC)) {
                objectLCSet.add(objectBranchLC);
                optAddTypeFolderIfNeeded(serverName, owner, NODE_KIND_TABLE, parentKey);
                beanCollector.add(new GenericNodeBean(objectBranch, parentKey, NODE_KIND_TABLE, table, comment));
            } else {
                duplicateObjectList.add(objectBranch);
            }
        }
    }

    protected void getViews(ServerConfigBean serv) throws SQLException {
//get views
        pumpLogger.pumpLog(serverNameWithSuffix + ": loading: views", actualServerNumber * serversStepCount + miniStep * (actualStep++));
        ResultSet views = statement.executeQuery(VIEW_QUERY);
        while (views.next()) {
            String owner = views.getString("owner");
            if (!databases.contains(owner)) {
                continue;
            }
            String view = views.getString("view_name");
            String text = views.getString("text");
            String comment = views.getString("comments");
            String parentKey = serverBranchName + owner + "|$" + NODE_KIND_VIEW + "|";
            String objectBranch = serverBranchName + owner + "|" + view + "|";
            String objectBranchLC = objectBranch.toLowerCase();
            if (!objectLCSet.contains(objectBranchLC)) {
                objectLCSet.add(objectBranchLC);
                optAddTypeFolderIfNeeded(serverName, owner, NODE_KIND_VIEW, parentKey);
                beanCollector.add(new GenericNodeBean(objectBranch, parentKey, NODE_KIND_VIEW, view, comment));
                if (!BaseUtils.isStrEmptyOrWhiteSpace(text)) {
                    beanDefinitionCollector.add(new OracleDefinitionBean(serverName, owner, view, text));
                }
            } else {
                duplicateObjectList.add(objectBranch);
            }
        }
    }

    protected void getProcedures(ServerConfigBean serv) throws SQLException {
// get procedures
        pumpLogger.pumpLog(serverNameWithSuffix + ": loading: procedures", actualServerNumber * serversStepCount + miniStep * (actualStep++));
        procedureList.clear();
        ResultSet procedures = statement.executeQuery(PROCEDURE_QUERY);
        int procId = 1;
        while (procedures.next()) {
            String owner = procedures.getString("owner");
            if (!databases.contains(owner)) {
                continue;
            }
            String proc = procedures.getString("name");
            String type = procedures.getString("type");
            Integer line = BaseUtils.tryParseInteger(procedures.getString("line"));
            String text = procedures.getString("text");
            procedureList.add(new OracleProcedureBean(procId++, proc, owner, type, line, text));
        }
        StringBuilder sb = new StringBuilder();
        OracleProcedureBean oldProcedure = null;
        for (OracleProcedureBean proc : procedureList) {
            if (oldProcedure == null || !(proc.uniqueProcedureName).equals(oldProcedure.uniqueProcedureName)) {
                if (oldProcedure != null) {
                    addProcedureOrFunction(sb, serverBranchName, oldProcedure, serverName, "PROCEDURE".equals(oldProcedure.type) ? NODE_KIND_PROCEDURE : NODE_KIND_FUNCTION);
                    sb = new StringBuilder();
                }
                oldProcedure = proc;
            }
            sb.append(proc.text);
            if (proc.id == procedureList.size()) {
                addProcedureOrFunction(sb, serverBranchName, proc, serverName, "PROCEDURE".equals(proc.type) ? NODE_KIND_PROCEDURE : NODE_KIND_FUNCTION);
            }
        }
    }

    protected void getColumns(ServerConfigBean serv) throws SQLException {
        // get columns
        int dbIdx = 1;
        for (String database : databases) {
            pumpLogger.pumpLog(serverNameWithSuffix + ": loading: columns for schema: " + database + " (" + dbIdx++ + " / " + databases.size() + ")", actualServerNumber * serversStepCount + miniStep * (actualStep));
            ResultSet columns = statement.executeQuery(COLUMN_QUERY.replace("%{OWNER}%", LameUtils.toSqlString(database, LameUtils.SqlFlavor.Other)));
            while (columns.next()) {
                String owner = columns.getString("owner");
                if (!databases.contains(owner)) {
                    continue;
                }
                String table = columns.getString("table_name");
                String column = columns.getString("column_name");
                String type = columns.getString("type");
                int columnId = BaseUtils.tryParseInt(columns.getString("column_id"));
                String nullable = columns.getString("nullable");
                String comment = columns.getString("comments");
//                    String defaultValue = columns.getString("data_default");
                boolean isNullable = "Y".equals(nullable);
                String objectParentBranch = serverBranchName + owner + "|" + table + "|";
                String objectParentBranchLC = objectParentBranch.toLowerCase();
                if (objectLCSet.contains(objectParentBranchLC) && !duplicateObjectList.contains(objectParentBranch)) {
                    beanColumnsCollector.add(new OracleColumnBean(column, owner, table, serverName, columnId, comment, isNullable, type.toLowerCase(), null));
                    beanCollector.add(new GenericNodeBean(objectParentBranch + column + "|", objectParentBranch, NODE_KIND_COLUMN, column, comment, columnId));
                }
            }
        }
        actualStep++;
    }

    protected void getIndices(ServerConfigBean serv) throws SQLException {
// get indices
        pumpLogger.pumpLog(serverNameWithSuffix + ": loading: indices", actualServerNumber * serversStepCount + miniStep * (actualStep++));
        ResultSet indices = statement.executeQuery(INDEX_QUERY);
        while (indices.next()) {
            String owner = indices.getString("owner");
            if (!databases.contains(owner)) {
                continue;
            }
            String index = indices.getString("name");
            String table = indices.getString("table_name");
            String column = indices.getString("column_name");
            int position = BaseUtils.tryParseInt(indices.getString("column_position"));
            String descend = indices.getString("descend");
            String uniqueness = indices.getString("uniqueness");
            String constraintType = indices.getString("constraint_type");
            boolean isUnique = "UNIQUE".equals(uniqueness);
            boolean isPrimary = "P".equals(constraintType);
            beanIndexCollector.add(new OracleIndexBean(index, serverName, owner, table, column, position, descend, isUnique, constraintTypeMap.get(constraintType), isPrimary));
        }
    }

    protected void getForeignKeys(ServerConfigBean serv) throws SQLException {
        // get foreign keys
        pumpLogger.pumpLog(serverNameWithSuffix + ": loading: foreign keys", actualServerNumber * serversStepCount + miniStep * (actualStep++));
        ResultSet foreignKeys = statement.executeQuery(FOREIGN_KEY_QUERY);
        while (foreignKeys.next()) {
            // name, from_schema, from_table, from_column, to_schema, to_table, to_column
            String name = foreignKeys.getString("name");
            String fromSchema = foreignKeys.getString("from_schema");
            if (!databases.contains(fromSchema)) {
                continue;
            }
            String fromTable = foreignKeys.getString("from_table");
            String fromColumn = foreignKeys.getString("from_column");
            String toSchema = foreignKeys.getString("to_schema");
            String toTable = foreignKeys.getString("to_table");
            String toColumn = foreignKeys.getString("to_column");
            beanForeignKeyCollector.add(new OracleForeignKeyBean(name, null, serverName, fromSchema, fromTable, fromColumn, toSchema, toTable, toColumn));
        }
    }

    protected void getDependencies(ServerConfigBean serv) throws SQLException {
// get dependencies
        pumpLogger.pumpLog(serverNameWithSuffix + ": loading: dependency", actualServerNumber * serversStepCount + miniStep * (actualStep++));
        ResultSet dependencies = statement.executeQuery(DEPENDENCY_QUERY);
        while (dependencies.next()) {
            // owner, name, referenced_owner, referenced_name
            String owner = dependencies.getString("owner");
            if (!databases.contains(owner)) {
                continue;
            }
            String name = dependencies.getString("name");
            String refOwner = dependencies.getString("referenced_owner");
            String refName = dependencies.getString("referenced_name");
            beanDependencyCollector.add(new OracleDependencyBean(serverName, owner, name, refOwner, refName));
        }
    }

    protected void getPackages(ServerConfigBean serv) throws SQLException {
        // get packages
        pumpLogger.pumpLog(serverNameWithSuffix + ": loading: packages", actualServerNumber * serversStepCount + miniStep * (actualStep++));
//        double s = 0;
        for (String owner : databases) {
//            long start = System.currentTimeMillis();
            ResultSet packages = statement.executeQuery(PACKAGE_QUERY.replace("%{OWNER}%", LameUtils.toSqlString(owner, LameUtils.SqlFlavor.Other)));
//            System.out.println("Owner: " + owner + ": " + (System.currentTimeMillis() - start) / 1000.0);
//            s += (System.currentTimeMillis() - start) / 1000.0;
            StringBuilder sb = null;
            String lastPackageName = null;
            while (packages.next()) {
                String name = packages.getString("name");
                Integer lineNr = BaseUtils.tryParseInteger(packages.getString("line"));
                String text = packages.getString("text");

                if (lineNr == 1) {
                    if (sb != null) {
                        addPackage(sb, owner, lastPackageName);
                        sb = null;
                    }
                    sb = new StringBuilder(text);
                } else {
                    sb.append(text);
                }
                lastPackageName = name;
            }
            if (sb != null && !sb.toString().isEmpty()) {
                addPackage(sb, owner, lastPackageName);
                sb = null;
            }
        }
//        System.out.println("Query: " + s);
    }

    protected void addPackage(StringBuilder sb, String owner, String packageName) {
        packageNames.add(packageName);
        String packageDefinition = sb.toString();
        String parentKey = serverBranchName + owner + "|$" + NODE_KIND_PACKAGE + "|";
        String objectBranch = serverBranchName + owner + "|" + packageName + "|";
        String objectBranchLC = objectBranch.toLowerCase();
        if (!objectLCSet.contains(objectBranchLC)) {
            objectLCSet.add(objectBranchLC);
            optAddTypeFolderIfNeeded(serverName, owner, NODE_KIND_PACKAGE, parentKey);
            beanCollector.add(new GenericNodeBean(objectBranch, parentKey, NODE_KIND_PACKAGE, packageName, null));
            if (!BaseUtils.isStrEmptyOrWhiteSpace(packageDefinition)) {
                beanDefinitionCollector.add(new OracleDefinitionBean(serverName, owner, packageName, packageDefinition));
            }
        } else {
            duplicateObjectList.add(objectBranch);
        }
    }

    protected void getPackageMethods(ServerConfigBean serv) throws SQLException {
        // get package's methods
        pumpLogger.pumpLog(serverNameWithSuffix + ": loading: package's methods", actualServerNumber * serversStepCount + miniStep * (actualStep++));
//        double processingTime = 0;
        for (String owner : databases) {
            Map<String, String> packageBodies = new HashMap<String, String>();
            getPackagesBodies(owner, packageBodies);

//            long start = System.nanoTime();
            ResultSet methods = statement.executeQuery(PACKAGE_METHOD_NAME_QUERY.replace("%{OWNER}%", LameUtils.toSqlString(owner, LameUtils.SqlFlavor.Other)));
            while (methods.next()) {
                String packageName = methods.getString("object_name");
                String methodName = methods.getString("procedure_name");
                addPackageMethod(packageBodies.get(packageName), owner, packageName, methodName);
            }
//            processingTime += (System.nanoTime() - start) / 1000000000.0;
        }
//        System.out.println("processingTime = " + processingTime);
    }

    protected void addPackageMethod(String methodDefinition, String owner, String packageName, String methodName) {
        String parentKey = serverBranchName + owner + "|" + packageName + "|";
        String objectBranch = serverBranchName + owner + "|" + packageName + "|" + methodName + "|";
        String objectBranchLC = objectBranch.toLowerCase();
        if (!objectLCSet.contains(objectBranchLC)) {
            objectLCSet.add(objectBranchLC);
//        optAddTypeFolderIfNeeded(serverName, owner, NODE_KIND_PROCEDURE, objectBranch);
            beanCollector.add(new GenericNodeBean(objectBranch, parentKey, NODE_KIND_PROCEDURE, methodName, null));
            if (!BaseUtils.isStrEmptyOrWhiteSpace(methodDefinition)) {
                beanDefinitionCollector.add(new OracleDefinitionBean(serverName, owner, packageName + "|" + methodName, methodDefinition));
            }
        } else {
            duplicateObjectList.add(objectBranch);
        }
    }

    protected void getPackagesBodies(String owner, Map<String, String> packageBodies) throws SQLException {
        ResultSet packageContents = statement.executeQuery(PACKAGE_BODY_QUERY.replace("%{OWNER}%", LameUtils.toSqlString(owner, LameUtils.SqlFlavor.Other)));
//            System.out.println("Owner = " + owner + ": " + (System.currentTimeMillis() - start) / 1000.0);
        StringBuilder sb = null;
        String lastPackageName = null, packageName = null, text = null;
        Integer lineNr = null;
//            System.out.println("chung timing: " + System.nanoTime());
//        int nLine = 0;
        while (packageContents.next()) {
//            nLine++;
//                System.out.println("chung timing2: " + System.nanoTime());
            packageName = packageContents.getString("name");
            lineNr = BaseUtils.tryParseInteger(packageContents.getString("line"));
            text = packageContents.getString("text");
            if (lineNr == 1) {
                if (sb != null) {
                    packageBodies.put(lastPackageName, sb.toString());
                    //                        addPackageMethod(sb, owner, lastPackageName, lastMethodName);
                    sb = null;
                }
                sb = new StringBuilder(text);
            } else {
                sb.append(text);
            }
            lastPackageName = packageName;
            //                System.out.println("ch/ung timing3: " + System.nanoTime());
        }
//            System.out.println("chung timing4: " + System.nanoTime());
        if (sb != null && !sb.toString().isEmpty()) {
            packageBodies.put(lastPackageName, sb.toString());
//                addPackageMethod(sb, owner, lastPackageName, lastMethodName);
            sb = null;
        }
//        System.out.println("nLine = " + nLine);
    }
}
