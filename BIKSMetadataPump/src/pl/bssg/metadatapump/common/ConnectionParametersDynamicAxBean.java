/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import simplelib.BeanWithIntIdAndNameBase;

/**
 *
 * @author ctran
 */
public class ConnectionParametersDynamicAxBean extends BeanWithIntIdAndNameBase {

    public int isActive;
    public String xpoFilePath;
    public int isRemote;
    public String domain;
    public String user;
    public String password;
    public String treeCode;
}
