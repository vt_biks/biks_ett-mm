/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BOXIServiceAsync;
import pl.fovis.client.bikpages.BikTreeWidget;
import pl.fovis.client.bikpages.IPageDataFetchBroker;
import pl.fovis.client.bikpages.PageDataFetchBroker;
import pl.fovis.client.bikpages.TreeDataFetchAsyncCallback;
import pl.fovis.client.treeandlist.TreeNodeBeanAsMapStorage;
import pl.fovis.common.TreeNodeBean;
import simplelib.BaseUtils;

/**
 *
 * @author pgajda
 */
public class TreeOfRoles {

    protected BikTreeWidget bikTree;
    protected IPageDataFetchBroker dataFetchBroker = new PageDataFetchBroker();
    protected boolean useLazyLoading = true;//false; //

    public Widget buildWidgets() {

        VerticalPanel main = new VerticalPanel();
        main.setSize("100%", "100%");

        bikTree = new BikTreeWidget(true,
                dataFetchBroker,
                true,
                TreeNodeBeanAsMapStorage.NAME_FIELD_NAME + "=300:Nazwa");

        Widget treeWidget = bikTree.getTreeGridWidget();
        treeWidget.setSize("100%", "100%");
        main.add(treeWidget);

        fetchData();
        return main;
    }

    public Integer getSelectedNode() {
        return bikTree != null ? bikTree.getCurrentNodeId() : null;
    }

    public int getIdOfSelectedRole() {
        if (bikTree != null) {
            return BaseUtils.tryParseInt(bikTree.getCurrentNodeBean().objId);
        } else {
            return 0;
        }
    }

    public String getNameOfSelectedRole() {
        if (bikTree != null) {
            return BaseUtils.safeToStringDef(bikTree.getCurrentNodeBean().name, "");
        } else {
            return "";
        }
    }

    protected void processAndShowNodes(List<TreeNodeBean> result, Integer optNodeIdToSelect) {
        bikTree.initWithFullData(result, useLazyLoading, null, null);
        bikTree.selectEntityById(optNodeIdToSelect);
    }

    protected void fetchData(final Integer optNodeIdToSelect) {
        callServiceToGetData(new TreeDataFetchAsyncCallback<List<TreeNodeBean>>(dataFetchBroker) {
            protected void doOnSuccess(List<TreeNodeBean> result) {
                processAndShowNodes(result, optNodeIdToSelect);
            }
        });
    }

    protected void fetchData() {
        fetchData(null);
    }

    protected BOXIServiceAsync getService() {
        return BIKClientSingletons.getService();
    }

    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        getService().getBikRolesTreeNodes(useLazyLoading, callback);
    }
}
