/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.invokers;

import java.util.HashMap;
import java.util.Map;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public class OneClassMethodInvoker<T> implements IDynamicMethodInvoker<T> {

    Map<String, IOneMethodInvoker<T>> uniqueByName = new HashMap<String, IOneMethodInvoker<T>>();
    Map<Pair<String, Integer>, IOneMethodInvoker<T>> uniqueByNameAndArity = new HashMap<Pair<String, Integer>, IOneMethodInvoker<T>>();

    protected void addWithNameAndArity(String methodName, IOneMethodInvoker<T> invoker) {
        int arity = invoker.getArity();
        Pair<String, Integer> nameWithArity = new Pair<String, Integer>(methodName, arity);
        if (uniqueByNameAndArity.get(nameWithArity) != null) {
            throw new LameRuntimeException("cannot add another invoker for method " + methodName + " with arity " + arity);
        }
        uniqueByNameAndArity.put(nameWithArity, invoker);
    }

    public void addInvoker(String methodName, IOneMethodInvoker<T> invoker) {
        IOneMethodInvoker<T> prevUniqueOne = uniqueByName.get(methodName);
        boolean hasManyByName = prevUniqueOne != null || uniqueByName.containsKey(methodName);

        if (hasManyByName) {
            if (prevUniqueOne != null) {
                addWithNameAndArity(methodName, prevUniqueOne);
                uniqueByName.put(methodName, null);
            }
            addWithNameAndArity(methodName, invoker);
        } else {
            uniqueByName.put(methodName, invoker);
        }
    }

    public IOneMethodInvoker<T> findInvoker(String methodName, Object... args) {
        IOneMethodInvoker<T> invoker = uniqueByName.get(methodName);
        if (invoker == null) {
            int arity = args.length;
            Pair<String, Integer> nameAndArity = new Pair<String, Integer>(methodName, arity);
            invoker = uniqueByNameAndArity.get(nameAndArity);
        }
        return invoker;
    }

    public boolean isInvokerCompatibleWithArgs(IOneMethodInvoker<T> invoker, Object... args) {
        int arity = args.length;
        if (arity != invoker.getArity()) {
            return false;
        }

        for (int i = 0; i < arity; i++) {
            if (!invoker.isParamAssignableFrom(i, args[i])) {
                return false;
            }
        }

        return true;
    }

    public Object invokeMethod(T obj, String methodName, Object... args) {
        IOneMethodInvoker<T> invoker = findInvoker(methodName, args);

        if (invoker == null) {
            throw new LameRuntimeException("no method with name " + methodName + " with arity " + args.length
                    + " for object of class " + BaseUtils.safeGetClassName(obj));
        }

        //ww: bez sprawdzania poprawności typów argumentów? na razie sprawdzimy
        if (!isInvokerCompatibleWithArgs(invoker, args)) {
            throw new LameRuntimeException("method " + methodName + " in object of class "
                    + BaseUtils.safeGetClassName(obj) + " cannot be invoked with given param values classes: "
                    + BaseUtils.objectsToClassList(args));
        }

        return invoker.invoke(obj, args);
    }

    public static void main(String[] args) {
        class Test1 {

            protected int z;

            public Test1(int z) {
                this.z = z;
            }

            public void doIt(int x) {
                System.out.println("z+x=" + (z + x));
            }

            public String calc(int x, String s) {
                return z + s + x;
            }
        }
        OneClassMethodInvoker<Test1> ocmi = new OneClassMethodInvoker<Test1>();
        ocmi.addInvoker("doIt", new IOneMethodInvoker<Test1>() {
            public Object invoke(Test1 obj, Object... args) {
                int arg0 = (Integer) args[0];
                obj.doIt(arg0);
                return null;
            }

            public boolean isParamAssignableFrom(int paramIdx, Object argVal) {
                switch (paramIdx) {
                    case 0:
                        return argVal instanceof Integer;
                    default:
                        throw new LameRuntimeException("too many arguments: " + paramIdx);
                }
            }

            public int getArity() {
                return 1;
            }
        });
        ocmi.addInvoker("calc", new IOneMethodInvoker<Test1>() {
            public Object invoke(Test1 obj, Object... args) {
                int arg0 = (Integer) args[0];
                String arg1 = (String) args[1];
                return obj.calc(arg0, arg1);
            }

            public boolean isParamAssignableFrom(int paramIdx, Object argVal) {
                switch (paramIdx) {
                    case 0:
                        return argVal instanceof Integer;
                    case 1:
                        return argVal instanceof String;
                    default:
                        throw new LameRuntimeException("too many arguments: " + paramIdx);
                }
            }

            public int getArity() {
                return 2;
            }
        });

        Test1 t1 = new Test1(1);
        Test1 t2 = new Test1(2);
        ocmi.invokeMethod(t1, "doIt", 10);
        ocmi.invokeMethod(t1, "doIt", 20);
        ocmi.invokeMethod(t2, "doIt", 20);
        System.out.println("res calc=" + ocmi.invokeMethod(t2, "calc", 42, "aqq"));

        class Test2 extends Test1 {

            Test2(int z) {
                super(z);
            }

            @Override
            public String calc(int x, String s) {
                return s + z + s + x;
            }
        }

        System.out.println("res calc on Test2=" + ocmi.invokeMethod(new Test2(333), "calc", 666, "hehe"));
    }
}
