/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.xml.builders.BooleanQueryBuilder;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import pl.fovis.common.SimilarNodeBean;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;

/**
 *
 * @author pmielanczuk
 */
public class FtsSearchByLucene {

    public static Analyzer createAnalyzer() {
        return FtsByLucene.createAnalyzer();
    }

    protected static void searchAndPrint(String queryString) {
        searchAndPrint(queryString, null);
    }

    protected static void searchAndPrint(String queryString, String optFieldName) {
        try {
            searchAndPrintInner(queryString, optFieldName);
        } catch (Exception ex) {
            throw new LameRuntimeException("error in searchAndPrint, queryString=[" + queryString + "]", ex);
        }
    }

    protected static void searchAndPrintInner(String queryString, String optFieldName) throws IOException, ParseException {
        String queries = null;
        int repeat = 0;
        boolean raw = false;
//        String queryString = null;
        int hitsPerPage = 100;
//        String field = "contents";
        String field = optFieldName != null ? optFieldName : FtsByLucene.COLLECTIVE_FIELD_NAME;

        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(FtsByLucene.INDEX_PATH)));
        IndexSearcher searcher = new IndexSearcher(reader);
        Analyzer analyzer = createAnalyzer();

        BufferedReader in;
        if (queries != null) {
            in = Files.newBufferedReader(Paths.get(queries), StandardCharsets.UTF_8);
        } else {
            in = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
        }

        QueryParser parser = new QueryParser(field, analyzer);
        parser.setAllowLeadingWildcard(true);
        parser.setDefaultOperator(QueryParser.Operator.AND);
//add all possible fileds in multifieldqueryparser using indexreader getFieldNames method
//        QueryParser parser = null;//new MultiFieldQueryParser(reader.GetFieldNames(IndexReader.FieldOption.ALL).ToArray(), analyzer);

        while (true) {
            if (queries == null && queryString == null) {                        // prompt the user
                System.out.println("Enter query: ");
            }

            String line = queryString != null ? queryString : in.readLine();

            if (line == null || line.length() == -1) {
                break;
            }

            line = line.trim();
            if (line.length() == 0) {
                break;
            }

            Query query = parser.parse(line);
//            System.out.println("Searching for: " + query.toString(field));
            System.out.println("Searching for: " + query.toString());

            if (repeat > 0) {                           // repeat & time as benchmark
                Date start = new Date();
                for (int i = 0; i < repeat; i++) {
                    searcher.search(query, 100);
                }
                Date end = new Date();
                System.out.println("Time: " + (end.getTime() - start.getTime()) + "ms");
            }

            doPagingSearch(in, searcher, query, hitsPerPage, raw, queries == null && queryString == null);

            if (queryString != null) {
                break;
            }
        }
        reader.close();

    }

    public static void doPagingSearch(BufferedReader in, IndexSearcher searcher, Query query,
            int hitsPerPage, boolean raw, boolean interactive) throws IOException {

        // Collect enough docs to show 5 pages
        TopDocs results = searcher.search(query, 5 * hitsPerPage);
        ScoreDoc[] hits = results.scoreDocs;

        int numTotalHits = results.totalHits;
        System.out.println(numTotalHits + " total matching documents");

        int start = 0;
        int end = Math.min(numTotalHits, hitsPerPage);

        while (true) {
            if (end > hits.length) {
                System.out.println("Only results 1 - " + hits.length + " of " + numTotalHits + " total matching documents collected.");
                System.out.println("Collect more (y/n) ?");
                String line = in.readLine();
                if (line.length() == 0 || line.charAt(0) == 'n') {
                    break;
                }

                hits = searcher.search(query, numTotalHits).scoreDocs;
            }

            end = Math.min(hits.length, start + hitsPerPage);

            StringBuilder sb = new StringBuilder();

            for (int i = start; i < end; i++) {
                if (raw) {                              // output raw format
                    System.out.println("doc=" + hits[i].doc + " score=" + hits[i].score);
                    continue;
                }

                Document doc = searcher.doc(hits[i].doc);
//                String path = doc.get("path");
                String path = doc.get(FtsByLucene.NODE_ID_FIELD_NAME);
                if (path != null) {
                    if (sb.length() > 0) {
                        sb.append(",");
                    }
                    sb.append("(").append(path).append(",").append(i).append(")");
                    System.out.println((i + 1) + ". " + path);
                    String title = doc.get("title");
                    if (title != null) {
                        System.out.println("   Title: " + doc.get("title"));
                    }
                    System.out.println("   doc id: " + hits[i].doc);
                } else {
                    System.out.println((i + 1) + ". " + "No path for this document");
                }

            }

            System.out.println("check in biks db:\n");
            System.out.println("select * from bik_node n inner join (values " + sb + ") as v(node_id, vo) on n.id = v.node_id order by v.vo;\n\n");

            if (!interactive || end == 0) {
                break;
            }

            if (numTotalHits >= end) {
                boolean quit = false;
                while (true) {
                    System.out.print("Press ");
                    if (start - hitsPerPage >= 0) {
                        System.out.print("(p)revious page, ");
                    }
                    if (start + hitsPerPage < numTotalHits) {
                        System.out.print("(n)ext page, ");
                    }
                    System.out.println("(q)uit or enter number to jump to a page.");

                    String line = in.readLine();
                    if (line.length() == 0 || line.charAt(0) == 'q') {
                        quit = true;
                        break;
                    }
                    if (line.charAt(0) == 'p') {
                        start = Math.max(0, start - hitsPerPage);
                        break;
                    } else if (line.charAt(0) == 'n') {
                        if (start + hitsPerPage < numTotalHits) {
                            start += hitsPerPage;
                        }
                        break;
                    } else {
                        int page = Integer.parseInt(line);
                        if ((page - 1) * hitsPerPage < numTotalHits) {
                            start = (page - 1) * hitsPerPage;
                            break;
                        } else {
                            System.out.println("No such page");
                        }
                    }
                }
                if (quit) {
                    break;
                }
                end = Math.min(numTotalHits, start + hitsPerPage);
            }
        }
    }

    public static void findMoreLikeThis(int docId) {
        try {
            findMoreLikeThisInner(docId);
        } catch (IOException ex) {
            throw new LameRuntimeException("error in findMoreLikeThis, docId=" + docId, ex);
        }
    }

    protected static List<SimilarNodeBean> gatherQueryResults(IndexSearcher searcher,
            Query query,
            int maxResults) throws IOException {
        List<SimilarNodeBean> res = new ArrayList<SimilarNodeBean>();

        TopDocs results = searcher.search(query, maxResults);
        ScoreDoc[] hits = results.scoreDocs;

        int numTotalHits = results.totalHits;
        //System.out.println(numTotalHits + " total matching documents");

        int start = 0;
        int end = Math.min(numTotalHits, maxResults);

        for (int i = start; i < end; i++) {
            final ScoreDoc hit = hits[i];
            Document doc = searcher.doc(hit.doc);
//                String path = doc.get("path");
            String objIdStr = doc.get(FtsByLucene.NODE_ID_FIELD_NAME);
            Integer objId = BaseUtils.tryParseInteger(objIdStr);
            if (objId != null) {
                SimilarNodeBean snb = new SimilarNodeBean();
                snb.id = objId;
                snb.relevance = hit.score;
                res.add(snb);
//                if (sb.length() > 0) {
//                    sb.append(",");
//                }
//                sb.append("(").append(objIdStr).append(",").append(i).append(")");
//                System.out.println((i + 1) + ". " + objIdStr);
//                System.out.println("   doc id: " + hits[i].doc);
            } else {
                System.out.println((i + 1) + ". " + "No objId for this document");
            }

            //System.out.println("Explanation for objId=" + objId + ": " + searcher.explain(query, hit.doc) + "\n\n");
        }

//            System.out.println("check in biks db:\n");
//            System.out.println("select * from bik_node n inner join (values " + sb + ") as v(node_id, vo) on n.id = v.node_id order by v.vo;\n\n");
        return res;
    }

    protected static void findMoreLikeThisInner(int docId) throws IOException {
        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(FtsByLucene.INDEX_PATH)));

//        Document doc = reader.document(docId);
//        if (doc == null) {
//            System.out.println("no document with id=" + docId);
//        }
//        System.out.println("nodeId=" + doc.get(FtsByLucene.NODE_ID_FIELD_NAME) + " for docId=" + docId);
        IndexSearcher searcher = new IndexSearcher(reader);
        Analyzer analyzer = createAnalyzer();
        MoreLikeThis mlt = new MoreLikeThis(reader);
//        mlt.setMinTermFreq(0);
//        mlt.setMinDocFreq(0);
        mlt.setFieldNames(new String[]{/*"name", "descr",*/FtsByLucene.COLLECTIVE_FIELD_NAME});
        mlt.setAnalyzer(analyzer);
        Query q = mlt.like(docId);
//        Query q = mlt.like(FtsByLucene.COLLECTIVE_FIELD_NAME, new StringReader("Rodzaj Dokumentu Wspolpracy"));

//        TopDocs similarDocs = searcher.search(q, 10); // Use the searcher
//        if (similarDocs.totalHits == 0) {
//            System.out.println("have no hits :-(");
//        }
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
        System.out.println("more like this");
        doPagingSearch(in, searcher, q, 100, false, false);
    }

    protected static List<SimilarNodeBean> getMoreLikeThisInner(int objId) throws IOException, ParseException {
        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(FtsByLucene.INDEX_PATH)));

//        Document doc = reader.document(docId);
//        if (doc == null) {
//            System.out.println("no document with id=" + docId);
//        }
//        System.out.println("nodeId=" + doc.get(FtsByLucene.NODE_ID_FIELD_NAME) + " for docId=" + docId);
        IndexSearcher searcher = new IndexSearcher(reader);
        Analyzer analyzer = createAnalyzer();

//        QueryParser parser = new QueryParser(FtsByLucene.NODE_ID_FIELD_NAME, analyzer);
//        Query q = parser.parse(Integer.toString(objId));
        Query q0 = new TermQuery(new Term(FtsByLucene.NODE_ID_FIELD_NAME, Integer.toString(objId)));

        TopDocs td = searcher.search(q0, 1);

        ScoreDoc[] hits = td.scoreDocs;

        if (hits.length == 0) {
            return null;
        }

        int docId = hits[0].doc;

        MoreLikeThis mlt = new MoreLikeThis(reader);
        mlt.setMinTermFreq(0);
        mlt.setMinDocFreq(0);
        mlt.setFieldNames(new String[]{/*"name", "descr",*/FtsByLucene.COLLECTIVE_FIELD_NAME});
        mlt.setAnalyzer(analyzer);
        Query mltQ = mlt.like(docId);

        BooleanQuery.Builder builder = new BooleanQuery.Builder();
        builder.add(mltQ, BooleanClause.Occur.MUST);
        builder.add(q0, BooleanClause.Occur.MUST_NOT);
        
//        BooleanQuery bq = new BooleanQuery();
//        bq.add(mltQ, BooleanClause.Occur.MUST);
//        bq.add(q0, BooleanClause.Occur.MUST_NOT);

//        Query negQ0 = new BooleanQuery();
//        Filter f = new QueryWrapperFilter(q0);
//        Query q = mlt.like(FtsByLucene.COLLECTIVE_FIELD_NAME, new StringReader("Rodzaj Dokumentu Wspolpracy"));
//        TopDocs similarDocs = searcher.search(q, 10); // Use the searcher
//        if (similarDocs.totalHits == 0) {
//            System.out.println("have no hits :-(");
//        }
//        return gatherQueryResults(searcher, bq, 100);
        return gatherQueryResults(searcher, builder.build(), 100);
    }

    public static List<SimilarNodeBean> getMoreLikeThis(int objId) {
        try {
            return getMoreLikeThisInner(objId);
        } catch (Exception ex) {
            throw new LameRuntimeException("error in getMoreLikeThis: objId=" + objId, ex);
        }
    }

    public static void main(String[] args) {
//        searchAndPrint("Rodzaj Dokumentu Wspolpracy");
//        searchAndPrint("korzeń cp");
//        searchAndPrint("test robert kowalski 2");
//        searchAndPrint("nazwa robert test kowalski");
//        searchAndPrint("*owalsk*", "name");
//        searchAndPrint("*owa*", "name");
//        searchAndPrint("kowalsk*", "name");
//        searchAndPrint("kowalsk*");
//        searchAndPrint("name:*kowa* tree_id:34");
//        searchAndPrint("name:*kow* tree_id:34");

//        searchAndPrint("name:*dok* tree_id:35");
//        findMoreLikeThis(95436);
//        searchAndPrint("Rodzaj Dokumentu Wspolpracy");
//        findMoreLikeThis(224270);
//        searchAndPrint("Hive Operators and User-Defined Functions (UDFs)");
//        findMoreLikeThis(190857);
        System.out.println("getMoreLikeThis:\n" + getMoreLikeThis(1106783));
//        searchAndPrint("1106783", FtsByLucene.NODE_ID_FIELD_NAME);
//        searchAndPrint("Churn brutto");
    }
}
