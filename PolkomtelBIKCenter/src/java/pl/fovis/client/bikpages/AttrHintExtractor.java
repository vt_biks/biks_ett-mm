/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.EditAttrHintDialog;
import pl.fovis.client.entitydetailswidgets.IGridBeanAction;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.AttrHintBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class AttrHintExtractor extends NodeAwareBeanExtractorBase<AttrHintBean> {

    protected final static int IS_BUILT_IN = 1;

    public AttrHintExtractor() {
        super(false);
    }

    @Override
    public boolean hasIcon() {
        return false;
    }

    @Override
    protected String getNodeKindCode(AttrHintBean b) {
        return "";
    }

    protected String getName(AttrHintBean b) {
        return null;//b.name;
    }

    public Integer getNodeId(AttrHintBean b) {
        return b.id;
    }

    public String getTreeCode(AttrHintBean b) {
        return "";
    }

    public String getBranchIds(AttrHintBean b) {
        return "";
    }

    @Override
    public int getAddColCnt() {
        return 1;
    }

    @Override
    public Object getAddColVal(AttrHintBean b, int addColIdx) {
        Map<Integer, Object> m = new HashMap<Integer, Object>();
        HTML loginName = new HTML(b.name);
        loginName.setStyleName("Notes" /* i18n: no:css-class */ + "-labBody");
        m.put(0, loginName);
        return m.get(addColIdx);
    }

    @Override
    public boolean createGridColumnNames(FlexTable gp) {
        gp.setStyleName("gridJoinedObj");
        gp.setWidget(0, 0, new HTML(I18n.nazwa.get() /* I18N:  */));
        gp.getRowFormatter().setStyleName(0, "RelatedObj-oneObj-dqc");
        return true;
    }

    @Override
    protected List<IGridBeanAction<AttrHintBean>> createActionList() {
        List<IGridBeanAction<AttrHintBean>> res = new ArrayList<IGridBeanAction<AttrHintBean>>();
        res.add(nodeAwareActEdit);
        return res;
    }

    @Override
    protected void execActEdit(final AttrHintBean b) {
        new EditAttrHintDialog().buildAndShowDialog(b.name, b.hint, new IParametrizedContinuation<AttrHintBean>() {
            @Override
            public void doIt(final AttrHintBean param) {
                param.hint = BIKClientSingletons.fixAttrHintValue(param.hint);

                BIKClientSingletons.getService().updateAttrHint(param.name, param.hint, IS_BUILT_IN, new StandardAsyncCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        b.hint = param.hint;
                        refreshView(I18n.zmienionoOpis.get() /* I18N:  */);
                    }
                });
                BIKClientSingletons.updateAttrHint(param.name, param.hint);
            }
        });
    }
}
