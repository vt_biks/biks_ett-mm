/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.serverlogic;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public interface IFoxyService<T extends IFoxyBizLogic, SCO/* extends ServletConfigObj, SD*/> {
    public void init(SCO configObj);
    public void setBizLogic(T bizLogic);
    public void attachSessionEnvironment(ISessionEnvironment sessionEnv);
    public void unboundedFromSession();
    public boolean isSystemAdminLogged();
    //public SD getSessionData();
    public Map<String, Object> getCustomSessionData();
}
