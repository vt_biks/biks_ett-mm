/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence;

import java.util.List;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;

/**
 *
 * @author tflorczak
 */
public interface IConfluenceConnector {

    public List<ConfluenceObjectBean> getSpaces();

    public List<ConfluenceObjectBean> getChildrenObjects(String parentId, String parentKind);

    public List<ConfluenceObjectBean> search(String text);

    public List<List<ConfluenceObjectBean>> getContentForObjects(List<ConfluenceGroupBean> objectIds);

    public ConfluenceObjectBean getContentForOneObject(ConfluenceObjectBean bean);

    public ConfluenceObjectBean publishArticleToConfluence(String articleName, String articleBody, String confluenceLogin, String confluencePassword, String parentObjId, String space);

    public void initializeService();

    public void login();

    public void logout();

    public boolean isServiceInitialized();

    public boolean isLogged();

    public String testConnection();
}
