/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import commonlib.LameUtils;
import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.trzy0.foxy.serverlogic.db.OracleConnection;
import pl.trzy0.foxy.serverlogic.db.OracleConnectionConfig;
import simplelib.BaseUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class OracleTestExecutor extends AbstractDatabaseTestExecutor {

//    protected IBeanConnectionEx<Object> mc;
    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected String databaseName;

    public OracleTestExecutor(IBIKSDQMSubservice service) {
        super(service);
    }

    @Override
    public boolean initConnection(DQMTestBean test) {
        this.test = test;
        this.databaseName = useFunctionInsteadSQLQuery ? test.functionDatabase : test.queryDatabase;
        return initConnectionInner(test.dbServerId, test.connectionId);
    }

    protected boolean initConnectionInner(Integer dbServerId, Integer connectionId) {
        try {
            ConnectionParametersDBServerBean serverConfig = getDatabaseConfig(dbServerId, connectionId);
            this.mc = new OracleConnection(new OracleConnectionConfig(serverConfig.server, serverConfig.instance, serverConfig.user, serverConfig.password, serverConfig.port, serverConfig.databaseName));
            mc.setDbStatementExecutionWarnThresholdMillis(-1);
            mc.setAutoCommit(true);
            mc.execQrySingleValInteger("SELECT 1 FROM DUAL", false);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in initConnection", e);
            }
            return false;
        }
        return true;
    }

//    @Override
//    public void closeConnection() {
//        mc.destroy();
//    }
    @Override
    protected String getCallTestFunctionQuery(String functionName) {
        StringBuilder query = new StringBuilder();
        query.append("select * from table(");
        if (!BaseUtils.isStrEmptyOrWhiteSpace(databaseName)) {
            query.append(databaseName).append(".");
        }
        query.append(functionName);
        if (!functionName.endsWith(")")) {
            query.append("()");
        }
        query.append(")");
        return query.toString();
//            return mc.execQry("select * from table(" + functionName + ")");
    }

//    @Override
//    protected List<Map<String, Object>> runTestQuery(String sqlText) {
//        return mc.execQry(sqlText, IRawConnection.FieldNameConversion.ToLowerCase);
//    }
    @Override
    public boolean initConnection(DQMMultiSourceBean source) {
        this.source = source;
        this.databaseName = source.databaseName;
        return initConnectionInner(source.dbServerId, source.connectionId);
    }

    @Override
    public String getSourceName() {
        return "Oracle";
    }
}
