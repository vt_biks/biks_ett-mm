﻿exec sp_check_version '1.0.89.4';
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values('ConnectionEngineFolder', 'Folder silnika bazy', 'connectionEngineFolder', 'Metadata',1,0)
insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank)
values('ConnectionUserFolder', 'Folder użytkownika bazy', 'connectionUserFolder', 'Metadata',1,0)
update bik_node_kind set icon_name='view' where code='TeradataView'
update bik_node_kind set icon_name='column' where code='TeradataColumn'
update bik_node_kind set icon_name='procedure' where code='TeradataProcedure'
update bik_node_kind set icon_name='universeTable' where code='UniverseTable'
update bik_node_kind set icon_name='aliasTable' where code='UniverseAliasTable'
update bik_node_kind set icon_name='deriveTable' where code='UniverseDerivedTable'
update bik_node_kind set caption='Folder dokumentów' where code='DocumentsFolder'

--------------------------------------------------
--------------------------------------------------
update bik_node
set node_kind_id=(select id from bik_node_kind where code='ConnectionEngineFolder')
where node_kind_id=(select id from bik_node_kind where code='ConnectionFolder') 
and parent_node_id=(select id from bik_node where name='Połączenia' and parent_node_id is null and tree_id=(select id from bik_tree where code='Connections'))

update bik_node
set node_kind_id=(select id from bik_node_kind where code='ConnectionUserFolder')
where node_kind_id=(select id from bik_node_kind where code='ConnectionFolder') 
and not name='Połączenia'
and parent_node_id is not null

--------------------------------------------------
--------------------------------------------------
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_connections_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_connections_into_bik_node
go


create procedure [dbo].[sp_insert_connections_into_bik_node]
as
begin

create table #tmp_engine (name varchar(max));
create table #tmp_users (user_name varchar(max),db_engine varchar(max));
create table #tmp_conn (conn_node_id int,parent_node_id int);

declare @connTree int;
select @connTree = id from bik_tree where code='Connections'
declare @connFolderKind int;
select @connFolderKind = id from bik_node_kind where code='ConnectionFolder'
declare @connEngineFolderKind int;
select @connEngineFolderKind = id from bik_node_kind where code='ConnectionEngineFolder'
declare @connUserFolderKind int;
select @connUserFolderKind = id from bik_node_kind where code='ConnectionUserFolder'
declare @parentNodeId int;
select @parentNodeId = id from bik_node where name='Połączenia' and tree_id=@connTree and node_kind_id=@connFolderKind and is_deleted=0 and linked_node_id is null

-- wrzucenie DB engine folders
insert into #tmp_engine(name)
select distinct database_enigme from bik_sapbo_universe_connection

insert into bik_node (parent_node_id, node_kind_id, name, tree_id)
	select @parentNodeId,@connEngineFolderKind, case when #tmp_engine.name is null then 'Inny' else #tmp_engine.name end, @connTree
	from #tmp_engine left join bik_node bn on bn.name=#tmp_engine.name and bn.is_deleted=0
	and bn.linked_node_id is null and bn.node_kind_id=@connEngineFolderKind and bn.tree_id=@connTree
	where bn.id is null;

-- wrzucenie users folders
insert into #tmp_users(user_name,db_engine)
select distinct case when user_name is null then '(bez użytkownika)' else user_name end,database_enigme from bik_sapbo_universe_connection

insert into bik_node (parent_node_id, node_kind_id, name, tree_id)
	select (select id from bik_node where name=#tmp_users.db_engine and is_deleted=0 and linked_node_id is null and node_kind_id=@connEngineFolderKind and tree_id=@connTree),@connUserFolderKind, #tmp_users.user_name, @connTree
	from #tmp_users left join bik_node bn on bn.name=#tmp_users.user_name and bn.is_deleted=0
	and bn.linked_node_id is null and bn.node_kind_id=@connUserFolderKind and bn.tree_id=@connTree
	and bn.parent_node_id=(select id from bik_node where name=#tmp_users.db_engine and is_deleted=0 and linked_node_id is null and node_kind_id=@connEngineFolderKind and tree_id=@connTree)
	where bn.id is null;

-- podłączenie połączeń pod odpowiednie foldery
insert into #tmp_conn(conn_node_id,parent_node_id)
select bsuc.node_id, bn.id from bik_sapbo_universe_connection bsuc 
join bik_node bn on bn.name = case when bsuc.user_name is null then '(bez użytkownika)' else bsuc.user_name end
and bn.is_deleted=0 and bn.linked_node_id is null 
and bn.node_kind_id=@connUserFolderKind and bn.tree_id=@connTree
join bik_node bn2 on bn.parent_node_id = bn2.id
and bn2.name=bsuc.database_enigme

update bik_node
set parent_node_id = #tmp_conn.parent_node_id
from #tmp_conn
where bik_node.id=#tmp_conn.conn_node_id

drop table #tmp_engine;
drop table #tmp_users;
drop table #tmp_conn;

exec sp_node_init_branch_id @connTree,@parentNodeId
end;
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

exec sp_update_version '1.0.89.4', '1.0.89.5';
go