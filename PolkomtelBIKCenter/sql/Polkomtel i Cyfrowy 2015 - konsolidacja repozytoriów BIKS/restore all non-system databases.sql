/*

Select 
'restore DATABASE ' + name + ' from DISK = ''c:\mssql-bak\' + name + '.bak'''
--,* 
From sys.databases Where [name] NOT IN ('master', 'tempdb', 'model', 'msdb')

*/

use master
go

restore DATABASE a_biks_en_W4KTRGKB from DISK = 'c:\mssql-bak\a_biks_en_W4KTRGKB.bak'
restore DATABASE a_biks_pl_QQW7A6UW from DISK = 'c:\mssql-bak\a_biks_pl_QQW7A6UW.bak'
restore DATABASE BIKS_GT_EN from DISK = 'c:\mssql-bak\BIKS_GT_EN.bak'
restore DATABASE BIKS_GT_PL from DISK = 'c:\mssql-bak\BIKS_GT_PL.bak'
restore DATABASE BIKS_OLD from DISK = 'c:\mssql-bak\BIKS_OLD.bak'
restore DATABASE biks_polkomtel_2012_10_22 from DISK = 'c:\mssql-bak\biks_polkomtel_2012_10_22.bak'
restore DATABASE bssg_sql_test from DISK = 'c:\mssql-bak\bssg_sql_test.bak'
restore DATABASE fita from DISK = 'c:\mssql-bak\fita.bak'
restore DATABASE MasterBIKS8 from DISK = 'c:\mssql-bak\MasterBIKS8.bak'
restore DATABASE sql_test from DISK = 'c:\mssql-bak\sql_test.bak'
restore DATABASE TaskResourcePlanner from DISK = 'c:\mssql-bak\TaskResourcePlanner.bak'
restore DATABASE uknf_nb300 from DISK = 'c:\mssql-bak\uknf_nb300.bak'
restore DATABASE uknf_zis from DISK = 'c:\mssql-bak\uknf_zis.bak'
