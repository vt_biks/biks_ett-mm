/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.sandbox;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.TabPanel;
import pl.fovis.foxygwtcommons.NewLookUtils;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public class NewLookTabLayoutPanel extends TabPanel {

    public NewLookTabLayoutPanel(double barHeight, Unit barUnit, String styleName) {
        //super(barHeight, barUnit);

        NewLookUtils.setupClassAndId(this, styleName);
    }
}
