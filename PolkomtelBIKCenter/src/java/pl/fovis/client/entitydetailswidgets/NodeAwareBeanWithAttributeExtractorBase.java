/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddOrEditJoinedAttributeLinkedDialog;
import static pl.fovis.client.entitydetailswidgets.GenericBeanExtractorBase.getIconUrlByIconName;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.JoinedObjAttributeLinkedBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 * @param <T>
 */
public abstract class NodeAwareBeanWithAttributeExtractorBase<T> extends NodeAwareBeanExtractorBase<T> {

    public NodeAwareBeanWithAttributeExtractorBase(boolean isAddJoinedButtonEnabled) {
        super(isAddJoinedButtonEnabled);
    }

    protected List<IGridBeanAction<T>> createActionList() {
        List<IGridBeanAction<T>> res = new ArrayList<IGridBeanAction<T>>();
        if (BIKClientSingletons.enableAttributeJoinedObjs()) {
            res.add(actEditAttr);
        }
        res.add(actView);
        res.add(actFollow);
        res.add(actDelete);
        return res;
    }

    protected IGridBeanAction<T> actEditAttr = new SimpleGridBeanAction<T>() {
        @Override
        public String getActionIconUrl(T b) {
            return getIconUrlByIconName("edit_gray");
        }

        @Override
        public String getActionIconHint(T b) {
            return I18n.edytujAtrybuty.get() /* I18N:  */;
        }

        @Override
        public void executeAction(T b) {
            execActEditAttribute(b);
        }

        @Override
        public boolean isEnabled(T b) {
            return isEditBtnEnabled(b) && !isFromHyperlink(b);
        }

        @Override
        public boolean isVisible(T b) {
            return (getOptSimilarityScore(b) == null)  && !isFromHyperlink(b);
        }

        public IGridBeanAction.TypeOfAction getActionType() {
            return IGridBeanAction.TypeOfAction.EditAttribute;
        }
    };

    protected void execActEditAttribute(final T b) {

        BIKClientSingletons.getService().getJoinedAttributesLinkedAndoinedObjAttributes(getJoinedObjId(b), new StandardAsyncCallback<Pair<List<JoinedObjAttributeLinkedBean>, Map<String, AttributeBean>>>() {

            @Override
            public void onSuccess(Pair<List<JoinedObjAttributeLinkedBean>, Map<String, AttributeBean>> result) {
                new AddOrEditJoinedAttributeLinkedDialog().buildAndShowDialog(result, false, new IParametrizedContinuation<Pair<List<JoinedObjAttributeLinkedBean>, Integer>>() {

                    @Override
                    public void doIt(final Pair<List<JoinedObjAttributeLinkedBean>, Integer> param) {

                        BIKClientSingletons.getService().addJoinedAttributeLinked(getJoinedObjId(b), param.v1, new StandardAsyncCallback<Void>() {

                            @Override
                            public void onSuccess(Void result) {
                                String mainJojnedObjAttributeLinked = null;
                                for (JoinedObjAttributeLinkedBean p : param.v1) {
                                    if (p.isMain) {
                                        mainJojnedObjAttributeLinked = p.value;
                                        break;
                                    }
                                }
                                setAttributes(b, mainJojnedObjAttributeLinked);
                                refreshView(I18n.atrybutyZostalyZaktualizowane.get());
                            }
                        });
                    }
                });
            }
        });
    }

    protected void setAttributes(T b, String value) {
        //no-op
    }

    protected boolean isFromHyperlink(T b) {
        return false;
    }
}
