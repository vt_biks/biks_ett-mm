/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.UserRoleInNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author beata
 */
public class UserRoleInNodeBeanExtractor extends NodeAwareBeanExtractorBase<UserRoleInNodeBean> {

    protected int currentNodeId;
    protected Map<Integer, String> idsAndNamesFromRoleBranches;

    public UserRoleInNodeBeanExtractor(boolean isAddJoinedButtonEnabled, int currentNodeId, Map<Integer, String> idsAndNamesFromBranches) {
        super(isAddJoinedButtonEnabled);
        this.currentNodeId = currentNodeId;
        this.idsAndNamesFromRoleBranches = idsAndNamesFromBranches;
    }

    public Integer getNodeId(UserRoleInNodeBean b) {
        return b.nodeId;
    }

    public String getTreeCode(UserRoleInNodeBean b) {
        return b.nodeTreeCode;
    }

    protected String getName(UserRoleInNodeBean b) {
        return b.nodeName; //ww: tak było (co dawało gwiazdkę przy nazwie obiektu):
        //+ (b.inheritToDescendants ? " *" : "");
    }

    @Override
    public boolean hasChildren(UserRoleInNodeBean b) {
        return b.dstHasChildren;
    }

    protected String getNodeKindCode(UserRoleInNodeBean b) {
        return b.nodeKindCode;
    }

    public String getBranchIds(UserRoleInNodeBean b) {
        return b.branchIds;
    }

    @Override
    public int getAddColCnt() {
        return BIKClientSingletons.isDescriptionInJoinedPanelVisible() ? 2 : 1;
    }

    @Override
    public Object getAddColVal(UserRoleInNodeBean b, int addColIdx) {
        if (BIKClientSingletons.isDescriptionInJoinedPanelVisible() && addColIdx == 0) {
            return getDescription(b.descrPlain);
        }

        if (b.isFromHyperlink){
            return BaseUtils.capitalize(I18n.atrybut.get()) + ": " + b.roleCaption;
        }
        List<Integer> nodeIdsFromRoleBranch = BIKCenterUtils.splitBranchIdsEx(b.roleBranchIds, false);
        List<String> nodeNames = new ArrayList<String>();
//        nodeIdsFromRoleBranch.remove(nodeIdsFromRoleBranch.size() - 1);
        for (Integer id : nodeIdsFromRoleBranch) {
            if (idsAndNamesFromRoleBranches.get(id) != null) {
                nodeNames.add(idsAndNamesFromRoleBranches.get(id));
            }
        }
//        ""
        String pathStr = BaseUtils.mergeWithSepEx(nodeNames, BIKGWTConstants.RAQUO_STR_SPACED);
        return pathStr + (b.isAuxiliary ? " (" + I18n.pomocniczy.get() + ")" : "") //+ " (" + (!b.isAuxiliary ? I18n.glowny.get() /* I18N:  */ : I18n.pomocniczy.get() /* I18N:  */) + ")"
                + (b.inheritToDescendants ? " *" : "");
    }

    @Override
    protected List<IGridBeanAction<UserRoleInNodeBean>> createActionList() {
        List<IGridBeanAction<UserRoleInNodeBean>> res = new ArrayList<IGridBeanAction<UserRoleInNodeBean>>();
        res.add(actView);
        res.add(actFollow);
        res.add(actDelete);
        return res;
    }

    @Override
    protected void execActDelete(final UserRoleInNodeBean b) {
        //
        BIKClientSingletons.getService().deleteJoinedObjectWithRoleFromUser(getNodeId(b), b.roleForNodeId, b.inheritToDescendants ? 1 : 0,
                b.isAuxiliary, BIKClientSingletons.getTabSelector().getCurrentTabIdAndSelectedVal().v2,
                new StandardAsyncCallback<Integer>() {
                    @Override
                    public void onSuccess(Integer result) {

                        boolean needRefresh = result != null;
                        BIKClientSingletons.refreshAndRedisplayDataUsersTree(needRefresh);
                        if (needRefresh) {
                            //jak usunie ostatnie powiązanie
                            BIKClientSingletons.getTabSelector().selectValueOnTab(BIKGWTConstants.TREE_CODE_USER_ROLES, result);
                        }
                    }
                });
        deleteBeanFromList(b, I18n.usunietoPowiazanie.get() /* I18N:  */);
    }

    @Override
    public int howManyTimesNameColumnWiderThanOther() {
        return 2;
    }

    @Override
    public boolean isDeleteBtnVisible(UserRoleInNodeBean b) {
        return !isUsingOnDialog && !b.isAutomaticJoined && !b.isFromHyperlink;
    }
    
//     public boolean isActEditBtnEnabled(UserRoleInNodeBean b) {
//        return !b.isFromHyperlink && super.isActEditBtnEnabled(b);
//    }
}
