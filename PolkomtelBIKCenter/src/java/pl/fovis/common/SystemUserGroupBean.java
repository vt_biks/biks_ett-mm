/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import simplelib.BeanWithIntIdBase;

/**
 *
 * @author bfechner
 */
public class SystemUserGroupBean extends BeanWithIntIdBase implements Serializable {

//    public Integer parentId;
    public String objId;
    public String parentId;
    public String name;
    public String domain;
    public boolean hasNoChildren;
    public int memberGroupId;
    public Map<Integer, Set<TreeNodeBranchBean>> customRightRoleUserGroup;
//    public List<SystemUserGroupBean> groupIn;
    public List<SystemUserGroupBean> inGroup;
    public List<String> usersName;
    public boolean isBuiltIn;
    public String description;
}
