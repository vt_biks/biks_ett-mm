﻿exec sp_check_version '1.8.1.5';
go

insert into bik_node_kind_4_tree_kind(tree_kind_id,src_node_kind_id,is_deleted)
select id,leaf_node_kind_id,0 from bik_tree_kind btk where not exists 
(select * from bik_node_kind_4_tree_kind bnk where bnk.tree_kind_id=btk.id 
and bnk.src_node_kind_id=btk.leaf_node_kind_id and dst_node_kind_id is null)

exec sp_update_version '1.8.1.5', '1.8.1.6';