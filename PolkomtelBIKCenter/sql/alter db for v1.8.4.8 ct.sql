﻿exec sp_check_version '1.8.4.8';
go

update bik_attr_category set name='metaBiks', is_deleted=0 where name='Metadata BIKS'
update bik_attr_def set name='metaBiks.'+substring(name, charindex('.', name)+1, len(name)) from bik_attr_def where name like 'metaBIKS%'
update bik_node_kind set tree_kind='metaBiks' where code like 'metaBiks%'
update bik_tree_kind set code='metaBiks' where code='metaBIKS'
update bik_tree_kind set code='metaBiksMenuConf' where code='metaBIKSMenuConf'
update bik_tree set tree_kind='metaBiks' where tree_kind='metaBIKS'
update bik_tree set tree_kind='metaBiksMenuConf' where tree_kind='metaBIKSMenuConf'

update bik_attr_def set type_attr='checkbox' where name='metaBiks.Typ relacji'
exec sp_add_attr_2_node_kind 'metaBiksAttribute4NodeKind', 'metaBIKS.Typ atrybutu', null, 1, null, null

update bik_node_kind set uploadable_children_kinds='Document' where uploadable_children_kinds is null and code not like 'metaBiks%'

update attr set value_opt='hyperlinkInMono,hyperlinkInMulti,hyperlinkOut,data,combobox,checkbox,comboBooleanBox,calculation,javascript,selectOneJoinedObject,ansiiText,longText,shortText,'
from bik_attribute attr join bik_node_kind nk on nk.id=attr.node_kind_id
join bik_attr_def adef on adef.id=attr.attr_def_id
where name='metaBiks.Typ atrybutu'

if object_id('aaa_attr_def') is null create table aaa_attr_def(
	id int,
	name varchar(max),
	attr_category_id int,
	hint varchar(max),
	type_attr varchar(max),
	value_opt varchar(max),
	display_as_number int,
	in_drools int
)
go

if object_id('aaa_node_kind') is null create table aaa_node_kind(
	id int,
	code varchar(max),
	caption varchar(max),
	icon_name varchar(max),
	tree_kind varchar(max),
	children_kinds varchar(max),
	children_attr_names varchar(max),
	is_registry int
)
go

if object_id('aaa_help') is null create table aaa_help(
	id int,
	code varchar(max),
	caption varchar(max),
	help_author varchar(max),
	help_user varchar(max)
)
go

if object_id('aaa_attribute') is null create table aaa_attribute(
	id int,
	node_kind_id int,
	attr_def_id int,
	default_value varchar(max),
	is_required int,
	type_attr varchar(max),
	value_opt varchar(max),
	is_empty int,
	is_public int
)
go

if object_id('aaa_tree_kind') is null create table aaa_tree_kind(
	id int,
	code varchar(max),
	caption varchar(max),
	allow_linking int
)
go

if object_id('aaa_node_kind_in_tree_kind') is null create table aaa_node_kind_in_tree_kind(
	id int,
	tree_kind_id int,
	node_kind_id int,
	type varchar(max)
)
go

if object_id('aaa_node_kind_4_tree_kind') is null create table aaa_node_kind_4_tree_kind(
	tree_kind_id int,
	src_node_kind_id int,
	dst_node_kind_id int,
	relation_type varchar(max)
)
go

if object_id('aaa_node_kind_relation_in_tree') is null create table aaa_node_kind_relation_in_tree(
	id int,
	tree_kind_id int,
	src_node_kind_id int,
	dst_node_kind_id int,
	relation_type varchar(max)
)
go

if object_id('sp_update_table_column') is not null 
drop procedure sp_update_table_column
go

create procedure sp_update_table_column(@tableName varchar(max), @metaAttr varchar(max), @columnName varchar(max), @converter varchar(max)) 
as
begin
	declare @ids bik_unique_not_null_id_table_type
	insert into @ids exec(N'select id from ' + @tableName)
	declare @sql nvarchar(max) 
	declare @paramDef nvarchar(max) 
	 
	execute sp_executesql @sql, @paramDef, @ids

	if @converter is null set @converter='x.value'

	set @sql=
	N'update conf set ' + @columnName + '=' + @converter + ' ' +
	N'from ' + quotename(@tableName) + ' conf join ' +
	N'(select bn.id, al.value from bik_node bn join bik_attribute_linked al on bn.id=al.node_id ' +
	N'join bik_attribute attr on attr.id=al.attribute_id '+
	N'join bik_attr_def adef on adef.id=attr.attr_def_id '+
	N'where adef.name=''' + @metaAttr +''' and al.is_deleted=0 and bn.id in (select id from @ids)) as x on x.id=conf.id'
	
	set @paramDef=
	N'@ids bik_unique_not_null_id_table_type readonly'
 
	execute sp_executesql @sql, @paramDef, @ids
end
go

if object_id('sp_build_metamodel') is not null 
drop procedure sp_build_metamodel
go

create procedure sp_build_metamodel(@deleteOldTree int) 
as
begin
    truncate table amg_node
    truncate table amg_attribute

    declare @confTreeCode varchar(max)=(select val from bik_app_prop where name='metadataTree')
    declare @treeId int = (select id from bik_tree where code=@confTreeCode)
    declare @root varchar(max)

    set @root = 'Definicje atrybutów'
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    values(@root, 'metaBiksDefinitionGroup', null, @root+'|', null, 0)

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select name, 'metaBiksAttributesCatalog', null, @root+'|'+name+'|', @root+'|', 0
    from bik_attr_category where is_deleted=0 and is_built_in=0 and name not like 'metaBiks%'

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select adef.name, 'metaBiksAttributeDef', null, @root+'|'+bac.name+'|'+adef.name+'|', @root+'|'+bac.name+'|', 0
    from bik_attr_def adef join bik_attr_category bac on adef.attr_category_id=bac.id
    where adef.is_deleted=0 and adef.is_built_in=0 and bac.is_deleted=0 and bac.is_built_in=0 and bac.name not like 'metaBiks%'

    --Meta atrybuty do definicji atrybutow
    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Lista wartości', adef.value_opt from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Treść pomocy', adef.hint from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Typ atrybutu', adef.type_attr from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Używaj w systemie zarządzania regułami', case when adef.in_drools=1 then 'Tak' else 'Nie' end from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wyświetlaj jako liczbę', case when adef.display_as_number=1 then 'Tak' else 'Nie' end from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    --Typ obiektu
    set @root = 'Definicje obiektów'
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    values(@root, 'metaBiksDefinitionGroup', null, @root+'|', null, 0)

    declare @ids table(id int)
    insert into @ids
    select distinct branch_node_kind_id from bik_tree_kind where is_deleted=0 union
    select distinct leaf_node_kind_id from bik_tree_kind where is_deleted=0 union
    select distinct dst_node_kind_id from bik_node_kind_4_tree_kind where (src_node_kind_id is null or relation_type = 'Dziecko') and (is_deleted=0) union
    select distinct src_node_kind_id from bik_node_kind_4_tree_kind where (dst_node_kind_id is null or relation_type = 'Dziecko') and (is_deleted=0) union
    select distinct id from bik_node_kind where tree_kind is null or ltrim(tree_kind)=''
    delete from @ids where id in (select nk.id from bik_node_kind nk join @ids ids on ids.id=nk.id and nk.code like 'metaBiks%')

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select code, 'metaBiksNodeKind', null, @root+'|'+code+'|', @root+'|', 0 from bik_node_kind where id in (select id from @ids) and is_deleted=0 and code not like 'metaBiks%'

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select code, 'metaBiksBuiltInNodeKind', null, @root+'|'+code+'|', @root+'|', 0 from bik_node_kind where id not in (select id from @ids) and is_deleted=0 and code not like 'metaBiks%'
    ---------
    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Atrybuty dzieci w tabeli', nk.children_attr_names from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Dzieci w tabeli', nk.children_kinds from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Ikona', nk.icon_name from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Nazwa typu obiektu', nk.caption from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Rejestr', case when nk.is_registry=1 then 'Tak' else 'Nie' end from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Treść pomocy dla autora', coalesce(h.text_help, '') from bik_node_kind nk join amg_node a on a.name=nk.code left join bik_help h on help_key=nk.code+':author' and lang='pl'
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Treść pomocy dla innych', coalesce(h.text_help, '') from bik_node_kind nk join amg_node a on a.name=nk.code left join bik_help h on help_key=nk.code+':user' and lang='pl'
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'
    ------------
    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Nazwa typu obiektu', nk.caption from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksBuiltInNodeKind' and nk.is_deleted=0 and nk.id not in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Treść pomocy dla autora', coalesce(h.text_help, '') from bik_node_kind nk join amg_node a on a.name=nk.code left join bik_help h on help_key=nk.code+':author' and lang='pl'
    where a.node_kind_code='metaBiksBuiltInNodeKind' and nk.is_deleted=0 and nk.id not in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Treść pomocy dla innych', coalesce(h.text_help, '') from bik_node_kind nk join amg_node a on a.name=nk.code left join bik_help h on help_key=nk.code+':user' and lang='pl'
    where a.node_kind_code='metaBiksBuiltInNodeKind' and nk.is_deleted=0 and nk.id not in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    declare @attr table(node_kind_code varchar(max), attr_name varchar(max), category varchar(max), default_value varchar(max), is_required int, type_attr varchar(max), value_opt varchar(max), is_empty int, is_public int)
    insert into @attr(node_kind_code, attr_name, category, default_value, is_required, type_attr, value_opt, is_empty, is_public)
    select nk.code, adef.name, c.name, attr.default_value, attr.is_required, attr.type_attr, attr.value_opt, attr.is_empty, attr.is_public
    from bik_attribute attr join bik_node_kind nk on nk.id=attr.node_kind_id
    join bik_attr_def adef on adef.id=attr.attr_def_id
    join bik_attr_category c on adef.attr_category_id=c.id
    where attr.is_deleted=0 and nk.id in (select id from @ids) and nk.code not like 'metaBiks%'

    declare @attr_builtin table(node_kind_code varchar(max), attr_name varchar(max), category varchar(max), default_value varchar(max), is_required int, type_attr varchar(max), value_opt varchar(max), is_empty int, is_public int)
    insert into @attr(node_kind_code, attr_name, category, default_value, is_required, type_attr, value_opt, is_empty, is_public)
    select nk.code, adef.name, c.name, attr.default_value, attr.is_required, attr.type_attr, attr.value_opt, attr.is_empty, attr.is_public
    from bik_attribute attr join bik_node_kind nk on nk.id=attr.node_kind_id
    join bik_attr_def adef on adef.id=attr.attr_def_id
    join bik_attr_category c on adef.attr_category_id=c.id
    where attr.is_deleted=0 and nk.id not in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%' and adef.name not like 'metaBiks%'

    --Atrybut dla typu obiektu dynamicznego
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select attr.attr_name, 'metaBiksAttribute4NodeKind', null, @root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|', @root+'|'+attr.node_kind_code+'|', 0
    from @attr attr

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Lista wartości', attr.value_opt from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Typ atrybutu', attr.type_attr from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Może być pusty', case when attr.is_empty=1 then 'Tak' else 'Nie' end from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Pokaż na formatce nowego obiektu', case when attr.is_required=1 then 'Tak' else 'Nie' end from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Publiczny', case when attr.is_public=1 then 'Tak' else 'Nie' end from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wartość domyślna', attr.default_value from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    --Atrybut dla typu obiektu wbudowanego
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select attr.attr_name, 'metaBiksAttribute4NodeKind', null, @root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|', @root+'|'+attr.node_kind_code+'|', 0
    from @attr_builtin attr

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Lista wartości', attr.value_opt from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Typ atrybutu', attr.type_attr from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Może być pusty', case when attr.is_empty=1 then 'Tak' else 'Nie' end from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Pokaż na formatce nowego obiektu', case when attr.is_required=1 then 'Tak' else 'Nie' end from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Publiczny', case when attr.is_public=1 then 'Tak' else 'Nie' end from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wartość domyślna', attr.default_value from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    --Typ drzew
    set @root='Definicje drzew'
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    values(@root, 'metaBiksDefinitionGroup', null, @root+'|', null, 0)

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select tk.code, 'metaBiksTreeKind', null, @root+'|'+tk.code+'|', @root+'|', 0
    from bik_tree_kind tk where tk.code not like 'metaBiks%' and tk.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Nazwa typu drzewa', tk.caption from amg_node a join bik_tree_kind tk on a.obj_id=@root+'|'+tk.code+'|'
    where a.node_kind_code='metaBiksTreeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Pozwolić dowiązać', case when tk.allow_linking=1 then 'Tak' else 'Nie' end from amg_node a join bik_tree_kind tk on a.obj_id=@root+'|'+tk.code+'|'
    where a.node_kind_code='metaBiksTreeKind'

    declare @nkInTk table(tk_code varchar(max), src_code varchar(max), dst_code varchar(max), relation_type varchar(max))
    insert into @nkInTk(tk_code, src_code, dst_code, relation_type)
    select tk.code, snk.code, dnk.code, relation_type from bik_node_kind_4_tree_kind nk4tk
    join bik_tree_kind tk on tk.id=nk4tk.tree_kind_id
    left join bik_node_kind snk on snk.id=nk4tk.src_node_kind_id
    left join bik_node_kind dnk on dnk.id=nk4tk.dst_node_kind_id
    where nk4tk.is_deleted=0 and tk.code not like 'metaBiks%' and tk.is_deleted=0 and (snk.is_deleted is null or snk.is_deleted=0) and (dnk.is_deleted is null or dnk.is_deleted=0)

    declare @nk4tk table(tk_code varchar(max), nk_code varchar(max), type_in_tree varchar(max) default '')
    insert into @nk4tk(tk_code, nk_code)
    select tk_code, src_code as nk_code from @nkInTk where (src_code is not null and relation_type is null) or relation_type='Dziecko' union
    select tk_code, dst_code as nk_code from @nkInTk where (dst_code is not null and relation_type is null) or relation_type='Dziecko' union
    select tk_code, src_code as nk_code from @nkInTk where relation_type is not null and relation_type <> 'Dziecko'

    update @nk4tk set type_in_tree='Główna gałąź+' where nk_code in (select dst_code from @nkInTk where dst_code is not null and relation_type is null)
    update @nk4tk set type_in_tree=type_in_tree+'Gałąź+' where nk_code in (select src_code from @nkInTk where relation_type='Dziecko')
    update @nk4tk set type_in_tree=type_in_tree+'Liść+' where nk_code in (select src_code from @nkInTk where src_code is not null and relation_type is null)
    update @nk4tk set type_in_tree=substring(type_in_tree, 0, len(type_in_tree)) where type_in_tree like '%+'

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select nk4tk.nk_code, 'metaBiksNodeKind4TreeKind', null, @root+'|'+nk4tk.tk_code+'|'+nk4tk.nk_code+'|', @root+'|'+nk4tk.tk_code+'|', 0
    from @nk4tk as nk4tk

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Typ w drzewie', nk4tk.type_in_tree from @nk4tk nk4tk join amg_node a on a.obj_id= @root + '|'+nk4tk.tk_code+'|'+nk4tk.nk_code+'|'
    where a.node_kind_code='metaBiksNodeKind4TreeKind'

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select x.dst_code, 'metaBiksNodeKindInRelation', null, @root+'|'+x.tk_code+'|'+x.src_code+'|'+x.dst_code+'|', @root+'|'+x.tk_code+'|'+x.src_code+'|', 0
    from @nkInTk as x where relation_type is not null

    declare @relation table(obj_id varchar(max), value varchar(max))
    insert into @relation(obj_id, value)
    select a.obj_id, nkInTk.relation_type from @nkInTk nkInTk join amg_node a on a.obj_id= @root + '|'+nkInTk.tk_code+'|'+nkInTk.src_code+'|'+nkInTk.dst_code+'|'
    where a.node_kind_code='metaBiksNodeKindInRelation' and nkInTk.relation_type is not null

    insert into amg_attribute(obj_id, name, value)
    select obj_id, 'metaBiks.Typ relacji', stuff((select '+' + r2.value from @relation r2 where r1.obj_id = r2.obj_id for xml path ('')),1,1,'') from @relation r1 group by obj_id

    exec sp_generic_insert_into_bik_node_ex @confTreeCode, null, @deleteOldTree;

    --Atrybuty Wzorzec
    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wzorzec', cast(bn.id as varchar(100))+'_'+@confTreeCode+','+attr.attr_name from amg_node a join @attr attr on a.obj_id='Definicje obiektów|'+attr.node_kind_code+'|'+attr.attr_name+'|' join bik_node bn on bn.obj_id='Definicje atrybutów|'+attr.category+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind' and bn.tree_id=@treeId and bn.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wzorzec', cast(bn.id as varchar(100))+'_'+@confTreeCode+','+attr.attr_name from amg_node a join @attr_builtin attr on a.obj_id='Definicje obiektów|'+attr.node_kind_code+'|'+attr.attr_name+'|' join bik_node bn on bn.obj_id='Definicje atrybutów|'+attr.category+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind' and bn.tree_id=@treeId and bn.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wzorzec', cast(bn.id as varchar(100))+'_'+@confTreeCode+','+nk_code from amg_node a join @nk4tk nk4tk on a.obj_id='Definicje drzew|'+nk4tk.tk_code+'|'+nk4tk.nk_code+'|' join bik_node bn on bn.obj_id='Definicje obiektów|'+nk4tk.nk_code+'|'
    where a.node_kind_code='metaBiksNodeKind4TreeKind' and bn.tree_id=@treeId and bn.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wzorzec', cast(bn.id as varchar(100))+'_'+@confTreeCode+','+dst_code from amg_node a join @nkInTk nkInTk on a.obj_id='Definicje drzew|'+nkInTk.tk_code+'|'+nkInTk.src_code+'|'+nkInTk.dst_code+'|' join bik_node bn on bn.obj_id='Definicje obiektów|'+nkInTk.dst_code+'|'
    where a.node_kind_code='metaBiksNodeKindInRelation' and bn.tree_id=@treeId and bn.is_deleted=0
    --
    merge bik_attribute_linked al
    using (
    select bn.id as node_id, attr.id as attr_id, a.value from bik_node bn join amg_attribute a on bn.obj_id=a.obj_id
    join bik_tree bt on bn.tree_id=bt.id
    join bik_attr_def adef on adef.name=a.name
    join bik_attribute attr on attr.node_kind_id=bn.node_kind_id and attr.attr_def_id=adef.id
    where bt.code=@confTreeCode
    ) x on x.node_id=al.node_id and x.attr_id=al.attribute_id
    when matched then
    update set al.value=x.value, al.is_deleted=0
    when not matched by target then
    insert (attribute_id, value, node_id, is_deleted) values (x.attr_id, x.value, x.node_id, 0)
    when not matched by source and al.node_id in (select bn.id from bik_node as bn inner join bik_tree as bt on bt.id = bn.tree_id and bt.code = @confTreeCode) then
    update set al.is_deleted=1
    ;
end
go

if object_id('sp_implement_metamodel') is not null 
drop procedure sp_implement_metamodel
go

create procedure sp_implement_metamodel
as
begin
	declare @metaTree int = (select id from bik_tree where code in (select val from bik_app_prop where name='metadataTree'))
	update bik_attr_category set is_deleted=1 where is_built_in=0 and name not like 'metaBiks%'

	--Kategorie atrybutow
	merge bik_attr_category cat
	using 
	(select bn.name from bik_node bn join bik_tree bt on bt.id=bn.tree_id join bik_node_kind nk on nk.id=bn.node_kind_id
	where nk.code='metaBiksAttributesCatalog' and bt.id=@metaTree and bn.is_deleted=0) as x on x.name=cat.name and cat.is_built_in=0
	when matched then update set is_deleted=0
	when not matched by target then insert (name) values(x.name);

	update bik_attr_def set is_deleted=1 where is_built_in=0 and name not like 'metaBiks%'
  
	truncate table aaa_attr_def
	insert into aaa_attr_def(id, name, attr_category_id)
	select bn1.id, bn1.name, cat.id 
	from bik_node bn1 join bik_node bn2 on bn1.parent_node_id=bn2.id 
	join bik_tree bt on bt.id=bn1.tree_id
	join bik_node_kind nk on bn1.node_kind_id=nk.id
	join bik_attr_category cat on cat.name=bn2.name and cat.is_built_in=0
	where bn1.is_deleted=0 and nk.code='metaBiksAttributeDef' and bt.id=@metaTree
 
	declare @binaryConverter varchar(max)='case x.value when ''Tak'' then 1 else 0 end'
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Lista wartości', 'value_opt', null
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Treść pomocy', 'hint', null
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Typ atrybutu', 'type_attr', null
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Używaj w systemie zarządzania regułami', 'in_drools', @binaryConverter
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Wyświetlaj jako liczbę', 'display_as_number', @binaryConverter
 
	--Atrybuty
	merge bik_attr_def adef 
	using aaa_attr_def x on x.name=adef.name and adef.is_built_in=0
	when matched then update set is_deleted=0, hint=x.hint, type_attr=x.type_attr, value_opt=x.value_opt, display_as_number=x.display_as_number, in_drools=x.in_drools
	when not matched by target then 
	insert(name, attr_category_id, hint, type_attr, value_opt, display_as_number, in_drools)
	values(x.name, x.attr_category_id, x.hint, x.type_attr, x.value_opt, x.display_as_number, x.in_drools);
 
	update bik_attr_def set hint=null where ltrim(hint)=''
	update bik_attr_def set value_opt=null where ltrim(value_opt)=''

	update bik_node_kind set is_deleted=1 where code not like 'metaBiks%'

	--node_kind
	truncate table aaa_node_kind
	insert into aaa_node_kind(id, code)
	select bn.id, bn.name from bik_node bn join bik_tree bt on bn.tree_id=bt.id
	join bik_node_kind nk on nk.id=bn.node_kind_id
	where nk.code='metaBiksNodeKind' and bn.is_deleted=0 and bt.id=@metaTree

	truncate table aaa_help
	insert into aaa_help(id, code)
	select bn.id, bn.name from bik_node bn join bik_tree bt on bn.tree_id=bt.id
	join bik_node_kind nk on nk.id=bn.node_kind_id
	where nk.code='metaBiksNodeKind' and bn.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Atrybuty dzieci w tabeli', 'children_attr_names', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Dzieci w tabeli', 'children_kinds', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Ikona', 'icon_name', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Nazwa typu obiektu', 'caption', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Rejestr', 'is_registry', @binaryConverter
	exec sp_update_table_column 'aaa_help', 'metaBiks.Nazwa typu obiektu', 'caption', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Treść pomocy dla autora', 'help_author', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Treść pomocy dla innych', 'help_user', null
 
	merge bik_node_kind nk
	using aaa_node_kind x on x.code=nk.code
	when matched then update set is_deleted=0, children_attr_names=x.children_attr_names, children_kinds=x.children_kinds, icon_name=x.icon_name, caption=x.caption, is_registry=x.is_registry
	when not matched by target then 
	insert(code, children_attr_names, children_kinds, icon_name, caption, is_registry)
	values(x.code, x.children_attr_names, x.children_kinds, x.icon_name, x.caption, x.is_registry);

	--built_in_node_kind
	truncate table aaa_node_kind
	insert into aaa_node_kind(id, code)
	select bn.id, bn.name from bik_node bn join bik_tree bt on bn.tree_id=bt.id
	join bik_node_kind nk on nk.id=bn.node_kind_id
	where nk.code='metaBiksBuiltInNodeKind' and bn.is_deleted=0 and bt.id=@metaTree 

	insert into aaa_help(id, code)
	select bn.id, bn.name from bik_node bn join bik_tree bt on bn.tree_id=bt.id
	join bik_node_kind nk on nk.id=bn.node_kind_id
	where nk.code='metaBiksBuiltInNodeKind' and bn.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Nazwa typu obiektu', 'caption', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Nazwa typu obiektu', 'caption', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Treść pomocy dla autora', 'help_author', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Treść pomocy dla innych', 'help_user', null

	merge bik_node_kind nk
	using aaa_node_kind x on x.code=nk.code
	when matched then update set is_deleted=0, caption=x.caption;

	--help
	merge bik_help bh
	using aaa_help x on x.code+':author'=bh.help_key and bh.lang='pl'
	when matched then update set is_hidden=0, caption=x.caption, text_help=x.help_author
	when not matched by target then
	insert(help_key, caption, text_help, lang)
	values(x.code+':author', x.caption, x.help_author, 'pl');

	merge bik_help bh
	using aaa_help x on x.code+':user'=bh.help_key and bh.lang='pl'
	when matched then update set is_hidden=0, caption=x.caption, text_help=x.help_author
	when not matched by target then
	insert(help_key, caption, text_help, lang)
	values(x.code+':user', x.caption, x.help_user, 'pl');

	update attr set is_deleted=1
	from bik_attribute attr join bik_attr_def adef on adef.id=attr.attr_def_id
	join bik_node_kind nk on nk.id=attr.node_kind_id
	where nk.code not like 'metaBiks%' and adef.name not like 'metaBiks%'

	--atrybuty dla node_kind
	truncate table aaa_attribute
	insert into aaa_attribute(id, node_kind_id, attr_def_id)
	select bn1.id, nk2.id, adef.id
	from bik_node bn1 join bik_node bn2 on bn1.parent_node_id=bn2.id
	join bik_tree bt on bt.id=bn1.tree_id
	join bik_node_kind nk1 on nk1.id=bn1.node_kind_id
	join bik_node_kind nk2 on nk2.code=bn2.name
	join bik_attr_def adef on adef.name=bn1.name
	where nk1.code='metaBiksAttribute4NodeKind' and bn1.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Lista wartości', 'value_opt', null
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Może być pusty', 'is_empty', @binaryConverter
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Pokaż na formatce nowego obiektu', 'is_required', @binaryConverter
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Publiczny', 'is_public', @binaryConverter
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Typ atrybutu', 'type_attr', null
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Wartość domyślna', 'default_value', null

	merge bik_attribute attr
	using aaa_attribute x on attr.node_kind_id=x.node_kind_id and attr.attr_def_id=x.attr_def_id
	when matched then update set is_deleted=0, default_value=x.default_value, is_required=x.is_required, type_attr=x.type_attr, value_opt=x.value_opt, is_empty=x.is_empty, is_public=x.is_public
	when not matched by target then
	insert(node_kind_id, attr_def_id, default_value, is_required, type_attr, value_opt, is_empty, is_public)
	values(x.node_kind_id, x.attr_def_id, x.default_value, x.is_required, x.type_attr, x.value_opt, x.is_empty, x.is_public);
	
	update bik_attribute set default_value=null where ltrim(default_value)=''
	update bik_attribute set value_opt=null where ltrim(value_opt)=''

	--tree_kind
	update bik_tree_kind set is_deleted=1 where code not like 'metaBiks%'

	truncate table aaa_tree_kind
	insert into aaa_tree_kind(id, code)
	select bn.id, bn.name
	from bik_node bn join bik_node_kind nk on nk.id=bn.node_kind_id
	join bik_tree bt on bt.id=bn.tree_id
	where nk.code='metaBiksTreeKind' and bn.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_tree_kind', 'metaBiks.Nazwa typu drzewa', 'caption', null
	exec sp_update_table_column 'aaa_tree_kind', 'metaBiks.Pozwolić dowiązać', 'allow_linking', @binaryConverter

	merge bik_tree_kind tk
	using aaa_tree_kind x on tk.code=x.code
	when matched then update set is_deleted=0, caption=x.caption, allow_linking=x.allow_linking
	when not matched by target then
	insert(code, caption, allow_linking)
	values(x.code, x.caption, x.allow_linking);
 
	truncate table aaa_node_kind_in_tree_kind
	insert into aaa_node_kind_in_tree_kind(id, tree_kind_id, node_kind_id)
	select bn1.id, tk.id, nk2.id
	from bik_node bn1 join bik_node bn2 on bn1.parent_node_id=bn2.id
	join bik_node_kind nk1 on nk1.id=bn1.node_kind_id
	join bik_tree bt on bt.id=bn1.tree_id
	join bik_node_kind nk2 on nk2.code=bn1.name
	join bik_tree_kind tk on tk.code=bn2.name 
	where nk1.code='metaBiksNodeKind4TreeKind' and bn1.is_deleted=0 and bt.id=@metaTree
	
	exec sp_update_table_column 'aaa_node_kind_in_tree_kind', 'metaBiks.Typ w drzewie', 'type', null

	update tk set branch_node_kind_id=x.node_kind_id
	from bik_tree_kind tk join aaa_node_kind_in_tree_kind x on x.tree_kind_id=tk.id 
	where charindex('Gałąź', x.type, 0)>0 or charindex('Główna gałąź', x.type, 0)>0

	update tk set leaf_node_kind_id=x.node_kind_id
	from bik_tree_kind tk join aaa_node_kind_in_tree_kind x on x.tree_kind_id=tk.id 
	where charindex('Liść', x.type, 0)>0  

	update nk4tk set is_deleted=1
	from bik_node_kind_4_tree_kind nk4tk join bik_tree_kind tk on tk.id=nk4tk.tree_kind_id
	left join bik_node_kind snk on snk.id=nk4tk.src_node_kind_id
	left join bik_node_kind dnk on dnk.id=nk4tk.dst_node_kind_id
	where tk.code not like 'metaBiks%' and (snk.code is null or snk.code not like 'metaBiks%') and (dnk.code is null or dnk.code not like 'metaBiks%')

	truncate table aaa_node_kind_4_tree_kind

	insert into aaa_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type)
	select tree_kind_id, null, node_kind_id, null
	from aaa_node_kind_in_tree_kind x
	where charindex('Główna gałąź', x.type, 0)>0

	insert into aaa_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type)
	select tree_kind_id, node_kind_id, null, null
	from aaa_node_kind_in_tree_kind x
	where charindex('Liść', x.type, 0)>0

	truncate table aaa_node_kind_relation_in_tree
	insert into aaa_node_kind_relation_in_tree(id, tree_kind_id, src_node_kind_id, dst_node_kind_id)
	select bn1.id, tk.id, snk.id, dnk.id
	from bik_node bn1 join bik_node bn2 on bn1.parent_node_id=bn2.id
	join bik_node bn3 on bn2.parent_node_id=bn3.id
	join bik_tree bt on bt.id=bn1.tree_id
	join bik_node_kind nk on bn1.node_kind_id=nk.id
	join bik_node_kind snk on bn2.name=snk.code
	join bik_node_kind dnk on bn1.name=dnk.code 
	join bik_tree_kind tk on bn3.name=tk.code
	where nk.code='metaBiksNodeKindInRelation' and bn1.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_node_kind_relation_in_tree', 'metaBiks.Typ relacji', 'relation_type', null

	insert into aaa_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type)
	select tree_kind_id, src_node_kind_id, dst_node_kind_id, 'Powiązanie' from aaa_node_kind_relation_in_tree where charindex('Powiązanie', relation_type, 0) > 0 union
	select tree_kind_id, src_node_kind_id, dst_node_kind_id, 'Dowiązanie' from aaa_node_kind_relation_in_tree where charindex('Dowiązanie', relation_type, 0) > 0 union
	select tree_kind_id, src_node_kind_id, dst_node_kind_id, 'Hyperlink' from aaa_node_kind_relation_in_tree where charindex('Hyperlink', relation_type, 0) > 0 union
	select tree_kind_id, src_node_kind_id, dst_node_kind_id, 'Dziecko' from aaa_node_kind_relation_in_tree where charindex('Dziecko', relation_type, 0) > 0  
 
	merge bik_node_kind_4_tree_kind nk4tk
	using aaa_node_kind_4_tree_kind x on x.tree_kind_id=nk4tk.tree_kind_id and x.src_node_kind_id=nk4tk.src_node_kind_id and x.dst_node_kind_id=nk4tk.dst_node_kind_id and x.relation_type=nk4tk.relation_type
	when matched then update set is_deleted=0
	when not matched by target then
	insert(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type)
	values(x.tree_kind_id, x.src_node_kind_id, x.dst_node_kind_id, x.relation_type);
end
go

exec sp_update_version '1.8.4.8', '1.8.4.9';
go

 