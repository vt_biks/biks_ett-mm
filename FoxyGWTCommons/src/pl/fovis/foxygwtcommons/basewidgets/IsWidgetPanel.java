/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.basewidgets;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author pmielanczuk
 */
public abstract class IsWidgetPanel<W extends Widget & HasWidgets> extends IsWidgetBase<W> {

    @Override
    protected void clearMainWidget() {
        mainWidget.clear();
    }
}
