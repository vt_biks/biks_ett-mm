/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class ThreadActivityCollector {

    public static final int TOP_ACTIVITY_COUNT = 5;
    public static final String TAC_THREAD_VAR_NAME = "TAC_THREAD_VAR";
    private String[] actDetails = new String[TOP_ACTIVITY_COUNT];
    private long[] actElapsed = new long[TOP_ACTIVITY_COUNT];
    private int totalActivities = 0;
    private long totalElapsed = 0;
    private int lastIdx = -1;

    public long getTotalElapsedMillis() {
        return totalElapsed;
    }

    private void addAct(String activityDetails, long elapsedMillis) {

        if (totalActivities == 0 || actElapsed[lastIdx] < elapsedMillis) {

            int i = 0;

            while (i < totalActivities && i < actDetails.length && actElapsed[i] >= elapsedMillis) {
                i++;
            }

            for (int j = lastIdx < actDetails.length - 1 ? lastIdx : lastIdx - 1; j >= i; j--) {
                actElapsed[j + 1] = actElapsed[j];
                actDetails[j + 1] = actDetails[j];
            }

            actDetails[i] = activityDetails;
            actElapsed[i] = elapsedMillis;
        } else {
            if (lastIdx < actDetails.length - 1) {
                actDetails[lastIdx + 1] = activityDetails;
                actElapsed[lastIdx + 1] = elapsedMillis;
            }
        }

        if (lastIdx < actDetails.length - 1) {
            lastIdx++;
        }

        totalActivities++;
        totalElapsed += elapsedMillis;
    }

    private void getInfo(StringBuilder sb) {
        sb.append("total activities: ").append(totalActivities).append(", time taken: ").append(totalElapsed);
        if (totalActivities > actDetails.length) {
            long topElapsed = 0;
            for (int i = 0; i < actElapsed.length; i++) {
                topElapsed += actElapsed[i];
            }

            sb.append("\ntop ").append(actDetails.length).append(" activities time: ").append(topElapsed).append(" = ").append(totalElapsed == 0 ? -1 : topElapsed * 100 / totalElapsed).append("%");
        }

        for (int i = 0; i <= lastIdx; i++) {
            sb.append("\n#").append(i).append(": time: ").append(actElapsed[i]).append(" = ").append(totalElapsed == 0 ? -1 : actElapsed[i] * 100 / totalElapsed).append("%, details: ").append(actDetails[i]);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        getInfo(sb);
        return sb.toString();
    }

    public static void addActivityElapsedTime(String activityKind, String activityDetails, long elapsedMillis) {
        @SuppressWarnings("unchecked")
        Map<String, ThreadActivityCollector> m = (Map) ThreadCustomVars.getVar(TAC_THREAD_VAR_NAME);
        if (m == null) {
            m = new HashMap<String, ThreadActivityCollector>();
            ThreadCustomVars.setVar(TAC_THREAD_VAR_NAME, m);
        }
        ThreadActivityCollector tac = m.get(activityKind);
        if (tac == null) {
            tac = new ThreadActivityCollector();
            m.put(activityKind, tac);
        }

        tac.addAct(activityDetails, elapsedMillis);
    }

    public static Map<String, ThreadActivityCollector> getAndResetCollectedActivities() {
        @SuppressWarnings("unchecked")
        Map<String, ThreadActivityCollector> m = (Map) ThreadCustomVars.getVar(TAC_THREAD_VAR_NAME);
        if (m != null) {
            ThreadCustomVars.setVar(TAC_THREAD_VAR_NAME, null);
        } else {
            m = new HashMap<String, ThreadActivityCollector>();
        }
        return m;
    }

    public static void main(String[] args) {
        ThreadActivityCollector tac = new ThreadActivityCollector();
        tac.addAct("select 1", 20);
        tac.addAct("select 1", 30);
        tac.addAct("select 1", 10);
        tac.addAct("select 1", 120);
        tac.addAct("select 1", 20);
        tac.addAct("select 1", 520);
        System.out.println("tac=" + tac);

        tac = new ThreadActivityCollector();
        tac.addAct("select 1", 20);
        tac.addAct("select 1", 30);
        tac.addAct("select 1", 10);
        System.out.println("tac=" + tac);

        tac = new ThreadActivityCollector();
        tac.addAct("select 1", 100);
        tac.addAct("select 1", 200);
        tac.addAct("select 1", 110);
        tac.addAct("select 1", 120);
        tac.addAct("select 1", 90);
        System.out.println("tac=" + tac);

        tac = new ThreadActivityCollector();
        tac.addAct("select 1", 100);
        tac.addAct("select 1", 200);
        tac.addAct("select 1", 110);
        tac.addAct("select 1", 120);
        tac.addAct("select 1", 90);
        tac.addAct("select 1", 120);
        System.out.println("tac=" + tac);

        tac = new ThreadActivityCollector();
        tac.addAct("select 1", 100);
        tac.addAct("select 1", 90);
        tac.addAct("select 1", 200);
        tac.addAct("select 1", 110);
        tac.addAct("select 1", 120);
        tac.addAct("select 1", 90);
        System.out.println("tac=" + tac);
    }
}
