/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog.AttributeType;
import pl.fovis.client.dialogs.RunScriptDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author ctran
 */
public class NodeActionRunScript extends NodeActionBase {

    protected List<AttributeBean> runnableAttrs = new ArrayList<AttributeBean>();

    public NodeActionRunScript() {
    }

    @Override
    public boolean isEnabledByDesign() {
        runnableAttrs.clear();
        List<AttributeBean> attrs = data.attributes;

        for (AttributeBean bean : attrs) {
            if (AttributeType.sql.name().equals(bean.typeAttr)) {
                runnableAttrs.add(bean);
            }
        }
        return !runnableAttrs.isEmpty();
    }

    @Override
    public String getCaption() {
        return I18n.wykonajSkrypt.get();
    }

    @Override
    public void execute() {
        new RunScriptDialog().buildAndShowDialog(node.id, runnableAttrs);
    }
}
