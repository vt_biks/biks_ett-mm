/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.gwtservlet;

import java.util.Set;
import pl.trzy0.foxy.serverlogic.IFoxySessionPrimitivesProvider;
import pl.trzy0.foxy.serverlogic.IServerSupportedLocaleNameRetriever;

/**
 *
 * @author pmielanczuk
 */
public class ServerSideI18nSupportingServiceImplDef extends ServerSideI18nSupportingServiceImplBase {

    protected IServerSupportedLocaleNameRetriever serverSupportedLocaleNameRetriever;

    public ServerSideI18nSupportingServiceImplDef(IFoxySessionPrimitivesProvider sessionPrimitivesProvider, IServerSupportedLocaleNameRetriever serverSupportedLocaleNameRetriever) {
        super(sessionPrimitivesProvider);
        this.serverSupportedLocaleNameRetriever = serverSupportedLocaleNameRetriever;
    }

    @Override
    public Set<String> getServerSupportedLocaleNames() {
        return serverSupportedLocaleNameRetriever == null ? null : serverSupportedLocaleNameRetriever.getServerSupportedLocaleNames();
    }
}
