exec sp_check_version '1.1.9.26';
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

-- poprawka do [TSB-FIX:14]
-- usuwamy (ustawiamy is_deleted = 1) w�z�y u�ytkownik�w w rolach, 
-- kt�rzy nie maj� przypisanych �adnych obiekt�w (wi�c powinni by� usuni�ci)
-------------------------
-- techniczna sp�jno�� bazy: 14. u�ytkownicy dodani w roli, 
-- ale brak przypisanych obiekt�w do nich (w tej roli)
declare @user_roles_tree_id int = (select id from bik_tree where code = 'UserRoles')
declare @user_node_kind_id int = (select id from bik_node_kind where code = 'User')

update bik_node
set is_deleted = 1
/* declare @user_roles_tree_id int = (select id from bik_tree where code = 'UserRoles')
declare @user_node_kind_id int = (select id from bik_node_kind where code = 'User')
select * --*/
from bik_node n
left join (bik_user u inner join
bik_user_in_node uin on u.id = uin.user_id inner join 
bik_role_for_node rfn on uin.role_for_node_id = rfn.id
) on u.node_id = n.linked_node_id and n.parent_node_id = rfn.node_id
where n.linked_node_id is not null and n.is_deleted = 0
and n.node_kind_id = @user_node_kind_id
and u.id is null
and n.tree_id = @user_roles_tree_id
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

create type bik_unique_not_null_id_table_type as table (id int not null primary key)
go


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_node_init_branch_idEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_node_init_branch_idEx]
GO

create procedure sp_node_init_branch_idEx(@node_ids bik_unique_not_null_id_table_type readonly)
as
begin  
  set nocount on

  declare @diags_level int = 0;


	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #1'

  create table #ids (id int not null primary key);
  
  insert into #ids (id) 
  select id from @node_ids

	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #2'
    
  create table #child_ids (id int not null primary key);
    
  while exists (select 1 from #ids)
  begin
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #3'
    
    update bik_node
    set branch_ids = coalesce(parent.branch_ids, '') + CAST(bik_node.id as varchar(10)) + '|'
    from bik_node inner join #ids on bik_node.id = #ids.id
      left join bik_node as parent on parent.id = bik_node.parent_node_id;    

	/*
    update bik_node
    set branch_ids = coalesce((select parent.branch_ids from bik_node as parent where parent.id = bik_node.parent_node_id), '')
    + CAST(id as varchar(10)) + '|'
    where bik_node.id in (select id from #ids);
    */
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #4'

    truncate table #child_ids;
    
    insert into #child_ids (id)
    select id from bik_node where is_deleted = 0 and parent_node_id in (select id from #ids);
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #5'

    truncate table #ids;
    
    insert into #ids (id) select ID from #child_ids;

	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #6'    
  end
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #7'

  drop table #ids;
  drop table #child_ids;
end
go


-- poprzednia wersja w alter db for v1.1.9.16 pm.sql
-- w nowej - robimy wersj� operuj�c� na wielu node_idkach (procedura EX), dodatkowo 
-- filtrujemy po is_deleted - nie ma sensu wyliczanie branch_ids dla usuni�tych
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_node_init_branch_id]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_node_init_branch_id]
GO

create procedure sp_node_init_branch_id (@opt_tree_id int, @opt_node_id int)
as
begin  
  set nocount on

  declare @node_ids bik_unique_not_null_id_table_type;
  
  insert into @node_ids (id) 
  select id from bik_node 
  where is_deleted = 0 and
    (@opt_tree_id is null or tree_id = @opt_tree_id)
    and 
    (  @opt_node_id is null and parent_node_id is null 
    or 
       @opt_node_id is not null and id = @opt_node_id
    )
   ;

  exec sp_node_init_branch_idEx @node_ids
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_node_init_missing_branch_ids_of_users]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_node_init_missing_branch_ids_of_users]
GO

create procedure sp_node_init_missing_branch_ids_of_users
as
begin  
  set nocount on

declare @user_roles_tree_id int = (select id from bik_tree where code = 'UserRoles')
declare @user_node_kind_id int = (select id from bik_node_kind where code = 'User')
declare @node_ids bik_unique_not_null_id_table_type

insert into @node_ids (id)
select id from bik_node where branch_ids is null and is_deleted = 0
and tree_id = @user_roles_tree_id and node_kind_id = @user_node_kind_id

exec sp_node_init_branch_idEx @node_ids
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_add_missing_users_in_roles_for_existing_objs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_add_missing_users_in_roles_for_existing_objs]
GO

create procedure sp_add_missing_users_in_roles_for_existing_objs
as
begin  
  set nocount on
  
    declare @tree_id int = (select id from bik_tree where code='UserRoles')
    declare @rc int

    --ww: to jest kod Beaty (przeniesiony z adHocSqls.html), mia� b��d - generuje [TSB:8], 
    -- bo dodaje te� u�ytkownik�w w rolach gdy oryginalny u�ytkownik jest usuni�ty
	--
	/*
    insert into bik_node (parent_node_id, node_kind_id, name, tree_id, linked_node_id)
    select x.role_id2, (select id from bik_node_kind where code='User'), x.name, @tree_id, x.user_id
    from (select bu.node_id as user_id,brfn.node_id as role_id2,bu.name from bik_user_in_node buin
    join bik_role_for_node brfn on buin.role_for_node_id=brfn.id
    join bik_user bu on bu.id=buin.user_id
    group by bu.node_id,brfn.node_id,bu.name
    )x left join (select linked_node_id,parent_node_id as role_id,name from bik_node where is_deleted=0
    and tree_id=@tree_id and linked_node_id is not null 
    group by linked_node_id,parent_node_id ,name) y
    on (y.role_id=x.role_id2  and x.user_id=y.linked_node_id )
    where y.linked_node_id is null or x.user_id is null
	--*/
	
	--ww: powy�sze by�o zbyt zagmatwane, ci�ko si� po�apa�, napisz� to pro�ciej, od razu
	-- ze stosown� poprawk� do [TSB:8]	
	-- generalnie s� 3 zagadnienia, na kt�re trzeba uwa�a�
	-- 1. obiekt powi�zany z u�ytkownikiem w roli jest skasowany
	-- 2. rola jest skasowana (a powi�zanie user/obj w tej roli jest)
	-- 3. u�ytkownik jest skasowany (orygina�)
	declare @user_node_kind_id int = (select id from bik_node_kind where code='User')

	insert into bik_node (parent_node_id, node_kind_id, name, tree_id, linked_node_id)
	select roln.id, @user_node_kind_id, u.name, @tree_id, orgn.id
	from
	bik_user u inner join
	bik_node orgn on orgn.id = u.node_id inner join
	bik_user_in_node uin on u.id = uin.user_id inner join 
	bik_node objn on uin.node_id = objn.id inner join
	bik_role_for_node rfn on uin.role_for_node_id = rfn.id inner join
	bik_node roln on roln.id = rfn.node_id left join
	bik_node un on un.linked_node_id = u.node_id and un.parent_node_id = rfn.node_id
	  and un.is_deleted = 0
	where orgn.is_deleted = 0 and objn.is_deleted = 0 and roln.is_deleted = 0 and un.id is null
	
    set @rc = @@ROWCOUNT

    if @rc > 0 begin
		-- ww: poprawia branch_ids tam gdzie trzeba - dla nowo dodanych
		-- kod powy�ej doda chyba tylko jednego (tak wynika z wywo�a�), ale
		-- skoro jest napisany w og�lno�ci (�e niby mo�e doda� wiele u�ytkownik�w
		-- w r�nych rolach) to tutaj te� stosujemy og�lne podej�cie
		exec sp_node_init_missing_branch_ids_of_users
    end

    return @rc
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_add_missing_users_in_roles_for_existing_objs
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

-- poprawka do [TSB-FIX:20]
declare @user_roles_tree_id int = (select id from bik_tree where code = 'UserRoles')
declare @role_node_kind_id int = (select id from bik_node_kind where code = 'UserRole')

insert into bik_role_for_node (caption, node_id)
select n.name, n.id
from bik_node n left join bik_role_for_node rfn on rfn.node_id = n.id
where n.tree_id = @user_roles_tree_id
and n.is_deleted = 0 and n.node_kind_id = @role_node_kind_id
and rfn.id is null
go


------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_update_version '1.1.9.26', '1.1.9.27';
go
