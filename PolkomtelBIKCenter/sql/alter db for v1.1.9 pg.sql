

exec sp_check_version '1.1.9';
go


if not exists (select * from bik_app_prop where name = 'isIconHomeInMenu' )

begin

	insert into bik_app_prop(name, val)
		values('isIconHomeInMenu', 'true')
	
end

exec sp_update_version '1.1.9', '1.1.9.1';
go