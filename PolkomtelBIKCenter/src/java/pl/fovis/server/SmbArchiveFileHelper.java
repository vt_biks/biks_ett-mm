/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import pl.bssg.metadatapump.filesystem.PumpFileUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author ctran
 */
public class SmbArchiveFileHelper implements IArchiveFileHelper {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    protected NtlmPasswordAuthentication auth;

    public SmbArchiveFileHelper(String domain, String user, String pw) {
        this.auth = new NtlmPasswordAuthentication(domain, user, pw);

    }

    @Override
    public void makeSureFolderExists(String dirPath) {
        try {
            SmbFile dir = new SmbFile(PumpFileUtils.fixPathIfNessecary(dirPath), auth);
            if (!dir.exists()) {
                dir.mkdirs();
            }
        } catch (IOException ex) {
            logger.error("error: makeSureFolderExists dirPath=" + dirPath);
        }
    }

    @Override
    public void writeTempFile2SavedFolder(String tempFilePath, String destFilePath) {
        try {
            PumpFileUtils.write2SmbFile(auth, PumpFileUtils.fixPathIfNessecary(destFilePath), new FileInputStream(new File(tempFilePath)));
        } catch (IOException ex) {
            logger.error("error: writeTempFile2SavedFolder tempFilePath=" + tempFilePath + " destFilePath=" + destFilePath + " ex=" + ex.getMessage());
        }
    }

    @Override
    public boolean checkFileExists(String filePath) {
        try {
            SmbFile file = new SmbFile(PumpFileUtils.fixPathIfNessecary(filePath), auth);
            return file.exists();
        } catch (IOException ex) {
//            logger.error("error: checkFileExists filePath=" + filePath);
            return false;
        }
    }
}
