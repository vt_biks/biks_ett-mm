/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.caches;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public class FoxyObjectUpdateData<K, V> {

    private K key;
    private V obj;
    private Map<String, Object> oldKeyVals;
    private FoxyObjectCache<K, V> cache;

    protected FoxyObjectUpdateData(FoxyObjectCache<K, V> cache, K key, V obj) {
        this.cache = cache;
        this.key = key;
        this.obj = obj;
        this.oldKeyVals = cache.getObjectAltKeyVals(obj);
    }

    public V getObj() {
        return obj;
    }

    public void commit() {
        cache.updateObjectAltKeys(obj, oldKeyVals);
    }
}
