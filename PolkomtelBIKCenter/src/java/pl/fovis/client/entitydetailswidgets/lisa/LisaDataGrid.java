/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.dom.client.BrowserEvents;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.layout.client.Layout.Layer;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.CustomScrollPanel;
import com.google.gwt.user.client.ui.HeaderPanel;
import com.google.gwt.user.client.ui.VerticalScrollbar;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ProvidesKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryValue;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class LisaDataGrid extends DataGrid<Map<String, LisaDictionaryValue>> {

    public static final int SEP_COLUMN_SIZE = 5; //pusta kolumna
    public static final int FIRST_COLUMN_SIZE = 40; //checkbox kolumna, Lp. kolumna
    public static final int LAST_COLUMN_SIZE = 20;

    private int seperatorId = 0;
    private int startX; //polozenie kursora gdy zaczyna przyciagac
    private boolean inEditMode = false;
    private boolean clickedAdd = false;
    private List<Integer> colWidth = new ArrayList<Integer>();
    private List<LisaDictColumnBean> columnInfo;
    private Tooltip tooltip = new Tooltip();

    public LisaDataGrid(int pageSize, List<LisaDictColumnBean> columnInfo) {
        super(pageSize, new ProvidesKey<Map<String, LisaDictionaryValue>>() {

            @Override
            public Object getKey(Map<String, LisaDictionaryValue> item) {
                return item.hashCode();
            }
        });
        this.columnInfo = columnInfo;
    }

    public void updateColumnSize(int endX) {
//        System.out.println("startX = " + startX + "  getChoosenColumn(startX) " + getChoosenColumn(startX));
//        System.out.println("endX = " + endX + "  getChoosenColumn(endX) " + getChoosenColumn(endX));

        if ((getChoosenColumn(startX) == getChoosenColumn(endX)) || (seperatorId == 1)) {
            //nie ruszy poza seperator i nie zmieniamy piersza kolumne
            seperatorId = 0;
            return;
        }
//        Window.alert("startX = " + startX + "  getChoosenColumn(startX) " + getChoosenColumn(startX) + "endX = " + endX + "  getChoosenColumn(endX) " + getChoosenColumn(endX));

        int oldWidth = colWidth.get(seperatorId - 1);
        startX = standardize(startX, endX);
        int newWidth = oldWidth + endX - startX;
        if (newWidth < 0) {
            newWidth = 0;
        }
        int newTableWidth = getOffsetWidth() - oldWidth + newWidth;
        setColumnWidthPx(seperatorId - 1, newWidth);
        setWidth(newTableWidth + "px");
        //resizeGrid(getOffsetHeight(), newTableWidth);

        redrawHeaders();
        seperatorId = 0;
    }

    public void setColumnWidthPx(int columnId, int width) {
        super.setColumnWidth(columnId, width, Unit.PX); //To change body of generated methods, choose Tools | Templates.
        if (columnId >= colWidth.size()) {
            colWidth.add(width);
        } else {
            colWidth.set(columnId, width);
        }
    }

    public boolean isInEditMode() {
        return inEditMode;
    }

    public void setInEditMode(boolean isInEditMode) {
        this.inEditMode = isInEditMode;
    }

    public int getSeperatorId() {
        return seperatorId;
    }

    public void setSeperatorId(int seperatorId) {
        this.seperatorId = seperatorId;
    }

    public Integer getStartX() {
        return startX;
    }

    public void setStartX(Integer startX) {
        this.startX = startX;
    }

    public boolean isClickedAdd() {
        return clickedAdd;
    }

    public void setClickedAdd(boolean value) {
        this.clickedAdd = value;
    }

    private int standardize(int startX, int endX) {
        int sumWidth = 0;
        for (int i = 0; i < getChoosenColumn(startX); i++) {
            sumWidth += colWidth.get(i);
        }
        return (startX < endX ? sumWidth + SEP_COLUMN_SIZE : sumWidth);
    }

    public int getChoosenColumn(int startX) {
        int sumWidth = 0;
        //Window.alert("getColumnCount() = " + getColumnCount());
        for (int i = 0; i < colWidth.size(); i++) {
            sumWidth += colWidth.get(i);
            if (sumWidth > startX) {
                return i;
            }
        }
        return colWidth.size() - 1;
    }

    /*
     * Zmian szerokosc tableli
     */
    public void adjustGrid(int width) {
        if (colWidth.size() == 0) {
            //jeszcze nie utworzona
            return;
        }
        Widget parentWidget = getParent();
        if ((parentWidget != null) && parentWidget.getOffsetWidth() > width) {
            width = parentWidget.getOffsetWidth();
        }
        setWidth(width + "px");
        int oldTableWidth = 0;
        for (int i = 2; i < colWidth.size() - 1; i += 2) {
            oldTableWidth += colWidth.get(i);
        }
        int availableWidth = width - FIRST_COLUMN_SIZE - (colWidth.size() / 2) * SEP_COLUMN_SIZE - LAST_COLUMN_SIZE;
        double ratio = ((double) availableWidth) / oldTableWidth;
        int consumedWidth = 0;
        for (int i = 2; i < colWidth.size() - 3; i += 2) {
            int newWidth = (int) Math.round(ratio * colWidth.get(i));
            consumedWidth += newWidth;
            setColumnWidthPx(i, newWidth);
        }
        setColumnWidthPx(colWidth.size() - 3, availableWidth - consumedWidth);
    }

    public void setVerticalScrollBarAlwaysVisible(boolean alwaysVisible) {
        if (!alwaysVisible || getParent() == null) {
            return;
        }
        HeaderPanel panel = (HeaderPanel) getWidget();
        CustomScrollPanel scrollPanel = (CustomScrollPanel) panel.getContentWidget();
        VerticalScrollbar scrollBar = scrollPanel.getVerticalScrollbar();
        Layer layer = (Layer) scrollBar.asWidget().getLayoutData();
        layer.setLeftWidth(0, Unit.PX, 800, Unit.PX);
    }

    public int getColumnWidth(int columnId) {
        return colWidth.get(columnId);
    }

    @Override
    protected void onBrowserEvent2(Event event) {
        super.onBrowserEvent2(event); //To change body of generated methods, choose Tools | Templates.
        String type = event.getType();
        if (!inEditMode) {
            if (BrowserEvents.MOUSEOVER.equalsIgnoreCase(type)) {
                tooltip.hide();
                String text = getColumnTextDisplayed(event);
                if (!BaseUtils.isStrEmptyOrWhiteSpace(text)) {
                    tooltip.setText(getColumnTextDisplayed(event));
                    tooltip.setPopupPosition(event.getClientX(), event.getClientY());
                    tooltip.show();
                }
            } else if (BrowserEvents.MOUSEOUT.equalsIgnoreCase(type)) {
                tooltip.hide();
            }
        }
    }

    private String getColumnTextDisplayed(Event event) {
        int id = (getChoosenColumn(event.getClientX()) - 1) / 2 - 1;
        if (id < 0) {
            return "";
        }
        LisaDictColumnBean column = columnInfo.get(id);
        return column.textDisplayed;
    }
}
