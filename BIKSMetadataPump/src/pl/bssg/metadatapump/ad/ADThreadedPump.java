/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.ad;

import commonlib.LameUtils;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.IContinuation;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
@Deprecated
public class ADThreadedPump implements Runnable {

    private static ILameLogger logger = LameUtils.getMyLogger();
    protected INamedSqlsDAO<Object> adhocDaoForPump;
    protected String loginName;
    protected boolean addSocialUserAndLink;

    public ADThreadedPump(INamedSqlsDAO<Object> adhocDaoForPump, String loginName, boolean addSocialUserAndLink) {
        this.adhocDaoForPump = adhocDaoForPump;
        this.loginName = loginName;
        this.addSocialUserAndLink = addSocialUserAndLink;
    }

    public void run() {
        final ADPump adPumpForOneLogin = new ADPump(loginName);
        adPumpForOneLogin.setAdhocDao(adhocDaoForPump);
        boolean success = false;
        try {
            adhocDaoForPump.execInAutoCommitMode(new IContinuation() {
                public void doIt() {
                    adPumpForOneLogin.pump();
                    if (addSocialUserAndLink) {
                        // teraz jest w adhocSqls
//                        adhocDaoForPump.execNamedCommand("createSocialUserAndLink", loginName);
                    }
                }
            });
            success = true;
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error with connection with domain controller", e);
            }
        } finally {
            adhocDaoForPump.finishWorkUnit(success);
        }
    }
}
