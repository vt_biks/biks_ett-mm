/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class CSVReader {

    public static final char STRING_QUOTE_CHAR = '\"';
    public static final String DOUBLE_QUOTE_STRING = STRING_QUOTE_CHAR + "" + STRING_QUOTE_CHAR;
    public static final char DEFAULT_SEPARATOR = ';';

    public static enum SepKind {

        None, Col, Row, Eof
    };
    protected char valueSepChar;
    private String txt;
    private int pos;
    private SepKind lastSepKind = SepKind.None;
    private String lastItem;
    //  protected boolean requireStringQuote = true;
    protected CSVParseMode parseMode = CSVParseMode.REQUIRE_QUOTES;
    protected boolean disableBackslashEscaping = false;
    protected List<String> colNames;

    public CSVReader(String txt, char separator) {
        this(txt, true, separator);
    }

    public CSVReader(String txt, boolean requireStringQuote, char separator) {
        this(txt, requireStringQuote ? CSVParseMode.REQUIRE_QUOTES : CSVParseMode.DONT_CONSIDER_QUOTES, separator, false);
    }

    public CSVReader(String txt, CSVParseMode quoteParseMode, char separator, boolean disableBackslashEscaping) {
        this.txt = txt;
        this.parseMode = quoteParseMode;
        this.valueSepChar = separator;
        this.disableBackslashEscaping = disableBackslashEscaping;
    }

    public CSVReader(String txt, boolean requireStringQuote) {
        this(txt, requireStringQuote, DEFAULT_SEPARATOR);
    }

    public CSVReader(String txt) {
        this(txt, true, DEFAULT_SEPARATOR);
    }

    public String readValue() {
        if (pos == txt.length()) {
            pos++;
            lastItem = "";
            lastSepKind = SepKind.Eof;
            return lastItem;
        }

        if (pos > txt.length()) {
            lastItem = null;
            lastSepKind = SepKind.Eof;
            return lastItem;
        }

        char startC = txt.charAt(pos);

        boolean isSeparatorChar = startC == valueSepChar || startC == '\r' || startC == '\n';

        boolean isProperChar
                = !parseMode.equals(CSVParseMode.REQUIRE_QUOTES)
                || startC == STRING_QUOTE_CHAR
                || isSeparatorChar;

        if (!isProperChar) {
            throw new LameRuntimeException("expected \" as value quote char or any proper sep, found: \'"
                    + startC + "\', at pos="
                    + pos);
        }

        if (!isSeparatorChar) {

            int pPos;

            boolean valStartedWithQuoteChar = startC == STRING_QUOTE_CHAR;
            if (valStartedWithQuoteChar) {
                pPos = ++pos;
            } else {
                pPos = pos;
            }

            boolean wasEscape = false;
            while (pos < txt.length()) {
                char c = txt.charAt(pos);
                boolean isSepOrEOL = (c == valueSepChar || c == '\n' || c == '\r');
                if ((valStartedWithQuoteChar && c == STRING_QUOTE_CHAR && !wasEscape
                        && (pos + 1 >= txt.length() || txt.charAt(pos + 1) != STRING_QUOTE_CHAR))
                        || isSepOrEOL && (parseMode.equals(CSVParseMode.DONT_CONSIDER_QUOTES) || !valStartedWithQuoteChar)) {
                    break;
                }

                if (valStartedWithQuoteChar) {
                    if (c == '\\' && !disableBackslashEscaping || c == STRING_QUOTE_CHAR) {
                        wasEscape = !wasEscape;
                    } else {
                        wasEscape = false;
                    }
                }
                pos++;
            }

            if (valStartedWithQuoteChar && pos >= txt.length()) {
                throw new LameRuntimeException("expected quote char at end of value but eof found");
            }

            lastItem = new String(txt.substring(pPos, pos));

            if (valStartedWithQuoteChar) {
                if (!disableBackslashEscaping) {
                    lastItem = lastItem.replace("\\" + STRING_QUOTE_CHAR, "" + STRING_QUOTE_CHAR).replace("\\\\", "\\");
                }
                lastItem = lastItem.replace(DOUBLE_QUOTE_STRING, STRING_QUOTE_CHAR + "");
                pos++;
            }

        } else {
            lastItem = "";
        }

        if (pos >= txt.length()) {
            lastSepKind = SepKind.Eof;
        } else {
            char endVC = txt.charAt(pos);
            if (endVC == valueSepChar) {
                lastSepKind = SepKind.Col;
                pos++;
            } else {
                if (endVC != '\n' && endVC != '\r') {
                    throw new LameRuntimeException("expected colsep ; or rowsep CRLF/CL/LF");
                }
                lastSepKind = SepKind.Row;
                // CR <-> \r
                // LF <-> \n
                boolean isCR = endVC == '\r';
                pos++;
                if (isCR && pos < txt.length()) {
                    if (txt.charAt(pos) == '\n') {
                        pos++;
                    }
                }
            }
        }
        return lastItem;
    }

    protected boolean isLastEmptyRow(List<String> row) {
        return row.size() == 1 && row.get(0).length() == 0 && lastSepKind == SepKind.Eof;
    }

    public boolean hasMoreValues() {
        return lastSepKind != SepKind.Eof && txt.length() != 0;
    }

    public List<String> readRow() {
        return readRow(true);
    }

    public List<String> readRow(boolean eliminateLastEmptyRow) {
        if (!hasMoreValues()) {
            return null;
        }

        SepKind sk = SepKind.None;

        List<String> res = new ArrayList<String>();

        while (sk != SepKind.Row && sk != SepKind.Eof) {
            String val = readValue();
            res.add(val);
            sk = lastSepKind;
        }

        if (eliminateLastEmptyRow && isLastEmptyRow(res)) {
            return null;
        }

        return res;
    }

    public Map<String, String> readRowAsMap(List<String> colNames) {
        Map<String, String> res = new LinkedHashMap<String, String>();
        List<String> row = readRow(true);
        if (row == null) {
            return null;
        }
        int lim = Math.max(colNames.size(), row.size());
        for (int i = 0; i < lim; i++) {
            String colName = i < colNames.size() ? colNames.get(i) : "_col_" + i;
            String val = i < row.size() ? row.get(i) : null;
            res.put(colName, val);
        }
        return res;
    }

    public List<List<String>> readRowsAsLists() {
        List<List<String>> res = new ArrayList<List<String>>();
        int i = 1;
        while (hasMoreValues()) {
            List<String> row = null;
            try {
                row = readRow(true);
                i++;
            } catch (Exception e) {
                System.out.println("Error in readCSVFile. Row: " + i);
            }
            if (row != null) {
                res.add(row);
            }
        }
        return res;
    }

    public List<Map<String, String>> readRowsAsMaps(List<String> colNames) {
        List<Map<String, String>> res = new ArrayList<Map<String, String>>();
        while (hasMoreValues()) {
            Map<String, String> row = readRowAsMap(colNames);
            if (row != null) {
                res.add(row);
            }
        }
        return res;
    }

    public List<String> getColNames() {
        return colNames;
    }

    public List<Map<String, String>> readFullTableAsMaps() {
        if (BaseUtils.isCollectionEmpty(colNames)) {
            colNames = readRow();
        }
        return readRowsAsMaps(colNames);
    }

    public enum CSVParseMode {

        REQUIRE_QUOTES,
        CONSIDER_QUOTES,
        DONT_CONSIDER_QUOTES
    }

//    public static void main(String[] args) {
//
//        int y1 = 7;
//        Integer y2 = null;
//
//        Integer res = false ? y1 : y2; // to się wywali!!! null-pointer!
//
//        System.out.println("res=" + res);
//
////        int nullable = 2;
////        Boolean isNullable2 = nullable == 2 ? null : true;
////        Boolean isNullable = nullable == 0 ? false : isNullable2;
////        isNullable = (nullable == 0 ? false : (nullable == 2 ? null : true));
////        System.out.println("isNullable=" + isNullable + ", isNullable2=" + isNullable2);
//    }
}
