﻿exec sp_check_version '1.0.63';

IF EXISTS(SELECT * FROM sys.tables where name ='bik_sapbo_extradata')
	DROP TABLE bik_sapbo_extradata;
go

IF EXISTS(SELECT * FROM sys.tables where name ='bik_sapbo_extradata_old')
	DROP TABLE bik_sapbo_extradata_old;
go

IF EXISTS(SELECT * FROM sys.views where name ='bik_sapbo_extradata')
	DROP VIEW bik_sapbo_extradata;
go

create table bik_sapbo_extradata(
	node_id int not null primary key references bik_node (id),
	author varchar(1000) null,
	owner varchar(1000) null,
	keyword varchar(1000) null,
	universe_node_id int null references bik_node (id),
	connection_node_id int null references bik_node (id)
);
go


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_sapbo_extradata]
GO

create procedure sp_insert_into_bik_node_sapbo_extradata
as
begin
insert into bik_sapbo_extradata  
    select bik.id as node_id, 
    nullif(ltrim(rtrim(p.SI_AUTHOR)), '') as author, 
    nullif(ltrim(rtrim(p.SI_OWNER)), '') as owner,
    nullif(ltrim(rtrim(p.SI_KEYWORD)), '') as keyword,
    uni.id as universe_node_id, conn.id as connection_node_id    
    from aaa_global_props p left join INFO_WEBI iw on p.si_id = iw.si_id
    left join APP_UNIVERSE au on iw.SI_UNIVERSE__1 = au.si_id
    left join APP_METADATA__DATACONNECTION amdc on au.SI_DATACONNECTION__1 = amdc.si_id
    inner join bik_node bik on convert(varchar,p.si_id) = bik.obj_id
    left join bik_node uni on convert(varchar,au.si_id) = uni.obj_id and uni.tree_id=(select id from bik_tree where name='Światy obiektów')
    left join bik_node conn on convert(varchar, amdc.si_id) = conn.obj_id and conn.tree_id=(select id from bik_tree where name='Połączenia')
    left join bik_sapbo_extradata spe on bik.id =spe.node_id
    where spe.node_id is null 
    order by bik.id 
end
go

exec sp_update_version '1.0.63', '1.0.64';
go
