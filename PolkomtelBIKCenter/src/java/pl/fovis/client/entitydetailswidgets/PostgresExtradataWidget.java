/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import pl.fovis.common.BIKConstants;

/**
 *
 * @author tflorczak
 */
public class PostgresExtradataWidget extends DatabaseExtradataWidget {

    @Override
    protected boolean isTableOrView() {
        return getData().node.nodeKindCode.equals(BIKConstants.NODE_KIND_POSTGRES_TABLE) || getData().node.nodeKindCode.equals(BIKConstants.NODE_KIND_POSTGRES_VIEW);
    }

    @Override
    protected String getFKColumnIconName() {
        return "images/postgresColumnFK.gif";
    }

    @Override
    protected String getClusteredIndexIconName() {
        return "images/postgresIndex.gif";
    }

    @Override
    protected String getNonClusteredIndexIconName() {
        return "images/postgresIndex.gif";
    }

    @Override
    protected boolean treeIdFilter() {
        return getData().node.treeCode.equals(BIKConstants.TREE_CODE_POSTGRES);
    }
}
