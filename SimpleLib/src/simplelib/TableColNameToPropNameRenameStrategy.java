/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author wezyr
 */
public class TableColNameToPropNameRenameStrategy implements INamedPropsRenameStrategy {

    public Iterable<String> getSrcPropNames(INamedPropsBean src) {
        return src.getPropNames();
    }

    public String getDestPropName(INamedPropsBean src, INamedPropsBeanWriter dst, String srcPropName) {
        String fixedPropName = BaseUtils.tableColNameToBeanFieldName(srcPropName);
        if (dst.getPropNames().contains(fixedPropName)) {
            return fixedPropName;
        }
        return null;
    }

    private static class SingletonHolder {

        private static final TableColNameToPropNameRenameStrategy INSTANCE = new TableColNameToPropNameRenameStrategy();
    }

    public static TableColNameToPropNameRenameStrategy getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
