/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.TreeNodeDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class NodeActionNewNestableParent extends NodeActionUpdatingBase {

    protected String captionOverride;

    public NodeActionNewNestableParent() {

    }

    public NodeActionNewNestableParent(String captionOverride) {
        this.captionOverride = captionOverride;
    }

    @Override
    public String getCaption() {
        if (captionOverride != null) {
            return captionOverride;
        }
        return I18n.dodaj.get() /* I18N:  */ + ": " + treeNodeKind.caption;
    }

    @Override
    public void execute() {
        if (!isSocialUserLoggedInWithInfo()) {
            return;
        }

        BIKClientSingletons.getService().getRequiredAttrs(treeBean.nodeKindId, new StandardAsyncCallback<List<AttributeBean>>() {
            @Override
            public void onSuccess(final List<AttributeBean> result) {

                BIKClientSingletons.getService().getBikAllArts(treeBean.id, new StandardAsyncCallback<List<String>>() {
                    @Override
                    public void onSuccess(final List<String> keywords) {

                        new TreeNodeDialog().buildAndShowDialog(treeBean.nodeKindId, treeBean.nodeKindCode, null,
                                node, treeBean.id, keywords,
                                result,
                                new IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>>() {
                                    public void doIt(final Pair<TreeNodeBean, Map<String, AttributeBean>> param2) {
                                        TreeNodeBean param = param2.v1;
                                        BIKClientSingletons.getService().createTreeNodeWithAttr(param.parentNodeId,
                                                param.nodeKindId, param.name, param.objId, param.treeId, param.linkedNodeId, null,
                                                param2.v2,
                                                new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on" /* I18N: no */ + " createTreeNode") {
                                                    @Override
                                                    public void onSuccess(Pair<Integer, List<String>> result) {
                                                        BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get() /* I18N:  */);
                                                        refreshRedisplayDataAndSelectRecord(result.v1);
                                                    }
                                                });
                                    }
                                });
                    }
                });
            }
        });
    }

    @Override
    public boolean isEnabledByDesign() {

//        Window.alert("NewNestableParent: node==null? " + (node == null) + ", getOptCurrentNodeActionCode: " + getOptCurrentNodeActionCode());
        if (isInUserRolesTree()) {
            return false;
        }
        if (isInDQMAdminMode() && (node == null || isNodeFolder())) {
//            Window.alert("NewNestableParent: is DQM Admin mode...");
            return true;
        }
//        Window.alert("NewNestableParent: no DQM Admin mode...!!! isDQMTree(): " + isDQMTree() + ", isEffectiveExpert(): " + isEffectiveExpert());
        return node == null && (!isTreeReadOnly() || isLoggedUserAuthorOfTree()) || isNodeNotLinkedAnyhowNorReadOnlyTreeAndNodeIsFolder() && !isNodeFromDynamicTree()
                && !isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_ALL_USERS_FOLDER) && !isConfluenceObject();
    }

//    protected boolean canBeEnabledByStateForThisNode() {
//        return !isTreeReadOnlyInner() && (node == null || isNodeFolder()) && !isNodeLinkedAnyhow();
//    }
//
//    @Override
//    protected boolean canBeEnabledByStateForThisNodeAndUserLoggedInByDesign() {
//        return (canBeEnabledByStateForThisNode() && (isEffectiveExpert() || isLoggedUserAuthorOfTree())) || isInDQMAdminMode();
//    }
}
