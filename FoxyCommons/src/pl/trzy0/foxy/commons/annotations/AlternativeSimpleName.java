/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons.annotations;

/**
 *
 * @author wezyr
 */
public @interface AlternativeSimpleName {
    String value();
    boolean replacesDefaultName() default false;
}
