/*
 * LameUtilsTest.java
 * JUnit based test
 *
 * Created on 30 stycze� 2007, 09:07
 */
package commonlib;

import java.util.Set;
import junit.framework.TestCase;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author msmoktunowicz
 */
public class LameUtilsTest extends TestCase {

    public LameUtilsTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
    }

    @Override
    protected void tearDown() throws Exception {
    }

    /**
     * Test of stringBetween method, of class commonlib.LameUtils.
     */
    public void testStringBetween() {
        String str = "smok@@obibok::sss";
        String fromStr = "@@";
        String toStr = "::s";

        String expResult = "";
        String result = LameUtils.stringBetween(str, fromStr, toStr);
        assertEquals("obibok", result);
    }

    public void testParseParamStm() {
        String str = "ax$ax$ax$ss";
        int result = LameUtils.nthIndexOf(str, "$", 1);
        assertEquals(2, result);
        result = LameUtils.nthIndexOf(str, "$", 0);
        assertEquals(-1, result);
        result = LameUtils.nthIndexOf(str, "$", 2);
        assertEquals(5, result);
        result = LameUtils.nthIndexOf(str, "$", 3);
        assertEquals(8, result);
        result = LameUtils.nthIndexOf(str, "$", 4);
        assertEquals(-1, result);
        Pair<String, String> pair = LameUtils.splitStringCnt(str, "$", 2);
        assertEquals("ax$ax", pair.v1);
        assertEquals("ax$ss", pair.v2);

        pair = LameUtils.splitStringCnt(str, "$", 3);
        assertEquals("ax$ax$ax", pair.v1);
        assertEquals("ss", pair.v2);
    }

    public void testExtractFileExt() {
        assertEquals("jpg", LameUtils.extractFileExtNoDot("ala.jpg"));
        assertEquals("jpg", LameUtils.extractFileExtNoDot("c:\\ala.ma.kota\\ala.jpg"));
        assertEquals("gif", LameUtils.extractFileExtNoDot("c:\\ala.ma.kota.jpg\\ala.jpg.gif"));
        assertEquals("", LameUtils.extractFileExtNoDot("c:\\ala.ma.kota.jpg\\ala.jpg.gif\\xxx."));
        assertEquals("", LameUtils.extractFileExtNoDot("c:\\ala.ma.kota.jpg\\ala.jpg.gif\\xxx"));
        assertEquals("", LameUtils.extractFileExtNoDot("c:\\"));
        assertEquals("", LameUtils.extractFileExtNoDot("a"));
        assertEquals("", LameUtils.extractFileExtNoDot("a."));
        assertEquals("", LameUtils.extractFileExtNoDot("/a.b/c./x"));
        assertEquals("x", LameUtils.extractFileExtNoDot("/a.b/c/.x"));
        assertEquals("", LameUtils.extractFileExtNoDot("/a.bc.x/a"));
        //System.out.println("File.separator: " + File.separator);
        //System.out.println("File.pathSeparator: " + File.pathSeparator);
    }

    public void testExtractFileDir() {
        assertEquals(null, LameUtils.extractFileDir("ala.jpg"));
        assertEquals("c:\\", LameUtils.extractFileDir("c:\\ala.jpg"));
        assertEquals("c:\\makota\\", LameUtils.extractFileDir("c:\\makota\\ala.jpg"));
        assertEquals("c:\\kto\\dogoni\\", LameUtils.extractFileDir("c:\\kto\\dogoni\\psa.jpg"));
        assertEquals("\\kto\\dogoni\\", LameUtils.extractFileDir("\\kto\\dogoni\\psa.jpg"));
        assertEquals("\\", LameUtils.extractFileDir("\\psa.jpg"));

        assertEquals("c:/", LameUtils.extractFileDir("c:/ala.jpg"));
        assertEquals("c:/makota/", LameUtils.extractFileDir("c:/makota/ala.jpg"));
        assertEquals("c:/kto/dogoni/", LameUtils.extractFileDir("c:/kto/dogoni/psa.jpg"));
        assertEquals("/kto/dogoni/", LameUtils.extractFileDir("/kto/dogoni/psa.jpg"));
        assertEquals("/", LameUtils.extractFileDir("/psa.jpg"));

    }

    static enum TestEnum {

        ala, ma, kota
    };

    public void testIntToEnum() {
        //EnumSet<TestNum> s = EnumSet.allOf(TestNum.class);

        assertEquals(TestEnum.ala, LameUtils.intToEnum(TestEnum.class, 0));
        assertEquals(TestEnum.ma, LameUtils.intToEnum(TestEnum.class, 1));
        assertEquals(TestEnum.kota, LameUtils.intToEnum(TestEnum.class, 2));
    }

    public void testReplaceAllLinksInWikipedia() {
        String pattern = "\\[\\[([^\\]\\[|]*\\|)?([^|\\]\\[]*)[^\\]\\[]*\\]\\]";

        String body = "ala ma kota";
        body = body.replaceAll(pattern, "$2");
        assertEquals("ala ma kota", body);
        body = "ala [[ma|mia�a]] kota".replaceAll(pattern, "$2");
        assertEquals("ala mia�a kota", body);
        body = "ala [[ma]] kota".replaceAll(pattern, "$2");
        assertEquals("ala ma kota", body);
        body = "ala [[ma|chcia�a|mia�a]] kota".replaceAll(pattern, "$2");
        assertEquals("ala chcia�a kota", body);
        body = "[[ma|chcia�a|mia�a]] kota".replaceAll(pattern, "$2");
        assertEquals("chcia�a kota", body);
        body = "ala[[ma| chcia�a|mia�a]]".replaceAll(pattern, "$2");
        assertEquals("ala chcia�a", body);
        body = "ala[[ma| chcia�a|[[mia�a|x]]]]".replaceAll(pattern, "$2");
        body = body.replaceAll(pattern, "$2");
        assertEquals("ala chcia�a", body);
        body = "ala [[ma|[[mia�a|x]]]]".replaceAll(pattern, "$2");
        body = body.replaceAll(pattern, "$2");
        assertEquals("ala x", body);

        String pattern2 = "\\[([^\\]\\[ \t\n\r<]+[ \t\n\r]+)?([^\\]\\[<]*)\\]";
        body = "ala [ma chcia�a] [<nielink>]".replaceAll(pattern2, "$2");
        assertEquals("ala chcia�a [<nielink>]", body);
        body = "ala [ma+chcia�a] [<nielink>]".replaceAll(pattern2, "$2");
        assertEquals("ala ma+chcia�a [<nielink>]", body);
        body = "ala [http://x.y.z]ko�".replaceAll(pattern2, "$2");
        assertEquals("ala http://x.y.zko�", body);
        body = "ala [http://x.y.z adres] konia".replaceAll(pattern2, "$2");
        assertEquals("ala adres konia", body);
        body = "ala [http://x.y.z adres] konia".replaceAll(pattern2, "$2");
        assertEquals("ala adres konia", body);
    }

    public void testReplaceAllHtmlLinks() {
        String patternOpen = "<[aA][ \t\n\r]+[^>]*[hH][rR][eE][fF][^>]*>";
        String patternClose = "<[ \t\n\r]*/[ \t\n\r]*[aA][ \t\n\r]*>";
        String body = "ala <a  href = 'pig' >ma</A> kota";
        body = body.replaceAll(patternOpen, "");
        body = body.replaceAll(patternClose, "");
        assertEquals("ala ma kota", body);

        body = "ala ma kota";
        body = body.replaceAll(patternOpen, "");
        body = body.replaceAll(patternClose, "");
        assertEquals("ala ma kota", body);

        body = "ala <A href='a'><b>ma</b></a> kota";
        body = body.replaceAll(patternOpen, "");
        body = body.replaceAll(patternClose, "");
        assertEquals("ala <b>ma</b> kota", body);

        body = "ala <i><A href='a'><b>ma</b></a></i> kota";
        body = body.replaceAll(patternOpen, "");
        body = body.replaceAll(patternClose, "");
        assertEquals("ala <i><b>ma</b></i> kota", body);

        assertEquals("ala ma kota", LameUtils.removeHtmlLinks("ala <a  href = 'pig' >ma</A> kota"));
        assertEquals("ala ma kota", LameUtils.removeHtmlLinks("ala ma kota"));
        assertEquals("ala <b>ma</b> kota", LameUtils.removeHtmlLinks("ala <A href='a'><b>ma</b></a> kota"));
        assertEquals("ala <i><b>ma</b></i> kota", LameUtils.removeHtmlLinks("ala <i><A href='a'><b>ma</b></a></i> kota"));
    }

    public void testMergeResourcePath() {
        assertEquals("/obibok", LameUtils.mergeResourcePaths("/smok/", "../obibok"));
        assertEquals("/1/obibok", LameUtils.mergeResourcePaths("/1/2/3/", "../../obibok"));
        assertEquals("/obibok", LameUtils.mergeResourcePaths("/", "obibok"));
        assertEquals("/obibok", LameUtils.mergeResourcePaths("", "/obibok"));
        boolean wasException = false;
        try {
            LameUtils.mergeResourcePaths("/", "../obibok");
        } catch (Exception ex) {
            wasException = true;
        }
        assertEquals(true, wasException);
    }

    public void testGetCommonItemsEx() {
        String[] arr1 = {"26s", "26s3000", "3000", "kdl", "kdl26", "kdl26s", "kdl26s3000", "s3000"};
        String[] arr2 = {"26s3000", "kdl", "kdl26s3000"};

        Set<String> res = LameUtils.getCommonItemsEx(arr1, arr2);

        assertEquals(3, res.size());
    }

    public void testSeparatorsToSpace() {
        String s = " \u0010ala\u0001kot?\u001F-";
        String r = LameUtils.separatorsToSpace(s);
        assertEquals("  ", r.substring(0, 2));
        assertEquals(" ", r.substring(5, 6));
        assertEquals("   ", r.substring(9));
    }

    public void testTrimAndCompactSpaces() {
        String s = "  ala  ma kota    x  ?   ";
        String r = LameUtils.trimAndCompactSpaces(s);
        assertEquals("ala ma kota x ?", r);
    }

    public void testGetFirstWords() {
        String str = "ala ma kota";
        assertEquals("ala ma", BaseUtils.getFirstWords(str, 2));
        assertEquals("ala ma kota", BaseUtils.getFirstWords(str, 3, -1));
        assertEquals("ala ma", BaseUtils.getFirstWords(str, 5, 7));
        assertEquals("ala ma", BaseUtils.getFirstWords(str, 5, 6));
        assertEquals("ala m", BaseUtils.getFirstWords(str, 5, 5));
        assertEquals("ala", BaseUtils.getFirstWords(str, 1, 5));
        assertEquals("ala", BaseUtils.getFirstWords(str, 1));
        assertEquals("ala ma kota", BaseUtils.getFirstWords(str, 100));
    }
    private static final char[] hexDigits = "0123456789ABCDEF".toCharArray();

    private String getHHBB(byte b) {
        StringBuilder sb = new StringBuilder();
        sb.append("%").append(hexDigits[(b >> 4) & 0xf]).append(hexDigits[b & 0xf]);
        return sb.toString();
    }

    private String getHHBBs(byte[] bs) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bs) {
            sb.append("%").append(hexDigits[(b >> 4) & 0xf]).append(hexDigits[b & 0xf]);
        }
        return sb.toString();
    }

    private String getHH(char c) {
        StringBuilder sb = new StringBuilder();
        if (c > 0xff) {
            sb.append("%").append(hexDigits[c >> 12]).append(hexDigits[(c >> 8) & 0xf]);
        }
        sb.append("%").append(hexDigits[(c >> 4) & 0xf]).append(hexDigits[c & 0xf]);
        return sb.toString();
    }

    public static abstract class SingleTestForNTimes<T, R> {

        public String testName;

        public SingleTestForNTimes(String testName) {
            this.testName = testName;
        }

        public abstract R execOneTime(T testVal);
    }

    protected void doTestEncodeForUrlNTimes(int n,
            SingleTestForNTimes<String, String> testSingle,
            String... testVals) {
        long startNano = System.nanoTime();
        for (int i = 0; i < n; i++) {
            for (String testVal : testVals) {
                String res = //UrlMakingUtils.encodeForURLNoExcWWOldLame(param);
                        testSingle.execOneTime(testVal);
            }
        }
        long endNano = System.nanoTime();
        System.out.println("Test \"" + testSingle.testName + "\" results: " + n + " executions, total time: "
                + BaseUtils.doubleToString((endNano - startNano) / 1000000000.0d, 6) + " s, " + (endNano - startNano) / n
                + " nanos per one execution, testVals=" + BaseUtils.paramsAsSet(testVals));
    }

    public void testEncodeForUrlMany() {
        int repeatCnt = 1000000;
        String[] testVals = new String[]{"za��� g�l� ja��", "parX", "ala ma kota"};
        doTestEncodeForUrlNTimes(repeatCnt,
                new SingleTestForNTimes<String, String>("encodeForURLNoExcSlow") {

                    @Override
                    public String execOneTime(String testVal) {
                        return UrlMakingUtils.encodeForURLNoExcSlow(testVal);
                    }
                }, testVals);
        doTestEncodeForUrlNTimes(repeatCnt,
                new SingleTestForNTimes<String, String>("encodeForURLNoExc") {

                    @Override
                    public String execOneTime(String testVal) {
                        return UrlMakingUtils.encodeStringForUrlWW(testVal);
                    }
                }, testVals);
        doTestEncodeForUrlNTimes(repeatCnt,
                new SingleTestForNTimes<String, String>("encodeForURLNoExcWWOld") {

                    @Override
                    public String execOneTime(String testVal) {
                        return UrlMakingUtils.encodeForURLNoExcWWOld(testVal);
                    }
                }, testVals);
        doTestEncodeForUrlNTimes(repeatCnt,
                new SingleTestForNTimes<String, String>("encodeForURLNoExcWW") {

                    @Override
                    public String execOneTime(String testVal) {
                        return UrlMakingUtils.encodeStringForUrlWW(testVal);
                    }
                }, testVals);
    }

    public void testEncodeForURLNoExcFast() {
        assertEquals("ala+ma+kota", UrlMakingUtils.encodeStringForUrlWW("ala ma kota"));
        byte b = (byte) 0xc7;
        System.out.println("b=" + b + "b>>4=" + ((b >> 4) & 0xf));
        System.out.println("za��� g�l� ja�� -> " + UrlMakingUtils.encodeForURLNoExcSlow("za��� g�l� ja��"));
        System.out.println("za��� g�l� ja�� -> " + UrlMakingUtils.encodeStringForUrlWW("za��� g�l� ja��"));
        System.out.println("za��� g�l� ja�� -> " + UrlMakingUtils.encodeStringForUrlWW("za��� g�l� ja��"));
//        try {
//            byte[] bb = "�".getBytes("utf-8");
//            System.out.println("� -> " + getHHBBs(bb));
//        } catch (Exception ex) {
//        }
        //System.out.println("� -> " + getHH('�'));
        assertEquals("za%C5%BC%C3%B3%C5%82%C4%87+g%C4%99%C5%9Bl%C4%85+ja%C5%BA%C5%84", UrlMakingUtils.encodeStringForUrlWW("za��� g�l� ja��"));
        assertEquals("za%C5%BC%C3%B3%C5%82%C4%87+g%C4%99%C5%9Bl%C4%85+ja%C5%BA%C5%84", UrlMakingUtils.encodeStringForUrlWW("za��� g�l� ja��"));
    }

    public void testGetPackagePathOfClass() {
        System.out.println("getPackagePathOfClass(LameUtils.class): "
                + LameUtils.getPackagePathOfClass(LameUtils.class));
    }

    public void testParseDate() {
        System.out.println(BaseUtils.safeToStringDef(LameUtils.parseDate("1970-01-01 00:00:01"), "NULL"));
        System.out.println(LameUtils.formatDate(LameUtils.parseDate("1970-01-01 00:00:01.000")));

    }
}
