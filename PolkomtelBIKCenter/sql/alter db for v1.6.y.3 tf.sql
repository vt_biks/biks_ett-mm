﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- NAS - fix i parametr do pobierania testów online
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_check_version '1.6.y.3';
go

alter table bik_lisa_dict_column
alter column dictionary_name varchar(100) null;
go
    
alter table bik_lisa_dict_column
alter column name_in_db varchar(100) not null;
go
    
alter table bik_lisa_dict_column
alter column title_to_display varchar(100) not null;
go
    
alter table bik_lisa_dict_column
alter column data_type varchar(100) null;
go

if not exists(select 1 from bik_admin where param = 'dqc.checkTestActivity')
begin
	insert into bik_admin(param,value)
	values('dqc.checkTestActivity', '0')
end;

exec sp_update_version '1.6.y.3', '1.6.y.4';
go
