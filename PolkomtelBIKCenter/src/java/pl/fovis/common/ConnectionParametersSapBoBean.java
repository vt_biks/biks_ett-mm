/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import simplelib.BeanWithIntIdAndNameBase;

/**
 *
 * @author tflorczak
 */
public class ConnectionParametersSapBoBean extends BeanWithIntIdAndNameBase {

    public String sourceName;
    public String server;
    public String userName;
    public String password;
    public Integer reportTreeId;
    public Integer universeTreeId;
    public Integer connectionTreeId;
    public String reportTreeCode;
    public String universeTreeCode;
    public String connectionTreeCode;
//    public String path;
    public String openReportTemplate;
    public boolean withUsers;
    public boolean checkRights;
    public boolean isBo40;
    public boolean isDeleted;
    public boolean isActive;
    public Integer visualOrder;
}
