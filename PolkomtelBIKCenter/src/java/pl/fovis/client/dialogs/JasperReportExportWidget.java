/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class JasperReportExportWidget extends BaseActionOrCancelDialog {

    protected int nodeId;
    protected Map<String, String> reports;
    private IParametrizedContinuation<String> saveCont;
    protected List<RadioButton> rbs;
    protected ListBox lb;

    public void buildAndShowDialog(int nodeId, Map<String, String> reports, IParametrizedContinuation<String> saveCont) {
        this.nodeId = nodeId;
        this.reports = reports;
        super.buildAndShowDialog();
    }

    @Override
    protected String getDialogCaption() {
        return I18n.raporty.get();
    }

    @Override
    protected void doAction() {

        final String reportTag = lb.getSelectedValue();
        String name = lb.getSelectedItemText();
        name = BaseUtils.deAccentPolishChars(name);
        String extension = null;
        for (RadioButton r : rbs) {
            if (r.getValue()) {
                extension = r.getText();
            }
        }

        StringBuilder sb = new StringBuilder(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet());
        sb.append("?").append(BIKConstants.TREE_EXPORT_PARAM_NAME).append("=").append(BIKConstants.JASPER_EXPORT_FORMAT_HTML).append("&")
                .append(BIKConstants.TREE_EXPORT_ACTION_PARAM_NAME).append("=").append(BIKConstants.TREE_EXPORT_ACTION_CREATE_DOCUMENT).append("&")
                .append(BIKConstants.JASPER_EXPORT_NODE_ID).append("=").append(nodeId).append("&")
                .append(BIKConstants.JASPER_EXPORT_PARAM_NAME).append("=").append(reportTag).append("&")
                .append(BIKConstants.JASPER_EXPORT_EXTENSION).append("=").append(extension).append("&")
                .append(BIKConstants.JASPER_EXPORT_NAME_PARAM_NAME).append("=").append(name);
        Window.open(sb.toString(), "_blank", "");
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.pobierz.get();
    }

    @Override
    protected void buildMainWidgets() {
        HorizontalPanel hp = new HorizontalPanel();

        rbs = new ArrayList<RadioButton>();
        lb = new ListBox();
        for (Entry<String, String> e : reports.entrySet()) {
            lb.addItem(e.getKey(), e.getValue());
        }

        hp.add(lb);
        hp.add(createRb("pdf", true));
        hp.add(createRb("html", false));
        hp.add(createRb("xls", false));
        main.add(hp);
    }

    protected RadioButton createRb(String caption, boolean isSelected) {
        RadioButton rb = new RadioButton("a", caption);
        rb.setValue(isSelected);
        rbs.add(rb);
        return rb;

    }

}
