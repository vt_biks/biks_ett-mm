if not exists(select * from sys.objects where object_id = object_id(N'bik_system_user_group') and type in (N'U'))
    create table bik_system_user_group (
        id int not null primary key identity(1,1),
        name varchar(512) not null,
        domain varchar(512) null,
        is_built_in tinyint not null default(1),
        is_deleted tinyint not null default(0),
        unique (domain, name)        
    );
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_system_group_in_group') and type in (N'U'))
    create table bik_system_group_in_group (
        id int not null primary key identity(1,1),
        group_id int not null references bik_system_user_group(id),
        member_group_id int not null references bik_system_user_group(id),
        is_deleted tinyint not null default(0),
        unique (group_id, member_group_id),        
        unique (member_group_id, group_id)        
    );
go

-- drop table bik_system_user_in_group

if not exists(select * from sys.objects where object_id = object_id(N'bik_system_user_in_group') and type in (N'U'))
    create table bik_system_user_in_group (
        id int not null primary key identity(1,1),
        user_id int not null references bik_system_user(id),
        group_id int not null references bik_system_user_group(id),
        is_deleted tinyint not null default(0),
        unique (group_id, user_id),        
        unique (user_id, group_id)        
    );
go


if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_custom_right_role_user_entry') and name = 'group_id')
 alter table bik_custom_right_role_user_entry add group_id int null references bik_system_user_group (id);
go


alter table bik_custom_right_role_user_entry alter column user_id int null;
go


------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------



if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_system_user_group') and name = 'scc_root_id')
  alter table bik_system_user_group add scc_root_id int null references bik_system_user_group (id);
go


------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_calc_itscc_for_sugs]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_calc_itscc_for_sugs]
go

create procedure [dbo].[sp_calc_itscc_for_sugs] @diag_level int = 0 as
begin
  set nocount on;

  declare @start_time datetime = getdate();

  --declare @diag_level int = 0;

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': [sp_calc_itscc_for_sugs] start';

  declare @idx int = 1;

  create table #stack (i int not null identity primary key, w_id int null, v_id int not null, f int not null, unique (w_id, v_id, f));

  --insert into #stack (w_id, v_id, f) select null, node_id, 0 from itscc_graph_node;
  insert into #stack (w_id, v_id, f) select null, id, 0 from bik_system_user_group where is_deleted = 0;

  create table #node_state (node_id int not null primary key, state int not null, idx int not null, lowlink int not null);

  create index idx_node_state_idx on #node_state (idx, state);

  insert into #node_state (node_id, state, idx, lowlink) select id, 0, 0, 0 from bik_system_user_group where is_deleted = 0;

  declare @pass int = 0;

  while 1 = 1
  begin
    if @diag_level > 1
      print '
    ----------------------------- pass #' + cast(@pass as varchar(20)) + ' -----------------------------

    ';
    if @diag_level > 2
    begin
      select * from #stack order by i;
      select * from #node_state order by node_id;
    end;

    declare @w_id int, @v_id int, @f int;
    declare @v_state int, @v_idx int, @v_lowlink int;
    
    declare @max_i int;
    set @max_i = null;
    select top 1 @max_i = i, @w_id = w_id, @v_id = v_id, @f = f from #stack order by i desc;
    if @max_i is null break;
    
    delete from #stack where i = @max_i;
    
    select @v_state = state, @v_idx = idx, @v_lowlink = lowlink from #node_state where node_id = @v_id;

    if @diag_level > 1
    print '@w_id = ' + coalesce(@w_id, '<null>') + ', @v_id = ' + @v_id + ', @f = ' + cast(@f as varchar(20)) +
      ', @v_state = ' + cast(@v_state as varchar(20)) + ', @v_idx = ' + cast(@v_idx as varchar(20)) + ', @v_lowlink = ' + cast(@v_lowlink as varchar(20));

    /*if @diag_level > 0
    begin
      declare @real_stack_size int = (select count(*) from #stack);
      if @stack_size <> @real_stack_size begin
        print '@stack_size = ' + cast(@stack_size as varchar(20)) + ', @real_stack_size = ' + cast(@real_stack_size as varchar(20));
        break;
      end;
    end;*/

    if @f = 0
    begin
      if @v_state <> 0 -- = 2
      begin
        continue;
      end;
      
      insert into #stack (w_id, v_id, f) values (@w_id, @v_id, 2);
      
      insert into #stack (w_id, v_id, f) select @v_id, e.member_group_id, 0
      from bik_system_group_in_group e inner join #node_state s on e.member_group_id = s.node_id
      where e.group_id = @v_id and e.is_deleted = 0 and s.state = 0;

      declare @lowlink int;
      
      select @lowlink = min(s.idx)
      from bik_system_group_in_group e inner join #node_state s on e.member_group_id = s.node_id
      where e.group_id = @v_id and e.is_deleted = 0 and s.state = 1;

      if @lowlink is null set @lowlink = @idx;

      update #node_state set state = 1, idx = @idx, lowlink = @lowlink where node_id = @v_id;
      
      set @idx += 1;
            
    end else if @f = 2
    begin
      if @v_idx = @v_lowlink
        update #node_state set state = 2, lowlink = @v_idx where idx >= @v_idx and state = 1;
      
      if @w_id is not null
        update #node_state set lowlink = case when lowlink > @v_lowlink then @v_lowlink else lowlink end where node_id = @w_id;        
    end;

    set @pass += 1;
    
    --if @pass > 10 break;
    
    -- exec sp_calc_itscc_for_sugs;

    --if @pass > 5000 set @diag_level = 1;
  end;

  declare @final_stack_size int = (select count(*) from #stack);

  if @final_stack_size > 0 print 'stack is not empty!!! stack size: ' + cast(@final_stack_size as varchar(20));

  if @diag_level > 0 
  begin
    declare @end_time datetime = getdate();

    declare @node_cnt int = (select count(*) from bik_system_user_group where is_deleted = 0);
    declare @edge_cnt int = (select count(*) from bik_system_group_in_group where is_deleted = 0);

    declare @ms_taken int = datediff(ms, @start_time, @end_time);

    print cast(sysdatetime() as varchar(23)) + ': [sp_calc_itscc_for_sugs] finished after ' + cast(@pass as varchar(20)) + ' passes, time taken: ' 
    + cast(@ms_taken as varchar(30)) + ' ms, nodes: ' + cast(@node_cnt as varchar(20)) + ', edges: ' + cast(@edge_cnt as varchar(20)) +
    ', passes per (n+e) = ' + cast(cast(@pass as float) / (@node_cnt + @edge_cnt) as varchar(20)) + ', time per (n+e) = ' + 
    cast(cast(@ms_taken as float) / (@node_cnt + @edge_cnt) as varchar(20));
  end;
  
  --select * from #node_state;
  
  update sug
  set sug.scc_root_id = ps.node_id
  from bik_system_user_group sug      -- u�yty left join spowoduje przypisanie scc_root_id = null dla grup z is_deleted <> 0
  left join (#node_state ns inner join #node_state ps on ns.lowlink = ps.idx) on sug.id = ns.node_id;
end;
go


------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants]
go

create procedure [dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants] @diag_level int = 0 as
begin
  set nocount on
  
  --declare @diag_level int = 1
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: start!'

  if coalesce((select val from bik_app_prop where name = 'customRightRolesTreeSelector'), '') = ''
  begin
    if @diag_level > 0 print 'bik_app_prop[customRightRolesTreeSelector] is empty, nothing to do';
  
    return;
  end;
  
  -- 1. 
  -- zak�adamy, �e zosta�y zidentyfikowane silnie powi�zane komponenty (strongly connected components, scc)
  -- i poprawnie wyliczone jest bik_system_user_group.scc_root_id
  -- je�eli dana grupa nie tworzy cyklu z innymi, to jest ona jedynym elementem swojego scc, wi�c 
  -- bik_system_user_group.scc_root_id = bik_system_user_group.id
  
  select distinct scc_root_id as scc_id
  into #rcrrabg_scc
  from bik_system_user_group where is_deleted = 0;
  
  select distinct pg.scc_root_id as parent_scc_id, cg.scc_root_id as child_scc_id
  into #rcrrabg_rel
  from bik_system_group_in_group gig inner join bik_system_user_group pg on gig.group_id = pg.id
  inner join bik_system_user_group cg on gig.member_group_id = cg.id
  where gig.is_deleted = 0 and pg.is_deleted = 0 and cg.is_deleted = 0 and pg.scc_root_id <> cg.scc_root_id;
    
  truncate table bik_custom_right_role_action_branch_grant;

  -- specjalna akcja widoczno�ci w drzewie na zak�adkach - ShowInTree
  declare @show_in_tree_act_id int = (select id from bik_node_action where code = 'ShowInTree');

  declare @trees_for_roles_0 table (role_id int not null, tree_id int not null, unique (role_id, tree_id));

  insert into @trees_for_roles_0 (role_id, tree_id)
  select distinct crr.id as role_id, t.id as tree_id
  from
    bik_custom_right_role crr cross apply dbo.fn_split_by_sep(crr.tree_selector, ',', 7) x inner join bik_tree t on
      substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
      substring(x.str, 1, 1) <> '@' and t.code = x.str;  

  -- select * from bik_custom_right_role_user_entry
  
  if @diag_level > 1 select * from @trees_for_roles_0;
  
  declare @right_for_group_flat table (role_id int not null, group_id int not null, tree_id int null, node_id int null,
    unique(group_id, role_id, tree_id, node_id));

  declare @groups_to_process table (group_id int not null primary key);

  declare @processed_groups table (group_id int not null primary key);

  declare @unprocessed_groups table (group_id int not null primary key);
  
  insert into @unprocessed_groups (group_id) --select id from bik_system_user_group;
  select scc_id from #rcrrabg_scc;

  insert into @groups_to_process (group_id)
  /*select id
  from bik_system_user_group
  where id not in (select member_group_id from bik_system_group_in_group);*/
  select scc_id from #rcrrabg_scc where scc_id not in (select child_scc_id from #rcrrabg_rel);
  
  select scc_id, scc_id as ancestor_scc_id
  into #rcrrabg_ancestors_flat
  from #rcrrabg_scc;
  
  while exists (select 1 from @groups_to_process)
  begin
    if @diag_level > 0
    begin
      declare @gtpstr varchar(max) = null;
      
      select @gtpstr = case when @gtpstr is null then '' else @gtpstr + ',' end + cast(group_id as varchar(20))
      from @groups_to_process;
      
      print 'groups to process: ' + @gtpstr;    
    end;

    insert into #rcrrabg_ancestors_flat (scc_id, ancestor_scc_id)
    select distinct gtp.group_id, af.ancestor_scc_id
    from @groups_to_process gtp inner join #rcrrabg_rel r on gtp.group_id = r.child_scc_id
    inner join #rcrrabg_ancestors_flat af on r.parent_scc_id = af.scc_id;
    
    insert into @processed_groups (group_id) select group_id from @groups_to_process;

    delete from @unprocessed_groups where group_id in (select group_id from @groups_to_process);

    delete from @groups_to_process;
         
    insert into @groups_to_process (group_id)
    select group_id
    from @unprocessed_groups
    where group_id not in 
      --(select member_group_id from bik_system_group_in_group where group_id in (select group_id from @unprocessed_groups));
      (select child_scc_id from #rcrrabg_rel where parent_scc_id in (select group_id from @unprocessed_groups));
  end;

  if @diag_level > 0 and exists(select 1 from @unprocessed_groups)
  begin
    declare @ugstr varchar(max) = null;
    
    select @ugstr = case when @ugstr is null then '' else @ugstr + ',' end + cast(group_id as varchar(20))
    from @unprocessed_groups;
    
    print 'nothing new to process, but there are unprocessed groups: ' + @ugstr;    
  end;

  -- poni�sze nie wy�apuje sytuacji gdy istnieje wpis nadrz�dny i podrz�dny 
  -- (w sensie w�z��w w tym samym drzewie, dla tej samej roli) - wrzucane s� oba wpisy
  insert into @right_for_group_flat (role_id, group_id, tree_id, node_id)
  select distinct crrue.role_id, cg.id, crrue.tree_id, crrue.node_id
  from #rcrrabg_ancestors_flat af inner join bik_system_user_group ag on af.ancestor_scc_id = ag.scc_root_id
  inner join bik_custom_right_role_user_entry crrue on crrue.group_id = ag.id
  inner join bik_system_user_group cg on af.scc_id = cg.scc_root_id
  where ag.is_deleted = 0;
    
  declare @right_for_user_flat table (role_id int not null, user_id int not null, tree_id int null, node_id int null,
    unique(user_id, role_id, tree_id, node_id));

  insert into @right_for_user_flat (user_id, role_id, tree_id, node_id)
  select distinct user_id, role_id, tree_id, node_id
  from
    (select uig.user_id, rfgf.role_id, rfgf.tree_id, rfgf.node_id
     from bik_system_user_in_group uig inner join @right_for_group_flat rfgf on uig.group_id = rfgf.group_id
     union
     select user_id, role_id, tree_id, node_id
     from bik_custom_right_role_user_entry crrue where user_id is not null) x;

  if @diag_level > 1 select * from bik_custom_right_role_user_entry;

  if @diag_level > 1 select *
  from --bik_custom_right_role_user_entry 
    @right_for_user_flat crrue left join
    @trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id;      

  insert into bik_custom_right_role_action_branch_grant (action_id, user_id, tree_id, branch_ids)
  select distinct @show_in_tree_act_id, crrue.user_id, coalesce(crrue.tree_id, rts.tree_id) as tree_id, --crrue.node_id,
    coalesce(n.branch_ids, '') as branch_ids
  from --bik_custom_right_role_user_entry 
    @right_for_user_flat crrue left join
    @trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id
  left join bik_node n on n.id = crrue.node_id
  where
    not exists -- nie dorzucamy wpisu, je�eli wyst�puje dla niego wpis nadrz�dny (na wy�szym poziomie w drzewie)
    (
      select --
        1
        --crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
        from --bik_custom_right_role_user_entry 
          @right_for_user_flat crrue2 left join
          @trees_for_roles_0 rts2 on crrue2.tree_id is null and rts2.role_id = crrue2.role_id
      left join bik_node n2 on n2.id = crrue2.node_id
      where crrue2.user_id = crrue.user_id and coalesce(n.branch_ids, '') like coalesce(n2.branch_ids, '') + '_%'
        and coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
        --and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
    );

  insert into bik_custom_right_role_action_branch_grant (action_id, user_id, tree_id, branch_ids)
  select distinct naicrr.action_id, crrue.user_id, coalesce(crrue.tree_id, rts.tree_id) as tree_id, --crrue.node_id,
    coalesce(n.branch_ids, '') as branch_ids
  from --bik_custom_right_role_user_entry 
    @right_for_user_flat crrue inner join bik_node_action_in_custom_right_role naicrr on crrue.role_id = naicrr.role_id left join
    @trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id
  left join bik_node n on n.id = crrue.node_id
  where
    naicrr.action_id <> @show_in_tree_act_id and
    not exists -- nie dorzucamy wpisu, je�eli wyst�puje dla niego wpis nadrz�dny (na wy�szym poziomie w drzewie)
    (
      select --
        1
        --crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
        from --bik_custom_right_role_user_entry 
          @right_for_user_flat crrue2 inner join bik_node_action_in_custom_right_role naicrr2 on crrue2.role_id = naicrr2.role_id left join
          @trees_for_roles_0 rts2 on crrue2.tree_id is null and rts2.role_id = crrue2.role_id
      left join bik_node n2 on n2.id = crrue2.node_id
      where naicrr2.action_id = naicrr.action_id and crrue2.user_id = crrue.user_id and coalesce(n.branch_ids, '') like coalesce(n2.branch_ids, '') + '_%'
        and coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
        --and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
    );
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: end'
end;
go

