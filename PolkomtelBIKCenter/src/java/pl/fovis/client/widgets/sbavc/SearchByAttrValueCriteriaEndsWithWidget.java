/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.widgets.sbavc;

import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import pl.fovis.common.sbavc.SearchByAttrValueCriteriaEndsWith;

/**
 *
 * @author tflorczak
 */
public class SearchByAttrValueCriteriaEndsWithWidget extends SearchByAttrValueCriteriaWidgetBase {

    protected TextBox endsWithTbx = new TextBox();

    @Override
    protected void createOptWidget() {
        row.setWidget(0, 4, endsWithTbx);
    }

    @Override
    public ISearchByAttrValueCriteria provideBean() {
        return (isValidInput() ? new SearchByAttrValueCriteriaEndsWith(getSelectedAttrId(), getSelectedAttrCaption(), endsWithTbx.getText(), negationChbx.getValue()) : null);
    }
}
