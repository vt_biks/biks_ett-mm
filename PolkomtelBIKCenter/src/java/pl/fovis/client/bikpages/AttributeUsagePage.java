/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.ResizeSensitiveWidgetHelper;
import pl.fovis.client.treeandlist.TreeNodeBeanAsMapStorage;
import pl.fovis.common.AttributeValueInNodeBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.HorizontalSplitPanelWrapper;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author ctran
 */
public class AttributeUsagePage extends BikPageMinimalBase {

    protected IPageDataFetchBroker dataFetchBroker = new PageDataFetchBroker();
    protected BikTreeWidget bikTree;
    protected FlowPanel rightPanel;
    protected FlexTable table = new FlexTable();
    protected HorizontalSplitPanelWrapper main;

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
    }

    @Override
    public boolean isPageEnabled() {
        return BIKClientSingletons.isEnableAttributeUsagePage() && super.isPageEnabled(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Widget buildWidgets() {
        main = new HorizontalSplitPanelWrapper();
        main.setWidth("100%");
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(main, BIKClientSingletons.MY_FUCKING_IE7_HACK_HEIGHT);
        main.setSplitPosition("400px");
        buildLeftPanel();
        buildRightPanel();
        VerticalPanel mainVp = new VerticalPanel();
        mainVp.setSpacing(6);
        table.setCellSpacing(0);
        fetchData();
        return main.asWidget();
    }

    @Override
    public String getId() {
        return "admin:dict:attrUsage";
    }

    private IsWidget createReloadBtn() {
        PushButton refreshbtn = new PushButton(I18n.zaladuj.get());
        NewLookUtils.makeCustomPushButton(refreshbtn);
        refreshbtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().calculateAttributeUsages(new StandardAsyncCallback<Void>("Error in calculateAttributeUsages") {

                    @Override
                    public void onSuccess(Void result) {
                        fetchData();
                        BIKClientSingletons.showInfo(I18n.zaimportowanoDaneDoDrzewa.get());
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        super.onFailure(caught);
                    }
                });
            }
        });

        return refreshbtn;
    }

    protected void fetchData() {
        BIKClientSingletons.getService().getAttributeUsageTreeNodes(new TreeDataFetchAsyncCallback<List<TreeNodeBean>>(dataFetchBroker) {
            @Override
            protected void doOnSuccess(List<TreeNodeBean> result) {
//                final SimpleProgressInfoDialog infoDialog = new SimpleProgressInfoDialog();
//                infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), "Blalal", true);
                bikTree.initWithFullData(result, true, null, null);
                bikTree.selectEntityById(null);
//                infoDialog.hideDialog();
            }

            @Override
            protected void doOnFailure(Throwable caught) {
                super.doOnFailure(caught); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }

    private void refreshRightPanel() {
        BIKClientSingletons.getService().getAttributeUsingNodes(bikTree.getCurrentNodeBean(), new StandardAsyncCallback<List<AttributeValueInNodeBean>>() {

            @Override
            public void onSuccess(List<AttributeValueInNodeBean> result) {
                for (int i = table.getRowCount() - 1; i >= 0; i--) {
                    table.removeRow(i);
                }
                int row = 0;
                for (int i = 0; i < result.size(); i++) {
                    final AttributeValueInNodeBean bean = result.get(i);

                    HorizontalPanel nameFp = new HorizontalPanel();
                    nameFp.add(BIKClientSingletons.createIconForNodeKind(bean.nodeKindCode, bean.treeCode, null));
                    HTML html = new HTML(bean.nodeName);
                    html.setTitle(BIKClientSingletons.getMenuPathForTreeCode(bean.treeCode) + bean.nodeName);
//                    html.addClickHandler(new ClickHandler() {
//
//                        @Override
//                        public void onClick(ClickEvent event) {
//                            BIKClientSingletons.getTabSelector().submitNewTabSelection(bean.treeCode, bean.nodeId, true);
//                        }
//                    });
//                    html.setStyleName("btnPointer");
                    nameFp.add(html);
                    table.setWidget(row, 0, nameFp);

                    HorizontalPanel hp = new HorizontalPanel();
                    hp.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
                    hp.setWidth("100%");
                    HorizontalPanel wrapper = new HorizontalPanel();
                    wrapper.add(BIKClientSingletons.createLookupButton(bean.treeCode, bean.nodeId, null, true));
                    wrapper.add(BIKClientSingletons.createFollowButton(bean.treeCode, bean.nodeId, true));
                    hp.add(wrapper);
                    table.setWidget(row++, 2, hp);
                    table.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
                }
            }
        });
    }

    private void buildLeftPanel() {
        bikTree = new BikTreeWidget(true,
                dataFetchBroker,
                true,
                TreeNodeBeanAsMapStorage.NAME_FIELD_NAME + "=300:Nazwa");
        final Widget treeWidget = bikTree.getTreeGridWidget();
        bikTree.addSelectionChangedHandler(new IParametrizedContinuation<Map<String, Object>>() {

            @Override
            public void doIt(Map<String, Object> param) {
                refreshRightPanel();
            }
        });
        final VerticalPanel vPanel = new VerticalPanel();
        vPanel.setStyleName("leftMainPanelTree");
        // vPanel.setHeight("100%");
        vPanel.setWidth("100%");
        final HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
        hPanel.add(createReloadBtn());
        treeWidget.setSize("100%", "100%");
        vPanel.add(treeWidget);
        BIKClientSingletons.registerResizeSensitiveWidget(new ResizeSensitiveWidgetHelper(vPanel) {
            @Override
            public void setFixedHeight(int height) {
                //height -= 4; // uwzględniam padding .treeBikFilterBox bik.css linia 1420
                vPanel.setCellHeight(treeWidget, height + "px");
                // Window.alert("wys: " + height);
            }
        }, BIKClientSingletons.ORIGINAL_TREE_GRID_PANEL_HEIGHT);

        vPanel.setCellWidth(treeWidget, "100%");

        hPanel.setStyleName("treeBikFilterBox");
        hPanel.setWidth("100%");
        vPanel.add(hPanel);
        vPanel.setCellHeight(hPanel, "32px");
        vPanel.setCellWidth(hPanel, "100%");
        main.add(vPanel);
    }

    private void buildRightPanel() {
        GWTUtils.setStyleAttribute(table, "padding" /* I18N: no */, "0px 0 0px 0");
        table.setStyleName("EntityDetailsPane");
        table.setWidth("100%");
        main.add(table);
    }
}
