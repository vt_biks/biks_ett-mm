/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static simplelib.SimpleDateUtils.getDay;
import static simplelib.SimpleDateUtils.getHours;
import static simplelib.SimpleDateUtils.getMinutes;
import static simplelib.SimpleDateUtils.getMonth;
import static simplelib.SimpleDateUtils.getSeconds;
import static simplelib.SimpleDateUtils.getYear;
import static simplelib.SimpleDateUtils.newDate;

/**
 *
 * @author wezyr
 */
public class SimpleDateUtilsTest {

    public SimpleDateUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getMillis method, of class SimpleDateUtils.
     */
    @Test
    public void testGetMillis() {
        System.out.println("getMillis");
        Date d = new Date();
        Date d2 = newDate(getYear(d), getMonth(d), getDay(d),
                getHours(d), getMinutes(d), getSeconds(d));
        long expResult = d.getTime() - d2.getTime();
        System.out.println("d.getTime=" + d.getTime());
        System.out.println("d2.getTime=" + d2.getTime());
        int result = SimpleDateUtils.getMillis(d);
        assertEquals(expResult, result);
    }
    private Map<String, Date> testDates;
    private Date endTestDate = new Date();

    private void addTestDate(String label, int secsAgo) {
        testDates.put(label, SimpleDateUtils.addMillis(endTestDate, -1000L * secsAgo));
    }

    @Test
    public void testGetShortElapsedTimeStr() {
        testDates = new LinkedHashMap<String, Date>();
        addTestDate("zeroSecsAgo", 0);
        addTestDate("5secsAgo", 5);
        addTestDate("1min0secsAgo", 60);
        addTestDate("1min20secsAgo", 80);
        addTestDate("1h0minAgo", 3600);
        addTestDate("1h0min5sAgo", 3605);
        addTestDate("1h3min5sAgo", 3605 + 180);
        addTestDate("1h3min0sAgo", 3605 + 180);
        addTestDate("1dAgo", 3600 * 24);
        addTestDate("1d5sAgo", 3600 * 24 + 5);
        addTestDate("1d3mAgo", 3600 * 24 + 180);
        addTestDate("1d3m7sAgo", 3600 * 24 + 187);
        addTestDate("1d2hAgo", 3600 * 26);
        addTestDate("1d2h3sAgo", 3600 * 26 + 3);
        addTestDate("1d2h7mAgo", 3600 * 26 + 7 * 60);
        addTestDate("1d2h7m9sAgo", 3600 * 26 + 7 * 60 + 9);
        addTestDate("10d2h7m9sAgo", 10 * 3600 * 24 + 3600 * 2 + 7 * 60 + 9);
        addTestDate("100d2h7m9sAgo", 10 * 3600 * 24 + 3600 * 2 + 7 * 60 + 9);
        addTestDate("1001d2h7m9sAgo", 1001 * 3600 * 24 + 3600 * 2 + 7 * 60 + 9);

        testWithCompactFactor(0);
        testWithCompactFactor(1);
        testWithCompactFactor(2);
    }

    private void testWithCompactFactor(int compactFactor) {
        System.out.println("with compact factor=" + compactFactor);
        for (Entry<String, Date> e : testDates.entrySet()) {
            System.out.println("[" + e.getKey() + "]: "
                    + SimpleDateUtils.getShortElapsedTimeStr(e.getValue(), endTestDate, "d", "h", "m", "s", compactFactor));
        }
    }

    @Test
    public void testGetElapsedSecondsComponentByPartNo() {
        assertEquals(100, SimpleDateUtils.getElapsedSecondsComponentByPartNo(3600 * 24 * 100, 3));
        assertEquals(1000, SimpleDateUtils.getElapsedSecondsComponentByPartNo(3600 * 24 * 1000, 3));
    }

    @Test
    public void testCanonicalNoSec() {
        System.out.println("now noSec=" + SimpleDateUtils.toCanonicalStringNoSec(new Date()));
    }

    @Test
    public void testTruncateDate() {
        System.out.println(SimpleDateUtils.truncateDay(new Date()));
    }
}
