/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.LinkedAlsoBean;
import pl.fovis.common.SapBoConnectionExtraDataBean;
import pl.fovis.common.SapBoExtraDataBean;
import pl.fovis.common.SapBoOLAPConnectionExtraDataBean;
import pl.fovis.common.SapBoQueryExtraDataBean;
import pl.fovis.common.SapBoUniverseExtraDataBean;
import pl.fovis.common.SapBoUniverseTableBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.SimpleDateUtils;
import simplelib.XMLNodeLameExtractor;

/**
 *
 * @author wezyr
 */
public class SAPBOExtradataWidget extends EntityDetailsWidgetFlatBase {

    protected BikCustomButton btnShowSQL = null;
    protected BikCustomButton btnChangeDatabaseName = null;
    protected BikCustomButton btnChangeServerForOracle = null;
    protected HorizontalPanel hpSQLPanel;
    protected FlowPanel fp;
    protected DisclosurePanel disc;
    protected IMainHeaderSharedTable imhst;
    protected int r = 0;
    protected FlexTable ft = new FlexTable();
    protected boolean isSomethingToAdd = false;

    public SAPBOExtradataWidget(IMainHeaderSharedTable imhst) {
        this.imhst = imhst;
    }

    public void addToFtIdCuidAndDisclosurePanel() {

        SapBoExtraDataBean sapBoExtraData = getData().sapBoExtraData;
        SapBoExtraDataBean sapBoFolderExtraData = getData().sapBoFolderExtraData;

        if (sapBoExtraData != null || sapBoFolderExtraData != null) {
            r = 0;
            String cuid;
            disc = new DisclosurePanel(I18n.dodatkoweID.get() /* I18N:  */);
            disc.setAnimationEnabled(true);
            disc.setStyleName("gwt-DisclosurePanel_header");
            // dla rapotów i folderow
            cuid = sapBoExtraData != null ? getData().node.objId + ", " + (sapBoExtraData.cuid != null ? sapBoExtraData.cuid : "-")
                    : getData().node.objId + ", " + (sapBoFolderExtraData.cuid != null ? sapBoFolderExtraData.cuid : "-");
            ft.clear();
            isSomethingToAdd = false;
            addTranslatedRow("Guid" /* I18N: no */, sapBoExtraData != null ? sapBoExtraData.guid : sapBoFolderExtraData.guid);
            addTranslatedRow("Ruid" /* i18n: no */, sapBoExtraData != null ? sapBoExtraData.ruid : sapBoFolderExtraData.ruid);
            addTranslatedRow("Progid" /* I18N: no */, sapBoExtraData != null ? sapBoExtraData.progid : sapBoFolderExtraData.progid);
            disc.setContent(ft);
            imhst.addTranslatedRow("ID, CUID" /* I18N: no */, cuid);
            if (isSomethingToAdd) {
                imhst.addTranslatedRow(null, null, disc);
            }
        }
    }

    public void displayData() {
        SapBoConnectionExtraDataBean sapBoConnectionExtraData = getData().sapBoConnectionExtraData;
        SapBoUniverseExtraDataBean sapBoUniverseExtraData = getData().sapBoUniverseExtraData;
        SapBoOLAPConnectionExtraDataBean boOLAPConnectionExtraDataBean = getData().sapBoOLAPConnectionExtraData;
        List<LinkedAlsoBean> otherBOObjects = getData().otherBOTheSameObject;
        if (otherBOObjects != null && !otherBOObjects.isEmpty()) {
            HorizontalPanel hp = new HorizontalPanel();
            boolean isFirst = true;
            for (final LinkedAlsoBean linkedObject : otherBOObjects) {
                if (!isFirst) {
                    hp.add(new HTML(",&nbsp"));
                } else {
                    isFirst = !isFirst;
                }
                hp.add(BIKClientSingletons.createLinkAsAnchor(linkedObject.caption, linkedObject.treeCode, linkedObject.linkedNodeId));
            }
            imhst.addTranslatedRow("Występowanie w innych instancjach" /* i18n: no */, null, hp);
        }
        if (sapBoConnectionExtraData != null) {
            imhst.addTranslatedRow("Server" /* I18N: no */, sapBoConnectionExtraData.server);
            imhst.addTranslatedRow("User" /* I18N: no */, sapBoConnectionExtraData.userName);
            imhst.addTranslatedRow("Password" /* I18N: no */, sapBoConnectionExtraData.password);
            imhst.addTranslatedRow("Source" /* I18N: no */, sapBoConnectionExtraData.databaseSource);
            imhst.addTranslatedRow("Network layer" /* I18N: no */, sapBoConnectionExtraData.connectionNetworkLayer);
            imhst.addTranslatedRow("Type" /* I18N: no */, sapBoConnectionExtraData.type);
            imhst.addTranslatedRow("Client number" /* I18N: no */, sapBoConnectionExtraData.clientNumber);
            imhst.addTranslatedRow("Database engine" /* I18N: no */, sapBoConnectionExtraData.databaseEngine);
            imhst.addTranslatedRow("Language" /* I18N: no */, sapBoConnectionExtraData.language);
            imhst.addTranslatedRow("System number" /* I18N: no */, sapBoConnectionExtraData.systemNumber);
            imhst.addTranslatedRow("System id" /* I18N: no */, sapBoConnectionExtraData.systemId);
            imhst.addTranslatedRow("ID, CUID" /* I18N: no */, sapBoConnectionExtraData.cuid != null ? getData().node.objId + ", " + sapBoConnectionExtraData.cuid : "");  ///??????????????????
            imhst.addTranslatedRow("Utworzono" /* I18N: no */, SimpleDateUtils.toCanonicalString(sapBoConnectionExtraData.creationTime));
            imhst.addTranslatedRow("Ostatnia modyfikacja" /* I18N: no */, SimpleDateUtils.toCanonicalString(sapBoConnectionExtraData.updateTime));
            // sprawdzanie czy moduł pompki z ms sql jest dostępny
            if (sapBoConnectionExtraData.databaseEngine != null && sapBoConnectionExtraData.databaseEngine.contains(PumpConstants.SOURCE_NAME_MSSQL) && BIKClientSingletons.isSysAdminLoggedInEx(BIKConstants.ACTION_PUMP_MSSQL) && BIKClientSingletons.isConnectorAvailable(PumpConstants.SOURCE_NAME_MSSQL)) {
                imhst.addTranslatedRow("Default schema name" /* I18N: no */, sapBoConnectionExtraData.defaultSchemaName);
                imhst.addTranslatedRow("Database name" /* I18N: no */, sapBoConnectionExtraData.databaseName);
                imhst.addTranslatedRow(null, null, btnChangeDatabaseName);
            }
            if (sapBoConnectionExtraData.databaseEngine != null && sapBoConnectionExtraData.databaseEngine.contains(PumpConstants.SOURCE_NAME_ORACLE) && BIKClientSingletons.isSysAdminLoggedInEx(BIKConstants.ACTION_PUMP_ORACLE) && BIKClientSingletons.isConnectorAvailable(PumpConstants.SOURCE_NAME_ORACLE)) {
                imhst.addTranslatedRow("Oracle server" /* I18N: no */, sapBoConnectionExtraData.serverNameForOracle);
                imhst.addTranslatedRow(null, null, btnChangeServerForOracle);
            }
        }
        if (boOLAPConnectionExtraDataBean != null) {
//            imhst.addTranslatedRow("ID, CUID" /* I18N: no */, boOLAPConnectionExtraDataBean.cuid != null ? getData().node.objId + ", " + boOLAPConnectionExtraDataBean.cuid : "");
            // to samo co database engine
//            imhst.addTranslatedRow("Dostawca" /* I18N: no */, boOLAPConnectionExtraDataBean.providerCaption);
            imhst.addTranslatedRow("Kostka" /* I18N: no */, boOLAPConnectionExtraDataBean.cubeCaption);
//            imhst.addTranslatedRow("Utworzono" /* I18N: no */, SimpleDateUtils.toCanonicalString(boOLAPConnectionExtraDataBean.created));
//            imhst.addTranslatedRow("Ostatnia modyfikacja" /* I18N: no */, SimpleDateUtils.toCanonicalString(boOLAPConnectionExtraDataBean.modified));
            imhst.addTranslatedRow("Właściciel w repozytorium BO" /* I18N: no */, boOLAPConnectionExtraDataBean.owner);
        }
        addToFtIdCuidAndDisclosurePanel();
        if (sapBoUniverseExtraData != null) {
            imhst.addTranslatedRow("Typ" /* I18N: no */, getType(sapBoUniverseExtraData.type));
            imhst.addTranslatedRow("Select" /* I18N: no */, sapBoUniverseExtraData.textOfSelect);
            imhst.addTranslatedRow("Where" /* I18N: no */, sapBoUniverseExtraData.textOfWhere);
            imhst.addTranslatedRow("Agregate" /* I18N: no */, sapBoUniverseExtraData.agregate != null ? getAgregateFunction(sapBoUniverseExtraData.agregate) : "");
        }
        SapBoQueryExtraDataBean sapBoQueryDataBean = getData().sapBoQueryDataBean;
        if (sapBoQueryDataBean != null) {
            imhst.addTranslatedRow("Id" /* I18N: no */, sapBoQueryDataBean.idText);
            imhst.addTranslatedRow("Filtr" /* I18N: no */, sapBoQueryDataBean.filtrText);
            if (sapBoQueryDataBean.sqlText != null) {
                imhst.addTranslatedRow(null, null, hpSQLPanel);
            }
        }
        SapBoUniverseTableBean sapBoUniverseTableBean = getData().tables;
        if (sapBoUniverseTableBean != null) {
            imhst.addTranslatedRow(null, null, new HTML(sapBoUniverseTableBean.type));
            if (sapBoUniverseTableBean.fhsql != null) {
                imhst.addTranslatedRow(null, null, hpSQLPanel);
            }
        }
        SapBoExtraDataBean sapBoFolderExtraData = getData().sapBoFolderExtraData;
        SapBoExtraDataBean sapBoExtraData = getData().sapBoExtraData;
        if (sapBoFolderExtraData != null) {
            imhst.addTranslatedRow("Liczba obiektów podrzędnych (children)" /* I18N: no */, sapBoFolderExtraData.children);
            imhst.addTranslatedRow("Utworzony" /* I18N: no */, SimpleDateUtils.toCanonicalString(sapBoFolderExtraData.created));
            imhst.addTranslatedRow("Zmodyfikowany" /* I18N: no */, SimpleDateUtils.toCanonicalString(sapBoFolderExtraData.modified));
        }
        if (sapBoExtraData != null) {
            imhst.addTranslatedRow("Nazwa pliku" /* I18N: no */, sapBoExtraData.filePath != null && sapBoExtraData.fileName != null ? (sapBoExtraData.filePath + sapBoExtraData.fileName) : null);
            imhst.addTranslatedRow("Data ostatniego uruchomienia" /* I18N: no */, SimpleDateUtils.toCanonicalString(sapBoExtraData.lastRunTime));
            imhst.addTranslatedRow("Słowa kluczowe z repozytorium BO" /* I18N: no */, sapBoExtraData.keyword);
            imhst.addTranslatedRow("Rozmiar w bajtach" /* I18N: no */, sapBoExtraData.filesValue);
            imhst.addTranslatedRow("Size" /* I18N: no */, sapBoExtraData.size);
            imhst.addTranslatedRow("HasChildren", sapBoExtraData.hasChildren);
            imhst.addTranslatedRow("Instance" /* I18N: no */, sapBoExtraData.instance);
            imhst.addTranslatedRow("Owner_id" /* I18N: no */, sapBoExtraData.owner_id);
            imhst.addTranslatedRow("Obtype" /* I18N: no */, sapBoExtraData.obtype);
            imhst.addTranslatedRow("Flags" /* I18N: no */, sapBoExtraData.flags);
            imhst.addTranslatedRow("RunnableObject" /* I18N: no */, sapBoExtraData.runnableObject);
            imhst.addTranslatedRow("ContentLocale" /* I18N: no */, sapBoExtraData.contentLocale);
            imhst.addTranslatedRow("IsSchedulable" /* I18N: no */, sapBoExtraData.isSchedulable);
            parseXMLAndAddRow(sapBoExtraData);
            imhst.addTranslatedRow("ReadOnly" /* I18N: no */, sapBoExtraData.readOnly);
            imhst.addTranslatedRow("LastSuccessfulInstanceId" /* I18N: no */, sapBoExtraData.lastSuccessfulInstanceId);
            imhst.addTranslatedRow("Timestamp" /* I18N: no */, SimpleDateUtils.toCanonicalString(sapBoExtraData.timestamp));
            imhst.addTranslatedRow("ProgidMachine" /* I18N: no */, sapBoExtraData.progidMachine);
            imhst.addTranslatedRow("Endtime" /* I18N: no */, SimpleDateUtils.toCanonicalString(sapBoExtraData.endtime));
            imhst.addTranslatedRow("Starttime" /* I18N: no */, SimpleDateUtils.toCanonicalString(sapBoExtraData.starttime));
            imhst.addTranslatedRow("ScheduleStatus" /* I18N: no */, sapBoExtraData.scheduleStatus);
            imhst.addTranslatedRow("Recurring" /* I18N: no */, sapBoExtraData.recurring);
            imhst.addTranslatedRow("Nextruntime" /* I18N: no */, SimpleDateUtils.toCanonicalString(sapBoExtraData.nextruntime));
            imhst.addTranslatedRow("Doc Sender" /* I18N: no */, sapBoExtraData.docSender);
            imhst.addTranslatedRow("Poprawka" /* I18N: no */, sapBoExtraData.revisionnum);
            imhst.addTranslatedRow("Application Object" /* I18N: no */, sapBoExtraData.applicationObject);
            imhst.addTranslatedRow("Nazwa skrócona" /* I18N: no */, sapBoExtraData.shortname);
            imhst.addTranslatedRow("Statystyki" /* I18N: no */, sapBoExtraData.statistic);
            imhst.addTranslatedRow("Opublikowano w repozytorium BO" /* I18N: no */, SimpleDateUtils.toCanonicalString(sapBoExtraData.created));
            imhst.addTranslatedRow("Właściciel w repozytorium BO" /* I18N: no */, sapBoExtraData.owner);
            imhst.addTranslatedRow("Autor [Created by]" /* I18N: no */, sapBoExtraData.author);
            imhst.addTranslatedRow("Ostatnia modyfikacja w repozytorium BO" /* I18N: no */, SimpleDateUtils.toCanonicalString(sapBoExtraData.modified));
        }
    }

    protected void parseXMLAndAddRow(SapBoExtraDataBean sapBoExtraData) {
        if (sapBoExtraData.webiDocProperties != null) {
            List<Map<String, String>> extractXmlNodes = XMLNodeLameExtractor.extractXmlNodes(sapBoExtraData.webiDocProperties, BaseUtils.paramsAsSet("webi_prop", "webi_prompt"), true, true);
            StringBuilder prompty = new StringBuilder();
            int i = 0;
            for (Map<String, String> map : extractXmlNodes) {
                if (map.containsValue("is_refresh_on_open")) {
                    imhst.addTranslatedRow("Sposób odświeżania raportu" /* I18N: no */, (map.get("value" /* I18N: no */).equals("false" /* I18N: no */) ? I18n.raportOfflineRaportJestOdswiezaZ.get() /* I18N:  */ : I18n.raportOnlineOdswiezaWMomencieJeg.get() /* I18N:  */));
                }
                if (map.containsKey("question" /* i18n: no */)) {
                    prompty.append("\"").append(map.get("question" /* i18n: no */)).append("\"").append(", ");
                    i++;
                }
            }
            if (i > 0) {
                imhst.addTranslatedRow("Parametry wprowadzane przez użytkownika w momencie odświeżania raportu" /* I18N: no */, prompty.substring(0, prompty.length() - 2));
            }
        }
    }

    @Override
    public boolean hasContent() {
        return (getData().sapBoExtraData != null) || (getData().sapBoConnectionExtraData != null) || (getData().sapBoUniverseExtraData != null) || (getData().sapBoQueryDataBean != null) || (getData().sapBoFolderExtraData != null) || (getData().tables != null);
    }

    @Override
    public Widget buildWidgets() {
        fp = new FlowPanel();
        hpSQLPanel = new HorizontalPanel();
        btnShowSQL = new BikCustomButton(I18n.pokazSQL.get() /* I18N:  */, "lookupTrashBtn",
                new IContinuation() {

                    public void doIt() {
                        if (getData().sapBoQueryDataBean != null) {
                            getForms().viewSQLDef(getData().node.name, (getData().sapBoQueryDataBean.sqlText != null ? getData().sapBoQueryDataBean.sqlText : "-"));
                        }
                        if (getData().tables != null) {
                            getForms().viewSQLDef(getData().node.name, (getData().tables.fhsql != null ? getData().tables.fhsql : "-"));
                        }
                    }
                });
        btnChangeDatabaseName = new BikCustomButton(I18n.zmienBazeDanych.get() /* I18N:  */, "lookupTrashBtn", new IContinuation() {

                    public void doIt() {
                        if (getData().sapBoConnectionExtraData.databaseEngine != null && getData().sapBoConnectionExtraData.databaseEngine.indexOf("MS SQL" /* I18N: no */) != -1) {
                            getForms().viewSetConnectionDatabaseName(getData().node.id);
                        }
                    }
                });
        btnChangeServerForOracle = new BikCustomButton(I18n.zmienSerwer.get() /* I18N:  */, "lookupTrashBtn", new IContinuation() {

                    public void doIt() {
                        if (getData().sapBoConnectionExtraData.databaseEngine != null && getData().sapBoConnectionExtraData.databaseEngine.indexOf("Oracle" /* I18N: no */) != -1) {
                            getForms().viewSetConnectionServerName(getData().node.id);
                        }
                    }
                });
        hpSQLPanel.add(btnShowSQL);
        return fp;
    }

    public String getType(String text) {
        String newString = text.replace("ds" /* i18n: no */, "");
        newString = newString.replace("Object" /* i18n: no */, "");

        return newString;
    }

    public String getAgregateFunction(String text) {
        String newString = text.replace("dsAggregateBy", "");
        newString = newString.replace("Object" /* i18n: no */, "").replace("Null" /* I18N: no */, "");

        return newString;
    }

    public boolean isEnabledDeprecated(String code, int count) {
        return true;
    }

    public void addTranslatedRow(String nameId, String value) {
        String nameToDisplay = BIKClientSingletons.getTranslatedAttributeNameOrNullForHidden(getData().node.nodeKindId, nameId);
        if (nameToDisplay != null) {
            if (!BaseUtils.isStrEmpty(value)) {
                Label labela = new Label(value);
                ft.setWidget(r, 0, createHintedHTML(nameToDisplay, null));
                ft.setWidget(r, 1, labela);
                labela.setStyleName("gp-html");
                r++;
                isSomethingToAdd = true;
            }
        }
    }
}
