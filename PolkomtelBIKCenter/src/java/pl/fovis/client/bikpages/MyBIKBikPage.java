/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.bikwidgets.ApplyForExtendedAccessFormPanel;
import pl.fovis.client.dialogs.ResetDatabaseDialog;
import pl.fovis.client.mltx.InviteUsersDialog;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.DisabledUserApplyForAccountMsgsBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author beata
 */
public class MyBIKBikPage extends BikPageMinimalBase implements IMyBIKSPage/*implements IBikPage<Integer>*/ {

    protected HorizontalPanel mainHp;
    protected VerticalPanel leftVp;
    protected VerticalPanel rightVp;
    protected VerticalPanel rightDownVp;
    protected VerticalPanel leftBtnVp;
//    protected Image avatarImage;
    protected HTML mustLoginHeaderLbl;
    private HTML loremHtml;
    private PushButton invite;
    private HTML caption;
    //ww->mybiks: co to ma niby być? kto zgadnie albo się połapie w kodzie?
//    private VerticalPanel mbv;
    private HTML tutorial;
    protected MyBIKSFavouriteTab favouriteTab = new MyBIKSFavouriteTab();
    protected MyBIKSHomePageTab homePageTab = new MyBIKSHomePageTab();
    protected MyBIKSMyObjects myObjects = new MyBIKSMyObjects();
    protected MyBIKSNewsTab newsTab = new MyBIKSNewsTab();
    protected boolean isDemoVersion = BIKClientSingletons.isDemoVersion();
    protected HorizontalPanel welcomeMsg;
    protected HorizontalPanel captionAndHelpFp;
//    protected String lorem =
//            isDemoVersion ? BIKClientSingletons.getMyBIKSDemoMsg()
//            : BIKClientSingletons.getMyBIKSGeneralMsg();
    protected Map<IMyBIKSTab, BikCustomButton> tabsBtn = new LinkedHashMap<IMyBIKSTab, BikCustomButton>();
    protected Map<ReachableMyBIKSTabCode, IMyBIKSTab> reachableTabs = new EnumMap<ReachableMyBIKSTabCode, IMyBIKSTab>(ReachableMyBIKSTabCode.class);
    protected IMyBIKSTab tabToShowOnActivatePage;

    public MyBIKBikPage() {
        //ww: musi być na konstruktorze, bo jest metoda showTabByCode, która
        // korzysta z m. in. reachableTabs i ta metoda może być wywołana
        // przed buildWidgets (jeżeli BIKS zostanie uruchomiony z linka
        // wskazującego konkretną stronę inną niż myBIKS)
        setupTabButtons();
    }

    private void setupTabButtons() {
        if (isDemoVersion) {
            setupTabButtons(homePageTab);
        }
        List<IMyBIKSTab> tabsToShow = new ArrayList<IMyBIKSTab>();
        tabsToShow.add(myObjects);
        tabsToShow.add(new MyBIKSUsersObjects());
        tabsToShow.add(newsTab);
        tabsToShow.add(favouriteTab);
        if (BIKClientSingletons.isShowTutorials()) {
            tabsToShow.add(new MyBIKSTutorialTab());
        }
        tabsToShow.add(new MyBIKSHistoryTabNew());
        if (!BIKClientSingletons.isHideElementsForMM()) {
            tabsToShow.add(new MyBIKSNotesTab());
        }
        tabsToShow.add(new MyBIKSVoteTab());
        tabsToShow.add(new MyBIKSEditUserTab());

        setupTabButtons(tabsToShow.toArray(new IMyBIKSTab[tabsToShow.size()]));
    }

    @Override
    public String getId() {
        return BIKGWTConstants.TAB_ID_MYBIKS; //"myBIK";
    }

//    public String getCaptionDeprecated() {
//        return BIKGWTConstants.MENU_NAME_MY_BIKS;
//    }
    @Override
    protected Widget buildWidgets() {

        mainHp = new HorizontalPanel();
        mainHp.setWidth("100%");
        mainHp.setHeight("100%");
        mainHp.setStyleName("startHps");
        leftVp = new VerticalPanel();
        rightVp = new VerticalPanel();
        rightDownVp = new VerticalPanel();
        leftBtnVp = new VerticalPanel();
        welcomeMsg = new HorizontalPanel();
        welcomeMsg.setStyleName("MyBIK");
        caption = new HTML();

        leftVp.setStyleName("MyBIKAvatarLefPanel");
        redisplayMainLeftPanel();

        loremHtml = new HTML(BIKClientSingletons.getWelcomeMsgForVersion());
        loremHtml.setStyleName("MyBIK");

        mustLoginHeaderLbl = new HTML("<h3>" + I18n.musiszSieZalogowac.get() /* I18N:  */ + "</h3>");
        mustLoginHeaderLbl.setStyleName("MyBIK");

        rightVp.add(loremHtml);

        DisabledUserApplyForAccountMsgsBean duafamb = BIKClientSingletons.getDisabledUserApplyForAccountMsgs();

        if (duafamb != null && !BaseUtils.isStrEmptyOrWhiteSpace(duafamb.availableOptions) && !BaseUtils.isStrEmptyOrWhiteSpace(duafamb.mailTo)) {

            ApplyForExtendedAccessFormPanel afeafp = new ApplyForExtendedAccessFormPanel(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet().replace("/fsb", ""));
            if (afeafp.asWidget().isVisible()) {
                DisclosurePanel dp = new DisclosurePanel(duafamb.availableOptions);
//            dp.setHeader(new HTML(duafamb.availableOptions));
                dp.setOpen(false);
                dp.addStyleName("accessRequest");
                dp.add(afeafp);
                rightVp.add(dp);
            }
        }

        if (BIKClientSingletons.isMultiXMode()) {
            invite = new PushButton(I18n.zaprosUzytkownikow.get());

            ClickHandler inviteHandler = new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    final InviteUsersDialog window = new InviteUsersDialog();
                    window.buildAndShowDialog(null, window.getContinuation());
                }
            };
            invite.addClickHandler(inviteHandler);

            invite.addStyleName("actionHtml");
//            DOM.setElementAttribute(invite.getElement(), "id", "myCustomPushButton");
            GWTUtils.setElementAttribute(invite, "id", "myCustomPushButton");

            if (BIKClientSingletons.isUserCreatorOfThisDb()) {
                rightVp.add(createResetPanel());
            }
            rightVp.add(invite);
        }

        rightVp.add(mustLoginHeaderLbl);

        //ww->mybiks: zadziwiające i błędne tworzenie zakładek, które nie są potrzebne
        // czasem nawet tworzenie jest po kilka razy, potem clear itp. znów inne się tworzą
        // itd. ---> do posprzątania
        if (!BIKClientSingletons.isUserLoggedIn() && BIKClientSingletons.isShowTutorials()) {
            MyBIKSTutorialTab mb = new MyBIKSTutorialTab();
            VerticalPanel mbv = new VerticalPanel();
            mb.show(mbv);
            mbv.setStyleName("MyBIK");
            tutorial = new HTML("<h3>" + I18n.samouczek.get() /* I18N:  */ + "</h3>");
            tutorial.setStyleName("MyBIK");
            rightVp.add(tutorial);
            rightVp.add(mbv);
        }
        rightVp.add(welcomeMsg);

        captionAndHelpFp = new HorizontalPanel();
        caption.setStyleName("MyBIKS-HTML");
        captionAndHelpFp.add(caption);
        captionAndHelpFp.setStyleName("MyBIKS-caption");

        rightVp.add(captionAndHelpFp);
        rightDownVp.setWidth("100%");
        rightVp.add(rightDownVp);//scPanelFvs
        rightDownVp.setStyleName("MyBIKpadding");
        ScrollPanel scPanelFvsm = new ScrollPanel(mainHp);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(scPanelFvsm, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT);
        scPanelFvsm.setStyleName("EntityDetailsPane");//mainHp

        mainHp.add(leftVp);
        mainHp.add(rightVp);

        return scPanelFvsm;//mainHp
    }

    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        if (BIKClientSingletons.isUserLoggedIn()) {
            refreshNewItemsMarksOnTabs();

            IMyBIKSTab tabToShow;

            if (isDemoVersion) {
                welcomeMsg.clear();
                welcomeMsg.add(new HTML(I18n.jestesZalogowanyJako.get() /* I18N:  */ + ":  "));
                welcomeMsg.add(homePageTab.getWelcomeMsg());
                tabToShow = homePageTab;
            } else if (myObjects.isVisible()) {
                tabToShow = myObjects;
            } else {
                tabToShow = newsTab;
            }

            if (tabToShowOnActivatePage != null) {
                tabToShow = tabToShowOnActivatePage;
                tabToShowOnActivatePage = null;
            }

            showTab(tabToShow);
        }

//        captionAndHelpFp.setCellWidth(caption, "100%");
        //ww->mybiks: mistrzostwo (osiedlowe) mikroarchitektury kodu, co tu się dzieje?
        // najpierw natworzone zbędnych widgetów, tabów itp., a teraz ukrywamy
        // co nam nie potrzeba, w tym mbv i tutorial, leftVp (!) itp.
        boolean isDataVisible = BIKClientSingletons.isUserLoggedIn();
        mustLoginHeaderLbl.setVisible(!isDataVisible);
//        tutorial.setVisible(!isDataVisible);
//        mbv.setVisible(!isDataVisible);
        welcomeMsg.setVisible(isDataVisible);
        caption.setVisible(isDataVisible);
        leftVp.setVisible(isDataVisible);
    }

    protected void setupTabButtons(IMyBIKSTab... mbts) {
        for (IMyBIKSTab mbt : mbts) {
            if (!mbt.isVisible()) {
                continue;
            }
            mbt.setMyBIKSPage(this);
            tabsBtn.put(mbt, createTabButton(mbt));
            ReachableMyBIKSTabCode tabCode = mbt.getOptTabCode();
            if (tabCode != null) {
                reachableTabs.put(tabCode, mbt);
            }
        }
    }

    //ww: to jest wołane, gdy już jesteśmy na jakiejś stronie myBIKS
    // czyli w trakcie lub po activatePage
    protected void showTab(IMyBIKSTab mbt) {
        caption.setHTML(/*"<h2>" +*/mbt.getCaption() /*+ hasNewItems +"</h2>"*/);
        mbt.show(rightDownVp);
        captionAndHelpFp.clear();
        captionAndHelpFp.add(caption);
        if (mbt.getHelpCaption() != null) {
            PushButton helpPb = BIKClientSingletons.createHelpHintButton(mbt.getHelpCaption(), I18n.samouczek2.get());
            helpPb.setStyleName("helpBtn");
            captionAndHelpFp.setCellWidth(caption, "100px");
            captionAndHelpFp.add(helpPb);
        } else {
            captionAndHelpFp.setCellWidth(caption, "100%");
        }
    }

    protected BikCustomButton createTabButton(final IMyBIKSTab mbt) {
        BikCustomButton actBtn = new BikCustomButton(mbt.getCaption(), mbt.getButtonStyle(),
                new IContinuation() {
                    public void doIt() {
                        showTab(mbt);
                    }
                });
        return actBtn;
    }

    protected void redisplayMainLeftPanel() {
        leftVp.clear();
        leftBtnVp.clear();
        Panel aPanel = new AbsolutePanel();

        aPanel.setStyleName("MyBIKAvatarPanel");
        String avatar = BIKClientSingletons.getLoggedUserAvatar();
        Image avatarImg = BIKClientSingletons.createAvatarImage(avatar, 190, 190, "images/noavatar.jpg" /* I18N: no */, "MyBIKAvatarImg", null);
        aPanel.add(avatarImg);
        for (BikCustomButton b : tabsBtn.values()) {
            leftBtnVp.add(b);
        }
        if (!BIKClientSingletons.isHideElementsForMM()) {
            BikCustomButton rssBtn = new BikCustomButton(I18n.rss.get(), "labelLink-rss",
                    new IContinuation() {
                        @Override
                        public void doIt() {
                            Window.open(generateDownloadLink(), "_blank", "");
                        }
                    });
            leftBtnVp.add(rssBtn);
        }
        leftVp.add(aPanel);
        leftVp.add(leftBtnVp);

    }

    private String generateDownloadLink() {
        StringBuilder sb = new StringBuilder(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet());
        sb.append("?").append(BIKConstants.RSS);
        return sb.toString();
    }

    //--------
    //ww: implementacja interfejsu IMyBIKSPage
    @Override
    public void refreshAvatarImg() {
        redisplayMainLeftPanel();
    }

    @Override
    public void refreshTabButton(IMyBIKSTab tab) {
        refreshNewItemsMarksOnTabs();
    }

    protected void refreshNewItemsMarkOnOneTab(IMyBIKSTab mbt, boolean showMark) {
        tabsBtn.get(mbt).setHTML(mbt.getCaption() + (showMark ? "<font color=\"FF6600\"><i> (" + I18n.nowy.get() /* I18N:  */ + ")</i></font>" : ""));
        mbt.updateHasNewItems(showMark);
    }

    protected void refreshNewItemsMarksOnTabs() {
        BIKClientSingletons.getService().getShortCodesForMyBIKSTabsWithNewItems(new StandardAsyncCallback<Set<String>>() {
            @Override
            public void onSuccess(Set<String> result) {
                boolean showNewItemsMarkOnNew = result.contains(BIKGWTConstants.MYBIKSTAB_SHORT_CODE_NEWS);
                boolean showNewItemsMarkOnFvs = result.contains(BIKGWTConstants.MYBIKSTAB_SHORT_CODE_FAVOURITES);

                refreshNewItemsMarkOnOneTab(favouriteTab, showNewItemsMarkOnFvs);
                refreshNewItemsMarkOnOneTab(newsTab, showNewItemsMarkOnNew);
            }
        });
    }

    // to może zostać wywołane gdy wcale nie jesteśmy na myBIKS - z innej strony
    // BIKSa, ale może też z myBIKS - dostępne przez interface
    protected void showTabWithHistoryEntryAndActivatePage(IMyBIKSTab tab) {
        //HistoryManager.addHistoryToken(getId(), null, false);
        tabToShowOnActivatePage = tab;
        //activatePage(false, null, true);
        //BIKClientSingletons.getTabSelector().selectValueOnTab(getId(), null);
        BIKClientSingletons.getTabSelector().submitNewTabSelection(getId(), null, true);
    }

    // to może zostać wywołane gdy wcale nie jesteśmy na myBIKS - z innej strony
    // BIKSa, ale może też z myBIKS - dostępne przez interface
    @Override
    public void showTabByCode(ReachableMyBIKSTabCode tabCode) {
        IMyBIKSTab tab = reachableTabs.get(tabCode);
        if (tab == null) {
            tab = BaseUtils.safeGetFirstItem(tabsBtn.keySet(), null);
        }
        if (tab != null) {
            showTabWithHistoryEntryAndActivatePage(tab);
        }
    }

    private HorizontalPanel createResetPanel() {
        PushButton resetButton = new PushButton(I18n.resetujBaze.get());

        resetButton.addStyleName("actionHtml resetBtn");
        resetButton.setTitle(I18n.resetujBazeOpis.get());
//        DOM.setElementAttribute(resetButton.getElement(), "id", "myCustomPushButton");
        GWTUtils.setElementAttribute(resetButton.getElement(), "id", "myCustomPushButton");
        ClickHandler resetHandler = new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ResetDatabaseDialog window = new ResetDatabaseDialog();
                window.buildAndShowDialog();
            }
        };
        resetButton.addClickHandler(resetHandler);

        HorizontalPanel resetPanel = new HorizontalPanel();

        resetPanel.add(resetButton);
        resetPanel.add(new Label(I18n.resetujBazeOpis.get()));
        return resetPanel;
    }
}
