set nocount on

declare @rc int

print cast(sysdatetime() as varchar(23)) + ': start'


        select top 20 bn.id as node_id,
        bn.name,
        bnk.code as node_kind_code,
        bnk.caption as node_kind_caption,
        bt.name as tree_name,
        bt.code as tree_code,
        bn.vote_sum,
        bn.vote_cnt
        from 
        bik_node bn
            inner join
            (select node_id,
            sum(sr.[rank] * coalesce(ap.search_weight, 1)) as sum_combined_rank,
            max(sr.[rank] * coalesce(ap.search_weight, 1)) as max_combined_rank,
            sum(coalesce(ap.search_weight, 1)) as sum_search_weight,
            max(coalesce(ap.search_weight, 1)) as max_search_weight
            
            , max(case when /*av.value like '%id[_]konto%'*/ 1 = 1 then 1 else 0 end) as has_match
            
            from bik_searchable_attr_val av inner join
            containstable(bik_searchable_attr_val, *, '"id konto"') sr on av.id = sr.[key]
            inner join bik_searchable_attr a on av.attr_id = a.id
            left join bik_searchable_attr_props ap on a.name = ap.name
where
        av.node_kind_id       in (56,39)  
        and av.tree_id in (1,2)
            group by node_id            
            ) sr on sr.node_id = bn.id
	inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
        inner join bik_tree bt on bn.tree_id = bt.id
where bn.is_deleted = 0 and (bn.linked_node_id is null or bn.disable_linked_subtree <> 0)
        and sr.has_match = 1
        --and bn.tree_id in (1,2)
        --and bn.node_kind_id in (56,39)          
order by sr.max_search_weight desc, bn.search_rank + bnk.search_rank desc, bn.vote_sum desc, sum_combined_rank desc
        
        
set @rc = @@rowcount
print cast(sysdatetime() as varchar(23)) + ': koniec, wierszy: ' + cast(@rc as varchar(20))

-- select * from bik_app_prop

