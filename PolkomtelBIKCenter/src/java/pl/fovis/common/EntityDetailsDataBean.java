/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;
import pl.bssg.metadatapump.common.ProfileBean;
import pl.bssg.metadatapump.common.ProfileFileBean;
import pl.fovis.common.dqm.DQMCoordinatorDataBean;
import pl.fovis.common.dqm.DQMDataErrorIssueBean;
import pl.fovis.common.dqm.DQMEvaluationBean;
import pl.fovis.common.dqm.DQMReportBean;
import pl.fovis.common.dqm.DataQualityGroupBean;
import pl.fovis.common.dqm.DataQualityRequestBean;
import pl.fovis.common.dqm.DataQualityTestBean;
import pl.fovis.common.dqm.DqmGrupaDanychPotomkowieBean;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class EntityDetailsDataBean implements Serializable {

    /*
     *
     * UWAGA
     *
     * Ostrożnie ze zmianami nazw pól w tej klasie. Po takiej zmianie należy
     * zmienić nazwę odpowiedniej metody w klasie
     * EntityDetailsDataBeanPropsReader
     *
     * Bez zmiany nazwy metody w EntityDetailsDataBeanPropsReader, a tylko po
     * zmianie nazwy pola - będą lecieć błędy z metody
     * BOXIServiceImpl.getBikEntityDetailsData
     *
     */
    //public int nodeId;
    //
    // to pole jest uzupełniane na szczególnych warunkach - zawsze
    @EDDBProp4NodeKind({})
    public TreeNodeBean node;
    //
    // to pole jest uzupełniane zawsze, wpisanie {} powoduje, że nie będzie ujęte
    // przez mechanizm refleksyjny
    @EDDBProp4NodeKind()
    public UserBean user;
    //
    @EDDBProp4NodeKind()
    public Set<Integer> grantedNodeRightActionIds;
    //
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_TERADATA_TABLE, BIKConstants.NODE_KIND_TERADATA_VIEW})
    public String teratadaExtradata; // jeśli będzie więcej extradaty z teradaty to można dać beana zamiast Stringa
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_MSSQL_VIEW, BIKConstants.NODE_KIND_MSSQL_PROCEDURE, BIKConstants.NODE_KIND_MSSQL_FUNCTION, BIKConstants.NODE_KIND_MSSQL_TABLE_FUNCTION})
    public String mssqlExtradata; // definicje dla widoków,procedur,funkcji
    @EDDBProp4TreeKind({BIKConstants.TREE_KIND_METADATA})
    public String dbDefinition; // definicje dla widoków,procedur,funkcji
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_MSSQL_VIEW, BIKConstants.NODE_KIND_MSSQL_TABLE, BIKConstants.NODE_KIND_MSSQL_COLUMN, BIKConstants.NODE_KIND_MSSQL_COLUMN_PK, BIKConstants.NODE_KIND_MSSQL_COLUMN_FK})
    public List<DBColumnBean> mssqlColumnExtradata;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_MSSQL_TABLE, BIKConstants.NODE_KIND_MSSQL_COLUMN_PK, BIKConstants.NODE_KIND_MSSQL_COLUMN_FK})
    public List<DBForeignKeyBean> mssqlForeignKeyExtradata;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_MSSQL_TABLE, BIKConstants.NODE_KIND_MSSQL_COLUMN, BIKConstants.NODE_KIND_MSSQL_COLUMN_PK, BIKConstants.NODE_KIND_MSSQL_COLUMN_FK})
    public List<DBPartitionBean> mssqlPartitionExtradata;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_MSSQL_PROCEDURE, BIKConstants.NODE_KIND_MSSQL_FUNCTION, BIKConstants.NODE_KIND_MSSQL_TABLE_FUNCTION, BIKConstants.NODE_KIND_MSSQL_VIEW})
    public List<DBDependencyBean> mssqlDependencyExtradata;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_MSSQL_PROCEDURE, BIKConstants.NODE_KIND_MSSQL_FUNCTION, BIKConstants.NODE_KIND_MSSQL_TABLE_FUNCTION, BIKConstants.NODE_KIND_MSSQL_VIEW, BIKConstants.NODE_KIND_MSSQL_COLUMN, BIKConstants.NODE_KIND_MSSQL_COLUMN_FK, BIKConstants.NODE_KIND_MSSQL_COLUMN_PK, BIKConstants.NODE_KIND_MSSQL_TABLE})
    public List<DBDependentObjectBean> mssqlDependentObjectsExtradata;
//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_MSSQL_SERVER, BIKConstants.NODE_KIND_MSSQL_DATABASE, BIKConstants.NODE_KIND_MSSQL_FOLDER})
//    public List<ChildrenBean> mssqlChildrenExtradata;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_MSSQL_DATABASE, BIKConstants.NODE_KIND_MSSQL_TABLE, BIKConstants.NODE_KIND_MSSQL_VIEW, BIKConstants.NODE_KIND_MSSQL_PROCEDURE, BIKConstants.NODE_KIND_MSSQL_FUNCTION, BIKConstants.NODE_KIND_MSSQL_TABLE_FUNCTION, BIKConstants.NODE_KIND_MSSQL_COLUMN, BIKConstants.NODE_KIND_MSSQL_COLUMN_FK, BIKConstants.NODE_KIND_MSSQL_COLUMN_PK})
    public List<MssqlExPropBean> mssqlExProps;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_MSSQL_TABLE, BIKConstants.NODE_KIND_MSSQL_COLUMN, BIKConstants.NODE_KIND_MSSQL_COLUMN_PK, BIKConstants.NODE_KIND_MSSQL_COLUMN_FK})
    public List<DBIndexBean> mssqlIndices;
    // extradata dla bazodanowych obiektów
//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_POSTGRES_TABLE, BIKConstants.NODE_KIND_POSTGRES_VIEW, BIKConstants.NODE_KIND_POSTGRES_COLUMN, BIKConstants.NODE_KIND_POSTGRES_COLUMN_FK, BIKConstants.NODE_KIND_POSTGRES_COLUMN_PK})
    @EDDBProp4TreeKind({BIKConstants.TREE_KIND_METADATA})
    public List<DBColumnBean> dbColumnExtradata;
//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_POSTGRES_TABLE, BIKConstants.NODE_KIND_POSTGRES_COLUMN_FK, BIKConstants.NODE_KIND_POSTGRES_COLUMN_PK})
    @EDDBProp4TreeKind({BIKConstants.TREE_KIND_METADATA})
    public List<DBForeignKeyBean> dbForeignKeyExtradata;
//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_POSTGRES_TABLE, BIKConstants.NODE_KIND_POSTGRES_COLUMN, BIKConstants.NODE_KIND_POSTGRES_COLUMN_PK, BIKConstants.NODE_KIND_POSTGRES_COLUMN_FK})
    @EDDBProp4TreeKind({BIKConstants.TREE_KIND_METADATA})
    public List<DBPartitionBean> dbPartitionExtradata;
//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_POSTGRES_FUNCTION, BIKConstants.NODE_KIND_POSTGRES_VIEW})
    @EDDBProp4TreeKind({BIKConstants.TREE_KIND_METADATA})
    public List<DBDependencyBean> dbDependencyExtradata;
//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_POSTGRES_FUNCTION, BIKConstants.NODE_KIND_POSTGRES_VIEW, BIKConstants.NODE_KIND_POSTGRES_COLUMN, BIKConstants.NODE_KIND_POSTGRES_COLUMN_FK, BIKConstants.NODE_KIND_POSTGRES_COLUMN_PK, BIKConstants.NODE_KIND_POSTGRES_TABLE})
    @EDDBProp4TreeKind({BIKConstants.TREE_KIND_METADATA})
    public List<DBDependentObjectBean> dbDependentObjectsExtradata;
//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_POSTGRES_TABLE, BIKConstants.NODE_KIND_POSTGRES_COLUMN, BIKConstants.NODE_KIND_POSTGRES_COLUMN_PK, BIKConstants.NODE_KIND_POSTGRES_COLUMN_FK})
    @EDDBProp4TreeKind({BIKConstants.TREE_KIND_METADATA})
    public List<DBIndexBean> dbIndices;
    //
//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_ORACLE_PROCEDURE, BIKConstants.NODE_KIND_ORACLE_VIEW})
//    public String oracleExtradata; // jeśli będzie więcej extradaty z oracle to można dać beana zamiast Stringa
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_TERADATA_TABLE, BIKConstants.NODE_KIND_TERADATA_COLUMN_PK, BIKConstants.NODE_KIND_TERADATA_COLUMN_IDX})
    public List<DBIndexBean> teradataIndices;
//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_ORACLE_TABLE, BIKConstants.NODE_KIND_ORACLE_COLUMN_PK, BIKConstants.NODE_KIND_ORACLE_COLUMN_FK})
//    public List<DBIndexBean> oracleIndices;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_UNIVERSE_TABLE, BIKConstants.NODE_KIND_UNIVERSE_TABLE_ALIAS, BIKConstants.NODE_KIND_UNIVERSE_TABLE_DERIVED})
    public SapBoUniverseTableBean tables;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_CONFLUENCE_SPACE, BIKConstants.NODE_KIND_CONFLUENCE_PAGE, BIKConstants.NODE_KIND_CONFLUENCE_BLOG})
    public ConfluenceObjectBean confluenceExtradata;
    //
    // zawsze - dla wszystkich kindów
//    public List<AttachmentBean> attachments;
    //
    // zawsze - dla wszystkich kindów
//    public List<NoteBean> notes;
    //
    // zawsze - dla wszystkich kindów
    public List<AttributeBean> attributes;
    public int countAttributeForKind;
    //
//    @EDDBProp4Kind({"User"})
    public List<TreeNodeBean> authorBlogs;

    // zawsze - warunek jest wewnątrz metody
    public List<ChildrenBean> childrenExtradata;
    // zawsze - warunek jest wewnątrz metody
    public List<Map<String, String>> genericNodeObjects;
    //
//    public List<BlogBean> blogs;
//    @EDDBProp4Kind({BIKConstants.NODE_KIND_FAQ_FOLDER})
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_ARTICLE})
//    public List<FrequentlyAskedQuestionsBean> frequentlyAskedQuestions;
    public FrequentlyAskedQuestionsBean frequentlyAskedQuestions;
    //
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_WEBI, BIKConstants.NODE_KIND_FLASH, BIKConstants.NODE_KIND_CRYSTAL_REPORTS,
        BIKConstants.NODE_KIND_UNIVERSE, BIKConstants.NODE_KIND_UNIVERSE_UNX, BIKConstants.NODE_KIND_EXCEL,
        BIKConstants.NODE_KIND_FULL_CLIENT, BIKConstants.NODE_KIND_PDF, BIKConstants.NODE_KIND_HYPERLINK,
        BIKConstants.NODE_KIND_POWERPOINT, BIKConstants.NODE_KIND_WORD, BIKConstants.NODE_KIND_TXT, BIKConstants.NODE_KIND_RTF})
    public SapBoExtraDataBean sapBoExtraData;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_REPORT_FOLDER, BIKConstants.NODE_KIND_UNIVERSE_FOLDER, BIKConstants.NODE_KIND_CONNECTION_FOLDER})
    public SapBoExtraDataBean sapBoFolderExtraData;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_REPORT_SCHEDULE})
    public SapBoScheduleBean sapBoScheduleBean;
    //
//    @EDDBProp4Kind({"UniverseObject"})
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_MEASURE, BIKConstants.NODE_KIND_DIMENSION, BIKConstants.NODE_KIND_DETAIL, BIKConstants.NODE_KIND_ATTRIBUTE, BIKConstants.NODE_KIND_FILTER})
    public SapBoUniverseExtraDataBean sapBoUniverseExtraData;
    //
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DATACONNECTION, BIKConstants.NODE_KIND_CONNECTION_RELATIONAL, BIKConstants.NODE_KIND_CONNECTION_OLAP})
    public SapBoConnectionExtraDataBean sapBoConnectionExtraData;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_CONNECTION_OLAP})
    public SapBoOLAPConnectionExtraDataBean sapBoOLAPConnectionExtraData;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_REPORT_QUERY})
    public SapBoQueryExtraDataBean sapBoQueryDataBean;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_WEBI, BIKConstants.NODE_KIND_FLASH, BIKConstants.NODE_KIND_CRYSTAL_REPORTS,
        BIKConstants.NODE_KIND_UNIVERSE, BIKConstants.NODE_KIND_UNIVERSE_UNX, BIKConstants.NODE_KIND_EXCEL,
        BIKConstants.NODE_KIND_FULL_CLIENT, BIKConstants.NODE_KIND_PDF, BIKConstants.NODE_KIND_HYPERLINK,
        BIKConstants.NODE_KIND_POWERPOINT, BIKConstants.NODE_KIND_WORD, BIKConstants.NODE_KIND_TXT, BIKConstants.NODE_KIND_RTF,
        BIKConstants.NODE_KIND_REPORT_FOLDER, BIKConstants.NODE_KIND_UNIVERSE_FOLDER, BIKConstants.NODE_KIND_CONNECTION_FOLDER,
        BIKConstants.NODE_KIND_DATACONNECTION, BIKConstants.NODE_KIND_CONNECTION_RELATIONAL, BIKConstants.NODE_KIND_CONNECTION_OLAP})
    public List<LinkedAlsoBean> otherBOTheSameObject;
//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_WEBI, BIKConstants.NODE_KIND_FLASH, BIKConstants.NODE_KIND_CRYSTAL_REPORTS, BIKConstants.NODE_KIND_UNIVERSE, BIKConstants.NODE_KIND_EXCEL, BIKConstants.NODE_KIND_FULL_CLIENT, BIKConstants.NODE_KIND_PDF,
//        BIKConstants.NODE_KIND_HYPERLINK, BIKConstants.NODE_KIND_POWERPOINT, BIKConstants.NODE_KIND_TERADATA_SCHEMA, BIKConstants.NODE_KIND_TERADATA_TABLE, BIKConstants.NODE_KIND_TERADATA_VIEW, BIKConstants.NODE_KIND_TERADATA_COLUMN, BIKConstants.NODE_KIND_TERADATA_PROCEDURE,
//        BIKConstants.NODE_KIND_MEASURE, BIKConstants.NODE_KIND_DETAIL, BIKConstants.NODE_KIND_DIMENSION, BIKConstants.NODE_KIND_FILTER, BIKConstants.NODE_KIND_DATACONNECTION, BIKConstants.NODE_KIND_REPORT_QUERY, BIKConstants.NODE_KIND_REPORT_FOLDER, BIKConstants.NODE_KIND_UNIVERSE_FOLDER,
//        BIKConstants.NODE_KIND_CONNECTION_FOLDER, BIKConstants.NODE_KIND_WORD, BIKConstants.NODE_KIND_TXT, BIKConstants.NODE_KIND_RTF, BIKConstants.NODE_KIND_TERADATA_COLUMN_PK, BIKConstants.NODE_KIND_TERADATA_COLUMN_IDX, BIKConstants.NODE_KIND_UNIVERSE_TABLES_FOLDER, BIKConstants.NODE_KIND_UNIVERSE_CLASS,
//        BIKConstants.NODE_KIND_UNIVERSE_TABLE, BIKConstants.NODE_KIND_UNIVERSE_TABLE_ALIAS, BIKConstants.NODE_KIND_UNIVERSE_TABLE_DERIVED, BIKConstants.NODE_KIND_TERADATA_OWNER, BIKConstants.NODE_KIND_DQC_GROUP, BIKConstants.NODE_KIND_DQC_ALL_TESTS_FOLDER, BIKConstants.NODE_KIND_DQC_TEST_SUCCESS})
    @EDDBProp4TreeKind({BIKConstants.TREE_KIND_METADATA/*, BIKConstants.TREE_KIND_QUALITY_METADATA*/})
    public String metadataEditedDescription;
    //public UserBean planner;
    //public UserBean sponsor;
    //public UserBean expert;
    // zawsze - dla wszystkich kindów
//    public List<JoinedObjBean> arts; // powiązane pojęcia biznesowe
//    public List<JoinedObjBean> dictionaries; // powiązane słowniki
    //
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_GLOSSARY, BIKConstants.NODE_KIND_GLOSSARY_VERSION, BIKConstants.NODE_KIND_GLOSSARY_NOT_CORPO})
    public ArticleBean thisBizDef;
    //
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_BLOG_ENTRY})
    public BlogBean blogEntry;
    //
    // teraz może być więcej nodów z dokumentem. Sprawdzam dla wszystkich
//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DOCUMENT})
    public AttachmentBean thisDocument;
    //
    public VoteBean vote;
    //
    // zawsze - dla wszystkich kindów
//    public List<JoinedObjBean> joinedObjs;
    //
    // zawsze - dla wszystkich kindów
    public boolean isInFav;
    //
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_GLOSSARY, BIKConstants.NODE_KIND_GLOSSARY_VERSION, BIKConstants.NODE_KIND_GLOSSARY_NOT_CORPO})
    public List<ArticleBean> versions;
    // zawsze - dla wszystkich kindów
    // TF: przerzucamy kod do BOXIServiceImpl - ładujemy na żądanie
//    public Pair<List<UserInNodeBean>, Map<Integer, String>> usersInNode;
//    @EDDBProp4Kind({BIKConstants.NODE_KIND_USER})
//    public Map<Integer, List<UserRoleInNodeBean>> userRolesInNodes;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_BLOG, BIKConstants.NODE_KIND_BLOG_ENTRY})
    public List<BlogAuthorBean> blogAuthors;
    // zawsze - dla wszystkich kindów
    public boolean hasChildren;
    @EDDBProp4NodeKind({})
    public String properDescr;
    @EDDBProp4NodeKind({})
    public Map<Integer, Pair<String, String>> innerLinks;
    @EDDBProp4NodeKind({})
    public List<String> ancestorNodePath;
    // Data Quality
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DQM_TEST_SUCCESS, BIKConstants.NODE_KIND_DQM_TEST_NOT_STARTED, BIKConstants.NODE_KIND_DQM_TEST_INACTIVE, BIKConstants.NODE_KIND_DQM_TEST_FAILED, BIKConstants.NODE_KIND_DQC_TEST_SUCCESS, BIKConstants.NODE_KIND_DQC_TEST_FAILED, BIKConstants.NODE_KIND_DQC_TEST_INACTIVE})
    public DataQualityTestBean dataQualityTestExtradata;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DQM_TEST_SUCCESS, BIKConstants.NODE_KIND_DQM_TEST_NOT_STARTED, BIKConstants.NODE_KIND_DQM_TEST_INACTIVE, BIKConstants.NODE_KIND_DQM_TEST_FAILED, BIKConstants.NODE_KIND_DQC_TEST_SUCCESS, BIKConstants.NODE_KIND_DQC_TEST_FAILED, BIKConstants.NODE_KIND_DQC_TEST_INACTIVE})
    public List<DataQualityRequestBean> dataQualityTestRequests;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DQM_GROUP, BIKConstants.NODE_KIND_DQC_GROUP})
    public List<DataQualityGroupBean> dataQualityTestGroupExtradata;
//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DQM_TEST_FAILED, BIKConstants.NODE_KIND_DQM_TEST_INACTIVE, BIKConstants.NODE_KIND_DQM_TEST_NOT_STARTED, BIKConstants.NODE_KIND_DQM_TEST_SUCCESS, BIKConstants.NODE_KIND_DQC_TEST_SUCCESS, BIKConstants.NODE_KIND_DQC_TEST_FAILED, BIKConstants.NODE_KIND_DQC_TEST_INACTIVE, BIKConstants.NODE_KIND_USER})
//    @EDDBProp4NodeKind()
    public List<LinkedAlsoBean> alsoLinked;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DQM_RATE, BIKConstants.NODE_KIND_DQM_RATE_ACCEPTED, BIKConstants.NODE_KIND_DQM_RATE_REJECTED})
    public DQMEvaluationBean dqmEvaluationBean;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DQM_REPORT})
    public Pair<DQMReportBean, List<DQMEvaluationBean>> dqmReportEvaluations;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DQM_FOLDER_COORDINATION})
    public Map<String, DQMCoordinatorDataBean> dqmCoordinatorData;
    //
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_USER, BIKConstants.NODE_KIND_BLOG, BIKConstants.NODE_KIND_BLOG_ENTRY})
    public Pair<List<UserRoleInNodeBean>, Map<Integer, String>> userRolesInNodes;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_USER})
    public UserExtradataBean userExtradata;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_SAPBW_BEX, BIKConstants.NODE_KIND_SAPBW_AREA, BIKConstants.NODE_KIND_SAPBW_CUBE, BIKConstants.NODE_KIND_SAPBW_ISET, BIKConstants.NODE_KIND_SAPBW_MPRO, BIKConstants.NODE_KIND_SAPBW_ODSO, BIKConstants.NODE_KIND_SAPBW_CHA, BIKConstants.NODE_KIND_SAPBW_DPA, BIKConstants.NODE_KIND_SAPBW_KYF, BIKConstants.NODE_KIND_SAPBW_TIM, BIKConstants.NODE_KIND_SAPBW_UNI})
    public SapBWBean sapbwExtradata;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_SAPBW_CUBE, BIKConstants.NODE_KIND_SAPBW_ISET, BIKConstants.NODE_KIND_SAPBW_MPRO, BIKConstants.NODE_KIND_SAPBW_ODSO})
    public String sapbwOriginalProvidersArea;
    //
    public boolean isAuthor;
    public boolean isEditor;
    //
//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_USERS_ROLE})
//    public String roleCode;
    //
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_USERS_ROLE, BIKConstants.NODE_KIND_USER})
    public Map<Integer, String> rolesNodeIdAndCode;
    //
    public AuthorBean author;

    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_TERADATA_TABLE, BIKConstants.NODE_KIND_TERADATA_COLUMN, BIKConstants.NODE_KIND_TERADATA_COLUMN_PK})
    public List<ErwinTableBean> erwinListTables;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_TERADATA_TABLE, BIKConstants.NODE_KIND_TERADATA_COLUMN, BIKConstants.NODE_KIND_TERADATA_COLUMN_PK})
    public List<ErwinShapeBean> erwinShapes;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_TERADATA_TABLE, BIKConstants.NODE_KIND_TERADATA_COLUMN, BIKConstants.NODE_KIND_TERADATA_COLUMN_PK})
    public List<ErwinTableColumnBean> erwinTableColumns;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_TERADATA_TABLE, BIKConstants.NODE_KIND_TERADATA_COLUMN, BIKConstants.NODE_KIND_TERADATA_COLUMN_PK})
    public List<ErwinRelationshipBean> erwinRelationships;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_TERADATA_TABLE, BIKConstants.NODE_KIND_TERADATA_COLUMN, BIKConstants.NODE_KIND_TERADATA_COLUMN_PK, BIKConstants.NODE_KIND_TERADATA_AREA})
    public List<ErwinSubjectAreaBean> erwinSubjectAreas;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_TERADATA_TABLE, BIKConstants.NODE_KIND_TERADATA_COLUMN, BIKConstants.NODE_KIND_TERADATA_COLUMN_PK})
    public List<ErwinSubjectAreaObjectRefBean> erwinErwinSubjectAreaObjectRefs;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_TERADATA_TABLE, BIKConstants.NODE_KIND_TERADATA_AREA})
    public List<ErwinDataModelProcessObjectsRel> erwinDataModelProcessObjectsRel;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_TERADATA_TABLE, BIKConstants.NODE_KIND_TERADATA_AREA})
    public List<String> erwinDataModelProcessAreas;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_PROFILE_ITEM})
    public ProfileBean profileItemExtradata;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_PROFILE_FILE})
    public ProfileFileBean profileFileExtradata;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DQM_ERROR_CLOSED, BIKConstants.NODE_KIND_DQM_ERROR_OPENED})
    public DQMDataErrorIssueBean dataErrorIssueBean;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DQM_ERROR_REGISTER})
    public List<DQMDataErrorIssueBean> allDataErrorIssuesInNode;

//    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DQM_GROUP})
//    public Map<Integer, String> dqmAtrybutyJakosciDanych;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DQM_GROUP}) // id atrybutu -> testy pod nim
    public DqmGrupaDanychPotomkowieBean dqmGrupaDanychPotomkowie;

    public String crrLabels;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_RAW_FILE})
    public Long lastModified;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_RAW_FILE})
    public String rawFileDownloadLink;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_RAW_FILE})
    public String filePath;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DOCUMENTS_FOLDER})
    public String folderPath;

    public List<NodeHistoryChangeBean> changeHistory;
//    public List<ObjectInFvsChangeBean> similarFavouriteWithSimilarityScore;

    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_LISA_CATEGORIZATION, BIKConstants.NODE_KIND_LISA_CATEGORY, BIKConstants.NODE_KIND_LISA_DICTIONARIES, BIKConstants.NODE_KIND_LISA_DICTIONARY})
    public LisaInstanceInfoBean lisaInstanceInfo;

    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_META_PRINT_ATTRIBUT})
    public String printAttribute;

//    public Map<String, Object> patterAndValueAttrPrint;
//    public Map<String, PrintTemplateBean> printTemplates;
    @EDDBProp4NodeKind({BIKConstants.NODE_KIND_META_NODE_KIND})
    public boolean isTemplatesFolderAdd;
}
