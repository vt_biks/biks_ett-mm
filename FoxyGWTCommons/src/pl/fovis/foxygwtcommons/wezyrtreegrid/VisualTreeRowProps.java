/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.wezyrtreegrid;

import java.util.List;

/**
 *
 * @author wezyr
 */
public class VisualTreeRowProps<ROW> {

    public ROW row;
    public VisualTreeRowProps<ROW> parentRow;
    public List<VisualTreeRowProps<ROW>> childRows;
    public boolean isExpanded; // true -> childRows != null
    //public Boolean hasChildren; // null -> unchecked (may have children)
    //public int idxAtParent;
    //public int expandedSubTreeCnt = 1;
    public int level; // 0 -> root
    public boolean isVisible;
    public int globalRowId;
    public boolean isLeafForSure;
    public boolean isExpanding;

    @Override
    public String toString() {
        return "VisualTreeRowProps{" + "row=" + row + ", isExpanded=" + isExpanded + ", level=" + level + ", isVisible=" + isVisible + ", globalRowId=" + globalRowId + ", isLeafForSure=" + isLeafForSure + ", isExpanding=" + isExpanding + '}';
    }

    public void setIsExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }
}
