/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.contentsExtractor;

/**
 *
 * @author pmielanczuk
 */
public interface IContentsExtractor {

    public boolean canExtractFromFile(String fullFilePath);

    public String extractContents(String fullFilePath);
}
