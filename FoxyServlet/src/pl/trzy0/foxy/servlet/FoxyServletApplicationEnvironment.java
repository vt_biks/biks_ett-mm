/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import commonlib.IOutboundUrlRewriter;
import java.util.Map;
import pl.foxys.foxymill.IRenderable;
import pl.trzy0.foxy.commons.templates.INamedTemplateProvider;
import pl.trzy0.foxy.serverlogic.IApplicationEnvironment;
import pl.trzy0.foxy.serverlogic.FoxyUrlRewriter;
import simplelib.MapHelper;

/**
 *
 * @author wezyr
 */
public class FoxyServletApplicationEnvironment implements IApplicationEnvironment {

    private final IFoxyMillBundleBroker bundleBroker;
    private final String canonicalSireUrl;
    private final String canonicalAppUrlBase;
    private final String canonicalResourceUrlBase;
    private final FoxyUrlRewriter urlRewriter;
    private boolean usesSubdomains;

    public FoxyServletApplicationEnvironment(IFoxyMillBundleBroker bundleBroker,
            String canonicalSireUrl,
            String canonicalAppUrlBase,
            String canonicalResourceUrlBase,
            FoxyUrlRewriter urlRewriter,
            boolean usesSubdomains) {
        this.bundleBroker = bundleBroker;
        this.canonicalSireUrl = canonicalSireUrl;
        this.canonicalAppUrlBase = canonicalAppUrlBase;
        this.canonicalResourceUrlBase = canonicalResourceUrlBase;
        this.urlRewriter = urlRewriter;
        this.usesSubdomains = usesSubdomains;
    }

    public boolean getUsesSubdomains() {
        return usesSubdomains;
    }

    protected FoxyMillBundle getBundle() {
        return bundleBroker.getBundle();
    }

    public MapHelper<String> getSysProps() {
        return getBundle().sysProps;
    }

    @SuppressWarnings("unchecked")
    public Map<String, String> getTextProps(String langCode) {
        return (Map) getBundle().textsProps.get(langCode);
    }

    public IRenderable getPage(String name) {
        return getBundle().boxes.get(name);
    }

    public Object getGlobal(String name) {
        return getBundle().globals.get(name);
    }

    public INamedTemplateProvider getTemplateProvider(String langCode) {
        return getBundle().tmplProviders.get(langCode);
    }

    public String getCanonicalAppUrlBase() {
        return canonicalAppUrlBase;
    }

    public String getCanonicalResourceUrlBase() {
        return canonicalResourceUrlBase;
    }

    public String getCanonicalSiteUrl() {
        return canonicalSireUrl;
    }

    public IOutboundUrlRewriter getOptOutboundUrlRewriter() {
//        if (urlRewriter == null) {
//            ILameResourceProvider resProvider = getBundle().getResourceProvider();
//            String fileName = "/WEB-INF/urlrewrite.xml";
//            IResourceFromProvider<InputStream> res = new ResourceFromProvider<InputStream>(resProvider, fileName);
//            IWatchedResourceFromProvider<InputStream> wr = new WatchedResourceFromProvider<InputStream>(res);
//
//            urlRewriter = new FoxyUrlRewriter(wr, fileName);
//        }
//
        return urlRewriter;
    }
}
