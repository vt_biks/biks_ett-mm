/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.renderctx;

import java.util.HashMap;
import pl.foxys.foxymill.CacheItem;
import pl.foxys.foxymill.tags.CacheTag;

/**
 *
 * @author wezyr
 */
public class CachingOutputDevice implements IOutputDevice {

    private CacheItem cacheItem = new CacheItem();
    private StringBuilder sb = new StringBuilder();

    public void append(Object o) {
        if (o != null) {
            sb.append(o);
        }
    }

    public void appendCacheTag(CacheTag ct, IRenderCtx renderCtx) {
        cacheItem.renderedParts.add(sb.toString());
        sb.setLength(0);
        cacheItem.subCacheTags.add(ct);
        cacheItem.subCacheVars.add(new HashMap<String, Object>(renderCtx.getVars()));
    }

    public CacheItem doneRendering() {
        cacheItem.renderedParts.add(sb.toString());
        sb.setLength(0);
        return cacheItem;
    }
}
