/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

/**
 *
 * @author ctran
 */
public class MsSqlConnectionCfg extends StandardDBConnectionConfig {

    public MsSqlConnectionCfg() {
    }

    public MsSqlConnectionCfg(String server, String database, String user, String password) {
        this.server = server;
        this.database = database;
        this.user = user;
        this.password = password;
    }
}
