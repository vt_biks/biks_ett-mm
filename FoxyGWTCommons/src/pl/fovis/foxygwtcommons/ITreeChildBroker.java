/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import java.util.List;

/**
 *
 * @author pmielanczuk
 */
public interface ITreeChildBroker<DN> {
    public List<DN> getChildNodes(DN node);
}
