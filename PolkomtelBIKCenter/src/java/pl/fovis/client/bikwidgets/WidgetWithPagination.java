/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author mmastalerz
 */
public class WidgetWithPagination<T> implements IPaginatedItemsProvider<T> {

    protected List<T> allItems;
    protected List<T> filteredItems;
    protected List<T> displayedItems;
    protected int currPageNum;
    protected int packSize;
    protected IPaginatedItemsDisplayer<T> itemsDisplayer;
    protected IGridItemsProvider<T> allItemsProvider;
    protected VerticalPanel gridWithPagination = new VerticalPanel() {
        {
            setWidth("100%");
        }
    };
    protected HorizontalPanel paginationHeaderHp = new HorizontalPanel() {
        {
            setStyleName("JoinedObj-pagination");

            setWidth("100%");
        }
    };

    public WidgetWithPagination(int packSize, IGridItemsProvider<T> dataPaginator, IPaginatedItemsDisplayer<T> pg) {
        this.packSize = packSize;
        this.allItemsProvider = dataPaginator;
        this.itemsDisplayer = pg;

        //ww: wywołanie tej metody (private itd.) powoduje, że nie leci ostrzeżenie
        // o wyciekającym this w konstruktorze
        setupItemsDisplayer();
    }

    private void setupItemsDisplayer() {
        itemsDisplayer.setPaginatedItemsProvider(this);
    }

    protected void navigateToPageAndBuild(int newPageNum) {
        gridWithPagination.clear();
        gridWithPagination.setWidth("100%");

        int filteredItemsCnt = filteredItems.size();

        if (newPageNum > 0 && newPageNum * packSize >= filteredItemsCnt) {
            newPageNum = (filteredItemsCnt - 1) / packSize;
        }
        if (newPageNum < 0) {
            newPageNum = 0;
        }
        this.currPageNum = newPageNum;
        this.displayedItems = getActualItemsToShow();

        if (filteredItemsCnt > packSize) {
            setPaginationControlsValues();
            gridWithPagination.add(paginationHeaderHp);
        }

        gridWithPagination.add(itemsDisplayer.build(displayedItems, currPageNum * packSize, filteredItems, allItems));
    }

    protected void setPaginationControlsValues() {
        paginationHeaderHp.clear();
        final int pageCnt = (filteredItems.size() + packSize - 1) / packSize;

        int firstPosNum = currPageNum * packSize;
        int lastPosNum = Math.min((currPageNum + 1) * packSize - 1, filteredItems.size() - 1);

        paginationHeaderHp.add(new Label(
                I18n.stronaZPozOdDoWszystkich.format(currPageNum + 1, pageCnt, firstPosNum + 1, lastPosNum + 1, filteredItems.size())) //
        //                I18n.strona2.get() /* I18N:  */+ ": " + (currPageNum + 1) + " " +I18n.z_noSingleLetter.get() /* I18N:  */+ " " + pageCnt + ", " +I18n.pozycjeOd.get() /* I18N:  */+ " "
        //                + (firstPosNum + 1) + " " +I18n.do_.get() /* I18N:  */+ " " + (lastPosNum + 1) + " (" +I18n.wszystkich.get() /* I18N:  */+ ": " + filteredItems.size() + ")")
        );

        FlowPanel btns = new FlowPanel();

        boolean prevEnabled = currPageNum > 0;
        if (true || prevEnabled) {
            BikCustomButton btnFirst = new BikCustomButton("|«", "labelLinkWWJoinedObjs", new IContinuation() {
                public void doIt() {
                    navigateToPageAndBuild(0);
                }
            });
            btnFirst.setTitle(I18n.pierwszaStrona.get() /* I18N:  */);

            btns.add(btnFirst);
            btnFirst.setEnabled(prevEnabled);

            BikCustomButton btnPrev = new BikCustomButton("«", "labelLinkWWJoinedObjs", new IContinuation() {
                public void doIt() {
                    navigateToPageAndBuild(currPageNum - 1);
                }
            });
            btnPrev.setTitle(I18n.poprzedniaStrona.get() /* I18N:  */);

            btns.add(btnPrev);
            btnPrev.setEnabled(prevEnabled);
        }

        boolean nextEnabled = currPageNum < pageCnt - 1;

        if (true || nextEnabled) {
            BikCustomButton btnNext = new BikCustomButton("»", "labelLinkWWJoinedObjs", new IContinuation() {
                public void doIt() {
                    navigateToPageAndBuild(currPageNum + 1);
                }
            });
            btnNext.setTitle(I18n.nastepnaStrona.get() /* I18N:  */);
            btns.add(btnNext);
            btnNext.setEnabled(nextEnabled);
            BikCustomButton btnLast = new BikCustomButton("»|", "labelLinkWWJoinedObjs", new IContinuation() {
                public void doIt() {
                    navigateToPageAndBuild(pageCnt - 1);
                }
            });
            btnLast.setTitle(I18n.ostatniaStrona.get() /* I18N:  */);
            btns.add(btnLast);
            btnLast.setEnabled(nextEnabled);
        }

        paginationHeaderHp.add(btns);
        paginationHeaderHp.setCellHorizontalAlignment(btns, HasHorizontalAlignment.ALIGN_RIGHT);
    }

//    protected HorizontalPanel buildPaginationControls() {
//        setPaginationControlsValues();
//        return paginationHeaderHp;
//    }
//    protected void refreshPaginWidget() {
//        dataChanged();
//    }
//    protected Widget internalBuildGrid() {
//        return ;
//    }
//    protected void dataChanged() {
//        navigateToPageAndBuild(allItems, currPageNum);
//    }
    protected List<T> getActualItemsToShow() {

        final List<T> itemsToShow = new ArrayList<T>();
        int left = filteredItems.size() <= packSize ? 0 : packSize * currPageNum;
        int right = filteredItems.size() <= packSize * (currPageNum + 1) ? filteredItems.size() : packSize * (currPageNum + 1);

        for (int i = left; i < right; i++) {
            itemsToShow.add(filteredItems.get(i));
        }
        return itemsToShow;
    }

    public void refetchItems(final boolean resetPosition) {
        currPageNum = resetPosition ? 0 : currPageNum;
        allItemsProvider.refetchItems();
    }

    protected void navigateToCurrentOrHomePageAndDisplay(boolean resetPosition) {
        navigateToPageAndBuild(resetPosition ? 0 : currPageNum);
    }

    public void recalcItemsToDisplay(boolean resetPosition) {
        allItemsProvider.itemsChanged();
        navigateToCurrentOrHomePageAndDisplay(resetPosition);
    }

    public void itemsChanged() {
        allItemsProvider.itemsChanged();
    }
    //----????

    protected Widget buildGrid() {
        navigateToPageAndBuild(0);
        return gridWithPagination;
    }
    protected VerticalPanel mainJoinedObjVl;
    protected VerticalPanel mainVp;
    private VerticalPanel vp;
    private FlowPanel addBtnJoinedFp;
    private ScrollPanel scPanel;

    public void displayData(List<T> filteredItems, List<T> allItems) {

        this.allItems = allItems;
        this.filteredItems = filteredItems;
        vp.clear();
        if (BaseUtils.isCollectionEmpty(filteredItems)) {
            vp.setWidth("100%");
            mainJoinedObjVl.setStyleName("Notes" /* i18n: no:css-class */ + "-mainNoteVp");
            mainVp.setStyleName("Joined" /* I18N: no */ + "-mainJoinedVp");
        } else {
            vp.add(buildGrid());
        }
    }

    public Widget buildWidgets() {
        mainJoinedObjVl = new VerticalPanel();
        mainVp = new VerticalPanel();
        vp = new VerticalPanel();
        addBtnJoinedFp = new FlowPanel();
        scPanel = new ScrollPanel();
        mainJoinedObjVl.setWidth("100%");
        vp.setWidth("100%");
        vp.addStyleName("obj" /* I18N: no */ + "-innerObjEntriesVp");
        mainJoinedObjVl.addStyleName("RelatedObj");
        mainJoinedObjVl.add(vp);
        scPanel.add(mainJoinedObjVl);
        scPanel.setHeight("100%");
        mainVp.add(scPanel);
        mainVp.add(addBtnJoinedFp);
        mainVp.setWidth("100%");

        return mainVp;
    }
}
