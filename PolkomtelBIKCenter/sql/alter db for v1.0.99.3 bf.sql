﻿exec sp_check_version '1.0.99.3';
go

alter table bik_attribute
	add is_deleted int not null default 0
go

alter table bik_attr_def
	add is_deleted int not null default 0
go

alter table bik_attribute_linked
	add is_deleted int not null default 0
go


---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
declare @idx_name sysname
select @idx_name = i.name
from sys.indexes i
inner join sys.index_columns ic on i.index_id = ic.index_id and ic.object_id = i.object_id
inner join sys.columns c on ic.object_id = c.object_id and ic.column_id = c.column_id
inner join sys.objects o on i.object_id = o.object_id
where o.name = 'bik_attribute' and c.name = 'name'
--exec ('drop index ' + @idx_name + ' on bik_searchable_attr') -- tak zrobimy dla indeksu
exec ('alter table bik_attribute drop constraint ' + @idx_name)
go


alter table [bik_attribute]
	add constraint UQ_attr_def_id_node_kind_id   unique(attr_def_id, node_kind_id)
go

alter table [bik_attr_def]
	add constraint UQ_bik_attr_def_name  unique(name)
go

alter table [bik_attr_category]
	add constraint UQ_bik_attr_category_name  unique(name)
go

exec sp_update_version '1.0.99.3', '1.0.99.4';
go