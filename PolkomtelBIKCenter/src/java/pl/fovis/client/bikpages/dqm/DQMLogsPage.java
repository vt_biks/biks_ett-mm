/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.dqm;

import java.util.List;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.LogsPageBase;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.client.entitydetailswidgets.dqm.DQMLogBeanExtractor;
import pl.fovis.common.dqm.DQMLogBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class DQMLogsPage extends LogsPageBase<DQMLogBean> {

    @Override
    public String getId() {
        return "admin:dqm:logs";
    }

    @Override
    public boolean isPageEnabled() {
//        return BIKClientSingletons.availableConnectors().contains(PumpConstants.SOURCE_NAME_DQM) && BIKClientSingletons.isSysAdminLoggedIn();
        return BIKClientSingletons.isConnectorAvailable(PumpConstants.SOURCE_NAME_DQM) && super.isPageEnabled()/*BIKClientSingletons.isSysAdminLoggedInEx(getId())*/;
    }

    @Override
    protected void callServiceToGetData(StandardAsyncCallback<Pair<Integer, List<DQMLogBean>>> callback) {
//        Integer.parseInt("I_AM_NOT_A_NUMBER");
        BIKClientSingletons.getDQMService().getDQMLogs(pwh.pageNum, pwh.pageSize, callback);
    }

    @Override
    protected NodeAwareBeanExtractorBase<DQMLogBean> getDataExtractor() {
        return new DQMLogBeanExtractor(false);
    }

    @Override
    protected String getHeaderString() {
        return I18n.logiZOstatnichWykonanTestow.get() /* I18N:  */;
    }
}
