	exec sp_drop_pk_col 'bik_active_directory', 'id'
	exec sp_drop_pk_col 'bik_app_prop', 'id'
	exec sp_drop_pk_col 'bik_news_readed_user', 'id'
	exec sp_drop_pk_col 'bik_tutorial_readed_user', 'id'
	exec sp_drop_pk_col 'bik_sapbw_area', 'id'
	exec sp_drop_pk_col 'bik_sapbw_provider', 'id'
	exec sp_drop_pk_col 'bik_statistic', 'id'
	exec sp_drop_pk_col 'bik_sapbw_flow', 'id'
	exec sp_drop_pk_col 'bik_sapbw_query', 'id'
	exec sp_drop_pk_col 'bik_statistic_dict', 'id'
	exec sp_drop_pk_col 'bik_home_page_hint', 'id'
	exec sp_drop_pk_col 'bik_sapbw_object', 'id'
	exec sp_drop_pk_col 'bik_user_right', 'id'
	exec sp_drop_pk_col 'bik_sapbo_report_extradata', 'id'
	exec sp_drop_pk_col 'bik_sapbo_user', 'id'
	exec sp_drop_pk_col 'bik_sapbo_schedule', 'id'
	exec sp_drop_pk_col 'bik_dqc_test_parameters', 'id'
	exec sp_drop_pk_col 'bik_specialist', 'id'
	exec sp_drop_pk_col 'bik_oracle_index', 'id'
	exec sp_drop_pk_col 'bik_sapbo_connection_to_db', 'id'
	exec sp_drop_pk_col 'bik_search_hint', 'id'
	exec sp_drop_pk_col 'bik_node_name_chunk', 'id'
	exec sp_drop_pk_col 'bik_mssql', 'id'


--	exec sp_drop_pk_col 'bik_data_load_log_details add constraint pk_bik_data_load_log_details primary key (id)
--	exec sp_drop_pk_col 'bik_joined_objs add constraint pk_bik_joined_objs primary key (id)

	alter table bik_data_load_log_details drop constraint pk_bik_data_load_log_details
	alter table bik_joined_objs drop constraint pk_bik_joined_objs
