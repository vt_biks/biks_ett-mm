/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.fovis.common.dqm.DataQualityTestBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public abstract class AddOrEditDQMTestDialogBase extends BaseActionOrCancelDialog {

    protected DataQualityTestBean bean;
    protected IParametrizedContinuation<DataQualityTestBean> saveCont;
    protected Set<String> alreadyAddedTests;
    protected Pair<Set<String>, Map<Integer, ConnectionParametersDQMConnectionsBean>> result;
    protected boolean isNewTest;
    protected TextBox tbxName;
    protected TextArea tbxDescription;
//    protected TextBox tbxLongName;
    protected TextArea tbxBenchmarkDefinition;
    protected TextBox tbxSamplingMethod;
    protected TextArea tbxAdditionInformation;
    protected TextArea tbxScopeOfData;
    protected TextBox tbxErrorThreshold;
    protected ListBox lbxOneSource;
    protected int nodeId;
//    protected Map<Integer, SourceType> indexToSourceTypeMap;
//    protected Map<Integer, Integer> indexToConnectionIdMap;
    protected DQMWidgetConfigMultiSourceBase multiWidget;
    protected DQMWidgetConfigConnectionSource connectionWidget;
    protected RadioButton oneSourceTest;
    protected RadioButton manySourceTest;
    protected VerticalPanel sourceWidget;
    protected VerticalPanel testTypePanel;
    protected FlexTable scopePanel;
    protected HorizontalPanel sourcePanel;
//    protected boolean useDqmConnectionMode = BIKClientSingletons.useDqmConnectionMode();

    public AddOrEditDQMTestDialogBase() {
    }

    @Override
    protected String getDialogCaption() {
        return isNewTest ? I18n.dodajTest.get() : I18n.edytujTest.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return isNewTest ? I18n.dodaj.get() : I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {
        VerticalPanel mainPanel = new VerticalPanel();

        testTypePanel = new VerticalPanel();
        // init dla multi
//        multiWidget = new DQMWidgetConfigMultiSourceForDatabases(result.v2);
//
//        multiWidget.getWidget(nodeId);

        oneSourceTest = new RadioButton("TEST", I18n.testJednozrodlowy.get());
        oneSourceTest.setValue(true);
        oneSourceTest.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetVisibility(!event.getValue());

            }
        });
        oneSourceTest.addStyleName("radioButtonBlack");
        manySourceTest = new RadioButton("TEST", I18n.testWielozrodlowy.get());
        manySourceTest.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetVisibility(event.getValue());
            }
        });
        manySourceTest.addStyleName("radioButtonBlack");
        testTypePanel.add(oneSourceTest);
        testTypePanel.add(manySourceTest);
        mainPanel.add(testTypePanel);
        mainPanel.add(new HTML("<hr/>"));
        // test attrs
        FlexTable northPanel = new FlexTable();
        northPanel.setWidget(0, 0, addLabel(I18n.nazwaTestu.get()));
//        northPanel.setWidget(1, 0, addLabel(I18n.nazwaBiznesowa.get()));
        northPanel.setWidget(1, 0, addLabel(I18n.opisTestu.get()));
        northPanel.setWidget(2, 0, addLabel(I18n.definicjaTestu.get()));
        northPanel.setWidget(3, 0, addLabel(I18n.czestotliwoscUruchamianiaTest.get()));
        northPanel.setWidget(4, 0, addLabel(I18n.sposobUruchamianiaTestu.get()));
        northPanel.setWidget(5, 0, addLabel(I18n.progBledu.get()));
        northPanel.setWidget(0, 1, tbxName = new TextBox());
//        northPanel.setWidget(1, 1, tbxLongName = new TextBox());
        northPanel.setWidget(1, 1, tbxDescription = new TextArea());
        northPanel.setWidget(2, 1, tbxBenchmarkDefinition = new TextArea());
        northPanel.setWidget(3, 1, tbxSamplingMethod = new TextBox());
        northPanel.setWidget(4, 1, tbxAdditionInformation = new TextArea());
        northPanel.setWidget(5, 1, tbxErrorThreshold = new TextBox());
        tbxName.setWidth("400px");
//        tbxLongName.setWidth("400px");
        tbxDescription.setWidth("400px");
        tbxDescription.setHeight("50px");
        tbxBenchmarkDefinition.setWidth("400px");
        tbxBenchmarkDefinition.setHeight("50px");
        tbxSamplingMethod.setWidth("400px");
        tbxAdditionInformation.setWidth("400px");
        tbxAdditionInformation.setHeight("20px");
        tbxErrorThreshold.setWidth("400px");
        northPanel.setWidth("300px");
        // scope panel
        scopePanel = new FlexTable();
        scopePanel.setWidget(0, 0, addLabel(I18n.zakresDanych.get()));
        scopePanel.setWidget(0, 1, tbxScopeOfData = new TextArea());
        tbxScopeOfData.setWidth("400px");
        tbxScopeOfData.setHeight("50px");
        scopePanel.setWidth("300px");
        // conf params
        sourceWidget = new VerticalPanel();
        sourceWidget.setWidth("582px");
        VerticalPanel southPanel = new VerticalPanel();
        sourcePanel = new HorizontalPanel();
//        southPanel.add(testTypePanel);
//        southPanel.add(new HTML("<hr/>"));
//            sourcePanel.add(addLabel(I18n.konektor.get()));
//            lbxOneSource = new ListBox();
//            int index = 0;
//            for (SourceType value : SourceType.values()) {
//                lbxOneSource.addItem(value.getName());
//                indexToSourceTypeMap.put(index++, value);
//                value.getWidget().getWidget(nodeId);
//            }
//            lbxOneSource.addChangeHandler(new ChangeHandler() {
//
//                @Override
//                public void onChange(ChangeEvent event) {
//                    setSelectedSourceWidget();
//                    cleanWidgets();
//                }
//            });
//            sourcePanel.add(lbxOneSource);

        addSourcePanel();
        southPanel.add(sourcePanel);
        //
        southPanel.add(sourceWidget);
        mainPanel.add(northPanel);
        mainPanel.add(scopePanel);
        mainPanel.add(new HTML("<hr/>"));
        southPanel.setWidth("582px");
        mainPanel.add(southPanel);
        main.add(mainPanel);
        // init
        tbxErrorThreshold.setText("0.0");
        addToSourceWidget();
//            sourceWidget.add(SourceType.Oracle.getWidget().getWidget(nodeId));
        // fill
        fillWidgetsWithData();
    }

    protected abstract void addSourcePanel();

    protected abstract void addToSourceWidget();

//    protected void setSelectedSourceWidget() {
//        setSelectedSourceWidget(getSelectedSource().getWidget().getWidget(nodeId));
//    }
    protected abstract void setWidgetVisibility(boolean isMulti);
//    {
//        sourcePanel.setVisible(!isMulti);
//        scopePanel.setVisible(!isMulti);

//        multiWidget.getWidget(nodeId).setVisible(isMulti);
//
//        if (!isMulti) {
//                setSelectedSourceWidget();
//        } else {
//            setSelectedSourceWidget(multiWidget.getWidget(nodeId));
//        }
//    }
    protected void setSelectedSourceWidget(Widget widget) {
        sourceWidget.clear();
        sourceWidget.add(widget);
    }

    protected static Label addLabel(String text) {
        Label label = new Label(text);
        label.setWidth("170px");
        return label;
    }

    protected static PushButton createDeleteButton(ClickHandler ch, String iconPath) {
        PushButton btnDelete = new PushButton(new Image(iconPath));
        btnDelete.addClickHandler(ch);
        Style style = btnDelete.getElement().getStyle();
        style.setMargin(0, Style.Unit.PX);
        style.setPadding(0, Style.Unit.PX);
        GWTUtils.setStyleAttribute(btnDelete.getElement(), "background" /* I18N: no */, "none" /* I18N: no */);
        style.setBorderWidth(0, Style.Unit.PX);
        return btnDelete;
    }

    @Override
    protected boolean doActionBeforeClose() {
        if (alreadyAddedTests.contains(tbxName.getText().trim())) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.istniejeTakiTest.get(), I18n.blad.get(), null);
            return false;
        }
        if (BaseUtils.isStrEmptyOrWhiteSpace(tbxName.getText().trim())) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.nazwaNieMozeBycPusta.get(), I18n.blad.get(), null);
            return false;
        }
        if (BaseUtils.tryParseDouble(tbxErrorThreshold.getText(), null) == null) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.progBleduJestNieprawidlowy.get(), I18n.blad.get(), null);
            return false;
        }
        // sprawdzenie sourceWidgetu
        return getSelectedWidget().validateWidgetsBeforeSave();
    }

    @Override
    protected void doAction() {
//        SourceType selectedSource = getSelectedSource();
        DataQualityTestBean newBean = new DataQualityTestBean();
        newBean.id = bean != null ? bean.id : null;
        newBean.name = tbxName.getText();
        newBean.description = tbxDescription.getText();
//        newBean.sourceName = !useDqmConnectionMode && oneSourceTest.getValue() ? getSelectedSource().getSourceName() : null;
//        newBean.longName = tbxLongName.getText();
        newBean.benchmarkDefinition = tbxBenchmarkDefinition.getText();
        newBean.additionalInformation = tbxAdditionInformation.getText();
        newBean.samplingMethod = tbxSamplingMethod.getText();
        newBean.dataScope = tbxScopeOfData.getText();
        newBean.errorThreshold = BaseUtils.tryParseDouble(tbxErrorThreshold.getText(), 0.0) / 100;
        // fill bean dla source widgetu
        setOtherValueInBean(newBean);
        getSelectedWidget().fillBean(newBean);
        saveCont.doIt(newBean);
    }

    protected abstract void setOtherValueInBean(DataQualityTestBean bean);

//    protected SourceType getSelectedSource() {
//        return indexToSourceTypeMap.get(lbxOneSource.getSelectedIndex());
//    }
    public void buildAndShowDialog(int nodeId, DataQualityTestBean bean, Pair<Set<String>, Map<Integer, ConnectionParametersDQMConnectionsBean>> result, IParametrizedContinuation<DataQualityTestBean> saveCont) {
        this.nodeId = nodeId;
        this.bean = bean;
        this.isNewTest = bean == null;
        this.result = result;
        this.alreadyAddedTests = result.v1;
        if (!isNewTest) {
            this.alreadyAddedTests.remove(bean.name);
        }
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }

    protected void cleanWidgets() {
        getSelectedWidget().cleanWidgets();
    }

    protected abstract IDQMWidgetConfig getSelectedWidget();

    protected void fillWidgetsWithData() {
        if (bean != null) {
            selectSourceLbxBySourceName(/*bean.sourceName*/);
            tbxName.setText(bean.name);
            tbxDescription.setText(bean.description);
//            tbxLongName.setText(bean.longName);
            tbxSamplingMethod.setText(bean.samplingMethod);
            tbxAdditionInformation.setText(bean.additionalInformation);
            tbxScopeOfData.setText(bean.dataScope);
            tbxBenchmarkDefinition.setText(bean.benchmarkDefinition);
            String errorAsString = "";
            try {
                errorAsString = bean.errorThreshold != null ? BaseUtils.doubleToString(bean.errorThreshold * 100, 2) : "0";
            } catch (Exception e) {
                errorAsString = "0";
            }
            tbxErrorThreshold.setText(errorAsString);
            // fill All source widgets
            boolean isSingle = BaseUtils.isCollectionEmpty(bean.multiSources);
            manySourceTest.setValue(!isSingle);
            oneSourceTest.setValue(isSingle);
            setWidgetVisibility(!isSingle);
            getSelectedWidget().fillWidgets(bean);
            testTypePanel.setVisible(false); // not visible for editing
        }
    }

    protected void selectSourceLbxBySourceName() {
        lbxOneSource.setSelectedIndex(getSelectedIdx());

        cleanWidgets();
    }

    protected abstract int getSelectedIdx();

    @Override
    protected void onEnterPressed() {
        // NO ACTION
    }

//    enum SourceType {
//
//        Oracle(PumpConstants.SOURCE_NAME_ORACLE, PumpConstants.SOURCE_NAME_ORACLE, new DQMWidgetConfigOracle(), BIKConstants.TREE_CODE_ORACLE), SQLServer(PumpConstants.SOURCE_NAME_MSSQL, PumpConstants.SOURCE_NAME_MSSQL, new DQMWidgetConfigMSSQL(), BIKConstants.TREE_CODE_MSSQL), PostgreSQL(PumpConstants.SOURCE_NAME_POSTGRESQL, PumpConstants.SOURCE_NAME_POSTGRESQL, new DQMWidgetConfigPostgreSQL(), BIKConstants.TREE_CODE_POSTGRES), Profile(PumpConstants.SOURCE_NAME_PROFILE, PumpConstants.SOURCE_NAME_PROFILE, new DQMWidgetConfigProfile(), BIKConstants.TREE_CODE_PROFILE)/*, Multi("Multi", null, new DQMWidgetConfigMultiSource(), null)*/;
//
//        private String sourceName;
//        private IDQMWidgetConfig widget;
//        private String name;
//        private String treeCode;
//
//        SourceType(String name, String sourceName, IDQMWidgetConfig widget, String treeCode) {
//            this.name = name;
//            this.sourceName = sourceName;
//            this.widget = widget;
//            this.treeCode = treeCode;
//        }
//
//        public String getSourceName() {
//            return sourceName;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public IDQMWidgetConfig getWidget() {
//            return widget;
//        }
//
//        public String getTreeCode() {
//            return treeCode;
//        }
//    }
}
