/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.lisa;

import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaConstants;
import pl.fovis.common.lisa.LisaDictionaryBean;
import pl.fovis.common.lisa.LisaUtils;
import pl.fovis.common.lisa.TeradataException;
import pl.fovis.common.lisa.TreeNodeChangeHierarchy;
import pl.fovis.server.BOXIServiceImpl;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pku
 */
// i18n-default: no
public class DictionaryGenerator {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    private final AuthorizedTeradataConnection tConnection;
    private final LisaTeradataQueryGenerator queryGen;
    private final INamedSqlsDAO<Object> adhocDao;
    private final BOXIServiceImpl boxiService;
    private Integer treeId;
    private final static int INSERT_GROUP_SIZE = 50;
    private final static String SEPARATOR = LisaConstants.OBJ_ID_SEPARATOR;
    private String teradataTableName;

    public DictionaryGenerator(AuthorizedTeradataConnection tConnection, LisaTeradataQueryGenerator queryGen,
            INamedSqlsDAO<Object> adhocDao, BOXIServiceImpl boxiService) {
        this.tConnection = tConnection;
        this.queryGen = queryGen;
        this.adhocDao = adhocDao;
        this.boxiService = boxiService;
    }

    public String generate(LisaDictionaryBean b) {
        String result = null;
        try {
            result = importTeradataDictionary(b);
        } catch (Exception e) {
            if (e instanceof TeradataException) {
                throw new TeradataException(e.getMessage());
            } else {
                throw new RuntimeException(e);
            }
        }
        return result;
    }

    private String importTeradataDictionary(LisaDictionaryBean b) throws Exception {
        this.teradataTableName = b.dbName;

        // looking for dictionary table in TeraData
        testTablesExistence(b);

        String result = importDictionaryMetadata(b.lisaId, b.dbName, (b.dbNameReal != null && b.dbNameReal.length() > 0) ? b.dbNameReal : b.dbName);

        this.treeId = execNamedQuerySingleValAsInteger("getTreeId", false, null, BIKConstants.TREE_CODE_DICTIONARIES_DWH);
//        String dictionaryCode = fetchTeradataDictionaryCode(teradataTableName);
        Integer dictionaryNodeId = createDictionaryNode(teradataTableName, b);

//        boxiService.addOrUpdateSpidHistoryForCurrentUser(); ?
        adhocDao.execNamedCommand("initLisaBranchIdsAndVerticalize", treeId, teradataTableName/*, boxiService.getLoggedUserId()*/);
        if (!BaseUtils.isStrEmptyOrWhiteSpace(b.dbRelTable)/*b.dbRelTable != null && !b.dbRelTable.trim().equals("")*/) {
            b.dbName = LisaUtils.deleteInstanceId(b.dbName);
            adhocDao.execNamedCommand("putNewLisaExtradata", dictionaryNodeId, b);
            insertCategoryTypesNodes(b.dbRelTable, dictionaryNodeId);
            insertCategoryNodes(dictionaryNodeId);
        }
        adhocDao.execNamedCommand("addDictSubTree2Instance", dictionaryNodeId, b.lisaId);
        //adhocDao.execNamedCommand("initLisaBranchIdsAndVerticalize", treeId, teradataTableName);
        return result;
    }

    private Integer createDictionaryNode(String teradataTableName, LisaDictionaryBean b) {
        final Integer rootId = execNamedQuerySingleValAsInteger("getLisaNodeIdForDictionariesRoot", false, null, treeId, LisaConstants.ROOT_OBJ_ID); // main root id, serves as parent for new dictionary
        final Integer dictionaryNodeKindId = execNamedQuerySingleValAsInteger("getNodeKindIdByCode", false, null, BIKConstants.NODE_KIND_LISA_DICTIONARY);
        final Integer dictId = execNamedQuerySingleValAsInteger("getLisaNodeIdForObjId", true, null, treeId, teradataTableName, b.lisaId);
        if (dictId != null) {
            return dictId;
        }
        TreeNodeBean node = new TreeNodeBean();
        node.parentNodeId = rootId;
        node.nodeKindId = dictionaryNodeKindId;
        node.name = b.displayName;
        node.treeId = treeId;
        node.objId = teradataTableName;
        return insertSingleNode(node);
    }

    private void insertCategoryTypesNodes(String dictionaryRelTable, Integer dictionaryNodeId) {
        List<Map<String, Object>> categoriesTypes = fetchCategoriesTypes(dictionaryRelTable, dictionaryNodeId);
        final Integer categorizationNodeKindId = execNamedQuerySingleValAsInteger("getNodeKindIdByCode", false, null, BIKConstants.NODE_KIND_LISA_CATEGORIZATION);
        List<TreeNodeBean> categoryTypeNodes = new ArrayList<TreeNodeBean>();
        for (Map<String, Object> categoryType : categoriesTypes) {
            String idTypKateg = getSafeStringFromMap(categoryType, LisaConstants.CATEGORIZATION_TYPE_ID);
            TreeNodeBean node = new TreeNodeBean();
            node.parentNodeId = dictionaryNodeId;
            node.nodeKindId = categorizationNodeKindId;
            node.name = getSafeStringFromMap(categoryType, LisaConstants.CATEGORIZATION_TYPE_NAME) + " (" + idTypKateg + ")";
            node.descr = getSafeStringFromMap(categoryType, LisaConstants.CATEGORIZATION_TYPE_DESCR);
            node.treeId = treeId;
            node.objId = getFullCategoryTypeObjId(idTypKateg);
            categoryTypeNodes.add(node);
        }
        insertManyNodesInGroups(categoryTypeNodes);
    }

    protected Integer execNamedQuerySingleValAsInteger(String qryName, boolean allowNoResult,
            String optColumnName, Object... args) {
        return adhocDao.sqlNumberToInt(execNamedQuerySingleVal(qryName, allowNoResult, optColumnName,
                args));
    }

    protected <V> V execNamedQuerySingleVal(String qryName, boolean allowNoResult,
            String optColumnName, Object... args) {
        return BeanConnectionUtils.<V>execNamedQuerySingleVal(adhocDao, qryName, allowNoResult, optColumnName,
                args);
    }

    private void insertManyNodesInGroups(List<TreeNodeBean> nodes) {
        int b = 0, e;
        int size = nodes.size();
        while (b < size) {
            e = b + INSERT_GROUP_SIZE;
            if (e > size) {
                e = size;
            }
            adhocDao.execNamedCommand("insertGroupOfBikNodes", nodes.subList(b, e));
            b += INSERT_GROUP_SIZE;
        }
    }

    private Integer insertSingleNode(TreeNodeBean node) {
        return execNamedQuerySingleValAsInteger("insertBikNodeRaw", true, "id", node);
    }

    private List<Map<String, Object>> fetchCategoriesTypes(String dictionaryRelTable, Integer dictionaryNodeId) {
//        List<Map<String, Object>> categoriesTypeDefs = tConnection.execQry(queryGen.getTeradataCategoryTypeDefQuery(dictionaryCode));
//        List<Map<String, Object>> categoriesTypes = new ArrayList<Map<String, Object>>();
//        List<String> existingCategoiesTypes = getExistingCategoriesTypes(dictionaryNodeId);
//        for (Map<String, Object> categoriesTypeDef : categoriesTypeDefs) {
//            String categoryTypeDefId = getSafeStringFromMap(categoriesTypeDef, LisaTeradataQueryGenerator.CATEGORY_TYPE_DEF_ID_COL);
//            categoriesTypes.addAll(tConnection.execQry(queryGen.getTeradataCategoryTypeQuery(categoryTypeDefId, existingCategoiesTypes)));
//        }
        List<Map<String, Object>> categoriesTypes = new ArrayList<Map<String, Object>>();
        List<String> existingCategoriesTypes = getExistingCategoriesTypes(dictionaryNodeId);
        categoriesTypes.addAll(tConnection.execQry(queryGen.getTeradataCategoryTypeQuery(dictionaryRelTable, existingCategoriesTypes)));

        return categoriesTypes;
    }

    private String fetchTeradataDictionaryCode(String teradataTableName) throws Exception {
        List<Map<String, Object>> dictionary = tConnection.execQry(String.format(queryGen.getTeradataDictionaryQuery(teradataTableName)));

        if (dictionary.size() != 1) {
            throw new Exception("Znalazłem liczbę słowników o nazwie " + teradataTableName + " różną od 1");
        }
        return getSafeStringFromMap(dictionary.get(0), LisaConstants.DICTIONARY_ID_COL);
    }

    private void insertCategoryNodes(Integer dictionaryNodeId) {
        Map<String, Integer> categoriesTypes = getCategoriesTypesMap(dictionaryNodeId);
        List<Map<String, Object>> categories = fetchCategories(categoriesTypes);
        final Integer categoryNodeKindId = execNamedQuerySingleValAsInteger("getNodeKindIdByCode", false, null, BIKConstants.NODE_KIND_LISA_CATEGORY);

        List<TreeNodeBean> categoriesNodes = new ArrayList<TreeNodeBean>();
        List<TreeNodeChangeHierarchy> categoriesNodesToHierarchy = new ArrayList<TreeNodeChangeHierarchy>();
//        String unassignedName = ((String) execNamedQuerySingleVal("getAdminSingleValue", false, null, LisaConstants.PARAM_UNASSIGNED_CATEGORY_NAME)).trim().toLowerCase();
        for (Map<String, Object> category : categories) {
            TreeNodeBean node = new TreeNodeBean();
            node.parentNodeId = categoriesTypes.get(getSafeStringFromMap(category, LisaConstants.CATEGORIZATION_TYPE_ID));
            if (node.parentNodeId == null) {
                continue;
            }
            node.nodeKindId = categoryNodeKindId;
            node.name = getSafeStringFromMap(category, LisaConstants.CATEGORY_NAME);
            node.treeId = treeId;
            String objId = getSafeStringFromMap(category, LisaConstants.CATEGORY_ID);
            if (isNull(objId)) {
                continue;
            }
            node.objId = teradataTableName + SEPARATOR + getSafeStringFromMap(category, LisaConstants.CATEGORIZATION_TYPE_ID)
                    + SEPARATOR + objId;
            node.visualOrder = objId.equals("0") ? -1 : 0;
            categoriesNodes.add(node);

            //zbieranie danych do hierarchizacji
            Integer prarentKategId = getIntFromMap(category, "parent_kateg_id");

            if (prarentKategId != null) {
                TreeNodeChangeHierarchy nodeToChange = new TreeNodeChangeHierarchy();
                nodeToChange.actualParentId = node.parentNodeId;
                nodeToChange.objId = node.objId;
                nodeToChange.objIdNewParent = teradataTableName + SEPARATOR + getSafeStringFromMap(category, LisaConstants.CATEGORIZATION_TYPE_ID)
                        + SEPARATOR + prarentKategId;
                categoriesNodesToHierarchy.add(nodeToChange);
            }
        }

        insertManyNodesInGroups(categoriesNodes);
//        boxiService.addOrUpdateSpidHistoryForCurrentUser();
        adhocDao.execNamedCommand("initLisaBranchIdsAndVerticalize", treeId, teradataTableName/*, boxiService.getLoggedUserId()*/);
        adhocDao.execNamedCommand("changeHierarchyOfNodes", categoriesNodesToHierarchy);

    }

    private Map<String, Integer> getCategoriesTypesMap(Integer dictionaryNodeId) {
        List<TreeNodeBean> categoriesTypes = adhocDao.createBeansFromNamedQry("getCategoriesTypes", TreeNodeBean.class, dictionaryNodeId);
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (TreeNodeBean pair : categoriesTypes) {
            map.put(getCoreCategoryTypeObjId(pair.objId), pair.id);
        }
        return map;
    }

    private String getSafeStringFromMap(Map<String, Object> map, String key) {
        Object o = map.get(key);
        if (o == null) {
            o = map.get(key.toUpperCase());
        }
        if (o == null) {
            return null;
        }
        return LisaServerUtils.transcodeFromLatin1(o.toString());
    }

    private void addArtificialUNASSIGNEDCategories(Integer dictNodeId, Integer categoryNodeKindId, List<TreeNodeBean> categoriesNodes) {
        List<TreeNodeBean> existingTypesWithoutUnassigned = adhocDao.createBeansFromNamedQry("getLisaCategoriesTypesWithoutUnassigned", TreeNodeBean.class, treeId, dictNodeId);
        for (TreeNodeBean b : existingTypesWithoutUnassigned) {
            TreeNodeBean node = new TreeNodeBean();
            node.parentNodeId = b.id;
            node.nodeKindId = categoryNodeKindId;
            node.name = execNamedQuerySingleVal("getAdminSingleValue", false, null, LisaConstants.PARAM_UNASSIGNED_CATEGORY_NAME);
            node.treeId = treeId;
            node.objId = b.objId + SEPARATOR + LisaConstants.UNASSIGNED_CATEGORY_OBJ_ID_SUFFIX;
            node.isBuiltIn = true;
            categoriesNodes.add(node);

        }
    }

    private List<String> getExistingCategoriesTypes(Integer dictionaryNodeId) {
        List<TreeNodeBean> categoriesTypes = adhocDao.createBeansFromNamedQry("getCategoriesTypes", TreeNodeBean.class, dictionaryNodeId);
        return BaseUtils.projectToList(categoriesTypes, new BaseUtils.Projector<TreeNodeBean, String>() {
            public String project(TreeNodeBean val) {
                return getCoreCategoryTypeObjId(val.objId);
            }
        });
    }

    private List<Map<String, Object>> fetchCategories(Map<String, Integer> categoriesTypes) {
        Collection<Integer> existingCategoriesTypesIds = categoriesTypes.values();
        List<String> existingCategories;
        if (existingCategoriesTypesIds.isEmpty()) {
            existingCategories = new ArrayList<String>();
        } else {
            existingCategories = execNamedQuerySingleColAsList("getLisaCategoriesForTypes", null, treeId, existingCategoriesTypesIds,
                    SEPARATOR);
        }
        List<Map<String, Object>> remoteCategories = tConnection.execQry(queryGen.getTeradataCategoriesQuery(categoriesTypes.keySet(), new ArrayList<String>()));

        return subtractLists(remoteCategories, existingCategories);

    }

    //zaklada, ze obie listy sa posortowane tak samo (wg id kategoryzacji i jako podrzedne kryterium  wg id kategorii)
    private List<Map<String, Object>> subtractLists(List<Map<String, Object>> remoteCategories,
            List<String> existingCategories) {
        if (existingCategories.isEmpty()) {
            return remoteCategories;
        }
        List<Map<String, Object>> toSave = new ArrayList<Map<String, Object>>();
        int localCategIndex = 0;
        int remoteCategIndex = 0;
        ObjIdCategoryDescriptor localId = new ObjIdCategoryDescriptor(existingCategories.get(0));
        ObjIdCategoryDescriptor remoteId;
        while (remoteCategIndex < remoteCategories.size()) {
            Map<String, Object> categ = remoteCategories.get(remoteCategIndex);
            remoteId = new ObjIdCategoryDescriptor(
                    getSafeStringFromMap(categ, LisaConstants.CATEGORIZATION_TYPE_ID),
                    getIntFromMap(categ, LisaConstants.CATEGORY_ID));
            while (localId.compareTo(remoteId) < 0 && localCategIndex < existingCategories.size()) {
                localId = new ObjIdCategoryDescriptor(existingCategories.get(++localCategIndex));
            }
            if (remoteId.compareTo(localId) < 0 || localCategIndex == existingCategories.size()) {
                toSave.add(remoteCategories.get(remoteCategIndex));
            }
            remoteCategIndex++;
        }
        return toSave;
    }

    private Integer getIntFromMap(Map<String, Object> map, String key) {
        Object o = map.get(key);
        if (o == null) {
            o = map.get(key.toUpperCase());
        }
        if (o == null) {
            return null;
        }
        return (Integer) (o);
    }

    private <V> List<V> execNamedQuerySingleColAsList(String qryName, String optColumnName, Object... args) {
        return BeanConnectionUtils.execNamedQuerySingleColAsList(adhocDao, qryName, optColumnName,
                args);
    }

    private String getFullCategoryTypeObjId(String coreObjId) {
        return teradataTableName + SEPARATOR + coreObjId;
    }

    private String getCoreCategoryTypeObjId(String fullObjId) {
        String[] a = fullObjId.split(SEPARATOR);
        return a[1];
    }

    private boolean isNull(String s) {
        return s == null || s.equalsIgnoreCase("null");
    }

    private void testTablesExistence(LisaDictionaryBean b) {
        try {
            tConnection.execQry(queryGen.getTeradataTableCheckQuery(b.dbName));
        } catch (Exception ex) {
            throw new TeradataException(I18n.brakWskazanejTabeliDlaSlownika.get());
        }
//        try {
//            tConnection.execQry(queryGen.getTeradataTableCheckQuery(b.dbRelTable));
//        } catch (Exception ex) {
//            throw new TeradataAuthorizationException(I18n.brakWskazanejTabeliRelacyjnej.get());
//        }
//        try {
//            tConnection.execQry(queryGen.getTeradataRefColCheckQuery(b.dbRelTable, b.columnRelName));
//        } catch (Exception ex) {
//            throw new TeradataAuthorizationException(I18n.blednaNazwaKolumnyRelacyjnej.get());
//        }
//        try {
//            tConnection.execQry(queryGen.getTeradataTableCheckQuery(b.dbRelTable));
//        } catch (Exception ex) {
//            throw new TeradataAuthorizationException(I18n.brakWskazanejTabeliRelacyjnej.get());
//        }
//        try {
//            tConnection.execQry(queryGen.getTeradataRefColCheckQuery(b.dbRelTable, b.columnRelName));
//        } catch (Exception ex) {
//            throw new TeradataAuthorizationException(I18n.blednaNazwaKolumnyRelacyjnej.get());
//        }
    }

    private String importDictionaryMetadata(Integer instanceId, String dictionaryNameOperational, String dictionaryNameReal) {
        String result = null;
        List<Map<String, Object>> columns = tConnection.execQry(queryGen.getTeradataDictionaryColumnsMetadataQuery(dictionaryNameReal));

        if (logger.isDebugEnabled()) {
            System.out.println("InstanceId = " + instanceId);
            System.out.println("dictionaryNameOperational = " + dictionaryNameOperational);
            System.out.println("dictionaryNameReal = " + dictionaryNameReal);
            System.out.print("columns[] = ");
            for (Map<String, Object> column : columns) {
                System.out.print(BaseUtils.safeToString(column.get(LisaConstants.METADATA_COLUMN_NAME)).trim() + " ");
            }
            System.out.println("");
        }
        Set<String> pk = getPrimaryKeys(dictionaryNameReal);
        if (pk.isEmpty()) {
            result = I18n.brakKluczaPodstawowego.get();
        }

        // deleting old metadata
        deleteOldMetadataForDictionary(instanceId, dictionaryNameOperational);

        for (Map<String, Object> column : columns) {
            String columnName = BaseUtils.safeToString(column.get(LisaConstants.METADATA_COLUMN_NAME)).trim();
            insertMetadataForColumn(instanceId, column, dictionaryNameOperational, pk.contains(columnName));
        }

        return result;
    }

    private void insertMetadataForColumn(Integer instanceId, Map<String, Object> column, String dictionaryName, boolean isMainKey) {
        String columnName = BaseUtils.safeToString(column.get(LisaConstants.METADATA_COLUMN_NAME));
        adhocDao.execNamedCommand("insertLisaDictionaryColumnMetadata",
                dictionaryName,//dictionaryName
                column.get(LisaConstants.METADATA_COLUMN_ID),//columnId
                column.get(LisaConstants.METADATA_COLUMN_ID),//sequence
                columnName.trim(), //columnName
                column.get(LisaConstants.METADATA_COLUMN_TYPE),//dataType
                isMainKey, //isMainKey
                column.get(LisaConstants.METADATA_COLUMN_LENGTH), //columnLength
                column.get(LisaConstants.METADATA_COLUMN_NULLABLE), //nullable
                column.get(LisaConstants.METADATA_COLUMN_DECIMALTOTALDIGITS), //decimalTotalDigits
                column.get(LisaConstants.METADATA_COLUMN_DECIMALFRACTIONALDIGITS),
                instanceId); //decimalFractionalDigits
    }

    private Set<String> getPrimaryKeys(String dictionaryName) {

        List<Map<String, Object>> pkColumns = tConnection.execQry(queryGen.getTeradataDictionaryPrimaryKeyColumnsQuery(dictionaryName));
        BaseUtils.Projector<Map<String, Object>, String> projector = new BaseUtils.Projector<Map<String, Object>, String>() {
            public String project(Map<String, Object> val) {
                return BaseUtils.safeToString(val.get(LisaConstants.METADATA_COLUMN_NAME)).trim();
            }
        };
        return BaseUtils.projectToSet(pkColumns, projector, true);
    }

    private void deleteOldMetadataForDictionary(Integer instanceId, String dictionaryName) {
        Integer dictNodeId = execNamedQuerySingleValAsInteger("getLisaDictionaryNodeID", true, "id", instanceId, dictionaryName);
        adhocDao.execNamedCommand("deleteLisaOldDictionaryMetadata", dictionaryName, dictNodeId);
    }

    private class ObjIdCategoryDescriptor {

        private Integer type;
        private Integer categ;

        public ObjIdCategoryDescriptor(String s) {
            try {
                String[] a = s.split(SEPARATOR);
                type = Integer.parseInt(a[1]);
                categ = Integer.parseInt(a[2]);
            } catch (IndexOutOfBoundsException e) {
                System.out.println(s);
            }
        }

        public ObjIdCategoryDescriptor(String type, Integer categ) {
            this.type = Integer.parseInt(type);
            this.categ = categ;
        }

        public int compareTo(ObjIdCategoryDescriptor d) {
            int firstLevel = type.compareTo(d.type);
            if (firstLevel != 0) {
                return firstLevel;
            }
            return categ.compareTo(d.categ);
        }
    }
}
