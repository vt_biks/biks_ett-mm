/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import pl.fovis.foxygwtcommons.ITreeBroker;

/**
 *
 * @author pmielanczuk
 */
public class TreeBrokerForMenuNodeBean implements ITreeBroker<MenuNodeBean, Integer> {

    public Integer getNodeId(MenuNodeBean n) {
        return n.id;
    }

    public Integer getParentId(MenuNodeBean n) {
        return n.parentNodeId;
    }
}
