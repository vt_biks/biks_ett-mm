/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.gwtservlet;

/**
 *
 * @author pmielanczuk
 */
public interface IRequestLoggingFoxyService {

    public boolean isServletRequestLoggingEnabled();

    public int insertServiceRequest(String methodName, String parameters, String ipAddr,
            String sessionId, String optExtraInfo);

    public void updateServiceRequest(int reqId, String optExtraInfo, String optExceptionMsg);
}
