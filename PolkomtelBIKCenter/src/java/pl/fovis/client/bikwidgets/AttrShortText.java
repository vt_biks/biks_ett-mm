/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author bfechner
 */
public class AttrShortText extends AttrWigetBase {

    protected String atrLinValue;
    protected TextBox tb = new TextBox();

    public AttrShortText(String atrLinValue) {
        this.atrLinValue = atrLinValue;
    }

    @Override
    public Widget getWidget() {
        tb.setText(atrLinValue);
        tb.setStyleName("addAttr");
        tb.setWidth(/*"800px"*/"100%");
        return tb;
    }

    @Override
    public String getValue() {
        return tb.getText();
    }

    @Override
    public int getWidgetHeight() {
        return 32;
    }

}
