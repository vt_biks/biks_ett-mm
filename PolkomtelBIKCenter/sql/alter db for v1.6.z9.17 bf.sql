﻿exec sp_check_version '1.6.z9.17';
go
	if not exists (select * from bik_app_prop where name='insertStatisticsForAllNodes')
	insert into bik_app_prop(name,val,is_editable)
    values ('insertStatisticsForAllNodes','false',0)
	go
	
	insert into bik_statistic_dict (counter_name,description)
	select code,name from bik_tree bt where 
	not exists (select * from bik_statistic_dict bs where bs.counter_name=bt.code /*and bs.description=bt.name*/)
	and code <>'TreeOfTrees'
    go     
	
exec sp_update_version '1.6.z9.17', '1.6.z9.18';
go