/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.foxygwtcommons;

/**
 *
 * @author wezyr
 */
public interface IObjectCriteriaTester<OBJ, CRIT> {

    public boolean meetsCriteria(OBJ obj, CRIT criteria);
}
