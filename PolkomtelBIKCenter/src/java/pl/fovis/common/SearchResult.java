package pl.fovis.common;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author wezyr
 */
public class SearchResult implements Serializable {

    public int nodeId;
    public String name;
    public String nodeKindCode;
    public String nodeKindCaption;
    public String treeName;
    public String treeCode;
    public int voteSum;
    public int voteCnt;
    public String branchIds;
    //public int ftsRank;
    public List<String> foundInAttrCaptions;
}
