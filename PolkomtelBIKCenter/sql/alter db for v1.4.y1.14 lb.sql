﻿----------------------------------------------------
----------------------------------------------------
----------------------------------------------------
--     dodanie nowego parametru dla LISY, przechowujacego nazwe wyswietlana dla kategorii grupujacych nieprzydzielone elementy
----------------------------------------------------
----------------------------------------------------
----------------------------------------------------
exec sp_check_version '1.4.y1.14';
go

insert into bik_admin (param, value) values('lisateradata.defaultUnassignedCategoryName', 'UNASSIGNED');
go

exec sp_update_version '1.4.y1.14', '1.4.y1.15';
go
