/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.FoxyFileUpload;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.dialogs.Dialogs;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author ctran
 */
public class ImportTreeWithLinksAndJoinedObjsDialog extends BaseActionOrCancelDialog {

    protected TreeBean treeBean;
    protected Map<Integer, ListBox> treesLbMap = new HashMap<Integer, ListBox>();
    protected Map<Integer, String> uploadedFilesMap = new HashMap<Integer, String>();
    protected Integer globalId = 0;
    protected FlexTable linesFt = new FlexTable();
    protected Set<String> tmpFiles = new HashSet<String>();
    protected Set<Integer> allTreeIds = new HashSet<Integer>();
    protected Map<Integer, String> bindingTreeIdWithFile = new HashMap<Integer, String>();
    protected HTML clientFileName4TreeLbl;
    protected HTML clientMetadataFileNameLbl;
    protected String serverFileName2ImportTree;
    protected String serverMetadataFileName;
    protected CheckBox isIncrementalChb;

    @Override
    protected String getDialogCaption() {
        return I18n.importDrzewa.get() + " " + treeBean.name;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {
        HorizontalPanel metadataHp = new HorizontalPanel();
        metadataHp.add(new Label(I18n.plikMetadata.get() + ":"));
        metadataHp.add(clientMetadataFileNameLbl = new HTML());
        PushButton importMetadataBtn = new PushButton(I18n.importuj.get());
        NewLookUtils.makeCustomPushButton(importMetadataBtn);
        importMetadataBtn.getElement().getStyle().setMarginLeft(6, Style.Unit.PX);
        importMetadataBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new FileUploadDialogSimple().buildAndShowDialog(new IParametrizedContinuation<FoxyFileUpload>() {

                    @Override
                    public void doIt(final FoxyFileUpload ffu) {
                        serverMetadataFileName = ffu.getServerFileName();
                        clientMetadataFileNameLbl.setHTML("<b>" + ffu.getClientFileName() + "</b>");
                    }
                }, Arrays.asList(BIKClientSingletons.allowedFileExtensionsList()));

            }
        });
        metadataHp.add(importMetadataBtn);
        main.add(metadataHp);

        HorizontalPanel treeHp = new HorizontalPanel();
        treeHp.add(new Label(I18n.drzewo.get() + ":"));
        treeHp.add(clientFileName4TreeLbl = new HTML());
        PushButton importTreeBtn = new PushButton(I18n.importuj.get());
        NewLookUtils.makeCustomPushButton(importTreeBtn);
        importTreeBtn.getElement().getStyle().setMarginLeft(6, Style.Unit.PX);
        importTreeBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new FileUploadDialogSimple().buildAndShowDialog(new IParametrizedContinuation<FoxyFileUpload>() {

                    @Override
                    public void doIt(final FoxyFileUpload ffu) {
                        serverFileName2ImportTree = ffu.getServerFileName();
                        clientFileName4TreeLbl.setHTML("<b>" + ffu.getClientFileName() + "</b>");
                    }
                }, Arrays.asList(BIKClientSingletons.allowedFileExtensionsList()));
            }
        });
        treeHp.add(importTreeBtn);
        main.add(treeHp);
        main.add(isIncrementalChb = new CheckBox(I18n.zasilaniePrzyrostowe.get()));

        main.add(new Label(I18n.importDowiazanLubPowiazan.get() + ":"));
        PushButton addBindedTreeBtn = new PushButton(I18n.dodaj.get());
        NewLookUtils.makeCustomPushButton(addBindedTreeBtn);
        main.add(addBindedTreeBtn);
        main.add(linesFt);

        addBindedTreeBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                createImportLinkedNodesPanel(globalId++);
            }
        });
//        createImportLinkedNodesPanel(globalId++);
        main.setWidth("600px");
    }

    @Override
    protected void doAction() {
        BIKClientSingletons.getService().importTreeWithLinkedAndJoinedObjs(treeBean.id, isIncrementalChb.getValue() ? 1 : 0, serverFileName2ImportTree, serverMetadataFileName, bindingTreeIdWithFile, new StandardAsyncCallback<Void>() {

            @Override
            public void onSuccess(Void result) {
                BIKClientSingletons.showInfo(I18n.trwaImportowanie.get());
            }
        });
    }

    protected void showImportHelp() {

    }

    public void buildAndShowDialog(TreeBean b) {
        this.treeBean = b;
        super.buildAndShowDialog();
    }

    private void createImportLinkedNodesPanel(final Integer lineId) {
        final int row = linesFt.getRowCount();
        final ListBox lb = new ListBox();
        for (TreeBean tree : BIKClientSingletons.getTrees()) {
            String menuPath = BIKClientSingletons.getMenuPathForTreeId(tree.id);
            if (!BaseUtils.isStrEmptyOrWhiteSpace(menuPath)) {
                lb.addItem(menuPath, BaseUtils.safeToString(tree.id));
            }
        }
        treesLbMap.put(lineId, lb);
        final Label fileNameLbl = new Label();

        final PushButton fileButton = new PushButton(I18n.plik.get());
        NewLookUtils.makeCustomPushButton(fileButton);
        fileButton.getElement().getStyle().setMarginLeft(6, Style.Unit.PX);
        fileButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new FileUploadDialogSimple().buildAndShowDialog(new IParametrizedContinuation<FoxyFileUpload>() {

                    @Override
                    public void doIt(final FoxyFileUpload ffu) {
                        uploadedFilesMap.put(lineId, ffu.getServerFileName());
                        tmpFiles.add(ffu.getServerFileName());
                        fileNameLbl.setText(ffu.getClientFileName());
                    }
                }, Arrays.asList(BIKClientSingletons.allowedFileExtensionsList()));
            }
        });

        Image deleteButton = new Image("images/delete.png");

        deleteButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                treesLbMap.remove(lineId);
                uploadedFilesMap.remove(lineId);
                fileNameLbl.setText("");
                int row = 0;
                while (row < linesFt.getRowCount() && event.getSource() != linesFt.getWidget(row, 3)) {
                    row++;
                }
                linesFt.removeRow(row);
            }
        });

        linesFt.setWidget(row, 0, lb);
        linesFt.setWidget(row, 1, fileButton);
        linesFt.setWidget(row, 2, fileNameLbl);
        linesFt.setWidget(row, 3, deleteButton);
    }

    @Override
    protected boolean doActionBeforeClose() {
//        if (BaseUtils.isStrEmptyOrWhiteSpace(serverMetadataFileName)) {
//            BIKClientSingletons.showWarning(I18n.brak.get() + " " + I18n.plikMetadata.get());
//            return false;
//        }
//        if (BaseUtils.isStrEmptyOrWhiteSpace(serverFileName2ImportTree)) {
//            BIKClientSingletons.showWarning(I18n.brak.get() + " " + I18n.plikZDanymi.get());
//            return false;
//        }

        bindingTreeIdWithFile.clear();
        for (Entry<Integer, ListBox> e : treesLbMap.entrySet()) {
            Integer lineId = e.getKey();
            ListBox lb = treesLbMap.get(lineId);
            Integer bindedTreeId = BaseUtils.tryParseInteger(lb.getSelectedValue());
            String fileName = uploadedFilesMap.get(lineId);
            if (bindedTreeId != null && !BaseUtils.isStrEmptyOrWhiteSpace(fileName)) {
                bindingTreeIdWithFile.put(bindedTreeId, fileName);
            }
        }
        allTreeIds.clear();
        allTreeIds.add(treeBean.id);
        allTreeIds.addAll(bindingTreeIdWithFile.keySet());
        BIKClientSingletons.getService().checkAutoObj4Tree(allTreeIds, new StandardAsyncCallback<Set<Integer>>() {

            @Override
            public void onSuccess(Set<Integer> result) {
                if (result.size() > 0) {
                    String html = I18n.brakAutoObjIdDlaNastepujacychDrzew.get() + ":<br/>";
                    for (Integer treeId : result) {
                        html += ("<b>" + BIKClientSingletons.getMenuPathForTreeId(treeId) + "\n</b><br/>");
                    }
                    Dialogs.alertHtml(html, I18n.blad.get(), null);
                } else {
                    doAction();
                    hideDialog();
                }
            }
        });
        return false;
    }

    //        BIKClientSingletons.getService().checkTreeObjIds(bindingTreeIdWithFile.keySet(), new StandardAsyncCallback<Boolean>() {
//
//            @Override
//            public void onSuccess(Boolean result) {
//                if (result) {
//                    BIKClientSingletons.getService().importLinksAndJoinedObjs2Tree(treeBean.id, bindingTreeIdWithFile, new StandardAsyncCallback<Void>() {
//
//                        @Override
//                        public void onSuccess(Void result) {
//                            BIKClientSingletons.getService().checkJoinedObjsAttributesCompatibilitiy(bindingTreeIdWithFile, new StandardAsyncCallback<Boolean>() {
//
//                                @Override
//                                public void onSuccess(Boolean result) {
//                                    if (result) {
//
//                                    } else {
//                                        showImportHelp();
//                                    }
//                                }
//                            });
//                        }
//                    });
//                } else {
//                    showImportHelp();
//                }
//            }
//        });
//    }
//    private void confirmCalculateObjIds() {
//        allTreeIds.clear();
//        allTreeIds.add(treeBean.id);
//        allTreeIds.addAll(bindingTreeIdWithFile.keySet());
//
//        String msg = I18n.importLinkAndJoinedObjsHelpMessage.get(); /*+ ": <br/><b>" + BIKClientSingletons.getMenuPathForTreeId(treeBean.id) + "</b>";
//         for (Integer treeId : bindingTreeIdWithFile.keySet()) {
//         msg += "<br/><b>" + BIKClientSingletons.getMenuPathForTreeId(treeId) + "</b>";
//         }*/
//
//        Dialogs.confirmHtml(msg, I18n.uWAGA.get(), "OK", I18n.anuluj.get(), new IParametrizedContinuation<Boolean>() {
//
//            @Override
//            public void doIt(Boolean param) {
//                callService2CalculateObjIds();
//            }
//        });
//    }
//
//    private void callService2CalculateObjIds() {
//        BIKClientSingletons.getService().calculateObjIds(treeBean.id, allTreeIds, new StandardAsyncCallback<Void>() {
//
//            @Override
//            public void onSuccess(Void result) {
//                callService2Import();
//            }
//        });
//    }
}
