/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs.valuehandlers;

import pl.fovis.foxygwtcommons.dialogs.validators.FoxyGWTValidationException;
import simplelib.INamedTypedPropsBean;

/**
 *
 * @author pmielanczuk
 */
public interface IWidgetValueHandler<T> {

    public static class GetSetValueExtraInfo {

        public String propName;
    }

    public static class GetValueExtraInfo extends GetSetValueExtraInfo {

        public Class noPrimTargetValueClass;

        public GetValueExtraInfo(String propName, Class noPrimTargetValueClass) {
            this.propName = propName;
            this.noPrimTargetValueClass = noPrimTargetValueClass;
        }
    }

    public static class SetValueExtraInfo extends GetSetValueExtraInfo {

        public INamedTypedPropsBean namedTypedPropsBean;

        public SetValueExtraInfo(String propName, INamedTypedPropsBean namedTypedPropsBean) {
            this.propName = propName;
            this.namedTypedPropsBean = namedTypedPropsBean;
        }
    }

    public boolean isWidgetClassHandled(Object widget);

    public Object getValueFromWidget(T widget, Class targetValueClass, GetValueExtraInfo extraInfo) throws FoxyGWTValidationException;

    public void setValueToWidget(T widget, Object value, SetValueExtraInfo extraInfo);
}
