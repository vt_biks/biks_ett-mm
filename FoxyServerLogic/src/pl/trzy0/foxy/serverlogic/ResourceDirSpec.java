/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.ILameResourceProvider;
import commonlib.LameUtils;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class ResourceDirSpec {

    private static ILameLogger logger = LameLoggerFactory.getLogger(ResourceDirSpec.class);
    public String path;
    public String ext;
    public boolean recursive;
    public Map<String, Long> fileLastMods;

    public static Map<String, Long> collectLastModifiedOfFiles(Collection<String> fileNames,
            ILameResourceProvider resProvider) {
        Map<String, Long> res = new HashMap<String, Long>();

        for (String fileName : fileNames) {
            res.put(fileName, resProvider.getLastModifiedOfResource(fileName));
        }

        return res;
    }

    public static boolean lastModifiedEquals(Map<String, Long> lm1, Map<String, Long> lm2) {
        if (lm1.size() != lm2.size()) {
            if (logger.isInfoEnabled()) {
                Set<String> common = LameUtils.setIntersection(lm1.keySet(), lm2.keySet());
                Set<String> diffs = new HashSet<String>(lm1.keySet());
                diffs.addAll(lm2.keySet());
                diffs.removeAll(common);
                logger.info("detected change in files list: " + diffs);
            }

            return false;
        }

        for (Entry<String, Long> e : lm1.entrySet()) {
            Long m2 = lm2.get(e.getKey());
            if (!BaseUtils.safeEquals(e.getValue(), m2)) {
                if (logger.isInfoEnabled()) {
                    logger.info("detected change in file: " + e.getKey());
                }
                return false;
            }
        }

        return true;
    }

    public static ResourceDirSpec makeInitial(String dir, String ext, boolean recursive, ILameResourceProvider resProvider) {
        Set<String> newNames = resProvider.getResourceNames(dir, BaseUtils.paramsAsSet(ext), recursive);
        ResourceDirSpec res = new ResourceDirSpec();

        res.path = dir;
        res.ext = ext;
        res.recursive = recursive;
        res.fileLastMods = collectLastModifiedOfFiles(newNames, resProvider);

        return res;
    }

    public static ResourceDirSpec calcChangedResourceDirSpec(ResourceDirSpec spec, ILameResourceProvider resProvider) {
        ResourceDirSpec res = makeInitial(spec.path, spec.ext, spec.recursive, resProvider);

        boolean modified = !lastModifiedEquals(res.fileLastMods, spec.fileLastMods);

        return modified ? res : null;
    }
}
