/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.ImportOverviewBean;
import pl.fovis.common.lisa.LisaConstants;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaImportActionBean;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.grid.IBeanProvider;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class LisaImportOverviewDialog extends BaseActionOrCancelDialog {

    protected ImportOverviewBean overviewInfo;
    protected String serverFileName;
    protected List<LisaDictColumnBean> pkColumnsList;
    protected IContinuation dialogAction;
    protected IContinuation cancelAction;

    @Override
    protected String getDialogCaption() {
        return I18n.przegladImportu.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zatwierdz.get();
    }

    @Override
    protected void doCancel() {
        cancelAction.doIt();
        super.doCancel();
    }

    @Override
    protected void buildMainWidgets() {
        Grid table = new Grid(6, 3);
        int nRow = 0;
        table.setWidget(nRow, 0, new Label(I18n.brakWartosciKluczaGlownego.get() + ":"));
        table.setWidget(nRow, 1, new Label(" " + overviewInfo.pkEmpty + " " + I18n.rekordy.get()));
        table.setWidget(nRow++, 2, createShowPkEmptyRecordsBtn());

        table.setWidget(nRow, 0, new Label(I18n.powtarzajaSieRekordyWPliku.get() + ":"));
        table.setWidget(nRow, 1, new Label(" " + overviewInfo.pkDuplicated + " " + I18n.rekordy.get()));
        table.setWidget(nRow++, 2, createShowPkDuplicatedRecordsBtn());

        table.setWidget(nRow, 0, new Label(I18n.niePrzeszloWalidacji.get() + ":"));
        table.setWidget(nRow, 1, new Label(" " + overviewInfo.invalid + " " + I18n.rekordy.get()));
        table.setWidget(nRow++, 2, createShowErrorRecordsBtn());

        table.setWidget(nRow, 0, new Label(I18n.dodanoDoSlownika.get() + ":"));
        table.setWidget(nRow, 1, new Label(" " + overviewInfo.added + " " + I18n.rekordy.get()));
        table.setWidget(nRow++, 2, createShowAddedRecordsBtn());

        table.setWidget(nRow, 0, new Label(I18n.usunietoZSlownika.get() + ":"));
        table.setWidget(nRow, 1, new Label(" " + overviewInfo.deleted + " " + I18n.rekordy.get()));
        table.setWidget(nRow++, 2, createShowDeletedRecordsBtn());

        table.setWidget(nRow, 0, new Label(I18n.zaktualizowanoWSlowniku.get() + ":"));
        table.setWidget(nRow, 1, new Label(" " + overviewInfo.updated + " " + I18n.rekordy.get()));
        table.setWidget(nRow++, 2, createShowUpdatedRecordsBtn());

        actionBtn.setEnabled(overviewInfo.pkDuplicated == 0);
        main.add(table);
    }

    @Override
    protected void doAction() {
        dialogAction.doIt();
    }

    private PushButton createShowAddedRecordsBtn() {
        return createShowBtnWithAction(LisaConstants.IMPORT_ACTION_ADD);
    }

    private PushButton createShowDeletedRecordsBtn() {
        return createShowBtnWithAction(LisaConstants.IMPORT_ACTION_DELETE);
    }

    private PushButton createShowUpdatedRecordsBtn() {
        return createShowBtnWithAction(LisaConstants.IMPORT_ACTION_UPDATE);
    }

    private PushButton createShowErrorRecordsBtn() {
        return createShowBtnWithAction(LisaConstants.IMPORT_ACTION_INVALID);
    }

    private Widget createShowPkEmptyRecordsBtn() {
        return createShowBtnWithAction(LisaConstants.IMPORT_ACTION_PK_EMPTY);
    }

    private Widget createShowPkDuplicatedRecordsBtn() {
        return createShowBtnWithAction(LisaConstants.IMPORT_ACTION_PK_DUPLICATE);
    }

    public void buildAndShowDialog(String serverFileName, ImportOverviewBean overviewInfo, List<LisaDictColumnBean> pkColumnsList, IContinuation dialogAction, IContinuation cancelAction) {
        this.serverFileName = serverFileName;
        this.overviewInfo = overviewInfo;
        this.pkColumnsList = pkColumnsList;
        this.dialogAction = dialogAction;
        this.cancelAction = cancelAction;
        super.buildAndShowDialog();
    }

    private PushButton createShowBtnWithAction(final String actionCode) {
        PushButton btn = new PushButton(I18n.pokaz.get());
        btn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new LisaImportActionListDialog().buildAndShowDialog(new LisaImportActionBeanProvider(serverFileName, actionCode), pkColumnsList);
            }
        });
        NewLookUtils.makeCustomPushButton(btn);
        return btn;
    }

    public static class LisaImportActionBeanProvider implements IBeanProvider<LisaImportActionBean> {

        protected String actionCode;
        protected String serverFileName;

        public LisaImportActionBeanProvider(String serverFileName, String actionCode) {
            this.serverFileName = serverFileName;
            this.actionCode = actionCode;
        }

        @Override
        public void getBeans(int offset, int limit, Iterable<String> sortCols, Iterable<String> descendingCols, final IParametrizedContinuation<Pair<? extends Iterable<LisaImportActionBean>, Integer>> showBeansCont) {
            BIKClientSingletons.getLisaService().getActionList(serverFileName, actionCode, offset, limit, new StandardAsyncCallback<List<LisaImportActionBean>>() {

                @Override
                public void onSuccess(List<LisaImportActionBean> result) {
                    showBeansCont.doIt(new Pair<Iterable<LisaImportActionBean>, Integer>(result, result.size()));
                }
            });
        }
    }
}
