/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.lisa.LisaBeanDisplay;
import pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.grid.BeanGrid;

/**
 *
 * @author lb
 */
public class LisaDictionaryMetadataContent extends EntityDetailsWidgetFlatBase {

    private static final Integer PAGE_SIZE = 50;
    private FlowPanel panel;
    private ChangableMetadataBeanProvider beanProvider = null;
    private BeanGrid<LisaDictColumnBean> grid = null;
    private LisaBeanDisplay<LisaDictColumnBean> beanDisplay = null;

    public LisaDictionaryMetadataContent() {
    }

    @Override
    public void displayData() {
        if (hasContent()) {
            generateGrid();
        }
    }

    private void generateGrid() {
        BIKClientSingletons.getLisaService().getDictionaryColumnsByNodeId(getNodeId(), new StandardAsyncCallback<List<LisaDictColumnBean>>() {
            public void onSuccess(List<LisaDictColumnBean> result) {
                beanProvider = new ChangableMetadataBeanProvider(getNodeId());
                beanDisplay = new LisaBeanDisplay<LisaDictColumnBean>(LisaDictColumnBean.class, null, BIKClientSingletons.beanCreator);
                grid = new BeanGrid<LisaDictColumnBean>(beanProvider, beanDisplay, PAGE_SIZE, LisaBeanDisplay.NAME_IN_DB,
                        LisaBeanDisplay.TITLE_TO_DISPLAY,
                        LisaBeanDisplay.DESCRIPTION,
                        LisaBeanDisplay.DATA_TYPE,
                        LisaBeanDisplay.IS_MAIN_KEY,
                        LisaBeanDisplay.ACCESS_LEVEL);
                grid.setHeaderStyleName("lisaDictHeader");
                panel.add(grid.asWidget());
            }
        });
        panel.clear();
    }

    public Widget buildWidgets() {
        panel = new FlowPanel();
        return panel;
    }

    @Override
    public boolean hasContent() {
        return BIKConstants.NODE_KIND_LISA_DICTIONARY.equals(getNodeKindCode()) && hasChildren();
    }
}
