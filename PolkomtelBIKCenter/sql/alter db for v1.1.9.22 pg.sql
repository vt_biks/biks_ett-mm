exec sp_check_version '1.1.9.22';
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_get_visual_order_for_new_node]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_get_visual_order_for_new_node]
GO

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

-- poprzednia wersja funkcji znajduje si� w skrypcie "alter db for v1.1.4.7 pm.sql"
--w tej funkcji doda�em drugi parametr @tree_id, je�li pierwszy parametr @parent_id jest null,
--czyli np na najwy�szym poziomie drzewa i wyliczony jest visual order na 0, czyli niepoprawne.
--Teraz je�li @parent_id is null to wprawdzam czy @tree_id is not null i dla drzewka wyliczam kolejno��
create function [dbo].[fn_get_visual_order_for_new_node](@parent_id int, @tree_id int)
returns int
as
begin
  declare @res int 
  if @parent_id is not null 
	begin
		select @res = (select max(visual_order) from bik_node where parent_node_id = @parent_id and is_deleted = 0)
	end
  else
  begin
	if @tree_id is not null 
	begin
		select @res = max(visual_order)
			from bik_node 
			where parent_node_id is null and tree_id=@tree_id and is_deleted = 0
	end
  end
  return case when @res is null then 0 else @res + 1 end
end

GO

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_update_version '1.1.9.22', '1.1.9.23';
go
