/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.exprlang;

import pl.foxys.foxymill.renderctx.IRenderCtx;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.TextFilePosition;

/**
 *
 * @author wezyr
 */
public abstract class ParsedExpressionBase<T> implements IParsedExpression {

    //private static final int REPEAT_EVAL_CNT = 100;
    protected String exprStr;
    protected String name;
    protected T parsedExpr;
    protected TextFilePosition filePos;
    protected boolean repeatEval;

    protected abstract T innerParse() throws Exception;

    protected abstract Object innerEval(//IRenderContext
            IRenderCtx renderCtx) throws Exception;

    public void parse(String exprStr, String name, TextFilePosition filePos) throws Exception {
        this.exprStr = exprStr;
        this.name = name;
        this.filePos = filePos;
        this.parsedExpr = innerParse();

//        String s = BaseUtils.trimLeft(exprStr);
//        if (BaseUtils.safeStartsWith(s, "makeUrl")) {
//            this.repeatEval = true;
//        }
    }

    public Object eval(//IRenderContext
            IRenderCtx renderCtx) {
        try {
            Object res = innerEval(renderCtx);

//            if (repeatEval) {
//                for (int i = 0; i < REPEAT_EVAL_CNT; i++) {
//                    res = innerEval(renderCtx);
//                }
//                System.out.println("***** EVAL REPEATED !!! ***** expr=" + exprStr);
//            }
//
            return res;
        } catch (Exception ex) {
            throw new LameRuntimeException("+++error executing expression:\n" + exprStr
                    + "\n +++at " + filePos + ", name: " + name + ", vars in ctx: " + renderCtx.getVars()
                    + ", +++model: " + renderCtx.getRootModel(), ex);
        }
    }
}
