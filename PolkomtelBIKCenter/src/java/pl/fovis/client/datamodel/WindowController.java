/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.datamodel;

import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.google.gwt.user.client.ui.AbsolutePanel;

/**
 *
 * @author pgajda
 */
public class WindowController {

        private final AbsolutePanel boundaryPanel;
        private PickupDragController pickupDragController;

        WindowController(AbsolutePanel boundaryPanel) {
            this.boundaryPanel = boundaryPanel;

            pickupDragController = new PickupDragController(boundaryPanel, true);
            pickupDragController.setBehaviorConstrainedToBoundaryPanel(true);
            pickupDragController.setBehaviorMultipleSelection(false);

        }

        public AbsolutePanel getBoundaryPanel() {
            return boundaryPanel;
        }

        public PickupDragController getPickupDragController() {
            return pickupDragController;
        }

}
