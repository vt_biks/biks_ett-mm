package pl.trzy0.foxy.commons.tags;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class StringStreamParser implements Iterator<TagData> {

    static ILameLogger logger = LameLoggerFactory.getLogger(StringStreamParser.class);
    private String content;
    private String sourceFileName;
    //private int contentLen;
    private ParsingPos currPos;
    //private int level = 0;
    private boolean foxyMode = false;
    private StringBuilder exclamationsText = new StringBuilder();
    
    /**
     * Creates a new instance of StringStreamParser
     */
    public StringStreamParser(String content, String sourceFileName) {
        //logger.debug("-------");
        this.content = content;
        //this.contentLen = content.length();
        this.currPos = new ParsingPos(content, sourceFileName);
        this.sourceFileName = sourceFileName;
        //this.levelDelta = 1;
        skipWhitespace(true);
    }

    private boolean isWhitespace(char c) {
        return (c <= ' ');
    }

    private boolean isProperCurrPos() {
        //return currPos < contentLen;
        return currPos.isProperCurrPos();
    }

    private boolean compareAtCurrPos(String substr) {
        //return content.startsWith(substr, currPos);
        return currPos.compareAtCurrPos(substr);
    }

    private boolean consumeChars(String chars) {
//        boolean hasChars = compareAtCurrPos(chars);
//        if (hasChars)
//            currPos += chars.length();
//        return hasChars;
        return currPos.consumeChars(chars);
    }

    public int getCurrPos() {
        return currPos.pos();
    }

    public String getExclamations() {
        return exclamationsText.toString();
    }

    private void skipWhitespace(boolean skipCommentsToo) {
        //ParsingPos hlpPos;
        int oldPos;
        do {
            oldPos = currPos.pos();
            // w trybie foxyTag nie powinno być komentarza,
            // bo już jesteśmy w komentatzu
            if (skipCommentsToo) {
                ParsingPos hlpPos = currPos.copy();
                boolean hasComment = consumeChars("<!--");
                if (hasComment) {
                    if (consumeChars("$")) {
                        currPos = hlpPos;
                        return;
                    }
//                currPos = content.indexOf("-->", currPos);
//                checkProperCurrPos();
//                currPos += 3;
                    currPos.moveBehind("-->");
                }
                int exclamStartPos = currPos.pos();
                boolean hasExclamationTag = consumeChars("<!");
                if (hasExclamationTag) {
                    currPos.moveBehind(">");
                    exclamationsText.append(content.substring(exclamStartPos, currPos.pos()));
                }
            }
            while (isProperCurrPos() && isWhitespace(/*content.charAt(currPos)*/currPos.currChar())) {
                currPos.movePos(1);
            }
        //currPos++;
        } while (oldPos < currPos.pos());
    }

    public boolean hasNext() {
        return isProperCurrPos();
    }

    public TagData next() {
        if (!hasNext()) {
            throw new NoSuchElementException("calling next() with no more tags");
        }

        if (consumeChars("-->")) {
            if (!foxyMode) {
                currPos.throwAtCurrPos("end of foxy tags outside of foxy tag mode");
            }
            foxyMode = false;
        }

        if (consumeChars("<!--$")) {
            if (foxyMode) {
                currPos.throwAtCurrPos("Start of foxy tags inside foxy tag mode");
            }
            foxyMode = true;
        }

        ParsingPos hlpPos = currPos.copy();
        if (consumeChars("<!--")) {
            currPos.moveBehind("-->");
            return new TagData("<!-- -->", true, null, 0, hlpPos.getRowNum(), hlpPos.getColNum(), sourceFileName);
        }

        String name;
        Map<String, String> attrs = null;
        int levelDelta;
        boolean isTextTag;

        ParsingPos tagStartPos = currPos.copy();

        if (consumeChars("<")) {
            boolean isEndTag = consumeChars("/");
            int tagEndKind;
            name = getIdent();

            while (true) {
                skipWhitespace(false);
                tagEndKind = consumeTagEnd();
                if (tagEndKind > 0) {
                    break;
                }

                if (attrs == null) {
                    attrs = new LinkedHashMap<String, String>();
                }
                
                String attrName, attrVal;
                skipWhitespace(false);
                attrName = getIdent();
                skipWhitespace(false);
                if (!consumeChars("=")) {
                    currPos.throwAtCurrPos("expected '=' inside tag");
                }
                skipWhitespace(false);
                attrVal = getQuotedString();
                attrs.put(attrName, attrVal);
            }
            if (isEndTag && tagEndKind == 2) {
                currPos.throwAtCurrPos("tag ends twice (</./>)");
            }
            if (isEndTag) {
                levelDelta = -1;
            } else {
                if (tagEndKind == 2) {
                    levelDelta = 0;
                } else {
                    levelDelta = 1;
                }
            }
            //kind = TagData.Kind.HTML;
            isTextTag = false;
        } else {
            // text
            ParsingPos endPos = findNextTagChar();
            if (endPos.isProperCurrPos() && foxyMode && endPos.currChar() == '>') {
                name = content.substring(currPos.pos(), endPos.pos() - 2);
                currPos.moveToPos(endPos.pos() - 2);
                if (!consumeChars("-->")) {
                    currPos.throwAtCurrPos("found '>' but not as end of foxy tag mode");
                }
            } else {
                name = content.substring(currPos.pos(), endPos.pos());
                currPos = endPos;
            }
            levelDelta = 0;
            //kind = TagData.Kind.TEXT;
            isTextTag = true;
        }
        //%^% skipWhitespace(); ///%^%
        TagData res = new TagData(name, isTextTag, attrs, levelDelta, tagStartPos.getRowNum(), tagStartPos.getColNum(), sourceFileName);
        if (/*LameUtils.globalEnableDebug ||*/logger.isDebugEnabled()) {
            logger.debug("pos: " + currPos.pos() + "/" + currPos.getColNum() + 
                    "/" + currPos.getRowNum() + ", tag:" + res.renderWithOutAttrs());
        }
        return res;
    }

    private static final boolean[] isIdentCharBitmap = new boolean[128];
    private static char lastIdentChar;

    static {
        for (char c = 0; c < isIdentCharBitmap.length; c++) {
            boolean isIC = isIdentChar(c);
            isIdentCharBitmap[c] = isIC;
            if (isIC) {
                lastIdentChar = c;
            }
        }
    }

    public static boolean isIdentCharFast(char c) {
        return c <= lastIdentChar && isIdentCharBitmap[c];
    }
    
    private static boolean isIdentChar(char c) {
        return c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' ||
                    c >= '0' && c <= '9' || c == ':' || c == '-' || c == '_' || c == '@';
    }

    private String getIdent() {
        ParsingPos i = currPos.copy();
        while (i.isProperCurrPos()) {
            char c = i.currChar(); //Character.toLowerCase(i.currChar());
            if (isIdentCharFast(c)) {
                i.movePos(1);
            } else {
                break;
            }
        }

        if (i.pos() == currPos.pos()) {
            currPos.throwAtCurrPos("empty ident");
        }
        String ret = content.substring(currPos.pos(), i.pos());
        currPos = i;
        return ret;
    }

    // 0 - brak, 1 - zwykły (>), 2 - zamykający (/>)
    private int consumeTagEnd() {
        if (consumeChars("/>")) {
            return 2;
        }
        if (consumeChars(">")) {
            return 1;
        }
        return 0;
    }

    private String getQuotedString() {
        //checkProperCurrPos();
        char quotChar = currPos.currChar();
        if (quotChar != '"' && quotChar != '\'') {
            currPos.throwAtCurrPos("expected quotchar");
        }
        currPos.movePos(1);
        int endPos = content.indexOf(quotChar, currPos.pos());
        if (endPos < 0) {
            currPos.throwAtCurrPos("unexpected end of content inside quoted string");
        }
        String ret = content.substring(currPos.pos(), endPos);
        currPos.moveToPos(endPos + 1);
        return ret;
    }

    // jak nie znajdzie, zwraca pozycję za końcem tekstu
//    private int findNextTagChar() {
//        int i = currPos;
//        while (i < contentLen) {
//            char c = content.charAt(i);
//            if (c == '<' || c == '>')
//                return i;
//            i++;
//        }
//        return contentLen;
//    }
    private ParsingPos findNextTagChar() {
        ParsingPos i = currPos.copy();
        while (i.isProperCurrPos()) {
            char c = i.currChar();
            if (c == '<' /*|| c == '>'*/) {
                break;
            }
            i.movePos(1);
        }
        return i;
    }

    public void remove() {
        throw new UnsupportedOperationException("remove not supported");
    }
}
