/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.bikpages.TreeBikPage;
import pl.fovis.client.dialogs.TreeNodeDialog;
import pl.fovis.client.dialogs.TreeSelector2ExportDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.popupmenu.FoxyPopUpMenu;
import pl.fovis.foxygwtcommons.popupmenu.IPopupMenuItemAction;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class BiksMainBranchPopUpMenu extends FoxyPopUpMenu<INodeActionContext> {

//    public BiksMainBranchPopUpMenu(INodeActionContext contextObject, int clientX, int clientY) {
//        super(contextObject, clientX, clientY);
//    }
    protected TreeBikPage bikPage;
    protected String tabId;
    protected TreeBean treeBean;

    public BiksMainBranchPopUpMenu(String tabId, TreeBikPage bikPage, int clientX, int clientY) {

        super(null, clientX, clientY);
        this.bikPage = bikPage;
        this.tabId = tabId;
        this.treeBean = bikPage.getTreeBean();
    }

    @Override
    public void addAction(IPopupMenuItemAction<INodeActionContext> act) {
        super.addAction(act); //To change body of generated methods, choose Tools | Templates.}
    }

    @Override
    protected void addActions() {
        //TreeBean treeBean = bikPage.getTreeBean();
//        ;

//        Window.alert(nodeRightsActionId() + "");
        if (treeBean.showAddMainBranchBtn && nodeRightsActionId() && !BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(treeBean.treeKind)) {
            addCustomTreeActions();
        }
        addExportTree();

    }

    public boolean isEnabledByDesign() {
        if (treeBean.code.equals(BIKGWTConstants.TREE_CODE_USER_ROLES)) {
            return false;
        }
        if (isInDQMAdminMode() /*&& (node == null || isNodeFolder())*/) {
//            Window.alert("NewNestableParent: is DQM Admin mode...");
            return true;
        }
        return (!isTreeReadOnly() || BIKClientSingletons.isLoggedUserAuthorOfTree(treeBean.id));
    }

    protected final boolean isTreeReadOnly() {
//        boolean adminOrall = isAdminOrExpertOrAuthorOrEditor();
//        boolean autoredi = isNodeAuthorOrEditor();
        if (!BIKClientSingletons.isEffectiveExpertLoggedIn() && !BIKClientSingletons.isLoggedUserAuthorOfTree(treeBean.id)
                && !BIKClientSingletons.isLoggedUserCreatorOfTree(treeBean.id)
                && !BIKClientSingletons.isLoggedUserIsNonPublicUserInTree(treeBean.id)) {
            return true;
        }

        return isTreeReadOnlyInner();
    }

    protected final boolean isTreeReadOnlyInner() {
        return !BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(treeBean.treeKind)
                && !BIKConstants.TREE_KIND_META_BIKS_MENU_CONF.equalsIgnoreCase(treeBean.treeKind)
                && (treeBean.nodeKindId == null || treeBean.nodeKindId != null && treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_QUALITY_METADATA));
//                (treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_METADATA) ||
//                treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_DQCMETADATA)
//                );
    }

    protected boolean isDQMTree() {
        return BIKConstants.TREE_KIND_QUALITY_METADATA.equals(treeBean.treeKind) && treeBean.nodeKindId != null;
    }

    protected boolean isInDQMAdminMode() {
        return isDQMTree() && BIKClientSingletons.isEffectiveExpertLoggedIn();
    }

    protected boolean nodeRightsActionId() {

        Integer nodeRightsActionId = BIKClientSingletons.getNodeActionIdByCode("NewNestableParent");
        if (nodeRightsActionId == null) {
            return isEnabledByDesign();
        }
        return BIKClientSingletons.isNodeActionGrantedInTreeRoot(treeBean.id, nodeRightsActionId);
//        return true;
    }

    protected void addCustomTreeActions() {
        final TreeBean treeBean = bikPage.getTreeBean();
        if (BIKClientSingletons.isTreeKindDynamic(treeBean.treeKind)) {

            for (final TreeBean tb : treeBean.nodeKindsToAddOnTreesAsMainBranch) {
                NodeKindBean nkb = new NodeKindBean();
                nkb.caption = tb.nodeKindCaption;
                nkb.id = tb.nodeKindId;
                popMenu.addMenuItem(I18n.dodaj.get() + " " + tb.nodeKindCaption, new IContinuation() {
                    @Override
                    public void doIt() {
                        buildTreeNodeDialog(tb, tb.requiredAttrs, null);
                    }
                }
                );
            }
        } else {
//            final TreeBean treeBean = BIKClientSingletons.getTreeBeanById(BIKClientSingletons.getTreeIdOfCurrentTab());
            String caption = BIKClientSingletons.getNodeKindCaptionByCode(treeBean.nodeKindCode);
            if (caption != null) {
                popMenu.addMenuItem(I18n.dodaj.get() /* I18N:  */ + " "
                        + (!treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_USERS) ? caption : I18n.grupeUzytkownikow.get() /* I18N:  */),
                        new IContinuation() {
                    @Override
                    public void doIt() {

                        BIKClientSingletons.getService().getRequiredAttrs(treeBean.nodeKindId, new StandardAsyncCallback<List<AttributeBean>>() {
                            @Override
                            public void onSuccess(final List<AttributeBean> result) {

                                BIKClientSingletons.getService().getBikAllArts(treeBean.id, new StandardAsyncCallback<List<String>>() {
                                    @Override
                                    public void onSuccess(final List<String> keywords) {
                                        buildTreeNodeDialog(treeBean, result, keywords);

                                    }
                                });
                            }
                        });
                    }
                });
            }
        }
    }

    protected void buildTreeNodeDialog(final TreeBean treeBean, final List<AttributeBean> requiredAttrs, List<String> keywords) {
        new TreeNodeDialog().buildAndShowDialog(treeBean.nodeKindId, treeBean.nodeKindCode, null,
                null, treeBean.id, keywords,
                requiredAttrs,
                new IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>>() {
            public void doIt(final Pair<TreeNodeBean, Map<String, AttributeBean>> param2) {
                TreeNodeBean param = param2.v1;
                BIKClientSingletons.getService().createTreeNodeWithAttr(param.parentNodeId,
                        param.nodeKindId, param.name, param.objId, param.treeId, param.linkedNodeId, null,
                        param2.v2,
                        new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on" /* I18N: no */ + " createTreeNode") {
                    @Override
                    public void onSuccess(Pair<Integer, List<String>> result) {
                        String treeId = BIKClientSingletons.getTreeCodeById(BIKClientSingletons.getTreeIdOfCurrentTab());
                        BIKClientSingletons.invalidateTabData(treeId);
                        BIKClientSingletons.getTabSelector().submitNewTabSelection(treeId,
                                result.v1, true);
                        BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get() /* I18N:  */);
                    }
                });
            }
        });

    }

    protected void addExportTree() {

        if (isVisibleExportImportBtn(tabId)) {
            popMenu.addMenuSeparator();
            popMenu.addMenuItem(I18n.eksportGeneruj.get(), new IContinuation() {
                @Override
                public void doIt() {

                    saveTree();
                }
            });
        }
    }

    protected boolean isVisibleExportImportBtn(String tabId) {
//        TreeBean tb = BIKClientSingletons.getTreeBeanByCode(tabId);

        if (treeBean == null) {
            return false;
        }
//        return true;

        return (BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_IMPORT_TREE, treeBean.id)
                || BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_TO_HTML, treeBean.id)
                || BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_TO_PDF, treeBean.id))
                && !BIKClientSingletons.disableImportExport()
                && !"DictionaryDWH".equalsIgnoreCase(treeBean.code);
    }

    public void saveTree() {
        //        Window.open(generateSaveTreeLink(BIKClientSingletons.getTreeIdOfCurrentTab()), "_blank", "");
        TreeSelector2ExportDialog dialog = new TreeSelector2ExportDialog(BIKClientSingletons.getTreeIdOfCurrentTab());
        dialog.buildAndShowDialog();
    }
}
