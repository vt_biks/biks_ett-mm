/*
 * JdbcUtil.java
 *
 * Created on 16 czerwiec 2006, 11:03
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package commonlib;

import java.lang.reflect.Modifier;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;
import simplelib.LameRuntimeException;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class JdbcUtil {

    static ILameLogger logger = LameLoggerFactory.getLogger(JdbcUtil.class);
    //LameLogger.getClassLogger();
    private String driverClassName;
    private String connUrl;
    private Connection conn;
//    private Statement st;

    public Connection getConnection() {
        return conn;
    }

    private void initWithConn(Connection conn) {
        try {
            this.conn = conn;

            //st = conn.createStatement();
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
    }

    public JdbcUtil(Connection conn) {
        try {
            conn.setAutoCommit(false);

            initWithConn(conn);
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
    }

    public JdbcUtil(String driverClassName, String connUrl, String user, String password) {
        this(driverClassName, connUrl, user, password, false);
    }

    public JdbcUtil(String driverClassName, String connUrl, String user, String password, boolean autoCommit) {
        this.driverClassName = driverClassName;
        this.connUrl = connUrl;
        Connection conn;
        try {
            Class.forName(driverClassName);
            conn = DriverManager.getConnection(connUrl, user, password);
            conn.setAutoCommit(false);
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }

        initWithConn(conn);
    }

    public JdbcUtil(String driverClassName, String connUrl) {
        this.driverClassName = driverClassName;
        this.connUrl = connUrl;
        Connection conn;

        try {
            Class.forName(driverClassName);
            conn = DriverManager.getConnection(connUrl);
            conn.setAutoCommit(false);
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
        initWithConn(conn);
    }

    public JdbcUtil(String driverClassName, String connUrl, Properties info) {
        this.driverClassName = driverClassName;
        this.connUrl = connUrl;
        Connection conn;

        try {
            Class.forName(driverClassName);
            conn = DriverManager.getConnection(connUrl, info);
            conn.setAutoCommit(false);
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
        initWithConn(conn);
    }

    public void close() {
        try {
            //st.close();
            //st = null;
            conn.close();
            conn = null;
        } catch (SQLException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    public static String fixStringForXml(String str) {
        return str.trim();

//        StringBuilder sb = new StringBuilder(str.length());
//
//        for (int j = 0; j < str.length(); j++) {
//            char c = str.charAt(j);
//            if (c < ' ') c = ' ';
//            //else if (c > '\u007f') c = '?';
//            sb.append(c);
//        }
//        return sb.toString();
    }

    /*
     public static Map<String,Object> resultSetCurrRecToMap(ResultSet rs) {
     try {
     return resultSetCurrRecToMap(rs, LameUtils.intIdentityArray(1, rs.getMetaData().getColumnCount()));
     } catch (SQLException ex) {
     throw new LameRuntimeException(ex);
     }
     }
     */
    public static interface MapEnhancer<K, V> {

        boolean enhance(Map<K, V> m) throws Exception;

        int extraColCnt();
    }

    public static Map<String, Object> resultSetCurrRecToMap(ResultSet rs, int[] colIdxes, int extraColCnt) {
        Map<String, Object> m;

        try {
            ResultSetMetaData rsm = rs.getMetaData();

            int colCnt = colIdxes.length; //rsm.getColumnCount();
            m = new HashMap<String, Object>(colCnt + extraColCnt);

            for (int i = 0; i < colCnt; i++) {
                int colIdx = colIdxes[i];
                Object fldObj = rs.getObject(colIdx);
                if (fldObj != null) {
                    if (fldObj instanceof String) {
                        fldObj = fixStringForXml((String) fldObj);
                    }
                    m.put(rsm.getColumnName(colIdx), fldObj);
                }
            }
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }

        return m;
    }

    public static List<Map<String, Object>> resultSetToListOfMap(ResultSet rs) {
        return resultSetToListOfMap(rs, (String[]) null);
    }

    public static List<Map<String, Object>> resultSetToListOfMap(ResultSet rs, String[] ignoreColNames) {
        try {
            ResultSetMetaData rsm = rs.getMetaData();

            int[] ignIdxes = getColumnIdxes(rsm, ignoreColNames);

            return resultSetToListOfMap(rs, LameUtils.intIdentityArray(1, rsm.getColumnCount()));
        } catch (SQLException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    public static List<Map<String, Object>> resultSetToListOfMap(ResultSet rs, int[] colIdxes) {
        return resultSetToListOfMap(rs, colIdxes, new ArrayList<Map<String, Object>>());
    }

    public static List<Map<String, Object>> resultSetToListOfMap(ResultSet rs, int[] colIdxes, List<Map<String, Object>> r) {
        return resultSetToListOfMap(rs, colIdxes, r, //0,
                null);
    }

    public static List<Map<String, Object>> resultSetToListOfMap(ResultSet rs, int[] colIdxes, List<Map<String, Object>> r,
            //int extraColCnt,
            MapEnhancer<String, Object> mapEnh) {
        if (logger.isDebugEnabled()) {
            logger.debug("resultSetToListOfMap: start");
        }

        int cntr = 0;
        int extraColCnt;

        if (mapEnh == null) {
            extraColCnt = 0;
        } else {
            extraColCnt = mapEnh.extraColCnt();
        }

        try {
            while (rs.next()) {
                if (++cntr % 100000 == 0) {
                    if (logger.isDebugEnabled()) {
                        logger.debug(String.format("resultSetToListOfMap: current progress = %d", cntr));
                    }
                }
                Map<String, Object> m = resultSetCurrRecToMap(rs, colIdxes, extraColCnt);

                if (mapEnh == null || mapEnh.enhance(m)) {
                    r.add(m);
                }
            }
        } catch (LameRuntimeException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(String.format("resultSetToListOfMap: done (%d rows)", cntr));
        }
        return r;
    }

    public PreparedStatement prepareQuery(String sqlQuery) {
        try {
            return conn.prepareStatement(sqlQuery, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        } catch (SQLException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    public static List<Map<String, Object>> resultSetToVerticalList(final ResultSet rs,
            final String[] idColNames, final String keyColName, final String valColName,
            String[] ignoreColNames) {
        List<Map<String, Object>> r = new ArrayList<Map<String, Object>>();

        int[] colIdxes = new int[idColNames.length];

        try {

            final ResultSetMetaData rsm = rs.getMetaData();

            for (int i = 0; i < idColNames.length; i++) {
                colIdxes[i] = getColumnIdx(rsm, idColNames[i]);
            }

            for (int i = 1; i <= rsm.getColumnCount(); i++) {
                if (LameUtils.linearSearch(colIdxes, i) >= 0) {
                    continue;
                }
                //colIdxes[colIdxes.length - 1] = i;

                final int ii = i;
                final String colName = rsm.getColumnName(ii);
                if (LameUtils.linearSearch(ignoreColNames, colName) >= 0) {
                    continue;
                }

                rs.beforeFirst();

                MapEnhancer<String, Object> mapEnh = new MapEnhancer<String, Object>() {

                    public boolean enhance(Map<String, Object> m) throws Exception {
                        Object colVal = rs.getObject(ii);
                        boolean isOk = colVal != null;
                        if (isOk) {
                            m.put(keyColName, colName);
                            m.put(valColName, colVal.toString());
                        }
                        return isOk;
                    }

                    public int extraColCnt() {
                        return 2;
                    }
                };

                resultSetToListOfMap(rs, colIdxes, r, //2 - ignoreColNames.length,
                        mapEnh);
            }

        } catch (SQLException ex) {
            throw new LameRuntimeException(ex);
        }

        return r;
    }

    public List<Map<String, Object>> readStmtToListOfMap(PreparedStatement st,
            String[] idColNames, String keyColName, String valColName, String[] ignoreColNames) {
        ResultSet rs;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("readPrepStmtToListOfMap: getResultSet");
            }

            rs = st.executeQuery();
        } catch (SQLException ex) {
            throw new LameRuntimeException(ex);
        }

        List<Map<String, Object>> res;
        if (LameUtils.arrLengthFix(idColNames) == 0) {
            res = resultSetToListOfMap(rs, ignoreColNames);
        } else {
            res = resultSetToVerticalList(rs, idColNames, keyColName, valColName, ignoreColNames);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("readSQLQueryToListOfMap: done");
        }
        return res;
    }

    /*
     public ResultSet executeSQLQuery(String sqlQuery) throws SQLException {
     if (logger.isDebugEnabled())
     logger.debug("executeSQLQuery: przed wykonaniem zapytania: " + sqlQuery);
     ResultSet rs = st.executeQuery(sqlQuery);
     logger.debug("executeSQLQuery: po wykonaniu zapytania");
     return rs;
     }

     public int executeSQLCommand(String sqlCommand) throws SQLException {
     if (logger.isDebugEnabled())
     logger.debug("executeSQLCommand: przed wykonaniem zapytania: " + sqlCommand);
     int res = st.executeUpdate(sqlCommand);
     if (logger.isDebugEnabled())
     logger.debug("executeSQLCommand: po wykonaniu zapytania, res = " + res);
     return res;
     }

     public List<Map<String,Object>> readSQLQueryToListOfMap(String sqlQuery) {
     ResultSet rs;
     try {
     if (logger.isDebugEnabled())
     logger.debug("readSQLQueryToListOfMap: executeQuery: " + sqlQuery);

     rs = st.executeQuery(sqlQuery);
     } catch (SQLException ex) {
     throw new LameRuntimeException(ex);
     }

     List<Map<String,Object>> res = resultSetToListOfMap(rs);
     logger.debug("readSQLQueryToListOfMap: done");
     return res;
     }
     */
    public static int getColumnIdx(ResultSetMetaData rsm, String colName) throws SQLException {
        for (int i = 1; i <= rsm.getColumnCount(); i++) {
            if (rsm.getColumnName(i).toUpperCase().equals(colName)) {
                return i;
            }
        }

        throw new LameRuntimeException(String.format("getColumnIdx: column \"%s\" not found", colName));
    }

    public static int[] getColumnIdxes(ResultSetMetaData rsm, String[] colNames) throws SQLException {
        int colNamesLen = LameUtils.arrLengthFix(colNames);
        int[] res = new int[colNamesLen];

        for (int i = 0; i < colNamesLen; i++) {
            res[i] = getColumnIdx(rsm, colNames[i]);
        }
        return res;
    }

    public static Map<Object, Object> resultSetToIdMap(ResultSet rs, String idColName, String valColName) throws SQLException {
        if (logger.isDebugEnabled()) {
            logger.debug("resultSetToIdMap: zaczynam");
        }
        HashMap<Object, Object> m = new HashMap<Object, Object>();

        while (rs.next()) {
            m.put(rs.getObject(idColName), rs.getObject(valColName));
        }

        if (logger.isDebugEnabled()) {
            logger.debug("resultSetToIdMap: koniec");
        }
        return m;
    }

    public static List<String> getTableColumns(Connection conn, String tableName, String[] ignoreCols) throws SQLException {
        ResultSet rs = conn.getMetaData().getColumns(null, null, tableName, "%");
        List<String> colNames = new ArrayList<String>();

        while (rs.next()) {
            String colName = rs.getString("COLUMN_NAME");
            if (LameUtils.linearSearch(ignoreCols, colName) == -1) {
                colNames.add(colName);
            }
        }

        return colNames;
    }

    public static List<String> getCommonColumns(Connection conn, String table1, String table2, String[] ignoreCols) throws SQLException {
        return LameUtils.getCommonItems(getTableColumns(conn, table1, ignoreCols), getTableColumns(conn, table2, ignoreCols));
    }

    public static String[] getCommonColumnsArr(Connection conn, String table1, String table2, String[] ignoreCols) throws SQLException {
        List<String> l = LameUtils.getCommonItems(getTableColumns(conn, table1, ignoreCols), getTableColumns(conn, table2, ignoreCols));
        return l.toArray(new String[l.size()]);
    }

    public static String getCommonColumnsStr(Connection conn, String table1, String table2, String[] ignoreCols, String sep) throws SQLException {
        List<String> l = LameUtils.getCommonItems(getTableColumns(conn, table1, ignoreCols), getTableColumns(conn, table2, ignoreCols));
        return LameUtils.mergeWithSep(l, sep);
    }

    /*
     public <T> T testX(T o) {
     Object o3 = o;
     return (T) o3;
     }

     public <T> List<T> readSQLQueryToList(String queryText, String colName) throws SQLException {
     List<T> res = new ArrayList<T>();

     ResultSet rs = executeSQLQuery(queryText);
     while (rs.next()) {
     T t = (T) rs.getObject(colName);
     res.add(t);
     }
     return res;
     }


     public List<Object> readSQLQueryToList(String queryText, String colName) throws SQLException {
     List<Object> res = new ArrayList<Object>();

     ResultSet rs = executeSQLQuery(queryText);
     while (rs.next()) {
     Object o = rs.getObject(colName);
     res.add(o);
     }
     return res;
     }

     @SuppressWarnings("unchecked")
     public <T> List<T> readSQLQueryToList2(String queryText, String colName) throws SQLException {
     List<T> res = new ArrayList<T>();

     ResultSet rs = executeSQLQuery(queryText);
     while (rs.next()) {
     Object o = rs.getObject(colName);
     res.add((T) o);
     }
     return res;
     }

     @SuppressWarnings("unchecked")
     public <T> T readFirstSQLQueryResult(String queryText, String colName) throws SQLException {

     ResultSet rs = executeSQLQuery(queryText);
     while (rs.next()) {
     return (T) rs.getObject(colName);
     }

     return null;
     }
     */
    public List<String> getDBObjectNames(String catalog, String schemaPattern, String tableNamePattern, String[] types) {
        List<String> result = new ArrayList<String>();
        ResultSet resultSet = null;
        try {
            DatabaseMetaData dbmd = conn.getMetaData();

            resultSet = dbmd.getTables(catalog, schemaPattern, tableNamePattern, types);

            while (resultSet.next()) {
                // Get the table name
                result.add(resultSet.getString(3));
            }
        } catch (SQLException e) {
            throw new LameRuntimeException(e);
        } finally {
            closeRs(resultSet);
        }
        return result;
    }

    public String getObjectType(String catalog, String schemaPattern, String objName) {
        List<String> result = new ArrayList<String>();
        ResultSet resultSet = null;
        try {
            DatabaseMetaData dbmd = conn.getMetaData();

            resultSet = dbmd.getTables(catalog, schemaPattern, objName, new String[]{"TABLE", "VIEW"});

            if (resultSet.next()) {
                // Get the table name
                return resultSet.getString("TABLE_TYPE");
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new LameRuntimeException(e);
        } finally {
            closeRs(resultSet);
        }
    }

    private void closeRs(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
            }
        }
    }

    private void closeStatement(Statement stm) {
        if (stm != null) {
            try {
                stm.close();
            } catch (SQLException e) {
            }
        }
    }

    // alternatywana metoda pobrania meta danych o obiekcie
    public List<Map<String, Object>> getObjColumnsByPrepare(String objName) {
        try {
            List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM " + objName + " WHERE 0=1 ");
            ResultSetMetaData rs = prep.getMetaData();
            for (int i = 1; i <= rs.getColumnCount(); i++) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("COLUMN_NAME", rs.getColumnName(i));
                map.put("DATA_TYPE", rs.getColumnType(i));
                map.put("DECIMAL_DIGITS", rs.getScale(i));
                map.put("COLUMN_SIZE", rs.getPrecision(i));
                map.put("NULLABLE", rs.isNullable(i));
                map.put("AUTOINCREMENT", rs.isAutoIncrement(i));
                result.add(map);
                /*
                 System.out.println("Column: " + rs.getColumnName(i));
                 System.out.println("Label: " + rs.getColumnLabel(i));
                 System.out.println("isAutoIncrement: " + rs.isAutoIncrement(i));
                 System.out.println("isCaseSensitive: " + rs.isCaseSensitive(i));
                 */
            }
            return result;
        } catch (Exception e) {
            throw new LameRuntimeException(e);
        }
    }

    public List<Map<String, Object>> getObjColumns(String catalog, String schema, String objName) {
        try {
            ResultSet rs = conn.getMetaData().getColumns(catalog, schema, objName, "%");
            return resultSetToListOfMapAndClose(rs);
        } catch (Exception e) {
            throw new LameRuntimeException(e);
        }
    }

    public List<Map<String, Object>> getProcColumns(String catalog, String schema, String procName) {
        try {
            ResultSet rs = conn.getMetaData().getProcedureColumns(catalog, schema, procName, "%");
            return resultSetToListOfMapAndClose(rs);
        } catch (Exception e) {
            throw new LameRuntimeException(e);
        }
    }

    /*    public List<String> getTables(String tableNamePattern) {
     return getDBObjectNames(schemaPattern, tableNamePattern, new String[]{"TABLE"});
     }

     public List<String> getViewNames(String tableNamePattern) {
     return getDBObjectNames(schemaPattern, tableNamePattern, new String[]{"VIEW"});
     }
     */
    /*
     public Map<String, JDBCType> getColumns(String catalog, String schema, String objName) {
     return getColumns(catalog, schema, objName, false);
     }

     public Map<String, JDBCType> getColumns(String catalog, String schema, String objName, boolean notNullable) {
     Map<String, JDBCType> colMap = new HashMap<String, JDBCType>();
     try {
     //System.out.println(LameUtils.toDiagString(JdbcUtil.getTableColumns(jdbcUtil.getConnection(), objName, null)));
     ResultSet rs = conn.getMetaData().getColumns(catalog, schema, objName, "%");

     while (rs.next()) {

     String colName = rs.getString("COLUMN_NAME");
     String typeName = rs.getString("TYPE_NAME");
     int dataType = rs.getInt("DATA_TYPE");
     int decimalDigits = rs.getInt("DECIMAL_DIGITS");
     int colSize = rs.getInt("COLUMN_SIZE");
     int nullable = rs.getInt("NULLABLE");

     if (!notNullable || (nullable == java.sql.ResultSetMetaData.columnNoNulls )) {
     colMap.put(colName,
     new JDBCType(dataType, nullable == ResultSetMetaData.columnNullable,
     colSize, decimalDigits, typeName ) );
     }
     }
     } catch(SQLException e) {
     throw new LameRuntimeException(e);
     }
     return colMap;
     }
     */
    public List<String> getPrimaryKey(String catalog, String schema, String obj) {
        List<String> result = new ArrayList<String>();
        try {
            ResultSet rs = conn.getMetaData().getPrimaryKeys(catalog, schema, obj);

            while (rs.next()) {
                String colName = rs.getString("COLUMN_NAME");
                result.add(colName);
            }
        } catch (SQLException e) {
            throw new LameRuntimeException(e);
        }
        return result;
    }

    public static List<Map<String, Object>> resultSetToListOfMapAndClose(ResultSet rs) {
        try {
            //System.out.println(LameUtils.toDiagString(JdbcUtil.getTableColumns(jdbcUtil.getConnection(), objName, null)));
            return JdbcUtil.resultSetToListOfMap(rs);
        } catch (Exception e) {
            throw new LameRuntimeException(e);
        } finally {
            try {
                rs.close();
            } catch (SQLException ex) {
                //
            }
        }
    }

    public List<Map<String, Object>> getUniqueKeys(String catalog, String schema, String objName) {
        try {
            return resultSetToListOfMapAndClose(
                    conn.getMetaData().getIndexInfo(catalog, schema, objName, true, true));
        } catch (SQLException e) {
            throw new LameRuntimeException(e);
        }
    }

    public List<Map<String, Object>> getForeignKeys(String catalog, String schema, String objName) {
        try {
            return resultSetToListOfMapAndClose(conn.getMetaData().getImportedKeys(catalog, schema, objName));
        } catch (SQLException e) {
            throw new LameRuntimeException(e);
        }
    }

    public List<Map<String, Object>> getExportedKeys(String catalog, String schema, String objName) {
        try {
            return resultSetToListOfMapAndClose(conn.getMetaData().getExportedKeys(catalog, schema, objName));
        } catch (SQLException e) {
            throw new LameRuntimeException(e);
        }
    }

    public static String getTypeString(int typeInt) {
        return typeIntToStr.get(typeInt);
    }

    public static int getTypeInt(String typeName) {
        return typeStrToInt.get(typeName);
    }
    static private Map<Integer, String> typeIntToStr;
    static private Map<String, Integer> typeStrToInt;

    static {
        prepareTypeMaps();
    }

    private static void prepareTypeMaps() throws SecurityException, RuntimeException, IllegalArgumentException {
        typeIntToStr = new HashMap<Integer, String>();
        typeStrToInt = new HashMap<String, Integer>();

        try {
            for (java.lang.reflect.Field field : Types.class.getFields()) {
                if (Modifier.isStatic(field.getModifiers())) {
                    int type = ((Integer) field.get(null)).intValue();
                    String name = field.getName().toUpperCase();
                    typeIntToStr.put(type, name);
                    typeStrToInt.put(name, type);
                }
            }
        } catch (IllegalAccessException exception) {
            throw new RuntimeException(exception.getMessage());
        }
    }

    public String getPrimaryKeyName(String catalog, String schema, String obj) {
        try {
            ResultSet rs = conn.getMetaData().getPrimaryKeys(catalog, schema, obj);
            List<Map<String, Object>> pks = resultSetToListOfMapAndClose(rs);
            return pks.isEmpty() ? null : (String) pks.get(0).get("PK_NAME");
        } catch (SQLException e) {
            throw new LameRuntimeException(e);
        }

    }

    public static Connection makeConn(String driverClassName, String connUrl, String user,
            String pwd) {
        Connection conn;

        try {
            Class.forName(driverClassName);
            conn = user != null ? DriverManager.getConnection(connUrl, user, pwd)
                    : DriverManager.getConnection(connUrl);
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }

        return conn;
    }

    public static Map<String, Object> resultSetCurrRecToMap(ResultSet rs) throws LameRuntimeException {
        Map<String, Object> m;

        try {
            ResultSetMetaData rsm = rs.getMetaData();

            m = new HashMap<String, Object>();

            for (int i = 1; i <= rsm.getColumnCount(); i++) {
                Object fldObj = rs.getObject(i);
                if (fldObj != null) {
                    m.put(rsm.getColumnName(i).toUpperCase(), fldObj);
                }
            }
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }

        return m;
    }

    // c&p z jdbclib.common.JdbcMetaData, special thx for Smok
    public static Connection getConnection(String driverClassName, String connUrl,
            String user, String password, String catalog, Boolean autoCommit) {
        Connection conn;
        try {
            Class.forName(driverClassName);
            conn = DriverManager.getConnection(connUrl, user, password);
            if (autoCommit != null) {
                conn.setAutoCommit(autoCommit);
            }
            // drut - tylko dla MSSQL
            if (catalog != null) {
                try {
                    conn.setCatalog("[" + catalog + "]");
                } catch (SQLException ex) {
                    throw new LameRuntimeException("B��d ustawiania katalogu " + ex);
                }
            }
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
        return conn;
    }

    public static String[] generateTargetColNames(ResultSet rs, FieldNameConversion optConversion) {
        try {
            ResultSetMetaData metaData = rs.getMetaData();
            int colCnt = metaData.getColumnCount();

            String[] res = new String[colCnt];

            for (int i = 1; i <= colCnt; i++) {
                String colName = metaData.getColumnName(i);
                if (optConversion == FieldNameConversion.ToUpperCase) {
                    colName = colName.toUpperCase();
                } else if (optConversion == FieldNameConversion.ToLowerCase) {
                    colName = colName.toLowerCase();
                } else if (optConversion == FieldNameConversion.ToJavaPropName) {
                    colName = BaseUtils.tableColNameToBeanFieldName(colName);
                }
                res[i - 1] = colName;
            }

            return res;
        } catch (SQLException ex) {
            throw new RuntimeException("error while generating target column names", ex);
        }
    }

    public static Map<String, Object> readOneRowFast(ResultSet rs, String[] targetColNames) {
        try {
            ResultSetMetaData metaData = rs.getMetaData();
            int colCnt = metaData.getColumnCount();

            Map<String, Object> res = new LinkedHashMap<String, Object>(/*colCnt * 4 / 3 + 1*/
                    BaseUtils.calcHashMapCapacity(colCnt));

            for (int i = 1; i <= colCnt; i++) {
                res.put(targetColNames[i - 1], rs.getObject(i));
            }

            return res;
        } catch (SQLException ex) {
            throw new RuntimeException("error reading row", ex);
        }
    }

}
