/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.stringeraser;

import simplelib.LameRuntimeException;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public class ESISqlQuotedStringOld implements IErasableSyntaxItem {

    protected char quoteChar;

    public ESISqlQuotedStringOld(char quoteChar) {
        this.quoteChar = quoteChar;
    }

    public EsiPos findStartPos(final String content, int pos, int searchPosLimit) {
        while (pos < searchPosLimit) {

            if (content.charAt(pos) == quoteChar) {
                final int outerPos = pos;
                return new EsiPos() {
                    {
                        startPos = outerPos;
                        innerStartPos = startPos + 1;
                    }

                    @Override
                    public Pair<Integer, Integer> calcEndPos() {
                        return calcEndPosX(content, innerStartPos);
                    }
                };
            }

            pos++;
        }

        return null;
    }

    protected Pair<Integer, Integer> calcEndPosX(String content, int pos) {
        int len = content.length();

        while (true) {
            int quoteCharPos = content.indexOf(quoteChar, pos);

            if (quoteCharPos == -1) {
                throw new LameRuntimeException("no closing quote-char \"" + quoteChar + "\" after pos=" + pos);
            }

            if (quoteCharPos == len - 1 || content.charAt(quoteCharPos + 1) != quoteChar) {
                return new Pair<Integer, Integer>(quoteCharPos, quoteCharPos + 1);
            }

            pos = quoteCharPos + 2;
        }
    }
}
