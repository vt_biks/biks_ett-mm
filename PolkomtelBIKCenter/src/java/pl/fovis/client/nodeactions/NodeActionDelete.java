/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.LameRuntimeException;

/**
 *
 * @author bfechner
 */
public class NodeActionDelete extends NodeActionUpdatingBase {

    @Override
    public String getCaption() {
        return I18n.usun.get() /* I18N:  */;
    }

    @Override
    public void execute() {
        HTML element = new HTML(getDeleteHTMLText());
        element.setStyleName("komunikat" /* I18N: no:css-class */);
        new ConfirmDialog().buildAndShowDialog(element, I18n.usun.get() /* I18N:  */, new IContinuation() {
                    @Override
                    public void doIt() {
                        final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                        infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaUsuwanie.get(), true);
                        callServiceToDelete(node, new StandardAsyncCallback<List<String>>("Failure on" /* I18N: no */ + " deleteTreeNode") {
                            @Override
                            public void onSuccess(List<String> result) {
                                infoDialog.hideDialog();
                                BIKClientSingletons.showInfo(I18n.usunietoElement.get() /* I18N:  */);
                                ctx.getTreeGrid().deleteNode(node.id);
                                refreshAndRedisplayDataByTreeCode(result);
                                //inaczej nie usuwa z ekranu podlinkowanych do grup (trzeba odswieżyć F5)
                                if (result.size() > 0 && node.treeKind.equals(BIKGWTConstants.TREE_KIND_USERS)/*node.treeCode.equals(BIKGWTConstants.TREE_CODE_USERS)*/) {
                                    refreshRedisplayDataAndSelectRecord(node.parentNodeId);
                                }
                            }

                            @Override
                            public void onFailure(Throwable caught) {
                                infoDialog.hideDialog();
                                LameRuntimeException ex = (LameRuntimeException) caught;
                                Window.alert(ex.getMessage());
                                super.onFailure(caught);
                            }
                        });
                    }
                });
        clipboard.clear();
    }

    protected String getDeleteHTMLText() {
        return I18n.czyNaPewnoUsunacElement.get() /* I18N:  */ + ": <div class='PUWelement'><img src='images/" /* I18N: no */ + BIKClientSingletons.getNodeKindIconNameByCode(node.nodeKindCode, node.treeCode) + "." + "gif" /* I18N: no */ + "'/> " + BaseUtils.encodeForHTMLTag(node.name) + "</div> " + I18n.wrazZElementamiPodrzednymi.get() /* I18N:  */ + "?";
    }

    protected void callServiceToDelete(TreeNodeBean node, AsyncCallback<List<String>> AsyncCallback) {
        BIKClientSingletons.getService().deleteTreeNode(node, AsyncCallback);
    }

    @Override
    public boolean isEnabledByDesign() {
        //ostateczny tryb edycji wszystkiego, wszystko można edytować!
        if (isNodeOneOfTheKind(false, BIKConstants.NODE_KIND_META_BUILT_IN_NODE_KIND)) {
            return false;
        }

        if (node != null && !BIKClientSingletons.isLoggedUserAuthorOfTree(node.treeId)
                && (BIKClientSingletons.isLoggedUserCreatorOfTree(node.treeId) || BIKClientSingletons.isBranchCreatorLoggedIn(node.branchIds))) {
            return data.isAuthor != false ? true : (
                    data.author!=null && 
                    data.author.authorSystemId != null && data.author.authorSystemId.equals(BIKClientSingletons.getLoggedUserBean().id));
        }

        if (isNodeOneOfTheKind(false, BIKConstants.NODE_KIND_LISA_FOLDER)) {
            return node.hasNoChildren;
        }
        if (isInUltimateEditMode()) {
            return true;
        }

        // W wuj brzydko. Wstyd, ale presja czasu jest i bżemek naciska ;/
        if (isDQMReportObject()) {
            return false;
        }
        if (BIKClientSingletons.isLoggedUserAuthorOfTree(node.treeId)) {
            return true;
        }

        //jeżeli nie jest żadnen użytkownik zalogowany
        if ((isNodeLinkedAsRoot() && treeBean.code.equals(BIKGWTConstants.TREE_CODE_USER_ROLES))
                || (isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_USERS_ROLE) && !isNodeLinkedAnyhow())) {
            return false;
        }
        if (!isAdminOrExpertOrAuthorOrEditor()) {
            return false;
        } else {
            if (isNodeLinkedAsRoot() && !isNodeReadOnlyAndBuiltIn()) {
                return true;
            }
            if (isConfluenceObject()) {
                return isNodeBuiltIn();
            }
            if (isInDQMAdminMode()) {
                return !isDQMTest() && !isNodeBuiltIn();
            }

//            if (isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_BLOG)) {
//                // 1. (węzeł nie może być podlinkowany lub jest podlinkowany jako root),
//                // 2. zalogowany użytkownik musi być adminem bloga
//                return isNodeNotLinkedUnderRootNorBuiltinNorReadOnlyNode()
//                        && isLoggedUserInBlogAuthors(true);
//            } else if (isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_BLOG_ENTRY)) {
//                // 1. (węzeł nie może być podlinkowany lub jest podlinkowany jako root),
//                // 2. zalogowany użytkownik musi być autorem wpisu i autorem bloga lub adminem bloga
//                return isNodeNotLinkedUnderRootNorBuiltinNorReadOnlyNode()
//                        && isLoggedUserIsProperAuthorOfBlogEntry();
//            } else {
//                return isNodeNotLinkedUnderRootNorBuiltinNorReadOnlyNode() && isAdminOrExpertOrAuthorOrEditor();
//            }
            return isNodeNotLinkedUnderRootNorBuiltinNorReadOnlyNode() && isAdminOrExpertOrAuthorOrEditor();
        }
    }

    protected boolean isDQMReportObject() {
        return node.linkedNodeId != null && isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_DQM_RATE, BIKGWTConstants.NODE_KIND_DQM_RATE_ACCEPTED, BIKGWTConstants.NODE_KIND_DQM_RATE_REJECTED) && !isNodeFromDynamicTree();
    }
}
