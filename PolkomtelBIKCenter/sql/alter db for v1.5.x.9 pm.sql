exec sp_check_version '1.5.x.9';
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
-- 1. dodanie roli AppAdmin
--   wygl�da niepozornie, a ile nowego wnosi ;-)
-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

insert into bik_right_role (code, caption, rank)
values ('AppAdmin', 'Admin aplikacji', 90)
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

exec sp_update_version '1.5.x.9', '1.5.x.10';
go
