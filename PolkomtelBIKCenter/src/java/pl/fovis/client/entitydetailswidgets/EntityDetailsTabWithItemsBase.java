/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import java.util.List;
import pl.fovis.common.EntityDetailsDataBean;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 *
 * klasa bazowa dla zakładek (subwidgetów) mających listę elementów, które są
 * wyświetlane (standardowy przypadek dla dolnych zakładek)
 *
 */
public abstract class EntityDetailsTabWithItemsBase<ITEM> extends EntityDetailsTabBase {

    protected List<ITEM> items;

    protected abstract List<ITEM> extractItemsFromFetchedData(EntityDetailsDataBean data);

    @Override
    protected void extractAndAssignExtraPropsFromFetchedData() {
        this.items = extractItemsFromFetchedData(getData());
    }

    @Override
    public Integer getItemCount() {
        return BaseUtils.safeGetCollectionSize(items);
    }

    @Override
    public boolean hasContent() {
        return getItemCount() > 0;
    }

    // domyślnie zakładki oparte o listę elementów nie mają kontrolek do dodania
    // nowego elementu, jest oddzielna klasa bazowa dla tych co jednak mają
    // (bo dużo takich przypadków mamy) -->
    // EntityDetailsTabWithItemsInEditableGridBase (to jest podklasa tej klasy)
    @Override
    public boolean hasEditableControls() {
        return false;
    }
}
