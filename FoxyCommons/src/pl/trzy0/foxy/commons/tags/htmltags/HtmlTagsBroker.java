/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.tags.htmltags;

import java.util.Collection;
import java.util.List;
import pl.trzy0.foxy.commons.tags.TagData;
import pl.trzy0.foxy.commons.tags.TagsUtils;
import simplelib.StopWatch;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class HtmlTagsBroker {

    private static ILameLogger logger = LameLoggerFactory.getLogger(HtmlTagsBroker.class);
    //private Map<String, IHtmlTag> tagsById = new HashMap<String, IHtmlTag>();
    //private LameBag<String> idBag = new LameBag<String>();
    //private HtmlTagLibrary lib = new HtmlTagLibrary();
    //private TagForestBuilder<IHtmlTag> forestBuilder = new TagForestBuilder(lib);
    private HtmlTagForestBuilder forestBuilder = new HtmlTagForestBuilder();
    private HtmlTagLibrary lib = forestBuilder.getLibrary();
    
    public List<IHtmlTag> addHtml(String fileName, String html) {
        try {
            return internalAddHtml(fileName, html);
        } catch (Exception ex) {
            logger.errorAndThrowNew("error while processing file: " + fileName, ex);
        }
        return null; // non-reachable in practice
    }

    public Collection<String> getRootIdsInFile(String fileName) {
        return lib.getRootTagsInFile(fileName);
    }

    public List<IHtmlTag> buildHtmlTags(Iterable<TagData> tags) {
        return forestBuilder.buildFoxyTagForest(tags.iterator());
    }

    public void addCreatedTags(Collection<IHtmlTag> tags) {
        for (IHtmlTag tag : tags) {
            lib.addTagToIdMap(tag);
            if (tag.getSubTags() != null) {
                addCreatedTags(tag.getSubTags());
            }
        }
    }

    private List<IHtmlTag> internalAddHtml(String fileName, String html) {
        StopWatch sw = new StopWatch(/*GWTDurationMeter.impl,*/logger.isDebugEnabled());

        if (logger.isDebugEnabled()) {
            logger.debug("addHtml: starts, fileName=\"" + fileName + "\", html size=" + html.length());
        }

        sw.start("parsing");
//        Date start = new Date();
//        if (logger.isDebugEnabled()) {
//            logger.debug("addHtml: starts, fileName=\"" + fileName + "\", html size=" + html.length());
//        }

//        StringStreamParser parser = new StringStreamParser(html, fileName);
////        Date parserReadyTime = new Date();
////        if (logger.isDebugEnabled()) {
////            logger.debug("addHtml: parser ready in " +
////                    SimpleDateUtils.getElapsedSecondsWithMilliPrec(start, parserReadyTime));
////        }
//
//        List<TagData> tags = new ArrayList<TagData>();
//        while (parser.hasNext()) {
//            tags.add(parser.next());
//        }

        List<TagData> tags = TagsUtils.parseHtmlFile(fileName, html);

//        Date tagsParsedTime = new Date();
//        if (logger.isDebugEnabled()) {
//            logger.debug("addHtml: tags parsed (" + tags.size() + ") in " +
//                    SimpleDateUtils.getElapsedSecondsWithMilliPrec(parserReadyTime, tagsParsedTime));
//        }

        sw.start("build tag forest");
//        Date forestBuilderReadyTime = new Date();
//        if (logger.isDebugEnabled()) {
//            logger.debug("addHtml: forestBuilder ready in " +
//                    SimpleDateUtils.getElapsedSecondsWithMilliPrec(tagsParsedTime, forestBuilderReadyTime));
//        }
        List<IHtmlTag> list = buildHtmlTags(tags); //forestBuilder.buildFoxyTagForest(/*parser*/tags.iterator());
//        Date afterBuildTime = new Date();
//        if (logger.isDebugEnabled()) {
//            logger.debug("addHtml: forestBuilder - after build, list.size=" + list.size() + " in " +
//                    SimpleDateUtils.getElapsedSecondsWithMilliPrec(forestBuilderReadyTime, afterBuildTime));
//        }

        //sw.start("putSubTagsIdsInMap");
        //Collection<String> addedIds = HtmlTag.putSubTagsIdsInMap(tagsById, list);
        //idBag.addAll(addedIds);

//        Date end = new Date();

        sw.stop();

        if (logger.isDebugEnabled()) {
            logger.debug("addHtml: done, html size: " + html.length() /*+
                    ", collected id count: " + addedIds.size() *//*+ ", ids added in " +
                    SimpleDateUtils.getElapsedSecondsWithMilliPrec(afterBuildTime, end)*/ + ", time taken:\n" +
                    sw.getExecutedLabelsPretty(2));
        }

        return list;
    }

    public IHtmlTag getById(String id) {
        return lib.getRootTagById(id);
//        int cnt = idBag.getCount(id);
//
//        if (cnt == 0) {
//            logger.errorAndThrowNew("no tag with id=" + id);
//        } else if (cnt > 1) {
//            logger.errorAndThrowNew("reference to tag with id=" + id + " is ambiguous, occurence count=" + cnt);
//        }
//
//        IHtmlTag ht = tagsById.get(id);
//
//        if (ht == null) {
//            logger.errorAndThrowNew("no tag with id=" + id);
//        }
//
//        return ht;
    }
}
