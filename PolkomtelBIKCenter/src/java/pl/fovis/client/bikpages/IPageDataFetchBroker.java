/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.http.client.Request;

/**
 *
 * @author wezyr
 */
public interface IPageDataFetchBroker {
    public static enum TreeDataRequestKind {

        Filtered, Full, Additional
    };

    public void dataFetchStarting(TreeDataRequestKind treeDataRequestKind, Request fetchRequest, String optRequestTicket);

    //public void dataFetchNextRequest(Request fetchRequest);

    // cancel previous request if any and it is still pending
    public void dataFetchCancel();

    public void dataFetchFinished(boolean success);

    public TreeDataRequestKind getRequestInProgressKind();

    // null -> no data is fetched and displayed
    public Boolean isFullDataFetched();
}
