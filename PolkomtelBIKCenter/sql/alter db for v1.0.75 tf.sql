﻿exec sp_check_version '1.0.75';
go

create table aaa_report_object (
    id int identity(1,1) primary key,
    report_node_id int,
    universe_CUID varchar(300) ,
    object_name varchar(255),
    class_name varchar(255),  
)

create table aaa_ids_for_report(
	id int identity(1,1) primary key,
	report_node_id int,
	object_node_id int
)

exec sp_update_version '1.0.75', '1.0.76';
go