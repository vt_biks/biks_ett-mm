/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.INewSearchPage;
import pl.fovis.client.widgets.sbavc.SearchByAttrValueCriteriaManager;
import pl.fovis.common.AttrSearchableBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class AdvancedSearchDialog extends BaseActionOrCancelDialog implements IAdvancedSearchDialog {

    private static final String ADVANCED_SEARCH_HINT_NAME = "AdvancedSearch";
    protected SearchByAttrValueCriteriaManager manager = new SearchByAttrValueCriteriaManager(this);
//    protected FlexTable advancedSearchTable = new FlexTable();
    protected PushButton addBtn;
    protected List<AttrSearchableBean> attrs;
    protected INewSearchPage searchPage;
    protected TextBox groupingExprTb = new TextBox();

    public AdvancedSearchDialog(INewSearchPage searchPage) {
        this.searchPage = searchPage;
    }

    public void buildAndShowDialog(String opExpr) {
        prepareCriteria(opExpr);
        super.buildAndShowDialog();
    }

//    private int findNewPositon(List<AttrSearchableBean> result, int selectedId) {
//        for (int i = 0; i < result.size(); i++) {
//            if (result.get(i).id == selectedId) {
//                return i;
//            }
//        }
//        return -1;
//    }
    @Override
    public void updateAttributes(List<AttrSearchableBean> result, String lastOpExpr) {
//        if (result.isEmpty()) {
//            advancedSearchTable.removeAllRows();
//        } else {
//            int nRow = advancedSearchTable.getRowCount();
//            for (int i = 0; i < nRow; i++) {
//                ListBox oldLbox = (ListBox) advancedSearchTable.getWidget(i, 0);
//                int selectedId = getAttrIdBySelectedIndex(oldLbox.getSelectedIndex());
//
//                ListBox newLbox = buildListBox(result);
//                if (selectedId != -1) {
//                    newLbox.setSelectedIndex(findNewPositon(result, selectedId));
//                }
//                advancedSearchTable.setWidget(i, 0, newLbox);
//            }
//        }
        groupingExprTb.setText(lastOpExpr);
        manager.populateWidgets(result);
        searchPage.updateAttributeShowedList(getAttributesToSearch(), getGroupingExpr());
        attrs = result;
    }

    public List<ISearchByAttrValueCriteria> getAttributesToSearch() {
        List<ISearchByAttrValueCriteria> criteriaToSearch = manager.getCriteriaToSearch();
        if (criteriaToSearch.isEmpty()) {
            return null;
        }
        return criteriaToSearch;
//        if (advancedSearchTable == null || advancedSearchTable.getRowCount() == 0) {
//            return null;
//        }

//        List<ISearchByAttrValueCriteria> sbavcs = new ArrayList<ISearchByAttrValueCriteria>();
//        int nRow = advancedSearchTable.getRowCount();
//        for (int i = 0; i < nRow; i++) {
//            ListBox attLbox = (ListBox) advancedSearchTable.getWidget(i, 0);
//            int selectedId = attLbox.getSelectedIndex();
//            int attrId = getAttrIdBySelectedIndex(selectedId);
//            String attrName = attLbox.getItemText(selectedId);
//            TextBox value1Tb, value2Tb;
//            switch (((ListBox) advancedSearchTable.getWidget(i, 1)).getSelectedIndex()) {
//                case 0:
//                    sbavcs.add(new SearchByAttrValueCriteriaNotEmpty(attrId, attrName));
//                    break;
//                case 1:
//                    value1Tb = (TextBox) advancedSearchTable.getWidget(i, 2);
//                    sbavcs.add(new SearchByAttrValueCriteriaEquals(attrId, attrName, value1Tb.getValue()));
//                    break;
//                case 2:
//                    value1Tb = (TextBox) advancedSearchTable.getWidget(i, 2);
//                    sbavcs.add(new SearchByAttrValueCriteriaContains(attrId, attrName, value1Tb.getValue()));
//                    break;
//                case 3:
//                    value1Tb = (TextBox) advancedSearchTable.getWidget(i, 2);
//                    sbavcs.add(new SearchByAttrValueCriteriaIn(attrId, attrName, value1Tb.getValue()));
//                    break;
//                case 4:
//                    value1Tb = (TextBox) advancedSearchTable.getWidget(i, 2);
//                    value2Tb = (TextBox) advancedSearchTable.getWidget(i, 3);
//                    sbavcs.add(new SearchByAttrValueCriteriaBetween(attrId, attrName, value1Tb.getValue(), value2Tb.getValue()));
//                    break;
//            }
//        }
//        return sbavcs;
    }

    protected void addAttributeToSearch() {
        manager.addRow();
//        int rowCount = advancedSearchTable.getRowCount();
//        advancedSearchTable.insertRow(rowCount);
//
//        ListBox attrsLbox = buildListBox(attrs);
//
//        advancedSearchTable.setWidget(rowCount, 0, attrsLbox);
//
//        final FoxyListBox<Integer> typeLbox = new FoxyListBox<Integer>();
//        typeLbox.addItemWithValue(I18n.noValueAttribute.get(), 0);
//        typeLbox.addItemWithValue(I18n.singleValueAttributeEquals.get(), 1);
//        typeLbox.addItemWithValue(I18n.singleValueAttributeContains.get(), 1);
//        typeLbox.addItemWithValue(I18n.singleValueAttributeIn.get(), 1);
//        typeLbox.addItemWithValue(I18n.multipleValueAttribute.get(), 2);
//        typeLbox.addChangeHandler(new ChangeHandler() {
//
//            @Override
//            public void onChange(ChangeEvent event) {
//                int row = findRow(event.getSource());
//                //Window.alert("row: " + row);
////                int choosen = ((ListBox) event.getSource()).getSelectedIndex();
//
//                advancedSearchTable.removeCells(row, 2, advancedSearchTable.getCellCount(row) - 3);
////                if (choosen > 0) {
////                    advancedSearchTable.insertCell(row, 2);
////                    advancedSearchTable.setWidget(row, 2, new TextBox());
////                    if (choosen == 2) {
////                        advancedSearchTable.insertCell(row, 3);
////                        advancedSearchTable.setWidget(row, 3, new TextBox());
////                    }
////                }
//                Integer textBoxCount = typeLbox.getSelectedValue();
//                for (int i = 0; i < textBoxCount; i++) {
//                    advancedSearchTable.insertCell(row, 2 + i);
//                    advancedSearchTable.setWidget(row, 2 + i, new TextBox());
//                }
//            }
//
//            private int findRow(Object source) {
//                int nRow = advancedSearchTable.getRowCount();
//                for (int i = 0; i < nRow; i++) {
//                    int nCol = advancedSearchTable.getCellCount(i);
//                    for (int j = 0; j < nCol; j++) {
//                        if (BaseUtils.safeEquals(source, advancedSearchTable.getWidget(i, j))) {
//                            return i;
//                        }
//                    }
//                }
//                return -1;
//            }
//        });
//        advancedSearchTable.setWidget(rowCount, 1, typeLbox);
//
//        Image removeBtn = new Image("images/delete.png");
//        removeBtn.addClickHandler(new ClickHandler() {
//
//            @Override
//            public void onClick(ClickEvent event) {
//                int rowIndex = advancedSearchTable.getCellForEvent(event).getRowIndex();
//                advancedSearchTable.removeRow(rowIndex);
//            }
//        });
//        advancedSearchTable.setWidget(rowCount, advancedSearchTable.getCellCount(rowCount), removeBtn);
    }

    @Override
    protected String getDialogCaption() {
        return I18n.wyszukiwanieZaawansowane.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.szukaj.get();
    }

    @Override
    protected String getCancelButtonCaption() {
        return I18n.anuluj.get();
    }

    @Override
    protected void buildMainWidgets() {
//        advancedSearchTable.setCellSpacing(2);
        HorizontalPanel headHp = new HorizontalPanel();
        headHp.setWidth("100%");
        Widget hintBtn = BIKClientSingletons.createHelpHintButton(ADVANCED_SEARCH_HINT_NAME, I18n.wyszukiwanieZaawansowane.get());
        headHp.add(new Label(I18n.podajKryterium.get()));
        headHp.add(hintBtn);
        headHp.setCellHorizontalAlignment(hintBtn, HasHorizontalAlignment.ALIGN_RIGHT);
        main.add(headHp);
        addBtn = new PushButton(I18n.dodajAtrybut.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                addAttributeToSearch();
            }
        });
        NewLookUtils.makeCustomPushButton(addBtn);

        main.add(addBtn);
        main.add(manager.getWidget());
        main.add(createOperationGroupingPanel());
//        main.add(new HTML(I18n.advancedSearchDataInfo.get()));
//        main.add(advancedSearchTable);
    }

    @Override
    protected void doAction() {
        searchPage.setHasNewAttributes(true);
        TextBox searchTbx = BIKClientSingletons.getGlobalSearchTextBox();
        if (BaseUtils.isStrEmpty(searchTbx.getText())) {
            searchTbx.setText("*");
            searchPage.getSearchPhraseTb().setText("*");
        }
        searchPage.performSearchAndShow();
    }

    @Override
    protected void doCancel() {
        manager.clearCriteria();
        searchPage.setHasNewAttributes(false);
        super.doCancel();
    }

//    private ListBox buildListBox(List<AttrSearchableBean> result) {
//        ListBox lbox = new ListBox();
//        //Window.alert("Building lbox");
//        if (result != null) {
//            for (AttrSearchableBean attrSearchable : result) {
//                lbox.addItem(attrSearchable.caption);
//            }
//        }
//        return lbox;
//    }
//
//    private int getAttrIdBySelectedIndex(int selectedIndex) {
//        //Window.alert("id: " + attrsSearchable.get(selectedIndex).id);
//        //nie jest wybrana zadna opcja
//        if (selectedIndex < 0 || attrs == null) {
//            return -1;
//        }
//        return attrs.get(selectedIndex).id;
//    }
    private DisclosurePanel createOperationGroupingPanel() {
        DisclosurePanel panel = new DisclosurePanel(I18n.zgrupowanieOperacji.get());
        panel.add(groupingExprTb);
        return panel;
    }

    @Override
    public void resetGroupingExprTb() {
        List<String> ids = new ArrayList<String>();
        for (int id = 0; id < manager.getCurretnSelectedCriteria(); id++) {
            ids.add(BaseUtils.createPlaceHolder(id + 1, BIKConstants.OPEN_PLACEHOLDER, BIKConstants.CLOSE_PLACEHOLDER)); //bo ludzie zaczynaja od 1
        }
        groupingExprTb.setText(BaseUtils.mergeWithSepEx(ids, " AND "));
    }

    @Override
    public String getGroupingExpr() {
        return groupingExprTb.getText();
    }

    @Override
    public void setGroupingExpr(String savedGroupingExpr) {
        groupingExprTb.setText(savedGroupingExpr);
    }

    public void enableSearchBtn(boolean enable) {
        actionBtn.setEnabled(enable);
    }

    @Override
    protected boolean doActionBeforeClose() {
        manager.storeCriteria();
        List<ISearchByAttrValueCriteria> criteriaToSearch = getAttributesToSearch();
        if (criteriaToSearch == null) {
            return true;
        }
        for (ISearchByAttrValueCriteria bean : criteriaToSearch) {
            if (bean == null) {
                BIKClientSingletons.showWarning(I18n.podanaWartoscNieJestLiczba.get());
                return false;
            }
        }
        return true;
    }

    public void prepareCriteria(String opExpr) {
        manager.prepareCriteria();
        groupingExprTb.setText(opExpr);
    }
}
