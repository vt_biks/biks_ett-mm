﻿exec sp_check_version '1.0.19';
go
----------------------------------------------------------------------
----------------------------------------------------------------------
update bik_tree
set node_kind_id = 
(select id from bik_node_kind where code='_BIZAREA_')
where tree_kind = 1
go
----------------------------------------------------------------------
----------------------------------------------------------------------

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted)
select NULL,id,'Słowa kluczowe',(select id from bik_tree where name='Tezaurus'),NULL,NULL,NULL,NULL,0 
from bik_node_kind where code='TezaurusCategory'

update bik_node set branch_ids=convert(varchar, id) + '|' where name='Słowa kluczowe'
----------------------------------------------------------------------
----------------------------------------------------------------------
exec sp_update_version '1.0.19', '1.0.20';
go

