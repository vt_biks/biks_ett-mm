/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class ViewSQLDialog extends ViewSomethingInBIKSDialog {

//    String title;
    String body;

    @Override
    protected String getDialogCaption() {
        return I18n.zapytanieSQL.get() /* I18N:  */;
    }

    public void buildAndShowDialog(String title, String body) {
        this.body = body;
        super.buildAndShowDialog(title);
    }

    @Override
    protected Widget getContentWidget() {
        String sqlText = BaseUtils.replaceUnixNewLinesToCorrectNewLine(BaseUtils.encodeForHTMLTag(body).replace(" ", "&" + "nbsp" /* I18N: no */ + ";"));
        sqlText = BIKCenterUtils.createSQLSyntaxHighlighterHTML(sqlText);

        // bez kolorowania, zamiana tylko nowe linie na <BR>'ki
//        String sqlText = BaseUtils.replaceNewLinesToRowSep(BaseUtils.encodeForHTMLTag(body).replace(" ", "&" + "nbsp" /* I18N: no */ + ";"), "");
        HTML label2 = new HTML(sqlText);
        label2.setStyleName("def-content");
        return label2;
    }
}
