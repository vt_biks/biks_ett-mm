/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class DQMDefProfileBean {

    public String fileName;
    public int isDefFile;
    public Date ztjd;
    public Double tamt;
    public String custacc;
    public String custad1;
    public String custad2;
    public String custad3;
    public String custad4;
    public String benacc;
    public String benad1;
    public String benad2;
    public String benad3;
    public String benad4;
    public String ref1;
    public String ref2;
    public String ref3;
    public String ref4;

    public DQMDefProfileBean() {
    }

    public DQMDefProfileBean(String fileName, int isDefFile, Date ztjd, Double tamt, String custacc, String custad1, String custad2, String custad3, String custad4, String benacc, String benad1, String benad2, String benad3, String benad4, String ref1, String ref2, String ref3, String ref4) {
        this.fileName = fileName;
        this.isDefFile = isDefFile;
        this.ztjd = ztjd;
        this.tamt = tamt;
        this.custacc = custacc;
        this.custad1 = custad1;
        this.custad2 = custad2;
        this.custad3 = custad3;
        this.custad4 = custad4;
        this.benacc = benacc;
        this.benad1 = benad1;
        this.benad2 = benad2;
        this.benad3 = benad3;
        this.benad4 = benad4;
        this.ref1 = ref1;
        this.ref2 = ref2;
        this.ref3 = ref3;
        this.ref4 = ref4;
    }

    @Override
    public String toString() {
        return "DQMDefProfileBean{" + "fileName=" + fileName + ", isDefFile=" + isDefFile + ", ztjd=" + ztjd + ", tamt=" + tamt + ", custacc=" + custacc + ", custad1=" + custad1 + ", custad2=" + custad2 + ", custad3=" + custad3 + ", custad4=" + custad4 + ", benacc=" + benacc + ", benad1=" + benad1 + ", benad2=" + benad2 + ", benad3=" + benad3 + ", benad4=" + benad4 + ", ref1=" + ref1 + ", ref2=" + ref2 + ", ref3=" + ref3 + ", ref4=" + ref4 + '}';
    }

}
