﻿exec sp_check_version '1.6.z9.11';
go


if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_attr_def]') and name = 'display_as_number')
begin
	alter table bik_attr_def add display_as_number int not null default (0);
end;
go

if not exists(select * from bik_node_action where code ='ShowDateAndAuthor')
begin
	insert into bik_node_action(code)
	values ('ShowDateAndAuthor')
end;

exec sp_update_version '1.6.z9.11', '1.6.z9.12';
go