/*
 * Pair.java
 *
 * Created on 12 styczeń 2007, 17:16
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package simplelib;

import java.io.Serializable;

/**
 *
 * @author wezyr
 */
public class Pair<T1, T2> implements Serializable {

    public T1 v1;
    public T2 v2;

    // for serialization
    public Pair() {
        //no-op
    }

    public Pair(T1 v1, T2 v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    @Override
    public String toString() {
        return "(" + v1 + "," + v2 + ")";
    }

    @Override
    public int hashCode() {
        return (v1 == null ? 0 : v1.hashCode() * 31) + (v2 == null ? 0 : v2.hashCode());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Pair) {
            Pair pair2 = (Pair) obj;
            return BaseUtils.safeEquals(v1, pair2.v1) && BaseUtils.safeEquals(v2, pair2.v2);
        } else {
            return false;
        }
    }

    public static <T1, T2> T1 getV1(Pair<T1, T2> p, T1 defForNullPair, T1 defForNullV1) {
        if (p == null) {
            return defForNullPair;
        }
        return BaseUtils.nullToDef(p.v1, defForNullV1);
    }

    public static <T1, T2> T1 getV1(Pair<T1, T2> p, T1 defForNull) {
        return getV1(p, defForNull, defForNull);
    }

    public static <T1, T2> T1 getV1(Pair<T1, T2> p) {
        return getV1(p, null);
    }

    public static <T1, T2> T2 getV2(Pair<T1, T2> p, T2 defForNullPair, T2 defForNullV2) {
        if (p == null) {
            return defForNullPair;
        }
        return BaseUtils.nullToDef(p.v2, defForNullV2);
    }

    public static <T1, T2> T2 getV2(Pair<T1, T2> p, T2 defForNull) {
        return getV2(p, defForNull, defForNull);
    }

    public static <T1, T2> T2 getV2(Pair<T1, T2> p) {
        return getV2(p, null);
    }
}
