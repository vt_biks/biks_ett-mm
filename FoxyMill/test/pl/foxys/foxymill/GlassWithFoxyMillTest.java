/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill;

import org.junit.Test;
import pl.trzy0.foxy.commons.ITextProvider;
import pl.trzy0.foxy.glass.Glass;
import pl.trzy0.foxy.glass.GlassToText;

/**
 *
 * @author witzar
 */
public class GlassWithFoxyMillTest {

    private static class TestTextProvider implements ITextProvider {

        public String getText(String key) {
            if ("subject".equals(key)) {
                return "temat";
            }
            return key;
        }
    }

    @Test
    public void test() {
        Glass g = new Glass(
                "Wartosc pola %{p0}% musi być %{p1 ? 'większa' : 'mniejsza'}% niż %{p2}%",
                new Glass("subject"), false, 123);
        GlassToText gp = new GlassToText(null,
                new FoxyMillNamedTemplateProvider(new TestTextProvider()));
        String s = gp.parse(g, null);
        System.out.println("s=" + s);
    }
}
