/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.common.SystemUserBean;

/**
 *
 * @author bfechner
 */
public class PickSystemUserDialog extends PickUserDialogBase<SystemUserBean> {

    @Override
    protected String getName(SystemUserBean b) {
        return b.userName;
    }

    @Override
    protected Integer getId(SystemUserBean b) {
        return b.id;
    }
}
