/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.dqm;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.SchedulePageBase;
import pl.fovis.client.dialogs.EntityDetailsPaneDialog;
import static pl.fovis.client.entitydetailswidgets.dqm.DQMLogBeanExtractor.setTitleForServerName;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.dqm.DQMScheduleBean;
import pl.fovis.common.dqm.DQMScheduleWidgetBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class DQMSchedulePage extends SchedulePageBase<DQMScheduleBean, DQMScheduleWidgetBean> {

//    protected Map<Integer, List<TextBox>> testToTimeTextBoxMap = new HashMap<Integer, List<TextBox>>();
    protected Map<Integer, List<DQMScheduleWidgetBean>> testToSubWidgetsMap = new HashMap<Integer, List<DQMScheduleWidgetBean>>();
    protected Map<Integer, List<DQMScheduleBean>> multiTests = new HashMap<Integer, List<DQMScheduleBean>>();

//    protected void aqq() {
//        CellTable<DQMScheduleBean> ct = new CellTable<DQMScheduleBean>();
//
//        TextColumn<DQMScheduleBean> tc = new TextColumn<DQMScheduleBean>() {
//
//            @Override
//            public String getValue(DQMScheduleBean object) {
//                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//            }
//        };
//
//    }
    @Override
    public String getId() {
        return "admin:dqm:schedule";
    }

    @Override
    public boolean isPageEnabled() {
        return BIKClientSingletons.isConnectorAvailable(PumpConstants.SOURCE_NAME_DQM) && super.isPageEnabled()/*BIKClientSingletons.isSysAdminLoggedInEx(getId())*/;
    }

    @Override
    protected void getSchedules(StandardAsyncCallback<Pair<Integer, List<DQMScheduleBean>>> async) {
        BIKClientSingletons.getDQMService().getDQMSchedules(pwh.pageNum, pwh.pageSize, async);
    }

    @Override
    protected void createHeaders() {
        grid.setWidget(0, 0, new HTML()); // icon
        grid.setWidget(0, 1, new HTML("<b>" + I18n.nazwaTestu.get() /* I18N:  */ + "</b>"));
        if (BIKClientSingletons.useDqmConnectionMode()) {
            grid.setWidget(0, 2, new HTML("<b>" + I18n.polaczenie.get() /* I18N:  */ + "</b>"));
            grid.setWidget(0, 3, new HTML());
            // TF: poniższe rzuca bledam w IE - komentuje, bo zbedne!
//            grid.getCellFormatter().setWidth(0, 3, "0px");
        } else {
            grid.setWidget(0, 2, new HTML("<b>" + I18n.konektor.get() /* I18N:  */ + "</b>"));
            grid.setWidget(0, 3, new HTML("<b>" + I18n.system.get() /* I18N:  */ + "</b>"));
        }
//        grid.setWidget(0, 4, new HTML("<b>" + I18n.bazaDanych.get() /* I18N:  */ + "</b>"));
        grid.setWidget(0, 4, new HTML("<b>" + I18n.uruchamiany.get() /* I18N:  */ + "</b>"));
        grid.setWidget(0, 5, new HTML("<b>" + I18n.godzina.get() /* I18N:  */ + " (hh:mm)</b>"));
        grid.setWidget(0, 6, new HTML("<b>" + I18n.dataRozpoczecia.get() /* I18N:  */ + "</b>"));
        grid.setWidget(0, 7, new HTML("<b>" + I18n.harmonogramAktywny.get() /* I18N:  */ + "</b>"));
        grid.setWidget(0, 8, new HTML("<b>" + I18n.uruchomTest.get() /* I18N:  */ + "</b>"));
    }

    @Override
    protected void populateRow(DQMScheduleBean bean) {
        DQMScheduleWidgetBean widgetBean = widgets.get(bean.id);
        populateWidgets(widgetBean, bean);
    }

    protected void gridSetWidget(int row, int column, Widget widget) {
        grid.setWidget(row, column, widget);
    }

    protected int mainTestRowNum;

    @Override
    protected void createRow(int row, final DQMScheduleBean bean) {
        boolean isMultiSourceTestSchedule = bean.sourceCount > 0 && bean.multiSourceId != null;
//        final DQMScheduleWidgetBean widgetBean = new DQMScheduleWidgetBean(grid, row, isMultiSourceTestSchedule ? null : 6);
        boolean isTimeVisible = bean.sourceCount == 0 || bean.multiSourceId != null;
//        Integer startTimeCol = isTimeVisible ? 6 : null;
//        if (BIKClientSingletons.useDqmConnectionMode() && startTimeCol != null) {
//            startTimeCol--;
//        }
        final DQMScheduleWidgetBean widgetBean = new DQMScheduleWidgetBean(grid, row, isTimeVisible ? 6 : null);
        setWidgetsFromBean(widgetBean, bean);
        widgetBean.name = bean.name;
        widgetBean.databaseName = bean.databaseName;
        widgetBean.testNodeId = bean.testNodeId;
        widgetBean.instance = bean.source.trim();
        widgetBean.multiSourceId = bean.multiSourceId;
        widgetBean.connectionName = bean.connectionName;
//        widgetBean.serverName = bean.serverName;
//        widgetBean.instanceName = bean.instanceName;
//        widgetBean.fileName = bean.fileName;
        widgetBean.icon = BIKClientSingletons.createIconImgByCode(bean.icon);
        if (!isMultiSourceTestSchedule) {
            mainTestRowNum++;

            gridSetWidget(row, 0, widgetBean.icon);
            Label name = new Label(widgetBean.name);
            name.setStyleName("btnPointer");
            name.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    EntityDetailsPaneDialog edpd = new EntityDetailsPaneDialog();
                    edpd.buildAndShowDialog(bean.testNodeId, null, bean.testTreeCode);
                }
            });
            gridSetWidget(row, 1, name);
        }
        if (BIKClientSingletons.useDqmConnectionMode()) {
            gridSetWidget(row, 2, new Label(BaseUtils.safeToStringDef(widgetBean.connectionName, "")));
            gridSetWidget(row, 3, new Label(""));
        } else {
            gridSetWidget(row, 2, new Label(widgetBean.instance != null ? widgetBean.instance : ""));
            Label serverName = new Label(bean.serverName);
            setTitleForServerName(serverName, bean.serverName, bean.testInstanceName, bean.fileName, bean.databaseName);
            gridSetWidget(row, 3, serverName);
        }
//        gridSetWidget(row, 4, new Label(widgetBean.databaseName != null ? widgetBean.databaseName : ""));
        if (isTimeVisible) {
            gridSetWidget(row, 5, widgetBean.time);
            gridSetWidget(row, 6, widgetBean.startTimeFake);
        }
        if (!isMultiSourceTestSchedule) {
            gridSetWidget(row, 4, widgetBean.interval);
//            gridSetWidget(row, 6, widgetBean.startTime);
//            gridSetWidget(row, 6, widgetBean.startTimeFake);
            gridSetWidget(row, 7, widgetBean.isSchedule);
            PushButton runTest = new PushButton(I18n.uruchom.get());
            NewLookUtils.makeCustomPushButton(runTest);
            runTest.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    BIKClientSingletons.getDQMService().runDQMTest(bean.testId, new StandardAsyncCallback<Void>("Error in runDQMTest") {

                        @Override
                        public void onSuccess(Void result) {
                            BIKClientSingletons.showInfo(I18n.uruchomionoTest.get() + bean.name);
                            invalidateDQMTab();
                        }
                    });
                }
            });
            gridSetWidget(row, 8, runTest);
        }
        if (isMultiSourceTestSchedule) {
//            List<TextBox> list = testToTimeTextBoxMap.get(bean.testNodeId);
//            if (list == null) {
//                list = new ArrayList<TextBox>();
//            }
//            list.add(widgetBean.time);
//            testToTimeTextBoxMap.put(bean.testNodeId, list);
            List<DQMScheduleWidgetBean> list = testToSubWidgetsMap.get(bean.testNodeId);
            if (list == null) {
                list = new ArrayList<DQMScheduleWidgetBean>();
            }
            list.add(widgetBean);
            testToSubWidgetsMap.put(bean.testNodeId, list);
            HTML status = new HTML();
            if (bean.statusFlag == 1) {
                status.setHTML("...");
            } else if (bean.statusFlag == 2) {
                status.setHTML("&#10003;");
                status.setTitle(I18n.status.get() + ": " + bean.lastLog);
            }
            gridSetWidget(row, 7, status);
        }
        widgets.put(widgetBean.id, widgetBean);

        if (mainTestRowNum % 2 != 0) {
            grid.getRowFormatter().addStyleName(row, "evenTestRow");
        }

        setWidgetEnabled(widgetBean, false, false);
    }

    @Override
    protected DQMScheduleBean getScheduleBean() {
        return new DQMScheduleBean();
    }

    @Override
    protected void setSchedules(List<DQMScheduleBean> schedules, StandardAsyncCallback<Void> async) {
        BIKClientSingletons.getDQMService().setDQMSchedules(schedules, async);
    }

    @Override
    protected void setAddsBeanFields(DQMScheduleWidgetBean scheduleBean, DQMScheduleBean newBean) {
        newBean.testNodeId = scheduleBean.testNodeId;
        newBean.multiSourceId = scheduleBean.multiSourceId;
        Date newDate = new Date();
        int actualYear = newBean.dateStarted == null ? SimpleDateUtils.getYear(newDate) : SimpleDateUtils.getYear(newBean.dateStarted);
        int actualMonth = newBean.dateStarted == null ? SimpleDateUtils.getMonth(newDate) : SimpleDateUtils.getMonth(newBean.dateStarted);
        int actualDay = newBean.dateStarted == null ? SimpleDateUtils.getDay(newDate) : SimpleDateUtils.getDay(newBean.dateStarted);
        Date actualDate = SimpleDateUtils.newDate(actualYear, actualMonth, actualDay, newBean.hour, newBean.minute);
        if (actualDate.before(newDate) && newBean.dateStarted == null) {
            actualDate = SimpleDateUtils.addDays(actualDate, 1);
        }
        newBean.scheduleDateWithTime = actualDate;
    }

    @Override
    protected void doOptActionAfterSave() {
        activatePage(true, null, true);
        invalidateDQMTab();
    }

    @Override
    protected void setAddsWidgetEnabled(DQMScheduleWidgetBean widgetBean, boolean isEnabled, boolean checkEnabling) {
//        List<TextBox> list = testToTimeTextBoxMap.get(widgetBean.testNodeId);
//        if (!BaseUtils.isCollectionEmpty(list)) {
//            for (TextBox tbx : list) {
//                tbx.setEnabled(isEnabled);
//            }
//        }
        List<DQMScheduleWidgetBean> list = testToSubWidgetsMap.get(widgetBean.testNodeId);
        if (!BaseUtils.isCollectionEmpty(list)) {
            for (DQMScheduleWidgetBean w : list) {
                if (checkEnabling) {
                    isEnabled = w.isSchedule.getValue();
                }
                w.time.setEnabled(isEnabled);
                w.setStartTimeEnabled(isEnabled);
            }
        }
    }

    protected void invalidateDQMTab() {
        BIKClientSingletons.invalidateTabData(BIKConstants.TREE_CODE_DQM);
    }

    @Override
    protected void doOptFix(List<DQMScheduleBean> beans) {
        // przygotowanie mapki
        multiTests.clear();
        for (DQMScheduleBean bean : beans) {
            List<DQMScheduleBean> multiSourceList = multiTests.get(bean.testNodeId);
            if (multiSourceList == null) {
                multiSourceList = new ArrayList<DQMScheduleBean>();
            }
            multiSourceList.add(bean);
            multiTests.put(bean.testNodeId, multiSourceList);
        }

        for (DQMScheduleBean bean : beans) {
            List<DQMScheduleBean> multiSourceList = multiTests.get(bean.testNodeId);
            if (multiSourceList == null || multiSourceList.size() < 2) {
                continue;
            }
            fixMultiSources(multiSourceList);
        }
    }

    protected void fixMultiSources(List<DQMScheduleBean> beans) {
        DQMScheduleBean mainTest = null;
        DQMScheduleBean maxTime = null;
        for (DQMScheduleBean bean : beans) {
            if (bean.multiSourceId == null) {
                mainTest = bean;
            } else {
                maxTime = checkAndSetMaxTimeBean(bean, maxTime);
            }
        }
        for (DQMScheduleBean bean : beans) {
            if (bean.equals(mainTest)) { // fix main test
                bean.hour = maxTime.hour;
                bean.minute = maxTime.minute;
                bean.minute++;
                bean.dateStarted = maxTime.dateStarted;
                if (bean.minute == 60) {
                    bean.minute = 0;
                    bean.hour++;
                    if (bean.hour == 24) {
                        bean.hour = 23;
                        bean.minute = 59;
                    }
                }
            } else { // fix other tests
                bean.isSchedule = mainTest.isSchedule;
//                bean.dateStarted = mainTest.dateStarted;
                bean.interval = mainTest.interval;
            }
        }
    }

    protected DQMScheduleBean checkAndSetMaxTimeBean(DQMScheduleBean bean, DQMScheduleBean maxTime) {
        if (maxTime == null) {
            return bean;
        }
        if (bean.scheduleDateWithTime.after(maxTime.scheduleDateWithTime)) {
            return bean;
        }
        return maxTime;
    }

    @Override
    protected int getColumnCount() {
        return 9;
    }
//
//    @Override
//    protected void setAddsWidgetBean(DQMScheduleWidgetBean widgetBean, DQMScheduleBean bean) {
//        widgetBean.lastLog = bean.lastLog;
//    }
}
