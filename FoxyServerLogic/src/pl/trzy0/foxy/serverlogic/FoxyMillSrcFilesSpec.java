/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.ILameResourceProvider;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class FoxyMillSrcFilesSpec {

    private static ILameLogger logger = LameLoggerFactory.getLogger(FoxyMillSrcFilesSpec.class);
    public Map<String, ResourceDirSpec> dirs;
    public Map<String, Long> fixedFiles;

    public static FoxyMillSrcFilesSpec makeInitial(Collection<String> dirs,
            Collection<String> fixedFiles, ILameResourceProvider resProvider) {
        FoxyMillSrcFilesSpec res = new FoxyMillSrcFilesSpec();
        res.dirs = new HashMap<String, ResourceDirSpec>();
        for (String dir : dirs) {
            Pair<String, String> p = BaseUtils.splitString(dir, "|");
            if (p.v2 == null) {
                p.v2 = ".html";
            }
            ResourceDirSpec dirSpec = ResourceDirSpec.makeInitial(p.v1, p.v2, true, resProvider);
            res.dirs.put(dir, dirSpec);
        }
        res.fixedFiles = ResourceDirSpec.collectLastModifiedOfFiles(fixedFiles, resProvider);
        return res;
    }

    public static FoxyMillSrcFilesSpec calcChangedSpec(FoxyMillSrcFilesSpec spec, ILameResourceProvider resProvider) {
        FoxyMillSrcFilesSpec res = new FoxyMillSrcFilesSpec();
        res.dirs = new HashMap<String, ResourceDirSpec>();

        boolean modified = false;
        for (ResourceDirSpec oldDirSpec : spec.dirs.values()) {
            ResourceDirSpec newDirSpec = ResourceDirSpec.calcChangedResourceDirSpec(oldDirSpec, resProvider);
            if (newDirSpec == null) {
                newDirSpec = oldDirSpec;
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("detected change in dir: " + oldDirSpec.path);
                }
                modified = true;
            }
            res.dirs.put(oldDirSpec.path, newDirSpec);
        }

        res.fixedFiles = ResourceDirSpec.collectLastModifiedOfFiles(spec.fixedFiles.keySet(), resProvider);
        if (!modified) {
            modified = !ResourceDirSpec.lastModifiedEquals(spec.fixedFiles, res.fixedFiles);
        }
        return modified ? res : null;
    }
}
