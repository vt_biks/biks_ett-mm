/*
 * DispatchingResourceProvider.java
 *
 * Created on 10 październik 2007, 12:33
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package commonlib;

import simplelib.LameRuntimeException;
import simplelib.Pair;
import java.util.Map;

/**
 *
 * @author wezyr
 * Jesli nie znajdzie wsrod subproviderow - przekazuje pare
 */
public class DispatchingResourceProvider implements IResourceProvider {

    protected Map<String, IResourceProvider> subProviders;
    protected IResourceProvider parent;

    public DispatchingResourceProvider(Map<String, IResourceProvider> subProviders,
            IResourceProvider parent) {
        this.subProviders = subProviders;
        this.parent = parent;
    }

    public IResourceProvider getSubProviderByPrefix(String prefix) {
        return subProviders.get(prefix);
    }

    protected Pair<IResourceProvider, String> getSubProvider(String resourceName) {
        Pair<String, String> ss = LameUtils.splitString(resourceName, "://");
        String prefix, resNameForSubProvider;

        if (ss.v2 == null) {
            prefix = "";
            resNameForSubProvider = ss.v1;
        } else {
            prefix = ss.v1;
            resNameForSubProvider = ss.v2;
        }

        IResourceProvider subProvider = subProviders.get(prefix);

//        System.out.println("getSubProvider: resourceName: " + resourceName +
//                ", prefix: " + prefix +
//                ", resNameForSubProvider: " + resNameForSubProvider +
//                ", subProvider.class: " + LameUtils.safeGetClassName(subProvider));

        if (subProvider == null) {
            if (parent == null) {
                throw new LameRuntimeException(
                        "getSubProvider: cannot find subProvider for resource: " + resourceName + ", prefix: " + prefix + ", providers: " + subProviders.keySet());
            }
            return null;
        /*            subProvider = parent;
        resNameForSubProvider = resourceName;
         */        }

        return new Pair<IResourceProvider, String>(subProvider, resNameForSubProvider);
    }

    public long getLastModified(String resourceName) {
        Pair<IResourceProvider, String> p = getSubProvider(resourceName);
        return p == null ? parent.getLastModified(resourceName) : p.v1.getLastModified(p.v2);
    }

    public byte[] loadAsBytes(String resourceName) {
        Pair<IResourceProvider, String> p = getSubProvider(resourceName);
        return p == null ? parent.loadAsBytes(resourceName) : p.v1.loadAsBytes(p.v2);
    }
}
