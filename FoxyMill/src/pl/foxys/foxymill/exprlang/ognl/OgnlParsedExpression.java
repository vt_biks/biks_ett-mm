/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.exprlang.ognl;

import ognl.Ognl;
import pl.foxys.foxymill.exprlang.ParsedExpressionBase;
import pl.foxys.foxymill.renderctx.IRenderCtx;

/**
 *
 * @author wezyr
 */
public class OgnlParsedExpression extends ParsedExpressionBase<Object> {

    @Override
    protected Object innerEval(//IRenderContext
            IRenderCtx renderCtx) throws Exception {
        return Ognl.getValue(parsedExpr, renderCtx.getVars(), renderCtx.//getEvalContext()
                getRootModel());
    }

    @Override
    protected Object innerParse() throws Exception {
        return Ognl.parseExpression(exprStr);
    }
}
