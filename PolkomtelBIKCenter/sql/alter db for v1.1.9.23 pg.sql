
exec sp_check_version '1.1.9.23';
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

if OBJECT_ID (N'dbo.fn_get_visual_order_for_new_node', N'FN') is not null
    drop function dbo.fn_get_visual_order_for_new_node;
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------
--poprzednia poprawna  "alter db for v1.1.4.7 pm.sql"
--moja jednak koncepcyjnie niezgodna/niepoprawna w pliku "alter db for v1.1.9.22 pg.sql"
create function dbo.fn_get_visual_order_for_new_node(@parent_id int, @tree_id int)
returns int
as
begin
  declare @res int = (select max(visual_order) from bik_node where tree_id = @tree_id and is_deleted = 0 and 
    ((@parent_id is null and parent_node_id is null) or (@parent_id is not null and parent_node_id = @parent_id)))
  return case when coalesce(@res, 0) = 0 then 0 else @res + 1 end
end
go


------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_update_version '1.1.9.23', '1.1.9.24';
go
