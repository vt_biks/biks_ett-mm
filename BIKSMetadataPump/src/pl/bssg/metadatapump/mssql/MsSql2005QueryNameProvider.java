/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.mssql;

/**
 *
 * @author tflorczak
 */
public class MsSql2005QueryNameProvider extends MsSql2008QueryNameProvider {

    @Override
    public String getDependencies() {
        return "getMSSQL2005Dependencies";
    }
}
