/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.dialogs.AddNoteDialog;
import pl.fovis.client.dialogs.AddOrEditAttributeDialog;
import pl.fovis.client.dialogs.ConnectionDatabaseNameDialog;
import pl.fovis.client.dialogs.ConnectionServerDialog;
import pl.fovis.client.dialogs.ViewSQLDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.NoteBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class EDPForms {

    public void showAttributeFormForStatus(final Integer nodeId, final TreeNodeBean node, final AttributeBean atr,
            final Integer treeId, Collection<AttributeBean> attributes, final Integer nodeKindId, boolean isNodeFromDynamicTree, Map<String, List<Integer>> usedLink) {
        final Map<String, String> attrNameToValue = new HashMap<String, String>();
        List<AttributeBean> attributesNotStatus = new ArrayList<AttributeBean>();
        for (AttributeBean ab : attributes) {

            if (!BIKClientSingletons.showAddStatus() || ab.isStatus) {
                attributesNotStatus.add(ab);

                attrNameToValue.put(ab.atrName, ab.atrLinValue == null ? ab.defaultValue : ab.atrLinValue);
            }
        }
        showAddOrEditForm(nodeId, node.branchIds, node, atr, treeId, attributesNotStatus, nodeKindId, isNodeFromDynamicTree, attrNameToValue, usedLink, true);
    }

    public void showAttributeForm(final Integer nodeId, final TreeNodeBean node, final AttributeBean atr,
            final Integer treeId, Collection<AttributeBean> attributes, final Integer nodeKindId, boolean isNodeFromDynamicTree, Map<String, List<Integer>> usedLink) {
        showAttributeForm(nodeId, node.branchIds, node, atr, treeId, attributes, nodeKindId, isNodeFromDynamicTree, usedLink, true);
    }

    public void showAttributeForm(final Integer nodeId, final String branchIds, final TreeNodeBean node,
            final AttributeBean atr, final Integer treeId, Collection<AttributeBean> attributes, final Integer nodeKindId, final boolean isNodeFromDynamicTree, final Map<String, List<Integer>> usedLink,
            boolean removeStatusAtr) {
//        if (atr == null) {
        final Map<String, String> attrNameToValue = new HashMap<String, String>();
        List<AttributeBean> attributesNotStatus = new ArrayList<AttributeBean>();
        for (AttributeBean ab : attributes) {

            if (BIKClientSingletons.showAddStatus() && removeStatusAtr) {
                if (!ab.isStatus) {
                    attributesNotStatus.add(ab);

                    attrNameToValue.put(ab.atrName, ab.atrLinValue == null ? ab.defaultValue : ab.atrLinValue);
                }
            } else {
                attrNameToValue.put(ab.atrName, ab.atrLinValue == null ? ab.defaultValue : ab.atrLinValue);
            }
        }

        showAddOrEditForm(nodeId, branchIds, node, atr, treeId, removeStatusAtr ? attributesNotStatus : attributes, nodeKindId, isNodeFromDynamicTree, attrNameToValue, usedLink, removeStatusAtr ? false : null);
    }

    public void showAddOrEditForm(final Integer nodeId, final String branchIds, final TreeNodeBean node, final AttributeBean atr,
            final Integer treeId, Collection<AttributeBean> attributes, final Integer nodeKindId, final boolean isNodeFromDynamicTree, final Map<String, String> attrNameToValue, final Map<String, List<Integer>> usedLink, final Boolean isStatus) {
        final List<String> rightRolesBefore = new ArrayList();
        if (atr != null && atr.atrLinValue != null) {
            rightRolesBefore.addAll(Arrays.asList(atr.atrLinValue.split("[|]")));
        }
        BIKClientSingletons.getService().getBikAttributesNameAndType(nodeId, nodeKindId,
                BIKConstants.HYPERLINK, treeId, usedLink, new StandardAsyncCallback<Pair<Map<String, AttributeBean>, Map<String, List<TreeNodeBean>>>>(I18n.brakZdefinioAtrybutoDlaDanegoTyp.get() /* I18N:  */) {

            public void onSuccess(Pair<Map<String, AttributeBean>, Map<String, List<TreeNodeBean>>> result) {
                Collection<AttributeBean> attrs = BIKClientSingletons.filtrPublicAttributes(node, new ArrayList<AttributeBean>(result.v1.values()));
                Map<String, AttributeBean> filtredMap = new HashMap<String, AttributeBean>();

                for (AttributeBean bean : attrs) {
                    if (!BIKClientSingletons.showAddStatus() || isStatus == null || (isStatus ? bean.isStatus : !bean.isStatus)) {
//                        Window.alert(bean.atrName);
                        filtredMap.put(bean.atrName, bean);
                    }
                }
                new AddOrEditAttributeDialog(nodeId, nodeKindId, isNodeFromDynamicTree)
                        .buildAndShowDialog(filtredMap, attrNameToValue, atr, treeId, I18n.dodajAtrybut.get(), result.v2, branchIds, node,
                                new IParametrizedContinuation<AttributeBean>() {
                            public void doIt(AttributeBean param) {
                                if (param.atrName.equals(BIKConstants.METABIKS_ATTRIBUTE_ASSOC_ROLE) && BIKClientSingletons.newRightsSysByAppProps()) {
                                    if (atr == null || atr.isAddNewInChildrenNodes) {
                                        addAttrAssociateRoleWithAnObj(nodeId, node.id, param);
                                    } else {
                                        editAttrAssociateRoleWithAnObj(nodeId, node.id, param, rightRolesBefore);
                                    }
                                } else {
                                    if (atr == null || atr.isAddNewInChildrenNodes) {
                                        addAtr(nodeId, node.id, param);
                                    } else {
                                        editAtr(nodeId, node.id, param);
                                    }
                                }
                            }
                        });
            }
        });
    }

    protected void refreshAndRedisplayData(int nodeId) {
        BIKClientSingletons.getTabSelector().invalidateNodeData(nodeId);
    }

    public void showNotesForm(final int nodeId, NoteBean note) {
        new AddNoteDialog().buildAndShowDialog(note, new IParametrizedContinuation<NoteBean>() {
            public void doIt(NoteBean param) {
                addNote(nodeId, param);

            }
        });
    }

    public void viewSQLDef(String title, String body) {
        new ViewSQLDialog().buildAndShowDialog(title, body);
    }

    public void viewSetConnectionDatabaseName(final int nodeId) {
        BIKClientSingletons.getService().getMSSQLServerAndDatabasesForBOConnection(new StandardAsyncCallback<Map<String, List<String>>>("Error in" /* I18N: no */ + " getMSSQLServerAndDatabases") {
            public void onSuccess(Map<String, List<String>> result) {
                new ConnectionDatabaseNameDialog().buildAndShowDialog(result, nodeId, new IContinuation() {
                    public void doIt() {
                        refreshAndRedisplayData(nodeId);
                    }
                });
            }
        });
    }

    public void viewSetConnectionServerName(final int nodeId) {
        BIKClientSingletons.getService().getOracleServerForBOConnection(new StandardAsyncCallback<Set<String>>("Error in" /* I18N: no */ + " getOracleServerForBOConnection") {
            public void onSuccess(Set<String> result) {
                new ConnectionServerDialog().buildAndShowDialog(result, nodeId, new IContinuation() {
                    public void doIt() {
                        refreshAndRedisplayData(nodeId);
                    }
                });
            }
        });
    }

    private void addNote(final int nodeId, NoteBean param) {
        if (param.id == -1) {
            BIKClientSingletons.getService().addBikEntityNote(param.title, param.body, nodeId,
                    new NodeRefreshingCallback<Void>(nodeId, I18n.dodanoKomentarz.get() /* I18N:  */));
            //BIKClientSingletons.showNotification("Dodano komentarz");
        } else {
            BIKClientSingletons.getService().editBikEntityNote(param.id, param.title, param.body,
                    new NodeRefreshingCallback<Void>(nodeId, I18n.edytowanoKomentarz.get() /* I18N:  */));
            //BIKClientSingletons.showNotification("Edytowano komentarz");
        }
    }

    private void addAtr(final int nodeId, Integer optNode2Refresh, AttributeBean param) {
        BIKClientSingletons.getService().addBikEntityAttribute(param.atrName.trim(), param.atrLinValue, nodeId,
                new NodeRefreshingCallback<Void>(optNode2Refresh == null ? nodeId : optNode2Refresh, I18n.dodanoAtrybut.get() /* I18N:  */, param.isAddNodeInToTheSameTree));
        //BIKClientSingletons.showNotification("Edytowano atrybuty");
    }

    private void editAtr(final int nodeId, Integer optNode2Refresh, AttributeBean param) {
        BIKClientSingletons.getService().editBikEntityAttribute(param.id, nodeId, param.atrLinValue,
                new NodeRefreshingCallback<Void>(optNode2Refresh == null ? nodeId : optNode2Refresh, I18n.edytowanoAtrybut.get() /* I18N:  */, param.isAddNodeInToTheSameTree));
        //BIKClientSingletons.showNotification("Edytowano atrybut");
    }

    private void addAttrAssociateRoleWithAnObj(final int nodeId, Integer optNode2Refresh, AttributeBean param) {
        BIKClientSingletons.getService().addBikEntityAttrAssociateRoleWithAnObj(param.atrName.trim(), param.atrLinValue, nodeId,
                new NodeRefreshingCallback<Void>(optNode2Refresh == null ? nodeId : optNode2Refresh, I18n.dodanoAtrybut.get() /* I18N:  */, param.isAddNodeInToTheSameTree));
        //BIKClientSingletons.showNotification("Edytowano atrybuty")
    }

    private void editAttrAssociateRoleWithAnObj(final int nodeId, Integer optNode2Refresh, AttributeBean param, List<String> rightRolesBefore) {
        BIKClientSingletons.getService().editBikEntityAttrAssociateRoleWithAnObj(param.id, nodeId, param.atrLinValue, rightRolesBefore,
                new NodeRefreshingCallback<List<String>>(optNode2Refresh == null ? nodeId : optNode2Refresh, I18n.edytowanoAtrybut.get() /* I18N:  */, param.isAddNodeInToTheSameTree) {

            public void onSuccess(List<String> result) {
                if (result.isEmpty()) {
                    BIKClientSingletons.showInfo(I18n.edytowanoAtrybut.get());
                    BIKClientSingletons.invalidateTabData(BIKClientSingletons.getTreeCodeById(BIKClientSingletons.getTreeIdOfCurrentTab()));
                } else {
                    StringBuilder sb = new StringBuilder();
                    for (String str : result) {
                        sb.append(str);
                        sb.append(" ");
                    }
                    sb.append("Usuń uprawnienia użytkowników w roli dla tego obiektu, a następnie powróć do edycji  metamodelu.");
                    BIKClientSingletons.invalidateTabData(BIKClientSingletons.getTreeCodeById(BIKClientSingletons.getTreeIdOfCurrentTab()));
                    BIKClientSingletons.showWarning(sb.toString());
                }
            }
        });
    }
}
