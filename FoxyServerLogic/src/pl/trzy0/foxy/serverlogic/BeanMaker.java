/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import simplelib.IIndexedNamedVals;
import commonlib.MuppetMerger;
import commonlib.MuppetToMapProjector;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import pl.trzy0.foxy.commons.FoxyRuntimeException;
import simplelib.BaseUtils.Projector;

/**
 *
 * @author wezyr
 */
public class BeanMaker<T> {

    private Map<String, String> columnToPropNameMapping;
    private Class<T> beanClass;

    public BeanMaker(Class<T> beanClass) {
        this.beanClass = beanClass;
    }

    public BeanMaker(Map<String, String> columnToPropNameMapping, Class<T> beanClass) {
        this.columnToPropNameMapping = columnToPropNameMapping;
        this.beanClass = beanClass;
    }

    public BeanMaker(Collection<String> columnNames, Class<T> beanClass) {
        this(prepareStandardColumnToPropNameMapping(columnNames, beanClass), beanClass);
    }

    public List<T> createBeans(List<Map<String, Object>> sourceValsList) {
        List<T> res = new ArrayList<T>();

        for (Map<String, Object> sourceVals : sourceValsList) {
            res.add(createBean(sourceVals));
        }

        return res;
    }

    public T createBean(IIndexedNamedVals sourceVals) {
        T res = createEmptyBean();

        int cnt = sourceVals.getValCnt();
        for (int i = 0; i < cnt; i++) {
            String propName = sourceVals.getDstName(i);
            if (propName != null) {
                MuppetMerger.setProperty(res, propName, sourceVals.getVal(i));
            }
        }

        return res;
    }

    protected T createEmptyBean() {
        T res;
        try {
            res = beanClass.newInstance();
        } catch (Exception ex) {
            throw new FoxyRuntimeException("error creating instance of class: " + beanClass.getCanonicalName(), ex);
        }
        return res;
    }

    // uwaga: jeżeli w columnToPropNameMapping brakuje klucza z sourceVals
    // to takie property nie będzie dorzucone do beana!!!
    public T createBean(Map<String, Object> sourceVals) {
        T res = createEmptyBean();
//        setOneBeanProperties(sourceVals, res);
        setBeanProperties(sourceVals, res);
        return res;
    }

    public void setBeanProperties(Map<String, Object> sourceVals, T targetBean) {
        for (Entry<String, Object> e : sourceVals.entrySet()) {
            String propName = columnToPropNameMapping.get(e.getKey());
            if (propName == null) {
                continue;
            }

//            if (BaseUtils.safeEquals("izD", propName)) {
//                System.out.println("+++ isDeleted found. sourceVals=" + sourceVals);
//            }

            MuppetMerger.setProperty(targetBean, propName, e.getValue());
        }
    }

    public static <T> Map<String, String> prepareStandardColumnToPropNameMapping(Collection<String> columnNames,
            Class<T> beanClass) {
        Map<String, String> res = new LinkedHashMap<String, String>();

        Collection<String> propNames = MuppetMerger.getMuppetProps(beanClass, 3);
        Map<String, String> lowerCasePropNamesMap = new HashMap<String, String>();
        for (String propName : propNames) {
            lowerCasePropNamesMap.put(propName.toLowerCase(), propName);
        }

        for (String colName : columnNames) {
            String mappedToPropName = colName.replaceAll("[_]+", "").toLowerCase();
            if (lowerCasePropNamesMap.containsKey(mappedToPropName)) {
                res.put(colName, lowerCasePropNamesMap.get(mappedToPropName));
            }
        }

        return res;
    }

    // pierwsza mapa (czyli wiersz) jest brana, z niej lista kluczy - to 
    // będzie użyte do ustalenia nazw kolumn (konwersja z db na java-props), 
    // czyli jeżeli w drugim wierszu pojawi się nowa kolumna, a w pierszym jej 
    // nie było ---> nie doda się!!!
    public static <T> List<T> createBeansWithColsAsInFirstOne(List<Map<String, Object>> sourceValsList,
            Class<T> beanClass) {
        if (sourceValsList.isEmpty()) {
            return new ArrayList<T>();
        }

        Collection<String> colNames = sourceValsList.get(0).keySet();
        BeanMaker<T> bm = new BeanMaker<T>(colNames, beanClass);
        return bm.createBeans(sourceValsList);
    }

    public static <T> T createOneBean(Map<String, Object> sourceVals, Class<T> beanClass) {
        Collection<String> colNames = sourceVals.keySet();
        BeanMaker<T> bm = new BeanMaker<T>(colNames, beanClass);
        return bm.createBean(sourceVals);
    }

    @SuppressWarnings("unchecked")
    public static <T> void setOneBeanProperties(Map<String, Object> sourceVals, T bean) {
        Collection<String> colNames = sourceVals.keySet();
        BeanMaker<T> bm = new BeanMaker<T>(colNames, (Class<T>) bean.getClass());
        bm.setBeanProperties(sourceVals, bean);
    }

    public static <T> Map<String, Object> beanToMap(T bean, String colNames) {
        Projector<T, Map<String, Object>> p = new MuppetToMapProjector<T>(colNames);
        return p.project(bean);
    }
}
