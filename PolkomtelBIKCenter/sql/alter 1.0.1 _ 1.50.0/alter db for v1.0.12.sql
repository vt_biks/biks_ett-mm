exec sp_check_version '1.0.12';
go

----------------------------------------------------------------------
----------------------------------------------------------------------

insert into bik_joined_objs (src_node_id, dst_node_id)
select dst_node_id, src_node_id
from 
bik_joined_objs
go

--select * from bik_joined_objs

delete 
--select * 
from bik_joined_objs
where id <> (select MIN(id) from bik_joined_objs dd where dd.src_node_id = bik_joined_objs.src_node_id
  and dd.dst_node_id = bik_joined_objs.dst_node_id)
go

/*
select src_node_id, dst_node_id, COUNT(*)
from bik_joined_objs
group by src_node_id, dst_node_id
having COUNT(*) > 1
*/

-- alter table bik_joined_objs drop constraint uk_bik_joined_objs_src_dst

alter table bik_joined_objs add constraint uk_bik_joined_objs_src_dst unique (src_node_id, dst_node_id)
go

delete 
--select * 
from bik_joined_objs where src_node_id = dst_node_id
go

--select * from bik_node_kind

update bik_node_kind
set caption = 'Po��czenie'
where code = 'MetaData.DataConnection'
go

update bik_node_kind
set caption = '�wiat obiekt�w'
where code = 'Universe'
go


--select * from bik_node where id = 10317

--select * from bik_node where id = 359
 
--select * from bik_node where obj_id = '16149'


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_fix_linked_node_ids_for_metadata]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_fix_linked_node_ids_for_metadata]
GO

create procedure [dbo].[sp_fix_linked_node_ids_for_metadata]
as
begin
update bik_node
set linked_node_id = (select n.id from bik_node n inner join bik_tree t on n.tree_id = t.id
  inner join bik_node_kind nk on nk.id = n.node_kind_id
where t.tree_kind = 0 and n.obj_id = bik_node.obj_id
and (
t.name = 'Po��czenia' and nk.code = 'MetaData.DataConnection' or
t.name = 'Raporty' and nk.code = 'Folder' or
t.name = 'Raporty' and nk.code = 'Webi' or
t.name = '�wiaty obiekt�w' and nk.code = 'Folder' or
t.name = '�wiaty obiekt�w' and nk.code = 'Universe' or
t.name = 'Teradata' and nk.code = 'PROCEDURE' or
t.name = 'Teradata' and nk.code = 'SCHEMA' or
t.name = 'Teradata' and nk.code = 'TABLE' or
t.name = 'Teradata' and nk.code = 'VIEW'
)
)
from bik_node inner join bik_tree t on bik_node.tree_id = t.id
  inner join bik_node_kind nk on nk.id = bik_node.node_kind_id
where
  t.tree_kind = 0
and
 not (
t.name = 'Po��czenia' and nk.code = 'MetaData.DataConnection' or
t.name = 'Raporty' and nk.code = 'Folder' or
t.name = 'Raporty' and nk.code = 'Webi' or
t.name = '�wiaty obiekt�w' and nk.code = 'Folder' or
t.name = '�wiaty obiekt�w' and nk.code = 'Universe' or
t.name = 'Teradata' and nk.code = 'PROCEDURE' or
t.name = 'Teradata' and nk.code = 'SCHEMA' or
t.name = 'Teradata' and nk.code = 'TABLE' or
t.name = 'Teradata' and nk.code = 'VIEW'
)
end
go

exec sp_fix_linked_node_ids_for_metadata
go

delete bik_joined_objs
--select * 
from bik_joined_objs jo inner join bik_node sn on jo.src_node_id = sn.id
  inner join bik_node dn on jo.dst_node_id = dn.id
where
  sn.linked_node_id is not null or dn.linked_node_id is not null
go


/*
select obj_id, COUNT(*) from (
select obj_id, n.id from bik_node n inner join bik_tree t on n.tree_id = t.id
  inner join bik_node_kind nk on nk.id = n.node_kind_id
where t.tree_kind = 0
and (
t.name = 'Po��czenia' and nk.code = 'MetaData.DataConnection' or
t.name = 'Raporty' and nk.code = 'Folder' or
t.name = 'Raporty' and nk.code = 'Webi' or
t.name = '�wiaty obiekt�w' and nk.code = 'Folder' or
t.name = '�wiaty obiekt�w' and nk.code = 'Universe' or
t.name = 'Teradata' and nk.code = 'PROCEDURE' or
t.name = 'Teradata' and nk.code = 'SCHEMA' or
t.name = 'Teradata' and nk.code = 'TABLE' or
t.name = 'Teradata' and nk.code = 'VIEW'
)
) x
group by obj_id
having COUNT(*) > 1


--select * from bik_node where node_kind_id = 4

t.name = 'Po��czenia' and nk.code = 'MetaData.DataConnection' or
t.name = 'Raporty' and nk.code = 'Folder' or
t.name = 'Raporty' and nk.code = 'Webi' or
t.name = '�wiaty obiekt�w' and nk.code = 'Folder' or
t.name = '�wiaty obiekt�w' and nk.code = 'Universe' or
t.name = 'Teradata' and nk.code = 'PROCEDURE' or
t.name = 'Teradata' and nk.code = 'SCHEMA' or
t.name = 'Teradata' and nk.code = 'TABLE' or
t.name = 'Teradata' and nk.code = 'VIEW' or


t.name = 'Po��czenia' and nk.code = 'Universe' or
t.name = 'Po��czenia' and nk.code = 'Webi' or
t.name = '�wiaty obiekt�w' and nk.code = 'Webi' or


select distinct 't.name = ''' + t.name + ''' and nk.code = ''' + nk.code + ''' or',
 t.name, nk.code, nk.caption
from bik_node n inner join bik_tree t on n.tree_id = t.id
  inner join bik_node_kind nk on nk.id = n.node_kind_id
where t.tree_kind = 0
order by t.name, nk.code

Raporty	Folder
Raporty	Webi
�wiaty obiekt�w	Folder
�wiaty obiekt�w	Universe
Po��czenia	MetaData.DataConnection
Teradata	PROCEDURE
Teradata	SCHEMA
Teradata	TABLE
Teradata	VIEW

---------
Po��czenia	Universe
�wiaty obiekt�w	Webi
Po��czenia	Webi
*/


----------------------------------------------------------------------
----------------------------------------------------------------------

exec sp_update_version '1.0.12', '1.0.13';
go
