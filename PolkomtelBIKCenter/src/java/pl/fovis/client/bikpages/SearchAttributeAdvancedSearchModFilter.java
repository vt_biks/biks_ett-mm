/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.server.searchservice.BiksSearchConstant;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class SearchAttributeAdvancedSearchModFilter extends SearchFilterBase {

    protected Map<String, BikCustomButton> allAttributesBikCustomButton = new HashMap<String, BikCustomButton>();
    protected VerticalPanel attributeFilterVp = new VerticalPanel();
    public Set<String> attrNames;
    protected IParametrizedContinuation<String> ic;
    protected Pair< Set<String>, Boolean> attrNamesAndisAnythingNodeKindSelected;
    public Map<String, String> allTextFieldsName = new HashMap<String, String>() {
        {
            put(BiksSearchConstant.BIK_NODE_NAME, I18n.nazwaObiektuSearch.get());
            put(BiksSearchConstant.BIK_NODE_DESC, I18n.opisObiektuSearch.get());
            put(BiksSearchConstant.BIK_JOINED_OBJ_ATTRIBUTE_LINKED, I18n.atrybutyPowiazan.get());
            put(BiksSearchConstant.BIK_FILE_CONTENT, I18n.zalaczniki.get());
        }
    };

    public SearchAttributeAdvancedSearchModFilter(IParametrizedContinuation<String> ic, Set<String> attrNames) {
        this.ic = ic;
        this.attrNames = attrNames;
    }

    @Override
    protected VerticalPanel buildWidget() {

        attributeFilterVp.add(new Label(I18n.atrybuty.get()));
        attributeFilterVp.addStyleName("lblPadding Search");

        VerticalPanel attributeVp = new VerticalPanel();
        ScrollPanel attrSP = new ScrollPanel();
        attrSP.setHeight("170px");
        attrSP.setWidth("300px");
        attrSP.add(attributeVp);

        for (Map.Entry<String, String> s : allTextFieldsName.entrySet()) {
            attributeVp.add(createAttributePb(s.getValue(), s.getKey()));
        }

        for (String name : attrNames) {
            String trimName = name.indexOf('.') < 0 ? name : name.split("\\.")[1];
            attributeVp.add(createAttributePb(trimName, name));

        }
        attributeFilterVp.add(filterPanel(filterTb = new TextBox()));
        FlowPanel allFp = new FlowPanel();
        allFp.add(new Label(""));
        allFp.addStyleName("borderBottom");
        allFp.setWidth("280px");
        allFp.getElement().getStyle().setHeight(19, Style.Unit.PX);

        attributeFilterVp.add(allFp);
        attributeFilterVp.add(attrSP);
        return attributeFilterVp;
    }

    protected BikCustomButton createAttributePb(final String name, final String formValue) {
//        FlowPanel fp = new FlowPanel();
        BikCustomButton addButton = new BikCustomButton(name, "addAttrToSearch", /* I18N:  */ new IContinuation() {

                    @Override
                    public void doIt() {

                        ic.doIt(formValue.startsWith("@") ? formValue + ":" : "@" + formValue.replace(" ", "_").toLowerCase() + ":");
//                            "@" + name.replace(" ", "_").toLowerCase();
                    }
                });

        addButton.setTitle(formValue);
        allAttributesBikCustomButton.put(formValue, addButton);
//        fp.add(addButton);
//        fp.getElement().getStyle().setPaddingTop(3, Style.Unit.PX);
//        fp.getElement().getStyle().setPaddingBottom(4, Style.Unit.PX);
        return addButton;

    }

    protected void filterAction() {

        if (attrNamesAndisAnythingNodeKindSelected == null) {
            reselectedAttributeCbxs(new Pair< Set<String>, Boolean>(attrNames, true));
        } else {
            reselectedAttributeCbxs(attrNamesAndisAnythingNodeKindSelected);
        }

//        reselectedAttributeCbxs(attrNamesAndisAnythingNodeKindSelected);
        ic.doIt(null);
    }

    protected void reselectedAttributeCbxs(Pair< Set<String>, Boolean> attrNamesAndisAnythingNodeKindSelected) {
        this.attrNamesAndisAnythingNodeKindSelected = attrNamesAndisAnythingNodeKindSelected;
        String filtr = filterTb.getText();
        boolean isAnythingNodeKindSelected = attrNamesAndisAnythingNodeKindSelected.v2;

        for (BikCustomButton cb : allAttributesBikCustomButton.values()) {
            String formValue = cb.getTitle();
            if (allTextFieldsName.containsKey(formValue)) {
                if (!isAnythingNodeKindSelected) {//sprawdzić
                    cb.setVisible(false);
                } else {
                    cb.setVisible(true);
                }
            } else if (attrNamesAndisAnythingNodeKindSelected.v1.contains(formValue)) {
                cb.setVisible(true);
            } else {
                cb.setVisible(false);
            }
            if (cb.isVisible()) {
                if (BaseUtils.isStrEmptyOrWhiteSpace(filtr) || cb.getHTML().toLowerCase().contains(filtr.toLowerCase())) {
                    cb.setVisible(true);
                } else {
                    cb.setVisible(false);
                }
            }
        }

    }

    public void selectOrUnselectAllFilter(boolean check) {
//        a.setValue(true);
        for (BikCustomButton cb : allAttributesBikCustomButton.values()) {
            cb.setVisible(check);
        }
        filterTb.setText("");
    }

}
