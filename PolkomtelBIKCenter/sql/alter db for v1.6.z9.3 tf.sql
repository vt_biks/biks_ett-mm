﻿exec sp_check_version '1.6.z9.3';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_dqm_request]') and name = N'idx_bik_dqm_request_test_id_error_count')
begin
	create index idx_bik_dqm_request_test_id_error_count on bik_dqm_request (test_id, error_count);
end;
go

if not exists(select * from bik_app_prop where name = 'showAttributesBeforeChildrenExtradataWidget')
begin
	insert into bik_app_prop (name, val, is_editable)
	values ('showAttributesBeforeChildrenExtradataWidget', 'false', 1)
end;
go

if not exists(select * from bik_app_prop where name = 'showGrantsInTestConnection')
begin
	insert into bik_app_prop (name, val, is_editable)
	values ('showGrantsInTestConnection', 'false', 1)
end;
go

if not exists(select * from bik_app_prop where name = 'useBasicModeInDBConnectors')
begin
	insert into bik_app_prop (name, val, is_editable)
	values ('useBasicModeInDBConnectors', 'false', 1)
end;
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------


exec sp_update_version '1.6.z9.3', '1.6.z9.4';
go
