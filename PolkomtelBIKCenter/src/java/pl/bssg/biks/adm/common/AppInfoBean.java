/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.common;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author pmielanczuk
 */
public class AppInfoBean implements Serializable {

    public String appCode;
    public String appVer;
    public List<InstanceInfoBean> instances;
}
