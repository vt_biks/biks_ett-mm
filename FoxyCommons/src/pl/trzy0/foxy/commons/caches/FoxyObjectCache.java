/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.caches;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class FoxyObjectCache<K, V> {

    private static ILameLogger logger = LameLoggerFactory.getLogger(
            FoxyObjectCache.class);
    private IFoxyObjectSupplier<K, V> supplier;
    private Map<K, V> cache = new HashMap<K, V>();
    private Map<String, Map<Object, V>> alternativeCaches;
    private Set<String> alternativeKeysUniqueOnNull;

    public FoxyObjectCache(IFoxyObjectSupplier<K, V> supplier) {
        this.supplier = supplier;
        createAlternativeCaches(supplier.getAlternativeKeyNames());
    }

    public V getFromCacheByKey(K key) {
        return cache.get(key);
    }

    protected boolean isAltKeyUniqueOnNull(String keyName) {
        return alternativeKeysUniqueOnNull.contains(keyName);
    }

    protected void putValInAltCache(Map<Object, V> alternativeCache, String keyName, Object keyVal, V obj) {
        if (keyVal != null || isAltKeyUniqueOnNull(keyName)) {
            V oldObj = alternativeCache.put(keyVal, obj);
            if (oldObj != null) {
                throw new LameRuntimeException("alternative key: \"" +
                        keyName + "\" is not unique, duplicated value: \"" +
                        keyVal + "\", old object=\"" + oldObj +
                        "\", new object=\"" + obj + "\"");
            }
        }
    }

    protected void putInCache(V obj) {
        putInCache(supplier.getKeyValue(obj), obj);
    }

    protected void putInCache(K key, V obj) {
        cache.put(key, obj);

        if (alternativeCaches != null) {
            for (Entry<String, Map<Object, V>> e : alternativeCaches.entrySet()) {
                String keyName = e.getKey();
                Map<Object, V> alternativeCache = e.getValue();

                Object keyVal = supplier.getObjectKeyValue(obj, keyName);
                putValInAltCache(alternativeCache, keyName, keyVal, obj);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("putInCache: " + key + ":" + getObjectAltKeyVals(obj));
        }
    }

    protected V getFromSupplierByKey(K key) {
        V res = supplier.getByKey(key);
        putInCache(key, res);
        return res;
    }

    public synchronized V getByKey(K key) {
        V res = getFromCacheByKey(key);

        if (res == null) {
            res = getFromSupplierByKey(key);
        }

        return res;
    }

    public V findByAlternativeKey(String keyName, Object keyVal) {
        Map<Object, V> altCache = alternativeCaches != null ? alternativeCaches.get(keyName) : null;

        if (altCache == null) {
            throw new LameRuntimeException("this cache does not have alternative key: " + keyName);
        }

        if (keyVal == null && !alternativeKeysUniqueOnNull.contains(keyName)) {
            throw new LameRuntimeException("search for null key value but key \"" +
                    keyName + "\" is not unique on nulls");
        }

        return altCache.get(keyVal);
    }

    public synchronized V getByAltKey(String keyName, Object keyVal) {
        V res = findByAlternativeKey(keyName, keyVal);

        if (res == null) {
            res = supplier.getByAltKeyVal(keyName, keyVal);
            putInCache(supplier.getKeyValue(res), res);
        }

        return res;
    }

    public Collection<V> getManyByAltKeys(String keyName, Collection<?> keyVals) {
        return internalGetManyByKeys(keyName, keyVals);
    }

    private void createAlternativeCaches(Set<String> alternativeKeyNames) {
        if (BaseUtils.isCollectionEmpty(alternativeKeyNames)) {
            return;
        }

        alternativeCaches = new HashMap<String, Map<Object, V>>();
        alternativeKeysUniqueOnNull = new HashSet<String>();

        for (String alternativeKeyName : alternativeKeyNames) {
            alternativeCaches.put(alternativeKeyName, new HashMap<Object, V>());
            if (supplier.isNullUniqueForKey(alternativeKeyName)) {
                alternativeKeysUniqueOnNull.add(alternativeKeyName);
            }
        }
    }

    protected Set<String> getAltKeys() {
        return alternativeCaches == null ? null : alternativeCaches.keySet();
    }

    public Map<String, Object> getObjectAltKeyVals(V obj) {
        Set<String> altKeys = getAltKeys();
        if (altKeys == null) {
            return null;
        }

        Map<String, Object> res = new HashMap<String, Object>();
        for (String altKey : altKeys) {
            res.put(altKey, supplier.getObjectKeyValue(obj, altKey));
        }
        return res;
    }

    public synchronized void updateObjectAltKeys(V obj, Map<String, Object> oldKeyVals) {
        Set<String> altKeys = getAltKeys();
        if (altKeys != null) {
            for (String altKey : altKeys) {
                Object newKeyVal = obj != null ? supplier.getObjectKeyValue(obj, altKey) : null;
                Object oldKeyVal = oldKeyVals.get(altKey);
                if (!BaseUtils.safeEquals(newKeyVal, oldKeyVal)) {
                    Map<Object, V> altCache = alternativeCaches.get(altKey);
                    if (oldKeyVal != null || isAltKeyUniqueOnNull(altKey)) {
                        altCache.remove(oldKeyVal);
                    }
                    if (obj != null) {
                        putValInAltCache(altCache, altKey, newKeyVal, obj);
                    }
                }
            }
        }
    }

    public synchronized FoxyObjectUpdateData getByKeyForUpdate(
            K key, boolean allowSupplying) {
        V obj = getFromCacheByKey(key);
        if (obj == null) {
            if (!allowSupplying) {
                return null;
            }

            obj = getFromSupplierByKey(key);
        }

        return new FoxyObjectUpdateData<K, V>(this, key, obj);
    }

    public synchronized void invalidateByKey(K key) {
        V obj = cache.remove(key);
        if (obj == null) {
            return;
        }
        Map<String, Object> keyVals = getObjectAltKeyVals(obj);
        updateObjectAltKeys(null, keyVals);
    }

    @SuppressWarnings("unchecked")
    protected synchronized Collection<V> internalGetManyByKeys(String keyName, Collection<?> keys) {
        List<Object> missingKeys = new ArrayList<Object>();
        List<V> res = new ArrayList<V>();

        boolean byPrimKey = keyName == null;

        for (Object key : keys) {
            V obj = byPrimKey ? getFromCacheByKey((K)key) : findByAlternativeKey(keyName, key);
            if (obj == null) {
                missingKeys.add(key);
                if (logger.isDebugEnabled()) {
                    logger.debug("internalGetManyByKeys: key " + keyName +
                            ", val: " + key + " not in cache");
                }
            } else {
                res.add(obj);
                if (logger.isDebugEnabled()) {
                    logger.debug("internalGetManyByKeys: found in cache: " +
                            key + ":" + getObjectAltKeyVals(obj));
                }
            }
        }

        if (missingKeys.size() > 0) {
            Collection<V> additional = byPrimKey ? supplier.getManyByKeys((Collection<K>) missingKeys) : supplier.getManyByAltKeyVals(keyName, missingKeys);
            for (V newObj : additional) {
                putInCache(newObj);
            }

            res.addAll(additional);
        }

        return res;
    }

    public Collection<V> getManyByIds(Collection<K> keys) {
        return internalGetManyByKeys(null, keys);
    }
}
