/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.sbavc;

import java.io.Serializable;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public abstract class SearchByAttrValueCriteriaBase implements ISearchByAttrValueCriteria, Serializable {

    protected String shortOperatorName;

    public String attrName;
    public int attrId;
    public boolean isNegation;

    public Integer idInSelectedCriteriaList;
    public String txtPrefix;

    public SearchByAttrValueCriteriaBase() {
    }

    public SearchByAttrValueCriteriaBase(int attrId, String attrName, boolean isNegation) {
        this.attrId = attrId;
        this.attrName = attrName;
        this.isNegation = isNegation;
    }

    @Override
    public synchronized String getShortOperatorName() {
        if (shortOperatorName == null) {
            shortOperatorName = BaseUtils.dropOptionalPrefix(getClass().getSimpleName(), "SearchByAttrValueCriteria");
        }

        // System.out.println("getShortOperatorName(): shortOperatorName=" + shortOperatorName + ", getClass().getSimpleName()=" + getClass().getSimpleName());
        return shortOperatorName;
    }

    @Override
    public Integer getIdInSelectedCriteriaList() {
        return idInSelectedCriteriaList;
    }

    @Override
    public void setIdInSelectedCriteriaList(int id) {
        idInSelectedCriteriaList = id;
    }

    @Override
    public void setTxtPrefix(String postFix) {
        txtPrefix = postFix;
    }

    @Override
    public int getAttrId() {
        return attrId;
    }
}
