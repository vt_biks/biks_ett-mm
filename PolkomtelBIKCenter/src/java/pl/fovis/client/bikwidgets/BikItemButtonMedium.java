/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.IContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author mgraczkowski
 */
public class BikItemButtonMedium extends BikCustomButton {

    public BikItemButtonMedium(String text, final IContinuation click) {
        super(text, "itemMediumBlackBtn", click);
    }
}
