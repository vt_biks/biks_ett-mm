/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import commonlib.LameUtils;
import commonlib.datapump.GenericDataPump;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.templates.INamedTemplateProvider;
import pl.trzy0.foxy.serverlogic.db.JdbcConnectionBase;
import pl.trzy0.foxy.serverlogic.db.datapump.JdbcDataPumpSource;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;
import simplelib.IParametrizedContinuation;
import simplelib.LameRuntimeException;
import simplelib.StackTraceUtil;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public abstract class AbstractDatabaseTestExecutor extends AbstractTestExecutorBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    protected boolean useFunctionInsteadSQLQuery = false;
    protected DQMTestBean test;
//    protected IBeanConnectionEx<Object> mc;
    protected JdbcConnectionBase<Object> mc;
    protected Map<String, String> codeNameToRealTableName;
    protected int dataReadSecs2;

    public AbstractDatabaseTestExecutor(IBIKSDQMSubservice service) {
        super(service);
        this.useFunctionInsteadSQLQuery = service.useFunctionInsteadSQLQuery();
    }

    public abstract String getSourceName();

    @Override
    public List<DQMTestBean> preInitAndGetRequestToRun(DQMTestBean test, List<DQMMultiSourceBean> optMultiSources) {

        codeNameToRealTableName = new LinkedHashMap<String, String>();

        if (optMultiSources != null) {
            for (DQMMultiSourceBean dmsb : optMultiSources) {
                codeNameToRealTableName.put(dmsb.codeName, dmsb.biksTableName);
            }
        }

        // domyślnie 1 uruchomienie testu to 1 wykonanie
        List<DQMTestBean> list = new ArrayList<DQMTestBean>();
        list.add(test);
        return list;
    }

    @Override
    public void exit() {
        // no op
    }

    protected ConnectionParametersDBServerBean getDatabaseConfig(Integer serverId, Integer connectionId) {
        return service.getDBParametersForId(serverId, connectionId);
    }

    protected abstract String getCallTestFunctionQuery(String functionName);

//    protected abstract List<Map<String, Object>> runTestQuery(String sqlText);
    protected final List<Map<String, Object>> runTestQuery(String sqlText) {

        sqlText = replaceFoxyTagsWithRealName(sqlText);

        try {

            return mc.execQry(sqlText, FieldNameConversion.ToLowerCase);

        } catch (Exception ex) {

            System.out.println("runTestQuery: exception! test=" + test);

            throw new RuntimeException("exception in runTestQuery"
                    + (BaseUtils.isStrEmptyOrWhiteSpace(test.optTestGrantsPerObjects) ? ""
                            : "\n" + BeanConnectionUtils.optTestGrantsPerObjects(mc, test.optTestGrantsPerObjects))
                    + "\n" + StackTraceUtil.getCustomStackTrace(ex),
                    ex
            );
        }
    }

    @Override
    public List<Map<String, Object>> runTest() {
        if (useFunctionInsteadSQLQuery) {
            return runTestQuery(getCallTestFunctionQuery(test.procedureName));
        } else {
            return runTestQuery(test.testSql);
        }
    }

    @Override
    public List<Map<String, Object>> getErrorData() {
        if (test.errorProcedureName == null && test.errorTestSql == null) {
            return null;
        }
        return runTestQuery(getErrorTestSQL());
    }

    @Override
    public void closeConnection() {
        if (logger.isDebugEnabled()) {
            logger.debug("closeConnection starts");
        }
        mc.destroy();
    }

//    @Override
//    public void pumpDataToBiksDatabase() {
//        final String typeMappingAppPropsName = "typeMapping_" + BaseUtils.compactWhiteSpaces(source.sourceName).replace(" ", "");
//        String typeMappingStr = service.getSensitiveAppPropValue(typeMappingAppPropsName);
//
//        Properties typeMappingProps = new Properties();
//
//        try {
//            typeMappingProps.load(new StringReader(typeMappingStr));
//        } catch (Exception ex) {
//            throw new RuntimeException("error reading typeMapping props file, typeMappingAppPropsName=" + typeMappingAppPropsName, ex);
//        }
//
//        TextProviderFromProperties typeMappingTexts = new TextProviderFromProperties(typeMappingProps, (String) null);
//
//        final INamedTemplateProvider typeMappingTemplates = new FoxyMillNamedTemplateProvider(typeMappingTexts);
//
//        mc.execWithJdbcConnection(new IParametrizedContinuation<Connection>() {
//
//            @Override
//            public void doIt(final Connection srcConn) {
//
//                service.execInAutoCommitMode(new IContinuation() {
//
//                    @Override
//                    public void doIt() {
//                        new JdbcToMssqlDataPump(srcConn, service.getConnection(), typeMappingTemplates, source.sqlTest, source.biksTableName).pumpIt();
//                    }
//                });
//            }
//        });
//    }
    @Override
    protected String getTypeMappingStr() {
        final String typeMappingAppPropsName = "typeMapping_" + BaseUtils.compactWhiteSpaces(source == null ? getSourceName() : source.sourceName).replace(" ", "");
        String typeMappingStr = service.getSensitiveAppPropValue(typeMappingAppPropsName);
        if (BaseUtils.isStrEmpty(typeMappingStr)) {
            throw new LameRuntimeException("no typeMappings in app_props with key: " + typeMappingAppPropsName);
        }
        return typeMappingStr;
    }

    @Override
    protected int performPumpingData(final INamedTemplateProvider typeMappingTemplates, final String tmpTableName, final String optTestGrantsPerObjects,boolean isMultiSource) {
        mc.execWithJdbcConnection(new IParametrizedContinuation<Connection>() {

            @Override
            public void doIt(final Connection srcConn) {

//                new JdbcToMssqlDataPump(srcConn, service.getConnection(), typeMappingTemplates, source.sqlTest, source.biksTableName).pumpIt();
                dataReadSecs2 = new GenericDataPump(
                        new JdbcDataPumpSource(srcConn, replaceFoxyTagsWithRealName(getErrorTestSQL()), mc, optTestGrantsPerObjects),
                        createDataPumpDestination(typeMappingTemplates, tmpTableName)).pumpIt();
            }
        });

        return dataReadSecs2;
    }

    protected String getErrorTestSQL() {
        if (useFunctionInsteadSQLQuery) {
            return getCallTestFunctionQuery(test.errorProcedureName);
        } else {
            return (source != null ? source.sqlTest : test.errorTestSql);
        }
    }

    private String replaceFoxyTagsWithRealName(String sqlText) {
        if (BaseUtils.isMapEmpty(codeNameToRealTableName)) {
            return sqlText;
        }

        for (Entry<String, String> e : codeNameToRealTableName.entrySet()) {
            sqlText = sqlText.replace("%{" + e.getKey() + "}%", e.getValue());
        }
        return sqlText;
    }
}
