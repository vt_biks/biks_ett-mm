    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author wezyr
 */
public class UserInNodeBean implements Serializable {

    public int nodeId;
    public int roleForNodeId;
    public String roleCaption;
    public String roleCode;
    public int userId;
    public String email;
    public String userName;
    public Integer sysUserId;
    public int userNodeId;
    public String treeCode;
    public String avatar;
    public String loginName;
    public boolean inheritToDescendants;
    public boolean isAutomaticJoined; // połączenie jest automatyczne (autor obiektu) - wynika to z metadanych
    public boolean isAuxiliary;
    public int userInRoleNodeId;
    public String userInRoleBranchIds;
    public int userInRoleNodeKindId;
    public String descrPlainUser;
    public int isDisabled;
}
