/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.dqm;

import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.dqm.AddOrEditDQMTestDialogForConnections;
import pl.fovis.client.dialogs.dqm.AddOrEditDQMTestDialogForDatabases;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.fovis.common.dqm.DataQualityTestBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class NodeActionDQMAddTest extends NodeActionDQMBase {

    @Override
    public boolean isEnabledByDesign() {
        return node != null && isInDQMAdminMode() && isNodeOneOfTheKind(false, /*BIKGWTConstants.NODE_KIND_DQM_ALL_TESTS_FOLDER, */ BIKGWTConstants.NODE_KIND_DQM_GROUP);
    }

    @Override
    public String getCaption() {
        return I18n.dodajTest.get();
    }

    @Override
    public void execute() {
        BIKClientSingletons.getDQMService().getDQMTestsNameAndOptConnections(new StandardAsyncCallback<Pair<Set<String>, Map<Integer, ConnectionParametersDQMConnectionsBean>>>("Error in getDQMTestsNameAndOptConnections") {

            @Override
            public void onSuccess(Pair<Set<String>, Map<Integer, ConnectionParametersDQMConnectionsBean>> result) {

                IParametrizedContinuation<DataQualityTestBean> continuation = new IParametrizedContinuation<DataQualityTestBean>() {

                    @Override
                    public void doIt(DataQualityTestBean param) {
                        BIKClientSingletons.getDQMService().addDQMTest(param, node.id, /*node.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_DQM_ALL_TESTS_FOLDER),*/ new StandardAsyncCallback<Integer>("Error in addDQMTest") {

                                    @Override
                                    public void onSuccess(Integer result) {
                                        BIKClientSingletons.showInfo(I18n.dodanoTest.get());
                                        refreshRedisplayDataAndSelectRecord(result);
                                    }
                                });
                    }
                };
                if (BIKClientSingletons.useDqmConnectionMode()) {
                    new AddOrEditDQMTestDialogForConnections().buildAndShowDialog(node.id, null, result, continuation);
                } else {
                    new AddOrEditDQMTestDialogForDatabases().buildAndShowDialog(node.id, null, result, continuation);
                }
            }
        });

    }
}
