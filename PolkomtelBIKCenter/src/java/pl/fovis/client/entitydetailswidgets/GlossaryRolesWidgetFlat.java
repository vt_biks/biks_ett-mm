/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import pl.fovis.client.BIKGWTConstants;

/**
 *
 * @author beata
 */
public class GlossaryRolesWidgetFlat extends UserInNodeWidgetFlatBase {

    @Override
    public String getCaption() {
        return BIKGWTConstants.MENU_NAME_GLOSSARY;
    }

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_GLOSSARY;
    }
}
