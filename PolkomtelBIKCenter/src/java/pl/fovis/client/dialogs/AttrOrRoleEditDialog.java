/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author beata
 */
public class AttrOrRoleEditDialog extends BaseActionOrCancelDialog {

    protected AttributeBean attr;
    protected TextBox tbTytul;
    protected TextBox tbAttr;
    protected ListBox listBox;
    private Label lblTitle;
    private Label lblAttr;
    protected Map<Integer, String> category;
    protected Map<Integer, Integer> listboxCategoryId = new HashMap<Integer, Integer>();
    protected Set<String> alreadyInUseInLowerCase; // = new ArrayList<String>();
    protected int categoryId;
    protected String attrOrRoleName;
    private IParametrizedContinuation<Pair<Integer, String>> saveCont;
    protected HTML errorMssg;

    @Override
    protected String getDialogCaption() {
        return I18n.edycja.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        main.add(errorMssg = new HTML());
        errorMssg.setStyleName("status" /* i18n: no */);
        Grid grid = new Grid(2, 2);
        grid.setCellSpacing(4);
        grid.setWidget(0, 0, lblTitle = new Label(I18n.wybierzKategorie.get() /* I18N:  */ + ":"));
        grid.setWidget(1, 0, lblAttr = new Label(I18n.nazwaAtrybutu.get() /* I18N:  */ + ":"));
        grid.setWidget(0, 1, listBox = new ListBox());
        grid.setWidget(1, 1, tbAttr = new TextBox());
        lblTitle.setStyleName("addAttrLbl");
        lblAttr.setStyleName("addAttrLbl");
        listBox.setStyleName("addAttr");
        listBox.setWidth("210px");
        tbAttr.setStyleName("addAttr");
        tbAttr.setWidth("200px");
        tbAttr.setText(attrOrRoleName);

        int i = 0;
        for (Entry<Integer, String> c : category.entrySet()) {
            listBox.addItem(c.getValue());
            if (categoryId == c.getKey()) {
                listBox.setSelectedIndex(i);
            }
            listboxCategoryId.put(i, c.getKey());
            i++;

        }

        main.add(grid);
    }

    @Override
    protected boolean doActionBeforeClose() {
        //ww: pachnie to c&p z AttrOrRoleEditDialog
        //ww: pachnie to c&p z AddOrUpdateItemNameDialog
        //ww: pachnie to c&p z AddOrEditAdminAttributeWithTypeDialog
        String newCaption = tbAttr.getText();
        int catId = listboxCategoryId.get(listBox.getSelectedIndex());
        if (!BaseUtils.isStrEmptyOrWhiteSpace(newCaption)) {
            if (alreadyInUseInLowerCase.contains(newCaption.toLowerCase()) && categoryId == catId) {
                errorMssg.setHTML(I18n.takaNazwaJuzIstnieje.get() /* I18N:  */ + ".");
                return false;
            }
//            int catId = listboxCategoryId.get(listBox.getSelectedIndex());
            saveCont.doIt(new Pair<Integer, String>(catId, tbAttr.getText()));
            return true;
        }
        errorMssg.setHTML(I18n.nazwaNieMozeBycPusta.get() /* I18N:  */ + ".");
        return false;
    }

    @Override
    protected void doAction() {
        //nie powinno się wywoływać
    }

    public void buildAndShowDialog(Map<Integer, String> category, int categoryId, String attrOrRoleName, Collection<String> alreadyInUse, IParametrizedContinuation<Pair<Integer, String>> saveCont) {
        attr = new AttributeBean();
        this.category = category;
        this.categoryId = categoryId;
        this.attrOrRoleName = attrOrRoleName;
        this.saveCont = saveCont;
        this.alreadyInUseInLowerCase = BaseUtils.toLowerCaseIterableToSet(alreadyInUse, true);
        super.buildAndShowDialog();
    }
}
