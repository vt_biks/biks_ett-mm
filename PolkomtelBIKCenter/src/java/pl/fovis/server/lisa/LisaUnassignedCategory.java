/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.lisa;

/**
 *
 * @author pgajda
 */
public class LisaUnassignedCategory {

    public String dbName;
    public String dictionary;
    public String condition;
    public String userName;
    public String userSurname;
    public String email;
    public Integer counter;

    public String getIdTypKategList() {
        String idTypKategList = "";
        int startInd = 0, endInd = 0;
        if (condition != null && condition.length() > 0) {

            condition = condition.toLowerCase();

            startInd = condition.indexOf("id_typ_kateg");
            if (startInd == -1) {
                return "";
            }

            endInd = condition.indexOf("and", startInd);
            if (endInd > 0) {
                idTypKategList = condition.substring(startInd, endInd);
            } else {
                idTypKategList = condition.substring(startInd);
            }

            idTypKategList = idTypKategList.replaceAll("[^0-9,]", "");
        }
        return idTypKategList;
    }

}
