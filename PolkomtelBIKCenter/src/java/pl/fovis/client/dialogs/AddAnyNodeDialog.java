/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextBox;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class AddAnyNodeDialog extends BaseActionOrCancelDialog {

    protected IParametrizedContinuation<TreeNodeBean> saveCont;
    protected TextBox tbxName;
    protected NodeKindBean selectedBean;

    @Override
    protected String getDialogCaption() {
        return I18n.addAnyNode.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.dodaj.get();
    }

    @Override
    protected void buildMainWidgets() {
        FlexTable table = new FlexTable();
        table.setCellSpacing(5);
        tbxName = new TextBox();
        tbxName.setWidth("300px");
        NodeOracle nodeOracle = new NodeOracle();
        SuggestBox sb = new SuggestBox(nodeOracle);
        sb.setWidth("300px");
        sb.addSelectionHandler(new SelectionHandler<Suggestion>() {

            @Override
            public void onSelection(SelectionEvent<Suggestion> event) {
                NodeSuggestion suggestion = (NodeSuggestion) event.getSelectedItem();
                selectedBean = suggestion.getBean();
            }
        });
        table.setWidget(0, 0, new Label(I18n.podajNazwe.get()));
        table.setWidget(0, 1, tbxName);
        table.setWidget(1, 0, new Label(I18n.wybierzTypWezla.get()));
        table.setWidget(1, 1, sb);
        main.add(table);
    }

    protected boolean doActionBeforeClose() {
        if (BaseUtils.isStrEmptyOrWhiteSpace(tbxName.getText())) {
            return false;
        }
        if (selectedBean == null) {
            return false;
        }
        return true;
    }

    @Override
    protected void doAction() {
        TreeNodeBean tb = new TreeNodeBean();
        tb.name = tbxName.getText();
        tb.nodeKindId = selectedBean.id;
        saveCont.doIt(tb);
    }

    public void buildAndShowDialog(IParametrizedContinuation<TreeNodeBean> saveCont) {
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }

    class NodeSuggestion implements Suggestion {

        protected NodeKindBean bean;
        protected String displayString;

        public NodeSuggestion() {
        }

        public NodeSuggestion(NodeKindBean bean, String displayString) {
            this.bean = bean;
            this.displayString = displayString;
        }

        @Override
        public String getDisplayString() {
            return "<img src='images/" + bean.iconName + ".gif'/> " + displayString;
        }

        @Override
        public String getReplacementString() {
            return bean.caption + " (" + bean.code + ")";
        }

        public NodeKindBean getBean() {
            return bean;
        }
    }

    class NodeOracle extends SuggestOracle {

        @Override
        public boolean isDisplayStringHTML() {
            return true;
        }

        @Override
        public void requestSuggestions(Request request, Callback callback) {
            callback.onSuggestionsReady(request, new Response(matchNodes(request)));
        }

        protected Collection<NodeSuggestion> matchNodes(Request request) {
            List<NodeSuggestion> matchedNodes = new ArrayList<NodeSuggestion>();
            String query = request.getQuery();
            int queryLen = query.length();
            if (queryLen >= 2) {
                String queryLC = query.toLowerCase();
                List<NodeKindBean> nodeKinds = BIKClientSingletons.getNodeKinds();
                for (NodeKindBean nkb : nodeKinds) {
                    int captionIdx = nkb.caption.toLowerCase().indexOf(queryLC);
                    int codeIdx = nkb.code.toLowerCase().indexOf(queryLC);
                    if (captionIdx != -1 || codeIdx != -1) {
                        matchedNodes.add(new NodeSuggestion(nkb, createDisplayString(nkb.caption, nkb.code, captionIdx, codeIdx, queryLen)));
                    }
                }
            }
            return matchedNodes;
        }
    }

    // Dodaj boldowanie od znalezionego wyrazenia
    protected String createDisplayString(String caption, String code, int captionIdx, int codeIdx, int queryLen) {
        StringBuilder sb = new StringBuilder();
        if (captionIdx != -1) {
            if (captionIdx > 0) {
                sb.append(caption.substring(0, captionIdx));
            }
            sb.append("<b>").append(caption.substring(captionIdx, captionIdx + queryLen)).append("</b>");
            if (captionIdx + queryLen < caption.length()) {
                sb.append(caption.substring(captionIdx + queryLen));
            }
        } else {
            sb.append(caption);
        }
        sb.append(" ").append("(");
        if (codeIdx != -1) {
            if (codeIdx > 0) {
                sb.append(code.substring(0, codeIdx));
            }
            sb.append("<b>").append(code.substring(codeIdx, codeIdx + queryLen)).append("</b>");
            if (codeIdx + queryLen < code.length()) {
                sb.append(code.substring(codeIdx + queryLen));
            }
        } else {
            sb.append(code);
        }
        sb.append(")");
        return sb.toString();
    }
}
