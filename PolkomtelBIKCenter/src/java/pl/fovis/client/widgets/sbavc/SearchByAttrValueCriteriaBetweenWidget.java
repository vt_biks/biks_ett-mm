/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.widgets.sbavc;

import com.google.gwt.user.client.ui.Widget;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import pl.fovis.common.sbavc.SearchByAttrValueCriteriaBetween;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class SearchByAttrValueCriteriaBetweenWidget extends SearchByAttrValueCriteriaWidgetBase {

    protected Widget lowWidget;
    protected Widget highWidget;

    @Override
    protected void createOptWidget() {
        row.setWidget(0, 4, lowWidget = createWidgetBox());
        row.setWidget(0, 5, highWidget = createWidgetBox());
    }

    @Override
    public ISearchByAttrValueCriteria provideBean() {
        return (isValidInput() ? new SearchByAttrValueCriteriaBetween(getSelectedAttrId(), getSelectedAttrCaption(), getWidgetValueAsString(lowWidget), getWidgetValueAsString(highWidget), negationChbx.getValue()) : null);
    }

    @Override
    protected boolean isValidInput() {
        if (manager.isDisplayedAsNumber(getSelectedAttrId())) {
            String lowValue = getWidgetValueAsString(lowWidget).replace(",", ".");
            String highValue = getWidgetValueAsString(highWidget).replace(",", ".");
            return (BaseUtils.tryParseNumber(lowValue) != null
                    && BaseUtils.tryParseNumber(highValue) != null);
        }
        return true;
    }
}
