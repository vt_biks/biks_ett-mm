package pl.fovis.client.nodeactions;

import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.GrantRightsDialogOldRightSys;
import pl.fovis.common.RightRoleBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author ctran
 */
public class NodeActionGrantRights extends NodeActionBase {

    @Override
    public boolean isEnabledByDesign() {
        return (BIKClientSingletons.isEffectiveExpertLoggedIn()
                || BIKClientSingletons.isLoggedUserAuthorOfTree(node.treeId)
                || BIKClientSingletons.isLoggedUserCreatorOfTree(node.treeId)
                || BIKClientSingletons.isLoggedUserIsNonPublicUserInTree(node.treeId)
                || BIKClientSingletons.isBranchAuthorLoggedIn(node.branchIds)
                || BIKClientSingletons.isBranchCreatorLoggedIn(node.branchIds)
                || BIKClientSingletons.isBranchNonPublicUserLoggedIn(node.branchIds)) 
                && !BIKClientSingletons.hideGrantRightsBtnByAppProps()
                && !BIKClientSingletons.newRightsSysByAppProps();
    }

    @Override
    public String getCaption() {
        return I18n.nadajUprawnienia.get();
    }

    @Override
    public void execute() {
        BIKClientSingletons.getService().getUsersMappedToRoles(null, false, null, null, null, null, BIKClientSingletons.isMultiDomainMode(), new StandardAsyncCallback<Map<SystemUserBean, List<RightRoleBean>>>() {

            @Override
            public void onSuccess(final Map<SystemUserBean, List<RightRoleBean>> resultMap) {
                BIKClientSingletons.getService().getAllUserRoles(new StandardAsyncCallback<List<RightRoleBean>>() {

                    @Override
                    public void onSuccess(final List<RightRoleBean> resultRoles) {

                        BIKClientSingletons.getService().getAllUserGroups(new StandardAsyncCallback<List<SystemUserGroupBean>>() {

                            @Override
                            public void onSuccess(List<SystemUserGroupBean> resultGroups) {
                                new GrantRightsDialogOldRightSys(node, resultMap, resultRoles, resultGroups,
                                        new IParametrizedContinuation<Map<SystemUserBean, List<RightRoleBean>>>() {

                                            @Override
                                            public void doIt(Map<SystemUserBean, List<RightRoleBean>> param) {
                                                //przesyłamy wartość "kon" przy nadaniu uprawnień, żeby można było rozróżnić skąd nadane uprawnienia (kon - kontekst, int - interfejs, sql - sqlką, AD - przy zasilaniu z AD)
                                                BIKClientSingletons.getService().grantRightsFromContextForUser(param, node.treeId, node.id, "kon", new StandardAsyncCallback<Void>() {

                                                    @Override
                                                    public void onSuccess(Void result) {
                                                        BIKClientSingletons.showInfo(I18n.uprawnieniaNadane.get() /* I18N:  */);
                                                    }
                                                });
                                            }
                                        }, new IParametrizedContinuation<Map<RightRoleBean, List<SystemUserGroupBean>>>() {

                                            @Override
                                            public void doIt(Map<RightRoleBean, List<SystemUserGroupBean>> param) {
                                                //przesyłamy wartość "kon" przy nadaniu uprawnień, żeby można było rozróżnić skąd nadane uprawnienia (kon - kontekst, int - interfejs, sql - sqlką, AD - przy zasilaniu z AD)
                                                BIKClientSingletons.getService().grantRightsFromContextForGroup(param, node.treeId, node.id, "kon", new StandardAsyncCallback<Void>() {

                                                    @Override
                                                    public void onSuccess(Void result) {
                                                        BIKClientSingletons.showInfo(I18n.uprawnieniaNadane.get() /* I18N:  */);
                                                    }
                                                });
                                            }
                                        }
                                ).buildAndShowDialog();
                            }
                        });

                    }

                });

            }
        });
    }
}
