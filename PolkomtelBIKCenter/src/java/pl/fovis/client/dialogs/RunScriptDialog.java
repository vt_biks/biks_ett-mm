/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;

/**
 *
 * @author ctran
 */
public class RunScriptDialog extends BaseActionOrCancelDialog {

    protected List<AttributeBean> runnableAttrs;
    protected Integer nodeId;
    protected ListBox lb;
    protected HTML script;

    @Override
    protected String getDialogCaption() {
        return I18n.wykonajSkrypt.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.wykonaj.get();
    }

    @Override
    protected void buildMainWidgets() {
        Grid grid = new Grid(2, 2);
        grid.setWidget(0, 0, new Label(I18n.skrypt.get() + ":"));
        lb = new ListBox();
        for (AttributeBean bean : runnableAttrs) {
            lb.addItem(bean.atrName, bean.atrLinValue);
        }
        lb.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                onSelectedScriptChanged();
            }

        });
        grid.setWidget(0, 1, lb);
        grid.setWidget(1, 1, script = new HTML());
        lb.setSelectedIndex(0);
        onSelectedScriptChanged();
        main.add(grid);
    }

    protected void onSelectedScriptChanged() {
        script.setHTML(lb.getSelectedValue().replace("\n", "<br>"));
    }

    @Override
    protected void doAction() {
        BIKClientSingletons.getService().runScript(nodeId, lb.getSelectedValue(), new StandardAsyncCallback<Void>() {

            @Override
            public void onSuccess(Void result) {
                BIKClientSingletons.showInfo(I18n.sukces.get());
            }
        });
    }

    public void buildAndShowDialog(Integer nodeId, List<AttributeBean> runnableAttrs) {
        this.runnableAttrs = runnableAttrs;
        this.nodeId = nodeId;
        super.buildAndShowDialog();
    }
}
