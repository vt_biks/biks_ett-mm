exec sp_check_version '1.6.z9.7';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------
--
-- zmiany og�lne - wchodz� w kolejny skrypt alteruj�cy
--
-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_recalculate_Custom_Right_Role_RutsEx]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_recalculate_Custom_Right_Role_RutsEx]
GO

create procedure [dbo].[sp_recalculate_Custom_Right_Role_RutsEx](@opt_user_id int) as
begin
  declare @regUserCrrId int = (select id from bik_custom_right_role where code = 'RegularUser');
  
  if @regUserCrrId is null return;
  
  merge into bik_custom_right_role_user_entry crrue
  using bik_custom_right_role_rut_entry crrre cross join bik_system_user su
  on crrue.role_id = @regUserCrrId and crrue.user_id = su.id and (@opt_user_id is null or su.id = @opt_user_id)
    and crrre.tree_id = crrue.tree_id and (crrre.node_id is null and crrue.node_id is null or crrre.node_id = crrue.node_id)
  when matched and crrre.is_deleted <> 0 then
    delete
  when not matched by target and crrre.is_deleted = 0 and (@opt_user_id is null or su.id = @opt_user_id) then
    insert (user_id, tree_id, node_id, role_id) values (su.id, crrre.tree_id, crrre.node_id, @regUserCrrId)
  ;
  
  delete from bik_custom_right_role_rut_entry where is_deleted <> 0;
    
  exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
end;
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_recalculate_Custom_Right_Role_Ruts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_recalculate_Custom_Right_Role_Ruts]
GO

create procedure [dbo].[sp_recalculate_Custom_Right_Role_Ruts] as
begin
  exec sp_recalculate_Custom_Right_Role_RutsEx null;
end
go

exec sp_recalculate_Custom_Right_Role_Ruts;
go


/*
select distinct tree_kind from bik_tree

select * from bik_tree

select * from bik_tree_kind
*/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors]
GO

create procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors] as
begin
  set nocount on;
end
go


delete from bik_translation
where kind = 'adef'
and code in ('Firma', 'Pion')
;

insert into bik_translation (code, txt, lang, kind) values
('Firma', 'Biuro', 'pl', 'adef'),
('Firma', 'Office', 'en', 'adef'),
('Pion', 'Dzia�', 'pl', 'adef'),
('Pion', 'Department', 'en', 'adef');
go


exec sp_update_version '1.6.z9.7', '1.6.z9.8';
go
