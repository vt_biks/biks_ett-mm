/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import commonlib.GlobalThreadStopWatch;
import commonlib.ILameResourceProvider;
import commonlib.LameUtils;
import commonlib.ThreadActivityCollector;
import commonlib.UrlMakingUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import pl.trzy0.foxy.serverlogic.FoxyServiceBroker;
import pl.trzy0.foxy.serverlogic.FoxyUrlRewriter;
import pl.trzy0.foxy.serverlogic.IApplicationEnvironment;
import pl.trzy0.foxy.serverlogic.IFoxyBizLogic;
import pl.trzy0.foxy.serverlogic.IFoxyService;
import pl.trzy0.foxy.serverlogic.ServletConfigObj;
import simplelib.BaseUtils;
import simplelib.IResourceFromProvider;
import simplelib.IWatchedResourceFromProvider;
import simplelib.LameRuntimeException;
import simplelib.MapHelper;
import simplelib.ResourceFromProvider;
import simplelib.SimpleDateUtils;
import simplelib.StackTraceUtil;
import simplelib.StringEscapeUtils;
import simplelib.WatchedResourceFromProvider;
import simplelib.logging.ILameLogger;
import simplelib.logging.ILameLogger.LameLogLevel;
import simplelib.logging.StdOutLameLoggerAppender;

/**
 *
 * @author wezyr
 */
public abstract class FoxyServlet<T extends IFoxyBizLogic, SCO extends ServletConfigObj /*AppConfigObj*/> extends FileHandlingServlet {

    // more than that will yield logger.warn
    private static final long REQ_EXECUTION_WARN_THRESHOLD_MILLIS = 600;
//    private static final String LAMELOGGERS_CONFIG_PROPERTIES_FILENAME = "/WEB-INF/lameLoggersConfig.properties";
    private static final ILameLogger logger = LameUtils.getMyLogger();
    public static final String FOXYMILL_ROOT_PATH = "/WEB-INF/FoxyMill/";
    private static final String FOXYMILL_PAGEFILES_PATH = FOXYMILL_ROOT_PATH + "pageFiles";
    //private static final String FOXYMILL_PAGEFILES_PATH_SLASH = FOXYMILL_PAGEFILES_PATH + "/";
    private static final String FOXYMILL_GLOBALS_PATH = FOXYMILL_ROOT_PATH + "globals";
    //private static final String FOXYMILL_PROPS_FILE = FOXYMILL_ROOT_PATH + "page-props.properties";
    private static final String FOXYMILL_PROPS_PATH = FOXYMILL_ROOT_PATH + "props";
    private static final String SessionObjectServiceKey = "foxyService";

    static {
        StdOutLameLoggerAppender.addAppender();
    }
    protected SCO configObj;
    private FoxyServiceBroker<IFoxyBizLogic, SCO> serviceBroker;
    private T bizLogic;
    protected ILameResourceProvider resProvider;

    protected abstract Class<SCO> getServletConfigClass();

    protected abstract String getConfigFileNameBase();

    protected abstract T createBizLogic();

    protected abstract Class<? extends IFoxyService<T, SCO>> getFoxyServiceClass();
    protected FoxyMillMentosThread foxyMillMentos;
    protected IBundleDirector bundleDirector;
//    protected IWatchedResourceFromProvider<InputStream> lameLoggersConfigResource;

    protected IBundleDirector createBundleDirector() {
        return new StandardBundleDirector();
    }

    protected void customInitFoxyServiceObject(IFoxyService service) {
        // no-op
    }

    protected void readServletConfigObj(ServletConfig config) {
        configObj = ServletConfigReader.readConfigObj(config.getServletContext(),
                getServletConfigClass(), getConfigFileNameBase());
    }

    protected void makeDummyServletConfigObj() {
        Class<SCO> c = getServletConfigClass();
        try {
            this.configObj = c.newInstance();
        } catch (Exception ex) {
            throw new LameRuntimeException("error creating dummy config obj", ex);
        }
    }

    private IApplicationEnvironment createApplicationEnvironment() {
        return new FoxyServletApplicationEnvironment(foxyMillMentos,
                configObj.httpBaseAddr,
                BaseUtils.nullToDef(configObj.httpBaseAddrForServlet, configObj.httpBaseAddr),
                BaseUtils.nullToDef(configObj.httpBaseAddrForResource, configObj.httpBaseAddr),
                getUrlRewriter(),
                configObj.usesSubdomains);
    }
    private IApplicationEnvironment appEnv;

    protected IApplicationEnvironment getApplicationEnvironment() {
//        if (appEnv == null) {
//            appEnv = createApplicationEnvironment();
//        }
        return appEnv;
    }

//    protected void reinitLameLoggers() {
//        if (lameLoggersConfigResource.hasBeenModifiedRecently()) {
//            System.out.println("reloading lameLoggersConfig");
//            InputStream is = lameLoggersConfigResource.gainResource();
//            Properties lameLoggersConfig = LameUtils.readPropsFileFromInputStream(is, LAMELOGGERS_CONFIG_PROPERTIES_FILENAME, true);
//            if (lameLoggersConfig != null) {
//                LameLoggerFactory.applyLameLoggersConfig((Map) lameLoggersConfig, false);
//            }
//        }
//    }
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        //ILameResourceProvider
        resProvider = new ServletResouceProvider(getServletContext());

        //        lameLoggersConfigResource = new WatchedResourceFromProvider<InputStream>(
        //                new ResourceFromProvider<InputStream>(resProvider, LAMELOGGERS_CONFIG_PROPERTIES_FILENAME));
        //
        //        reinitLameLoggers();
        //        //Properties lameLoggersConfig = FoxyMillBundle.readPropsFileFromResProvider(resProvider, "/WEB-INF/lameLoggersConfig.properties", true);
        //        if (lameLoggersConfig != null) {
        //            LameLoggerFactory.applyLameLoggersConfig((Map) lameLoggersConfig);
        //        }
        readServletConfigObj(config);

        bundleDirector = createBundleDirector();
        foxyMillMentos = new FoxyMillMentosThread(bundleDirector,
                FOXYMILL_PROPS_PATH, FOXYMILL_PAGEFILES_PATH, FOXYMILL_GLOBALS_PATH, resProvider,
                getUrlRewriter());

        appEnv = createApplicationEnvironment();

        foxyMillMentos.setApplicationEnvironment(appEnv);

        recreateBizLogic();
        foxyMillMentos.start();

//        bizLogic = createBizLogic();
//        //DBLameLoggerAppender.addAppender(bizLogic);
//        initWithNewBizLogic();
        serviceBroker = new FoxyServiceBroker<IFoxyBizLogic, SCO>();
        serviceBroker.init(getFoxyServiceClass());
    }

    protected void recreateBizLogic() {
        T newBizLogic = createBizLogic();
        T oldBizLogic = bizLogic;
        this.bizLogic = newBizLogic;
        initWithNewBizLogic();
        if (oldBizLogic != null) {
            oldBizLogic.destroy();
        }
    }

    protected IFoxyBizLogic getBizLogic() {
        return bizLogic;
    }

    @Override
    public void destroy() {
        if (foxyMillMentos != null) {
            foxyMillMentos.interrupt();
            foxyMillMentos = null;
        }

        //PostgresConnection.disposeConnectionPool();
        if (bizLogic != null) {
            bizLogic.destroy();
            bizLogic = null;
        }
        super.destroy();
    }

    public ServletConfigObj getConfigObj() {
        return configObj;
    }
    private Map<String, IFoxyService<T, SCO>> sharedServices = new HashMap<String, IFoxyService<T, SCO>>();

    public IFoxyService getFoxyService(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        //System.out.println("************** getFoxyService: sid=" + session.getId() + " *****************");
        @SuppressWarnings("unchecked")
        FoxyServiceWrapper<T, SCO> wrapper = (FoxyServiceWrapper<T, SCO>) session.getAttribute(SessionObjectServiceKey);
        IFoxyService<T, SCO> service;
        if (wrapper != null) {
            //System.out.println("************** wrapper not null *****************");
            service = wrapper.service;
        } else {
            //System.out.println("************** wrapper is null *****************");
            service = null;
            if (appEnv.getUsesSubdomains()) {
                //System.out.println("************** using subdomains *****************");
                String globalSID = FoxyServletUtils.getCookieValue(request, NAQNAQ_PROPER_SESSIONID_COOKIE_NAME, null);
                //System.out.println("************** globalSID: " + globalSID + " *****************");
                if (globalSID != null) {
                    service = sharedServices.get(globalSID);
                    //System.out.println("************** has service: " + (service != null ? "true" : "false") + " *****************");
                }
            } else {
                //System.out.println("************** not using subdomains *****************");
            }

            if (service == null) {
                //System.out.println("************** creating service *****************");
                try {
                    service = getFoxyServiceClass().newInstance();
                } catch (Exception ex) {
                    throw new LameRuntimeException("create service", ex);
                }
                service.init(configObj);
                customInitFoxyServiceObject(service);
                if (appEnv.getUsesSubdomains()) {
                    Cookie c = new Cookie(NAQNAQ_PROPER_SESSIONID_COOKIE_NAME, session.getId());
                    c.setDomain("." + BaseUtils.getDomainFromGlobalUrl(appEnv.getCanonicalSiteUrl()));
                    c.setPath("/");
//                    System.out.println("************** before add cookie: name=" + c.getName() +
//                            ", val=" + c.getValue() +
//                            ", domain=" + c.getDomain() +
//                            ", path=" + c.getPath() +
//                            " *****************");
                    response.addCookie(c);
                }
            }

            sharedServices.put(session.getId(), service);
            session.setAttribute(SessionObjectServiceKey, new FoxyServiceWrapper<T, SCO>(service, sharedServices));
        }

        service.setBizLogic(bizLogic);

        return service;
    }

    protected String executeObjRequest(HttpServletRequest request,
            HttpServletResponse response,
            String objName, MapHelper<String> mh) {
        IFoxyService service = getFoxyService(request, response);

        if (objName.equals("resetBizLogic") && service.isSystemAdminLogged()) {
            T newBizLogic = createBizLogic();
            T oldBizLogic = bizLogic;
            bizLogic = newBizLogic;
            initWithNewBizLogic();
            oldBizLogic.destroy();
            return "";
        }

        String modelAsJson = serviceBroker.execute(
                new SessionEnvironment(request.getSession()/*, configObj*/),
                service, objName, mh);
        Integer scriptId = mh.getInteger("_scriptId");

        if (scriptId != null) {
            modelAsJson = "foxyScriptLoaded(" + scriptId + ", '" + StringEscapeUtils.escapeJavaScript(modelAsJson) + "');";
        }

        return modelAsJson;
    }

    private void processForObjName(String objName, HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> map = FoxyServletUtils.getParamsAsMap(request);
        MapHelper<String> mh = new MapHelper<String>(map);

        //if (logger.isDebugEnabled()) logger.debug("doPost: response: modelAsJson=" + modelAsJson);
        String responseStr = executeObjRequest(request, response, objName, mh);

        PrintWriter out = response.getWriter();
        out.print(responseStr);
        out.close();
    }

    private static String getRequestParams(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();

        boolean first = true;
        @SuppressWarnings("unchecked")
        Map<String, String[]> params = request.getParameterMap();

        for (Entry<String, String[]> param : params.entrySet()) {
            String paramName = param.getKey();
            String[] paramVals = param.getValue();
            String paramNameEncoded = UrlMakingUtils.encodeStringForUrlWW(paramName);

            for (String paramVal : paramVals) {
                if (first) {
                    first = false;
                } else {
                    sb.append("&");
                }
                sb.append(paramNameEncoded).
                        append("=").append(UrlMakingUtils.encodeStringForUrlWW(paramVal));
            }
        }

        return sb.toString();
    }

    private String getRequestHeaders(HttpServletRequest request) {
        return request.getHeader("user-agent");
    }

    @Override
    public void doPost(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
//        reinitLameLoggers();

        request.setCharacterEncoding("utf8");
//        if (appEnv.getUsesSubdomains()) {
//            String sId = request.getSession().getId();
//            Cookie ck = new Cookie("JSESSIONID", sId);
//            ck.setDomain(BaseUtils.getDomainFromGlobalUrl(appEnv.getCanonicalSiteUrl()));
//            response.addCookie(ck);
//        }

        String reqDescr = "???";
        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
            GlobalThreadStopWatch.startProcess("foxyServlet.doPost");
        }
        try {
            Date reqStartTime = new Date();
            String objName = request.getParameter("objName");
            String fileName = request.getParameter("get");
            String pageName = request.getParameter("page");
            boolean isObjectReq = !BaseUtils.isStrEmptyOrWhiteSpace(objName);
            boolean isPageReq = !isObjectReq && !BaseUtils.isStrEmptyOrWhiteSpace(pageName);
            boolean isServeReq = !isPageReq && !isObjectReq && !BaseUtils.isStrEmptyOrWhiteSpace(fileName);
            boolean isUploadReq = !isPageReq && !isObjectReq && !isServeReq
                    && ServletFileUpload.isMultipartContent(request);

            if (!isUploadReq && !isObjectReq && !isServeReq) {
                isPageReq = true;
            }

            String errorMsg = null;
            Throwable exception = null;
            try {
                try {
                    response.setContentType("text/plain;charset=UTF-8");

                    if (isUploadReq || isPageReq) {
                        response.setContentType("text/html;charset=UTF-8");
                    } else if (isObjectReq) {
                        response.setContentType("text/plain;charset=UTF-8");
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("isUploadReq=" + isUploadReq
                                + ", isServeReq=" + isServeReq
                                + ", isObjectReq=" + isObjectReq
                                + ", isPageReq=" + isPageReq
                                + ", objName=" + objName
                                + ", pageName=" + pageName
                                + ", fileName=" + fileName
                                + ", reqParams=" + getRequestParams(request));
                    }

                    if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                        if (GlobalThreadStopWatch.isProcInfoLoggingEnabled() || logger.isInfoEnabled()) {
                            if (isPageReq) {
                                reqDescr = "pageReq: " + pageName;
                            } else if (isObjectReq) {
                                reqDescr = "objReq: " + objName;
                            } else if (isServeReq) {
                                reqDescr = "serveFileReq: " + fileName;
                            } else {
                                reqDescr = "uploadReq";
                            }

                            GlobalThreadStopWatch.startProcessStage("entering proper process method for " + reqDescr);
                        }
                    }

                    if (isPageReq) {
                        processForPageName(pageName, request, response);
                    } else if (isObjectReq) {
                        //response.addHeader("Cache", "no-cache");
                        //response.addHeader("CacheControl", "no-cache");
                        //response.addHeader("Pragma", "no-cache");
                        processForObjName(objName, request, response);
                        //throw new LameRuntimeException("old style object request are no longer supported");
                    } else if (isServeReq) {
                        serveFile(fileName, response);
                    } else {
                        processUploadRequest(request, response);
                    }

                } catch (Throwable ex) {
                    //System.out.println("!!!! error 3 !!!!");
                    errorMsg = ex.getMessage();
                    exception = ex;
                }
            } finally {
                //System.out.println("!!!! error 3.1 !!!!");
                bizLogic.finishWorkUnit(exception == null);
                //System.out.println("!!!! error 3.2 !!!!");
                afterFinishedBizLogicWorkUnit(exception == null);
                //System.out.println("!!!! error 3.3 !!!!");
//            PostgresConnection.recycleThreadConnection();
            }

            try {
                Date reqEndTime = new Date();

                HttpSession sss = request.getSession(false);
                //System.out.println("!!!! error 3.4 !!!!");
                String sssId = sss == null ? "<null>" : sss.getId();

                //System.out.println("!!!! error 3.5 !!!!");
                bizLogic.logRequest(request.getRemoteAddr(), request.getRemotePort(),
                        request.getMethod(), request.getRequestURL().toString(),
                        getRequestHeaders(request),
                        getRequestParams(request), sssId,
                        (int) SimpleDateUtils.getElapsedMillis(reqStartTime, reqEndTime),
                        errorMsg, request.getHeader("referer"));
                //System.out.println("!!!! error 3.6 !!!!");

                //System.out.println("!!!! error 3.7 !!!!");
                bizLogic.finishWorkUnit(true);
                //System.out.println("!!!! error 3.8 !!!!");

            } catch (Exception ex) {
                //System.out.println("!!!! error 4 !!!!");
                exception = ex;
                bizLogic.finishWorkUnit(false);
                //System.out.println("!!!! error 4.1 !!!!");
            }

            //System.out.println("!!!! error 5 !!!!");
            if (exception != null) {
                throw new LameRuntimeException("error in servlet", exception);
            }
            //System.out.println("!!!! error 6 !!!!");

            try {
                //System.out.println("!!!! error 7 !!!!");
                Date reqEndTime = new Date();
                long reqElaMillis = SimpleDateUtils.getElapsedMillis(reqStartTime, reqEndTime);

                LameLogLevel lvl = LameLogLevel.Debug;
                boolean tookTooLong = reqElaMillis > REQ_EXECUTION_WARN_THRESHOLD_MILLIS;

                if (tookTooLong) {
                    lvl = LameLogLevel.Warn;
                } else if (isPageReq) {
                    lvl = LameLogLevel.Info;
                }

                //System.out.println("!!!! error 8 !!!!");
                if (logger.isEnabledAtLevel(lvl)) {
                    //System.out.println("!!!! error 8.1 !!!!");
                    Map<String, ThreadActivityCollector> tacMap = ThreadActivityCollector.getAndResetCollectedActivities();
                    //System.out.println("!!!! error 8.2 !!!!");
                    ThreadActivityCollector tac = BaseUtils.safeMapGet(tacMap, "db");
                    //System.out.println("!!!! error 8.3 !!!!");
                    long timeInDB = tac == null ? 0 : tac.getTotalElapsedMillis();
                    //System.out.println("!!!! error 8.4 !!!!");
                    logger.logAtLevel(lvl, "request" + (tookTooLong ? " (took too long!!!)" : "") + ": " + reqDescr + " (params: " + getRequestParams(request) + ") took "
                            + reqElaMillis
                            + " millis"
                            + (tac != null ? ", time in db: " + timeInDB + " ms (" + (reqElaMillis == 0 ? -1 : tac.getTotalElapsedMillis() * 100 / reqElaMillis)
                                    + "%)" : "")
                            + ", time outside db: " + (reqElaMillis - timeInDB) + " ms");
                    //System.out.println("!!!! error 8.5 !!!!");
                    if (tac != null) {
                        if (logger.isDebugEnabled()) {
                            //System.out.println("!!!! error 8.6 !!!!");
                            logger.debug("collected db activities in request: " + tac);
                            //System.out.println("!!!! error 8.7 !!!!");
                        }
                        //System.out.println("!!!! error 8.8 !!!!");
                    }
                    //System.out.println("!!!! error 8.9 !!!!");
                }
                //System.out.println("!!!! error 9 !!!!");

//                if (reqElaMillis > REQ_EXECUTION_WARN_THRESHOLD_MILLIS && logger.isWarnEnabled()) {
//                    logger.warn("request took too long: " + reqElaMillis + ", params: " + getRequestParams(request));
//                }
            } catch (Exception ex) {
                System.out.println("ERROR!!! failed at activity collector dump!");
                //ex.printStackTrace();
                System.out.println(StackTraceUtil.getCustomStackTrace(ex));
            }
            //System.out.println("!!!! error 9.1 !!!!");

        } finally {
            //System.out.println("!!!! error 9.2 !!!!");
            if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
                GlobalThreadStopWatch.endProcess(reqDescr);
            }
            //System.out.println("!!!! error 9.3 !!!!");
        }
        //System.out.println("!!!! error 9.4 !!!!");
    }

    // default implementation based on assumption that config is in
    // subpackage cfg at the same level as subpackage containing
    // servlet class (descendant of this class)
    protected String getConfigObjectBasePath() {
        String fullClassName = getClass().getCanonicalName();
        String packagePart = BaseUtils.dropLastItems(fullClassName, ".", 2);
        return "/" + packagePart.replace(".", "/") + "/cfg/";
    }
//    //private static final Map<String, IExpressionLanguage> exprLangs = FoxyMillUtils.getExprLangs(true);
//    private static final RenderEnvironment renderEnv = FoxyMillUtils.makeRenderEnv(true);
//
//    private String loadHtmlFile(String filePath) {
//        InputStream is = getServletConfig().getServletContext().getResourceAsStream(filePath);
//        if (is == null) {
//            throw new LameRuntimeException("no such file: " + filePath);
//        }
//        String pageContent = LameUtils.loadAsString(is, "UTF-8");
//        return pageContent;
//    }
//
//    private IRenderable readFMFile(String filePath) {
//        String pageContent = loadHtmlFile(filePath);
//
//        ParserTask pt = new ParserTask(filePath, pageContent);
//        final List<ITag> tags = pt.read();
//        FoxyMillUtils.prepareTags(tags, renderEnv);
//
//        return FoxyMillUtils.wrapAsRenderable(tags, null);
////                new IRenderable() {
////
////            public void render(IRenderContext renderCtx) {
////                FoxyMillUtils.render(tags, renderCtx);
////            }
////        };
//    }
//
//    private Map<String, IRenderable> getBoxes() {
//        Map<String, IRenderable> boxes = null;
//
//        if (boxes == null) {
//            boxes = new LinkedHashMap<String, IRenderable>();
//
//            ILameResourceProvider resProvider = new ServletResouceProvider(getServletContext());
//            Set<String> fileNames = resProvider.getResourceNames(FOXYMILL_PAGEFILES_PATH,
//                    BaseUtils.paramsAsSet(".html"), true);
//
//            // if (logger.isDebugEnabled()) logger.debug("fileNames=" + fileNames);
//
//            for (String fileName : fileNames) {
//                String boxName = BaseUtils.dropFileExt(
//                        BaseUtils.dropOptionalPrefix(fileName, FOXYMILL_PAGEFILES_PATH_SLASH) //BaseUtils.extractFileName(fileName)
//                        );
//                boxes.put(boxName, readFMFile(fileName));
//            }
//
//            //boxes.put("index", readFMFile(FOXYMILL_ROOT_PATH + "index.html"));
//        }
//        return boxes;
//    }
//
//    private Map<String, Object> collectGlobals(Properties props) {
//        ILameResourceProvider resProvider = new ServletResouceProvider(getServletContext());
//        Set<String> fileNames = resProvider.getResourceNames(FOXYMILL_GLOBALS_PATH,
//                BaseUtils.paramsAsSet(".html"), true);
//        //SimpleRenderContext renderCtx = new SimpleRenderContext(new FoxyMillModelBase(props));
//        IRenderCtx renderCtx = RenderCtx.makeRootRenderCtx(new FoxyMillModelBase(props));
//        for (String fileName : fileNames) {
//            IRenderable renderable = readFMFile(fileName);
//            renderable.render(renderCtx);
//        }
//        return renderCtx.getVars();
//    }
//
//    private Properties readProperties() {
//        Properties props = new Properties();
//        InputStream is = getServletConfig().getServletContext().getResourceAsStream(FOXYMILL_ROOT_PATH + "page-props.properties");
//
//        try {
//            try {
//                props.load(is);
//            } finally {
//                is.close();
//            }
//        } catch (Exception ex) {
//            throw new LameRuntimeException("cannot read props file", ex);
//        }
//        return props;
//    }
//    protected Class<? extends FoxyMillHttpModel> getModelClass() {
//        return FoxyMillHttpModel.class;
//    }
//    private static final String NAQNAQ_PROPER_SESSION_KEY = "_NAQNAQSESSION_";
    private static final String NAQNAQ_PROPER_SESSIONID_COOKIE_NAME = "_NNPSCN_";

//    protected HttpSession getNaqnaqSession(HttpServletRequest request) {
//        HttpSession s = request.getSession();
//        HttpSession properSession = (HttpSession) s.getAttribute(NAQNAQ_PROPER_SESSION_KEY);
//        if (properSession != null) {
//            return properSession;
//        }
//
//        String psID = FoxyServletUtils.getCookieValue(request, NAQNAQ_PROPER_SESSIONID_COOKIE_NAME, null);
//
//        if (psID == null) {
//            return s;
//        }
//
//        s.getSessionContext()
//    }
    private void processForPageName(String pageName, HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        IFoxyService foxyService = getFoxyService(request, response);
        //... poniższe powinna robić metoda getFoxyService ...
        foxyService.attachSessionEnvironment(new SessionEnvironment(request.getSession()/*, configObj*/));

        String res = foxyMillMentos.getBundle().renderPageFile(pageName,
                request, response, foxyService,
                //getModelClass(),
                getUploadFileSizeMax());

//        InputStream is = getServletConfig().getServletContext().getResourceAsStream("/WEB-INF/pages/index.html");
//        String pageContent = LameUtils.loadAsString(is, "UTF-8");
//
//        ParserTask pt = new ParserTask(pageContent);
//        List<ITag> tags = pt.read();
//        FoxyMillUtils.prepareTags(tags, exprLang);
//        Properties props = readProperties();
//        Map<String, Object> globals = collectGlobals(props);
//
//        Map<String, IRenderable> boxes = getBoxes();
//
//        Date start = new Date();
//        Map map = FoxyServletUtils.getParamsAsMap(request);
//        map.remove("page");
//        MapHelper<String> mh = new MapHelper<String>(map);
//
//        RenderCtx rc = RenderCtx.makeRootRenderCtx(
////        SimpleRenderContext rc = new SimpleRenderContext(
//                new FoxyMillHttpModel(
//                request, response, boxes, getFoxyService(request),
//                pageName, mh, globals, props, renderEnv.cacheStorage));
//
//        //FoxyMillUtils.render(tags, rc);
//        Object pageVal;
////        if (pageName.startsWith("action_")) {
////            pageVal = globals.get(pageName);
////        } else {
////            IRenderable page = readFMFile("/WEB-INF/pages/index.html");
////            pageVal = page;
////        }
//
//        pageVal = globals.get("dispatchRequest");
//
//        String res;
//        if (pageVal instanceof IRenderable) {
//            IRenderable page = (IRenderable) pageVal;
//            page.render(rc);
//            res = rc.getOutputStr();
//        } else {
//            res = BaseUtils.safeToString(pageVal);
//        }
//
//        Date end = new Date();
//
//        if (logger.isDebugEnabled()) logger.debug("rendered page=" + pageName + ", time=" + SimpleDateUtils.getElapsedSecondsWithMilliPrec(start, end) + " s.");
//
        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
            GlobalThreadStopWatch.startProcessStage("before print res to response");
        }

        PrintWriter out = response.getWriter();

        if (res != null) {
            out.print(res);
        }
        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
            GlobalThreadStopWatch.startProcessStage("before close output of response");
        }
        out.close();
        if (GlobalThreadStopWatch.IsGlobalThreadStopWatchEnabled) {
            GlobalThreadStopWatch.startProcessStage("after close output of response");
        }
    }

    protected void afterFinishedBizLogicWorkUnit(boolean success) {
        //no-op
    }

    protected void initWithNewBizLogic() {
        // no-op
    }
    private FoxyUrlRewriter urlRewriter;

    protected FoxyUrlRewriter getUrlRewriter() {
        if (urlRewriter == null) {
            String fileName = FoxyUrlRewriteFilter.URLREWRITE_CONFIG_XML_IN_WEBINF;
            IResourceFromProvider<InputStream> res = new ResourceFromProvider<InputStream>(resProvider, fileName);
            IWatchedResourceFromProvider<InputStream> wr = new WatchedResourceFromProvider<InputStream>(res);

            urlRewriter = new FoxyUrlRewriter(wr, fileName);
        }

        return urlRewriter;
    }
}
