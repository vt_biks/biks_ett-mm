exec sp_check_version '1.1.4.4';
go

------------------------------------
------------------------------------

delete
--select * 
from bik_searchable_attr_val where node_id in (select id from bik_node where is_deleted = 1)
go

------------------------------------
------------------------------------

exec sp_update_version '1.1.4.4', '1.1.4.5';
go
