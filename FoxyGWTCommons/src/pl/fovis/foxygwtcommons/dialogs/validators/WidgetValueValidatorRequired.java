/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs.validators;

import com.google.gwt.user.client.ui.Widget;
import pl.fovis.foxygwtcommons.dialogs.FoxyDialogSerializer;
import pl.fovis.foxygwtcommons.i18n.I18n;

/**
 *
 * @author pmielanczuk
 */
public class WidgetValueValidatorRequired<BB, INB extends BB, OUTB extends BB> implements IWidgetValueValidator {

    protected final Widget inputControl;
    protected final FoxyDialogSerializer<BB, INB, OUTB> serializer;
    protected boolean doTrimValue;

    public WidgetValueValidatorRequired(FoxyDialogSerializer<BB, INB, OUTB> serializer, Widget inputControl,
            boolean doTrimValue) {
        this.inputControl = inputControl;
        this.serializer = serializer;
        this.doTrimValue = doTrimValue;
    }

    public String validate() throws FoxyGWTValidationException {
        Object value = serializer.getInputControlValue(inputControl);
        if (doTrimValue && value instanceof String) {
            value = ((String)value).trim();
        }
        return value == null ? I18n.poleNieMozeBycPuste.get() /* I18N:  */ : null;
    }
}
