/*
 * BaseUrlResourceProvider.java
 *
 * Created on 10 październik 2007, 12:21
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public abstract class BaseUrlResourceProvider implements IResourceProvider {
     
    protected BaseUrlResourceProvider() {
    }
    
    protected abstract URL getUrlForResource(String resourceName);
    
    protected String noUrlExplanation(String resourceName) {
        return this.getClass() + 
                ".getCheckedUrlForResource: url not found for resource: " + 
                resourceName;
    }
    
    protected URL getCheckedUrlForResource(String resourceName) {
        URL res = getUrlForResource(resourceName);
        if (res == null) {
            throw new LameRuntimeException(noUrlExplanation(resourceName));
        }
        return res;
    }
    
    public long getLastModified(String resourceName) {
        URL url = getCheckedUrlForResource(resourceName);
        try {
            return url.openConnection().getLastModified();
        } catch (IOException ex) {
            throw new LameRuntimeException("getLastModified: " + resourceName, ex);
        }
    }
    
    protected InputStream getInputStreamForResource(String resourceName) {
        URL url = getCheckedUrlForResource(resourceName);
        try {
            return url.openStream();
        } catch (IOException ex) {
            throw new LameRuntimeException("getInputStreamForResource: " + resourceName, ex);
        }
    }
    
    public byte[] loadAsBytes(String resourceName) {
        InputStream is = getInputStreamForResource(resourceName);
        try {
            return LameUtils.inputStreamToByteArray(is);
        } catch (IOException ex) {
            throw new LameRuntimeException("loadAsBytes: " + resourceName, ex);
        }
    }
}
