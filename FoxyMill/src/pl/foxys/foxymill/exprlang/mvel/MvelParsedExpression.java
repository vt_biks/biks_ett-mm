/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.exprlang.mvel;

import commonlib.LameUtils;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.mvel2.MVEL;
import pl.foxys.foxymill.exprlang.ParsedExpressionBase;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class MvelParsedExpression extends ParsedExpressionBase<Serializable> {

    public static final boolean CollectMvelExpressions = false;
    private static Map<String, List<Pair<String, Integer>>> exprsInFiles =
            new LinkedHashMap<String, List<Pair<String, Integer>>>();

    private static synchronized void addToExprsInFiles(String fileName, String exprStr, int rowNum) {
        if (CollectMvelExpressions) {
            LameUtils.addToCollectingMapWithList(exprsInFiles, fileName, new Pair<String, Integer>(exprStr, rowNum));
        }
    }

    public static synchronized void clearExprsInFiles() {
        exprsInFiles.clear();
    }

    public static synchronized String getPrettyExprsInFiles() {
        StringBuilder sb = new StringBuilder();

        for (String fileName : exprsInFiles.keySet()) {
            List<Pair<String, Integer>> lst = exprsInFiles.get(fileName);

            sb.append(fileName).append(" (").append(lst.size()).append("):\n");

            for (Pair<String, Integer> item : lst) {
                sb.append("  ").append(BaseUtils.padIntEx(item.v2, 4, ' ')).
                        append(" : ").append(item.v1).append("\n");
            }
        }

        return sb.toString();
    }

    @Override
    protected Serializable innerParse() throws Exception {
        if (CollectMvelExpressions) {
            addToExprsInFiles(filePos.getFileName(), exprStr, filePos.getRow());
        }
        return MVEL.compileExpression(exprStr);
    }

    @Override
    protected Object innerEval(//IRenderContext
            IRenderCtx renderCtx) throws Exception {
        return MVEL.executeExpression(parsedExpr, renderCtx.//getEvalContext(),
                getRootModel(), renderCtx.getVars());
    }
}
