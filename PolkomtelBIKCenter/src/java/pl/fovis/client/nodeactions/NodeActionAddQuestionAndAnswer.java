/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.AddOrEditQuestionAndAnswer;
import pl.fovis.common.FrequentlyAskedQuestionsBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author beata
 */
public class NodeActionAddQuestionAndAnswer extends NodeActionUpdatingBase {

    @Override
    public boolean isEnabledByDesign() {
        return isNodeNotLinkedAnyhowNorReadOnlyTreeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_DOCUMENTS_FOLDER);
    }

    @Override
    public String getCaption() {
        return I18n.dodajArtykul.get();
    }

    @Override
    public void execute() {
        if (!isSocialUserLoggedInWithInfo()) {
            return;
        }

        new AddOrEditQuestionAndAnswer().buildAndShowDialog(null, new IParametrizedContinuation<FrequentlyAskedQuestionsBean>() {
            public void doIt(FrequentlyAskedQuestionsBean param) {
                BIKClientSingletons.getService().addFrequentlyAskedQuestions(node.id, param.questionText, param.answerText,
                        new StandardAsyncCallback<Integer>("Failed in inserting question and answer" /* I18N: no:err-fail-in */) {
                            public void onSuccess(Integer result) {
                                BIKClientSingletons.showInfo(I18n.dodanoArtykul.get() /* I18N:  */);
                                refreshRedisplayDataAndSelectRecord(result);
                            }
                        });
            }
        });
    }
}
