/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import commonlib.LameUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public class DiagServlet extends HttpServlet {

    public DiagServlet() {
    }

    @Override
    public void doPost(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        //response.addHeader("Location", "/WebSvr/Servlet1?page=tab7&bizObj=XYZ");
        //response.sendRedirect("/WebSvr/Servlet1?page=tab7&bizObj=XYZ");
        String firstNameParamVal = request.getParameter("firstname");
        boolean mustRunGcSlow = firstNameParamVal != null;
        if (mustRunGcSlow) {
            LameUtils.runGc(10, 100);
        }
        response.setContentType("text/html");
        //response.setContentType("application/xhtml+xml");
        PrintWriter out = response.getWriter();

        // Print the HTML header
        out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
        out.println("<HTML xmlns=\"http://www.w3.org/1999/xhtml\"><HEAD><TITLE>");
        out.println("Request info");
        out.println("</TITLE></HEAD>");

        // Print the HTML body
        out.println("<BODY>");
        //InputStream is = getClass().getResourceAsStream("/empik/testfile.txt");
        //out.println("<p>testfile: " + (is == null ? "<null>" : LameUtils.loadAsString(is) + "</p>"));
        out.println("<H1>Form</H1>");
        out.println("<form action=\"" + getServletName() + "?firstname=7&amp;firstname=13\" method=\"post\">");
        out.println("<P>");
        out.println("<LABEL for=\"firstname\">First name: </LABEL>");
        out.println("<INPUT type=\"text\" name=\"firstname\" value=\"" + firstNameParamVal + "\" /><BR />");
        out.println("<INPUT type=\"submit\" />");
        if (mustRunGcSlow) {
            out.println("gc(slow) have been performed");
        } else {
            out.println("press submit to run GC(slow) and see updated mem-usage stats ;-)");
        }
        out.println("</P>");
        out.println("</form>");
        
        out.println("<H1>Request info</H1><PRE>");
        out.println("getCharacterEncoding: " + request.getCharacterEncoding());
        out.println("getContentLength: " + request.getContentLength());
        out.println("getContentType: " + request.getContentType());
        out.println("getProtocol: " + request.getProtocol());
        out.println("getRemoteAddr: " + request.getRemoteAddr());
        out.println("getRemoteHost: " + request.getRemoteHost());
        out.println("getRemotePort: " + request.getRemotePort());
        out.println("getScheme: " + request.getScheme());
        out.println("getServerName: " + request.getServerName());
        out.println("getServerPort: " + request.getServerPort());
        out.println("getAuthType: " + request.getAuthType());
        out.println("getMethod: " + request.getMethod());
        out.println("getPathTranslated: " + request.getPathTranslated());
        out.println("getQueryString: " + request.getQueryString());
        out.println("getRemoteUser: " + request.getRemoteUser());
        out.println("getRequestURI: " + request.getRequestURI());
        out.println("getContextPath(req): " + request.getContextPath());
        out.println("getContextPath(ctx): " + getServletContext().getContextPath());
        out.println("getWebAppName: " + FoxyServletUtils.getWebAppRealName(getServletContext()));
        out.println("getPathInfo: " + request.getPathInfo());
        out.println("isRequestedSessionIdFromURL: " + request.isRequestedSessionIdFromURL());
        out.println("getServletPath: " + request.getServletPath());
        out.println("ctx.getRealPath(\"/\"): " + getServletContext().getRealPath("/"));
        out.println("getServletName(): " + getServletName());
        out.println("getServletConfig().getServletName(): " + getServletConfig().getServletName());
        out.println("getServletContext().getServletContextName(): " + getServletContext().getServletContextName());
        out.println("your request url webapp base: " + FoxyServletUtils.getRemoteWebAppUrl(request));
        out.println("your request url servlet base: " + FoxyServletUtils.getRemoteServletUrl(request));

        out.println();
        out.println("Parameters:");
        Enumeration paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String name = (String) paramNames.nextElement();
            String[] values = request.getParameterValues(name);
            out.println("    " + name + ":");
            for (int i = 0; i < values.length; i++) {
                out.println("      " + values[i]);
            }
        }

        out.println();
        out.println("Request headers:");
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String name = (String) headerNames.nextElement();
            String value = request.getHeader(name);
            out.println("  " + name + " : " + value);
        }

        out.println();
        out.println("Cookies:");
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                String name = cookies[i].getName();
                String value = cookies[i].getValue();
                out.println("  " + name + " : " + value);
            }
        }

        HttpSession s = request.getSession();

        out.println();
        out.println("s.getId(): " + s.getId());

        out.println();
        out.println("Session attributes:");
        @SuppressWarnings("unchecked")
        Enumeration<String> attrNames = s.getAttributeNames();

        while (attrNames.hasMoreElements()) {
            String attrName = attrNames.nextElement();
            out.println("  " + attrName + " : " + s.getAttribute(attrName));
        }

        out.println();
        out.println("mem-usage " + (mustRunGcSlow ? "after" : "without") + " running gc(slow):");
        out.println(LameUtils.getMemoryDiagInfoCompact(false, "\n") + "</PRE>");
        Map<String, Long> memUsageVals = LameUtils.getMemoryDiagVals(true);
        out.println(BaseUtils.formatMapOfIntNumbers(memUsageVals, "", "\n", " : ", true, true));

        // Print the HTML footer
        out.println("</PRE></BODY></HTML>");
        out.close();
    }

    @Override
    public void doGet(
            HttpServletRequest req,
            HttpServletResponse res)
            throws ServletException, IOException {
        doPost(req, res);
    }
}
