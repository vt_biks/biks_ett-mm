/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BlogBean;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author mgraczkowski
 */
public class UserBlogsWidget extends //EntityDetailsTabBase/*BlogsWidgetBase*/
        EntityDetailsTabPaginationBase<BlogBean> //        EntityDetailsTabWithItemsInEditableGridBase<BlogBean>
{

    protected Map<Integer, String> idsAndNamesFromBranches;

    @Override
    protected List<BlogBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
        this.idsAndNamesFromBranches = data.userRolesInNodes != null ? data.userRolesInNodes.v2 : null;
//        return data.blogs;
        return BaseUtils.isCollectionEmpty(items) ? new ArrayList<BlogBean>() : items;
    }

    @Override
    public String getCaption() {
        return /*BIKGWTConstants.MENU_NAME_USERS + "<br>" + BIKGWTConstants.MENU_NAME_BLOGS*/ I18n.notatki.get();
    }

    @Override
    public boolean hasContent() {
        return isNodeKindCode(getNodeKindCode(), BIKGWTConstants.NODE_KIND_USER) && !items.isEmpty();
    }

    @Override
    public boolean hasEditableControls() {
        return false;
    }

    @Override
    protected boolean hasAddJoinedButton() {
        return false;
    }

    @Override
    protected NodeAwareBeanExtractorBase<BlogBean> getBeanExtractor() {
//        EntityDetailsDataBean data = ctx.getNodeData();
//        Map<Integer, String> idsAndNamesFromBranches;
//        idsAndNamesFromBranches = data.userRolesInNodes != null ? data.userRolesInNodes.v2 : null;

        return new UserBlogBeanExtractor(hasAddJoinedButton, nodeId, idsAndNamesFromBranches);
    }

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_BLOGS;
    }

    @Override
    protected BikCustomButton createAddJoinedButton(Set<String> treeKinds) {
        return null;
    }

    @Override
    protected boolean isChangedViewDescriptionInJoinedPanelVisible() {
        return true;
    }

    @Override
    public void optCallServiceToGetData(final IContinuation con) {
        BIKClientSingletons.getService().getUserBlogs(nodeId, new AsyncCallback<List<BlogBean>>() {

            @Override
            public void onFailure(Throwable caught) {
                displayData();
            }

            @Override
            public void onSuccess(List<BlogBean> result) {
                items = result;
                con.doIt();
            }
        });
    }

    @Override
    public boolean isEnabled() {
        String nodeKindCode = getNodeKindCode();
        return BIKConstants.NODE_KIND_USER.equals(nodeKindCode);
    }
}
