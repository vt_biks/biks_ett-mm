if object_id('[dbo].[fn_get_menu_path_by_tree_code]') is not null
  drop function [dbo].[fn_get_menu_path_by_tree_code]
go

create function [dbo].[fn_get_menu_path_by_tree_code] (@tree_code varchar(255))
returns varchar(max)
as
begin
  declare @tree_of_trees_id int = (select id from bik_tree where code = 'TreeOfTrees');

  -- select * from bik_tree

  declare @node_id int = (select top 1 id from bik_node where tree_id = @tree_of_trees_id and obj_id = '$' + @tree_code and is_deleted = 0);

  declare @node_name varchar(max) = (select name from bik_node where id = @node_id);

  declare @res varchar(max) = case when @node_name = '@' then (select name from bik_tree where code = @tree_code) else @node_name end;

  while 1 = 1 begin
    set @node_id = (select parent_node_id from bik_node where id = @node_id);
    if @node_id is null break;

    set @node_name = (select name from bik_node where id = @node_id);

    set @res = @node_name + ' � ' + @res;
  end;

  return @res;
end;
go


---------------------------------

if object_id('[dbo].[fn_get_menu_path_by_tree_id]') is not null
  drop function [dbo].[fn_get_menu_path_by_tree_id]
go

create function [dbo].[fn_get_menu_path_by_tree_id] (@tree_id int)
returns varchar(max)
as
begin
  declare @tree_code varchar(255) = (select code from bik_tree where id = @tree_id);
  return [dbo].[fn_get_menu_path_by_tree_code](@tree_code);
end;
go


-------------------------------------------------------------
-------------------------------------------------------------
-------------------------------------------------------------
-------------------------------------------------------------



/*

select * from sys.objects where type = 'u' and name like 'bik_%sta%'

select * from sys.objects where type = 'u' and name like 'bik_%his%'

select * from sys.objects where type = 'u' and name like '%ser%'


select top 100 * from bik_service_request 
where method_name not in ('keepSessionAlive', 'getChangedRanking')
order by id desc



select * from sys.objects where name like '%tree%'


*/


/*

select top 100 * from bik_object_in_history

select top 100 * from bik_object_excluded_from_statistic
select top 100 * from bik_statistic_dict

select top 5 * from bik_statistic order by event_datetime desc
select top 5 * from bik_statistic_ext order by event_datetime desc

select count(1) from bik_statistic
select count(1) from bik_statistic_ext

select count(1) from bik_node where is_deleted <> 0

select count(1) from bik_node where is_deleted = 0

*/

/*
select
  case when grouping(menu_item) = 1 then '<Wszystko>' else menu_item end + case when grouping(year) = 1 then ' <razem>' else '' end as menu_item,
  case when grouping(year) = 1 then null else year end as year,
  sum(usage_cnt) as usage_cnt
from
  (
  select 
    [dbo].[fn_get_menu_path_by_tree_id](n.tree_id) as menu_item,
    datepart(year, date_added) as year,
    count(*) as usage_cnt
  from bik_object_in_history x inner join bik_node n on x.node_id = n.id
  group by datepart(year, date_added), --t.name, 
    [dbo].[fn_get_menu_path_by_tree_id](n.tree_id)
  ) x
group by menu_item, year with rollup
--order by case when menu_item = '<Wszystko>' then 1 else 0 end, menu_item, case when year is null then 1 else 0 end, year
*/



/*
select *, [dbo].[fn_get_menu_path_by_tree_code](code) as menu_path 
from bik_tree where is_hidden = 0 and code <> 'TreeOfTrees'
*/



/*
select t.id as tree_id, x.event_date 
from bik_statistic x inner join bik_tree t on x.counter_name = t.code
union all
select tree_id, event_date 
from bik_statistic_ext
--order by event_datetime desc
*/


-------------------------------------------------------------
-------------------------------------------------------------
-------------------------------------------------------------
-------------------------------------------------------------


select
  case when grouping(menu_item) = 1 then '<Wszystko>' else menu_item end + case when grouping(year) = 1 then ' <Razem>' else '' end as menu_item,
  case when grouping(year) = 1 then null else year end as year,
  sum(usage_cnt) as usage_cnt
from
  (
  select 
    [dbo].[fn_get_menu_path_by_tree_id](x.tree_id) as menu_item,
    datepart(year, x.event_date) as year,
    count(*) as usage_cnt
  from
    (
    select t.id as tree_id, x.event_date 
    from bik_statistic x inner join bik_tree t on x.counter_name = t.code
    union all
    select tree_id, event_date 
    from bik_statistic_ext
    union all
    select 
      n.tree_id as tree_id, x.date_added as event_date
    from bik_object_in_history x inner join bik_node n on x.node_id = n.id    
    ) x
  group by datepart(year, x.event_date), --t.name, 
    [dbo].[fn_get_menu_path_by_tree_id](x.tree_id)
  ) x
group by menu_item, year with rollup
--order by case when menu_item = '<Wszystko>' then 1 else 0 end, menu_item, case when year is null then 1 else 0 end, year

