/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.AttachmentBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author beata
 */
public class AttachmentBeanExtractor extends NodeAwareBeanWithAttributeExtractorBase<AttachmentBean> {

    protected int currentNodeId;

    public AttachmentBeanExtractor(boolean isAddJoinedButtonEnabled, int currentNodeId) {
        super(isAddJoinedButtonEnabled);
        this.currentNodeId = currentNodeId;
    }

    public Integer getNodeId(AttachmentBean b) {
        return b.nodeId;
    }

    public String getTreeCode(AttachmentBean b) {
        return b.treeCode;
    }

    protected String getName(AttachmentBean b) {
        return b.caption;
    }

    protected String getNodeKindCode(AttachmentBean b) {
        return b.dstCode;
    }

    @Override
    public boolean hasChildren(AttachmentBean b) {
        return b.dstHasChildren;
    }

    public String getBranchIds(AttachmentBean b) {
        return b.branchIds;
    }

    @Override
    protected void execActDelete(AttachmentBean b) {
        int nodeId = b.srcNodeId;
        BIKClientSingletons.getService().deleteBikLinkedMeta(nodeId, getNodeId(b), getRefreshingCallback(nodeId));
        deleteBeanFromList(b, I18n.usunietoZalacznik.get() /* I18N:  */);
        //refreshView("Usunieto załącznik");
    }

    @Override
    public boolean isDeleteBtnVisible2Wtf(AttachmentBean b) {
        return b.srcNodeId == currentNodeId || b.similarityScore != null;
    }

    @Override
    protected boolean hasInheritedMarkByName(AttachmentBean b) {
        return b.inheritToDescendants;
    }

    @Override
    public Object getAddColVal(AttachmentBean ab, int addColIdx) {
        return BIKClientSingletons.isDifferentViewType() ? getDescription(ab.descrPlain) : ab.attributes;
    }

    @Override
    public int getAddColCnt() {
        return BIKClientSingletons.isDescriptionInJoinedPanelVisible() ? 1 : 0;
    }

    @Override
    public Double getOptSimilarityScore(AttachmentBean b) {
        return b.similarityScore;
    }

    @Override
    protected Integer getCurrentNodeId() {
        return currentNodeId;
    }

    @Override
    public Integer getJoinedObjId(AttachmentBean b) {
        return b.joinedObjsId;
    }

    @Override
    protected void setAttributes(AttachmentBean b, String value) {
        b.attributes = value;
    }
}
