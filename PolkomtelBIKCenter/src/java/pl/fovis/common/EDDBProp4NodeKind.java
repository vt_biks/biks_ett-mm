/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author wezyr
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface EDDBProp4NodeKind {
    String[] value() default {};
}
