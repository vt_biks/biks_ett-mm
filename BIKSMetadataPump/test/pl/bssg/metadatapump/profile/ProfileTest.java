/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.profile;

import pl.bssg.metadatapump.common.ProfileBean;
import commonlib.LameUtils;
import java.util.List;
import java.util.Map;
import static pl.bssg.metadatapump.profile.ProfilePump.createListFromItemMap;
import simplelib.CSVReader;

/**
 *
 * @author tflorczak
 */
public class ProfileTest {

    public static void main(String[] args) {
        String csvContent = LameUtils.loadAsString("C:\\temp\\profile\\DBTBL1D-CIF.txt", "ISO-8859-2");
        CSVReader reader = new CSVReader(csvContent, false, '\t');
        // %LIBS, FID, DI, NOD, LEN, DFT, DOM, TBL, PTN, XPO, XPR, TYP, DES, ITP, MIN, MAX, DEC, REQ, CMP, SFD, SFD1, SFD2, SFP, SFT, SIZ, DEL, POS, RHD, SRL, CNV, LTD, USER, MDD, VAL4EXT, DEPREP, DEPOSTP, NULLIND, MDDFID, VALIDCMP
        List<Map<String, String>> content = reader.readFullTableAsMaps();
        List<ProfileBean> createListFromMap = createListFromItemMap(content);
        for (ProfileBean bean : createListFromMap) {
            System.out.println("-> " + bean);
        }
    }
}
