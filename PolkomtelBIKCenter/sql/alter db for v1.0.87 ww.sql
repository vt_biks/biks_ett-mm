--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_check_version]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_check_version]
GO

create procedure [dbo].[sp_check_version] (@ver varchar(255)) as
begin	
	declare @version varchar(255)
	select @version = val from bik_app_prop where name = 'bik_ver';
	if(@version != @ver)
	begin
	    declare @txt nvarchar(4000) = N'!!! Wrong version number: ' + @ver + 
	      '. Current version is ' + @version + '. !!!'
	    
	    while 1 = 1 begin
	    print @txt
	    
		raiserror(@txt,
				15,
				-1);
				
		print 'you must manually stop execution of this script!!!'
		print 'prosz� przerwa� dalsze wykonanie tego skryptu r�cznie!!!'
		
		select @txt
		select 'you must manually stop execution of this script!!!'
		select 'prosz� przerwa� dalsze wykonanie tego skryptu r�cznie!!!'
		
		WAITFOR DELAY '00:00:10';
	    end
	end	
end
GO

--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

exec sp_check_version '1.0.87';
go

--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

if not exists (select 1 from sys.all_columns where object_id = object_id('bik_node') and name = 'search_rank')
alter table bik_node add search_rank int not null default 0;
go

if not exists (select 1 from sys.all_columns where object_id = object_id('bik_node_kind') and name = 'search_rank')
alter table bik_node_kind add search_rank int not null default 0;
go


--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

exec sp_update_version '1.0.87', '1.0.87.1';
go
