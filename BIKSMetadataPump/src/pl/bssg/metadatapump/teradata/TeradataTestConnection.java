/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.teradata;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import pl.bssg.metadatapump.MetadataTestConnectionBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.serverlogic.DriverNameConstants;
import pl.trzy0.foxy.serverlogic.db.ConnectionUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class TeradataTestConnection extends MetadataTestConnectionBase {

    protected Connection tc = null;

    public TeradataTestConnection(MssqlConnectionConfig mcfg) {
        super(mcfg);
    }

    @Override
    public Pair<Integer, String> testConnection() {
        TeradataConfigBean configData = null;
        try {
            configData = PumpUtils.readAdminBean("teradata", TeradataConfigBean.class, adhocDao);
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Invalid login for data loading! Error: " + e.getMessage());
        }
        if (configData == null) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Error with loading config data!");
        }
        try {
//            tc = PumpUtils.getConnection(teradataDriverClass, teradataUrl + "/tmode=ANSI,charset=UTF8", teradataUser, teradataPassword);
//            Class.forName(teradataDriverClass);
            tc = ConnectionUtils.getConnectionByConnectionString(DriverNameConstants.TERADATA, configData.url, configData.user, configData.password, "tmode=ANSI,charset=UTF8");
            if (tc == null) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connection with Teradata!");
            }
            if (configData.select == null) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Wrong select query");
            }
            Statement createStatement = tc.createStatement();
            ResultSet executeQuery = createStatement.executeQuery(configData.select);
            if (executeQuery == null || !executeQuery.next()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Wrong select query or no access");
            }
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connection with Teradata! Error: " + e.getMessage());
        } finally {
            if (tc != null) {
                PumpUtils.closeConnection(tc);
            }
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, PumpConstants.PUMP_TEST_CONNECTION_OK);
    }
}
