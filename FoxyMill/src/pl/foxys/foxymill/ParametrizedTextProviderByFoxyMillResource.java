/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill;

import commonlib.LameUtils;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import pl.foxys.foxymill.renderctx.RenderCtx;
import pl.trzy0.foxy.commons.IParametrizedNamedTextProvider;
import pl.trzy0.foxy.commons.templates.IParamSignature;
import simplelib.BaseUtils;
import simplelib.IGenericResourceProvider;
import simplelib.IResourceFromProvider;
import simplelib.IWatchedGenericResourceProvider;
import simplelib.IWatchedResourceFromProvider;
import simplelib.IWatchedResources;
import simplelib.LameRuntimeException;
import simplelib.SingletonResourceProvider;
import simplelib.WatchedGenericResourceProvider;
import simplelib.WatchedResourceFromProvider;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class ParametrizedTextProviderByFoxyMillResource implements IParametrizedNamedTextProvider {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    public static boolean DumpMemUsageModeOn = false;
    private IWatchedResources rrr;
    private IGenericResourceProvider<String> resProvider;
    private Set<String> fileNames = new LinkedHashSet<String>();
    private Map<String, Object> globals;
    private RenderEnvironment renderEnv;
    private Object model;

    public ParametrizedTextProviderByFoxyMillResource(IGenericResourceProvider<String> resProvider,
            Collection<String> fileNames,
            RenderEnvironment renderEnv, Object model) {
        IWatchedGenericResourceProvider<String> wgrp = new WatchedGenericResourceProvider<String>(resProvider);

        innerSetup(wgrp, wgrp, renderEnv, model);

        this.fileNames.addAll(fileNames);
        //ww: to zainicjalizuje listę obserwowanych zasobów
        for (String fileName : fileNames) {
            wgrp.hasBeenModifiedRecently(fileName);
        }
    }

    public ParametrizedTextProviderByFoxyMillResource(IResourceFromProvider<String> srcResource,
            RenderEnvironment renderEnv, Object model) {
        IWatchedResourceFromProvider<String> wrfp = new WatchedResourceFromProvider<String>(srcResource);
        innerSetup(wrfp, new SingletonResourceProvider<String>(wrfp), renderEnv, model);
        this.fileNames.add("x");
    }

    private void innerSetup(IWatchedResources rrr, IGenericResourceProvider<String> resProvider,
            RenderEnvironment renderEnv, Object model) {
        this.rrr = rrr;
        this.resProvider = resProvider;
        this.renderEnv = renderEnv;
        this.model = model;
    }

    public void setModel(Object model) {
        this.model = model;
    }

    protected IRenderableWithParamSignature findFresh(String name) {
        ensureFreshGlobals();

        Object rObj = globals.get(name);

        if (rObj == null) {
            return null;
        }

        if (rObj instanceof IRenderableWithParamSignature) {
            IRenderableWithParamSignature r = (IRenderableWithParamSignature) rObj;

            return r;
        }

        throw new LameRuntimeException("named text of unproper class: \"" + name
                + "\", class=" + BaseUtils.safeGetClassName(rObj));
    }

    public String provide(String name, Object... args) {
        if (DumpMemUsageModeOn) {
            LameUtils.dumpMemoryUsage(" >> provide: starts");
        }

        IRenderableWithParamSignature r = findFresh(name);

        if (DumpMemUsageModeOn) {
            LameUtils.dumpMemoryUsage(" >> provide: after fresh");
        }

        if (r == null) {
            throw new LameRuntimeException("no such named text: \"" + name + "\"");
        }

        Map<String, Object> vars = new LinkedHashMap<String, Object>();
        int i = 0;
        for (String paramName : r.getParamNames()) {
            if (i >= args.length) {
                throw new LameRuntimeException("named query " + name + " has more args than supplied ("
                        + args.length + ")");
            }
            vars.put(paramName, args[i]);
            i++;
        }

        if (i != args.length) {
            throw new LameRuntimeException("named query " + name + " has less args ("
                    + i + ") than supplied (" + args.length + ")");
        }

        if (DumpMemUsageModeOn) {
            LameUtils.dumpMemoryUsage(" >> provide: before makeRootRenderCtx");
        }
        IRenderCtx ctx = RenderCtx.makeRootRenderCtx(model, vars);
        if (DumpMemUsageModeOn) {
            LameUtils.dumpMemoryUsage(" >> provide: before renderToString");
        }
        String res = FoxyMillUtils.renderToString(ctx, r);

        if (logger.isInfoEnabled()) {
            logger.info("Named query: " + name + ", args: " + BaseUtils.arrayToString(args) + ". SQL: " + res);
        }

        if (DumpMemUsageModeOn) {
            LameUtils.dumpMemoryUsage(" >> provide: ends");
        }
        return res;
    }

    public IParamSignature getSignature(String name) {
        IRenderableWithParamSignature r = findFresh(name);
        if (r == null) {
            return null;
        }
        return r;
    }

    private void ensureFreshGlobals() {
        if (DumpMemUsageModeOn) {
            LameUtils.dumpMemoryUsage(" >> ensureFreshGlobals: starts");
        }
        boolean hbmr = rrr.hasBeenModifiedRecently();
        if (DumpMemUsageModeOn) {
            LameUtils.dumpMemoryUsage(" >> ensureFreshGlobals: after hasBeenModifiedRecently, hbmr=" + hbmr);
        }

//        if (!hbmr && globals == null) {
//            throw new LameRuntimeException("not modified recently and globals==null!");
//        }
        if (hbmr || globals == null) {
//            System.out.println("fileNames=" + fileNames);
            globals = GlobalDefsBuilder.buildGlobalsSimple(resProvider, renderEnv, fileNames, model);
            if (DumpMemUsageModeOn) {
                LameUtils.dumpMemoryUsage(" >> ensureFreshGlobals: after buildGlobalsSimple");
            }
        }

        if (globals == null) {
            throw new LameRuntimeException("was modified recently, rebuild but still globals==null!");
        }
    }

    public Map<String, Object> getGlobals() {
        ensureFreshGlobals();
        return globals;
    }
}
