/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.user.client.rpc.AsyncCallback;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public abstract class StandardAsyncCallback<T> implements AsyncCallback<T> {

    protected String errorMsg;

    public StandardAsyncCallback() {
        this(null);
    }

    public StandardAsyncCallback(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public void onFailure(Throwable caught) {
        //ww: tekst komunikatu trzeba pobrać dopiero tutaj, bo wcześniej może
        // być źle ustawiony język 
        showOnFailureError(BaseUtils.nullToDef(errorMsg, I18n.bladKomunikacji.get()), caught);
    }

    public static void showOnFailureError(String errorMsg, Throwable caught) {
        //ww: może lepsze by było FoxyClientSingletons.showError ???
        FoxyClientSingletons.showWarning(errorMsg, caught);
    }
}
