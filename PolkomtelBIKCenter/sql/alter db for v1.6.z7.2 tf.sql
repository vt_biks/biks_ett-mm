﻿-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------   Implementacja modułu Profile  ---------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
exec sp_check_version '1.6.z7.2';
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_dqm_request_error') and type in (N'U'))
begin
	create table bik_dqm_request_error (
		id bigint not null identity(1,1) primary key,
		request_id bigint not null references bik_dqm_request(id),
		row int not null,
		column_ordinal_position int not null,
		column_name varchar(512) null,
		value varchar(max) not null
	);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_dqm_request_error]') and name = N'uq_bik_dqm_request_error_request_id_row_column_ordinal_position')
begin
	create unique index uq_bik_dqm_request_error_request_id_row_column_ordinal_position on bik_dqm_request_error (request_id, row, column_ordinal_position);
end;
go

if not exists(select * from bik_node_kind where code = 'ProfileLibrary')
begin

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('ProfileLibrary', 'Biblioteka', 'profileLibrary', 'Metadata', 0, 10, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('ProfileFile', 'Plik', 'profileFile', 'Metadata', 0, 9, 0)

	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values ('ProfileDataItem', 'Pozycja danych', 'profileItem', 'Metadata', 0, 8, 1)
	
	-- tree
	insert into bik_tree(name, node_kind_id, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
	values('Profile', null, 'Profile', 'Metadata', 1, 0, 1, 0);
	
	insert into bik_translation(code, txt, lang, kind)
	values ('Profile', 'Profile', 'en', 'tree')
	
	insert into bik_icon(name)
	values  ('profileLibrary'),
			('profileFile'),
			('profileItem');
			
	insert into bik_translation(code, txt, lang, kind)
	values  ('ProfileLibrary', 'Library', 'en', 'kind'),
			('ProfileFile', 'File', 'en', 'kind'),
			('ProfileDataItem', 'Data Item', 'en', 'kind');
end;

declare @treeOfTreeId int = (select id from bik_tree where code = 'TreeOfTrees');

if not exists(select * from bik_node where tree_id = @treeOfTreeId and obj_id = '$Profile')
begin
	exec sp_add_menu_node 'metadata', '@', '$Profile'
	exec sp_add_menu_node '$Profile', '@', '&ProfileLibrary'
	exec sp_add_menu_node '&ProfileLibrary', '@', '&ProfileFile'
	exec sp_add_menu_node '&ProfileFile', '@', '&ProfileDataItem'
end;

exec sp_node_init_branch_id @treeOfTreeId, null
go

if not exists(select * from bik_admin where param = 'profile.isActive')
begin
	insert into bik_admin(param, value)
	values ('profile.isActive', '0')
	
	insert into bik_admin(param, value)
	values ('profile.path', '')
	
	insert into bik_admin(param, value)
	values ('profile.testPath', '')
	
	insert into bik_admin(param, value)
	values ('profile.host', '')
	
	insert into bik_admin(param, value)
	values ('profile.port', '22')
	
	insert into bik_admin(param, value)
	values ('profile.user', '')
	
	insert into bik_admin(param, value)
	values ('profile.password', '')
end;
go

declare @profileSourceId int = (select id from bik_data_source_def where description = 'Profile');
if not exists(select * from bik_schedule where source_id = @profileSourceId)
begin
	insert into bik_schedule(source_id, hour, minute, interval, is_schedule, priority)
	values (@profileSourceId, 0, 0, 1, 0, 13)
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_profile_item_extradata') and type in (N'U'))
begin
	create table aaa_profile_item_extradata (
		id bigint not null identity(1,1) primary key,
		libs varchar(256) not null,
		fid varchar(256) not null,
		di varchar(256) not null,
		nod varchar(256) null,
		len int null,
		dft varchar(256) null,
		dom varchar(256) null,
		tbl varchar(256) null,
		ptn varchar(256) null,
		xpo varchar(256) null,
		xpr varchar(256) null,
		typ varchar(256) null,
		des varchar(256) null,
		itp varchar(256) null,
		min varchar(256) null,
		max varchar(256) null,
		dec int null,
		req varchar(256) null,
		cmp varchar(256) null,
		sfd varchar(256) null,
		sfd1 int null,
		sfd2 int null,
		sfp int null,
		sft varchar(256) null,
		siz int null,
		del int null,
		pos int null,
		rhd varchar(256) null,
		srl varchar(256) null,
		cnv int null,
		ltd datetime null,
		usr varchar(256) null,
		mdd varchar(256) null,
		val4ext varchar(256) null,
		deprep varchar(256) null,
		depostp varchar(256) null,
		nullind varchar(256) null,
		mddfid varchar(256) null,
		validcmp varchar(256) null
		
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_profile_item_extradata') and type in (N'U'))
begin
	create table bik_profile_item_extradata (
		id bigint not null identity(1,1) primary key,
		item_node_id int not null references bik_node(id),
		libs varchar(256) not null,
		fid varchar(256) not null,
		di varchar(256) not null,
		nod varchar(256) null,
		len int null,
		dft varchar(256) null,
		dom varchar(256) null,
		tbl varchar(256) null,
		ptn varchar(256) null,
		xpo varchar(256) null,
		xpr varchar(256) null,
		typ varchar(256) null,
		des varchar(256) null,
		itp varchar(256) null,
		min varchar(256) null,
		max varchar(256) null,
		dec int null,
		req varchar(256) null,
		cmp varchar(256) null,
		sfd varchar(256) null,
		sfd1 int null,
		sfd2 int null,
		sfp int null,
		sft varchar(256) null,
		siz int null,
		del int null,
		pos int null,
		rhd varchar(256) null,
		srl varchar(256) null,
		cnv int null,
		ltd datetime null,
		usr varchar(256) null,
		mdd varchar(256) null,
		val4ext varchar(256) null,
		deprep varchar(256) null,
		depostp varchar(256) null,
		nullind varchar(256) null,
		mddfid varchar(256) null,
		validcmp varchar(256) null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_profile_file_extradata') and type in (N'U'))
begin
	create table aaa_profile_file_extradata (
		id bigint not null identity(1,1) primary key,
		libs varchar(256) not null,
		fid varchar(256) not null,
		global varchar(256) null,
		akey1 varchar(256) null,
		akey2 varchar(256) null,
		akey3 varchar(256) null,
		akey4 varchar(256) null,
		akey5 varchar(256) null,
		akey6 varchar(256) null,
		akey7 varchar(256) null,
		del int null,
		syssn varchar(256) null,
		netloc int null,
		parfid varchar(256) null,
		mplctdd varchar(256) null,
		dftdes varchar(256) null,
		dftord varchar(256) null,
		dfthdr varchar(256) null,
		dftdes1 varchar(256) null,
		ltd datetime null,
		usr varchar(256) null,
		filetyp int null,
		exist int null,
		extendlength varchar(256) null,
		fsn varchar(256) null,
		fdoc varchar(256) null,
		qid1 varchar(256) null,
		acckeys varchar(256) null,
		val4ext varchar(256) null,
		predaen varchar(256) null,
		screen varchar(256) null,
		rflag varchar(256) null,
		dflag varchar(256) null,
		udacc varchar(256) null,
		udfile varchar(256) null,
		fpn varchar(256) null,
		udpre varchar(256) null,
		udpost varchar(256) null,
		publish varchar(256) null,
		zrsfile varchar(256) null,
		glref varchar(256) null,
		rectyp int null,
		ptruser varchar(256) null,
		ptrtld varchar(256) null,
		log varchar(256) null,
		archfiles varchar(256) null,
		archkey varchar(256) null,
		ptrtim varchar(256) null,
		ptruseru varchar(256) null,
		ptrtldu varchar(256) null,
		ptrtimu varchar(256) null,
		listdft varchar(512) null,
		listreq varchar(512) null,
		des varchar(256) null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_profile_file_extradata') and type in (N'U'))
begin
	create table bik_profile_file_extradata (
		id bigint not null identity(1,1) primary key,
		item_node_id int not null references bik_node(id),
		libs varchar(256) not null,
		fid varchar(256) not null,
		global varchar(256) null,
		akey1 varchar(256) null,
		akey2 varchar(256) null,
		akey3 varchar(256) null,
		akey4 varchar(256) null,
		akey5 varchar(256) null,
		akey6 varchar(256) null,
		akey7 varchar(256) null,
		del int null,
		syssn varchar(256) null,
		netloc int null,
		parfid varchar(256) null,
		mplctdd varchar(256) null,
		dftdes varchar(256) null,
		dftord varchar(256) null,
		dfthdr varchar(256) null,
		dftdes1 varchar(256) null,
		ltd datetime null,
		usr varchar(256) null,
		filetyp int null,
		exist int null,
		extendlength varchar(256) null,
		fsn varchar(256) null,
		fdoc varchar(256) null,
		qid1 varchar(256) null,
		acckeys varchar(256) null,
		val4ext varchar(256) null,
		predaen varchar(256) null,
		screen varchar(256) null,
		rflag varchar(256) null,
		dflag varchar(256) null,
		udacc varchar(256) null,
		udfile varchar(256) null,
		fpn varchar(256) null,
		udpre varchar(256) null,
		udpost varchar(256) null,
		publish varchar(256) null,
		zrsfile varchar(256) null,
		glref varchar(256) null,
		rectyp int null,
		ptruser varchar(256) null,
		ptrtld varchar(256) null,
		log varchar(256) null,
		archfiles varchar(256) null,
		archkey varchar(256) null,
		ptrtim varchar(256) null,
		ptruseru varchar(256) null,
		ptrtldu varchar(256) null,
		ptrtimu varchar(256) null,
		listdft varchar(512) null,
		listreq varchar(512) null,
		des varchar(256) null
	);
end;
go


if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_profile_item_extradata]') and name = N'uq_bik_profile_item_extradata_item_node_id')
begin
	create unique index uq_bik_profile_item_extradata_item_node_id on bik_profile_item_extradata (item_node_id);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[aaa_profile_item_extradata]') and name = N'uq_aaa_profile_item_extradata_libs_fi_di')
begin
	create unique index uq_aaa_profile_item_extradata_libs_fi_di on aaa_profile_item_extradata (libs, fid, di);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_profile_file_extradata]') and name = N'uq_bik_profile_file_extradata_item_node_id')
begin
	create unique index uq_bik_profile_file_extradata_item_node_id on bik_profile_file_extradata (item_node_id);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[aaa_profile_file_extradata]') and name = N'uq_aaa_profile_file_extradata_libs_fi_di')
begin
	create unique index uq_aaa_profile_file_extradata_libs_fi_di on aaa_profile_file_extradata (libs, fid);
end;
go


if not exists (select * from bik_attr_category where name = 'Profile' and is_built_in = 1)
begin

	insert into bik_attr_category(name, is_built_in)
	values ('Profile', 1);

	declare @categoryId int = (select id from bik_attr_category where name = 'Profile' and is_built_in = 1);

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('libs', @categoryId, 1, '', 'shortText')

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('fid', @categoryId, 1, '', 'shortText')

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('di', @categoryId, 1, '', 'shortText')

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('nod', @categoryId, 1, '', 'shortText')

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('len', @categoryId, 1, '', 'shortText')

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('dft', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('dom', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('tbl', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('ptn', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('xpo', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('xpr', @categoryId, 1, '', 'shortText')
		
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('typ1', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('des', @categoryId, 1, '', 'shortText')
		
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('itp', @categoryId, 1, '', 'shortText')
		
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('min', @categoryId, 1, '', 'shortText')
		
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('max', @categoryId, 1, '', 'shortText')
		
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('dec', @categoryId, 1, '', 'shortText')
		
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('req', @categoryId, 1, '', 'shortText')
		
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('cmp', @categoryId, 1, '', 'shortText')
		
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('sfd', @categoryId, 1, '', 'shortText')
		
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('sfd1', @categoryId, 1, '', 'shortText')
		
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('sfd2', @categoryId, 1, '', 'shortText')
		
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('sfp', @categoryId, 1, '', 'shortText')
			
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('sft', @categoryId, 1, '', 'shortText')
			
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('siz', @categoryId, 1, '', 'shortText')
			
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('del', @categoryId, 1, '', 'shortText')
			
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('pos', @categoryId, 1, '', 'shortText')
			
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('rhd', @categoryId, 1, '', 'shortText')
			
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('srl', @categoryId, 1, '', 'shortText')
			
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('cnv', @categoryId, 1, '', 'shortText')
			
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('ltd', @categoryId, 1, '', 'shortText')
				
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('usr', @categoryId, 1, '', 'shortText')
				
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('mdd', @categoryId, 1, '', 'shortText')
				
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('val4ext', @categoryId, 1, '', 'shortText')
				
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('deprep', @categoryId, 1, '', 'shortText')
				
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('depostp', @categoryId, 1, '', 'shortText')
				
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('nullind', @categoryId, 1, '', 'shortText')
				
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('mddfid', @categoryId, 1, '', 'shortText')
				
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('validcmp', @categoryId, 1, '', 'shortText')
	

	-- tłumacznia
	insert into bik_translation (code, txt, lang, kind) values('libs','Library name', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('fid','File Name', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('di','Data Item Name', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('nod','Subscript Key', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('len','Maximum Field Length', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dft','Default Value', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dom','User-Defined Data Type', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('tbl','Look-Up table Name', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptn','MUMPS Pattern Match', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('xpo','Post Processor expression', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('xpr','Pre Processor Expression', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('typ1','Data Type', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('des','Description', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('itp','Internal Data Type', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('min','Minimum Value', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('max','Maximum Value', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dec','Decimal Precision', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('req','Required Indicator', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('cmp','Computed Expression', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('sfd','Sub Field Definition', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('sfd1','Sub-Field Delimitor (Tag Prefix)', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('sfd2','Sub-Field Delimiter (Tag Suffix)', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('sfp','Sub-Field Position', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('sft','Sub-Field Tag', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('siz','Field Display Size', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('del','Delimeter', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('pos','Field Position', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('rhd','Report Header', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('srl','Serial Value', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('cnv','Conversion Flag', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ltd','Last Updated', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('usr','User ID', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('mdd','Master Dictionary Reference', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('val4ext','Valid for Extraction', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('deprep','Data Entry Pre-Processor', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('depostp','Data Entry Post-Processor', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('nullind','Null Vs. Zero Indicator', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('mddfid','Master Dictionary File Name', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('validcmp','Validate the CMP field syntax', 'en', 'adef');
	-- a polskie są takie same
	insert into bik_translation (code, txt, lang, kind) values('libs','Library name', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('fid','File Name', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('di','Data Item Name', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('nod','Subscript Key', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('len','Maximum Field Length', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dft','Default Value', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dom','User-Defined Data Type', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('tbl','Look-Up table Name', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptn','MUMPS Pattern Match   ', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('xpo','Post Processor expression', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('xpr','Pre Processor Expression   ', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('typ1','Data Type', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('des','Description', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('itp','Internal Data Type', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('min','Minimum Value', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('max','Maximum Value', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dec','Decimal Precision', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('req','Required Indicator', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('cmp','Computed Expression', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('sfd','Sub Field Definition', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('sfd1','Sub-Field Delimitor (Tag Prefix)', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('sfd2','Sub-Field Delimiter (Tag Suffix) ', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('sfp','Sub-Field Position  ', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('sft','Sub-Field Tag', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('siz','Field Display Size', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('del','Delimeter', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('pos','Field Position', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('rhd','Report Header', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('srl','Serial Value', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('cnv','Conversion Flag', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ltd','Last Updated', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('usr','User ID', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('mdd','Master Dictionary Reference', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('val4ext','Valid for Extraction', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('deprep','Data Entry Pre-Processor', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('depostp','Data Entry Post-Processor', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('nullind','Null Vs. Zero Indicator', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('mddfid','Master Dictionary File Name', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('validcmp','Validate the CMP field syntax', 'pl', 'adef');


	--
    declare @profileItemNodeKindId int = dbo.fn_node_kind_id_by_code('ProfileDataItem');
    
    declare @libs int = (select id from bik_attr_def where name = 'libs' and is_built_in = 1);
	declare @fid int = (select id from bik_attr_def where name = 'fid' and is_built_in = 1);
	declare @di int = (select id from bik_attr_def where name = 'di' and is_built_in = 1);
	declare @nod int = (select id from bik_attr_def where name = 'nod' and is_built_in = 1);
	declare @len int = (select id from bik_attr_def where name = 'len' and is_built_in = 1);
	declare @dft int = (select id from bik_attr_def where name = 'dft' and is_built_in = 1);
	declare @dom int = (select id from bik_attr_def where name = 'dom' and is_built_in = 1);
	declare @tbl int = (select id from bik_attr_def where name = 'tbl' and is_built_in = 1);
	declare @ptn int = (select id from bik_attr_def where name = 'ptn' and is_built_in = 1);
	declare @xpo int = (select id from bik_attr_def where name = 'xpo' and is_built_in = 1);
	declare @xpr int = (select id from bik_attr_def where name = 'xpr' and is_built_in = 1);
	declare @typ int = (select id from bik_attr_def where name = 'typ1' and is_built_in = 1);
	declare @des int = (select id from bik_attr_def where name = 'des' and is_built_in = 1);
	declare @itp int = (select id from bik_attr_def where name = 'itp' and is_built_in = 1);
	declare @min int = (select id from bik_attr_def where name = 'min' and is_built_in = 1);
	declare @max int = (select id from bik_attr_def where name = 'max' and is_built_in = 1);
	declare @dec int = (select id from bik_attr_def where name = 'dec' and is_built_in = 1);
	declare @req int = (select id from bik_attr_def where name = 'req' and is_built_in = 1);
	declare @cmp int = (select id from bik_attr_def where name = 'cmp' and is_built_in = 1);
	declare @sfd int = (select id from bik_attr_def where name = 'sfd' and is_built_in = 1);
	declare @sfd1 int = (select id from bik_attr_def where name = 'sfd1' and is_built_in = 1);
	declare @sfd2 int = (select id from bik_attr_def where name = 'sfd2' and is_built_in = 1);
	declare @sfp int = (select id from bik_attr_def where name = 'sfp' and is_built_in = 1);
	declare @sft int = (select id from bik_attr_def where name = 'sft' and is_built_in = 1);
	declare @siz int = (select id from bik_attr_def where name = 'siz' and is_built_in = 1);
	declare @del int = (select id from bik_attr_def where name = 'del' and is_built_in = 1);
	declare @pos int = (select id from bik_attr_def where name = 'pos' and is_built_in = 1);
	declare @rhd int = (select id from bik_attr_def where name = 'rhd' and is_built_in = 1);
	declare @srl int = (select id from bik_attr_def where name = 'srl' and is_built_in = 1);
	declare @cnv int = (select id from bik_attr_def where name = 'cnv' and is_built_in = 1);
	declare @ltd int = (select id from bik_attr_def where name = 'ltd' and is_built_in = 1);
	declare @usr int = (select id from bik_attr_def where name = 'usr' and is_built_in = 1);
	declare @mdd int = (select id from bik_attr_def where name = 'mdd' and is_built_in = 1);
	declare @val4ext int = (select id from bik_attr_def where name = 'val4ext' and is_built_in = 1);
	declare @deprep int = (select id from bik_attr_def where name = 'deprep' and is_built_in = 1);
	declare @depostp int = (select id from bik_attr_def where name = 'depostp' and is_built_in = 1);
	declare @nullind int = (select id from bik_attr_def where name = 'nullind' and is_built_in = 1);
	declare @mddfid int = (select id from bik_attr_def where name = 'mddfid' and is_built_in = 1);
	declare @validcmp int = (select id from bik_attr_def where name = 'validcmp' and is_built_in = 1);

	--
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @libs, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @fid, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @di, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @nod, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @len, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @dft, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @dom, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @tbl, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @ptn, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @xpo, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @xpr, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @typ, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @des, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @itp, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @min, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @max, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @dec, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @req, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @cmp, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @sfd, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @sfd1, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @sfd2, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @sfp, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @sft, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @siz, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @del, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @pos, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @rhd, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @srl, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @cnv, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @ltd, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @usr, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @mdd, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @val4ext, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @deprep, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @depostp, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @nullind, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @mddfid, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileItemNodeKindId, @validcmp, 1);
	
	-- profile file extradata
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('global', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('akey1', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('akey2', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('akey3', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('akey4', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('akey5', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('akey6', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('akey7', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('syssn', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('netloc', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('parfid', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('mplctdd', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('dftdes', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('dftord', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('dfthdr', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('dftdes1', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('filetyp', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('exist', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('extendlength', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('fsn', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('fdoc', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('qid1', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('acckeys', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('predaen', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('screen', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('rflag', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('dflag', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('udacc', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('udfile', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('fpn', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('udpre', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('udpost', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('publish', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('zrsfile', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('glref', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('rectyp', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('ptruser', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('ptrtld', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('log', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('archfiles', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('archkey', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('ptrtim', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('ptruseru', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('ptrtldu', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('ptrtimu', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('listdft', @categoryId, 1, '', 'shortText');
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr) values ('listreq', @categoryId, 1, '', 'shortText');

	-- tłumaczenia
	insert into bik_translation (code, txt, lang, kind) values('global','Global Name', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey1','Access Key 1', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey2','Access Key 2', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey3','Access Key 3', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey4','Access Key 4', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey5','Access Key 5', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey6','Access Key 6', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey7','Access Key 7', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('syssn','System Name', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('netloc','Network Location', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('parfid','Inheritance File Name', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('mplctdd','Implicit Data Dictionary Reference', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dftdes','Look-up Table List', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dftord','Descending Order', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dfthdr','Default Heading', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dftdes1','Look-up Table List (Line 2)', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('filetyp','File Type', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('exist','Record Existed Indicator (node number)', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('extendlength','Extended File and Column Names Allowed', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('fsn','File Short Name', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('fdoc','Documentation File Name', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('qid1','Query', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('acckeys','Primary Keys', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('predaen','Data Entry Pre-Processor', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('screen','File Maintenance Data Entry Screen IDs', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('rflag','File Maintenance Restriction Flag', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dflag','Deletion Restriction Flag', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('udacc','Primary Keys Access Routine', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('udfile','Record Filer Routine', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('fpn','Data Item Protection Filename', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('udpre','User Defined Authorization Routine', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('udpost','User Defined File Post Processor', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('publish','Publish Routine', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('zrsfile','Rutyna restrykcji', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('glref','MUMPS Global Reference', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('rectyp','Record Type', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptruser','Data Item Name (User Created)', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptrtld','Data Item Name (Date Created)', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('log','Enable Automatic Logging', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('archfiles','Archive Filelist', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('archkey','Archive Key', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptrtim','Data Item@Name (Time Created)', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptruseru','Date Item Name (User Last Updated)', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptrtldu','Data Item Name (Date Last Updated)', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptrtimu','Data Item Name (Time Last Updated)', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('listdft','Default Data Item List', 'en', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('listreq','Required Data Item List', 'en', 'adef');
	-- polskie
	insert into bik_translation (code, txt, lang, kind) values('global','Global Name', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey1','Access Key 1', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey2','Access Key 2', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey3','Access Key 3', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey4','Access Key 4', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey5','Access Key 5', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey6','Access Key 6', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('akey7','Access Key 7', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('syssn','System Name', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('netloc','Network Location', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('parfid','Inheritance File Name', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('mplctdd','Implicit Data Dictionary Reference', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dftdes','Look-up Table List', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dftord','Descending Order', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dfthdr','Default Heading', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dftdes1','Look-up Table List (Line 2)', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('filetyp','File Type', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('exist','Record Existed Indicator (node number)', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('extendlength','Extended File and Column Names Allowed', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('fsn','File Short Name', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('fdoc','Documentation File Name', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('qid1','Query', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('acckeys','Primary Keys', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('predaen','Data Entry Pre-Processor', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('screen','File Maintenance Data Entry Screen IDs', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('rflag','File Maintenance Restriction Flag', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('dflag','Deletion Restriction Flag', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('udacc','Primary Keys Access Routine', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('udfile','Record Filer Routine', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('fpn','Data Item Protection Filename', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('udpre','User Defined Authorization Routine', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('udpost','User Defined File Post Processor', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('publish','Publish Routine', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('zrsfile','Rutyna restrykcji', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('glref','MUMPS Global Reference', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('rectyp','Record Type', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptruser','Data Item Name (User Created)', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptrtld','Data Item Name (Date Created)', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('log','Enable Automatic Logging', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('archfiles','Archive Filelist', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('archkey','Archive Key', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptrtim','Data Item@Name (Time Created)', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptruseru','Date Item Name (User Last Updated)', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptrtldu','Data Item Name (Date Last Updated)', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('ptrtimu','Data Item Name (Time Last Updated)', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('listdft','Default Data Item List', 'pl', 'adef');
	insert into bik_translation (code, txt, lang, kind) values('listreq','Required Data Item List', 'pl', 'adef');

	declare @profileFileNodeKindId int = dbo.fn_node_kind_id_by_code('ProfileFile');
    declare @global int = (select id from bik_attr_def where name = 'global' and is_built_in = 1);
	declare @akey1 int = (select id from bik_attr_def where name = 'akey1' and is_built_in = 1);
	declare @akey2 int = (select id from bik_attr_def where name = 'akey2' and is_built_in = 1);
	declare @akey3 int = (select id from bik_attr_def where name = 'akey3' and is_built_in = 1);
	declare @akey4 int = (select id from bik_attr_def where name = 'akey4' and is_built_in = 1);
	declare @akey5 int = (select id from bik_attr_def where name = 'akey5' and is_built_in = 1);
	declare @akey6 int = (select id from bik_attr_def where name = 'akey6' and is_built_in = 1);
	declare @akey7 int = (select id from bik_attr_def where name = 'akey7' and is_built_in = 1);
	declare @syssn int = (select id from bik_attr_def where name = 'syssn' and is_built_in = 1);
	declare @netloc int = (select id from bik_attr_def where name = 'netloc' and is_built_in = 1);
	declare @parfid int = (select id from bik_attr_def where name = 'parfid' and is_built_in = 1);
	declare @mplctdd int = (select id from bik_attr_def where name = 'mplctdd' and is_built_in = 1);
	declare @dftdes int = (select id from bik_attr_def where name = 'dftdes' and is_built_in = 1);
	declare @dftord int = (select id from bik_attr_def where name = 'dftord' and is_built_in = 1);
	declare @dfthdr int = (select id from bik_attr_def where name = 'dfthdr' and is_built_in = 1);
	declare @dftdes1 int = (select id from bik_attr_def where name = 'dftdes1' and is_built_in = 1);
	declare @filetyp int = (select id from bik_attr_def where name = 'filetyp' and is_built_in = 1);
	declare @exist int = (select id from bik_attr_def where name = 'exist' and is_built_in = 1);
	declare @extendlength int = (select id from bik_attr_def where name = 'extendlength' and is_built_in = 1);
	declare @fsn int = (select id from bik_attr_def where name = 'fsn' and is_built_in = 1);
	declare @fdoc int = (select id from bik_attr_def where name = 'fdoc' and is_built_in = 1);
	declare @qid1 int = (select id from bik_attr_def where name = 'qid1' and is_built_in = 1);
	declare @acckeys int = (select id from bik_attr_def where name = 'acckeys' and is_built_in = 1);
	declare @predaen int = (select id from bik_attr_def where name = 'predaen' and is_built_in = 1);
	declare @screen int = (select id from bik_attr_def where name = 'screen' and is_built_in = 1);
	declare @rflag int = (select id from bik_attr_def where name = 'rflag' and is_built_in = 1);
	declare @dflag int = (select id from bik_attr_def where name = 'dflag' and is_built_in = 1);
	declare @udacc int = (select id from bik_attr_def where name = 'udacc' and is_built_in = 1);
	declare @udfile int = (select id from bik_attr_def where name = 'udfile' and is_built_in = 1);
	declare @fpn int = (select id from bik_attr_def where name = 'fpn' and is_built_in = 1);
	declare @udpre int = (select id from bik_attr_def where name = 'udpre' and is_built_in = 1);
	declare @udpost int = (select id from bik_attr_def where name = 'udpost' and is_built_in = 1);
	declare @publish int = (select id from bik_attr_def where name = 'publish' and is_built_in = 1);
	declare @zrsfile int = (select id from bik_attr_def where name = 'zrsfile' and is_built_in = 1);
	declare @glref int = (select id from bik_attr_def where name = 'glref' and is_built_in = 1);
	declare @rectyp int = (select id from bik_attr_def where name = 'rectyp' and is_built_in = 1);
	declare @ptruser int = (select id from bik_attr_def where name = 'ptruser' and is_built_in = 1);
	declare @ptrtld int = (select id from bik_attr_def where name = 'ptrtld' and is_built_in = 1);
	declare @log int = (select id from bik_attr_def where name = 'log' and is_built_in = 1);
	declare @archfiles int = (select id from bik_attr_def where name = 'archfiles' and is_built_in = 1);
	declare @archkey int = (select id from bik_attr_def where name = 'archkey' and is_built_in = 1);
	declare @ptrtim int = (select id from bik_attr_def where name = 'ptrtim' and is_built_in = 1);
	declare @ptruseru int = (select id from bik_attr_def where name = 'ptruseru' and is_built_in = 1);
	declare @ptrtldu int = (select id from bik_attr_def where name = 'ptrtldu' and is_built_in = 1);
	declare @ptrtimu int = (select id from bik_attr_def where name = 'ptrtimu' and is_built_in = 1);
	declare @listdft int = (select id from bik_attr_def where name = 'listdft' and is_built_in = 1);
	declare @listreq int = (select id from bik_attr_def where name = 'listreq' and is_built_in = 1);

	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @libs, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @fid, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @global, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @akey1, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @akey2, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @akey3, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @akey4, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @akey5, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @akey6, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @akey7, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @del, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @syssn, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @netloc, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @parfid, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @mplctdd, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @dftdes, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @dftord, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @dfthdr, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @dftdes1, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @ltd, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @usr, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @filetyp, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @exist, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @extendlength, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @fsn, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @fdoc, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @qid1, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @acckeys, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @val4ext, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @predaen, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @screen, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @rflag, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @dflag, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @udacc, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @udfile, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @fpn, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @udpre, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @udpost, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @publish, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @zrsfile, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @glref, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @rectyp, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @ptruser, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @ptrtld, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @log, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @archfiles, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @archkey, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @ptrtim, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @ptruseru, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @ptrtldu, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @ptrtimu, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @listdft, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @listreq, 1);
	insert into bik_attr_system(node_kind_id, attr_id, is_visible) values (@profileFileNodeKindId, @des, 1);
    
end;
go


-- poprzednia wersja w pliku: alter db for v1.6.z6.7 tf.sql
-- dodanie konektora do Profile
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_metadata]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_metadata
go

create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
	declare @fixedOptNodeFilter varchar(max), @baseFixedOptNodeFilter varchar(max);
	if @optNodeFilter is null
	begin
		set @baseFixedOptNodeFilter = '(1 = 1)';
	end
	else
	begin
		set @baseFixedOptNodeFilter = '(' + @optNodeFilter + ')';
	end;

	-- SAP BO
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select report_tree_id from bik_sapbo_server union all select universe_tree_id from bik_sapbo_server union all select connection_tree_id from bik_sapbo_server))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_engine, database_source, connetion_networklayer_name, user_name, server', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table', null, @fixedOptNodeFilter
	
	declare @bo_extradata_sql varchar(max) = '(select node_id, author, owner, cuid, guid, ruid, convert(varchar(24),created,120) as created_time, convert(varchar(24),modified,120) as modified_time, convert(varchar(24),last_run_time,120) as last_run_time from bik_sapbo_extradata)';
	exec sp_verticalize_node_attrs_one_metadata_table @bo_extradata_sql, 'node_id', 'author, owner, cuid, guid, ruid, created_time, modified_time, last_run_time', null, @fixedOptNodeFilter
	
	declare @schedule_sql varchar(max) = '(select node_id, owner as schedule_owner, convert(varchar(24),creation_time,120) as creation_time, convert(varchar(24),nextruntime,120) as nextruntime, convert(varchar(24),expire,120) as expire, destination, format, parameters, destination_email_to from bik_sapbo_schedule)';
	exec sp_verticalize_node_attrs_one_metadata_table @schedule_sql, 'node_id', 'schedule_owner, destination, creation_time, nextruntime, expire, format, parameters, destination_email_to', null, @fixedOptNodeFilter
	
	declare @olap_sql varchar(max) = '(select node_id, type, provider_caption, cube_caption, cuid as olap_cuid, convert(varchar(24),created,120) as created, owner as olap_owner, convert(varchar(24),modified,120) as modified from bik_sapbo_olap_connection)';
	exec sp_verticalize_node_attrs_one_metadata_table @olap_sql, 'node_id', 'type, provider_caption, cube_caption, olap_cuid, created, olap_owner, modified', null, @fixedOptNodeFilter
	
	declare @obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as si_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''DataConnection'', ''Webi'', ''Flash'', ''CrystalReport'', ''Universe'', ''Excel'', ''FullClient'', ''Pdf'', ''Hyperlink'', ''Powerpoint'',
		''ReportFolder'', ''UniversesFolder'', ''ConnectionFolder'', ''ReportSchedule'', ''DSL.MetaDataFile'', ''CommonConnection'', ''CCIS.DataConnection''))'
	exec sp_verticalize_node_attrs_one_metadata_table @obj_id_sql, 'id', 'si_id', null, @fixedOptNodeFilter
	
	-- Bazy danych (Oracle, Postgres, ...)
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select distinct tree_id from bik_db_server))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_db_definition', 'object_node_id', 'definition', 'definition_sql', @fixedOptNodeFilter
	
	-- MS SQL
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''MSSQL''))';
	declare @mssql_tree_id int = (select id from bik_tree where code = 'MSSQL')
	declare @mssql_sql varchar(max) = '(select bn.id, ms.definition_text from bik_mssql ms 
		inner join bik_node bn on bn.obj_id = ms.branch_names
		where bn.tree_id = ' +  cast(@mssql_tree_id as varchar(20)) + '
		and bn.is_deleted = 0
		and linked_node_id is null
		and ms.definition_text is not null)'
	exec sp_verticalize_node_attrs_one_metadata_table @mssql_sql, 'id', 'definition_text', null, @fixedOptNodeFilter
	-- Usuwanie starych ex propsów
	delete from bik_searchable_attr_val where attr_id in 
	(select att.id from bik_searchable_attr att 
	left join (select distinct name from bik_mssql_extended_properties prop) x on att.name = '$exprop_' + x.name
	where att.name like '$exprop_%' and x.name is null)

	delete from bik_searchable_attr from bik_searchable_attr att 
	left join (select distinct name from bik_mssql_extended_properties prop) x on att.name = '$exprop_' + x.name
	where att.name like '$exprop_%' and x.name is null
	--
	exec sp_verticalize_node_attrs_ex_props @fixedOptNodeFilter
	  
	-- DQC
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''DQC''))';
	declare @tree_id int = (select id from bik_tree where code = 'DQC')
	declare @inact_nk_id int = (select id from bik_node_kind where code = 'DQCTestInactive'),
    @succ_nk_id int = (select id from bik_node_kind where code = 'DQCTestSuccess'),
    @fail_nk_id int = (select id from bik_node_kind where code = 'DQCTestFailed')

	declare @dqc_src_sql varchar(max) = '(select cast(dt.long_name as varchar(max)) as long_name, cast(dt.sampling_method as varchar(max)) as sampling_method, cast(dt.verified_attributes as varchar(max)) as verified_attributes,cast(dt.logging_details as varchar(max)) as logging_details, cast(dt.benchmark_definition as varchar(max)) as benchmark_definition, cast(dt.results_object as varchar(max)) as results_object, cast(dt.additional_information as varchar(max)) as additional_information, n.id as node_id
		from bik_dqc_test dt inner join bik_node n on dt.__obj_id = n.obj_id and n.node_kind_id in (' +
		cast(@inact_nk_id as varchar(20)) + ',' + cast(@succ_nk_id as varchar(20)) + ',' + cast(@fail_nk_id as varchar(20)) + ') and tree_id = ' + cast(@tree_id as varchar(20)) + '
		where n.linked_node_id is null and n.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @dqc_src_sql, 'node_id', 'long_name, sampling_method, verified_attributes,logging_details, benchmark_definition, results_object, additional_information', null, @fixedOptNodeFilter
	
	-- AD
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''UserRoles''))';
	declare @ad_src_sql varchar(max) = '(select bu.email, coalesce(bu.phone_num, ad.telephone_number) as phone_num,
		ad.physical_delivery_office_name,ad.postal_code, ad.manager, 
		ad.description, ad.department, ad.title, ad.mobile, 
		ad.display_name, bu.node_id
		from bik_user bu
		join bik_node bn on bn.id = bu.node_id and bn.is_deleted = 0
		left join bik_system_user bsu on bsu.user_id = bu.id 
		left join bik_active_directory ad 
		on (bu.login_name_for_ad = ad.s_a_m_account_name or bsu.login_name = ad.s_a_m_account_name))'
	exec sp_verticalize_node_attrs_one_metadata_table @ad_src_sql, 'node_id', 'email, phone_num, physical_delivery_office_name,postal_code,manager, description, department, title, mobile, display_name', null, @fixedOptNodeFilter
	
	-- SAP BW query extradata
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code in (''BWProviders'', ''BWReports'')))';
	declare @bwreports_id int = (select id from bik_tree where code = 'BWReports')
	declare @query_nk_id int = (select id from bik_node_kind where code = 'BWBEx')
	declare @sapbw_query_src_sql varchar(max) = '(select q.update_time, q.owner as sapbw_owner, q.last_edit, bn.id as node_id
		from bik_sapbw_query q inner join bik_node bn on q.obj_name = bn.obj_id and bn.node_kind_id = ' +
		cast(@query_nk_id as varchar(20)) + ' and bn.tree_id = ' + cast(@bwreports_id as varchar(20)) + '
		where bn.linked_node_id is null and bn.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @sapbw_query_src_sql, 'node_id', 'update_time, sapbw_owner, last_edit', null, @fixedOptNodeFilter
	
	-- SAP BW providers, query and areas - technical id
	declare @bw_obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWBEx'', ''BWArea'', ''BWMPRO'', ''BWCUBE'', ''BWISET'', ''BWODSO''))'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_obj_id_sql, 'id', 'technical_id', null, @fixedOptNodeFilter
	
	-- SAP BW objects - technical id
	declare @bw_objs_id_sql varchar(max) = '(select bn.id, obj.obj_name as obj_technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		inner join bik_sapbw_object obj on obj.provider + ''|'' + obj.obj_name = bn.obj_id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWUni'',''BWKyf'', ''BWTim'', ''BWDpa'', ''BWCha'')
		and obj.provider_parent is null)'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_objs_id_sql, 'id', 'obj_technical_id', null, @fixedOptNodeFilter
	
	-- Profile
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''Profile''))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_profile_item_extradata', 'item_node_id', 'libs, fid, di, nod, len, dft, dom, tbl, ptn, xpo, xpr, typ, des, itp, min, max, dec, req, cmp, sfd, sfd1, sfd2, sfp, sft, siz, del, pos, rhd, srl, cnv, ltd, usr, mdd, val4ext, deprep, depostp, nullind, mddfid, validcmp', 'libs, fid, di, nod, len, dft, dom, tbl, ptn, xpo, xpr, typ1, des, itp, min, max, dec, req, cmp, sfd, sfd1, sfd2, sfp, sft, siz, del, pos, rhd, srl, cnv, ltd, usr, mdd, val4ext, deprep, depostp, nullind, mddfid, validcmp', @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_profile_file_extradata', 'item_node_id', 'libs, fid, global, akey1, akey2, akey3, akey4, akey5, akey6, akey7, del, syssn, netloc, parfid, mplctdd, dftdes, dftord, dfthdr, dftdes1, ltd, usr, filetyp, exist, extendlength, fsn, fdoc, qid1, acckeys, val4ext, predaen, screen, rflag, dflag, udacc, udfile, fpn, udpre, udpost, publish, zrsfile, glref, rectyp, ptruser, ptrtld, log, archfiles, archkey, ptrtim, ptruseru, ptrtldu, ptrtimu, listdft, listreq, des', null, @fixedOptNodeFilter

end;
go


if not exists(select * from bik_admin where param = 'dqm.isActive')
begin
	insert into bik_admin(param, value)
	values ('dqm.isActive', '0');
	
	insert into bik_admin(param, value)
	values ('dqm.lastErrorRequests', '10');
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_dqm_schedule') and type in (N'U'))
begin
	create table bik_dqm_schedule (
		id int not null identity(1,1) primary key,
		test_id int not null references bik_dqm_test(id),
		hour int not null,
		minute int not null,
		interval int not null,
		date_started date null,
		is_schedule int not null,
		is_deleted int not null,
		priority int not null
	);
end;
go

exec sp_update_version '1.6.z7.2', '1.6.z8';
go