/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.util.List;

/**
 *
 * @author tflorczak
 */
public class ConnectionParametersDQMBean extends ParametersBaseBean {

    public int id;
    public Integer lastErrorRequests;
    public List<ConnectionParametersDQMConnectionsBean> connections;
}
