﻿exec sp_check_version '1.1.8.9';
go


if not exists (select * from bik_app_prop where name = 'myBIKSDemoMsg' )

begin

insert into bik_app_prop (name, val, is_editable)
	values(
		'myBIKSDemoMsg',
		'<h3>Witaj w systemie BI Knowledge System – wersja demonstracyjna </h3> BI Knowledge System jest narzędziem, które pozwala na zarządzanie wiedzą związaną z systemami Business Intelligence.Wersja demonstracyjna aplikacji jest w pełni funkcjonalnym programem, który umożliwia normalną pracę w systemie i pozwala poznać korzyści płynące z jego użytkowania. Zmiany zapisane przez użytkownika odwiedzającego wersję demonstracyjną BI Knowledge System będą usuwane na koniec każdego dnia roboczego. Wszystkie zawarte w niej dane są przykładowe. Treści opisów obiektów mogą nie być zgodne z prawdą, a zbieżność nazwisk użytkowników jest przypadkowa.',
		0	
	);

end


if not exists  (select * from bik_app_prop where name = 'myBIKSGeneralMsg')

begin
	
insert into bik_app_prop (name, val, is_editable)
	values(
		'myBIKSGeneralMsg',
		'<h3>Witaj w systemie BI Knowledge System</h3> Jest to narzędzie, które pozwala na zarządzanie wiedzą związaną z systemami Business Intelligence. Umożliwia gromadzenie w jednymi miejscu ogólnej i szczegółowej wiedzy związanej z wykorzystaniem BI w firmie w taki sposób, aby brak dostępu do osób ze specjalistyczną wiedzą nie powodował zatrzymania procesów związanych z podejmowaniem decyzji. BI Knowledge System pomaga również budować społeczność użytkowników BI dając im możliwość nieskrępowanej komunikacji i wymiany wiedzy.',
		0	
	);	
end	


exec sp_update_version '1.1.8.9', '1.1.8.10';
go