/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author wezyr
 */
public class BaseNodeRefreshingCallback<T> extends StandardAsyncCallback<T> {

    protected int nodeId;
    protected String msgOnSuccess;

    public BaseNodeRefreshingCallback(int nodeId, String msgOnSuccess, String errorMsg) {
        super(errorMsg);
        this.nodeId = nodeId;
        this.msgOnSuccess = msgOnSuccess;
    }

    public BaseNodeRefreshingCallback(int nodeId, String msgOnSuccess) {
        this(nodeId, msgOnSuccess, null);
    }

    protected void refreshNode() {
        BIKClientSingletons.getTabSelector().invalidateNodeData(nodeId);
    }

    public void onSuccess(T result) {
        if (msgOnSuccess != null) {
            BIKClientSingletons.showInfo(msgOnSuccess);
        }        
    }
}
