/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons;

import pl.trzy0.foxy.commons.templates.IParamSignature;

/**
 *
 * @author wezyr
 */
public interface IParametrizedNamedTextProvider {
    public String provide(String name, Object... args);
    public IParamSignature getSignature(String name);
}
