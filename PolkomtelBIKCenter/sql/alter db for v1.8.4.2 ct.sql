﻿exec sp_check_version '1.8.4.2';
go

update bik_node_kind set uploadable_children_kinds=null where code like 'metaBiks%'
 
declare @metaCategory int = (select id from bik_attr_category where name='Metadata BIKS')
exec sp_add_attr_def 'metaBIKS.Opis', @metaCategory, 'longText', null, 0, 0
exec sp_add_attr_2_node_kind 'metaBiksAttributeDef', 'metaBIKS.Opis', null, 1, null, null

exec sp_update_version '1.8.4.2', '1.8.4.3';
go
