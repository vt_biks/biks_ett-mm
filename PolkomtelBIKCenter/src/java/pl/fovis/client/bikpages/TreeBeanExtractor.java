/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.client.dialogs.DataDictionaryForTreeKindDialog;
import pl.fovis.client.dialogs.DataDictionaryForTreeKindDialog.TreeUpdateBean;
import pl.fovis.client.dialogs.ImportTreeLogDialog;
import pl.fovis.client.dialogs.ImportTreeLogDialog.TreeImportLogStatus;
import pl.fovis.client.dialogs.ImportTreeWithLinksAndJoinedObjsDialog;
import pl.fovis.client.dialogs.TreeIconDialog;
import static pl.fovis.client.entitydetailswidgets.GenericBeanExtractorBase.getIconUrlByIconName;
import pl.fovis.client.entitydetailswidgets.IGridBeanAction;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.client.entitydetailswidgets.SimpleGridBeanAction;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author beata
 */
public class TreeBeanExtractor extends NodeAwareBeanExtractorBase<TreeBean> {
//      Teraz mapka jest w BIKClientSingletons.getTreeKindCaptionsMap()
//    public static final Map<String, String> treeKindCaptions = new LinkedHashMap<String, String>() {
//
//        {
////            put(BIKGWTConstants.TREE_KIND_TAXONOMY, "Kategoryzacja");
//            put(BIKGWTConstants.TREE_KIND_DICTIONARY, "Słownik");
//            put(BIKGWTConstants.TREE_KIND_USERS, "Użytkownicy");
//        }
//    };

    protected Map<String, MenuNodeBean> menuByCodeMap = new HashMap<String, MenuNodeBean>();
    protected Map<Integer, MenuNodeBean> menuByIdMap = new HashMap<Integer, MenuNodeBean>();
    protected List<MenuNodeBean> menuNodes;
    protected Collection<TreeBean> trees;
    protected IGridBeanAction<TreeBean> actChangeTreeIcon = new SimpleGridBeanAction<TreeBean>() {
        @Override
        public String getActionIconUrl(TreeBean b) {
            return getIconUrlByIconName("itutorial");
        }

        @Override
        public String getActionIconHint(TreeBean b) {
            return I18n.zmienIkony.get() /* I18N:  */;
        }

        @Override
        public void executeAction(TreeBean b) {
            execActEditTreeIcons(b);
        }

        @Override
        public boolean isEnabled(TreeBean b) {
            return true; // po co sprawdzać? user widzi zakładkę to widzi też akcję.
//            return isEffectiveExpertLoggedInEx(b);
//            return BIKClientSingletons.isEffectiveExpertLoggedIn();
        }

        @Override
        public boolean isVisible(TreeBean b) {
            return true;
        }

        @Override
        public IGridBeanAction.TypeOfAction getActionType() {
            return IGridBeanAction.TypeOfAction.EditTreeIcon;
        }
    };

    protected IGridBeanAction<TreeBean> actImportLinks2Tree = new SimpleGridBeanAction<TreeBean>() {

        @Override
        public String getActionIconUrl(TreeBean b) {
            String url = getIconUrlByIconName("joined_single");
            url = url.substring(0, url.lastIndexOf(".")) + ".png";
            return url;
        }

        @Override
        public String getActionIconHint(TreeBean b) {
            return I18n.importDrzewa.get() + " " + b.name;
        }

        @Override
        public void executeAction(TreeBean b) {
            execActImportLinks2Tree(b);
        }

        @Override
        public boolean isVisible(TreeBean b) {
            return BIKClientSingletons.isEnableFullImportExportTree();
        }

        @Override
        public boolean isEnabled(TreeBean b) {
            return BIKClientSingletons.isEnableFullImportExportTree();
        }

        @Override
        public IGridBeanAction.TypeOfAction getActionType() {
            return IGridBeanAction.TypeOfAction.ImportLinksAndJoinedObjs;
        }
    };

    protected IGridBeanAction<TreeBean> actLogImportTree = new SimpleGridBeanAction<TreeBean>() {

        @Override
        public String getActionIconUrl(TreeBean b) {
            if (b.importStatus == TreeImportLogStatus.RUNNING.getCode()) {
                return getIconUrlByIconName("bwdpa_granat");
            } else if (b.importStatus == TreeImportLogStatus.SUCCESS.getCode()) {
                return getIconUrlByIconName("bwdpa_zielone");
            } else if (b.importStatus == TreeImportLogStatus.ERROR.getCode()) {
                return getIconUrlByIconName("bwdpa_czerwone");
            }

            return getIconUrlByIconName("bwdpa");
        }

        @Override
        public String getActionIconHint(TreeBean b) {
            return I18n.logi.get() + " " + b.name;
        }

        @Override
        public void executeAction(TreeBean b) {
            execActShowImportTreLog(b);
        }

        @Override
        public boolean isVisible(TreeBean b) {
            return true;
        }

        @Override
        public boolean isEnabled(TreeBean b) {
            return true;
        }

        @Override
        public IGridBeanAction.TypeOfAction getActionType() {
            return IGridBeanAction.TypeOfAction.ShowImportLog;
        }
    };

    private void execActShowImportTreLog(TreeBean b) {
        new ImportTreeLogDialog().buildAndShowDialog(b);
    }

    public TreeBeanExtractor(List<MenuNodeBean> menuNodes, Collection<TreeBean> trees) {
        super(false);
        this.menuNodes = menuNodes;
        this.trees = trees;

        menuByCodeMap = BaseUtils.projectToMap(menuNodes, new Projector<MenuNodeBean, String>() {

            @Override
            public String project(MenuNodeBean val) {
                return val.code;
            }
        });

        menuByIdMap = BaseUtils.makeBeanMap(menuNodes);
    }

    @Override
    public boolean hasIcon() {
        return false;
    }

    @Override
    protected String getNodeKindCode(TreeBean b) {
        return "";
    }

    @Override
    protected String getName(TreeBean b) {
        return b.name;// null;//b.name;
    }

    @Override
    public Integer getNodeId(TreeBean b) {
        return b.id;
    }

    @Override
    public String getTreeCode(TreeBean b) {
        return b.code;
    }

    @Override
    public String getBranchIds(TreeBean b) {
        return "";
    }

    @Override
    public int getAddColCnt() {
        return 2;
    }

    protected String getMenuPath(TreeBean b) {
        MenuNodeBean mnb = menuByCodeMap.get("$" + b.code);
        if (mnb == null) {
            return "(" + I18n.brak.get() /* I18N:  */ + ")";
        }

        List<String> captions = new ArrayList<String>();
        while (true) {
            mnb = menuByIdMap.get(mnb.parentNodeId);
            if (mnb == null) {
                break;
            }
            captions.add(mnb.name);
        }

        Collections.reverse(captions);

        return captions.isEmpty() ? "(" + I18n.glowneMenu.get() /* I18N:  */ + ")" : BaseUtils.mergeWithSepEx(captions, BIKGWTConstants.RAQUO_STR_SPACED);
    }

    @Override
    public Object getAddColVal(TreeBean b, int addColIdx) {
        if (addColIdx == 0) {
            String treeKind = b.treeKind;
            String caption = BaseUtils.nullToDef(BIKClientSingletons.getTreeKindCaptionsMap().get(treeKind), treeKind);
            return caption;
        } else {
            return getMenuPath(b);
        }
    }

    @Override
    public boolean createGridColumnNames(FlexTable gp) {
        gp.setStyleName("gridJoinedObj");
        int colNum = 0;
        gp.setWidget(0, colNum++, new HTML(I18n.nazwaDrzewa.get() /* I18N:  */));
//        gp.setWidget(0, 1, new HTML(""));
//        gp.setWidget(0, 2, new HTML(""));
        gp.setWidget(0, colNum++, new HTML(I18n.typDrzewa.get() /* I18N:  */));
        gp.setWidget(0, colNum++, new HTML(I18n.miejsceWMenu.get() /* I18N:  */));

        //ww: akcje są 3
        gp.getFlexCellFormatter().setColSpan(0, colNum, 3);
        gp.setWidget(0, colNum++, new HTML(I18n.akcje.get() /* I18N:  */));

        gp.getRowFormatter().setStyleName(0, "RelatedObj-oneObj-dqc paddedCells");

        return true;
    }

    @Override
    protected List<IGridBeanAction<TreeBean>> createActionList() {
        List<IGridBeanAction<TreeBean>> res = new ArrayList<IGridBeanAction<TreeBean>>();
        res.add(actChangeTreeIcon);
        res.add(nodeAwareActEdit);
        res.add(actDelete);
        res.add(actLogImportTree);
        res.add(actImportLinks2Tree);
        return res;
    }

    @Override
    protected void execActEdit(final TreeBean b) {
        BIKClientSingletons.getService().getTranslatedTrees(b.id, new StandardAsyncCallback<Map<String, String>>() {

            @Override
            public void onSuccess(Map<String, String> result) {
                new DataDictionaryForTreeKindDialog().buildAndShowDialog(b,
                        menuNodes, trees, result,
                        new IParametrizedContinuation<TreeUpdateBean>() {

                            @Override
                            public void doIt(final TreeUpdateBean param) {
                                BIKClientSingletons.showInfo(I18n.trwaPostepowanie.get());
                                BIKClientSingletons.getService().updateBikTree(b.id, param.name,
                                        param.parentMenuCode, BaseUtils.nullToDef(param.visualOrder, 0), param.isAutoObjId,
                                        param.optMenuSubGroup, !param.treeKind.equals(b.treeKind) ? param.treeKind : null,
                                        param.serverTreeFileName,/* param.serverLinkFileName,*/
                                        new StandardAsyncCallback<Void>("Failure on edit tree" /* I18N: no:err-fail-in */) {

                                            @Override
                                            public void onSuccess(Void result) {
                                                b.name = param.name.get(BIKGWTConstants.LANG_PL);
                                                refreshView(I18n.zmodyfikowanoDrzewo.get() /* I18N:  */);

//                                                if (!BaseUtils.isStrEmpty(param.parentMenuCode)) {
                                                DataDictionaryPage.showConfirmationAndMaybeReloadAfterTreeOperation(I18n.poEdycjiDrzewa);
//                                                BIKClientSingletons.callService2DeleteTmpFiles(param.tmpFileName2Delete);
//                                                }
                                            }

                                            @Override
                                            public void onFailure(Throwable caught) {
                                                super.onFailure(caught); //To change body of generated methods, choose Tools | Templates.
//                                                BIKClientSingletons.callService2DeleteTmpFiles(param.tmpFileName2Delete);
                                            }
                                        });
                            }
                        });
            }
        });
    }

    @Override
    protected void execActDelete(final TreeBean b) {
        new ConfirmDialog().buildAndShowDialog(I18n.czyNAPEWNOUsunacDrzewoWrazZJegoE.get() /* I18N:  */ + ".", I18n.usun.get() /* I18N:  */, new IContinuation() {

                    @Override
                    public void doIt() {
                        final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                        infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaUsuwanieDrzewaProszeCzekac.get(), true);

                        BIKClientSingletons.getService().deleteTree(b.id, new AsyncCallback<Void>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                infoDialog.hideDialog();
                                BIKClientSingletons.showError("Failure on deleteTree");
                            }

                            @Override
                            public void onSuccess(Void result) {
                                infoDialog.hideDialog();
                                deleteBeanFromList(b, I18n.usunietoDrzewo.get() /* I18N:  */);

                                DataDictionaryPage.showConfirmationAndMaybeReloadAfterTreeOperation(I18n.poUsunieciuDrzewa);
                            }
                        });
                    }
                });
    }

    protected void execActImportLinks2Tree(final TreeBean b) {
        new ImportTreeWithLinksAndJoinedObjsDialog().buildAndShowDialog(b);
    }

    protected void execActEditTreeIcons(final TreeBean b) {
        BIKClientSingletons.getService().getNodeKindsIconsForTree(b.id, b.treeKind, new StandardAsyncCallback<List<NodeKindBean>>("Error in getNodeKindsIconsForTree") {

            @Override
            public void onSuccess(List<NodeKindBean> result) {
                new TreeIconDialog().buildAndShowDialog(result, new IParametrizedContinuation<List<NodeKindBean>>() {

                    @Override
                    public void doIt(final List<NodeKindBean> param) {
                        BIKClientSingletons.getService().setNodeKindsIconsForTree(b.id, param, new StandardAsyncCallback<Void>("Error in setNodeKindsIconsForTree") {

                            @Override
                            public void onSuccess(Void result) {
                                Map<Pair<String, String>, String> treeIcons = BIKClientSingletons.getTreeIcons();
                                for (NodeKindBean nodeKindBean : param) {
                                    treeIcons.put(new Pair<String, String>(b.code, nodeKindBean.code), nodeKindBean.iconName);
                                }
                                BIKClientSingletons.showInfo(I18n.zapisanoIkonyDlaDrzewa.get());
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    public boolean isActEditBtnEnabled(TreeBean b) {
        return true;
    }

    @Override
    public boolean isActDeleteBtnEnabled(TreeBean b) {
        return true;
    }
}
