/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons.tags.htmltags;

import pl.trzy0.foxy.commons.tags.TagForestBuilder;

/**
 *
 * @author wezyr
 */
public class HtmlTagForestBuilder extends TagForestBuilder<IHtmlTag> {
    private HtmlTagLibrary lib;

    public HtmlTagForestBuilder() {
        super(new HtmlTagLibrary());
        this.lib = (HtmlTagLibrary)super.getLibrary();
    }

    @Override
    public HtmlTagLibrary getLibrary() {
        return lib;
    }
}
