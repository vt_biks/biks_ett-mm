/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.Label;
import java.util.List;
import java.util.Map;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class AddAnyNodeForLinkKindDialog extends AbstractAddAnyNodeDialog {

    @Override
    protected String getDialogCaption() {
        if (node != null && node.isRegistry) {
            return I18n.addEvent.get();
        } else {
            return I18n.addAnyNode.get();
        }
    }

    @Override
    protected void buildMainWidgets() {

        if (BaseUtils.isCollectionEmpty(nodeKinds)) {
            vp.add(new Label(I18n.brakZdefiniowanychTypowObiektow.get()));
        } else {
            someObjectsAreAvailable();
        }
    }

    public void buildAndShowDialog(Integer nodeKindId, Integer treeId, boolean isNodeFromDynamicTree, List<TreeNodeBean> tnb, TreeNodeBean node,
            IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>> saveCont
    /*IParametrizedContinuation<TreeNodeBean> saveCont*/) {
        this.nodeKindId = nodeKindId;
        this.treeId = treeId;
        this.isNodeFromDynamicTree = isNodeFromDynamicTree;
        this.saveCont = saveCont;
        this.nodeKinds = tnb;
        this.node = node;
        super.buildAndShowDialog();
    }

    @Override
    protected void fillAttributeFields() {
    }
}
