﻿exec sp_check_version '1.6.z4';
go


if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_report_schedule') and name = 'destination_email_to')
begin
	alter table aaa_report_schedule add destination_email_to varchar(max) null;
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_sapbo_schedule') and name = 'destination_email_to')
begin
	alter table bik_sapbo_schedule add destination_email_to varchar(max) null;
end;
go


-- poprzednia wersja w: alter db for v1.4.y1.17 tf.sql
-- dodanie do BO harmonogramów emaili adresatów
if exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
as
begin
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	declare @boServer int = dbo.fn_get_bo_actual_server_id();

	delete from bik_sapbo_extradata
	from bik_sapbo_extradata
	inner join bik_node bn on bn.id = bik_sapbo_extradata.node_id
	where bn.tree_id in (@reportTreeId , @universeTreeId, @connectionTreeId)
	
	
	declare @tempTable table (
		id int identity(1,1) not null,
		column_name varchar(50) not null,
		table_name varchar(50) not null
	);
	
	-- ważna jest kolejnosc wstawiania
	insert into @tempTable(column_name,table_name)
	values
		('SI_AUTHOR','INFO_WEBI'),
		('SI_OWNER','aaa_global_props'),
		('SI_KEYWORD','aaa_global_props'),
		('SI_FILES__SI_PATH','aaa_global_props'),
		('SI_FILES__SI_FILE1','aaa_global_props'),
		('SI_CREATION_TIME','aaa_global_props'),
		('SI_UPDATE_TS','aaa_global_props'),
		('SI_CUID','aaa_global_props'),
		('SI_FILES__SI_VALUE1','aaa_global_props'),
		('SI_SIZE','aaa_global_props'),
		('SI_HAS_CHILDREN','aaa_global_props'),
		('SI_GUID','aaa_global_props'),
		('SI_INSTANCE','aaa_global_props'),
		('SI_OWNERID','aaa_global_props'),
		('SI_PROGID','aaa_global_props'),
		('SI_OBTYPE','aaa_global_props'),
		('SI_FLAGS','aaa_global_props'),
		('SI_CHILDREN','aaa_global_props'),
		('SI_RUID','aaa_global_props'),
		('SI_RUNNABLE_OBJECT','aaa_global_props'),
		('SI_CONTENT_LOCALE','aaa_global_props'),
		('SI_IS_SCHEDULABLE','aaa_global_props'),
		('SI_WEBI_DOC_PROPERTIES','aaa_global_props'),
		('SI_READ_ONLY','aaa_global_props'),
		('SI_LAST_SUCCESSFUL_INSTANCE_ID','aaa_global_props'),
		('SI_LAST_RUN_TIME','aaa_global_props'),
		('SI_TIMESTAMP','INFO_WEBI'),
		('SI_PROGID_MACHINE','INFO_WEBI'),
		('SI_ENDTIME','aaa_global_props'),
		('SI_STARTTIME','aaa_global_props'),
		('SI_SCHEDULE_STATUS','aaa_global_props'),
		('SI_RECURRING','aaa_global_props'),
		('SI_NEXTRUNTIME','aaa_global_props'),
		('SI_DOC_SENDER','aaa_global_props'),
		('SI_REVISIONNUM','APP_UNIVERSE'),
		('SI_APPLICATION_OBJECT','APP_UNIVERSE'),
		('SI_SHORTNAME','APP_UNIVERSE'),
		('SI_DATACONNECTION__SI_TOTAL','APP_UNIVERSE'),
		('SI_UNIVERSE__SI_TOTAL','INFO_WEBI')
		
	declare @sqlText varchar(max);
	declare @count int;
	select @count = count(*) from @tempTable
	
	set @sqlText = 'insert into bik_sapbo_extradata (node_id,author,owner,keyword,file_path,file_name,created,modified,cuid,files_value,size,has_children,guid,instance,owner_id,progid,obtype,flags,children,ruid,runnable_object,content_locale,is_schedulable,webi_doc_properties,read_only,last_successful_instance_id,last_run_time,timestamp,progid_machine,endtime,starttime,schedule_status,recurring,nextruntime,doc_sender,revisionnum,application_object,shortname,dataconnection_total,universe_total )
    select bik.id as node_id, '
		
	declare kurs cursor for 
	select column_name, table_name from @tempTable order by id 

	open kurs;
	declare @column varchar(50);
	declare @table varchar(50);
	declare @i int = 1;

	fetch next from kurs into @column, @table;
	while @@fetch_status = 0
	begin
		if exists(select * from sys.columns where name = @column and Object_ID = Object_ID(@table))    
		begin
			set @sqlText = @sqlText + @table + '.' + @column
		end
		else
		begin
			set @sqlText = @sqlText + 'null'
		end
		if(@i <> @count)
		begin
			set @sqlText = @sqlText + ', '
		end
		--
		set @i = @i + 1;
		fetch next from kurs into @column, @table;
	end
	close kurs;
	deallocate kurs;	

	set @sqlText = @sqlText + ' from aaa_global_props
    left join INFO_WEBI on aaa_global_props.si_id = INFO_WEBI.si_id
    left join APP_UNIVERSE on aaa_global_props.si_id = APP_UNIVERSE.si_id
    inner join bik_node bik on convert(varchar(30),aaa_global_props.si_id) = bik.obj_id
    where bik.tree_id in (' + convert(varchar(10),@reportTreeId) + ', ' + convert(varchar(10),@universeTreeId) + ', ' + convert(varchar(10),@connectionTreeId) + ') 
    and bik.is_deleted = 0
    and bik.linked_node_id is null
    order by bik.id'
    
    exec(@sqlText);
    
    delete from bik_sapbo_schedule
    from bik_sapbo_schedule
	inner join bik_node bn on bn.id = bik_sapbo_schedule.node_id
	where bn.tree_id = @reportTreeId
    
    insert into bik_sapbo_schedule(node_id,destination,owner,creation_time,nextruntime,expire,type,schedule_type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters,destination_email_to)
    select bn.id,destination,owner,creation_time,nextruntime,expire,type,schedule_type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters,destination_email_to 
    from aaa_report_schedule sch
    inner join bik_node bn on convert(varchar(30),sch.id) = bn.obj_id
    --inner join bik_tree bt on bt.id = bn.tree_id
    where bn.tree_id = @reportTreeId 
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    --and bt.code in ('Reports')
    

    /*
	if exists(select 1 from bik_sapbo_server ser 
		inner join bik_data_source_def def on ser.source_id = def.id
		where ser.id = @boServer and def.description = 'Business Objects 4.x') and exists(select 1 from sys.objects where type = 'U' and name = 'APP_COMMONCONNECTION')
	begin
		    delete from bik_sapbo_olap_connection
			from bik_sapbo_olap_connection
			inner join bik_node bn on bik_sapbo_olap_connection.node_id = bn.id
			where bn.tree_id = @connectionTreeId
			
			insert into bik_sapbo_olap_connection(node_id, type, provider_caption, cube_caption, cuid, created, modified, owner, network_layer, md_username)
			select bn.id, app.SI_CONNECTION_TYPE, app.SI_PROVIDER_CAPTION, app.SI_CUBE_CAPTION, app.SI_CUID, app.SI_CREATION_TIME, app.SI_UPDATE_TS, app.SI_OWNER, app.SI_NETWORK_LAYER, app.SI_MD_USERNAME 
			from APP_COMMONCONNECTION app 
			inner join bik_node bn on bn.obj_id = convert(varchar(30),app.si_id) and tree_id = @connectionTreeId
	end
    */
end;
go

declare @catBO int = (select id from bik_attr_category where name = 'SAP BO' and is_built_in = 1);

if not exists(select 1 from bik_attr_def where name = 'Adresaci' and is_built_in = 1 and attr_category_id = @catBO)
begin
	insert into bik_attr_def(name,attr_category_id,is_deleted,is_built_in)
	values('Adresaci', @catBO, 0, 1)

	declare @scheduleKind int = (select id from bik_node_kind where code = 'ReportSchedule');

	insert into bik_attr_system(attr_id,node_kind_id,is_visible)
	select (select id from bik_attr_def where name = 'Adresaci' and attr_category_id = @catBO and is_built_in = 1), @scheduleKind, 1
	
	insert into bik_translation (code, lang, kind, txt) values ('Adresaci', 'en', 'adef', 'Recipients')
end;
go

-- poprzednia wersja w pliku: alter db for v1.6.z3.7 tf.sql 
-- fix na @optNodeFilter dla subskrypcji
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_metadata]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_metadata
go

create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
	declare @fixedOptNodeFilter varchar(max), @baseFixedOptNodeFilter varchar(max);
	if @optNodeFilter is null
	begin
		set @baseFixedOptNodeFilter = '(1 = 1)';
	end
	else
	begin
		set @baseFixedOptNodeFilter = '(' + @optNodeFilter + ')';
	end;

	-- SAP BO
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select report_tree_id from bik_sapbo_server union all select universe_tree_id from bik_sapbo_server union all select connection_tree_id from bik_sapbo_server))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_engine, database_source, connetion_networklayer_name, user_name, server', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table', null, @fixedOptNodeFilter
	
	declare @bo_extradata_sql varchar(max) = '(select node_id, author, owner, cuid, guid, ruid, convert(varchar(24),created,120) as created_time, convert(varchar(24),modified,120) as modified_time, convert(varchar(24),last_run_time,120) as last_run_time from bik_sapbo_extradata)';
	exec sp_verticalize_node_attrs_one_metadata_table @bo_extradata_sql, 'node_id', 'author, owner, cuid, guid, ruid, created_time, modified_time, last_run_time', null, @fixedOptNodeFilter
	
	declare @schedule_sql varchar(max) = '(select node_id, owner as schedule_owner, convert(varchar(24),creation_time,120) as creation_time, convert(varchar(24),nextruntime,120) as nextruntime, convert(varchar(24),expire,120) as expire, destination, format, parameters, destination_email_to from bik_sapbo_schedule)';
	exec sp_verticalize_node_attrs_one_metadata_table @schedule_sql, 'node_id', 'schedule_owner, destination, creation_time, nextruntime, expire, format, parameters, destination_email_to', null, @fixedOptNodeFilter
	
	declare @olap_sql varchar(max) = '(select node_id, type, provider_caption, cube_caption, cuid as olap_cuid, convert(varchar(24),created,120) as created, owner as olap_owner, convert(varchar(24),modified,120) as modified from bik_sapbo_olap_connection)';
	exec sp_verticalize_node_attrs_one_metadata_table @olap_sql, 'node_id', 'type, provider_caption, cube_caption, olap_cuid, created, olap_owner, modified', null, @fixedOptNodeFilter
	
	declare @obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as si_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''DataConnection'', ''Webi'', ''Flash'', ''CrystalReport'', ''Universe'', ''Excel'', ''FullClient'', ''Pdf'', ''Hyperlink'', ''Powerpoint'',
		''ReportFolder'', ''UniversesFolder'', ''ConnectionFolder'', ''ReportSchedule'', ''DSL.MetaDataFile'', ''CommonConnection'', ''CCIS.DataConnection''))'
	exec sp_verticalize_node_attrs_one_metadata_table @obj_id_sql, 'id', 'si_id', null, @fixedOptNodeFilter
	
	-- Oracle
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''Oracle''))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_oracle_extradata', 'node_id', 'definition_sql', null, @fixedOptNodeFilter
	
	-- MS SQL
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''MSSQL''))';
	declare @mssql_tree_id int = (select id from bik_tree where code = 'MSSQL')
	declare @mssql_sql varchar(max) = '(select bn.id, ms.definition_text from bik_mssql ms 
		inner join bik_node bn on bn.obj_id = ms.branch_names
		where bn.tree_id = ' +  cast(@mssql_tree_id as varchar(20)) + '
		and bn.is_deleted = 0
		and linked_node_id is null
		and ms.definition_text is not null)'
	exec sp_verticalize_node_attrs_one_metadata_table @mssql_sql, 'id', 'definition_text', null, @fixedOptNodeFilter
	-- Usuwanie starych ex propsów
	delete from bik_searchable_attr_val where attr_id in 
	(select att.id from bik_searchable_attr att 
	left join (select distinct name from bik_mssql_extended_properties prop) x on att.name = '$exprop_' + x.name
	where att.name like '$exprop_%' and x.name is null)

	delete from bik_searchable_attr from bik_searchable_attr att 
	left join (select distinct name from bik_mssql_extended_properties prop) x on att.name = '$exprop_' + x.name
	where att.name like '$exprop_%' and x.name is null
	--
	exec sp_verticalize_node_attrs_ex_props @fixedOptNodeFilter
	  
	-- DQC
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''DQC''))';
	declare @tree_id int = (select id from bik_tree where code = 'DQC')
	declare @inact_nk_id int = (select id from bik_node_kind where code = 'DQCTestInactive'),
    @succ_nk_id int = (select id from bik_node_kind where code = 'DQCTestSuccess'),
    @fail_nk_id int = (select id from bik_node_kind where code = 'DQCTestFailed')

	declare @dqc_src_sql varchar(max) = '(select cast(dt.long_name as varchar(max)) as long_name, cast(dt.sampling_method as varchar(max)) as sampling_method, cast(dt.verified_attributes as varchar(max)) as verified_attributes,cast(dt.logging_details as varchar(max)) as logging_details, cast(dt.benchmark_definition as varchar(max)) as benchmark_definition, cast(dt.results_object as varchar(max)) as results_object, cast(dt.additional_information as varchar(max)) as additional_information, n.id as node_id
		from bik_dqc_test dt inner join bik_node n on dt.__obj_id = n.obj_id and n.node_kind_id in (' +
		cast(@inact_nk_id as varchar(20)) + ',' + cast(@succ_nk_id as varchar(20)) + ',' + cast(@fail_nk_id as varchar(20)) + ') and tree_id = ' + cast(@tree_id as varchar(20)) + '
		where n.linked_node_id is null and n.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @dqc_src_sql, 'node_id', 'long_name, sampling_method, verified_attributes,logging_details, benchmark_definition, results_object, additional_information', null, @fixedOptNodeFilter
	
	-- AD
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''UserRoles''))';
	declare @ad_src_sql varchar(max) = '(select bu.email, coalesce(bu.phone_num, ad.telephone_number) as phone_num,
		ad.physical_delivery_office_name,ad.postal_code, ad.manager, 
		ad.description, ad.department, ad.title, ad.mobile, 
		ad.display_name, bu.node_id
		from bik_user bu
		join bik_node bn on bn.id = bu.node_id and bn.is_deleted = 0
		left join bik_system_user bsu on bsu.user_id = bu.id 
		left join bik_active_directory ad 
		on (bu.login_name_for_ad = ad.s_a_m_account_name or bsu.login_name = ad.s_a_m_account_name))'
	exec sp_verticalize_node_attrs_one_metadata_table @ad_src_sql, 'node_id', 'email, phone_num, physical_delivery_office_name,postal_code,manager, description, department, title, mobile, display_name', null, @fixedOptNodeFilter
	
	-- SAP BW query extradata
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code in (''BWProviders'', ''BWReports'')))';
	declare @bwreports_id int = (select id from bik_tree where code = 'BWReports')
	declare @query_nk_id int = (select id from bik_node_kind where code = 'BWBEx')
	declare @sapbw_query_src_sql varchar(max) = '(select q.update_time, q.owner as sapbw_owner, q.last_edit, bn.id as node_id
		from bik_sapbw_query q inner join bik_node bn on q.obj_name = bn.obj_id and bn.node_kind_id = ' +
		cast(@query_nk_id as varchar(20)) + ' and bn.tree_id = ' + cast(@bwreports_id as varchar(20)) + '
		where bn.linked_node_id is null and bn.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @sapbw_query_src_sql, 'node_id', 'update_time, sapbw_owner, last_edit', null, @fixedOptNodeFilter
	
	-- SAP BW providers, query and areas - technical id
	declare @bw_obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWBEx'', ''BWArea'', ''BWMPRO'', ''BWCUBE'', ''BWISET'', ''BWODSO''))'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_obj_id_sql, 'id', 'technical_id', null, @fixedOptNodeFilter
	
	-- SAP BW objects - technical id
	declare @bw_objs_id_sql varchar(max) = '(select bn.id, obj.obj_name as obj_technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		inner join bik_sapbw_object obj on obj.provider + ''|'' + obj.obj_name = bn.obj_id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWUni'',''BWKyf'', ''BWTim'', ''BWDpa'', ''BWCha'')
		and obj.provider_parent is null)'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_objs_id_sql, 'id', 'obj_technical_id', null, @fixedOptNodeFilter
	
end;
go

-- usuwamy niepotrzebne
delete from bik_searchable_attr
--select *
from bik_searchable_attr attr
where not exists (select attr_id from bik_searchable_attr_val where attr_id = attr.id)


if not exists(select * from bik_translation where kind = 'adef' and code = 'sql_text')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('sql_text', 'Treść zapytania raportowego', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('sql_text', 'Report sql text', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'filtr_text')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('filtr_text', 'Treść filtru raportowego', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('filtr_text', 'Report filter text', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'text_of_select')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('text_of_select', 'Treść select obiektu', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('text_of_select', 'Text of select', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'text_of_where')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('text_of_where', 'Treść where obiektu', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('text_of_where', 'Text of where', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'sql_of_derived_table')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('sql_of_derived_table', 'Zapytanie tabeli pochodnej', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('sql_of_derived_table', 'SQL of derived table', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'author')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('author', 'Autor', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('author', 'Author', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'owner')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('owner', 'Właściciel', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('owner', 'Owner', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'created_time')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('created_time', 'Opublikowano w repozytorium BO', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('created_time', 'Created time', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'modified_time')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('modified_time', 'Ostatnia modyfikacja w repozytorium BO', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('modified_time', 'Modified time', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'last_run_time')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('last_run_time', 'Data ostatniego uruchomienia', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('last_run_time', 'Last runtime', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'schedule_owner')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('schedule_owner', 'Właściciel harmonogramu', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('schedule_owner', 'Schedule owner', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'destination')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('destination', 'Miejsce docelowe', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('destination', 'Destination', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'creation_time')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('creation_time', 'Godzina utworzenia', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('creation_time', 'Creation time', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'nextruntime' and lang = 'pl')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('Nextruntime', 'Godzina następnego uruchomienia raportu', 'pl', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'expire')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('expire', 'Godzina wygaśnięcia', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('expire', 'Expire time', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'format')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('format', 'Format harmonogramu', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('format', 'Format', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'parameters')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('parameters', 'Parametry harmonogramu', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('parameters', 'Parameters', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'destination_email_to')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('destination_email_to', 'Adresaci', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('destination_email_to', 'Recipients', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'type' and lang = 'pl')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('type', 'Typ', 'pl', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'provider_caption')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('provider_caption', 'Nazwa dostawcy', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('provider_caption', 'Provider caption', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'cube_caption')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('cube_caption', 'Kostka', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('cube_caption', 'Cube caption', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'olap_cuid')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('olap_cuid', 'Olap cuid', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('olap_cuid', 'Olap cuid', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'created')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('created', 'Utworzono', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('created', 'Created by', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'olap_owner')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('olap_owner', 'Właściciel w repozytorium BO', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('olap_owner', 'Olap owner', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'modified')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('modified', 'Ostatnia modyfikacja', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('modified', 'Modified', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'si_id')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('si_id', 'SI ID', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('si_id', 'SI ID', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'definition_sql')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('definition_sql', 'Oracle definicja sql', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('definition_sql', 'Oracle definition sql', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'definition_text')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('definition_text', 'MSSQL definicja sql', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('definition_text', 'MSSQL definition sql', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'database_engine')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('database_engine', 'Database engine', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('database_engine', 'Database engine', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'database_source')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('database_source', 'Database source', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('database_source', 'Database source', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'connetion_networklayer_name')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('connetion_networklayer_name', 'Network layer', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('connetion_networklayer_name', 'Network layer', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'user_name')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('user_name', 'Użytkownik', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('user_name', 'User name', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'server' and lang = 'pl')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('server', 'Serwer', 'pl', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'cuid')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('cuid', 'Cuid', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('cuid', 'Cuid', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'guid')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('guid', 'Guid', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('guid', 'Guid', 'en', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'ruid' and lang = 'pl')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('ruid', 'Ruid', 'pl', 'adef')
end;

if not exists(select * from bik_translation where kind = 'adef' and code = 'node_id')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('node_id', 'Identyfikator węzła', 'pl', 'adef')
	insert into bik_translation(code, txt, lang, kind)
	values('node_id', 'Node id', 'en', 'adef')
end;

-- poprzednia wersja w pliku alter: db for v1.6.z3.7 tf
-- fix na prefixy: '$exprop_'
if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_verticalize_node_attrs_ex_props') and type in (N'P', N'PC'))
drop procedure dbo.sp_verticalize_node_attrs_ex_props
go

create procedure dbo.sp_verticalize_node_attrs_ex_props(@optNodeFilter varchar(max))
as
begin
  set nocount on

  declare prop_names_cur cursor for select distinct name from bik_mssql_extended_properties

  declare @name varchar(512)
  
  open prop_names_cur
  
  fetch next from prop_names_cur into @name
  
  while @@fetch_status = 0 begin
    declare @prop_source_sql varchar(max) = '(select node_id, value from bik_mssql_extended_properties where name = '''+ cast(@name as varchar(20)) + ''')'
    
    set @name = '$exprop_' + @name;

    exec sp_verticalize_node_attrs_one_table @prop_source_sql, 'node_id', 'value', @name, @optNodeFilter    
  
    fetch next from prop_names_cur into @name
  end
  
  close prop_names_cur
  
  deallocate prop_names_cur
end
go

-- poprzednia wersja w "alter db for v1.1.7.1 tf.sql"
-- nie dodajemy usunietych i nieuzywanych atrybutow
if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_verticalize_node_attrs_add_attrs') and type in (N'P', N'PC'))
drop procedure dbo.sp_verticalize_node_attrs_add_attrs
go

create procedure dbo.sp_verticalize_node_attrs_add_attrs(@optNodeFilter varchar(max))
as
begin
  set nocount on

  declare @diags_level int = 0 -- 0 oznacza brak logowania

  --declare @attr_names table (attr_id int not null unique, name varchar(255) not null unique, is_deleted int not null)
  
  --declare @attr_names_sql varchar(max) = 'select ad.id, ad.name, ad.is_deleted from bik_attr_def ad where is_built_in=0 /*where ad.is_deleted = 0*/'
  
  /*
  if (@optNodeFilter is not null) begin
    set @attr_names_sql = @attr_names_sql + ' and ad.id in (select a.attr_def_id from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id
    inner join bik_node n on al.node_id = n.id where ' + @optNodeFilter + ')'
  end
  */
  
  --insert into @attr_names (attr_id, name, is_deleted)
  --exec(@attr_names_sql)
  
  --select * from @attr_names
  
  --declare attr_names_cur cursor for select attr_id, name, is_deleted from @attr_names
  declare attr_names_cur cursor for select ad.id, ad.name, ad.is_deleted from bik_attr_def ad where is_built_in = 0 and is_deleted = 0 and exists (select 1 from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id where a.attr_def_id = ad.id and al.is_deleted = 0 and a.is_deleted = 0)
  
  declare @attr_id int, @name varchar(255), @is_deleted int
  
  open attr_names_cur
  
  fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  
  --return
  
  while @@fetch_status = 0 begin
    declare @attr_source_sql varchar(max) = '(select node_id, value from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id 
    where ' + case when @is_deleted = 0 then '' else '1 = 0 and ' end + 'al.is_deleted = 0 and a.is_deleted = 0 and a.attr_def_id = ' + cast(@attr_id as varchar(20)) + ')'
    
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': before verticalize one, @name=' + @name + ', source_sql=' + @attr_source_sql

	set @name = '$exprop_' + @name;
    exec sp_verticalize_node_attrs_one_table @attr_source_sql, 'node_id', 'value', @name, @optNodeFilter    
  
    fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  end
  
  close attr_names_cur
  
  deallocate attr_names_cur
end
go

exec sp_update_version '1.6.z4', '1.6.z5';
go