/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.ftp;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author tflorczak
 */
public class SFTP4JFoxyFTPClient implements IFoxyFTPClient {

    protected JSch jsch;
    protected Session session = null;
    protected ChannelSftp client;
    protected String host;
    protected int port;
    protected String user;
    protected String password;
    protected String currentDirectory;

    @Override
    public void connectAndLogin(String host, int port, String user, String password) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
        jsch = new JSch();
        try {
            session = jsch.getSession(user, host, port); // default port is 22
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(password);
            session.connect(20000); // 20 sec

            Channel channel = session.openChannel("sftp");
            channel.connect();
            client = (ChannelSftp) channel;
        } catch (Exception ex) {
            throw new FoxyFTPException("error in connectAndLogin", ex);
        }
    }

    @Override
    public void changeDirectory(String dir) {
        changeDirWithReconnectOnError(dir);
    }

    protected void innerChangeDir(String dir) {
        try {
            currentDirectory = dir;
            client.cd(dir);
        } catch (Exception ex) {
            throw new FoxyFTPException("error in changeDirectory", ex);
        }
    }

    @Override
    public String getCurrentDirectory() {
        try {
            return client.pwd();
        } catch (Exception ex) {
            throw new FoxyFTPException("error in changeDirectory", ex);
        }
    }

    @Override
    public List<IFoxyFTPFile> getFiles(String fileMask) {
        return innerGetFiles(fileMask);
    }

    @Override
    public List<IFoxyFTPFile> getFiles() {
        return innerGetFiles(currentDirectory);
    }

    @SuppressWarnings("unchecked")
    protected List<IFoxyFTPFile> innerGetFiles(String optFileMask) {
        try {
            Vector<LsEntry> files = null;
            int retryNum = 0;

            while (files == null) {
                try {
                    files = (Vector<LsEntry>) client.ls(optFileMask);
                } catch (Exception ex) {
                    retryNum++;
                    if (retryNum > 5) {
                        throw ex;
                    } else {
                        System.out.println("error in list, retrying #" + retryNum);
                        if (retryNum % 2 == 0) {
                            changeDirWithReconnectOnError(currentDirectory);
                        }
                    }
                }
            }
            List<IFoxyFTPFile> res = new ArrayList<IFoxyFTPFile>(files.size());
            for (LsEntry file : files) {
                res.add(new SFTP4JFoxyFTPFile(file));
            }
            return res;
        } catch (Exception ex) {
            throw new FoxyFTPException(ex);
        }
    }

    @Override
    public void disconnect() {
        if (client != null && client.isConnected()) {
            try {
                client.exit();
                session.disconnect();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public void download(String remoteFileName, String localFileName) {
        try {
            int retryNum = 0;
            boolean success = false;

            while (!success) {
                try {
                    File newFile = new File(localFileName);
                    client.get(remoteFileName, localFileName);

                    SftpATTRS remoteStat = client.stat(remoteFileName);
                    long remoteSize = remoteStat.getSize();
                    long localSize = newFile.length();

                    if (remoteSize != localSize) {
                        System.out.println("FTP SIZE ERROR! Remote file: " + remoteFileName + ", remote size: "
                                + remoteSize + ", local file: " + localFileName + ", local size: " + localSize + ". Local file not REMOVED!");
                    }
                    success = true;

                } catch (Exception ex) {
                    retryNum++;
                    if (retryNum > 5) {
                        throw ex;
                    } else {
                        System.out.println("error in download, retrying #" + retryNum);
                        if (retryNum % 2 == 0) {
                            changeDirWithReconnectOnError(currentDirectory);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw new FoxyFTPException(ex);
        }
    }

    protected void changeDirWithReconnectOnError(String currentDirectory) {
        try {
            innerChangeDir(currentDirectory);
        } catch (Exception ex) {
            System.out.println("error in innerChangeDir: dir=" + currentDirectory + ", will try to reconnect");
            reconnectAndResetCurrentDir();
        }
    }

    protected void reconnectAndResetCurrentDir() {
        String oldCurDir = currentDirectory;
        try {
            disconnect();
        } catch (Exception ex) {
            // no-op: silent
        }

        connectAndLogin(host, port, user, password);
        innerChangeDir(oldCurDir);
    }
}
