/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.TreeOfTrees;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author pgajda
 */
public class TreesDialog extends BaseActionOrCancelDialog {

    protected TreeOfTrees treeOfTrees;
    protected Set<Integer> initiallyCheckedTreeIDs;

    public TreesDialog() {
    }

    public TreesDialog(Set<Integer> initiallyCheckedIDs) {
        this.initiallyCheckedTreeIDs = initiallyCheckedIDs;
    }

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzDrzewaUzytkownika.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.wybierz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        VerticalPanel rightVp = new VerticalPanel();

        main.setWidth("500px");
        rightVp.setStyleName("EntityDetailsPane selectRoleInDialogPane");
        rightVp.setWidth("100%");

        treeOfTrees = new TreeOfTrees(initiallyCheckedTreeIDs);
        Widget treeTreesWidget = treeOfTrees.buildWidgets();
        treeTreesWidget.setSize("100%", "100%");

        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(treeTreesWidget,
                BIKClientSingletons.ORIGINAL_SELECT_ROLE_IN_DIALOG_SCROLL_HEIGHT);

        rightVp.add(treeTreesWidget);
        main.add(rightVp);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public Collection<Integer> getCheckedTreeIds() {
        return treeOfTrees.getCheckedTreeIds();
    }

    @Override
    protected void doAction() {
    }

    public void showDialog() {
        dialog.center();
    }
}
