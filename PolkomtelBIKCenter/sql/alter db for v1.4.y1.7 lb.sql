exec sp_check_version '1.4.y1.7';
go

---------------------------------------------------------
-- Nowy wezel z logami dla LISY
---------------------------------------------------------

exec sp_add_menu_node 'admin:lisa', 'Logi', '#admin:lisa:logs'
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
declare @menuNodeId int = (select id from bik_node where tree_id = @tree_id and obj_id = '#admin:lisa:logs')        
exec sp_node_init_branch_id @tree_id, @menuNodeId
go

exec sp_update_version '1.4.y1.7', '1.4.y1.8';
go



