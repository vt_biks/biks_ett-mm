/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.treeandlist;

import java.util.Collection;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pmielanczuk
 */
public interface ILazyRowLoaderMultiExpand<IDT> {

    public void loadNodesAndAncestorsAndExpand(final Collection<IDT> nodeIds, final IDT optNodeIdToSelect, final IParametrizedContinuation<IDT> optCont);
}
