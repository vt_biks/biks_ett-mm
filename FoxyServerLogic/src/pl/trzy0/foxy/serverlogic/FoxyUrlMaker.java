/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.IRequestContext;
import commonlib.UrlMaker;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public class FoxyUrlMaker extends UrlMaker {

    private static final Set<String> DEF_PARAM_NAMES = BaseUtils.splitUniqueBySep("_ver,lang", ",");
    private static final Set<String> PARAMS_ORDER = BaseUtils.splitUniqueBySep("lang,_ver,page", ",");
    //protected boolean forRemoteURLs;
    protected String currentPageName;
    protected Map<String, Object> defaultParamsVals;

    public FoxyUrlMaker(IRequestContext reqCtx,
            Map<String, Object> defaultParamsVals //, //boolean forRemoteURLs,
            //Map<String, Object> defaults
            ) {
        super(reqCtx);

        //this.forRemoteURLs = forRemoteURLs;
        this.currentPageName = (String) getValOfDefaultParam("page");
        this.defaultParamsVals = defaultParamsVals;
    }

    @Override
    protected Iterable<String> getParamsOrder(Map<String, Object> newParams, Map<String, Object> fixing) {
        return PARAMS_ORDER;
    }

    @Override
    protected Iterable<String> getDefaultParamNames(Map<String, Object> newParams,
            Map<String, Object> fixing) {
        return DEF_PARAM_NAMES;
    }

//    @Override
//    protected String postProcessUrlAsString(String url) {
//        return reqCtx.encodeUrl(super.postProcessUrlAsString(url));
//    }

    protected boolean useRemoteForResources() {
        return false;
    }

//    public String makeResourceUrl(String resource) {
//        //wz: tymczasowo tak zrobiłem, ww jak wróci to mi wytłumaczy o co tu chodzi i jak powinno być...
//        String result = reqCtx.getResourceUrlBase(true) + (resource.startsWith("/") ? "" : "/") + resource;
//        System.out.println("makeResourceUrl() - result=" + result);
//        return result;
//    }
    public String makeResourceUrl(String resource, boolean forceForRemote) {
        String result = reqCtx.getResourceUrlBase(
                forceForRemote || useRemoteForResources())
                + (resource.startsWith("/") ? "" : "/") + resource;
        //System.out.println("makeResourceUrl() - result=" + result);
        return result;
    }

//    @Override
//    protected Iterable<String> getDefaultParamNames(Map<String, Object> newParams, Map<String, Object> fixing) {
//        return defParamNames;
//    }
//
//    @Override
//    protected Iterable<String> getParamsOrder(Map<String, Object> newParams, Map<String, Object> fixing) {
//        return paramsOrder;
//    }
    protected Map<String, Object> preparePageFixing(String pageName) {
        Map<String, Object> fixing = new HashMap<String, Object>();
        fixing.put("page", pageName);
        return fixing;
    }

    public String makeUrl(String pageName, String paramName, Object val) {
        return makeUrl(pageName, Collections.singletonMap(paramName, val));
    }

    public String makeUrl(boolean forRemoteUrl, String pageName, Map<String, Object> newParams) {
        return innerMakeUrl(forRemoteUrl, newParams, preparePageFixing(pageName));
    }

    public String makeUrl(String pageName, Map<String, Object> newParams) {
        return makeUrl(false, pageName, newParams);
    }

    public String makeRemoteUrl(String pageName, Map<String, Object> newParams) {
        return makeUrl(true, pageName, newParams);
    }

    @Override
    protected Map<String, Object> getDefaultVals(Map<String, Object> newParams, Map<String, Object> fixing) {
        return this.defaultParamsVals;
    }
}
