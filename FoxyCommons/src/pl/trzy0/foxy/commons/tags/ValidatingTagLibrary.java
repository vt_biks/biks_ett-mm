/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons.tags;

import java.util.Collection;
import java.util.Set;

/**
 *
 * @author wezyr
 */
public class ValidatingTagLibrary implements ITagLibrary<Object> {

    private Set<String> allowedTags;

    public ValidatingTagLibrary(Set<String> allowedTags) {
        this.allowedTags = allowedTags;
    }
    
    public Object createTag(Object parentTag, TagData tagData) {
        String tagName = tagData.getName();
        if (!tagData.isTextTag() && allowedTags != null && !allowedTags.contains(tagName)) {
            throw new FoxyTagsRuntimeException("tag " + tagName + " is not allowed");
        }
        return null;
    }

    public void setSubTags(Object tag, Collection<Object> subTags) {
        // no-op
    }
}
