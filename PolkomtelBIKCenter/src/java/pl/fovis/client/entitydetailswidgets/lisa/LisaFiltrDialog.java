/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.dialogs.lisa.LisaDataType;
import static pl.fovis.client.dialogs.lisa.LisaDataType.DA;
import static pl.fovis.client.dialogs.lisa.LisaDataType.I;
import static pl.fovis.client.dialogs.lisa.LisaDataType.I1;
import static pl.fovis.client.dialogs.lisa.LisaDataType.I2;
import static pl.fovis.client.dialogs.lisa.LisaDataType.I8;
import pl.fovis.client.entitydetailswidgets.lisa.LisaFiltrColumnValueSingle.FILTER_KIND;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class LisaFiltrDialog extends BaseActionOrCancelDialog {

    private IDataGrid displayer;
    private FlexTable table;
    private WidgetSnapshot[][] tableBackup = null;

    LisaFiltrDialog(IDataGrid displayer) {
        this.displayer = displayer;
    }

    @Override
    protected String getDialogCaption() {
        return I18n.wprowadzFrazeDoFiltru.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.filtruj.get();
    }

    @Override
    protected String getCancelButtonCaption() {
        return I18n.anuluj.get();
    }

    private void addRowToFiltr() {
        final int nRow = table.getRowCount();
        table.insertRow(nRow);
        final ListBox columnNameLbox = createColumnNameLbox();
        table.setWidget(nRow, 0, columnNameLbox);
        final ListBox typeBox = new ListBox();
        columnNameLbox.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                typeBox.clear();
                int index = ((ListBox) event.getSource()).getSelectedIndex();
                LisaDictColumnBean columnBean = displayer.getColumnInfo().get(index);
                switch (LisaDataType.getValueFor(columnBean.dataType)) {
                    case DA:
                    case I1:
                    case I2:
                    case I8:
                    case I:
                        typeBox.addItem(I18n.fromTo.get());
                        typeBox.addItem(I18n.equal.get());
                        typeBox.addItem(I18n.greaterThan.get());
                        typeBox.addItem(I18n.smallerThan.get());
                        break;
                    default:
                        typeBox.addItem(I18n.like.get());
                        break;
                }
                typeBox.setSelectedIndex(0);
                DomEvent.fireNativeEvent(Document.get().createChangeEvent(), typeBox);
            }
        });
        columnNameLbox.setSelectedIndex(0);
        DomEvent.fireNativeEvent(Document.get().createChangeEvent(), columnNameLbox);
        table.setWidget(nRow, 1, typeBox);
        typeBox.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                int row = findRow(event.getSource());
                ListBox lb = (ListBox) event.getSource();
                int index = lb.getSelectedIndex();
                String choice = lb.getItemText(index);
                int colIndex = columnNameLbox.getSelectedIndex();
                LisaDictColumnBean columnBean = displayer.getColumnInfo().get(colIndex);

                table.removeCells(row, 2, table.getCellCount(row) - 3);

                LisaDataType cellType = LisaDataType.getValueFor(columnBean.dataType);
                addCell(cellType, row, 2);
                if (choice.equals(I18n.fromTo.get())) {
                    addCell(cellType, row, 3);
                }
            }

            private int findRow(Object source) {
                int nRow = table.getRowCount();
                for (int i = 0; i < nRow; i++) {
                    int nCol = table.getCellCount(i);
                    for (int j = 0; j < nCol; j++) {
                        if (BaseUtils.safeEquals(source, table.getWidget(i, j))) {
                            return i;
                        }
                    }
                }

                return -1;
            }
        });
        DomEvent.fireNativeEvent(Document.get().createChangeEvent(), typeBox);
        Image removeBtn = new Image("images/delete.png");
        removeBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                int rowIndex = table.getCellForEvent(event).getRowIndex();
                table.removeRow(rowIndex);
            }
        });
        table.setWidget(nRow, table.getCellCount(nRow), removeBtn);
    }

    private void addCell(LisaDataType cellType, int row, int col) {
        table.insertCell(row, col);
        switch (cellType) {
            case DA:
                DateTimeFormat dateFormat = DateTimeFormat.getFormat("yyyy-MM-dd");
                DateBox dateBox = new DateBox();
                dateBox.setFormat(new DateBox.DefaultFormat(dateFormat));
                dateBox.getDatePicker().setYearArrowsVisible(true);
                dateBox.getDatePicker().setYearAndMonthDropdownVisible(true);
                table.setWidget(row, col, dateBox);
                break;
            default:
                TextBox tbox = new TextBox();
                table.setWidget(row, col, tbox);
        }
    }

    @Override
    protected void buildMainWidgets() {
        PushButton addBtn = new PushButton(I18n.dodaj.get());
        NewLookUtils.makeCustomPushButton(addBtn);
        addBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                addRowToFiltr();
            }
        });

        PushButton clearBtn = new PushButton(I18n.wyczyscFiltr.get());
        NewLookUtils.makeCustomPushButton(clearBtn);
        clearBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                table.removeAllRows();
            }
        });

        HorizontalPanel buttonPan = new HorizontalPanel();
        buttonPan.add(addBtn);
        buttonPan.add(new HTML("&nbsp;"));
        buttonPan.add(clearBtn);

        table = new FlexTable();
        table.setCellPadding(2);
        main.add(buttonPan);
        addRowToFiltr();
        main.add(table);
    }

    @Override
    protected void doAction() {
        displayer.performLisaFiltr(getInfoToFiltr());
    }

    public void clearFilter() {
        if (table != null) {
            table.removeAllRows();
        }
    }

    @Override
    protected boolean doActionBeforeClose() {
        boolean ret = validateFiltr();
        if (!ret) {
            Window.alert(I18n.proszeUzupelnicWszyskieDane.get());
        }
        return ret;
    }

    protected void doCancel() {
        restoreFilterSnapshot();
    }

    private List<LisaFiltrColumnValueBase> getInfoToFiltr() {
        List<LisaFiltrColumnValueBase> av = new ArrayList<LisaFiltrColumnValueBase>();
        for (int i = 0; i < table.getRowCount(); i++) {
            int index = ((ListBox) table.getWidget(i, 0)).getSelectedIndex();
            LisaDictColumnBean columnBean = displayer.getColumnInfo().get(index);
            index = ((ListBox) table.getWidget(i, 1)).getSelectedIndex();
            String choice = ((ListBox) table.getWidget(i, 1)).getItemText(index);

            if (choice.equals(I18n.fromTo.get())) {
                av.add(new LisaFiltrColumnValueBetween(columnBean, table.getWidget(i, 2), table.getWidget(i, 3)));
            } else if (choice.equals(I18n.greaterThan.get())) {
                av.add(new LisaFiltrColumnValueSingle(columnBean, table.getWidget(i, 2), FILTER_KIND.GREATER));
            } else if (choice.equals(I18n.smallerThan.get())) {
                av.add(new LisaFiltrColumnValueSingle(columnBean, table.getWidget(i, 2), FILTER_KIND.SMALLER));
            } else if (choice.equals(I18n.equal.get())) {
                av.add(new LisaFiltrColumnValueSingle(columnBean, table.getWidget(i, 2), FILTER_KIND.EQUAL));
            } else if (choice.equals(I18n.like.get())) {
                av.add(new LisaFiltrColumnValueSingle(columnBean, table.getWidget(i, 2), FILTER_KIND.LIKE));
            }
        }
        return av;
    }

    private boolean validateFiltr() {
        List<LisaFiltrColumnValueBase> av = new ArrayList<LisaFiltrColumnValueBase>();
        LisaFiltrColumnValueBase lfcv = new LisaFiltrColumnValueBase();

        for (int i = 0; i < table.getRowCount(); i++) {
            int index = ((ListBox) table.getWidget(i, 0)).getSelectedIndex();
            LisaDictColumnBean columnBean = displayer.getColumnInfo().get(index);
            index = ((ListBox) table.getWidget(i, 1)).getSelectedIndex();
            String choice = ((ListBox) table.getWidget(i, 1)).getItemText(index);

            if (lfcv.parseFromWidget(columnBean, table.getWidget(i, 2)).getValue() == null) {
                return false;
            }

            if (choice.equals(I18n.fromTo.get())) {
                if (lfcv.parseFromWidget(columnBean, table.getWidget(i, 3)).getValue() == null) {
                    return false;
                }
            }
        }
        return true;
    }

    private void makeFilterSnapshot() {
        int rowNum = table.getRowCount();
        tableBackup = new WidgetSnapshot[rowNum][5];
        for (int row = 0; row < rowNum; row++) {
            for (int col = 0; col < table.getCellCount(row); col++) {
                Widget wid = table.getWidget(row, col);
                tableBackup[row][col] = new WidgetSnapshot(wid);
            }
        }
    }

    private void restoreFilterSnapshot() {
        table.clear();
        for (int row = 0; row < tableBackup.length; row++) {
            for (int col = 0; col < 5; col++) {
                WidgetSnapshot widSnap = tableBackup[row][col];
                if (widSnap != null) {
                    table.setWidget(row, col, widSnap.getRestoredWidget());
                }
            }
        }

    }

    private ListBox createColumnNameLbox() {
        ListBox lBox = new ListBox();
        for (LisaDictColumnBean columnBean : displayer.getColumnInfo()) {
            lBox.addItem(columnBean.nameInDb);
        }
        return lBox;
    }

    public void showDialog() {
        makeFilterSnapshot();
        dialog.show();
    }

    class WidgetSnapshot {

        private Widget widget;
        private String value;
        private Integer index;

        public WidgetSnapshot(Widget wid) {
            widget = wid;
            getWidgetValue();
        }

        public Widget getRestoredWidget() {
            restoreWidget();
            return widget;
        }

        private void getWidgetValue() {
            if (widget instanceof ListBox) {
                index = ((ListBox) widget).getSelectedIndex();
                String className = widget.getElement().getClassName();
                String innerHTML = widget.getElement().getInnerHTML();
                value = "<select class=\"" + className + "\">" + innerHTML + "</select>";
            } else if (widget instanceof DateBox) {
                value = ((DateBox) widget).getTextBox().getText();
            } else if (widget instanceof TextBox) {
                value = ((TextBox) widget).getText();
            }
        }

        private void restoreWidget() {
            if (widget instanceof ListBox) {
                widget.getElement().setInnerHTML(value);
                ((ListBox) widget).setItemSelected(index, true);
            } else if (widget instanceof DateBox) {
                ((DateBox) widget).getTextBox().setText(value);
            } else if (widget instanceof TextBox) {
                ((TextBox) widget).setText(value);
            }
        }
    }

}
