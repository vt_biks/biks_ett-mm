﻿exec sp_check_version '1.0.98.1';
go
---------------------------------
---------------------------------
alter table bik_node
drop column old_name;
go

alter table bik_node
add descr_plain varchar(max);
go

update bik_app_prop
set val='1'
where name='mustRunStripHtmlTagsFromRichTextFields'

delete from bik_blog where node_id is null -- usunięcie złych / starych / niepodłączonych blogów
--------------------------------
--------------------------------
exec sp_update_version '1.0.98.1', '1.0.98.2';
go