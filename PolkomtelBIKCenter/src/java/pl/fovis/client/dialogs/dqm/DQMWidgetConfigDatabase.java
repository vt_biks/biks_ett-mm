/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.TextAreaDialog;
import static pl.fovis.client.dialogs.dqm.AddOrEditDQMTestDialogBase.addLabel;
import static pl.fovis.client.dialogs.dqm.AddOrEditDQMTestDialogBase.createDeleteButton;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.dqm.DataQualityTestBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public abstract class DQMWidgetConfigDatabase implements IDQMWidgetConfig {

    protected VerticalPanel main;
    protected Label lblFunctionName;
    protected Label lblErrorFunctionName;
    protected Label lblDatabaseName;
    protected PushButton deleteFunctionButton;
    protected boolean runAsFunction;
    protected Integer nodeId;
    protected Integer procedureNodeId;
    protected Integer errorProcedureNodeId;
    protected Integer databaseNodeId;
    protected String sqlTestText;
    protected String sqlTestErrorText;

    public DQMWidgetConfigDatabase() {
        runAsFunction = BIKClientSingletons.isDQMRunTestAsFunction();
    }

    @Override
    public void cleanWidgets() {
        procedureNodeId = null;
        errorProcedureNodeId = null;
        databaseNodeId = null;
        sqlTestText = null;
        sqlTestErrorText = null;
        lblFunctionName.setText(I18n.brak.get());
        lblErrorFunctionName.setText(I18n.brak.get());
        lblDatabaseName.setText(I18n.brak.get());
        updateDeleteButtonVisible();
    }

    @Override
    public Widget getWidget(Integer nodeId) {
        this.nodeId = nodeId;
        if (main == null) {
            buildWidget();
        }
        return main;
    }

    protected void buildWidget() {
        main = new VerticalPanel();
        lblFunctionName = new Label(I18n.brak.get());
        lblDatabaseName = new Label(I18n.brak.get());
        lblErrorFunctionName = new Label(I18n.brak.get());
        deleteFunctionButton = createDeleteButton(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                errorProcedureNodeId = null;
                lblErrorFunctionName.setText(I18n.brak.get());
                updateDeleteButtonVisible();
            }
        }, "images/delete.png");
        if (runAsFunction) {
            HorizontalPanel functionPanel = new HorizontalPanel();
            functionPanel.add(addLabel(I18n.funkcjaTestujaca.get()));
            VerticalPanel functionVP = new VerticalPanel();
            functionVP.add(lblFunctionName);
            PushButton btnFunction = new PushButton(I18n.wybierz.get());
            NewLookUtils.makeCustomPushButton(btnFunction);
            btnFunction.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    new TreeSelectorForDQMFunction().buildAndShowDialog(nodeId, getTreeCode(), new IParametrizedContinuation<TreeNodeBean>() {

                        @Override
                        public void doIt(TreeNodeBean param) {
                            lblFunctionName.setText(param.name);
                            procedureNodeId = param.id;
                        }
                    });
                }
            });
            functionVP.add(btnFunction);
            functionPanel.add(functionVP);
            main.add(functionPanel);
            //
            HorizontalPanel functionPanelError = new HorizontalPanel();
            functionPanelError.add(addLabel(I18n.funkcjaZwracajacaBledy.get()));
            VerticalPanel functionErrorVP = new VerticalPanel();
            HorizontalPanel functionErrorHP = new HorizontalPanel();
            functionErrorHP.add(lblErrorFunctionName);
            functionErrorHP.add(deleteFunctionButton);
            functionErrorVP.add(functionErrorHP);
            PushButton btnErrorFunction = new PushButton(I18n.wybierz.get());
            NewLookUtils.makeCustomPushButton(btnErrorFunction);
            btnErrorFunction.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    new TreeSelectorForDQMFunction().buildAndShowDialog(nodeId, getTreeCode(), new IParametrizedContinuation<TreeNodeBean>() {

                        @Override
                        public void doIt(TreeNodeBean param) {
                            lblErrorFunctionName.setText(param.name);
                            errorProcedureNodeId = param.id;
                            updateDeleteButtonVisible();
                        }
                    });
                }
            });
            functionErrorVP.add(btnErrorFunction);
            functionPanelError.add(functionErrorVP);
            main.add(functionPanelError);
        } else {
            HorizontalPanel databasePanel = new HorizontalPanel();
            databasePanel.add(addLabel(I18n.bazaDanych.get()));
            databasePanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
            HorizontalPanel databaseHP = new HorizontalPanel();
            databaseHP.setSpacing(5);
            databaseHP.add(lblDatabaseName);
            PushButton selectDatabaseBtn = new PushButton(I18n.edytuj.get());
            NewLookUtils.makeCustomPushButton(selectDatabaseBtn);
            selectDatabaseBtn.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    new TreeSelectorForDQMDatabase().buildAndShowDialog(nodeId, getTreeCode(), new IParametrizedContinuation<TreeNodeBean>() {

                        @Override
                        public void doIt(TreeNodeBean param) {
                            lblDatabaseName.setText(param.name);
                            databaseNodeId = param.id;
                        }
                    });
                }
            });
            databaseHP.add(selectDatabaseBtn);
            databasePanel.add(databaseHP);
            main.add(databasePanel);

            HorizontalPanel sqlTextPanel = new HorizontalPanel();
            sqlTextPanel.add(addLabel(I18n.zapytanieSprawdzajaceCzyWystepujaBledy.get()));
            PushButton editTestBtn = new PushButton(I18n.edytuj.get());
            NewLookUtils.makeCustomPushButton(editTestBtn);
            editTestBtn.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    new TextAreaDialog().buildAndShowDialog(sqlTestText, new IParametrizedContinuation<String>() {

                        @Override
                        public void doIt(String param) {
                            sqlTestText = param;
                        }
                    });
                }
            });
            sqlTextPanel.add(editTestBtn);
            main.add(sqlTextPanel);

            HorizontalPanel sqlErrorTextPanel = new HorizontalPanel();
            sqlErrorTextPanel.add(addLabel(I18n.zapytanieZwracajaceDane.get()));
            PushButton editErrorTestBtn = new PushButton(I18n.edytuj.get());
            NewLookUtils.makeCustomPushButton(editErrorTestBtn);
            editErrorTestBtn.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    new TextAreaDialog().buildAndShowDialog(sqlTestErrorText, new IParametrizedContinuation<String>() {

                        @Override
                        public void doIt(String param) {
                            sqlTestErrorText = param;
                        }
                    });
                }
            });
            sqlErrorTextPanel.add(editErrorTestBtn);
            main.add(sqlErrorTextPanel);
        }
        updateDeleteButtonVisible();
    }

    @Override
    public boolean validateWidgetsBeforeSave() {
        if (runAsFunction && procedureNodeId == null) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.wybierzFunkcjeTestujaca.get(), I18n.blad.get(), null);
            return false;
        }
        if (!runAsFunction && databaseNodeId == null) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.wybierzBazeDanych.get(), I18n.blad.get(), null);
            return false;
        }
        if (!runAsFunction && BaseUtils.isStrEmptyOrWhiteSpace(sqlTestText)) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.zapytanieTestujaceNieMozeBycPuste.get(), I18n.blad.get(), null);
            return false;
        }
        return true;
    }

    @Override
    public void fillWidgets(DataQualityTestBean bean) {
        lblFunctionName.setText(bean.procedureName == null ? I18n.brak.get() : bean.procedureName);
        lblErrorFunctionName.setText(bean.errorProcedureName == null ? I18n.brak.get() : bean.errorProcedureName);
        lblDatabaseName.setText(bean.databaseName == null ? I18n.brak.get() : bean.databaseName);
        procedureNodeId = bean.procedureNodeId;
        errorProcedureNodeId = bean.errorProcedureNodeId;
        databaseNodeId = bean.databaseNodeId;
        sqlTestText = bean.sqlText;
        sqlTestErrorText = bean.errorSqlText;
    }

    @Override
    public void fillBean(DataQualityTestBean bean) {
        bean.procedureNodeId = procedureNodeId;
        bean.errorProcedureNodeId = errorProcedureNodeId;
        bean.databaseNodeId = databaseNodeId;
        bean.sqlText = sqlTestText;
        bean.errorSqlText = sqlTestErrorText;
    }

    protected void updateDeleteButtonVisible() {
        deleteFunctionButton.setVisible(errorProcedureNodeId != null);
    }

    public abstract String getTreeCode();

}
