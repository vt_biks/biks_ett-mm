/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.ui.IsWidget;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
public class ResizeSensitiveWidgetByHeight extends ResizeSensitiveWidgetHelper {

    public ResizeSensitiveWidgetByHeight(IsWidget iw) {
        super(iw);
    }

    @Override
    public void setFixedHeight(int height) {
        asWidget().setHeight(height + "px");
    }
}
