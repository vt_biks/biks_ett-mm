/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import pl.fovis.client.dialogs.UserDialog;
import pl.fovis.client.BIKGWTConstants;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author misia
 * 
 */
public class NodeActionAddUser extends NodeActionUpdatingBase {

    public String getCaption() {
        return I18n.dodajUzytkownika.get() /* I18N:  */;
    }

    public void execute() {
        new UserDialog().buildAndShowDialog(node, null,/* userKindList,*/ new IParametrizedContinuation<Pair<Integer, List<String>>/*Integer*/>() {
            public void doIt(Pair<Integer, List<String>> result) {
                refreshRedisplayDataAndSelectRecord(result.v1/*, false*/);
                refreshAndRedisplayDataByTreeCode(result.v2);
            }
        });
    }

    @Override
    public boolean isEnabledByDesign() {

        return isNodeNotLinkedAnyhowNorReadOnlyTreeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_ALL_USERS_FOLDER);
    }
}
