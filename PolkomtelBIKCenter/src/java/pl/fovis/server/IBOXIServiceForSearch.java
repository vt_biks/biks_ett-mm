/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.SearchFullResult.IndexCrawlStatus;
import pl.fovis.server.fts.ISimilarNodesWizard;
import simplelib.IContinuation;

/**
 *
 * @author wezyr
 */
public interface IBOXIServiceForSearch {

//ww: popłuczyny po lucynie
//    public List<NodeIndexingTaskData> getBikDocumentResults(int indexVer);
//
//    public NodeIndexingTaskData getBikDocumentResult(int nodeId);
    public void setIndexVerIntoBikNode(Set<Integer> nodeIds, int indexVer);

    public void setIndexVer(int indexVer);

    public int getIndexVer();

    public void finishWorkUnit(boolean success);

    public void addAfterCommitTaskForCurrentThreadX(IContinuation taskCont, String descr);

    public String getAppPropValueEx(String propName, boolean allowSensitive,
            String defVal);

    public Map<Integer, String> getNodeNamesByIds(Collection<Integer> nodeIds);

    public IndexCrawlStatus getFullTextCrawlCompleteStatus(boolean useLucene);

    public boolean isDBJobCanceled();

    public ISimilarNodesWizard getSearchWizard();

    public boolean hasFulltextCatalog();

//    public void addOrUpdateSpidHistoryForCurrentUser();
}
