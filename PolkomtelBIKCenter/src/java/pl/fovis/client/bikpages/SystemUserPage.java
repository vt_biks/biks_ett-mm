/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.bikwidgets.TreeOfTrees;
import pl.fovis.client.dialogs.AddADUserDialog;
import pl.fovis.client.dialogs.SystemUserDialog;
import pl.fovis.client.dialogs.TreeSelectorForRoleRestrictionsDialog;
import pl.fovis.client.entitydetailswidgets.GenericBeanGridWidget;
import pl.fovis.client.mltx.InviteUsersDialog;
import pl.fovis.common.BIKRightRoles;
import pl.fovis.common.CustomRightRoleBean;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.RightRoleBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeNodeBranchBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.OneRadioBoxDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author beata
 */
public class SystemUserPage extends BikPageMinimalBase implements IBikPageForAppAdmin {

    protected final Set<String> regularUserTreeCodes = BIKClientSingletons.treeSelectorToTreeCodes(BIKClientSingletons.getTreeSelectorForRegularUserCrr());
    protected VerticalPanel rightVp;
    protected VerticalPanel leftVp;
    protected VerticalPanel vp;
    protected HorizontalPanel mainHp;
    protected ListBox statusOfUserAccount;
    protected ListBox assigningCommunityStatus;
    protected VerticalPanel rightRolesVp;
    protected VerticalPanel treeOfTreesPanel; //pg: panel do filtracji po autorze
    protected TreeOfTrees treeOfTrees;
    protected Map<String, RightRoleBean> allRightRolesByCode = new LinkedHashMap<String, RightRoleBean>();
    protected Map<Integer, CustomRightRoleBean> allRightRolesByCodeCrr = new LinkedHashMap<Integer, CustomRightRoleBean>();
    protected TextBox filterBox;
    protected SystemUserBeanExtractor extractor;
    protected GenericBeanGridWidget<SystemUserBean> gridWidget;
    protected RightRoleBean selectedRightRole;
    protected String selectedRightRoleCode;
    protected Request filterRequest;
    protected String textOfFilter = null;
    protected Boolean isSystemUserAccountDisabled = null;
    protected Boolean hasCommunityUser = null;
    protected List<SystemUserBean> displayedSystemUsers = new ArrayList<SystemUserBean>();
//    protected boolean noNeedToRefetchBeforeShow = false;
    protected String filterRequestTicket = null;
    protected Integer optFilterOnCustomRightRoleId;
    protected ListBox permissionDomainLb;
    protected ListBox rightRolesLB;

    protected void editRegularUserTemplate() {
        BIKClientSingletons.getService().getRutEntries(new StandardAsyncCallback<Set<TreeNodeBranchBean>>() {

            @Override
            public void onSuccess(Set<TreeNodeBranchBean> result) {
                new TreeSelectorForRoleRestrictionsDialog().buildAndShowDialog(I18n.zwyklyUzytkownik.get(), regularUserTreeCodes,
                        result, new IParametrizedContinuation<Set<TreeNodeBranchBean>>() {

                            @Override
                            public void doIt(Set<TreeNodeBranchBean> param) {

                                Set<Pair<Integer, Integer>> vals = new HashSet<Pair<Integer, Integer>>();

                                for (TreeNodeBranchBean tnbb : param) {
                                    vals.add(new Pair<Integer, Integer>(tnbb.treeId, tnbb.nodeId));
                                }

                                BIKClientSingletons.getService().updateRutEntries(vals, new StandardAsyncCallback<Void>() {

                                    @Override
                                    public void onSuccess(Void result) {
                                        show();
                                        BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get());
                                    }
                                });
                            }
                        });
            }
        });
    }

    @Override
    protected Widget buildWidgets() {
        mainHp = new HorizontalPanel();
        mainHp.setWidth("100%");
        mainHp.setHeight("100%");
        leftVp = new VerticalPanel();
        leftVp.setWidth("100%");
        leftVp.setHeight("100%");
        leftVp.setStyleName("SystemUserPageColor");
        rightVp = new VerticalPanel();
        rightRolesVp = new VerticalPanel();
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(leftVp, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT /*- 18*/);
        vp = new VerticalPanel();
        if (!BIKClientSingletons.newRightsSysByAppProps()) {
            rightRolesVp.setStyleName("SystemUserPageLeftVp");
            rightRolesVp.add(createCaption(I18n.uprawnienia.get() /* I18N:  */ + ":"));
            addRightRolesToRightRolesVp();
            rightRolesVp.add(treeOfTreesPanel = createTreeOfTreesPanel());
            leftVp.add(rightRolesVp);
        }
        vp.setWidth("100%");
        rightVp.setWidth("100%");
        
        leftVp.add(createFilters());
        show(/*vp*/);
        if (!regularUserTreeCodes.isEmpty() && BIKClientSingletons.disableBuiltInRoles()) {
            FlowPanel fpEditRut = new FlowPanel();
            BikCustomButton btnEditRut = new BikCustomButton(I18n.edytujSzablonZwyklegoUzytkownika.get(), "Users-addUser", new IContinuation() {

                @Override
                public void doIt() {
                    editRegularUserTemplate();
                }
            });

            fpEditRut.add(btnEditRut);
            leftVp.add(fpEditRut);
            leftVp.setCellVerticalAlignment(fpEditRut, HasVerticalAlignment.ALIGN_BOTTOM);
        }

        FlowPanel addUserPanel = createAddUserPanel();
        leftVp.add(addUserPanel);
        leftVp.setCellVerticalAlignment(addUserPanel, HasVerticalAlignment.ALIGN_BOTTOM);

        rightVp.add(createScrollPanel(vp));
        ScrollPanel sc = createScrollPanelEx(leftVp, LEFT_PANE_WIDTH);
        mainHp.add(sc);
        mainHp.setCellWidth(sc, LEFT_PANE_WIDTH);
        mainHp.add(rightVp);
        return mainHp;
    }
//    public static final String LEFT_PANE_WIDTH = "243px";
    public static final String LEFT_PANE_WIDTH = "255px";

//    public String getCaptionDeprecated() {
//        return I18n.uzytkownicySystemu.get() /* I18N:  */;
//    }
    @Override
    public String getId() {
        return "admin:dict:sysUsers";
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        //no-op
    }

    protected Request callServiceToGetData(String ticket, boolean showUserInRoleFromGroup, AsyncCallback<List<SystemUserBean>> callback) {
        Set<Integer> checkedTreeIds = null;

        if ((selectedRightRole != null && selectedRightRole.code != null && selectedRightRole.code.equalsIgnoreCase(BIKGWTConstants.USER_KIND_CODE_AUTHOR))
                || (optFilterOnCustomRightRoleId != null
                && BIKClientSingletons.getMapOfAllRightRolesByCodeCrr().get(optFilterOnCustomRightRoleId) != null
                && BIKClientSingletons.getMapOfAllRightRolesByCodeCrr().get(optFilterOnCustomRightRoleId).code.equalsIgnoreCase(BIKGWTConstants.USER_KIND_CODE_AUTHOR))) {
            checkedTreeIds = new HashSet<Integer>(treeOfTrees.getCheckedTreeIds());
            if (checkedTreeIds.isEmpty()) {
                checkedTreeIds.add(-1);
            }
        }

        return BIKClientSingletons.getService().getAllBikSystemUserWithFilter(ticket,
                textOfFilter, isSystemUserAccountDisabled, hasCommunityUser, selectedRightRole, checkedTreeIds, optFilterOnCustomRightRoleId, showUserInRoleFromGroup, callback);
    }

    protected FlowPanel createAddUserPanel() {
        FlowPanel fp = new FlowPanel();
        fp.setStyleName("SystemUserPageAddUser");
        fp.add(createAddButton());
        return fp;
    }

    protected VerticalPanel createTreeOfTreesPanel() {
        VerticalPanel vpan = new VerticalPanel();
        vpan.setStyleName("SystemUserPageTreeOfTrees");
        vpan.setSize("210px", "100px");

        treeOfTrees = new TreeOfTrees() {

            @Override
            protected void selectionChanged() {
                show(/*vp*/);
            }

            @Override
            protected boolean isInitiallyChecked(MenuNodeBean mnb) {
                return true;
            }

            @Override
            protected Collection<TreeBean> getTrees() {

                if (!BIKClientSingletons.disableBuiltInRoles()) {
                    return super.getTrees();
                }

                return BIKClientSingletons.getAllTrees();
            }
        };

        Widget treeTreesWidget = treeOfTrees.buildWidgets();
        vpan.add(treeTreesWidget);

        vpan.setVisible(false);
        return vpan;
    }

    protected BikCustomButton createAddButton() {
        final boolean multiXMode = BIKClientSingletons.isMultiXMode();
        return new BikCustomButton(multiXMode ? I18n.zaprosUzytkownikow.get() : I18n.dodajUzytkownika.get(), "Users-addUser",
                new IContinuation() {
                    public void doIt() {
                        if (multiXMode) {
                            final InviteUsersDialog window = new InviteUsersDialog();
                            window.buildAndShowDialog(null, window.getContinuation());
                        } else {
                            addSystemUser();
                        }
                    }
                });
    }

    protected void addSystemUser() {
        List<String> accTypes = new ArrayList<String>();
        accTypes.add(I18n.kontoZAD.get());
        accTypes.add(I18n.kontoKlasyczne.get());
        new OneRadioBoxDialog().buildAndShowDialog(I18n.rodzajUzytkownika.get(), accTypes, new IParametrizedContinuation<String>() {

            @Override
            public void doIt(String param) {
                if (BaseUtils.safeEquals(param, I18n.kontoZAD.get())) {
                    addDomainAccount();
                } else if (BaseUtils.safeEquals(param, I18n.kontoKlasyczne.get())) {
                    addEnterpriseAccount();
                } else {
                    Window.alert("Wybierz typ użytkownika do dodania!");
                }
            }
        });
    }

    protected void addDomainAccount() {
        new AddADUserDialog().buildAndShowDialog(new IParametrizedContinuation<SystemUserBean>() {

            @Override
            public void doIt(SystemUserBean param) {
                BIKClientSingletons.getService().getOrCreateSystemUserBeanByLogin(param.loginName, param.ssoDomain, new StandardAsyncCallback<SystemUserBean>("Error in getOrCreateSystemUserBeanByLogin") {

                    @Override
                    public void onSuccess(SystemUserBean result) {
                        BIKClientSingletons.showInfo(I18n.dodanoUzytkownika.get());
                    }
                });
            }
        });
    }

    protected void addEnterpriseAccount() {
        BIKClientSingletons.getService().getAlreadyJoinedUserIds(new StandardAsyncCallback<Set<Integer>>() {
            @Override
            public void onSuccess(Set<Integer> result) {
                new SystemUserDialog().buildAndShowDialog(null, result, new IParametrizedContinuation<SystemUserBean>() {
                    @Override
                    public void doIt(SystemUserBean param) {
                        displayedSystemUsers.add(param);
//                        noNeedToRefetchBeforeShow = true;
//                        show(/*vp,*/true);
                        show();
                    }
                }, !regularUserTreeCodes.isEmpty());
            }
        });
    }

    protected void createRightRolesItemAndAddToListBox(String rightRoleCaption, String rightRoleCode,
            //Map<String, String> mapOfAllRightRoleCodesByCaptions,
            ListBox rightRolesLB) {
//        mapOfAllRightRoleCodesByCaptions.put(rightRoleCaption, rightRoleCode);
//        rightRolesLB.addItem(rightRoleCaption);
        rightRolesLB.addItem(rightRoleCaption, rightRoleCode);
    }

    protected void addRightRolesToRightRolesVp() {
        allRightRolesByCode.putAll(BIKClientSingletons.getMapOfAllRightRolesByCode());
        allRightRolesByCodeCrr.putAll(BIKClientSingletons.getMapOfAllRightRolesByCodeCrr());
//        final Map<String, String> mapOfAllRightRoleCodesByCaptions = new LinkedHashMap<String, String>();
      /* final ListBox*/ rightRolesLB = new ListBox();
        createRightRolesItemAndAddToListBox(I18n.wszyscy.get() /* I18N:  */, null, /*mapOfAllRightRoleCodesByCaptions,*/ rightRolesLB);
        for (String code : allRightRolesByCode.keySet()) {
            createRightRolesItemAndAddToListBox(BIKRightRoles.getRightRoleCaptionByCode(code), code, /*mapOfAllRightRoleCodesByCaptions,*/ rightRolesLB);
//            createRightRolesItemAndAddToListBox(allRightRolesByCode.get(code).caption, code, mapOfAllRightRoleCodesByCaptions, rightRolesLB);
        }

        List<CustomRightRoleBean> crrbs = BIKClientSingletons.getCustomRightRoles();
        for (CustomRightRoleBean crrb : crrbs) {
            if (!BaseUtils.safeEquals("RegularUser", crrb.code)) {
                createRightRolesItemAndAddToListBox(crrb.caption, Integer.toString(crrb.id), /*mapOfAllRightRoleCodesByCaptions,*/ rightRolesLB);
            }
        }
        //dla nowego systemu uprawnieñ
//        List<SystemUserGroupBean> crrbs = BIKClientSingletons.getAllRoles();
//        for (SystemUserGroupBean crrb : crrbs) {
////            if (!BaseUtils.safeEquals("RegularUser", crrb.code)) {
//                createRightRolesItemAndAddToListBox(crrb.name, Integer.toString(crrb.id), /*mapOfAllRightRoleCodesByCaptions,*/ rightRolesLB);
////            }
//        }

        RightRoleBean simpleUserRightRole = new RightRoleBean();
        simpleUserRightRole.id = -1;
        simpleUserRightRole.code = "simpleUser";
        allRightRolesByCode.put("simpleUser", simpleUserRightRole);
        createRightRolesItemAndAddToListBox(I18n.zwyklyUzytkownik.get() /* I18N:  */, simpleUserRightRole.code, /*mapOfAllRightRoleCodesByCaptions,*/ rightRolesLB);
//        rightRolesLB.setVisibleItemCount(allRightRolesByCode != null ? allRightRolesByCode.size() + 1 : 2);
        rightRolesLB.setVisibleItemCount(rightRolesLB.getItemCount());
        rightRolesLB.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
//                String code = mapOfAllRightRoleCodesByCaptions.get(rightRolesLB.getItemText(rightRolesLB.getSelectedIndex()));
                String code = rightRolesLB.getValue(rightRolesLB.getSelectedIndex());
                selectedRightRole = allRightRolesByCode.get(code);
                optFilterOnCustomRightRoleId = BaseUtils.tryParseInteger(code);
                show(/*vp*/);
                boolean isAuthorRightRole = false, isAuthorRightRoleCrr = false;

                if (code != null) {
                    isAuthorRightRole = code.equals(BIKGWTConstants.USER_KIND_CODE_AUTHOR);
                    if (BaseUtils.tryParseInteger(code) != null && !BaseUtils.isMapEmpty(allRightRolesByCodeCrr)) {
                        CustomRightRoleBean crrb = allRightRolesByCodeCrr.get(Integer.parseInt(code));
                        isAuthorRightRoleCrr = crrb != null && crrb.code.equals(BIKGWTConstants.USER_KIND_CODE_AUTHOR);
                    }
                }
                treeOfTreesPanel.setVisible(isAuthorRightRole || isAuthorRightRoleCrr);
//                treeOfTreesPanel.setVisible(code != null && code.equals(BIKGWTConstants.USER_KIND_CODE_AUTHOR));
            }
        });
        rightRolesLB.setStyleName("FilteringListBox");
        rightRolesVp.add(rightRolesLB);
    }

    protected ListBox createFilteringListBox(Map<String, Boolean> statusOfFilter, String listBoxTitle) {
        final ListBox lb = new ListBox();
        for (String s : statusOfFilter.keySet()) {
            lb.addItem(s);
        }
        lb.setStyleName("FilteringListBox");
//        lb.setTitle(listBoxTitle);
        return lb;
    }

    protected Label createCaption(String caption) {
        Label l = new Label(caption);
        l.setStyleName("SystemUserPageCaption");
        return l;
    }

    protected void innerOnFilterHandler() {
        textOfFilter = filterBox.getText();
        show(/*vp*/);
    }

    protected HorizontalPanel createFilterHp() {
        HorizontalPanel filterHp = new HorizontalPanel();
        filterBox = new TextBox();
        filterBox.setStyleName("SystemUserPageFilterBox");
        filterBox.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                NativeEvent ne = event.getNativeEvent();
                if (ne.getKeyCode() == KeyCodes.KEY_ENTER) {
                    innerOnFilterHandler();
                }
            }
        });
        filterHp.add(filterBox);
        filterHp.add(createPushButtonOnImage("images/filter_icon.gif" /* I18N: no */, I18n.filtruj.get() /* I18N:  */, new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        innerOnFilterHandler();
                    }
                }));
        filterHp.add(createPushButtonOnImage("images/clear_filter_16.gif" /* I18N: no */, I18n.wyczyscFiltr.get() /* I18N:  */, new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        filterBox.setText(null);
                        textOfFilter = null;
                        show(/*vp*/);
                    }
                }));
        return filterHp;
    }

    protected PushButton createPushButtonOnImage(String imageUrl, String title, ClickHandler clickHandler) {
        PushButton pBtn = new PushButton(new Image(imageUrl), clickHandler);
        pBtn.setTitle(title);
        pBtn.setStyleName("SystemUserPageFilterPushButton");
        return pBtn;
    }

    protected Map<String, Boolean> createFilterStatusMap(String statusAll, String statusTrue, String statusFalse) {
        Map<String, Boolean> filterStatusMap = new LinkedHashMap<String, Boolean>();
        filterStatusMap.put(statusAll, null);
        filterStatusMap.put(statusTrue, true);
        filterStatusMap.put(statusFalse, false);
        return filterStatusMap;
    }

    protected Widget createFilters() {
        VerticalPanel filtersVp = new VerticalPanel();
        assigningCommunityStatus = new ListBox();
        statusOfUserAccount = new ListBox();
        final Map<String, Boolean> communityStatus = createFilterStatusMap(I18n.wszyscy.get() /* I18N:  */, I18n.przypisany.get() /* I18N:  */, I18n.brak2.get() /* I18N:  */);
        final Map<String, Boolean> userAccountStatus = createFilterStatusMap(I18n.wszyscy.get() /* I18N:  */, I18n.wylaczony.get() /* I18N:  */, I18n.wlaczony.get() /* I18N:  */);
        assigningCommunityStatus = createFilteringListBox(communityStatus, I18n.tutajBedzieOpis.get() /* I18N:  */);
        assigningCommunityStatus.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                hasCommunityUser = communityStatus.get(assigningCommunityStatus.getItemText(assigningCommunityStatus.getSelectedIndex()));
                show(/*vp*/);
            }
        });
        statusOfUserAccount = createFilteringListBox(userAccountStatus, I18n.tutajBedzieOpis.get() /* I18N:  */);
        statusOfUserAccount.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                isSystemUserAccountDisabled = userAccountStatus.get(statusOfUserAccount.getItemText(statusOfUserAccount.getSelectedIndex()));
                show(/*vp*/);
            }
        });
        filtersVp.add(createCaption(I18n.wprowadzFrazeDoFiltru.get() /* I18N:  */ + ":"));
        filtersVp.add(createFilterHp());
        if (!BIKClientSingletons.hideSocialUserInfoOnAdminUsersTab()) {
            filtersVp.add(createCaption(I18n.uzytkownikSpolecznosciowy.get() /* I18N:  */));
            filtersVp.add(assigningCommunityStatus);
        }
        filtersVp.add(createCaption(I18n.aktywnoscKonta.get() /* I18N:  */));
        filtersVp.add(statusOfUserAccount);
        if (BIKClientSingletons.isMultiDomainMode()) {
            filtersVp.add(createCaption(I18n.uwzglednijUprawnieniaZDomen.get()));
            permissionDomainLb = new ListBox();
            permissionDomainLb.addItem(I18n.tak.get());
            permissionDomainLb.addItem(I18n.nie.get());
            permissionDomainLb.setSelectedIndex(1);
            permissionDomainLb.setStyleName("FilteringListBox");
            filtersVp.add(permissionDomainLb);

            permissionDomainLb.addChangeHandler(new ChangeHandler() {

                @Override
                public void onChange(ChangeEvent event) {
                    if (rightRolesLB.getItemText(rightRolesLB.getSelectedIndex()).equals(I18n.wszyscy.get())) {
//                        extractor.setVisible(permissionDomainLb.getSelectedIndex() == 0);
                    } else {
                        show();
                    }
//                    Window.alert(rightRolesLB.getItemText(rightRolesLB.getSelectedIndex()) + " tak=" + rightRolesLB.getItemText(rightRolesLB.getSelectedIndex()).equals(I18n.wszyscy.get()));
//                    show();
                }
            });
        }

        return filtersVp;
    }

    protected void innerShow(/*VerticalPanel vp,*/List<SystemUserBean> systemUsersToDisplay) {
        vp.clear();
        if (extractor == null) {
            extractor = new SystemUserBeanExtractor();
        }
        gridWidget = new GenericBeanGridWidget<SystemUserBean>(true, extractor);
        vp.add(gridWidget.buildGrid(systemUsersToDisplay, systemUsersToDisplay, 0, systemUsersToDisplay.size()));
        displayedSystemUsers = systemUsersToDisplay;
//        extractor.setVisible(BIKClientSingletons.isMultiDomainMode()
//                && permissionDomainLb.getSelectedIndex() == 0);
    }

    protected void show(/*final VerticalPanel vp*/) {
        show(/*vp,*/false);
    }

    protected void show(/*final VerticalPanel vp,*/boolean noNeedToRefetchBeforeShow) {
        boolean showUserInRoleFromGroup = BIKClientSingletons.isMultiDomainMode()
                && permissionDomainLb.getSelectedIndex() == 0;
        if (!noNeedToRefetchBeforeShow) {
            String ticket = BIKClientSingletons.getCancellableRequestTicket();
            if (filterRequestTicket != null) {
                BIKClientSingletons.cancelServiceRequest(filterRequestTicket);
                if (filterRequest != null) {
                    filterRequest.cancel();
                    filterRequest = null;
                }
            }
            filterRequestTicket = ticket;
            filterRequest = callServiceToGetData(ticket, showUserInRoleFromGroup, new StandardAsyncCallback<List<SystemUserBean>>() {
                @Override
                public void onSuccess(final List<SystemUserBean> result) {
                    innerShow(/*vp,*/result);
                }
            });
        } else {
            innerShow(/*vp,*/displayedSystemUsers);
        }
//        noNeedToRefetchBeforeShow = false;
    }

    public static ScrollPanel createScrollPanel(Widget w) {
        return createScrollPanelEx(w, null);
    }

    public static ScrollPanel createScrollPanelEx(Widget w, String width) {
        ScrollPanel scPanelFvsm = new ScrollPanel(w);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(scPanelFvsm, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT);
        scPanelFvsm.setStyleName("EntityDetailsPane");
        if (!BaseUtils.isStrEmptyOrWhiteSpace(width)) {
            scPanelFvsm.setWidth(width);
        }
        return scPanelFvsm;
    }
}
