/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.TreeNodeDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public abstract class NodeActionAddNodeBaseOnTreeKindBase extends NodeActionUpdatingBase {

    protected boolean getNodeKindFromParent = false;
    protected int nodeKindId;
    protected String nodeKindCode;

    @Override
    public void execute() {
        if (!isSocialUserLoggedInWithInfo()) {
            return;
        }
        if (nodeKind.treeKind.equals(BIKConstants.TREE_KIND_META_BIKS) || nodeKind.treeKind.equals(BIKConstants.TREE_KIND_META_BIKS_MENU_CONF)) {
            BIKClientSingletons.getService().getRequiredAttrs(nodeKindId, new StandardAsyncCallback<List<AttributeBean>>() {
                @Override
                public void onSuccess(List<AttributeBean> result) {
                    showDialog(result);
                }
            });
        } else {
            BIKClientSingletons.getService().getRequiredAttrsWithVisOrder(nodeKindId, new StandardAsyncCallback<List<AttributeBean>>() {
                @Override
                public void onSuccess(List<AttributeBean> result) {
                    showDialog(result);
                }
            });
        }
    }

    public void showDialog(List<AttributeBean> result) {
        new TreeNodeDialog().buildAndShowDialog(nodeKindId, nodeKindCode, null,
                node, treeBean.id, null, getNodeKindFromParent, result, new IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>>() {
            @Override
            public void doIt(final Pair<TreeNodeBean, Map<String, AttributeBean>> param2) {

//                Map<String, AttributeBean> attributesFromParent = getAttributesFromParent();
                TreeNodeBean param = param2.v1;
                BIKClientSingletons.getService().createTreeNodeWithAttr(param.parentNodeId,
                        param.nodeKindId, param.name, param.objId, param.treeId, param.linkedNodeId, param.addingJoinedObjs,
                        /*  attributesFromParent != null ? attributesFromParent :*/ param2.v2,
                        new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on" /* I18N: no */ + " createTreeNode") {
                    @Override
                    public void onSuccess(Pair<Integer, List<String>> result) {
                        BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get() /* I18N:  */);
                        //refreshRedisplayDataAndSelectRecord(result.v1, false); // byl zwykly refresh
//                                param.id = result.v1;
//                                param.isFolder = true;
//                                param.branchIds = (node != null) ? (node.branchIds + param.id + "|") : (param.id + "|");
//                                ctx.addTreeGridNode(param); // a co jeśli drzewo jest puste? wtedy wywali sie na tej linii
//                                refreshAndRedisplayDataByTreeCode(result.v2);
//                                node.hasNoChildren = false;
                        refreshRedisplayDataAndSelectRecord(result.v1);
                    }
                });
            }
        });
    }

//    protected Map<String, AttributeBean> getAttributesFromParent() {
//        return null;
//    }
}
