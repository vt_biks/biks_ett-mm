/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author wezyr
 */
public class OrderedList extends ComplexPanel {

    public static OrderedList wrap(Element element) {
        // Assert that the element is attached.
        assert Document.get().getBody().isOrHasChild(element);

        OrderedList widget = new OrderedList(element);

        // Mark it attached and remember it for cleanup.
        widget.onAttach();
        RootPanel.detachOnWindowClose(widget);

        return widget;
    }

    protected OrderedList(Element ele) {
        setElement(ele);
    }

    public OrderedList() {
        this(DOM.createElement("OL" /* I18N: no */));
    }

    @Override
    @SuppressWarnings("deprecation")
    public void add(Widget w) {
        super.add(w, getElement());
    }

    @SuppressWarnings("deprecation")
    public void insert(Widget w, int beforeIndex) {
        super.insert(w, getElement(), beforeIndex, true);
    }
}
