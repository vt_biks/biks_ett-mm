/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import java.util.Set;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.NodeHistoryChangeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.BaseUtils;
import simplelib.SimpleDateUtils;

/**
 *
 * @author ctran
 */
public class NodeHistoryWidgetFlat extends EntityDetailsTabPaginationBase<NodeHistoryChangeBean> {

    protected VerticalPanel gridPanel;

    @Override
    protected List<NodeHistoryChangeBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
        return data.changeHistory;
    }
//
//    @Override
//    public void displayData() {
//        gridPanel.clear();
//        for (NodeHistoryChangeBean change : items) {
//            gridPanel.add(createOneHistoryChangeRow(change));
//        }
//    }

//    @Override
//    public Widget buildWidgets() {
//        gridPanel = new VerticalPanel();
//        gridPanel.setWidth("100%");
//        return gridPanel;
//    }
    @Override
    public String getCaption() {
        return I18n.historia.get();
    }

    @Override
    public String getTreeKindForSelected() {
        return null;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    private Widget createOneHistoryChangeRow(NodeHistoryChangeBean change) {
        StringBuilder msg = new StringBuilder("<b>" + SimpleDateUtils.toCanonicalString(change.dateAdded) + ":</b> " + change.nodeName + ":<b> " + change.changeSource + "</b> ");
        if (BaseUtils.isStrEmpty(change.oldValue)) {
            msg.append(I18n.utworzyl.get() + " <b>" + change.attrName + "</b>: " + BaseUtils.safeToStringTxt(change.newValue));
        } else if (BaseUtils.isStrEmpty(change.newValue)) {
            msg.append(I18n.skasowal.get() + " <b>" + change.attrName + "<b>");
        } else {
            msg.append(I18n.zmienil.get() + " <b>" + change.attrName + "</b>" + (BaseUtils.isStrEmpty(change.oldValue) ? "" : " <b>z</b> " + BaseUtils.safeToStringTxt(change.oldValue)) + " <b>na</b> " + BaseUtils.safeToStringTxt(change.newValue));
        };
        return new HTML(msg.toString());
    }

    @Override
    protected IGridNodeAwareBeanPropsExtractor<NodeHistoryChangeBean> getBeanExtractor() {
        return new NodeHistoryChangeBeanExtractor(hasAddJoinedButton);
    }

    @Override
    protected BikCustomButton createAddJoinedButton(Set<String> treeKinds) {
        return null;
    }
}
