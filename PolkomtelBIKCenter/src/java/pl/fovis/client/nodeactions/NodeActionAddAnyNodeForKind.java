/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class NodeActionAddAnyNodeForKind extends NodeActionAddAnyNode4KindAbstract {

    public NodeActionAddAnyNodeForKind() {
    }

    public NodeActionAddAnyNodeForKind(boolean addAsParent, int treeId) {
        super(addAsParent, treeId);
    }

    @Override
    public String getCaption() {
        return I18n.addAnyNode.get() /* I18N:  */;
    }

    @Override
    public void redisplayData(Integer result) {
        BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get() /* I18N:  */);
        refreshRedisplayDataAndSelectRecord(result);
    }

    @Override
    public boolean isEnabledByDesign() {
        if(BIKClientSingletons.hideAddAnyNodeForKindBtn()){
            return false;
        }
//        return isNodeBranch() ;
        return (!addAsParent ? //isNodeFromDynamicTree()
                isNodeBranch()
                : true) && !isMetaBiks();

    }
}
