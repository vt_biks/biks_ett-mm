/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill.tags;

import java.util.Map;
import pl.foxys.foxymill.IHasTextFilePosition;
import pl.foxys.foxymill.IRenderable;
import pl.foxys.foxymill.RenderEnvironment;

/**
 *
 * @author wezyr
 */
public interface ITag extends IRenderable, IHasTextFilePosition {
//    public void init(Map<String, String> attrs, List<ITag> subTags, ITag prevTag,
//            TextFilePosition filePos);
    public void prepare(RenderEnvironment renderEnv);
    //public void render(IRenderContext renderCtx);
    public void fixUp(Map<String, Object> globals);
}
