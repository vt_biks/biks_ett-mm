/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.TreeAndDictionarySelectorForUsers;
import pl.fovis.common.JoinTypeRoles;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.UseInNodeSelectionBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pgajda
 */
public class NodeActionLinkUserToRoleForBlogs extends NodeActionUpdatingBase {

    @Override
    public boolean isEnabledByDesign() {
        if (isNodeOneOfTheKind(true, BIKGWTConstants.NODE_KIND_BLOG) && isAdminOrExpertOrAuthorOrEditor()
                && (!isNodeBuiltIn())) {
            return true;
        }

        return false;
    }

    public String getCaption() {
        return I18n.przypiszJakoRedaktor.get() /* I18N:  */;
    }

    protected void execAddUserInNodeAndRefresh(UseInNodeSelectionBean uinb) {
        BIKClientSingletons.getService().addUserInNode(node.id, uinb.userId, uinb.roleForNodeId, uinb.inheritToDescendants, uinb.joinTypeRole == JoinTypeRoles.Auxiliary, uinb.joinTypeRole,
                new StandardAsyncCallback<Boolean>("Failed in adding new pair of role and user" /* I18N: no:err-fail-in */) {
                    public void onSuccess(Boolean result) {
                        //refreshAndRedisplayData();
                        refreshRedisplayDataAndSelectRecord(node.id);
                        BIKClientSingletons.showInfo(I18n.dodanoPowiazanie.get() /* I18N:  */);
                        BIKClientSingletons.refreshAndRedisplayDataUsersTree(result);
                    }
                });
    }

    public void execute() {
        //!!!items = null
        BIKClientSingletons.getService().getDescendantsCntInMainRoles(node.id, new StandardAsyncCallback<Map<Integer, Integer>>() {
            public void onSuccess(Map<Integer, Integer> result) {
                new TreeAndDictionarySelectorForUsers() {
                    @Override
                    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
                        BIKClientSingletons.getService().getBikUserRolesTreeNodes(optUpSelectedNodeId, "Editor" /* i18n: no */, useLazyLoading, callback);
                    }

                    @Override
                    protected Widget buildOptionalRightSide() {
                        return null;
                    }
                }.buildAndShowDialog(node.id, //"Users" /* I18N: no */,
                        null, result, new IParametrizedContinuation<UseInNodeSelectionBean>() {
                            public void doIt(UseInNodeSelectionBean param) {
                                execAddUserInNodeAndRefresh(param);
                            }
                        });
            }
        });

    }
}
