/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.TreeBikPage;
import pl.fovis.client.nodeactions.dqm.NodeActionDQMAddDataErrorIssue;
import pl.fovis.client.nodeactions.dqm.NodeActionDQMAddEvaluation;
import pl.fovis.client.nodeactions.dqm.NodeActionDQMAddReport;
import pl.fovis.client.nodeactions.dqm.NodeActionDQMAddTest;
import pl.fovis.client.nodeactions.dqm.NodeActionDQMCheckRate;
import pl.fovis.client.nodeactions.dqm.NodeActionDQMDeleteTest;
import pl.fovis.client.nodeactions.dqm.NodeActionDQMDownloadXLSX;
import pl.fovis.client.nodeactions.dqm.NodeActionDQMEditDataErrorIssue;
import pl.fovis.client.nodeactions.dqm.NodeActionDQMEditEvaluation;
import pl.fovis.client.nodeactions.dqm.NodeActionDQMEditTest;
import pl.fovis.client.nodeactions.dqm.NodeActionDQMRefreshReport;
import pl.fovis.client.nodeactions.lisa.NodeActionAddLisaCategorization;
import pl.fovis.client.nodeactions.lisa.NodeActionAddLisaCategory;
import pl.fovis.client.nodeactions.lisa.NodeActionDelLisaCategory;
import pl.fovis.client.nodeactions.lisa.NodeActionLisaAddFolder;
import pl.fovis.client.nodeactions.lisa.NodeActionLisaExport;
import pl.fovis.client.nodeactions.lisa.NodeActionLisaImport;
import pl.fovis.client.nodeactions.lisa.NodeActionMoveLisaCategory;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.MoveNodeInTreeMode;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.popupmenu.FoxyPopUpMenu;
import pl.fovis.foxygwtcommons.popupmenu.IPopupMenuItemAction;

/**
 *
 * @author wezyr
 */
public class BiksPopUpMenu extends FoxyPopUpMenu<INodeActionContext> {

    List<NodeKindBean> nodeKindsForTreeKind = new ArrayList();

    public BiksPopUpMenu(TreeBikPage bikPage, TreeNodeBean node, EntityDetailsDataBean data, int clientX, int clientY) {
        super(new NodeActionContext(node, bikPage, data), clientX, clientY);
    }

    @Override
    protected void addActions() {
        getNodeKinds();
        //Zarzadaznie Menu
        addAction(new NodeActionMetaMenuApply());
        addAction(new NodeActionMetaMenuAddSection());

        addAction(new NodeActionRename());
        addAction(new NodeActionRenameRole());
        addAction(new NodeActionEditDescription());
        addAction(new NodeActionNewNestableParent());
        addAction(new NodeActionAddRole());

        // NAS
        //akcje chwilowo wycofane tylko na ST i PRD, uzgodnione z Jerzym, musi stworzyc procedury w teradacie
        addAction(new NodeActionAddLisaCategory());
        addAction(new NodeActionDelLisaCategory());
        addAction(new NodeActionMoveLisaCategory());
        addAction(new NodeActionAddLisaCategorization());
        addAction(new NodeActionLisaAddFolder());
        addAction(new NodeActionLisaExport());
        addAction(new NodeActionLisaImport());

        // Confluence
        addAction(new NodeActionAddObjectFromConfluence());
        addAction(new NodeActionAddObjectToConfluence());

//        addSeparator();
//        addAction(new NodeActionRunScript());
        // DQM
        addSeparator();
        addAction(new NodeActionDQMAddTest());
        addAction(new NodeActionDQMEditTest());
        addAction(new NodeActionDQMDeleteTest());
//        addAction(new NodeActionDQMUnassign()); // rezygnujemy z podlinkowywania testów
//        addAction(new NodeActionDQMActivateTest());
//        addAction(new NodeActionDQMDeactivateTest());
//        addAction(new NodeActionDQMAssignTestToGroup()); // rezygnujemy z podlinkowywania testów
        addAction(new NodeActionDQMAddEvaluation());
        addAction(new NodeActionDQMAddReport());
        addAction(new NodeActionDQMAddDataErrorIssue());
        addAction(new NodeActionDQMEditDataErrorIssue());
        addAction(new NodeActionDQMEditEvaluation());
        addAction(new NodeActionDQMCheckRate());
        addAction(new NodeActionDQMRefreshReport());
        addAction(new NodeActionDQMDownloadXLSX());
        addSeparator();

        addAction(new NodeActionAddDefinition());
//        addAction(new NodeActionPromoteToDefinition());
        addAction(new NodeActionAddVersionDefinition());
        addAction(new NodeActionEditDefinition());

        addAction(new NodeActionAddQuestionAndAnswer());

        addSeparator();
        addAction(new NodeActionCopy());
        addAction(new NodeActionCut());
        addAction(new NodeActionPaste());
        addAction(new NodeActionDelete());
        addAction(new NodeActionDeleteRole());
        addSeparator();

        addAction(new NodeActionMoveInTree(I18n.przeniesNaGore.get() /* I18N:  */, MoveNodeInTreeMode.MakeFirst));
        addAction(new NodeActionMoveInTree(I18n.przeniesWyzej.get() /* I18N:  */, MoveNodeInTreeMode.MoveUp));
        addAction(new NodeActionMoveInTree(I18n.przeniesNizej.get() /* I18N:  */, MoveNodeInTreeMode.MoveDown));
        addAction(new NodeActionMoveInTree(I18n.przeniesNaDol.get() /* I18N:  */, MoveNodeInTreeMode.MakeLast));
        addSeparator();
        addAction(new NodeActionResetVisualOrder());
        addAction(new NodeActionResetVisualOrderToDefObject());
        addSeparator();

        addAction(new NodeActionGrantRightsOldRightsSys());
        addCustomRoleRightsActions();
//        addAction();

        addSeparator();
        addAction(new NodeActionAddBlogEntry());
        addAction(new NodeActionEditBlogEntry());

        addAction(new NodeActionLinkNode());
        addAction(new NodeActionAddAnyNode());
        addAction(new NodeActionAddAnyNodeForKind());
        addAction(new NodeActionAddAttributeForKind());
        addAction(new NodeActionAddStatus());
//        addAction(new NodeActionLinkNodeList());
//        addAction(new NodeActionChangeToLeaf());
//        addAction(new NodeActionChangeToBranch());
//        addAction(new NodeActionAddBranch());
//        addAction(new NodeActionAddLeaf());

        //metaBIKS
        addAction(new NodeActionAddAttributesCatalog4MetaBiks());
        addAction(new NodeActionAddAttributeDef4MetaBiks());
        addAction(new NodeActionAddNodeKind4MetaBiks());
        addAction(new NodeActionAddAttribute4NodeKind4MetaBiks());
        addAction(new NodeActionAddTreeKindDef4MetaBiks());
        addAction(new NodeActionAddNodeKind4TreeKind4MetaBiks());
        addAction(new NodeActionAddNodeKindInRelation4MetaBiks());
        addAction(new NodeActionAddPrintTemplate4MetaBiks());
        addAction(new NodaActionAddPrintAttribut());
        addAction(new NodeActionAddPrintNodeKindObject());

//
        addAction(new NodeActionAddDocument());
        addCustomTreeActions();
        //        addCustomTreeActions();

        addAction(new NodeActionJumpToLinkedNode());

        addAction(new NodeActionAddUser());
        addAction(new NodeActionLinkUser());
        addAction(new NodeActionEditUser());
        addAction(new NodeActionCopyJojnedObj());
        addAction(new NodeActionLinkUserToRole());
        addAction(new NodeActionLinkUserToRoleForBlogs());
        addSeparator();
        addAction(new NodeActionGetJasperReport());
        addAction(new NodeActionSaveTree());
        addAction(new NodeActionPrintDocument());
    }

    @Override
    public void addAction(IPopupMenuItemAction<INodeActionContext> act) {
        super.addAction(act); //To change body of generated methods, choose Tools | Templates.}
    }

    protected void getNodeKinds() {
        nodeKindsForTreeKind = BIKClientSingletons.getNodeKindsForTreeKind(contextObject.getNode());
    }

    protected void addCustomTreeActions() {
        for (NodeKindBean nkb : nodeKindsForTreeKind) {
            addAction(new NodeActionAddCustomTreeNode(nkb));
        }
    }

    protected void addCustomRoleRightsActions() {
        NodeKindBean nkb = BIKClientSingletons.getNodeKindBeanByCode(contextObject.getNode().nodeKindCode);

        for (SystemUserGroupBean sugb : nkb.associatedRoles) {
            addAction(new NodeActionGrantRightsNewRightsSys(sugb));
        }
    }
}
