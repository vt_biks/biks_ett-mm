/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
public class EditEntityKeywordsDialog extends BaseActionOrCancelDialog {

    private TextBox tbKeywords;
    private Label status;
    private String keywords;
    private IParametrizedContinuation<String> saveCont;

    @Override
    protected String getDialogCaption() {
        return I18n.edycjaSlowKluczowych.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {


        status = new Label();
        main.add(status);

        Grid grid = new Grid(1, 2);

        grid.setWidget(0, 0, new Label(I18n.slowaKluczowe.get() /* I18N:  */));
        grid.setWidget(0, 1, tbKeywords = new TextBox());
        tbKeywords.setText(keywords);

        main.add(grid);
    }

    @Override
    protected void doAction() {
        saveCont.doIt(tbKeywords.getText());
    }

    public void buildAndShowDialog(String keywords, IParametrizedContinuation<String> saveCont) {
        this.saveCont = saveCont;
        this.keywords = keywords;

        super.buildAndShowDialog();
    }
}
