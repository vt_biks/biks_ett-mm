/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import java.util.Set;

/**
 *
 * @author pmielanczuk
 */
public interface IServerSupportedLocaleNameRetriever {

    public Set<String> getServerSupportedLocaleNames();
}
