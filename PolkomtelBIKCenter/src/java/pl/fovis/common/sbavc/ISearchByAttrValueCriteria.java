/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.sbavc;

/**
 *
 * @author pmielanczuk
 */
public interface ISearchByAttrValueCriteria {

    public String getShortOperatorName();

    public String getCriteriaMessage();

    public void setIdInSelectedCriteriaList(int id);

    public Integer getIdInSelectedCriteriaList();

    public void setTxtPrefix(String postFix);

    public int getAttrId();
}
