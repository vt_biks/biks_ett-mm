/*
 * ComplexSimilarityContainer.java
 *
 * Created on 5 wrzesie� 2007, 17:34
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package commonlib.similarity;

import commonlib.JdbcUtil;
import commonlib.LameUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import simplelib.LameBag;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class ComplexSimilarityContainer<T> {

    private SimilarityContainerIntf sc;// = new SimilarityContainer3(50);
    private Map<String, List<String[]>> wordsMap = new HashMap<String, List<String[]>>();
    private Map<String[], T> normalizedArticles = new HashMap<String[], T>();

    public ComplexSimilarityContainer(boolean useOldContainer) {
        sc = useOldContainer ? new SimilarityContainer2(50) : new SimilarityContainer3(50);
    }

    public ComplexSimilarityContainer() {
        this(false);
    }

    @Deprecated
    @SuppressWarnings("unchecked")
    public void add(String words) {
        add(words, (T) words);
    }

    public void add(String words, T article) {
        String[] wordArr = LameUtils.buildWordArrayNoSort(words);
        if (wordArr == null) {
            return;
        }

        normalizedArticles.put(wordArr, article);

        for (int i = 0; i < wordArr.length; i++) {
            sc.add(wordArr[i]);
            //System.out.println("simple cntr.add: " + wordArr[i]);
            LameUtils.addToCollectingMapWithList(wordsMap, wordArr[i], wordArr);
        }
    }

    static protected class AWordMatch implements Comparable<AWordMatch> {

        String word;
        double rank;

        AWordMatch(String word, double rank) {
            this.word = word;
            this.rank = rank;
        }

        public int compareTo(ComplexSimilarityContainer.AWordMatch o) {
            return Double.compare(rank, o.rank);
        }

        public String toString() {
            return word + ": " + rank;
        }
    }

//    static protected class SWordMatches {
//        String word;
//        List<AWordMatch> aMatches;
//        SWordMatches(String word, List<AWordMatch> aMatches) {
//            this.word = word;
//            this.aMatches = aMatches;
//        }
//    }
    public Map<T, Double> find(String words) {
        //System.out.println("find: " + words);
        String[] sWordArr = LameUtils.buildWordArrayNoSort(words);

        if (sWordArr == null) {
            return null;
        }

        int sWordArrCharLen = 0;
        for (String sWord : sWordArr) {
            sWordArrCharLen += sWord.length();
        }

        // [article:[sword:awordmatch]]
        Map<String[], Map<String, List<AWordMatch>>> articleMatches
                = new HashMap<String[], Map<String, List<AWordMatch>>>();

        for (String sw : sWordArr) {
            Map<String, Double> matches = sc.findSimilar(sw);

            for (Entry<String, Double> e : matches.entrySet()) {
                String aw = e.getKey();
                Double rank = e.getValue();

                List<String[]> articles = wordsMap.get(aw);

                for (String[] art : articles) {
                    Map<String, List<AWordMatch>> oneArtMatches = articleMatches.get(art);
                    if (oneArtMatches == null) {
                        oneArtMatches = new HashMap<String, List<AWordMatch>>();
                        articleMatches.put(art, oneArtMatches);
                    }
                    LameUtils.addToCollectingMapWithList(oneArtMatches, sw, new AWordMatch(aw, rank));
                }
            }
        }

        Map<T, Double> res = new HashMap<T, Double>();

        //System.out.println("articleCnt: " + articleMatches.size());
        for (Entry<String[], Map<String, List<AWordMatch>>> artEntry : articleMatches.entrySet()) {
            String[] art = artEntry.getKey();
            Map<String, List<AWordMatch>> oneArtMatches = artEntry.getValue();

            if (oneArtMatches.size() < art.length - 1 || oneArtMatches.size() < sWordArr.length - 1) {
                continue;
            }

            //System.out.println("article: " + Arrays.toString(art) + ", matches: " + oneArtMatches);
            Iterator<Entry<String, List<AWordMatch>>> oneArtMatchesIter
                    = oneArtMatches.entrySet().iterator();
            while (oneArtMatchesIter.hasNext()) {
                Entry<String, List<AWordMatch>> e = oneArtMatchesIter.next();
                String sw = e.getKey();
                List<AWordMatch> matches = e.getValue();

                Collections.sort(matches);
            }

            List<Entry<String, List<AWordMatch>>> sortedArtMatches = new ArrayList<Entry<String, List<AWordMatch>>>(oneArtMatches.entrySet());
            Collections.sort(sortedArtMatches, new Comparator<Entry<String, List<AWordMatch>>>() {
                public int compare(Map.Entry<String, List<ComplexSimilarityContainer.AWordMatch>> o1, Map.Entry<String, List<ComplexSimilarityContainer.AWordMatch>> o2) {
                    return Double.compare(o2.getValue().get(0).rank, o1.getValue().get(0).rank);
                }
            });

            LameBag<String> artWordBag = new LameBag<String>(art);
            int matchCnt = 0, unitMatchCnt = 0, nonUnitUnmatchedCnt = 0;
            double rankSum = 0;
            double weightSum = 0;
            int sWordArrUnmatchedCharLen = sWordArrCharLen;
            for (Entry<String, List<AWordMatch>> e : sortedArtMatches) {
                List<AWordMatch> aMatches = e.getValue();
                int idx = 0;
                while (idx < aMatches.size()) {
                    AWordMatch aMatch = aMatches.get(idx);
                    boolean isUnitMatch = aMatch.word.length() <= 2;

                    if (artWordBag.remove(aMatch.word)) {
                        sWordArrUnmatchedCharLen -= e.getKey().length();

                        matchCnt++;
                        double weight = Math.sqrt(aMatch.word.length());
                        rankSum += aMatch.rank * weight;
                        weightSum += weight;
                        if (isUnitMatch) {
                            unitMatchCnt++;
                        }

                        break;
                    }

                    idx++;
                }
            }
            if (matchCnt == 0 || matchCnt < sWordArr.length - 1) {
                continue;
            }

            if (sWordArrUnmatchedCharLen > 2) {
                nonUnitUnmatchedCnt++;
            }

            for (String wordFromBag : artWordBag) {
                boolean isUnitMatch = wordFromBag.length() <= 2;
                if (!isUnitMatch) {
                    nonUnitUnmatchedCnt++;
                }
            }

            double penalty = unitMatchCnt == 0 || nonUnitUnmatchedCnt == 0 ? 1.0 : 0.96;

            if (matchCnt != sWordArr.length || matchCnt != art.length) {
                int minLenDiff = sWordArr.length - matchCnt;
                int maxLenDiff = art.length - matchCnt;
                if (minLenDiff > maxLenDiff) {
                    int t = minLenDiff;
                    minLenDiff = maxLenDiff;
                    maxLenDiff = t;
                }

                if (maxLenDiff > 1) {
                    penalty *= 0.94;
                }
                if (minLenDiff > 1) {
                    penalty *= 0.94;
                } else //if (minLenDiff == 1)
                {
                    penalty *= 0.985;
                }
                //else penalty *= 0.985;

                if (matchCnt < 2) {
                    penalty *= 0.90;
                }
            } else if (matchCnt == 1) {
                // wszystko OK, dok�adnie 1 s�owo i pasuje
                penalty = rankSum / weightSum / 100;
            }

//            double penalty = (matchCnt == sWordArr.length ? 1.0 :
//                matchCnt > 1 ? 0.98 : 0.94) *
//                    (matchCnt == art.length ? 1.0 : 0.98);
//            System.out.println("matchCnt=" + matchCnt + ", rankSum=" + rankSum + ", weightSum=" + weightSum +
//                    ", penalty=" + penalty);
            res.put(normalizedArticles.get(art), //LameUtils.mergeWithSep(art, " "),
                    rankSum / weightSum * penalty);
        }

        List<Entry<T, Double>> resToSort = new ArrayList<Entry<T, Double>>(res.entrySet());

        Collections.sort(resToSort, new Comparator<Entry<T, Double>>() {
            public int compare(Map.Entry<T, Double> o1, Map.Entry<T, Double> o2) {
                return Double.compare(o2.getValue(), o1.getValue());
            }
        });

        Map<T, Double> sortedRes = new LinkedHashMap<T, Double>();

        int finalResCnt = 0;
        for (Entry<T, Double> e : resToSort) {
            finalResCnt++;
            if (finalResCnt > 10) {
                break;
            }
            sortedRes.put(e.getKey(), e.getValue());
        }

        return sortedRes;
    }

    public static void simpleTestAlaMaKota() {
        ComplexSimilarityContainer<String> csc = new ComplexSimilarityContainer<String>();
        csc.add("ala ma kota");
        csc.add("ala ma kota ma");
        csc.add("hela ma psa");
        csc.add("szczaw");
        System.out.println(csc.find("koty ma ala"));
        System.out.println(csc.find("hela mam psy"));
        System.out.println(csc.find("szcza"));
        System.out.println(csc.find("szczawy"));
    }

    public static void simpleGeorgTest() {
        ComplexSimilarityContainer csc = new ComplexSimilarityContainer();
        csc.add("michal kosciuszko");
        csc.add("george michael");
        csc.add("michael george");
        System.out.println(csc.find("georg micheal"));
    }

    private static void printTest(ComplexSimilarityContainer csc, String s) {
        System.out.println("test: " + s + ", result: " + csc.find(s));
    }

    public static void repeatWordTest() {
        ComplexSimilarityContainer csc = new ComplexSimilarityContainer();
        csc.add("michal kosciuszko");
        csc.add("george michael");
        csc.add("michael michael");
        csc.add("george george");
        csc.add("michael george");
        csc.add("micheal george");
        csc.add("c c music factory");
        csc.add("schwarz c m seaton m a fisiak j");
        csc.add("seaton c a fisiak j");
        csc.add("jane blige mary");
        csc.add("mary j blige");
        csc.add("Atom Egoyan ");
        csc.add("aAWK");
        csc.add("ABWK");
        csc.add("AK");
        csc.add("AWZK");
        csc.add("AWKX");
        csc.add("UAWK");
        csc.add("WK");
        csc.add("Bj�rk");
        csc.add("Bjork");
        csc.add("Wolfgang B�ck");
        csc.add("Wolfgang Bock");

        printTest(csc, "georg micheal");
        printTest(csc, "mary j kolo");
        printTest(csc, "mary j blige");
        printTest(csc, "mary jene blige");
        printTest(csc, "mary blige");
        printTest(csc, "mery j blige");
        printTest(csc, "marry blige");
        printTest(csc, "AWK");
        printTest(csc, "AZXK");
        printTest(csc, "Atom");
        printTest(csc, "Bj�rk");
        printTest(csc, "Bjork");

        System.out.println("DamerauLevenshteinDistance(AZXK, AWZK): " + StringSimilarityUtils.calcDamerauLevenshteinDistance("AZXK", "AWZK"));
    }

    public static void main(String[] args) throws IOException {
        //simpleTestAlaMaKota();
        //simpleGeorgTest();
        repeatWordTest();

        //testBPR_OSOBY();
        //testBPR_OSOBYAuto();
    }

    public static ComplexSimilarityContainer<String> readBPR_OSOBY() throws IOException {
        ComplexSimilarityContainer<String> csc = new ComplexSimilarityContainer<String>();

        Connection conn = JdbcUtil.makeConn("org.postgresql.Driver", //"jdbc:postgresql://localhost/postgres", "postgres", "kolo"
                "jdbc:postgresql://212.244.29.28/empikWW", "smok", "euro2012byflexi"
        );

        int rowCnt = 0;
        try {
            //conn.setCatalog("bpr");
            Statement st = conn.createStatement();

            System.out.println("before execute query");
            ResultSet rs = st.executeQuery("select OSO_ID, IMIE, NAZWISKO, NAZWA_ZESPOLU from bpr_osoba");
            System.out.println("before read data loop");

            while (rs.next()) {
                Map<String, Object> dataRow = JdbcUtil.resultSetCurrRecToMap(rs);
                String words = LameUtils.nullToDef(dataRow.get("IMIE"), "") + " "
                        + LameUtils.nullToDef(dataRow.get("NAZWISKO"), "") + " "
                        + LameUtils.nullToDef(dataRow.get("NAZWA_ZESPOLU"), "");
                csc.add(words);
                rowCnt++;
                if (rowCnt % 1000 == 0) {
                    System.out.println("read in progress, rowCnt=" + rowCnt);
                }
            }
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }

        System.out.println("read finished, rowCnt=" + rowCnt);

        return csc;
    }

    public static void testBPR_OSOBYAuto() throws IOException {
        ComplexSimilarityContainer<String> csc = readBPR_OSOBY();

        List<String> searchList = new ArrayList<String>(csc.normalizedArticles.values());

        for (int i = 0; i < 10000 && i < searchList.size(); i++) {
            System.out.println("progress: " + i + ", *** " + csc.find(searchList.get(//searchList.size() - 1 -
                    i)));
        }
    }

    public static void testBPR_OSOBY() throws IOException {
        ComplexSimilarityContainer csc = readBPR_OSOBY();

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            String line = in.readLine();
            if (line == null) {
                break;
            }
            if (line.equals("")) {
                continue;
            }
            System.out.println(csc.find(line));
        }
    }
}
