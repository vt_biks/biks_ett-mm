/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import java.util.List;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.ParametersAnySqlBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class AdminConfigAnySqlWidget extends AdminConfigManyServersBase<List<ParametersAnySqlBean>, ParametersAnySqlBean> {
//extends AdminConfigBIKBaseWidget<ConnectionParametersPermissionBean> {

    protected CheckBox chbIsActive;
    protected PushButton btnSave;
    protected Label lbl, nameLbl;
    protected TextBox nameSqlTb;
    protected TextArea sqlTa;
    protected ParametersAnySqlBean anySqlBean;

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_BIKS_SQL;
    }

    @Override
    protected void buildRightWidget(CellPanel panelRight) {
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
            }
        }, false, panelRight);
        nameLbl = createLabel(I18n.nazwa.get(), panelRight);
        createLine(panelRight);
        nameSqlTb = createTextBox(panelRight);
        lbl = createLabel(I18n.zapytanieSQL.get(), panelRight);
        sqlTa = createTextArea(panelRight);

        btnSave = createButton(I18n.zapisz.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

//                ParametersAnySqlBean bean = new ParametersAnySqlBean();
                anySqlBean.name=nameSqlTb.getText();
                anySqlBean.query = sqlTa.getText();
                anySqlBean.isActive = chbIsActive.getValue()/* == true ? 1 : 0*/;

                callServiceToSaveData(anySqlBean, new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " setAnySqlParameters " + getSourceName()) {

                            @Override
                            public void onSuccess(Void result) {
                                listbox.setItemText(getSelectedIndex(), anySqlBean.name);
                                BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get() /* I18N:  */);
                            }
                        });
            }
        }, panelRight);

    }

    @Override
    protected void activateForServer(String id) {
        anySqlBean = serversMap.get(id);
        nameSqlTb.setText(anySqlBean.name);
        sqlTa.setText(anySqlBean.query);
        chbIsActive.setValue(anySqlBean.isActive/* == 1*/);
        setWidgetsEnabled(chbIsActive.getValue());
    }

    protected void callServiceToSaveData(ParametersAnySqlBean bean, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().setAnySqlParameters(bean, asyncCallback);
    }

    @Override
    protected void callServiceToAddNewServer(String source, String serverName, boolean needSeparateSchedule, AsyncCallback<ParametersAnySqlBean> asyncCallback) {
        BIKClientSingletons.getService().addAnySql(source, serverName, asyncCallback);
    }

    @Override
    protected void callServiceToDeleteServer(int id, String serverName, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().deleteAnySql(getSourceName(), id, serverName, asyncCallback);
    }

    @Override
    protected void callServiceToTestConnection(int id, AsyncCallback<Pair<Integer, String>> asyncCallback) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    protected String getListBoxName() {
        return I18n.wybierzZapytanie.get();
    }

    protected String getTextToShowOnDialogAfterClickAddButton() {
        return I18n.nazwaZapytania.get();
    }

}
