/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import com.google.gwt.user.client.ui.HTML;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.IContinuation;

/**
 *
 * @author bfechner
 */
public class NodeActionDeleteRole extends NodeActionUpdatingBase {

    @Override
    public boolean isEnabledByDesign() {
        return //isAppAdmin()
                //ww->authors: przerabiam na eksperta
                isEffectiveExpert()
                && !isNodeBuiltIn()
                && isNodeNotLinkedAnyhowNorReadOnlyTreeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_USERS_ROLE);
    }

    public String getCaption() {
        return I18n.usunRole.get() /* I18N:  */;
    }

    public void execute() {
        HTML element = new HTML(
                I18n.czyNaPewnoUsunacElement.get() /* I18N:  */ + ": <div class='PUWelement'><img src='images" /* I18N: no */ + "/"
                + BIKClientSingletons.getNodeKindIconNameByCode(node.nodeKindCode, node.treeCode) + "." + "gif" /* I18N: no */ + "'/> " + node.name + "</div> " + I18n.wrazZElementaPodrzednUsunieszWsz.get() /* I18N:  */ + ".");
        element.setStyleName("komunikat" /* I18N: no:css-class */);
        new ConfirmDialog().buildAndShowDialog(element, I18n.usun.get() /* I18N:  */, new IContinuation() {
                    public void doIt() {

                        BIKClientSingletons.getService().deleteTreeNodeWithTheUserRole(node.id, node.branchIds, new StandardAsyncCallback<List<String>>("Failure on" /* I18N: no */ + " deleteTreeNode") {
                            public void onSuccess(List<String> result) {
                                //mm-> po co zwracana jest lista drzew do odświeżenia, jeśli nic się z nią nie robi?
                                BIKClientSingletons.showInfo(I18n.usunietoElement.get() /* I18N:  */);
                                ctx.getTreeGrid().deleteNode(node.id);
                            }
                        });
                    }
                });
        clipboard.clear();
    }
}
