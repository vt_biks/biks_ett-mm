/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.connector;

import java.util.Set;
import pl.fovis.server.BOXIServiceImpl;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.INamedSqlsDAO;

/**
 *
 * @author ctran
 */
public class PumpBase {

    protected BOXIServiceImpl service;
    protected INamedSqlsDAO<Object> adhocDao;

    public PumpBase(BOXIServiceImpl service) {
        this.service = service;
        this.adhocDao = service.getAdhocDao();
    }

    protected void execNamedCommandWithCommit(String name, Object... args) {
        adhocDao.execNamedCommand(name, args);
//        commitTran(); // polaczenie jest juz w autocommitmode
    }

    protected <V> V execNamedQuerySingleVal(String qryName, boolean allowNoResult,
            String optColumnName, Object... args) {
        return BeanConnectionUtils.<V>execNamedQuerySingleVal(adhocDao, qryName, allowNoResult,
                optColumnName, args);
    }

    protected <V> Set<V> execNamedQuerySingleColAsSet(String qryName, String optColumnName, Object... args) {
        return BeanConnectionUtils.execNamedQuerySingleColAsSet(adhocDao, qryName, optColumnName, args);
    }
}
