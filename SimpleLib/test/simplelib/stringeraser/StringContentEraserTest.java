/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.stringeraser;

import commonlib.FileNamesProvider;
import commonlib.LameUtils;
import java.util.HashSet;
import java.util.Set;
import org.junit.Test;
import simplelib.BaseUtils;
import simplelib.stringeraser.StringContentEraser.IScriptBatchCollector;

/**
 *
 * @author pmielanczuk
 */
public class StringContentEraserTest {

    private static void testSplitMSSQLScriptOneFile(String fileName) {
        final String encoding = "UTF-8";

        String content = LameUtils.loadAsString(fileName, encoding);

        final StringBuilder sb = new StringBuilder();

        StringContentEraser.splitMSSQLScript(content, new IScriptBatchCollector() {
            public void collect(String batch, int startLine, int endLine) {
                sb.append(batch);
                sb.append("/*normalized*/GO --lines ").append(startLine + 1).append(" to ").append(endLine).append("\n");
            }

            public void initialInfo(int batchCount, int lineCount) {
                //ww: no-op
            }
        });

        LameUtils.saveStringNoExc(fileName + "-norma.sql", sb.toString(), encoding, false);
        LameUtils.saveStringNoExc(fileName + "-erase.sql", StringContentEraser.eraseMSSQLScript(content, true, true),
                encoding, false);
    }

    @Test
    public void testSplitMSSQLScript() {

        String dir = "c:/temp/SplitMSSQLScripts/";

        Set<String> fileNames = BaseUtils.listResources(FileNamesProvider.getInstance(), dir, BaseUtils.paramsAsSet(".sql"), true);

        Set<String> properFileNames = new HashSet<String>();
        for (String fileName : fileNames) {
            if (BaseUtils.strHasSuffix(fileName, "-erase.sql", true)
                    || BaseUtils.strHasSuffix(fileName, "-norma.sql", true)) {
                continue;
            }
            properFileNames.add(fileName);
        }

        fileNames = properFileNames;
        final int fileCnt = fileNames.size();
        System.out.println("dir=\"" + dir + "\", found sql files: " + fileCnt);

        int fileNum = 0;

        for (String fileName : fileNames) {
            fileNum++;
            System.out.println("processing file [" + fileNum + "/" + fileCnt + "]: \"" + fileName + "\"");
            testSplitMSSQLScriptOneFile(fileName);
        }

        System.out.println("done");
    }

    private static void testEraseForOneStringInner(StringContentEraser sce, String s,
            boolean ewd, boolean pwc) {
        sce.isErasingWithDelims = ewd;
        sce.isPreservingWhiteChars = pwc;
        String res = sce.erase(s);
        System.out.println(
                "e=" + (ewd ? "T" : "F") + "/p=" + (pwc ? "T" : "F")
                + (s.length() == res.length() ? "  length OK" : "  bad length") + "; res=" + res);
    }

    private static void testEraseForOneString(String s) {
        System.out.println("--- testing string:" + s);
        StringContentEraser sce = new StringContentEraser();
        sce.esis.add(new ESISqlQuotedString('\''));
        sce.esis.add(new ESISqlQuotedString('"'));
        sce.esis.add(new ESISqlPrefixSuffixDelimitedString("[", "]"));
        sce.esis.add(new EsiBlockComment());
        sce.esis.add(new ESILineComment());
        testEraseForOneStringInner(sce, s, false, false);
        testEraseForOneStringInner(sce, s, false, true);
        testEraseForOneStringInner(sce, s, true, false);
        testEraseForOneStringInner(sce, s, true, true);
    }

//    @Test
    public void testErase() {
        testEraseForOneString("ala 'ma\nkota''x'");
        testEraseForOneString("'koń''świnia'");
        testEraseForOneString("knurzyca");
        testEraseForOneString("knurzyca '' 2");
        testEraseForOneString("knurzyca '''' 3");
        testEraseForOneString("[knu'rzyca '']'' 3");
        testEraseForOneString("/*[knu'rzyca*/ '']'' 3");
        testEraseForOneString("/*[knu'rzy/*ca*/ '']'' 3");
        testEraseForOneString("'/*[knu'rzy/*ca*/ '']'' 3");
        testEraseForOneString("'/*[knu\n--aqq \r'rzy/*ca*/ '']'' 3");
        testEraseForOneString("knu\n--aqq \r['rzy/*ca*/ '']'' 3");
    }
}
