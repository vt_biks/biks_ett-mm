/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import simplelib.BeanWithIntIdBase;

/**
 *
 * @author pmielanczuk
 */
public class SimilarNodeBean extends BeanWithIntIdBase {

    public String name;
    public double relevance;
    public int treeId;
    public String branchIds;
    public int nodeKindId;

    @Override
    public String toString() {
        return "SimilarNodeBean{id=" + id + ", name=" + name + ", relevance=" + relevance + ", treeId=" + treeId + ", branchIds=" + branchIds + ", nodeKindId=" + nodeKindId + '}';
    }
}
