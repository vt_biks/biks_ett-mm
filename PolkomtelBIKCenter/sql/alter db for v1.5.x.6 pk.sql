exec sp_check_version '1.5.x.6';
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
-- poprawki w konfiguracji - ustawienie sciezki docelowej 
--   kopiowanych baz, wylaczenie SSO
-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------


declare @regular varchar(max) = (select top 1 (physical_name) from sys.database_files where name ='boxi');
declare @reversed varchar(max) = (REVERSE(@regular) );
declare @path varchar(max) =  LEFT(@regular, len(@regular) - CHARINDEX('\', @reversed)+1) ;
--'
update mltx_app_prop set val = @path where name = 'database_files_destination_path'
go

update bik_app_prop set val =0 where name ='showSSOLoginDialog';
go


exec sp_update_version '1.5.x.6', '1.5.x.7';
go
