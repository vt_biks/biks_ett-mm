/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts.luceneUtils;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import org.apache.lucene.document.Document;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.JoinedObjsTabKindEx;
import pl.fovis.server.fts.ISimilarNodesWizard;

/**
 *
 * @author pmielanczuk
 */
public class SearchWizardBucketManagerJoinedObjsTabKindEx implements ISearchWizardBucketManager<JoinedObjsTabKindEx> {

    private final Map<String, JoinedObjsTabKindEx> map;
    private final int bucketCount;

    public SearchWizardBucketManagerJoinedObjsTabKindEx(Collection<String> dynamicTreeKinds) {

        map = BIKCenterUtils.makeTreeKindToJoinedObjsTabKindMap(dynamicTreeKinds);

        Set<JoinedObjsTabKindEx> bucketVals = EnumSet.noneOf(JoinedObjsTabKindEx.class);
        bucketVals.addAll(map.values());
        bucketCount = bucketVals.size();
    }

    @Override
    public int getBucketCount() {
        return bucketCount;
    }

    @Override
    public JoinedObjsTabKindEx assignToBucket(Document doc) {
        String treeKind = doc.get(ISimilarNodesWizard.TREE_KIND_ATTR_NAME);
        JoinedObjsTabKindEx res = map.get(treeKind);

//        if (res == JoinedObjsTabKindEx.Blogs || res == JoinedObjsTabKindEx.Documents) {
//            System.out.println("assignToBucket: id=" + doc.get(NODE_ID_FIELD_NAME) + ", kind=" + res);
//        }
        return res;
    }
}
