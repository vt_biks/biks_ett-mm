/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.i18nsupport;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author pmielanczuk
 */
public class LocaleNegotiationResultBean implements Serializable {

    public String clientLocaleName;
    public String serverLocaleName;
    public Set<String> serverSupportedLocaleNames;
}
