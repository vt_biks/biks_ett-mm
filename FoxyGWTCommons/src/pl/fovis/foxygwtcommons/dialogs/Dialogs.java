/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import com.google.gwt.user.client.ui.Label;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pmielanczuk
 */
public class Dialogs {

    public static void alertHtml(String text, String optCaption, IContinuation optCloseCont) {
        new SimpleInfoDialog().buildAndShowDialog(text, false, optCaption, null, optCloseCont);
    }

    public static void alert(String text, String optCaption, IContinuation optCloseCont) {
        new SimpleInfoDialog().buildAndShowDialog(text, true, optCaption, null, optCloseCont);
    }

    public static ConfirmDialog confirmHtml(String htmlTxt, String caption,
            String actionBtnCaption, String cancelBtnCaption,
            IParametrizedContinuation<Boolean> closeCont) {
        ConfirmDialog res = new ConfirmDialog();

        res.buildAndShowDialog(htmlTxt, caption, actionBtnCaption, cancelBtnCaption,
                closeCont);

        return res;
    }

    public static ConfirmDialog confirm(String text, String caption,
            String actionBtnCaption, String cancelBtnCaption,
            IParametrizedContinuation<Boolean> closeCont) {

        ConfirmDialog res = new ConfirmDialog();

        res.buildAndShowDialog(new Label(text), caption, actionBtnCaption, cancelBtnCaption,
                closeCont);

        return res;
    }

    public static void prompt(String name, String initValue, IParametrizedContinuation<String> saveCont) {
        new OneTextBoxDialog().buildAndShowDialog(name, initValue, saveCont);
    }
}
