/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.servlet;

import commonlib.ILameResourceProvider;
import pl.trzy0.foxy.serverlogic.ServletConfigObj;
import javax.servlet.http.HttpSession;
import pl.trzy0.foxy.serverlogic.ISessionEnvironment;

/**
 *
 * @author wezyr
 */
public class SessionEnvironment implements ISessionEnvironment {
    private HttpSession session;
    //private ServletConfigObj configObj;
    private ILameResourceProvider resourceProvider;

    public SessionEnvironment(HttpSession session/*, ServletConfigObj configObj*/) {
        this.session = session;
        //this.configObj = configObj;
        this.resourceProvider = new ServletResouceProvider(session.getServletContext());
    }

    public String getCaptchaText() {
        return (String) session.getAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
    }

//    public String getHttpBaseAddr() {
//        return configObj.httpBaseAddr;
//    }

    public ILameResourceProvider getResourceProvider() {
        return resourceProvider;
    }
}
