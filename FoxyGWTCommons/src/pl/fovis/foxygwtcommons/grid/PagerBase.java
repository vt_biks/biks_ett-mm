/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.grid;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.trzy0.foxy.commons.FoxyCommonConsts;
import simplelib.IContinuation;

/**
 *
 * @author ctran
 */
public abstract class PagerBase {

    protected HorizontalPanel hp = new HorizontalPanel();
    protected BikCustomButton firstPageBtn;
    protected BikCustomButton prevPageBtn;
    protected BikCustomButton nextPageBtn;
    protected BikCustomButton lastPageBtn;
    protected Label pageInfoLbl = new Label();

    public PagerBase() {
        pageInfoLbl.setWordWrap(false);
        hp.add(firstPageBtn = new BikCustomButton("|" + FoxyCommonConsts.LAQUO_STR, "labelLinkWWBeanGridPaper", new IContinuation() {

            @Override
            public void doIt() {
                navigate(0);
            }
        }));
        hp.add(prevPageBtn = new BikCustomButton(FoxyCommonConsts.LAQUO_STR, "labelLinkWWBeanGridPaper", new IContinuation() {

            @Override
            public void doIt() {
                navigate(getPageNum() - 1);
            }
        }));

        hp.add(pageInfoLbl);

        hp.add(nextPageBtn = new BikCustomButton(FoxyCommonConsts.RAQUO_STR, "labelLinkWWBeanGridPaper", new IContinuation() {

            @Override
            public void doIt() {
                navigate(getPageNum() + 1);
            }
        }));
        hp.add(lastPageBtn = new BikCustomButton(FoxyCommonConsts.RAQUO_STR + "|", "labelLinkWWBeanGridPaper", new IContinuation() {

            @Override
            public void doIt() {
                navigate(getMaxPageNum());
            }
        }));

    }

    public void add(Widget widget) {
        hp.add(widget);
    }

    public void setSpacing(int space) {
        hp.setSpacing(space);
    }

    public abstract void navigate(int pageNum);

    public abstract int getPageNum();

    public abstract int getMaxPageNum();

    public BikCustomButton getNextPageBtn() {
        return nextPageBtn;
    }
}
