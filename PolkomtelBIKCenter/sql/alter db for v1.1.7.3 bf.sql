﻿exec sp_check_version '1.1.7.3';
go

create table bik_object_in_fvs_change (
id int identity(1,1) NOT NULL primary key,
node_id int references bik_node (id) not null,
fvs_id int references bik_object_in_fvs (id) not null,
action_type int not null 
);

exec sp_update_version '1.1.7.3', '1.1.7.4';
go
