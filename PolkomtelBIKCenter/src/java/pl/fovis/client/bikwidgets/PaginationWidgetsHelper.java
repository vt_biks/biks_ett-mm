/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.IContinuation;

/**
 *
 * @author pmielanczuk
 */
public abstract class PaginationWidgetsHelper {

    public int pageSize = 20;
    public int pageNum = 0;
    private int pageCnt;
    public HorizontalPanel paginationHp = new HorizontalPanel();
    private final BikCustomButton prevPageBtn = new BikCustomButton("«", "labelLinkWW", new IContinuation() {
        @Override
        public void doIt() {
            paginationNavigate(pageNum - 1);
        }
    });
    private final BikCustomButton nextPageBtn = new BikCustomButton("»", "labelLinkWW", new IContinuation() {
        @Override
        public void doIt() {
            paginationNavigate(pageNum + 1);
        }
    });
    private final Label pageNumOfCntLbl = new Label();

    public PaginationWidgetsHelper() {

    }

    public PaginationWidgetsHelper(int pageSize) {
        this.pageSize = pageSize;
    }

    public void init() {
        paginationHp.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
        paginationHp.add(prevPageBtn);
        paginationHp.add(pageNumOfCntLbl);
        paginationHp.add(nextPageBtn);
        paginationHp.setVisible(false);
    }

    public void refresh() {
        pageNumOfCntLbl.setText(
                I18n.stronaZ.format(pageNum + 1, pageCnt) //
        //I18n.strona.get() /* I18N:  */+ " " + (pageNum + 1) + " " +I18n.z_noSingleLetter.get() /* i18n:  */+ " " + pageCnt
        );
        prevPageBtn.setEnabled(pageNum > 0);
        nextPageBtn.setEnabled(pageNum < pageCnt - 1);
//        paginationHp.setVisible(inResCnt > 0);
        paginationHp.setVisible(pageCnt > 0);
    }

    protected void paginationNavigate(int newPageNum) {
        if (newPageNum >= pageCnt) {
            newPageNum = pageCnt - 1;
        }
        if (newPageNum < 0) {
            newPageNum = 0;
        }
        pageNum = newPageNum;
        pageNumChanged();
    }

    public void setPageCntByItemCnt(Integer itemCnt) {
        if (itemCnt == null) {
            itemCnt = 0;
        }
        pageCnt = (itemCnt + pageSize - 1) / pageSize;
    }

    protected abstract void pageNumChanged();

    public boolean isLastPage() {
        return pageNum == pageCnt - 1;
    }
}
