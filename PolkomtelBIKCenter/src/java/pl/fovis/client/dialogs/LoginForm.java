/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author pku
 */
public class LoginForm extends VerticalPanel {

    protected final static int TEXTBOX_WIDTH = 258;
    protected final static int DOMAIN_TB_WIDTH = 138;
    protected TextBox tbLogin;
    protected ListBox tbLoginDomain;
    protected TextBox tbPassword;
    protected Label status;
    protected Label lblLogin;
    protected Label lblPass;
    protected RadioButton btSSO;
    protected RadioButton btWithLogin;
    protected RadioButton btEnterprise;
//    protected HTML lblClassicLogon;
    protected InlineHTML lblSSOLogon;
//    protected InlineHTML htmlLogBySSO;
    protected FlowPanel fpSSOPanel;
    //private final IContinuation afterProperLoginCont;
    private final IParametrizedContinuation<Boolean> loggingResultCont;
    private boolean addEnterPressedInTextBoxesHandler;

    public LoginForm(/*IContinuation afterProperLoginCont,*/IParametrizedContinuation<Boolean> hideForm) {
        //this.afterProperLoginCont = afterProperLoginCont;
        this.loggingResultCont = hideForm;
    }

    public void addEnterPressedInTextBoxesHandler() {
        this.addEnterPressedInTextBoxesHandler = true;
    }

    public void buildMainWidgets() {
        VerticalPanel mainContainer = this;

        status = new Label();
        status.setStyleName("status" /* I18N: no */);
        mainContainer.add(status);

        if (BIKClientSingletons.userExistButDisabled()) {
//            DisabledUserApplyForAccountMsgsBean duafamb = BIKClientSingletons.getDisabledUserApplyForAccountMsgs();
//
//            if (!BaseUtils.isStrEmptyOrWhiteSpace(duafamb.mailTo)) {
//                mainContainer = new VerticalPanel();
////                add(generateApplyForAccountPart());
//                add(new ApplyForExtendedAccessFormPanel());
//                mainContainer.add(new HTML("<br/><HR />"));
//                add(mainContainer);
//            } else {
//                status.setText(I18n.kontoUzytkownikaJestWylaczone.get() /* I18N:  */);
//            }
            status.setText(I18n.kontoUzytkownikaJestWylaczone.get() /* I18N:  */);
        }

        btSSO = new RadioButton("GR1", I18n.logowanieDomenoweSSO.get());
        btWithLogin = new RadioButton("GR1", I18n.logowanieDomenoweNaLogin.get());
        btEnterprise = new RadioButton("GR1", I18n.logowanieKlasyczne.get());
        btSSO.setStyleName("loginLbl");
        btWithLogin.setStyleName("loginLbl");
        btEnterprise.setStyleName("loginLbl");
        btSSO.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                activateSSO();
            }
        });
        btWithLogin.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                activateSSOWithLogin();
            }
        });
        btEnterprise.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                activateEnterprise();
            }
        });

        Grid grid = new Grid(2, 2);
        grid.setCellSpacing(4);
        grid.setWidget(0, 0, lblLogin = new Label((BIKClientSingletons.isMultiXMode() ? I18n.email.get() : I18n.login.get()) /* I18N:  */ + ":"));
        grid.setWidget(1, 0, lblPass = new Label(I18n.haslo.get() /* I18N:  */ + ":"));
        HorizontalPanel hp = new HorizontalPanel();
        tbLogin = new TextBox();
        tbLoginDomain = new ListBox();
        List<String> adDomains = BIKClientSingletons.getAdDomains();
        if (BaseUtils.isCollectionEmpty(adDomains)) {
            tbLoginDomain.addItem(I18n.domyslnaDomena.get());
        } else {
            for (String domain : adDomains) {
                tbLoginDomain.addItem(/*"@" + */domain);
            }
        }
        hp.add(tbLogin);
        hp.add(tbLoginDomain);
        grid.setWidget(0, 1, hp);
//        grid.setWidget(0, 1, tbLogin = new TextBox());
        grid.setWidget(1, 1, tbPassword = new PasswordTextBox());
        lblLogin.setStyleName("loginLbl");
        lblPass.setStyleName("loginLbl");
        tbLogin.setStyleName("loginAndPass");
        tbLoginDomain.setStyleName("loginAndPass loginListBox");
        tbPassword.setStyleName("loginAndPass");
        tbLogin.setWidth((TEXTBOX_WIDTH - DOMAIN_TB_WIDTH) + "px");
        tbLogin.setFocus(true);
        tbLoginDomain.setWidth(DOMAIN_TB_WIDTH + "px");
        tbPassword.setWidth(TEXTBOX_WIDTH + "px");

        if (BIKClientSingletons.mustShowSSOLoginDialog() && !BIKClientSingletons.isMultiXMode()) {
            Label welcome = new Label(I18n.wybierzSposobLogowania.get() + ":");
            welcome.setStyleName("NewLoginLbl");
            mainContainer.add(welcome);
            mainContainer.add(btSSO);
            mainContainer.add(btWithLogin);
            mainContainer.add(btEnterprise);
            btSSO.setValue(true);
            activateSSO();
            // ukrycie logowania na wybrany login domenowy
            if (BIKClientSingletons.disableSSOForLoginOnDialog()) {
                btWithLogin.setVisible(false);
            }
        } else {
            HTML lblClassicLogon = new HTML(I18n.zalogujSieKlasycznPodajacLoginIH.get() /* I18N:  */ + ":");
            lblClassicLogon.setStyleName("NewLoginLbl");
            mainContainer.add(lblClassicLogon);
            btSSO.setVisible(false);
            btWithLogin.setVisible(false);
            btEnterprise.setVisible(false);
            btEnterprise.setValue(true);
            activateEnterprise();
        }
        mainContainer.add(new HTML("<HR />"));
        mainContainer.add(grid);

        if (addEnterPressedInTextBoxesHandler) {
            KeyPressHandler kph = new KeyPressHandler() {
                @Override
                public void onKeyPress(KeyPressEvent event) {
                    boolean enterPressed = KeyCodes.KEY_ENTER == event.getNativeEvent().getKeyCode();
                    if (enterPressed) {
                        onLoginClick();
                    }
                }
            };

            tbLogin.addKeyPressHandler(kph);
            tbPassword.addKeyPressHandler(kph);
        }

//        addGUIForTimeZoneTests();
    }

//    private TextBox tbDateAndTime = new TextBox();
//
//    private void addGUIForTimeZoneTests() {
//        VerticalPanel mainContainer = this;
//        tbDateAndTime.setValue(SimpleDateUtils.toCanonicalStringWithMillis(new Date()));
//
//        mainContainer.add(tbDateAndTime);
//        Button b = new Button("Date and time fun ;-)", new ClickHandler() {
//
//            @Override
//            public void onClick(ClickEvent event) {
//                final Date d = SimpleDateUtils.parseFromCanonicalString(tbDateAndTime.getValue());
//
//                BIKClientSingletons.getFtsService().getDateDiagInfo(d, new StandardAsyncCallback<String>() {
//
//                    @Override
//                    public void onSuccess(String result) {
//                        Window.alert("Date on client: " + SimpleDateUtils.dateDiagInfo(d)
//                                + "\n\nDate on server: " + result);
//                    }
//                });
//            }
//        });
//
//        mainContainer.add(b);
//    }
    public void onLoginClick() {
        status.setText(null);

//        if (btSSO.getValue()) { // przez SSO
//            BIKClientSingletons.getService().performUserLoginBySSO(new StandardAsyncCallback<Void>() {
//                @Override
//                public void onSuccess(Void result) {
//                    checkLoginResultAndDoAfterCont(false);
//                }
//            });
//        } else if (btWithLogin.getValue()) { // przez login do wybranej domeny
//            BIKClientSingletons.performUserLoginBySSOLoginAndDomain(tbLogin.getText(), tbPassword.getText(), getSelectedDomain(), new IContinuation() {
//                @Override
//                public void doIt() {
//                    checkLoginResultAndDoAfterCont();
//                }
//            });
//        } else if (btEnterprise.getValue()) { // klasycznie
//            BIKClientSingletons.performUserLogin(tbLogin.getText(),
//                    tbPassword.getText(), new IContinuation() {
//                        @Override
//                        public void doIt() {
//                            checkLoginResultAndDoAfterCont();
//                        }
//                    });
//        } else {
//            status.setText(I18n.bladLogowaniaSprobujPonownie.get() /* I18N:  */);
//        }
        String login = null;
        String pwd = null;
        String domain = null;

        if (btSSO.getValue()) { // przez SSO
            // nic nie trzeba ustawiać - mają być same nulle
        } else { // domenowe SSO lub klasyczne
            login = tbLogin.getText();
            pwd = tbPassword.getText();

            if (btWithLogin.getValue()) { // przez login do wybranej domeny
                domain = getSelectedDomain();
            } else if (btEnterprise.getValue()) { // klasycznie
                // wszystko już poustawiane
            } else {
                status.setText(I18n.bladLogowaniaSprobujPonownie.get() /* I18N:  */);
                return;
            }
        }

        BIKClientSingletons.markToIgnoreDifferentUserInNextCall();

        BIKClientSingletons.getService().performUserLogin(login, pwd, domain, new StandardAsyncCallback<Pair<String, Boolean>>() {
            @Override
            public void onSuccess(Pair<String, Boolean> result) {
                String dbName = result != null ? result.v1 : null;
                boolean isUserEnabled = result != null && result.v2;
                checkLoginResultAndDoAfterCont(dbName, isUserEnabled);
            }
        });
    }

//    protected void checkLoginResultAndDoAfterCont() {
//        checkLoginResultAndDoAfterCont(true);
//    }
    protected void checkLoginResultAndDoAfterCont(String dbName, boolean isUserEnabled) {
        boolean isSucccessLoggingResult = dbName != null && isUserEnabled;
        if (isSucccessLoggingResult) {
            //afterProperLoginCont.doIt();
//            BIKClientSingletons.ignoreDifferentUserOnce = true;
            BIKClientSingletons.reloadWithSetDb(dbName);
        } else if (dbName != null && !isUserEnabled) {
            status.setText(I18n.kontoUzytkownikaJestWylaczone.get());
        } else {
            status.setText(I18n.bladLogowaniaSprobujPonownie.get());
        }
        loggingResultCont.doIt(isSucccessLoggingResult);
    }

    protected String getSelectedDomain() {
        String selectedDomain = tbLoginDomain.getItemText(tbLoginDomain.getSelectedIndex());
        if (selectedDomain.equals(I18n.domyslnaDomena.get())) {
            return null;
        }
        return selectedDomain;
    }

    protected void activateSSO() {
        tbLoginDomain.setVisible(false);
        tbLogin.setWidth(TEXTBOX_WIDTH + "px");
        tbLogin.setEnabled(false);
        tbPassword.setEnabled(false);
    }

    protected void activateSSOWithLogin() {
        tbLoginDomain.setVisible(true);
        tbLogin.setWidth((TEXTBOX_WIDTH - DOMAIN_TB_WIDTH) + "px");
        tbLogin.setEnabled(true);
        tbPassword.setEnabled(true);
    }

    protected void activateEnterprise() {
        tbLoginDomain.setVisible(false);
        tbLogin.setWidth(TEXTBOX_WIDTH + "px");
        tbLogin.setEnabled(true);
        tbPassword.setEnabled(true);
    }

    public FocusWidget getWidgetToFocus() {
        return tbLogin;
    }
}
