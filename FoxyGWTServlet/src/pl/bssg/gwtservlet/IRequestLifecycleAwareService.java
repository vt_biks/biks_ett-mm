/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.gwtservlet;

/**
 *
 * @author pmielanczuk
 */
public interface IRequestLifecycleAwareService {

    public void requestStarting(IServiceRequestContext ctx);

    public void requestFinishing(IServiceRequestContext ctx, Throwable optExceptionInServiceMethodCall);
}
