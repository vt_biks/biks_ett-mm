/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import commonlib.ILameResourceProvider;
import java.io.InputStream;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.servlet.ServletContext;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class ServletResouceProvider implements ILameResourceProvider {

    private ServletContext servletContext;

    public ServletResouceProvider(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public ServletContext getServletContext() {
        return servletContext;
    }

    protected void listFiles(String base, Set<String> res, Set<String> fileExts, boolean recurseSubDirs) {
        @SuppressWarnings("unchecked")
        Set<String> set = getServletContext().getResourcePaths(base);

        //System.out.println("**** base=" + base + ", ResourcePaths=" + set);

        if (set == null) {
            return;
        }
        for (String resName : set) {
            boolean isDir = resName.endsWith("/");
            String ext = isDir ? "/" : BaseUtils.extractFileExtFromFullPath(resName);
            boolean extOk = fileExts == null || fileExts.contains(ext);

            if (extOk && (isDir && fileExts != null || !isDir)) {
                res.add(resName);
            }

            if (isDir && recurseSubDirs) {
                listFiles(resName, res, fileExts, recurseSubDirs);
            }
        }
    }

    public Set<String> getResourceNames(String base, Set<String> fileExts, boolean recurseSubDirs) {
        Set<String> res = new LinkedHashSet<String>();

        listFiles(base, res, fileExts, recurseSubDirs);

        return res;
    }

    public InputStream gainResource(String resourceName) {
        return getServletContext().getResourceAsStream(resourceName);
    }

    public long getLastModifiedOfResource(String resourceName) {
        try {
            return getServletContext().getResource(resourceName).openConnection().getLastModified();
        } catch (Exception ex) {
            throw new LameRuntimeException("error for resource: " + resourceName, ex);
        }
    }
}
