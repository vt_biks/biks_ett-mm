/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author tflorczak
 */
public class UserObjectBean implements Serializable {

    public UserBean user;
    public List<MyObjectsBean> objects;

    public UserObjectBean() {
    }

    public UserObjectBean(UserBean user, List<MyObjectsBean> objects) {
        this.user = user;
        this.objects = objects;
    }

}
