/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.VerticalPanel;
import pl.fovis.client.bikpages.IMyBIKSPage.ReachableMyBIKSTabCode;

/**
 *
 * @author beata
 */
public interface IMyBIKSTab {

    public String getCaption();

    public String getButtonStyle();

    public void show(VerticalPanel vp);

    public String getHelpCaption();

    public void setMyBIKSPage(IMyBIKSPage myBIKSPage);

    public ReachableMyBIKSTabCode getOptTabCode();

    public void updateHasNewItems(boolean hasNewItems);

    public boolean isVisible();
}
