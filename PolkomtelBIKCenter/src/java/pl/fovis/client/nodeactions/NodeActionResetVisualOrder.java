/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.Collection;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;

/**
 *
 * @author pmielanczuk
 */
public class NodeActionResetVisualOrder extends NodeActionUpdatingBase {

    @Override
    public boolean isEnabledByDesign() {
        if (BIKClientSingletons.hideActionMoveInTree()
                && !(BIKConstants.TREE_KIND_META_BIKS.equals(node.treeKind) || BIKConstants.TREE_KIND_META_BIKS_MENU_CONF.equals(node.treeKind))) {
            return false;
        }
        return isNodeNotLinkedUnderRootNorReadOnlyNode() && areSiblingsCustomSorted();
    }

    @Override
    public String getCaption() {
        return I18n.sortujAlfabetycznie.get() /* I18N:  */;
    }

    @Override
    public void execute() {

        getService().resetVisualOrderForNodes(node.parentNodeId, node.treeId,
                getAntiTreeRefreshingCallback(I18n.kolejnoscZmieniona.get() /* I18N:  */));

        final ITreeRowAndBeanStorage<Integer, Map<String, Object>, TreeNodeBean> storage = ctx.getTreeStorage();

        Collection<Map<String, Object>> siblingRows = storage.getChildRows(storage.getRowById(node.parentNodeId));
        for (Map<String, Object> row : siblingRows) {
            TreeNodeBean tnb = storage.getBeanById(storage.getTreeBroker().getNodeId(row));
            tnb.visualOrder = 0;
        }

        sortSiblingsWithDefaultOrder();
    }
}
