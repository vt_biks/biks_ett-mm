/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author bfechner
 */
public class AttrSelectOneJoinedObject extends AttrWigetBase {

    protected List<RadioButton> rb = new ArrayList<RadioButton>();
    protected String atrLinValue;
    protected List<String> attrOpt;
    protected boolean isEditAttr;
    protected String valueOpt;
    protected Map<String, String> nodePath2NodeLink = new HashMap<String, String>();

    public AttrSelectOneJoinedObject(String atrLinValue, String valueOpt, boolean isEditAttr) {
        this.atrLinValue = atrLinValue;
        this.valueOpt = valueOpt;
        this.isEditAttr = isEditAttr;
    }

    @Override
    public Widget getWidget() {
        attrOpt = new ArrayList<String>();

        attrOpt.add(I18n.nieDotyczy.get());
        attrOpt.addAll(BaseUtils.splitBySep(valueOpt, ";"));
        for (int i = 0; i < attrOpt.size(); i++) {
            String ao = attrOpt.get(i);
            Integer splitIndex = ao.indexOf(",");
            String nodeIdLink = ao.substring(0, splitIndex);
            String nodePath = ao.substring(splitIndex + 1);
            nodePath2NodeLink.put(nodePath, nodeIdLink);
            attrOpt.set(i, nodePath);
        }

        rb.clear();
        VerticalPanel vp = new VerticalPanel();
        ScrollPanel scollPanel = createScrolledPanel(vp, attrOpt.size());
        for (String ao : attrOpt) {
            RadioButton radioButton = new RadioButton("atr" /* I18N: no */, ao);
            vp.add(radioButton);
            if (isEditAttr && ao.equals(atrLinValue)) {
                radioButton.setValue(true);
            }
            rb.add(radioButton);
        }
        return scollPanel;
    }

    @Override
    public String getValue() {
        String attrDescription = "";
        for (RadioButton r : rb) {
            if (r.getValue()) {
                attrDescription = r.getHTML();
            }
        }
        if (!BaseUtils.isStrEmptyOrWhiteSpace(attrDescription)) {
            attrDescription = (nodePath2NodeLink.containsKey(attrDescription) ? nodePath2NodeLink.get(attrDescription) + "," : "") + attrDescription;
        }
        return attrDescription;
    }

    @Override
    public int getWidgetHeight() {
        return 0;
    }

}
