﻿exec sp_check_version '1.1.8.4';
go

create table bik_tutorial_readed_user(
	user_id int not null references bik_system_user(id),
	tutorial_id int not null references bik_tutorial(id),
	constraint UQ_user_id_tutorial_id unique(user_id, tutorial_id)
)
go

exec sp_update_version '1.1.8.4', '1.1.8.5';
go