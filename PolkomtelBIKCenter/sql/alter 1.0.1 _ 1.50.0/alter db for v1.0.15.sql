exec sp_check_version '1.0.15';
go

----------------------------------------------------------------------
----------------------------------------------------------------------
/*Aby co� si� pokazywa�o w bazie wiedzy*/

update bik_node
set is_deleted = 1
from bik_node join bik_node n2 on bik_node.linked_node_id = n2.id
where bik_node.is_deleted = 0 and n2.is_deleted = 1

----------------------------------------------------------------------
----------------------------------------------------------------------

exec sp_update_version '1.0.15', '1.0.16';
go
