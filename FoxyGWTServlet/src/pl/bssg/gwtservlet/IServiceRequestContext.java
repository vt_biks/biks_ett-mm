/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.gwtservlet;

import java.lang.reflect.Method;

/**
 *
 * @author pmielanczuk
 */
public interface IServiceRequestContext {

    public String getContextParamValue(String paramName);

    public String getServiceMethodName();

    public Method getServiceImplMethod();

    public Method getServiceIntfMethod();
}
