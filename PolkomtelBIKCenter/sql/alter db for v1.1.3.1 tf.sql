﻿exec sp_check_version '1.1.3.1';
go

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

create table aaa_universe_delta(
		si_id int, 
		name varchar(max),
		folderCUID varchar(max),
		to_delete int,
		to_load int
);
go


alter table aaa_universe
add update_ts datetime null;
go

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

exec sp_update_version '1.1.3.1', '1.1.4';
go
