/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddSystemUserGroupDialog;
import pl.fovis.client.dialogs.EditDescriptionDialog;
import pl.fovis.common.NameDescBean;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import pl.fovis.foxygwtcommons.popupmenu.PopupMenuDisplayer;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author Eugeniusz
 */
public class UserRolePopUpMenu {

    protected PopupMenuDisplayer popMenu = new PopupMenuDisplayer();
    protected int clientX;
    protected int clientY;
    protected SystemUserGroupBean sub;
    protected IContinuation optDoAfterDataFetched;

    public UserRolePopUpMenu(SystemUserGroupBean sub, int clientX, int clientY, IContinuation optDoAfterDataFetched) {
        this.sub = sub;
        this.clientX = clientX;
        this.clientY = clientY;
        this.optDoAfterDataFetched = optDoAfterDataFetched;
    }

    public void createAndShowPopup() {
        addAction();
        popMenu.showAtPosition(clientX, clientY);
    }

//    @Override
    public void addAction() {
        popMenu.addMenuItem(I18n.zmienNazwe.get(), renameUserGroup());
        popMenu.addMenuItem(I18n.edytujOpis.get(), editDescription());
        addSeparator();
        popMenu.addMenuItem(I18n.usun.get(), deleteUserGroup());
    }

    public void addSeparator() {
        popMenu.addMenuSeparator();
    }

    protected IContinuation renameUserGroup() {
        return new IContinuation() {

            @Override
            public void doIt() {
                new AddSystemUserGroupDialog().buildAndShowDialog(sub.name, I18n.zmienNazwe.get(), new IParametrizedContinuation<String>() {

                    @Override
                    public void doIt(String param) {
                        sub.name = param;

                        BIKClientSingletons.getService().renameUserGroup(sub, new StandardAsyncCallback<Integer>() {

                            @Override
                            public void onSuccess(Integer result) {
                                BIKClientSingletons.showInfo(result == 1 ? I18n.nazwaZostalaZmieniona.get() : I18n.rolaIstnieje.get() /* I18N:  */);
                                optDoAfterDataFetched.doIt();
                            }
                        });

                    }
                });
            }
        };
    }

    protected IContinuation deleteUserGroup() {
        return new IContinuation() {

            @Override
            public void doIt() {

                StringBuilder sb = new StringBuilder(I18n.czyNaPewnoUsunac.get());
                sb.append(" ").append(sub.name).append(" ").append("?");
                new ConfirmDialog().buildAndShowDialog(sb.toString(), new IContinuation() {
                    @SuppressWarnings("unchecked")
                    public void doIt() {
                        BIKClientSingletons.getService().deleteUserGroup(sub.id, new StandardAsyncCallback() {
//                        BIKClientSingletons.getService().deleteUserGroup(sub.id, new StandardAsyncCallback() {

                            @Override
                            public void onSuccess(Object result) {
                                BIKClientSingletons.showInfo(I18n.rolaUsunieta.get());
                                optDoAfterDataFetched.doIt();
                            }
                        });
                    }
                });

            }
        };
    }

    protected IContinuation addUserGroup() {
        return new IContinuation() {

            @Override
            public void doIt() {
                new AddSystemUserGroupDialog().buildAndShowDialog(null, I18n.dodajRole.get(), new IParametrizedContinuation<String>() {

                    @Override
                    public void doIt(String param) {
                        sub.name = param;

                        BIKClientSingletons.getService().addSystemUserGroup(param, sub.id, new StandardAsyncCallback<Integer>() {

                            @Override
                            public void onSuccess(Integer result) {
                                BIKClientSingletons.showInfo(result == 1 ? I18n.rolaDodana.get() : I18n.rolaIstnieje.get() /* I18N:  */);
                                optDoAfterDataFetched.doIt();
                            }
                        });

                    }
                });
            }
        };
    }

    protected IContinuation editDescription() {
        return new IContinuation() {

            @Override
            public void doIt() {
                BIKClientSingletons.getService().getRoleDescr(sub.id, new StandardAsyncCallback<String>("Error!") {

                    @Override
                    public void onSuccess(String result) {
                        final NameDescBean nameDesc = new NameDescBean();
                        nameDesc.content = result;
                        nameDesc.subject = sub.name;
                        new EditDescriptionDialog().buildAndShowDialog(nameDesc, new IParametrizedContinuation<NameDescBean>() {

                            @Override
                            public void doIt(NameDescBean param) {
                                BIKClientSingletons.getService().updateRoleDescription(param, sub.id, new StandardAsyncCallback<Void>("Failure on" /* I18N: no */ + " updateRoleDescription") {

                                            @Override
                                            public void onSuccess(Void result) {
                                                BIKClientSingletons.showInfo(I18n.zedytowanoOpis.get() /* I18N:  */);
                                                optDoAfterDataFetched.doIt();
                                            }
                                        });
                            }
                        });
                    }
                });

            }
        };
    }
}
