﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- Poprawki do konektora MSSQL
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_check_version '1.6.y2';
go

if not exists(select 1 from bik_icon where name = 'mssqlNonClustredIndex')
begin
	insert into bik_icon (name)
	values('mssqlNonClustredIndex')
	insert into bik_icon (name)
	values('mssqlClustredIndex')
	insert into bik_icon (name)
	values('mssqlColumnFK')
	insert into bik_icon (name)
	values('mssqlColumnPK')
	insert into bik_icon (name)
	values('mssqlFolder')
	insert into bik_icon (name)
	values('mssqlTableFunction')
	insert into bik_icon (name)
	values('confluenceBlog')
	insert into bik_icon (name)
	values('confluenceFolder')
	insert into bik_icon (name)
	values('confluencePage')
	insert into bik_icon (name)
	values('confluenceSpace')
end;
go

if not exists(select 1 from bik_admin where param = 'mssql.exPropDatabase')
begin
	insert into bik_admin (param, value)
	values ('mssql.exPropDatabase','')

	insert into bik_admin (param, value)
	values ('mssql.exPropTable','')

	insert into bik_admin (param, value)
	values ('mssql.exPropView','')

	insert into bik_admin (param, value)
	values ('mssql.exPropProcedure','')

	insert into bik_admin (param, value)
	values ('mssql.exPropScalarFunction','')

	insert into bik_admin (param, value)
	values ('mssql.exPropTableFunction','')

	insert into bik_admin (param, value)
	values ('mssql.exPropColumn','')
end;
go

alter table aaa_mssql_extended_properties alter column type varchar(50) not null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_mssql_extended_properties') and name = 'branch_names')
begin
	alter table aaa_mssql_extended_properties add branch_names varchar(512) null;
end;
go

if exists(select * from sys.objects obj where obj.parent_object_id = OBJECT_ID('bik_mssql_extended_properties') and obj.type = ('UQ'))
begin
	declare @constName varchar(512);
	select @constName = obj.name from sys.objects obj where obj.parent_object_id = OBJECT_ID('bik_mssql_extended_properties') and obj.type = ('UQ')
	
	exec('alter table bik_mssql_extended_properties drop constraint ' + @constName);
end;
go

alter table bik_mssql_extended_properties add constraint UQ_bik_mssql_extended_properties_node_id_name UNIQUE (node_id, name);

if not exists(select 1 from bik_node where name = 'Autor obiektu' and is_built_in = 1 and tree_id = (select id from bik_tree where code = 'UserRoles'))
begin
	declare @node_kind_id int;
	declare @tree_id int;
	declare @add_id int;
	declare @brfn_id int;
	
	select @tree_id = id, @node_kind_id = node_kind_id from bik_tree where code = 'UserRoles'
	
    insert into bik_node( name, node_kind_id, tree_id, is_built_in) values ('Autor obiektu', @node_kind_id, @tree_id, 1);
	select @add_id = scope_identity()
	insert into bik_role_for_node(caption, node_id, code) values('Autor obiektu', @add_id, 'AuthorOfObject');
	select @brfn_id = scope_identity()
	update bik_node set branch_ids = convert(varchar(20),@add_id)+'|', obj_id = @brfn_id where id = @add_id;
end;
go

exec sp_update_version '1.6.y2', '1.6.y2.1';
go