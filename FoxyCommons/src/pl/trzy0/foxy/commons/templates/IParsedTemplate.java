/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.templates;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public interface IParsedTemplate {

    public void render(StringBuilder sb, Map<String, Object> vars, Object model);
}
