/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

/**
 *
 * @author mmastalerz
 */
public interface IGridItemsProvider<T> {

    public void refetchItems();

    public void itemsChanged();
}
