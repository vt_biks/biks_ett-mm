/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.TreeSelectorForExpliciteTreeCodesDialog;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class TreeSelectorForDQMLeader extends TreeSelectorForExpliciteTreeCodesDialog {

    @Override
    protected boolean isInheritToDescendantsCheckBoxVisible() {
        return false;
    }

    @Override
    protected boolean isActionButtonEnabled() {
        return selectedNode != null && selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_USER);
    }

//    @Override
//    protected BikNodeTreeOptMode getTreeOptExtraMode() {
//        return BikNodeTreeOptMode.DQMDBFunction;
//    }
    public void buildAndShowDialog(Integer thisNodeId, IParametrizedContinuation<TreeNodeBean> saveCont) {
        super.buildAndShowDialog(thisNodeId, BaseUtils.paramsAsSet(BIKConstants.TREE_CODE_USER_ROLES), saveCont, null);
    }

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzUzytkownika.get() /* I18N:  */;
    }

    @Override
    protected boolean isHeaderVisible() {
        return false;
    }

    @Override
    protected boolean mustHideLinkedNodes() {
        return false;
    }
}
