﻿exec sp_check_version '1.0.47';

alter table bik_authors 
add is_admin int default 0
go
    
   
--------------------------------------

-- select * from bik_authors

update bik_authors 
set is_admin = 1
from (select node_id, min(user_id) as user_id from bik_authors group by node_id) x
where bik_authors.node_id = x.node_id and bik_authors.user_id = x.user_id


/*
declare @node_id int;
declare contact_cursor CURSOR FOR
select node_id from bik_authors order by node_id

OPEN contact_cursor;

FETCH NEXT FROM contact_cursor
INTO @node_id;

-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
WHILE @@FETCH_STATUS = 0
BEGIN
	 update bik_authors set is_admin=1 where node_id=@node_id and user_id=(select min(user_id) from bik_authors where node_id=@node_id)
   
   FETCH NEXT FROM contact_cursor
   INTO @node_id;
END

CLOSE contact_cursor;
DEALLOCATE contact_cursor;
GO
*/


go
exec sp_update_version '1.0.47', '1.0.48';
go
