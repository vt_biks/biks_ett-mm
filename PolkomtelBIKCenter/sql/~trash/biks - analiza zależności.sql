IF OBJECT_ID('tempdb..#xxx0') IS NOT NULL 
drop table #xxx0
go

declare @rapTreeId int = (select id from bik_tree where code = 'Reports')
declare @rapWebiKindId int = (select dbo.fn_node_kind_id_by_code('Webi'))
declare @qryKindId int = (select dbo.fn_node_kind_id_by_code('ReportQuery'))
declare @objMeasureKindId int = (select dbo.fn_node_kind_id_by_code('Measure')) 
declare @objDimensionKindId int = (select dbo.fn_node_kind_id_by_code('Dimension')) 
declare @objDetailKindId int = (select dbo.fn_node_kind_id_by_code('Detail')) 
declare @univTreeId int = (select id from bik_tree where code = 'ObjectUniverses')
declare @univKindId int = (select dbo.fn_node_kind_id_by_code('Universe'))
declare @connKindId int = (select dbo.fn_node_kind_id_by_code('DataConnection'))
declare @connTreeId int = (select id from bik_tree where code = 'Connections')
declare @derivedTableKindId int = (select dbo.fn_node_kind_id_by_code('UniverseDerivedTable')) 
declare @aliasTableKindId int = (select dbo.fn_node_kind_id_by_code('UniverseAliasTable')) 
declare @tableKindId int = (select dbo.fn_node_kind_id_by_code('UniverseTable')) 

select
		--/*
		rap.name as report_name,
		qry.name as qry_name,
		qryText.sql_text as sql_text,
		obj.name as obj_name,
		univ.name as univ_name,
		conn.name as conn_name,
		tab.name as tab_name,
		tab.node_kind_id as node_kind_id,
		case when tab.node_kind_id = @derivedTableKindId then 'UniverseDerivedTable'
				when tab.node_kind_id =@aliasTableKindId then 'UniverseAliasTable'
				else 'UniverseTable'end as tab_kind,
		cast('' as varchar(max)) as rap_parent_path, 
		rap.parent_node_id as rap_ancestor_id,
		cast('' as varchar(max)) as univ_parent_path, 
		univ.parent_node_id as univ_ancestor_id,
		tab.id
		--,		bsut.sql_of_derived_table
		--*/   count(*)
 -- *
into #xxx0
from
	-- raporty -> zapytania dla raport�w
		bik_node rap inner join bik_node qry on qry.parent_node_id = rap.id 
		-- dodatkowe info dla zapytania - tre�� SQL
		inner join bik_sapbo_query qryText on qry.id = qryText.node_id
		-- obiekty u�yte w zapytaniu na raporcie (miary, wymiary, detale)
		inner join bik_node objlinked on objlinked.parent_node_id = qry.id 
		-- dok�adna liczba rekord�w: 149247 do poziomu objlinked
		-------- 150 000 rekord�w, czas dzia�ania 3 sekundy z wy�wietlaniem bez tekstu zapytania (tylko rap.name i qry.name)
		-------- wrzucanie tego do tabelki tymczasowej (z sql_text i objlinked.name) trwa 17 sekund
		-------- wrzucanie tego do tabelki tymczasowej (z objlinked.name ale bez sql_text) trwa poni�ej 1 sekundy!!!
		-- idziemy do orygina�u dla miar, wymiar�w itp. -> czyli do obiekt�w ze �wiata obiekt�w
		inner join bik_node obj on objlinked.linked_node_id = obj.id 
		-- tu bierzemy kind dla obiekt�w �wiata obiekt�w
		--inner join bik_node_kind objkind on obj.node_kind_id = objkind.id
        -- z czym si� ��czy zapytanie
		inner join bik_joined_objs qryjoins on qry.id = qryjoins.src_node_id
		-- ��czymy zapytanie ze �wiatem obiekt�w
		inner join bik_node univ on qryjoins.dst_node_id = univ.id
		-- kind dla �wiata obiekt�w
		--inner join bik_node_kind univkind on univkind.id = univ.node_kind_id
		-- z raportu idziemy do po��czenia
		---------------- ^^^^^^ 149247 ^^^^^^ ------------ vvvvvv 149275 vvvvvv ------------
		left join ( bik_joined_objs connjoins
		-- dok�adna warto��: 149275 rekord�w
		-- node po��czenia dla raportu
		inner join bik_node conn on conn.id = connjoins.dst_node_id ) on qry.id = connjoins.src_node_id and conn.is_deleted = 0 and conn.linked_node_id is null and conn.tree_id = @connTreeId and conn.node_kind_id = @connKindId
		-- kind dla node'a po��czenia dla raportu
		--inner join bik_node_kind connkind on connkind.id = conn.node_kind_id
		-- teraz z miary / wymiaru itp. idziemy do tabelki
		left join (bik_joined_objs tabjoins
		-- node tabelki
		inner join bik_node tab on tab.id = tabjoins.dst_node_id) on tabjoins.src_node_id = obj.id and tab.is_deleted = 0 and tab.linked_node_id is null and tab.tree_id = @univTreeId and tab.node_kind_id in (@derivedTableKindId, @aliasTableKindId, @tableKindId)
		/*
		left join bik_sapbo_universe_table bsut on bsut.node_id = tab.id  and tab.node_kind_id = @derivedTableKindId
        left join (bik_joined_objs tabjoins_orig        
		-- node tabelki
		inner join bik_node tab_orig on tab_orig.id = tabjoins_orig.dst_node_id) on tabjoins_orig.src_node_id = tab.id and tab_orig.is_deleted = 0 and tab_orig.linked_node_id is null and tab_orig.tree_id = @univTreeId and tab_orig.node_kind_id = @tableKindId and tab.node_kind_id = @aliasTableKindId
*/
/*		
		-- kind dla tabelki
		--inner join bik_node_kind tabKind on tabKind.id = tab.node_kind_id
*/
where 
		rap.is_deleted = 0 and rap.linked_node_id is null and rap.tree_id = @rapTreeId and rap.node_kind_id = @rapWebiKindId
		and qry.is_deleted = 0 and qry.tree_id = @rapTreeId and qry.node_kind_id = @qryKindId
		and objlinked.is_deleted = 0 and objlinked.tree_id = @rapTreeId and objlinked.node_kind_id in (@objMeasureKindId, @objDimensionKindId, @objDetailKindId)
		and obj.is_deleted = 0 and obj.tree_id = @univTreeId and obj.linked_node_id is null and obj.node_kind_id in (@objMeasureKindId, @objDimensionKindId, @objDetailKindId)
		and univ.is_deleted = 0 and univ.linked_node_id is null and univ.tree_id = @univTreeId and univ.node_kind_id = @univKindId
go



IF OBJECT_ID('tempdb..#xxx') IS NOT NULL 
drop table #xxx
go


declare @univTreeId int = (select id from bik_tree where code = 'ObjectUniverses')
declare @univKindId int = (select dbo.fn_node_kind_id_by_code('Universe'))
declare @connKindId int = (select dbo.fn_node_kind_id_by_code('DataConnection'))
declare @derivedTableKindId int = (select dbo.fn_node_kind_id_by_code('UniverseDerivedTable')) 
declare @aliasTableKindId int = (select dbo.fn_node_kind_id_by_code('UniverseAliasTable')) 
declare @tableKindId int = (select dbo.fn_node_kind_id_by_code('UniverseTable')) 

select 
--/*
		report_name,
		qry_name,
		sql_text,
		obj_name,
		univ_name,
		conn_name,
		tab_name,
		tab_kind,
		rap_parent_path, 
		rap_ancestor_id,
		univ_parent_path, 
		univ_ancestor_id,
		bsut.sql_of_derived_table,
		tab_orig.name as tab_name_original_for_alias
		-- */ -- 				count(*)
--
into #xxx
from #xxx0 left join bik_sapbo_universe_table bsut 
on bsut.node_id = #xxx0.id  --and tab.node_kind_id = @derivedTableKindId
        left join (bik_joined_objs tabjoins_orig        
		inner join bik_node tab_orig on tab_orig.id = tabjoins_orig.dst_node_id) on tabjoins_orig.src_node_id = #xxx0.id and tab_orig.is_deleted = 0 and tab_orig.linked_node_id is null 
		and tab_orig.tree_id = @univTreeId and tab_orig.node_kind_id = @tableKindId and #xxx0.node_kind_id = @aliasTableKindId 
go


while 1 = 1 begin
  declare @rc int = null

  update #xxx
  set rap_parent_path = bn.name + '\' + rap_parent_path, rap_ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #xxx.rap_ancestor_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go


while 1 = 1 begin
  declare @rc int = null

  update #xxx
  set univ_parent_path = bn.name + '\' + univ_parent_path, univ_ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #xxx.univ_ancestor_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go


select * from #xxx
