/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.wezyrtreegrid;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.ITreeBuilder;
import pl.fovis.foxygwtcommons.ITreeLevelBuilder;
import pl.fovis.foxygwtcommons.RepeatingCommandFix;
import pl.fovis.foxygwtcommons.TreeAlgorithms;
import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeGrid;
import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeOrListGridRightClickHandler;
import pl.fovis.foxygwtcommons.treeandlist.ILazyRowLoaderEx;
import pl.fovis.foxygwtcommons.treeandlist.ILazyRowLoaderMultiExpand;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowStorage;
import pl.fovis.foxygwtcommons.wezyrtreegrid.IWezyrTreeGrid.AddRowMode;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.IParametrizedContinuationWithReturn;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class FoxyWezyrTreeGrid<IDT> implements IFoxyTreeGrid<IDT> {

    //public static final boolean USE_FASTER_TREE_GRID = true;
    protected ScrollPanel scrollPanel;
    protected IWezyrTreeGrid<Map<String, Object>> treeGrid;
    protected String idFieldName;
    protected String parentIdFieldName;
    protected String nameFieldName;
    protected String prettyKindFieldName;
    protected String nodeIconFieldName;
    protected String additionalNodeIconFieldName;
    protected Map<String, Integer> fieldWidths = new HashMap<String, Integer>();
    // name->caption
    protected Map<String, String> fields = new LinkedHashMap<String, String>();
    protected List<Pair<String, Integer>> fieldsWithWidths = new ArrayList<Pair<String, Integer>>();
    //protected Map<IDT, Map<String, Object>> idToRowMap;
    protected Map<String, Object> firstRow;
    protected ILazyRowLoaderEx<Map<String, Object>, IDT> optLazyLoader;
    protected Map<String, Object> rowToScrollIntoView;
    protected ITreeRowStorage<IDT, Map<String, Object>> storage;
    protected String firstColStyleName;

//    public FoxyWezyrTreeGrid() {
//    }
    public void defineTreeFields(String idFieldName,
            String parentIdFieldName,
            String nameFieldName,
            String prettyKindFieldName,
            String nodeIconFieldName,
            String additionalNodeIconFieldName) {
        this.idFieldName = idFieldName;
        this.parentIdFieldName = parentIdFieldName;
        this.nameFieldName = nameFieldName;
        this.prettyKindFieldName = prettyKindFieldName;
        this.nodeIconFieldName = nodeIconFieldName;
        this.additionalNodeIconFieldName = additionalNodeIconFieldName;
    }

    public void setFirstColStyleName(String styleName) {
        this.firstColStyleName = styleName;
    }

    public void selectRecord(IDT id, boolean expand) {
        Map<String, Object> row = storage.getRowById(id);
        rowToScrollIntoView = row;

        if (id != null && row == null && optLazyLoader != null) {
            optLazyLoader.loadAncestorsForSelection(id, expand, null);
            return;
        }

        treeGrid.selectRow(row, true);
        if (expand && row != null) {
            treeGrid.expandRow(row);
        }
    }

    @Override
    public Set<IDT> getExpandedRowIds() {
        Set<Map<String, Object>> rows = treeGrid.getExpandedRows();

        Set<IDT> res = new HashSet<IDT>();

        for (Map<String, Object> row : rows) {
            IDT id = getIdValOfRow(row);
            res.add(id);
        }

        return res;
    }

    @Override
    public void expandNodesByIds(final Collection<IDT> ids, final IParametrizedContinuation<Integer> optExpandInProgressCallback) {
        expandNodesByIds(ids, optExpandInProgressCallback, false);
    }

    @Override
    public void expandNodesByIds(final Collection<IDT> ids, final IParametrizedContinuation<Integer> optExpandInProgressCallback, final boolean allowDataLoading) {
        expandNodesByIdsInner(ids, optExpandInProgressCallback, allowDataLoading, 0);
    }

    @Override
    public boolean expandNodesByIdsWithDataLoading(final Collection<IDT> ids, IDT optIdToSelect, final IParametrizedContinuation<IDT> optCont) {
        if (!(optLazyLoader instanceof ILazyRowLoaderMultiExpand)) {
            return false;
        }

        @SuppressWarnings("unchecked")
        ILazyRowLoaderMultiExpand<IDT> lazyLoaderMulti = (ILazyRowLoaderMultiExpand<IDT>) optLazyLoader;

        lazyLoaderMulti.loadNodesAndAncestorsAndExpand(ids, optIdToSelect, optCont);

        return true;
    }

    protected void expandNodesByIdsInner(final Collection<IDT> ids, final IParametrizedContinuation<Integer> optExpandInProgressCallback,
            final boolean allowDataLoading, final int externalCnt) {

        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS, "starting for ids=" + ids + ", externalCnt=" + externalCnt);
        }

        if (ids.isEmpty()) {
            return;
        }

//        final int size = ids.size();
        RepeatingCommand rc = new RepeatingCommandFix() {
            Iterator<IDT> iter = ids.iterator();
            int cnt = externalCnt;

            @Override
            public boolean doExecute() {

                if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS)) {
                    ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS, "start of doExecute, externalCnt=" + externalCnt + ", cnt=" + cnt);
                }

                while (iter.hasNext()) {
                    final IDT id = iter.next();

                    Map<String, Object> row = storage.getRowById(id);

                    if (allowDataLoading && row == null) {

                        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS)) {
                            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS, "row is null for id=" + id + ", cnt=" + cnt);
                        }

                        optLazyLoader.loadAncestorsForSelection(id, true, new IParametrizedContinuation<IDT>() {

                            @Override
                            public void doIt(IDT param) {
                                if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS)) {
                                    ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS, "loaded ancestors for id=" + id + ", best node to select has id=" + param);
                                }

                                if (param != null) {
                                    Map<String, Object> row = storage.getRowById(param);
                                    if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS)) {
                                        ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS, "has row for id=" + id + ", row=" + row);
                                    }
                                    if (row != null) {
                                        treeGrid.expandRow(row);
                                    }
                                }
                                cnt++;
                                final Set<IDT> remainingIds = new LinkedHashSet<IDT>();
                                while (iter.hasNext()) {
                                    remainingIds.add(iter.next());
                                }
                                if (!remainingIds.isEmpty()) {
                                    if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS)) {
                                        ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS, "will run deferred for ids=" + remainingIds + ", cnt=" + cnt);
                                    }
                                    GWTUtils.runDeferred(new IContinuation() {

                                        @Override
                                        public void doIt() {
                                            expandNodesByIdsInner(remainingIds, optExpandInProgressCallback, allowDataLoading, cnt);
                                        }
                                    });
                                } else {
                                    if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS)) {
                                        ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS, "no more remaining ids");
                                    }
                                    if (optExpandInProgressCallback != null) {
                                        optExpandInProgressCallback.doIt(cnt);
                                    }
                                }
                            }
                        });

                        return false;
                    }

                    if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS)) {
                        ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS, "has row, will expand, id=" + id + ", cnt=" + cnt);
                    }

                    treeGrid.expandRow(row);

                    cnt++;

                    if (cnt % 20 == 0) {
                        break;
                    }
                }

                if (optExpandInProgressCallback != null) {
                    optExpandInProgressCallback.doIt(cnt);
                }

                boolean hasNext = iter.hasNext();

                if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS)) {
                    ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS, "end of doExecute, externalCnt=" + externalCnt + ", cnt=" + cnt + ", hasNext=" + hasNext);
                }

//
//                if (!hasNext) {
//                    BIKClientSingletons.showNotification("Filtrowanie zakończone");
//                } else {
//                    BIKClientSingletons.showNotification("Wyświetlam znalezione pozycje: " + (cnt * 100L / size) + "%");
//                }
                return hasNext;
            }
        };

        if (externalCnt % 20 == 0 && optExpandInProgressCallback != null) {
            optExpandInProgressCallback.doIt(externalCnt);
        }

        if (rc.execute()) {
            Scheduler.get().scheduleIncremental(rc);
        }

//        for (IDT id : ids) {
//            Map<String, Object> row = storage.getRowById(id);
//            treeGrid.expandRow(row);
//        }
    }
    public static final String DIAG_MSG_KIND$EXPAND_NODES_BY_IDS = "expandNodesByIds";

    public void setNodeName(IDT id, final String name) {
        final Map<String, Object> row = storage.getRowById(id);
        treeGrid.updateRow(row, new IContinuation() {
            public void doIt() {
                row.put(nameFieldName, name);
            }
        });
    }

    protected void internalAddNodeEx(Map<String, Object> row, Map<String, Object> prevRow, boolean isLeafForSure, AddRowMode mode) {
        if (firstRow == null) {
            firstRow = row;
        }

        treeGrid.addRow(row, prevRow, mode, isLeafForSure);
    }

    protected void internalAddNode(Map<String, Object> row, boolean isLeafForSure, AddRowMode mode) {
        internalAddNodeEx(row, storage.getRowById(getParentIdValOfRow(row)), isLeafForSure, mode);
    }

    public void addNode(Map<String, Object> row, Integer optIdxInSiblings) {

        if (optIdxInSiblings == null) {
            internalAddNode(row, true, AddRowMode.AsLastChild);
        } else {
            Map<String, Object> parentRow = storage.getRowById(getParentIdValOfRow(row));
            int childCnt = treeGrid.getRowChildCount(parentRow);
            if (optIdxInSiblings >= childCnt) {
                internalAddNode(row, true, AddRowMode.AsLastChild);
            } else if (optIdxInSiblings == 0) {
                internalAddNode(row, true, AddRowMode.AsFirstChild);
            } else {
                Map<String, Object> prevRow = treeGrid.getRowAtIndexInSiblings(parentRow, optIdxInSiblings - 1);

                internalAddNodeEx(row, prevRow, true, AddRowMode.AsSibling);
            }
        }
        //treeGrid.selectRow(row, true);
    }

    @Override
    public void deleteNode(IDT nodeId) {
        Map<String, Object> row = storage.getRowById(nodeId);
        treeGrid.removeRow(row);
        storage.deleteRowById(nodeId);
        //idToRowMap.remove(nodeId);
    }

    @Override
    public void markAsCutNode(IDT nodeId) {
        Map<String, Object> row = storage.getRowById(nodeId);
        treeGrid.markAsCutRow(row);
    }

    public void markAsUncutNode(IDT nodeId) {
        Map<String, Object> row = storage.getRowById(nodeId);
        treeGrid.markAsUncutRow(row);
    }

    public void pasteNode(IDT nodeId, IDT srcNodeId, boolean isCut) {
        Map<String, Object> row = storage.getRowById(nodeId);
        Map<String, Object> srcRow = storage.getRowById(srcNodeId);
        treeGrid.pasteRow(row, srcRow, isCut);
    }

    // "siName=Nazwa", "siKind=Rodzaj", "link=Przejdź", "icon=Ikonka"
    // name=width:caption
    // name=caption
    public void setFields(String... fieldDef) {
        for (int i = 0; i < fieldDef.length; i++) {
            Pair<String, String> p1 = BaseUtils.splitString(fieldDef[i], "=");
            Pair<String, String> p2 = BaseUtils.splitString(p1.v2, ":");
            if (p2.v2 != null) {
//                fields.put(p1.v1, p2.v2);
//                defineFieldWidth(p1.v1, new Integer(p2.v1));
                fieldsWithWidths.add(new Pair<String, Integer>(p1.v1, new Integer(p2.v1)));
            } else {
//                fields.put(p1.v1, p1.v2);
                fieldsWithWidths.add(new Pair<String, Integer>(p1.v1, null));
            }
        }
        //resetVisibleFieldNames();

        MapRowCellValBroker mrcvb = createMapRowCellValBroker(nodeIconFieldName, additionalNodeIconFieldName, prettyKindFieldName);

//        Set<String> fieldNames = fields.keySet();
//        switch (BIKClientSingletons.appPropsBean.treeGridSpeed) {
//            case 2:
        WezyrTreeGridFasterAndClean<Map<String, Object>> t = new WezyrTreeGridFasterAndClean<Map<String, Object>>(mrcvb, fieldsWithWidths);
        treeGrid = t;
        if (firstColStyleName != null) {
            t.setFirstColStyleName(firstColStyleName);
        }
//                break;
//            case 1:
//                treeGrid = new WezyrTreeGridFaster<Map<String, Object>>(mrcvb, fieldNames);
//                break;
//            default:
//                treeGrid = new WezyrTreeGridSlower<Map<String, Object>>(mrcvb, fieldNames);
//                break;
//        }

        treeGrid.addStyleName("wezyrTreeGrid");
    }

    // nie jest używane w TreeGridach w BIK Center
    public void defineButtonField(String fieldName, String upImage, String downImage, IParametrizedContinuation<Map<String, Object>> handler, IParametrizedContinuationWithReturn<Map<String, Object>, Boolean> stateHandler) {
        throw new UnsupportedOperationException("Not supported yet" /* I18N: no */ + ".");
    }

    // nie jest używane w TreeGridach w BIK Center
    public void defineIconField(String fieldName) {
        throw new UnsupportedOperationException("Not supported yet" /* I18N: no */ + ".");
    }

    public void defineFieldWidth(String fieldName, int width) {
        fieldWidths.put(fieldName, width);
    }

    public void refreshWidgetsState() {
        // nie jest EFEKTYWNIE używane w TreeGridach w BIK Center
        // no-op
    }

    @SuppressWarnings("unchecked")
    protected IDT getIdValOfRow(Map<String, Object> row) {
        return (IDT) row.get(idFieldName);
    }

    @SuppressWarnings("unchecked")
    protected IDT getParentIdValOfRow(Map<String, Object> row) {
        return (IDT) row.get(parentIdFieldName);
    }

    public void addChildNodes(Map<String, Object> parent, Collection<Map<String, Object>> rows) {
        //Window.alert("addChildNodes: cnt=" + rows.size());
        if (!BaseUtils.isCollectionEmpty(rows)) {
//            for (Map<String, Object> row : rows) {
//                internalAddNode(row, false, AddRowMode.AsLastChild);
//                //treeGrid.addRow(row, parent, AddRowMode.AsLastChild);
//            }
            treeGrid.addRows(rows, parent, AddRowMode.AsLastChild, false);
        } else {
            treeGrid.markNodeAsLeafForSure(parent);
        }
    }

    public void sortChildNodes(IDT nodeId, Comparator<Map<String, Object>> cmp) {
        treeGrid.sortChildRows(storage.getRowById(nodeId), cmp);
    }

    public boolean isNodeExpanded(IDT nodeId) {
        return treeGrid.isRowExpanded(storage.getRowById(nodeId));
    }

    public void addHeaderRow(Object... headerRowVals) {
        treeGrid.addHeaderRow(headerRowVals);
    }

    public void addFooterRow(Object... footerRowVals) {
        treeGrid.addFooterRow(footerRowVals);
    }

    public ITreeRowStorage<IDT, Map<String, Object>> getStorage() {
        return storage;
    }

    protected MapRowCellValBroker createMapRowCellValBroker(String nodeIconFieldName, String additionalNodeIconFieldName, String prettyKindFieldName) {
        return new MapRowCellValBroker(nodeIconFieldName, additionalNodeIconFieldName, prettyKindFieldName);
    }

    private class InternalTreeBuilder implements ITreeBuilder<Map<String, Object>, Map<String, Object>, IDT>,
            ITreeLevelBuilder<Map<String, Object>> {

        public Map<String, Object> createNode(Map<String, Object> n, Map<String, Object> p) {
//            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_FoxyWezyrTreeGrid)) {
//                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_FoxyWezyrTreeGrid, "createNode: p=" + (p == null ? "<null>" : p.get(nameFieldName))
//                        + ", n=" + n.get(nameFieldName));
//            }

            //treeGrid.addRow(n, p, AddRowMode.AsLastChild);
            internalAddNode(n, false, AddRowMode.AsLastChild);

            return n;
        }

        public IDT getNodeId(Map<String, Object> n) {
            return getIdValOfRow(n);
        }

        public IDT getParentId(Map<String, Object> n) {
            return getParentIdValOfRow(n);
        }

        public void createChildNodes(Collection<Map<String, Object>> ns, Map<String, Object> p) {
            if (firstRow == null && !ns.isEmpty()) {
                firstRow = ns.iterator().next();
            }

            treeGrid.addRows(ns, p, AddRowMode.AsLastChild, false);
        }
    }

    public void setData(//Collection<Map<String, Object>> rows,
            ITreeRowStorage<IDT, Map<String, Object>> storage,
            ILazyRowLoaderEx<Map<String, Object>, IDT> optLazyLoader) {
        Collection<Map<String, Object>> rows = storage.getAllRows();
        this.storage = storage;
        this.optLazyLoader = optLazyLoader;
        firstRow = null;
        treeGrid.clearAllRows();
        treeGrid.setLazyRowLoader(optLazyLoader);

//        if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_FoxyWezyrTreeGrid)) {
//            ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_FoxyWezyrTreeGrid, "setData:" + rows.toString());
//        }
        //idToRowMap = new HashMap<IDT, Map<String, Object>>();
        TreeAlgorithms.buildTree(new InternalTreeBuilder(), rows);

//        for (Map<String, Object> row : rows) {
//            idToRowMap.put(getIdValOfRow(row), row);
//        }
    }

    public void addSelectionChangedHandler(final IParametrizedContinuation<Map<String, Object>> handler) {
        treeGrid.addRowSelectionHandler(new IRowSelectionHandler<Map<String, Object>>() {
            public void rowSelected(Map<String, Object> row) {
                if (row == rowToScrollIntoView) {

                    final Map<String, Object> rowToScrollCache = row;
//                    if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_lazyLoad)) {
//                        ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_lazyLoad, "scrollPanel.ensureVisible before deferring");
//                    }

                    GWTUtils.runDeferred(new IContinuation() {
                        public void doIt() {
                            if (treeGrid.getSelectedRow() != rowToScrollCache) {
//                                if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_lazyLoad)) {
//                                    ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_lazyLoad, "scrollPanel.ensureVisible not scrolling, other is now selected");
//                                }
                                return;
                            }

//                            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_lazyLoad)) {
//                                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_lazyLoad, "scrollPanel.ensureVisible before");
//                            }
//                            final com.google.gwt.user.client.Element trElement = treeGrid.getSelectedRowElement();
                            final Element trElement = treeGrid.getSelectedRowElement();

                            UIObject tr = new UIObject() {
                                @SuppressWarnings("deprecation")
                                @Override
                                public com.google.gwt.user.client.Element getElement() {
                                    return trElement.cast();
                                }
                            };

                            scrollPanel.ensureVisible(tr);
//                            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_lazyLoad)) {
//                                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_lazyLoad, "scrollPanel.ensureVisible after");
//                            }
                        }
                    });
//                    if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$WW_lazyLoad)) {
//                        ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$WW_lazyLoad, "scrollPanel.ensureVisible after deferring");
//                    }
//                    scrollPanel.setWidth("400px");
                    setScrollPanelWidth();

                }
                rowToScrollIntoView = null;

                handler.doIt(row);
            }
        });
    }

    public void setScrollPanelWidth() {
        //no-op
    }

    public void addRightClickHandler(final IFoxyTreeOrListGridRightClickHandler<Map<String, Object>> handler) {
        treeGrid.addRightClickHandler(new IRightClickHandler<Map<String, Object>>() {
            public void rowRightClicked(Map<String, Object> row, int clientX, int clientY) {
                if (getIdOfSelectedRecord() == getIdValOfRow(row)) { // dane wczytane, pokazuję dialog
                    handler.doIt(row, clientX, clientY, true);
                } else { // doczytuję dane i dopiero pokazuję dialog
                    handler.doIt(row, clientX, clientY, false);
                    treeGrid.selectRow(row, true);
                }
            }
        });
    }

    public void addDoubleClickHandler(final IParametrizedContinuation<Map<String, Object>> handler) {
        treeGrid.addDoubleClickHandler(new IDoubleClickHandler<Map<String, Object>>() {
            public void rowDoubleClicked(Map<String, Object> row) {
                treeGrid.selectRow(row, true);
                // tf: po co to sprawdzenie? Rzutowanie ponadto jest nieprawidłowe (np. gdy IDT jest Stringiem)
//                int id = (Integer) row.get(idFieldName);
//                if (id >= 0) {
                handler.doIt(row);
//                }
            }
        });
    }

    public void addExpandCollapseHandler(final IParametrizedContinuation<Map<String, Object>> handler) {
        treeGrid.addExpandCollapseHandler(new IExpandCollapseHandler<Map<String, Object>>() {
            public void rowExpandedCollapsed(Map<String, Object> row) {
                handler.doIt(row);
            }
        });
    }

    @Override
    public void setEmptyMessage(String message) {
        // no-op
    }

    @Override
    public void selectRecord(IDT id) {
        selectRecord(id, false);
    }

    @Override
    public void selectFirstRecord() {
        if (firstRow != null) {
            treeGrid.selectRow(firstRow, true);
        } else {
            treeGrid.selectRow(null, false);
        }
    }

    @Override
    public Widget buildWidget() {
        scrollPanel = new ScrollPanel(treeGrid.asWidget());

        return scrollPanel;
    }

    public void setHeight(int value) {
        scrollPanel.setHeight(value + "px");
    }

    public Map<String, Object> getSelectedRecord() {
        return treeGrid.getSelectedRow();
    }

    public IDT getIdOfSelectedRecord() {
        Map<String, Object> selectedRow = getSelectedRecord();
        return selectedRow == null ? null : getIdValOfRow(selectedRow);
    }

    public String getIdFieldName() {
        return idFieldName;
    }

    public int getNodeChildCount(IDT nodeId) {
        return treeGrid.getRowChildCount(storage.getRowById(nodeId));
    }

    public int getNodeIndexInSiblings(IDT nodeId) {
        return treeGrid.getRowIndexInSiblings(storage.getRowById(nodeId));
    }

    public void setNodeIndexInSiblings(IDT nodeId, int idx) {
        treeGrid.setRowIndexInSiblings(storage.getRowById(nodeId), idx);
    }

    public IDT getNodeIdAtIndexInSiblings(IDT parentId, int idx) {
        return storage.getTreeBroker().getNodeId(treeGrid.getRowAtIndexInSiblings(storage.getRowById(parentId), idx));
    }

    public boolean hasUnloadedChildren(IDT nodeId) {
        return treeGrid.hasUnloadedChildren(storage.getRowById(nodeId));
    }

    public IDT getParentIdOfNode(IDT nodeId) {
        Map<String, Object> row = storage.getRowById(nodeId);
        return row != null ? getParentIdValOfRow(row) : null;
    }
}
