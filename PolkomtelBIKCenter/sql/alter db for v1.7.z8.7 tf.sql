﻿exec sp_check_version '1.7.z8.7';
go

if not exists(select * from bik_admin where param = 'biadmin.serverBoId')
begin
	insert into bik_admin(param, value) values ('biadmin.serverBoId', '')
end;

exec sp_update_version '1.7.z8.7', '1.7.z8.8';
go