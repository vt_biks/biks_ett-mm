﻿exec sp_check_version '1.1.7.6';
go

if not exists(select * from bik_app_prop where name='hideDictionaryRootTab')
begin
	exec sp_set_app_prop 'hideDictionaryRootTab', 'false' -- hasła użytkownika do zasilania
end
go

exec sp_update_version '1.1.7.6', '1.1.7.7';
go
