/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.Window;
import pl.fovis.common.HelpBean;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author mmastalerz
 */
//ww->help mocny c&p poleciał ;-)... Misiu?
public class EditHelpDescriptionDialog extends BaseRichTextAndTextboxDialog {

    private IParametrizedContinuation<HelpBean> saveCont;
    private HelpBean hbRecord;

    protected void innerDoAction(String title, String body) {
        HelpBean hb = new HelpBean();
        hb.helpKey = hbRecord.helpKey;
        hb.caption = hbRecord.caption;
        hb.textHelp = body;
        saveCont.doIt(hb);
    }

    public void buildAndShowDialog(HelpBean hb, IParametrizedContinuation<HelpBean> saveCont) {
        this.saveCont = saveCont;
        this.hbRecord = hb;
        super.buildAndShowDialog(getHelpCaption(), hbRecord.textHelp != null ? hbRecord.textHelp : I18n.brakOpisu.get() /* I18N:  */ + ".",
                false);
    }

    @Override
    protected String getDialogCaption() {
        return I18n.edycja.get() /* I18N:  */ + ": " + BaseUtils.nullToDef(getHelpCaption(), "");
    }

    protected String getHelpCaption() {
        return hbRecord != null ? hbRecord.caption : null;
    }
}
