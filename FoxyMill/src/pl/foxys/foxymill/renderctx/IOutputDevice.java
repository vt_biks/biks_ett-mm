/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill.renderctx;

import pl.foxys.foxymill.tags.CacheTag;

/**
 *
 * @author wezyr
 */
public interface IOutputDevice {
    public void append(Object o);
    public void appendCacheTag(CacheTag ct, IRenderCtx renderCtx);
//    public int getLength();
//    public String substring(int start, int end);
}
