/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs.valuehandlers;

import com.google.gwt.user.client.ui.SuggestBox;
import pl.fovis.foxygwtcommons.dialogs.validators.FoxyGWTValidationException;
import simplelib.BaseUtils;

/**
 *
 * @author bfechner
 */
public class SuggestBoxValueHandler implements IWidgetValueHandler<SuggestBox> {

    public boolean isWidgetClassHandled(Object widget) {
        return widget instanceof SuggestBox;
    }

    public Object getValueFromWidget(SuggestBox widget, Class targetValueClass, GetValueExtraInfo extraInfo) throws FoxyGWTValidationException {
        return BaseUtils.emptyStrToDef(widget.getValue(), null);
    }

    public void setValueToWidget(SuggestBox widget, Object value, SetValueExtraInfo extraInfo) {
        widget.setValue(BaseUtils.safeToString(value));
    }
}
