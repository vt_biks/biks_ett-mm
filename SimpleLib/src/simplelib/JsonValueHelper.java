/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class JsonValueHelper {

    public static boolean getBool(Object o, boolean defVal) {
        Boolean b = getBoolean(o);
        return b == null ? defVal : b;
    }

    public static boolean getBool(Object o) {
        Boolean b = getBoolean(o);
        return b == null ? false : b;
    }

    public static Boolean getBoolean(Object o) {
        if (o == null) {
            return null;
        }

        if (o instanceof Boolean) {
            return (Boolean) o;
        } else {
            String s = BaseUtils.safeToString(o);
            if (s != null) {
                s = s.trim().toLowerCase();
            }

            if (BaseUtils.isStrEmpty(s) || s.equals("false") || s.equals("0")) {
                return false;
            }

            return s.equals("true") || getInt(BaseUtils.tryParseNumber(s)) != 0;
        }
    }

    public static long getLong(Object o) {
        if (o == null) {
            return 0;
        }
        if (o instanceof Long) {
            return (Long) o;
        }

        return getInt(o);
    }

    public static Date getDate(Object o) {
        return (Date) o;
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> getMap(Object o) {
        return (Map<String, Object>) o;
    }

    @SuppressWarnings("unchecked")
    public static MapHelper<String> getMapHelper(Object o) {
        Map<String, Object> m2 = (Map<String, Object>) getMap(o);
        return new MapHelper<String>(m2);
    }

    public static String getString(Object o) {
        return getString(o, null);
    }

    public static String getString(Object o, String defVal) {
        return o == null ? defVal : BaseUtils.safeToString(o);
    }

    public static Integer getInteger(Object o) {
        if (o == null) {
            return null;
        }

        Object newO = o;

        if (!(newO instanceof Integer || newO instanceof Double)) {
            newO = BaseUtils.tryParseNumber(BaseUtils.safeToString(newO));
        }

        if (newO instanceof Integer) {
            return (Integer) newO;
        }
        if (newO instanceof Double) {
            return ((Double) newO).intValue();
        }

        throw new LameRuntimeException("cannot parse number: val=" + StringEscapeUtils.escapeJavaScriptAndQuote(o));
    }

    public static int getInt(Object o) {
        return getInt(o, 0);
    }

    public static int getInt(Object o, int defVal) {
        Integer i = getInteger(o);
        return i == null ? defVal : i;
    }

    public static Double getDouble(Object o) {
        if (o == null) {
            return null;
        }

        Object newO = o;
        
        if (!(o instanceof Integer || o instanceof Double)) {
            newO = BaseUtils.tryParseNumber(BaseUtils.safeToString(o));
        }

        if (newO instanceof Integer) {
            return ((Integer) newO).doubleValue();
        }
        if (newO instanceof Double) {
            return (Double) newO;
        }

        throw new LameRuntimeException("cannot parse number (for double): val=" + StringEscapeUtils.escapeJavaScriptAndQuote(o));
    }

    @SuppressWarnings("unchecked")
    public static Collection<Map> getMapCollection(Object o) {
        return (Collection<Map>) o;
    }

    public static Collection getCollection(Object o) {
        return (Collection) o;
    }
}
