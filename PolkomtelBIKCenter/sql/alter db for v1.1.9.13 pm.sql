exec sp_check_version '1.1.9.13';
go

if exists(select * from bik_app_prop where name = 'hideDictionaryRootTab' and rtrim(ltrim(val)) in ('1', 'true', 'yes', 'on'))
update bik_tree set is_hidden = 1 where code in ('ShortcutsGlossary', 'BusinessGlossary')

declare @tree_of_trees_id int

select @tree_of_trees_id = id from bik_tree where code = 'TreeOfTrees'

declare @dynamicTreeNodes table (node_id int not null primary key, code varchar(255) not null unique)

insert into @dynamicTreeNodes (node_id, code)
select id, substring(obj_id, 2, len(obj_id))
from bik_node with(index(idx_bik_node_tree_id_is_deleted))
where tree_id = @tree_of_trees_id and is_deleted = 0
and obj_id like '@%'

declare @sqlTxt varchar(max) = ''


update bik_tree
set @sqlTxt = @sqlTxt + 'exec sp_add_menu_node ''@' + t.tree_kind + ''', ''@'', ''$' + t.code + '''
'
from bik_tree t inner join @dynamicTreeNodes dtn on dtn.code = t.tree_kind
left join bik_node totn on totn.tree_id = @tree_of_trees_id and totn.is_deleted = 0 and '$' + t.code = totn.obj_id
where totn.id is null

if ltrim(rtrim(@sqlTxt)) <> ''
exec(@sqlTxt)

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

/*
select *
from bik_tree t inner join @dynamicTreeNodes dtn on dtn.code = t.tree_kind
left join bik_node totn on totn.tree_id = @tree_of_trees_id and totn.is_deleted = 0 and '$' + t.code = totn.obj_id
where totn.id is null
*/


-- exec sp_add_menu_node '@Taxonomy', '@', '$BusinessGlossary' 
-- exec sp_add_menu_node 'metadata', '@', '$ShortcutsGlossary' 

exec sp_update_version '1.1.9.13', '1.1.9.14';
go
