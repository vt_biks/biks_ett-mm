/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.tags;

import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.exprlang.IParsedExpression;
import pl.foxys.foxymill.renderctx.IRenderCtx;

/**
 *
 * @author wezyr
 */
public class WhileTag extends TagBase {

    private IParsedExpression condExpr;

    @Override
    protected void prepareSelf() {
        condExpr = attrToReqExpr("cond"); //exprLang.parseExpression(attrs.get("cond"));
    }

    @Override
    public void internalRender(//IRenderContext
            IRenderCtx renderCtx) {
        while (FoxyMillUtils.evalCondExpr(condExpr, renderCtx)) {
            FoxyMillUtils.render(subTags, renderCtx);
        }
    }
}
