/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.TreeSelectorForJoinedObjectsDialog;
import pl.fovis.common.BlogBean;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author mgraczkowski
 */
public class BlogsWidget extends EntityDetailsTabPaginationBase<BlogBean> {

    protected BikCustomButton addAddBlogEntryButton() {

        actionBtn = new BikCustomButton(I18n.dodajWpisBlogowy.get() /* I18N:  */, "Blogs-addBlogEntry",
                new IContinuation() {
                    @Override
                    public void doIt() {

                        final TreeSelectorForJoinedObjectsDialog tsfjod = new TreeSelectorForJoinedObjectsDialog();

                        tsfjod.buildAndShowDialog(nodeId, BaseUtils.paramsAsSet(BIKGWTConstants.TREE_KIND_BLOGS), new IParametrizedContinuation<TreeNodeBean>() {
                            @Override
                            public void doIt(TreeNodeBean param) {

                                BIKClientSingletons.getService().addBikEntry(nodeId,
                                        param.id, tsfjod.isInheritToDescendantsChecked(),
                                        getAntiNodeRefreshingCallback());
                                refreshAndRedisplayData();
                            }
                        }, getAlreadyJoinedManager());

                        addWidgetVisibledByState(actionBtn);
                    }
                });

        addWidgetVisibledByState(actionBtn);
        return actionBtn;
    }

    @Override
    protected List<BlogBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
//        return data.blogs;
        return BaseUtils.isCollectionEmpty(items) ? new ArrayList<BlogBean>() : items;
    }

    @Override
    public String getCaption() {
        return /*BIKGWTConstants.MENU_NAME_USERS + "<br>" + BIKGWTConstants.MENU_NAME_BLOGS*/ I18n.notatki.get();
    }

    @Override
    public boolean hasEditableControls() {
        return !isInUserTree(getNodeTreeCode());
    }

    @Override
    public boolean hasContent() {
        return !isInUserTree(getNodeTreeCode()) && getItemCount() > 0;
    }

    @Override
    protected NodeAwareBeanExtractorBase<BlogBean> getBeanExtractor() {
        return new BlogBeanExtractor(hasAddJoinedButton, nodeId);
    }

    @Override
    protected String textWhenNoItems() {
        return I18n.brakWpisow.get() /* I18N:  */;
    }

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_BLOGS;
    }

    @Override
    protected BikCustomButton createAddJoinedButton(Set<String> treeKinds) {
        return addAddBlogEntryButton();
    }

    @Override
    protected boolean isChangedViewDescriptionInJoinedPanelVisible() {
        return true;
    }

    @Override
    protected boolean isViewWithAttributeRbInJoinedPanelVisible() {
        return true;
    }

    @Override
    public void optCallServiceToGetData(final IContinuation con) {
        BIKClientSingletons.getService().getBlogs(nodeId, getData().node.linkedNodeId, new AsyncCallback<List<BlogBean>>() {

            @Override
            public void onFailure(Throwable caught) {
                displayData();
            }

            @Override
            public void onSuccess(List<BlogBean> result) {
                items = result;
                con.doIt();
            }
        });
    }

    /**
     * Pobieramy zawsze
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}
