/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextBox;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaConstants;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.foxygwtcommons.NewLookUtils;

/**
 *
 * @author tbzowka
 */
public class LisaDictionaryColumnEditionDialog extends SerializableLisaDialogBase<LisaDictColumnBean> {

    public ListBox dataTypeLb;
    public TextBox nameInDbTb, titleToDisplayTb, descriptionTb;
    public CheckBox isMainKeyCb;
    public CheckBox isLatinCb;
    private List<RadioButton> availabilityRadios;
    protected Label dataTypeLbl, nameInDbLbl, titleToDisplayLbl, descriptionLbl, isMainKeyLbl, dictLbl, colLbl, validationFromListLbl, textDisplayedLbl, isLatinLbl;
    protected List<String> dataTypesList,/* dictValidationList, */ colNameList = new ArrayList<String>();
    private Label accessLevelLbl;
    private final String AVAILABILITY_RADIO_GROUP = "availabilityRadioGroup";
    private final String VALIDITY_RADIO_GROUP = "validityRadioGroup";
    public TextBox dictValidationTb, colValidationTb, textDisplayedTb, defaultValueTb;
    protected RadioButton notValidatedBtn, validateFromDictBtn, validateFromListBtn;
    private Label validationFromDictLbl, notBeValidatedLbl;
    private SuggestBox availableSg;
    private PushButton addValueBtn, deleteValueBtn;

//    public LisaDictionaryColumnEditionDialog(List<String> dictValidationList) {
////        this.dictValidationList = dictValidationList;
//        colNameList = new ArrayList<String>();
//    }
    @Override
    protected String getDialogCaption() {
        return I18n.edycjaKolumnySlownika.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }

    @Override
    protected void customFillControlsBeforeShow() {
        if (bean != null) {
            int index = dataTypesList.indexOf(bean.dataType != null ? bean.dataType.trim() : bean.dataType);
            dataTypeLb.setSelectedIndex(index);
            availabilityRadios.get(bean.accessLevel).setValue(true);
//            if (bean.validateLevel == null) {
//            bean.validateLevel = LisaConstants.NO_VALIDATED;
//            }
            notValidatedBtn.setValue(true);
            if (bean.validateLevel == LisaConstants.VALIDATE_FROM_LIST) {
                validateFromListBtn.setValue(true);
                setValidationPanel(false, true, false);
            } else if (bean.validateLevel == LisaConstants.VALIDATE_FROM_DICT) {
                validateFromDictBtn.setValue(true);
                //                index = dictValidationList.indexOf(bean.dictValidation);
//                dictValidationTb.setSelectedIndex(index);
                dictValidationTb.setText(bean.dictValidation);
                if (bean.colValidation != null) {
//                    getColumnNames(bean.dictValidation);
                    colValidationTb.setText(bean.colValidation);
                }
                if (bean.textDisplayed != null) {
                    textDisplayedTb.setText(bean.textDisplayed);
                }
                setValidationPanel(false, false, true);
            } else if (bean.validateLevel == LisaConstants.NO_VALIDATED) {
                notValidatedBtn.setValue(true);
                setValidationPanel(true, false, false);
            }
        }
    }

    protected void setValidationPanel(boolean notValidated, boolean fromList, boolean fromDict) {
        setDefaultValuePanelEnable(notValidated);
        setValidationFromListEnable(fromList);
        setValidationFromDictEnable(fromDict);
    }

    @Override
    protected void buildMainWidgets() {
        dataTypeLbl = new Label(I18n.typDanych.get() /* I18N:  */);
        nameInDbLbl = new Label(I18n.nazwaKolumny.get() /* I18N:  */);
        titleToDisplayLbl = new Label(I18n.nazwaWyswietlana.get() /* I18N:  */);
        descriptionLbl = new Label(I18n.opisKolumnny.get() /* I18N:  */);
        isMainKeyLbl = new Label(I18n.kluczGlowny.get() /* I18N:  */);
        isLatinLbl = new Label(I18n.latinKolumna.get() /* I18N:  */);
        accessLevelLbl = new Label(I18n.poziomDostepu.get() /* i18n:  */);
        dictLbl = new Label(I18n.slownik_wtf.get());
        colLbl = new Label(I18n.column.get());
        notBeValidatedLbl = new Label(I18n.nieZostaniePotwierdzone.get());
        validationFromListLbl = new Label(I18n.listaDostepnychWartosci.get());
        validationFromDictLbl = new Label(I18n.zBazy.get());
        textDisplayedLbl = new Label(I18n.tekstWyswietlanyZ.get());

        nameInDbTb = new TextBox();
        titleToDisplayTb = new TextBox();
        descriptionTb = new TextBox();

        defineRadios();

        LisaDataType[] dataTypes = LisaDataType.values();
        dataTypesList = new ArrayList<String>();
        dataTypeLb = new ListBox();
        for (LisaDataType nextType : dataTypes) {
            dataTypeLb.addItem(nextType.toString());
            dataTypesList.add(nextType.toString());
        }

        isMainKeyCb = new CheckBox();
        isLatinCb = new CheckBox();
        dictValidationTb = new TextBox();
//        for (String dictName : dictValidationList) {
//            dictValidationTb.addItem(dictName);
//        }
//        dictValidationTb.setSelectedIndex(dictValidationList.indexOf(bean.dictValidation));
//        dictValidationTb.addChangeHandler(new ChangeHandler() {
//
//            @Override
//            public void onChange(ChangeEvent event) {
//                int index = dictValidationTb.getSelectedIndex();
//                getColumnNames(dictValidationTb.getItemText(index));
//            }
//        });
        colValidationTb = new TextBox();
        textDisplayedTb = new TextBox();
        defaultValueTb = new TextBox();
//        if (bean.dictValidation == null) {
//            getColumnNames(dictValidationList.get(0));
//        } else {
//            getColumnNames(bean.dictValidation);
//        }

        notValidatedBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                setValidationPanel(true, false, false);
            }
        });

        validateFromListBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                setValidationPanel(false, true, false);
            }
        });

        validateFromDictBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                setValidationPanel(false, false, true);
            }
        });

        int row = 0;
        int nWidgets = 17;
        Grid grid = new Grid(nWidgets, 2);
        grid.setWidget(row, 0, nameInDbLbl);
        grid.setWidget(row++, 1, nameInDbTb);
        grid.setWidget(row, 0, titleToDisplayLbl);
        grid.setWidget(row++, 1, titleToDisplayTb);
        grid.setWidget(row, 0, descriptionLbl);
        grid.setWidget(row++, 1, descriptionTb);
        grid.setWidget(row, 0, dataTypeLbl);
        grid.setWidget(row++, 1, dataTypeLb);
        grid.setWidget(row, 0, isMainKeyLbl);
        grid.setWidget(row++, 1, isMainKeyCb);
        grid.setWidget(row, 0, isLatinLbl);
        grid.setWidget(row++, 1, isLatinCb);
        grid.setWidget(row, 0, accessLevelLbl);
        grid.setWidget(row++, 1, availabilityRadios.get(0));
        grid.setWidget(row++, 1, availabilityRadios.get(1));
        grid.setWidget(row++, 1, availabilityRadios.get(2));
        grid.setWidget(row, 0, notBeValidatedLbl);
        grid.setWidget(row++, 1, notValidatedBtn);
        grid.setWidget(row, 0, new Label(I18n.wartoscDomyslna.get()));
        grid.setWidget(row++, 1, defaultValueTb);
        grid.setWidget(row, 0, validationFromListLbl);
        grid.setWidget(row++, 1, validateFromListBtn);
        grid.setWidget(row++, 1, createValidateFromListPanelAndAddBtn());
        grid.setWidget(row, 0, validationFromDictLbl);
        grid.setWidget(row++, 1, validateFromDictBtn);
        grid.setWidget(row, 0, dictLbl);
        grid.setWidget(row++, 1, dictValidationTb);
        grid.setWidget(row, 0, colLbl);
        grid.setWidget(row++, 1, colValidationTb);
        grid.setWidget(row, 0, textDisplayedLbl);
        grid.setWidget(row++, 1, textDisplayedTb);

        setValidationFromListEnable(false);
        setValidationFromDictEnable(false);
        main.add(grid);
    }

    private void setDefaultValuePanelEnable(boolean isEnable) {
        defaultValueTb.setEnabled(isEnable);
    }

//    private void getColumnNames(String dictionaryName) {
//        colValidationTb.clear();
//        textDisplayedTb.clear();
//        colNameList.clear();
//        BIKClientSingletons.getLisaService().getDictionaryColumns(dictionaryName, new StandardAsyncCallback<List<LisaDictColumnBean>>() {
//            @Override
//            public void onSuccess(List<LisaDictColumnBean> result) {
//                for (LisaDictColumnBean col : result) {
//                    colValidationTb.addItem(col.nameInDb);
//                    textDisplayedTb.addItem(col.nameInDb);
//                    colNameList.add(col.nameInDb);
//                }
//                System.out.println(bean.colValidation + " " + bean.textDisplayed);
//                System.out.println(colNameList.indexOf(bean.colValidation) + " " + colNameList.indexOf(bean.textDisplayed));
//                colValidationTb.setSelectedIndex(colNameList.indexOf(bean.colValidation));
//                textDisplayedTb.setSelectedIndex(colNameList.indexOf(bean.textDisplayed));
//            }
//        });
//    }
    @Override
    protected LisaDictColumnBean createBean() {
        return new LisaDictColumnBean();
    }

    @Override
    protected void customFillBeanBeforeAction() {
        if (!notValidatedBtn.getValue()) {
            bean.defaultValue = null;
        } else {
            bean.validateLevel = LisaConstants.NO_VALIDATED;
//            bean.defaultValue = defaultValueTb.getText();
        }
        if (!validateFromListBtn.getValue()) {
            bean.choices = null;
        } else {
            bean.validateLevel = LisaConstants.VALIDATE_FROM_LIST;
        }
        if (!validateFromDictBtn.getValue()) {
            bean.dictValidation = null;
            bean.colValidation = null;
            bean.textDisplayed = null;
        } else {
            bean.validateLevel = LisaConstants.VALIDATE_FROM_DICT;
        }

        for (int i = 0; i < 3; i++) {
            if (availabilityRadios.get(i).getValue()) {
                bean.accessLevel = i;
                return;
            }
        }
        bean.accessLevel = LisaConstants.READ_ONLY;
    }

    private void defineRadios() {
        availabilityRadios = new ArrayList<RadioButton>();

        availabilityRadios.add(new RadioButton(AVAILABILITY_RADIO_GROUP, ColumnAvailabilityText.NOT_VISIBLE.description));
        availabilityRadios.add(new RadioButton(AVAILABILITY_RADIO_GROUP, ColumnAvailabilityText.READ_ONLY.description));
        availabilityRadios.add(new RadioButton(AVAILABILITY_RADIO_GROUP, ColumnAvailabilityText.EDITABLE.description));

        notValidatedBtn = new RadioButton(VALIDITY_RADIO_GROUP);
        validateFromListBtn = new RadioButton(VALIDITY_RADIO_GROUP);
        validateFromDictBtn = new RadioButton(VALIDITY_RADIO_GROUP);
    }

    private void setValidationFromDictEnable(boolean value) {
        dictValidationTb.setEnabled(value);
        colValidationTb.setEnabled(value);
        textDisplayedTb.setEnabled(value);
    }

    private void setValidationFromListEnable(boolean value) {
        addValueBtn.setEnabled(value);
        availableSg.setEnabled(value);
        deleteValueBtn.setEnabled(false);
    }

    private HorizontalPanel createValidateFromListPanelAndAddBtn() {
        HorizontalPanel hb = new HorizontalPanel();
        final MultiWordSuggestOracle oracle = new MultiWordSuggestOracle();
        bean.choices = new LinkedList<String>();
        BIKClientSingletons.getLisaService().getValidationList(bean.id, new AsyncCallback<List<String>>() {

            @Override
            public void onFailure(Throwable caught) {
                BIKClientSingletons.showError("getValidationList - Error");
            }

            @Override
            public void onSuccess(List<String> result) {
                for (String choice : result) {
                    bean.choices.add(choice);
                }
                oracle.setDefaultSuggestionsFromText(bean.choices);
                oracle.addAll(bean.choices);
            }
        });
        //System.out.println(bean.id);
        availableSg = new SuggestBox(oracle);

        availableSg.getValueBox().addDoubleClickHandler(new DoubleClickHandler() {
            @Override
            public void onDoubleClick(DoubleClickEvent event) {
                availableSg.showSuggestionList();
            }
        });
        availableSg.addSelectionHandler(new SelectionHandler<Suggestion>() {
            @Override
            public void onSelection(SelectionEvent<Suggestion> event) {
                deleteValueBtn.setEnabled(true);
            }
        });
        availableSg.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                deleteValueBtn.setEnabled(bean.choices.contains(availableSg.getValue()));
            }
        });

        addValueBtn = new PushButton(I18n.dodaj.get());
        addValueBtn.setHTML(createHTMLForActionButtonCaptionIcon() + addValueBtn.getHTML());
        NewLookUtils.makeCustomPushButton(addValueBtn);
        addValueBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                addValidationItem(availableSg.getValue());
                oracle.clear();
                oracle.setDefaultSuggestionsFromText(bean.choices);
                oracle.addAll(bean.choices);
                availableSg.setText("");
            }

        });
        deleteValueBtn = new PushButton(I18n.usun.get());
        deleteValueBtn.setEnabled(false);
        deleteValueBtn.setHTML(createHTMLForActionButtonCaptionIcon() + deleteValueBtn.getHTML());
        NewLookUtils.makeCustomPushButton(deleteValueBtn);

        deleteValueBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                deleteValidationItem(availableSg.getValue());
                deleteValueBtn.setEnabled(false);
                oracle.clear();
                oracle.setDefaultSuggestionsFromText(bean.choices);
                oracle.addAll(bean.choices);
                availableSg.setText("");
            }

        });
        hb.setSpacing(6);
        hb.add(availableSg);
        hb.add(addValueBtn);
        hb.add(deleteValueBtn);
        return hb;
    }

    private void addValidationItem(String value) {
        if (bean.choices.contains(value)) {
            return;
        }

        bean.choices.add(value);
        BIKClientSingletons.getLisaService().addValidationItem(bean.id, value, new AsyncCallback<Void>() {

            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(Void result) {
            }
        });
    }

    private void deleteValidationItem(String value) {
        bean.choices.remove(value);
        BIKClientSingletons.getLisaService().deleteValidationItem(bean.id, value, new AsyncCallback<Void>() {

            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(Void result) {
            }
        });
    }
}
