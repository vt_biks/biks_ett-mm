/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.common.fts.StemmedWordBean;
import pl.fovis.common.fts.WordOccurrencesInOneAttr;
import pl.foxys.ftshelper.OTFOracleFactory;
import pl.trzy0.foxy.commons.FTSWordOrigin;
import pl.trzy0.foxy.commons.IOtherTermFormsOracle;
import simplelib.BaseUtils;
import simplelib.ILameCollector;
import simplelib.LameBag;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class TextSlicer implements ISlicer {

    private static final boolean WITH_STEMMING = false;
    private static final boolean WITH_WORD_PAIRS = false;
    private static final ILameLogger logger = LameUtils.getMyLogger();
    private static final IOtherTermFormsOracle otfOracle = OTFOracleFactory.getOracle("pl");
    protected final WordsStorage storage;// = new WordsStorage();
    public static LameBag<Character> charUsage = new LameBag<Character>();
    public static char maxChar = 0;

    public TextSlicer(WordsStorage storage) {
        this.storage = storage;
    }

    private static final boolean[] charIsSep = new boolean[10000];

    private static boolean calcCharIsSep(char c) {

//        return !Character.isAlphabetic(c);
        int charType = Character.getType(c);

        return !(charType == Character.DECIMAL_DIGIT_NUMBER
                || charType == Character.LOWERCASE_LETTER);
    }

    private static void initCharTypes() {
        if (logger.isInfoEnabled()) {
            logger.info("initCharTypes: start");
        }

        for (char c = 0; c < charIsSep.length; c++) {
//            charIsSep[c] = !Character.isAlphabetic(c);
            charIsSep[c] = calcCharIsSep(c);
        }

        if (logger.isInfoEnabled()) {
            logger.info("initCharTypes: end");
        }
    }

    static {
        initCharTypes();
    }

    private static List<String> splitToWords(String text) {

        final List<String> res = new ArrayList<String>();

        if (text == null) {
            return res;
        }

        int len = text.length();
        int startPos = -1;
        boolean wasSep = true;
        boolean hasUnderscore = false;
        String prevWord = null;

        final List<String> wordPairs = WITH_WORD_PAIRS ? new ArrayList<String>() : null;

        // uwaga, sztuczka z <=
        for (int pos = 0; pos <= len; pos++) {
            char c = pos < len ? text.charAt(pos) : ' ';

            if (c > maxChar) {
                maxChar = c;
            }

            charUsage.add(c);

            boolean isSep;

            if (c == '_') {
                isSep = false;
                hasUnderscore = true;
            } else {
                if (c < charIsSep.length) {
                    isSep = charIsSep[c];
                } else {
                    isSep = calcCharIsSep(c);
                }
            }

            if (isSep) {

                if (!wasSep) {
                    String word = text.substring(startPos, pos);
                    res.add(word);

                    if (wordPairs != null && prevWord != null) {
                        wordPairs.add(prevWord + " " + word);
                    }

                    prevWord = word;

                    if (hasUnderscore) {
                        BaseUtils.splitBySepIntoCollector(word, "_", new ILameCollector<String>() {

                            @Override
                            public void add(String item) {
                                if (!item.isEmpty()) {
                                    res.add(item);
                                }
                            }
                        }, false);
                    }
                    hasUnderscore = false;
                }
            } else {
                if (wasSep) {
                    startPos = pos;
                }
            }

            wasSep = isSep;
        }

        if (wordPairs != null) {
            res.addAll(wordPairs);
        }

        return res;
    }

    @Override
    public void addOrUpdateObject(int objId, Map<String, Object> attrVals) {

        Map<String, List<WordOccurrencesInOneAttr>> wordsMap = new HashMap<String, List<WordOccurrencesInOneAttr>>();

        for (Entry<String, Object> e : attrVals.entrySet()) {
            Object attrVal = e.getValue();

            if (attrVal == null) {
                continue;
            }

            String attrValStr = attrVal.toString();

//            List<FTSWord> ftsWords = splitToWordsWithStemming(attrValStr);
            Set<String> words = new HashSet<String>(splitToWords(attrValStr.toLowerCase()));

            if (words.isEmpty()) {
                continue;
            }

            List<WordOccurrencesInOneAttr> occs = new ArrayList<WordOccurrencesInOneAttr>(words.size());

            for (String word : words) {
                occs.add(new WordOccurrencesInOneAttr(word, 0));
            }

            String attrName = e.getKey();
            wordsMap.put(attrName, occs);
        }

//        if (wordsMap.isEmpty()) {
//            return;
//        }
        Map<Integer, String> newWords = storage.processObject(objId, wordsMap);

        if (WITH_STEMMING) {
            addStemmedWords(newWords);
        }
    }

    // FTSWordOrigin.Original, FTSWordOrigin.Synonym, FTSWordOrigin.DictStemmed,
    // FTSWordOrigin.AlgoStemmed, FTSWordOrigin.AccentFree
    protected static final EnumSet<FTSWordOrigin> forAccNoDigNorUndSco = BaseUtils.paramsToEnumSet(
            FTSWordOrigin.Synonym, FTSWordOrigin.DictStemmed,
            FTSWordOrigin.AlgoStemmed, FTSWordOrigin.AccentFree);
    protected static final EnumSet<FTSWordOrigin> forAccDigOrUndSco = BaseUtils.paramsToEnumSet(
            FTSWordOrigin.Synonym, FTSWordOrigin.AccentFree);
    protected static final EnumSet<FTSWordOrigin> forUnAccNoDigNorUndSco = BaseUtils.paramsToEnumSet(
            FTSWordOrigin.Synonym, FTSWordOrigin.DictStemmed,
            FTSWordOrigin.AlgoStemmed);
    protected static final EnumSet<FTSWordOrigin> forUnAccDigOrUndSco = BaseUtils.paramsToEnumSet(
            FTSWordOrigin.Synonym);

    protected void addStemmedWords(Map<Integer, String> newWords) {
        List<StemmedWordBean> newStemmed = new ArrayList<StemmedWordBean>();

        for (Entry<Integer, String> e : newWords.entrySet()) {
            int wordId = e.getKey();
            String word = e.getValue();

            // tu być może nie wszystkie powinniśmy stemować, bo np. abc_xyz - nie koniecznie
            boolean hasDigitOrUnderScore = false;
            boolean hasAccentedChars = false;

            int wordLen = word.length();

            for (int i = 0; i < wordLen; i++) {
                char c = word.charAt(i);
                if (c >= 128) {
                    hasAccentedChars = true;
                } else if (c == '_' || c >= '0' && c <= '9') {
                    hasDigitOrUnderScore = true;
                }
            }

            EnumSet<FTSWordOrigin> mode;

            if (hasAccentedChars) {
                mode = hasDigitOrUnderScore ? forAccDigOrUndSco : forAccNoDigNorUndSco;
            } else {
                mode = hasDigitOrUnderScore ? forUnAccDigOrUndSco : forUnAccNoDigNorUndSco;
            }

            Set<Pair<String, FTSWordOrigin>> otfs = otfOracle.getOtherForms(word, mode);

            Map<String, Integer> stemmedWithOrigins = new HashMap<String, Integer>();

            stemmedWithOrigins.put(word, FTSWordOrigin.Original.getOriginStrengthFlag());

            for (Pair<String, FTSWordOrigin> stemmed : otfs) {
                final String stemmedWord = stemmed.v1;
                Integer flag = stemmedWithOrigins.get(stemmedWord);
                if (flag == null) {
                    flag = 0;
                }
                flag |= stemmed.v2.getOriginStrengthFlag();
                stemmedWithOrigins.put(stemmedWord, flag);
            }

            for (Entry<String, Integer> ee : stemmedWithOrigins.entrySet()) {
                newStemmed.add(new StemmedWordBean(wordId, ee.getKey(), ee.getValue()));
            }
        }

        storage.addStemmedWords(newStemmed);
    }

    public static void main(String[] args) {
        System.out.println(otfOracle.getOtherForms("system", IOtherTermFormsOracle.allFormKinds));
        System.out.println(otfOracle.getOtherForms("systemów", IOtherTermFormsOracle.allFormKinds));
        System.out.println(otfOracle.getOtherForms("systemy", IOtherTermFormsOracle.allFormKinds));
        System.out.println(otfOracle.getOtherForms("psów", IOtherTermFormsOracle.allFormKinds));
        System.out.println(otfOracle.getOtherForms("psy", IOtherTermFormsOracle.allFormKinds));
    }

    @Override
    public void init() {
        // no-op
    }

    @Override
    public void close() {
        // no-op
    }
}
