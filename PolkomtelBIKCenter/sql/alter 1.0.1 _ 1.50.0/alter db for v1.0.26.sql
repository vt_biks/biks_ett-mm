exec sp_check_version '1.0.26';
go

update bik_tree set node_kind_id=(select id from bik_node_kind where code='TezaurusCategory') where name='Tezaurus'
go

alter table bik_tree
add code varchar(255)
go

update bik_tree
set code = 'Reports' where name = 'Raporty'
go



update bik_tree
set code = 'ObjectUniverses' where name = '�wiaty obiekt�w'
go


update bik_tree
set code = 'Connections' where name = 'Po��czenia'
go


update bik_tree
set code = 'Teradata' where name = 'Teradata'
go


update bik_tree
set code = 'OrgStructure' where name = 'Struktura organizacyjna'
go


update bik_tree
set code = 'ITSystems' where name = 'Systemy IT'
go


update bik_tree
set code = 'Tezaurus' where name = 'Tezaurus'
go


update bik_tree
set code = 'Documents' where name = 'Dokumenty'
go

alter table bik_tree alter column code varchar(255) not null
go


exec sp_update_version '1.0.26', '1.0.27';
go


