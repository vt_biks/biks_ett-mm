﻿exec sp_check_version '1.0.88.13';
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


exec sp_rename 'bik_sapbo_object_table.table_id', 'table_node_id', 'COLUMN'
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_object_table]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_sapbo_object_table]
GO


create procedure [dbo].[sp_insert_into_bik_node_sapbo_object_table]
as
begin

declare @tree_id int;
select @tree_id = id from bik_tree where code='ObjectUniverses'
declare @universe_table_kind int;
select @universe_table_kind = id from bik_node_kind where code='UniverseTable'
declare @filtr_kind int;
select @filtr_kind = id from bik_node_kind where code='Filter'
declare @detail_nki int;
select @detail_nki = id	from bik_node_kind where code = 'Detail';
declare @dimension_nki int;
select @dimension_nki = id from bik_node_kind where code = 'Dimension';
declare @measure_nki int;
select @measure_nki = id from bik_node_kind	where code = 'Measure';

delete from bik_sapbo_object_table

insert into bik_sapbo_object_table(object_node_id,table_node_id)
select *
from (
select case when uot.obj_type=1 then bn.id else bn3.id end as obj_node_ide, bn2.id from aaa_universe_obj_tables uot
left join aaa_universe_obj uo on uot.universe_obj_id=uo.id
left join bik_node bn on bn.obj_id = uo.obj_branch
and bn.is_deleted=0
and bn.linked_node_id is null
and bn.tree_id=@tree_id
and bn.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)
join aaa_universe_table ut on uot.universe_table_id=ut.id
join bik_node bn2 on bn2.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
and bn2.is_deleted=0
and bn2.linked_node_id is null
and bn2.node_kind_id=@universe_table_kind
left join aaa_universe_filter uf on uot.universe_obj_id=uf.id
left join bik_node bn3 on bn3.obj_id = uf.filtr_branch
and bn3.is_deleted=0
and bn3.linked_node_id is null
and bn3.tree_id=@tree_id
and bn3.node_kind_id=@filtr_kind
) x
where obj_node_ide is not null

insert into bik_joined_objs(src_node_id,dst_node_id,type)
select object_node_id, table_node_id, 1 from bik_sapbo_object_table a 
left join bik_joined_objs b on a.object_node_id=b.src_node_id and a.table_node_id=b.dst_node_id
where b.id is null

insert into bik_joined_objs(src_node_id,dst_node_id,type)
select table_node_id, object_node_id, 1 from bik_sapbo_object_table a
left join bik_joined_objs b on a.table_node_id=b.src_node_id and a.object_node_id=b.dst_node_id
where b.id is null

end
go

-------------------------------------------------
-------------------------------------------------


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_metadata]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_metadata
go

create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_extradata', 'node_id', 'author, owner, cuid, guid, ruid', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_column', 'table_id', 'name', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_enigme, database_source, connetion_networklayer_name, user_name, server', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table, sql_of_derived_table_with_alias', null, @optNodeFilter
end
go


-------------------------------------------------
-------------------------------------------------


if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_node'))
drop fulltext index on bik_node
go


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

exec sp_update_version '1.0.88.13', '1.0.89';
go