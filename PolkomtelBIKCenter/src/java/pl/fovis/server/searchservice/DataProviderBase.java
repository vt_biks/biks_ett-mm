/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.searchservice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.lucene.document.Document;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;

/**
 *
 * @author ctran
 */
public abstract class DataProviderBase implements IDataProvider {

    protected static Integer DEFAULT_LOADED_PACK_SIZE = 500;
    protected INamedSqlsDAO adhocDao;
    protected Integer loadedPackSize;
    protected String namedSqlToLoadContent;
    protected int packNum = 0;
    protected Date indexFromDate;
    protected Date indexToDate;

    protected List<Map<String, Object>> buffer = new ArrayList<Map<String, Object>>();

    public DataProviderBase(INamedSqlsDAO adhocDao, String namedSqlToLoadContent) {
        this.adhocDao = adhocDao;
        this.namedSqlToLoadContent = namedSqlToLoadContent;
        this.loadedPackSize = BaseUtils.tryParseInteger((String) BeanConnectionUtils.execNamedQuerySingleVal(adhocDao, "getAppPropValue", true, null, "defaultPackSize"));
        if (this.loadedPackSize == null) {
            this.loadedPackSize = 5000;
        }
    }

    @Override
    public List<Document> next() {
        loadMoreDataToBuffer();
        List<Document> res = new ArrayList<Document>();
        for (Map<String, Object> content : buffer) {
            generateDocumentsFromContentAndAddToIndexingList(content, res);
        }
        return res;
    }

    @Override
    public void restart(Date indexFromDate, Date indexToDate) {
        this.indexFromDate = indexFromDate;
        this.indexToDate = indexToDate;
        buffer.clear();
        packNum = 0;
    }

    @SuppressWarnings("unchecked")
    protected void loadMoreDataToBuffer() {
        buffer = adhocDao.execNamedQueryCFN(namedSqlToLoadContent, FieldNameConversion.ToLowerCase, packNum++, loadedPackSize);
    }

    protected abstract void generateDocumentsFromContentAndAddToIndexingList(Map<String, Object> content, List<Document> indexingList);
}
