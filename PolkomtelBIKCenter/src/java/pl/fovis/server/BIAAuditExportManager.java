/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.UrlMakingUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import pl.fovis.common.AuditBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class BIAAuditExportManager extends ExportManagerBase {

    protected BOXIServiceImpl service;

    public BIAAuditExportManager(BOXIServiceImpl boxiService) {
        this.service = boxiService;
    }

    @Override
    protected Pair<String, List<Map<String, Object>>> getFileNamePrefixAndContent(HttpServletRequest req) {

        String fileName = I18n.audit.get();
        List<Map<String, Object>> content = getBIAAudti();
        return new Pair<String, List<Map<String, Object>>>(UrlMakingUtils.encodeStringForUrlWW(fileName).replace("+", "%20"), content);
    }

    public List<Map<String, Object>> getBIAAudti() {
        List<Map<String, Object>> content = new ArrayList<Map<String, Object>>();
        List<AuditBean> audit = service.getBIAAuditInformation();
        for (AuditBean au : audit) {
            Map<String, Object> item = new LinkedHashMap<String, Object>();
            item.put(I18n.lokalizacja.get(), au.path);
            item.put(I18n.nazwa.get(), au.name);
            item.put(I18n.scheduleCUID.get(), au.cuid);
            item.put(I18n.auditRefreshCnt.get(), au.refreshCnt);
            item.put(I18n.auditViewCnt.get(), au.viewCnt);
            item.put(I18n.auditLastRefresh.get(), au.refreshLast);
            item.put(I18n.auditLastView.get(), au.viewLast);
            item.put(I18n.auditUserLastRefresh.get(), au.userRefreshLast);
            item.put(I18n.auditUserLastView.get(), au.userViewLast);
            content.add(item);
        }
        return content;
    }

    @Override
    protected String getFileFormat(HttpServletRequest req) {
        return req.getParameter(BIKConstants.BIA_AUDIT_RESULTS_EXPORT_PARAM_NAME);
    }

}
