/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import java.util.ArrayList;
import java.util.Collection;
import simplelib.BaseUtils.Projector;

/**
 *
 * @author wezyr
 */
public class CollectionCollectorEx<T, T2> implements ILameCollector<T> {
    private Collection<T2> coll;
    private Projector<T, T2> itemProj;
    
    public CollectionCollectorEx(Projector<T, T2> itemProj, Collection<T2> coll) {
        this.itemProj = itemProj;
        this.coll = coll;
    }
    
    public CollectionCollectorEx(Projector<T, T2> itemProj) {
        this.itemProj = itemProj;
    }

//    public CollectionCollectorEx(Collection<T2> coll) {
//        this.coll = coll;
//    }

    public void setCollection(Collection<T2> coll) {
        this.coll = coll;
    }

    protected Collection<T2> createDefaultCollection() {
        return new ArrayList<T2>();
    }

    protected T2 convertItem(T item) {
        return itemProj.project(item);
    }

    protected void ensureColl() {
        if (coll == null) {
            coll = createDefaultCollection();
        }
    }

    public void add(T item) {
        ensureColl();
        coll.add(convertItem(item));
    }

    public Collection<T2> getCollection() {
        ensureColl();
        return coll;
    }
}
