/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.lisa;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.lisa.ChangeCategoryDialog;
import pl.fovis.client.entitydetailswidgets.lisa.LisaAuthorizedRequest;
import pl.fovis.client.nodeactions.NodeActionUpdatingBase;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pgajda
 */
public class NodeActionMoveLisaCategory extends NodeActionUpdatingBase {

    public NodeActionMoveLisaCategory() {
    }

    @Override
    public boolean isEnabledByDesign() {
        return isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_LISA_CATEGORY) && BIKClientSingletons.isBikUserLoggedIn();
    }

    @Override
    public String getCaption() {
        return I18n.przeniesKategorie.get() /* I18N:  */;
    }

    @Override
    public void execute() {
        BIKClientSingletons.getLisaService().getCategoryzationNodeId(node.parentNodeId, new StandardAsyncCallback<Integer>("Error in getCategoryzationNodeId") {

            @Override
            public void onSuccess(Integer rootNodeId) {
                ChangeCategoryDialog dialog = new ChangeCategoryDialog(rootNodeId, node);
                dialog.itemsToMoveCount = 1;
                dialog.currentCategoryName = node.name;
                dialog.saveCont = new IParametrizedContinuation<Integer>() {
                    @Override
                    public void doIt(final Integer newCategoryNodeId) {
                        BIKClientSingletons.getLisaService().getLisaInstanceByNodeId(node.id, new StandardAsyncCallback<LisaInstanceInfoBean>() {

                            @Override
                            public void onSuccess(final LisaInstanceInfoBean result) {
                                new LisaAuthorizedRequest<List<String>>(result.id, result.name) {
                                    @Override
                                    protected void performServerAction(final AsyncCallback<List<String>> callback) {
                                        //final TreeNodeBean srcNode = clipboard.getNode();
                                        //srcNode.parentNodeId = newCategoryNodeId;
                                        node.parentNodeId = newCategoryNodeId;

                                        BIKClientSingletons.getLisaService().moveCategory(result.id, node, newCategoryNodeId, callback);
                                    }

                                    @Override
                                    public void onActionSuccess(List<String> result) {
                                        BIKClientSingletons.showInfo(I18n.wklejonoElement.get() /* I18N:  */);
                                        refreshRedisplayDataAndSelectRecord(node.id);
                                        BIKClientSingletons.showInfo(I18n.kategoriaZostalaPrzeniesiona.get() /* I18N:  */);
                                    }
                                }.go();
                            }
                        });
                    }
                };
                dialog.buildAndShowDialog();
            }
        });
    }
}
