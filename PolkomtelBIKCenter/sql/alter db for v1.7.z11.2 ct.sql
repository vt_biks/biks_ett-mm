﻿exec sp_check_version '1.7.z11.2';
go

if (OBJECT_ID('bik_lisa_instance') is null)
create table bik_lisa_instance (
	id int primary key identity(1,1),
	name varchar(max),
	url varchar(max),
	port varchar(max),
	db_name varchar(max),
	charset varchar(max),
	tmode varchar(max),
	schema_name varchar(max),
	default_unassigned_category_name varchar(max),
	is_deleted int default 0
);
go

if (COL_LENGTH('bik_lisa_dict_column', 'instance_id') is null)
alter table bik_lisa_dict_column add instance_id int foreign key references bik_lisa_instance(id)
go

if (OBJECT_ID('bik_lisa_dict_instance') is null)
create table bik_lisa_dict_instance (
	id int primary key identity(1,1),
	node_id int foreign key references bik_node(id),
	instance_id int foreign key references bik_lisa_instance(id)
)
go

if exists (select 1 from bik_admin where param like 'lisateradata.%') 
begin
	declare @url varchar(max) = (select value from bik_admin where param = 'lisateradata.url');
	declare @port varchar(max) = (select value from bik_admin where param = 'lisateradata.port');
	declare @database varchar(max) = (select value from bik_admin where param = 'lisateradata.database');
	declare @charset varchar(max) = (select value from bik_admin where param = 'lisateradata.charset');
	declare @tmode varchar(max) = (select value from bik_admin where param = 'lisateradata.tmode');
	declare @schema_name varchar(max) = (select value from bik_admin where param = 'lisateradata.schemaName');
	declare @ducn varchar(max) = (select value from bik_admin where param = 'lisateradata.defaultUnassignedCategoryName');
	
	if not exists (select 1 from bik_lisa_instance where url=@url and port=@port and db_name=@database and charset=@charset and tmode=@tmode and schema_name=@schema_name and default_unassigned_category_name=@ducn)
	begin
		insert into bik_lisa_instance(name, url, port, db_name, charset, tmode, schema_name, default_unassigned_category_name)
		values ('dbc', @url, @port, @database, @charset, @tmode, @schema_name, @ducn)
		declare @instance_id int = scope_identity();
		declare @lisa_tree_id int = dbo.fn_tree_id_by_code('DictionaryDWH');
		declare @dwh_dict_id int = dbo.fn_node_kind_id_by_code('DWHDictionary');
    
		insert into bik_lisa_dict_instance(node_id, instance_id) 
		select id, @instance_id
		from bik_node 
		where tree_id = @lisa_tree_id
		and is_deleted = 0 
		and node_kind_id in (select id from bik_node_kind where code in ('DWHDictionaries', 'DWHDictionary', 'DWHCategorization', 'DWHCategory'))
		
		update bik_lisa_dict_column set instance_id = @instance_id
		update bik_node set obj_id=CAST(@instance_id as varchar)+'.'+obj_id where tree_id=@lisa_tree_id and node_kind_id=@dwh_dict_id and parent_node_id is not null 
		update bik_lisa_dict_column set dictionary_name=CAST(@instance_id as varchar)+'.'+dictionary_name
		
		delete from bik_admin where param in ('lisateradata.url', 'lisateradata.port', 'lisateradata.database', 'lisateradata.charset', 'lisateradata.tmode', 'lisateradata.schemaName', 'lisateradata.defaultUnassignedCategoryName');
	end;	
end;

exec sp_update_version '1.7.z11.2', '1.7.z12';
go