exec sp_check_version '1.4.y1.1';
go

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------
-- poprawki do bik_help. 
--   - usuni�cie zb�dnego text_help_plain
--   - poprawka MyBIK na MyBIKS
--   - dodanie lang do bik_help (wszystko co jest do
--       tej pory ma lang = pl)
--   - inicjalne przerzucenie polskich help�w do 
--       wer. en, z przedrostkiem en_
--   - usuni�cie tabelki bik_help_translation
----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

-- je�eli nie ma dobrego wpisu dla MyBIKS
if not exists (select * from bik_help where help_key = 'MyBIKS' and rtrim(ltrim(coalesce(bik_help.text_help, ''))) <> '') begin
  -- mo�e jest, ale pusty - wywali� tak czy inaczej (jak nie ma, to si� nie usunie, bo ju� go nie ma)
  delete from bik_help where help_key = 'MyBIKS'
  
  insert into bik_help (help_key, caption, text_help, text_help_plain, is_hidden)
  select 'MyBIKS', caption, text_help, text_help_plain, is_hidden
  from bik_help where help_key = 'MyBIK'    
end

delete from bik_help where help_key = 'myBIK'
go


alter table bik_help add lang varchar(3) null
go

update bik_help set lang = 'pl'
go

alter table bik_help alter column lang varchar(3) not null
go

alter table bik_help drop column text_help_plain
go

declare @idx_name sysname
select @idx_name = i.name
from sys.indexes i
inner join sys.index_columns ic on i.index_id = ic.index_id and ic.object_id = i.object_id
inner join sys.columns c on ic.object_id = c.object_id and ic.column_id = c.column_id
inner join sys.objects o on i.object_id = o.object_id
where o.name = 'bik_help' and c.name = 'help_key'
exec ('alter table bik_help drop constraint ' + @idx_name)
go

alter table bik_help add constraint PK_bik_help_help_key_lang primary key (help_key, lang)
go

insert into bik_help (help_key, caption, text_help, is_hidden, lang)
select help_key, 'en_' + caption, 'en_<br/>' + text_help, is_hidden, 'en'
from bik_help where is_hidden = 0
go

drop table bik_help_translation
go

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

exec sp_update_version '1.4.y1.1', '1.4.y1.2';
go
