/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.io.IOException;

/**
 *
 * @author ctran
 */
public interface IArchiveFileHelper {

    public void makeSureFolderExists(String dirPath);

    public void writeTempFile2SavedFolder(String tempFilePath, String destFilePath) throws IOException;

    public boolean checkFileExists(String filePath);
}
