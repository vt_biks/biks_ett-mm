/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.mailing;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import pl.trzy0.foxy.commons.MailServerConfig;
import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.LameRuntimeException;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class SendMail {

    private static ILameLogger logger = LameLoggerFactory.getLogger(SendMail.class);
    private MailServerConfig server;
    private static final String MAIL_SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
    private static boolean MAIL_DEBUG_MODE = false;

    public SendMail(MailServerConfig server) {
        this.server = server;
    }

    public void send(MailProps mail, boolean ssl) {
//        if (logger.isInfoEnabled()) {
//            logger.info("send starts...");
//        }
        addLog("send starts...");
        boolean debug = MAIL_DEBUG_MODE;

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", server.host);
        if (!BaseUtils.isStrEmptyOrWhiteSpace(server.user)) {
            props.put("mail.smtp.auth", "true");
        } else { // dla exchange - false
            props.put("mail.smtp.auth", "false");
        }
        props.put("mail.debug", String.valueOf(debug));
        props.put("mail.smtp.port", server.port);
        props.put("mail.smtp.ssl.trust", "*");

        if (ssl) {
//            props.put("mail.smtp.socketFactory.port", server.port);
//            props.put("mail.smtp.socketFactory.class", MAIL_SSL_FACTORY); // niepotrzebne w nowej wersji biblioteki mail.jar
//            props.put("mail.smtp.socketFactory.fallback", "false");
            props.put("mail.smtp.ssl.enable", "true");
//            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.ssl.checkserveridentity", "false");
        }

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(server.user, server.password);
            }
        });

        session.setDebug(debug);

        MimeMessage msg = new MimeMessage(session);
        try {
            InternetAddress addressFrom = new InternetAddress(server.fromAddress);
            msg.setFrom(addressFrom);

            InternetAddress[] addressTo = new InternetAddress[mail.recipients.length];
            for (int i = 0; i < mail.recipients.length; i++) {
                addressTo[i] = new InternetAddress(mail.recipients[i]);

//                if (logger.isInfoEnabled()) {
//                    logger.info("recipients: " + mail.recipients[i]);
//                }
                addLog("recipients: " + mail.recipients[i]);
            }
            InternetAddress[] addressCC = new InternetAddress[mail.cc.length];
            for (int i = 0; i < mail.cc.length; i++) {
                addressCC[i] = new InternetAddress(mail.cc[i]);

//                if (logger.isInfoEnabled()) {
//                    logger.info("recipients: " + mail.cc[i]);
//                }
                addLog("recipients cc: " + mail.cc[i]);

            }
            msg.setRecipients(Message.RecipientType.TO, addressTo);
            if (addressCC.length > 0) {
                msg.setRecipients(Message.RecipientType.CC, addressCC);
            }

            // Setting the Subject and Content Type
            msg.setSubject(mail.subject, "UTF-8");
//            if (logger.isInfoEnabled()) {
//                logger.info("subject: " + mail.subject);
//            }
            addLog("subject: " + mail.subject);
            if (mail.embeddedFiles == null && mail.embeddedByteArrays == null) {
                msg.setContent(mail.message, "text/html;charset=UTF-8");
                if (logger.isInfoEnabled()) {
                    logger.info("Message: " + mail.message);
                }

            } else {
                // Create your new message part
                BodyPart messageBodyPart = new MimeBodyPart();
                //String htmlText = "<H1>Hello</H1>" +
                //        "<img src=\"cid:memememe\">";
                messageBodyPart.setContent(mail.message, "text/html;charset=UTF-8");
                if (logger.isInfoEnabled()) {
                    logger.info("Message: " + mail.message);
                }

                // Create a related multi-part to combine the parts
                MimeMultipart multipart = new MimeMultipart("related");
                multipart.addBodyPart(messageBodyPart);

                if (mail.embeddedByteArrays != null) {
                    addByteArraysToMail(multipart, mail.embeddedByteArrays);
                }
                if (mail.embeddedFiles != null) {
                    addFilesToMail(multipart, mail.embeddedFiles);
                }

                // Associate multi-part with message
                msg.setContent(multipart);
            }

            //msg.setContent(message, "text/plain");
            //msg.setText(message, "utf8");
            //Transport.send(msg);
            msg.saveChanges(); // implicit with send()
            Transport transport = session.getTransport("smtp"); //session.getTransport("smtp");
            //transport.connect(host, username, password);
//            if (logger.isInfoEnabled()) {
//                logger.info("connecting server...");
//            }
            addLog("connecting server...");
            transport.connect();
//            if (logger.isInfoEnabled()) {
//                logger.info("sending message...");
//            }
            addLog("sending message...");
            transport.sendMessage(msg, msg.getAllRecipients());
//            if (logger.isInfoEnabled()) {
//                logger.info("closing...");
//            }
            addLog("closing...");
            transport.close();
//            if (logger.isInfoEnabled()) {
//                logger.info("done!");
//            }
            addLog("done");
        } catch (Exception ex) {
            addLog("send mail: "+ ex);
            
            throw new LameRuntimeException("send mail", ex);
        }
    }

    private <DATA> void addDataToMail(MimeMultipart multipart, Map<String, DATA> dataEntries,
            Projector<Entry<String, DATA>, DataSource> dataSourceCreator) throws MessagingException {
        for (Entry<String, DATA> e : dataEntries.entrySet()) {
            MimeBodyPart messageBodyPart = new MimeBodyPart();

            DataSource ds = dataSourceCreator.project(e);

            messageBodyPart.setFileName(e.getKey());
            messageBodyPart.setDataHandler(new DataHandler(ds));
            messageBodyPart.setHeader("Content-ID", "<" + e.getKey() + ">");
            // Add part to multi-part
            multipart.addBodyPart(messageBodyPart);
        }
    }

    private void addFilesToMail(MimeMultipart multipart, Map<String, String> embeddedFiles) throws MessagingException {
        addDataToMail(multipart, embeddedFiles, new Projector<Entry<String, String>, DataSource>() {
            public DataSource project(Entry<String, String> val) {
                return new FileDataSource(val.getValue());
            }
        });
//        for (Entry<String, String> e : embeddedFiles.entrySet()) {
//            MimeBodyPart messageBodyPart = new MimeBodyPart();
//
//            DataSource fds = new FileDataSource(e.getValue());
//            messageBodyPart.setDataHandler(new DataHandler(fds));
//            messageBodyPart.setHeader("Content-ID", "<" + e.getKey() + ">");
//            // Add part to multi-part
//            multipart.addBodyPart(messageBodyPart);
//        }
    }

    private void addByteArraysToMail(MimeMultipart multipart, Map<String, byte[]> embeddedByteArrays) throws MessagingException {
        addDataToMail(multipart, embeddedByteArrays, new Projector<Entry<String, byte[]>, DataSource>() {
            public DataSource project(Entry<String, byte[]> val) {
                ByteArrayDataSource bads = new ByteArrayDataSource(val.getValue(), "application/octet-stream");
                bads.setName(val.getKey());
                return bads;
            }
        });
//        for (Entry<String, byte[]> e : embeddedByteArrays.entrySet()) {
//            MimeBodyPart messageBodyPart = new MimeBodyPart();
//
//            ByteArrayDataSource bads = new ByteArrayDataSource(e.getValue(), "application/octet-stream");
//            bads.setName(e.getKey());
//            messageBodyPart.setFileName(e.getKey());
//            messageBodyPart.setDataHandler(new DataHandler(bads));
//            messageBodyPart.setHeader("Content-ID", "<" + e.getKey() + ">");
//            // Add part to multi-part
//            multipart.addBodyPart(messageBodyPart);
//        }
    }

    //ww: wersja uproszczona, wstecznie kompatybilna z istniejącym kodem
    public static void sendMessage(MailServerConfig server,
            final String recipients[], final String subject, final String message,
            final Map<String, String> embeddedFiles) throws MessagingException {
        sendMessageWithCC(server, recipients, new String[]{}, subject, message, embeddedFiles, null);
    }

    public static void sendMessageWithCC(MailServerConfig server,
            final String recipients[], final String[] cc, final String subject, final String message,
            final Map<String, String> embeddedFiles) throws MessagingException {
        sendMessageWithCC(server, recipients, cc, subject, message, embeddedFiles, null);
    }

    public static void sendMessageWithCC(MailServerConfig server,
            final String recipients[], final String[] cc, final String subject, final String message,
            final Map<String, String> embeddedFiles, final Map<String, byte[]> embeddedByteArrays) throws MessagingException {

        SendMail sm = new SendMail(server);
        sm.send(new MailProps(recipients, cc, subject, message, embeddedFiles, embeddedByteArrays), server.ssl);
    }

    public static void sendMessage(MailServerConfig server,
            final String recipients[], final String subject, final String message,
            final Map<String, String> embeddedFiles, final Map<String, byte[]> embeddedByteArrays) throws MessagingException {

        sendMessageWithCC(server, recipients, new String[]{}, subject, message, embeddedFiles);
    }

    public static void sendMessage(final String smtpHostName, final String smtpPort,
            final String user, final String pwd,
            final String recipients[], final String subject, final String message,
            final String from, final boolean ssl,
            final Map<String, String> embeddedFiles) throws MessagingException {
        sendMessage(smtpHostName, smtpPort, user, pwd, recipients, subject, message, from, ssl, embeddedFiles, null);
    }

    public static void sendMessage(final String smtpHostName, final String smtpPort,
            final String user, final String pwd,
            final String recipients[], final String subject, final String message,
            final String from, final boolean ssl,
            final Map<String, String> embeddedFiles,
            Map<String, byte[]> embeddedByteArrays) throws MessagingException {

        MailServerConfig server = new MailServerConfig(smtpHostName, smtpPort, user, pwd, from,
                ssl);
        sendMessage(server, recipients, subject, message, embeddedFiles, embeddedByteArrays);
    }

//    public static void main(String args[]) throws Exception {
////     final String SMTP_HOST_NAME = "pm.top-chip.pl";
////     final String SMTP_PORT = "587";
////     final String emailUser = "pm@top-chip.pl";
////     final String emailPassword = "xxx";
////     final String emailFromAddress = "PM <pm@top-chip.pl>";
////     final boolean useSSL = false;
//
//        final String SMTP_HOST_NAME = "bssg.home.pl";
//        final String SMTP_PORT = "465";
//        final String emailUser = "info@biks.com.pl";
//        final String emailPassword = "xxx";
//        final String emailFromAddress = "BIKS team <info@biks.com.pl>";
//        final boolean useSSL = true;
//
//        final String emailMsgTxt = "Wiadomość <b>testowa</b>:<hr/><img src=\"cid:i1\"><hr/><br/>"
//                + "<img src='http://pl.memgenerator.pl/mem-image/zazolc-gesla-jazn'/><hr/>\nO, mógłże sęp <i>chlań</i> wyjść furtką bździn.";
//        final String emailSubjectTxt = "Test 1777 - zażółć gęślą jaźń 666";
//        //        final String[] sendTo = {"pmielanczuk@bssg.pl", "wildwezyr@gmail.com", "lukasz.biegniewski@innotion.pl"};
//        final String[] sendTo = {"tflorczak@bssg.pl"};
//
//        //ww: może być null
//        //        Map<String, byte[]> arrays = new HashMap<String, byte[]>();
//        //        RandomAccessFile a = new RandomAccessFile("c:/temp/aqq.pdf", "r");
//        //        byte[] b = new byte[(int) a.length()];
//        //        a.read(b);
//        //        arrays.put("Dokument Ex PRO Aqq.pdf", b);
//        //        Map<String, byte[]> arrays = null;
//        //ww: może być null
//        //        Map<String, String> files = new HashMap<String, String>();
//        //        files.put("i1", "c:/temp/Lighthouse.jpg");
//        Map<String, String> files = null;
//        sendMessage(SMTP_HOST_NAME, SMTP_PORT, emailUser, emailPassword,
//                sendTo, emailSubjectTxt, emailMsgTxt, emailFromAddress, useSSL, files);
//    }
    protected void addLog(String msg) {
        if (logger.isInfoEnabled()) {
            logger.info(msg);
        }
    }
}
