/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import simplelib.IParametrizedContinuation;

/**
 *
 * @author pmielanczuk
 */
public interface IAsyncDialogAction<B> {
    public void doIt(B bean, IParametrizedContinuation<String> onCompleteCont);
}
