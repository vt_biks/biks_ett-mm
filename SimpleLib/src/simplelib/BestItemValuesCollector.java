/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class BestItemValuesCollector<K, V extends Comparable<V>> {

    private Map<K, BestValueCollector<V>> m =
            new LinkedHashMap<K, BestValueCollector<V>>();

    public void add(K key, V val) {
        BestValueCollector<V> bvc = m.get(key);
        if (bvc == null) {
            bvc = new BestValueCollector<V>(BaseUtils.<V>makeComparator());
            m.put(key, bvc);
        }
        bvc.add(val);
    }

    public Iterable<K> getCollectedWords() {
        return m.keySet();
    }

    public V getBestWordValue(K word) {
        return m.get(word).getBestVal();
    }
}
