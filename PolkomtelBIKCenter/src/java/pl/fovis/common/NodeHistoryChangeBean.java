/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author ctran
 */
public class NodeHistoryChangeBean implements Serializable {

    public Date dateAdded;
    public String changeSource;
    public Integer attrId;
    public String attrName;
    public String typeAttr;
    public String oldValue;
    public String newValue;
    public String nodeName;
    public Integer nodeId;

}
