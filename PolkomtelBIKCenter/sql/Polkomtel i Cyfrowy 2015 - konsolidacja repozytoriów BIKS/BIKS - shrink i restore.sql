---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------


declare @log_name varchar(max);
select @log_name=name from sys.database_files where type = 1
exec('DBCC SHRINKFILE (N''' + @log_name + '''  , 0)')
go

declare @cmd_txt varchar(max) = 'DBCC SHRINKDATABASE (''' + db_name() + ''' ,0, NOTRUNCATE)'
exec(@cmd_txt)
go

declare @cmd_txt varchar(max) = 'DBCC SHRINKDATABASE (''' + db_name() + ''' ,0, TRUNCATEONLY)'
exec(@cmd_txt)
go

declare @cmd_txt varchar(max) = 'DBCC SHRINKDATABASE (''' + db_name() + ''' ,0)'
exec(@cmd_txt)
go



---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------



-- select top 1 substring(physical_name, 1, len(physical_name) - charindex('\', reverse(physical_name))) as files_path from master.sys.database_files where type = 0

-- restore filelistonly from DISK = 'C:\projekty\mssql-backups\Polkomtel i Cyfrowy 2015\BIKS_Polkomtel_2015_shrinked.bak';

restore DATABASE BIKS_Polkomtel_2015 from DISK = 'C:\projekty\mssql-backups\Polkomtel i Cyfrowy 2015\BIKS_Polkomtel_2015_shrinked.bak'
with 
  move 'boxi' to 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\BIKS_Polkomtel_2015.mdf',
  move 'boxi_log' to 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\BIKS_Polkomtel_2015.ldf';
go

