/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import pl.fovis.client.bikpages.IMyBIKSPage.ReachableMyBIKSTabCode;

/**
 *
 * @author mmastalerz
 */
public abstract class MyBIKSTabBase implements IMyBIKSTab {

    protected IMyBIKSPage myBIKSPage;

    @Override
    public String getHelpCaption() {
        return null;
    }

    @Override
    public void setMyBIKSPage(IMyBIKSPage myBIKSPage) {
        this.myBIKSPage = myBIKSPage;
    }

    @Override
    public ReachableMyBIKSTabCode getOptTabCode() {
        return null;
    }

    @Override
    public void updateHasNewItems(boolean hasNewItems) {
        //ww: no-op - domyślnie nic nie robimy, tylko news i favourites coś będą robić
    }

    @Override
    public boolean isVisible() {
        return true;
    }
}
