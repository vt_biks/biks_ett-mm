/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class DQCTestBean implements Serializable {

    public String longName;
    public String benchmarkDefinition;
    public Double errorThreshold;
    public String expectedResult;
    public String samplingMethod;
    public String additionalInformation;
    public Date createDate;
    public Date modifyDate;
    public Date suspendDate;

}
