/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence;

import commonlib.LameUtils;
import edu.emory.mathcs.backport.java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.swift.common.soap.confluence.ConfluenceSoapService;
import org.swift.common.soap.confluence.ConfluenceSoapServiceServiceLocator;
import org.swift.common.soap.confluence.RemoteBlogEntry;
import org.swift.common.soap.confluence.RemoteBlogEntrySummary;
import org.swift.common.soap.confluence.RemoteComment;
import org.swift.common.soap.confluence.RemotePage;
import org.swift.common.soap.confluence.RemotePageSummary;
import org.swift.common.soap.confluence.RemoteSearchResult;
import org.swift.common.soap.confluence.RemoteServerInfo;
import org.swift.common.soap.confluence.RemoteSpace;
import org.swift.common.soap.confluence.RemoteSpaceSummary;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;
import pl.bssg.metadatapump.confluence.wuwu.ConfluenceObjectContentRetrieverViaHttp;
import pl.bssg.metadatapump.confluence.wuwu.IConfluenceObjectContentRetriever;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.StackTraceUtil;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class ConfluenceConnector implements IConfluenceConnector {

    private static ILameLogger logger = LameUtils.getMyLogger();
    public static final String TYPE_PAGE = "page";
    public static final String TYPE_BLOG = "blogpost";
    public static final String KIND_SPACE = "ConfluenceSpace";
    public static final String KIND_PAGE = "ConfluencePage";
    public static final String KIND_BLOG = "ConfluenceBlog";
    public static final String CURRENT_STATUS = "current";
    public static final Map<String, String> SEARCH_TYPES = new HashMap<String, String>() {
        {
//            add("spacedesc");
            put(TYPE_PAGE, KIND_PAGE);
            put(TYPE_BLOG, KIND_BLOG);
        }
    };
    public static final int SEARCH_MAX_RESULTS = 100; // ew do przeneisienia do customowania w zakładce Admin
    protected ConfluenceConfigBean configBean;
    protected ConfluenceSoapService service;
    protected String token;
//    protected Map<String, String> idToTypeMap = new HashMap<String, String>();
//    protected Map<String, String> fullNameMap = new HashMap<String, String>(); // nieuzywane
    protected IConfluenceObjectContentRetriever contentRetriever;

    public ConfluenceConnector(ConfluenceConfigBean configBean) {
        this.configBean = configBean;
        this.contentRetriever = new ConfluenceObjectContentRetrieverViaHttp(
                BaseUtils.dropOptionalSuffix(configBean.url, "/") + "/",
                configBean.user, configBean.password);
    }

    public List<ConfluenceObjectBean> getSpaces() {
        List<ConfluenceObjectBean> objectList = new ArrayList<ConfluenceObjectBean>();
        try {
            RemoteSpaceSummary[] spaces = service.getSpaces(token);
            for (int i = 0; i < spaces.length; i++) {
                RemoteSpaceSummary oneSpace = spaces[i];
                String spaceId = oneSpace.getKey();
                ConfluenceObjectBean newBean = new ConfluenceObjectBean(spaceId, null, oneSpace.getName(), KIND_SPACE, null, oneSpace.getUrl(), null, null, spaceId + "|", spaceId, false, false);
                objectList.add(newBean);
//                idToTypeMap.put(spaceId, KIND_SPACE);
            }
        } catch (Exception e) {
//            throw new LameRuntimeException(e.getMessage());
            throw new LameRuntimeException("Error in getSpaces", e);
        }
        return objectList;
    }

    public List<ConfluenceObjectBean> getChildrenObjects(String parentId, String parentKind) {
        List<ConfluenceObjectBean> objectList = new ArrayList<ConfluenceObjectBean>();
        try {
//            long pageId;
//            try {
//                pageId = Long.parseLong(parentId);
//            } catch (Exception e) {
//                pageId = -1;
//            }
//            String parentType = idToTypeMap.get(parentId);
            if (parentKind.equals(KIND_PAGE)/* != -1*/) { // parent jest stroną - pobieramy podrzędne (children) strony
                RemotePageSummary[] childrenPages = service.getChildren(token, Long.parseLong(parentId));
                for (RemotePageSummary onePage : childrenPages) {
                    boolean hasNoChildren = false;
                    String onePageId = String.valueOf(onePage.getId());
                    if (configBean.checkChildrenCount == 1) {
                        RemotePageSummary[] childs = service.getChildren(token, onePage.getId());
                        hasNoChildren = childs.length == 0;
                    }
                    ConfluenceObjectBean newBean = new ConfluenceObjectBean(onePageId, String.valueOf(onePage.getParentId()), onePage.getTitle(), KIND_PAGE, null, onePage.getUrl(), null, null, null, onePage.getSpace(), hasNoChildren, false);
                    objectList.add(newBean);
//                    idToTypeMap.put(onePageId, KIND_PAGE);
                }
            } else { // jest bezpośrednio pod spejsem - pobieramy strony i blogi
                RemotePageSummary[] topLevelPages = service.getTopLevelPages(token, parentId);
                for (RemotePageSummary onePage : topLevelPages) {
                    String onePageId = String.valueOf(onePage.getId());
                    ConfluenceObjectBean newBean = new ConfluenceObjectBean(onePageId, parentId, onePage.getTitle(), KIND_PAGE, null, onePage.getUrl(), null, null, null, onePage.getSpace(), false, false);
                    objectList.add(newBean);
//                    idToTypeMap.put(onePageId, KIND_PAGE);
                }
                RemoteBlogEntrySummary[] blogEntries = service.getBlogEntries(token, parentId);
                for (RemoteBlogEntrySummary oneBlog : blogEntries) {
                    String oneBlogId = String.valueOf(oneBlog.getId());
                    String authorEmail = service.getUser(token, oneBlog.getAuthor()).getEmail();
                    ConfluenceObjectBean newBean = new ConfluenceObjectBean(oneBlogId, parentId, oneBlog.getTitle(), KIND_BLOG, null, oneBlog.getUrl(), oneBlog.getAuthor(), oneBlog.getPublishDate().getTime(), null, oneBlog.getSpace(), true, false);
                    //ConfluenceObjectBean newBean = new ConfluenceObjectBean(oneBlogId, parentId, oneBlog.getTitle(), KIND_BLOG, null, oneBlog.getUrl(), oneBlog.getAuthor(), authorEmail, oneBlog.getPublishDate().getTime(), null, oneBlog.getSpace(), true, false);
                    objectList.add(newBean);
//                    idToTypeMap.put(oneBlogId, KIND_BLOG);
                }
            }
        } catch (Exception e) {
            throw new LameRuntimeException(e.getMessage(), e);
        }
        return objectList;
    }

    public List<ConfluenceObjectBean> search(String text) {
//        System.out.println("ww:conf: search starts for text=" + text);
        try {
            List<ConfluenceObjectBean> objectList = new ArrayList<ConfluenceObjectBean>();
            try {
                System.out.println("Trying to search: " + text + ", SEARCH_MAX_RESULTS=" + SEARCH_MAX_RESULTS);

                HashMap<String, Object> searchParams = new HashMap<String, Object>();
//                HashSet<String> searchTypes = new HashSet<String>();
//                searchTypes.add(TYPE_PAGE);
//                searchTypes.add(TYPE_BLOG);
//                searchParams.put("type", searchTypes);
                searchParams.put("type", TYPE_PAGE);
//                searchParams.put("type", TYPE_PAGE + ";" + TYPE_BLOG);
                RemoteSearchResult[] results1 = service.search(token, text, searchParams, SEARCH_MAX_RESULTS);
                System.out.println("Search results1: " + results1.length);

                searchParams.put("type", TYPE_BLOG);
//                searchParams.put("type", TYPE_PAGE + ";" + TYPE_BLOG);
                RemoteSearchResult[] results2 = service.search(token, text, searchParams, SEARCH_MAX_RESULTS);
                System.out.println("Search results2: " + results2.length);

                @SuppressWarnings("unchecked")
                List<RemoteSearchResult> results = new ArrayList<RemoteSearchResult>(Arrays.asList(results1));
                @SuppressWarnings("unchecked")
                List<RemoteSearchResult> resultsTwo = new ArrayList<RemoteSearchResult>(Arrays.asList(results2));
                results.addAll(resultsTwo);

                Set<String> alreadyAddedObjects = new HashSet<String>();
                final String textFixed = text.replace("*", "");
                for (RemoteSearchResult result : results) {
                    final boolean entryMatched = result.getTitle().contains(textFixed) && SEARCH_TYPES.keySet().contains(result.getType());

//                    System.out.println("ww:conf: result entry: " + result.getTitle() + ", type=" + result.getType() + ": " + (entryMatched ? "" : "not ") + "matched");
                    if (entryMatched) {
                        long resultId = result.getId();
                        String resultIdAsString = String.valueOf(resultId);
                        String parentId;
                        boolean isBlog = false;
                        if (result.getType().equals(TYPE_PAGE)) { // page
                            RemotePage resultAsPage = service.getPage(token, resultId);
                            parentId = resultAsPage.getSpace();
//                        idToTypeMap.put(resultIdAsString, KIND_PAGE);
                        } else { // blog
                            isBlog = true;
                            RemoteBlogEntry resultAsBlog = service.getBlogEntry(token, resultId);
                            parentId = resultAsBlog.getSpace();
//                        idToTypeMap.put(resultIdAsString, KIND_BLOG);
                        }
                        // space - dodajemy zawsze
                        RemoteSpace space = service.getSpace(token, parentId);
                        String spaceId = space.getKey();
                        StringBuilder branchBuilder = new StringBuilder(spaceId + "|");
                        if (!alreadyAddedObjects.contains(spaceId)) {
                            ConfluenceObjectBean spaceBean = new ConfluenceObjectBean(spaceId, null, space.getName(), KIND_SPACE, null, space.getUrl(), null, null, branchBuilder.toString(), spaceId, false, false);
                            objectList.add(spaceBean);
                            alreadyAddedObjects.add(spaceId);
//                        idToTypeMap.put(spaceId, KIND_SPACE);
                        }
                        // ancestory dla stron
                        if (result.getType().equals(TYPE_PAGE)) {
                            RemotePageSummary[] ancestors = service.getAncestors(token, resultId);
                            for (int j = 0; j < ancestors.length; j++) {
                                RemotePageSummary anc = ancestors[j];
                                String ancestorId = String.valueOf(anc.getId());
                                branchBuilder.append(anc.getId()).append("|");
                                if (!alreadyAddedObjects.contains(ancestorId)) {
                                    ConfluenceObjectBean ancestorBean = new ConfluenceObjectBean(ancestorId, j == 0 ? spaceId : String.valueOf(anc.getParentId()), anc.getTitle(), KIND_PAGE, null, anc.getUrl(), null, null, branchBuilder.toString(), spaceId, false, false);
                                    objectList.add(ancestorBean);
                                    alreadyAddedObjects.add(ancestorId);
//                                idToTypeMap.put(ancestorId, KIND_PAGE);
                                }
                            }
                            if (ancestors.length != 0) {
                                parentId = String.valueOf(ancestors[ancestors.length - 1].getId());
                            }
                        }
                        // wyszukany obiekt
                        branchBuilder.append(resultId).append("|");
                        if (!alreadyAddedObjects.contains(resultIdAsString)) {
                            ConfluenceObjectBean newBean = new ConfluenceObjectBean(resultIdAsString, parentId, result.getTitle(), SEARCH_TYPES.get(result.getType()), null, result.getUrl(), null, null, branchBuilder.toString(), spaceId, isBlog, false);
                            objectList.add(newBean);
                            alreadyAddedObjects.add(resultIdAsString);
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Error in Confluence search:\n" + StackTraceUtil.getCustomStackTrace(e));
                throw new LameRuntimeException("error in confluence search", e);
            }

//            System.out.println("ww:conf: final result size=" + objectList.size());
            return objectList;
        } finally {
//            System.out.println("ww:conf: search ends for text=" + text);
        }
    }

    public List<List<ConfluenceObjectBean>> getContentForObjects(List<ConfluenceGroupBean> objectIds) {
        List<List<ConfluenceObjectBean>> objectListOfList = new ArrayList<List<ConfluenceObjectBean>>();
        for (ConfluenceGroupBean objectId : objectIds) {
            String objType = objectId.kind;//idToTypeMap.get(objectId);
            try {
                List<ConfluenceObjectBean> objectList = new ArrayList<ConfluenceObjectBean>();
                if (objType.equals(KIND_SPACE)) {
                    try {
                        // get actual space
                        ConfluenceObjectBean spaceContent = getSpaceContent(objectId, true);
                        if (spaceContent != null) {
                            objectList.add(spaceContent);
                        }
                    } catch (Exception e) {
                        // przestrzeń nie istnieje lub brak uprawnień - nie pobieramy
                        continue;
                    }
                    // get children pages
                    RemotePageSummary[] pages = service.getPages(token, objectId.id);
                    getPagesContent(pages, objectList, objectId);
                    // get blogs
                    RemoteBlogEntrySummary[] blogs = service.getBlogEntries(token, objectId.id);
                    getBlogsContent(blogs, objectList, objectId);
                } else if (objType.equals(KIND_PAGE)) {
                    long objectIdAsLong = Long.valueOf(objectId.id);
                    try {
                        // get actual page
                        ConfluenceObjectBean pageContent = getPageContent(objectIdAsLong, true, objectId);
                        if (pageContent != null) {
                            objectList.add(pageContent);
                        }
                    } catch (Exception e) {
                        // strona nie istnieje lub brak uprawnień - nie pobieramy
                        continue;
                    }
                    // get children pages
                    RemotePageSummary[] descendents = service.getDescendents(token, objectIdAsLong);
                    getPagesContent(descendents, objectList, objectId);
                } else if (objType.equals(KIND_BLOG)) {
                    try {
                        // get actual blog
                        ConfluenceObjectBean blogContent = getBlogContent(Long.parseLong(objectId.id), true, objectId);
                        if (blogContent != null) {
                            objectList.add(blogContent);
                        }
                    } catch (Exception e) {
                        // blog nie istnieje lub brak uprawnień - nie pobieramy
                        continue;
                    }
                }
                objectListOfList.add(objectList);
            } catch (Exception e) {
                if (logger.isErrorEnabled()) {
                    logger.error("Error with getting Confluence objects", e);
                }
                throw new LameRuntimeException(e.getMessage(), e);
            }
        }
        return objectListOfList;
    }

    public ConfluenceObjectBean getContentForOneObject(ConfluenceObjectBean bean) {
        ConfluenceObjectBean beanToReturn = null;
        try {
            long idAsLong = Long.parseLong(bean.id);
            if (bean.kind.equals(KIND_PAGE)) {
                beanToReturn = getPageContent(idAsLong, false, new ConfluenceGroupBean(bean.id, bean.kind, bean.groupId));
            } else if (bean.kind.equals(KIND_BLOG)) {
                beanToReturn = getBlogContent(idAsLong, false, new ConfluenceGroupBean(bean.id, bean.kind, bean.groupId));
            }
        } catch (Exception e) {
            throw new LameRuntimeException(e.getMessage(), e);
        }
        return beanToReturn;
    }

    protected ConfluenceObjectBean getSpaceContent(ConfluenceGroupBean id, boolean isRoot) throws Exception {
        RemoteSpace space = service.getSpace(token, id.id);
        String descr = space.getDescription();
        return new ConfluenceObjectBean(id.id, null, space.getName(), KIND_SPACE, descr != null ? descr : "", space.getUrl(), null, null, null, id.id, false, isRoot, null, id.groupId, null, id.treeId);
    }

    protected String getConfluenceContentHtml(long id) {
        return ConfluenceObjectContentRetrieverViaHttp.extractWikiContentFromFullHtml(contentRetriever.getHtmlFull(id), id);
    }

    protected ConfluenceObjectBean getPageContent(long id, boolean isRoot, ConfluenceGroupBean groupBean) throws Exception {
        RemotePage page = service.getPage(token, id);
        if (!page.getContentStatus().equals(CURRENT_STATUS)) {
            return null;
        }
        String space = page.getSpace();
        long parentId = page.getParentId();
        String creator = page.getCreator();
        //String authorEmail = service.getUser(token, page.getCreator()).getEmail();
        //System.out.println(user.getEmail());
//        return new ConfluenceObjectBean(String.valueOf(id), parentId == 0 ? space : String.valueOf(parentId), page.getTitle(), KIND_PAGE,
//                getConfluenceContentHtml(id),
//                //                page.getContent(),
//                page.getUrl(), creator, authorEmail, page.getCreated().getTime(), null, space, false, isRoot, getAttachments(id, space), groupBean.groupId, getUserFullName(creator), groupBean.treeId);
        return new ConfluenceObjectBean(String.valueOf(id), parentId == 0 ? space : String.valueOf(parentId), page.getTitle(), KIND_PAGE,
                getConfluenceContentHtml(id),
                //                page.getContent(),
                page.getUrl(), creator, page.getCreated().getTime(), null, space, false, isRoot, getAttachments(id, space), groupBean.groupId, getUserFullName(creator), groupBean.treeId);

    }

    protected ConfluenceObjectBean getBlogContent(long id, boolean isRoot, ConfluenceGroupBean groupBean) throws Exception {
        RemoteBlogEntry blog = service.getBlogEntry(token, id);
        String space = blog.getSpace();
        String author = blog.getAuthor();
//        String authorEmail = service.getUser(token, author).getEmail();
//        return new ConfluenceObjectBean(String.valueOf(id), space, blog.getTitle(), KIND_BLOG,
//                //                blog.getContent(),
//                getConfluenceContentHtml(id),
//                blog.getUrl(), author, authorEmail, blog.getPublishDate().getTime(), null, space, true, isRoot, getAttachments(id, space), groupBean.groupId, getUserFullName(author), groupBean.treeId);
        return new ConfluenceObjectBean(String.valueOf(id), space, blog.getTitle(), KIND_BLOG,
                //                blog.getContent(),
                getConfluenceContentHtml(id),
                blog.getUrl(), author, blog.getPublishDate().getTime(), null, space, true, isRoot, getAttachments(id, space), groupBean.groupId, getUserFullName(author), groupBean.treeId);
    }

    protected void getPagesContent(RemotePageSummary[] pages, List<ConfluenceObjectBean> objectList, ConfluenceGroupBean groupBean) throws Exception {
        for (int i = 0; i < pages.length; i++) {
            RemotePageSummary pageSummary = pages[i];
            ConfluenceObjectBean pageContent = getPageContent(pageSummary.getId(), false, groupBean);
            if (pageContent != null) {
                objectList.add(pageContent);
            }
        }
    }

    protected void getBlogsContent(RemoteBlogEntrySummary[] blogs, List<ConfluenceObjectBean> objectList, ConfluenceGroupBean groupBean) throws Exception {
        for (int i = 0; i < blogs.length; i++) {
            RemoteBlogEntrySummary blog = blogs[i];
            objectList.add(getBlogContent(blog.getId(), false, groupBean));
        }
    }

    public ConfluenceObjectBean publishArticleToConfluence(String articleName, String articleBody, String confluenceLogin, String confluencePassword, String parentObjId, String space) {
        try {
            RemotePage rp = new RemotePage();
            rp.setTitle(articleName);
            rp.setParentId(Long.valueOf(parentObjId));
            rp.setSpace(space);
            rp.setContent(articleBody);
//            rp.setContent(service.convertWikiToStorageFormat(token, articleBody));
            String tokenForUser = service.login(confluenceLogin, confluencePassword);
            RemotePage storePage = service.storePage(tokenForUser, rp);
            return getPageContent(storePage.getId(), true, new ConfluenceGroupBean());
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in publishing article to Confluence", e);
            }
        }
        return null;
    }

    protected List<ConfluenceObjectBean> getAttachments(long pageId, String space) {
        List<ConfluenceObjectBean> list = new ArrayList<ConfluenceObjectBean>();
//        TF: KOD DZIALA, ALE JEST ZAKOMENTOWANY, BO NIE POTRZEBUJEMY NA RAZIE POBIERAC ZALACZNIKOW
//        (I WYDLUZAC CZAS POBIERANIA)
//        try {
//            RemoteAttachment[] attachments = service.getAttachments(token, pageId);
//            for (int i = 0; i < attachments.length; i++) {
//                RemoteAttachment att = attachments[i];
//                list.add(new ConfluenceObjectBean(String.valueOf(att.getId()), String.valueOf(att.getPageId()), att.getFileName(), att.getContentType(), att.getComment(), att.getUrl(), att.getCreator(), att.getCreated().getTime(), null, space, true, true));
//            }
//        } catch (Exception e) {
//            System.out.println("Err: " + e.getMessage());
//        }
        return list;
    }

    protected String getUserFullName(String creator) {
//        TF: KOD DZIALA, ALE JEST ZAKOMENTOWANY, BO NIE POTRZEBUJEMY NA RAZIE TEGO POBIERAC
//        (I WYDLUZAC CZAS POBIERANIA)
//        String creatorFullName = fullNameMap.get(creator);
//        if (creatorFullName == null) {
//            try {
//                RemoteUser user = service.getUser(token, creator);
//                creatorFullName = user.getFullname();
//                fullNameMap.put(creator, creatorFullName);
//            } catch (Exception ex) {
//        if (logger.isErrorEnabled()) {
//                logger.error("Error in get user full name", ex);
//            }
//            }
//        }
//        return creatorFullName;
        return creator;
    }

    protected List<ConfluenceObjectBean> getComments(long pageId, String space) {
        List<ConfluenceObjectBean> list = new ArrayList<ConfluenceObjectBean>();
        try {
            RemoteComment[] comments = service.getComments(token, pageId);
            for (int i = 0; i < comments.length; i++) {
                RemoteComment comm = comments[i];
                list.add(new ConfluenceObjectBean(String.valueOf(comm.getId()), String.valueOf(comm.getPageId()), comm.getTitle(), null, comm.getContent(), comm.getUrl(), comm.getCreator(), comm.getCreated().getTime(), null, space, true, true));
            }
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in get comments", e);
            }
        }
        return list;
    }

    public void initializeService() {
        if (!configBean.url.contains("http://") && !configBean.url.contains("https://")) {
            configBean.url = "http://" + configBean.url;
        }
        ConfluenceSoapServiceServiceLocator fConfluenceSoapServiceGetter = new ConfluenceSoapServiceServiceLocator();
//                fConfluenceSoapServiceGetter.setConfluenceserviceV1EndpointAddress(server + endPoint);
        fConfluenceSoapServiceGetter.setConfluenceserviceV2EndpointAddress(configBean.url + ConfluencePump.endPoint);
        fConfluenceSoapServiceGetter.setMaintainSession(true);
//                fConfluenceSoapService = fConfluenceSoapServiceGetter.getConfluenceserviceV1();
        try {
            service = fConfluenceSoapServiceGetter.getConfluenceserviceV2();
        } catch (/*ServiceException*/Exception e) {
            throw new LameRuntimeException("Error with connect to Confluence service: " + e.getMessage(), e);
        }
    }

    public void login() {
        try {
            token = service.login(configBean.user, configBean.password);
        } catch (/*RemoteException*/Exception e) {
            throw new LameRuntimeException("Error with login to Confluence: " + e.getMessage(), e);
        }
    }

    public void logout() {
        try {
            service.logout(token);
            token = null;
        } catch (Exception e) {
            throw new LameRuntimeException("Error in logout: " + e.getMessage(), e);
        }
    }

    public boolean isServiceInitialized() {
        return service != null;
    }

    public boolean isLogged() {
        return token != null;
    }

    @Override
    public String testConnection() {
        String version = "";
        try {
            RemoteServerInfo info = service.getServerInfo(token);
            version = info.getMajorVersion() + "." + info.getMinorVersion();
        } catch (Exception ex) {
            throw new LameRuntimeException("Error in getting server info: " + ex.getMessage(), ex);
        }
        return version;
    }
}
