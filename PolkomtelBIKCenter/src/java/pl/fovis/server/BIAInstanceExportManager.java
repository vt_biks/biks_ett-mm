/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.UrlMakingUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import pl.bssg.metadatapump.common.SAPBOScheduleBean;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author bfechner
 */
public class BIAInstanceExportManager extends ExportManagerBase {

    protected BOXIServiceImpl service;

    public BIAInstanceExportManager(BOXIServiceImpl boxiService) {
        this.service = boxiService;
    }

    @Override
    protected Pair<String, List<Map<String, Object>>> getFileNamePrefixAndContent(HttpServletRequest req) {

        String fileName = I18n.biInstances.get();

        SAPBOScheduleBean bean = new SAPBOScheduleBean();
        bean.scheduleType = BaseUtils.tryParseInt(req.getParameter(BIKConstants.BIAINSTANCE_SCHEDULE_TYPE));
        bean.recurring = BaseUtils.tryParseInt(req.getParameter(BIKConstants.BIAINSTANCE_RECURRING));

        bean.id = BaseUtils.tryParseInt(req.getParameter(BIKConstants.BIAINSTANCE_ID));
        bean.parentId = BaseUtils.tryParseInt(req.getParameter(BIKConstants.BIAINSTANCE_PARENT_ID));
        bean.owner = req.getParameter(BIKConstants.BIAINSTANCE_OWNER);

        bean.ancestorId = BaseUtils.tryParseInt(req.getParameter(BIKConstants.BIAINSTANCE_ANCESTOR_ID));
        bean.intervalHours = BaseUtils.tryParseInt(req.getParameter(BIKConstants.BIAINSTANCE_INTERVAL_HOURS));
        bean.started = SimpleDateUtils.parseFromCanonicalString(req.getParameter(BIKConstants.BIAINSTANCE_STARTED));
        bean.ended = SimpleDateUtils.parseFromCanonicalString(req.getParameter(BIKConstants.BIAINSTANCE_ENDED));
        bean.reportsId = BaseUtils.splitBySep(req.getParameter(BIKConstants.BIAINSTANCE_REPORTS), ",");
        bean.ancestorsId = BaseUtils.splitBySep(req.getParameter(BIKConstants.BIAINSTANCE_FOLDERS), ",");
        List<String> connectionsNodeId = BaseUtils.splitBySep(req.getParameter(BIKConstants.BIAINSTANCE_CONNECTIONS), ",");
        bean.connectionsNodeId = new ArrayList<Integer>();
        for (String s : connectionsNodeId) {
            bean.connectionsNodeId.add(BaseUtils.tryParseInt(s));
        }
        List<Map<String, Object>> content = getBIAInstanceContent(bean);
        return new Pair<String, List<Map<String, Object>>>(UrlMakingUtils.encodeStringForUrlWW(fileName).replace("+", "%20"), content);

    }

    @Override
    protected String getFileFormat(HttpServletRequest req) {
        return req.getParameter(BIKConstants.BIAINSTANCE_RESULTS_EXPORT_PARAM_NAME);
    }

    public List<Map<String, Object>> getBIAInstanceContent(SAPBOScheduleBean filter) {
        List<Map<String, Object>> content = new ArrayList<Map<String, Object>>();
        List<SAPBOScheduleBean> instances = service.getBIASchedules(filter);
        for (SAPBOScheduleBean sr : instances) {
            Map<String, Object> item = new LinkedHashMap<String, Object>();
            item.put(I18n.scheduleID.get(), sr.id);
            item.put(I18n.scheduleStatus.get(), BIKCenterUtils.getStatus(sr.scheduleType));
            item.put(I18n.scheduleCykliczny.get(), BIKCenterUtils.getRecurring(sr.recurring));
            item.put(I18n.lokalizacja.get(), sr.location);
            item.put(I18n.scheduleNazwa.get(), sr.name);
            item.put(I18n.scheduleStart.get(), SimpleDateUtils.toCanonicalString(sr.started));
            item.put(I18n.scheduleKoniec.get(), SimpleDateUtils.toCanonicalString(sr.ended));
            item.put(I18n.scheduleCzasTrwania.get(), BIKCenterUtils.getDuration(sr));
            item.put(I18n.scheduleWlasciciel.get(), sr.owner);
            item.put(I18n.scheduleIDRaportu.get(), sr.parentId);
            item.put(I18n.scheduleUtworzono.get(), SimpleDateUtils.toCanonicalString(sr.creationTime));
            item.put(I18n.scheduleZmodyfikowano.get(), SimpleDateUtils.toCanonicalString(sr.modifyTime));
            item.put(I18n.scheduleCUID.get(), sr.cuid);
            item.put(I18n.scheduleMiejsceDocelowe.get(), sr.destination);
            item.put(I18n.scheduleMiejsceDoceloweSzczegoly.get(), sr.getDestinationPaths());
            item.put(I18n.scheduleBlad.get(), sr.errorMessage);
            item.put(I18n.serwer.get(), sr.server);
            item.put(I18n.scheduleZmodyfikowanoRaport.get(), sr.modifyTimeReport);
            item.put(I18n.scheduleUniverses.get(), sr.universes != null ? sr.universes.replaceAll("<br/>", "") : "");
            item.put(I18n.scheduleZmodyfikowanoSwiatobiektow.get(), sr.modifyTimeUniverse != null ? sr.modifyTimeUniverse.replaceAll("<br/>", "") : "");
            item.put(I18n.scheduleConnection.get(), sr.connections != null ? sr.connections.replaceAll("<br/>", "") : "");
            item.put(I18n.scheduleDatabaseEngine.get(), sr.databaseEngine != null ? sr.databaseEngine.replaceAll("<br/>", "") : "");
            content.add(item);
        }
        return content;
    }
}
