/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.ftp;

import java.util.List;

/**
 *
 * @author pmielanczuk
 */
public interface IFoxyFTPClient {

    public void connectAndLogin(String host, int port, String user, String password);

    public void changeDirectory(String dir);

    public String getCurrentDirectory();

    public List<IFoxyFTPFile> getFiles(String fileMask);

    public List<IFoxyFTPFile> getFiles();

    public void disconnect();

    public void download(String remoteFileName, String localFileName);

//    public String getCurrentDir();
}
