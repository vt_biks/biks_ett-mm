exec sp_check_version '1.0.90.2';
go

if exists( select * from bik_node where name='RODOS Artyku�y')
	update bik_node set 
	parent_node_id=(select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id  where bn.name='RODOS Artyku�y' and bt.code='FAQ')
		where parent_node_id=(select bn.id from bik_node bn 
		join bik_tree bt on bn.tree_id=bt.id where bn.name='Glosariusz'  )
	and is_deleted=0 and node_kind_id=(select id from bik_node_kind where code='Question')
else
begin
	insert into bik_node (parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,descr,obj_id,is_deleted,indexVer,is_built_in,old_name,chunked_ver,disable_linked_subtree,search_rank,vote_cnt,vote_val)
	select bn.id,(select id from bik_node_kind where code='CategoriesOfQuestions'),'RODOS Artyku�y',
	bt.id,null,'',null,null,0,0,0,null,0,0,0,0,0
	from bik_node bn join bik_tree bt on bn.tree_id=bt.id where bn.name='Glosariusz' and bt.code='FAQ'
	
	update bik_node set 
	parent_node_id=(select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id  where bn.name='RODOS Artyku�y' and bt.code='FAQ')
		where parent_node_id=(select bn.id from bik_node bn 
		join bik_tree bt on bn.tree_id=bt.id where bn.name='Glosariusz'  )
	and is_deleted=0 and node_kind_id=(select id from bik_node_kind where code='Question')
end


declare @tree_id int;

select @tree_id = id 
	from bik_tree
	where code='FAQ';
	
exec sp_node_init_branch_id @tree_id, null;


exec sp_update_version '1.0.90.2', '1.0.90.3';
go

