exec sp_check_version '1.0.22';
go


/****** Object:  StoredProcedure [dbo].[sp_check_version]    Script Date: 05/19/2011 11:04:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_check_version]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_check_version]
GO



create procedure [dbo].[sp_check_version] (@ver varchar(255))
as
	declare @version varchar(255)
begin	
	select @version = val from bik_app_prop where name = 'bik_ver';
	if(@version != @ver)
	begin
		raiserror(N'!!! Wrong version number %s. Current version is %s. !!!',--Message text
				20,
				-1,
				@ver, @version) with log;
				print N'Wrong number of version.Current version is %s';
	end
	
end

GO


insert into bik_app_prop (name, val)
values('indexVer', '0')
go

exec sp_update_version '1.0.22', '1.0.23';
go
