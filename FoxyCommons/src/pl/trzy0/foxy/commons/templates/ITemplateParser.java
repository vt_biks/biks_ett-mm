/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons.templates;

/**
 *
 * @author wezyr
 */
public interface ITemplateParser {
    public IParsedTemplate parse(String fileName, String content);
}
