exec sp_check_version '2.0.10';
GO
DELETE
FROM bik_app_prop
WHERE name = 'hideAttrAndNodeKindBtnsOnRight'


if not exists (select 1 from bik_app_prop where name='hideAddAttrBtnsOnRight' )
insert into bik_app_prop(name, val) values ('hideAddAttrBtnsOnRight', 'false')
else 
update bik_app_prop set val='false' where name='hideAddAttrBtnsOnRight'
go 


if not exists (select 1 from bik_app_prop where name='hideAddNodeKindBtnsOnRight' )
insert into bik_app_prop(name, val) values ('hideAddNodeKindBtnsOnRight', 'false')
else 
update bik_app_prop set val='false' where name='hideAddNodeKindBtnsOnRight'
go 

exec sp_update_version '2.0.10','2.0.11';
GO
