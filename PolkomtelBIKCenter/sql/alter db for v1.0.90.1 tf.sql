﻿exec sp_check_version '1.0.90.1';
go

update bik_node_kind set caption='Zapytanie raportowe' where code='ReportQuery'
update bik_node_kind set caption='MS Office' where code='Powerpoint'
update bik_node_kind set caption='Widok DWH' where code='TeradataView'
update bik_node_kind set caption='Procedura DWH' where code='TeradataProcedure'
update bik_node_kind set caption='Kolumna DWH' where code='TeradataColumn'
update bik_node_kind set caption='Tabela DWH' where code='TeradataTable'

delete from bik_authors where node_id=(select id from bik_node where name='aqq' and node_kind_id=(select id from bik_node_kind where code='Blog'))
delete from bik_joined_objs where src_node_id=(select id from bik_node where name='aqq' and node_kind_id=(select id from bik_node_kind where code='Blog')) or dst_node_id=(select id from bik_node where name='aqq' and node_kind_id=(select id from bik_node_kind where code='Blog'))
delete from bik_node where name='aqq' and node_kind_id=(select id from bik_node_kind where code='Blog')

if not exists( select * from bik_node where node_kind_id=(select id from bik_node_kind where code='AllUsersFolder') and is_deleted=0 and linked_node_id is null)
begin
    insert into bik_node(parent_node_id,node_kind_id,name,tree_id,is_built_in)
    values(null,(select id from bik_node_kind where code='AllUsersFolder'), 'Wszyscy użytkownicy', (select id from bik_tree where code='Users'),1)
    
    update bik_node
	set parent_node_id=(select id from bik_node where name='Wszyscy użytkownicy' and tree_id=(select id from bik_tree where code='Users'))
	where node_kind_id=(select id from bik_node_kind where code='User')
end
go

exec sp_update_version '1.0.90.1', '1.0.90.2';
go