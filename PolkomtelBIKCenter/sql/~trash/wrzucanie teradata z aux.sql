truncate table aaa_teradata_object
go


set IDENTITY_INSERT aaa_teradata_object ON
go


insert into aaa_teradata_object (id, parent_id, data_load_log_id,type, name, extra_info)
select -1000000 - id, case when parent_id IS null then null else -1000000- parent_id end, data_load_log_id,type, name, extra_info
from aaa_aux_teradata_object
go


set IDENTITY_INSERT aaa_teradata_object off
go
