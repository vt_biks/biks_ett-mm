exec sp_check_version '1.0.0';

ALTER TABLE bik_node  WITH CHECK ADD FOREIGN KEY(parent_node_id)
REFERENCES bik_node(id);

update bik_tree set is_metadata=1
where name in('Raporty', '�wiaty obiekt�w', 'Po��czenia','Teradata');

delete from bik_tree where name = 'Obszary biznesowe';

insert into bik_tree (name, is_metadata) values('Struktura organizacyjna', 0);
insert into bik_tree (name, is_metadata) values('Systemy IT', 0);
insert into bik_tree (name, is_metadata) values('Tezaurus', 0);


insert into bik_node_kind (code, caption) values('TaxonomyEntity', 'Jednostka taksonomiczna');

exec sp_insert_into_bik_node 'Raporty';
exec sp_insert_into_bik_node '�wiaty obiekt�w';
exec sp_insert_into_bik_node 'Po��czenia';
exec sp_insert_into_bik_node 'Teradata';

--wrzucam Tezaurusa
/******************************************************
*******************************************************
dla marcinka do wrzucania danych :D
*******************************************************
*******************************************************/

declare @si_id int, @si_parentid int, @si_kind varchar(max), @si_name varchar(max);
declare @tree_id int;
create table #pomTable (si_parentid int, id int, si_id int);
declare @i int = 1;
while @i<4
begin
print N'@i='+convert(varchar,@i);

	if(@i=1)
		select @tree_id =id 
		from bik_tree
		where name='Struktura organizacyjna';
	if(@i=2)
		select @tree_id =id 
		from bik_tree
		where name='Systemy IT';
	if(@i=3)
		select @tree_id =id 
		from bik_tree
		where name='Tezaurus';
		
	declare @node_kind_id int;
	
	
	DECLARE RepoEntity CURSOR FOR	select id as SI_ID, parent_id as SI_PARENTID, case when tree_num = 3 then 'Tezaurus' 
									else  '_BIZAREA_' end as SI_KIND, caption as SI_NAME 
								from aaa_tree_node
								where tree_num = @i;														
--otwieram kursor
	set nocount on;
	OPEN RepoEntity;

	FETCH NEXT FROM RepoEntity
	into @si_id, @si_parentid, @si_kind, @si_name;

WHILE @@FETCH_STATUS = 0
BEGIN
		if(isnull((	select id
					from bik_node_kind
					where code=@si_kind),'')!='')
		begin
				select @node_kind_id = id
				from bik_node_kind
				where code=@si_kind;
		end
		else
		begin
			insert into bik_node_kind (code,caption) values(@si_kind,@si_kind);
			select @node_kind_id = SCOPE_IDENTITY();
		end
				--nie by�o wcze�niej tego si_Id
		if(@si_id not in (select obj_id from bik_node where tree_id = @tree_id and @si_id=obj_id))
		begin
			insert into bik_node (parent_node_id, node_kind_id, name, tree_id, obj_id)
			values (null, @node_kind_id, @si_name, @tree_id, @si_id);
				
			insert into #pomTable (si_parentid, id, si_id) select @si_parentid, SCOPE_IDENTITY(), @si_id;
		end
				--by�o to si_id
		if(@si_id in (select obj_id from bik_node where tree_id = @tree_id and @si_id=obj_id))
		begin
			update bik_node 
			set parent_node_id = null, node_kind_id = @node_kind_id, name = @si_name,
				 tree_id = @tree_id
			where obj_id = @si_id and @tree_id = tree_id;
						
			insert into #pomTable (si_parentid, id, si_id) values (@si_parentid,(select id from bik_node where obj_id=@si_id and tree_id = @tree_id), @si_id)
		end

   FETCH NEXT FROM RepoEntity
   into @si_id, @si_parentid, @si_kind, @si_name;

END

CLOSE RepoEntity;
DEALLOCATE RepoEntity;

set nocount on;

	update bik_node 
	set parent_node_id=(select bk.id from bik_node bk where bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id)
	from #pomTable as pt
	where bik_node.id = pt.id and bik_node.tree_id = @tree_id;

	delete from bik_node
	where obj_id not in(select si_id from #pomTable) and tree_id = @tree_id;
	delete from #pomTable;
	set @i = @i+1;
end
				
drop table #pomTable;

/******************************************************
*******************************************************
*******************************************************
*******************************************************/



exec sp_update_version '1.0.0', '1.0.1';



