/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.event.logical.shared.AttachEvent.Handler;
import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.PaginationWidgetsHelper;
import pl.fovis.client.bikwidgets.TreeAndNodeKindTreeCheckBox;
import pl.fovis.client.dialogs.AdvancedSearchDialog;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.client.dialogs.SearchResultsExportOptionsDialog;
import pl.fovis.common.AttrSearchableBean;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.SearchContextBean;
import pl.fovis.common.SearchFullResult;
import pl.fovis.common.SearchFullResult.IndexCrawlStatus;
import pl.fovis.common.SearchResult;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.VisualMetricsUtils;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
@Deprecated
public class NewSearchBikPage extends BikPageMinimalBase implements INewSearchPage {

    public static final String NEW_SEARCH_TAB_ID = "NewSearch";
    private static final int NUMBER_OF_KIND_SELECTED = 3;
    public static final boolean KIND_FILTER_CHECKBOX_SEARCHES_ON_CLICK = true;
    protected static String lastOpExpr = "";
    protected Map<Integer, CheckBox> checkBoxMap = new HashMap<Integer, CheckBox>();
    protected int disableFireCheckBoxChange;
//    protected ITreeRowAndBeanStorage<Integer, Map<String, Object>, MenuNodeBean> storage =
//            new MapTreeRowAndBeanStorageBase<Integer, MenuNodeBean>("id", "parentId") {
//
//                @Override
//                protected Map<String, Object> convertBeanToRow(final MenuNodeBean mnb) {
//                    return NewSearchBikPage.this.convertBeanToRow(mnb);
//                }
//            };
//            new SimpleTreeRowStorage<Integer, Map<String, Object>>(
//            new TreeBrokerForMapNodes<Integer>("id", "parentId"));
    protected HorizontalPanel searchPhrasePnl;
    //protected InlineLabel searchPhraseTxt;
    protected TextBox searchPhraseTb = new TextBox();
    protected Map<Integer, CheckBox> currentKindsCheckboxes = new LinkedHashMap<Integer, CheckBox>();
    protected FlowPanel kindCheckBoxesPnl = new FlowPanel();
    protected DisclosurePanel filtrPnl;
    protected Label foundItemsHeader = new Label();
    protected HorizontalPanel foundItemsHeaderHp = new HorizontalPanel();
//    protected HorizontalPanel paginationHp = new HorizontalPanel();
//    protected BikCustomButton prevPageBtn = new BikCustomButton("«", "labelLinkWW", new IContinuation() {
//        public void doIt() {
//            paginationNavigate(pageNum - 1);
//        }
//    });
//    protected BikCustomButton nextPageBtn = new BikCustomButton("»", "labelLinkWW", new IContinuation() {
//        public void doIt() {
//            paginationNavigate(pageNum + 1);
//        }
//    });
//    Label pageNumOfCntLbl = new Label();
    protected boolean isInKindCheckBoxOnClick = false;
    protected CheckBox allKindsCb = new CheckBox();
//    protected CheckBox useLuceneCb = new CheckBox();
    //protected FlowPanel foundItemsPnl = new FlowPanel();
    protected FlexTable foundItemsGrid = new FlexTable();
    protected ScrollPanel foundItemsScrollPnl = new ScrollPanel();
    protected FlowPanel right = new FlowPanel();
    protected FlowPanel pnlSearchInProgress = new FlowPanel();
    protected InlineLabel lblSearchInProgress = new InlineLabel();
//    protected Button btnCancelSearchInProgress = new Button("Przerwij");
    protected PushButton btnCancelSearchInProgress = new PushButton(I18n.przerwij.get() /* I18N:  */);
    protected InlineLabel lblIndexCrawlStatusWarning = new InlineLabel();
    protected Request searchInProgressRequest;
    protected String ticket;
    protected ScrollPanel treeScrollPnl = new ScrollPanel();
    protected SearchContextBean lastSearchContext = new SearchContextBean();
//    protected int pageSize = 20;
//    protected int pageNum = 0;
//    protected int pageCnt;
    protected CheckBox cbSearchWithLucene = new CheckBox("Szukaj z Lucene");

    protected TreeAndNodeKindTreeCheckBox treeForSearch = new TreeAndNodeKindTreeCheckBox() {
        @Override
        protected void selectionChanged() {
            infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaWyszukiwanie.get(), true);
            callServiceToGetAttributes(new IContinuation() {

                @Override
                public void doIt() {
                    performSearchAndShow();
                }
            });
//            if (BIKClientSingletons.getSearchPageClickTreeNodeToSearch()) {
//                performSearchAndShow();
//            }
        }

        @Override
        protected boolean calcCheckBoxValueForTreeIdNode(Map<String, Object> row) {
            return !areAllDescendantsOneVal(row, false);
        }

        @Override
        protected boolean isExpandAllNodes() {
            return false;
        }

//        @Override
//        protected boolean mustHideReduntantGroups() {
//            return true;
//        }
    };
    //advancedSearchDialog
    protected AdvancedSearchDialog adSearchDlg = new AdvancedSearchDialog(this);
    protected PushButton adSearchBtn;
    protected DisclosurePanel criterionsPnl;
    protected boolean hasNewAttributes = true;
    protected BikCustomButton exportResultsBtn;
    protected PaginationWidgetsHelper pwh = new PaginationWidgetsHelper() {
        @Override
        protected void pageNumChanged() {
            performSearchAndShowInternal();
        }
    };

    protected BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();

    //    protected int pageNum = 0;
//    protected void paginationNavigate(int newPageNum) {
//        if (newPageNum >= pageCnt) {
//            newPageNum = pageCnt - 1;
//        }
//        if (newPageNum < 0) {
//            newPageNum = 0;
//        }
//        pageNum = newPageNum;
//        performSearchAndShowInternal();
//    }
    protected void performSearchAndShowEx(/*boolean useLucene*/) {
//        pageNum = 0;
        pwh.pageNum = 0;
        performSearchAndShowInternalEx(/*useLucene*/);
    }

    @Override
    public void performSearchAndShow() {
        performSearchAndShowEx(/*false*/);
    }

    protected boolean cancelRequestIfRunning() {
        return cancelRequestIfRunning(ticket);
    }

    protected boolean cancelRequestIfRunning(String ticket) {
        boolean isRuning = BIKClientSingletons.cancelRequestIfRunning(searchInProgressRequest, ticket);
        searchInProgressRequest = null;
        return isRuning;
    }

//    boolean useLuceneInSearch = false;
    protected void performSearchAndShowInternal() {
        performSearchAndShowInternalEx(/*useLuceneInSearch*/);
    }

    protected boolean useLucene() {

        if (BIKClientSingletons.isDeveloperMode()) {
            return cbSearchWithLucene.getValue();
        }
        return !BIKClientSingletons.appPropsBean.useFullTextIndex;
    }

    protected void performSearchAndShowInternalEx(/*boolean useLucene*/) {
        boolean useLuceneInSearch = useLucene();

        cancelRequestIfRunning();

        lblIndexCrawlStatusWarning.setVisible(false);
        //searchPhraseTxt.setText(BIKClientSingletons.getGlobalSearchTextBox().getText());
        Collection<Integer> treeIds = treeForSearch.getCheckedTreeIds();
        String textToSearch = BIKClientSingletons.getGlobalSearchTextBox().getText();
        String txtFix = BIKCenterUtils.normalizeTextForFTS(textToSearch, true);
        List<ISearchByAttrValueCriteria> sbavcs = adSearchDlg.getAttributesToSearch();
        lastOpExpr = adSearchDlg.getGroupingExpr();
        updateAttributeShowedList(sbavcs, lastOpExpr);
        if (!hasNewAttributes) {
            sbavcs = lastSearchContext.sbavcs;
        }

        if (treeIds.isEmpty() || txtFix == null) {
            lastSearchContext.searchKindAndCounts = null;
            final SearchFullResult sfr = new SearchFullResult();
            sfr.searchedText = "";
            sfr.searchedTreeIds = treeIds;
            sfr.sbavcs = sbavcs;
            BIKClientSingletons.getService().getFullTextCrawlCompleteStatus(useLucene(), new StandardAsyncCallback<IndexCrawlStatus>() {
                @Override
                public void onSuccess(IndexCrawlStatus result) {
                    sfr.indexCrawlStatus = result;
                    populateWithSearchFullResult(sfr);
                }

                @Override
                public void onFailure(Throwable caught) {
                    infoDialog.hideDialog();
                    super.onFailure(caught); //To change body of generated methods, choose Tools | Templates.
                }
            });
            return;
        }

        BIKClientSingletons.showInfo(I18n.trwaWyszukiwanie.get() /* I18N:  */ + "...");

        lblSearchInProgress.setText(I18n.trwaWyszukiwanie.get() /* I18N:  */ + " [" + txtFix + "]...");
        pnlSearchInProgress.setVisible(true);

        Set<Integer> kindsToSearchFor = treeForSearch.getCheckedNodeKindIdsOnTree();

        boolean isSameSearchPhrase = BaseUtils.safeEquals(lastSearchContext.query, txtFix);
        Set<Integer> kindIdsToExclude = new HashSet<Integer>();
        boolean calcKindCounts = true;

        for (Entry<Integer, CheckBox> e : currentKindsCheckboxes.entrySet()) {
            if (!e.getValue().getValue()) {
                kindIdsToExclude.add(e.getKey());
            }
        }
        if (lastSearchContext.searchKindAndCounts != null
                && isSameSearchPhrase
                && BaseUtils.safeEquals(lastSearchContext.kindsToSearchFor, kindsToSearchFor)
                && BaseUtils.safeEquals(lastSearchContext.treeIds, treeIds)
                && BaseUtils.safeEquals(lastSearchContext.sbavcs, sbavcs)) {
            calcKindCounts = false;
            //Window.alert("Will not recalc kindsAndCounts");
        }

        lastSearchContext.kindIdsToExclude = kindIdsToExclude;
//        Set<Integer> effectiveSearchKindIds = new HashSet<Integer>(kindsToSearchFor);
        //        effectiveSearchKindIds.removeAll(kindIdsToExclude);
        //kindsToSearchFor.removeAll(kindIdsToExclude);
        //System.out.println(sbavcs);
        ticket = BIKClientSingletons.getCancellableRequestTicket();

        infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaWyszukiwanie.get(), true);
        searchInProgressRequest = BIKClientSingletons.getService().newSearch(//                txtFix, pageNum * pageSize,
                //                pageSize, treeIds, kindsToSearchFor, kindIdsToExclude, calcKindCounts,
                txtFix, pwh.pageNum * pwh.pageSize,
                pwh.pageSize, treeIds, kindsToSearchFor, kindIdsToExclude, calcKindCounts,
                sbavcs,
                ticket, lastOpExpr, useLuceneInSearch,
                new StandardAsyncCallback<SearchFullResult>() {
                    @Override
                    public void onSuccess(SearchFullResult result) {
                        populateWithSearchFullResult(result);
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        infoDialog.hideDialog();
                        super.onFailure(caught); //To change body of generated methods, choose Tools | Templates.
                    }
                });
    }

    public String getId() {
        return NEW_SEARCH_TAB_ID;
    }

    protected Widget buildWidgets() {
        NewLookUtils.makeCustomPushButton(btnCancelSearchInProgress);
        HorizontalPanel main = new HorizontalPanel();

        FlowPanel left = new FlowPanel();
        left.setStyleName("EntityDetailsPane");
        //left.setHeight("100%");
        GWTUtils.setStyleAttribute(left, "padding" /* I18N: no */, "4px");

        //right.setHeight("100%");
        GWTUtils.setStyleAttribute(right, "padding" /* I18N: no */, "4px");
        right.setStyleName("EntityDetailsPane");

        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(left,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 8);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(right,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 8);

        main.add(left);
        main.add(right);

        main.setCellWidth(right, "100%");
        main.setCellHeight(left, "100%");
        main.setCellHeight(right, "100%");

        HTML searchTabsLbl = new HTML("<h3><b>" + I18n.przeszukiwaneZakladki.get() /* I18N:  */ + "</b></h3>");
        searchTabsLbl.addStyleName("searchPageBigTxt");
//         searchTabsLbl.addStyleName("BizDef-titleLbl");

        left.add(searchTabsLbl);

        left.add(treeScrollPnl);
        //treeScrollPnl.setHeight("100%");
        treeScrollPnl.add(treeForSearch.buildWidgets());
//        treeScrollPnl.add(treeWidget);
        treeScrollPnl.setWidth("350px");

        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(treeScrollPnl,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 4 - 28);

        searchPhrasePnl = new HorizontalPanel();
        searchPhrasePnl.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);

        searchPhrasePnl.setStyleName("searchPhrasePnl");
        InlineHTML searchPhraseHdr = new InlineHTML(I18n.szukanaFraza.get() /* I18N:  */ + ":");
        searchPhrasePnl.add(searchPhraseHdr);
        searchPhraseHdr.addStyleName("searchPageBigTxtP");
        searchPhraseTb.setStyleName("searchPhraseTb");
        searchPhraseTb.setWidth("250px");
        searchPhrasePnl.add(/*searchPhraseTxt = new InlineLabel()*/
                searchPhraseTb);
//        searchPhraseTb.addKeyUpHandler(new KeyUpHandler() {
//
//            public void onKeyUp(KeyUpEvent event) {
//                BIKClientSingletons.getGlobalSearchTextBox().setText(searchPhraseTb.getText());
//            }
//        });
//
//        searchPhraseTb.addAttachHandler(new Handler() {
//
//            public void onAttachOrDetach(AttachEvent event) {
//                BIKClientSingletons.addOrRemoveAdditionalSearchPagePhraseTb(event.isAttached(), searchPhraseTb);
//                //Window.alert("searchPhraseTb.attachHandler: is " + (event.isAttached() ? "attached" : "detached"));
//                searchPhraseTb.setText(BIKClientSingletons.getGlobalSearchTextBox().getText());
//            }
//        });
//
//        final IContinuation searchCont = new IContinuation() {
//
//            public void doIt() {
//                performSearchAndShow();
//            }
//        };

//        Image searchImg = new Image("images/searchBtn.gif" /* I18N: no */);
//        searchImg.setStyleName("searchImg");
//        searchImg.setTitle(I18n.szukaj.get() /* I18N:  */);
        PushButton searchBtn = new PushButton(I18n.szukaj.get());
        searchBtn.setHTML("<img  style=\"margin: 0px 4px 0px 0px; border: 0px none; float: left; padding: 0px 3px 0px 3px;\" src=\"images/loupe_search.png" + "\"/>" + searchBtn.getHTML());
        NewLookUtils.makeCustomPushButton(searchBtn);
        BIKClientSingletons.setupAdditionalSearchTextBoxEvents(searchPhraseTb, searchBtn);
        searchBtn.setTitle(I18n.szukaj.get() /* I18N:  */);
        searchPhrasePnl.setSpacing(3);
        searchPhrasePnl.add(searchBtn);

        if (BIKClientSingletons.isDeveloperMode()) {
//            PushButton searchWithLuceneBtn = new PushButton("Wyszukaj z Lucene", new ClickHandler() {
//
//                @Override
//                public void onClick(ClickEvent event) {
//                    performSearchAndShowEx(true);
//                }
//            });
//            NewLookUtils.makeCustomPushButton(searchWithLuceneBtn);
//            searchPhrasePnl.add(searchWithLuceneBtn);
            cbSearchWithLucene.setValue(!BIKClientSingletons.appPropsBean.useFullTextIndex);
            searchPhrasePnl.add(cbSearchWithLucene);
        }

//        searchImg.addClickHandler(new ClickHandler() {
//
//            public void onClick(ClickEvent event) {
//                searchCont.doIt();
//            }
//        });
//        GWTUtils.addEnterKeyHandler(searchPhraseTb, searchCont);
//        searchPhrasePnl.add(searchImg);
        adSearchBtn = new PushButton(I18n.zaawansowane.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                adSearchDlg.buildAndShowDialog(lastOpExpr);
            }
        });
        callServiceToGetAttributes(null);
        NewLookUtils.makeCustomPushButton(adSearchBtn);
        searchPhrasePnl.add(adSearchBtn);
//        searchPhrasePnl.add(BIKClientSingletons.createHintButton("Podpowiedź do wyszukiwarki")); // scalamy w jedną pomoc do wyszukiwarki

        //searchPhrasePnl.add(advancedSearch);
        pnlSearchInProgress.add(lblSearchInProgress);
        pnlSearchInProgress.add(btnCancelSearchInProgress);
        btnCancelSearchInProgress.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (cancelRequestIfRunning()) {
//                    BIKClientSingletons.cancelServiceRequest(searchInProgressRequestTicket);
//                    searchInProgressRequest.cancel();
//                    searchInProgressRequest = null;
                    pnlSearchInProgress.setVisible(false);
                    BIKClientSingletons.showInfo(I18n.wyszukiwanieZostaloPrzerwane.get() /* I18N:  */);
                }
            }
        });

        pnlSearchInProgress.setVisible(false);
        lblIndexCrawlStatusWarning.setVisible(false);
        searchPhrasePnl.add(lblIndexCrawlStatusWarning);
        searchPhrasePnl.add(pnlSearchInProgress);

        right.add(searchPhrasePnl);

        //criterionsPnl = new DisclosurePanel(I18n.criterions.get());
        criterionsPnl = new DisclosurePanel();
        criterionsPnl.setAnimationEnabled(true);
        setHTMLHeader(criterionsPnl, /*"<b>" + */ I18n.criterions.get()/* + "</b>"*//*, false*/);
        right.add(criterionsPnl);
        setDynamicHeightPanelUnderDisclosurePanel(criterionsPnl);
        criterionsPnl.setVisible(false);
        //        Label filtrLbl = new Label(I18n.filtrowaWynikowWgTypowObiektow.get() /* I18N:  */ + ":");
        //        kindCheckBoxesPnl.add(filtrLbl);
        filtrPnl = new DisclosurePanel(I18n.filtrowaWynikowWgTypowObiektow.get() /* I18N:  */);
        setDynamicHeightPanelUnderDisclosurePanel(filtrPnl);
        filtrPnl.setAnimationEnabled(true);
        filtrPnl.setStyleName("gwt-DisclosurePanel_header td a");
        allKindsCb.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                fixKindCheckBoxesByAll();
            }
        });
        allKindsCb.setStyleName("Filters-Checkbox");

        FlowPanel fp = new FlowPanel();

        fp.setStyleName("leftFloatingPanel paddingBottom4px");
        fp.add(allKindsCb);

        kindCheckBoxesPnl.add(fp);

        filtrPnl.setContent(kindCheckBoxesPnl);

        filtrPnl.setVisible(false);
        right.add(filtrPnl);

        foundItemsHeaderHp.setStyleName("clearBoth searchPageBigTxtP paddingTop8px");
        //foundItemsHeader.setStyleName("clearBoth searchPageBigTxtP paddingTop8px");
        foundItemsHeaderHp.setWidth("100%");
        foundItemsHeaderHp.add(foundItemsHeader);

//        paginationHp.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
//        paginationHp.add(prevPageBtn);
//        paginationHp.add(pageNumOfCntLbl);
//        paginationHp.add(nextPageBtn);
//        paginationHp.setVisible(false);
//        foundItemsHeaderHp.add(paginationHp);
//        foundItemsHeaderHp.setCellHorizontalAlignment(paginationHp, HasHorizontalAlignment.ALIGN_RIGHT);
        pwh.init();

        foundItemsHeaderHp.add(exportResultsBtn = createExportResultsBtn());
        exportResultsBtn.setVisible(false);
        foundItemsHeaderHp.add(pwh.paginationHp);
        foundItemsHeaderHp.setCellHorizontalAlignment(exportResultsBtn, HasHorizontalAlignment.ALIGN_RIGHT);
        foundItemsHeaderHp.setCellHorizontalAlignment(pwh.paginationHp, HasHorizontalAlignment.ALIGN_RIGHT);

        right.add(foundItemsHeaderHp);

        foundItemsGrid.setStyleName("gridJoinedObj");
        foundItemsGrid.setWidth("100%");

        foundItemsScrollPnl.setWidget(foundItemsGrid);

        foundItemsScrollPnl.addAttachHandler(new Handler() {

            public void onAttachOrDetach(AttachEvent event) {
                if (!event.isAttached()) {
                    BIKClientSingletons.getGUIResizer().removeCustomWidget(foundItemsScrollPnl);
                }
            }
        });

        //foundItemsPnl.setStyleName("clearBoth");
        right.add(foundItemsScrollPnl);

        return main; //tree.buildWidget(); //new Label("Aqq!");
    }

    protected void fixKindCheckBoxesByAll() {
        if (isInKindCheckBoxOnClick) {
            return;
        }

        isInKindCheckBoxOnClick = true;
        try {
            boolean allSet = allKindsCb.getValue();
            for (CheckBox cb : currentKindsCheckboxes.values()) {
                cb.setValue(allSet);
            }
//            updateFiltrTitle(/*false*/);

            if (KIND_FILTER_CHECKBOX_SEARCHES_ON_CLICK) {
                //ww:cb-click:
                performSearchAndShow();
            }
        } finally {
            isInKindCheckBoxOnClick = false;
        }
    }

    protected void fixAllKindsCheckBox() {
        if (isInKindCheckBoxOnClick) {
            return;
        }

        isInKindCheckBoxOnClick = true;
        try {
            boolean allSet = true;
            for (CheckBox cb : currentKindsCheckboxes.values()) {
                if (!cb.getValue()) {
                    allSet = false;
                    break;
                }
            }

            allKindsCb.setValue(allSet);
//            updateFiltrTitle(/*false*/);
        } finally {
            isInKindCheckBoxOnClick = false;
        }
    }

    protected void fixAllKindsCheckBoxAndMaybePerformSearch() {
        fixAllKindsCheckBox();

        if (KIND_FILTER_CHECKBOX_SEARCHES_ON_CLICK) {
            //ww:cb-click:
            performSearchAndShow();
        }
    }

    protected void updateIndexCrawlStatusInfo(IndexCrawlStatus ics) {
        boolean crawlComplete = ics == IndexCrawlStatus.CrawlComplete;
        if (!crawlComplete) {
            String msg = I18n.uWAGA.get() /* I18N:  */ + ": " + (ics == IndexCrawlStatus.NoIndex
                            ? I18n.brakIndeksuWyszukiwNieMoznaWyszu.get() /* I18N:  */ + "."
                            : I18n.indeksWyszukiwarkiJestAktualnie.get() /* I18N:  */ + " "
                            + (ics == IndexCrawlStatus.CrawlInProgress ? I18n.przebudowywany.get() /* I18N:  */ : I18n.zatrzymany.get() /* I18N:  */)
                            + ", " + I18n.wyszukiwMozeZwracacTerazCzesciow.get() /* I18N:  */);
            lblIndexCrawlStatusWarning.setText(msg);
        }
        lblIndexCrawlStatusWarning.setVisible(!crawlComplete);
    }

    protected void populateWithSearchFullResult(SearchFullResult sfr) {
        searchInProgressRequest = null;
        updateIndexCrawlStatusInfo(sfr.indexCrawlStatus);
        pnlSearchInProgress.setVisible(false);

        Map<Integer, Integer> kindIdsAndCounts;

        if (sfr.calcKindCounts || lastSearchContext.searchKindAndCounts == null) {
            kindIdsAndCounts = sfr.kindIdsAndCounts != null ? sfr.kindIdsAndCounts : new HashMap<Integer, Integer>();
        } else {
            kindIdsAndCounts = lastSearchContext.searchKindAndCounts;
        }
        sortByResult(kindIdsAndCounts);

        lastSearchContext.kindsToSearchFor = sfr.searchedKindIds == null ? null : new HashSet<Integer>(sfr.searchedKindIds);
        lastSearchContext.treeIds = sfr.searchedTreeIds == null ? null : new HashSet<Integer>(sfr.searchedTreeIds);
        lastSearchContext.searchKindAndCounts = kindIdsAndCounts;
        lastSearchContext.query = sfr.searchedText;
        lastSearchContext.sbavcs = (List<ISearchByAttrValueCriteria>) (sfr.sbavcs);
        exportResultsBtn.setVisible(!BaseUtils.isCollectionEmpty(sfr.foundItems));

        Iterator<Entry<Integer, CheckBox>> iterE = currentKindsCheckboxes.entrySet().iterator();
        while (iterE.hasNext()) {
            Entry<Integer, CheckBox> e = iterE.next();
            if (!kindIdsAndCounts.containsKey(e.getKey())) {
                kindCheckBoxesPnl.remove(e.getValue().getParent());
                iterE.remove();
            }
        }

        int allCnt = 0;
        int inResCnt = 0;

        Set<Integer> excludedKindIdsSet = new HashSet<Integer>();
        if (sfr.excludedKindIds != null) {
            excludedKindIdsSet.addAll(sfr.excludedKindIds);
        }

        for (Entry<Integer, Integer> kindIdAndCnt : kindIdsAndCounts.entrySet()) {
            int nodeKindId = kindIdAndCnt.getKey();
            int cnt = kindIdAndCnt.getValue();
            CheckBox cb = currentKindsCheckboxes.get(nodeKindId);
            if (cb == null) {
                cb = new CheckBox();
                cb.addStyleName("Filters-Checkbox");
                cb.addClickHandler(new ClickHandler() {
                    public void onClick(ClickEvent event) {
                        fixAllKindsCheckBoxAndMaybePerformSearch();
                    }
                });

                FlowPanel ifp = new FlowPanel();
                ifp.setStyleName("leftFloatingPanel paddingBottom4px");
                ifp.add(cb);
                //InlineLabel il = new InlineLabel(" ");
                //il.setStyleName("cb-spacer-special-style");
                //ifp.add(il);
                kindCheckBoxesPnl.add(ifp);
                currentKindsCheckboxes.put(nodeKindId, cb);
            }
            boolean isInRes = !excludedKindIdsSet.contains(nodeKindId);
            cb.setValue(isInRes);
            if (isInRes) {
                inResCnt += cnt;
            }

            //cb.setText(BIKClientSingletons.getNodeKindById(nodeKindId).caption + " (" + cnt + ")");
            NodeKindBean nkb = BIKClientSingletons.getNodeKindById(nodeKindId);
            cb.setHTML(BIKClientSingletons.getHtmlForNodeKindIconWithText(nkb.code, null, nkb.caption + " (" + cnt + ")"));

            allCnt += cnt;
        }
        lastSearchContext.allCnt = allCnt;

//        kindCheckBoxesPnl.clear();
//        kindCheckBoxesPnl.add(allKindsCb);
//        for (CheckBox cb : currentKindsCheckboxes.values()) {
//            FlowPanel ifp = new FlowPanel();
//            ifp.setStyleName("leftFloatingPanel paddingBottom4px");
//            ifp.add(cb);
//            //InlineLabel il = new InlineLabel(" ");
//            //il.setStyleName("cb-spacer-special-style");
//            //ifp.add(il);
//            kindCheckBoxesPnl.add(ifp);
//        }
        allKindsCb.setHTML("<b>" + I18n.wszystkie.get() /* I18N:  */ + "</b> (" + allCnt + ")");

        fixAllKindsCheckBox();

        //foundItemsPnl.clear();
        String foundItemsHeaderTxt;
//        pageCnt = 0;
//        pwh.pageCnt = 0;
        pwh.setPageCntByItemCnt(0);

        if (sfr.foundItems == null) {
            // brak wybranych zakładek lub frazy
            if (sfr.searchedText.isEmpty()) {
                foundItemsHeaderTxt = I18n.brakSzukanejFrazyWpiszCzegoSzuka.get() /* I18N:  */;
            } else {
                foundItemsHeaderTxt = "";
            }

            if (sfr.searchedTreeIds.isEmpty()) {
                foundItemsHeaderTxt
                        += (foundItemsHeaderTxt.isEmpty() ? "" : ". ")
                        + I18n.wybierzZakladkiTypyObiektowDoPrz.get() /* I18N:  */;
            }
        } else {
            //            pageCnt = (inResCnt + pageSize - 1) / pageSize;
//            pwh.pageCnt = (inResCnt + pwh.pageSize - 1) / pwh.pageSize;
            pwh.setPageCntByItemCnt(inResCnt);

            if (allCnt == 0) {
                foundItemsHeaderTxt = I18n.brakWynikowDla.get() /* I18N:  */ + ": [" + sfr.searchedText + "], " + I18n.wpiszInnaFrazeLubWybierzInneZakl.get() /* I18N:  */;
            } else if (inResCnt == 0) {
                foundItemsHeaderTxt = I18n.zaznaczWybraneTypyObiektowDoFilt.get() /* I18N:  */ + "\'";
            } else {
//                int lastDisplayedItemNum = Math.min((pageNum + 1) * pageSize, inResCnt);
                int lastDisplayedItemNum = Math.min((pwh.pageNum + 1) * pwh.pageSize, inResCnt);

                foundItemsHeaderTxt = I18n.wynikiDla.get() /* I18N:  */ + ": [" + sfr.searchedText + "], " + I18n.pozycje.get() /* I18N:  */ + ": "
                        //                        + (pageNum * pageSize + 1) + "-" + lastDisplayedItemNum
                        + (pwh.pageNum * pwh.pageSize + 1) + "-" + lastDisplayedItemNum
                        + " (" + I18n.lacznie.get() /* I18N:  */ + ": " + inResCnt + ")";
            }
        }

//        pageNumOfCntLbl.setText(
//                I18n.stronaZ.format(pageNum + 1, pageCnt) //
//        //I18n.strona.get() /* I18N:  */+ " " + (pageNum + 1) + " " +I18n.z_noSingleLetter.get() /* i18n:  */+ " " + pageCnt
//        );
//        prevPageBtn.setEnabled(pageNum > 0);
//        nextPageBtn.setEnabled(pageNum < pageCnt - 1);
//        paginationHp.setVisible(inResCnt > 0);
        pwh.refresh();

        //kindCheckBoxesPnl.setVisible(allCnt > 0);
        updateFiltrTitle(/*filtrPnl.isOpen()*/);
        filtrPnl.setVisible(allCnt > 0);

//        Label resultsLbl = new Label(foundItemsHeaderTxt);
//        foundItemsPnl.add(resultsLbl);
        //resultsLbl.setStyleName("searchPageBigTxt paddingTop8px");
        foundItemsHeader.setText(foundItemsHeaderTxt);

        foundItemsScrollPnl.setHeight("2px");
        foundItemsGrid.removeAllRows();

        //Window.alert("i co - jak jest?");
        setFoundItemsScrollPnlHeight();
        //setHeightMethod2(right, foundItemsScrollPnl);

        int row = 0;
        int hc = 0;
        foundItemsGrid.getFlexCellFormatter().setColSpan(row, hc, 2);
        foundItemsGrid.setText(row, hc++, I18n.nazwaObiektu.get() /* I18N:  */);
        //foundItemsGrid.setText(row, hc++, "Typ");
        //foundItemsGrid.setText(row, hc++, "Zakładka");
        foundItemsGrid.setText(row, hc++, /*"Użyteczność"*/ I18n.oceny.get() /* I18N:  */);
        foundItemsGrid.getCellFormatter().getElement(row, hc - 1).setTitle(I18n.uzytecznosc.get() /* I18N:  */);
        foundItemsGrid.setText(row, hc++, I18n.wystepowanie.get() /* I18N:  */);
        foundItemsGrid.getFlexCellFormatter().setColSpan(row, hc, 2);
        foundItemsGrid.setText(row, hc++, I18n.akcje.get() /* I18N:  */);
        foundItemsGrid.getRowFormatter().setStyleName(row, "related-obj-header-row");
        row++;

        if (sfr.foundItems != null) {
            Map<Integer, String> ancestorNames = new HashMap<Integer, String>();
            if (sfr.ancestorNames != null) {
                ancestorNames.putAll(sfr.ancestorNames);
            }
            for (SearchResult item : sfr.foundItems) {
                ancestorNames.put(item.nodeId, item.name);

                int col = 0;
                //foundItemsPnl.add(new Label(item.name + " [" + item.nodeKindCaption + "] (" + item.treeName + ")"));
                foundItemsGrid.setWidget(row, col++, BIKClientSingletons.createIconForNodeKind(item.nodeKindCode, item.treeCode,
                        item.treeName + ": " + item.nodeKindCaption));
                GWTUtils.setStyleAttribute(foundItemsGrid.getFlexCellFormatter().getElement(row, col - 1), "width" /* I18N: no */, "16px");

                //ww: pełna ścieżka zamiast samej nazwy psuje układ i czytelność tabelki z wynikami
                //foundItemsGrid.setText(row, col++, buildAncestorPath(item.branchIds, sfr.ancestorNames));
                //ww: imho lepiej dać samą nazwę i ścieżkę w hincie (title)
//                foundItemsGrid.setText(row, col++, item.name);
                foundItemsGrid.setWidget(row, col++, BIKClientSingletons.createLabelLookupButton(item.name, item.treeCode, item.nodeId, null, true));

                foundItemsGrid.getCellFormatter().getElement(row, col - 1).setTitle(buildAncestorPath(item.branchIds, ancestorNames, item.treeCode));

//            if (false && item.voteCnt == 0) {
//                foundItemsGrid.setText(row, col++, "(brak)");
//            } else {
//                double vv = item.voteCnt == 0 ? 0 : (double) item.voteSum / item.voteCnt;
//                int fullStars = (int) (Math.round(vv * 2) / 2);
//                boolean addHalf = vv - fullStars >= 0.5;
//
//                StringBuilder sb = new StringBuilder();
//                for (int i = 0; i < 5; i++) {
//                    sb.append("<img src='images/starsmall").append(i < fullStars ? "gold" : (addHalf && i == fullStars ? "halfgold" : "grey")).append(".gif'/>");
//                }
//                foundItemsGrid.setHTML(row, col++, sb.toString());
//            }
                //foundItemsGrid.setText(row, col++, item.nodeKindCaption);
                //foundItemsGrid.setText(row, col++, item.treeName);
                int allVoteCnt = item.voteCnt + item.voteSum;
                foundItemsGrid.setText(row, col++, (allVoteCnt != 0) ? ("" + item.voteSum + " " + I18n.z_noSingleLetter.get() /* I18N:  */ + " " + allVoteCnt) : "(" + I18n.brakOcen2.get() /* I18N:  */ + ")");
                if (allVoteCnt != 0) {
                    foundItemsGrid.getCellFormatter().getElement(row, col - 1).setTitle(item.voteSum + " " + I18n.z_noSingleLetter.get() /* I18N:  */ + " " + allVoteCnt + " " + I18n.osobUznaloToZaUzyteczne.get() /* I18N:  */);
                }
                //foundItemsGrid.getCellFormatter().getElement(row, col - 1).setTitle("głosów: " + item.voteCnt /*+ ", zgodność frazy: " + item.ftsRank*/);
                //foundItemsGrid.setText(row, col++, item.voteSum + " / " + item.voteCnt);

                Pair<String, String> p = getAttrListShortAndFull(item.foundInAttrCaptions);
                foundItemsGrid.setText(row, col++, p.v1);
                foundItemsGrid.getCellFormatter().getElement(row, col - 1).setTitle(p.v2);

                foundItemsGrid.setWidget(row, col++, BIKClientSingletons.createLookupButton(item.treeCode, item.nodeId, null, true));
                GWTUtils.setStyleAttribute(foundItemsGrid.getFlexCellFormatter().getElement(row, col - 1), "width" /* I18N: no */, "16px");
                foundItemsGrid.setWidget(row, col++, BIKClientSingletons.createFollowButton(item.treeCode, item.nodeId, true));
                GWTUtils.setStyleAttribute(foundItemsGrid.getFlexCellFormatter().getElement(row, col - 1), "width" /* I18N: no */, "16px");

                foundItemsGrid.getRowFormatter().setStyleName(row, row % 2 != 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
                row++;
            }

            BIKClientSingletons.showInfo(I18n.wyszukiwZakonczoWszystkiWynikow.get() /* I18N:  */ + ": " + allCnt);
        }

        infoDialog.hideDialog();
        //foundItemsPnl.add(foundItemsGrid);
//        StringBuilder sb = new StringBuilder();
//
//        boolean first = true;
//        for (Entry<Integer, Integer> e : sfr.kindIdsAndCounts.entrySet()) {
//            if (first) {
//                first = false;
//            } else {
//                sb.append(", ");
//            }
//
//            sb.append(BIKClientSingletons.getNodeKindById(e.getKey()).caption).append(" (").append(e.getValue()).append(")");
//        }
//
//        Window.alert("sfr.rows=" + sfr.foundItemsPnl.size() + ", kinds=" + sb.toString());
    }

    protected Pair<String, String> getAttrListShortAndFull(List<String> attrNames) {
        final int attrNameCnt = BaseUtils.collectionSizeFix(attrNames);

        List<String> translatedAttrNames = new ArrayList<String>(attrNameCnt);
        if (attrNameCnt > 0) {
            for (String attrName : attrNames) {
                translatedAttrNames.add(BIKClientSingletons.getTranslatedAttributeName(attrName));
            }
        }

        final int shortListCnt = 3;
        final String sep = ", ";

        String longList = BaseUtils.mergeWithSepEx(translatedAttrNames, sep);
        String shortList;

        if (attrNameCnt > shortListCnt) {
            shortList = BaseUtils.mergeWithSepEx(translatedAttrNames.subList(0, shortListCnt - 2), sep)
                    + " + " + I18n.innych.get() /* I18N:  */ + ": " + (attrNameCnt - (shortListCnt - 1));
        } else {
            shortList = longList;
        }

        return new Pair<String, String>(shortList, longList);
    }

    //ww: wersja z ręcznym budowaniem wyniku - klecenie stringów - z użyciem
    // StringBuildera - bardzo dobrze, ale można bez klepania pętli (zawsze jest
    // taka sama), a zamiast tego - użyć Projectora i BaseUtils.mergeWithSepEx
    // patrz wersja poniżej.
//    protected String buildAncestorPath(String branchIds, Map<Integer, String> nodeNames) { //"»"
//
//        List<Integer> ancestorIds = BIKCenterUtils.splitBranchIdsIntoColl(branchIds, new ArrayList<Integer>());
//
//        StringBuilder sb = new StringBuilder();
//        boolean isFirst = true;
//        for (Integer id : ancestorIds) {
//            if (isFirst) {
//                isFirst = false;
//            } else {
//                sb.append(" » ");
//            }
//            sb.append(nodeNames.get(id));
//        }
//
//        return sb.toString();
//    }
    protected String buildAncestorPath(String branchIds, final Map<Integer, String> nodeNames, String treeCode) { //"»"

        return BIKClientSingletons.getMenuPathForTreeCode(treeCode) + BIKCenterUtils.buildAncestorPathByNodeNames(branchIds, nodeNames, null);
//        List<Integer> ancestorIds = BIKCenterUtils.splitBranchIdsEx(branchIds, false);
//
//        return BaseUtils.mergeWithSepEx(ancestorIds, " » ", new BaseUtils.Projector<Integer, String>() {
//
//            public String project(Integer val) {
//                return nodeNames.get(val);
//            }
//        });
    }

//    public String getCaptionDeprecated() {
//        return I18n.szukaj.get() /* I18N:  */;
//    }
    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaWyszukiwanie.get(), true);

        adSearchDlg.prepareCriteria(lastOpExpr);
        performSearchAndShow();
    }

//    public void addOrRemoveAdvancedSearch() {
//        if (isAddedField) {
//            isAddedField = false;
//        } else {
//            if (advancedSearchTable.getRowCount() == 1) {
//                advancedSearchTable = addAttributeToSearch(advancedSearchTable);
//            }
//            isAddedField = true;
//        }
//    }
    public void callServiceToGetAttributes(final IContinuation optCon) {
        adSearchBtn.setEnabled(false);
        BIKClientSingletons.getService().getAttributesBySelectedTreeNode(treeForSearch.getCheckedTreeIds(), treeForSearch.getCheckedNodeKindIdsOnTree(), new AsyncCallback<List<AttrSearchableBean>>() {

            @Override
            public void onFailure(Throwable caught) {
                BIKClientSingletons.showInfo(I18n.blad.get() /* I18N:  */ + "...");
                infoDialog.hideDialog();
            }

            @Override
            public void onSuccess(List<AttrSearchableBean> result) {
                adSearchDlg.updateAttributes(result, lastOpExpr);
                adSearchBtn.setEnabled(!BaseUtils.isCollectionEmpty(result));
                updateAttributeShowedList(adSearchDlg.getAttributesToSearch(), lastOpExpr = adSearchDlg.getGroupingExpr());
                if (optCon != null) {
                    optCon.doIt();
                }
            }
        });
    }

    @Override
    public void updateAttributeShowedList(List<ISearchByAttrValueCriteria> sbavcs, String groupingExpr) {
        if (sbavcs != null) {
            VerticalPanel content = new VerticalPanel();
            Map<Integer, String> placeHolderValues = new HashMap<Integer, String>();
            for (int i = 0; i < sbavcs.size(); i++) {

//                HTML attrLbl = new HTML();
//                switch (attr.getValuesCount()) {
//                    case 0:
//                        SearchByAttrValueCriteriaNotEmpty attrNotEmpty = (SearchByAttrValueCriteriaNotEmpty) attr;
//                        attrLbl.setHTML("<b>" + attrNotEmpty.attrName + "</b> " + I18n.noValueAttribute.get());
//                        break;
//                    case 1:
//                        SearchByAttrValueCriteriaSingleValue attrSingleVal = (SearchByAttrValueCriteriaSingleValue) attr;
//                        attrLbl.setHTML("<b>" + attrSingleVal.attrName + "</b> " + I18n.singleValueAttribute.get() + ": <b>" + attrSingleVal.value + "</b>");
//                        break;
//                    case 2:
//                        SearchByAttrValueCriteriaBetween attrBetween = (SearchByAttrValueCriteriaBetween) attr;
//                        attrLbl.setHTML("<b>" + attrBetween.attrName + "</b> " + I18n.multipleValueAttribute.get() + ": <b>" + attrBetween.lowerValue + "</b> -> <b>" + attrBetween.higherValue + "</b>");
//                        break;
//                }
                ISearchByAttrValueCriteria attr = sbavcs.get(i);
                placeHolderValues.put(i + 1, attr.getCriteriaMessage());
            }
            content.add(new HTML(BaseUtils.replacePlaceHoldersWithValues(groupingExpr, "(", ")", placeHolderValues)));
            criterionsPnl.setContent(content);
        }
        criterionsPnl.setVisible(sbavcs != null);
    }

    private void updateFiltrTitle(/*boolean isPanelOpen*/) {
        StringBuilder title;
        if (allKindsCb.getValue()) {
            title = new StringBuilder(I18n.filtrowaWynikowWgTypowObiektow.get() + ": " + allKindsCb.getHTML());
        } else {
            int selectedCount = 0, cnt = 0;
            title = new StringBuilder(/*"<b>" +*/I18n.filtrowaWynikowWgTypowObiektow.get() /*+ "</b>: "*/);

            for (CheckBox cb : currentKindsCheckboxes.values()) {
                if (cb.getValue()) {
                    selectedCount++;
                    if (cnt < NUMBER_OF_KIND_SELECTED) {
                        if (cnt == 0) {
                            title.append(cb.getText());
                        } else {
                            title.append(",").append(cb.getText());
                        }
                        cnt++;
                    }
                }
            }
            if (selectedCount > NUMBER_OF_KIND_SELECTED) {
                title.append(" ").append(I18n.and.get()).append(" <i>").append(selectedCount - NUMBER_OF_KIND_SELECTED).append(" ");
                if (selectedCount == NUMBER_OF_KIND_SELECTED + 1) {
                    title.append(I18n.inna.get()).append("</i>");
                } else if (selectedCount > NUMBER_OF_KIND_SELECTED + 1) {
                    title.append(I18n.innych.get()).append("</i>");
                }
            }
        }
        setHTMLHeader(filtrPnl, title.toString()/*, isPanelOpen*/);
    }

    private void setFoundItemsScrollPnlHeight() {
        int height = VisualMetricsUtils.getDynamicHeightForLowerWidget(right, foundItemsScrollPnl);
        BIKClientSingletons.getGUIResizer().setResizeSensitiveCustomWidgetHeight(foundItemsScrollPnl,
                height < 0 ? 0 : height);
    }

    private void setDynamicHeightPanelUnderDisclosurePanel(final DisclosurePanel panel) {

        VisualMetricsUtils.getDynamicHeightPanelUnderDisclosurePanel(panel, foundItemsScrollPnl, new IParametrizedContinuation<Integer>() {
            @Override
            public void doIt(Integer height) {
                changeHeader(panel);
                BIKClientSingletons.getGUIResizer().setResizeSensitiveCustomWidgetHeight(foundItemsScrollPnl,
                        height < 0 ? 0 : height);
            }
        });

    }

    protected void changeHeader(DisclosurePanel panel) {
        String header = panel.getHeader().toString();
        header = panel.isOpen() ? header.replace("close", "open") : header.replace("open", "close");
        panel.setHeader(new HTML(header));
    }

    private void setHTMLHeader(final DisclosurePanel panel, final String header/*, boolean isPanelOpen*/) {

        panel.setHeader(new HTML("<img src='images/" + (panel.isOpen() ? "open" : "close") + "DefaultDisclosurePanel.png' align=\"top\"/>" + header));
//        panel.addOpenHandler(new OpenHandler<DisclosurePanel>() {
//
//            @Override
//            public void onOpen(OpenEvent<DisclosurePanel> event) {
//                event.getTarget().setHeader(new HTML("<img src='images/openDefaultDisclosurePanel.png' align=\"top\"/>" + header.toString()));
//
//            }
//        });
//        panel.addCloseHandler(new CloseHandler<DisclosurePanel>() {
//
//            @Override
//            public void onClose(CloseEvent<DisclosurePanel> event) {
//                event.getTarget().setHeader(new HTML("<img src='images/closeDefaultDisclosurePanel.png' align=\"top\"/>" + header.toString()));
//                BIKClientSingletons.registerResizeSensitiveWidgetByHeight(foundItemsScrollPnl,
//                        BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 90);
//
//            }
//        });
        //criterionsPnl.getHeaderTextAccessor().setText("Change");
        panel.setStyleName("gwt-DisclosurePanel_header td a");
    }

    private void sortByResult(Map<Integer, Integer> kindIdsAndCounts) {
        List<Entry<Integer, Integer>> listCbs = new ArrayList<Entry<Integer, Integer>>(kindIdsAndCounts.entrySet());

        Collections.sort(listCbs, new Comparator<Entry<Integer, Integer>>() {

            @Override
            public int compare(Entry<Integer, Integer> a, Entry<Integer, Integer> b) {
                //System.out.println(aValue + " " + bValue);
                if (a.getValue() < b.getValue()) {
                    return 1;
                }
                if (a.getValue() > b.getValue()) {
                    return -1;
                }
                return 0;
            }
        });
        kindIdsAndCounts.clear();
        for (Entry<Integer, Integer> pair : listCbs) {
            kindIdsAndCounts.put(pair.getKey(), pair.getValue());
        }
    }

    public TextBox getSearchPhraseTb() {
        return searchPhraseTb;
    }

    public boolean isHasNewAttributes() {
        return hasNewAttributes;
    }

    public void setHasNewAttributes(boolean hasNewAttributes) {
        this.hasNewAttributes = hasNewAttributes;
    }

    private BikCustomButton createExportResultsBtn() {
        return new BikCustomButton(I18n.eksportWynikow.get() /* I18N:  */, "importCsv" /* I18N: no */,
                new IContinuation() {

                    public void doIt() {
                        final String ticketForFullSearch = BIKClientSingletons.getCancellableRequestTicket();
                        BIKClientSingletons.showInfo(I18n.wTrakciePrzygotowaniaWynikow.get());
                        final SearchResultsExportOptionsDialog exportDialog = new SearchResultsExportOptionsDialog(ticketForFullSearch, lastSearchContext.query) {

                            @Override
                            protected void doCancel() {
                                infoDialog.hideDialog();
                                cancelRequestIfRunning(ticketForFullSearch);
                                callServiceToClearSearchResultsCache(ticketForFullSearch);
                            }

                            @Override
                            protected void doAction() {
//                                final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                                infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaPrzygotowanieEksportu.get(), true);
                                BIKClientSingletons.getService().performFullSearch(lastSearchContext.query, lastSearchContext.allCnt, lastSearchContext.treeIds,
                                        lastSearchContext.kindsToSearchFor, lastSearchContext.kindIdsToExclude,
                                        lastSearchContext.calcKindCounts, lastSearchContext.sbavcs,
                                        ticketForFullSearch, adSearchDlg.getGroupingExpr(),
                                        useLucene(),
                                        new StandardAsyncCallback<String>() {

                                            @Override
                                            public void onSuccess(String token) {
                                                infoDialog.hideDialog();
                                                Window.open(generateDownloadLink(), "_blank", "");
                                            }

                                            @Override
                                            public void onFailure(Throwable caught) {
                                                infoDialog.hideDialog();
                                                super.onFailure(caught);
                                            }
                                        });
                            }
                        };
                        exportDialog.buildAndShowDialog();
                    }
                });
    }

    private void callServiceToClearSearchResultsCache(String ticketForFullSearch) {
        BIKClientSingletons.getService().clearSearchResultsCache(ticketForFullSearch, new StandardAsyncCallback<Void>() {

            @Override
            public void onSuccess(Void result) {
                infoDialog.hideDialog();
//                BIKClientSingletons.showInfo(I18n.sukces.get());
            }
        });
    }
}
