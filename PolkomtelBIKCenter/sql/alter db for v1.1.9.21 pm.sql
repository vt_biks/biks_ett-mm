exec sp_check_version '1.1.9.21';
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

-- [TSB-FIX:2]
--ww: naprawiamy problem technicznej sp�jno�ci bazy
-- dziecko nie jest usuni�te, a rodzic owszem - to b��d
declare @rc int = 1

-- trzeba to robi� w p�tli, bo dzieci mog� mie� dzieci itd.
while @rc > 0 begin
  update bik_node
  set is_deleted = 1
  where 
  parent_node_id is not null
  and is_deleted = 0 and 
  exists (select 1 from bik_node p where p.id = bik_node.parent_node_id and p.is_deleted = 1)

  set @rc = @@rowcount
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

--ww: gdy jest wersja demo to searchPageClickTreeNodeToSearch ma by� true
-- wpp ma by� false (tzn. zgodno�� z isDemoVersion)
-- brak info o wersji demo to znak, �e nie jest wersja demo
if not exists (select 1 from bik_app_prop where name = 'searchPageClickTreeNodeToSearch') begin
  declare @val varchar(255) = coalesce((select val from bik_app_prop where name = 'isDemoVersion'), 'false')
  exec sp_set_app_prop 'searchPageClickTreeNodeToSearch', @val
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

-- poprawka [TSB-FIX:8]
--ww: podlinkowany nie jest usuni�ty, a orygina� jest usuni�ty
-- to b��d!
update bik_node
set is_deleted = 1
where linked_node_id is not null and is_deleted = 0 and
exists (select 1 from bik_node ln where ln.id = bik_node.linked_node_id and ln.is_deleted = 1)
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

-- poprawka [TSB-FIX:3]
--ww: naprawiamy z�e lub puste branch_ids w bik_node
-- ze wzgl�du na to, �e rodzic i dziecko (w og�lno�ci przodek i potomek)
-- mog� mie� z�e (np. puste) branch_ids, a procedura sp_node_init_branch_id
-- bazuje na warto�ci z rodzica, to lepiej ca�e drzewa naprawi�, kt�rych
-- wyst�puje jaki� problem, bo w przeciwnym razie trzeba by by�o pisa� tutaj
-- algorytm brania w�z��w w dobrej kolejno�ci (przodkowie najpierw), a to
-- w�a�nie robimy zazwyczaj na podstawie branch_ids, kt�re jest tu b��dne
-- i chcemy naprawia�... ;-)

declare @tree_id int

declare cur cursor for 
select distinct tree_id
from bik_node
where is_deleted = 0 and ltrim(rtrim(coalesce(branch_ids, ''))) not like '%' + 
  case when parent_node_id is not null then cast(parent_node_id as varchar(20)) + '|' else '' end +
  cast(id as varchar(20)) + '|'

open cur

fetch next from cur into @tree_id

while @@fetch_status = 0 begin
  exec sp_node_init_branch_id @tree_id, null
  
  fetch next from cur into @tree_id
end

close cur

deallocate cur

go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_update_version '1.1.9.21', '1.1.9.22';
go
