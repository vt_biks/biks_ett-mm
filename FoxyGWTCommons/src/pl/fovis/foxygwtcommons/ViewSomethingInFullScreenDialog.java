/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.i18n.I18n;

/**
 *
 * @author wezyr
 */
public abstract class ViewSomethingInFullScreenDialog extends BaseActionOrCancelDialog {

    private String title;

    @Override
    protected String getActionButtonCaption() {
        return null;
    }

    @Override
    protected void doAction() {
        //no-op
    }

    @Override
    protected void buildMainWidgets() {
        if (title != null) {
            Label titleLabel = new Label(title);
            titleLabel.setStyleName("def-title");
            main.add(titleLabel);
        }

        Widget contentWidget = getContentWidget();
//        Label label2 = new Label(body);
//        label2.setStyleName("def-content");
        ScrollPanel scPanel = new ScrollPanel(contentWidget);
        setScrollPanelSize(scPanel, 100);

        main.add(scPanel);
    }

    protected abstract Widget getContentWidget();

    protected void buildAndShowDialog(String title) {
        this.title = title;
        super.buildAndShowDialog();
    }

    @Override
    protected final String getCancelButtonCaption() {
        return I18n.zamknij.get() /* I18N:  */;
    }
}
