/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.client.bikwidgets.IPaginatedItemsDisplayer;
import pl.fovis.client.bikwidgets.NodesToLinkGrid;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class AddLinkingDialog extends AddNodesDialogBase {

    @Override
    protected IPaginatedItemsDisplayer<TreeNodeBean> createPaginatedItemsDisplayer() {
        paginatorFvs = new NodesToLinkGrid();
        return paginatorFvs;
    }

    @Override
    protected void doAction() {
        saveCont.doIt(paginatorFvs.tnb);
    }

    @Override
    protected String getRelationType() {
        return BIKConstants.LINKING;
    }

    protected String getTextWhenKindsListEmpty() {
        return I18n.brakObiektowDoDowiazania.get();
    }

    protected String getDialogCaption() {
        return I18n.dodajDowiazanie.get() /* I18N:  */;
    }
}
