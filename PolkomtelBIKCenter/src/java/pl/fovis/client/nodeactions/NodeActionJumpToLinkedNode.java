/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
public class NodeActionJumpToLinkedNode extends NodeActionReadOnlyBase {

    @Override
    public String getCaption() {
        return I18n.przejdzDoOryginalu.get() /* I18N:  */;
    }

    @Override
    public void execute() {
        BIKClientSingletons.getTabSelector().submitNewTabSelection(data.node.treeCode, data.node.id, true);
    }

    @Override
    public boolean isEnabledByDesign() {
        return isNodeLinkedAnyhow();
    }
}
