/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.reportsdk;

import com.businessobjects.rebean.wi.DataProvider;
import com.businessobjects.rebean.wi.DataProviders;
import com.businessobjects.rebean.wi.DataSource;
import com.businessobjects.rebean.wi.DataSourceObject;
import com.businessobjects.rebean.wi.DocumentInstance;
import com.businessobjects.rebean.wi.OpenDocumentParameters;
import com.businessobjects.rebean.wi.PropertiesType;
import com.businessobjects.rebean.wi.Query;
import com.businessobjects.rebean.wi.ReportEngine;
import com.businessobjects.rebean.wi.ReportEngines;
import commonlib.LameUtils;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import pl.bssg.metadatapump.common.PumpConstants;
import simplelib.BaseUtils;
import simplelib.SingleValue;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class ReportPump extends AbstractReportPump {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected ReportEngines repEng;
    protected ReportEngine widocRepEngine;

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_SAPBO;
    }

    @Override
    protected void doOptInitialize() throws Exception {
        repEng = (ReportEngines) enterpriseSession.getService("ReportEngines");
        widocRepEngine = (ReportEngine) repEng.getService(ReportEngines.ReportEngineType.WI_REPORT_ENGINE);

    }

    @Override
    protected void openReportAndSaveMetadata(String reportName, int reportId, int reportCount, SingleValue<Integer> queryCount, Integer reportNodeId, Date reportModifiedTime) {
        DocumentInstance doc = null;
        try {
            if (logger.isInfoEnabled()) {
                logger.info("Opening report: " + reportName + ", si_id: " + reportName);
            }
            if (ignoreRefreshOnOpen) {
                OpenDocumentParameters param = new OpenDocumentParameters();
                param.setIgnoreRefreshOnOpen(true);
                doc = widocRepEngine.openDocument(reportId, param);
            } else {
                doc = widocRepEngine.openDocument(reportId);
            }
            int actualProgress = (((reportCount + 1) * 100) / reportsToAdd.size());
            safeLog("Raport: " + reportName + " ( " + (reportCount + 1) + " / " + reportsToAdd.size() + " )", actualProgress);
            DataProviders dataProviders = doc.getDataProviders();
            safeLogWithDetails("EDL: after getDataProviders", actualProgress, isExtraDetailsLoggingEnable);
            int dpCount = dataProviders.getCount();
            safeLogWithDetails("EDL: after getCount, starting iterating", actualProgress, isExtraDetailsLoggingEnable);
            for (int k = 0; k < dpCount; k++) {
                DataProvider item = dataProviders.getItem(k);
                safeLogWithDetails("EDL: after getDataProvider: " + k, actualProgress, isExtraDetailsLoggingEnable);
                DataSource itemDataSource = item.getDataSource();
                safeLogWithDetails("EDL: after getDataSource", actualProgress, isExtraDetailsLoggingEnable);
                String univCUID = getUniverseCUID(itemDataSource.getUniverseID());
                String univName = itemDataSource.getName();
                Query query;
                safeLogWithDetails("EDL: after get id and name", actualProgress, isExtraDetailsLoggingEnable);
                if (item.hasCombinedQueries()) {
                    query = (Query) item.getCombinedQueries().getQueryNodeAt(0);
                } else {
                    query = item.getQuery();
                }
                safeLogWithDetails("EDL: after get query, is combined? " + item.hasCombinedQueries(), actualProgress, isExtraDetailsLoggingEnable);
                String sqlString;
                try {
                    @SuppressWarnings("deprecation")
                    String sqlStringDeprecated = query.getSQL();
                    sqlString = sqlStringDeprecated;
                } catch (Exception e) {
                    sqlString = "Error: " + e.getMessage();
                }
                safeLogWithDetails("EDL: after get query SQL", actualProgress, isExtraDetailsLoggingEnable);
                if (STORE_DATA) {
                    // id, name, idText, filtrText, SQLText, universeCUID, reportNodeId, universeName, reportSiId
                    execNamedCommandWithCommit("insertReportQueriesSingle", ++queryCount.v, item.getName(), item.getID(), query.hasCondition() ? query.getCondition().getName() : null, sqlString, univCUID, reportNodeId, univName, reportId, reportModifiedTime);
                    //                        reportQueryList.add(new ReportQueryBean(++queryCount, item.getName(), item.getID(), query.hasCondition() == true ? query.getCondition().getName() : null, sqlString, univCUID, univName, reportNodeId, reportsToAdd.get(i).id));
                    safeLogWithDetails("EDL: after insert qury to DB, get name, id and condition", actualProgress, isExtraDetailsLoggingEnable);
                    int objCount = query.getResultObjectCount();
                    safeLogWithDetails("EDL: after get Result Count, starting iterating", actualProgress, isExtraDetailsLoggingEnable);
                    for (int j = 0; j < objCount; j++) {
                        // queryNodeId, universeCUID, objectName, className, universeName, reportSiId
                        DataSourceObject resultObject = query.getResultObject(j);
                        safeLogWithDetails("EDL: after get resultObject", actualProgress, isExtraDetailsLoggingEnable);
                        execNamedCommandWithCommit("insertReportObjectsSingle", queryCount.v, univCUID, resultObject.getName(), getClassName((DataSourceObject) resultObject.getParent()), univName, reportId);
                        safeLogWithDetails("EDL: after insert object to DB, get name, parent", actualProgress, isExtraDetailsLoggingEnable);
//                            reportObjectList.add(new ReportObjectBean(queryCount, univCUID, univName, query.getResultObject(j).getName(), getClassName((DataSourceObject) query.getResultObject(j).getParent()), reportsToAdd.get(i).id));
                    }
                }
                if (purgeWebi) {
                    safeLogWithDetails("Purge data provider", actualProgress, isExtraDetailsLoggingEnable);
                    item.purge();
                    safeLogWithDetails("Purge success", actualProgress, isExtraDetailsLoggingEnable);
                }
            }
            // pobranie report extradata
            Properties properties = doc.getProperties();
            safeLogWithDetails("EDL: after getProperties", actualProgress, isExtraDetailsLoggingEnable);
            // reportNodeId, name, author, description, refreshOO, lastRefreshDuration, lastRefreshDate, lastSavedBy, modificationDate, creationDate
            execNamedCommandWithCommit("insertReportExtradata", reportNodeId, properties.getProperty(PropertiesType.NAME), properties.getProperty(PropertiesType.AUTHOR), properties.getProperty(PropertiesType.DESCRIPTION), BaseUtils.tryParseBoolean(properties.getProperty(PropertiesType.REFRESH_ON_OPEN)), BaseUtils.tryParseInteger(properties.getProperty(PropertiesType.LAST_REFRESH_DURATION)), properties.getProperty("lastsavedby"));
            safeLogWithDetails("EDL: after insert report properties to DB", actualProgress, isExtraDetailsLoggingEnable);
            if (purgeWebi) {
                safeLogWithDetails("Save report after purge DP", actualProgress, isExtraDetailsLoggingEnable);
                doc.save();
                safeLogWithDetails("Save success", actualProgress, isExtraDetailsLoggingEnable);
            }
        } catch (Exception e) {
            safeLog("Report has not been opened: " + reportName + ", error: " + e.getMessage(), null);
            if (logger.isWarnEnabled()) {
                logger.warn("Report has not been opened: " + reportName + ", si_id: " + reportId + ", error: " + e.getMessage(), e);
            }
        } finally {
            if (doc != null) {
                doc.closeDocument();
            }
        }
    }

    protected String getUniverseCUID(String universeId) {
        int indexStarted = universeId.indexOf("UnivCUID=", 0) + 9;
        String newString = universeId.substring(indexStarted);
        List<String> splitBySep = BaseUtils.splitBySep(newString, ";");
        return splitBySep.get(0);
    }
}
