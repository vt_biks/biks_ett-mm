/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.invokers;

/**
 *
 * @author pmielanczuk
 */
public interface IDynamicMethodInvoker<T> {
    public Object invokeMethod(T obj, String methodName, Object... args);
}
