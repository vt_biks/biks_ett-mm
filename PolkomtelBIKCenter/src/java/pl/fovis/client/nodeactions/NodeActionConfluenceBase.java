/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKGWTConstants;

/**
 *
 * @author tflorczak
 */
public abstract class NodeActionConfluenceBase extends NodeActionBase {

    @Override
    public boolean isEnabledByDesign() {
        return node == null && (!isTreeReadOnly() || isLoggedUserAuthorOfTree()) || isNodeNotLinkedAnyhowNorReadOnlyTreeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_CONFLUENCE_FOLDER);
    }

//    protected boolean canBeEnabledByStateForThisNode() {
//        return !isTreeReadOnlyInner() && (node == null || isNodeFolder()) && !isNodeLinkedAnyhow();
//    }
//
//    @Override
//    protected boolean canBeEnabledByStateForThisNodeAndUserLoggedInByDesign() {
//        return canBeEnabledByStateForThisNode() && (isBikUserLoggedIn() || !isInBlogsTree());
//    }
}
