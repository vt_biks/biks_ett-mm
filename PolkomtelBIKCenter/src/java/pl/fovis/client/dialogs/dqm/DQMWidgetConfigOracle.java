/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import pl.fovis.common.BIKConstants;

/**
 *
 * @author tflorczak
 */
public class DQMWidgetConfigOracle extends DQMWidgetConfigDatabase {

    @Override
    public String getTreeCode() {
        return BIKConstants.TREE_CODE_ORACLE;
    }
}
