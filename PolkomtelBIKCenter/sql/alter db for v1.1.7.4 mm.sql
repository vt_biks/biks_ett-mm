﻿exec sp_check_version '1.1.7.4';
go

create table bik_statistic(
	counter_name varchar(255) not null,
	user_id int not null references bik_system_user(id),
	event_date date not null default cast(SYSDATETIME() as date),
	event_datetime datetime not null default SYSDATETIME(),
	parametr varchar(255) null	
);

exec sp_update_version '1.1.7.4', '1.1.7.5';
go