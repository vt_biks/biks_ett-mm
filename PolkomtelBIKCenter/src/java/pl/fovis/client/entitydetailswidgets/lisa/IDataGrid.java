/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import java.util.List;
import java.util.Map;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryValue;

/**
 *
 * @author ctran
 */
public interface IDataGrid {

    public void getData(int pageNum, int pageSize);

    public String getSortColumnName();

    public int getPageNum();

    public int getPageSize();

    public Integer getBeanCnt();

    public List<LisaDictColumnBean> getColumnInfo();

    public void updateDataGrid(List<Map<String, LisaDictionaryValue>> dataList);

    public void changeDirectionOrder();

    public void changePage(int pageNum);

    public void performLisaFiltr(List<LisaFiltrColumnValueBase> infoToFiltr);

    public boolean isHasMoreData();

}
