/*
 * SimilarityContainer.java
 *
 * Created on 31 sierpie� 2007, 19:31
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib.similarity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author wezyr
 */
public class SimilarityContainer {
    
    private static Locale PolishLocale = new Locale("pl");
    
    private int partCount;
    private Set<String> exactWords = new HashSet<String>();
    private Map<Integer, Map<String, List<String>>[]> partialWordsMap;
    private double minSimilarity;
    
    public SimilarityContainer(int partCount, double minSimilarity) {
        this.minSimilarity = minSimilarity;
        this.partCount = partCount;
        this.partialWordsMap = new HashMap<Integer, Map<String, List<String>>[]>();//[partCount]; //HashMap<String, List<String>>
    }
    
    protected Map<String, List<String>>[] newPartialWords() {
        @SuppressWarnings("unchecked")
        Map<String, List<String>>[] partialWords = new HashMap[partCount];
        for (int i = 0; i < partCount; i++)
            partialWords[i] = new HashMap<String, List<String>>();
        return partialWords;
    }
    
    protected String normalizeWord(String word) {
        return word.toLowerCase(PolishLocale);
    }
    
    protected String makePartial(String word, int partNo, int partNo2) {
        char buff[] = new char[word.length()];
        int outPos = 0;
        
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            if (c % partCount != partNo && c % partCount != partNo2)
                buff[outPos++] = c;
        }
        
        String res = new String(buff, 0, outPos);
        System.out.println("makePartial: word=" + word + ", partNo=" + partNo + ", res=" + res);
        return res;
    }
    
    public void add(String word) {
        String normalizedWord = normalizeWord(word);
        
        if (exactWords.contains(word))
            return;
        
        exactWords.add(normalizedWord);
        
        Map<String, List<String>>[] partialWords = partialWordsMap.get(normalizedWord.length());
        if (partialWords == null) {
            partialWords = newPartialWords();
            partialWordsMap.put(normalizedWord.length(), partialWords);
        }
            
        for (int i = 0; i < partCount; i++) {
            String partialWord = makePartial(normalizedWord, i, -1);
            //System.out.println("normalized: " + normalizedWord + ", partial: " + partialWord);
            List<String> fullWords = partialWords[i].get(partialWord);
            if (fullWords == null) {
                fullWords = new ArrayList<String>();
                partialWords[i].put(partialWord, fullWords);
            }
            fullWords.add(normalizedWord);
        }
    }
    
    public int getWordCount() {
        return exactWords.size();
    }
    
    public double calcSimilarity(String w1, String w2) {
        int maxL = Math.max(w1.length(), w2.length());
        return (maxL - StringSimilarityUtils.calcDamerauLevenshteinDistance(w1, w2)) * 100.0 /
                maxL;
//        return //100.0;
//                20.0 * LetterTupleSimilarity.calcRankForStrings(w1, w2, 2, false) +
//                30.0 * LetterTupleSimilarity.calcRankForStrings(w1, w2, 2, true) +
//                50.0 * LetterTupleSimilarity.calcRankForStrings(w1, w2, 1, false);
    }
    
    public Map<String, Double> findSimilar(String word) {
        String normalizedWord = normalizeWord(word);
        
        Map<String, Double> candidates = new HashMap<String, Double>();
        
        int calcCnt = 0;
        
        for (int wordLen = normalizedWord.length() - 1; wordLen <= normalizedWord.length() + 1; wordLen++) {
            Map<String, List<String>>[] partialWords = partialWordsMap.get(wordLen);
            if (partialWords == null)
                continue;
            
            for (int i = 0; i < partCount; i++) {
                String partialWord = makePartial(normalizedWord, i, -1);
                List<String> fullWords = partialWords[i].get(partialWord);
                if (fullWords != null) {
                    for (String w : fullWords) {
                        calcCnt++;
                        double similarity = calcSimilarity(normalizedWord, w);
                        if (similarity >= minSimilarity)
                            candidates.put(w, similarity);
                    }
                }
            }
        }
        
        System.out.println("findSimilar: " + word + ", calcCnt=" + calcCnt + ", res=" + candidates);
        return candidates;
    }
}
