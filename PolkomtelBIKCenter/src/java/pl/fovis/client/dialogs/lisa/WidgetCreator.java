/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import pl.fovis.client.entitydetailswidgets.lisa.WidgetWithValue;

/**
 *
 * @author tbzowka
 */
public interface WidgetCreator {
   public WidgetWithValue  createWidget();
}
