﻿exec sp_check_version '1.2.3.3';
go


-- poprawka do bw kindów
update bik_node_kind
set is_folder = 0
where code in ('BWDpa', 'BWCha', 'BWKyf', 'BWTim', 'BWUni')

create table bik_icon (
	id int identity(1,1) not null primary key,
	name varchar(100) not null unique
);
go

update bik_node_kind
set icon_name = 'report'
where icon_name in ('word', 'txt', 'rtf')

insert into bik_icon(name)
select distinct icon_name from bik_node_kind

create table bik_tree_kind (
	id int identity(1,1) not null primary key,
	code varchar(255) not null unique,
	caption varchar(max) not null,
	branch_node_kind_id int not null,
	leaf_node_kind_id int not null,
	is_deleted int not null default 0,
	allow_linking int not null default 0
);
go

update bik_node_kind
set code = 'dtk_TaxonomyEntity'
where code = 'TaxonomyEntity'

insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
values ('dtk_TaxonomyEntityLeaf', 'Obszar biznesowy', 'bfolder', 'Taxonomy', 0, 17, 1)

declare @BizAreaKind int;
select @BizAreaKind = id from bik_node_kind where code = 'dtk_TaxonomyEntity'

declare @BizAreaLeafKind int;
select @BizAreaLeafKind = id from bik_node_kind where code = 'dtk_TaxonomyEntityLeaf'

insert into bik_tree_kind(code,caption,branch_node_kind_id,leaf_node_kind_id, allow_linking)
values('Taxonomy','Kategoryzacje', @BizAreaKind, @BizAreaLeafKind, 1)


-- poprzednia wersja w: alter db for v1.2.3.2 tf.sql
-- dostosowanie procedury do szablonów drzew (rezygnacja z Taxonomy)
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node]
go

create procedure [dbo].[sp_insert_into_bik_node](@tree_code varchar(255))
as
	declare @table_folders varchar(255);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	declare @diag_level int = 1;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'
	
	select @tree_id = id 
	from bik_tree
	where code=@tree_code;

	create table #primaryIds(si_id varchar(5000) primary key);

	if(@tree_code!='Reports' and @tree_code!='Teradata')
	begin
		set @table_folders = ' APP_FOLDER ';
		if(@tree_code = 'Connections')
		begin
			set @kinds = '''MetaData.DataConnection''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_METADATA__DATACONNECTION
					where SI_KIND in (' + @kinds + ') and SI_OBJECT_IS_CONTAINER=0');			
		end;
		if(@tree_code = 'ObjectUniverses')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_UNIVERSE
					where SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_code='Reports')
	begin
		-- wybranie folderw raportów bez raportów użytkowników
		set @table_folders = ' INFO_FOLDER where si_name != ''~Webintelligence''';
		--set @table_folders = ' INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 is null or SI_PATH__SI_FOLDER_NAME2 != ''User Folders''';
		set @kinds = '''Webi'', ''Flash'', ''CrystalReport'',''Excel'',''FullClient'',''Pdf'',''Hyperlink''
		,''Rtf'',''Txt'',''Powerpoint'',''Word''';
	end;
	
    declare @sql nvarchar(max);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		--print cast(sysdatetime() as varchar(22)) + ': faza 4.1 - teradata przed sp_teradata_init_branch_names'
		--exec sp_teradata_init_branch_names;
		if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': tworzenie pierwszego @sql'
		set @sql = N'select branch_names as SI_ID, parent_branch_names	as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata';
	end;
	else
	begin
		if(@tree_code = 'Reports')
		-- sztuczny podzial na root folder i user folders dla raportów
		set @sql = N'select distinct SI_PARENT_FOLDER as SI_ID, (select si_id from INFO_FOLDER where SI_NAME=''User Folders'') as SI_PARENTID, SI_KIND, SI_PATH__SI_FOLDER_NAME1 as SI_NAME, NULL as DESCR
				 from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' 
				 and SI_NAME!=''~Webintelligence'' 

				 union all
				
				 select si_id, case
									when SI_PARENTID=0 and si_name!=''User Folders''
										then (select si_id from INFO_FOLDER where SI_NAME=''Root Folder'')
									when si_id in(select si_id from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 = ''User Folders'')
                                        then SI_PARENTID
									when SI_PARENTID in(select si_id from INFO_FOLDER)
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case
																			when SI_NAME=''Root Folder''
																				then ''Foldery korporacyjne''
																			when SI_NAME=''User Folders''
																				then ''Foldery użytkowników''
																			else SI_NAME end, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 								
				' union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') and SI_INSTANCE = 0
				union all
				
				select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR
				from aaa_global_props where si_parentid in (select si_parentid from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' and SI_NAME!=''~Webintelligence'') and si_kind!=''Folder''
				
				union all
				
				select id, parent_id, ''ReportSchedule'', name, null
				from aaa_report_schedule sch
				inner join aaa_global_props gp on gp.si_id=sch.parent_id
				';
		if(@tree_code = 'Connections')
			set @sql = N'select 1 as si_id, 0 as si_parentid, ''ConnectionFolder'' as si_kind, ''Połączenia'' as si_name, '''' as descr
				
				 union all

				 select SI_ID, 1, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from APP_METADATA__DATACONNECTION
				 where SI_KIND in ('+ @kinds +') and SI_OBJECT_IS_CONTAINER=0';

		if(@tree_code = 'ObjectUniverses')
			set @sql = N'select si_id, case 
									when SI_NAME=''Universes''
										then 0
									when SI_PARENTID in(select si_id from ' + @table_folders +') 
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case when SI_NAME=''Universes'' then ''Światy obiektów'' else SI_NAME end as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 
													
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +')';
	end;					

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed zapytaniem'
	print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(1000) primary key, si_parentid varchar(1000), si_kind varchar(max), si_name varchar(max), descr varchar(max));
	if(@tree_code = 'Reports')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ReportFolder'' 
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'Connections')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ConnectionFolder''
										when si_kind=''MetaData.DataConnection'' then ''DataConnection''
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'ObjectUniverses')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''UniversesFolder'' else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'Teradata')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, si_kind, si_name, DESCR
					from (' + @sql + ') xxx';

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po stworzeniu drugiego @sql'
			   
	EXECUTE(@sql);

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/

	declare @rc int = 1

	while @rc > 0 begin
	
	delete from #tmpNodes
	where (si_kind = 'UniversesFolder' or si_kind = 'ConnectionFolder' or si_kind = 'ReportFolder') and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/
	
	declare @report_tree_id int;
	select @report_tree_id=id from bik_tree where code='Reports'

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updatetami'

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.descr, tree_id = @tree_id
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and tree_id = @tree_id and bik_node.linked_node_id is null
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed update podlinkowanych'
	--update podlinkowanych
	update bik_node 
	set /*parent_node_id = null, */node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		 descr = #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and (tree_id = @report_tree_id or tree_id in (select id from bik_tree where tree_kind in (select code from bik_tree_kind where allow_linking = 1))) and bik_node.linked_node_id is not null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed insertami'
	
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id
	where bik_node.id is null;

	set nocount on;

	drop table #primaryIds;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updateami parentow'
	

	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id
	inner join bik_node bk on bk.obj_id = pt.si_parentid where bik_node.linked_node_id is null and bk.tree_id = @tree_id 

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed usuwaniem'
	
	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in 
	('Measure', 'Dimension', 'Detail', 'UniverseClass', 'ReportQuery', 'Filter', 'UniverseColumn', 'UniverseTable', 'UniverseAliasTable', 'UniverseDerivedTable', 'UniverseTablesFolder', 'ConnectionEngineFolder', 'ConnectionNetworkFolder')
				
	drop table #tmpNodes
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po usuwaniu i po dropie'

	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': koniec'
end;
go

-- poprzednia wersja w: alter db for v1.2.1.5 tf.sql
-- dostosowanie procedury do szablonów drzew (rezygnacja z Taxonomy)
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbw]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbw]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbw](@tree_code varchar(255))
as
begin
	declare @diag_level int = 1;
	declare @AreaNodeKind int;
	declare @BExNodeKind int;
	declare @CUBENodeKind int;
	declare @MPRONodeKind int;
	declare @ODSONodeKind int;
	declare @ISETNodeKind int;
	declare @ObjectsFolderNodeKind int;
	declare @UNINodeKind int;
	declare @TIMNodeKind int;
	declare @KYFNodeKind int;
	declare @DPANodeKind int;
	declare @CHANodeKind int;

	declare @tree_id int;
	select @AreaNodeKind = id from bik_node_kind where code = 'BWArea'
	select @BExNodeKind = id from bik_node_kind where code = 'BWBEx'
	select @CUBENodeKind = id from bik_node_kind where code = 'BWCUBE'
	select @MPRONodeKind = id from bik_node_kind where code = 'BWMPRO'
	select @ODSONodeKind = id from bik_node_kind where code = 'BWODSO'
	select @ISETNodeKind = id from bik_node_kind where code = 'BWISET'
	select @ObjectsFolderNodeKind = id from bik_node_kind where code = 'BWObjsFolder'
	select @UNINodeKind = id from bik_node_kind where code = 'BWUni'
	select @TIMNodeKind = id from bik_node_kind where code = 'BWTim'
	select @KYFNodeKind = id from bik_node_kind where code = 'BWKyf'
	select @DPANodeKind = id from bik_node_kind where code = 'BWDpa'
	select @CHANodeKind = id from bik_node_kind where code = 'BWCha'
	select @tree_id = id from bik_tree where code = @tree_code;
	
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'
	
	create table #tmpNodes (si_id varchar(900) primary key, si_parentid varchar(1000), si_kind varchar(100), si_name varchar(1000), descr varchar(max), visual_order int default 0);
	create table #tmpNodesLinked (si_id varchar(900), si_parentid varchar(1000));
	
	-- wrzucanie obszarów inf.
	insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
	select obj_name, parent, @AreaNodeKind, descr, null, -1 from bik_sapbw_area 
		
	if(@tree_code='BWReports')
	begin
		-- wrzucanie obszaru na zapytania nieprzypisane
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		values('$$BIKS_TEMP_ARCHIWUM$$', null, @AreaNodeKind, '(Archiwum)', 'Obszar gromadzący zapytania BEx, dla których nie odnaleziono dostawcy informacji. Obszar został wygenerowany automatycznie i nie znajduje się w repozytorium SAP BW.',100)

		-- wrzucanie zapytań BEx
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr)
		select convert(varchar(200),obj_name), coalesce((select ar.obj_name from bik_sapbw_provider pv left join bik_sapbw_area ar on pv.info_area=ar.obj_name where pv.obj_name = q.provider),'$$BIKS_TEMP_ARCHIWUM$$'), @BExNodeKind, case when rtrim(descr) <> '' then rtrim(descr) else obj_name end,'' from bik_sapbw_query q
	end;
	if(@tree_code='BWProviders')
	begin
		-- wrzucanie obszaru na dostawców nieprzypisanych
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		values('$$BIKS_TEMP_ARCHIWUM$$', null, @AreaNodeKind, '(Archiwum)', 'Obszar gromadzący dostawców informacji, dla których nie odnaleziono obszaru informacji. Obszar został wygenerowany automatycznie i nie znajduje się w repozytorium SAP BW.',100)

		-- wrzucanie folderów na cechy i wskaźniki
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		select obj_name + '|objectsFolder|', obj_name, @ObjectsFolderNodeKind, 'Cechy i wskaźniki', 'Folder zawierający obiekty informacji', -1 from bik_sapbw_provider

		-- wrzucanie dostawców informacji
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order) -- visual_order zgodny z tym w SAP GUI
		select obj_name, coalesce(area,'$$BIKS_TEMP_ARCHIWUM$$'), case type when 'MPRO' then @MPRONodeKind
																			when 'CUBE' then @CUBENodeKind
																			when 'ODSO' then @ODSONodeKind
																			when 'ISET' then @ISETNodeKind end,  
			case when rtrim(descr) <> '' then rtrim(descr) else obj_name end, null, case type when 'CUBE' then 1
																							when 'ISET' then 2
																							when 'MPRO' then 3
																							when 'ODSO' then 4 end  
		from bik_sapbw_provider xx left join (select obj_name as area from bik_sapbw_area) yy on xx.info_area = yy.area
		
		-- wrzucanie obiektów
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		select provider + '|' + obj_name, provider + '|objectsFolder|' ,case type when 'CHA' then @CHANodeKind 
																				  when 'UNI' then @UNINodeKind
																				  when 'KYF' then @KYFNodeKind
																				  when 'TIM' then @TIMNodeKind
																				  when 'DPA' then @DPANodeKind
																				  else @CHANodeKind end, 
																				  descr, null, case type when 'CHA' then 1
																										 when 'KYF' then 2
																										 when 'TIM' then 3
																										 when 'UNI' then 4
																										 when 'DPA' then 5
																										 else 6 end 
		from bik_sapbw_object 
		where provider_parent is null
		
		-- wrzucanie przepływów
		insert into #tmpNodesLinked(si_id,si_parentid)
		select obj_name, parent from bik_sapbw_flow
	end;
	
	-- usuwanie pustych folderów
	declare @rc int = 1

	while @rc > 0 
	begin
		delete from #tmpNodes
		where si_kind in (@AreaNodeKind, @ObjectsFolderNodeKind) and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	
	-- wdrażnie danych z tabeli tymczasowej to bik_node
	
	-- update istniejących
	update bik_node 
	set node_kind_id = #tmpNodes.si_kind, name = ltrim(rtrim(#tmpNodes.si_name)), descr = #tmpNodes.descr,
		visual_order = #tmpNodes.visual_order, is_deleted = 0
	from #tmpNodes
	where bik_node.obj_id = #tmpNodes.si_id and (bik_node.tree_id = @tree_id or (bik_node.linked_node_id is not null and bik_node.tree_id in (select id from bik_tree where tree_kind in (select code from bik_tree_kind where allow_linking = 1))))
	
	-- wrzucanie nowych
	insert into bik_node(name,node_kind_id,tree_id,descr,obj_id, visual_order)
	select ltrim(rtrim(#tmpNodes.si_name)), #tmpNodes.si_kind, @tree_id, #tmpNodes.descr, #tmpNodes.si_id, #tmpNodes.visual_order
	from #tmpNodes --inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id and bik_node.linked_node_id is null
	where bik_node.id is null;
	
	-- wrzucanie podlinkowanych
	insert into bik_node(name,parent_node_id,node_kind_id,tree_id,obj_id, linked_node_id, disable_linked_subtree)
	select lin.name, par.id, lin.node_kind_id, lin.tree_id, tmp.si_id, lin.id, 1 from #tmpNodesLinked tmp 
	left join bik_node lin on lin.obj_id = tmp.si_id and lin.is_deleted = 0 and lin.tree_id = @tree_id and lin.linked_node_id is null
	left join bik_node par on par.obj_id = tmp.si_parentid and par.is_deleted = 0 and par.tree_id = @tree_id and par.linked_node_id is null
	left join bik_node oryg on oryg.obj_id = tmp.si_id and oryg.is_deleted = 0 and oryg.tree_id = @tree_id and oryg.linked_node_id is not null
	where oryg.id is null and par.id is not null and lin.id is not null
	
	-- update parentów
	update bik_node 
	set parent_node_id= bk.id
	from bik_node 
	inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id
	inner join bik_node bk on bk.obj_id = pt.si_parentid 
	where bik_node.linked_node_id is null and bk.tree_id = @tree_id 
	
	-- usuwanie starych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id 
	where #tmpNodes.si_id is null and (bik_node.tree_id = @tree_id or (bik_node.linked_node_id is not null and bik_node.tree_id in (select id from bik_tree where tree_kind in (select code from bik_tree_kind where allow_linking = 1))))
	
	-- usuwanie starych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join #tmpNodesLinked on bik_node.obj_id = #tmpNodesLinked.si_id 
	where #tmpNodesLinked.si_id is null and bik_node.linked_node_id is not null
	and bik_node.tree_id = @tree_id
	
	drop table #tmpNodes;
	drop table #tmpNodesLinked;
	
	exec sp_node_init_branch_id @tree_id, null;
	
end;
go

-- poprzednia wersja w: alter db for v1.2.3.2 tf.sql
-- fix błędu z dodawanie do podlinkowanych
if exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
as
begin
	delete from bik_sapbo_extradata

	insert into bik_sapbo_extradata (node_id,author,owner,keyword,file_path,file_name,created,modified,cuid,files_value,size,has_children,guid,instance,owner_id,progid,obtype,flags,children,ruid,runnable_object,content_locale,is_schedulable,webi_doc_properties,read_only,last_successful_instance_id,last_run_time,timestamp,progid_machine,endtime,starttime,schedule_status,recurring,nextruntime,doc_sender,revisionnum,application_object,shortname,dataconnection_total,universe_total )
    select bik.id as node_id,
    nullif(ltrim(rtrim(iw.SI_AUTHOR)), '') as author,
    nullif(ltrim(rtrim(p.SI_OWNER)), '') as owner,
    nullif(ltrim(rtrim(p.SI_KEYWORD)), '') as keyword,
    nullif(ltrim(rtrim(p.SI_FILES__SI_PATH)), '') as file_path,
    nullif(ltrim(rtrim(p.SI_FILES__SI_FILE1)), '') as file_name,
    nullif(ltrim(rtrim(p.SI_CREATION_TIME)), '') as created,
    nullif(ltrim(rtrim(p.SI_UPDATE_TS)), '') as modified,
    nullif(ltrim(rtrim(p.SI_CUID)), '') as cuid,
    nullif(ltrim(rtrim(p.SI_FILES__SI_VALUE1)), '') as files_value,
    nullif(ltrim(rtrim(p.SI_SIZE)), '') as size,
    p.SI_HAS_CHILDREN as has_children,
    nullif(ltrim(rtrim(p.SI_GUID)), '') as guid,
    p.SI_INSTANCE as instance,
    p.SI_OWNERID as owner_id,
    nullif(ltrim(rtrim(p.SI_PROGID)), '') as progid,
    p.SI_OBTYPE as obtype,
    p.SI_FLAGS as flags,
    p.SI_CHILDREN as children,
    nullif(ltrim(rtrim(p.SI_RUID)), '') as ruid,
    p.SI_RUNNABLE_OBJECT as runnable_object,
    nullif(ltrim(rtrim(p.SI_CONTENT_LOCALE)), '') as content_locale,
    p.SI_IS_SCHEDULABLE as is_schedulable,
    nullif(ltrim(rtrim(p.SI_WEBI_DOC_PROPERTIES)), '') as webi_doc_properties,
    p.SI_READ_ONLY as read_only,
    nullif(ltrim(rtrim(p.SI_LAST_SUCCESSFUL_INSTANCE_ID)), '') as last_successful_instance_id,
    nullif(ltrim(rtrim(p.SI_LAST_RUN_TIME)), '') as last_run_time,
    nullif(ltrim(rtrim(iw.SI_TIMESTAMP)), '')  as timestamp,
    nullif(ltrim(rtrim(iw.SI_PROGID_MACHINE)), '') as progid_machine,
    nullif(ltrim(rtrim(p.SI_ENDTIME)), '') as endtime,
    nullif(ltrim(rtrim(p.SI_STARTTIME)), '') as starttime,
    nullif(ltrim(rtrim(p.SI_SCHEDULE_STATUS)), '') as schedule_status,
    nullif(ltrim(rtrim(p.SI_RECURRING)), '') as recurring,
    nullif(ltrim(rtrim(p.SI_NEXTRUNTIME)), '') as nextruntime,
    nullif(ltrim(rtrim(p.SI_DOC_SENDER)), '') as doc_sender,
    au.SI_REVISIONNUM as revisionnum,
    au.SI_APPLICATION_OBJECT as application_object,
    nullif(ltrim(rtrim(au.SI_SHORTNAME)), '') as shortname,
    au.SI_DATACONNECTION__SI_TOTAL as dataconnection_total,
    iw.SI_UNIVERSE__SI_TOTAL as universe_total
    from aaa_global_props p
    left join INFO_WEBI iw on p.si_id = iw.si_id
    left join APP_UNIVERSE au on p.si_id = au.si_id
    inner join bik_node bik on convert(varchar(30),p.si_id) = bik.obj_id
    inner join bik_tree bt on bt.id = bik.tree_id
    where bik.linked_node_id is null 
    and bik.is_deleted = 0
    and bt.code in ('Reports', 'ObjectUniverses', 'Connections')
    order by bik.id
    
    delete from bik_sapbo_schedule
    
    insert into bik_sapbo_schedule(node_id,destination,owner,creation_time,nextruntime,expire,type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters)
    select bn.id,destination,owner,creation_time,nextruntime,expire,type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters 
    from aaa_report_schedule sch
    inner join bik_node bn on convert(varchar(30),sch.id) = bn.obj_id
    inner join bik_tree bt on bt.id = bn.tree_id
    where bn.linked_node_id is null 
    and bn.is_deleted = 0
    and bt.code in ('Reports')
    
end;
go

declare @MenuItemKindId int;
select @MenuItemKindId = id from bik_node_kind where code = 'MenuItem'

update bik_node
set obj_id = '&dtk_TaxonomyEntity'
where obj_id = '&TaxonomyEntity'
and node_kind_id = @MenuItemKindId

exec sp_add_menu_node '&dtk_TaxonomyEntity', '@', '&dtk_TaxonomyEntityLeaf'

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

exec sp_update_version '1.2.3.3', '1.2.3.4';
go