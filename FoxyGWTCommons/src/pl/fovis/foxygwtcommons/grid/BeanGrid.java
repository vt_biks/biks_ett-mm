/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.grid;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable.ColumnFormatter;
import com.google.gwt.user.client.ui.HTMLTable.RowFormatter;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.foxygwtcommons.GWTUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public class BeanGrid<T> implements IsWidget, IBeanGrid<T> {

    protected final IBeanProvider<T> provider;
    protected FlexTable table = new FlexTable();
    protected int currPage = 0;
    protected int pageSize = 10;
    protected int beanCnt;
    protected IBeanDisplay<T> display;
    protected String[] colNames;
    protected Map<T, Integer> beanToRowIdxMap = new HashMap<T, Integer>();
    protected List<IBeanGridEventsListener<T>> eventsListeners = new ArrayList<IBeanGridEventsListener<T>>();
    protected boolean showHeader = true;
    private boolean isUnknownPageCountModeEnabled = false;

    public BeanGrid(IBeanProvider<T> provider, IBeanDisplay<T> display, int pageSize,
            String... colNames) {
        this.provider = provider;
        this.display = display;
        this.colNames = colNames;
        this.pageSize = pageSize;
//        display.setBeanGrid(this);
        table.setWidth("100%");
        setPageNumPrivate(0);
    }

    public void addEventsListener(IBeanGridEventsListener<T> eventsListener) {
        eventsListeners.add(eventsListener);
    }

    @Override
    public Widget asWidget() {
        return table;
    }

    protected void showHeader() {
        int col = 0;

        for (String colName : colNames) {
            GWTUtils.setHTMLTableCellVal(table, 0, col++,
                    display.getHeader(colName));
        }
    }

    protected void showBeans(Iterable<T> beans) {
        int delIdx = showHeader ? 1 : 0;
        int beanRowCnt = table.getRowCount() - delIdx;

        while (beanRowCnt-- > 0) {
            table.removeRow(delIdx);
        }

        if (showHeader) {
            showHeader();
        }

        beanToRowIdxMap.clear();

        int rowIdx = delIdx;
        for (T b : beans) {
            beanToRowIdxMap.put(b, rowIdx);
            showOneBean(b, rowIdx++);
        }
    }

    protected void dataFetched(Iterable<T> beans, int beanCnt) {
        if (isUnknownPageCountModeEnabled) {
            this.beanCnt = Math.max(currPage - 1, 0) * pageSize + beanCnt;
        } else {
            this.beanCnt = beanCnt;
        }
        if ((currPage * pageSize >= beanCnt) && !isUnknownPageCountModeEnabled) {
            currPage = (beanCnt - 1) / pageSize;
            if (currPage < 0) {
                currPage = 0;
            }
        }
        showBeans(beans);

        for (IBeanGridEventsListener<T> bgel : eventsListeners) {
            bgel.pageChanged();
        }
    }

    private void setPageNumPrivate(int pageNum) {
        setPageNum(pageNum);
    }

    @Override
    public void setPageNum(int pageNum) {
        this.currPage = pageNum;
        provider.getBeans(pageNum * pageSize, pageSize, null, null, new IParametrizedContinuation<Pair<? extends Iterable<T>, Integer>>() {
            @Override
            public void doIt(Pair<? extends Iterable<T>, Integer> param) {
                dataFetched(param.v1, param.v2);
            }
        });
    }

    protected void showOneBean(T b, int rowIdx) {
        int col = 0;

        for (String colName : colNames) {
            GWTUtils.setHTMLTableCellVal(table, rowIdx, col++,
                    display.getValue(b, colName));
        }

        String styleName = table.getRowFormatter().getStyleName(rowIdx);
        styleName += ((styleName.length() > 0) ? " " : "") + ((rowIdx % 2 == 1) ? "odd" : "even");

        table.getRowFormatter().setStyleName(rowIdx, styleName);
    }

    public void insertFirstRow(T b) {
        table.insertRow(1);
        showOneBean(b, 1);
    }

    public void removeFirstRow() {
        table.removeRow(1);
    }

    public void beansChanged() {
        setPageNumPrivate(currPage);
    }

    public void oneBeanChanged(T bean) {
        Integer rowIdx = beanToRowIdxMap.get(bean);
        if (rowIdx != null) {
            showOneBean(bean, rowIdx);
        }

        for (IBeanGridEventsListener<T> bgel : eventsListeners) {
            bgel.oneBeanChanged(bean);
        }
    }

    public void setHeaderStyleName(String styleName) {
        table.getRowFormatter().setStyleName(0, styleName);
    }

    public void setTableStyleName(String styleName) {
        table.setStyleName(styleName);
    }

    @Override
    public int getBeanCount() {
        return beanCnt;
    }

    @Override
    public int getCurrPage() {
        return currPage;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    public int getNumberOfVisibleRows() {
        return beanToRowIdxMap.keySet().size();
    }

    boolean isIsUnknownPageCountModeEnabled() {
        return isUnknownPageCountModeEnabled;
    }

    void setIsUnknownPageCountModeEnabled(boolean isUnknownPageCountModeEnabled) {
        this.isUnknownPageCountModeEnabled = isUnknownPageCountModeEnabled;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public ColumnFormatter getColumnFormatter() {
        return table.getColumnFormatter();
    }

    public RowFormatter getRowFormatter() {
        return table.getRowFormatter();
    }

    public void setStyleName(String styleName) {
        table.setStyleName(styleName);
    }

}
