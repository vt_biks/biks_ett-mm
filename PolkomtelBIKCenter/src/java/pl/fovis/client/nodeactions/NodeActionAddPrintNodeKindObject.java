/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.TreeSelectorAttributeOrKindObjectToConfigPrintTemplateDialog;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class NodeActionAddPrintNodeKindObject extends NodeActionBase {

    @Override
    public boolean isEnabledByDesign() {
        if (!BIKClientSingletons.isEnablePrinting()) {
            return false;
        }
        return isMetaBiks() && BIKConstants.NODE_KIND_META_PRINT_ATTRIBUT.equalsIgnoreCase(node.nodeKindCode)
                && data.printAttribute != null && !BaseUtils.isStrEmptyOrWhiteSpace(data.printAttribute);
    }

    @Override
    public String getCaption() {
        return I18n.dodajTypObiektuZagniezdzony.get();
    }

    @Override
    public void execute() {
        new TreeSelectorAttributeOrKindObjectToConfigPrintTemplateDialog(BIKConstants.NODE_KIND_META_PRINT_ATTRIBUT, data.printAttribute).buildAndShowDialog(node.id, node.name,
                new IParametrizedContinuation<TreeNodeBean>() {
            @Override
            public void doIt(TreeNodeBean param) {

                BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get() /* I18N:  */);
                refreshRedisplayDataAndSelectRecord(param.id);
            }
        }, null
        );

    }

}
