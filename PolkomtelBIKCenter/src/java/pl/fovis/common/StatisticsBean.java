/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author mmastalerz
 */
public class StatisticsBean extends PeriodStatisticsBean implements Serializable {

    public String counterName;
    public String description;
    public int allCnt;
    public int total;
    public String treePath;
}
