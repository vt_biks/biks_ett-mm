﻿exec sp_check_version '1.3.0.2';
go

declare @glossaryTreeId int = dbo.fn_tree_id_by_code('Glossary')
declare @documentsTreeId int = dbo.fn_tree_id_by_code('Documents')
declare @userRolesTreeId int = dbo.fn_tree_id_by_code('UserRoles')
declare @dqcTreeId int = dbo.fn_tree_id_by_code('DQCMetadata')

update bik_node set obj_id='#IsBuiltIn$AllDocuments' where is_built_in=1 and name='Wszystkie dokumenty' and tree_id = @documentsTreeId and obj_id = null ;
update bik_node set obj_id='#IsBuiltIn$KeyWords' where is_built_in=1 and name='Słowa kluczowe' and tree_id = @glossaryTreeId and obj_id = null ;
update bik_node set obj_id='#IsBuiltIn$AllUsers' where is_built_in=1 and name='Wszyscy użytkownicy' and tree_id = @userRolesTreeId and obj_id = null ;
update bik_node set obj_id='#IsBuiltIn$AllTests' where is_built_in=1 and name='Wszystkie testy' and tree_id = @dqcTreeId and obj_id = null ;
update bik_node set obj_id='#IsBuiltIn$Author' where is_built_in=1 and name='Autor' and tree_id = @userRolesTreeId and obj_id = null ;
update bik_node set obj_id='#IsBuiltIn$Editor' where is_built_in=1 and name='Redaktor' and tree_id = @userRolesTreeId and obj_id = null ;
go

create table bik_help_translation(
id int identity(1,1) NOT NULL primary key,
help_key varchar(890)  not null,
caption varchar(max),
text_help varchar(max),
text_help_plain varchar(max),
lang varchar(3),
constraint UQ_bik_help_key_language unique (help_key,lang)
);
go


exec sp_update_version '1.3.0.2', '1.3.0.3';
go
