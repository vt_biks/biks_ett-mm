package pl.fovis.foxygwtcommons;

import simplelib.IRegExpMatcher;

public class JavaScriptRegExpMatcher implements IRegExpMatcher {

    private Object regExp;
    private String s;
    private Object execRes;

    public JavaScriptRegExpMatcher(Object regExp, String s) {
        this.regExp = regExp;
        this.s = s;
    }

    private static native String innerReplaceAll(Object regExp, String s, String replacement) /*-{
     return s.replace(regExp, replacement);
     }-*/;

    private static native String innerGroup(Object execRes, int grp) /*-{
     return execRes[grp];
     }-*/;

    private static native Object innerFindFromPos(Object regExp, String s, int fromPos) /*-{
     regExp.lastIndex = fromPos;
     return regExp.exec(s);
     }-*/;

    private static native Object innerFind(Object regExp, String s) /*-{
     return regExp.exec(s);
     }-*/;

    private static native int innerStart(Object execRes) /*-{
     return execRes.index;
     }-*/;

    private static native int innerEnd(Object regExp) /*-{
     return regExp.lastIndex;
     }-*/;

    public boolean find() {
        execRes = innerFind(regExp, s);
        return execRes != null;
    }

    public int start() {
        return innerStart(execRes);
    }

    public int end() {
        return innerEnd(regExp);
    }

    public String group(int grp) {
        return innerGroup(execRes, grp);
    }

    public String replaceAll(String replacement) {
        return innerReplaceAll(regExp, s, replacement);
    }

    public boolean find(int startPos) {
        execRes = innerFindFromPos(regExp, s, startPos);
        return execRes != null;
    }
}
