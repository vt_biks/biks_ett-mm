/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author bfechner
 */
public class NodeAuthorBean implements Serializable {

    public String userName;
    public int cnt;
    public int voteUsefulSum;
    public int voteUselessSum;
    public int nodeId;
    public String avatar;
}
