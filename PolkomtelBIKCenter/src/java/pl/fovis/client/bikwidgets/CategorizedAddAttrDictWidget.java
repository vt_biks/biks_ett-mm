/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog.AttributeType;
import pl.fovis.common.AttrHintBean;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author beata
 */
public class CategorizedAddAttrDictWidget extends CategorisedAttrWidgetBase {

    // czy zakładka dotyczy atrybutów wbudowanych/systemowych: 0-false, 1-true
    protected final static int IS_BUILT_IN = 0;

    public void displayData() {

        BIKClientSingletons.getService().getAttrDefAndCategory(new StandardAsyncCallback<List<AttributeBean>>() {
            public void onSuccess(List<AttributeBean> result) {
                displayFetchedData(result);
            }
        });
    }

    @Override
    protected String getAddItemButtonCaption() {
        return I18n.dodajAtrybut.get() /* I18N:  */;
    }

    @Override
    protected void updateItemInCategory(final int itemId, String name, int categoryId) {
//        BIKClientSingletons.getService().updateItemInCategory(itemId, categoryId, name, new StandardAsyncCallback<Void>("Failed in update attribute.") {
//
//            public void onSuccess(Void result) {
//                BIKClientSingletons.showInfo("Zmodyfikowano atrybut.");
//                refreshData();
//            }
//        });
    }

    protected void addItemInCategory(int categoryId, Integer attributeId, String itemName, String typeAttr, String valueOptList, boolean displayAsNumber, boolean usedInDrools) {
        BIKClientSingletons.getService().addAttributeDict(itemName, categoryId, typeAttr, valueOptList, displayAsNumber, usedInDrools, new StandardAsyncCallback<Integer>("Failed in adding new attribute" /* I18N: no:err-fail-in */) {
            public void onSuccess(Integer result) {
                BIKClientSingletons.showInfo(I18n.dodanoNowyAtrybut.get() /* I18N:  */);
                refreshData();
            }
        });
    }

    @Override
    protected Widget createOptCheckboxForCategoryWidget(final int catId) {
        final String check = categoryCheckboxImage.get(catId) != null ? categoryCheckboxImage.get(catId) : "0";
        final PushButton categoryBtn = new PushButton(new Image("images/checkbox" /* I18N: no */ + check + "." + "gif" /* I18N: no */));
        categoryCheckboxBtns.put(catId, categoryBtn);
        categoryBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

                if (selectedIdCnt > 0) {
                    for (Integer ic : itemsInCategory.get(catId)) {
                        int cnt = atrIdCntInCategory.get(catId) != null && atrIdCntInCategory.get(catId) == itemsInCategory.get(catId).size() ? 0 : selectedIdCnt;
                        atrIdsCnt.put(ic, cnt);
                    }
                    refreshCheckbox(atrIdsCnt, selectedIdCnt);
                } else {
                    Window.alert(I18n.zaznaczWezel.get() /* I18N:  */);
                }
            }
        });
        categoryBtn.addStyleName("checkboxAttr");
        return categoryCheckboxBtns.get(catId);
    }

    @Override
    protected Widget createOptCheckboxForItemWidget(final int attrId) {
        String check = atrIdsCheckboxImage.get(attrId) != null ? atrIdsCheckboxImage.get(attrId) : "0";

        PushButton attrCheckboxBtn = new PushButton(new Image("images/checkbox" /* I18N: no */ + check + "." + "gif" /* I18N: no */));
        atrIdsCheckboxBtns.put(attrId, attrCheckboxBtn);

        attrCheckboxBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (selectedIdCnt > 0) {
                    int cnt = atrIdsCheckboxImage.get(attrId) != null && atrIdsCheckboxImage.get(attrId).equals("1") ? 0 : selectedIdCnt;
                    atrIdsCnt.put(attrId, cnt);
                    refreshCheckbox(atrIdsCnt, selectedIdCnt);
                } else {
                    Window.alert(I18n.zaznaczWezel.get() /* I18N:  */);
                }
            }
        });
        attrCheckboxBtn.addStyleName("checkboxAttr");
        return atrIdsCheckboxBtns.get(attrId);
    }

    @Override
    protected void refreshData() {
        displayData();
    }

    @Override
    protected String getItemTypeForItem(AttributeBean item) {
        return item.typeAttr != null ? AttributeType.valueOf(item.typeAttr).caption : null;
    }

    protected void showDeleteItemConfirmDialog(final int itemId) {
        BIKClientSingletons.getService().getCountOfLinkedElements(itemId, new StandardAsyncCallback<Integer>("Failed in" /* I18N: no:err-fail-in */ + " getCountOfLinkedElements.") {
                    public void onSuccess(Integer result) {
                        new ConfirmDialog().buildAndShowDialog(I18n.czyNaPewnoChceszUsunacAtrybut.get() /* I18N:  */ + " "
                                + getItemNameForItemId(itemId) + "? " + I18n.tenAtrybutJestUzyty.get() /* I18N:  */ + " " + result + " " + I18n.razy.get() /* I18N:  */ + ".", I18n.usun.get() /* I18N:  */, new IContinuation() {
                                    public void doIt() {
                                        BIKClientSingletons.getService().deleteAttributeDict(itemId,
                                                new StandardAsyncCallback<Void>("Failed in update attribute" /* I18N: no:err-fail-in */ + ".") {
                                                    public void onSuccess(Void result) {
                                                        BIKClientSingletons.showInfo(I18n.usunietoAtrybut.get() /* I18N:  */ + ".");
                                                        int categoryId = getCategoryIdForItemId(itemId);
                                                        if (atrIdCntInCategory.get(categoryId) != null) {
                                                            if (atrIdCntInCategory.get(categoryId) == 1) {
                                                                categoryCheckboxImage.remove(categoryId);
                                                            }
                                                            atrIdsCnt.remove(itemId);
                                                        }
                                                        refreshData();
                                                    }
                                                });
                                    }
                                });
                    }
                });
    }

    @Override
    protected PushButton saveBtn() {
        PushButton saveButton = new PushButton(I18n.zapiszZmiany.get() /* I18N:  */);
        NewLookUtils.makeCustomPushButton(saveButton);
        saveButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().addOrDeleteAttributeDictForKind(atrIdsCnt, selectedNodeKindIds, new StandardAsyncCallback<Void>("Failed in save changes" /* I18N: no:err-fail-in */) {
                    @Override
                    public void onSuccess(Void result) {

                        for (int snk : selectedNodeKindIds) {

                            if (atrIdsCnt != null) {
                                for (Entry<Integer, Integer> atr : atrIdsCnt.entrySet()) {
                                    List<Integer> l = BIKClientSingletons.getAttributeDict().get(snk);
                                    if (l == null) {
                                        l = new ArrayList<Integer>();
                                        if (atr.getValue() != 0) {
                                            l.add(atr.getKey());
                                        }
                                    } else if (!l.contains(atr.getKey())) {
                                        if (atr.getValue() == selectedNodeKindIds.size()) {
                                            l.add(atr.getKey());
                                        }
                                    } else if (atr.getValue() == 0) {
                                        l.remove(atr.getKey());

                                    }
                                    BIKClientSingletons.getAttributeDict().put(snk, l);
                                    BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get() /* I18N:  */);
                                }
                            }
                        }
                    }
                });
            }
        });
        return saveButton;
    }

    public void recalcCheckBoxesByNodeKindIds(Set<Integer> selectedNodeKindIds) {
        this.selectedNodeKindIds = selectedNodeKindIds;
        atrIdsCnt.clear();
        for (int snk : selectedNodeKindIds) {
            List<Integer> l = BIKClientSingletons.getAttributeDict().get(snk);
            if (l != null) {
                for (Integer n : l) {
                    int cnt = atrIdsCnt != null && atrIdsCnt.containsKey(n) ? atrIdsCnt.get(n) + 1 : 1;
                    atrIdsCnt.put(n, cnt);
                }
            }
        }
        selectedIdCnt = selectedNodeKindIds.size();
        refreshCheckbox(atrIdsCnt, selectedIdCnt);
    }

    public void refreshCheckbox(Map<Integer, Integer> atrIdAndCnt, int selectedIdCnt) {
        String checkImg = "0";
        atrIdsCheckboxImage.clear();
        atrIdCntInCategory.clear();
        Map<Integer, Boolean> atrIdInCategoryCheck2 = new HashMap<Integer, Boolean>();
        for (Entry<Integer, PushButton> btn : atrIdsCheckboxBtns.entrySet()) {
            if (!atrIdAndCnt.containsKey(btn.getKey())) {
                btn.getValue().getUpFace().setImage(new Image("images/checkbox" /* I18N: no */ + checkImg + "." + "gif" /* I18N: no */));
            }
        }
        for (Entry<Integer, Integer> m : atrIdAndCnt.entrySet()) {
            if (m.getValue() == 0) {
                checkImg = "0";
            } else {
                checkImg = m.getValue() == selectedIdCnt ? "1" : "2";
                int categoryId = getCategoryIdForItemId(m.getKey());
                int cnt = atrIdCntInCategory.get(categoryId) != null && checkImg.equals("1") ? atrIdCntInCategory.get(categoryId) + 1 : 1;
                atrIdCntInCategory.put(categoryId, cnt);
                if (checkImg.equals("2")/*
                         * m.getValue() == 2
                         */) {
                    atrIdInCategoryCheck2.put(categoryId, true);
                }
            }
            atrIdsCheckboxImage.put(m.getKey(), checkImg);
            atrIdsCheckboxBtns.get(m.getKey()).getUpFace().setImage(new Image("images/checkbox" /* I18N: no */ + checkImg + "." + "gif" /* I18N: no */));
        }
        setCategoryCheckboxes(atrIdInCategoryCheck2);
    }

    @Override
    protected int getAttributeType() {
        return IS_BUILT_IN;
    }

    @Override
    protected void showAddItemInCategoryDialog(final int categoryId) {
        BIKClientSingletons.getService().getAttributes(new StandardAsyncCallback<List<AttrHintBean>>("Error in" /* I18N: no */ + " getAttributes()") {
                    @Override
                    public void onSuccess(List<AttrHintBean> result) {
                        new AddOrEditAdminAttributeWithTypeDialog().buildAndShowDialog(category, getAddItemButtonCaption(), null, result,
                                new IParametrizedContinuation<AttributeBean>() {
                                    @Override
                                    public void doIt(final AttributeBean param) {
                                        String attrName = param.atrName.trim();
                                        addItemInCategory(categoryId, null, attrName, param.typeAttr, param.valueOpt, param.displayAsNumber, param.usedInDrools);
                                        BIKClientSingletons.addNewAttrToHintsMap(attrName);
                                        refreshData();
                                    }
                                });
                    }
                });
    }

    @Override
    protected void showEditItemDialog(final int itemId, String attrOrRoleName) {
        final int categoryId = getCategoryIdForItemId(itemId);
        AttributeBean item = items.get(itemId);

        final AttributeBean ab = new AttributeBean();
        ab.id = itemId;
        ab.atrName = attrOrRoleName;
        ab.typeAttr = getItemTypeForItem(item);
        ab.valueOpt = item.valueOpt;
        ab.categoryId = categoryId;
        ab.displayAsNumber = item.displayAsNumber;
        ab.usedInDrools = item.usedInDrools;

        BIKClientSingletons.getService().getAttributes(new StandardAsyncCallback<List<AttrHintBean>>("Error in" /* I18N: no */ + " getAttributes") {
                    @Override
                    public void onSuccess(List<AttrHintBean> result) {
                        new AddOrEditAdminAttributeWithTypeDialog().buildAndShowDialog(category, I18n.edytujAtrybut.get() /* I18N:  */, ab, result,
                                new IParametrizedContinuation<AttributeBean>() {
                                    @Override
                                    public void doIt(final AttributeBean param) {
                                        BIKClientSingletons.getService().updateItemInCategory(itemId, param.categoryId, param.atrName.trim(), param.typeAttr, param.valueOpt, param.displayAsNumber, param.usedInDrools, new StandardAsyncCallback<Void>("Failed in update attribute" /* I18N: no:err-fail-in */ + ".") {
                                            @Override
                                            public void onSuccess(Void result) {
                                                BIKClientSingletons.showInfo(I18n.zmodyfikowanoAtrybut.get() /* I18N:  */ + ".");
                                                refreshData();
                                            }
                                        });
                                    }
                                });
                    }
                });
    }
}
