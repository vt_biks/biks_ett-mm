/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import pl.fovis.foxygwtcommons.dialogs.validators.WidgetValueValidatorParsing;
import pl.fovis.foxygwtcommons.dialogs.validators.WidgetValueValidatorLength;
import pl.fovis.foxygwtcommons.dialogs.validators.WidgetValueValidatorRequired;
import pl.fovis.foxygwtcommons.dialogs.validators.FoxyGWTValidationException;
import pl.fovis.foxygwtcommons.dialogs.validators.IWidgetValueValidator;
import pl.fovis.foxygwtcommons.dialogs.validators.IValidationStateChangeHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TextBoxBase;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.foxygwtcommons.dialogs.valuehandlers.DateBoxValueHandler;
import pl.fovis.foxygwtcommons.dialogs.valuehandlers.IWidgetValueHandler;
import pl.fovis.foxygwtcommons.dialogs.valuehandlers.IWidgetValueHandler.GetValueExtraInfo;
import pl.fovis.foxygwtcommons.dialogs.valuehandlers.IWidgetValueHandler.SetValueExtraInfo;
import pl.fovis.foxygwtcommons.dialogs.valuehandlers.ListBoxValueHandler;
import pl.fovis.foxygwtcommons.dialogs.valuehandlers.SuggestBoxValueHandler;
import pl.fovis.foxygwtcommons.dialogs.valuehandlers.TextBoxValueHandler;
import simplelib.BaseUtils;
import simplelib.INamedTypedBeanPropsBeanWrapperCreator;
import simplelib.INamedTypedPropsBean;
import simplelib.INamedTypedPropsBeanBroker;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import pl.fovis.foxygwtcommons.i18n.I18n;

/**
 *
 * @author pmielanczuk
 */
public class FoxyDialogSerializer<BB, INB extends BB, OUTB extends BB> {

    //ww: TRIM_STRING_VALUES == true --> trimuje stringi, aby traktować
    // same spacje jak brak wartości - to jest ważne tylko przy sprawdzaniu
    // wymagalności (walidator)
    public static boolean TRIM_STRING_VALUES = true;
    //ww: czy wszystkie wartości mają być trimowane? wtedy też liczby
    // będą miały obcięte białe znaki z obu końców i się dobrze sparsują
    // UWAGA: ustawienie TRIM_ALL_VALUES == true sprawia, że
    // TRIM_STRING_VALUES działa tak jakby było równe true (bo sama wartość już
    // ma obcięte spacje, więc przy sprawdzaniu siłą rzeczy wyjdzie na ew. pustą)
    public static boolean TRIM_ALL_VALUES = true;
    protected INamedTypedBeanPropsBeanWrapperCreator<BB> beanWrapperCreator;
    protected INamedTypedBeanPropsBeanWrapperCreator<IHasInputControls> dialogWrapperCreator;
//    protected final INamedTypedPropsBean dialogPropsBean;
//    protected final Set<String> dialogPropNames;
    protected Map<String, String> beanPropNamesToDialogPropNamesMap;
    protected Map<String, Widget> dialogPropNameToInputControl = new HashMap<String, Widget>();
    protected List<IWidgetValueHandler<?>> valueHandlers = new ArrayList<IWidgetValueHandler<?>>();
    protected Map<Class, IWidgetValueHandler<?>> valueHandlersByEffectiveWidgetClasses = new LinkedHashMap<Class, IWidgetValueHandler<?>>();
    protected Map<Widget, String> inputControlToBeanPropNameMap;
//    protected IParametrizedContinuation<Object> valueChangedHandler;
    protected IValidationStateChangeHandler validationStateChangeHandler;
    protected Set<String> inputControlPropNameSuffixes = new LinkedHashSet<String>();
//    protected Class<?> outputBeanClass;
    protected INamedTypedPropsBeanBroker<OUTB> outputBeanBroker;
    protected Map<String, List<IWidgetValueValidator>> beanPropNameToValidatorsMap = new LinkedHashMap<String, List<IWidgetValueValidator>>();
    protected Map<IWidgetValueValidator, List<String>> validatorToBeanPropNamesMap = new LinkedHashMap<IWidgetValueValidator, List<String>>();
    protected IHasInputControls dialog;
    protected Map<Widget, Set<IWidgetValueValidator>> invalidControlsMap = new HashMap<Widget, Set<IWidgetValueValidator>>();
    protected Set<IWidgetValueValidator> disabledValidators = new HashSet<IWidgetValueValidator>();

    public FoxyDialogSerializer(INamedTypedBeanPropsBeanWrapperCreator<IHasInputControls> dialogWrapperCreator,
            INamedTypedBeanPropsBeanWrapperCreator<BB> beanWrapperCreator,
            IHasInputControls dialog) {
        this.dialogWrapperCreator = dialogWrapperCreator;
        this.beanWrapperCreator = beanWrapperCreator;
//        this.dialogPropsBean = this.dialogWrapperCreator.create(dialog);
        this.dialog = dialog;

        registerValueHandlersPrivate();
        registerAllInputControlPropNameSuffixesPrivate();

//        this.dialogPropNames = dialogPropsBean.getPropNames();
    }

    public void setValidatorsEnabled(boolean enabled, IWidgetValueValidator... validators) {
        Set<IWidgetValueValidator> changedValidators = new HashSet<IWidgetValueValidator>(Arrays.asList(validators));

        if (enabled) {
            changedValidators.retainAll(disabledValidators);
            disabledValidators.removeAll(changedValidators);
        } else {
            changedValidators.removeAll(disabledValidators);
            disabledValidators.addAll(changedValidators);
        }

        validateValidators(changedValidators);
    }

    //ww: uzupełnia ważne pola, używane potem przez mechanizmy 
    // walidacji / serializacji
    protected void fillPropsMaps(IHasInputControls dialogOrPart) {

        INamedTypedPropsBean dialogPropsBean = this.dialogWrapperCreator.create(dialogOrPart);
        Set<String> dialogPropNames = dialogPropsBean.getPropNames();

        for (String dialogPropName : dialogPropNames) {
            Object propVal = dialogPropsBean.getPropValue(dialogPropName);
            if (propVal instanceof IHasInputControls) {
                IHasInputControls hic = (IHasInputControls) propVal;
                fillPropsMaps(hic);
            } else {
                String beanPropName = convertDialogPropNameToBeanPropNamePrivate(dialogPropName);
                if (beanPropName != null) {
                    beanPropNamesToDialogPropNamesMap.put(beanPropName, dialogPropName);
                    dialogPropNameToInputControl.put(dialogPropName, (Widget) propVal);
                }
            }
        }
    }

    protected void ensureBeanPropNamesToDialogPropNamesMapInitialized() {
        if (beanPropNamesToDialogPropNamesMap != null) {
            return;
        }

        beanPropNamesToDialogPropNamesMap = new HashMap<String, String>();

        fillPropsMaps(dialog);
    }

    protected IWidgetValueValidator createParsingValidator(String beanPropName, Widget inputControl) {
        return new WidgetValueValidatorParsing<BB, INB, OUTB>(this, inputControl);
    }

    public void setOutputBeanClass(Class<OUTB> outputBeanClass) {
        ensureBeanPropNamesToDialogPropNamesMapInitialized();

//        this.outputBeanClass = outputBeanClass;        
        this.outputBeanBroker = beanWrapperCreator.getBroker(outputBeanClass);
        Set<String> outputPropNames = outputBeanBroker.getPropNames();

        //ww: podłączamy specjalne walidatory "parsujące"  
        // (BYŁO: pod pola, które ich nie mają - takie puste)
        // mają one na celu ściągnięcie wartości z kontrolki i zderzenie tego
        // z wartością property w beanie - np. textbox vs int - nie każda wartość
        // w TextBox jest dobra dla inta
        // teraz podłączamy je zawsze, bo zwykłe validatory nie zawsze badają 
        // wartości przez getInputControlValue()!
        for (String propName : beanPropNamesToDialogPropNamesMap.keySet()) {
            //ww: zmiana podejścia, podłączamy pod wszystko
            if (/*!beanPropNameToValidatorsMap.containsKey(propName) &&*/outputPropNames.contains(propName)) {
                final Widget inputControl = findInputWidgetForBeanPropName(propName);
                addValidatorByBeanPropName(propName, createParsingValidator(propName, inputControl));
            }
        }
    }

    private void registerAllInputControlPropNameSuffixesPrivate() {
        registerAllInputControlPropNameSuffixes();
    }

    public void registerInputControlPropNameSuffixes(String... suffixes) {
        BaseUtils.addMany(inputControlPropNameSuffixes, suffixes);
    }

    protected void registerAllInputControlPropNameSuffixes() {
        //i18n-default: no
        registerInputControlPropNameSuffixes("Tb" /* I18N: no */, "Lb" /* I18N: no */, "Db" /* I18N: no */, "Ta" /* I18N: no */, "Sb" /* I18N: no */);
        //i18n-default: 
    }

    private void registerValueHandlersPrivate() {
        registerValueHandlers();
    }

    protected void registerValueHandlers() {
        registerValueHandler(TextBoxBase.class, new TextBoxValueHandler(TRIM_ALL_VALUES));
        registerValueHandler(DateBox.class, new DateBoxValueHandler());
        registerValueHandler(ListBox.class, new ListBoxValueHandler());
        registerValueHandler(SuggestBox.class, new SuggestBoxValueHandler());
    }

    private String convertDialogPropNameToBeanPropNamePrivate(String dialogPropName) {
        return convertDialogPropNameToBeanPropName(dialogPropName);
    }

    protected String convertDialogPropNameToBeanPropName(String dialogPropName) {
        return BaseUtils.dropOneOfSuffixesOrReturnNull(dialogPropName, false, inputControlPropNameSuffixes);
    }

    public Widget getInputControlByDialogName(String inputControlName) {
        return dialogPropNameToInputControl.get(inputControlName);
    }

    public void fillControlsFromBean(INB inputBean) {
        ensureBeanPropNamesToDialogPropNamesMapInitialized();

//        Window.alert("odpalam fillControlsFromBean!");
        if (inputBean == null) {
            return;
        }

        INamedTypedPropsBean beanNamedProps = beanWrapperCreator.create(inputBean);
        for (String beanPropName : beanNamedProps.getPropNames()) {
            String dialogPropName = beanPropNamesToDialogPropNamesMap.get(beanPropName);
            if (dialogPropName == null) {
                continue;
            }

            Object inputControl = //dialogPropsBean.getPropValue(dialogPropName);
                    getInputControlByDialogName(dialogPropName);
            setInputControlValue(inputControl, beanNamedProps.getPropValue(beanPropName),
                    beanPropName, beanNamedProps);
        }
    }

    protected void setInputControlValue(Object inputControl, Object propValue, String beanPropName,
            INamedTypedPropsBean beanNamedProps) {
        IWidgetValueHandler<Object> valueHandler = findValueHandler(inputControl);
        if (valueHandler == null) {
            setInputControlValueNoHandler(inputControl, propValue);
            return;
        }

        valueHandler.setValueToWidget(inputControl, propValue,
                new SetValueExtraInfo(beanPropName, beanNamedProps));
    }

    protected void setInputControlValueNoHandler(Object inputControl, Object propValue) {
        throw new LameRuntimeException("setInputControlValue undefined for inputControl class" /* I18N: no */+ " "
                + BaseUtils.safeGetClassName(inputControl));
    }

    public Object getInputControlValue(Widget inputControl) throws FoxyGWTValidationException {
        String beanPropName = findBeanPropNameForInputWidget(inputControl);

        Class beanPropClass;

        if (beanPropName != null && outputBeanBroker != null && outputBeanBroker.getPropNames().contains(beanPropName)) {
            beanPropClass = outputBeanBroker.getPropClass(beanPropName);
        } else {
            beanPropClass = Object.class;
        }

        return getInputControlValueInner(inputControl, beanPropClass, beanPropName);
    }

    protected Object convertInputControlValueToBeanPropClass(Object value, Class outputPropClass) throws FoxyGWTValidationException {
        if (value == null) {
            return null;
        }

        if (outputPropClass == Integer.class && !(value instanceof Integer)) {
            String valAsStr = value.toString();
            Integer res = BaseUtils.tryParseInteger(valAsStr);
            if (res == null) {
                throw new FoxyGWTValidationException(I18n.wprowadzWartoscNieJestLiczbaCalk.get() /* I18N:  */);
            }
            return res;
        }

        if (outputPropClass == Double.class && !(value instanceof Double)) {
            String valAsStr = value.toString();
            Double res = BaseUtils.tryParseDouble(valAsStr);
            if (res == null) {
                throw new FoxyGWTValidationException(I18n.wprowadzonaWartoscNieJestLiczba.get() /* I18N:  */);
            }
            return res;
        }

        return value;
    }

    protected Object getInputControlValueInner(Object inputControl, Class outputPropClass,
            String beanPropName) throws FoxyGWTValidationException {
        IWidgetValueHandler<Object> valueHandler = findValueHandler(inputControl);
        if (valueHandler == null) {
            return getInputControlValueNoHandler(inputControl, outputPropClass);
        }

        Class noPrimClass = BaseUtils.fixPrimitiveClassToEnvelope(outputPropClass);

        Object value = valueHandler.getValueFromWidget(inputControl, outputPropClass,
                new GetValueExtraInfo(beanPropName, noPrimClass));

        return convertInputControlValueToBeanPropClass(value, noPrimClass);
    }

    protected Object getInputControlValueNoHandler(Object inputControl, Class outputPropClass) {
        throw new LameRuntimeException("getInputControlValue undefined for inputControl class" /* I18N: no */+ " "
                + BaseUtils.safeGetClassName(inputControl) + " " +"and outputPropClass" /* I18N: no */+ ": "
                + outputPropClass.getName());
    }

    public String execOneValidator(IWidgetValueValidator validator) {
        String res;
        try {
            res = disabledValidators.contains(validator) ? null : validator.validate();
        } catch (FoxyGWTValidationException ex) {
            res = ex.getMessage();
        }

        return res;
    }

    protected Pair<String, IWidgetValueValidator> execOptValidators(List<IWidgetValueValidator> optValidators) {
        if (optValidators == null) {
            return null;
        }

        for (IWidgetValueValidator validator : optValidators) {
            String res = execOneValidator(validator);
            if (res != null) {
                return new Pair<String, IWidgetValueValidator>(res, validator);
            }
        }
        return null;
    }

    public Pair<String, IWidgetValueValidator> validateInputControl(Widget inputControl) {
        String beanPropName = findBeanPropNameForInputWidget(inputControl);
        if (beanPropName == null) {
            return null;
        }

        return execOptValidators(beanPropNameToValidatorsMap.get(beanPropName));
    }

    public Map<String, String> validateForm() {
        Map<String, String> res = new LinkedHashMap<String, String>();

        for (Entry<IWidgetValueValidator, List<String>> e : validatorToBeanPropNamesMap.entrySet()) {

            String errMsg = execOneValidator(e.getKey());

            if (errMsg != null) {
                for (String beanPropName : e.getValue()) {
                    if (!res.containsKey(beanPropName)) {
                        res.put(beanPropName, errMsg);
                    }
                }
            }
        }

        return res;
    }

    public Widget findInputWidgetForBeanPropName(String beanPropName) {
        ensureBeanPropNamesToDialogPropNamesMapInitialized();

        String dialogPropName = beanPropNamesToDialogPropNamesMap.get(beanPropName);
        if (dialogPropName == null) {
            return null;
        }

        return //dialogPropsBean.getPropValue(dialogPropName);
                getInputControlByDialogName(dialogPropName);
    }

    public void fillBeanFromControls(OUTB outputBean) {
//        Window.alert("odpalam fillBeanFromControls!");
        @SuppressWarnings("unchecked")
        Class<OUTB> beanClass = (Class<OUTB>) outputBean.getClass();
        setOutputBeanClass(beanClass);

        for (String beanPropName : outputBeanBroker.getPropNames()) {
            Object inputControl = findInputWidgetForBeanPropName(beanPropName);
            if (inputControl == null) {
                continue;
            }

            Object inputValue;
            Class propClass = outputBeanBroker.getPropClass(beanPropName);
            try {
                inputValue = getInputControlValueInner(inputControl, propClass, beanPropName);
            } catch (FoxyGWTValidationException ex) {
                inputValue = null;
            }
            outputBeanBroker.setPropValue(outputBean, beanPropName, inputValue);
        }

//        INamedTypedPropsBean beanNamedProps = beanWrapperCreator.create(outputBean);
//        for (String beanPropName : beanNamedProps.getPropNames()) {
//            String dialogPropName = beanPropNamesToDialogPropNamesMap.get(beanPropName);
//            if (dialogPropName == null) {
//                continue;
//            }
//
//            Object inputControl = dialogPropsBean.getPropValue(dialogPropName);
//            Class propClass = beanNamedProps.getPropClass(beanPropName);
//            Object inputValue = getInputControlValueNoHandler(inputControl, propClass);
//            beanNamedProps.setPropValue(beanPropName, inputValue);
//        }
    }

    public <B extends BB> void fillListBox(ListBox lb, Iterable<B> beans, String idProp, String nameProp, String selectedValue) {

        for (B bean : beans) {
            INamedTypedPropsBean ntpb = beanWrapperCreator.create(bean);

            String id = BaseUtils.safeToString(ntpb.getPropValue(idProp));
            String name = BaseUtils.safeToString(ntpb.getPropValue(nameProp));

            lb.addItem(name, id);
            if (id.equals(selectedValue)) {
                lb.setSelectedIndex(lb.getItemCount() - 1);
            }
        }
    }

    public <T> void registerValueHandler(Class<T> widgetClass, IWidgetValueHandler<T> handler) {
        valueHandlers.add(handler);
        valueHandlersByEffectiveWidgetClasses.put(widgetClass, handler);
    }

    public <W> IWidgetValueHandler<W> findValueHandler(W w) {
        Class<?> widgetClass = w.getClass();
        IWidgetValueHandler<?> fromCache = valueHandlersByEffectiveWidgetClasses.get(widgetClass);
        if (fromCache == null) {
            for (int idx = valueHandlers.size() - 1; idx >= 0; idx--) {
                IWidgetValueHandler<?> handler = valueHandlers.get(idx);
                if (handler.isWidgetClassHandled(w)) {
                    fromCache = handler;
                    break;
                }
            }
            if (fromCache != null) {
                valueHandlersByEffectiveWidgetClasses.put(widgetClass, fromCache);
            }
        }
        @SuppressWarnings("unchecked")
        IWidgetValueHandler<W> res = (IWidgetValueHandler) fromCache;
        return res;
    }

    public void fixUpInputControlsIfNeeded() {
        if (inputControlToBeanPropNameMap != null) {
            return;
        }

        ensureBeanPropNamesToDialogPropNamesMapInitialized();

        inputControlToBeanPropNameMap = new HashMap<Widget, String>();
        for (Entry<String, String> e : beanPropNamesToDialogPropNamesMap.entrySet()) {
            String beanPropName = e.getKey();
            String dialogPropName = e.getValue();
            Widget iw = //dialogPropsBean.getPropValue(dialogPropName);
                    getInputControlByDialogName(dialogPropName);
            inputControlToBeanPropNameMap.put(iw, beanPropName);
        }
    }

    protected String findBeanPropNameForInputWidget(Widget inputControl) {
        fixUpInputControlsIfNeeded();

        return inputControlToBeanPropNameMap.get(inputControl);
//        //ww: uwaga, to jest nieefektywne! proof-of-concept! do poprawki!
//        for (Entry<String, String> e : beanPropNamesToDialogPropNamesMap.entrySet()) {
//            String beanPropName = e.getKey();
//            String dialogPropName = e.getValue();
//            Object iw = dialogPropsBean.getPropValue(dialogPropName);
//            if (iw == inputControl) {
//                return beanPropName;
//            }
//        }
//        return null;
    }

    protected IWidgetValueValidator createValueRequiredValidator(String beanPropName, Widget inputControl) {
        return new WidgetValueValidatorRequired<BB, INB, OUTB>(this, inputControl, TRIM_STRING_VALUES);
    }

    protected IWidgetValueValidator createValueLengthValidator(String beanPropName, final Widget inputControl, final Integer minLength, final Integer maxLength) {
        return new WidgetValueValidatorLength<BB, INB, OUTB>(this, inputControl, minLength, maxLength);
    }

    //ww: na lepszą nazwę klasy zabrakło chwilowo weny :-(
    protected class UberChangesHandler implements ChangeHandler, KeyPressHandler, KeyUpHandler,
            ValueChangeHandler<Object>, ClickHandler {

//        Object lastVal;
//        boolean wasError;
        Set<Widget> inputControls;
        Map<Widget, Object> inputControlToLastValMap = new HashMap<Widget, Object>();
        Map<Object, Widget> childControlToProperInputControlParentMap = new HashMap<Object, Widget>();

        public UberChangesHandler(List<Widget> inputControls) {
            this.inputControls = new HashSet<Widget>(inputControls);
        }

        protected Widget fixInputControlIfChild(Object inputControlOrChild) {
            Widget icFix = childControlToProperInputControlParentMap.get(inputControlOrChild);
            if (icFix != null) {
                return icFix;
            }

            if (!(inputControlOrChild instanceof Widget)) {
                throw new LameRuntimeException("unknown child or inputControl not a Widget (class" /* i18n: no */+ "=" + BaseUtils.safeGetClassName(inputControlOrChild) + ")");
            }
            Widget iw = (Widget) inputControlOrChild;
            if (!inputControls.contains(iw)) {
                throw new LameRuntimeException("unknown child or inputControl" /* i18n: no */);
            }
            return iw;
        }

        protected void maybeCallValueChanged(Object inputControlOrChild) {
            Widget iw = fixInputControlIfChild(inputControlOrChild);

            boolean isError = false;
            Object currVal;

            try {
                currVal = getInputControlValue(iw);
            } catch (FoxyGWTValidationException ex) {
                isError = true;
                currVal = null;
            }

            boolean wasError = !inputControlToLastValMap.containsKey(iw);
            Object lastVal = inputControlToLastValMap.get(iw);

            if (wasError == isError && BaseUtils.safeEquals(currVal, lastVal)) {
                return;
            }

            if (isError) {
                inputControlToLastValMap.remove(iw);
            } else {
                inputControlToLastValMap.put(iw, currVal);
            }

            inputControlValueChanged(iw);
//            valueChangedHandler.doIt(inputControlOrChild);
        }

        public void onChange(ChangeEvent event) {
            maybeCallValueChanged(event.getSource());
        }

        public void onKeyPress(KeyPressEvent event) {
            maybeCallValueChanged(event.getSource());
        }

        public void onKeyUp(KeyUpEvent event) {
            maybeCallValueChanged(event.getSource());
        }

        public void onValueChange(ValueChangeEvent event) {
            maybeCallValueChanged(event.getSource());
        }

        public void onClick(ClickEvent event) {
            maybeCallValueChanged(event.getSource());
        }

        public void addChildForInputControl(Widget inputControl, Object child) {
            inputControl = fixInputControlIfChild(inputControl);
            childControlToProperInputControlParentMap.put(child, inputControl);
        }
    }

    protected void inputControlValueChanged(Widget inputControl) {
        validateValidators(getValidatorsForControl(inputControl));
    }

    protected void addUberHandlerToInputControl(Widget inputControl, UberChangesHandler handler) {
        if (inputControl instanceof HasChangeHandlers) {
            HasChangeHandlers hh = (HasChangeHandlers) inputControl;
            hh.addChangeHandler(handler);
        }

        if (inputControl instanceof HasKeyUpHandlers) {
            HasKeyUpHandlers hh = (HasKeyUpHandlers) inputControl;
            hh.addKeyUpHandler(handler);
        }

        if (inputControl instanceof HasValueChangeHandlers) {
            @SuppressWarnings("unchecked")
            HasValueChangeHandlers<Object> hh = (HasValueChangeHandlers) inputControl;
            hh.addValueChangeHandler(handler);
        }

        if (inputControl instanceof HasClickHandlers) {
            HasClickHandlers hh = (HasClickHandlers) inputControl;
            hh.addClickHandler(handler);
        }

        if (inputControl instanceof DateBox) {
            TextBox dtb = ((DateBox) inputControl).getTextBox();
            handler.addChildForInputControl(inputControl, dtb);
            addUberHandlerToInputControl(dtb, handler);
        }

        if (inputControl instanceof IHasAdditionalValueChangesSourceControl) {
            Widget source = ((IHasAdditionalValueChangesSourceControl) inputControl).getAdditionalValueChangesSourceControl();
            handler.addChildForInputControl(inputControl, source);
            addUberHandlerToInputControl(source, handler);
        }
    }

    public IWidgetValueValidator addValidatorByBeanPropName(String beanPropName, IWidgetValueValidator validator) {
        addValidatorByBeanPropNames(validator, beanPropName);
        return validator;
    }

    public void addValidatorByBeanPropNames(IWidgetValueValidator validator, String... beanPropNames) {

        for (String beanPropName : beanPropNames) {
            BaseUtils.addToCollectingMapWithList(beanPropNameToValidatorsMap, beanPropName, validator);
        }

        validatorToBeanPropNamesMap.put(validator, Arrays.asList(beanPropNames));

        if (/*valueChangedHandler == null*/validationStateChangeHandler == null) {
            return;
        }

        List<Widget> inputControls = new ArrayList<Widget>();
        for (String beanPropName : beanPropNames) {
            inputControls.add(findInputWidgetForBeanPropName(beanPropName));
        }
        UberChangesHandler kkk = new UberChangesHandler(inputControls);
        for (Widget inputControl : inputControls) {
            addUberHandlerToInputControl(inputControl, kkk);
        }
    }

    public IWidgetValueValidator addValidator(Widget inputControl, IWidgetValueValidator validator) {
        return addValidatorByBeanPropName(findBeanPropNameForInputWidget(inputControl), validator);
    }

    public IWidgetValueValidator addValidator(IWidgetValueValidator validator, Widget... inputControls) {

        String[] beanPropNames = new String[inputControls.length];
        int idx = 0;
        for (Widget inputControl : inputControls) {
            String beanPropName = findBeanPropNameForInputWidget(inputControl);
            if (beanPropName == null) {
                throw new LameRuntimeException("cannot add validator, no bean prop for input control" /* I18N: no */+ "!");
            }
            beanPropNames[idx++] = beanPropName;
        }
        addValidatorByBeanPropNames(validator, beanPropNames);
        return validator;
    }

    public IWidgetValueValidator addValueRequiredValidator(final Widget inputControl) {
        final String beanPropName = findBeanPropNameForInputWidget(inputControl);
//        final String dialogPropName = beanPropNamesToDialogPropNamesMap.get(beanPropName);

        return addValidatorByBeanPropName(beanPropName, createValueRequiredValidator(beanPropName, inputControl));
    }

    public IWidgetValueValidator addValueLengthValidator(final Widget inputControl, final Integer minLength, final Integer maxLength) {
        final String beanPropName = findBeanPropNameForInputWidget(inputControl);
//        final String dialogPropName = beanPropNamesToDialogPropNamesMap.get(beanPropName);

        return addValidatorByBeanPropName(beanPropName, createValueLengthValidator(beanPropName, inputControl, minLength, maxLength));
    }

    public void addValueRequiredValidators(Widget... inputWidgets) {
        for (Widget inputControl : inputWidgets) {
            addValueRequiredValidator(inputControl);
        }
    }

    public void setValidationStateChangeHandler(IValidationStateChangeHandler handler) {
        this.validationStateChangeHandler = handler;
    }
//    public void setValueChangedHandler(IParametrizedContinuation<Object> handler) {
//        this.valueChangedHandler = handler;
//    }

    public List<IWidgetValueValidator> getValidatorsForControl(Widget inputControl) {
        String beanPropName = findBeanPropNameForInputWidget(inputControl);
        return beanPropNameToValidatorsMap.get(beanPropName);
    }

    public List<Widget> getControlsForValidator(IWidgetValueValidator validator) {
        List<String> beanPropNames = validatorToBeanPropNamesMap.get(validator);
        List<Widget> res = new ArrayList<Widget>();
        if (beanPropNames != null) {
            for (String beanPropName : beanPropNames) {
                Widget inputControl = findInputWidgetForBeanPropName(beanPropName);
                if (inputControl != null) {
                    res.add(inputControl);
                }
            }
        }
        return res;
    }

    public Collection<IWidgetValueValidator> getAllValidators() {
        return validatorToBeanPropNamesMap.keySet();
    }

    public boolean hasValidationErrors() {
        return !invalidControlsMap.isEmpty();
    }

    public void validateValidators(IWidgetValueValidator... validatorsToExecute) {
        validateValidators(Arrays.asList(validatorsToExecute));
    }

    public void validateValidators(Iterable<IWidgetValueValidator> validatorsToExecute) {
        if (validatorsToExecute == null) {
            return;
        }

        Set<Object> nowProperControls = new HashSet<Object>();
        Set<Object> nowFailedControls = new HashSet<Object>();
        Set<Object> stillFailedControls = new HashSet<Object>();
        Map<Object, Set<String>> newErrorMessages = new HashMap<Object, Set<String>>();

        for (IWidgetValueValidator validator : validatorsToExecute) {
            List<Widget> controlsForValidator = getControlsForValidator(validator);

            String errMsg = execOneValidator(validator);

            for (Widget oneControl : controlsForValidator) {
                Set<IWidgetValueValidator> failedValidatorsForOneControl = invalidControlsMap.get(oneControl);

                if (errMsg != null) {
                    BaseUtils.addToCollectingMapWithLinkedSet(newErrorMessages, oneControl, errMsg);
                    if (failedValidatorsForOneControl == null) {
                        //ww: dopiero teraz się okazała błędna (wcześniej musiała być OK)
                        // teraz / wcześniej dotyczy poprzedniej walidacji jako całości procesu
                        nowFailedControls.add(oneControl);
                        failedValidatorsForOneControl = new HashSet<IWidgetValueValidator>();
                        invalidControlsMap.put(oneControl, failedValidatorsForOneControl);
                    } else {
                        //ww: ta kontrolka już wcześniej miała zaznaczony błąd, ale może inny komunikat
                        if (!nowFailedControls.contains(oneControl)) {
                            stillFailedControls.add(oneControl);
                        }
                    }
                    failedValidatorsForOneControl.add(validator);
                } else {
                    if (failedValidatorsForOneControl != null) {
                        //ww: to znaczy, że poprzedni proces walidacji wykrył błąd na tej kontrolce
                        failedValidatorsForOneControl.remove(validator);
                        if (failedValidatorsForOneControl.isEmpty()) {
                            //ww: były błędy, a teraz ich nie ma - dodajemy do listy
                            // naprawionych kontrolek
                            nowProperControls.add(oneControl);
                        }
                    }
                }
            }
        }

        nowProperControls.removeAll(nowFailedControls);

        invalidControlsMap.keySet().removeAll(nowProperControls);

        if (validationStateChangeHandler == null) {
            return;
        }

        Set<Object> allNowAffectedControls = new HashSet<Object>();
        allNowAffectedControls.addAll(nowProperControls);
        allNowAffectedControls.addAll(nowFailedControls);
        allNowAffectedControls.addAll(stillFailedControls);

        for (Object oneControl : allNowAffectedControls) {
            Widget oneControlWidget = (Widget) oneControl;
            Set<String> errMsgs = newErrorMessages.get(oneControl);
            String errMsgMerged = BaseUtils.mergeWithSepEx(errMsgs, "\n");

            validationStateChangeHandler.setErrorMessage(oneControlWidget, errMsgMerged);
//            oneControlWidget.setTitle(errMsgMerged);
            if (!stillFailedControls.contains(oneControl)) {
                boolean failed = errMsgs != null;
//                oneControlWidget.setStyleName(VALIDATION_ERROR_CSS_CLASS_NAME, failed);
                validationStateChangeHandler.stateChanged(oneControlWidget, failed);
            }
        }

        //updateActionButtonEnabled();
        validationStateChangeHandler.updateFinished(hasValidationErrors());
    }
}
