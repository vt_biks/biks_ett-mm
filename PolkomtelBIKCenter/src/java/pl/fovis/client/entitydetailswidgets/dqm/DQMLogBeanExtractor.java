/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import java.util.HashMap;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.dqm.DQMLogBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import simplelib.BaseUtils;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class DQMLogBeanExtractor extends NodeAwareBeanExtractorBase<DQMLogBean> {

    public DQMLogBeanExtractor(boolean isAddJoinedButtonEnabled) {
        super(isAddJoinedButtonEnabled);
    }

    @Override
    public boolean hasIcon() {
        return false;
    }

    @Override
    protected String getNodeKindCode(DQMLogBean b) {
        return null;
    }

    @Override
    protected String getName(DQMLogBean b) {
        return null;
    }

    @Override
    public Integer getNodeId(DQMLogBean b) {
        return null;
    }

    @Override
    public String getTreeCode(DQMLogBean b) {
        return null;
    }

    @Override
    public String getBranchIds(DQMLogBean b) {
        return null;
    }

    @Override
    public int getAddColCnt() {
        return (BIKClientSingletons.useDqmConnectionMode() ? 6 : 7);
    }

    @Override
    public Object getAddColVal(DQMLogBean b, int addColIdx) {
        Map<Integer, Object> m = new HashMap<Integer, Object>();
        HTML testName = new HTML(b.testName);
        testName.setStyleName("Notes-labBody");
        int colId = 0;
        m.put(colId++, testName);
        if (BIKClientSingletons.useDqmConnectionMode()) {
            m.put(colId++, BaseUtils.safeToStringDef(b.connectionName, ""));
        } else {
            m.put(colId++, b.sourceName != null ? b.sourceName.trim() : "");
            Label serverName = new Label(b.serverName);
            setTitleForServerName(serverName, b.serverName, b.serverInstance, b.fileName, b.databaseName);
            m.put(colId++, serverName);
            //        m.put(3, b.databaseName);
        }
        m.put(colId++, SimpleDateUtils.toCanonicalString(b.startTime));

        String dataReadSecsStr;

        if (b.dataReadSecs == null) {
            dataReadSecsStr = "";
        } else if (b.dataReadSecs < 1) {
            dataReadSecsStr = "< 1";
        } else {
            dataReadSecsStr = b.dataReadSecs + "";
        }

        m.put(colId++, GWTUtils.ensureIsWidgetWithTitle(dataReadSecsStr, I18n.lacznyCzasOdczytuDanych.get()));
//        m.put(5, b.finishTime != null ? (SimpleDateUtils.getElapsedSeconds(b.startTime, b.finishTime) + "") : I18n.testWTrakcie.get());

        int dataReadSecsV = b.dataReadSecs == null ? 0 : b.dataReadSecs;

        String elapsedSecsStr;

        if (b.finishTime == null) {
            elapsedSecsStr = I18n.testWTrakcie.get();
        } else {
            int realElapsed = (int) SimpleDateUtils.getElapsedSeconds(b.startTime, b.finishTime) - dataReadSecsV;
            if (realElapsed < 1) {
                elapsedSecsStr = "< 1";
            } else {
                elapsedSecsStr = realElapsed + "";
            }
        }

        m.put(colId++, GWTUtils.ensureIsWidgetWithTitle(elapsedSecsStr, I18n.lacznyCzasZapisuWBIKS.get()));
        m.put(colId++, b.description);
        return m.get(addColIdx);
    }

    public static void setTitleForServerName(Label widget, String serverName, String serverInstance, String fileName, String databaseName) {
        if (serverName.equals("BIKS")) {
            // NO OP
        } else if (serverName.equals("FTP")) {
            widget.setTitle(I18n.prefiksPliku.get() + ": " + fileName);
        } else {
            widget.setTitle((!BaseUtils.isStrEmptyOrWhiteSpace(serverInstance) ? I18n.instancja.get() + " / SID: " + serverInstance + " \n" : "") + I18n.bazaDanych.get() + ": " + databaseName);
        }
    }

    @Override
    public boolean createGridColumnNames(FlexTable gp) {
        gp.setStyleName("gridJoinedObj");
        int colId = 0;
        gp.setWidget(0, colId, new HTML(I18n.nazwaTestu.get()));
        gp.getColumnFormatter().setWidth(colId++, "24%");

        if (BIKClientSingletons.useDqmConnectionMode()) {
            gp.setWidget(0, colId, new HTML(I18n.polaczenie.get() + "&nbsp;&nbsp;&nbsp;&nbsp;"));
            gp.getColumnFormatter().setWidth(colId++, "14%");
        } else {
            gp.setWidget(0, colId, new HTML(I18n.konektor.get() + "&nbsp;&nbsp;&nbsp;&nbsp;"));
            gp.getColumnFormatter().setWidth(colId++, "6%");
            gp.setWidget(0, colId, new HTML(I18n.system.get() + "&nbsp;&nbsp;&nbsp;&nbsp;"));
            gp.getColumnFormatter().setWidth(colId++, "8%");
//            gp.setWidget(0, 3, new HTML(I18n.bazaDanychLubPlik.get()));
        }
        gp.setWidget(0, colId, new HTML(I18n.dataUruchomienia.get() + "&nbsp;&nbsp;&nbsp;&nbsp;"));
        gp.getColumnFormatter().setWidth(colId++, "6%");

        gp.setWidget(0, colId, new HTML(I18n.czasOdczytuDanych.get() + " (s)&nbsp;&nbsp;&nbsp;&nbsp;"));
        gp.getColumnFormatter().setWidth(colId++, "6%");
//        gp.setWidget(0, 5, new HTML(I18n.czasTrwaniaS.get() + " (s)&nbsp;&nbsp;&nbsp;&nbsp;"));
        gp.setWidget(0, colId, new HTML(I18n.zapisDanyWBiks.get() + " (s)&nbsp;&nbsp;&nbsp;&nbsp;"));
        gp.getColumnFormatter().setWidth(colId++, "35%");

        gp.setWidget(0, colId, new HTML(I18n.status.get()));
//        gp.getColumnFormatter().setWidth(2, "8%");
//        gp.getColumnFormatter().setWidth(3, "7%");
        gp.getRowFormatter().setStyleName(0, "RelatedObj-oneObj-dqc");
        return true;
    }

    @Override
    public int getActionCnt() {
        return 0;
    }
}
