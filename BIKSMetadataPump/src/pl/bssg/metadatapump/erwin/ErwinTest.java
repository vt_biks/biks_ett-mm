/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.erwin;

import pl.bssg.metadatapump.IPumpLogger;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;

/**
 *
 * @author pgajda
 */
public class ErwinTest {

    public static void main(String[] args) {
        String fileName = "c:\\temp\\DWH_DM_EWin9.5.00_v14.12_20150123.xml";
        //String fileName = "c:\\temp\\DWH_DM_EWin9.5.00_v14.12_20151014_lingaro.xml";
//        String fileName = "c:\\moje_dokumenty\\polkomtel\\Erwin\\modele\\DWH_DM_EWin7.3.11_v13.04_20130705_1_standard.xml";
//          String fileName = "c:\\moje_dokumenty\\polkomtel\\Erwin\\modele\\2014-03-25\\Model_Erwin_2014_03_25_1_min.xml";
//        Date beg_date = new Date();
        ErwinStaXParser esp = new ErwinStaXParser(fileName);
        esp.readObject();
        System.out.println("KONIEC");
//        Date beg1_date = new Date();

//        Pattern pattern = Pattern.compile("^([0-9]+)_.*");
//        Matcher matcher = pattern.matcher("0000_aaa");
//        Matcher matcher1 = pattern.matcher("000b_0aa");
//        Matcher matcher2 = pattern.matcher("a000_0aa");
//        Matcher matcher3 = pattern.matcher("a0aa");
//        System.out.println(matcher.matches());
//        System.out.println(matcher1.matches());
//        System.out.println(matcher2.matches());
//        System.out.println(matcher3.matches());
//        System.out.println("0000_aaa".replaceFirst("^([0-9]+_)", ""));
//        System.out.println("1234567890_aaa".replaceFirst("^([0-9]+_)", ""));
//        System.out.println("1_aaa".replaceFirst("^([0-9]+_)", ""));
//        System.out.println("0a000_aaa".replaceFirst("^([0-9]+_)", ""));
//        System.out.println("a_aaa".replaceFirst("^([0-9]+_)", ""));
//        Pattern pattern = Pattern.compile("^T([0-9]+)_.*");
//        Matcher matcher = null;
//        String tableNameObjId = new String("T0001_test");
//        if (tableNameObjId != null) {
//            matcher = pattern.matcher(tableNameObjId);
//            if (matcher.matches()) {
//                tableNameObjId = tableNameObjId.substring(0, tableNameObjId.indexOf('_'));
//            }
//
//        }
//        System.out.println("TEST: "+tableNameObjId);
        if(true){
            System.exit(0);
        }
        final MssqlConnectionConfig msconf = new MssqlConnectionConfig();
        msconf.database = "pl_2";
        msconf.instance = "SQLEXPRESS";
        msconf.server = "bssg-dell11";
        msconf.poolSize = 30;
        msconf.timeout = 10;
        msconf.user = "sa";
        msconf.password = "sasasa";

        IBeanConnectionEx<Object> mc = new MssqlConnection(msconf);
        mc.setAutoCommit(true);
        mc.setDbStatementExecutionWarnThresholdMillis(-1);
        INamedSqlsDAO<Object> adhocDao = createAdhocDao(mc);

        IPumpLogger pLogger = new IPumpLogger() {
            @Override
            public void pumpLog(String str, Integer proc) {
                //ThreadedPumpExecutor.this.pumpLog(str, proc);
                System.out.println("Proc: " + proc + " " + str);
            }

            @Override
            public MssqlConnectionConfig getConnectionConfig() {
                return msconf;
            }
        };

        ErwinPump ep = new ErwinPump();
        ep.setConnectionAndLog("", null, mc, adhocDao, pLogger);
        ep.startPump();

    }

    protected static INamedSqlsDAO<Object> createAdhocDao(IBeanConnectionEx<Object> mc) {
        return FoxyAppUtils.createNamedSqlsDAOFromFile(mc, "c:\\projekty\\svn\\BIKSMetadataPump\\src\\pl\\bssg\\metadatapump\\metadataAdhocSqls.html");
        //return FoxyAppUtils.createNamedSqlsDAOFromFile(mc,"c:\\temp\\DWH_DM_EWin9.5.00_v14.12_20151014_lingaro.xml");
    }
}
