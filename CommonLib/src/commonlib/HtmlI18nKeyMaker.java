/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.IRegExpMatcher;
import simplelib.IRegExpPattern;
import simplelib.Pair;
import static simplelib.HtmlUtils.*;

/**
 *
 * @author wezyr
 */
public class HtmlI18nKeyMaker {

    static {
        LameUtils.initFactories();
    }
    @SuppressWarnings("unchecked")
    private static final Pair<String, String>[] commentDelims = //{new Pair<String, String>("<!--", "-->")};
            BaseUtils.<Pair<String, String>>paramsAsArrayEx(new Pair<String, String>("<!--", "-->"));
    @SuppressWarnings("unchecked")
    private static final Pair<String, String>[] foxyDelims = //{new Pair<String, String>("<!--", "-->")};
            BaseUtils.<Pair<String, String>>paramsAsArrayEx(new Pair<String, String>("<!--", "-->"),
            new Pair<String, String>("%${", "}$%"),
            new Pair<String, String>("%{", "}%"),
            new Pair<String, String>("<", ">"),
            new Pair<String, String>("<script", "</script>"),
            new Pair<String, String>("<f:comment>", "</f:comment>"),
            new Pair<String, String>("<f:log ", "</f:log>"));
    private static IRegExpPattern dynGetText = BaseUtils.getRegExpMatcherFactory().compile(
            "getText[ \t\r\n]*\\([ \t\r\n]*['\"]([^'\"]+)['\"]", false, true);
    private static IRegExpPattern getTextPatt =
            BaseUtils.getRegExpMatcherFactory().compile(
            "getText[ \t\n\r]*\\([ \t\n\r]*['\"]([^'\"]+)['\"][ \t\n\r]*\\)", true, true);
//    private static IRegExpPattern getTextPatt =
//            BaseUtils.getRegExpMatcherFactory().compile(
//            "\\%\\{[ \t\n\r]*getText[ \t\n\r]*\\([ \t\n\r]*['\"]([^'\"]+)['\"][ \t\n\r]*\\)[ \t\n\r]*\\}\\%", true, true);
    private static List<IRegExpPattern> keepAttrValPatts = new ArrayList<IRegExpPattern>();
    private static List<IRegExpPattern> tweakAttrValPatts = new ArrayList<IRegExpPattern>();
    public Map<String, String> keys = new LinkedHashMap<String, String>();

//    private static Map<IRegExpPattern, String> fixLtGtInAttrValPatts = new HashMap<IRegExpPattern, String>();
//    private static void addSingleFixLtGtAttrValPatt(String attrQuoteChar, String ltGtChar) {
//        IRegExpPattern p =
//                BaseUtils.getRegExpMatcherFactory().compile(
//                "(<[^/>]+[ \t\r\n][a-z:_]+[ \t\r\n]*=[ \t\r\n]*[" + attrQuoteChar + "][^"
//                + attrQuoteChar + ltGtChar + "]*)" + ltGtChar, true, true);
//
//        fixLtGtInAttrValPatts.put(p, ltGtChar);
//    }
    private static void addSingleKeepAttrValPatt(String attrName, String attrQuoteChar) {
        String regExp =
                "(<[^>]+[ \t\r\n])" + attrName + "[ \t\r\n]*=[ \t\r\n]*["
                + attrQuoteChar + "]([^" + attrQuoteChar + "]*)["
                + attrQuoteChar + "]";
        IRegExpPattern p = BaseUtils.getRegExpMatcherFactory().compile(regExp, true, true);
        keepAttrValPatts.add(p);

        regExp =
                "(<[^>]+[ \t\r\n]" + attrName
                + "[ \t\r\n]*=[ \t\r\n]*[" + attrQuoteChar + "])([^" + attrQuoteChar + "]*)([" + attrQuoteChar + "])";

        p = BaseUtils.getRegExpMatcherFactory().compile(regExp, true, true);
        tweakAttrValPatts.add(p);
    }

    private static void addAttrPatt(String attrName) {
        addSingleKeepAttrValPatt(attrName, "'");
        addSingleKeepAttrValPatt(attrName, "\"");
    }

    static {
        addAttrPatt("alt");
        addAttrPatt("title");
        addAttrPatt("value");
//        addSingleFixLtGtAttrValPatt("'", "<");
//        addSingleFixLtGtAttrValPatt("'", ">");
//        addSingleFixLtGtAttrValPatt("\"", "<");
//        addSingleFixLtGtAttrValPatt("\"", ">");
    }

    public boolean isPlainTextPossibleError(String txt) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(txt)) {
            return true;
        }
        if (txt.contains("&&") || txt.contains("\">") || txt.contains("'>") || txt.contains("||")) {
            return true;
        }
        return false;
    }

    public static String makeSimpleMagicGetTexts(String res) {
        int pos = 0;
        StringBuilder sb = new StringBuilder();
        while (pos < res.length()) {
            int newPos = res.indexOf("%${", pos);
            if (newPos < 0) {
                break;
            }
            int endPos = res.indexOf("}$%", newPos);
            if (endPos < 0) {
                break;
            }
            sb.append(res.substring(pos, newPos + 3));
            Pair<String, String> p = BaseUtils.splitString(res.substring(newPos + 3, endPos), "=");
            sb.append(p.v1);
            sb.append("}$%");
            pos = endPos + 3;
        }
        sb.append(res.substring(pos));
        res = sb.toString();
        return res;
    }

    public String doubleTryFixPlainTextPossibleError(String txt) {
        String fixedTxt = tryFixPlainTextPossibleError(txt);
        if (fixedTxt.length() == 0) {
            return fixedTxt;
        }
        String ommitableSurroundingCharsPattStr =
                "[\\\\/ \t\r\n|<>{}\\(\\)\\+\\-\\?\\!\\,\\.\\:\\@\\#\\$\\%\\^\\&\\*]+";
        //"[^a-zA-Z0-9]+";

        fixedTxt = fixedTxt.replaceAll("^" + ommitableSurroundingCharsPattStr, "");
        fixedTxt = fixedTxt.replaceAll(ommitableSurroundingCharsPattStr + "$", "");

        return tryFixPlainTextPossibleError(fixedTxt);
    }

    public String tryFixPlainTextPossibleError(String txt) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(txt)) {
            return "";
        }
        if (txt.contains("&&") || txt.contains("||")) {
            return "";
        }

        String[] badCSes = {"\">", "'>"};

        for (String badCS : badCSes) {
            int pos = txt.lastIndexOf(badCS);
            if (pos >= 0) {
                txt = txt.substring(pos + badCS.length()).replaceAll("^[ \t\r\n]+", "");
            }
        }

        if (txt.contains("\"") || txt.contains("'")) {
            return "";
        }

        if (txt.length() <= 3) {
            return "";
        }

        return txt;
    }
    public Map<String, Set<String>> keyToFilesMap = new LinkedHashMap<String, Set<String>>();
    public Map<String, String> lastFileKeys;
    public Set<String> lastFileNewKeys;

    protected void extractDynKeyCandidates(String content, String name) {
        Set<String> ofdkc = new LinkedHashSet<String>();

        IRegExpMatcher m = dynGetText.matcher(content);
        while (m.find()) {
            //int startPos = m.start(1);
            //int endPos = m.end(1);
            String dkc = //content.substring(startPos, endPos);
                    m.group(1);
            addDynKeyPrefixUsageForFile(name, dkc);
            ofdkc.add(dkc);
        }

        if (!ofdkc.isEmpty()) {
            System.out.println("file=" + name + ", has dkcs=" + ofdkc);
        }
    }

    private void diagMsg(String msg) {
        //System.out.println(msg);
    }

    protected void addKeyInFileUsage(String fileName, String k) {
        Set<String> filesForKey = keyToFilesMap.get(k);
        if (filesForKey == null) {
            filesForKey = new LinkedHashSet<String>();
            keyToFilesMap.put(k, filesForKey);
        }
        filesForKey.add(fileName);
    }

    protected void addKeyValForFile(String fileName, String k, String v) {
        keys.put(k, v);
        lastFileKeys.put(k, v);
        addKeyInFileUsage(fileName, k);
    }
    public Map<String, Set<String>> dynKeyPrefixToFilesMap = new LinkedHashMap<String, Set<String>>();

    protected void addDynKeyPrefixUsageForFile(String fileName, String keyPrefix) {
        Set<String> filesForKey = dynKeyPrefixToFilesMap.get(keyPrefix);
        if (filesForKey == null) {
            filesForKey = new LinkedHashSet<String>();
            dynKeyPrefixToFilesMap.put(keyPrefix, filesForKey);
        }
        filesForKey.add(fileName);
    }

    public String processFile(String name, String content) {
        String mySep = "~:~";

        lastFileKeys = new LinkedHashMap<String, String>();
        lastFileNewKeys = new LinkedHashSet<String>();

        content = getTextPatt.matcher(content).replaceAll(
                "}%" + mySep + "%\\${$1}\\$%" + mySep + "%{");

        for (IRegExpPattern p : tweakAttrValPatts) {
            content = p.matcher(content).replaceAll("$1>" + mySep + "$2" + mySep + "<$3");
        }

        String shredded = content;

        shredded = shredded.replaceAll("&[a-z0-9A-Z_]+;", mySep);

        shredded = shredded.replace("%${", mySep + "%${");

        shredded = shredded.replace("}$%", "}$%" + mySep);

        shredded = replaceHtmlComments(shredded, mySep);

        shredded = BaseUtils.replaceEnclosedParts("<f:log ", "</f:log>", shredded, mySep);

        shredded = BaseUtils.replaceEnclosedParts("<script", "</script>", shredded, mySep);

        shredded = BaseUtils.replaceEnclosedParts("<f:comment>", "</f:comment>", shredded, mySep);

        extractDynKeyCandidates(shredded, name);

//        for (Entry<IRegExpPattern, String> e : fixLtGtInAttrValPatts.entrySet()) {
//            IRegExpPattern p = e.getKey();
//            res = p.matcher(res).replaceAll("$1" + e.getValue());
//        }

//        diagMsg("res after ltgtfix=" + res);

        for (IRegExpPattern p : keepAttrValPatts) {
            shredded = p.matcher(shredded).replaceAll("$1>$2<k");
        }

        shredded = replaceHtmlTags(shredded, mySep);

//        shredded = getTextPatt.matcher(shredded).replaceAll(mySep + "%\\${$1}\\$%" + mySep);

        shredded = BaseUtils.replaceEnclosedParts("%{", "}%", shredded, mySep);

        String oldRes;
        do {
            oldRes = shredded;
            shredded = shredded.replaceAll("[ \n\t\r]*" + mySep + "[ \n\t\r]*" + mySep + "[ \n\t\r]*", mySep);
        } while (!shredded.equals(oldRes));

        shredded = shredded.replaceAll("^" + mySep, "").replaceAll(mySep + "$", "");

        //res = BaseUtils.compactWhiteSpaces(res).trim();

        String[] txtArr = shredded.split(mySep);

        for (String txt : txtArr) {
            if (txt.startsWith("%${")) {
                diagMsg("getText=" + txt);
                //System.out.println("getText=" + txt);
                Pair<String, String> p = BaseUtils.splitString(txt.substring(3, txt.length() - 3), "=");

                String k = p.v1;
                String v = p.v2;
                String currKeyTxt = keys.get(k);

                if (BaseUtils.isStrEmpty(currKeyTxt)) {
                    v = BaseUtils.nullToDef(v, "???" + k + "???");
                    addKeyValForFile(name, k, v);
                } else {
                    addKeyInFileUsage(name, k);
                }
            } else {
                //String fixedTxt = tryFixPlainTextPossibleError(txt);
                String fixedTxt = doubleTryFixPlainTextPossibleError(txt);
                boolean emptyBeforeFix = false;
                if (fixedTxt != txt) {
                    if (BaseUtils.isStrEmpty(fixedTxt)) {
                        emptyBeforeFix = true;

                        diagMsg("plainText with possible error reduced to no-text=" + txt);
                    } else {
                        diagMsg("plainText with possible error: " + txt);
                    }
                } else {
                    //diagMsg("plainText=" + txt);
                }

//                String ommitableSurroundingCharsPattStr =
//                        "[\\\\/ \t\r\n|<>{}\\(\\)\\+\\-\\?\\!\\.\\:\\@\\#\\$\\%\\^\\&\\*]+";
//                //"[^a-zA-Z0-9]+";
//
//                fixedTxt = fixedTxt.replaceAll("^" + ommitableSurroundingCharsPattStr, "");
//                fixedTxt = fixedTxt.replaceAll(ommitableSurroundingCharsPattStr + "$", "");

                if (!BaseUtils.isStrEmpty(fixedTxt)) {
                    String key = makeKey(fixedTxt);
                    String currTxt = keys.get(key);

                    if (currTxt != null) {
                        if (currTxt.equals(fixedTxt)) {
                            diagMsg("duplicate key with the same val, key=" + key);
                        } else {
                            String newKey;

                            for (int i = 2; true; i++) {
                                newKey = key + ".o" + i;
                                currTxt = keys.get(newKey);
                                if (currTxt == null || currTxt.equals(fixedTxt)) {
                                    break;
                                }
                            }

                            diagMsg("duplicate key, different val, differentiated key=" + newKey + ", " + currTxt == null ? "is new" : "found");
                            key = newKey;
                        }
                    }

                    diagMsg("final fixed plainText=" + fixedTxt + ", key=" + key);
                    addKeyValForFile(name, key, fixedTxt);
                    lastFileNewKeys.add(key);
                } else if (!emptyBeforeFix) {
                    diagMsg("final fixed plainText is empty for txt=" + txt);
                }
            }
        }

        String res = content;

        res = makeSimpleMagicGetTexts(res);

        //extractDynKeyCandidates(res, name);

//        List<String> longestFirst = new ArrayList<String>(keys.keySet());
//
//        Collections.sort(longestFirst, new Comparator<String>() {
//
//            public int compare(String o1, String o2) {
//                return BaseUtils.strLengthFix(o2) - BaseUtils.strLengthFix(o1);
//            }
//        });

        List<Entry<String, String>> longestFirst = new ArrayList<Entry<String, String>>(keys.entrySet());

        Collections.sort(longestFirst, new Comparator<Entry<String, String>>() {

            public int compare(Entry<String, String> o1, Entry<String, String> o2) {
                String s1 = o1.getValue();
                String s2 = o2.getValue();
                String k1 = o1.getKey();
                String k2 = o2.getKey();

                int res = BaseUtils.strLengthFix(s2) - BaseUtils.strLengthFix(s1);
                if (res == 0) {
                    //System.out.println("keys with same len! k1=" + o1.getKey() + ", k2=" + o2.getKey());
                    if (k1.startsWith(magicKeyPrefix)) {
                        res = -1;
                    } else if (k2.startsWith(magicKeyPrefix)) {
                        res = 1;
                    }
                }

//                if ((k1.equals("pageTitle.editQuestion") || k1.equals("magic.pytanie"))
//                        && (k2.equals("pageTitle.editQuestion") || k2.equals("magic.pytanie"))) {
//                    System.out.println("failing key compare! k1=" + k1 + ", k2=" + k2 + ", res=" + res);
//                }
                return res;
            }
        });

        //System.out.println("longestFirst=" + longestFirst);

        List<String> notUsedKeysInFile = new ArrayList<String>();

        for (//String key : longestFirst) {
                Entry<String, String> e : //keys.entrySet()
                longestFirst) {
            String key = e.getKey();
            String txt = //keys.get(key); //
                    e.getValue();
            String getTextCmdTxt = "%${" + key + "=" + txt + "}$%";

//            if (key.equals("pageTitle.editQuestion") || key.equals("magic.pytanie")) {
//                System.out.println("detected failing key in file: " + name
//                        + ", text=\"" + txt + "\""
//                        + ", key=" + key);
//            }

            String oldTmpRes = res;
            res = replaceButNotInFoxyElems(res, txt, getTextCmdTxt);

            if (res == oldTmpRes && lastFileNewKeys.contains(key)) {
                System.out.println("------ file: " + name + ", key=\"" + key + "\", txt=" + txt);
                notUsedKeysInFile.add(key);
            }

            res = res.replace("%${" + key + "}$%", getTextCmdTxt);
        }

        if (!notUsedKeysInFile.isEmpty()) {
            System.out.println("file: " + name + " has unusedKeys=" + notUsedKeysInFile);
        }

        res = res.replace("}%" + mySep + "%${", "getText('");
        res = res.replace("}$%" + mySep + "%{", "')");

        res = res.replace(">" + mySep, "");
        res = res.replace(mySep + "<", "");

        res = res.replace("%${", "%{getText('");
        res = res.replace("}$%", "')}%");

        return res;
    }

    public static String replaceButNotInFoxyElems(String txt, String target, String replacement) {
        return replaceButNotInsideBlocks(txt, target, replacement, foxyDelims);
    }

    public static String replaceButNotInComments(String txt, String target, String replacement) {
        return replaceButNotInsideBlocks(txt, target, replacement, commentDelims);
    }

    public static String replaceButNotInsideBlocks(String txt, String target, String replacement,
            Pair<String, String>[] blocksDelims) {
//        String commStart = "<!--";
//        String commEnd = "-->";

        boolean changedAnything = false;

        StringBuilder sb = new StringBuilder();

        int pos = 0;

        mainLoop:
        while (pos < txt.length()) {
            int targetPos = txt.indexOf(target, pos);
            if (targetPos < 0) {
                break mainLoop;
            }

            String subTxt = txt.substring(pos, targetPos);

            for (Pair<String, String> singleDelims : blocksDelims) {
                String blockStart = singleDelims.v1;
                String blockEnd = singleDelims.v2;

                int blockStartPos = subTxt.lastIndexOf(blockStart);

                if (blockStartPos >= 0) {
                    int blockEndPos = subTxt.indexOf(blockEnd, blockStartPos);
                    if (blockEndPos < 0) {
                        blockEndPos = txt.indexOf(blockEnd, targetPos);

                        if (blockEndPos < 0) {
                            System.out.println("strange! for blockStart=" + blockStart
                                    + ", there is no end=" + blockEnd + " from pos=" + targetPos
                                    + ", target=" + target + ", replacement=" + replacement);
                            continue;
                        }

                        int newPos = blockEndPos + blockEnd.length();
                        sb.append(txt.substring(pos, newPos));
                        pos = newPos;
                        continue mainLoop;
                    }
                }
            }

            sb.append(txt.substring(pos, targetPos));
            sb.append(replacement);
            changedAnything = true;
            pos = targetPos + target.length();
        }

        sb.append(txt.substring(pos));

        String res = sb.toString();

        if (!changedAnything) {
            if (!res.equals(txt)) {
                System.out.println("----------- NOT CHANGED FLAG, but has changes! BAD, ERROR!");
            } else {
                res = txt;
            }
        }

        return res;
    }
    private static String magicKeyPrefix = "magic.";

    public static String makeKey(String txt) {
//        txt = LameUtils.deAccentExAndLowerCase(txt);
//        String nonAlphaNum = "[^a-zA-Z0-9]";
//        txt = txt.replaceAll(nonAlphaNum + "+", "_");
        txt = LameUtils.deAccentAndAlphaNumOnly(txt, ".");
        txt = BaseUtils.tableColNameToBeanFieldName(txt);
        return magicKeyPrefix + txt;
    }

    public static String stripHtmlTagsAndCommentsUnescapeEntities(String htmlTxt, boolean replaceTagsWithSpace) {
        return StringEscapeUtilsEx.unescapeHtml(stripHtmlTags(stripHtmlComments(htmlTxt), replaceTagsWithSpace));
    }

    public static String stripHtmlTagsAndCommentsUnescapeEntities(String htmlTxt) {
        return stripHtmlTagsAndCommentsUnescapeEntities(htmlTxt, false);
    }
    
    private List<String> sortedDynKeyPrefixes;

    public void sortDynKeyPrefixes() {
        if (sortedDynKeyPrefixes != null) {
            return;
        }

        sortedDynKeyPrefixes = new ArrayList<String>(dynKeyPrefixToFilesMap.keySet());

        Collections.sort(sortedDynKeyPrefixes, new Comparator<String>() {

            public int compare(String o1, String o2) {
                return BaseUtils.strLengthFix(o2) - BaseUtils.strLengthFix(o1);
            }
        });
    }

    public String findDynKeyPrefix(String key) {
        sortDynKeyPrefixes();

        for (String dynKeyPrefix : sortedDynKeyPrefixes) {
            if (key.startsWith(dynKeyPrefix)) {
                return dynKeyPrefix;
            }
        }

        return null;
    }
}
//
//
//-------------------------------------------------------
//
//    private List<ITag> parseFile(String name, String content) {
//        try {
//            ParserTask pt = new ParserTask(name, content);
//            final List<ITag> tags = pt.read();
//            //FoxyMillUtils.prepareTags(tags, renderEnv);
//
//            return tags;
//        } catch (Exception ex) {
//            throw new LameRuntimeException("error reading foxymill file '" + name + "'", ex);
//        }
//    }
//
//    public static void main(String[] args) {
//        String basePath = "C:/3zero/QnaApp/web/WEB-INF/FoxyMill";
//
////        String[] fileNames = new File(basePath).list(new FilenameFilter() {
////
////            public boolean accept(File dir, String name) {
////                diagMsg("dir|name=" + dir + "|" + name);
////                return false;
////            }
////        });
//
//        diagMsg("listResources=" + BaseUtils.listResourcesEx(
//                FileNamesProvider.getInstance(),
//                basePath, new IPredicate<String>() {
//
//            public Boolean project(String val) {
//                return //".html".equals(BaseUtils.extractFileExtFromFullPath(val).toLowerCase());
//                        BaseUtils.fileNameHasExt(val, ".html", true, true);
//            }
//        }, true, false));
//
////        diagMsg(
////                LameUtils.getFilePaths("C:/3zero/QnaApp/web/WEB-INF/FoxyMill" //+ "/*.html"
////                ,
////                true));
//    }

