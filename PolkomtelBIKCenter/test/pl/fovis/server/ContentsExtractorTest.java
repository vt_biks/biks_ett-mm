/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import pl.fovis.server.contentsExtractor.CompoundContentsExtractor;
import pl.fovis.server.contentsExtractor.IContentsExtractor;
import pl.fovis.server.contentsExtractor.PlainTextContentsExtractor;
import pl.fovis.server.contentsExtractor.TikaContentsExtractor;

/**
 *
 * @author pmielanczuk
 */
public class ContentsExtractorTest {

    IContentsExtractor ce;

    public ContentsExtractorTest(IContentsExtractor ce) {
        this.ce = ce;
    }

    public void parseToStringExample(String fileName) {
        System.out.println("\n\ncontents of file \"" + fileName + "\":\n"
                + ce.extractContents("c:/temp/bikcenter-upload/" + fileName) + "\n\n");
    }

    public void runMe() {
        parseToStringExample("file_2638751705764413807.doc");
        parseToStringExample("file_1745990685174817216.docx");
        parseToStringExample("file_46110668119256867.xlsx");
        parseToStringExample("file_3585560613022830945.sql");
        parseToStringExample("file_4966000171121519651.txt");
        parseToStringExample("BIKSPresentationv35-CONVERT.flv");
        parseToStringExample("file_8571208163283539751.zip");
        parseToStringExample("file_4226611549070938634.pdf");
        parseToStringExample("file_6630198607402781372.xml");
        parseToStringExample("file_6213211962781481479.csv");
        parseToStringExample("file_7463432215716230859.pptx");
        parseToStringExample("file_5150537702932174615.rar");
    }

    public static void main(String[] args) {
        PlainTextContentsExtractor e1 = new PlainTextContentsExtractor("txt|log|sql|csv");
        TikaContentsExtractor e2 = new TikaContentsExtractor("C:\\projekty\\svn\\PolkomtelBIKCenter\\libs\\tika-app-1.11.jar", "*");
        CompoundContentsExtractor ce = new CompoundContentsExtractor(e1, e2);
        new ContentsExtractorTest(ce).runMe();
    }
}
