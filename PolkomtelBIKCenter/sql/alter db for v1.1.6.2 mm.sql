﻿exec sp_check_version '1.1.6.2';
go

 alter table bik_help
 add primary key (tab_code) 
 go

exec sp_update_version '1.1.6.2', '1.1.6.3';
go