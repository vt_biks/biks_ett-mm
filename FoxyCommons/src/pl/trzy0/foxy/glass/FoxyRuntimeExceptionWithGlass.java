/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.glass;

import pl.trzy0.foxy.commons.FoxyRuntimeException;

/**
 *
 * @author wezyr
 */
public class FoxyRuntimeExceptionWithGlass extends FoxyRuntimeException {

    private Glass info;

    public FoxyRuntimeExceptionWithGlass(String errMsg, Glass info, Throwable cause) {
        super(errMsg, cause);
    }
    
    public FoxyRuntimeExceptionWithGlass(String errMsg, Glass info) {
        this(errMsg, info, null);
    }

    public static Glass exceptionToGlass(Throwable ex, String defGlassMsg) {
        if (ex instanceof FoxyRuntimeExceptionWithGlass) {
            FoxyRuntimeExceptionWithGlass frewg = (FoxyRuntimeExceptionWithGlass)ex;
            return frewg.info;
        }
        return new Glass(defGlassMsg, true);
    }
}
