/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.BaseUtils.MapProjector;
import simplelib.BaseUtils.Projector;
import simplelib.CollectionCollectorEx;
import simplelib.FieldNameConversion;
import simplelib.IContinuation;
import simplelib.IValueFormatter;
import simplelib.SimpleArrayIterator;
import simplelib.StackTraceUtil;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class BeanConnectionUtils {

    private static ILameLogger logger = LameLoggerFactory.getLogger(BeanConnectionUtils.class);

    @SuppressWarnings("unchecked")
    public static String mergeAsSqlStrings(IValueFormatter formatter, Object... items) {
        return mergeAsSqlStrings(formatter, //new GenericArrayIterator(items)
                new SimpleArrayIterator<Object>(items));
    }

//    @SuppressWarnings("unchecked")
//    public static <T> String mergeAsSqlStrings(IValueFormatter formatter, T[] items) {
//        return mergeAsSqlStrings(formatter, new GenericArrayIterator(items));
//    }
    public static <T> String mergeAsSqlStrings(IValueFormatter formatter, Iterable<T> items) {
        return mergeAsSqlStrings(formatter, items.iterator());
    }

    public static <T> String mergeAsSqlStrings(IValueFormatter formatter, Iterator<T> iter) {
        StringBuilder sb = new StringBuilder();

        boolean first = true;

        while (iter.hasNext()) {
            T item = iter.next();
            if (first) {
                first = false;
            } else {
                sb.append(",");
            }
            sb.append(formatter.valToString(item));
        }

        return sb.toString();
    }

    public static String generateInsertInto(IValueFormatter formatter,
            String tableName, Map<String, Object> vals) {
        StringBuilder sbCols = new StringBuilder();
        sbCols.append("insert into ").append(tableName).append(" (");
        StringBuilder sbVals = new StringBuilder();

        boolean first = true;

        for (Entry<String, Object> e : vals.entrySet()) {
            String col = e.getKey();
            Object val = e.getValue();
            if (val != null) {
                if (first) {
                    first = false;
                } else {
                    sbCols.append(",");
                    sbVals.append(",");
                }
                sbCols.append(col);
                sbVals.append(formatter.valToString(val));
            }
        }

        if (first) {
            return null;
        }

        sbCols.append(") values (").append(sbVals).append(")");

        return sbCols.toString();
    }

    @SuppressWarnings("unchecked")
    public static <V, T extends Collection<V>> T execNamedQuerySingleColEx(INamedSqlsDAO adhocDao,
            String qryName, boolean maxOneRow, boolean minOneRow, String optColumnName,
            T coll, Object... args) {

        Projector proj = optColumnName == null ? BaseUtils.anyMapValProjector
                : new MapProjector(optColumnName);

        CollectionCollectorEx cce = new CollectionCollectorEx(proj, coll);

        adhocDao.execNamedQueryExCFN(qryName, maxOneRow, minOneRow, cce,
                FieldNameConversion.ToLowerCase,
                args);

        return coll;
    }

    public static <V> List<V> execNamedQuerySingleColAsList(INamedSqlsDAO adhocDao,
            String qryName, String optColumnName, Object... args) {
        return execNamedQuerySingleColEx(adhocDao, qryName, false, false,
                optColumnName, new ArrayList<V>(), args);
    }

    public static <V> Set<V> execNamedQuerySingleColAsSet(INamedSqlsDAO adhocDao,
            String qryName, String optColumnName, Object... args) {
        return execNamedQuerySingleColEx(adhocDao, qryName, false, false,
                optColumnName, new LinkedHashSet<V>(), args);
    }

    @SuppressWarnings("unchecked")
    public static <V> V execNamedQuerySingleVal(INamedSqlsDAO adhocDao,
            String qryName, boolean allowNoResult, String optColumnName, Object... args) {
        Map<String, Object> row = adhocDao.execNamedQuerySingleRowCFN(qryName, allowNoResult,
                FieldNameConversion.ToLowerCase, args);

        if (row == null) {
            return null;
        }

        return (V) row.get(optColumnName == null ? row.keySet().iterator().next() : optColumnName);
    }

    public static void execInAutoCommitMode(IRawConnection connection, IContinuation whatToExec) {
        final boolean oldAutoCommit = connection.getAutoCommit();

        if (!oldAutoCommit) {
            connection.setAutoCommit(true);
        }
        try {
            whatToExec.doIt();
        } finally {
            if (!oldAutoCommit) {
                connection.setAutoCommit(false);
            }
        }
    }

    public static String optTestGrantsPerObjects(IBeanConnectionEx<Object> conn, String optObjsForGrantsTesting) {

        System.out.println("optTestGrantsPerObjects: optObjsForGrantsTesting=" + optObjsForGrantsTesting);

        if (BaseUtils.isStrEmptyOrWhiteSpace(optObjsForGrantsTesting)) {
            return "";
        }

        if (conn == null) {
            return "conn == null";
        }

        List<String> okObjs = new ArrayList<String>();
        List<String> errObjs = new ArrayList<String>();
        String objF = null;
        StringBuilder sb = new StringBuilder();

        try {
            sb.append("objs for testing: \"").append(optObjsForGrantsTesting).append("\"\n");

            String o = optObjsForGrantsTesting.trim();

            List<String> objs = BaseUtils.splitBySep(o.substring(1), o.substring(0, 1), true);

            for (String obj : objs) {
                try {
                    objF = obj;
                    conn.execOneRowQry("select * from " + obj + " where 1 = 0", true, FieldNameConversion.ToLowerCase);
                    okObjs.add(obj);
                } catch (Exception ex) {
                    errObjs.add(obj);
                    sb.append(obj).append(": ").append(StackTraceUtil.getCustomStackTrace(ex)).append("\n");
                }
            }
        } catch (Exception ex) {
            try {
                sb.append("\nfatal error while processing objF=").append(objF).append(":\n").append(StackTraceUtil.getCustomStackTrace(ex));
            } catch (Exception exx) {

                if (logger.isErrorEnabled()) {
                    logger.error("exx", exx);
                    logger.error("ex", ex);
                }

                exx.printStackTrace();
                ex.printStackTrace();
                return "\ntotal fatal error in optTestGrantsPerObjects\n";
            }
        }

        String res = "OkObjs: " + okObjs + "\n" + "ErrObjs: " + errObjs + "\n" + (sb.length() == 0 ? "No grant errors" : "Grant errors: \n" + sb.toString());
        System.out.println("optTestGrantsPerObjects: res=" + res);
        return res;
    }

}
