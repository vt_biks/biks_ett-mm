/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts.luceneUtils;

/**
 *
 * @author pmielanczuk
 */
public class WordFromAnalyzer {

    public String termText;
    public int startPos;
    public int endPos;

    public WordFromAnalyzer(String termText, int startPos, int endPos) {
        this.termText = termText;
        this.startPos = startPos;
        this.endPos = endPos;
    }
}
