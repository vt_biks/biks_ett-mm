/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.tags.htmltags;

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public interface IHtmlTag extends ISimpleHtmlTag, IRenderableTag {

    public IHtmlTag getById(String id);

    public String getId();

    public Collection<IHtmlTag> getSubTags();

    public Map<String, IHtmlTag> getTagsById();

    public boolean isTextTag();

    public String getName();

    public Map<String, String> getAttrs();

    public boolean isClosedInPlace();

    public void setSubTags(Collection<IHtmlTag> subTags);

    public boolean isVerbatimWhiteSpace();

    public boolean isInRootIdClass();

    public String getFileName();

    // sets override from attribute of tag
    // name == null stands for innerHtml
    // to clear attribute value it must be set to "" (empty string, not null)
    // setting to null will revoke to original value from tag
    public void setAttribute(String name, String val);

    // return value from override (via setAttribute) or from tag
    public String getAttribute(String name);

    public Map<String, String> getAttributes();

    public IHtmlTag getParent();
}
