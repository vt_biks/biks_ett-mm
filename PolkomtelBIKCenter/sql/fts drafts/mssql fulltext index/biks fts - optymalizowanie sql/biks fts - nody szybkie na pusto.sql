    select top 20 
	bn.id as node_id,
    bn.name,
    bnk.code as node_kind_code,
    bnk.caption as node_kind_caption,
    bt.name as tree_name,
    bt.code as tree_code,
    bn.vote_sum,
    bn.vote_cnt
    from
    (select
        *,
    0 as sum_combined_rank, 0 as max_combined_rank, 0 as sum_search_weight, 0 as max_search_weight    
    from bik_node bn
    where bn.is_deleted = 0 and (bn.linked_node_id is null or bn.disable_linked_subtree <> 0)
    and bn.tree_id in (1,2,3,4,8,12,15,14,16,6,7,13,18)
    and bn.node_kind_id in (24,14,32,42,17,31,43,4,29,48,57,49,58,21,34,16,44,25,10,62,61,40,37,13,33,2,12,54,8,6,55,9,56,53,22,3,11,60,23,41,39)
    ) as bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    inner join bik_tree bt on bn.tree_id = bt.id
    order by bn.max_search_weight desc, bn.search_rank + bnk.search_rank desc, bn.vote_sum desc, sum_combined_rank desc
 