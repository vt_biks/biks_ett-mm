/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.tags.htmltags;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import pl.trzy0.foxy.commons.tags.ITagLibrary;
import pl.trzy0.foxy.commons.tags.TagData;
import simplelib.BaseUtils;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class HtmlTagLibrary implements ITagLibrary<IHtmlTag> {

    private static ILameLogger logger = LameLoggerFactory.getLogger(HtmlTagLibrary.class);
    private Map<String, IHtmlTag> tagByIdMap = new HashMap<String, IHtmlTag>();
    private Map<String, IHtmlTag> rootTagByIdMap = new HashMap<String, IHtmlTag>();
    private Map<String, Collection<String>> fileToRootIdMap = new LinkedHashMap<String, Collection<String>>();
    private Set<String> duplicates = new HashSet<String>();

    public void addTagToIdMap(IHtmlTag tag) {
        String id = tag.getId();
        if (BaseUtils.isStrEmptyOrWhiteSpace(id)) {
            return;
        }

        if (!tag.isInRootIdClass() && hasRootIdClass(tag)) {
            IHtmlTag prevRootTagInMap = rootTagByIdMap.put(id, tag);
            if (prevRootTagInMap != null) {
                logger.errorAndThrowNew("root tag id=" + id + " is duplicated, prev fileName=" +
                        prevRootTagInMap.getFileName() + ", current fileName=" +
                        tag.getFileName());
            }

            String fileName = tag.getFileName();

            Collection<String> rootIds = fileToRootIdMap.get(fileName);

            if (rootIds == null) {
                rootIds = new ArrayList<String>();
                fileToRootIdMap.put(fileName, rootIds);
            }

            rootIds.add(id);
        }

        IHtmlTag prevTagInMap = tagByIdMap.put(id, tag);

        if (prevTagInMap != null) {
            duplicates.add(id);
        }
    }

    private static final Set<String> verbatimWhiteSpaceTagNames =
            BaseUtils.paramsAsSet("input", "textarea", "script");

    public static boolean isVerbatimWhiteSpaceTag(TagData tagData) {
        return !tagData.isTextTag() &&
                verbatimWhiteSpaceTagNames.contains(tagData.getName().toLowerCase());
    }

    public static boolean hasRootIdClass(IHtmlTag tag) {
        boolean isInRootIdClass = false;

        if (tag != null) {
            String parentClassAttr = BaseUtils.safeMapGet(tag.getAttrs(), "class");
            isInRootIdClass = BaseUtils.strHasItem(parentClassAttr, "root-id", " ");
        }

        return isInRootIdClass;
    }

    public static IHtmlTag createHtmlTag(IHtmlTag parentTag, TagData tagData) {
        boolean verbatim = (parentTag != null && parentTag.isVerbatimWhiteSpace()) ||
                isVerbatimWhiteSpaceTag(tagData);

        if (!verbatim) {
            tagData.setName(BaseUtils.compactWhiteSpaces(tagData.getName()));
        }

        boolean isInRootIdClass = hasRootIdClass(parentTag);

        IHtmlTag res = new HtmlTag(tagData, verbatim, isInRootIdClass, parentTag);

        return res;
    }

    public IHtmlTag createTag(IHtmlTag parentTag, TagData tagData) {
        IHtmlTag res = createHtmlTag(parentTag, tagData);

        addTagToIdMap(res);
        
        return res;
    }

    public void setSubTags(IHtmlTag tag, Collection<IHtmlTag> subTags) {
        tag.setSubTags(subTags);
    }

//    public boolean isStructuralTag(TagData tagData) {
//        return true;
//    }

    public IHtmlTag getTagById(String id) {
        if (duplicates.contains(id)) {
            logger.errorAndThrowNew("element with id: \"" + id + "\" is duplicated");
        }
        IHtmlTag res = tagByIdMap.get(id);
        if (res == null) {
            logger.errorAndThrowNew("no element with id: \"" + id + "\"");
        }

        return res;
    }

    public IHtmlTag getRootTagById(String id) {
        IHtmlTag res = rootTagByIdMap.get(id);
        if (res == null) {
            logger.errorAndThrowNew("no root element with id: \"" + id + "\"");
        }

        return res;
    }

    public Collection<String> getRootTagsInFile(String fileName) {
        return fileToRootIdMap.get(fileName);
    }
}
