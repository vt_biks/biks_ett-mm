/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import simplelib.BaseUtils.Projector;

/**
 *
 * @author wezyr
 */
public class CompoundGenericResourceProvider<T1, T2> implements IGenericResourceProvider<T2> {

    protected IGenericResourceProvider<T1> provider;
    protected Projector<T1, T2> proj;

    public CompoundGenericResourceProvider(IGenericResourceProvider<T1> provider,
            Projector<T1, T2> proj) {
        this.provider = provider;
        this.proj = proj;
    }

    public T2 gainResource(String resourceName) {
        return proj.project(provider.gainResource(resourceName));
    }

    public long getLastModifiedOfResource(String resourceName) {
        return provider.getLastModifiedOfResource(resourceName);
    }
}
