/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.designersdk;

import commonlib.LameUtils;
import commonlib.ProcessUtils;
import commonlib.StreamGobbler;
import java.io.File;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.sapbo.javasdk.SapBOBean;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class DesignerPump extends MetadataPumpBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected final static String PROCESS_NAME_DESIGNER_PUMP = "BOXIUniversDataPump.exe";
    protected final static String PROCESS_NAME_DESIGNER_PUMP_BO4 = "BOXIUniversDataPump4BO4.exe";
    protected final static String PROCESS_NAME_DESIGNER = "designer.exe";

    protected IBeanConnectionEx<Object> mc;

    @Override
    public String getPumpGroup() {
        return PumpConstants.PUMP_GROUP_BO;
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_SAPBO;
    }

    @Override
    protected Integer insertMainLog() {
        return getLastStartLog(getSourceName());
    }

    @Override
    protected Pair<Integer, String> startPump() {
        execNamedCommandWithCommit("createBOInstanceFunctions", getSourceName(), instanceName);

        SapBOBean configBean = getAdhocDao().createBeanFromNamedQry("getBOConnectionParamatersForSource", SapBOBean.class, true, getSourceName(), instanceName);
        String path = execNamedQuerySingleVal("getConfigValue", true, "value", getAdminStringPath());
        File file = new File(path);
        if (!file.exists()) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Wrong path or file (designer pump) does not exists!");
        }

        MssqlConnectionConfig connectionConfig = pumpLogger.getConnectionConfig();

        execNamedCommandWithCommit("prepareMultiInstanceTablesForDesignerSDK");
        // create candidate list (in aaa_universe_delta)
        execNamedCommandWithCommit("createCandidateList", false);
        // remove old / bad records in temp tables
        execNamedCommandWithCommit("deleteOldOrBadRecords");
        // run exe file / pump
        try {
            checkIfProcessesWorksAndKillIfNeed();
            String[] callAndArgs = {"cmd.exe", "/C", path, configBean.server, configBean.userName, configBean.password, connectionConfig.server, (connectionConfig.instance != null) ? connectionConfig.instance : "", connectionConfig.database, connectionConfig.user, connectionConfig.password};
            Process p = ProcessUtils.executeProcess(callAndArgs);
            StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), "ERROR");
            StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), "OUTPUT");
            errorGobbler.start();
            outputGobbler.start();
            //
            p.waitFor();
            errorGobbler.join();
            String errorMsg = errorGobbler.getErrorMsg();

            if (errorMsg != null) {
                return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, errorMsg);
            }
            execNamedCommandWithCommit("finishMultiInstanceTablesForDesignerSDK");

            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, "Designer SDK: Success");
        } catch (InterruptedException ie) {
            if (logger.isErrorEnabled()) {
                logger.error("InterruptedException Error: " + ie.getMessage(), ie);
            }
            throw new LameRuntimeException(ie.getMessage(), ie);
        }
    }

    protected String getAdminStringPath() {
        return "sapbo.path";
    }

    protected void checkIfProcessesWorksAndKillIfNeed() {
        if (ProcessUtils.isProcessRunning(PROCESS_NAME_DESIGNER_PUMP)) {
            pumpLogger.pumpLog("Process " + PROCESS_NAME_DESIGNER_PUMP + " is running!!! Killing...", null);
            ProcessUtils.killProcessByName(PROCESS_NAME_DESIGNER_PUMP);
        }
        if (ProcessUtils.isProcessRunning(PROCESS_NAME_DESIGNER_PUMP_BO4)) {
            pumpLogger.pumpLog("Process " + PROCESS_NAME_DESIGNER_PUMP_BO4 + " is running!!! Killing...", null);
            ProcessUtils.killProcessByName(PROCESS_NAME_DESIGNER_PUMP_BO4);
        }
        if (ProcessUtils.isProcessRunning(PROCESS_NAME_DESIGNER)) {
            pumpLogger.pumpLog("Process " + PROCESS_NAME_DESIGNER + " is running!!! Killing...", null);
            ProcessUtils.killProcessByName(PROCESS_NAME_DESIGNER);
        }
    }
}
