﻿exec sp_check_version '1.1.2.2';
go

alter table bik_attr_category
add is_deleted int not null default 0
go

alter table bik_attr_category
add old_name varchar(255) null
go

exec sp_update_version '1.1.2.2', '1.1.2.3';
go