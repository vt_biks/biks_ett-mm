/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import commonlib.LameUtils;
import commonlib.datapump.GenericDataPump;
import commonlib.datapump.ListOfListsDataPumpReaderSource;
import commonlib.ftp.FTP4JFoxyFTPClient;
import commonlib.ftp.IFoxyFTPClient;
import commonlib.ftp.IFoxyFTPFile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.profile.ProfilePump;
import static pl.bssg.metadatapump.profile.ProfilePump.deleteLocalTempFiles;
import pl.fovis.common.ConnectionParametersProfileBean;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.fovis.common.dqm.DataQualityRequestBean;
import pl.fovis.common.i18npoc.I18n;
import pl.trzy0.foxy.commons.templates.INamedTemplateProvider;
import simplelib.BaseUtils;
import simplelib.CSVReader;
import simplelib.IContinuation;
import simplelib.LameRuntimeException;
import simplelib.SimpleDateUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class ProfileTestExecutor extends AbstractTestExecutorBase {

//    public static final String LOCAL_FOLDER = BaseUtils.ensureStrHasSuffix(System.getProperty("java.io.tmpdir"), "\\", true) + "DQM";
    private static final ILameLogger logger = LameUtils.getMyLogger();
    public static final String ERROR_FILE_SUFFIX = "_error.csv";
    public static final String FILE_SUFFIX = ".csv";
    public static final String ERROR_COUNT = "error_count";
    public static final String CASE_COUNT = "case_count";
    public static final char FILE_SEPARATOR = ';';
    protected IFoxyFTPClient client;
    protected String actualFileToDownload;
    protected String dirForDownload;
    protected ConnectionParametersProfileBean config;

    public ProfileTestExecutor(IBIKSDQMSubservice service, String dirForUpload) {
        super(service);
        this.dirForDownload = BaseUtils.ensureStrHasSuffix(dirForUpload.replace("/", "\\"), "\\", true) + "DQM";
    }

    @Override
    public List<DQMTestBean> preInitAndGetRequestToRun(DQMTestBean test, List<DQMMultiSourceBean> optMultiSources) {
        List<DQMTestBean> requests = new ArrayList<DQMTestBean>();
        config = service.getProfileConnectionParameters();
        List<DataQualityRequestBean> requestsStartDateForTest = service.getRequestsStartDateForTest(test.id);
        Set<String> oldRequests = createRequestStartSet(requestsStartDateForTest);
        client = new FTP4JFoxyFTPClient();//SFTP4JFoxyFTPClient();
        try {
            client.connectAndLogin(config.host, BaseUtils.tryParseInteger(config.port, 21/*22*/), config.user, config.password);
            client.changeDirectory(config.testPath);
            List<IFoxyFTPFile> files = client.getFiles();
            for (IFoxyFTPFile file : files) {
                try {
                    String orygFileName = file.getName();
                    String orygFileNameAfterLowerCase = orygFileName.toLowerCase();
                    String resultFileAfterLowerCase = test.resultFileName.toLowerCase();
                    if (!file.isDirectory() && orygFileNameAfterLowerCase.startsWith(resultFileAfterLowerCase) && !orygFileNameAfterLowerCase.endsWith(ERROR_FILE_SUFFIX)) {
                        String dateFileName = getDateStringFromFile(test.resultFileName, orygFileName);
                        if (!oldRequests.contains(dateFileName)) {
                            DQMTestBean newTest = test.clone();
                            newTest.resultFileName = orygFileName;
                            Date startTime;
                            try {
                                startTime = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).parse(dateFileName);
                            } catch (ParseException ex) {
                                startTime = null;
                            }
                            newTest.optStartedDate = startTime;
                            requests.add(newTest);
                        }
                    }
                } catch (Exception e) {
                    continue;
                }
            }
        } catch (Exception e) {
            return null;
        }
        if (requests.isEmpty() && oldRequests.isEmpty()) { //
            throw new LameRuntimeException("Brak pliku (plików) do pobrania");
        }
        return requests;
    }

    @Override
    public boolean initConnection(DQMTestBean test) {
        this.actualFileToDownload = test.resultFileName;
//        this.source = source;

        // no op
        // inicjalizacja połączenia następuje w metodzie preInitAndGetRequestToRun
        return true;
    }

    @Override
    public void closeConnection() {
        // NO OP
        // połaczenie zamykamy w metodzie exit()
    }

    @Override
    public List<Map<String, Object>> runTest() {
//        System.out.println("Pobieram plik: " + actualFileToDownload);
        String remotePathToFile = BaseUtils.ensureDirSepPostfix(config.testPath) + actualFileToDownload;
        String localPathToFile = dirForDownload + "\\" + actualFileToDownload;
        return getDataInner(remotePathToFile, localPathToFile);
    }

    @Override
    public List<Map<String, Object>> getErrorData() {
        String fileWithErrors = new String(actualFileToDownload.substring(0, actualFileToDownload.lastIndexOf(FILE_SUFFIX))) + ERROR_FILE_SUFFIX;
//        System.out.println("Sciagamy bledy dla: " + fileWithErrors);
        String remotePathToFile = BaseUtils.ensureDirSepPostfix(config.testPath) + fileWithErrors;
        String localPathToFile = dirForDownload + "\\" + fileWithErrors;
        return getDataInner(remotePathToFile, localPathToFile);
    }

    @Override
    public void exit() {
        try {
            deleteLocalTempFiles(dirForDownload);
            client.disconnect();
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in exitConnection", e);
            }
        }
    }

    protected Set<String> createRequestStartSet(List<DataQualityRequestBean> requestsStartDateForTest) {
        Set<String> oldRequests = new HashSet<String>();
        for (DataQualityRequestBean req : requestsStartDateForTest) {
            Date startDate = req.startTimestamp;
            StringBuilder sb = new StringBuilder();
            int year = SimpleDateUtils.getYear(startDate);
            int month = SimpleDateUtils.getMonth(startDate);
            int day = SimpleDateUtils.getDay(startDate);
            int hours = SimpleDateUtils.getHours(startDate);
            int minutes = SimpleDateUtils.getMinutes(startDate);
            int seconds = SimpleDateUtils.getSeconds(startDate);
            sb.append(year).append(month < 10 ? "0" + month : month).append(day < 10 ? "0" + day : day).append(hours < 10 ? "0" + hours : hours).append(minutes < 10 ? "0" + minutes : minutes).append(seconds < 10 ? "0" + seconds : seconds);
            oldRequests.add(sb.toString());
        }
        return oldRequests;
    }

    protected String getDateStringFromFile(String resultFileName, String orygFileName) {
        int prefixLength = resultFileName.length();
        int postfix = orygFileName.indexOf(FILE_SUFFIX);
        return new String(orygFileName.substring(prefixLength + 1, postfix));
    }

    @Override
    public boolean initConnection(DQMMultiSourceBean source) {
        this.source = source;
        config = service.getProfileConnectionParameters();
        client = new FTP4JFoxyFTPClient();//SFTP4JFoxyFTPClient();
        try {
            client.connectAndLogin(config.host, BaseUtils.tryParseInteger(config.port, 21/*22*/), config.user, config.password);
            client.changeDirectory(config.testPath);
            List<IFoxyFTPFile> files = client.getFiles();
            Date maxDate = null;
            for (IFoxyFTPFile file : files) {
                try {
                    String orygFileName = file.getName();
                    String orygFileNameAfterLowerCase = orygFileName.toLowerCase();
                    String resultFileAfterLowerCase = source.resultFileName.toLowerCase();
                    if (!file.isDirectory() && !orygFileNameAfterLowerCase.endsWith(ERROR_FILE_SUFFIX) && orygFileNameAfterLowerCase.startsWith(resultFileAfterLowerCase)) {
                        String dateFileName = getDateStringFromFile(source.resultFileName, orygFileName);
                        Date fileDate;
                        fileDate = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).parse(dateFileName);
                        if (maxDate == null || fileDate.after(maxDate)) {
                            maxDate = fileDate;
                            actualFileToDownload = orygFileName;

                        }
                    }
                } catch (Exception e) {
                    continue;
                }
            }
        } catch (Exception e) {
            return false;
        }
        if (actualFileToDownload == null) {
            throw new LameRuntimeException("Brak pliku (plików) do pobrania");
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    protected List<Map<String, Object>> getDataInner(String remotePathToFile, String localPathToFile) {
        List<Map<String, Object>> mapToReturn = null;
        try {
            ProfilePump.createFolderIfNotExists(dirForDownload);
//        System.out.println("Error remotePathToFile = " + remotePathToFile + ", localPathToFile = " + localPathToFile);
            client.download(remotePathToFile, localPathToFile);
            String csvContent = LameUtils.loadAsString(localPathToFile, "ISO-8859-2");
            CSVReader reader = new CSVReader(csvContent, CSVReader.CSVParseMode.CONSIDER_QUOTES, FILE_SEPARATOR, true);
            mapToReturn = (List) reader.readFullTableAsMaps();
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in getData (download and read from csv)", e);
            }
            throw new LameRuntimeException(I18n.plik.get() + ": " + remotePathToFile + " " + I18n.blad.get() + ": " + e.getMessage());
        }
        return mapToReturn;
    }

    @Override
    protected int performPumpingData(INamedTemplateProvider typeMappingTemplates, String optTmpTableName, String optTestGrantsPerObjects, boolean isMultiSource) {
//        String remotePathToFile = BaseUtils.ensureDirSepPostfix(config.testPath) + actualFileToDownload;
       String remotePathToFile = BaseUtils.ensureDirSepPostfix(config.testPath)
                + (isMultiSource ? actualFileToDownload : new String(actualFileToDownload.substring(0, actualFileToDownload.lastIndexOf(FILE_SUFFIX))) + ERROR_FILE_SUFFIX);
       
        String localPathToFile = dirForDownload + "\\" + actualFileToDownload;

        ProfilePump.createFolderIfNotExists(dirForDownload);
//        System.out.println("Error remotePathToFile = " + remotePathToFile + ", localPathToFile = " + localPathToFile);
        client.download(remotePathToFile, localPathToFile);

        String csvContent = LameUtils.loadAsString(localPathToFile, "ISO-8859-2");

        CSVReader reader = new CSVReader(csvContent, CSVReader.CSVParseMode.CONSIDER_QUOTES, FILE_SEPARATOR, true);

        List<String> colNames = reader.readRow();
        List<List<String>> rows = reader.readRowsAsLists();

        return new GenericDataPump(new ListOfListsDataPumpReaderSource<String>(colNames, rows),
                BaseUtils.isStrEmptyOrWhiteSpace(optTmpTableName) ? createDataPumpDestination(typeMappingTemplates)
                        : createDataPumpDestination(typeMappingTemplates, optTmpTableName)).pumpIt();
    }

    @Override
    protected String getTypeMappingStr() {
        return "VARCHAR=varchar(max)";
    }

    @Override
    public int pumpErrorDataToBiksDatabase(final String tmpTableName) {
        final INamedTemplateProvider typeMappingTemplates = prepareTypeMappingTemplates();

        service.execInAutoCommitMode(new IContinuation() {

            @Override
            public void doIt() {
                dataReadSecs = performPumpingData(typeMappingTemplates, tmpTableName, null, false);

            }
        });
        return dataReadSecs;
    }
}
