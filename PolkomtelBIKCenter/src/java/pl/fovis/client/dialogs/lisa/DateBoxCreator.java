/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.Date;
import pl.fovis.client.entitydetailswidgets.lisa.WidgetWithValue;
import pl.fovis.common.lisa.DateLDV;
import pl.fovis.common.lisa.LisaDictionaryValue;
import simplelib.IContinuation;

/**
 *
 * @author tbzowka
 */
public class DateBoxCreator implements WidgetCreator {

    public WidgetWithValue createWidget() {
        return new WidgetWithValue() {
            DateBox db = new DateBox();

            public Widget getWidget() {
                db.setStyleName("dateBoxPopup");
//                db.setWidth("100%");
                return db;
            }

            public void setValue(Object value) {
                if (value instanceof Date) {
                    db.setValue((Date) value);
                }
            }

            public LisaDictionaryValue getValue() {
                return new DateLDV(db.getValue());
            }

            public void setEditable(boolean b) {
                if (b) {
                    db.setStyleName("dateBoxPopup");
                } else {
                    db.setStyleName("dateBoxPopup-readonly");
                }
                db.setEnabled(b);
            }

            public void addChangeHandler(final IContinuation con) {
                db.addValueChangeHandler(new ValueChangeHandler<Date>() {
                    public void onValueChange(ValueChangeEvent<Date> event) {
                        con.doIt();
                    }
                });
            }

            public void setStyleName(String styleName) {
                db.setStyleName(styleName);
            }
        };
    }
;
}
