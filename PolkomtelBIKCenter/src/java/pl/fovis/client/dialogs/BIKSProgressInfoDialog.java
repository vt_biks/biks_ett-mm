/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import pl.fovis.foxygwtcommons.dialogs.SimpleProgressInfoDialog;

/**
 *
 * @author tflorczak
 */
public class BIKSProgressInfoDialog extends SimpleProgressInfoDialog {

    protected String loadingGifPath;
    protected boolean isShowing = false;

    @Override
    protected void buildMainWidgets() {
        main.clear();
        HorizontalPanel hp = new HorizontalPanel();
        hp.add(isPlainText ? new Label(htmlOrTxt) : new HTML(htmlOrTxt));
        if (loadingGifPath != null) {
            Image iconImg = new Image(loadingGifPath);
            iconImg.setSize("16px", "16px");
            hp.add(iconImg);
            hp.setSpacing(10);
        }
        main.add(hp);
    }

    public void buildAndShowDialog(String optCaption, String htmlOrTxt, boolean isPlainText, String loadingGifPath) {
        this.loadingGifPath = loadingGifPath;
        if (isShowing) {
            this.isPlainText = isPlainText;
            this.htmlOrTxt = htmlOrTxt;
            mdc.setCaptionHtml(optCaption);
            buildMainWidgets();
        } else {
            super.buildAndShowDialog(optCaption, htmlOrTxt, isPlainText);
            this.isShowing = true;
        }
    }

    @Override
    public void buildAndShowDialog(String optCaption, String htmlOrTxt, boolean isPlainText) {
        buildAndShowDialog(optCaption, htmlOrTxt, isPlainText, "images/loader.gif");
    }

    @Override
    public void hideDialog() {
        isShowing = false;
        super.hideDialog(); //To change body of generated methods, choose Tools | Templates.
    }
}
