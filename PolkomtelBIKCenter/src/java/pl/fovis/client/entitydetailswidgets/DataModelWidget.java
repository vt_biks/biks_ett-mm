/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.datamodel.DataModel;
import pl.fovis.client.datamodel.DataProcessRef;
import pl.fovis.client.datamodel.DataSummary;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author pgajda
 */
public class DataModelWidget extends EntityDetailsWidgetFlatBase implements IEntityDetailsWidgetWithContlolWiget {

    protected DataModel dataModel;
    protected DataProcessRef dataProcessRef;
    protected DataSummary dataSummary;
    protected Widget summaryWidget;
//    protected IMainHeaderSharedTable imhst;
    protected VerticalPanel vpanel;
    protected Widget dataModelWidget;
    protected Widget procesWidget;
    protected List<PushButton> pushButtonsList;

    public DataModelWidget(/*IMainHeaderSharedTable imhst*/) {
        dataModel = new DataModel(false) {
            @Override
            protected void setScrollPosition(int left, int top) {
                setScrollPositionInDetailWidget(left, top);
            }
        };
        dataProcessRef = new DataProcessRef();
        dataSummary = new DataSummary();
//        this.imhst = imhst;
    }

    @Override
    public void displayData() {
        if (!(BaseUtils.safeEquals(getNodeTreeCode(), BIKConstants.TREE_CODE_TERADATADATAMODEL)
                || BaseUtils.safeEquals(getNodeKindCode(), BIKConstants.NODE_KIND_TERADATA_TABLE)
                || BaseUtils.safeEquals(getNodeKindCode(), BIKConstants.NODE_KIND_TERADATA_COLUMN)
                || BaseUtils.safeEquals(getNodeKindCode(), BIKConstants.NODE_KIND_TERADATA_COLUMN_PK))) {
            return;
        }

        vpanel.clear();
//        if (hasSummaryWidget()) {
//            vpanel.add(summaryWidget);
//        } else {
        vpanel.add(dataModelWidget);
//        }
    }

    protected void setScrollPositionInDetailWidget(int left, int top) {

    }

    @Override
    public Widget buildWidgets() {
        vpanel = new VerticalPanel();
        summaryWidget = dataSummary.buildWidgets();
        dataModelWidget = dataModel.buildWidgets();
        procesWidget = dataProcessRef.buildWidgets();
//        Window.alert("hasSummaryWidget = " + hasSummaryWidget());
//        if (hasSummaryWidget()) {
//            vpanel.add(summaryWidget);
//        } else {
//            vpanel.add(dataModelWidget);
//        }
        return vpanel;
    }

    @Override
    public boolean hasContent() {
        String nodeKindCode = getNodeKindCode();
        return getData() != null
                && BaseUtils.safeEquals(getNodeTreeCode(), BIKConstants.TREE_CODE_TERADATADATAMODEL)
                && (BaseUtils.safeEquals(nodeKindCode, BIKConstants.NODE_KIND_TERADATA_TABLE)
                || BaseUtils.safeEquals(nodeKindCode, BIKConstants.NODE_KIND_TERADATA_COLUMN)
                || BaseUtils.safeEquals(nodeKindCode, BIKConstants.NODE_KIND_TERADATA_COLUMN_PK)
                || BaseUtils.safeEquals(nodeKindCode, BIKConstants.NODE_KIND_TERADATA_AREA)
                || BaseUtils.safeEquals(nodeKindCode, BIKConstants.NODE_KIND_TERADATA_SERVER));
    }

    public boolean hasDataLineageContent() {
        String nodeKindCode = getNodeKindCode();
        return getData() != null
                && BaseUtils.safeEquals(getNodeTreeCode(), BIKConstants.TREE_CODE_TERADATADATAMODEL)
                && (BaseUtils.safeEquals(nodeKindCode, BIKConstants.NODE_KIND_TERADATA_TABLE)
                || BaseUtils.safeEquals(nodeKindCode, BIKConstants.NODE_KIND_TERADATA_AREA));
    }

    protected boolean hasSummaryWidget() {
//        return false;
        return getData() != null
                && BaseUtils.safeEquals(getNodeTreeCode(), BIKConstants.TREE_CODE_TERADATADATAMODEL)
                && BaseUtils.safeEquals(getNodeKindCode(), BIKConstants.NODE_KIND_TERADATA_AREA);
    }

    @Override
    public Widget buildContlolWidgets() {
        EntityDetailsDataBean eddb = getData();

        dataModel.setEntityDetailsDataBean(eddb);
        dataProcessRef.setEntityDetailsDataBean(eddb);
        dataSummary.setEntityDetailsDataBean(eddb);

        VerticalPanel controlPanel = new VerticalPanel();
        controlPanel.setWidth("100%");
        if (BaseUtils.safeEquals(getNodeKindCode(), BIKConstants.NODE_KIND_TERADATA_SERVER)) {
            vpanel.clear();
            return controlPanel;
        }

        final Widget diagramControlPan = dataModel.buildContlolWidgets();
        final SimplePanel controlPan = new SimplePanel();
        controlPan.add(diagramControlPan);

        pushButtonsList = new ArrayList<PushButton>();
        FlowPanel floPan = new FlowPanel();
        final PushButton summaryButton = new PushButton(I18n.status.get());

        final PushButton diagramButton = new PushButton(I18n.diagram.get());
        pushButtonsList.add(diagramButton);
        final PushButton procButton = new PushButton("Data lineage");
        pushButtonsList.add(procButton);
//        setStyleAsActive(diagramButton);
//        NewLookUtils.makeCustomPushButton(diagramButton);
        diagramButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                vpanel.clear();
                setStyleAsActive(diagramButton);
                controlPan.clear();
                controlPan.setWidget(diagramControlPan);

//                if (dataModelWidget == null) {
//                    dataModelWidget = dataModel.buildWidgets();
//                }
                dataModel.displayData();
                vpanel.add(dataModelWidget);
            }
        });

//        GWTUtils.setStyleAttribute(diagramButton, "margin", "2px");
        floPan.add(diagramButton);

        if (hasDataLineageContent()) {

            final Widget processControlPan = dataProcessRef.buildContlolWidgets();
//            NewLookUtils.makeCustomPushButton(procButton);
            procButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    vpanel.clear();
                    setStyleAsActive(procButton);
                    controlPan.clear();
                    controlPan.setWidget(processControlPan);

//                    if (procesWidget == null) {
//                        procesWidget = dataProcessRef.buildWidgets();
//                    }
                    dataProcessRef.displayData();
                    vpanel.add(procesWidget);
                }
            });

//            GWTUtils.setStyleAttribute(procButton, "margin", "2px");
            floPan.add(procButton);
        }
        if (hasSummaryWidget()) {
            pushButtonsList.add(summaryButton);
            final Widget processControlPan = dataSummary.buildContlolWidgets();
            summaryButton.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    vpanel.clear();
                    setStyleAsActive(summaryButton);
                    controlPan.clear();
                    controlPan.setWidget(processControlPan);
//                    if (summaryWidget == null) {
//                        summaryWidget = new Label("dupcia");
//                    }
                    dataSummary.displayData();
                    vpanel.add(summaryWidget);
                }
            });
            floPan.add(summaryButton);
            controlPan.clear();
        }
        floPan.setStyleName("borderBotton");

        controlPanel.add(floPan);
        controlPanel.add(controlPan);
        setFirstTabActive();
        return controlPanel;
    }

    protected void setFirstTabActive() {
        boolean isFirst = true;
        for (PushButton btn : pushButtonsList) {
            if (isFirst) {
                setStyleAsActive(btn);
                isFirst = false;
            } else {
                setStyleAsUnActive(btn);
            }
        }
        dataModel.displayData();
    }

    protected void setStyleAsActive(PushButton btn) {
        for (PushButton btnInList : pushButtonsList) {
            if (BaseUtils.safeEquals(btn, btnInList)) {
                btn.setStyleName("buttonAsTabActive");
            } else {
                setStyleAsUnActive(btnInList);
            }
        }
    }

    protected void setStyleAsUnActive(PushButton btn) {
        btn.setStyleName("buttonAsTabUnActive");
    }
}
