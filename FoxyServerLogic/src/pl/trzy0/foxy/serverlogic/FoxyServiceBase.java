/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import simplelib.MapHelper;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;
import simplelib.logging.LameLoggerMsg;

/**
 *
 * @author wezyr
 */
public abstract class FoxyServiceBase<T extends IFoxyBizLogic, SCO/* extends ServletConfigObj*/, SD>
        implements IFoxyService<T, SCO/*, SD*/> {

    public static final ILameLogger clientMsgsLogger =
            LameLoggerFactory.getLogger(FoxyServiceBase.class);
    protected T bizLogic;
    protected ISessionEnvironment sessionEnv;
    public SD sessionData;
    protected SCO configObj;
    protected Map<String, Object> customSessionData;

    protected abstract SD createSessionData();

    public void setBizLogic(T bizLogic) {
        this.bizLogic = bizLogic;
    }

    public void init(SCO configObj) {
        this.configObj = configObj;
        this.sessionData = createSessionData();
    }

    public void attachSessionEnvironment(ISessionEnvironment sessionEnv) {
        this.sessionEnv = sessionEnv;
    }

    public void unboundedFromSession() {
        // just a stub
    }

    public void getLogClientMsgsData(MapHelper<String> mh) {
        if (clientMsgsLogger.isDebugEnabled()) {
            List<LameLoggerMsg> msgs = LameLoggerMsg.createLameLoggerMsgList(mh.getString("msgs"));
            for (LameLoggerMsg msg : msgs) {
                clientMsgsLogger.debug(msg.getFormattedMsg());
            }
        }
    }

    public SD getSessionData() {
        return sessionData;
    }

    public Map<String, Object> getCustomSessionData() {
        if (customSessionData == null) {
            customSessionData = new HashMap<String, Object>();
        }

        return customSessionData;
    }

    public boolean isSystemAdminLogged() {
        return false;
    }
}
