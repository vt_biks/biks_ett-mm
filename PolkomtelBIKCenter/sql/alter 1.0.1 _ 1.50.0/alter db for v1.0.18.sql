exec sp_check_version '1.0.18';
go
----------------------------------------------------------------------
----------------------------------------------------------------------
alter table bik_joined_objs drop constraint uk_bik_joined_objs_src_dst
go

insert into bik_joined_objs (src_node_id, dst_node_id)
select dst_node_id, src_node_id
from
bik_joined_objs
go

delete
from bik_joined_objs
where id <> (select MIN(id) from bik_joined_objs dd where dd.src_node_id = bik_joined_objs.src_node_id
  and dd.dst_node_id = bik_joined_objs.dst_node_id)
go

alter table bik_joined_objs add constraint uk_bik_joined_objs_src_dst unique (src_node_id, dst_node_id)
go

delete
from bik_joined_objs where src_node_id = dst_node_id
go
----------------------------------------------------------------------
----------------------------------------------------------------------
exec sp_update_version '1.0.18', '1.0.19';
go

