/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.db.setdbperformance;

import commonlib.GlobalThreadStopWatch;

/**
 *
 * @author pmielanczuk
 */
public class SetDbStep extends Step {

    public static boolean fakeMode = false;
    String dbName;

    public SetDbStep(String dbName) {
        this.dbName = dbName;
    }

    public void run() {
        GlobalThreadStopWatch.startProcess("step setDb[" + dbName + "] run", true);
        try {
            if (!fakeMode || dbName.equals("biks_en_8X7CSJ5B")) {
//            System.out.println("setDb to " + dbName);
                DaoSingleton.setDatabase(dbName);
            }
        } finally {
            GlobalThreadStopWatch.endProcess();
        }
    }
}
