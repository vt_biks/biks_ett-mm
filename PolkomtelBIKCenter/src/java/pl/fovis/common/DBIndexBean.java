/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class DBIndexBean implements Serializable {

    public String name;
    public String type;
    public boolean isUnique;
    public String columnName;
    public int columnPosition;
    public String descend;
    public boolean isPrimaryKey;
    public int partitionOrdinal;
    public String partitionColumnName;
    // technical
    public String icon;
    public Integer nodeId;
    public String treeCode;

    public DBIndexBean() {
    }

    public DBIndexBean(String name, String type, String columnName, String partitionColumnName, String icon) {
        this.name = name;
        this.type = type;
        this.columnName = columnName;
        this.partitionColumnName = partitionColumnName;
        this.icon = icon;
    }
}
