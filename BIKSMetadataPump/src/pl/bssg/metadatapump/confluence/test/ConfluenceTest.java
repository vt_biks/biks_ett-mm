/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence.test;

import commonlib.LameUtils;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.swift.common.soap.confluence.AbstractRemotePageSummary;
import org.swift.common.soap.confluence.ConfluenceSoapService;
import org.swift.common.soap.confluence.ConfluenceSoapServiceServiceLocator;
import org.swift.common.soap.confluence.RemoteBlogEntrySummary;
import org.swift.common.soap.confluence.RemoteComment;
import org.swift.common.soap.confluence.RemotePageSummary;
import org.swift.common.soap.confluence.RemoteServerInfo;
import org.swift.common.soap.confluence.RemoteSpaceSummary;
import pl.bssg.metadatapump.confluence.wuwu.ConfluenceContentFixer;
import pl.bssg.metadatapump.confluence.wuwu.ConfluenceObjectContentRetrieverViaHttp;
import pl.bssg.metadatapump.confluence.wuwu.IConfluenceObjectImportedChecker;
import simplelib.BaseUtils;
import simplelib.StackTraceUtil;

/**
 *
 * @author tflorczak
 */
public class ConfluenceTest {

//    private static ConfluenceSoapServiceServiceLocator fConfluenceSoapServiceGetter = new ConfluenceSoapServiceServiceLocator();
    private static ConfluenceSoapService fConfluenceSoapService = null;
    private static String fToken = null;

    public static String getToken() {
        return fToken;
    }

    public static ConfluenceSoapService getConfluenceSOAPService() {
        return fConfluenceSoapService;
    }
    public static HashMap<String, String> renderOptions = new HashMap<String, String>() {
        {
            put("style", "clean");
        }
    };

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
//            String server = "http://192.168.167.57:8090"; // winbor3

//            lokalne BSSG
            String server = "http://192.168.167.54:8090"; // 4Monia
            String user = "admin";
            String pass = "1qaz2wsx#EDC";
            String spaceKeyToFetch = "SPEJ";
//            // odkomentować dla cyfrowego
//            String server = "http://192.168.92.56:80"; // Polsat Cyfrowy DEV
//            String user = "mtararuj";
//            String pass = "TajneHasło06";
//            String spaceKeyToFetch = "PIDOSIiR";
//// //           String server = "http://192.168.82.82:80"; // Polsat Cyfrowy PRD

            ConfluenceContentFixer fixer = new ConfluenceContentFixer("http://localhost:8080/BIKS", server + "/", new IConfluenceObjectImportedChecker() {
                @Override
                public Map<String, String> checkConfluenceImportedObjs(Collection<String> objIds, Integer optSelectedNodeId) {
                    final HashMap<String, String> res = new HashMap<String, String>();
//                    res.put(557117L, "http://biksunio.biks.pl/node#" + 557117L);
                    return res;
                }

                @Override
                public Map<String, String> checkDwhImportedObjs(Collection<String> dwhObjNames) {
                    return new HashMap<String, String>();
                }

                public Map<String, String> checkConfluenceImportedLogins(Collection<String> logins) {
                    return new HashMap<String, String>();
                }
            });
            //            String pass = "TajneHasło05";
            try {
//                String endPoint = "/rpc/soap-axis/confluenceservice-v1";
                String endPoint = "/rpc/soap-axis/confluenceservice-v2";
//                String endPoint = "/wiki/rpc/soap-axis/confluenceservice-v1";
//                String endPoint = "/rpc/xmlrpc";
//                String endPoint = "/wiki/rpc/xmlrpc/";
                ConfluenceSoapServiceServiceLocator fConfluenceSoapServiceGetter = new ConfluenceSoapServiceServiceLocator();
//                fConfluenceSoapServiceGetter.setConfluenceserviceV1EndpointAddress(server + endPoint);
                fConfluenceSoapServiceGetter.setConfluenceserviceV2EndpointAddress(server + endPoint);
                fConfluenceSoapServiceGetter.setMaintainSession(true);
//                fConfluenceSoapService = fConfluenceSoapServiceGetter.getConfluenceserviceV1();
                fConfluenceSoapService = fConfluenceSoapServiceGetter.getConfluenceserviceV2();
                System.out.println("before login");
                fToken = fConfluenceSoapService.login(user, pass);
                System.out.println("after login");
            } catch (Exception e) {
                System.out.println("Err: " + e.getMessage());
            }
            System.out.println("Connected ok.");
            ConfluenceSoapService service = getConfluenceSOAPService();
            String token = getToken();

            RemoteServerInfo info;
            try {
                info = service.getServerInfo(token);
                System.out.println("Confluence version: " + info.getMajorVersion() + "." + info.getMinorVersion());
            } catch (Exception ex) {
                System.out.println("Error: " + ex.getMessage());
            }

            ConfluenceObjectContentRetrieverViaHttp via = new ConfluenceObjectContentRetrieverViaHttp(server + "/", user, pass);

            RemoteSpaceSummary[] spaces = service.getSpaces(token);
            for (int i = 0; i < spaces.length; i++) {
                RemoteSpaceSummary oneSpace = spaces[i];
                final String spaceKey = oneSpace.getKey();
                System.out.println("<<<<<<<<<< Space: " + oneSpace.getName() + ", type: " + oneSpace.getType() + /*", url: " + oneSpace.getUrl() + */ ", key: " + spaceKey);
                long startNano = System.nanoTime();

                if (!BaseUtils.safeEquals(spaceKey, spaceKeyToFetch)) {
                    continue;
                }

                RemotePageSummary[] pages = service.getPages(token, spaceKey);
                System.out.println("Page size: " + pages.length + ", getPages() takes: " + String.valueOf((System.nanoTime() - startNano) / (double) 1000000000l) + " second");
                for (int j = 0; j < pages.length; j++) {
                    RemotePageSummary onePage = pages[j];
                    System.out.println("P--------> Id: " + onePage.getId() + ", parentID: " + onePage.getParentId() + ", title: " + onePage.getTitle() + ", space: " + onePage.getSpace() /*+ ", version:" + onePage.getVersion() + ", url: " + onePage.getUrl()*/);
//                    RemotePage page = service.getPage(token, onePage.getId());
                    saveConfluenceObjectById(service, token, fixer, via, ObjectKind.Page, onePage.getId(), spaceKey);
//                    saveConfluenceComments(service, token, onePage, fixer, via, spaceKey);
                }
                long startBlogNano = System.nanoTime();
                RemoteBlogEntrySummary[] blogEntries = service.getBlogEntries(token, spaceKey);
                System.out.println("Blog size: " + blogEntries.length + ", getBlogEntries() takes: " + String.valueOf((System.nanoTime() - startBlogNano) / (double) 1000000000l) + " second");
                for (int j = 0; j < blogEntries.length; j++) {
                    RemoteBlogEntrySummary blog = blogEntries[j];
                    System.out.println("$--------> Id: " + blog.getId() + ", name: " + blog.getTitle() + ", space: " + blog.getSpace() + ", author: " + blog.getAuthor() + ", date: " + blog.getPublishDate().getTime());
                    saveConfluenceObjectById(service, token, fixer, via, ObjectKind.Blog, blog.getId(), spaceKey);
//                    saveConfluenceComments(service, token, blog, fixer, via, spaceKey);
                }

            }
            System.out.println("Completed.");
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }

    protected static void saveConfluenceComments(ConfluenceSoapService service, String token, AbstractRemotePageSummary onePage, ConfluenceContentFixer fixer, ConfluenceObjectContentRetrieverViaHttp via, final String spaceKey) throws RemoteException {
        RemoteComment[] comments = service.getComments(token, onePage.getId());
        for (RemoteComment comment : comments) {
            saveConfluenceObjectById(service, token, fixer, via, ObjectKind.Comment,
                    comment.getPageId(),
                    //                    comment.getId(),
                    spaceKey,
                    comment.getContent());
        }

//                    System.out.println("page before render:\n" + page.getContent());
//                    System.out.println("page rendered=\n" + service.renderContent(token, oneSpace.getKey(), page.getId(), null, renderOptions));
//                    page.getContent();
    }

    public static enum ObjectKind {

        Blog, Page, Comment
    };

    protected static void saveConfluenceObjectById(ConfluenceSoapService service,
            String token,
            ConfluenceContentFixer fixer,
            ConfluenceObjectContentRetrieverViaHttp via, ObjectKind objKind, long objectId,
            String spaceKey) {
        saveConfluenceObjectById(service, token, fixer, via, objKind, objectId, spaceKey, null);
    }

    protected static void saveConfluenceObjectById(ConfluenceSoapService service,
            String token,
            ConfluenceContentFixer fixer,
            ConfluenceObjectContentRetrieverViaHttp via, ObjectKind objKind, long objectId,
            String spaceKey, String optContent) {
        Exception exx = null;
        String html = null;
        String htmlFull = null;

        if (objKind == ObjectKind.Blog || objKind == ObjectKind.Page) {
            try {
                html = via.getHtml(objectId);
                htmlFull = via.getHtmlFull(objectId);
            } catch (Exception ex) {
                exx = ex;
            }
        }
        final String namePrefix = "c:/temp/Confluence/content_" + objectId + "_" + objKind;

        if (exx == null) {
            if (objKind == ObjectKind.Blog || objKind == ObjectKind.Page) {
                LameUtils.saveStringNoExc(namePrefix + ".html", html, "UTF8", true, true);
                LameUtils.saveStringNoExc(namePrefix + "_full.html", htmlFull, "UTF8", true, true);
                LameUtils.saveStringNoExc(namePrefix + "_fix.html", fixer.fixContent(html, null), "UTF8", true, true);
                LameUtils.saveStringNoExc(namePrefix + "_fullFix.html", fixer.fixContent(
                        ConfluenceObjectContentRetrieverViaHttp.extractWikiContentFromFullHtml(htmlFull, null), null), "UTF8", true, true);
            }

            try {

                Long objIdForRender = objectId;// objKind == ObjectKind.Comment ? null : objectId;

                LameUtils.saveStringNoExc(namePrefix + "_rendered.html",
                        service.renderContent(token, spaceKey, objIdForRender, optContent), "UTF8", true, true);
                LameUtils.saveStringNoExc(namePrefix + "_renderedClean.html",
                        service.renderContent(token, spaceKey, objIdForRender, optContent, renderOptions), "UTF8", true, true);

//                http://192.168.167.54:8090/pages/viewpage.action?pageId=557100
            } catch (Exception ex) {
                exx = ex;
            }

        }

        if (exx != null) {
            LameUtils.saveStringNoExc(namePrefix + "_err.html",
                    StackTraceUtil.getCustomStackTrace(exx), "UTF8", true);
        }
    }
}
