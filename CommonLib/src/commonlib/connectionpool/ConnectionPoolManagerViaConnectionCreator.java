/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.connectionpool;

import java.sql.Connection;
import java.sql.SQLException;
import simplelib.IContinuationWithReturn;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pmielanczuk
 */
public class ConnectionPoolManagerViaConnectionCreator extends ConnectionPoolManagerBase {

    protected IContinuationWithReturn<Connection> connectionCreator;

    public ConnectionPoolManagerViaConnectionCreator(IContinuationWithReturn<Connection> connectionCreator, int poolSize, int secsTimeout, IParametrizedContinuation<Connection> prepareNewConnCont) {
        super(poolSize, secsTimeout, prepareNewConnCont);
        this.connectionCreator = connectionCreator;
    }

    public ConnectionPoolManagerViaConnectionCreator(IContinuationWithReturn<Connection> connectionCreator, int poolSize, int secsTimeout) {
        this(connectionCreator, poolSize, secsTimeout, null);
    }

    @Override
    protected Connection openNewConnectionInternal() throws SQLException {
        return connectionCreator.doIt();
    }
}
