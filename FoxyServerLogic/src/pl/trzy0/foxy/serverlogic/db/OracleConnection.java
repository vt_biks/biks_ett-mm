/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import commonlib.connectionpool.ConnectionPoolManagerViaDataSource;
import oracle.jdbc.xa.client.OracleXADataSource;
import pl.trzy0.foxy.serverlogic.IConnectionDialectHelper;
import simplelib.LameRuntimeException;

/**
 *
 * @author tflorczak
 */
public class OracleConnection extends PooledJdbcConnectionBase<OracleConnectionConfig> {

    public OracleConnection(OracleConnectionConfig config) {
        initConnectionFinal(config);
    }

    @Override
    public void initConnection(OracleConnectionConfig cfg) {
        try {
//            OracleConnectionPoolDataSource
            OracleXADataSource ds = new OracleXADataSource();
            ds.setServerName(cfg.server);
            ds.setDatabaseName(cfg.database);
            ds.setPortNumber(cfg.port);
            ds.setServiceName(cfg.instance);
            ds.setUser(cfg.user);
            ds.setPassword(cfg.password);
            ds.setDriverType("thin");
            cpm = new ConnectionPoolManagerViaDataSource(ds, 30, 30) {
                {
                    disableConnectionIsValidCheck();
                }
            };

        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
    }

    @Override
    protected IConnectionDialectHelper createDialectHelper() {
        return new SqlValueFormatterCurlyTimeStamp();
        // do testow
    }
}
