﻿exec sp_check_version '1.0.88.2';
go

update bik_joined_objs
set inherit_to_descendants=1
where src_node_id in (select id from bik_node where node_kind_id=(select id from bik_node_kind where code='ReportQuery'))
and dst_node_id in (select id from bik_node where node_kind_id=(select id from bik_node_kind where code='Universe'))


exec sp_update_version '1.0.88.2', '1.0.88.5';
go