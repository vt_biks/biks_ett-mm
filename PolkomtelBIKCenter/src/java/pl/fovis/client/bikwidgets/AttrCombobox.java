/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog;
import pl.fovis.common.BIKConstants;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class AttrCombobox extends AttrWigetBase {

    protected String atrLinValue;
    protected List<String> attrOpt;
    protected String atrName;
    protected boolean isEditAttr;
    protected List<RadioButton> rbs = new ArrayList<RadioButton>();
    protected Grid gridIcons;
    protected int height = 0;
    protected String defaultValue;
    protected boolean isEnabledForEdition = true;

    protected IParametrizedContinuation<Boolean> ic;

    public AttrCombobox(String atrLinValue, List<String> attrOpt, String atrName, boolean isEditAttr, String defaultValue) {
        this.atrLinValue = atrLinValue;
        this.attrOpt = attrOpt;
        this.atrName = atrName;
        this.isEditAttr = isEditAttr;
        this.defaultValue = defaultValue;
    }

    public AttrCombobox(String atrLinValue, List<String> attrOpt, String atrName, boolean isEditAttr, String defaultValue, IParametrizedContinuation<Boolean> ic) {
        this.atrLinValue = atrLinValue;
        this.attrOpt = attrOpt;
        this.atrName = atrName;
        this.isEditAttr = isEditAttr;
        this.defaultValue = defaultValue;
        this.ic = ic;
    }

    public AttrCombobox(String atrLinValue, List<String> attrOpt, String atrName, boolean isEditAttr, String defaultValue, boolean isEnabledForEdition) {
        this.atrLinValue = atrLinValue;
        this.attrOpt = attrOpt;
        this.atrName = atrName;
        this.isEditAttr = isEditAttr;
        this.defaultValue = defaultValue;
        this.isEnabledForEdition = isEnabledForEdition;
    }

    @Override
    public Widget getWidget() {
        rbs.clear();
        VerticalPanel vp = new VerticalPanel();
        int attrWidth = 242;
        vp.setWidth(attrWidth + "px");
        ScrollPanel scollPanel = createScrolledPanel(vp, attrOpt.size());
        if (BIKConstants.METABIKS_ATTR_ICON_NAME.equals(atrName)) {
            gridIcons = BIKClientSingletons.createSelectIconView(attrOpt, atrLinValue);
            for (int i = 0; i < gridIcons.getRowCount(); i++) {
                for (int j = 0; j < gridIcons.getCellCount(i); j++) {
                    if (gridIcons.getWidget(i, j) instanceof RadioButton) {
                        rbs.add((RadioButton) gridIcons.getWidget(i, j));
                    }
                }
            }
            if (!isEditAttr) {
                for (RadioButton r : rbs) {
                    r.setEnabled(false);
                }
            }
            vp.add(gridIcons);
        } else {
            for (final String ao : attrOpt) {
//                 RadioButton radioButton = new RadioButton(BaseUtils.trimAndCompactSpaces(ab.atrName), ao);
                final RadioButton radioButton = new RadioButton("atr" + BaseUtils.trimAndCompactSpaces(atrName) /* I18N: no */, ao);
                vp.add(radioButton);
                if (isEditAttr && BaseUtils.trimLeftAndRight(ao).equals(BaseUtils.trimLeftAndRight(atrLinValue))) {
                    radioButton.setValue(true);
                }
                if (!isEnabledForEdition) {
                    radioButton.setEnabled(false);
                }

                if (!attrOpt.get(0).equals(ao)) {
                    height = height + 19;
                }
                if (BIKClientSingletons.showAddStatus() && BIKConstants.METABIKS_ATTR_STATUS_NAME.equals(atrName)) {
                    radioButton.addClickHandler(new ClickHandler() {
                        @Override
                        public void onClick(ClickEvent event) {
//                            Window.alert(radioButton.getName() + " " + BIKConstants.METABIKS_ATTR_STATUS_NAME + " " + atrName + " " + ao);
                            ic.doIt(ao.equals(BIKConstants.YES));
                        }
                    });
                }

                rbs.add(radioButton);
            }
            height = height + 26;
        }
        return scollPanel;
    }

    @Override
    public String getValue() {

        String attrDescription = "";
        if (BIKConstants.METABIKS_ATTR_ICON_NAME.equals(atrName)) {
            attrDescription = BIKClientSingletons.getSelectedIconFromView(gridIcons);
        } else {
            for (RadioButton r : rbs) {
                if (r.getValue()) {
                    attrDescription = r.getHTML();
                }
            }
        }
        if (BaseUtils.isStrEmptyOrWhiteSpace(attrDescription)) {
            if (!BaseUtils.isStrEmptyOrWhiteSpace(defaultValue)) {
                return defaultValue;
            }
        }
        return attrDescription;
    }

    @Override
    public int getWidgetHeight() {
        return height;
    }

    @Override
    public void setEnable(boolean isEnable, String attrName) {
        for (RadioButton rb : rbs) {
            rb.setEnabled(isEnable);
//            Window.alert(rb.getHTML() + " " + rb.getText());
            if (!isEnable) {
                if (attrName.equals(BIKConstants.METABIKS_ATTR_TYPE_ATTR)) {
                    rb.setValue(rb.getHTML().equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.combobox.name()));
                } else {
                    rb.setValue(rb.getHTML().equals("Nie"));
                }
            }
        }
    }

}
