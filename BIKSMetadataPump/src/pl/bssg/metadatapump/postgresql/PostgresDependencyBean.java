/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.postgresql;

/**
 *
 * @author tflorczak
 */
public class PostgresDependencyBean {

    public String server;
    public String databaseName;
    public String refSchema;
    public String refName;
    public String depSchema;
    public String depName;

    public PostgresDependencyBean(String server, String databaseName, String refSchema, String refName, String depSchema, String depName) {
        this.server = server;
        this.databaseName = databaseName;
        this.refSchema = refSchema;
        this.refName = refName;
        this.depSchema = depSchema;
        this.depName = depName;
    }
}
