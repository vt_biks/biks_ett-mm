/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.dqm.AddOrEditDQMDataErrorIssueDialog;
import pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase;
import pl.fovis.client.entitydetailswidgets.IMainHeaderSharedTable;
import pl.fovis.common.dqm.DQMDataErrorIssueBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.SimpleDateUtils;

/**
 *
 * @author ctran
 */
public class DQMDataErrorIssueWidget extends EntityDetailsWidgetFlatBase {

    private static final int ALL = 2;
    protected IMainHeaderSharedTable imhst;
    protected VerticalPanel mainVp;
    protected List<DQMDataErrorIssueBean> allIssues;
    protected FlexTable table;
    protected int selectedStatus = ALL;

    public DQMDataErrorIssueWidget(IMainHeaderSharedTable imhst) {
        this.imhst = imhst;
    }

    @Override
    public void displayData() {
        mainVp.clear();

        if (getData().dataErrorIssueBean != null) {
            DQMDataErrorIssueBean bean = getData().dataErrorIssueBean;
            imhst.addTranslatedRow(I18n.id.get(), bean.id);
            imhst.addTranslatedRow(I18n.data.get(), bean.createdDate);
            imhst.addTranslatedRow(I18n.status.get(), bean.errorStatus == 0 ? I18n.otwarty.get() : I18n.zamkniety.get());
            imhst.addTranslatedRow(I18n.grupaDanych.get(), bean.groupName);
            imhst.addTranslatedRow(I18n.opiekunGrupyDanych.get(), bean.leaderName);
            imhst.addTranslatedRow(I18n.akcje.get(), bean.actionPlan);
            return;
        }
        allIssues = getData().allDataErrorIssuesInNode;
        if (allIssues != null) {
            mainVp.add(createHeaderPanel());
            table = new FlexTable();
            table.setCellSpacing(5);
            mainVp.add(table);
            doFilter(ALL);
            return;
        }
    }

    @Override
    public Widget buildWidgets() {
        mainVp = new VerticalPanel();
        mainVp.setWidth("100%");
        return mainVp;
    }

    private HorizontalPanel createHeaderPanel() {
        HorizontalPanel hp = new HorizontalPanel();
        final ListBox lbStatusFilter = new ListBox();
        lbStatusFilter.addItem(I18n.otwarty.get());
        lbStatusFilter.addItem(I18n.zamkniety.get());
        lbStatusFilter.addItem(I18n.wszystkie.get());
        lbStatusFilter.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                selectedStatus = lbStatusFilter.getSelectedIndex();
                doFilter(selectedStatus);
            }
        });
        lbStatusFilter.setSelectedIndex(ALL);
        hp.add(new Label(I18n.status.get()));
        hp.add(lbStatusFilter);
        hp.setSpacing(6);
        hp.add(createAddBtn());
        return hp;
    }

    private PushButton createEditBtn(final DQMDataErrorIssueBean bean) {
        PushButton selectLeaderBtn = new PushButton(new Image("images/edit_yellow.png" /* I18N: no */), new Image("images/edit_yellow.png" /* I18N: no */));
        selectLeaderBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new AddOrEditDQMDataErrorIssueDialog().buildAndShowDialog(bean, new IParametrizedContinuation<DQMDataErrorIssueBean>() {

                    @Override
                    public void doIt(DQMDataErrorIssueBean param) {
                        BIKClientSingletons.getDQMService().saveDataErrorIssue(param, new StandardAsyncCallback<Integer>("Error in saveDataErrorIssue") {

                            @Override
                            public void onSuccess(Integer result) {
                                BIKClientSingletons.showInfo(I18n.zapisanoZgloszenie.get());
                                ctx.refreshAndRedisplayData();
                                doFilter(selectedStatus);
                            }
                        });
                    }
                });
            }
        });
        selectLeaderBtn.setStyleName("admin-btn");
        selectLeaderBtn.setWidth("16px");
        selectLeaderBtn.setTitle(I18n.edytujZgloszenie.get() /* I18N:  */);
        return selectLeaderBtn;
    }

    private void createErrorHeader(FlexTable table) {
        int col = 0;
        table.setHTML(0, col++, "<b>" + I18n.lp.get() + "</b>");
        table.setHTML(0, col++, "<b>" + I18n.nazwa.get() + "</b>");
//        table.setHTML(0, col++, "<b>" + I18n.id.get() + "</b>");
        table.setHTML(0, col++, "<b>" + I18n.data.get() + "</b>");
        table.setHTML(0, col++, "<b>" + I18n.opiekunGrupyDanych.get() + "</b>");
        table.setHTML(0, col++, "<b>" + I18n.status.get() + "</b>");
        table.setHTML(0, col++, "<b>" + I18n.akcje.get() + "</b>");
    }

    private void setDataErrorIssueRow(FlexTable table, int row, DQMDataErrorIssueBean bean) {
        int col = 0;
        table.setText(row, col++, String.valueOf(row));
        table.setText(row, col++, bean.name);
//        table.setText(row, col++, String.valueOf(bean.id));
        table.setText(row, col++, SimpleDateUtils.dateTimeToString(bean.createdDate, "-", false));
        table.setText(row, col++, bean.leaderName);
        table.setText(row, col++, (bean.errorStatus == 0 ? I18n.otwarty.get() : I18n.zamkniety.get()));
        table.setText(row, col++, bean.actionPlan);
        table.setWidget(row, col++, createEditBtn(bean));
        table.getRowFormatter().setStyleName(row, row % 2 != 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
    }

    private void doFilter(Integer statusId) {
        table.removeAllRows();
        table.setCellSpacing(5);
        table.setWidth("100%");
        createErrorHeader(table);
        int row = 1;
        for (DQMDataErrorIssueBean bean : allIssues) {
            if (ALL == statusId || statusId == bean.errorStatus) {
                setDataErrorIssueRow(table, row++, bean);
            }
        }
    }

    private PushButton createAddBtn() {
        PushButton addBtn = new PushButton(I18n.dodaj.get());
        NewLookUtils.makeCustomPushButton(addBtn);
        addBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getDQMService().getDQMDataErrorIssuePreInfo(getNodeId(), new StandardAsyncCallback<DQMDataErrorIssueBean>() {

                    @Override
                    public void onSuccess(DQMDataErrorIssueBean result) {
                        new AddOrEditDQMDataErrorIssueDialog().buildAndShowDialog(result, new IParametrizedContinuation<DQMDataErrorIssueBean>() {

                            @Override
                            public void doIt(DQMDataErrorIssueBean param) {
                                BIKClientSingletons.getDQMService().saveDataErrorIssue(param, new StandardAsyncCallback<Integer>("Error in saveDataErrorIssue") {

                                    @Override
                                    public void onSuccess(Integer result) {
                                        BIKClientSingletons.showInfo(I18n.dodanoZgloszenie.get());
                                        ctx.refreshAndRedisplayData();
                                        doFilter(selectedStatus);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
        return addBtn;
    }
}
