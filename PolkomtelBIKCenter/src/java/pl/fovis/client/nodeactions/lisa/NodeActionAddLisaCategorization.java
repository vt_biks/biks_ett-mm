/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.lisa;

import com.google.gwt.user.client.rpc.AsyncCallback;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.entitydetailswidgets.lisa.LisaAuthorizedRequest;
import pl.fovis.client.nodeactions.NodeActionUpdatingBase;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.OneTextBoxDialog;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author pku
 */
public class NodeActionAddLisaCategorization extends NodeActionUpdatingBase {

    public NodeActionAddLisaCategorization() {
    }

    @Override
    public boolean isEnabledByDesign() {
        return isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_LISA_DICTIONARY) && BIKClientSingletons.isBikUserLoggedIn() && hasChildren();
    }

    @Override
    public String getCaption() {
        return I18n.dodajKategoryzacje.get() /* I18N:  */;
    }

    @Override
    public void execute() {
        OneTextBoxDialog dialog = new OneTextBoxDialog();
        dialog.buildAndShowDialog(I18n.podajNazwe.get() /* I18N:  */, "", new IParametrizedContinuation<String>() {
                    @Override
                    public void doIt(String param) {
                        saveNewCategorization(param);
                    }
                });
    }

    private void saveNewCategorization(final String categorizationName) {
        BIKClientSingletons.getLisaService().getLisaInstanceByNodeId(node.id, new StandardAsyncCallback<LisaInstanceInfoBean>() {

            @Override
            public void onSuccess(final LisaInstanceInfoBean result) {
                new LisaAuthorizedRequest<Pair<TreeNodeBean, TreeNodeBean>>(result.id, result.name) {
                    @Override
                    protected void performServerAction(AsyncCallback<Pair<TreeNodeBean, TreeNodeBean>> callback) {
                        BIKClientSingletons.getLisaService().saveNewCategorization(result.id, categorizationName, data.node.id, callback);
                    }

                    @Override
                    public void onActionSuccess(Pair<TreeNodeBean, TreeNodeBean> result) {
                        BIKClientSingletons.showInfo(I18n.kategoryzacja.get() /* I18N:  */ + " \"" + categorizationName + "\" " + I18n.zostalaDodana.get() /* I18N:  */);
                        // refreshRedisplayDataAndSelectRecord(getSelectedNodeId());
                        refreshRedisplayDataAndSelectRecord(result.v1.id);
//                ctx.addTreeGridNode(result.v1);
//                ctx.addTreeGridNode(result.v2);
                    }
                }.setNodeId(node.id).go();
            }
        });
    }
}
