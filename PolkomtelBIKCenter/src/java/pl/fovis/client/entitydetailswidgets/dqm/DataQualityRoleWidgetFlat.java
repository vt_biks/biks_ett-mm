/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.entitydetailswidgets.UserInNodeWidgetFlatBase;

/**
 *
 * @author beata
 */
public class DataQualityRoleWidgetFlat extends UserInNodeWidgetFlatBase {

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_QUALITY_METADATA;
    }

    @Override
    public String getCaption() {
        return /*BIKGWTConstants.MENU_NAME_DQC + "<br>" + */ BIKGWTConstants.MENU_NAME_DATA_QUALITY;
    }
}
