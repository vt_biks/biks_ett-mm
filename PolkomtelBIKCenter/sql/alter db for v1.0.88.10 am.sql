﻿exec sp_check_version '1.0.88.10';
go
--PO WYKONANIU TEGO SKRYPTU NALEŻY WYKONAĆ ZASILANIE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
alter table aaa_universe_class
	add si_id_designer varchar(1000);
go
----------------------------------------------------------------------
----------------------------------------------------------------------
alter table aaa_universe_class
	add si_parentid varchar(1000);
go
----------------------------------------------------------------------
----------------------------------------------------------------------
alter table aaa_universe_obj
	add si_parentid varchar(1000);
go
----------------------------------------------------------------------
----------------------------------------------------------------------
insert into bik_node_kind(code, caption, icon_name, tree_kind)
values('UniverseTable', 'Tabela', 'blueTable', 'Metadata')
go
----------------------------------------------------------------------
----------------------------------------------------------------------
alter table bik_sapbo_universe_table
	add node_id int references bik_node(id);
go
----------------------------------------------------------------------
----------------------------------------------------------------------
alter table bik_sapbo_universe_table
	drop column name;
go
----------------------------------------------------------------------
----------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
declare @nameFK varchar(255);

select @nameFK = fk.ForeignKey_Name
from sysobjects so inner join syscolumns sc on so.id = sc.id
	left join vw_ww_foreignkeys fk on fk.table_name = so.name and fk.name = sc.name
where  so.name like 'bik_sapbo_universe_table'
	and fk.Name = 'universe_node_id'

if(not @nameFK is null)
	exec ('alter table bik_sapbo_universe_table drop constraint ' + @nameFK)
go
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
if exists (select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[bik_sapbo_universe_table]') AND name = N'UNIQ_unique_their_id_bik_sapbo_universe_table')
alter table [dbo].[bik_sapbo_universe_table] drop constraint [UNIQ_unique_their_id_bik_sapbo_universe_table]
go
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
delete from bik_sapbo_universe_column;
delete from bik_sapbo_universe_table;
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
alter table bik_sapbo_universe_table
	add constraint UNIQ_unique_their_id_bik_sapbo_universe_table unique(their_id, node_id)
go
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
alter table bik_sapbo_universe_table
	drop column universe_node_id;
go
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
declare @nameFK varchar(255);

select @nameFK = fk.ForeignKey_Name
from sysobjects so inner join syscolumns sc on so.id = sc.id
	left join vw_ww_foreignkeys fk on fk.table_name = so.name and fk.name = sc.name
where  so.name like 'bik_sapbo_universe_column'
	and fk.Name = 'table_id'

if(not @nameFK is null)
	exec ('alter table bik_sapbo_universe_column drop constraint ' + @nameFK)
go
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_objects_from_Designer]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]
go

create procedure sp_insert_into_bik_node_objects_from_Designer
as
	declare @tree_id int;
	declare @kind_id int;
	declare @dimension_nki int, @measure_nki int, @detail_nki int, @filtr_nki int;
begin--begin procedure

	----wybranie id odpowiedniego drzewa
	select @tree_id = id
	from bik_tree
	where code = 'ObjectUniverses';	
	
	----wybranie id odpowiedniego kind'a
	select @kind_id = id
	from bik_node_kind
	where code = 'UniverseClass';
		
	create table #tmpNodes (si_id varchar(1000) , si_parentid varchar(1000), si_kind_id int, si_name varchar(max), si_description varchar(max));

	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Class
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select si_id_designer, si_parentid, @kind_id, name, description
	from aaa_universe_class  
	
	
	----wybranie id odpowiedniego kind'a
	--detail
	select @detail_nki = id
	from bik_node_kind
	where code = 'Detail';
	--demension
	select @dimension_nki = id
	from bik_node_kind
	where code = 'Dimension';
	--measure
	select @measure_nki = id
	from bik_node_kind
	where code = 'Measure';
	--filtr
	select @filtr_nki = id
	from bik_node_kind
	where code = 'Filter';
	
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Obj
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select obj_branch, si_parentid, 
			case
				when qualification = 'dsDetailObject' then @detail_nki
				when qualification = 'dsMeasureObject' then @measure_nki
				when qualification = 'dsDimensionObject' then @dimension_nki
				end as node_kind_id,
		 name, description
	from aaa_universe_obj
							 						 
	--wrzucam do tabeli tymczasowej wszystkie dane z Universe_filrt
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select uf.filtr_branch,	uc.si_id_designer, @filtr_nki, uf.name, nullif(ltrim(rtrim(uf.description)), '') 
	from aaa_universe_filter uf join aaa_universe_class uc on uf.universe_class_id = uc.id
	
	declare @folder_node_kind_id int;
	select @folder_node_kind_id = dbo.fn_node_kind_id_by_code('UniversesFolder');

	--wrzucam katalogi do których są wrzucane tabele
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)
	select convert(varchar(10),global_props_si_id) + '|tableCatalog', convert(varchar(10),global_props_si_id), @folder_node_kind_id, 'Tabele'
	from aaa_universe 


	--wybranie id odpowiedniego kind'a
	declare @tabele_node_kind_id int;
	select @tabele_node_kind_id = dbo.fn_node_kind_id_by_code('UniverseTable');
						 
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Table	
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)		
	select convert(varchar(10), their_id) + '|' + convert(varchar(1000), universe_branch), convert(varchar(1000), universe_branch) + '|tableCatalog', 
			@tabele_node_kind_id, name 
	from aaa_universe_table 
	
	----wybranie id odpowiedniego kind'a
	declare @column_node_kind_id int;
	select @column_node_kind_id = dbo.fn_node_kind_id_by_code('UniverseColumn');
	
	--aktualizacja istniejących węzłów w drzewie							
	update bik_node 
	set parent_node_id = null, node_kind_id = #tmpNodes.si_kind_id, name = #tmpNodes.si_name,
		is_deleted = 0, descr = #tmpNodes.si_description, tree_id = @tree_id
	from #tmpNodes 
	where bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id and bik_node.linked_node_id is null;
	
	--dorzucanie nowych węzłów w drzewie
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select #tmpNodes.si_kind_id, #tmpNodes.si_name, @tree_id, #tmpNodes.si_id, #tmpNodes.si_description
	from #tmpNodes left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
		and bik_node.tree_id = @tree_id
	where bik_node.id is null;	
	
	--uaktualnianie parentów w nodach
	declare @universe_kind_id int;
	select @universe_kind_id = id
	from bik_node_kind
	where code = 'Universe';
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id and bik_node.node_kind_id = pt.si_kind_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id 
		and  (
		 (pt.si_kind_id=@detail_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@dimension_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@measure_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@filtr_nki and bk.node_kind_id=@kind_id) or
		 (pt.si_kind_id=@kind_id and (bk.node_kind_id = @kind_id or bk.node_kind_id = @universe_kind_id))or
		 (pt.si_kind_id=@tabele_node_kind_id and bk.node_kind_id=@folder_node_kind_id)or
		 (pt.si_kind_id=@folder_node_kind_id and bk.node_kind_id=@universe_kind_id)
		 )
	where bik_node.linked_node_id is null;
		
	--usuwanie nodów
	update bik_node
	set is_deleted = 1
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and bik_node.node_kind_id in (@kind_id, @detail_nki, @dimension_nki, @measure_nki,
	  @filtr_nki);
	
	drop table #tmpNodes;
	--uzupełnianie danych w tabelach extra 
	--dla object
	create table #tmpObjExtra(text_of_select varchar(max),text_of_where varchar(max), type varchar(155), aggregate_function varchar(50), node_id int)
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select univ_obj.text_of_select, univ_obj.text_of_where, univ_obj.type, univ_obj.aggregate_function, bik_node.id
	from aaa_universe_obj univ_obj join bik_node on bik_node.obj_id = univ_obj.obj_branch 
		and bik_node.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)-- = @obj_node_kind_id;
	/*
	select univ_obj.text_of_select, univ_obj.text_of_where, univ_obj.type, univ_obj.aggregate_function, bik_node.id
	from (
	 select obj_branch as obj_id, text_of_select, text_of_where, type, aggregate_function 
	 from aaa_universe_obj)
							 univ_obj 
			join bik_node on bik_node.obj_id = univ_obj.obj_id and bik_node.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)-- = @obj_node_kind_id;
			*/
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select null, uf.where_clause, uf.type, null, bik_node.id
	from aaa_universe_filter uf join bik_node on bik_node.obj_id = uf.filtr_branch and bik_node.node_kind_id = @filtr_nki
	/*
	select null, univ_filtr.where_clause, univ_filtr.type, null, bik_node.id
	from (
		select uf.filtr_branch as obj_id, uf.where_clause, uf.type
		from aaa_universe_filter uf ) univ_filtr
				 join bik_node on bik_node.obj_id = univ_filtr.obj_id and bik_node.node_kind_id = @filtr_nki
		*/	
	update bik_sapbo_universe_object
	set text_of_select = #tmpObjExtra.text_of_select, text_of_where = #tmpObjExtra.text_of_where, type = #tmpObjExtra.type,  
		           aggregate_function = #tmpObjExtra.aggregate_function 
	from #tmpObjExtra join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id;
	
	insert into bik_sapbo_universe_object(text_of_select, text_of_where, type, aggregate_function, node_id)
	select #tmpObjExtra.text_of_select,#tmpObjExtra.text_of_where, #tmpObjExtra.type, #tmpObjExtra.aggregate_function, #tmpObjExtra.node_id 
	from #tmpObjExtra left join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id
	where bik_sapbo_universe_object.id is null;

	drop table #tmpObjExtra;
	
	--dla tabeli
	
	update bik_sapbo_universe_table
	set is_alias = ut.is_alias, is_derived = ut.is_derived, original_table = ut.original_table, sql_of_derived_table = ut.sql_of_derived_table, 
		sql_of_derived_table_with_alias = ut.sql_of_derived_table_with_alias, their_id = ut.their_id, node_id = bik_node.id, type = ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
	where linked_node_id is null and bik_node.node_kind_id = @tabele_node_kind_id
	
	insert into bik_sapbo_universe_table(is_alias, is_derived, original_table, sql_of_derived_table, sql_of_derived_table_with_alias, their_id, node_id, type)
	select ut.is_alias, ut.is_derived, ut.original_table, ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, ut.their_id, 
					bik_node.id as node_id, ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
		
	where  bik_node.linked_node_id is null and bsut.id is null and bik_node.node_kind_id = @tabele_node_kind_id

	--usupełnianie name_for_teradata
	update bik_sapbo_universe_table
	set name_for_teradata = replace(bik_node.name,'.', '|') + '|'
	from bik_sapbo_universe_table join bik_node on bik_sapbo_universe_table.node_id = bik_node.id
	where bik_sapbo_universe_table.type = 'Teradata' and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 0
	/*update bik_sapbo_universe_table
	set name_for_teradata = replace(name,'.', '|') + '|'
	where type = 'Teradata' and is_derived = 0 and is_alias = 0
	*/
	update bik_sapbo_universe_table
	set name_for_teradata = bsut.name_for_teradata
	from bik_sapbo_universe_table join bik_sapbo_universe_table bsut on bik_sapbo_universe_table.original_table = bsut.their_id and 
		bik_sapbo_universe_table.node_id = bsut.node_id
	where bik_sapbo_universe_table.type = 'Teradata' and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 1
	/*
	update bik_sapbo_universe_table
	set name_for_teradata = bsut.name_for_teradata
	from bik_sapbo_universe_table join bik_sapbo_universe_table bsut on bik_sapbo_universe_table.original_table = bsut.their_id and 
		bik_sapbo_universe_table.universe_node_id = bsut.universe_node_id
	where bik_sapbo_universe_table.type = 'Teradata' and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 1
	*/
	
	update bik_sapbo_universe_table
	set schema_name = left(name_for_teradata, charindex('|',name_for_teradata))
	where  type = 'Teradata' and is_derived = 0
	
	--dla kolumn
	update bik_sapbo_universe_column
	set name = uc.name, type = uc.type, table_id = bik_node.id--bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			--join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
			join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bik_node.id--bsut.id
	where linked_node_id is null and bik_node.node_kind_id = @tabele_node_kind_id
	
	insert into bik_sapbo_universe_column(name, type, table_id)
	select uc.name, uc.type, bik_node.id--bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			--join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id =bik_node.id--bsut.id
	where linked_node_id is null and bsuc.id is null  and bik_node.node_kind_id = @tabele_node_kind_id

	--delete from bik_sapbo_universe_column i bik_sapbo_universe_column nie jest potrzebne bo w bik_node jest ustawiont isDeleted na 1:)
	
	delete from bik_sapbo_universe_connection;
	
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id, type, database_enigme, client_number, language, system_number, system_id)
	select aaa_universe_connection.server, aaa_universe_connection.user_name, aaa_universe_connection.password, aaa_universe_connection.database_source, 
		aaa_universe_connection_networklayer.name, bik_node.id,
		aaa_universe_connection.type, aaa_universe_connection.database_enigme, aaa_universe_connection.client_number, aaa_universe_connection.language,
		aaa_universe_connection.system_number, aaa_universe_connection.system_id
	from aaa_universe_connection join aaa_universe_connection_networklayer on aaa_universe_connection.connetion_networklayer_id = aaa_universe_connection_networklayer.id
		join bik_node on aaa_universe_connection.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		left join bik_sapbo_universe_connection on  bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections' and bik_sapbo_universe_connection.id is null;
		
	update bik_sapbo_extradata
	set statistic = u.statistic
	from bik_sapbo_extradata join bik_node bn on bn.id = bik_sapbo_extradata.node_id
		join bik_node_kind bnk on bn.node_kind_id = bnk.id
		join aaa_universe u on convert(varchar,u.global_props_si_id) = bn.obj_id
	where bnk.code = 'Universe'	
				
	exec sp_delete_linked_nodes_where_orignal_is_deleted;	
	exec sp_node_init_branch_id @tree_id, null;
end--end procedure
go

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_add_converted_varchar_branch_after_insert_to_bik_node]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_add_converted_varchar_branch_after_insert_to_bik_node]
GO

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
exec sp_update_version '1.0.88.10', '1.0.88.11';
go