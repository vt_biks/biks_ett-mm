SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[aaa_data_source_def](
	[id] [int] IDENTITY(1,1) NOT NULL,
        [description] [char](50) NULL
PRIMARY KEY CLUSTERED
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
)

GO

SET IDENTITY_INSERT aaa_data_source_def ON
GO

insert into dbo.aaa_data_source_def(id, description) values(1, 'TERADATA DBMS')
insert into dbo.aaa_data_source_def(id, description) values(2, 'BO')
GO

SET IDENTITY_INSERT aaa_data_source_def OFF
GO

SET ANSI_PADDING OFF
GO