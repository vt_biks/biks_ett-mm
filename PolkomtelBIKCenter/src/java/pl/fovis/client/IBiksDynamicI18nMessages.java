/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import pl.fovis.common.i18npoc.I18n;
import simplelib.i18nsupport.IDynamicI18nMessages;

/**
 *
 * @author pmielanczuk
 */
public interface IBiksDynamicI18nMessages extends IDynamicI18nMessages<I18n> {

}
