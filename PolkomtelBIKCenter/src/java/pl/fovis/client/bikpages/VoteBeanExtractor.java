/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.IGridBeanAction;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.VoteBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author beata
 */
public class VoteBeanExtractor extends NodeAwareBeanExtractorBase<VoteBean> {

    public VoteBeanExtractor() {
        super(false);
    }

    public Integer getNodeId(VoteBean b) {
        return b.nodeId;
    }

    public String getTreeCode(VoteBean b) {
        return b.code;
    }

    protected String getName(VoteBean b) {
        return b.caption;
    }

    protected String getNodeKindCode(VoteBean b) {
        return b.nodeKindCode;
    }

    public String getBranchIds(VoteBean b) {
        return b.branchIds;
    }

    @Override
    public int getAddColCnt() {
        return 1;
    }

    @Override
    public Object getAddColVal(VoteBean b, int addColIdx) {
        boolean isUserValueLike = b.userValue == 1;
        return BIKClientSingletons.createIconImg("images" /* I18N: no */ + "/" + (isUserValueLike ? "like" /* I18N: no */ : "unlike" /* I18N: no */) + "3." + "gif" /* I18N: no */, "27px", "20px", I18n.ocenilesJako.get() /* I18N:  */ + " " + (isUserValueLike ? I18n.uzyteczne.get() /* I18N:  */ : I18n.nieuzyteczne.get() /* I18N:  */));
    }

    @Override
    protected List<IGridBeanAction<VoteBean>> createActionList() {
        List<IGridBeanAction<VoteBean>> res = new ArrayList<IGridBeanAction<VoteBean>>();
        res.add(actView);
        res.add(actFollow);
        return res;
    }
}
