/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author wezyr
 */
public class HtmlUtils {

    public static IRegExpPattern stripHtmlTagsPatt
            = BaseUtils.getRegExpMatcherFactory().compile("\\<[^>]*>", true, true);

    public static String replaceHtmlTags(String htmlTxt, String replacement) {
        return htmlTxt == null ? null
                : stripHtmlTagsPatt.matcher(htmlTxt).replaceAll(replacement);
    }

    public static String stripHtmlTags(String htmlTxt, boolean replaceWithSpace) {
        return replaceHtmlTags(htmlTxt, replaceWithSpace ? " " : "");
    }

    public static String stripHtmlTags(String htmlTxt) {
        return stripHtmlTags(htmlTxt, false);
    }

    public static String stripHtmlComments(String htmlTxt) {
        return replaceHtmlComments(htmlTxt, null);
    }

    public static String replaceHtmlComments(String htmlTxt, String replacement) {
        return BaseUtils.replaceEnclosedParts("<!--", "-->", htmlTxt, replacement);
    }

    public static String stripHtmlTagsAndComments(String htmlTxt) {
        return stripHtmlTags(stripHtmlComments(htmlTxt));
    }
}
