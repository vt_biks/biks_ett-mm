-- UWAGA: tylko dla porz�dku zmieni�em komentarz przy sp_verticalize_node_attrs
-- bo by� nieprawdziwy. innych zmian nie ma teraz (2012-03-22),
-- czyli nie trzeba tego uruchamia� ponownie

exec sp_check_version '1.1.0.7';
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- poprzednia wersja z pliku "alter db for v1.1.0.6 pm.sql"
-- powr�t do podej�cia z usuwaniem - wy��czanie aktualizacji jednak nie dzia�a�o dobrze...

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs
go

create procedure [dbo].[sp_verticalize_node_attrs](@optNodeFilter varchar(max))
as
begin
  set nocount on
  
  if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) begin
    exec('drop fulltext index on bik_searchable_attr_val')
    --exec('ALTER FULLTEXT INDEX ON bik_searchable_attr_val SET CHANGE_TRACKING MANUAL')
  end
  
  exec sp_verticalize_node_attrs_inner @optNodeFilter

  if not exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) begin
    declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_searchable_attr_val') and is_primary_key = 1)
    exec('CREATE FULLTEXT INDEX ON bik_searchable_attr_val (value, fixed_value) key index ' + @pk_name + ' with stoplist = off')
  end
  
  if (select change_tracking_state from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) <> 'A' begin
    exec('ALTER FULLTEXT INDEX ON bik_searchable_attr_val SET CHANGE_TRACKING AUTO')
  end
end
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

exec sp_update_version '1.1.0.7', '1.1.0.7.1';
go
