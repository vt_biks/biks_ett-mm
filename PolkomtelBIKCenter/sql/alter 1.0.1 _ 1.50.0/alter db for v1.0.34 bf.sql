exec sp_check_version '1.0.34';
go

alter table bik_metapedia
add def_status int not null default 0
go

alter table bik_metapedia 
add user_id int references bik_user (id) 
go 

/*przypisany jeden uzytkownik*/
update bik_metapedia set user_id=(select MIN(id) from bik_user) 
go

insert into bik_department (name) values ('departament 1');
insert into bik_department (name) values ('departament 2');
insert into bik_department (name) values ('departament 3');
insert into bik_department (name) values ('departament 4');
insert into bik_department (name) values ('departament 5');
	
drop table  bik_def_dependency
go
create table bik_def_dependency (
id int identity (1,1) primary key not null,
metapedia_id int references bik_metapedia (id) not null,
node_id int references bik_node (id) not null
constraint uk_bik_def_dependency unique (metapedia_id, node_id)
);
go
drop table  bik_def_department;
go
create table bik_def_department (
id int identity (1,1) primary key not null,
department_id int references bik_department (id) not null,
metapedia_id int references bik_metapedia (id) not null
constraint uk_bik_def_department unique (department_id, metapedia_id)
);
go
alter table bik_user add department_id int 
references bik_department (id)
go

/*przypisani do jednego dep*/
update bik_user set department_id=(select MIN(id) from bik_department)
go

alter table bik_metapedia drop CONSTRAINT uk_bik_metapedia_subject
go
alter table bik_metapedia drop column subject
go
exec sp_update_version '1.0.34', '1.0.35';
go
