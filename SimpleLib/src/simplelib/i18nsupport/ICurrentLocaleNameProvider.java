/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.i18nsupport;

/**
 *
 * @author pmielanczuk
 */
public interface ICurrentLocaleNameProvider {

    public static String DEFAULT_LOCALE_NAME = "";

    public String getCurrentLocaleName();
}
