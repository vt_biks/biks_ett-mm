/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.lisa;

import com.google.gwt.user.client.ui.HTML;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.nodeactions.NodeActionUpdatingBase;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author pgajda
 */
public class NodeActionDelLisaCategory extends NodeActionUpdatingBase {

    public NodeActionDelLisaCategory() {
    }

    @Override
    public boolean isEnabledByDesign() {
        return isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_LISA_CATEGORY) && BIKClientSingletons.isBikUserLoggedIn();
    }

    @Override
    public String getCaption() {
        return I18n.usunKategorie.get() /* I18N:  */;
    }

    @Override
    public void execute() {
        HTML element = new HTML(
                I18n.czyNaPewnoUsunacElement.get() /* I18N:  */ + ": <div class='PUWelement'><img src='images/" /* I18N: no */ + BIKClientSingletons.getNodeKindIconNameByCode(node.nodeKindCode, node.treeCode) + "." + "gif" /* I18N: no */ + "'/> " + BaseUtils.encodeForHTMLTag(node.name) + "</div> " + I18n.wrazZElementamiPodrzednymi.get() /* I18N:  */ + "?");
        element.setStyleName("komunikat" /* I18N: no:css-class */);
        new ConfirmDialog().buildAndShowDialog(element, I18n.usun.get() /* I18N:  */, new IContinuation() {
                    @Override
                    public void doIt() {
                        BIKClientSingletons.getLisaService().getLisaInstanceByNodeId(node.id, new StandardAsyncCallback<LisaInstanceInfoBean>() {

                            @Override
                            public void onSuccess(final LisaInstanceInfoBean result) {
                                BIKClientSingletons.getLisaService().deleteCategory(result.id, node, new StandardAsyncCallback<List<String>>("Failure on" /* I18N: no */ + " deleteTreeNode") {
                                    @Override
                                    public void onSuccess(List<String> result) {
                                        BIKClientSingletons.showInfo(I18n.usunietoElement.get() /* I18N:  */);
//                                ctx.getTreeGrid().deleteNode(node.id);
//                                refreshAndRedisplayDataByTreeCode(result);
                                        refreshRedisplayDataAndSelectRecord(node.parentNodeId);
                                    }
                                });

                            }
                        });
                    }
                });
        clipboard.clear();
    }
}
