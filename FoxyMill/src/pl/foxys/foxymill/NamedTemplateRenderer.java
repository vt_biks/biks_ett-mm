/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill;

import java.util.List;
import java.util.Map;
import pl.foxys.foxymill.renderctx.RenderCtx;
import pl.foxys.foxymill.tags.ITag;

/**
 *
 * @author wezyr
 */
public class NamedTemplateRenderer implements INamedTemplateRenderer {

    private IFoxyMillNamedTagsProvider tmplProvider;

    public NamedTemplateRenderer(IFoxyMillNamedTagsProvider tmplProvider) {
        this.tmplProvider = tmplProvider;
    }

    @SuppressWarnings("deprecation")
    public String render(String templateName, Map<String, Object> vars, Object model) {
        List<ITag> tags = tmplProvider.getParsedTemplate(templateName);
//        if (model == null) {
//            model = new Object();
//        }
        RenderCtx renderCtx = RenderCtx.makeRootRenderCtx(model, vars);
        FoxyMillUtils.render(tags, renderCtx);
        return renderCtx.getOutputStr();
    }
}
