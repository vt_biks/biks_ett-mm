/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.dqc;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class DQCGroupBean implements Serializable {

    public int idGroup;
    public String name;
    public String description;
    public boolean deleted; // BIT NOT NULL DEFAULT 0,
    public String objId;
}
