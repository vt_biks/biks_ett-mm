/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Map;
import pl.bssg.biks.adm.common.AuthenticationDataBean;
import pl.bssg.biks.adm.common.MultiInfoBean;

/**
 *
 * @author pmielanczuk
 */
public interface AppAndDBVerInfoServiceAsync {

//    public void checkSqlFilesDir(Map<String, String> dirs, AsyncCallback<Map<String, Set<String>>> asyncCallback);
    public void getInfo(AuthenticationDataBean adb, String optDBInstanceNameForLogs,
            Integer minLogId, Map<String, String> sqlFilesDirsToCheck,
            AsyncCallback<MultiInfoBean> asyncCallback);

//    public void getLogMsgs(AuthenticationDataBean adb, int procId, Integer minLogId, AsyncCallback<List<DbauLogBean>> asyncCallback);
    public void runDbUpgradeProcess(AuthenticationDataBean adb,
            String dbInstanceName, String dbLogin, String dbPass, String sqlFilesDir,
            AsyncCallback<Integer> asyncCallback);

    public void keepSessionAlive(AsyncCallback<Void> asyncCallback);
}
