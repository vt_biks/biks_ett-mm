/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.common;

import java.io.Serializable;

/**
 *
 * @author pmielanczuk
 */
public class InstanceInfoBean implements Serializable {

    public String dbInstanceName;
    public String dbVer;
    public DbauProcessBean lastProcessInfo;
    public boolean sqlFilesDirHasScript;
//    //--- to-do!
//    public String accAppLogin;
//    public String accAppPass;
//    public String load_user_login;
//    public String load_user_pwd;
}
