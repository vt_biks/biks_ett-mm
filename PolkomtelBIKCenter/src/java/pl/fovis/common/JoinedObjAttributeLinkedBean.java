/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author bfechner
 */
public class JoinedObjAttributeLinkedBean implements Serializable {

    public int id;
    public int srcId;
    public int dstId;
    public String name;
    public String value;
    public String valueTo;
    public int attributeId;
    public boolean isMain;
}
