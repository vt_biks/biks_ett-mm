exec sp_check_version '1.5.x.4';
go

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------
--    wyciągnięcie danych o nowotworzonych baz danych
--    w multiXie do osobnej tabeli
--    
--    dodanie namiarow na skrzynkę pocztową
----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

create table mltx_database (
	id int identity(1,1) not null primary key,
	created_at datetime default GETDATE() not null,
	database_name varchar(255) not null
	unique (database_name)
);
go

insert into mltx_database (database_name) 
select distinct database_name from mltx_login
go

alter table mltx_login
add database_id integer;
go

alter table mltx_login
add constraint mltx_login_FK_to_mltx_database foreign key (database_id) references mltx_database(id);
go

update mltx_login set database_id = d.id
from mltx_database d where
mltx_login.database_name = d.database_name;
go

alter table mltx_login alter column database_id integer not null;
go

alter table mltx_login
drop column database_name ;
go

insert into mltx_app_prop (name, val) values ('database_files_destination_path','D:\db\')
go

insert into mltx_app_prop (name, val) values ('multixEmailUser','testerbps');
insert into mltx_app_prop (name, val) values ('multixEmailPassword','maselnica');
insert into mltx_app_prop (name, val) values ('multixEmailSmtpHostName','smtp.gmail.com');
insert into mltx_app_prop (name, val) values ('multixEmailSmtpPort','465');
insert into mltx_app_prop (name, val) values ('multixEmailFrom','BIKS team <testerbps@gmail.com>');
insert into mltx_app_prop (name, val) values ('multixEmailSsl','true')

exec sp_update_version '1.5.x.4', '1.5.x.5';
go