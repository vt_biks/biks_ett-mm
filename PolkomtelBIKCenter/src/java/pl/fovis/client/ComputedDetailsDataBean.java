/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import java.util.List;
import java.util.Map;
import pl.fovis.common.JoinedObjBean;
import pl.fovis.common.JoinedObjsTabKind;
import pl.fovis.common.UserRoleInNodeBean;

/**
 *
 * @author wezyr
 */
public class ComputedDetailsDataBean {

    public Map<JoinedObjsTabKind, List<JoinedObjBean>> jobs;
    public Map<Integer, JoinedObjBean> jobsByDstNodeId;
    public Map<Integer, String> nodeNamesById;
    //role
    public Map<Integer, UserRoleInNodeBean> userRoleInNodeByDstNodeId;
    public Map<String, List<UserRoleInNodeBean>> userRoleInNodeByTreeKind;
}
