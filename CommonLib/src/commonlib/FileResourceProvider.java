/*
 * FileResourceProvider.java
 *
 * Created on 10 październik 2007, 14:23
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib;

import simplelib.LameRuntimeException;
import java.io.FileInputStream;

/**
 *
 * @author wezyr
 */
public class FileResourceProvider implements IResourceProvider {
    
    public FileResourceProvider() {
    }

    
    public long getLastModified(String resourceName) {
        return LameUtils.getLastModified(resourceName);
    }

    public byte[] loadAsBytes(String resourceName) {
        try {
            return LameUtils.inputStreamToByteArray(new FileInputStream(resourceName));
        } catch (Exception ex) {
            throw new LameRuntimeException("loadAsBytes: " + resourceName, ex);
        }
    }
}
