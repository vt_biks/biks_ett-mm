/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

/**
 *
 * @author wezyr
 */
public class AntiNodeRefreshingCallback<T> extends BaseNodeRefreshingCallback<T> {

    public AntiNodeRefreshingCallback(int nodeId, String msgOnSuccess, String errorMsg) {
        super(nodeId, msgOnSuccess, errorMsg);
    }

    public AntiNodeRefreshingCallback(int nodeId, String msgOnSuccess) {
        this(nodeId, msgOnSuccess, null);
    }

    @Override
    public void onFailure(Throwable caught) {
        super.onFailure(caught);
        refreshNode();
    }
}
