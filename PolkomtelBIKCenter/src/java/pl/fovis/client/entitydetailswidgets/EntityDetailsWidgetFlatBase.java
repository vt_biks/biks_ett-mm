/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import pl.fovis.client.AntiNodeRefreshingCallback;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.BOXIServiceAsync;
import pl.fovis.client.ComputedDetailsDataBean;
import pl.fovis.client.EDPForms;
import pl.fovis.client.ICheckEnabledByDesign;
import pl.fovis.client.ITabSelector;
import pl.fovis.client.NodeRefreshingCallback;
import pl.fovis.client.nodeactions.INodeActionContext;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.KeywordsWidget;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IContinuationWithReturn;
import simplelib.IParametrizedContinuationWithReturn;

/**
 *
 * @author wezyr
 */
public abstract class EntityDetailsWidgetFlatBase implements IEntityDetailsWidget {

    // specjalnie private!
    private EntityDetailsDataBean data;
    protected INodeActionContext ctx;
    protected ComputedDetailsDataBean cddb;
    protected Integer currentLinkingParentId;
    protected int nodeId;
    protected int nodeKindId;
    protected int treeId;
    private List<Widget> visibledByStateWidgets = new ArrayList<Widget>();
    protected BikCustomButton actionBtn;
//    protected List<PushButton> buttons = new ArrayList<PushButton>();
    protected Map<PushButton, Boolean> buttons = new HashMap<PushButton, Boolean>();
    private HTML lblZero;
    protected boolean widgetsActive;
    protected KeywordsWidget tabWidget;
//    protected Map<Widget, HorizontalPanel> hintWrappersForLabels = new HashMap<Widget, HorizontalPanel>();
    protected Map<String, HorizontalPanel> hintWrappersByHintName = new HashMap<String, HorizontalPanel>();
    protected Map<String, Widget> hintButtonsByHintName = new HashMap<String, Widget>();
    protected TreeBean treeBean;
    protected boolean isNodeLinked;

    public void setup(boolean widgetsActive, TreeBean treeBean) {
        this.widgetsActive = widgetsActive;
        this.treeBean = treeBean;
    }

//    protected void setVisibleOfLabelWithHint(Label lbl, boolean val) {
//        Widget wrapper = hintWrappersForLabels.get(lbl);
//        if (wrapper == null) {
//            wrapper = lbl;
//        }
//        wrapper.setVisible(val);
//    }
    protected Panel createHintedHTML(final String hintName, String text) {

        HorizontalPanel wrapper = new HorizontalPanel();
        HTML html = new HTML("<b>" + hintName + ":</b> ");
        wrapper.add(html);
        if (!BIKClientSingletons.hideHelpForAttr()) {
            Widget btn = BIKClientSingletons.createHintButton(hintName);

            wrapper.add(btn);

            BIKClientSingletons.setupDisplayOnHover(btn, new IContinuationWithReturn<Boolean>() {
                @Override
                public Boolean doIt() {
                    return BIKClientSingletons.attrHasHint(hintName);
                }
            });

            if (text != null) {
                wrapper.add(new HTML(text));
            }
            hintButtonsByHintName.put(hintName, btn);
        }
        wrapper.setStyleName("gp-html");
        hintWrappersByHintName.put(hintName, wrapper);
        return wrapper;
    }

    protected PushButton createFollowButton(final String nodeTreeCode, final int nodeId) {
        return BIKClientSingletons.createFollowButton(nodeTreeCode, nodeId, true);
    }

    protected BikCustomButton createLabelLookupButton(String caption, final String nodeTreeCode, final int nodeId,
            final Integer linkingParentId) {
        return BIKClientSingletons.createLabelLookupButton(caption, nodeTreeCode, nodeId, linkingParentId, widgetsActive);
    }

    protected PushButton createLookupButton(final String nodeTreeCode, final int nodeId,
            final Integer linkingParentId) {
        return BIKClientSingletons.createLookupButton(nodeTreeCode, nodeId, linkingParentId, widgetsActive);
    }

    protected void addWidgetVisibledByState(Widget he) {
        visibledByStateWidgets.add(he);
    }

    protected void refreshactionBtn() {
        if (actionBtn != null) {
//            actionBtn.setEnabled(isEditonEnabled());
            actionBtn.setVisible(isEditonEnabled());
        }
    }

//    public void refreshWidgetsVisible() {
//        for (Widget he : visibledByStateWidgets) {
//            he.setVisible(widgetsActive);
//        }
//    }
    public void refreshWidgetsState() {

        for (Widget he : visibledByStateWidgets) {
            he.setVisible(isEditonEnabled());
        }

        refreshactionBtn();
        refreshButtons(isEditonEnabled());

        for (Entry<String, Widget> e : hintButtonsByHintName.entrySet()) {
            String hintName = e.getKey();
            Widget btn = e.getValue();
            Widget wrapper = hintWrappersByHintName.get(hintName);
            if (wrapper.isVisible()) {
                btn.setVisible(BIKClientSingletons.attrHasHint(hintName));
            }
        }
    }

    protected void refreshButtons(boolean isEditonEnabled) {
        for (Entry<PushButton, Boolean> b : buttons.entrySet()) {
//            b.setEnabled(isEditonEnabled);
            b.getKey().setVisible(isEditonEnabled && !b.getValue());
        }
    }

    final protected void refreshAndRedisplayData() {
        BIKClientSingletons.getTabSelector().invalidateNodeData(nodeId);
    }

    @Override
    abstract public void displayData();

    // dla bazowych podklas - do ustawiania specyficznych informacji
    // np. items dla EntityDetailsTabWithItemsBase, ale też np. do zawołania
    // dataFetched w subwidgetach (w subwidgetach subwidgetu - jasne?)
    protected void extractAndAssignExtraPropsFromFetchedData() {
        // no-op
    }

    @Override
    final public void dataFetched(INodeActionContext optCtx, EntityDetailsDataBean data,
            ComputedDetailsDataBean cddb) {
        this.ctx = optCtx;
        this.data = data;
        this.nodeId = data.node.id;
        this.nodeKindId = data.node.nodeKindId;
        this.treeId = data.node.treeId;
        this.cddb = cddb;

        extractAndAssignExtraPropsFromFetchedData();

        buttons.clear();

        //displayData();
    }

    public HTML zeroMsg(String msg) {
        lblZero = new HTML("<i>" + msg + "</i>");
        lblZero.setWidth("100%");
        lblZero.setStyleName(I18n.brak.get() /* I18N:  */);

        return lblZero;
    }

    public FlexTable makeGridSpace(int row, int node, FlexTable gp) {
        FlowPanel space = new FlowPanel();
        for (int i = 0; i < node; i++) {
            gp.setWidget(row + 1, i, space);
        }
        gp.getRowFormatter().setStyleName(row + 1, "space" /* i18n: no */);
        gp.getCellFormatter().setWidth(row + 1, 0, "32");
        gp.getCellFormatter().setStyleName(row + 1, 0, "eloziom" /* I18N: no:css-class */);
        return gp;
    }

    protected ITabSelector<Integer> getTabSelector() {
        return BIKClientSingletons.getTabSelector();
    }

    protected EDPForms getForms() {
        return BIKClientSingletons.getEDPForms();
    }

    protected AsyncCallback<Void> getNodeRefreshingCallback(String msgOnSuccess) {
        return new NodeRefreshingCallback<Void>(nodeId, msgOnSuccess);
    }

    protected AsyncCallback<Void> getNodeRefreshingCallback() {
        return getNodeRefreshingCallback(null);
    }

    protected AsyncCallback<Void> getAntiNodeRefreshingCallback(String msgOnSuccess) {
        return new AntiNodeRefreshingCallback<Void>(nodeId, msgOnSuccess);
    }

    protected AsyncCallback<Void> getAntiNodeRefreshingCallback() {
        return getAntiNodeRefreshingCallback(null);
    }

//    public boolean canCurrentUserEdit() {
//        // obie metody najlepiej scalić - może edytować gdy może edytować ;-)
//        return isEditonEnabled();
//    }
    protected String getOptNodeActionCode() {
        return "EditNodeDetails";
    }

    protected boolean isEditionEnabledByDesign() {
        return widgetsActive && (BIKClientSingletons.isEffectiveExpertLoggedIn()
                || BIKClientSingletons.isBranchAuthorLoggedIn(data.node.branchIds) || BIKClientSingletons.isLoggedUserAuthorOfTree(data.node.treeId)
                || BIKClientSingletons.isLoggedUserCreatorOfTree(data.node.treeId) || BIKClientSingletons.isBranchCreatorLoggedIn(data.node.branchIds));
//        return widgetsActive && (BIKClientSingletons.isEffectiveExpertLoggedIn() || BIKClientSingletons.isNodeAuthorOrEditorLoggedIn(data));
    }

    public boolean isEditonEnabled() {

        return BIKClientSingletons.isNodeActionEnabledByCustomRightRoles(getOptNodeActionCode(), data, new ICheckEnabledByDesign() {

            @Override
            public boolean isEnabledByDesign() {
                return isEditionEnabledByDesign();
            }
        });
        //ww: domyślnie tylko admin i ekspert mogą edytować
        // return widgetsActive && (BIKClientSingletons.isEffectiveExpertLoggedIn() || BIKClientSingletons.isNodeAuthorOrEditorLoggedIn(data));
    }

    //ww->authors: komentuję, bo zbędne, użycia przerobiłem na użycia
    // isEffectiveExpertLoggedIn
//    protected boolean isAdminLoggedIn() {
//        return BIKClientSingletons.isSysAdminLoggedIn();
//    }
//    protected boolean isEffectiveExpertLoggedIn() {
//        return BIKClientSingletons.isEffectiveExpertLoggedIn();
//    }
//    protected final void refreshAndRedisplayDataByTreeCode(List<String> results) {
//        if (results != null) {
//            for (String codeLinkedNode : results) {
//                BIKClientSingletons.invalidateTabData(codeLinkedNode);
//            }
//        }
//    }
    public boolean hasContent() {
        return true;
    }

    protected boolean isNodeKindCode(String nodeKindCode, String code) {
        return BaseUtils.safeEquals(nodeKindCode, code);
    }

    protected boolean isInUserTree(String nodeTreeCode) {
        //wyłącza zakładki w drzewkach z rolami i grupami uzytkownikow
        return BaseUtils.safeEquals(nodeTreeCode, BIKGWTConstants.TREE_CODE_USER_ROLES)
                || BaseUtils.safeEquals(nodeTreeCode, BIKGWTConstants.TREE_CODE_USERS);
//                isNodeKindCode(nodeKindCode, BIKGWTConstants.NODE_KIND_USER) || isNodeKindCode(nodeKindCode, BIKGWTConstants.NODE_KIND_USERS_GROUP);
    }

    public boolean hasEditableControls() {
        return true;
    }

    protected BOXIServiceAsync getService() {
        return BIKClientSingletons.getService();
    }

    protected ScrollPanel addScrollPanel(VerticalPanel vp) {
        ScrollPanel scrollPanel = new ScrollPanel();
        scrollPanel.add(vp);
        scrollPanel.setHeight((BIKClientSingletons.ORIGINAL_EDP_BODY + 28) + "px");
        return scrollPanel;
    }

    protected EntityDetailsDataBean getData() {
        return data;
    }

    protected String getNodeTreeCode() {
        return data.node.treeCode;
    }

    protected String getNodeKindCode() {
        return data.node.nodeKindCode;
    }

    protected String getTreeKind() {
        return data.node.treeKind;
    }

    protected int getNodeId() {
        return data.node.id;
    }

    protected String getNodeName() {
        return data.node.name;
    }

    protected String getNodeKindCaption() {

        if (BaseUtils.safeEquals(data.node.treeCode, BIKGWTConstants.TREE_CODE_DQM)
                && BaseUtils.safeEquals(data.node.nodeKindCode, BIKGWTConstants.NODE_KIND_DQM_GROUP)) {
            int cnt = BaseUtils.countSubstrings(data.node.branchIds, "|");
            if (cnt == 1) { // grupa danych
                return "Grupa danych";
            } else if (cnt == 2) { // atrybut grupy danych
                return "Atrybut jakości danych";
            }
        }

        return data.node.nodeKindCaption;
    }

    protected boolean hasChildren() {
        return data.hasChildren;
    }

    /**
     * Sprawdza, czy widget jest umiejscowiony w dialogu. Trochę nieciekawa
     * metada, dlatego umieszczam w dodatkowej metodzie z opisems
     *
     * @return true jeśli jest z dialogu, false inaczej
     */
    protected boolean isWidgetRunFromDialog() {
        return treeBean == null;
    }

    public void setIsLinked(boolean isLinked) {
        this.isNodeLinked = isLinked;
    }

    //
    public static void createHeaders(FlexTable gp, String... columns) {
        createHeaders(gp, true, columns);
    }

    public static void createHeaders(FlexTable gp, boolean showLastColumn, String... columns) {
        if (columns.length == 0) {
            return;
        }
        for (int i = 0; i < columns.length; i++) {
            if (i == columns.length - 1 && !showLastColumn) {
                continue;
            }
            HTML obj = new HTML("<b>" + columns[i] + "</b>");
            obj.setStyleName("cellWithPadding");
            gp.setWidget(0, i + 1, obj);
            gp.getCellFormatter().addStyleName(0, i + 1, "cellWithPaddingWithTable");
        }
    }

    public static IParametrizedContinuationWithReturn<Integer, String> defaultRowStyleCalc = new IParametrizedContinuationWithReturn<Integer, String>() {

        @Override
        public String doIt(Integer inx) {
            return inx % 2 == 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey";
        }
    };

    public static void createRow(FlexTable gp, int inx, IParametrizedContinuationWithReturn<Integer, String> rowStyleCalc, Widget... widgets) {
        createRow(gp, inx, true, true, rowStyleCalc, widgets);
    }

    public static void createRow(FlexTable gp, int inx, Widget... widgets) {
        createRow(gp, inx, true, true, defaultRowStyleCalc, widgets);
    }

    public static ClickHandler createFollowCH(final String treeCode, final Integer nodeId) {
        return new ClickHandler() {
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getTabSelector().submitNewTabSelection(treeCode, nodeId, true);
            }
        };
    }

    public static void createRow(FlexTable gp, int inx, boolean showPreLastColumn, boolean showLastColumn, Widget... widgets) {
        createRow(gp, inx, showPreLastColumn, showLastColumn, defaultRowStyleCalc, widgets);
    }

    public static void createRow(FlexTable gp, int inx, boolean showPreLastColumn, boolean showLastColumn, IParametrizedContinuationWithReturn<Integer, String> rowStyleCalc, Widget... widgets) {
        if (widgets.length == 0) {
            return;
        }
        for (int i = 0; i < widgets.length; i++) {
            if ((i == widgets.length - 1 && !showLastColumn) || (i == widgets.length - 2 && !showPreLastColumn)) {
                continue;
            }
            widgets[i].addStyleName("cellWithPadding");
            gp.setWidget(inx, i, widgets[i]);
        }

        if (rowStyleCalc != null) {
            final String rowStyle = rowStyleCalc.doIt(inx);
            if (rowStyle != null) {
                gp.getRowFormatter().setStyleName(inx, rowStyle);
            }
        }
    }

    protected Widget createHeaderEx(String text, boolean addPreSpacing) {
        VerticalPanel vp = new VerticalPanel();
        if (addPreSpacing) {
            Label lbl1 = new Label();
            lbl1.setHeight("30px");
            vp.add(lbl1);
        }
        vp.add(new HTML("<B>" + text + ":</B>"));
        Label lbl2 = new Label();
        lbl2.setHeight("5px");
        vp.add(lbl2);
        return vp;
    }

    public static Widget createPostSpacing() {
        Label lbl2 = new Label();
        lbl2.setHeight("20px");
        return lbl2;
    }

    protected Widget createHeader(String text) {
        return createHeaderEx(text, false);
    }

    public static void setColumnWidth(FlexTable gp, Integer... procents) {
        for (int i = 0; i < procents.length; i++) {
            gp.getColumnFormatter().setWidth(i, procents[i] + "%");
        }
    }

    // TF: W przypadku widgetów, które potrzebują dociągnąc dane - metoda będzie pokryta
    @Override
    public void optCallServiceToGetData(IContinuation con) {
        // NO OP
    }

    @Override
    public void optSetCurrentLinkingParentId(Integer currentLinkingParentId) {
        this.currentLinkingParentId = currentLinkingParentId;
    }

    public Integer getLisaInstanceId() {
        return data.lisaInstanceInfo.id;
    }

    public String getLisaInstanceName() {
        return data.lisaInstanceInfo.name;
    }
}
