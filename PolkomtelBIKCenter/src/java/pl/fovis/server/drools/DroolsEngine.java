/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.drools;

import commonlib.LameUtils;
import java.io.File;
import java.util.Collection;
import java.util.List;
import org.drools.core.ObjectFilter;
import org.drools.core.impl.StatefulKnowledgeSessionImpl;
import org.drools.io.ResourceFactory;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.Message.Level;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import pl.fovis.common.DroolsEnvironment;
import pl.fovis.server.BOXIServiceImpl;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class DroolsEngine implements IRulesManagementEngine {

    protected KieServices kieServices = KieServices.Factory.get();
    protected KieSession kieSession;
    protected BOXIServiceImpl service;

    public DroolsEngine() {
    }

    @Override
    public Pair<EngineStatus, String> reset(String drlFolder) {
        return reset(drlFolder, false);
    }

    @Override
    public Pair<EngineStatus, String> reset(String drlFolder, boolean doReset) {
        try {
            File dir = new File(drlFolder);
            if (!dir.isDirectory()) {
                return new Pair<EngineStatus, String>(EngineStatus.FAILED, drlFolder + " is not folder.");
            }

            KieFileSystem kfs = kieServices.newKieFileSystem();

            List<File> rulesFiles = LameUtils.getFileListInFolder(drlFolder, ".drl", ".xls");

            for (File file : rulesFiles) {
                kfs.write(ResourceFactory.newFileResource(file));
            }

            KieBuilder kieBuilder = kieServices.newKieBuilder(kfs).buildAll();
            StringBuilder sb = new StringBuilder();
            List<Message> warnings = kieBuilder.getResults().getMessages(Level.WARNING);
            for (Message msg : warnings) {
                sb.append("\n  " + msg);
            }
            if (kieBuilder.getResults().hasMessages(Level.ERROR)) {
                List<Message> errors = kieBuilder.getResults().getMessages(Level.ERROR);
                for (Message msg : errors) {
                    sb.append("\n  " + msg);
                }
                return new Pair<EngineStatus, String>(EngineStatus.FAILED, sb.toString());
            }

            kieServices.getRepository().addKieModule(kieBuilder.getKieModule());
            KieContainer kieContainer = kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());

            Collection<? extends Object> allOldObjsInMem = null;
            if (kieSession != null) {
                allOldObjsInMem = kieSession.getObjects(new ObjectFilter() {

                    @Override
                    public boolean accept(Object object) {
                        if (object instanceof StatefulKnowledgeSessionImpl.ObjectStoreWrapper) {
                            return false;
                        }
                        if (object instanceof DroolsEnvironment) {
                            return false;
                        }
                        return true;
                    }
                });
                kieSession.dispose();
                kieSession.destroy();
            }

            kieSession = kieContainer.newKieSession();
            if (allOldObjsInMem != null && !doReset) {
                addAll(allOldObjsInMem);
            }

            kieSession.setGlobal("service", service);

            return new Pair<EngineStatus, String>(EngineStatus.OK, "OK");
        } catch (Exception ex) {
            return new Pair<EngineStatus, String>(EngineStatus.FAILED, LameUtils.printExceptionAsString(ex));
        }
    }

    @Override
    public void fireAllRules() {
        try {
            kieSession.fireAllRules();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void addAll(Object... objs) {
        for (Object o : objs) {
            kieSession.insert(o);
        }
    }

    public void setService(BOXIServiceImpl service) {
        this.service = service;
    }

    @Override
    public Collection<? extends Object> getObjects() {
        return kieSession.getObjects();
    }
}
