/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;

/**
 *
 * @author pku
 */
public class LisaDictionaryBean implements Serializable, IsSerializable {

    public String displayName;
    public String dbName;      //tabela slownikowa, moze byc widok lub tabela
    public String dbNameReal;  //faktyczna tabela slownikowa, fizyczna
    public String dbRelTable;
    public String columnRelName;
    public Integer isVersioned;
    public int nodeId;
    public Integer lisaId;
    public String lisaName;
}
