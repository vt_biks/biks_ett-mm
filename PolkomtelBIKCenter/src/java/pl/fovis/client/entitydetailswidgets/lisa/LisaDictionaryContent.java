/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import java.util.Map;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase;
import pl.fovis.common.lisa.LisaDictionaryValue;
import pl.fovis.foxygwtcommons.grid.BeanGrid;
import pl.fovis.foxygwtcommons.grid.BeanGridPager;

/**
 *
 * @author pku
 */
public class LisaDictionaryContent extends EntityDetailsWidgetFlatBase {

    private static final Integer PAGE_SIZE = 50;
//    private static final Integer DEPTH_OF_DICT_IN_BIK_NODE = 2;
    private FlowPanel panel;
//    private ChangableBeanProvider beanProvider = null;
    private BeanGrid<Map<String, LisaDictionaryValue>> grid = null;
    private LisaDictGridDisplay beanDisplay = null;
    private Button changeCategoryButton = null;
    private Button refreshButton = null;
    private Button modifyButton = null;
    private Button cancelButton = null;
    private Button confirmModifyButton = null;
    private Button deleteButton = null;
    private Button insertButton = null;
    private Button confirmInsertButton = null;
    private Button cancelInsertButton = null;
    private BeanGridPager gridPager;
    protected Panel headerPanel = null;
//    private Integer previousDictId = null;

    public LisaDictionaryContent() {
    }

    @Override
    public void displayData() {
        if (hasContent()) {
//            if (setOfColumnsShouldBeReloaded()) {
            generateGrid();
//            } else {
//                reloadGridContent();
//            }
        }
    }

    private void generateGrid() {
//        StandardAsyncCallback<List<LisaDictColumnBean>> callback = new StandardAsyncCallback<List<LisaDictColumnBean>>() {
//            public void onSuccess(List<LisaDictColumnBean> result) {
//                Integer categoryNodeId = getNodeId();
//                String nodeKindCode = getNodeKindCode();
//                beanProvider = new ChangableBeanProvider(categoryNodeId, nodeKindCode);
////                ClickHandlerCreator clickHandlerCreator = new ClickHandlerCreator();
//                beanDisplay = new LisaDictGridDisplay(result, !nodeKindCode.equals(BIKConstants.NODE_KIND_LISA_DICTIONARY));
//                String[] cols = beanDisplay.getColArray(result);
//                grid = new BeanGrid<Map<String, LisaDictionaryValue>>(beanProvider, beanDisplay, PAGE_SIZE, cols);
//
//                grid.setTableStyleName("lisaDictGrid");
//                grid.setHeaderStyleName("lisaDictHeaderGrid");
//                headerPanel = createPaginatorAndOptionsPanel();
//                headerPanel.setVisible(false);
//                panel.add(headerPanel);
//                panel.add(grid.asWidget());
//            }
//        };
//        if (BIKGWTConstants.NODE_KIND_LISA_DICTIONARY.equals(getNodeKindCode())) {
//            BIKClientSingletons.getLisaService().getDictionaryColumnsForDic(getNodeId(), callback);
//        } else {
//            BIKClientSingletons.getLisaService().getDictionaryColumns(getNodeId(), callback);
//        }
//        panel.clear();
    }

//    private HorizontalPanel createPaginatorAndOptionsPanel() {
//        HorizontalPanel hp = new HorizontalPanel();
//        gridPager = createGridPager();
//        hp.add(gridPager);
//        hp.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
//        hp.add(getOrCreateRefreshButton());
//        hp.add(getOrCreateModifyButton());
//        if (BIKGWTConstants.NODE_KIND_LISA_CATEGORY.equals(getNodeKindCode())) {
//            hp.add(getOrCreateChangeCategoryButton());
//            hp.add(createSpacePanel());
//        } else {
//            hp.add(getOrCreateDeleteButton());
//            hp.add(getOrCreateInsertButton());
//            hp.add(createSpacePanel());
//            hp.add(getOrCreateConfirmInsertButton());
//            hp.add(getOrCreateCancelInsertButton());
//        }
//        hp.add(getOrCreateConfirmModifyButton());
//        hp.add(getOrCreateCancelButton());
//        return hp;
//    }
//    private BeanGridPager createGridPager() {
//        BeanGridPager<Map<String, LisaDictionaryValue>> pager = new BeanGridPager<Map<String, LisaDictionaryValue>>(grid) {
//            @Override
//            public void pageChanged() {
//                super.pageChanged();
//                setGridAndButtonsEnabled(false);
//                setInsertButtonsEnabled(false);
//                beanDisplay.resetCheckedItems();
//                headerPanel.setVisible(true);
//            }
//
//            @Override
//            protected void doSomethingOptBeforeNavigate() {
//                beanDisplay.resetTextBoxes();
//            }
//        };
//        pager.setUknownPageCountMode(true);
//        return pager;
//    }
//    protected Panel createSpacePanel() {
//        Panel tmp = new VerticalPanel();
//        tmp.setWidth("100px");
//        return tmp;
//    }
//    protected Button getOrCreateModifyButton() {
//        if (modifyButton == null) {
//            modifyButton = new Button(I18n.edytuj.get() /* I18N:  */);
//            modifyButton.addClickHandler(new ClickHandler() {
//                public void onClick(ClickEvent event) {
//                    setGridAndButtonsEnabled(true);
//                }
//            });
//            modifyButton.setVisible(true);
//        }
//        return modifyButton;
//    }
//
//    protected Button getOrCreateCancelButton() {
//        if (cancelButton == null) {
//            cancelButton = new Button(I18n.anuluj.get() /* I18N:  */);
//            cancelButton.addClickHandler(new ClickHandler() {
//                public void onClick(ClickEvent event) {
//                    setGridAndButtonsEnabled(false);
//                    beanDisplay.cancelModify();
//                }
//            });
//            cancelButton.setVisible(false);
//        }
//        return cancelButton;
//    }
//
//    protected Button getOrCreateConfirmModifyButton() {
//        if (confirmModifyButton == null) {
//            confirmModifyButton = new Button(I18n.zatwierdzModyfikacje.get() /* I18N:  */);
//            confirmModifyButton.addClickHandler(new ClickHandler() {
//                public void onClick(ClickEvent event) {
//                    setGridAndButtonsEnabled(false);
//                    final Set<Map<String, LisaDictionaryValue>> changedItems = beanDisplay.getChangedItems();
//                    if (changedItems.isEmpty()) {
//                        new SimpleInfoDialog().buildAndShowDialog(I18n.nicNieZmieniono.get() /* I18N:  */, null, null);
//                    } else {
//                        updateRow(changedItems);
//                    }
//                }
//            });
//            confirmModifyButton.setVisible(false);
//        }
//        return confirmModifyButton;
//    }
//
//    protected Button getOrCreateInsertButton() {
//        if (insertButton == null) {
//            insertButton = new Button(I18n.dodaj.get() /* I18N:  */);
//            insertButton.addClickHandler(new ClickHandler() {
//                public void onClick(ClickEvent event) {
//                    Map<String, LisaDictionaryValue> map = new HashMap<String, LisaDictionaryValue>();
//                    grid.insertFirstRow(map);
//                    setInsertButtonsEnabled(true);
//                }
//            });
//        }
//        return insertButton;
//    }
//
//    protected Button getOrCreateConfirmInsertButton() {
//        if (confirmInsertButton == null) {
//            confirmInsertButton = new Button(I18n.dodajPozycje.get() /* I18N:  */);
//            confirmInsertButton.addClickHandler(new ClickHandler() {
//                public void onClick(ClickEvent event) {
//                    setInsertButtonsEnabled(false);
//                    beanDisplay.setNewRowWidgetsDisabled();
//                    insertRow(beanDisplay.getNewRowWidgetsValues());
//                }
//            });
//        }
//        confirmInsertButton.setVisible(false);
//        return confirmInsertButton;
//    }
//
//    protected Button getOrCreateCancelInsertButton() {
//        if (cancelInsertButton == null) {
//            cancelInsertButton = new Button(I18n.anuluj.get() /* I18N:  */);
//            cancelInsertButton.addClickHandler(new ClickHandler() {
//                public void onClick(ClickEvent event) {
//                    grid.removeFirstRow();
//                    setInsertButtonsEnabled(false);
//                }
//            });
//        }
//        cancelInsertButton.setVisible(false);
//        return cancelInsertButton;
//    }
//
//    protected void setInsertButtonsEnabled(boolean isEnabled) {
//        if (!BIKGWTConstants.NODE_KIND_LISA_CATEGORY.equals(getNodeKindCode())) {
//            confirmInsertButton.setVisible(isEnabled);
//            cancelInsertButton.setVisible(isEnabled);
//            insertButton.setVisible(!isEnabled);
//            deleteButton.setVisible(!isEnabled);
//            refreshButton.setVisible(!isEnabled);
//            if (changeCategoryButton != null) {
//                changeCategoryButton.setVisible(!isEnabled);
//            }
//            gridPager.setVisible(!isEnabled);
//
//        }
//        modifyButton.setVisible(!isEnabled);
//    }
//
//    protected void setGridAndButtonsEnabled(boolean isEnabled) {
//        modifyButton.setVisible(!isEnabled);
//        cancelButton.setVisible(isEnabled);
//        confirmModifyButton.setVisible(isEnabled);
//        refreshButton.setVisible(!isEnabled);
//        if (changeCategoryButton != null) {
//            changeCategoryButton.setVisible(!isEnabled);
//        }
//        gridPager.setVisible(!isEnabled);
//        if (insertButton != null) {
//            insertButton.setVisible(!isEnabled);
//        }
//        if (deleteButton != null) {
//            deleteButton.setVisible(!isEnabled);
//        }
//        beanDisplay.setRecordsEnabled(isEnabled);
//    }
//
//    protected Button getOrCreateDeleteButton() {
//        if (deleteButton == null) {
//            deleteButton = new Button(I18n.usun.get() /* I18N:  */);
//            deleteButton.addClickHandler(new ClickHandler() {
//                public void onClick(ClickEvent event) {
//                    int itemsCount = beanDisplay.getCheckedItems().size();
//                    if (itemsCount < 1) {
//                        new SimpleInfoDialog().buildAndShowDialog(I18n.zaznaczPozycjeDoSkasowania.get() /* I18N:  */ + ".", null, null);
//                    } else {
//                        StringBuilder sb = new StringBuilder(I18n.czyNaPewnoUsunac.get());
//                        sb.append(" ").append(itemsCount).append(" ").append(I18n.pozycje.get()).append("?");
//                        new ConfirmDialog().buildAndShowDialog(sb.toString(), new IContinuation() {
//                            public void doIt() {
//                                deleteRows();
//                            }
//                        });
//                    }
//
//                }
//            });
//        }
//        return deleteButton;
//    }
//
//    protected Button getOrCreateRefreshButton() {
//        if (refreshButton == null) {
//            refreshButton = new Button(I18n.odswiez.get() /* I18N:  */);
//            refreshButton.addClickHandler(new ClickHandler() {
//                public void onClick(ClickEvent event) {
//                    reloadGridContent();
//                }
//            });
//        }
//        return refreshButton;
//    }
//    private Button getOrCreateChangeCategoryButton() {
//        if (changeCategoryButton == null) {
//            changeCategoryButton = new Button(I18n.zmienKategorie.get() /* I18N:  */);
//            changeCategoryButton.addClickHandler(new ClickHandler() {
//                public void onClick(ClickEvent event) {
//                    final int itemsCount = beanDisplay.getCheckedItems().size();
//                    if (itemsCount < 1) {
//                        new SimpleInfoDialog().buildAndShowDialog(I18n.zaznaczPozycjeDoPrzeniesienia.get() /* I18N:  */ + ".", null, null);
//                    } else {
//                        final TreeNodeBean node = getData().node;
//                        BIKClientSingletons.getLisaService().getCategoryzationNodeId(node.parentNodeId, new StandardAsyncCallback<Integer>("Error in getCategoryzationNodeId") {
//
//                            @Override
//                            public void onSuccess(Integer rootNodeId) {
//                                ChangeCategoryDialog dialog = new ChangeCategoryDialog(rootNodeId);
//                                dialog.itemsToMoveCount = itemsCount;
//                                dialog.currentCategoryName = node.name;
//                                dialog.saveCont = new IParametrizedContinuation<Integer>() {
//                                    public void doIt(final Integer newCategoryNodeId) {
//                                        new LisaAuthorizedRequest<Void>() {
//                                            @Override
//                                            protected void performServerAction(AsyncCallback<Void> callback) {
//                                                BIKClientSingletons.getLisaService().moveValuesToCategory(beanDisplay.getCheckedItems(), getNodeId(), newCategoryNodeId, callback);
//                                            }
//
//                                            @Override
//                                            public void onActionSuccess(Void result) {
//                                                BIKClientSingletons.showInfo(I18n.pozycjeZostalyPrzeniesDoNowejKat.get() /* I18N:  */);
//                                                grid.beansChanged();
//                                            }
//                                        }.go();
//                                    }
//                                };
//                                dialog.buildAndShowDialog();
//                            }
//                        });
//                    }
//                }
//            });
//        }
//        return changeCategoryButton;
//    }
    public Widget buildWidgets() {
        panel = new FlowPanel();
        return panel;
    }

    @Override
    public boolean hasContent() {
        return BIKGWTConstants.NODE_KIND_LISA_CATEGORY.equals(getNodeKindCode()) || (BIKGWTConstants.NODE_KIND_LISA_DICTIONARY.equals(getNodeKindCode()) && !hasChildren());
    }

//    private boolean setOfColumnsShouldBeReloaded() {
//        TreeNodeBean treeNode = getData().node;
//        String[] ids = treeNode.branchIds.split("\\|");
//        if (ids.length < DEPTH_OF_DICT_IN_BIK_NODE) {
//            previousDictId = null;
//            return true;
//        }
//        Integer newDictId = Integer.parseInt(ids[DEPTH_OF_DICT_IN_BIK_NODE - 1]);
//        if (newDictId != null && newDictId.equals(previousDictId)) {
//            return false;
//        }
//        previousDictId = newDictId;
//        return true;
//    }
//    private void reloadGridContent() {
//        beanProvider.setCategoryId(getNodeId());
//        beanProvider.setNodeKindCode(getNodeKindCode());
//        grid.setPageNum(0);
//    }
//    protected void updateRow(final Set<Map<String, LisaDictionaryValue>> changedItems) {
//        new LisaAuthorizedRequest<Void>() {
//            @Override
//            protected void performServerAction(AsyncCallback<Void> callback) {
//                TreeNodeBean node = getData().node;
//                BIKClientSingletons.getLisaService().updateDictionaryContents(changedItems, node.id, node.nodeKindCode.equals(BIKConstants.NODE_KIND_LISA_DICTIONARY), callback);
//            }
//
//            @Override
//            public void onActionSuccess(Void result) {
////                grid.beansChanged(); // nie odswiezamy
//                BIKClientSingletons.showInfo(I18n.pozycjaZostalaZmieniona.get() /* i18n:  */);
//            }
//        }.go();
//    }
//    protected void deleteRows() {
//        new LisaAuthorizedRequest<Void>() {
//            @Override
//            protected void performServerAction(AsyncCallback<Void> callback) {
//                TreeNodeBean node = getData().node;
//                BIKClientSingletons.getLisaService().deleteDictionaryContent(beanDisplay.getCheckedItems(), node.id, node.nodeKindCode.equals(BIKConstants.NODE_KIND_LISA_DICTIONARY), callback);
//            }
//
//            @Override
//            public void onActionSuccess(Void result) {
//                BIKClientSingletons.showInfo(I18n.pozycjeZostalyUsuniete.get() /* I18N:  */);
//                grid.beansChanged();
//            }
//        }.go();
//    }
//    protected void insertRow(final Map<String, LisaDictionaryValue> recordToAdd) {
//        new LisaAuthorizedRequest<Void>() {
//            @Override
//            protected void performServerAction(AsyncCallback<Void> callback) {
//                TreeNodeBean node = getData().node;
//                BIKClientSingletons.getLisaService().insertDictionaryContent(recordToAdd, node.id, node.nodeKindCode.equals(BIKConstants.NODE_KIND_LISA_DICTIONARY), callback);
//            }
//
//            @Override
//            public void onActionSuccess(Void result) {
//                grid.beansChanged();
//                BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get() /* i18n:  */);
//            }
//
//            @Override
//            public void doSomethingOptOnFailure() {
//                Map<String, LisaDictionaryValue> news = beanDisplay.getNewRowWidgetsValues();
//                if (!news.isEmpty()) {
//                    beanDisplay.clearNewWidgetMap();
//                    grid.removeFirstRow();
//                }
//                BIKClientSingletons.showWarning(I18n.bladPrzyDodawaniuWiersza.get() /* i18n:  */);
//            }
//        }.go();
//    }
}
