/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.TreeSelectorAttributeOrKindObjectToConfigPrintTemplateDialog;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class NodaActionAddPrintAttribut extends NodeActionBase {

    @Override

    public boolean isEnabledByDesign() {
        if (!BIKClientSingletons.isEnablePrinting()) {
            return false;
        }
        return isMetaBiks() && (BIKConstants.NODE_KIND_META_PRINT.equalsIgnoreCase(node.nodeKindCode)
                || BIKConstants.NODE_KIND_META_PRINT_KIND.equalsIgnoreCase(node.nodeKindCode)
                || BIKConstants.NODE_KIND_META_PRINT_BUILT_IN_KIND.equalsIgnoreCase(node.nodeKindCode));
    }

    @Override
    public String getCaption() {
        return BIKConstants.NODE_KIND_META_PRINT.equalsIgnoreCase(node.nodeKindCode) ? I18n.dodajAtrybutDoSzablonyWydruku.get() : I18n.dodajAtrybutZagniezdzony.get();
    }

    @Override
    public void execute() {
        if (BIKConstants.NODE_KIND_META_PRINT.equalsIgnoreCase(node.nodeKindCode)) {
            String nodeIdOb = node.branchIds.replace(node.id + "|", "").replace(node.parentNodeId + "|", "");
            Integer nodeIdObjectKind = Integer.parseInt(nodeIdOb.substring(nodeIdOb.indexOf("|") + 1, nodeIdOb.lastIndexOf("|")));

            new TreeSelectorAttributeOrKindObjectToConfigPrintTemplateDialog(nodeIdObjectKind, BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND).buildAndShowDialog(node.id, node.name,
                    new IParametrizedContinuation<TreeNodeBean>() {
                @Override
                public void doIt(TreeNodeBean param) {

                    BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get() /* I18N:  */);
                    refreshRedisplayDataAndSelectRecord(param.id);

                }
            },
                    null);
        } else {
//            Window.alert(node.nodeKindCode);
            new TreeSelectorAttributeOrKindObjectToConfigPrintTemplateDialog(node.name, BIKConstants.NODE_KIND_META_NODE_KIND,
                    true).buildAndShowDialog(node.id, node.name, new IParametrizedContinuation<TreeNodeBean>() {
                @Override
                public void doIt(TreeNodeBean param) {
                    BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get() /* I18N:  */);
                    refreshRedisplayDataAndSelectRecord(param.id);
                }
            }, null);

        }
    }

}
