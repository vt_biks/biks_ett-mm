﻿-- użytkownik aplikacyjny (login/hasło do serwera)
if not exists(select * from sys.syslogins where loginname='biks_app')
begin
create login biks_app with password = 'app' -- to należy wpisać w pliku konfiguracyjnym BIKSa: bikcenter-config.cfg
end
go

-- użytkownik do zasileń (login/hasło do serwera)
if not exists(select * from sys.syslogins where loginname='biks_load')
begin
create login biks_load with password = 'load'
end
go

-- użytkownik do raportowania (login/hasło do serwera)
if not exists(select * from sys.syslogins where loginname='biks_rep')
begin
create login biks_rep with password = 'rep'
end
go


alter user biks_app with login = biks_app
go 

alter user biks_load with login = biks_load
go

alter user biks_rep with login = biks_rep
go