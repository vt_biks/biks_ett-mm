﻿exec sp_check_version '1.0.99.5';
go

insert into bik_attr_hint(name,hint) values ('Long name','');
insert into bik_attr_hint(name,hint) values ('Benchmark definition','');
insert into bik_attr_hint(name,hint) values ('Error Threshold','');
insert into bik_attr_hint(name,hint) values ('Expected Result','');
insert into bik_attr_hint(name,hint) values ('Sampling Method','');
insert into bik_attr_hint(name,hint) values ('Additional Information','');
insert into bik_attr_hint(name,hint) values ('Create Date','');
insert into bik_attr_hint(name,hint) values ('Modify Date','');
insert into bik_attr_hint(name,hint) values ('Suspend Date','');



exec sp_update_version '1.0.99.5', '1.0.99.6';
go