/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.sandbox;

import com.google.gwt.user.client.Window;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public class LittleDogPlayground {

    private String lastMsg;

    public void hałhał(String msg) {
        lastMsg = msg;
        Window.alert("LittleDogs " + "says" /* I18N:  */ + ": " + msg);
    }

    public String getLastMsg() {
        return lastMsg;
    }
}
