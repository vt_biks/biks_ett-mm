/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.syntaxhighlighter;

import com.google.gwt.core.client.JavaScriptObject;

/**
 *
 * @author tflorczak
 */
public class BrushFactory {

    public static native JavaScriptObject newJavaBrush() /*-{
     return new $wnd.SyntaxHighlighter.brushes.Java();
     }-*/;

    public static native JavaScriptObject newSqlBrush() /*-{
     return new $wnd.SyntaxHighlighter.brushes.Sql();
     }-*/;

    public static native JavaScriptObject newXmlBrush() /*-{
     return new $wnd.SyntaxHighlighter.brushes.Xml();
     }-*/;
}
