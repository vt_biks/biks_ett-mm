/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class LinkedHashMapEx<K, V> extends LinkedHashMap<K, V> {

    public LinkedHashMapEx() {
        super();
    }

    public LinkedHashMapEx<K, V> set(K key, V val) {
        super.put(key, val);
        return this;
    }

    public LinkedHashMapEx<K, V> set(Map<? extends K, ? extends V> m) {
        if (m != null) {
            super.putAll(m);
        }
        return this;
    }
}
