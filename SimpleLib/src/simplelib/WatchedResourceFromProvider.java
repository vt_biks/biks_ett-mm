/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class WatchedResourceFromProvider<T> implements IWatchedResourceFromProvider<T> {

    private static final ILameLogger logger = LameLoggerFactory.getLogger(WatchedResourceFromProvider.class);
    private final static long TIMESTAMP_CHECK_INTERVAL_MILLIS = 1000;
    private T cachedVal;
    private IResourceFromProvider<T> resource;
    private long lastMod = -1;
    private long lastCheck = -1;
    private boolean cacheExpired = false;

    public WatchedResourceFromProvider(IResourceFromProvider<T> resource) {
        this.resource = resource;
    }

    public boolean hasBeenModifiedRecently() {
        if (logger.isDebugEnabled()) {
            logger.debug("hasBeenModifiedRecently: cacheExpired=" + cacheExpired);
        }

        boolean val = cacheExpired || updateLastModIfNeeded();
        return val;
    }

    public T gainResource() {
        if (cacheExpired || hasBeenModifiedRecently()) {
            if (logger.isInfoEnabled()) {
                logger.info("gainResource: regain resource, resource class=" + BaseUtils.safeGetClassName(resource));
            }
            cachedVal = resource.gainResource();
            cacheExpired = false;
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("gainResource: use cached resource");
            }
        }
        return cachedVal;
    }

    protected boolean updateLastModIfNeeded() {
        long currSysMillis = System.currentTimeMillis();

        long prevLastMod = lastMod;

        boolean mustCheck = (lastCheck == -1 || lastCheck + TIMESTAMP_CHECK_INTERVAL_MILLIS < currSysMillis);

        if (mustCheck) {
            lastCheck = currSysMillis;
            lastMod = resource.getLastModifiedOfResource();
            if (logger.isDebugEnabled()) {
                logger.debug("updateLastModIfNeeded: mustCheck=" + mustCheck + ", (prev)cacheExpired=" + cacheExpired
                        + ", lastMod=" + lastMod + ", prevLastMod=" + prevLastMod);
            }
        }
        cacheExpired = cacheExpired || lastMod != prevLastMod;

        if (logger.isDebugEnabled()) {
            logger.debug("updateLastModIfNeeded: mustCheck=" + mustCheck + ", cacheExpired=" + cacheExpired);
        }

        return cacheExpired;
    }

    public long getLastModifiedOfResource() {
        updateLastModIfNeeded();
        return lastMod;
    }
}
