/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.RadioButton;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.BikTreeWidget;
import pl.fovis.client.bikpages.IPageDataFetchBroker;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase;
import static pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase.ID_FIELD_NAME;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author bfechner
 */
public class ApplyForExtendedAccessTreeAndRootTreeWidget extends BikTreeWidget {

    protected final Integer NODE_ACTION_ID_SHOW_IN_TREE = BIKClientSingletons.getNodeActionIdByCode(BIKConstants.NODE_ACTION_CODE_SHOW_IN_TREE);
    protected IContinuation setProperMailToPartCont;

    protected final List<CheckBox> treeCheckboxes = new ArrayList<CheckBox>();
    protected final List<RadioButton> roAccessRbs = new ArrayList<RadioButton>();
    protected final List<RadioButton> rwAccessRbs = new ArrayList<RadioButton>();
    protected final List<TreeNodeBean> menuNodeBeans = new ArrayList<TreeNodeBean>();
    protected final Map<Integer, List<CheckBox>> childrenCheckboxMap = new HashMap<Integer, List<CheckBox>>();
    protected int idx = 0;

    public ApplyForExtendedAccessTreeAndRootTreeWidget(boolean noLinkedNodes, IPageDataFetchBroker dataFetchBroker, boolean isWidgetRunFromDialog, IContinuation setProperMailToPartCont, String... fields) {
        super(noLinkedNodes, dataFetchBroker, isWidgetRunFromDialog, fields);

        this.setProperMailToPartCont = setProperMailToPartCont;
    }

    public ApplyForExtendedAccessTreeAndRootTreeWidget(Integer treeGridHeight,
            String firstColStyleName, IPageDataFetchBroker dataFetchBroker, IContinuation setProperMailToPartCont,
            String... fields) {
        super(treeGridHeight, firstColStyleName, dataFetchBroker, fields);
        this.setProperMailToPartCont = setProperMailToPartCont;

    }

    protected MapTreeRowAndBeanStorageBase<Integer, TreeNodeBean> createStorage() {
        return new MapTreeRowAndBeanStorageBase<Integer, TreeNodeBean>(MapTreeRowAndBeanStorageBase.ID_FIELD_NAME, MapTreeRowAndBeanStorageBase.PARENT_ID_FIELD_NAME) {

            @Override
            protected Map<String, Object> convertBeanToRow(final TreeNodeBean bean) {

                final Integer treeId = bean.treeId;
                final boolean needsReadOnlyAccessInTreeRoot = !BIKClientSingletons.isNodeActionGrantedInTreeRoot(Math.abs(treeId), NODE_ACTION_ID_SHOW_IN_TREE);
                final TreeBean tb = BIKClientSingletons.getTreeBeanById(Math.abs(treeId));
                final boolean needsModifyAccessInTreeRoot = tb != null && tb.nodeKindId != null;

                Map<String, Object> row = new HashMap<String, Object>();
//                final RadioButton rbRo = new RadioButton("rb" + idx, I18n.tylkoOdczyt.get());
//                final RadioButton rbRw = new RadioButton("rb" + idx, I18n.modyfikacjaDanych.get());
//                rbRo.setVisible(false);
//                rbRw.setVisible(false);
                final CheckBox cb = new CheckBox(bean.name);

                if (bean.parentTreeId == null) {
                    cb.setStyleName("gwt-InlineHTML treeLbl");

                }
                Integer parentNodeId = bean.parentNodeId;
                if (parentNodeId != null) {
                    if (!childrenCheckboxMap.containsKey(bean.parentNodeId)) {
                        childrenCheckboxMap.put(parentNodeId, new ArrayList<CheckBox>());
                    }
                    childrenCheckboxMap.get(parentNodeId).add(cb);
                }
                cb.addClickHandler(new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
//                        rbRo.setVisible(needsReadOnlyAccessInTreeRoot && cb.getValue());
//                        rbRw.setVisible(needsModifyAccessInTreeRoot && cb.getValue());

                        List<CheckBox> cbs = childrenCheckboxMap.get(bean.id);
                        if (!BaseUtils.isCollectionEmpty(cbs)) {
                            for (CheckBox cbChild : cbs) {
                                int chIdx = treeCheckboxes.indexOf(cb);

                                boolean isRwAccessRbSelect = rwAccessRbs.get(chIdx).getValue();
                                boolean isRoAccessRbsSelect = roAccessRbs.get(chIdx).getValue();

                                if (!cb.getValue()) {
                                    cbChild.setEnabled(true);
                                } else if (isRwAccessRbSelect) {
                                    cbChild.setEnabled(!cb.getValue());
                                } else if (isRoAccessRbsSelect && !needsModifyAccessInTreeRoot) {
                                    setChildrenCbxs(cbChild, needsModifyAccessInTreeRoot);
                                }
                            }
                        }

                        setProperMailToPartCont.doIt();
                    }
                });
                row.put(ID_FIELD_NAME, bean.id);
                row.put(PARENT_ID_FIELD_NAME, bean.parentNodeId);

                if (treeId == -1) {
                    final InlineLabel lbl = new InlineLabel(bean.name);
                    lbl.setStyleName("gwt-InlineHTML treeLbl");
                    row.put(CHECKBOX, lbl);

                } else {
                    row.put(CHECKBOX, cb);
                }

                ClickHandler ch = new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        List<CheckBox> cbs = childrenCheckboxMap.get(bean.id);
                        if (!BaseUtils.isCollectionEmpty(cbs)) {
                            for (CheckBox cbChild : cbs) {
                                int chIdx = treeCheckboxes.indexOf(cb);
                                boolean isRwAccessRbSelect = rwAccessRbs.get(chIdx).getValue();
                                boolean isRoAccessRbsSelect = roAccessRbs.get(chIdx).getValue();

                                if (isRwAccessRbSelect) {
                                    cbChild.setEnabled(!cb.getValue());
                                    setChildrenCbxs(cbChild, !cb.getValue());
                                }
                                if (isRoAccessRbsSelect) {
                                    cbChild.setEnabled(true);
                                }
                            }
                        }

                        setProperMailToPartCont.doIt();
                    }
                };

//                rbRo.addClickHandler(ch);
//                rbRw.addClickHandler(ch);
//                row.put(RADIOBUTTONO, rbRo);
//                row.put(RADIOBUTTONW, rbRw);

                if (needsReadOnlyAccessInTreeRoot) {
//                    rbRo.setValue(true);
                } else {
//                    rbRw.setValue(true);
                }

                treeCheckboxes.add(cb);
//                roAccessRbs.add(rbRo);
//                rwAccessRbs.add(rbRw);
                menuNodeBeans.add(bean);
                idx++;
                return row;
            }
        };
    }

    protected void setChildrenCbxs(CheckBox cbChild, boolean enabled) {
        boolean isVisible = false;
        cbChild.setEnabled(enabled);
        cbChild.setValue(isVisible);
        roAccessRbs.get(treeCheckboxes.indexOf(cbChild)).setVisible(isVisible);
        rwAccessRbs.get(treeCheckboxes.indexOf(cbChild)).setVisible(isVisible);
    }

    @Override
    protected void populateTreeGridWithData(Collection<TreeNodeBean> items,
            boolean useLazyLoading, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode) {
        super.populateTreeGridWithData(items, false, optExtraMode, optFilteringNodeActionCode);
    }

    public int getOptionCount() {
        return treeCheckboxes.size();
    }

    public boolean isOptionSelected(int idx) {
        return treeCheckboxes.get(idx).getValue();
    }

    public String getOptionName(int idx) {
        List<String> revPathNames = new ArrayList<String>();

        TreeNodeBean mnb = getOptionMenuNodeBean(idx);

        while (mnb != null) {
            revPathNames.add(mnb.name);
            mnb = storage.getBeanById(mnb.parentNodeId);
        }

        Collections.reverse(revPathNames);

        return BaseUtils.mergeWithSepEx(revPathNames, BIKConstants.RAQUO_STR_SPACED);

    }

    public TreeNodeBean getOptionMenuNodeBean(int idx) {
        return menuNodeBeans.get(idx);
    }

    public boolean isReadOnlyAccessForOption(int idx) {
        return roAccessRbs.get(idx).getValue();
    }
}
