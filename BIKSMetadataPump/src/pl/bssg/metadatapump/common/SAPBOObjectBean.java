/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import java.util.Date;
import java.util.List;
import simplelib.BeanWithIntegerIdBase;

/**
 *
 * @author tflorczak
 */
public class SAPBOObjectBean extends BeanWithIntegerIdBase implements Comparable<SAPBOObjectBean> {

    public Integer parentId;
    public String name;
    public String type; // Webi, Folder, Universe
    public String location;
    public String owner;
    public Date created;
    public Date modify;
    public String description;
    public Integer children;
    public String cuid;
    public String guid;
    public String ruid;
    // adds
    public String icon;
    // filter
    public int manageType; // 1- reports, 2- univ, 3-conn 4-usergroups
    public List<String> kindsToFilter;

    public Integer getParentId() {
        return parentId;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getLocation() {
        return location;
    }

    public String getOwner() {
        return owner;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModify() {
        return modify;
    }

    public String getDescription() {
        return description;
    }

    public Integer getChildren() {
        return children;
    }

    public String getCuid() {
        return cuid;
    }

    public String getGuid() {
        return guid;
    }

    public String getRuid() {
        return ruid;
    }

    public Integer getId() {
        return id;
    }

    public SAPBOObjectBean() {
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SAPBOObjectBean other = (SAPBOObjectBean) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(SAPBOObjectBean o) {
        return (o == null || o.id == null) ? -1 : -o.id.compareTo(id);
    }

    @Override
    public String toString() {
        return "SAPBOObjectBean{" + "parentId=" + parentId + ", name=" + name + ", type=" + type + ", location=" + location + ", owner=" + owner + ", created=" + created + ", modify=" + modify + ", description=" + description + ", children=" + children + ", cuid=" + cuid + ", guid=" + guid + ", ruid=" + ruid + ", icon=" + icon + ", manageType=" + manageType + ", kindsToFilter=" + kindsToFilter + '}';
    }
}
