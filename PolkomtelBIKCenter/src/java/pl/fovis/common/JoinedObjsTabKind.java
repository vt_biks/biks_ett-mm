/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

/**
 *
 * @author pmielanczuk
 */
public enum JoinedObjsTabKind {
    Metadata, Taxonomy, Dqc, Confluence, Glossary, Dictionary

}
