exec sp_check_version '1.0.90';
go

create table bik_attr_hint(
id int not null identity(1,1) primary key,
name varchar(350) not null,
hint varchar(max) not null,
);
go

--------------------------------------wbudowane------------------------
insert into bik_attr_hint(name,hint) values ('Server','');
insert into bik_attr_hint(name,hint) values ('User','');
insert into bik_attr_hint(name,hint) values ('Password','');
insert into bik_attr_hint(name,hint) values ('Source','');
insert into bik_attr_hint(name,hint) values ('Network layer','');
insert into bik_attr_hint(name,hint) values ('Type','');
insert into bik_attr_hint(name,hint) values ('Client number','');
insert into bik_attr_hint(name,hint) values ('Database engine','');
insert into bik_attr_hint(name,hint) values ('Language','');
insert into bik_attr_hint(name,hint) values ('System number','');
insert into bik_attr_hint(name,hint) values ('System id','');
insert into bik_attr_hint(name,hint) values ('Ostatnia modyfikacja','');
insert into bik_attr_hint(name,hint) values ('ID, CUID','');
insert into bik_attr_hint(name,hint) values ('Nazwa pliku','');
insert into bik_attr_hint(name,hint) values ('Data ostatniego uruchomienia','');
insert into bik_attr_hint(name,hint) values ('Typ','');
insert into bik_attr_hint(name,hint) values ('Select','');
insert into bik_attr_hint(name,hint) values ('Where','');
insert into bik_attr_hint(name,hint) values ('Agregate','');
insert into bik_attr_hint(name,hint) values ('Tabele','');
insert into bik_attr_hint(name,hint) values ('Id','');
insert into bik_attr_hint(name,hint) values ('Filtr','');
insert into bik_attr_hint(name,hint) values ('Guid','');
insert into bik_attr_hint(name,hint) values ('Ruid','');
insert into bik_attr_hint(name,hint) values ('Progid','');
insert into bik_attr_hint(name,hint) values ('Liczba obiekt�w podrz�dnych (children)','');
insert into bik_attr_hint(name,hint) values ('Rozmiar w bajtach','');
insert into bik_attr_hint(name,hint) values ('Size','');
insert into bik_attr_hint(name,hint) values ('HasChildren','');
insert into bik_attr_hint(name,hint) values ('Instance','');
insert into bik_attr_hint(name,hint) values ('Owner_id','');
insert into bik_attr_hint(name,hint) values ('Obtype','');
insert into bik_attr_hint(name,hint) values ('Flags','');
insert into bik_attr_hint(name,hint) values ('RunnableObject','');
insert into bik_attr_hint(name,hint) values ('ContentLocale','');
insert into bik_attr_hint(name,hint) values ('IsSchedulable','');
insert into bik_attr_hint(name,hint) values ('Spos�b od�wie�ania raportu','');
insert into bik_attr_hint(name,hint) values ('Parametry wprowadzane przez u�ytkownika w momencie od�wie�ania raportu','');
insert into bik_attr_hint(name,hint) values ('ReadOnly','');
insert into bik_attr_hint(name,hint) values ('LastSuccessfulInstanceId','');
insert into bik_attr_hint(name,hint) values ('Timestamp','');
insert into bik_attr_hint(name,hint) values ('ProgidMachine','');
insert into bik_attr_hint(name,hint) values ('Endtime','');
insert into bik_attr_hint(name,hint) values ('Starttime','');
insert into bik_attr_hint(name,hint) values ('ScheduleStatus','');
insert into bik_attr_hint(name,hint) values ('Recurring','');
insert into bik_attr_hint(name,hint) values ('Nextruntime','');
insert into bik_attr_hint(name,hint) values ('Doc Sender','');
insert into bik_attr_hint(name,hint) values ('Poprawka','');
insert into bik_attr_hint(name,hint) values ('Application Object','');
insert into bik_attr_hint(name,hint) values ('Nazwa skr�cona','');
insert into bik_attr_hint(name,hint) values ('Statystyki','');
insert into bik_attr_hint(name,hint) values ('Utworzono','');
insert into bik_attr_hint(name,hint) values ('Zmodyfikowany','');

exec sp_update_version '1.0.90', '1.0.90.1';
go