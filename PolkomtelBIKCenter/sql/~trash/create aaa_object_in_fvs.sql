CREATE TABLE aaa_object_in_fvs (
	id int IDENTITY(1,1) NOT NULL primary key,
	user_id int not null,
	tab_id varchar(100) not null,
	obj_id varchar(100) not null,
	caption varchar(300) not null,
	kind varchar(100) not null,
	date_added datetime,
	constraint uk_aaa_object_in_fvs unique (user_id, obj_id)
);

-- ALTER TABLE aaa_object_in_fvs add date_added datetime;

GO

alter table aaa_object_in_fvs add date_added datetime

/*
 userId;
    public String tabId;
    public String objId;
    public String caption;
    public String kind;
    
 */