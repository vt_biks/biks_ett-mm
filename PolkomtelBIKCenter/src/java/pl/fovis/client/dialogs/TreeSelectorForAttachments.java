/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PushButton;
import java.util.Collection;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.FoxyFileUpload;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author mgraczkowski
 */
public class TreeSelectorForAttachments extends TreeSelectorDialog/*TreeSelectorDialogBase*/ {

    protected PushButton uploadBtn;

    @Override
    protected void addAdditionalButtons(HorizontalPanel hp) {
        uploadBtn = NewLookUtils.newCustomPushButton(I18n.pobierzNowy.get() /* I18N:  */, new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        addNewDocument();
                    }
                });
        hp.add(uploadBtn);
        uploadBtn.setVisible(/*BIKClientSingletons.isEffectiveExpertLoggedIn() */
                BIKClientSingletons.isEffectiveExpertLoggedInEx(BIKConstants.ACTION_ADD_DOCUMENT)/*ROLA Jak w dodaj dokument */
                && BIKClientSingletons.isDocumentTreeVisible());
    }

    private void addNewDocument() {
        final FileUploadDialogForDocuments fud = new FileUploadDialogForDocuments(BIKClientSingletons.getUploadableChildrenNodeKinds(BIKConstants.NODE_KIND_DOCUMENT));
        fud.buildAndShowDialog(new IParametrizedContinuation<Pair<String, FoxyFileUpload>>() {
            @Override
            public void doIt(Pair<String, FoxyFileUpload> param) {
                getService().addBikDocumentAdHoc(
                        param.v2.getClientFileName(),
                        param.v2.getServerFileName(),
                        param.v1,
                        new StandardAsyncCallback<Integer>("Failure on" /* I18N: no */ + " addNewDocument") {
                            @Override
                            public void onSuccess(Integer result) {
                                BIKClientSingletons.showInfo(I18n.dodanoDokument.get() /* I18N:  */);
                                fetchData(result);
                                //bikTree.selectEntityById(result/*, false*/);
                                BIKClientSingletons.invalidateTabData(BIKGWTConstants.TREE_CODE_DOCUMENTS);
                                //fud.hideDialog();//dialog.hide();
                            }
                        });
            }
        });
    }

    // tf: zakomentowuje, z powodu rezygnacji z lookupBtn
//    @Override
//    protected void configureButtons() {
//        super.configureButtons();
//        if (selectedNode != null && BIKClientSingletons.isDocumentNodeKind(selectedNode.nodeKindCode)) {
//            getService().getBikDocumentForSiId(
//                    selectedNode.id, new AsyncCallback<AttachmentBean>() {
//                        public void onSuccess(AttachmentBean att) {
//                            lookupBtn.setHTML(
//                                    BIKClientSingletons.createLink(I18n.podglad.get() /* I18N:  */, att.href) //                            "<a href='"
//                            //                            + FoxyFileUpload.getWebAppUrlPrefixForUploadedFile("fsb?get=")
//                            //                            + att.href + "' target='_blank'>Podgląd</a>"
//                            );
//                            lookupBtn.setEnabled(true);
//                        }
//
//                        @Override
//                        public void onFailure(Throwable th) {
//                            BIKClientSingletons.showWarning(I18n.nieZnalezionoDokumentu.get() /* I18N:  */);
//                        }
//                    });
//            fpHelp.setStyleName("attViewBtn");
//            GWTUtils.removeElementAttribute(lookupBtn.getElement(), "disabled" /* I18N: no */);
//        } else {
//            // lookupBtn.setText("Podgląd");
//            lookupBtn.setEnabled(false);
//            GWTUtils.setElementPropertyBoolean(lookupBtn.getElement(), "disabled" /* I18N: no */, true);
//            lookupBtn.setHTML(I18n.podglad.get() /* I18N:  */);
////            actionBtn.setEnabled(false);
//            // DOM.removeElementAttribute(actionBtn.getElement(), "disabled");
//            fpHelp.addStyleName("fpHelp-disabled");
//        }
//    }
    @Override
    protected String getDialogCaption() {
        return I18n.wybierzZalacznik.get() /* I18N:  */;
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        if (useLazyLoading) {
            getService().getBikDocumentRootTreeNodes(callback);
        } else {
            getService().getBikDocumentTreeNodes(callback);
        }
    }

    @Override
    protected String getMessageForUnselectableNode(TreeNodeBean tnb) {
        return null;
    }

    @Override
    protected boolean isInheritToDescendantsCheckBoxVisible() {
        return true;
    }

    @Override
    protected Collection<Integer> getTreeIdsForFiltering() {
        return BIKClientSingletons.getProperTreeIdsWithTreeKind(BIKGWTConstants.TREE_KIND_DOCUMENTS);
        //return " tree_id in (select id from bik_tree where tree_kind in ( '" + BIKGWTConstants.TREE_CODE_DOCUMENTS + "'))";
    }
}
