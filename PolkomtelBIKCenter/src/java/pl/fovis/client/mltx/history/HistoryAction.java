/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.mltx.history;

import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author pku
 */
public interface HistoryAction {

    public Widget performAction(String id);
}
