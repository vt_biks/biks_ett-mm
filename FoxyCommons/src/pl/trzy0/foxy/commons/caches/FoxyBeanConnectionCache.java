/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons.caches;

import pl.trzy0.foxy.commons.IBeanConnectionEx;
import simplelib.INamedPropsBean;

/**
 *
 * @author wezyr
 */
public class FoxyBeanConnectionCache<K, V extends INamedPropsBean> extends FoxyObjectCache<K, V> {

    public FoxyBeanConnectionCache(IBeanConnectionEx<? super V> conn,
            Class<V> beanClass, String tableName, String primaryKeyName,
            String altKeyNames) {
        super(new FoxyBeanSupplier<K, V>(conn, beanClass, tableName, primaryKeyName, altKeyNames));
    }
}
