﻿exec sp_check_version '1.8.0.25';
go

alter table dbo.bik_teradata alter column type varchar(max)
alter table dbo.bik_teradata alter column name varchar(max)
alter table dbo.bik_teradata alter column branch_names varchar(max)
alter table dbo.bik_teradata alter column type varchar(max)
go

if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model]') and name = N'idx_bik_teradata_data_model_name')
	drop index [idx_bik_teradata_data_model_name] on [dbo].[bik_teradata_data_model]

if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model]') and name = N'idx_bik_teradata_data_model_database_name')
	drop index [idx_bik_teradata_data_model_database_name] on [dbo].[bik_teradata_data_model]
	
if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model]') and name = N'idx_bik_teradata_data_model_table_name')
	drop index [idx_bik_teradata_data_model_table_name] on [dbo].[bik_teradata_data_model]

if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model]') and name = N'idx_bik_teradata_data_model_branch_names')
	drop index [idx_bik_teradata_data_model_branch_names] on [dbo].[bik_teradata_data_model]
go

alter table dbo.bik_teradata_data_model alter column type varchar(max)
alter table dbo.bik_teradata_data_model alter column name varchar(450)
alter table dbo.bik_teradata_data_model alter column branch_names varchar(450)
alter table dbo.bik_teradata_data_model alter column database_name varchar(450)
alter table dbo.bik_teradata_data_model alter column table_name varchar(450)
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model]') and name = N'idx_bik_teradata_data_model_name')
	create nonclustered index [idx_bik_teradata_data_model_name] on [dbo].[bik_teradata_data_model]
	(
		name asc
	)

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model]') and name = N'idx_bik_teradata_data_model_database_name')
	create nonclustered index [idx_bik_teradata_data_model_database_name] on [dbo].[bik_teradata_data_model]
	(
		database_name asc
	)

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model]') and name = N'idx_bik_teradata_data_model_table_name')
	create nonclustered index [idx_bik_teradata_data_model_table_name] on [dbo].[bik_teradata_data_model]
	(
		table_name asc
	)

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model]') and name = N'idx_bik_teradata_data_model_branch_names')
	create nonclustered index [idx_bik_teradata_data_model_branch_names] on [dbo].[bik_teradata_data_model]
	(
		branch_names asc
	)
go

alter table dbo.bik_teradata_data_model_index alter column type varchar(max)
alter table dbo.bik_teradata_data_model_index alter column unique_flag varchar(max)
go

if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model_process_object_rel]') and name = N'idx_tdmpor_bik_node_obj_id')
	drop index [idx_tdmpor_bik_node_obj_id] on [dbo].[bik_teradata_data_model_process_object_rel]
go

alter table dbo.bik_teradata_data_model_process_object_rel alter column symb_proc varchar(max)
alter table dbo.bik_teradata_data_model_process_object_rel alter column bik_node_obj_id varchar(450)
alter table dbo.bik_teradata_data_model_process_object_rel alter column area varchar(max)
alter table dbo.bik_teradata_data_model_process_object_rel alter column database_name varchar(max)
if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_teradata_data_model_process_object_rel]') and name = N'idx_tdmpor_bik_node_obj_id')
	create nonclustered index [idx_tdmpor_bik_node_obj_id] on [dbo].[bik_teradata_data_model_process_object_rel]
	(
		[bik_node_obj_id] asc
	)
go
	
alter table dbo.bik_teradata_index alter column type varchar(max)
alter table dbo.bik_teradata_index alter column unique_flag varchar(max)
go

exec sp_update_version '1.8.0.25', '1.8.0.26';
go
