/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class AutomaticUpdateJoinedObjsForNodeDialog extends BaseActionOrCancelDialog {

    protected IParametrizedContinuation<Integer> saveCont;

    private Label lblTitle;
    private TextBox tbNodeId;

    public void buildAndShowDialog(IParametrizedContinuation<Integer> saveCont) {
        this.saveCont = saveCont;

        super.buildAndShowDialog();

    }

    @Override
    protected String getDialogCaption() {
        return I18n.dodajPowiazanie.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {
        Grid grid = new Grid(1, 2);
        grid.setCellSpacing(4);
        grid.setWidget(0, 0, lblTitle = new Label("Node Id" /* I18N:  */ + ":"));
        grid.setWidget(0, 1, tbNodeId = new TextBox());
        lblTitle.setStyleName("addNoteLbl");
        tbNodeId.setStyleName("addNote");
        tbNodeId.setWidth("100px");

        main.add(grid);
    }

    @Override
    protected void doAction() {

        saveCont.doIt(Integer.valueOf(tbNodeId.getValue()));
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean doActionBeforeClose() {

        if (BaseUtils.isStrEmptyOrWhiteSpace(tbNodeId.getText())) {

            BIKClientSingletons.showWarning(I18n.nodeIdNiePuste.get() /* I18N:  */);
            return false;

        }
        return true;
    }
}
