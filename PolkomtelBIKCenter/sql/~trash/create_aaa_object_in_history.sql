CREATE TABLE aaa_object_in_history (
	id int IDENTITY(1,1) NOT NULL primary key,
	user_id int not null,
	tab_id varchar(100) not null,
	obj_id varchar(100),
	date_added datetime,
);