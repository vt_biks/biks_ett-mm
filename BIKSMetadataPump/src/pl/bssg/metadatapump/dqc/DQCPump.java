/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.dqc;

import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;
import simplelib.Pair;
import simplelib.SimpleDateUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class DQCPump extends MetadataPumpBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected static final int INACTIVE_ID_TEST_STAGE = 4;
    protected INamedSqlsDAO<Object> adhocDaoDQC;
    protected IBeanConnectionEx<Object> dqcConncetion = null;
    protected String dqcServer;
    protected String dqcInstance;
    protected String dqcDatabase;
    protected String dqcUser;
    protected String dqcPassword;
    protected List<String> dqcGroupFilter;
    protected Integer dqcActiveFilter;
    protected Integer withInactiveTests;
    protected List<DQCTestBean> testsList;
    protected List<DQCGroupBean> groupList;
    protected List<DQCRequestBean> requestList;
    protected List<DQCGroupTestBean> groupTestsList;
    protected List<Integer> dqcDeletedGroupId = new ArrayList<Integer>();
    protected List<Integer> dqcDeletedTestId = new ArrayList<Integer>();
    protected List<DQCGroupBean> toDeleteGroupList = new ArrayList<DQCGroupBean>();
    protected List<DQCGroupTestBean> toDeleteGroupTestsList = new ArrayList<DQCGroupTestBean>();
    protected List<DQCTestBean> toDeleteTestsList = new ArrayList<DQCTestBean>();
    protected List<DQCRequestBean> toDeleteRequestsList = new ArrayList<DQCRequestBean>();
    protected List<Map<String, Object>> querySQL = null;
    protected List<Map<String, Object>> allDQCParameters = null;
    protected Map<String, String> allParametersMap = new HashMap<String, String>();
    protected int insertedLogId;
    protected Date activeTestDate;

//    public String getPumpGroup() {
//        return PumpConstants.PUMP_GROUP_DQC;
//    }
    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_DQC;
    }

    @Override
    protected Pair<Integer, String> startPump() {
        // get connection properties
        dqcServer = execNamedQuerySingleVal("getConfigValue", false, "value", "dqc.server");
        dqcInstance = execNamedQuerySingleVal("getConfigValue", false, "value", "dqc.instance");
        dqcDatabase = execNamedQuerySingleVal("getConfigValue", false, "value", "dqc.database");
        dqcUser = execNamedQuerySingleVal("getConfigValue", false, "value", "dqc.user");
        dqcPassword = execNamedQuerySingleVal("getConfigValue", false, "value", "dqc.password");
        dqcGroupFilter = BaseUtils.splitBySep((String) execNamedQuerySingleVal("getConfigValue", true, "value", "dqc.groupFilter"), ",");
        dqcActiveFilter = BaseUtils.tryParseInteger((String) execNamedQuerySingleVal("getConfigValue", false, "value", "dqc.activeFilter"), -1);
        withInactiveTests = BaseUtils.tryParseInteger((String) execNamedQuerySingleVal("getConfigValue", false, "value", "dqc.withInactiveTests"), 1);
        if (dqcInstance.equals("")) {
            dqcInstance = null;
        }

        // create connection to DQC MS SQL Database
        dqcConncetion = new MssqlConnection(
                new MssqlConnectionConfig(dqcServer, dqcInstance, dqcDatabase, dqcUser, dqcPassword));
        if (dqcConncetion == null) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "No connection with DQC DB!");
//            updateLogInCaseError("Brak polaczenia z DQC DB!");
        }
        dqcConncetion.setDbStatementExecutionWarnThresholdMillis(-1);
        adhocDaoDQC = FoxyAppUtils.createNamedSqlsDAOFromJar(dqcConncetion, ADHOC_SQLS_PATH);

        pumpLogger.pumpLog("Retrieving data from DQC database", 0);

        try {
            testsList = adhocDaoDQC.createBeansFromNamedQry("getDQCTests", DQCTestBean.class);
            groupList = adhocDaoDQC.createBeansFromNamedQry("getDQCGroups", DQCGroupBean.class);
            requestList = adhocDaoDQC.createBeansFromNamedQry("getDQCRequests", DQCRequestBean.class);
            groupTestsList = adhocDaoDQC.createBeansFromNamedQry("getDQCGroupTests", DQCGroupTestBean.class);
            querySQL = adhocDaoDQC.execNamedQueryCFN("getDQCSQLS", FieldNameConversion.ToLowerCase);
            allDQCParameters = adhocDaoDQC.execNamedQueryCFN("getDQCAllParameters", FieldNameConversion.ToLowerCase);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage(), e);
            }
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "No connection with DQC DB!");
        }

        pumpLogger.pumpLog("Deleting temporary tables", 40);

        execNamedCommandWithCommit("deleteDQCOldTables");

        if (dqcActiveFilter == -1) { // pokazuj wszystkie requesty
            activeTestDate = SimpleDateUtils.newDate(1990, 1, 1); // na sztywno stara data, testy na pewno będą nowsze
        } else { // filtr na requesty w dniach
            activeTestDate = new Date();
            activeTestDate = SimpleDateUtils.addMillis(activeTestDate, -1L * dqcActiveFilter * SimpleDateUtils.DAY_AS_MILLIS);
        }
        // wyznaczanie typów dla testów
        for (DQCTestBean bean : testsList) {
            bean.typeTest = getTypeForTest(bean.idTest, bean.errorThreshold, bean.idTestStage);
        }

        deleteInactiveTests();
        deleteInactiveRequests();

        pumpLogger.pumpLog("Moving data to temporary tables", 50);
//        updateLogs("Przerzucanie danych do tabel tymczasowych");

        int packageSize = 100; // rozmiar pakietu celowo zmniejszony z 500 do 100
        insertListToDBInPackagesEx(testsList, "insertTests", packageSize);
        insertListToDBInPackagesEx(groupList, "insertGroup", packageSize);
        insertListToDBInPackagesEx(requestList, "insertRequests", packageSize);
        insertListToDBInPackagesEx(groupTestsList, "insertGroupTests", packageSize);

        pumpLogger.pumpLog("Update BIKS structure", 70);
//        updateLogs("Update struktury danych BIKSa");

        execNamedCommandWithCommit("moveDQCMetadataIntoBikNode");

        // auto łączenie metadanych
        for (Map<String, Object> param : allDQCParameters) {
            allParametersMap.put((String) param.get("name"), (String) param.get("value"));
        }
        for (Map<String, Object> query : querySQL) {
            Set<String> parametersSet = BaseUtils.parseStringAndGetSetOfParams((String) query.get("generic_code"), "${", "}");
            // odfiltrowanie interesujacych parametrów
            Set<String> schemaSet = new HashSet<String>();
            Set<String> tableSet = new HashSet<String>();
            for (String string : parametersSet) {
                if (string.startsWith("DB")) {
                    schemaSet.add(allParametersMap.get(string));
                } else if (string.startsWith("T")) {
                    if (BaseUtils.tryParseInteger(string.substring(1), -1) != -1) { // dodajemy tylko dopasowane do maski Txxx gdzie xxx to liczby
                        tableSet.add(allParametersMap.get(string));
                    }
                }
            }
            Set<String> objIdSet = createObjIdSet(schemaSet, tableSet);
            for (String objId : objIdSet) {
                execNamedCommandWithCommit("insertIntoTestParameters", (Integer) query.get("id_test"), objId);
            }
        }
        execNamedCommandWithCommit("insertDQCMetadataConnection");

        execNamedCommandWithCommit("updateFvsWithFailedDqcTestExecutionsFromLastLoad");

//        insertLogSizeToDB();
//
//        execNamedCommandWithCommit("insertLogToDB", "Zasilanie z DQC zakończone!", insertedLogId);
//        execNamedCommandWithCommit("updateMainLogWithStatus", "Sukces", insertedLogId, PumpConstants.LOG_LOAD_STATUS_DONE);
//        return PumpConstants.LOG_LOAD_STATUS_DONE;
        return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
    }

    // to check
    protected void deleteInactiveRequests() {
        toDeleteRequestsList.clear();
        for (DQCRequestBean bean : requestList) {
            if (bean.startTimestamp.before(activeTestDate)) {
                toDeleteRequestsList.add(bean);
            }
        }
        requestList.removeAll(toDeleteRequestsList);
    }

    protected void deleteInactiveTests() {
        // usuwanie zbędnych grup
        for (DQCGroupBean bean : groupList) {
            if (dqcGroupFilter.contains(bean.name)) {
                toDeleteGroupList.add(bean);
                dqcDeletedGroupId.add(bean.idGroup);
            }
        }
        groupList.removeAll(toDeleteGroupList);
        for (DQCGroupTestBean bean : groupTestsList) {
            if (dqcDeletedGroupId.contains(bean.idGroup)) {
                toDeleteGroupTestsList.add(bean);
            }
        }
        groupTestsList.removeAll(toDeleteGroupTestsList);
        toDeleteGroupTestsList.clear();
        // usuwanie nieaktywnych testów, wykonań i przyporządkowania do grup
        if (withInactiveTests == 0) {
            for (DQCTestBean testBean : testsList) {
                if (testBean.typeTest == PumpConstants.DQC_TEST_INACTIVE) {
                    dqcDeletedTestId.add(testBean.idTest);
                    toDeleteTestsList.add(testBean);
                }
            }
            testsList.removeAll(toDeleteTestsList);
            for (DQCGroupTestBean bean : groupTestsList) {
                if (dqcDeletedTestId.contains(bean.idTest)) {
                    toDeleteGroupTestsList.add(bean);
                }
            }
            groupTestsList.removeAll(toDeleteGroupTestsList);
            for (DQCRequestBean bean : requestList) {
                if (dqcDeletedTestId.contains(bean.idTest)) {
                    toDeleteRequestsList.add(bean);
                }
            }
            requestList.removeAll(toDeleteRequestsList);
        }
    }

    protected Integer getTypeForTest(int testId, Double errorThreshold, int idTestStage) {
        Date maxDateForTest = null;
        Double reportError = null;
        for (DQCRequestBean bean : requestList) {
            if (bean.idTest == testId) {
                if (maxDateForTest == null) {
                    maxDateForTest = bean.startTimestamp;
                    reportError = setReportError(bean);
                } else {
                    if (maxDateForTest.before(bean.startTimestamp)) {
                        maxDateForTest = bean.startTimestamp;
                        reportError = setReportError(bean);
                    }
                }
            }
        }
        if (idTestStage != INACTIVE_ID_TEST_STAGE || maxDateForTest == null) {
            return PumpConstants.DQC_TEST_INACTIVE;
        }
        if (errorThreshold < reportError) {
            return PumpConstants.DQC_TEST_FAILED;
        } else {
            return PumpConstants.DQC_TEST_SUCCESS;
        }
    }

    protected Double setReportError(DQCRequestBean bean) {
        Double reportError;
        if (bean.errorCount == null || bean.caseCount == null) {
            reportError = 0.0;
        } else {
            reportError = ((double) bean.errorCount) / ((double) bean.caseCount);
        }
        return reportError;
    }

    protected Set<String> createObjIdSet(Set<String> schemaSet, Set<String> tableSet) {
        Set<String> objIdSet = new HashSet<String>();
        for (String schema : schemaSet) {
            objIdSet.add(schema + "|");
            for (String table : tableSet) {
                objIdSet.add(schema + "|" + table + "|");
            }
        }
        return objIdSet;
    }
}
