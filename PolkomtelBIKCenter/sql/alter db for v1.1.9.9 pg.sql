
exec sp_check_version '1.1.9.9';
go

delete from bik_help where help_key='65'

-- poprzednia wersja w pliku: "alter db for v1.1.6.8 tf.sql"
delete from bik_node where tree_id = dbo.fn_tree_id_by_code('TreeOfTrees')
go

exec sp_add_menu_node null, 'M�j BIKS', '#MyBIKS'
exec sp_add_menu_node null, 'Biblioteka', 'metadata'
exec sp_add_menu_node 'metadata', 'SAP BO', 'sapbo'
exec sp_add_menu_node 'sapbo', '@', '$Reports'
exec sp_add_menu_node 'sapbo', '@', '$ObjectUniverses'
exec sp_add_menu_node 'sapbo', '@', '$Connections'
exec sp_add_menu_node 'metadata', '@', '$Teradata'
exec sp_add_menu_node 'metadata', '@', '$DQC'
exec sp_add_menu_node null, '@', '$Glossary'
exec sp_add_menu_node null, 'S�owniki', '@Dictionary'
--exec sp_add_menu_node null, 'Kategoryzacje', '@Taxonomy'
--exec sp_add_menu_node null, 'Dokumenty', '$Documents'
--exec sp_add_menu_node null, 'Blogi', '$Blogs'
exec sp_add_menu_node null, 'Baza Wiedzy', 'knowledge'
exec sp_add_menu_node null, 'Spo�eczno�� BI', 'community'
exec sp_add_menu_node null, 'Admin', '#Admin'
--exec sp_add_menu_node null, 'Szukaj', '#Search'
exec sp_add_menu_node null, 'Szukaj', '#NewSearch'
		
exec sp_add_menu_node '#Admin', 'S�owniki', '#admin:dict' 
exec sp_add_menu_node '#admin:dict', 'Taksonomie', '#admin:dict:trees' 
exec sp_add_menu_node '#admin:dict', 'AppProps', '#admin:dict:bikAppProps'
exec sp_add_menu_node '#admin:dict', 'U�ytkownicy systemu', '#admin:dict:sysUsers'
exec sp_add_menu_node '#admin:dict', 'Atrybuty', '#admin:dict:attrDefs'
exec sp_add_menu_node '#admin:dict', 'Role U�ytkownik�w', '#admin:dict:role'
exec sp_add_menu_node '#admin:dict', 'Tre�� pomocy', '#admin:dict:attrHints'

exec sp_add_menu_node '#Admin', 'Zasilania', '#admin:load'
exec sp_add_menu_node '#admin:load', 'Logi', '#admin:load:logs'
exec sp_add_menu_node '#admin:load', 'Uruchamianie', '#admin:load:runManual'
exec sp_add_menu_node '#admin:load', 'Harmonogram', '#admin:load:schedule'
exec sp_add_menu_node '#admin:load', 'Konfiguracja po��cze�', '#admin:load:cfg'

exec sp_add_menu_node '#Admin', 'Statystyki', '#admin:stat'
exec sp_add_menu_node '#admin:stat', 'Statystyki', '#admin:dict:statistics'
exec sp_add_menu_node '#admin:stat', 'Zalogowani u�ytkownicy', '#admin:dict:userstatistics'


exec sp_add_menu_node '$DQC', '@', '&DQCGroup'
exec sp_add_menu_node '&DQCGroup', '@', '&DQCTestSuccess'
exec sp_add_menu_node '&DQCGroup', '@', '&DQCTestFailed'
exec sp_add_menu_node '&DQCGroup', '@', '&DQCTestInactive'

exec sp_add_menu_node '$Teradata', '@', '&TeradataOwner'
exec sp_add_menu_node '&TeradataOwner', '@', '&TeradataSchema'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataView'
exec sp_add_menu_node '&TeradataView', '@', '&TeradataColumn'

exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataTable'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumn'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumnPK'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumnIDX'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataProcedure'
exec sp_add_menu_node '$Reports', '@', '&ReportFolder'
exec sp_add_menu_node '&ReportFolder', '@', '&Webi'
exec sp_add_menu_node '&ReportFolder', '@', '&FullClient'
exec sp_add_menu_node '&Webi', '@', '&ReportQuery'
exec sp_add_menu_node '&ReportQuery', '@', '&Dimension$Reports'
exec sp_add_menu_node '&ReportQuery', '@', '&Measure'
exec sp_add_menu_node '&ReportQuery', '@', '&Detail'
exec sp_add_menu_node '&ReportFolder', '@', '&Flash'
exec sp_add_menu_node '&ReportFolder', '@', '&Excel'
exec sp_add_menu_node '&ReportFolder', '@', '&Hyperlink'
exec sp_add_menu_node '&ReportFolder', '@', '&Powerpoint'
exec sp_add_menu_node '&ReportFolder', '@', '&Pdf'
exec sp_add_menu_node '&ReportFolder', '@', '&CrystalReport'
exec sp_add_menu_node '$ObjectUniverses', '@', '&UniversesFolder'
exec sp_add_menu_node '&UniversesFolder', '@', '&Universe'

exec sp_add_menu_node '&Universe', '@', '&UniverseDerivedTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseAliasTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseTable'

exec sp_add_menu_node '&Universe', '@', '&UniverseClass'
exec sp_add_menu_node '&UniverseClass', '@', '&Dimension'
exec sp_add_menu_node '&UniverseClass', '@', '&Measure'
exec sp_add_menu_node '&Dimension', '@', '&Detail'
exec sp_add_menu_node '&UniverseClass', '@', '&Filter'

exec sp_add_menu_node '$Connections', '@', '&ConnectionNetworkFolder'
exec sp_add_menu_node '&ConnectionNetworkFolder', '@', '&ConnectionEngineFolder'
exec sp_add_menu_node '&ConnectionEngineFolder', '@', '&DataConnection'

exec sp_add_menu_node 'knowledge', 'Dokumentacja', '$Documents'
exec sp_add_menu_node 'knowledge', 'Kategoryzacje', '@Taxonomy'

exec sp_add_menu_node '@Taxonomy', '@', '&TaxonomyEntity'

exec sp_add_menu_node 'community', 'Blogi', '$Blogs'
exec sp_add_menu_node 'community', 'U�ytkownicy', '$Users'
exec sp_add_menu_node 'community', 'Ranking u�ytkownik�w', '#Rank'  --new

exec sp_add_menu_node '$Documents', '@', '&DocumentsFolder'
exec sp_add_menu_node '&DocumentsFolder', '@', '&Document'
exec sp_add_menu_node '&DocumentsFolder', '@', '&Article'

exec sp_add_menu_node '$Users', '@', '&UsersGroup'
exec sp_add_menu_node '&UsersGroup', '@', '&User'
exec sp_add_menu_node '$Blogs', '@', '&Blog'

exec sp_add_menu_node '&Blog', '@', '&BlogEntry'

exec sp_add_menu_node '$Glossary', '@', '&GlossaryCategory$Glossary'

exec sp_add_menu_node '&GlossaryCategory$Glossary', '@', '&Glossary'
exec sp_add_menu_node '&GlossaryCategory$Glossary', '@', '&GlossaryNotCorpo'
exec sp_add_menu_node '&GlossaryCategory$Glossary', '@', '&GlossaryVersion'

exec sp_add_menu_node '@Dictionary', '@', '&GlossaryCategory'

exec sp_add_menu_node '&GlossaryCategory', '@', '&Glossary'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryNotCorpo'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryVersion'


exec sp_add_menu_node 'bazydanych', '@', '$MSSQL'
exec sp_add_menu_node 'metadata', 'SAS', 'sas'
exec sp_add_menu_node 'sas', '@', '$SASReports'
exec sp_add_menu_node 'sas', '@', '$SASInformationMaps'
exec sp_add_menu_node 'sas', '@', '$SASLibraries'
exec sp_add_menu_node 'sas', '@', '$SASCubes'

exec sp_add_menu_node '$MSSQL', '@', '&MSSQLServer'
exec sp_add_menu_node '&MSSQLServer', '@', '&MSSQLDatabase'
exec sp_add_menu_node '&MSSQLDatabase', '@', '&MSSQLOwner'
exec sp_add_menu_node '&MSSQLOwner', '@', '&MSSQLTable'
exec sp_add_menu_node '&MSSQLTable', '@', '&MSSQLColumn'
exec sp_add_menu_node '&MSSQLOwner', '@', '&MSSQLProcedure'
exec sp_add_menu_node '&MSSQLOwner', '@', '&MSSQLFunction'
exec sp_add_menu_node '&MSSQLOwner', '@', '&MSSQLView'
exec sp_add_menu_node '&MSSQLView', '@', '&MSSQLColumn'

exec sp_add_menu_node '$SASReports', '@', '&SASReportFolder'
exec sp_add_menu_node '&SASReportFolder', '@', '&SASWrs'

exec sp_add_menu_node '$SASInformationMaps', '@', '&SASInformationMapFolder'
exec sp_add_menu_node '&SASInformationMapFolder', '@', '&SASInformationMapCube'
exec sp_add_menu_node '&SASInformationMapCube', '@', '&SASFolder'
exec sp_add_menu_node '&SASFolder', '@', '&SASMeasure'
exec sp_add_menu_node '&SASFolder', '@', '&SASCategory'
exec sp_add_menu_node '&SASFolder', '@', '&SASFilter'

exec sp_add_menu_node '&SASInformationMapFolder', '@', '&SASInformationMap'

exec sp_add_menu_node '&SASInformationMap', '@', '&SASFolder$SAS'

exec sp_add_menu_node '&SASFolder$SAS', '@', '&SASMeasure'
exec sp_add_menu_node '&SASFolder$SAS', '@', '&SASCategory'
exec sp_add_menu_node '&SASFolder$SAS', '@', '&SASFilter'

exec sp_add_menu_node '$SASLibraries', '@', '&SASLibraryFolder'
exec sp_add_menu_node '&SASLibraryFolder', '@', '&SASLibrary'
exec sp_add_menu_node '&SASLibrary', '@', '&SASLibraryTable'
exec sp_add_menu_node '&SASLibraryTable', '@', '&SASLibraryColumnNum'
exec sp_add_menu_node '&SASLibraryTable', '@', '&SASLibraryColumnChar'
exec sp_add_menu_node '&SASLibraryTable', '@', '&SASLibraryColumnDate'

exec sp_add_menu_node '$SASCubes', '@', '&SASCubeFolder'
exec sp_add_menu_node '&SASCubeFolder', '@', '&SASCube'
exec sp_add_menu_node '&SASCube', '@', '&SASCubeMeasures'
exec sp_add_menu_node '&SASCubeMeasures', '@', '&SASMeasure'
exec sp_add_menu_node '&SASCubeMeasures', '@', '&SASCalculatedMeasure'
exec sp_add_menu_node '&SASCube', '@', '&SASCubeDimension'
exec sp_add_menu_node '&SASCubeDimension', '@', '&SASHierarchy'
exec sp_add_menu_node '&SASHierarchy', '@', '&SASLevel'

go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go

exec sp_update_version '1.1.9.9', '1.1.9.10';
go