if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_itscc_calc_scc_v3]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_itscc_calc_scc_v3]
go

create procedure [dbo].[sp_itscc_calc_scc_v3] as
begin
  set nocount on;

  declare @start_time datetime = getdate();

  declare @diag_level int = 0;

  if @diag_level >= 0 print cast(sysdatetime() as varchar(23)) + ': [sp_itscc_calc_scc_v3] start';

  declare @idx int = 1;

  --declare @stack_size int;
  --declare @max_i int, @max_i_ic int, @max_i_id int, @max_i_max int;

  create table #stack (i int not null identity primary key, w_id varchar(17) null, v_id varchar(17) not null, f int not null, unique (w_id, v_id, f));

  insert into #stack (w_id, v_id, f) select null, node_id, 0 from itscc_graph_node;

  --create table #scc_stack (node_id varchar(17) not null primary key);

  --select @stack_size = @@rowcount, @max_i = scope_identity(), @max_i_ic = ident_current('#stack'), @max_i_id = @@identity;

  --select @max_i_max = (select max(i) from #stack);

  /*print '@stack_size = ' + cast(@stack_size as varchar(20)) + ', @max_i = ' + cast(@max_i as varchar(20)) + 
    ', @max_i_ic = ' + cast(@max_i_ic as varchar(20)) + 
    ', @max_i_id = ' + cast(@max_i_id as varchar(20)) + 
    ', @max_i_max = ' + cast(@max_i_max as varchar(20));

  set @max_i = @max_i_ic;
  */
  create table #node_state (node_id varchar(17) not null primary key, state int not null, idx int not null, lowlink int not null);

  create index idx_node_state_idx on #node_state (idx, state);

  insert into #node_state (node_id, state, idx, lowlink) select node_id, 0, 0, 0 from itscc_graph_node;

  declare @pass int = 0;

  while 1 = 1 --@stack_size > 0 --exists (select 1 from #stack)
  begin
    if @diag_level > 0
      print '
    ----------------------------- pass #' + cast(@pass as varchar(20)) + ' -----------------------------

    ';
    if @diag_level > 1
    begin
      select * from #stack order by i;
      select * from #node_state order by node_id;
    end;

    /*if @diag_level > 0
    begin
      declare @real_max_i int = (select max(i) from #stack);
      if @max_i <> @real_max_i
      begin
        print '@max_i = ' + cast(@max_i as varchar(20)) + ', @real_max_i = ' + cast(@real_max_i as varchar(20));
        break;
      end;
    end;*/
    
    --declare @max_i int = (select max(i) from #stack);
    declare @w_id varchar(17), @v_id varchar(17), @f int;
    declare @v_state int, @v_idx int, @v_lowlink int;
    
    --select @w_id = w_id, @v_id = v_id, @f = f from #stack where i = @max_i;
    declare @max_i int;
    set @max_i = null;
    select top 1 @max_i = i, @w_id = w_id, @v_id = v_id, @f = f from #stack order by i desc;
    if @max_i is null break;
    
    delete from #stack where i = @max_i;
    --set @stack_size -= 1;
    
    select @v_state = state, @v_idx = idx, @v_lowlink = lowlink from #node_state where node_id = @v_id;

    if @diag_level > 0
    print '@w_id = ' + coalesce(@w_id, '<null>') + ', @v_id = ' + @v_id + ', @f = ' + cast(@f as varchar(20)) +
      ', @v_state = ' + cast(@v_state as varchar(20)) + ', @v_idx = ' + cast(@v_idx as varchar(20)) + ', @v_lowlink = ' + cast(@v_lowlink as varchar(20));

    /*if @diag_level > 0
    begin
      declare @real_stack_size int = (select count(*) from #stack);
      if @stack_size <> @real_stack_size begin
        print '@stack_size = ' + cast(@stack_size as varchar(20)) + ', @real_stack_size = ' + cast(@real_stack_size as varchar(20));
        break;
      end;
    end;*/

    if @f = 0
    begin
      if @v_state <> 0 -- = 2
      begin
        --select @max_i = max(i) from #stack;
        continue;
      end;
      
      --declare @ident int;
      
      insert into #stack (w_id, v_id, f) values (@w_id, @v_id, 2);
      
      --set @max_i = ident_current('#stack'); --scope_identity();
      
      insert into #stack (w_id, v_id, f) select @v_id, e.dst_node_id, 0
      from itscc_graph_edge e inner join #node_state s on e.dst_node_id = s.node_id
      where e.src_node_id = @v_id and s.state = 0;

      declare @lowlink int;
      
      select @lowlink = min(s.idx)
      from itscc_graph_edge e inner join #node_state s on e.dst_node_id = s.node_id
      where e.src_node_id = @v_id and s.state = 1;

      if @lowlink is null set @lowlink = @idx;

      update #node_state set state = 1, idx = @idx, lowlink = @lowlink where node_id = @v_id;
      
      set @idx += 1;
            
      --select @stack_size += 1 + @@rowcount, @ident = ident_current('#stack'); --scope_identity();
      --if @ident is not null set @max_i = @ident; 
      
      --print 'stack after inserts:';    
      --select * from #stack order by i;
            
    end else if @f = 1
    begin
      /*
        4.3. je�eli f = 1 ->
        4.3.1. je�eli v jest w trakcie odwiedzin ->
        4.3.1.1. w.minlp = min(w.minlp, v.minlp)
        4.3.2. je�eli v jest nieodwiedzone ->
        4.3.2.1. wrzu� na stos <w, v, 0>
      */

      if @v_state = 1
      begin
        --select @max_i = max(i) from #stack;
        update #node_state set lowlink = case when lowlink > @v_lowlink then @v_lowlink else lowlink end where node_id = @w_id;
      end else if @v_state = 0 begin
        insert into #stack (w_id, v_id, f) values (@w_id, @v_id, 0);
        --select @stack_size += 1, @max_i = ident_current('#stack'); --scope_identity();
      end --else select @max_i = max(i) from #stack;    
    end else if @f = 2
    begin
      /*
        4.4. je�eli f = 2 (czyli wszystkie dzieci v s� przetworzone w wewn. p�tli) ->
        4.4.1. je�eli v.lp = v.minlp ->
        4.4.1.1. dla ka�dego w�z�a q:
        4.4.1.1.1. je�eli q.lp >= v.lp i q jest w trakcie odwiedzin ->
        4.4.1.1.1.1. oznacz q jako odwiedzony, ustaw q.minlp = v.lp
        4.4.2. je�eli w <> null ->
        4.4.2.1. w.minlp = min(w.minlp, v.minlp)
      */
      
      --select @max_i = max(i) from #stack;
      
      if @v_idx = @v_lowlink
        update #node_state set state = 2, lowlink = @v_idx where idx >= @v_idx and state = 1;
      
      if @w_id is not null
        update #node_state set lowlink = case when lowlink > @v_lowlink then @v_lowlink else lowlink end where node_id = @w_id;        
    end;

    set @pass += 1;
    
    --if @pass > 10 break;
    
    -- exec sp_itscc_calc_scc_v3;

    --if @pass > 5000 set @diag_level = 1;
  end;

  declare @final_stack_size int = (select count(*) from #stack);

  if @final_stack_size > 0 print 'stack is not empty!!! stack size: ' + cast(@final_stack_size as varchar(20));

  declare @end_time datetime = getdate();

  declare @node_cnt int = (select count(*) from itscc_graph_node);
  declare @edge_cnt int = (select count(*) from itscc_graph_edge);

  declare @ms_taken int = datediff(ms, @start_time, @end_time);

  if @diag_level >= 0 print cast(sysdatetime() as varchar(23)) + ': [sp_itscc_calc_scc_v3] finished after ' + cast(@pass as varchar(20)) + ' passes, time taken: ' 
    + cast(@ms_taken as varchar(30)) + ' ms, nodes: ' + cast(@node_cnt as varchar(20)) + ', edges: ' + cast(@edge_cnt as varchar(20)) +
    ', passes per (n+e) = ' + cast(cast(@pass as float) / (@node_cnt + @edge_cnt) as varchar(20)) + ', time per (n+e) = ' + 
    cast(cast(@ms_taken as float) / (@node_cnt + @edge_cnt) as varchar(20));

  select * from #node_state;
end;
go

-- select cast(10 as float)


-------------------------------------------------------
-------------------------------------------------------
-------------------------------------------------------


/*

exec sp_itscc_init_data 20000;

exec sp_itscc_calc_scc_v3;

select * from #node_state;



---- wersja v3:
-- 5000
2015-09-04 19:23:06.398: [sp_itscc_calc_scc_v3] start
2015-09-04 19:23:25.047: [sp_itscc_calc_scc_v3] finished after 170034 passes, time taken: 18633 ms, nodes: 85017, edges: 127191, passes per (n+e) = 0.801261, time per (n+e) = 0.0878054

-- 10000
2015-09-04 19:29:52.664: [sp_itscc_calc_scc_v3] start
2015-09-04 19:30:36.234: [sp_itscc_calc_scc_v3] finished after 340034 passes, time taken: 43543 ms, nodes: 170017, edges: 288952, passes per (n+e) = 0.740865, time per (n+e) = 0.0948713

-- 20000
2015-09-04 19:44:05.369: [sp_itscc_calc_scc_v3] start
2015-09-04 19:45:21.563: [sp_itscc_calc_scc_v3] finished after 680034 passes, time taken: 76143 ms, nodes: 340017, edges: 517584, passes per (n+e) = 0.792949, time per (n+e) = 0.088786


---- nowsza wersja v2:

-- 5000
2015-09-04 18:14:39.001: [sp_itscc_calc_scc_v3] start
2015-09-04 18:15:32.325: [sp_itscc_calc_scc_v3] finished after 292421 passes, time taken: 53303 ms, nodes: 85017, edges: 122387, passes per (n+e) = 1.40991, time per (n+e) = 0.257001

-- 10000
2015-09-04 18:17:34.399: [sp_itscc_calc_scc_v3] start
2015-09-04 18:20:48.435: [sp_itscc_calc_scc_v3] finished after 578495 passes, time taken: 194013 ms, nodes: 170017, edges: 238461, passes per (n+e) = 1.41622, time per (n+e) = 0.474966


---- starsza wersja v2:

-- 5000
2015-09-04 16:25:06.655: [sp_itscc_calc_scc_v3] start
2015-09-04 16:26:13.810: [sp_itscc_calc_scc_v3] finished after 292343 passes, time taken: 67136 ms, nodes: 85017, edges: 122309, passes per (n+e) = 1.41006, time per (n+e) = 0.323819

-- 10000
2015-09-04 16:13:31.561: [sp_itscc_calc_scc_v3] start
2015-09-04 16:16:51.056: [sp_itscc_calc_scc_v3] finished after 628109 passes, time taken: 199460 ms, nodes: 170017, edges: 288075, passes per (n+e) = 1.37114, time per (n+e) = 0.435415

-- 20000
2015-09-04 15:49:11.119: [sp_itscc_calc_scc_v3] start
2015-09-04 16:04:04.697: [sp_itscc_calc_scc_v3] finished after 1160051 passes, time taken: 893530 ms, nodes: 340017, edges: 480017, passes per (n+e) = 1.41464, time per (n+e) = 1.08963


*/
