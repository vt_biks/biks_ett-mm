/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.dialogs.lisa.LisaDataType;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryValue;
import pl.fovis.foxygwtcommons.grid.IBeanDisplay;
import simplelib.IContinuation;

/**
 *
 * @author pku
 */
public class LisaDictGridDisplay implements IBeanDisplay<Map<String, LisaDictionaryValue>> {

    private final static String CHECKBOX_COL = "LisaCheckboxColumn";
    private final static String EDIT_ACTION_COL = "LisaEditActionColumn";
    private Map<String, LisaDictColumnBean> columns;
    private Set<Map<String, LisaDictionaryValue>> checkedItems = new HashSet<Map<String, LisaDictionaryValue>>();
//    private List<LisaDictColumnBean> columnsList;
//    private final ClickHandlerCreator editClickHandlerCreator;
    protected Map<Map<String, LisaDictionaryValue>, Map<String, WidgetWithValue>> recordTextBoxMap = new HashMap<Map<String, LisaDictionaryValue>, Map<String, WidgetWithValue>>();
//    protected IParametrizedContinuation<Pair<OperationType, Map<String, LisaDictionaryValue>>> continuation;
    protected boolean isDictionaryWithCategorizations;
    protected Set<Map<String, LisaDictionaryValue>> editedRecords = new HashSet<Map<String, LisaDictionaryValue>>();
    protected Map<String, WidgetWithValue> newRowWidgets = new HashMap<String, WidgetWithValue>();
    protected ArrayList<CheckBox> listOfCheckBox = new ArrayList<CheckBox>();

    public LisaDictGridDisplay(List<LisaDictColumnBean> list/*, IParametrizedContinuation<Pair<OperationType, Map<String, LisaDictionaryValue>>> continuation*/) {
        this(list, true/*, continuation*/);
    }

    public LisaDictGridDisplay(List<LisaDictColumnBean> list, boolean isDictionaryWithCategorizations/*, IParametrizedContinuation<Pair<OperationType, Map<String, LisaDictionaryValue>>> continuation*/) {
        this.columns = createColumnsMap(list);
//        this.columnsList = list;
//        this.continuation = continuation;
        this.isDictionaryWithCategorizations = isDictionaryWithCategorizations;
    }

    private Map<String, LisaDictColumnBean> createColumnsMap(List<LisaDictColumnBean> list) {
        Map<String, LisaDictColumnBean> colMap = new HashMap<String, LisaDictColumnBean>();
        for (LisaDictColumnBean column : list) {
            colMap.put(column.nameInDb, column);
        }
        return colMap;
    }

    public String[] getColArray(List<LisaDictColumnBean> result) {
        List<String> list = new ArrayList<String>();
        list.add(CHECKBOX_COL);
        for (LisaDictColumnBean record : result) {
            if (!record.isNotVisibleAccess()) {
                list.add(record.nameInDb);
            }
        }
//        list.add(EDIT_ACTION_COL);
        return list.toArray(new String[list.size()]);
    }

    public Object getHeader(String colName) {
        if (CHECKBOX_COL.equals(colName)) {
            return getNewCheckBoxHeader();
        } else if (EDIT_ACTION_COL.equals(colName)) {
            return "";
        }
        return columns.get(colName).titleToDisplay;
    }

    private CheckBox getNewCheckBoxHeader() {
        listOfCheckBox.clear();
        CheckBox cb = new CheckBox();

        cb.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                boolean isChecked = ((CheckBox) event.getSource()).getValue();
                for (CheckBox cbTmp : listOfCheckBox) {
                    cbTmp.setValue(isChecked, true);
                }
            }
        });
        return cb;
    }

    public Object getValue(Map<String, LisaDictionaryValue> record, String colName) {
        if (CHECKBOX_COL.equals(colName)) {
            return getNewCheckBoxValue(record);
        } else if (EDIT_ACTION_COL.equals(colName)) {
            return createButtonsPanel(record);//getEditButton(record);
        }
        return record.get(colName) != null ? getTextWidget(record, colName)/*record.get(colName).getValue())*/ : getEmptyWidget(colName);//I18n.nD.get() /* I18N:  */;
    }

    private CheckBox getNewCheckBoxValue(final Map<String, LisaDictionaryValue> record) {
        CheckBox cb = new CheckBox();
        listOfCheckBox.add(cb);

        cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                if (event.getValue()) {
                    checkedItems.add(record);
                } else {
                    checkedItems.remove(record);
                }
            }
        });
//        cb.setVisible(isDictionaryWithCategorizations);
        return cb;
    }

    protected IsWidget getEmptyWidget(String colName) {
        LisaDictColumnBean bean = columns.get(colName);
        LisaDataType dictionaryEnum = LisaDataType.getValueFor(bean.dataType.trim());
        WidgetWithValue newField = dictionaryEnum.createWidget();
        newField.setValue(null);
        newRowWidgets.put(colName, newField);
        return newField.getWidget();
    }

    protected IsWidget getTextWidget(final Map<String, LisaDictionaryValue> record, String colName) {
        LisaDictColumnBean bean = columns.get(colName);
        LisaDataType dictionaryEnum = LisaDataType.getValueFor(bean.dataType.trim());
        WidgetWithValue newField = dictionaryEnum.createWidget();
        newField.setStyleName("textBox");
        LisaDictionaryValue colValue = record.get(bean.nameInDb);

        newField.setValue(colValue.getValue() != null ? colValue.getValue() : null);
        newField.setEditable(false);
        newField.addChangeHandler(new IContinuation() {
            public void doIt() {
                editedRecords.add(record);
            }
        });
//        if (bean.isReadOnlyAccess()) {
//            newField.setEditable(false);
//        }
//        newField.
        insertTextBoxToMap(record, colName, newField);
        return newField.getWidget();
    }

//    private Object getEditButton(final Map<String, LisaDictionaryValue> record) {
//        Button b = new Button(I18n.edytuj.get() /* I18N:  */);
//        b.addClickHandler(new ClickHandler() {
//            public void onClick(ClickEvent event) {
//                setRecordEnabled(record, true);
//            }
//        });
////        b.addClickHandler(editClickHandlerCreator.createClickHandler(record, columnsList));
//        return b;
//    }
    public void resetCheckedItems() {
        checkedItems.clear();
    }

    public void resetTextBoxes() {
        recordTextBoxMap.clear();
        editedRecords.clear();
        newRowWidgets.clear();
    }

    // modyfikuje i pobiera listę recordów ze zmianami
    // rekordy powinny być zapisane do bazy
    // w przeciwnym przypadku należu odświeżyć grid - pobrać ponownie rekordy
    public Set<Map<String, LisaDictionaryValue>> getChangedItems() {
        for (Map<String, LisaDictionaryValue> record : editedRecords) {
            Map<String, WidgetWithValue> row = recordTextBoxMap.get(record);
            for (Map.Entry<String, WidgetWithValue> entryRow : row.entrySet()) {
                record.get(entryRow.getKey()).setValue(entryRow.getValue().getValue().getValue());
            }
        }
        return editedRecords;
    }

    public Set<Map<String, LisaDictionaryValue>> getCheckedItems() {
        return checkedItems;
    }

    protected void insertTextBoxToMap(Map<String, LisaDictionaryValue> record, String colName, WidgetWithValue tbx) {
        if (!recordTextBoxMap.containsKey(record)) {
            recordTextBoxMap.put(record, new HashMap<String, WidgetWithValue>());
        }
        Map<String, WidgetWithValue> row = recordTextBoxMap.get(record);
        row.put(colName, tbx);
    }

    protected Object createButtonsPanel(final Map<String, LisaDictionaryValue> record) {
        FlowPanel fp = new FlowPanel();
//        fp.setWidth("100px");
        final Button edit = new Button(I18n.edytuj.get() /* I18N:  */);
        edit.setWidth("100px");
        edit.setVisible(true);
//        final Button delete = new Button(I18n.usun.get() /* I18N:  */);
//        delete.setWidth("50px");
//        delete.setVisible(true);
        final Button save = new Button(I18n.zapisz.get() /* I18N:  */);
        save.setWidth("50px");
        save.setVisible(false);
        final Button cancel = new Button(I18n.anuluj.get() /* I18N:  */);
        cancel.setWidth("50px");
        cancel.setVisible(false);

        edit.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                setRecordEnabled(record, true);
                edit.setVisible(false);
//                delete.setVisible(false);
                save.setVisible(true);
                cancel.setVisible(true);
            }
        });
//        delete.addClickHandler(new ClickHandler() {
//            public void onClick(ClickEvent event) {
//                //delete
//                // ukrywanie linii
//            }
//        });
        save.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                setRecordEnabled(record, false);
                edit.setVisible(true);
//                delete.setVisible(true);
                save.setVisible(false);
                cancel.setVisible(false);
                editRecordInDB(record);
            }
        });
        cancel.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                setInTextBoxesOldValue(record);
                setRecordEnabled(record, false);
                edit.setVisible(true);
//                delete.setVisible(true);
                save.setVisible(false);
                cancel.setVisible(false);
            }
        });

        fp.add(edit);
//        fp.add(delete);
        fp.add(save);
        fp.add(cancel);
        return fp;
    }

    protected void setInTextBoxesOldValue(Map<String, LisaDictionaryValue> record) {
        Map<String, WidgetWithValue> row = recordTextBoxMap.get(record);
        for (Map.Entry<String, WidgetWithValue> entry : row.entrySet()) {
            entry.getValue().setValue(record.get(entry.getKey()).getValue());
        }
    }

    protected void setRecordEnabled(Map<String, LisaDictionaryValue> record, boolean isVisible) {
        Map<String, WidgetWithValue> row = recordTextBoxMap.get(record);
        for (WidgetWithValue textBox : row.values()) {
            textBox.setEditable(isVisible);
        }
    }

    public void setRecordsEnabled(boolean isVisible) {
        for (Map<String, WidgetWithValue> row : recordTextBoxMap.values()) {

            for (Map.Entry<String, WidgetWithValue> entry : row.entrySet()) {
                String columnName = entry.getKey();
                WidgetWithValue widgetWithValue = entry.getValue();

                LisaDictColumnBean lisaDictColumnBean = columns.get(columnName);
                if (lisaDictColumnBean != null && !lisaDictColumnBean.isReadOnlyAccess() && !lisaDictColumnBean.isNotVisibleAccess()) {
                    widgetWithValue.setEditable(isVisible);
                }
            }
        }
    }

    protected void editRecordInDB(Map<String, LisaDictionaryValue> record) {
        Map<String, WidgetWithValue> row = recordTextBoxMap.get(record);
        for (Map.Entry<String, WidgetWithValue> entryRow : row.entrySet()) {
            record.get(entryRow.getKey()).setValue(entryRow.getValue().getValue().getValue());
        }
//        continuation.doIt(new Pair<OperationType, Map<String, LisaDictionaryValue>>(OperationType.EDIT, record));
    }

    public void cancelModify() {
        for (Map.Entry<Map<String, LisaDictionaryValue>, Map<String, WidgetWithValue>> entry : recordTextBoxMap.entrySet()) {
            for (Map.Entry<String, WidgetWithValue> entryInside : entry.getValue().entrySet()) {
                entryInside.getValue().setValue(entry.getKey().get(entryInside.getKey()).getValue());
                //entry.getValue().setValue(record.get(entry.getKey()).getValue());
            }
        }
    }

    public Map<String, LisaDictionaryValue> getNewRowWidgetsValues() {
        Map<String, LisaDictionaryValue> mapOfNewValues = new HashMap<String, LisaDictionaryValue>();
        for (Map.Entry<String, WidgetWithValue> entry : newRowWidgets.entrySet()) {
            mapOfNewValues.put(entry.getKey(), entry.getValue().getValue());
        }
        return mapOfNewValues;
    }

    public void setNewRowWidgetsDisabled() {
        for (WidgetWithValue widget : newRowWidgets.values()) {
            widget.setEditable(false);
        }
    }

    public void clearNewWidgetMap() {
        newRowWidgets.clear();
    }
}
