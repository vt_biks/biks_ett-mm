/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.tags;

import pl.foxys.foxymill.CacheItem;
import pl.foxys.foxymill.CacheStorage;
import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.exprlang.IParsedExpression;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class CacheTag extends TagBase {

    private static final ILameLogger logger = LameLoggerFactory.getLogger(CacheTag.class);
    private IParsedExpression keyExpr;

    protected CacheStorage getStorage() {
        return renderEnv.cacheStorage;
    }

    @Override
    protected void prepareSelf() {
        keyExpr = attrToReqExpr("key"); //exprLang.parseExpression(cond);
    }

    public Object renderSubTags(//IRenderContext
            IRenderCtx renderCtx) {
        Object key = keyExpr.eval(renderCtx);
        CacheItem item = key == null ? null : getStorage().getItem(key);
        if (logger.isDebugEnabled()) {
            logger.debug("cacheTag at " + tfp.toString() + ", key=" + key + ", has item? " + (item != null));
        }

        //System.out.println("cacheTag, key=" + key + ", has item? " + (item != null));

        if (item == null) {
            item =
                    FoxyMillUtils.renderToCacheItem(//renderCtx,
                    renderCtx.getRootModel(), renderCtx.getVars(),
                    FoxyMillUtils.wrapAsRenderable(subTags, null));
//            CachingOutputDevice cod = new CachingOutputDevice();
//            IRenderCtx cachingRenderCtx = RenderCtx.makeRenderCtx(
//                    renderCtx.getRootModel(), renderCtx.getVars(), cod);
//            FoxyMillUtils.internalRender(subTags, cachingRenderCtx);
//            item = cod.doneRendering();
            getStorage().setItem(key, item);
        }

//        int i = 0;
//        while (i < item.subCacheTags.size()) {
//            renderCtx.getOutputDevice().append(item.renderedParts.get(i));
//            //SimpleRenderContext src = new SimpleRenderContext(renderCtx.getEvalContext(), item.subCacheVars.get(i));
//            //renderCtx.getVars().putAll(item.subCacheVars.get(i));
//            IRenderCtx rc = RenderCtx.makeRenderCtx(renderCtx.getRootModel(), item.subCacheVars.get(i),
//                    renderCtx.getOutputDevice());
//            item.subCacheTags.get(i).renderSubTags(rc);
//            //renderCtx.appendToOutput(src.getOutputStr());
//            i++;
//        }
//        renderCtx.getOutputDevice().append(item.renderedParts.get(i));

        FoxyMillUtils.renderCacheItem(item, renderCtx.getRootModel(), renderCtx.getOutputDevice(), false);

        return key;
    }

    @Override
    public void internalRender(//IRenderContext
            IRenderCtx renderCtx) {
        if (renderEnv.cacheStorage == null) {
            FoxyMillUtils.render(subTags, renderCtx);
        } else {
            renderCtx.getOutputDevice().appendCacheTag(this, renderCtx);
        }
        
//        Pair<Object, CacheItem> p = renderCtx.addSubCache(this, renderCtx);
//        if (p.v1 != null) {
//            storage.setItem(p.v1, p.v2);
//        }

//        Object key = keyExpr.eval(renderCtx);
//        CacheItem item = storage.getItem(key);
//        System.out.println("cacheTag, key=" + key + ", has item? " + (item != null));
//
//        if (item == null) {
//            item = renderCtx.addSubCache(this);
//            storage.setItem(key, item);
//        } else {
//            int i = 0;
//            while (i < item.subCacheTags.size()) {
//                renderCtx.appendToOutput(item.renderedParts.get(i));
//                SimpleRenderContext src = new SimpleRenderContext(renderCtx.getEvalContext(), item.subCacheVars.get(i));
//                item.subCacheTags.get(i).internalRender(src);
//                renderCtx.appendToOutput(src.getOutputStr());
//                i++;
//            }
//            renderCtx.appendToOutput(item.renderedParts.get(i));
//        }
    }
}
