/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.TextAreaDialog;
import static pl.fovis.client.dialogs.dqm.AddOrEditDQMTestDialogBase.addLabel;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.fovis.common.dqm.DataQualityTestBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public abstract class DQMWidgetConfigMultiSourceBase implements IDQMWidgetConfig {

    protected VerticalPanel main;
    protected String sqlTestText;
    protected String sqlTestErrorText;
    protected FlexTable sourceTable;
    protected Integer nodeId;
    protected List<DQMMultiSourceBean> multiSources;
//    protected Map<Integer, ConnectionParametersDQMConnectionsBean> connections;

//    public DQMWidgetConfigMultiSourceBase(Map<Integer, ConnectionParametersDQMConnectionsBean> connections) {
//        this.connections = connections;
//    }
    @Override
    public void cleanWidgets() {
        sqlTestText = null;
        sqlTestErrorText = null;
        multiSources = new ArrayList<DQMMultiSourceBean>();
        redisplayGrid();
    }

    @Override
    public boolean validateWidgetsBeforeSave() {
        if (BaseUtils.isStrEmptyOrWhiteSpace(sqlTestText)) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.zapytanieTestujaceNieMozeBycPuste.get(), I18n.blad.get(), null);
            return false;
        }
        if (multiSources == null || multiSources.size() < 1) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.zaMaloZrodel.get(), I18n.blad.get(), null);
            return false;
        }
        return true;
    }

    @Override
    public void fillWidgets(DataQualityTestBean bean) {
        sqlTestText = bean.sqlText;
        sqlTestErrorText = bean.errorSqlText;
        multiSources = bean.multiSources;
        redisplayGrid();
    }

    protected abstract void insertSourceRow(final int row, final DQMMultiSourceBean bean);
//    public void insertSourceRow(final int row, final DQMMultiSourceBean bean) {
//        sourceTable.setHTML(row, 0, bean.sourceName);
//        sourceTable.setHTML(row, 1, bean.serverName);
//        sourceTable.setHTML(row, 2, !BaseUtils.isStrEmptyOrWhiteSpace(bean.databaseName) ? bean.databaseName : bean.resultFileName);
//        PushButton editQueryBtn = new PushButton(I18n.edytuj.get());
//        NewLookUtils.makeCustomPushButton(editQueryBtn);
//        editQueryBtn.addClickHandler(new ClickHandler() {
//
//            @Override
//            public void onClick(ClickEvent event) {
//
//                IParametrizedContinuation<DQMMultiSourceBean> continuation = new IParametrizedContinuation<DQMMultiSourceBean>() {
//
//                    @Override
//                    public void doIt(DQMMultiSourceBean param) {
//                        redisplayGrid();
//                    }
//                };
//
//                if (BIKClientSingletons.useDqmConnectionMode()) {
//                    new AddOrEditDQMSourceDialogForConnections().buildAndShowDialog(nodeId, bean,connections, continuation);
//                } else {
//
//                    new AddOrEditDQMSourceDialogForDatabases().buildAndShowDialog(nodeId, bean, continuation);
//                }
//            }
//        });
//        sourceTable.setWidget(row, 3, editQueryBtn);
//        sourceTable.setWidget(row, 4, createDeleteButton(new ClickHandler() {
//
//            @Override
//            public void onClick(ClickEvent event) {
//                multiSources.remove(bean);
//                redisplayGrid();
//            }
//        }, "images/trash_gray.gif"));
//    }

    @Override
    public void fillBean(DataQualityTestBean bean) {
        bean.sqlText = sqlTestText;
        bean.errorSqlText = sqlTestErrorText;
        bean.multiSources = multiSources;
    }

    protected void redisplayGrid() {
        sourceTable.removeAllRows();
        int index = 1;
        createHeader();
        for (DQMMultiSourceBean multiSource : multiSources) {
            insertSourceRow(index++, multiSource);
        }
    }

    @Override
    public Widget getWidget(Integer nodeId) {
        this.nodeId = nodeId;
        if (main == null) {
            buildWidget();
        }
        return main;
    }

    protected abstract PushButton addSourceBtn();

    protected void buildWidget() {
        main = new VerticalPanel();
        main.setWidth("100%");
        multiSources = new ArrayList<DQMMultiSourceBean>();
        //
//        PushButton addSourceBtn = new PushButton(I18n.dodajZrodlo.get());
//        NewLookUtils.makeCustomPushButton(addSourceBtn);
//        addSourceBtn.addClickHandler(new ClickHandler() {
//
//            @Override
//            public void onClick(ClickEvent event) {
//
//                IParametrizedContinuation<DQMMultiSourceBean> continuation = new IParametrizedContinuation<DQMMultiSourceBean>() {
//
//                    @Override
//                    public void doIt(DQMMultiSourceBean param) {
//                        multiSources.add(param);
//                        redisplayGrid();
//                    }
//                };
//
//                if (BIKClientSingletons.useDqmConnectionMode()) {
//                    new AddOrEditDQMSourceDialogForConnections().buildAndShowDialog(nodeId, null, connections, continuation);
//                } else {
//                    new AddOrEditDQMSourceDialogForDatabases().buildAndShowDialog(nodeId, null, continuation);
//                }
//            }
//        });
        sourceTable = new FlexTable();
        createHeader();
        sourceTable.setWidth("100%");
        sourceTable.getColumnFormatter().setWidth(0, "15%");
        sourceTable.getColumnFormatter().setWidth(1, "15%");
        sourceTable.getColumnFormatter().setWidth(2, "35%");
        sourceTable.getColumnFormatter().setWidth(3, "25%");
        sourceTable.getColumnFormatter().setWidth(4, "10%");
        main.add(addSourceBtn());
        main.add(sourceTable);
        main.add(new HTML("<hr>"));
        //
        HorizontalPanel sqlTextPanel = new HorizontalPanel();
        sqlTextPanel.add(addLabel(I18n.zapytanieSprawdzajaceCzyWystepujaBledy.get()));
        PushButton editTestBtn = new PushButton(I18n.edytuj.get());
        NewLookUtils.makeCustomPushButton(editTestBtn);
        editTestBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new TextAreaDialog().buildAndShowDialog(sqlTestText, new IParametrizedContinuation<String>() {

                    @Override
                    public void doIt(String param) {
                        sqlTestText = param;
                    }
                });
            }
        });
        sqlTextPanel.add(editTestBtn);
        main.add(sqlTextPanel);

        HorizontalPanel sqlErrorTextPanel = new HorizontalPanel();
        sqlErrorTextPanel.add(addLabel(I18n.zapytanieZwracajaceDane.get()));
        PushButton editErrorTestBtn = new PushButton(I18n.edytuj.get());
        NewLookUtils.makeCustomPushButton(editErrorTestBtn);
        editErrorTestBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new TextAreaDialog().buildAndShowDialog(sqlTestErrorText, new IParametrizedContinuation<String>() {

                    @Override
                    public void doIt(String param) {
                        sqlTestErrorText = param;
                    }
                });
            }
        });
        sqlErrorTextPanel.add(editErrorTestBtn);
        main.add(sqlErrorTextPanel);
    }

    protected abstract void createHeader();
//    protected void createHeader() {
//        sourceTable.setHTML(0, 0, "<b>" + I18n.konektor.get() + "</b>");
//        sourceTable.setHTML(0, 1, "<b>" + I18n.system.get() + "</b>");
//        sourceTable.setHTML(0, 2, "<b>" + I18n.bazaDanychLubPlik.get() + "</b>");
//        sourceTable.setHTML(0, 3, "<b>" + I18n.edytuj.get() + "</b>");
//        sourceTable.setHTML(0, 4, "<b>" + I18n.usun.get() + "</b>");
//    }

    protected PushButton createDeleteButton(ClickHandler ch, String iconPath) {
        PushButton btnDelete = new PushButton(new Image(iconPath));
        btnDelete.addClickHandler(ch);
        Style style = btnDelete.getElement().getStyle();
        style.setMargin(0, Style.Unit.PX);
        style.setPadding(0, Style.Unit.PX);
        GWTUtils.setStyleAttribute(btnDelete.getElement(), "background" /* I18N: no */, "none" /* I18N: no */);
        style.setBorderWidth(0, Style.Unit.PX);
        return btnDelete;
    }
}
