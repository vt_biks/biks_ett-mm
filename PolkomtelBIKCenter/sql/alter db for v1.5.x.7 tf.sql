﻿exec sp_check_version '1.5.x.7';
go


-- poprzednia wersja w pliku: alter db for v1.4.y2 tf.sql
-- usunięcie dodowania powiązań z dziedziczeniem dla zapytań BO
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_metadata_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
	
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();

    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @universeNodeKind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');

    ----------   zapytanie  -->  universe    ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id as src_node_id, bnu.id as dst_node_id, 1 as type
    from bik_sapbo_query sbq
    inner join bik_node bn on sbq.node_id = bn.id
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind,@uniNewKind)
	where bn.tree_id = @reportTreeId
	and bn.is_deleted = 0

    ----------   zapytanie  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id as src_node_id, bnc.id as dst_node_id, 1 as type
    from bik_sapbo_query sbq
    inner join bik_node bn on sbq.node_id = bn.id
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
    and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
    and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	where bn.tree_id = @reportTreeId
	and bn.is_deleted = 0 

    ----------   universe  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnu.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
    and bnu.obj_id = convert(varchar(30),uni.si_id) 
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)

    ----------   raport  -->  universe   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnu.id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)

    ----------   raport  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnc.id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
    and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
    and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)

	----------   connection  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnc.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
    and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
    and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)

	----------   obiekt  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct ids.object_node_id, bn.parent_node_id, 1 
    from aaa_ids_for_report ids 
    inner join bik_node bn on ids.query_node_id = bn.id
    and bn.tree_id = @reportTreeId
    and bn.is_deleted = 0
    inner join bik_node bno on bno.id = ids.object_node_id
    and bno.tree_id = @universeTreeId
    and bno.is_deleted = 0
	
	----------   UniverseAliasTable  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bsut1.node_id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_sapbo_universe_table bsut1 on bsut.original_table = bsut1.their_id
	inner join bik_node bn on bn.id = bsut.node_id
	inner join bik_node bn2 on bn2.id = bsut1.node_id
	where bsut.is_alias = 1 and bn2.parent_node_id = bn.parent_node_id
	and bn.tree_id = @universeTreeId
	and bn2.tree_id = @universeTreeId

	-- usuniecie alias table - univ table
	exec sp_delete_bik_joined_objs_by_kinds_fast 'UniverseTable', 'UniverseAliasTable'
	
    exec sp_move_bik_joined_objs_tmp 'Webi,ReportQuery', 'Universe,DSL.MetaDataFile,DataConnection,CommonConnection,CCIS.DataConnection,Measure,Dimension,Detail,Filter'
	
end;
go

exec sp_update_version '1.5.x.7', '1.5.x.8';
go
