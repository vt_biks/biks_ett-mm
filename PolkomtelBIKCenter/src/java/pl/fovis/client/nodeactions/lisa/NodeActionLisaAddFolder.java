/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.lisa;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.nodeactions.NodeActionBase;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.OneTextBoxDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author ctran
 */
public class NodeActionLisaAddFolder extends NodeActionBase {

    @Override
    public boolean isEnabledByDesign() {
        return isNodeOneOfTheKind(false, BIKConstants.NODE_KIND_LISA_DICTIONARIES, BIKConstants.NODE_KIND_LISA_FOLDER) && BIKClientSingletons.isBikUserLoggedIn();
    }

    @Override
    public String getCaption() {
        return I18n.dodajFolder.get();
    }

    @Override
    public void execute() {
        new OneTextBoxDialog().buildAndShowDialog(I18n.podajNazwe.get(), "", new IParametrizedContinuation<String>() {

            @Override
            public void doIt(String folderName) {
                BIKClientSingletons.getLisaService().addLisaFolder(getSelectedNodeId(), folderName, new StandardAsyncCallback<Void>() {

                    @Override
                    public void onSuccess(Void result) {
                        BIKClientSingletons.showInfo(I18n.gotowe.get());
                        String tabId = BIKClientSingletons.getTabSelector().getCurrentTab().getId();
                        BIKClientSingletons.getTabSelector().invalidateTabData(tabId);
                    }
                });
            }
        });
    }
}
