/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence.wuwu;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import simplelib.DelimStringMatcher;
import simplelib.deferredstringconverter.IDeferredRegExprReplacer;

/**
 *
 * @author pmielanczuk
 */
public class DeferredConfluenceDwhLinkReplacer implements IDeferredRegExprReplacer<DelimStringMatcher, String> {

    protected IConfluenceObjectImportedChecker importedChecker;

    public DeferredConfluenceDwhLinkReplacer(IConfluenceObjectImportedChecker importedChecker) {
        this.importedChecker = importedChecker;
    }

    @Override
    public DelimStringMatcher[] makeMatchers(String s) {
        return new DelimStringMatcher[]{new DelimStringMatcher(s, "[{[", "]}]")};
    }

    @Override
    public String getDeferredReplacement(DelimStringMatcher m) {
        return m.group(1);
    }

    @Override
    public Collection<String> resolveReplacements(Collection<String> replacements) {
        Map<String, String> importedMap = importedChecker.checkDwhImportedObjs(replacements);

        List< String> res = new ArrayList<String>(replacements.size());

        for (String item : replacements) {
            String biksLink = importedMap.get(item);

            if (biksLink != null) {
                res.add(biksLink);
            } else {
                res.add("[{[" + item + " :-( ]}]");
            }
        }

        return res;
    }
}
