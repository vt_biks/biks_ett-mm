﻿exec sp_check_version '1.8.0.15';
go

if not exists (select 1 from sys.all_columns where object_id = object_id('bik_tree') and name='is_auto_obj_id')
alter table bik_tree add is_auto_obj_id int 
go

if not exists (select 1 from sys.all_columns where object_id = object_id('bik_plain_file') and name='is_incremental')
alter table bik_plain_file add is_incremental int 
go

update bik_plain_file set is_incremental = 0 where is_incremental is null

exec sp_update_version '1.8.0.15', '1.8.0.16';
go

 