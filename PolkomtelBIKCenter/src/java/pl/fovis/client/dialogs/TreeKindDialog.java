/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.TreeKindBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class TreeKindDialog extends BaseActionOrCancelDialog {

    protected boolean isEdit;
    protected Map<String, TreeKindBean> treeKindBean;
    protected FlexTable grid;
    protected PushButton branchIcon;
    protected PushButton leafIcon;
    protected CheckBox allowLinkingChk;
    protected Image branchImage;
    protected Image leafImage;
    protected String branchStringIcon;
    protected String leafStringIcon;
    protected IParametrizedContinuation<TreeKindBean> con;
    protected Map<String, TreeKindTextBox> langToTreeTb = new HashMap<String, TreeKindTextBox>();
    protected Integer kindId;

    public static class TreeKindTextBox {

        public TextBox typeName;
        public TextBox branchName;
        public TextBox leafName;
    };

    @Override
    protected String getDialogCaption() {
        if (isEdit) {
            return I18n.edytujSzablonDrzewa.get() /* I18N:  */;
        } else {
            return I18n.utworzNowySzablonDrzewa.get() /* I18N:  */;
        }
    }

    @Override
    protected String getActionButtonCaption() {
        if (isEdit) {
            return I18n.zapisz.get() /* I18N:  */;
        } else {
            return I18n.utworz.get() /* I18N:  */;
        }
    }

    @Override
    protected void buildMainWidgets() {
        branchImage = new Image(BIKGWTConstants.DEFAULT_NODE_KIND_ICON_lOCATION);
        leafImage = new Image(BIKGWTConstants.DEFAULT_NODE_KIND_ICON_lOCATION);

        grid = new FlexTable();
        int column = 1;
        int row = 0;
        for (String lang : BIKClientSingletons.getLanguages()) {
            TextBox typeName = new TextBox();
            TextBox branchName = new TextBox();
            TextBox leafName = new TextBox();
            grid.setWidget(row++, column, new Label(lang));
            grid.setWidget(row++, column, typeName);
            grid.setWidget(row++, column, branchName);
            grid.setWidget(row, column, leafName);
            createTreeKindTextBox(typeName, branchName, leafName, lang);
            row = 0;
            column++;

        }

        grid.setCellSpacing(4);
        grid.setWidget(1, 0, new Label(I18n.nazwaTypuDrzewa.get() /* I18N:  */ + ":"));
        grid.setWidget(2, 0, new Label(I18n.nazwaGalezi.get() /* I18N:  */ + ":"));
        grid.setWidget(3, 0, new Label(I18n.nazwaLiscia.get() /* I18N:  */ + ":"));

        branchIcon = new PushButton(branchImage, new ClickHandler() {

            public void onClick(ClickEvent event) {
                callServiceToGetData(new StandardAsyncCallback<Set<String>>("Error in" /* I18N: no */ + " getIcons") {

                    public void onSuccess(Set<String> result) {
                        new PickIconDialog().buildAndShowDialog(branchStringIcon, result, new IParametrizedContinuation<String>() {

                            public void doIt(String param) {
                                branchStringIcon = param;
                                branchIcon.getUpFace().setImage(new Image(BIKGWTConstants.ICON_DIR_PREFIX + param + BIKGWTConstants.ICON_DIR_POSTFIX));
                            }
                        });
                    }
                });
            }
        });
        leafIcon = new PushButton(leafImage, new ClickHandler() {

            public void onClick(ClickEvent event) {
                callServiceToGetData(new StandardAsyncCallback<Set<String>>("Error in" /* I18N: no */ + " getIcons") {

                    public void onSuccess(Set<String> result) {
                        new PickIconDialog().buildAndShowDialog(leafStringIcon, result, new IParametrizedContinuation<String>() {

                            public void doIt(String param) {
                                leafStringIcon = param;
                                leafIcon.getUpFace().setImage(new Image(BIKGWTConstants.ICON_DIR_PREFIX + param + BIKGWTConstants.ICON_DIR_POSTFIX));
                            }
                        });
                    }
                });
            }
        });
        branchIcon.setStyleName("followBtn");
        branchIcon.setTitle(I18n.kliknijAbyZmienicIkoneDlaGalezi.get() /* I18N:  */);
        leafIcon.setStyleName("followBtn");
        leafIcon.setTitle(I18n.kliknijAbyZmienicIkoneDlaLiscia.get() /* I18N:  */);

        grid.setWidget(4, 0, new Label(I18n.ikonaGalezi.get() /* I18N:  */ + ":"));
        grid.setWidget(4, 1, branchIcon);
        grid.setWidget(5, 0, new Label(I18n.ikonaLiscia.get() /* I18N:  */ + ":"));
        grid.setWidget(5, 1, leafIcon);
        grid.setWidget(6, 0, allowLinkingChk = new CheckBox(I18n.pozwalajNaDodawanieDowiazan.get() /* I18N:  */));

        main.add(grid);
    }

    @Override
    protected void doAction() {
        List<TreeKindBean> tkbl = new ArrayList<TreeKindBean>();
        TreeKindBean tpl = null;

        for (Entry<String, TreeKindTextBox> t : langToTreeTb.entrySet()) {
            TreeKindBean tkb = new TreeKindBean();
            tkb.caption = t.getValue().typeName.getText().trim();
            tkb.branchCaption = t.getValue().branchName.getText().trim();
            tkb.leafCaption = t.getValue().leafName.getText().trim();
            tkb.lang = t.getKey();
            tkbl.add(tkb);
            if (tkb.lang.toLowerCase().equals(BIKGWTConstants.LANG_PL)) {
                tpl = tkb;
            }
        }
        if (tpl == null) {
            tpl = tkbl.get(0);
        }
        tpl.branchIcon = branchStringIcon != null ? branchStringIcon : BIKGWTConstants.DEFAULT_NODE_KIND_ICON_NAME;
        tpl.leafIcon = leafStringIcon != null ? leafStringIcon : BIKGWTConstants.DEFAULT_NODE_KIND_ICON_NAME;
        tpl.allowLinking = allowLinkingChk.getValue() ? 1 : 0;


        BIKClientSingletons.getService().addOrUpdateTreeType(tkbl, kindId, new StandardAsyncCallback<TreeKindBean>("Error in" /* I18N: no */ + " addOrUpdateTreeType") {

            public void onSuccess(TreeKindBean result) {
                BIKClientSingletons.showInfo(isEdit ? I18n.zmodyfikowanoTypDrzewa.get() /* I18N:  */ : I18n.dodanoTypDrzewa.get() /* I18N:  */);
                con.doIt(result);
            }
        });
    }

    protected Request callServiceToGetData(AsyncCallback<Set<String>> callback) {
        return BIKClientSingletons.getService().getIcons(callback);
    }

    public void buildAndShowDialog(Map<String, TreeKindBean> treeKindBeans, Integer id, IParametrizedContinuation<TreeKindBean> con) {
        this.isEdit = treeKindBeans != null && !treeKindBeans.isEmpty();
        this.treeKindBean = isEdit ? treeKindBeans : new HashMap<String, TreeKindBean>();
        this.con = con;
        super.buildAndShowDialog();
        if (isEdit) {
            populateWidgets(treeKindBean);
        }
        this.kindId = id;
    }

    protected void populateWidgets(Map<String, TreeKindBean> treeKindBeans) {
        boolean isFirst = true;
        TreeKindBean treeKindBeanTmp = new TreeKindBean();
        for (Entry<String, TreeKindTextBox> t : langToTreeTb.entrySet()) {
            TreeKindBean tkb = treeKindBeans.get(t.getKey().toLowerCase());
            if (tkb != null) {
                TreeKindTextBox ttb = t.getValue();
                ttb.typeName.setText(tkb.caption);
                ttb.leafName.setText(tkb.leafCaption);
                ttb.branchName.setText(tkb.branchCaption);
                t.setValue(ttb);
                if (isFirst) {
                    treeKindBeanTmp = tkb;
                    isFirst = false;
                }
            }

        }
        branchImage.setUrl(BIKGWTConstants.ICON_DIR_PREFIX + treeKindBeanTmp.branchIcon + BIKGWTConstants.ICON_DIR_POSTFIX);
        leafImage.setUrl(BIKGWTConstants.ICON_DIR_PREFIX + treeKindBeanTmp.leafIcon + BIKGWTConstants.ICON_DIR_POSTFIX);
        allowLinkingChk.setValue(treeKindBeanTmp.allowLinking == 1);
        branchStringIcon = treeKindBeanTmp.branchIcon;
        leafStringIcon = treeKindBeanTmp.leafIcon;
    }

    @Override
    protected boolean doActionBeforeClose() {

        for (Entry<String, TreeKindTextBox> tkb : langToTreeTb.entrySet()) {
            TreeKindTextBox t = tkb.getValue();
            String branchCaption = t.branchName.getValue();
            String leafCaption = t.leafName.getValue();
            if (BaseUtils.isStrEmpty(branchCaption)
                    || BaseUtils.isStrEmpty(leafCaption)
                    || BaseUtils.isStrEmpty(t.typeName.getValue())) {
                new SimpleInfoDialog().buildAndShowDialog(I18n.uzupelniNazwyDlaTypuGaleziILisci.get() /* I18N:  */, null, null);
                return false;
            }
            if (branchCaption.equals(leafCaption)) {
                new SimpleInfoDialog().buildAndShowDialog(I18n.nazwaLisciaIGaleziNieMozeBycTaka.get() /* I18N:  */, null, null);
                return false;
            }
        }

        return true;
    }

    protected void createTreeKindTextBox(TextBox typeName, TextBox branchName, TextBox leafName, String lang) {
        TreeKindTextBox tktb = new TreeKindTextBox();
        tktb.typeName = typeName;
        tktb.branchName = branchName;
        tktb.leafName = leafName;
        langToTreeTb.put(lang, tktb);
    }
}
