/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author pmielanczuk
 */
public class ShowablePasswordTextBox implements IsWidget {

    protected TextBox tb = new PasswordTextBox();
    protected CheckBox cbShowPass = new CheckBox("Show pwd");
    protected HorizontalPanel containter = new HorizontalPanel();
    protected KeyUpHandler kuh;
    protected HandlerRegistration hr;

    public ShowablePasswordTextBox() {
        containter.add(tb);
        containter.add(cbShowPass);
        cbShowPass.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                changeTb();
            }
        });
    }

    protected void setKeyUpHandler(KeyUpHandler kuh) {
        if (hr != null) {
            hr.removeHandler();
        }
        hr = kuh != null ? tb.addKeyUpHandler(kuh) : null;
        this.kuh = kuh;
    }

    protected void changeTb() {
        if (hr != null) {
            hr.removeHandler();
            hr = null;
        }
        containter.remove(0);
        String txt = getText();
        tb = cbShowPass.getValue() ? new TextBox() : new PasswordTextBox();
        tb.setText(txt);
        containter.insert(tb, 0);
        setKeyUpHandler(kuh);
    }

    public Widget asWidget() {
        return containter;
    }

    public String getText() {
        return tb.getText();
    }

    public void setText(String text) {
        tb.setText(text);
    }
}
