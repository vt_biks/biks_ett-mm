/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

/**
 *
 * @author ctran
 */
public interface BIKResources extends ClientBundle {

    public static final BIKResources INSTANCE = GWT.create(BIKResources.class);

    @Source("vis.min.js")
    public TextResource visJs();

    @Source("vis.min.css")
    public TextResource visCss();
}
