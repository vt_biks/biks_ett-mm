/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author pgajda
 */
public class ErwinRelationShapeBean implements Serializable{
    public String shapeId;
    public Integer parentConnectorPointX;
    public Integer parentConnectorPointY;
    public Integer childConnectorPointX;
    public Integer childConnectorPointY;  
    public String childShapeRef;
    public String parentShapeRef;
}
