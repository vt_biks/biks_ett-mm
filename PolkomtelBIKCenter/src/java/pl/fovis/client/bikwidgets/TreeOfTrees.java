/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.MenuExtender;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.TreeBean;

/**
 *
 * @author pgajda
 */
public class TreeOfTrees extends TreeAndNodeKindTreeCheckBox {

    protected Set<Integer> initiallyCheckedTreeIDs;

    public TreeOfTrees() {
    }

    public TreeOfTrees(Set<Integer> initiallyCheckedIDs) {
        this.initiallyCheckedTreeIDs = initiallyCheckedIDs;
    }

    @Override
    protected boolean mustHideReduntantGroups() {
        return true;
    }

    @Override
    protected MenuExtender.MenuBottomLevel whatToKeepAsLeavesInMenuTree() {
        return MenuExtender.MenuBottomLevel.NodeTrees;
    }

    @Override
    protected boolean calcCheckBoxValueForTreeIdNode(Map<String, Object> row) {
        return !areAllDescendantsOneVal(row, false);
    }

    @Override
    protected boolean isExpandAllNodes() {
        return true;
    }

    @Override
    protected boolean isInitiallyChecked(MenuNodeBean mnb) {
        if (initiallyCheckedTreeIDs == null) {
            return false;
        }

        if (initiallyCheckedTreeIDs.contains(mnb.treeId)) {
            return true;
        }

        return false;
    }

    @Override
    protected Collection<TreeBean> getTrees() {
        Collection<TreeBean> trees = new HashSet<TreeBean>();

        for (TreeBean tb : BIKClientSingletons.getTrees()) {
            if (tb.nodeKindId != null && BIKClientSingletons.isRegularUserTree(tb)) {
                trees.add(tb);
            }
        }
        
        return trees;
    }
}
