﻿exec sp_check_version '1.0.93.1';
go
--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
alter table bik_teradata drop constraint uk_teradata_object;


if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_DOC_SENDER')
 alter table aaa_global_props add SI_DOC_SENDER varchar(max) null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_NEXTRUNTIME')
 alter table aaa_global_props add SI_NEXTRUNTIME datetime null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_RECURRING')
 alter table aaa_global_props add SI_RECURRING int null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_SCHEDULE_STATUS')
 alter table aaa_global_props add SI_SCHEDULE_STATUS int null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_STARTTIME')
 alter table aaa_global_props add SI_STARTTIME datetime null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_ENDTIME')
 alter table aaa_global_props add SI_ENDTIME datetime null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_LAST_RUN_TIME')
 alter table aaa_global_props add SI_LAST_RUN_TIME datetime null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_LAST_SUCCESSFUL_INSTANCE_ID')
 alter table aaa_global_props add SI_LAST_SUCCESSFUL_INSTANCE_ID int null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_READ_ONLY')
 alter table aaa_global_props add SI_READ_ONLY int null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_WEBI_DOC_PROPERTIES')
 alter table aaa_global_props add SI_WEBI_DOC_PROPERTIES varchar(max) null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_CONTENT_LOCALE')
 alter table aaa_global_props add SI_CONTENT_LOCALE varchar(max) null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_HAS_CHILDREN')
 alter table aaa_global_props add SI_HAS_CHILDREN int null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_SIZE')
 alter table aaa_global_props add SI_SIZE int null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_global_props') and name = 'SI_AUTHOR')
 alter table aaa_global_props add SI_AUTHOR varchar(max) null;
go


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_sapbo_extradata]
GO

create procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
as
begin
	delete from bik_sapbo_extradata

	insert into bik_sapbo_extradata (node_id,author,owner,keyword,file_path,file_name,created,modified,cuid,files_value,size,has_children,guid,instance,owner_id,progid,obtype,flags,children,ruid,runnable_object,content_locale,is_schedulable,webi_doc_properties,read_only,last_successful_instance_id,last_run_time,timestamp,progid_machine,endtime,starttime,schedule_status,recurring,nextruntime,doc_sender,revisionnum,application_object,shortname,dataconnection_total,universe_total )
    select bik.id as node_id,
    nullif(ltrim(rtrim(iw.SI_AUTHOR)), '') as author,
    nullif(ltrim(rtrim(p.SI_OWNER)), '') as owner,
    nullif(ltrim(rtrim(p.SI_KEYWORD)), '') as keyword,
    nullif(ltrim(rtrim(p.SI_FILES__SI_PATH)), '') as file_path,
    nullif(ltrim(rtrim(p.SI_FILES__SI_FILE1)), '') as file_name,
    nullif(ltrim(rtrim(p.SI_CREATION_TIME)), '') as created,
    nullif(ltrim(rtrim(p.SI_UPDATE_TS)), '') as modified,
    nullif(ltrim(rtrim(p.SI_CUID)), '') as cuid,
    nullif(ltrim(rtrim(p.SI_FILES__SI_VALUE1)), '') as files_value,
    nullif(ltrim(rtrim(p.SI_SIZE)), '') as size,
    p.SI_HAS_CHILDREN as has_children,
    nullif(ltrim(rtrim(p.SI_GUID)), '') as guid,
    p.SI_INSTANCE as instance,
    p.SI_OWNERID as owner_id,
    nullif(ltrim(rtrim(p.SI_PROGID)), '') as progid,
    p.SI_OBTYPE as obtype,
    p.SI_FLAGS as flags,
    p.SI_CHILDREN as children,
    nullif(ltrim(rtrim(p.SI_RUID)), '') as ruid,
    p.SI_RUNNABLE_OBJECT as runnable_object,
    nullif(ltrim(rtrim(p.SI_CONTENT_LOCALE)), '') as content_locale,
    p.SI_IS_SCHEDULABLE as is_schedulable,
    nullif(ltrim(rtrim(p.SI_WEBI_DOC_PROPERTIES)), '') as webi_doc_properties,
    p.SI_READ_ONLY as read_only,
    nullif(ltrim(rtrim(p.SI_LAST_SUCCESSFUL_INSTANCE_ID)), '') as last_successful_instance_id,
    nullif(ltrim(rtrim(p.SI_LAST_RUN_TIME)), '') as last_run_time,
    nullif(ltrim(rtrim(iw.SI_TIMESTAMP)), '')  as timestamp,
    nullif(ltrim(rtrim(iw.SI_PROGID_MACHINE)), '') as progid_machine,
    nullif(ltrim(rtrim(p.SI_ENDTIME)), '') as endtime,
    nullif(ltrim(rtrim(p.SI_STARTTIME)), '') as starttime,
    nullif(ltrim(rtrim(p.SI_SCHEDULE_STATUS)), '') as schedule_status,
    nullif(ltrim(rtrim(p.SI_RECURRING)), '') as recurring,
    nullif(ltrim(rtrim(p.SI_NEXTRUNTIME)), '') as nextruntime,
    nullif(ltrim(rtrim(p.SI_DOC_SENDER)), '') as doc_sender,
    au.SI_REVISIONNUM as revisionnum,
    au.SI_APPLICATION_OBJECT as application_object,
    nullif(ltrim(rtrim(au.SI_SHORTNAME)), '') as shortname,
    au.SI_DATACONNECTION__SI_TOTAL as dataconnection_total,
    iw.SI_UNIVERSE__SI_TOTAL as universe_total
    from aaa_global_props p
    left join INFO_WEBI iw on p.si_id = iw.si_id
    left join APP_UNIVERSE au on p.si_id = au.si_id
    inner join bik_node bik on convert(varchar,p.si_id) = bik.obj_id
    order by bik.id
end
GO
--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
exec sp_update_version '1.0.93.1', '1.0.93.2';
go