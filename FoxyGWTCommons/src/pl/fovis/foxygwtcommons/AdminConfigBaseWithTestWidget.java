/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public abstract class AdminConfigBaseWithTestWidget<T> extends AdminConfigBaseWidget<T> {

    public static final int TEST_CONNECTION_SUCCESS = 0;
    public static final int TEST_CONNECTION_FAIL = 1;
    protected HTML resultTest;
    protected TextBox tbObjectsForGrantsTest;

    protected boolean isGrantsTestPerObjectAvailable() {
        return false;
    }

    protected String getOptObjsForGrantsTesting() {
        return tbObjectsForGrantsTest == null ? null : tbObjectsForGrantsTest.getValue();
    }

    protected void createTestConnectionWidget(String name, ClickHandler clickHandler, CellPanel cp) {
        PushButton button = new PushButton(name);
        NewLookUtils.makeCustomPushButton(button);
        button.addClickHandler(clickHandler);
        createLine(cp);

        Widget w = button;

        if (isGrantsTestPerObjectAvailable()) {
            HorizontalPanel hp = new HorizontalPanel();
            w = hp;
            hp.add(button);
            tbObjectsForGrantsTest = new TextBox();
            hp.add(tbObjectsForGrantsTest);
            hp.add(new Label("First char is separator declaration!!!"));
        }

        cp.add(w);
//        cp.add(button);
        cp.add(resultTest);
    }

    protected abstract void showSuccessTestInfo();

    protected abstract void showErrorTestInfo();

    protected void setResultTestConnection(Pair<Integer, String> result) {
        resultTest.setHTML(BaseUtils.encodeForHTMLWithNewLinesAsBRs(result.v2));
        if (result.v1 == TEST_CONNECTION_SUCCESS) {
            showSuccessTestInfo();
        } else {
            showErrorTestInfo();
        }
    }

    protected void createTestConnectionWidget(String name, ClickHandler clickHandler) {
        createTestConnectionWidget(name, clickHandler, main);
    }

    @Override
    public final Widget buildWidget() {
        resultTest = new HTML();
        return super.buildWidget();
    }
}
