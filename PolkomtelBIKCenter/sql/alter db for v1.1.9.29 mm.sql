﻿exec sp_check_version '1.1.9.29';
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_add_missing_users_in_roles_for_existing_objs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_add_missing_users_in_roles_for_existing_objs]
GO

create procedure sp_add_missing_users_in_roles_for_existing_objs
as
begin  
  set nocount on
  
    declare @tree_id int = (select id from bik_tree where code='UserRoles')
    declare @rc int

    --ww: to jest kod Beaty (przeniesiony z adHocSqls.html), miał błąd - generuje [TSB:8], 
    -- bo dodaje też użytkowników w rolach gdy oryginalny użytkownik jest usunięty
	--
	/*
    insert into bik_node (parent_node_id, node_kind_id, name, tree_id, linked_node_id)
    select x.role_id2, (select id from bik_node_kind where code='User'), x.name, @tree_id, x.user_id
    from (select bu.node_id as user_id,brfn.node_id as role_id2,bu.name from bik_user_in_node buin
    join bik_role_for_node brfn on buin.role_for_node_id=brfn.id
    join bik_user bu on bu.id=buin.user_id
    group by bu.node_id,brfn.node_id,bu.name
    )x left join (select linked_node_id,parent_node_id as role_id,name from bik_node where is_deleted=0
    and tree_id=@tree_id and linked_node_id is not null 
    group by linked_node_id,parent_node_id ,name) y
    on (y.role_id=x.role_id2  and x.user_id=y.linked_node_id )
    where y.linked_node_id is null or x.user_id is null
	--*/
	
	--mm: trzeba dodać distinct, ponieważ możemy dodać kilka powiązań w nowej roli naraz
	-- wtedy inner join bik_user_in_node zwielokrotni nam rekordy, a tym samym dostaniemy
	-- za dużo podlinkowanych użytkowników do jednej roli (zamiast jednego)
	--ww: powyższe było zbyt zagmatwane, ciężko się połapać, napiszę to prościej, od razu
	-- ze stosowną poprawką do [TSB:8]	
	-- generalnie są 3 zagadnienia, na które trzeba uważać
	-- 1. obiekt powiązany z użytkownikiem w roli jest skasowany
	-- 2. rola jest skasowana (a powiązanie user/obj w tej roli jest)
	-- 3. użytkownik jest skasowany (oryginał)
	declare @user_node_kind_id int = (select id from bik_node_kind where code='User')

	insert into bik_node (parent_node_id, node_kind_id, name, tree_id, linked_node_id)
	select distinct roln.id, @user_node_kind_id, u.name, @tree_id, orgn.id
	from
	bik_user u inner join
	bik_node orgn on orgn.id = u.node_id inner join
	bik_user_in_node uin on u.id = uin.user_id inner join 
	bik_node objn on uin.node_id = objn.id inner join
	bik_role_for_node rfn on uin.role_for_node_id = rfn.id inner join
	bik_node roln on roln.id = rfn.node_id left join
	bik_node un on un.linked_node_id = u.node_id and un.parent_node_id = rfn.node_id
	  and un.is_deleted = 0
	where orgn.is_deleted = 0 and objn.is_deleted = 0 and roln.is_deleted = 0 and un.id is null
	
    set @rc = @@ROWCOUNT

    if @rc > 0 begin
		-- ww: poprawia branch_ids tam gdzie trzeba - dla nowo dodanych
		-- kod powyżej doda chyba tylko jednego (tak wynika z wywołań), ale
		-- skoro jest napisany w ogólności (że niby może dodać wiele użytkowników
		-- w różnych rolach) to tutaj też stosujemy ogólne podejście
		exec sp_node_init_missing_branch_ids_of_users
    end

    return @rc
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_add_missing_users_in_roles_for_existing_objs
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

------------------------------------------------------------------
--- poprawka do [TSB-FIX:24]--------------------------------------
------------------------------------------------------------------

declare @tree_id int =  (select id from bik_tree where code = 'UserRoles')
declare @node_kind int = (select id from bik_node_kind where code = 'User')
declare @tab table (role_node_id int, user_node_id int, max_linked_node_id int )

insert into @tab
select  br.id as role_node_id, bu.id as user_node_id, max(bn.id) as max_linked_node_id
	from bik_node bn
	join bik_node br on bn.parent_node_id = br.id
	join bik_node bu on bn.linked_node_id = bu.id
	where br.is_deleted = 0 and 
	bu.is_deleted = 0 and bn.is_deleted = 0
	and br.tree_id = @tree_id
	and bu.node_kind_id = @node_kind
	group by  br.name, br.id, bu.name, bu.id 
	having count(*)>1 
	
update bik_node set is_deleted = 1 
from (select  br.id as role_node_id, bu.id as user_node_id
			from bik_node bn
			join bik_node br on bn.parent_node_id = br.id
			join bik_node bu on bn.linked_node_id = bu.id
			where br.is_deleted = 0 and bu.is_deleted = 0 and bn.is_deleted = 0
			and br.tree_id = @tree_id
			and bu.node_kind_id = @node_kind
			group by br.id, bu.id
			having count(bn.id)>1) x 
where bik_node.linked_node_id = x.user_node_id and bik_node.parent_node_id = x.role_node_id

update bik_node set is_deleted = 0 
	where bik_node.id in (select max_linked_node_id from @tab)
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_update_version '1.1.9.29', '1.1.9.30';
go