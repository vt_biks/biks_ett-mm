/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

/**
 *
 * @author wezyr
 */
public interface IBeanConnectionThreadHandle {

    public void cancelExecution();

    public void clearCancelFlag();

    public boolean isExecutionCanceled();
}
