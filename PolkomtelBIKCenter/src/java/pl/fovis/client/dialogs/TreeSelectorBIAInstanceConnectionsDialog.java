/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import java.util.HashSet;
import java.util.Set;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class TreeSelectorBIAInstanceConnectionsDialog extends TreeSelectorBIAInstanceDialog {


    @Override
    protected BikNodeTreeOptMode getTreeOptExtraMode() {
        return BikNodeTreeOptMode.SAPBOReportConnection;
    }

    @Override
    protected String getDialogCaption() {
        return I18n.scheduleSelectConnections.get();
    }

    @Override
    protected Set<String> nodeKindsToCreateWidgetAsCheckBox() {
        Set<String> l = new HashSet<String>();
        l.add(BIKConstants.NODE_KIND_DATACONNECTION);
        l.add(BIKConstants.NODE_KIND_CONNECTION_OLAP);
        l.add(BIKConstants.NODE_KIND_CONNECTION_RELATIONAL);

        return l;
    }
}
