/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.ui.IsWidget;

/**
 *
 * @author wezyr
 */
public interface IResizeSensitiveWidget extends IsWidget {
    public void setFixedHeight(int height);
}
