/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import java.io.StringReader;
import java.util.Properties;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.foxys.foxymill.FoxyMillNamedTemplateProvider;
import pl.trzy0.foxy.commons.templates.INamedTemplateProvider;
import pl.trzy0.foxy.serverlogic.TextProviderFromProperties;
import pl.trzy0.foxy.serverlogic.db.datapump.MssqlDataPumpDestination;
import simplelib.IContinuation;

/**
 *
 * @author pmielanczuk
 */
public abstract class AbstractTestExecutorBase implements ITestExecutor {

    protected IBIKSDQMSubservice service;
    protected DQMMultiSourceBean source;
    protected int dataReadSecs;

    public AbstractTestExecutorBase(IBIKSDQMSubservice service) {
        this.service = service;
    }

    protected abstract String getTypeMappingStr();

    protected abstract int performPumpingData(INamedTemplateProvider typeMappingTemplates, String tmpTableName, String optTestGrantsPerObjects,boolean isMultiSource);

    protected INamedTemplateProvider prepareTypeMappingTemplates() {
        final String typeMappingStr = getTypeMappingStr();

        Properties typeMappingProps = new Properties();
        try {
            typeMappingProps.load(new StringReader(typeMappingStr));
        } catch (Exception ex) {
            throw new RuntimeException("error reading typeMapping props file, typeMapping=" + typeMappingStr, ex);
        }

        TextProviderFromProperties typeMappingTexts = new TextProviderFromProperties(typeMappingProps, (String) null);
        return new FoxyMillNamedTemplateProvider(typeMappingTexts);
    }

    @Override
    public int pumpDataToBiksDatabase() {
        final INamedTemplateProvider typeMappingTemplates = prepareTypeMappingTemplates();

        service.execInAutoCommitMode(new IContinuation() {

            @Override
            public void doIt() {
                dataReadSecs = performPumpingData(typeMappingTemplates, source.biksTableName, source.optTestGrantsPerObjects,true);
            }
        });

        return dataReadSecs;
    }

    @Override
    public int pumpErrorDataToBiksDatabase(final String tmpTableName) {
        final INamedTemplateProvider typeMappingTemplates = prepareTypeMappingTemplates();

        service.execInAutoCommitMode(new IContinuation() {

            @Override
            public void doIt() {
                dataReadSecs = performPumpingData(typeMappingTemplates, tmpTableName, null,true); 
            }
        });
        return dataReadSecs;
    }

    protected MssqlDataPumpDestination createDataPumpDestination(INamedTemplateProvider typeMappingTemplates) {
        return createDataPumpDestination(typeMappingTemplates, source.biksTableName);
    }

    protected MssqlDataPumpDestination createDataPumpDestination(INamedTemplateProvider typeMappingTemplates, String pumpTableName) {
        return new MssqlDataPumpDestination(service.getConnection(), typeMappingTemplates, pumpTableName);
    }
}
