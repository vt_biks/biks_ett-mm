/*
Missing Index Details from SQLQuery1.sql - fando-asus\sqlexpress.biks_polkomtel_2011_11_29 (sa (53))
The Query Processor estimates that implementing the following index could improve the query cost by 99.3507%.
*/

/*
USE [biks_polkomtel_2011_11_29]
GO
CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>]
ON [dbo].[bik_searchable_attr_val] ([node_id])

GO
*/


CREATE unique INDEX idx_bik_searchable_attr_val_node_attr_ids
ON [dbo].[bik_searchable_attr_val] ([node_id], attr_id)
