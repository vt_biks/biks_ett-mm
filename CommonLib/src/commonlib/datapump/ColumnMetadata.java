/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.datapump;

/**
 *
 * @author pmielanczuk
 */
public class ColumnMetadata {

    public String colName;
    public String typeName;
    public int displaySize;
    public int prec;
    public int scale;
    public Boolean isNullable; // null -> nullability unknown
    public int colIdx;

    public ColumnMetadata(String colName, String typeName, int displaySize, int prec, int scale, Boolean isNullable, int colIdx) {
        this.colName = colName;
        this.typeName = typeName;
        this.displaySize = displaySize;
        this.prec = prec;
        this.scale = scale;
        this.isNullable = isNullable;
        this.colIdx = colIdx;
    }

    @Override
    public String toString() {
        return '{' + "colName=" + colName + ", typeName=" + typeName + ", displaySize=" + displaySize + ", prec=" + prec + ", scale=" + scale + ", isNullable=" + isNullable + ", colIdx=" + colIdx + '}';
    }
}
