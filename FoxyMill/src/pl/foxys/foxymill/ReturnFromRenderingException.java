package pl.foxys.foxymill;

/**
 *
 * @author wezyr
 */
public class ReturnFromRenderingException extends FoxyMillRenderingFallThroughException {

    public boolean abortRendering;

    public ReturnFromRenderingException(boolean abortRendering) {
        this.abortRendering = abortRendering;
    }
}
