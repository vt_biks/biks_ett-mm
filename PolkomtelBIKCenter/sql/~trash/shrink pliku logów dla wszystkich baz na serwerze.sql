select 'select ''' + name + ''' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from ' + name + '.sys.database_files where type = 1 union all ' from sys.databases

set nocount on

select 'use ' + name + '
go
declare @logfile_name sysname = (select name from ' + name + '.sys.database_files where type = 1)
exec(''DBCC SHRINKFILE ('' + @logfile_name + '', 1)'')
go
' from sys.databases
where name not in ('master', 'model', 'msdb')


select * from master.sys.database_files

select 'master' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from master.sys.database_files where type = 1 union all 
select 'tempdb' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from tempdb.sys.database_files where type = 1 union all 
select 'model' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from model.sys.database_files where type = 1 union all 
select 'msdb' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from msdb.sys.database_files where type = 1 union all 
select 'boxi' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from boxi.sys.database_files where type = 1 union all 
select 'boxi_polkomtel2' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from boxi_polkomtel2.sys.database_files where type = 1 union all 
select 'biks_boug_demo' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from biks_boug_demo.sys.database_files where type = 1 union all 
select 'biks_boug_demo_r2' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from biks_boug_demo_r2.sys.database_files where type = 1 union all 
select 'Share_PO' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from Share_PO.sys.database_files where type = 1 union all 
select 'szkolenie_sql' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from szkolenie_sql.sys.database_files where type = 1 union all 
select 'biks_polkomtel_2011_10_10_orig' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from biks_polkomtel_2011_10_10_orig.sys.database_files where type = 1 union all 
select 'biks_polkomtel_2011_10_27' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from biks_polkomtel_2011_10_27.sys.database_files where type = 1 union all 
select 'Share_PO_2011_10_25' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from Share_PO_2011_10_25.sys.database_files where type = 1 union all 
select 'biks_polkomtel_2011_11_29' as db_name, cast(size as bigint) * 8196 as size_in_bytes, name as logfile_name from biks_polkomtel_2011_11_29.sys.database_files where type = 1



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
use tempdb
go
declare @logfile_name sysname = (select name from sys.database_files where type = 1)
exec('DBCC SHRINKFILE (' + @logfile_name + ', 1)')
go
use biks_boug_demo
go
declare @logfile_name sysname = (select name from biks_boug_demo.sys.database_files where type = 1)
exec('DBCC SHRINKFILE (' + @logfile_name + ', 1)')
go
use biks_boug_demo_r2
go
declare @logfile_name sysname = (select name from biks_boug_demo_r2.sys.database_files where type = 1)
exec('DBCC SHRINKFILE (' + @logfile_name + ', 1)')
go
use biks_polkomtel_2011_10_10_orig
go
declare @logfile_name sysname = (select name from biks_polkomtel_2011_10_10_orig.sys.database_files where type = 1)
exec('DBCC SHRINKFILE (' + @logfile_name + ', 1)')
go
use biks_polkomtel_2011_10_27
go
declare @logfile_name sysname = (select name from biks_polkomtel_2011_10_27.sys.database_files where type = 1)
exec('DBCC SHRINKFILE (' + @logfile_name + ', 1)')
go
use biks_polkomtel_2011_11_29
go
declare @logfile_name sysname = (select name from biks_polkomtel_2011_11_29.sys.database_files where type = 1)
exec('DBCC SHRINKFILE (' + @logfile_name + ', 1)')
go
use boxi
go
declare @logfile_name sysname = (select name from boxi.sys.database_files where type = 1)
exec('DBCC SHRINKFILE (' + @logfile_name + ', 1)')
go
use boxi_polkomtel2
go
declare @logfile_name sysname = (select name from boxi_polkomtel2.sys.database_files where type = 1)
exec('DBCC SHRINKFILE (' + @logfile_name + ', 1)')
go
use Share_PO
go
declare @logfile_name sysname = (select name from Share_PO.sys.database_files where type = 1)
exec('DBCC SHRINKFILE (' + @logfile_name + ', 1)')
go
use Share_PO_2011_10_25
go
declare @logfile_name sysname = (select name from Share_PO_2011_10_25.sys.database_files where type = 1)
exec('DBCC SHRINKFILE (' + @logfile_name + ', 1)')
go
use szkolenie_sql
go
declare @logfile_name sysname = (select name from szkolenie_sql.sys.database_files where type = 1)
exec('DBCC SHRINKFILE (' + @logfile_name + ', 1)')
go

