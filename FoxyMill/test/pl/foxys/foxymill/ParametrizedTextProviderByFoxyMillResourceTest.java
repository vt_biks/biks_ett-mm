/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill;

import simplelib.RawString;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import simplelib.ConstantResourceFromProvider;
import static org.junit.Assert.*;

/**
 *
 * @author wezyr
 */
public class ParametrizedTextProviderByFoxyMillResourceTest {

    public ParametrizedTextProviderByFoxyMillResourceTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     * Test of provide method, of class ParametrizedTextProviderByFoxyMillResource.
     */
    @Test
    public void testProvide() {
        ParametrizedTextProviderByFoxyMillResource xxx = new ParametrizedTextProviderByFoxyMillResource(
                new ConstantResourceFromProvider<String>("<f:set var='yyy' params='uuu'>%{uuu}%%{uuu}%</f:set>"),
                FoxyMillUtils.makeRenderEnvForHTML(true, true), null);

        assertEquals("1010", xxx.provide("yyy", 10));

        xxx = new ParametrizedTextProviderByFoxyMillResource(
                new ConstantResourceFromProvider<String>("<f:set var='yyy' params='uuu,vvv'>%{uuu}%%{vvv+10}%%{uuu}%</f:set>"),
                FoxyMillUtils.makeRenderEnvForHTML(true, true), null);

        //System.out.println("res=" + xxx.provide("yyy", "<br/>", 10));

        assertEquals("&lt;br/&gt;20&lt;br/&gt;", xxx.provide("yyy", "<br/>", 10));

        assertEquals("<br/>22<br/>", xxx.provide("yyy", new RawString("<br/>"), 12));
    }
}
