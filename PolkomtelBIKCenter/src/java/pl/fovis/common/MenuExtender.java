/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.BIKCenterUtils.MenuNodeBeanType;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GenericTreeHelper;
import pl.fovis.foxygwtcommons.ITreeBroker;
import pl.fovis.foxygwtcommons.TreeAlgorithms;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public class MenuExtender {

    public static enum MenuBottomLevel {

        NodeKinds, BikPages, BiksPagesOrGroups, NodeTrees
    };

    public static void prettyPrintMenuNodes(String label, List<MenuNodeBean> menuNodes) {
        System.out.println("prettyPrintMenuNodes: ### " + label + " ###");
        GenericTreeHelper<MenuNodeBean, Integer> menuNodeHelper = new GenericTreeHelper<MenuNodeBean, Integer>(new ITreeBroker<MenuNodeBean, Integer>() {
            public Integer getNodeId(MenuNodeBean n) {
                return n.id;
            }

            public Integer getParentId(MenuNodeBean n) {
                return n.parentNodeId;
            }
        }, menuNodes);

        TreeAlgorithms.prettyPrintTree(menuNodeHelper, 0, new BaseUtils.Projector<MenuNodeBean, String>() {
            public String project(MenuNodeBean val) {
                return "[" + val.code + "] " + val.name;
            }
        });
    }

    public static int getNextNewId(List<MenuNodeBean> menuNodes) {
        int nextNewId = -1;

        for (MenuNodeBean mnb : menuNodes) {
            if (mnb.id >= nextNewId) {
                nextNewId = mnb.id;
            }
        }

        return nextNewId + 1;
    }

    //ww: MenuBottomLevel keepAsLeaves - co może być liściem w menu (najniższy
    // poziom):
    // NodeKinds - węzły nodeKindowe '&',
    // BikPages - strony biksa (drzewa '$' i zakładki zakodowane '#'),
    // BiksPagesOrGroups - strony albo grupy menu
    // NodeTrees - tylko drzewa
    public static List<MenuNodeBean> extend(List<MenuNodeBean> menuNodes,
            Collection<TreeBean> trees,
            Map<String, NodeKindBean> nodeKindsByCodeMap,
            boolean expandDynamicTrees, MenuBottomLevel keepAsLeaves,
            boolean addUltimateRootNode,
            boolean hideReduntantGroups) {
//        prettyPrintMenuNodes("przed prepare", menuNodes);

        List<MenuNodeBean> res = new ArrayList<MenuNodeBean>();

        int nextNewId = getNextNewId(menuNodes);

        Integer rootNodeId;

        if (addUltimateRootNode) {
            rootNodeId = nextNewId++;
            res.add(new MenuNodeBean(rootNodeId, I18n.wszystkie.get() /* I18N:  */, null, null));
        } else {
            rootNodeId = null;
        }

        Map<String, MenuNodeBean> menuByCodeMap = new LinkedHashMap<String, MenuNodeBean>();
        //ww: uwaga - w childNodesMap jest trzymana oryginalna struktura,
        // to jest ważne dla węzłów @treeKind (np. taksonomie) - drzewa
        // dynamiczne, które w Modrzewiu mają szablon, który potem jest
        // podmieniany na konkretne drzewa tego typu (tree_kind), z
        // kopiowaniem węzłów do każdego z tych drzew. wtedy childNodes
        // zawiera nieświeżą informację!
        Map<Integer, List<MenuNodeBean>> childNodesMap = new HashMap<Integer, List<MenuNodeBean>>();

        //Collection<TreeBean> trees = BIKClientSingletons.getTrees();
//        Set<String> treeCodes = new HashSet<String>();
//        Set<String> treeKinds = new HashSet<String>();
//        for (TreeBean t : trees) {
//            treeCodes.add(t.code);
//            treeKinds.add(t.treeKind);
//        }
        Set<Integer> idsToClear = new HashSet<Integer>();

        //ww: inicjalne przejście po menu, tu dopiero budują się
        // struktury, np. childNodesMap, dopiero po tej pętli
        // będzie łatwo odnajdywać dzieci węzłów itp.
        for (MenuNodeBean mnb : menuNodes) {
            String code = mnb.code;
            MenuNodeBeanType typeChar = BIKCenterUtils.getMenuNodeBeanType(mnb); //BaseUtils.isStrEmpty(code) ? ' ' : code.charAt(0);
            if (keepAsLeaves != MenuBottomLevel.NodeKinds && typeChar == MenuNodeBeanType.NodeKind
                    || keepAsLeaves == MenuBottomLevel.NodeKinds && typeChar == MenuNodeBeanType.CustomPage) {
                // nie dodajemy typów węzłow gdy nie mają być liśćmi w menu
                // nie dodajemy stałych zakładek jeżeli liśćmi mają być typy
                // węzłów
                continue;
            }

            MenuNodeBean copy = makeMNBCopy(mnb, rootNodeId);

            menuByCodeMap.put(code, copy);
            addToResAndChildNodesMap(copy, res, childNodesMap);

//            //ww: zapamiętaj pozycję w menu do skasowania - nie ma drzewa o
//            // takim kodzie (typ menu '$') albo rodzaju (treeKind dla typu menu
//            // '@' - dynamiczne drzewa)
//            if (!BaseUtils.isStrEmpty(code)) {
//                String typeName = code.substring(1);
//                if (typeChar == '@' && !treeKinds.contains(typeName)
//                        || typeChar == '$' && !treeCodes.contains(typeName)) {
//                    //menuItemsToClear.add(copy);
//                    idsToClear.add(copy.id);
//                }
//            }
        }

        for (TreeBean tb : trees) {
            MenuNodeBean mnb = menuByCodeMap.get("$" + tb.code);
            if (mnb != null) {
                mnb.treeId = tb.id;
                if (BaseUtils.safeEquals(mnb.name, "@")) {
                    mnb.name = tb.name;
                }
            }

            MenuNodeBean template = menuByCodeMap.get("@" + tb.treeKind);
            if (template != null) {
                if (expandDynamicTrees) {

                    if (mnb == null) {
                        //ww: po nowemu - nie ma drzewa podczepionego gdzieś, to
                        // go nie będzie widać
                        // to tak działać ma od v1.1.9.14
                        continue;

//                        //ww: jeżeli nie jest zaczepione w innym miejscu...
//                        // dorzucanie pod kategoryzacje lub inne drzewa dynamiczne
//                        // (też np. słowniki)
//                        mnb = new MenuNodeBean(nextNewId++, tb.name, template.id, "$" + tb.code);
//                        mnb.treeId = tb.id;
//                        res.add(mnb);
                    }
//                    else {
//                        Window.alert("dynamiczne i podpięte: " + mnb.code + ", " + tb.treeKind);
//                    }

                    //ww: a teraz węzły nodeKindowe - z szablonu (węzła @treeKind)
                    // kopiujemy pod to drzewko (bez względu czy podczepione
                    // gdzieś jawnie czy właśnie dodane pod szablon)
                    List<MenuNodeBean> children = childNodesMap.get(template.id);
                    nextNewId += makeMNBCopiesWithChildren(children, mnb.id,
                            childNodesMap, nextNewId, res, MenuNodeBeanType.NodeKind);

//                    //ww: kształt węzłów zostanie usunięty (czyli leżące pod szablonem węzły &nodeKind)
//                    if (children != null) {
//                        for (MenuNodeBean templateChild : children) {
//                            if (getMenuNodeBeanType(templateChild) == '&') {
//                                idsToClear.add(templateChild.id);
//                            }
//                        }
//                    }
                } else {
                    //ww: węzeł mnb (o ile jest) musi być wyeliminowany,
                    // bo to węzeł drzewa dynamicznego, a mamy
                    // expandDynamicTrees == false (pomijaj drzewa dynamiczne)
                    if (mnb != null) {
                        idsToClear.add(mnb.id);
                    }
                }
            }
        }

        //ww: usuwamy z menu pozycje nodeKindowe ('&'), które są pod
        // drzewiastymi ('$') bez treeId oraz szablonowymi ('@')
        for (MenuNodeBean mnb : res) {
            MenuNodeBeanType type = BIKCenterUtils.getMenuNodeBeanType(mnb);
            if (type == MenuNodeBeanType.Tree && mnb.treeId == null
                    || type == MenuNodeBeanType.TreeTemplate && (expandDynamicTrees || keepAsLeaves != MenuBottomLevel.NodeKinds)) {
                addDescendantMenuItemsIdsToClear(mnb.id, idsToClear,
                        childNodesMap, MenuNodeBeanType.NodeKind);
            }
        }

        if (hideReduntantGroups) {

            Map<MenuNodeBean, Integer> repositions = new HashMap<MenuNodeBean, Integer>();

            for (int i = 0; i < res.size(); i++) {
                MenuNodeBean mnb = res.get(i);

                MenuNodeBeanType type = BIKCenterUtils.getMenuNodeBeanType(mnb);
                if (type != MenuNodeBeanType.TreeTemplate) {
                    continue;
                }

                List<MenuNodeBean> children = childNodesMap.get(mnb.id);

                if (BaseUtils.collectionSizeFix(children) != 1) {
                    continue;
                }

                MenuNodeBean oneAndOnlyChild = children.get(0);
                if (!BaseUtils.safeEquals(oneAndOnlyChild.name, mnb.name)) {
                    continue;
                }

                oneAndOnlyChild.parentNodeId = mnb.parentNodeId;
                idsToClear.add(mnb.id);
                children.clear();

                repositions.put(oneAndOnlyChild, i);
            }

            //ww: tak jest źle :-(
//            for (int i = 0, j = 0; j < res.size(); j++) {
//                MenuNodeBean mnb = res.get(j);
//
//                Integer reposition = repositions.get(mnb);
//
//                if (reposition != null) {
//                    res.set(reposition, mnb);
//                } else {
//                    res.set(i++, mnb);
//                }
//            }
            if (!repositions.isEmpty()) {

                for (int i = 0; i < res.size(); i++) {
                    MenuNodeBean mnb = res.get(i);

                    Integer reposition = repositions.get(mnb);

                    if (reposition != null) {
                        res.set(reposition, mnb);
                        res.set(i, null);
                        repositions.remove(mnb);
                    }
                }

                int properIdx = 0;

                for (int i = 0; i < res.size(); i++) {
                    MenuNodeBean mnb = res.get(i);

                    if (mnb == null) {
                        continue;
                    }

                    if (i != properIdx) {
                        res.set(properIdx, mnb);
                    }

                    properIdx++;
                }

                res = res.subList(0, properIdx);
            }
        }

        //ww: konieczne przepisanie na inną listę, bo inaczej będzie błąd
        // iteratora - jednoczesne chodzenie po kolekcji i jej modyfikowanie
        List<Integer> idsToClearHelp = new ArrayList<Integer>(idsToClear);

        for (Integer id : idsToClearHelp) {
            //ww: poniższe dopisuje do listy idsToClear, więc nie można po niej
            // chodzić w tej pętli
            addDescendantMenuItemsIdsToClear(id, idsToClear, childNodesMap, null);
        }

        for (MenuNodeBean mnb : res) {
            if (idsToClear.contains(mnb.id) || mnb.code == null || !mnb.code.startsWith("&")) {
                continue;
            }

            String nodeKindCode = mnb.code.substring(1);
            nodeKindCode = BaseUtils.splitString(nodeKindCode, "$").v1; // obetnie końcówkę

            NodeKindBean nkb = nodeKindsByCodeMap.get(nodeKindCode);
            //BIKClientSingletons.getNodeKindBeanByCode(nodeKindCode);
            if (nkb == null) {
                continue;
            }

            mnb.name = nkb.caption;
            mnb.nodeKindCode = nkb.code;//childNodesMap.get(mnb.id)==null?"#":""+
            mnb.nodeKindId = nkb.id;
        }

        //ww: przyda nam się mapa z ID menuItema na beana menuItema
        Map<Integer, MenuNodeBean> idToMenuItemMap = BaseUtils.makeBeanMap(res);

        Set<Integer> goodMenuItemIds = new HashSet<Integer>();
        for (MenuNodeBean mnb : res) {
            //String code = mnb.code;
            MenuNodeBeanType type = BIKCenterUtils.getMenuNodeBeanType(mnb);// code.charAt(0);
//            if (type == MenuNodeBeanType.Group) { //ww: spacja oznacza pusty code
//                continue;
//            }
            if (keepAsLeaves == MenuBottomLevel.NodeKinds && type == MenuNodeBeanType.NodeKind && mnb.nodeKindId != null
                    || keepAsLeaves == MenuBottomLevel.BikPages && (type == MenuNodeBeanType.CustomPage || type == MenuNodeBeanType.Tree && mnb.treeId != null)
                    || keepAsLeaves == MenuBottomLevel.NodeTrees && type == MenuNodeBeanType.Tree && mnb.treeId != null
                    || keepAsLeaves == MenuBottomLevel.BiksPagesOrGroups && type != MenuNodeBeanType.NodeKind && (type != MenuNodeBeanType.Tree || mnb.treeId != null)) {

                //ww: ta pozycja w menu jest dobra - dobry typ, więc idziemy w górę
                // i oznaczamy całą gałąź, że dobra - tzn. wszystkie pozycje
                // menu w górę dorzucamy do goodMenuItemIds (o ile nie są na
                // kolekcji idsToClear)
                MenuNodeBean m = mnb;
                //ww: dodawanie do Seta zwraca false jak już ten element w secie
                // jest
                while (m != null && !idsToClear.contains(m.id) && goodMenuItemIds.add(m.id)) {
                    m = idToMenuItemMap.get(m.parentNodeId);
                }
            }
        }

//        prettyPrintMenuNodes("przed czyszczeniem zbędnych", res);
        // teraz znamy już ID wszystkich dobrych pozycji, a więc wygenerujemy
        // finalną listę
        List<MenuNodeBean> resFix = new ArrayList<MenuNodeBean>();
        for (MenuNodeBean mnb : res) {
            if (goodMenuItemIds.contains(mnb.id)) {
                resFix.add(mnb);
            }
        }

//        prettyPrintMenuNodes("ostateczny wynik", resFix);
        return resFix;
    }

    protected static MenuNodeBean makeMNBCopy(MenuNodeBean mnb, Integer rootNodeId) {
        MenuNodeBean copy = new MenuNodeBean(mnb.id, mnb.name, BaseUtils.nullToDef(mnb.parentNodeId, rootNodeId), mnb.code, mnb.visualOrder);
        return copy;
    }

    protected static int makeMNBCopiesWithChildren(List<MenuNodeBean> nodesToCopy,
            int newParentId, Map<Integer, List<MenuNodeBean>> childNodesMap, int nextNewId,
            List<MenuNodeBean> res, MenuNodeBeanType optMenuType) {

        int cnt = 0;

        if (nodesToCopy != null) {
            for (MenuNodeBean childNode : nodesToCopy) {

                if (!BIKCenterUtils.isMenuOfOptType(childNode, optMenuType)) {
                    //ww: pomijamy pozycje innych typów niż określony (o ile jest
                    // podany określony typ jaki mamy brać pod uwagę)
                    continue;
                }

                int childCnt = makeMNBCopyWithChildren(childNode, newParentId,
                        childNodesMap, nextNewId, res, optMenuType);
                nextNewId += childCnt;
                cnt += childCnt;
            }
        }

        return cnt;
    }

    protected static int makeMNBCopyWithChildren(MenuNodeBean mnb, int newParentId,
            Map<Integer, List<MenuNodeBean>> childNodesMap, int nextNewId,
            List<MenuNodeBean> res, MenuNodeBeanType optMenuType) {
        int nodeCnt = 1;

        MenuNodeBean copy = makeMNBCopy(mnb, newParentId);
        copy.parentNodeId = newParentId;
        copy.id = nextNewId++;

        res.add(copy);

        List<MenuNodeBean> childNodes = childNodesMap.get(mnb.id);

        nodeCnt += makeMNBCopiesWithChildren(childNodes, copy.id, childNodesMap,
                nextNewId, res, optMenuType);

        return nodeCnt;
    }

    protected static void addToResAndChildNodesMap(MenuNodeBean menuItem, List<MenuNodeBean> res,
            Map<Integer, List<MenuNodeBean>> childNodesMap) {
        res.add(menuItem);
        Integer parentNodeId = menuItem.parentNodeId;
        List<MenuNodeBean> childNodes = childNodesMap.get(parentNodeId);
        if (childNodes == null) {
            childNodes = new ArrayList<MenuNodeBean>();
            childNodesMap.put(parentNodeId, childNodes);
        }
        childNodes.add(menuItem);
    }

    protected static void addDescendantMenuItemsIdsToClear(Integer id,
            Set<Integer> idsToClear, Map<Integer, List<MenuNodeBean>> childNodesMap,
            MenuNodeBeanType optMenuType) {
        List<MenuNodeBean> childMenus = childNodesMap.get(id);
        if (childMenus != null) {
            for (MenuNodeBean mnb : childMenus) {

                if (BIKCenterUtils.isMenuOfOptType(mnb, optMenuType)) {
                    int childId = mnb.id;
                    idsToClear.add(childId);
                    addDescendantMenuItemsIdsToClear(childId, idsToClear,
                            childNodesMap, optMenuType);
                }
            }
        }
    }
}
