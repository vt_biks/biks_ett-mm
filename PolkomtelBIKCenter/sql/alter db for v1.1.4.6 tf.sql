﻿--
-- NIE BĘDZIE create login w skrypcie. To ma zrobić ADMIN ręcznie przy pierwszej instalacji BIKSA
--
-- UWAGA:
-- UWAGA:
-- UWAGA: ten skrypt będzie rzucał BŁĘDY!!! w przypadku braku założonych na serwerze loginów biks_app i biks_load !!!!
-- UWAGA:
-- UWAGA:
--
------------------------------------------------------------------------------------------

exec sp_check_version '1.1.4.6';
go
------------------------------------
------------------------------------


if not exists(select * from bik_app_prop where name='load_user_login')
begin
	exec sp_set_app_prop 'load_user_login', 'biks_load'; -- login użytkownika do zasilania
end
go

if not exists(select * from bik_app_prop where name='load_user_pwd')
begin
	exec sp_set_app_prop 'load_user_pwd', 'load' -- hasła użytkownika do zasilania
end
go

------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

-- NIE BĘDZIE create login w skrypcie. To ma zrobić ADMIN ręcznie przy pierwszej instalacji BIKSA
-- użytkownik aplikacyjny (login/hasło do serwera)
/*
if not exists(select * from sys.syslogins where name='biks_app')
begin
create login biks_app with password = 'app' -- to należy wpisać w pliku konfiguracyjnym BIKSa: bikcenter-config.cfg
end
*/--go

-- użytkownik do zasileń (login/hasło do serwera)
/*if not exists(select * from sys.syslogins where name='biks_load')
begin
create login biks_load with password = 'load' -- to należy wpisać poniżej - aby się zapisało w konfiguracji w bazie danych
end
*/--go

-- użytkownik do raportowania (login/hasło do serwera)
/*if not exists(select * from sys.syslogins where name='biks_rep')
begin
create login biks_rep with password = 'rep'
end
*/--go

if not exists(select * from sys.database_principals where name='biks_executor' and type='R')
begin
CREATE ROLE biks_executor
end
go

grant execute to biks_executor
go

-- UWAGA:
-- UWAGA:
-- UWAGA: ten skrypt będzie rzucał BŁĘDY!!! w przypadku braku założonych na serwerze loginów biks_app i biks_load !!!!
-- UWAGA:
-- UWAGA:

if not exists(select * from sys.database_principals where name='biks_app' and type='S')
begin
create user biks_app for login biks_app
end
go


EXEC sp_addrolemember 'db_ddladmin', 'biks_app'
go
EXEC sp_addrolemember 'db_datawriter', 'biks_app'
go
EXEC sp_addrolemember 'db_datareader', 'biks_app'
go
EXEC sp_addrolemember 'biks_executor', 'biks_app'
go


-- UWAGA:
-- UWAGA:
-- UWAGA: ten skrypt będzie rzucał BŁĘDY!!! w przypadku braku założonych na serwerze loginów biks_app i biks_load !!!!
-- UWAGA:
-- UWAGA:

if not exists(select * from sys.database_principals where name='biks_load' and type='S')
begin
create user biks_load for login biks_load
end
go

EXEC sp_addrolemember 'db_owner', 'biks_load'
go
EXEC sp_addrolemember 'db_ddladmin', 'biks_load'
go
EXEC sp_addrolemember 'db_datawriter', 'biks_load'
go
EXEC sp_addrolemember 'db_datareader', 'biks_load'
go
EXEC sp_addrolemember 'biks_executor', 'biks_app'
go

if not exists(select * from sys.database_principals where name='biks_rep' and type='S')
begin
create user biks_rep for login biks_rep
end
go

EXEC sp_addrolemember 'db_datareader', 'biks_load'
go




------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------


delete from bik_admin where param like 'mail.%'
delete from bik_admin where param like 'lucene.%'
delete from bik_admin where param like 'upload.%'


------------------------------------
------------------------------------

exec sp_update_version '1.1.4.6', '1.1.4.7';
go
-- UWAGA:
-- UWAGA:
-- UWAGA: ten skrypt będzie rzucał BŁĘDY!!! w przypadku braku założonych na serwerze loginów biks_app i biks_load !!!!
-- UWAGA:
-- UWAGA:
