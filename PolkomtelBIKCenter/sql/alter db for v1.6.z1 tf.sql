﻿exec sp_check_version '1.6.z1';
go

declare @sourceId int = (select id from bik_data_source_def where description = 'MS SQL')
if exists(select 1 from bik_schedule where instance_name is null and source_id = @sourceId)
begin
	declare @instanceName varchar(512) = (select min(name) from bik_db_server where source_id = @sourceId)

	update bik_schedule
	set instance_name = @instanceName
	where source_id = @sourceId

	insert into bik_schedule(source_id, hour, minute, interval, is_schedule, priority, instance_name)
	select db.source_id, 0, 0, 1, 0, 4, db.name from bik_db_server db
	left join bik_schedule sch on db.name = sch.instance_name and db.source_id = sch.source_id
	where db.source_id = @sourceId and sch.id is null
end;
go

truncate table bik_mssql_index
truncate table bik_mssql_foreign_key
truncate table bik_mssql_column_extradata
truncate table bik_mssql_dependency
truncate table bik_mssql_extended_properties
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_mssql_column_extradata') and name = 'database_name')
begin
	alter table bik_mssql_column_extradata add database_name varchar(512) not null;
	alter table bik_mssql_dependency add database_name varchar(512) not null;
	alter table bik_mssql_extended_properties add database_name varchar(512) not null;
	alter table bik_mssql_foreign_key add database_name varchar(512) not null;
	alter table bik_mssql_index add database_name varchar(512) not null;
end;
go

declare @mssql_tree_id int = (select id from bik_tree where code = 'MSSQL');

insert into bik_mssql_extended_properties(server, node_id, name, value, database_name)
select prop.server, bn.id, prop.name, prop.value, prop.database_name
from aaa_mssql_extended_properties prop
inner join bik_node bn on bn.tree_id = @mssql_tree_id and bn.is_deleted = 0 and bn.obj_id = prop.branch_names
    
insert into bik_mssql_index(table_node_id, column_node_id, server, column_name, name, column_position, is_unique, type, is_primary_key, partition_ordinal, partition_schema, partition_function, partition_function_type, database_name)
select tab.id, col.id, server, column_name, ind.name, column_position, is_unique, type, is_primary_key, partition_ordinal, partition_schema, partition_function, partition_function_type, ind.database_name
from aaa_mssql_index ind
inner join bik_node tab on tab.tree_id = @mssql_tree_id and tab.is_deleted = 0 and tab.obj_id = server + '|' + database_name + '|' + owner + '|' + table_name + '|'
inner join bik_node col on col.tree_id = @mssql_tree_id and col.is_deleted = 0 and col.obj_id = server + '|' + database_name + '|' + owner + '|' + table_name + '|' + column_name + '|'
    
insert into bik_mssql_foreign_key(name, description, server, fk_table, fk_table_node_id, fk_column, fk_column_node_id, pk_table, pk_table_node_id, pk_column, pk_column_node_id, fk_owner, pk_owner, database_name)
select aaa.name, aaa.description, aaa.server, aaa.fk_table, fkt.id, aaa.fk_column, fkc.id, aaa.pk_table, pkt.id, aaa.pk_column, pkc.id, aaa.fk_schema, aaa.pk_schema, aaa.database_name
from aaa_mssql_foreign_key aaa
inner join bik_node fkt on fkt.tree_id = @mssql_tree_id and fkt.is_deleted = 0 and fkt.obj_id = server + '|' + database_name + '|' + fk_schema + '|' + fk_table + '|'
inner join bik_node fkc on fkc.tree_id = @mssql_tree_id and fkc.is_deleted = 0 and fkc.obj_id = server + '|' + database_name + '|' + fk_schema + '|' + fk_table + '|' + fk_column + '|'
inner join bik_node pkt on pkt.tree_id = @mssql_tree_id and pkt.is_deleted = 0 and pkt.obj_id = server + '|' + database_name + '|' + pk_schema + '|' + pk_table + '|'
inner join bik_node pkc on pkc.tree_id = @mssql_tree_id and pkc.is_deleted = 0 and pkc.obj_id = server + '|' + database_name + '|' + pk_schema + '|' + pk_table + '|' + pk_column + '|'
	
insert into bik_mssql_column_extradata(name, server, table_node_id, column_node_id, column_id, description, is_nullable, type, default_field, seed_value, increment_value, database_name)
select aaa.name, aaa.server_name, bnt.id, bnc.id, aaa.column_id, aaa.description, aaa.is_nullable, aaa.type, aaa.default_field, aaa.seed_value, aaa.increment_value, aaa.database_name
from aaa_mssql_column_extradata aaa
inner join bik_node bnt on bnt.tree_id = @mssql_tree_id and bnt.is_deleted = 0 and bnt.obj_id = aaa.server_name + '|' + aaa.database_name + '|' + aaa.owner + '|' + aaa.table_name + '|'
inner join bik_node bnc on bnc.tree_id = @mssql_tree_id and bnc.is_deleted = 0 and bnc.obj_id = aaa.server_name + '|' + aaa.database_name + '|' + aaa.owner + '|' + aaa.table_name + '|' + aaa.name + '|'
	
insert into bik_mssql_dependency(server, ref_node_id, dep_node_id, dep_name, dep_owner, ref_name, ref_owner, database_name)
select aaa.server, obj.id, dep.id, aaa.dep_name, aaa.dep_owner, aaa.ref_name, aaa.ref_owner, aaa.database_name
from aaa_mssql_dependency aaa
inner join bik_node obj on obj.tree_id = @mssql_tree_id and obj.is_deleted = 0 and obj.obj_id = aaa.server + '|' + aaa.database_name + '|' + aaa.ref_owner + '|' + aaa.ref_name + '|'
inner join bik_node dep on dep.tree_id = @mssql_tree_id and dep.is_deleted = 0 and dep.obj_id = aaa.server + '|' + aaa.database_name + '|' + aaa.dep_owner + '|' + aaa.dep_name + '|'


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_mssql_one_server_and_database]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_mssql_one_server_and_database]
go

create procedure [dbo].[sp_insert_into_bik_node_mssql_one_server_and_database](@serverName varchar(max), @database varchar(255))
as
begin

	declare @mssql_tree_id int;
	select @mssql_tree_id = id from bik_tree where code = 'MSSQL'

	-- update
	update bik_node 
	set name = mssql.name, node_kind_id = bnk.id, is_deleted = 0, descr = mssql.extra_info, visual_order = mssql.visual_order
	from bik_mssql mssql
	inner join bik_node_kind bnk on mssql.type = bnk.code
	where bik_node.tree_id = @mssql_tree_id
	and bik_node.obj_id like (@serverName + '|' + @database + '|%')
	and bik_node.obj_id = mssql.branch_names
	and bik_node.linked_node_id is null

	declare @serverNodeId int = (select id from bik_node where tree_id = @mssql_tree_id and obj_id = @serverName + '|' and parent_node_id is null);
	if @serverNodeId is null
	begin
		insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id)
		values(null, dbo.fn_node_kind_id_by_code('MSSQLServer'), @serverName, null, @mssql_tree_id, @serverName + '|')
		
		set @serverNodeId = (select convert(int, scope_identity()) as id);
	end
	else
	begin
		update bik_node set is_deleted = 0 where id = @serverNodeId
	end

	-- insert nowych
	insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id, visual_order)
	select @serverNodeId, bnk.id, ms.name, ms.extra_info, @mssql_tree_id, ms.branch_names, ms.visual_order
	from bik_mssql ms
	inner join bik_node_kind bnk on ms.type = bnk.code
	left join (bik_node bn inner join bik_tree bt on bn.tree_id = bt.id and bn.tree_id = @mssql_tree_id and bn.obj_id like (@serverName + '|' + @database + '|%')) on bn.obj_id = ms.branch_names
	where ms.branch_names like (@serverName + '|' + @database + '|%')
	and bn.id is null
	order by ms.branch_names

	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join bik_mssql as ms on bik_node.obj_id = ms.branch_names
	inner join bik_node bk on bk.obj_id = ms.parent_branch_names 
	where bik_node.tree_id = @mssql_tree_id
	and bik_node.obj_id like (@serverName + '|' + @database + '|%')
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.tree_id = @mssql_tree_id
	and bk.obj_id like (@serverName + '|%')
	and bk.is_deleted = 0
	and bk.linked_node_id is null

	-- usunięcie zbędnych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join bik_mssql on bik_node.obj_id = bik_mssql.branch_names and bik_mssql.branch_names like (@serverName + '|' + @database + '|%')
	where bik_mssql.name is null
	and tree_id = @mssql_tree_id
	and obj_id like (@serverName + '|' + @database + '|%')
	and is_deleted = 0
	and linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @mssql_tree_id 
	and bn2.obj_id like (@serverName + '|' + @database + '|%')
	and bn2.is_deleted = 1

	exec sp_node_init_branch_id @mssql_tree_id,null

end;
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_mssql_one_server]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_mssql_one_server]
go

create procedure [dbo].[sp_insert_into_bik_node_mssql_one_server](@serverName varchar(max), @databases varchar(max))
as
begin

	declare @db_name varchar(255);
	declare database_curs cursor for 
	select str from dbo.fn_split_by_sep(@databases, ',', 3)
		
	open database_curs
	fetch next from database_curs into @db_name
	while @@fetch_status = 0
	begin 
		exec sp_insert_into_bik_node_mssql_one_server_and_database @serverName, @db_name;
		fetch next from database_curs into @db_name;
	end
	close database_curs;
	deallocate database_curs;
	
	declare @mssql_tree_id int = (select id from bik_tree where code = 'MSSQL');
	exec sp_node_init_branch_id @mssql_tree_id,null

end;
go


if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_db_server') and name = 'database_list')
begin
	alter table bik_db_server add database_list varchar(max) null;
end;
go

exec sp_update_version '1.6.z1', '1.6.z2';
go