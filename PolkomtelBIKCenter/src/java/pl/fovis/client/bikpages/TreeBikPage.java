/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.BOXIServiceAsync;
import pl.fovis.client.EntityDetailsPane;
import pl.fovis.client.HistoryManager;
import pl.fovis.client.ResizeSensitiveWidgetHelper;
import pl.fovis.client.bikwidgets.BikItemButtonBig;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.client.dialogs.SystemConfigurationDialog;
import pl.fovis.client.dialogs.SystemConfigurationDialogBase;
import pl.fovis.client.nodeactions.BiksEventPopUpMenu;
import pl.fovis.client.nodeactions.BiksPopUpMenu;
import pl.fovis.client.nodeactions.INodeAction;
import pl.fovis.client.nodeactions.INodeClipboard;
import pl.fovis.client.nodeactions.NodeActionAddAnyNode;
import pl.fovis.client.nodeactions.NodeActionAddAnyNodeForKind;
import pl.fovis.client.nodeactions.NodeActionAddRole;
import pl.fovis.client.nodeactions.NodeActionContext;
import pl.fovis.client.nodeactions.NodeActionNewNestableParent;
import pl.fovis.client.nodeactions.NodeActionShowAnticipatedObjLinkDialog;
import pl.fovis.client.nodeactions.NodeClipboard;
import pl.fovis.client.treeandlist.TreeNodeBeanAsMapStorage;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.HorizontalSplitPanelWrapper;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.VisualMetricsUtils;
import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeOrListGrid;
import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeOrListGridRightClickHandler;
import static pl.fovis.foxygwtcommons.wezyrtreegrid.FoxyWezyrTreeGrid.DIAG_MSG_KIND$EXPAND_NODES_BY_IDS;
import pl.trzy0.foxy.commons.annotations.DisableDynamicClassCreation;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.LameRuntimeException;

/**
 *
 * @author mgraczkowski
 */
@DisableDynamicClassCreation
public class TreeBikPage extends SingleTreeOrListMasterPageBase<List<TreeNodeBean>>
        implements IBikTreePage {

    public static final String[] TREE_BIK_PAGE_TWO_COLS_FOR_TREE = {TreeNodeBeanAsMapStorage.NAME_FIELD_NAME + "=300:Nazwa",
        TreeNodeBeanAsMapStorage.PRETTY_KIND_FIELD_NAME + "=100:Typ"};
    public static final String[] TREE_BIK_PAGE_ONE_COL_FOR_TREE = {TreeNodeBeanAsMapStorage.NAME_FIELD_NAME + "=" + I18n.nazwa.get() /* I18N:  */};
    public static final boolean useLazyLoading = true; //false; //
    private TreeBean tb;
    protected PushButton addButton;
    private BikItemButtonBig filterButton;
//    private PushButton clearFilterBtn;
//    private PushButton resetTreeBtn;
    private String[] fields;
    protected BikTreeWidget bikTree;
    protected EntityDetailsPane edp;
    //protected SplitLayoutPanel main;
    protected HorizontalSplitPanelWrapper main;
    //protected DecoratedPopupPanel pop = new DecoratedPopupPanel(true);
    //private BiksPopUpMenu popWidget;
    private TextBox filterBox;
    protected String currentFilterVal = "";
    private final CheckBox cbFilterInSubTree = new CheckBox();
    protected Integer currentFilterInSubTreeNodeId = null;
//    protected String currentFilterInSubTreeNamePath;
    //private boolean emptyFlag = true;
    //protected boolean expandSelectedNode = false;
    protected INodeClipboard clipboard = new NodeClipboard();
    protected INodeAction addRootNodeAct;
    protected BikNodeTreeOptMode bikNodeTreeOptMode;
    protected INodeAction actShowAnticipatedObjLinkDialog;
    protected INodeAction actShowAddAnyNodeDialog;

    public TreeBikPage(TreeBean tb, String... fields) {
        this.fields = fields;
        this.tb = tb;
    }

    protected BikNodeTreeOptMode getBikNodeTreeOptMode() {
        return bikNodeTreeOptMode;
    }

    protected Request callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
//        getService().getTreeNodes(getTreeId(), callback);

        if (useLazyLoading) {
            return getService().getOneSubtreeLevelOfTree(getTreeId(), null, false, getBikNodeTreeOptMode(), null, true, null, null, null, null,
                    callback);
        } else {
            return getService().getTrimmedTree(getTreeId(), callback);
        }
    }

    protected BOXIServiceAsync getService() {
        return BIKClientSingletons.getService();
    }

    protected void loadDetailsDataForSelectedRow() {
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding, "loadDetailsDataForSelectedRow: start");
//        }

        Integer nodeId = getDisplayedNodeId();

//        if (ClientDiagMsgs.isShowDiagEnabled(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding, "loadDetailsDataForSelectedRow: after getDisplayedNodeId");
//        }
        TreeNodeBean tnb = getBikTree().getCurrentNodeBean();

        if (BIKClientSingletons.isUserLoggedIn() && tnb != null && bikTree.isTreeInitialized()) {
            BIKClientSingletons.getService().insertBIKStatisticsExtTabAndNode(tnb.id, tnb.branchIds, new StandardAsyncCallback<Void>() {
                public void onSuccess(Void result) {
                    //...
                }
            });
        }

//        if (isFirstSelected) {
//            BIKClientSingletons.getService().insertBIKStatisticsTabAndNode(tb.code, tb.name, tnb.name, new StandardAsyncCallback<Void>() {
//                public void onSuccess(Void result) {
//                    //...
//                }
//            });
//        }
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding, "loadDetailsDataForSelectedRow: after getCurrentNodeBean");
//        }
        Integer linkingParentNodeId = getLinkingParentNodeId(tnb);

//        if (ClientDiagMsgs.isShowDiagEnabled(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding, "loadDetailsDataForSelectedRow: after getLinkingParentNodeId");
//        }
        edp.setNodeIdAndFetchData(nodeId, linkingParentNodeId);

//        if (ClientDiagMsgs.isShowDiagEnabled(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//            ClientDiagMsgs.showDiagMsg(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding, "loadDetailsDataForSelectedRow: end");
//        }
    }

    protected PushButton makeButtonWithImage(String imgUrl, final IContinuation doOnClick,
            String title) {
        PushButton btn = new PushButton(new Image(imgUrl), new Image(imgUrl));
        btn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                doOnClick.doIt();
            }
        });

        btn.setTitle(title);
        btn.addStyleName("plusLarge");

        return btn;
    }

    protected void resetTree() {
        filterWithCurrentText();
        // albo:
        //clearFilter();
    }

    protected Widget buildWidgets() {
        //main = new SplitLayoutPanel();
        main = new HorizontalSplitPanelWrapper();
        main.setWidth("100%");
        // main.setHeight("100%");
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(main, BIKClientSingletons.MY_FUCKING_IE7_HACK_HEIGHT);

        main.setSplitPosition("410px");
        bikTree = new BikTreeWidget(tb.id, dataFetchBroker, fields);

        //bikTree.setDataFetchBroker(dataFetchBroker);
//        main.addWest(bikTree.getTreeGridWidget(), 400);
        final Widget treeWidget = getBikTree().getTreeGridWidget();

        getBikTree().addSelectionChangedHandler(new IParametrizedContinuation<Map<String, Object>>() {
            public void doIt(Map<String, Object> param) {
//                TreeNodeBean tnb = //(TreeNodeBean) param.get(BaseBIKTreeWidget.TREE_NODE_BEAN_KEY_IN_ROW);
//                        bikTree.getNodeBeanByDataNode(param);
//                int id = getDisplayedNodeId();

//                if (ClientDiagMsgs.isShowDiagEnabled(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//                    ClientDiagMsgs.showDiagMsg(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding, "SelectionChangedHandler: start");
//                }
//                HistoryManager.addHistoryToken(getId(), BaseUtils.safeToString(getSelectedNodeId()), false);
                if (bikTree.isTreeInitialized()) {
                    HistoryManager.addToUserHistoryAndHistoryToken(getId(), BaseUtils.safeToString(getSelectedNodeId()), false);
                }

                int splitPosition = VisualMetricsUtils.getWidthOfLeftHorizontalSplitPanelElement(main);
                if (splitPosition != 0) {
                    int treeWidth = treeWidget.getElement().getClientWidth();
                    if (treeWidth != splitPosition) {
                        treeWidth = treeWidth + 17;
                    }
                    main.setSplitPosition(treeWidth + "px");
                } else {
                    String width = "410px";
                    treeWidget.setWidth(width);
                    main.setSplitPosition(width);
                }

//                if (ClientDiagMsgs.isShowDiagEnabled(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//                    ClientDiagMsgs.showDiagMsg(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding, "SelectionChangedHandler: history token added");
//                }
                loadDetailsDataForSelectedRow();

                updateFilterButtonEnabled();

//                if (ClientDiagMsgs.isShowDiagEnabled(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding)) {
//                    ClientDiagMsgs.showDiagMsg(BIKGWTConstants.DIAG_MSG_KIND$WW_nodeExpanding, "SelectionChangedHandler: end");
//                }
            }
        });

        //popWidget = new BiksPopUpMenu(/*getTreeId(), getTreeNodeKindId(),*/this);
        final VerticalPanel vPanel = new VerticalPanel();
        vPanel.setStyleName("leftMainPanelTree");
        // vPanel.setHeight("100%");
        vPanel.setWidth("100%");
        final HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);

        /*   if (bikTree.isSubTreeFilteringCapable()) {
            cbFilterInSubTree.setTitle(I18n.filtrujWPoddrzewie.get());
            cbFilterInSubTree.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    updateFilterButtonEnabled();
                }
            });

            hPanel.add(cbFilterInSubTree);
            hPanel.setCellWidth(cbFilterInSubTree, "22px");
        }
         */
        filterBox = new TextBox();
        filterBox.setStyleName("filterBox");
        filterBox.setWidth("164px"); // musi być "px" - ac

        hPanel.add(filterBox);
        hPanel.setCellWidth(filterBox, "180px");

        IContinuation filterCont = new IContinuation() {
            public void doIt() {
                filterWithCurrentText();
            }
        };

        filterButton = new BikItemButtonBig(I18n.szukaj.get() /* I18N:  */, filterCont);
        GWTUtils.addEnterKeyHandler(filterBox, filterCont);

        filterBox.addKeyUpHandler(new KeyUpHandler() {
            public void onKeyUp(KeyUpEvent event) {
                updateFilterButtonEnabled();
            }
        });
        filterButton.setWidth("50px");

//        filterBox.addKeyUpHandler(new KeyUpHandler() {
//
//            public void onKeyUp(KeyUpEvent event) {
//                NativeEvent ne = event.getNativeEvent();
//                if (ne.getKeyCode() == KeyCodes.KEY_ENTER) {
//                    clickEnter();
//                }
//            }
//        });
        hPanel.add(filterButton);
        hPanel.setCellWidth(filterButton, "50px");
//

//        clearFilterBtn = new PushButton(new Image("images/clear_filter.png"), new Image("images/clear_filter.png"));
//        clearFilterBtn.addClickHandler(new ClickHandler() {
//
//            public void onClick(ClickEvent event) {
//                clearFilter();
//            }
//        });
//
//        clearFilterBtn.setTitle("Wyczyść filtr (pokaż wszystko)");
//        clearFilterBtn.setStyleName("plusLarge");
//
//        hPanel.add(clearFilterBtn);
        HorizontalPanel rBtns = new HorizontalPanel();
        rBtns.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
        rBtns.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
//        rBtns.setSpacing(3);

        addOptionalExtraWidgetsToTreeFooter(rBtns);
        rBtns.add(/*clearFilterBtn = */makeButtonWithImage("images/clear_filter_16.gif" /* I18N: no */,
                        new IContinuation() {
                    public void doIt() {
                        clearFilter();
                    }
                }, I18n.odswiez.get() /* I18N:  */));
//        HorizontalPanel lb = new HorizontalPanel();
//        if (!BIKClientSingletons.isDeveloperMode()) {
//            lb.setWidth("70px");
//        }
        hPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        //   hPanel.add(lb);
        hPanel.add(rBtns);

//        hPanel.add(resetTreeBtn = makeButtonWithImage("images/collapse_all_16.png",
//                new IContinuation() {
//
//                    public void doIt() {
//                        resetTree();
//                    }
//                }, "Wczytaj ponownie (zwiń wszystko)"));
        // hPanel.setCellVerticalAlignment(filterButton, HasVerticalAlignment.ALIGN_MIDDLE);
//        vPanel.addDomHandler(new ClickHandler() {
//
//            public void onClick(ClickEvent event) {
//                if (!emptyFlag && BIKClientSingletons.isUserLoggedIn() && getTreeNodeKindId() > 1) {
//                    showPopUp(null, event.getClientX(), event.getClientY(), popWidget);
//                }
//            }
//        }, ClickEvent.getType());
//        final Widget treeWidget = getBikTree().getTreeGridWidget();
        treeWidget.setSize("100%", "100%");
        //treeWidget.setStyleName("craz4beer");

        //treeWidget.get
        vPanel.add(treeWidget);

//        vPanel.setCellHeight(treeWidget,
//                BIKClientSingletons.fixBigWidgetHeightPx(BIKClientSingletons.ORIGINAL_TREE_GRID_PANEL_HEIGHT) //                (540 - 19 + BIKClientSingletons.getMainBodyHeightDelta())
//                //                //-(BIKClientSingletons.oldBodyHeight -BIKClientSingletons.newBodyHeight)
//                //                + "px"
//                );
        BIKClientSingletons.registerResizeSensitiveWidget(new ResizeSensitiveWidgetHelper(vPanel) {
            @Override
            public void setFixedHeight(int height) {
                //height -= 4; // uwzględniam padding .treeBikFilterBox bik.css linia 1420
                vPanel.setCellHeight(treeWidget, height + "px");
                // Window.alert("wys: " + height);
            }
        }, BIKClientSingletons.ORIGINAL_TREE_GRID_PANEL_HEIGHT);

        vPanel.setCellWidth(treeWidget, "100%");

        hPanel.setStyleName("treeBikFilterBox");
        hPanel.setWidth("100%");
        vPanel.add(hPanel);
        vPanel.setCellHeight(hPanel, "32px");
        vPanel.setCellWidth(hPanel, "100%");
//        main.addWest(vPanel, 400);
        //main.addWest(vPanel, 300);
        main.add(vPanel);

        VisualMetricsUtils.fixHorizontalSplitPanelAndWatchForSplitterMoves(main, new IParametrizedContinuation<Integer>() {
            public void doIt(Integer width) {
                treeWidget.setWidth(width + "px");
                edp.setHeightBodyEdp();
            }
        });

//        main.addHandler(new MouseMoveHandler() {
//
//            public void onMouseMove(MouseMoveEvent event) {
//                if (main.isResizing()) {
////                    final int width = VisualMetricsUtils.getWidth(vPanel);
//                    //main.get
//
//                    //width = main.getElement().getFirstChildElement().getFirstChildElement()
//                    //treeWidget.setWidth((0) + "px");
//                    //vPanel.setWidth((width - 2) + "px");
//
////                    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
////
////                        public void execute() {
//                    int width = VisualMetricsUtils.getWidthOfLeftHorizontalSplitPanelElement(main);
//                    treeWidget.setWidth((width - 2) + "px");
//                    Window.setTitle("it is moving! vPanel.width=" + width);
////                        }
////                    });
//                }
//            }
//        }, MouseMoveEvent.getType());
//
//        VisualMetricsUtils.disableScrollPanelOfLeftHorizontalSplitPanelElement(main);
        //main.getElement().getFirstChildElement().getFirstChildElement().getStyle().setOverflow(Overflow.HIDDEN);
        // main.getWidgetContainerElement(vPanel).setClassName("crazy4beer");
        getBikTree().addRightClickHandler(new IFoxyTreeOrListGridRightClickHandler<Map<String, Object>>() {
            @Override
            public void doIt(Map<String, Object> param, final int clientX, final int clientY, boolean isRowSelected) {
                final TreeNodeBean tnb
                        = //(TreeNodeBean) param.get(BaseBIKTreeWidget.TREE_NODE_BEAN_KEY_IN_ROW);
                        getBikTree().getNodeBeanByDataNode(param);
//                Window.alert("TreeNodeBean.name = " + tnb.name + ", bean: " + tnb + ", isRowSelected = " + isRowSelected);
//                Window.alert("" + (edp.data.node.id == tnb.id || edp.data.node.id == tnb.linkedNodeId));
                if (isRowSelected && (tnb != null && edp.data != null && edp.data.node != null && (edp.data.node.id == tnb.id || edp.data.node.id == tnb.linkedNodeId))) {
                    showPopUp(tnb, clientX, clientY);
                } else {
                    edp.setDataFetchedCallback(new IContinuation() {
                        @Override
                        public void doIt() {
                            edp.setDataFetchedCallback(null);
                            if (getBikTree().getCurrentNodeBean() != tnb) {
                                return;
                            }

                            showPopUp(tnb, clientX, clientY);
                        }
                    });
                }

                //bikTree.selectEntityById(tnb.id, false);
//                loadDetailsDataForSelectedRow();
            }
        });

        edp = new EntityDetailsPane(this);
        //edp.setTabSelector(getTabSelector());
        main.add(edp.getWidget());

        return main.asWidget();
    }

    protected void updateFilterButtonEnabled() {
        boolean subTreeFilteringChanged = (currentFilterInSubTreeNodeId != null) != cbFilterInSubTree.getValue()
                || !BaseUtils.safeEquals(currentFilterInSubTreeNodeId, getSelectedNodeId());
        boolean textHasChanged = !BaseUtils.safeEquals(filterBox.getText(), currentFilterVal);
        filterButton.setEnabled(subTreeFilteringChanged || textHasChanged);

        boolean filterIsOn = !BaseUtils.isStrEmpty(currentFilterVal);

        GWTUtils.addOrRemoveStyleName(filterButton, "filterIsOn", filterIsOn);

        if (filterIsOn) {
            if (currentFilterInSubTreeNodeId == null) {
                filterButton.setTitle("Filtrowanie [" + currentFilterVal + "]");
            }
//            filterButton.setTitle("Filtrowanie [" + currentFilterVal + "]"
//                    + (currentFilterInSubTreeNodeId != null ? " w poddrzewie węzła: " + currentFilterInSubTreeNamePath : ""));
        } else {
            filterButton.setTitle(I18n.szukaj.get());
        }
    }

    protected void updateFilterAndClearButtonsEnabled() {
        updateFilterButtonEnabled();
        //clearFilterBtn.setEnabled(!BaseUtils.isStrEmpty(currentFilterVal));
    }

    protected void fetchCurrentFilterInSubTreeNamePathAndUpdateFilterButtonTitle() {

        final String oldFilterVal = currentFilterVal;
        final Integer oldFilterNodeId = currentFilterInSubTreeNodeId;

        getService().getAncestorNodePathByNodeId(currentFilterInSubTreeNodeId, new StandardAsyncCallback<List<String>>() {

            @Override
            public void onSuccess(List<String> result) {

                if (!BaseUtils.safeEquals(currentFilterVal, oldFilterVal)
                        || !BaseUtils.safeEquals(currentFilterInSubTreeNodeId, oldFilterNodeId)) {
                    return;
                }

                filterButton.setTitle("Filtrowanie [" + currentFilterVal + "] w poddrzewie węzła: " + BaseUtils.mergeWithSepEx(result, BIKConstants.RAQUO_STR_SPACED));
            }
        });
    }

    protected void filterTreeWithVal(String val) {
        this.currentFilterVal = val;
        filterBox.setText(val);

        BikNodeTreeOptMode mode = getBikNodeTreeOptMode();

        if (!BaseUtils.isStrEmpty(currentFilterVal) || mode != null) {

            currentFilterInSubTreeNodeId = cbFilterInSubTree.getValue() ? getBikTree().getCurrentNodeId() : null;

            if (currentFilterInSubTreeNodeId != null) {
                fetchCurrentFilterInSubTreeNamePathAndUpdateFilterButtonTitle();
            }
//            currentFilterInSubTreeNamePath = currentFilterInSubTreeNodeId == null ? null : BIKCenterUtils.buildAncestorPathByStorage(getBikTree().getCurrentNodeBean().branchIds, getBikTree().getTreeStorage());

            getBikTree().filterTree(currentFilterVal, currentFilterInSubTreeNodeId, useLazyLoading, mode, null);
        } else {
            currentFilterInSubTreeNodeId = null;
//            currentFilterInSubTreeNamePath = null;
            fetchData(false);
        }

        updateFilterAndClearButtonsEnabled();
    }

    private void filterWithCurrentText() {
        filterTreeWithVal(filterBox.getText());
    }

    protected void clearFilter() {
        filterTreeWithVal("");
    }

    @Override
    protected void clearFilterControls() {
        super.clearFilterControls();
        this.currentFilterVal = "";
        filterBox.setText("");
        //filterButton.setEnabled(false);
        updateFilterAndClearButtonsEnabled();
    }

//    @Override
//    protected void clearFilterIfNeeded() {
//        if (!BaseUtils.isStrEmpty(currentFilterVal)) {
//            clearFilter();
//        }
//    }
    public void refreshRedisplayDataAndSelectRecord(final int id/*, final boolean expand*/) {
        //this.expandSelectedNode = expand;
        refreshPage(id);
    }

//    private void trimTree(int treeId, List<TreeNodeBean> l) {
//        Map<Integer, TreeNodeBean> tnm = new HashMap<Integer, TreeNodeBean>();
//        for (TreeNodeBean tnb : l) {
//            tnm.put(tnb.id, tnb);
//        }
//        UniTreeBuilder utb = new UniTreeBuilder();
//        TreeAlgorithms.buildTree(utb, l);
//        utb.copyLinkedSubtrees(treeId);
//        Map<Integer, UniTreeNode> m = utb.getUniNodesMap();
//        l.clear();
//        for (Entry e : m.entrySet()) {
//            l.add(((UniTreeNode) e.getValue()).tnb);
//        }
//    }
    public String getAlternativeId() {
        return null;
    }

    public void refreshDetailProps() {
        if (edp != null) {
            edp.setForceDataRefresh(true);
            edp.refreshAndRedisplayData();
        }
    }

    public void refreshAddButtonState() {
        if (addButton != null) {
//            addButton.setVisible(addRootNodeAct.canBeEnabledByStateForThisNodeAndUserLoggedIn());
            addButton.setVisible(addRootNodeAct.isEnabled());
        }
        optRefreshOtherButtons();
    }

    protected void optRefreshOtherButtons() {
        if (actShowAnticipatedObjLinkDialog != null || actShowAddAnyNodeDialog != null) {
            final TreeNodeBean cnb = getBikTree().getCurrentNodeBean();
            final EntityDetailsDataBean data = edp == null ? null : edp.data;
            final NodeActionContext ctx = new NodeActionContext(cnb, this, data);
            actShowAnticipatedObjLinkDialog.setup(ctx);
            actShowAddAnyNodeDialog.setup(ctx);
        }
    }

    public int getTreeId() {
        return tb.id;
    }

    protected Integer getTreeNodeKindId() {
        return tb.nodeKindId;
    }

    public String getTreeKind() {
        return tb.treeKind;
    }

    public String getTreeCode() {
        return tb.code;
    }

    public String getId() {
        return getTreeCode();
    }

//    @Override
//    public String getCaption() {
//        return tb.name;
//    }
    @Override
    protected void refreshDetailWidgetsState() {
        super.refreshDetailWidgetsState();
        edp.refreshWidgetsState();
    }

    @Override
    public void refreshMainWidgetState() {
        super.refreshMainWidgetState();
        //bikTree.refreshWidgetsState();
        refreshAddButtonState();
    }

    public void showPopUp(TreeNodeBean tnb, int clientX, int clientY) {

        //DecoratedPopupPanel pop = new DecoratedPopupPanel(true);
        if (tnb.isRegistry == true) {
            new BiksEventPopUpMenu(this, tnb, edp.data, clientX, clientY).createAndShowPopup();
        } else {
            new BiksPopUpMenu(this, tnb, edp.data, clientX, clientY).createAndShowPopup();
        }
//        popWidget.createAndShowPopup(this, tnb, edp.data, clientX, clientY);

        //pop.clear();
        //pop.setWidth("150px");
        // pop.setStyleName("testtest");
        //GWTUtils.setStyleAttribute(pop, "zIndex", "10");
        //Widget popupPanel = popWidget.createPopUp(tnb, edp.data);
//        if (popupPanel == null) {
//            return;
//        }
//
//        pop.add(popupPanel);
//        int left = clientX;
//        int top = clientY;
//        if (top > 780) {
//            top -= 80;
//        }
//        pop.setPopupPosition(left, top);
//        pop.setVisible(true);
//        pop.show();
        // Window.alert("left: " + left + ", top: " + top);
    }

    protected void addOptionalExtraWidgetsToTreeFooter(HorizontalPanel hPanel) {

        String treeKind = "";
        if (getId().equals(BIKGWTConstants.TREE_CODE_USER_ROLES)) {
            addRootNodeAct = new NodeActionAddRole();
        } else {
            TreeBean treeBean = getTreeBean();
            treeKind = treeBean.treeKind;
            NodeKindBean treeNodeKind = BIKClientSingletons.getNodeKindById(treeBean.nodeKindId);
            final String treeNodeKindCaption = treeNodeKind == null ? "???" : treeNodeKind.caption.toLowerCase();
//
            if (BIKClientSingletons.isTreeKindDynamic(getTreeKind())) {
                addRootNodeAct = new NodeActionAddAnyNodeForKind(true, getTreeId());

            } else //            Window.alert(BIKClientSingletons.isTreeKindDynamic(getTreeKind())+" "+getTreeKind()+" "+treeBean.treeKind+" "+getTreeId());
            {

                addRootNodeAct = new NodeActionNewNestableParent(I18n.dodaj.get() /* I18N:  */ + " "
                        + (!treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_USERS) ? treeNodeKindCaption : I18n.grupeUzytkownikow.get() /* I18N:  */)
                        + (!treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_BLOGS)
                        ? " " + I18n.naNajwyzszymPoziomie.get() /* I18N:  */ : ""));
            }
//            addRootNodeAct = new NodeActionNewNestableParent() {
//
//                @Override
//                public String getOptCurrentNodeActionCode() {
//                    return "NewNestableParent";
//                }
//
//                @Override
//                public String getCaption() {
//                    return I18n.dodaj.get() /* I18N:  */ + " " + (!treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_USERS) ? treeNodeKind.caption.toLowerCase() : I18n.grupeUzytkownikow.get() /* I18N:  */)
//                            + (!treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_BLOGS)
//                                    ? " " + I18n.naNajwyzszymPoziomie.get() /* I18N:  */ : "");
//                }
//            };
        }

        addRootNodeAct.setup(new NodeActionContext(null, this, null));

//        if (addRootNodeAct.canBeEnabledByStateForThisNodeAndUserLoggedIn()) {
        if (addRootNodeAct.isEnabled() && showAddMainBranchBtn() && BIKClientSingletons.isDeveloperMode()) {
            addButton = new PushButton(new Image("images/plus_large.png" /* I18N: no */), new Image("images/plus_large.png" /* I18N: no */));
            addButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    addRootNodeAct.execute();
                }
            });
            addButton.setTitle(addRootNodeAct.getCaption());

            addButton.addStyleName("plusLarge");

            if (!BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(treeKind)
                    && !BIKConstants.TREE_KIND_META_BIKS_MENU_CONF.equalsIgnoreCase(treeKind)) {
                hPanel.add(addButton);
            }
        }

        actShowAnticipatedObjLinkDialog = new NodeActionShowAnticipatedObjLinkDialog();
        actShowAddAnyNodeDialog = new NodeActionAddAnyNode(true, getTreeId());

        refreshAddButtonState();

        addPushButtonToTreeFooter(hPanel, "images/biks_link2.png", actShowAnticipatedObjLinkDialog);
        if (!BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(treeKind)
                && !BIKConstants.TREE_KIND_META_BIKS_MENU_CONF.equalsIgnoreCase(treeKind)) {
            addPushButtonToTreeFooter(hPanel, "images/plus_large2.png", actShowAddAnyNodeDialog);
        }

        if (BIKClientSingletons.isAppAdminLoggedIn()) {
            IContinuation cont = new IContinuation() {

                @Override
                public void doIt() {
                    String conf = getTreeKind().equals(BIKConstants.TREE_KIND_META_BIKS) ? BIKConstants.APP_PROP_SYS_CONF : BIKConstants.APP_PROP_MENU_CONF;
                    BIKClientSingletons.getService().changeConfigurationTree(conf, getTreeCode(), new StandardAsyncCallback<Void>() {

                        @Override
                        public void onSuccess(Void result) {
                            onMetaButtonClicked();
                        }
                    });

                }
            };
            if (getTreeKind().equals(BIKConstants.TREE_KIND_META_BIKS)) {
                hPanel.add(makeButtonWithImage("images/config.gif", cont, I18n.konfiguracja.get()));
            } else if (getTreeKind().equals(BIKConstants.TREE_KIND_META_BIKS_MENU_CONF)) {
                hPanel.add(makeButtonWithImage("images/hammer.gif", cont, I18n.zbuduj.get()));
            }
        }
    }

    protected void onMetaButtonClicked() {
        if (getTreeKind().equals(BIKConstants.TREE_KIND_META_BIKS)) {
            SystemConfigurationDialogBase dialog = new SystemConfigurationDialog();
            dialog.buildAndShowDialog(getTreeBean());
        } else {
            final BIKSProgressInfoDialog dialog = new BIKSProgressInfoDialog();
            dialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaPostepowanie.get(), true);
            BIKClientSingletons.getService().buildMenuConfigTree(true, new StandardAsyncCallback<Void>() {

                @Override
                public void onSuccess(Void result) {
                    dialog.hideDialog();
                    BIKClientSingletons.showInfo(I18n.proszeOdswiezycPrzegladarke.get());
                }

                @Override
                public void onFailure(Throwable caught) {
                    dialog.hideDialog();
                    LameRuntimeException e = (LameRuntimeException) caught;
                    Window.alert(e.getMessage());
                }
            });
        }
    }

    protected void addPushButtonToTreeFooter(HorizontalPanel hPanel, String imageUrl, final INodeAction act) {
        if (act.isEnabled()) {
            PushButton res = new PushButton(new Image(imageUrl), new Image(imageUrl));
            res.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    act.execute();
                }
            });

            res.setTitle(act.getCaption());
            res.addStyleName("plusLarge");
            hPanel.add(res);
        }
    }

    @Override
    protected void populateWidgetsWithData(List<TreeNodeBean> data/*, boolean hasIdToSelect*/) {
        //Window.alert("hasIdToSelect = " + hasIdToSelect);
        getBikTree().initWithFullData(data, useLazyLoading, getBikNodeTreeOptMode(), null, optIdToSelect);
    }

    @Override
    protected void refreshDetailData() {
        super.refreshDetailData();
        edp.refreshAndRedisplayData();
    }

    protected Integer getLinkingParentNodeId(TreeNodeBean tnb) { // dla podlinkowanych obiektów pod zapytaniem na raporcie
        if (tnb == null) {
            return null;
        }
        return tnb.linkedNodeId != null && BIKClientSingletons.isTreeBOReport(tnb.treeCode)/*tnb.treeCode.equals(BIKGWTConstants.TREE_CODE_REPORTS)*/ ? tnb.parentNodeId : null;
    }

    public Integer getNodeIdToDisplay(TreeNodeBean tnb) {
        return tnb == null ? null : BaseUtils.nullToDef(tnb.linkedNodeId, tnb.id);
    }

    @Override
    public Integer getDisplayedNodeId() {
        return getNodeIdToDisplay(getBikTree().getCurrentNodeBean());
    }

    public INodeClipboard getNodeClipboard() {
        return clipboard;
    }

    @Override
    protected IFoxyTreeOrListGrid<Integer> getTreeOrListGrid() {
        return getBikTree().getTreeGrid();
    }

    public TreeBean getTreeBean() {
        return tb;
    }

    public BikTreeWidget getBikTree() {
        return bikTree;
    }

    @Override
    protected void reexpandAfterFetch(Set<Integer> expandedBeforeFetch, final Integer optIdToSelect) {

        boolean expanded = bikTree.getTreeGrid().expandNodesByIdsWithDataLoading(expandedBeforeFetch, optIdToSelect, null);

        if (!expanded) {
            innerSelectEntityById(optIdToSelect);
        }

//        if (ClientDiagMsgs.isShowDiagEnabled("expandNodesByIds")) {
//            ClientDiagMsgs.showDiagMsg("expandNodesByIds", "reexpandAfterFetch for expandedBeforeFetch=" + expandedBeforeFetch);
//        }
//
//        final int size = expandedBeforeFetch.size();
//
//        bikTree.getTreeGrid().expandNodesByIds(expandedBeforeFetch, new IParametrizedContinuation<Integer>() {
//
//            @Override
//            public void doIt(Integer param) {
//                if (param >= size) {
//                    innerSelectEntityById(optIdToSelect);
//                }
//            }
//        }, true);
    }

    @Override
    protected Set<Integer> getExpandedBeforeFetch() {
        if (BIKClientSingletons.getTreeTabAutoRefreshLevel() != 3) {
            return null;
        }

        final Set<Integer> expandedRowIds = bikTree.getTreeGrid().getExpandedRowIds();

        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$EXPAND_NODES_BY_IDS, "getExpandedBeforeFetch for expandedRowIds=" + expandedRowIds);
        }

        return expandedRowIds;
    }

    public boolean showAddMainBranchBtn() {
        return tb.showAddMainBranchBtn;
    }
}
