/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.Comparator;

/**
 *
 * @author wezyr
 */
public class BestValueCollector<T> implements ILameCollector<T> {

    private T bestVal;
    private int bestValIdx = -1;
    private int currIdx;
    private Comparator<T> cmpr;

    public BestValueCollector(Comparator<T> cmpr) {
        this.cmpr = cmpr;
    }

    public void add(T item) {
        if (bestValIdx < 0 || cmpr.compare(bestVal, item) > 0) {
            bestVal = item;
            bestValIdx = currIdx;
        }
        currIdx++;
    }

    public T getBestVal() {
        return bestVal;
    }

    public int getBestValIdx() {
        return bestValIdx;
    }
}
