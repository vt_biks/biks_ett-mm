/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.oracle;

/**
 *
 * @author tflorczak
 */
public class OracleDependencyBean {

    public String server;
    public String refOwner;
    public String refName;
    public String depOwner;
    public String depName;

    public OracleDependencyBean(String server, String refOwner, String refName, String depOwner, String depName) {
        this.server = server;
        this.refOwner = refOwner;
        this.refName = refName;
        this.depOwner = depOwner;
        this.depName = depName;
    }
}
