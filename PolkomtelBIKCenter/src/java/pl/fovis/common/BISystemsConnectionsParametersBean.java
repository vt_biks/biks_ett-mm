/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.List;
import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import pl.bssg.metadatapump.common.ConnectionParametersDynamicAxBean;
import pl.bssg.metadatapump.common.ConnectionParametersFileSystemBean;
import pl.bssg.metadatapump.common.ConnectionParametersJdbcBean;
import pl.bssg.metadatapump.common.ConnectionParametersPlainFileBean;

/**
 *
 * @author wezyr
 */
public class BISystemsConnectionsParametersBean implements Serializable {

    public ConnectionParametersSapBoExBean sapboCPB;
    public ConnectionParametersSapBo4ExBean sapbo4CPB;
    public ConnectionParametersTeradataBean teradataCPB;
    public ConnectionParametersDQCBean dqcCPB;
    public ConnectionParametersADBean adCPB;
    public List<ConnectionParametersDBServerBean> oracleCPB;
    public List<ConnectionParametersDBServerBean> postgresCPB;
    public ConnectionParametersMsSqlBean mssqlCPB;
    public List<ConnectionParametersJdbcBean> jdbcCPB;
    public List<ConnectionParametersFileSystemBean> fileSystemCPB;
    public List<ConnectionParametersPlainFileBean> plainFileCPB;
    public List<ConnectionParametersDynamicAxBean> dynamicAxCPB;
    public ConnectionParametersSASBean sasCPB;
    public ConnectionParametersSapBWBean sapbwCPB;
    public ConnectionParametersProfileBean profileCPB;
    public ConnectionParametersErwinBean erwinCPB;
    public ConnectionParametersConfluenceBean confluenceCPB;
    public ConnectionParametersDQMBean dqmCPB;
    public List<ParametersAnySqlBean> anySqlsPB;
    
}
