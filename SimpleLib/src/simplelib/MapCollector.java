/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import simplelib.BaseUtils.Projector;

/**
 *
 * @author wezyr
 */
public class MapCollector<K, V> extends MapCollectorEx<K, V, V> {

    public MapCollector(Projector<V, K> keyProj) {
        super(keyProj, null, null);
    }

    @Override
    protected V convertItem(V item) {
        return item;
    }
}
