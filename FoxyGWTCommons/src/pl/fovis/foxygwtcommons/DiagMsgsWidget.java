/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.LameBag;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public class DiagMsgsWidget extends Composite {

    protected FlexTable grid = new FlexTable();
    protected ScrollPanel scrollPnl = new ScrollPanel(grid);
    protected TextArea taDiagMsgs = new TextArea();
//    protected Map<String, TextBox> editableProps = new LinkedHashMap<String, TextBox>();
    protected FlowPanel fpAllowedKindsInUse = new FlowPanel();
    protected FlowPanel fpDisallowedKindsInUse = new FlowPanel();
    protected Label lblAllowedKindLabel = new Label("allowed" /* I18N: no */ + ":");
    protected Label lblDisallowedKindLabel = new Label("disallowed" /* I18N: no */ + ":");
    protected Set<String> disallowedKinds = new HashSet<String>();
    protected Set<String> allowedKinds = new HashSet<String>();
    protected IParametrizedContinuation<Map<String, String>> savePropsCont;

    public DiagMsgsWidget(IParametrizedContinuation<Map<String, String>> savePropsCont) {
        this.savePropsCont = savePropsCont;

        scrollPnl.setSize("100%", (Window.getClientHeight() - 60) + "px");

        grid.setWidth("99%");
        grid.setHeight("100%");

        initWidget(scrollPnl);

        taDiagMsgs.setSize(
                //(Window.getClientWidth() - 200) + "px"
                "99%",
                (Window.getClientHeight() - 135) + "px" //"100%"
        );

        grid.getFlexCellFormatter().setWidth(0, 0, "1%");
        grid.setText(0, 0, "diagMsgs:");
        grid.getFlexCellFormatter().setWidth(0, 1, "98%");
        grid.getFlexCellFormatter().setStyleName(0, 0, "diagMsgsWidgetLabel");
        grid.getFlexCellFormatter().setWidth(0, 2, "1%");
        grid.getFlexCellFormatter().setHorizontalAlignment(0, 2, HasHorizontalAlignment.ALIGN_RIGHT);
        grid.setWidget(0, 2, new Button("Clear" /* I18N: no */) {

            {
                addClickHandler(new ClickHandler() {

                    public void onClick(ClickEvent event) {
                        ClientDiagMsgs.clearDiagMsgs();
                    }
                });
            }
        });
        grid.getFlexCellFormatter().setColSpan(1, 0, 3);
        grid.setWidget(1, 0, taDiagMsgs);
        grid.setWidget(2, 0, lblAllowedKindLabel);
        grid.getFlexCellFormatter().setStyleName(2, 0, "diagMsgsWidgetLabel");
        grid.getFlexCellFormatter().setColSpan(2, 1, 2);
        grid.getFlexCellFormatter().setWidth(2, 1, "99%");
        grid.setWidget(2, 1, fpAllowedKindsInUse);
        grid.setWidget(3, 0, lblDisallowedKindLabel);
        grid.getFlexCellFormatter().setStyleName(3, 0, "diagMsgsWidgetLabel");
        grid.getFlexCellFormatter().setColSpan(3, 1, 2);
        grid.getFlexCellFormatter().setWidth(3, 1, "99%");
        grid.setWidget(3, 1, fpDisallowedKindsInUse);
    }

    public void setDiagMsgs(Collection<String> diagMsgs) {
        taDiagMsgs.setText(BaseUtils.mergeWithSepEx(diagMsgs, "\n"));
    }

//    public void setAllowedKinds(Collection<String> allowedKinds) {
//        fpAllowedKindsInUse.setText(BaseUtils.mergeWithSepEx(allowedKinds, ","));
//    }
    protected void addOneKindInUse(FlowPanel dst, String kind, Integer cnt, Set<String> declaredKinds,
            IContinuation action) {

        int definedLen = 0;
        String byDeclaredKind = null;

        if (declaredKinds.contains(kind)) {
            definedLen = kind.length();
            byDeclaredKind = kind;
        } else {
            for (String declaredKind : declaredKinds) {
                if (kind.startsWith(declaredKind)) {
                    definedLen = declaredKind.length();
                    byDeclaredKind = declaredKind;
                    break;
                }
            }
        }

        StringBuilder sb = new StringBuilder();

        if (dst.getWidgetCount() != 0) {
            sb.append(", ");
        }

        if (definedLen > 0) {
            sb.append("<b>");
            sb.append(kind.substring(0, definedLen));
            sb.append("</b>");
            sb.append(kind.substring(definedLen));
        } else {
            sb.append(kind);
        }

        if (cnt != null) {
            sb.append(" (").append(cnt).append(")");
        }

        if (action != null) {
            sb.append(" ");
        }

        InlineHTML h = new InlineHTML(sb.toString());
        if (byDeclaredKind != null) {
            h.setTitle("implied by" /* I18N: no */ + " [" + byDeclaredKind + "]" + (byDeclaredKind.equals(kind) ? " (" + "self" /* I18N: no */ + ")" : ""));
        }

        dst.add(h);

        if (action != null) {
            InlineHTML btn = new InlineHTML("[" + "x" /* I18N: no */ + "]");
            Style style = btn.getElement().getStyle();

            style.setCursor(Cursor.POINTER);
            style.setBackgroundColor("#" + "ccc" /* I18N: no */);
            GWTUtils.addOnClickHandler(btn, action);
            dst.add(btn);
//            dst.add(new BikActionButton("[x]", action));
        }
    }

    public void setAllowedKindsInUseCounters(LameBag<String> allowedKindsCounters) {
//        StringBuilder sb = new StringBuilder();
//
//        boolean isFirst = true;
//        for (String k : allowedKindsCounters.uniqueSet()) {
//            if (isFirst) {
//                isFirst = false;
//            } else {
//                sb.append(", ");
//            }
//            sb.append(k).append(" (").append(allowedKindsCounters.getCount(k)).append(")");
//        }
        //fpAllowedKindsInUse.setText(sb.toString());

        fpAllowedKindsInUse.clear();

        for (String kind : allowedKindsCounters.uniqueSet()) {
            final String kindFinal = kind;
            int cnt = allowedKindsCounters.getCount(kind);
            addOneKindInUse(fpAllowedKindsInUse, kind, cnt, allowedKinds, new IContinuation() {

                public void doIt() {
                    disallowedKinds.add(kindFinal);
                    allowedKinds.remove(kindFinal);
                    saveModifiedDeclaredKinds();
                }
            });
        }
    }

    protected void saveModifiedDeclaredKinds() {

        Map<String, String> modifiedProps = new HashMap<String, String>();
        modifiedProps.put("disallowedShowDiagKinds", BaseUtils.mergeWithSepEx(disallowedKinds, ","));
        modifiedProps.put("allowedShowDiagKinds", BaseUtils.mergeWithSepEx(allowedKinds, ","));

        savePropsCont.doIt(modifiedProps);

//        BIKClientSingletons.getService().setEditableAppProps(modifiedProps, new AsyncCallback<List<AppPropBean>>() {
//
//            public void onFailure(Throwable caught) {
//                Window.alert("error!");
//            }
//
//            public void onSuccess(List<AppPropBean> result) {
//                BIKClientSingletons.setupAppProps(result);
//
//                Window.alert("saved! current messages and kinds - cleared...");
//            }
//        });
    }

    public void setDisallowedKindsInUse(Collection<String> disallowedKindsInUse) {
        fpDisallowedKindsInUse.clear();

//        Set<String> allDisallowedKinds = new HashSet<String>(disallowedKindsInUse);
//        allDisallowedKinds.addAll(disallowedKinds);
        for (String kind : disallowedKindsInUse) {
            final String kindFinal = kind;
            addOneKindInUse(fpDisallowedKindsInUse, kind, null, this.disallowedKinds, new IContinuation() {

                public void doIt() {
                    allowedKinds.add(kindFinal);
                    disallowedKinds.remove(kindFinal);
                    saveModifiedDeclaredKinds();
                }
            });
        }
    }

    public void setDeclaredDisallowedKinds(Set<String> disallowedKinds) {
        this.disallowedKinds = disallowedKinds;
        lblDisallowedKindLabel.setTitle("declared" /* I18N: no */ + ": " + BaseUtils.mergeWithSepEx(disallowedKinds, ","));
    }

    public void setDeclaredAllowedKinds(Set<String> allowedKinds) {
        this.allowedKinds = allowedKinds;
        lblAllowedKindLabel.setTitle("declared" /* I18N: no */ + ": " + BaseUtils.mergeWithSepEx(allowedKinds, ","));
    }
}
