﻿exec sp_check_version '1.4.y1.17';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-----------------   Dodanie konektora do SAP BO 4.0
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

insert into bik_admin(param, value)
values('sapbo4.path', 'C:/Temp/designer-pump/BOXIUniversDataPump4BO4.exe')

update bik_app_prop
set val = replace(val, 'Business Objects,', 'Business Objects 3.1,')
from bik_app_prop
where name = 'availableConnectors'

update bik_data_source_def
set description = 'Business Objects 3.1'
where description = 'Business Objects'

insert into bik_data_source_def(description)
values ('Business Objects 4.x')

declare @connectors varchar(max);
select @connectors = val from bik_app_prop where name = 'availableConnectors'

update bik_app_prop
set val = @connectors + ',Business Objects 4.x'
where name = 'availableConnectors'

insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
values('DSL.MetaDataFile', 'Świat obiektów UNX', 'universeUNX', 'Metadata', 0, 14, 1)

insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
values('CommonConnection', 'Połączenie OLAP', 'connectionOLAP', 'Metadata', 0, 10, 1)

insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
values('CCIS.DataConnection', 'Połączenie relacyjne', 'connectionRel', 'Metadata', 0, 10, 1)

-- tranlate for node kinds
insert into bik_translation(code, txt, lang, kind)
values('DSL.MetaDataFile', 'Universe (information design tool)', 'en', 'kind')

insert into bik_translation(code, txt, lang, kind)
values('CommonConnection', 'OLAP Connection', 'en', 'kind')

insert into bik_translation(code, txt, lang, kind)
values('CCIS.DataConnection', 'Relational Connection', 'en', 'kind')

create table bik_sapbo_olap_connection (
	node_id int not null primary key references bik_node(id),
	type varchar(max) null,
	provider_caption varchar(max) null,
	cube_caption varchar(max) null,
	cuid varchar(max) null,
	created datetime null,
	modified datetime null,
	owner varchar(max) null,
	network_layer varchar(max) null,
	md_username varchar(max) null
);

alter table bik_sapbo_schedule
add schedule_type int null
go

alter table aaa_report_schedule
add schedule_type int null
go

alter table bik_sapbo_extradata
alter column cuid varchar(100) null
go

create unique index idx_bik_sapbo_extradata_cuid_node_id on bik_sapbo_extradata (cuid,node_id)
go

-- fix po babalu Wezyra!!!
declare @attr_category_id_sapbw int
select @attr_category_id_sapbw = id from bik_attr_category where name = 'SAP BW' and is_built_in = 1

update bik_attr_def
set attr_category_id = @attr_category_id_sapbw
where name in ('Występuje w obszarze', 'Ostatnio zmieniony przez') and is_built_in = 1

--
declare @catId int;
select @catId = id from bik_attr_category where name = 'SAP BO' and is_built_in = 1

insert into bik_attr_def(name, attr_category_id,is_deleted,is_built_in)
values ('Status', @catId, 0, 1)

insert into bik_attr_def(name, attr_category_id,is_deleted,is_built_in)
values ('Dostawca', @catId, 0, 1)

insert into bik_attr_def(name, attr_category_id,is_deleted,is_built_in)
values ('Kostka', @catId, 0, 1)

insert into bik_attr_def(name, attr_category_id,is_deleted,is_built_in)
values ('Występowanie w innych instancjach', @catId, 0, 1)

insert into bik_translation(code, txt, lang, kind)
values ('Status', 'Status', 'en', 'adef')

insert into bik_translation(code, txt, lang, kind)
values ('Dostawca', 'Provider', 'en', 'adef')

insert into bik_translation(code, txt, lang, kind)
values ('Kostka', 'Cube', 'en', 'adef')

insert into bik_translation(code, txt, lang, kind)
values ('Występowanie w innych instancjach', 'Occurrence in other instances', 'en', 'adef')

declare @statusId int, @providerId int, @cubeId int, @otherId int, @cuidId int, @createdId int, @modifiedId int, @authorId int, @publishId int, @boModifiedId int, @guidId int, @ruidId int, @progId int;
select @statusId = id from bik_attr_def where name = 'Status' and is_built_in = 1
select @providerId = id from bik_attr_def where name = 'Dostawca' and is_built_in = 1
select @cubeId = id from bik_attr_def where name = 'Kostka' and is_built_in = 1
select @otherId = id from bik_attr_def where name = 'Występowanie w innych instancjach' and is_built_in = 1
select @cuidId = id from bik_attr_def where name = 'ID, CUID' and is_built_in = 1
select @createdId = id from bik_attr_def where name = 'Utworzono' and is_built_in = 1
select @modifiedId = id from bik_attr_def where name = 'Ostatnia modyfikacja' and is_built_in = 1
select @authorId = id from bik_attr_def where name = 'Właściciel w repozytorium BO' and is_built_in = 1
select @publishId = id from bik_attr_def where name = 'Opublikowano w repozytorium BO' and is_built_in = 1
select @boModifiedId = id from bik_attr_def where name = 'Ostatnia modyfikacja w repozytorium BO' and is_built_in = 1
select @guidId = id from bik_attr_def where name = 'Guid' and is_built_in = 1
select @ruidId = id from bik_attr_def where name = 'Ruid' and is_built_in = 1
select @progId = id from bik_attr_def where name = 'Progid' and is_built_in = 1

declare @scheduleKind int = dbo.fn_node_kind_id_by_code('ReportSchedule')
declare @oldConn int = dbo.fn_node_kind_id_by_code('DataConnection')
declare @relConn int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection')
declare @olapConn int = dbo.fn_node_kind_id_by_code('CommonConnection')
declare @newUniv int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile')

-- dla harmonogramow
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@scheduleKind, @statusId, 1)

-- dla polaczen relacyjnych
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
select @relConn, attr_id, is_visible
from bik_attr_system 
where node_kind_id = @oldConn

-- dla polaczen olap
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@olapConn, @cuidId, 1)
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@olapConn, @providerId, 1)
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@olapConn, @cubeId, 1)
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@olapConn, @createdId, 1)
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@olapConn, @modifiedId, 1)
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@olapConn, @authorId, 1)

-- dla universów UNX
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@newUniv, @cuidId, 1)
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@newUniv, @guidId, 1)
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@newUniv, @ruidId, 1)
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@newUniv, @progId, 1)
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@newUniv, @boModifiedId, 1)
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@newUniv, @publishId, 1)
insert into bik_attr_system(node_kind_id, attr_id, is_visible)
values (@newUniv, @authorId, 1)

-- dodanie drzewek i konfiguracji pod BO4
    declare @reportId int, @universeId int, @connId int;
    declare @treeOfTreesId int = dbo.fn_tree_id_by_code('TreeOfTrees');

    -- ustawiam code dla raportów, światów obiektów i połączeń
    declare @reportCode varchar(255) = 'Reports'
    declare @reportNum int = 1
    declare @reportCodeBase varchar(255) = @reportCode

    while exists(select 1 from bik_tree where code = @reportCode)
    begin
    set @reportNum = @reportNum + 1  
    set @reportCode = @reportCodeBase + cast(@reportNum as varchar(20))
    end

    declare @universeCode varchar(255) = 'ObjectUniverses'
    declare @universeNum int = 1
    declare @universeCodeBase varchar(255) = @universeCode

    while exists(select 1 from bik_tree where code = @universeCode)
    begin
    set @universeNum = @universeNum + 1  
    set @universeCode = @universeCodeBase + cast(@universeNum as varchar(20))
    end

    declare @connectionCode varchar(255) = 'Connections'
    declare @connectionNum int = 1
    declare @connectionCodeBase varchar(255) = @connectionCode

    while exists(select 1 from bik_tree where code = @connectionCode)
    begin
    set @connectionNum = @connectionNum + 1  
    set @connectionCode = @connectionCodeBase + cast(@connectionNum as varchar(20))
    end

    insert into bik_tree(name, code, tree_kind, branch_level_for_statistics, is_hidden)
    values('Raporty', @reportCode, 'Metadata', 2, 1)

    select @reportId = scope_identity()

    insert into bik_tree(name, code, tree_kind, branch_level_for_statistics, is_hidden)
    values('Światy obiektów', @universeCode, 'Metadata', 2, 1)

    select @universeId = scope_identity()

    insert into bik_tree(name, code, tree_kind, branch_level_for_statistics, is_hidden)
    values('Połączenia', @connectionCode, 'Metadata', 0, 1)

    select @connId = scope_identity()
    
    -- dodanie translacji
    insert into bik_translation(code, txt, lang, kind)
	values(@reportCode, 'Reports', 'en', 'tree')
	
    insert into bik_translation(code, txt, lang, kind)
	values(@universeCode, 'Universes', 'en', 'tree')
	
    insert into bik_translation(code, txt, lang, kind)
	values(@connectionCode, 'Connections', 'en', 'tree')


    declare @sourceId int, @priority int;
    select @sourceId = id from bik_data_source_def where description = 'Business Objects 4.x'
    select @priority = max(priority) from bik_schedule where source_id = @sourceId

	insert into bik_schedule(source_id, hour, minute, interval, is_schedule, priority, instance_name)
	values(@sourceId, 0, 0, 1, 0, 6, 'BO4')

    -- dodanie do modrzewia
    declare @boCode varchar(255) = 'sap4bo'
    declare @boNum int = 1
    declare @boCodeBase varchar(255) = @boCode

    while exists(select 1 from bik_node where tree_id = @treeOfTreesId and obj_id = @boCode and is_deleted = 0)
    begin
    set @boNum = @boNum + 1  
    set @boCode = @boCodeBase + cast(@boNum as varchar(20))
    end

    set @reportCode = '$' + @reportCode;
    set @universeCode = '$' + @universeCode;
    set @connectionCode = '$' + @connectionCode;


    exec sp_add_menu_node 'metadata', 'BO4', @boCode
    exec sp_add_menu_node @boCode, '@', @reportCode
    exec sp_add_menu_node @boCode, '@', @universeCode
    exec sp_add_menu_node @boCode, '@', @connectionCode


    declare @parent varchar(50);
    declare @child varchar(50);

    -- raporty
    set @child = '&ReportFolder' + @reportCode;
    exec sp_add_menu_node @reportCode, '@', @child
    set @parent = '&ReportFolder' + @reportCode
    set @child = '&Webi' + @reportCode;
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&FullClient' + @reportCode;
    exec sp_add_menu_node @parent, '@', @child
    set @parent = '&Webi' + @reportCode;
    set @child = '&ReportQuery' + @reportCode;
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&ReportSchedule' + @reportCode;
    exec sp_add_menu_node @parent, '@', @child
    set @parent = '&FullClient' + @reportCode;
    exec sp_add_menu_node @parent, '@', @child
    set @parent = '&ReportQuery' + @reportCode;
    set @child = '&Dimension' + @reportCode;
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&Measure' + @reportCode
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&Detail' + @reportCode
    exec sp_add_menu_node @parent, '@', @child
    set @parent = '&ReportFolder' + @reportCode;
    set @child = '&Flash' + @reportCode;
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&Excel' + @reportCode;
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&Hyperlink' + @reportCode;
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&Powerpoint' + @reportCode;
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&Pdf' + @reportCode;
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&CrystalReport' + @reportCode;
    exec sp_add_menu_node @parent, '@', @child
    -- światy obiektów
    set @child = '&UniversesFolder' + @universeCode
    exec sp_add_menu_node @universeCode, '@', @child
    set @parent = '&UniversesFolder' + @universeCode;
    set @child = '&Universe' + @universeCode;
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&DSL.MetaDataFile' + @universeCode;
    exec sp_add_menu_node @parent, '@', @child
    set @parent = '&Universe' + @universeCode
    set @child = '&UniverseDerivedTable' + @universeCode
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&UniverseAliasTable' + @universeCode;
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&UniverseTable' + @universeCode;
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&UniverseClass' + @universeCode;
    exec sp_add_menu_node @parent, '@', @child
    set @parent = '&UniverseClass' + @universeCode
    set @child = '&Dimension' + @universeCode;
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&Measure' + @universeCode;
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&Filter' + @universeCode
    exec sp_add_menu_node @parent, '@', @child
    set @parent = '&Dimension' + @universeCode
    set @child = '&Detail' + @universeCode
    exec sp_add_menu_node @parent, '@', @child
    -- połączenia
    set @child = '&ConnectionNetworkFolder' + @connectionCode;
    exec sp_add_menu_node @connectionCode, '@', @child
    set @parent = '&ConnectionNetworkFolder' + @connectionCode;
    set @child = '&ConnectionEngineFolder' + @connectionCode;
    exec sp_add_menu_node @parent, '@', @child
    set @parent = '&ConnectionEngineFolder' + @connectionCode;
    set @child = '&CommonConnection' + @connectionCode
    exec sp_add_menu_node @parent, '@', @child
    set @child = '&CCIS.DataConnection' + @connectionCode
    exec sp_add_menu_node @parent, '@', @child

    exec sp_node_init_branch_id @treeOfTreesId, null
--

insert into bik_sapbo_server (source_id, name, server, user_name, password, with_users, open_report_template, check_rights, report_tree_id, universe_tree_id, connection_tree_id, is_active, is_deleted)
values(@sourceId, 'BO4', '', '', '', 1, 'http://<nazwa_serwera>:8080/BOE/OpenDocument/opendoc/openDocument.jsp?iDocID={si_id}', 1,  @reportId, @universeId, @connId, 0, 0)

-- poprzednia wersja w: alter db for v1.4.y1.2 tf.sql
-- fix dla BO4
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node]
go

create procedure [dbo].[sp_insert_into_bik_node](@tree_code varchar(255))
as
	declare @table_folders varchar(255);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	declare @diag_level int = 0;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'

	if(@tree_code = 'Connections')
	begin
		set @kinds = '''MetaData.DataConnection'', ''CCIS.DataConnection'', ''CommonConnection''';		
	end;

	if(@tree_code = 'ObjectUniverses')
	begin
		set @kinds = '''Universe'', ''DSL.MetaDataFile''';
	end;		
	
	if(@tree_code='Reports')
	begin
		-- wybranie folderw raportów bez raportów użytkowników
		set @table_folders = ' INFO_FOLDER where si_name not like ''~Webintelligence%''';
		--set @table_folders = ' INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 is null or SI_PATH__SI_FOLDER_NAME2 != ''User Folders''';
		set @kinds = '''Webi'', ''Flash'', ''CrystalReport'',''Excel'',''FullClient'',''Pdf'',''Hyperlink''
		,''Rtf'',''Txt'',''Powerpoint'',''Word''';
	end;
	
    declare @sql nvarchar(max);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		select @tree_id = id from bik_tree where code = @tree_code;
		set @sql = N'select branch_names as SI_ID, parent_branch_names	as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata';
	end;
	if(@tree_code = 'Reports')
	begin
		set @tree_id = dbo.fn_get_bo_actual_report_tree_id();
		
		declare @rootFolderName varchar(max) = null, @userFolderName varchar(max) = null;
		select @rootFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$RootFolder' and is_deleted = 0
		select @userFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$UserFolder' and is_deleted = 0
		
		-- sztuczny podzial na root folder i user folders dla raportów
		set @sql = N'select distinct convert(varchar(30),SI_PARENT_FOLDER) as SI_ID, ''#BO$UserFolder'' as SI_PARENTID, SI_KIND, SI_PATH__SI_FOLDER_NAME1 as SI_NAME, NULL as DESCR
				 from INFO_FOLDER where SI_PATH__SI_FOLDER_ID2 in (select si_id from INFO_FOLDER where SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c'') 
				 and SI_NAME not like ''~Webintelligence%'' 
				
				 union all
				
				 select case when SI_CUID = ''ASHnC0S_Pw5LhKFbZ.iA_j4'' then ''#BO$RootFolder'' when SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c'' then ''#BO$UserFolder'' else convert(varchar(30),si_id) end as si_id, case
									when SI_PARENTID = 0 and SI_CUID <> ''AWigQI18AAZJoXfRHLzWJ2c''
										then ''#BO$RootFolder''
									when si_id in (select si_id from INFO_FOLDER where SI_PATH__SI_FOLDER_ID2 in (select si_id from INFO_FOLDER where SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c''))
										then convert(varchar(30),SI_PARENTID)
									when SI_PARENTID in(select si_id from INFO_FOLDER)
										then convert(varchar(30),SI_PARENTID)
									else ''0'' end as SI_PARENTID, SI_KIND, case
																			when SI_CUID = ''ASHnC0S_Pw5LhKFbZ.iA_j4''
																				then coalesce(' + coalesce(QUOTENAME(substring(@rootFolderName,1,128),''''),'null') + ',SI_NAME)
																			when SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c''
																				then coalesce(' + coalesce(QUOTENAME(substring(@userFolderName,1,128), ''''),'null') + ',SI_NAME)
																			else SI_NAME end, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + '
				
				 union all
				
				 select convert(varchar(30),SI_ID), convert(varchar(30),SI_PARENTID), SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') and SI_INSTANCE = 0
				
				union all
				
				select convert(varchar(30),SI_ID), convert(varchar(30),SI_PARENTID), SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR
				from aaa_global_props where si_parentid in (select si_parentid from INFO_FOLDER where SI_PATH__SI_FOLDER_ID2 in (select si_id from INFO_FOLDER where SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c'') and SI_NAME <> ''~Webintelligence'') and si_kind <> ''Folder''
				
				union all
				
				select convert(varchar(30),id), convert(varchar(30),parent_id), ''ReportSchedule'', name, null
				from aaa_report_schedule sch
				inner join aaa_global_props gp on gp.si_id = sch.parent_id';
	end;
	if(@tree_code = 'Connections')
	begin
		set @tree_id = dbo.fn_get_bo_actual_connection_tree_id();
		
		declare @conenctionFolderName varchar(max) = null;
		select @conenctionFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$ConnectionFolder' and is_deleted = 0
		
		set @sql = N'select ''#BO$ConnectionFolder'' as si_id, ''0'' as si_parentid, si_kind, coalesce(' + coalesce(QUOTENAME(substring(@conenctionFolderName,1,128), ''''),'null') + ',SI_NAME) as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR from app_folder where SI_CUID = ''AZVXOgKIBEdOkiXzUuybja4''
		
			 union all
			 
			 select ''#BO$ConnectionOLAPFolder'', ''#BO$ConnectionFolder'', ''Folder'', ''OLAP'', null
			 
			 union all
			 
			 select convert(varchar(30),SI_ID), case when SI_KIND = ''CommonConnection'' then ''#BO$ConnectionOLAPFolder'' else ''#BO$ConnectionFolder'' end, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
			 from aaa_global_props
			 where SI_KIND in ('+ @kinds +') and SI_OBJECT_IS_CONTAINER = 0';
	end;
	if(@tree_code = 'ObjectUniverses')
	begin
		set @tree_id = dbo.fn_get_bo_actual_universe_tree_id();
		
		declare @universeFolderName varchar(max) = null;
		select @universeFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$UniverseFolder' and is_deleted = 0
		
		set @sql = N'select case when SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'' then ''#BO$UniverseFolder'' else convert(varchar(30),si_id) end as si_id, case 
								when SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk''
									then ''0''
								when SI_PARENTID in (select si_id from app_folder where SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'') 
									then ''#BO$UniverseFolder''
								when SI_PARENTID in(select si_id from APP_FOLDER) 
									then convert(varchar(30),SI_PARENTID)
								else ''0'' end as SI_PARENTID, SI_KIND, case when SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'' then coalesce(' + coalesce(QUOTENAME(substring(@universeFolderName,1,128), ''''),'null') + ',SI_NAME) else SI_NAME end as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
			 from APP_FOLDER
			 
			 union all
			 
			 select convert(varchar(30),SI_ID), case when SI_PARENTID in (select si_id from app_folder where SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'') then ''#BO$UniverseFolder'' else convert(varchar(30),SI_PARENTID) end, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
			 from aaa_global_props
			 where SI_PARENTID in (select si_id from APP_FOLDER) 
					and SI_KIND in ('+ @kinds +')';
	end;					

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed zapytaniem'
	print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(1000) collate DATABASE_DEFAULT primary key, si_parentid varchar(1000) collate DATABASE_DEFAULT, si_kind varchar(max) collate DATABASE_DEFAULT, si_name varchar(max) collate DATABASE_DEFAULT, descr varchar(max) collate DATABASE_DEFAULT);
	if(@tree_code = 'Reports')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ReportFolder'' 
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	if(@tree_code = 'Connections')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ConnectionFolder''
										when si_kind=''MetaData.DataConnection'' then ''DataConnection''
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	if(@tree_code = 'ObjectUniverses')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''UniversesFolder'' else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	if(@tree_code = 'Teradata')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, si_kind, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po stworzeniu drugiego @sql'
			   
	EXECUTE(@sql);

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);
	
	-- fix na foldery główne
	if(@tree_code = 'Reports' or @tree_code = 'ObjectUniverses' or @tree_code = 'Connections')
	begin
		delete from #tmpNodes
		where si_parentid = '0' and si_id not in ('#BO$RootFolder', '#BO$UserFolder', '#BO$UniverseFolder', '#BO$ConnectionFolder')
		
		declare @rc1 int = 1
		while @rc1 > 0 begin
			delete from #tmpNodes
			where si_parentid <> '0' and not exists(select 1 from #tmpNodes g where g.si_id = #tmpNodes.si_parentid)
			
			set @rc1 = @@ROWCOUNT
		end;
	end;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po fixie rootów dla #tmpNodes'
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/

	declare @rc int = 1

	while @rc > 0 begin
	
	delete from #tmpNodes
	where (si_kind = 'UniversesFolder' or si_kind = 'ConnectionFolder' or si_kind = 'ReportFolder') and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/
	
	declare @report_tree_id int = dbo.fn_get_bo_actual_report_tree_id();

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updatetami'

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.descr, tree_id = @tree_id
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.tree_id = @tree_id and bik_node.obj_id = #tmpNodes.si_id and bik_node.linked_node_id is null
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed update podlinkowanych'
	--update podlinkowanych
	update bik_node 
	set /*parent_node_id = null, */node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		 descr = #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and (tree_id = @report_tree_id or tree_id in (select id from bik_tree where tree_kind in (select code from bik_tree_kind where allow_linking = 1))) and bik_node.linked_node_id is not null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed insertami'
	
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id
	where bik_node.id is null;

	set nocount on;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updateami parentow'
	

	update bik_node 
	set parent_node_id = bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id
	inner join bik_node bk on bk.tree_id = @tree_id and bk.obj_id = pt.si_parentid where bik_node.linked_node_id is null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed usuwaniem'
	
	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in 
	('Measure', 'Dimension', 'Detail', 'UniverseClass', 'ReportQuery', 'Filter', 'UniverseColumn', 'UniverseTable', 'UniverseAliasTable', 'UniverseDerivedTable', 'UniverseTablesFolder', 'ConnectionEngineFolder', 'ConnectionNetworkFolder')
				
	drop table #tmpNodes
	
	-- fix na is_built_in
	update bik_node
	set is_built_in = 1
	where tree_id = @tree_id
	and obj_id in ('#BO$RootFolder', '#BO$UserFolder', '#BO$UniverseFolder', '#BO$ConnectionFolder', '#BO$ConnectionOLAPFolder')
	and is_built_in = 0
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po usuwaniu i po dropie'

	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': koniec'
end;
go

-- poprzednia wersja w: alter db for v1.3.0.6 tf.sql
-- dodanie: extradata dla OLAP
if exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
as
begin
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	declare @boServer int = dbo.fn_get_bo_actual_server_id();

	delete from bik_sapbo_extradata
	from bik_sapbo_extradata
	inner join bik_node bn on bn.id = bik_sapbo_extradata.node_id
	where bn.tree_id in (@reportTreeId , @universeTreeId, @connectionTreeId)
	
	
	declare @tempTable table (
		id int identity(1,1) not null,
		column_name varchar(50) not null,
		table_name varchar(50) not null
	);
	
	-- ważna jest kolejnosc wstawiania
	insert into @tempTable(column_name,table_name)
	values
		('SI_AUTHOR','INFO_WEBI'),
		('SI_OWNER','aaa_global_props'),
		('SI_KEYWORD','aaa_global_props'),
		('SI_FILES__SI_PATH','aaa_global_props'),
		('SI_FILES__SI_FILE1','aaa_global_props'),
		('SI_CREATION_TIME','aaa_global_props'),
		('SI_UPDATE_TS','aaa_global_props'),
		('SI_CUID','aaa_global_props'),
		('SI_FILES__SI_VALUE1','aaa_global_props'),
		('SI_SIZE','aaa_global_props'),
		('SI_HAS_CHILDREN','aaa_global_props'),
		('SI_GUID','aaa_global_props'),
		('SI_INSTANCE','aaa_global_props'),
		('SI_OWNERID','aaa_global_props'),
		('SI_PROGID','aaa_global_props'),
		('SI_OBTYPE','aaa_global_props'),
		('SI_FLAGS','aaa_global_props'),
		('SI_CHILDREN','aaa_global_props'),
		('SI_RUID','aaa_global_props'),
		('SI_RUNNABLE_OBJECT','aaa_global_props'),
		('SI_CONTENT_LOCALE','aaa_global_props'),
		('SI_IS_SCHEDULABLE','aaa_global_props'),
		('SI_WEBI_DOC_PROPERTIES','aaa_global_props'),
		('SI_READ_ONLY','aaa_global_props'),
		('SI_LAST_SUCCESSFUL_INSTANCE_ID','aaa_global_props'),
		('SI_LAST_RUN_TIME','aaa_global_props'),
		('SI_TIMESTAMP','INFO_WEBI'),
		('SI_PROGID_MACHINE','INFO_WEBI'),
		('SI_ENDTIME','aaa_global_props'),
		('SI_STARTTIME','aaa_global_props'),
		('SI_SCHEDULE_STATUS','aaa_global_props'),
		('SI_RECURRING','aaa_global_props'),
		('SI_NEXTRUNTIME','aaa_global_props'),
		('SI_DOC_SENDER','aaa_global_props'),
		('SI_REVISIONNUM','APP_UNIVERSE'),
		('SI_APPLICATION_OBJECT','APP_UNIVERSE'),
		('SI_SHORTNAME','APP_UNIVERSE'),
		('SI_DATACONNECTION__SI_TOTAL','APP_UNIVERSE'),
		('SI_UNIVERSE__SI_TOTAL','INFO_WEBI')
		
	declare @sqlText varchar(max);
	declare @count int;
	select @count = count(*) from @tempTable
	
	set @sqlText = 'insert into bik_sapbo_extradata (node_id,author,owner,keyword,file_path,file_name,created,modified,cuid,files_value,size,has_children,guid,instance,owner_id,progid,obtype,flags,children,ruid,runnable_object,content_locale,is_schedulable,webi_doc_properties,read_only,last_successful_instance_id,last_run_time,timestamp,progid_machine,endtime,starttime,schedule_status,recurring,nextruntime,doc_sender,revisionnum,application_object,shortname,dataconnection_total,universe_total )
    select bik.id as node_id, '
		
	declare kurs cursor for 
	select column_name, table_name from @tempTable order by id 

	open kurs;
	declare @column varchar(50);
	declare @table varchar(50);
	declare @i int = 1;

	fetch next from kurs into @column, @table;
	while @@fetch_status = 0
	begin
		if exists(select * from sys.columns where name = @column and Object_ID = Object_ID(@table))    
		begin
			set @sqlText = @sqlText + @table + '.' + @column
		end
		else
		begin
			set @sqlText = @sqlText + 'null'
		end
		if(@i <> @count)
		begin
			set @sqlText = @sqlText + ', '
		end
		--
		set @i = @i + 1;
		fetch next from kurs into @column, @table;
	end
	close kurs;
	deallocate kurs;	

	set @sqlText = @sqlText + ' from aaa_global_props
    left join INFO_WEBI on aaa_global_props.si_id = INFO_WEBI.si_id
    left join APP_UNIVERSE on aaa_global_props.si_id = APP_UNIVERSE.si_id
    inner join bik_node bik on convert(varchar(30),aaa_global_props.si_id) = bik.obj_id
    where bik.tree_id in (' + convert(varchar(10),@reportTreeId) + ', ' + convert(varchar(10),@universeTreeId) + ', ' + convert(varchar(10),@connectionTreeId) + ') 
    and bik.is_deleted = 0
    and bik.linked_node_id is null
    order by bik.id'
    
    exec(@sqlText);
	
	/*insert into bik_sapbo_extradata (node_id,author,owner,keyword,file_path,file_name,created,modified,cuid,files_value,size,has_children,guid,instance,owner_id,progid,obtype,flags,children,ruid,runnable_object,content_locale,is_schedulable,webi_doc_properties,read_only,last_successful_instance_id,last_run_time,timestamp,progid_machine,endtime,starttime,schedule_status,recurring,nextruntime,doc_sender,revisionnum,application_object,shortname,dataconnection_total,universe_total )
    select bik.id as node_id,
    nullif(ltrim(rtrim(iw.SI_AUTHOR)), '') as author,
    nullif(ltrim(rtrim(p.SI_OWNER)), '') as owner,
    nullif(ltrim(rtrim(p.SI_KEYWORD)), '') as keyword,
    nullif(ltrim(rtrim(p.SI_FILES__SI_PATH)), '') as file_path,
    nullif(ltrim(rtrim(p.SI_FILES__SI_FILE1)), '') as file_name,
    nullif(ltrim(rtrim(p.SI_CREATION_TIME)), '') as created,
    nullif(ltrim(rtrim(p.SI_UPDATE_TS)), '') as modified,
    nullif(ltrim(rtrim(p.SI_CUID)), '') as cuid,
    nullif(ltrim(rtrim(p.SI_FILES__SI_VALUE1)), '') as files_value,
    nullif(ltrim(rtrim(p.SI_SIZE)), '') as size,
    p.SI_HAS_CHILDREN as has_children,
    nullif(ltrim(rtrim(p.SI_GUID)), '') as guid,
    p.SI_INSTANCE as instance,
    p.SI_OWNERID as owner_id,
    nullif(ltrim(rtrim(p.SI_PROGID)), '') as progid,
    p.SI_OBTYPE as obtype,
    p.SI_FLAGS as flags,
    p.SI_CHILDREN as children,
    nullif(ltrim(rtrim(p.SI_RUID)), '') as ruid,
    p.SI_RUNNABLE_OBJECT as runnable_object,
    nullif(ltrim(rtrim(p.SI_CONTENT_LOCALE)), '') as content_locale,
    p.SI_IS_SCHEDULABLE as is_schedulable,
    nullif(ltrim(rtrim(p.SI_WEBI_DOC_PROPERTIES)), '') as webi_doc_properties,
    p.SI_READ_ONLY as read_only,
    nullif(ltrim(rtrim(p.SI_LAST_SUCCESSFUL_INSTANCE_ID)), '') as last_successful_instance_id,
    nullif(ltrim(rtrim(p.SI_LAST_RUN_TIME)), '') as last_run_time,
    nullif(ltrim(rtrim(iw.SI_TIMESTAMP)), '')  as timestamp,
    nullif(ltrim(rtrim(iw.SI_PROGID_MACHINE)), '') as progid_machine,
    nullif(ltrim(rtrim(p.SI_ENDTIME)), '') as endtime,
    nullif(ltrim(rtrim(p.SI_STARTTIME)), '') as starttime,
    nullif(ltrim(rtrim(p.SI_SCHEDULE_STATUS)), '') as schedule_status,
    nullif(ltrim(rtrim(p.SI_RECURRING)), '') as recurring,
    nullif(ltrim(rtrim(p.SI_NEXTRUNTIME)), '') as nextruntime,
    nullif(ltrim(rtrim(p.SI_DOC_SENDER)), '') as doc_sender,
    au.SI_REVISIONNUM as revisionnum,
    au.SI_APPLICATION_OBJECT as application_object,
    nullif(ltrim(rtrim(au.SI_SHORTNAME)), '') as shortname,
    au.SI_DATACONNECTION__SI_TOTAL as dataconnection_total,
    iw.SI_UNIVERSE__SI_TOTAL as universe_total
    from aaa_global_props p
    left join INFO_WEBI iw on p.si_id = iw.si_id
    left join APP_UNIVERSE au on p.si_id = au.si_id
    inner join bik_node bik on convert(varchar(30),p.si_id) = bik.obj_id
    where bik.tree_id in (@reportTreeId, @universeTreeId, @connectionTreeId)
    and bik.is_deleted = 0
    and bik.linked_node_id is null 
    order by bik.id*/
    
    delete from bik_sapbo_schedule
    from bik_sapbo_schedule
	inner join bik_node bn on bn.id = bik_sapbo_schedule.node_id
	where bn.tree_id = @reportTreeId
    
    insert into bik_sapbo_schedule(node_id,destination,owner,creation_time,nextruntime,expire,type,schedule_type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters)
    select bn.id,destination,owner,creation_time,nextruntime,expire,type,schedule_type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters 
    from aaa_report_schedule sch
    inner join bik_node bn on convert(varchar(30),sch.id) = bn.obj_id
    --inner join bik_tree bt on bt.id = bn.tree_id
    where bn.tree_id = @reportTreeId 
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    --and bt.code in ('Reports')
    

    
	if exists(select 1 from bik_sapbo_server ser 
		inner join bik_data_source_def def on ser.source_id = def.id
		where ser.id = @boServer and def.description = 'Business Objects 4.x') and exists(select 1 from sys.objects where type = 'U' and name = 'APP_COMMONCONNECTION')
	begin
		    delete from bik_sapbo_olap_connection
			from bik_sapbo_olap_connection
			inner join bik_node bn on bik_sapbo_olap_connection.node_id = bn.id
			where bn.tree_id = @connectionTreeId
			
			insert into bik_sapbo_olap_connection(node_id, type, provider_caption, cube_caption, cuid, created, modified, owner, network_layer, md_username)
			select bn.id, app.SI_CONNECTION_TYPE, app.SI_PROVIDER_CAPTION, app.SI_CUBE_CAPTION, app.SI_CUID, app.SI_CREATION_TIME, app.SI_UPDATE_TS, app.SI_OWNER, app.SI_NETWORK_LAYER, app.SI_MD_USERNAME 
			from APP_COMMONCONNECTION app 
			inner join bik_node bn on bn.obj_id = convert(varchar(30),app.si_id) and tree_id = @connectionTreeId
	end
    
end;
go


-- poprzednia wersja w: alter db for v1.3.0.10 tf.sql
-- dodanie połączeń między uni <-> conn i fix dla BO4
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_universe_objects]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_joined_objs_universe_objects
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_universe_objects]
as
begin
	
	declare @uniTree int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connTree int = dbo.fn_get_bo_actual_connection_tree_id();
	declare @boServer int = dbo.fn_get_bo_actual_server_id();
    declare @uniKind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcKind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');
    
    -- dodanie polaczen universe <-> connection
    exec sp_prepare_bik_joined_objs_tmp
	
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id, bnu.id, 1 from aaa_universe uni
	inner join aaa_universe_connection con on uni.universe_connetion_id = con.id
	inner join bik_node bn on bn.tree_id = @connTree and bn.name = con.connetion_name 
	inner join bik_node bnu on bnu.tree_id = @uniTree and bnu.obj_id = convert(varchar(30), uni.global_props_si_id)
	where bn.node_kind_id in (@dcKind, @dcNewKind)
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bnu.node_kind_id = @uniKind
	and bnu.is_deleted = 0
	and bnu.linked_node_id is null
	
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bnu.id, bn.id, 1 from aaa_universe uni
	inner join aaa_universe_connection con on uni.universe_connetion_id = con.id
	inner join bik_node bn on bn.tree_id = @connTree and bn.name = con.connetion_name 
	inner join bik_node bnu on bnu.tree_id = @uniTree and bnu.obj_id = convert(varchar(30), uni.global_props_si_id)
	where bn.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bnu.node_kind_id = @uniKind
	and bnu.is_deleted = 0
	and bnu.linked_node_id is null
	
	-- polaczenia OLAP
	if exists(select 1 from bik_sapbo_server ser 
		inner join bik_data_source_def def on ser.source_id = def.id
		where ser.id = @boServer and def.description = 'Business Objects 4.x') and exists(select 1 from sys.objects where type = 'U' and name = 'APP_DSL__METADATAFILE')
	begin
		declare @connColCode varchar(255) = 'SI_SL_UNIVERSE_TO_CONNECTIONS__'
		declare @num int = 1
		declare @connTableCodeBase varchar(255) = @connColCode
		set @connColCode = @connColCode + cast(@num as varchar(20))
		
		declare @table table (
			col_name varchar(max) not null
		);
		
		while exists(select 1 from sys.columns where name = @connColCode and Object_ID = Object_ID('APP_DSL__METADATAFILE'))
		begin
			insert into @table(col_name)
			values (@connColCode)
			
			set @num = @num + 1  
			set @connColCode = @connTableCodeBase + cast(@num as varchar(20))
		end
		
		declare curs cursor for 
		select col_name from @table order by col_name 

		open curs;
		declare @column varchar(50);

		fetch next from curs into @column;
		while @@fetch_status = 0
		begin
			declare @sql1 varchar(max);
			declare @sql2 varchar(max);
			set @sql1 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnu.id, bnc.id, 1 from APP_DSL__METADATAFILE uni
			inner join bik_node bnu on bnu.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),uni.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),uni.' + @column + ')
			where bnu.is_deleted = 0
			and bnc.is_deleted = 0'
			
			set @sql2 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnc.id, bnu.id, 1 from APP_DSL__METADATAFILE uni
			inner join bik_node bnu on bnu.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),uni.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),uni.' + @column + ')
			where bnu.is_deleted = 0
			and bnc.is_deleted = 0'

			exec(@sql1);
			exec(@sql2);
			fetch next from curs into @column;
		end
		close curs;
		deallocate curs;
	end
	
    exec sp_move_bik_joined_objs_tmp 'Universe,DSL.MetaDataFile', 'DataConnection,CCIS.DataConnection,CommonConnection'
	
    create table #tmpBranch (branch_id varchar(max) collate DATABASE_DEFAULT, node_id int);

    insert into #tmpBranch(branch_id,node_id)
    select bn.branch_ids, bn2.id from bik_node bn
    inner join bik_joined_objs jo on jo.src_node_id = bn.id
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    where bn.tree_id = @uniTree
    and bn.node_kind_id = @uniKind
    and bn.linked_node_id is null
    and bn.is_deleted = 0
    and bn2.tree_id = @connTree
    and bn2.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
    and bn2.linked_node_id is null
    and bn2.is_deleted = 0

    exec sp_prepare_bik_joined_objs_tmp

	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
	select bn.id, a.node_id, 1 from #tmpBranch a 
	inner join bik_node bn on bn.branch_ids like a.branch_id + '%'
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	where node_kind_id <> @uniKind
		
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
	select bn2.id, bn.id, 1 from bik_node bn 
	inner join bik_node bn2 on bn2.branch_ids like bn.branch_ids + '%'
	and bn2.is_deleted = 0
	and bn2.linked_node_id is null
	and bn2.node_kind_id <> @uniKind
	and bn.tree_id = @uniTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bn.node_kind_id = @uniKind

    exec sp_move_bik_joined_objs_tmp 'Universe,DSL.MetaDataFile,DataConnection,CCIS.DataConnection,CommonConnection', 'UniverseClass,Measure,Dimension,Detail,Filter,UniverseAliasTable,UniverseDerivedTable,UniverseTable,UniverseTablesFolder'
	
	drop table #tmpBranch
end;
go


-- poprzednia wersja w pliku alter db for v1.2.4 tf.sql 
-- dodanie polaczen OLAP i fix pod BO4
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_metadata]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_metadata
go

create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
	-- SAP BO
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_extradata', 'node_id', 'author, owner, cuid, guid, ruid', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_engine, database_source, connetion_networklayer_name, user_name, server', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_schedule', 'node_id', 'owner, creation_time, nextruntime, expire, format, parameters', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_olap_connection', 'node_id', 'type, provider_caption, cube_caption, cuid, created, owner, modified', null, @optNodeFilter
	
	declare @obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as si_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''DataConnection'', ''Webi'', ''Flash'', ''CrystalReport'', ''Universe'', ''Excel'', ''FullClient'', ''Pdf'', ''Hyperlink'', ''Powerpoint'',
		''ReportFolder'', ''UniversesFolder'', ''ConnectionFolder'', ''ReportSchedule'', ''DSL.MetaDataFile'', ''CommonConnection'', ''CCIS.DataConnection''))'
	exec sp_verticalize_node_attrs_one_metadata_table @obj_id_sql, 'id', 'si_id', null, @optNodeFilter
	
	-- Oracle
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_oracle_extradata', 'node_id', 'definition_sql', null, @optNodeFilter
	
	-- MS SQL
	declare @mssql_tree_id int = (select id from bik_tree where code = 'MSSQL')
	declare @mssql_sql varchar(max) = '(select bn.id, ms.definition_text from bik_mssql ms 
		inner join bik_node bn on bn.obj_id = ms.branch_names
		where bn.tree_id = ' +  cast(@mssql_tree_id as varchar(20)) + '
		and bn.is_deleted = 0
		and linked_node_id is null
		and ms.definition_text is not null)'
	exec sp_verticalize_node_attrs_one_metadata_table @mssql_sql, 'id', 'definition_text', null, @optNodeFilter
	  
	-- DQC
	declare @tree_id int = (select id from bik_tree where code = 'DQC')
	declare @inact_nk_id int = (select id from bik_node_kind where code = 'DQCTestInactive'),
    @succ_nk_id int = (select id from bik_node_kind where code = 'DQCTestSuccess'),
    @fail_nk_id int = (select id from bik_node_kind where code = 'DQCTestFailed')

	declare @dqc_src_sql varchar(max) = '(select cast(dt.long_name as varchar(max)) as long_name, cast(dt.sampling_method as varchar(max)) as sampling_method, cast(dt.verified_attributes as varchar(max)) as verified_attributes,cast(dt.expected_result as varchar(max)) as expected_result,cast(dt.logging_details as varchar(max)) as logging_details, cast(dt.benchmark_definition as varchar(max)) as benchmark_definition, cast(dt.results_object as varchar(max)) as results_object, cast(dt.additional_information as varchar(max)) as additional_information, n.id as node_id
		from bik_dqc_test dt inner join bik_node n on dt.__obj_id = n.obj_id and n.node_kind_id in (' +
		cast(@inact_nk_id as varchar(20)) + ',' + cast(@succ_nk_id as varchar(20)) + ',' + cast(@fail_nk_id as varchar(20)) + ') and tree_id = ' + cast(@tree_id as varchar(20)) + '
		where n.linked_node_id is null and n.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @dqc_src_sql, 'node_id', 'long_name, sampling_method, verified_attributes,expected_result,logging_details, benchmark_definition, results_object, additional_information', null, @optNodeFilter
	
	-- AD
	declare @ad_src_sql varchar(max) = '(select bu.email, coalesce(bu.phone_num, ad.telephone_number) as phone_num,
		ad.physical_delivery_office_name,ad.postal_code, ad.manager, 
		ad.description, ad.department, ad.title, ad.mobile, 
		ad.display_name, bu.node_id
		from bik_user bu
		join bik_node bn on bn.id = bu.node_id and bn.is_deleted = 0
		left join bik_system_user bsu on bsu.user_id = bu.id 
		left join bik_active_directory ad 
		on (bu.login_name_for_ad = ad.s_a_m_account_name or bsu.login_name = ad.s_a_m_account_name))'
	exec sp_verticalize_node_attrs_one_metadata_table @ad_src_sql, 'node_id', 'email, phone_num, physical_delivery_office_name,postal_code,manager, description, department, title, mobile, display_name', null, @optNodeFilter
	
	-- SAP BW query extradata
	declare @bwreports_id int = (select id from bik_tree where code = 'BWReports')
	declare @query_nk_id int = (select id from bik_node_kind where code = 'BWBEx')
	declare @sapbw_query_src_sql varchar(max) = '(select q.update_time, q.owner, q.last_edit, bn.id as node_id
		from bik_sapbw_query q inner join bik_node bn on q.obj_name = bn.obj_id and bn.node_kind_id = ' +
		cast(@query_nk_id as varchar(20)) + ' and bn.tree_id = ' + cast(@bwreports_id as varchar(20)) + '
		where bn.linked_node_id is null and bn.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @sapbw_query_src_sql, 'node_id', 'update_time, owner, last_edit', null, @optNodeFilter
	
	-- SAP BW providers, query and areas - technical id
	declare @bw_obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id=bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWBEx'', ''BWArea'', ''BWMPRO'', ''BWCUBE'', ''BWISET'', ''BWODSO''))'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_obj_id_sql, 'id', 'technical_id', null, @optNodeFilter
	
	-- SAP BW objects - technical id
	declare @bw_objs_id_sql varchar(max) = '(select bn.id, obj.obj_name as obj_technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id=bnk.id
		inner join bik_sapbw_object obj on obj.provider + ''|'' + obj.obj_name=bn.obj_id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWUni'',''BWKyf'', ''BWTim'', ''BWDpa'', ''BWCha'')
		and obj.provider_parent is null)'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_objs_id_sql, 'id', 'obj_technical_id', null, @optNodeFilter
	
end;
go

-- poprzednia wersja w pliku alter db for v1.3.0.6 tf.sql
-- fix z convertem intów na varchar
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_create_bo_actual_instance_function]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_create_bo_actual_instance_function]
go

create procedure [dbo].[sp_create_bo_actual_instance_function](@serverId int)
as
begin

	declare @reportTreeId int, @universeTreeId int, @connectionTreeId int;
	select @reportTreeId = report_tree_id, @universeTreeId = universe_tree_id, @connectionTreeId = connection_tree_id from bik_sapbo_server where id = @serverId
    
    -- drop if exists
    exec('if exists (select * from sys.objects where object_id = OBJECT_ID(N''[dbo].[fn_get_bo_actual_report_tree_id]'') and type in (N''FN''))
			drop function [dbo].[fn_get_bo_actual_report_tree_id]');
	exec('if exists (select * from sys.objects where object_id = OBJECT_ID(N''[dbo].[fn_get_bo_actual_universe_tree_id]'') and type in (N''FN''))
			drop function [dbo].[fn_get_bo_actual_universe_tree_id]');
    exec('if exists (select * from sys.objects where object_id = OBJECT_ID(N''[dbo].[fn_get_bo_actual_connection_tree_id]'') and type in (N''FN''))
			drop function [dbo].[fn_get_bo_actual_connection_tree_id]');
    exec('if exists (select * from sys.objects where object_id = OBJECT_ID(N''[dbo].[fn_get_bo_actual_server_id]'') and type in (N''FN''))
			drop function [dbo].[fn_get_bo_actual_server_id]');
	-- create
	declare @sql varchar(max);
	
	set @sql = 'create function fn_get_bo_actual_report_tree_id()
		returns int
		as
		begin
			return ' + convert(varchar(10),@reportTreeId) + ';
		end';
		
	exec(@sql)
	
	set @sql = 'create function fn_get_bo_actual_universe_tree_id()
		returns int
		as
		begin
			return ' + convert(varchar(10),@universeTreeId) + ';
		end'
	exec(@sql)
	
	set @sql = 'create function fn_get_bo_actual_connection_tree_id()
		returns int
		as
		begin
			return ' + convert(varchar(10),@connectionTreeId) + ';
		end';
		
	exec(@sql)
	
	set @sql = 'create function fn_get_bo_actual_server_id()
		returns int
		as
		begin
			return ' + convert(varchar(10),@serverId) + ';
		end'
	exec(@sql)
end;
go

-- poprzednia wersja w: alter db for v1.3.0.6 tf.sql 
-- fix dla BO4
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_delete_bik_joined_objs_by_kinds_fast]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_delete_bik_joined_objs_by_kinds_fast
go

create procedure sp_delete_bik_joined_objs_by_kinds_fast @kinds_one_str varchar(max), @kinds_two_str varchar(max) as
begin
  set nocount on

  declare @timestamp_start_total datetime = current_timestamp
  declare @diags_level int = 0 -- wartość 0 oznacza brak logowania, 1 i więcej - jest logowanie
  declare @rc int
  declare @timestamp_start datetime 
  declare @timestamp_end datetime
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: start' +
      ', @kinds_one_str = ' + @kinds_one_str + 
      ', @kinds_two_str = ' + @kinds_two_str
  end

  -- declare @kinds_one varchar(max) = 'Universe', @kinds_two varchar(max) = 'TeradataSchema'
  declare @codes_one table (code varchar(255) not null primary key)
  declare @codes_two table (code varchar(255) not null primary key)

  insert into @codes_one (code)
  select distinct str
  from dbo.fn_split_by_sep(@kinds_one_str, ',', 7)

  insert into @codes_two (code)
  select distinct str
  from dbo.fn_split_by_sep(@kinds_two_str, ',', 7)

  declare @kind_one_id table (id int not null primary key)
  declare @kind_two_id table (id int not null primary key)

  insert into @kind_one_id (id)
  select distinct k.id
  from @codes_one x inner join bik_node_kind k on x.code = k.code
  where substring(x.code, 1, 1) <> '@'
  
  insert into @kind_two_id (id)
  select distinct k.id
  from @codes_two x inner join bik_node_kind k on x.code = k.code
  where substring(x.code, 1, 1) <> '@'

  declare @tree_one_id table (id int not null primary key)
  declare @tree_two_id table (id int not null primary key)

  delete from @codes_one where substring(code, 1, 1) <> '@'
  delete from @codes_two where substring(code, 1, 1) <> '@'

  update @codes_one set code = substring(code, 2, len(code))
  update @codes_two set code = substring(code, 2, len(code))

  -- fix tree codes
  declare @reportTreeCode varchar(255), @universeTreeCode varchar(255), @connectionTreeCode varchar(255);
  select @reportTreeCode = code from bik_tree where id = dbo.fn_get_bo_actual_report_tree_id()
  select @universeTreeCode = code from bik_tree where id = dbo.fn_get_bo_actual_universe_tree_id()  
  select @connectionTreeCode = code from bik_tree where id = dbo.fn_get_bo_actual_connection_tree_id()
  
  update @codes_one
  set code = @reportTreeCode
  where code = 'Reports'
  
  update @codes_one
  set code = @universeTreeCode
  where code = 'ObjectUniverses'
  
  update @codes_one
  set code = @connectionTreeCode
  where code = 'Connections'
  
  update @codes_two
  set code = @reportTreeCode
  where code = 'Reports'
  
  update @codes_two
  set code = @universeTreeCode
  where code = 'ObjectUniverses'
  
  update @codes_two
  set code = @connectionTreeCode
  where code = 'Connections'  
  -- end fix


  insert into @tree_one_id (id)
  select distinct t.id
  from @codes_one x inner join bik_tree t on x.code = t.code

  insert into @tree_two_id (id)
  select distinct t.id
  from @codes_two x inner join bik_tree t on x.code = t.code

  declare @tree_id_to_kind_id table (tree_id int not null, node_kind_id int not null)
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before insert into @tree_id_to_kind_id'
  end

  set @timestamp_start = current_timestamp
  
  insert into @tree_id_to_kind_id (tree_id, node_kind_id)
  select distinct tree_id, node_kind_id
  from bik_node where is_deleted = 0 and linked_node_id is null

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after insert into @tree_id_to_kind_id, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end

  insert into @kind_one_id (id)
  select t2k.node_kind_id
  from @tree_one_id t inner join @tree_id_to_kind_id t2k on t.id = t2k.tree_id
  left join @kind_one_id k on k.id = t2k.node_kind_id
  where k.id is null

  insert into @kind_two_id (id)
  select t2k.node_kind_id
  from @tree_two_id t inner join @tree_id_to_kind_id t2k on t.id = t2k.tree_id
  left join @kind_two_id k on k.id = t2k.node_kind_id
  where k.id is null

  declare @joined_ids_to_del table (id int not null primary key)

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before insert into @joined_ids_to_del'
  end

  set @timestamp_start = current_timestamp
  
  declare @actualServerId int = dbo.fn_get_bo_actual_server_id();
  declare @actualSourceType int;
  select @actualSourceType = source_id from bik_sapbo_server where id = @actualServerId
  declare @otherInstanceBOTree table (
	 tree_id int not null
  );
  
  insert into @otherInstanceBOTree(tree_id)
  select report_tree_id from bik_sapbo_server
  where id <> @actualServerId
  --and source_id = @actualSourceType
  union all
  select universe_tree_id from bik_sapbo_server
  where id <> @actualServerId
  --and source_id = @actualSourceType
  union all
  select connection_tree_id from bik_sapbo_server
  where id <> @actualServerId
  --and source_id = @actualSourceType  

  insert into @joined_ids_to_del (id)
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.src_node_id = n_one.id
  inner join bik_node n_two on jo.dst_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1
  and n_one.tree_id not in (select tree_id from @otherInstanceBOTree)
  and n_two.tree_id not in (select tree_id from @otherInstanceBOTree)
  union -- bez all, bo mogą być duplikaty, a chcemy je właśnie wyeliminować
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.dst_node_id = n_one.id
  inner join bik_node n_two on jo.src_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1
  and n_one.tree_id not in (select tree_id from @otherInstanceBOTree)
  and n_two.tree_id not in (select tree_id from @otherInstanceBOTree)

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after insert into @joined_ids_to_del, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end
  
  --select count(*) from @joined_ids_to_del

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before final delete'
  end

  set @timestamp_start = current_timestamp
  
  delete from bik_joined_objs where id in (select id from @joined_ids_to_del)
  
  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  declare @timestamp_end_total datetime = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after final delete, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
      + ', TOTAL TIME TAKEN: ' + cast(datediff(ms, @timestamp_start_total, @timestamp_end_total) / 1000.0 as varchar(20)) + ' s'
  end
end
go


-- poprzednia wersja w pliku: alter db for v1.3.0.6 tf.sql
-- fix dla BO4 (nowe node kindy)
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_metadata_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
	
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();

    declare @teradataKind int = dbo.fn_tree_id_by_code('Teradata');
    declare @schemaKind int = dbo.fn_node_kind_id_by_code('TeradataSchema');
    declare @tabKind int = dbo.fn_node_kind_id_by_code('TeradataTable');
    declare @viewKind int = dbo.fn_node_kind_id_by_code('TeradataView');
    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @universeNodeKind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');

    ----------   zapytanie  -->  universe    ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
    select bn.id as src_node_id, bnu.id as dst_node_id, 1 as type, 1 as inherit_to_descendants
    from bik_sapbo_query sbq
    inner join bik_node bn on sbq.node_id = bn.id
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind,@uniNewKind)
	where bn.tree_id = @reportTreeId
	and bn.is_deleted = 0

    ----------   zapytanie  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id as src_node_id, bnc.id as dst_node_id, 1 as type
    from bik_sapbo_query sbq
    inner join bik_node bn on sbq.node_id = bn.id
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
    and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
    and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	where bn.tree_id = @reportTreeId
	and bn.is_deleted = 0 

    ----------   universe  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnu.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
    and bnu.obj_id = convert(varchar(30),uni.si_id) 
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)

    ----------   raport  -->  universe   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnu.id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)

    ----------   raport  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnc.id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
    and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
    and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)

	----------   connection  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnc.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
    and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
    and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)

    ----------   obiekt  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_node obj on a.object_node_id = obj.id
    and obj.tree_id = @universeTreeId and obj.is_deleted = 0
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)

    ----------   obiekt  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_node obj on a.object_node_id = obj.id
    and obj.tree_id = @universeTreeId and obj.is_deleted = 0
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind

    ----------   universum  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type = 'Teradata' and b.is_derived = 0 
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Table/View  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type = 'Teradata' and b.is_derived = 0 
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   universum  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Schema  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   connection  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
    where b.type = 'Teradata' and b.is_derived = 0 and bn2.id is not null
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Schema  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
    where b.type = 'Teradata' and b.is_derived = 0 and bn2.id is not null
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Table/View  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   connection  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Table/View  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   raport  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   teradata Schema  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   raport  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   zapytanie  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   zapytanie  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

	----------   obiekt  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct ids.object_node_id, bn.parent_node_id, 1 
    from aaa_ids_for_report ids 
    inner join bik_node bn on ids.query_node_id = bn.id
    and bn.tree_id = @reportTreeId
    and bn.is_deleted = 0
    inner join bik_node bno on bno.id = ids.object_node_id
    and bno.tree_id = @universeTreeId
    and bno.is_deleted = 0
	
	----------   UniverseTable  -->  teradata Table/View   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.name_for_teradata = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'
	inner join bik_node tab on tab.id = bsut.node_id
	and tab.tree_id = @universeTreeId
	and tab.is_deleted = 0

	----------   teradata Table/View  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bsut.node_id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.name_for_teradata = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'
	inner join bik_node tab on tab.id = bsut.node_id
	and tab.tree_id = @universeTreeId
	and tab.is_deleted = 0

	----------   UniverseTable  -->  teradata Schema   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.schema_name = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'
	inner join bik_node tab on tab.id = bsut.node_id
	and tab.tree_id = @universeTreeId
	and tab.is_deleted = 0
	
	----------   UniverseAliasTable  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bsut1.node_id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_sapbo_universe_table bsut1 on bsut.original_table = bsut1.their_id
	inner join bik_node bn on bn.id = bsut.node_id
	inner join bik_node bn2 on bn2.id = bsut1.node_id
	where bsut.is_alias = 1 and bn2.parent_node_id = bn.parent_node_id
	and bn.tree_id = @universeTreeId
	and bn2.tree_id = @universeTreeId

	-- usuniecie alias table - univ table
	exec sp_delete_bik_joined_objs_by_kinds_fast 'UniverseTable', 'UniverseAliasTable'
	
    exec sp_move_bik_joined_objs_tmp 'TeradataView,TeradataTable,TeradataSchema,Webi,ReportQuery', 'Universe,DSL.MetaDataFile,DataConnection,CommonConnection,CCIS.DataConnection,Measure,Dimension,Detail,Filter,Webi,ReportQuery,UniverseAliasTable,UniverseDerivedTable,UniverseTable'
	
end;
go

-- poprzednia wersja w pliku: alter db for v1.4.y1.2 tf.sql
-- fix dla BO4 (nowe node kindy)
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_mssql_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
    
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	
    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');    
    declare @universe_kind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @mssqlTree int = dbo.fn_tree_id_by_code('MSSQL');

	---------- universe table --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ut.node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- universe table --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ut.node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- object --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- object --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- query --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- query --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- report --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- report --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId

	---------- universe --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- universe --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null


	
	---------- mssql table --> universe table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, ut.node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted=0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- mssql table --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bno on bno.id = ot.object_node_id
	and bno.tree_id = @universeTreeId
	and bno.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- mssql table --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql table --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null

	---------- mssql DB --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bno on bno.id = ot.object_node_id
	and bno.tree_id = @universeTreeId
	and bno.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null		
	
    exec sp_move_bik_joined_objs_tmp '@MSSQL', '@Reports, @ObjectUniverses, @Connections'
    
end;
go

-- poprzednia wersja w pliku: alter db for v1.3.0.6 tf.sql
-- fix dla BO4 (nowe node kindy)
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_oracle_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_oracle_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_oracle_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
    
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	
    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @universe_kind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @oracleTree int = dbo.fn_tree_id_by_code('Oracle');

	---------- universe table --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- universe table --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- object --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- object --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- query --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- query --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null	

	---------- universe --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- universe --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null


	
	---------- oracle table --> universe table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, ut.node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- oracle table --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- oracle table --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- oracle table --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- oracle DB --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null

	---------- oracle DB --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- oracle DB --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null		
	
    exec sp_move_bik_joined_objs_tmp '@Oracle', '@Reports, @ObjectUniverses, @Connections'
    
end;
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

exec sp_update_version '1.4.y1.17', '1.4.y1.18';
go
