/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.mltx;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.*;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.mltx.history.HistoryAction;
import pl.fovis.client.mltx.history.MultiXHistoryToken;
import pl.fovis.client.mltx.history.MultiXUrlCreator;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.mltx.MultiRegistrationBean;
import pl.fovis.common.mltx.MultiXDatabaseTemplateBean;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author lbiegniewski
 */
public class MultiRegistrationPage extends SimplePanel {

    private TextBox emailTb;
    private PushButton registerBtn;
    private Label emailLbl;
    private Label infoEmailSend;
    private Label infoFreeAcount;

    public static HistoryAction getHistoryAction() {
        return new HistoryAction() {
            public Widget performAction(String id) {
                return new MultiRegistrationPage();
            }
        };
    }

    public MultiRegistrationPage() {
        buildWidgets();
    }

    private void buildWidgets() {

        final SimplePanel main = this;
        BIKClientSingletons.getMultiXService().getDatabaseTemplates(new StandardAsyncCallback<List<MultiXDatabaseTemplateBean>>() {
            public void onSuccess(List<MultiXDatabaseTemplateBean> result) {


                registerBtn = new PushButton(I18n.rejestrujDarmoweKontoBiks.get());
                NewLookUtils.makeCustomPushButton(registerBtn);
                registerBtn.addClickHandler(prepareRegistrationClickHandler());
                registerBtn.setStyleName("gwt-PushButton");

                emailTb = new TextBox();

                emailTb.setStyleName("userDialogInput");
                emailTb.setWidth("258px");

                emailLbl = new Label(I18n.proszePodacAdresEmailUzytkownika.get() + ":");

                emailLbl.setStyleName("userDialogLbl");

                infoEmailSend = new Label(I18n.naPodanyPrzezCiebieAdres.get());
                infoEmailSend.setStyleName("miniInfoText");
                infoFreeAcount = new Label(I18n.zalozDarmoweKonto.get());
                infoFreeAcount.setStyleName("NewLoginLbl");

                int row = 0;
                FlexTable ft = new FlexTable();
                ft.setWidth("100%");
//                ft.getFlexCellFormatter().setColSpan(row, 0, 2);
//                ft.setWidget(row++, 0, infoFreeAcount);
//                ft.getFlexCellFormatter().setColSpan(row, 0, 2);
//                ft.setWidget(row++, 0, fpBiksVersions);
                ft.setWidget(row, 0, emailLbl);
                ft.setWidget(row++, 1, emailTb);

//                ft.getFlexCellFormatter().setColSpan(row, 0, 2);
//                ft.setWidget(row++, 0, registerBtn);

//                ft.getFlexCellFormatter().setColSpan(row, 0, 2);
//                ft.setWidget(row++, 0, infoEmailSend);

//                FlexTable mmrp = new FlexTable();
//                mmrp.setStyleName("bfpl");
//                mmrp.getFlexCellFormatter().setHorizontalAlignment(0, 0, HasHorizontalAlignment.ALIGN_CENTER);



//                mft.setStyleName("multixRegistrationPanel");
                FlexTable mft = new FlexTable();

                mft.setWidth("520px");
                mft.setWidget(0, 0, infoFreeAcount);
                mft.setWidget(1, 0, ft);

                HorizontalPanel fpBtns = new HorizontalPanel();
                fpBtns.setWidth("100%");
                fpBtns.add(registerBtn);
                AnchorAsHyperlink loginHL = new AnchorAsHyperlink(I18n.logowanie.get(), MultiXHistoryToken.LOGIN.hname);
                fpBtns.add(loginHL);
                fpBtns.setCellHorizontalAlignment(loginHL, HasHorizontalAlignment.ALIGN_RIGHT);
                mft.setWidget(2, 0, fpBtns);
                mft.setWidget(3, 0, infoEmailSend);

                KeyPressHandler kph = new KeyPressHandler() {
                    public void onKeyPress(KeyPressEvent event) {
//                        Window.alert("aqq!");
                        boolean enterPressed = KeyCodes.KEY_ENTER == event.getNativeEvent().getKeyCode();
                        if (enterPressed) {
                            performRegistration();
                        }
                    }
                };


                emailTb.addKeyPressHandler(kph);

//                mmrp.setWidget(0, 0, mft);
//
//                main.add(mmrp);
                FlexTable MltxBackground = NewLookUtils.mltxBackground(mft);
                main.add(MltxBackground);
            }
        });
    }

    private ClickHandler prepareRegistrationClickHandler() {
        return new ClickHandler() {
            public void onClick(ClickEvent event) {
                prepareRegistrationCont().doIt();
            }
        };
    }

    protected void performRegistration() {
        if (!BaseUtils.isEmailValid(emailTb.getText())) {
            BIKClientSingletons.showError(I18n.niepoprawnyAdresEmail.get());
            return;
        }
        MultiRegistrationBean mrb = new MultiRegistrationBean();

        mrb.email = emailTb.getText();

        if (isEmailProvided(mrb.email)) {
            registerBtn.setEnabled(false);
            registerMultiUser(mrb);
        } else {
            BIKClientSingletons.showError(I18n.proszePodacAdresEmail.get());
            registerBtn.setEnabled(true);
        }
    }

    private IContinuation prepareRegistrationCont() {
        return new IContinuation() {
            public void doIt() {
                performRegistration();
            }
        };
    }

    private void registerMultiUser(MultiRegistrationBean mrb) {
        BIKClientSingletons.showInfo(I18n.sendingRegistrationEmail.get());

        final SimplePanel sp = this;

        String propUrl = MultiXUrlCreator.createUrl();

//        Window.alert("Window.Location.getHref()=" + Window.Location.getHref()
//                + "\nWindow.Location.getQueryString()=" + Window.Location.getQueryString()
//                + "\npropUrl=" + propUrl);
//
//        if (true) {
//            return;
//        }

        BIKClientSingletons.getMultiXService().sendConfirmationEmail(mrb, propUrl, new StandardAsyncCallback<String>() {
            public void onSuccess(String result) {
                if (result == null) {
                    sp.clear();
                    Label InfoLabel = new Label(I18n.naPodanyAdresWyslanoWiadomosc.get());
                    InfoLabel.setStyleName("NewLoginLbl");
                    FlexTable MltxBackground = NewLookUtils.mltxBackground(InfoLabel);
                    sp.add(MltxBackground);
                } else {
                    FoxyClientSingletons.showError(result);
                    registerBtn.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Throwable caught) {
                FoxyClientSingletons.showError(I18n.wystapilBladWCzasieRejestracji.get());
                registerBtn.setEnabled(true);
            }
        });
    }

    private boolean isEmailProvided(String email) {
        return email.length() != 0;
    }
}
