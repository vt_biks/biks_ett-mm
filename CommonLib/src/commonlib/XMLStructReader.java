/*
 * XMLStructReader.java
 *
 * Created on 10 lipiec 2007, 16:51
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib;

import simplelib.LameRuntimeException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.kxml2.io.KXmlParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author msmoktunowicz
 */
public class XMLStructReader {
    static ILameLogger logger = LameLoggerFactory.getLogger(XMLStructReader.class);
            //LameLogger.getClassLogger();
    private int maxLines;
    private int linesCnt;
    private Map<String, Map> tree;
    
    /** Creates a new instance of XMLStructReader */
    public XMLStructReader(int maxLines) {
        this.maxLines = maxLines;
        linesCnt = 0;
        tree = new HashMap<String, Map>();
    }
    
    public void readStruct(InputStream is, String location) {
        try {
            KXmlParser parser = new KXmlParser();
            parser.setInput(is, null);
            if (logger.isInfoEnabled())
                logger.info("parser encoding 1: " + parser.getInputEncoding());
            
            parser.require(XmlPullParser.START_DOCUMENT, null, null);
            if (logger.isInfoEnabled())
                logger.info("parser encoding 1.1: " + parser.getInputEncoding());
            parser.nextTag();
            if (logger.isInfoEnabled())
                logger.info("parser encoding 2: " + parser.getInputEncoding());
            
            internalReadStruct(parser, 0, tree);
            
            System.out.println(tree);
            if (logger.isInfoEnabled())
                logger.info("parser encoding 3: " + parser.getInputEncoding());
            parser.require(XmlPullParser.END_DOCUMENT, null, null);
            
        } catch (Exception ex) {
            throw new LameRuntimeException("error converting content (location: " + location + ") to map: " + ex.getMessage(), ex);
        }
    }
    
    @SuppressWarnings("unchecked")
    private void internalReadStruct(final KXmlParser parser, int level, Map<String, Map> tree) throws XmlPullParserException, IOException {
        if (linesCnt % 100000 == 0)
            System.out.println("Done: " + linesCnt);
        
//        if (linesCnt++ > maxLines)
//            return;
        linesCnt++;
        String space = LameUtils.replicateStr(" ", level);
        
        String tagName = parser.getName();
        if (tree.containsKey(tagName)) {
            tree = tree.get(tagName);
        } else {
            Map<String, Map> newMap = new HashMap<String, Map>();
            tree.put(tagName, newMap);
            tree = newMap;
            System.out.println(space + "new tagName: " + tagName);
        }
                
        int attrCount = parser.getAttributeCount();
        //for (int i = 0; i < attrCount; i++)
        //    System.out.println(space + parser.getAttributeName(i)  + " = "  + parser.getAttributeValue(i));
        
        parser.next();
        //List subNodes = null;
        
        while (parser.getEventType() != XmlPullParser.END_TAG) {
            
            //Object childNode;
            
            if (parser.getEventType() == XmlPullParser.TEXT) {
                //childNode = parser.getText();
                parser.next();
            } else if (parser.getEventType() == XmlPullParser.START_TAG) {
                internalReadStruct(parser, level + 1, tree);
            } else throw new LameRuntimeException("unexpected event type: " + parser.getEventType());
/*            if (subNodes == null) {
                subNodes = new ArrayList();
                System.out.println("#subNodes " + subNodes);
            }
            subNodes.add(childNode);
 */        }
//        if (linesCnt > maxLines)
//            return;
        parser.next();
    }
}
