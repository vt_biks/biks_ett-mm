/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.util.Collections;
import java.util.Set;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
//i18n-default: no
public class BIKRightRoles {

    public static enum RightRole {
        //ww: role muszą być ułożone w hierarchi - od najsłabszych uprawnień

        RegularUser,
        Author(null),
        Editor(null),
        Expert,
        AppAdmin,
        Administrator;
        //ww: kod jest opcjonalny
        protected String code;

        RightRole() {
            this.code = name();
        }

        RightRole(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }
    //ww: te stałe mają być tu, bo z pozostałych miejsc nie wolno sprawdzać
    // po kodach ról, tylko ta klasa to może robić!
    // a jeżeli brakuje jakiejś metody sprawdzającej role uprawnień,
    // należy tutaj ją dodać
    private static final String RIGHT_ROLE_CODE_EXPERT = "Expert";
    private static final String RIGHT_ROLE_CODE_APPADMIN = "AppAdmin";
    private static final String RIGHT_ROLE_CODE_SYSADMIN = "Administrator";
    private static final String[] RIGHT_ROLE_CODES_EFFECTIVE_EXPERT = {RIGHT_ROLE_CODE_EXPERT, RIGHT_ROLE_CODE_APPADMIN, RIGHT_ROLE_CODE_SYSADMIN};
    private static final String[] RIGHT_ROLE_CODES_EFFECTIVE_APPADMIN = {RIGHT_ROLE_CODE_APPADMIN, RIGHT_ROLE_CODE_SYSADMIN};

    //---
    //ww: w poniższych metodach liczy się tylko pole
    // SystemUserBean.userRightRoleCodes
    // co pozwala robić fejkowe obiekty SystemUserBean z wypełnionym tylko tym
    // polem i sprawdzać uprawnienia w ten sposób
    public static boolean isSysUserEffectiveExpert(SystemUserBean optSysUserBean) {
        return userHasRightRoleWithOneOfCodes(optSysUserBean, RIGHT_ROLE_CODES_EFFECTIVE_EXPERT);
    }

    public static boolean isSysUserEffectiveAppAdmin(SystemUserBean optSysUserBean) {
        return userHasRightRoleWithOneOfCodes(optSysUserBean, RIGHT_ROLE_CODES_EFFECTIVE_APPADMIN);
    }

    public static boolean isSysUserSysAdmin(SystemUserBean optSysUserBean) {
        return userHasRightRoleWithOneOfCodes(optSysUserBean, RIGHT_ROLE_CODE_SYSADMIN);
    }

    public static boolean userHasRightRoleWithCode(SystemUserBean optSysUserBean, String code) {
        return userHasRightRoleWithOneOfCodes(optSysUserBean, code);
    }

    public static boolean userHasRightRoleWithOneOfCodes(SystemUserBean optSysUserBean, String... codes) {
        if (optSysUserBean == null) {
            return false;
        }

        Set<String> userRightRoleCodes = optSysUserBean.userRightRoleCodes;

        if (userRightRoleCodes == null) {
            return false;
        }

        for (String code : codes) {
            if (userRightRoleCodes.contains(code)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSysAdminRightRoleCode(String rightRoleCode) {
        return BaseUtils.safeEquals(rightRoleCode, RIGHT_ROLE_CODE_SYSADMIN);
    }

    public static boolean canSysUserAddOrEditSysUsers(SystemUserBean optLoggedUser,
            SystemUserBean optSysUserToEdit) {

        if (!isSysUserEffectiveAppAdmin(optLoggedUser)) {
            return false;
        }

        return !isSysUserSysAdmin(optSysUserToEdit)
                || isSysUserSysAdmin(optLoggedUser);
    }

    public static void clearSysAdminRightRole(Set<String> rightRoles) {
        if (rightRoles != null) {
            rightRoles.remove(RIGHT_ROLE_CODE_SYSADMIN);
        }
    }

    public static Set<String> getAppAdminRightRoles() {
        return Collections.singleton(RIGHT_ROLE_CODE_APPADMIN);
    }

    public static Set<String> getSysAdminRightRoles() {
        return Collections.singleton(RIGHT_ROLE_CODE_SYSADMIN);
    }

    public static String getRightRoleCaptionByCode(String code) {
        RightRole rr = RightRole.valueOf(code);
        switch (rr) {
            case RegularUser:
                return I18n.zwyklyUzytkownik.get();
            case Author:
                return I18n.rightRoleAuthor.get();
            case Editor:
                return I18n.rightRoleEditor.get();
            case Expert:
                return I18n.rightRoleExpert.get();
            case AppAdmin:
                return I18n.rightRoleAppAdmin.get();
            case Administrator:
                return I18n.rightRoleAdministrator.get();
        }
        throw new UnsupportedOperationException("unsupported code=\"" + code + "\"");
    }
}
