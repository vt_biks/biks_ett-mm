/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.server.searchservice.BiksSearchConstant;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class SearchAttributesNormalSearchModeFilter extends SearchFilterBase {

    protected Map<String, CheckBox> allAttributesCheckBoxes = new HashMap<String, CheckBox>();
    protected CheckBox allAttributesCb;
    protected VerticalPanel attributeFilterVp = new VerticalPanel();
    public Set<String> attrNames;
    protected IContinuation ic;

    protected Pair< Set<String>, Boolean> attrNamesAndisAnythingNodeKindSelected;

    public Map<String, String> allTextFieldsName = new HashMap<String, String>() {
        {
            put(BiksSearchConstant.BIK_NODE_NAME, I18n.nazwaObiektuSearch.get());
            put(BiksSearchConstant.BIK_NODE_DESC, I18n.opisObiektuSearch.get());
            put(BiksSearchConstant.BIK_JOINED_OBJ_ATTRIBUTE_LINKED, I18n.atrybutyPowiazan.get());
            put(BiksSearchConstant.BIK_FILE_CONTENT, I18n.zalaczniki.get());
        }
    };

    public SearchAttributesNormalSearchModeFilter(IContinuation ic, Set<String> attrNames) {
        this.ic = ic;
        this.attrNames = attrNames;
    }

    protected VerticalPanel buildWidget() {

        attributeFilterVp.add(new Label(I18n.atrybuty.get()));
        attributeFilterVp.addStyleName("lblPadding Search");

        VerticalPanel attributeVp = new VerticalPanel();
        ScrollPanel attrSP = new ScrollPanel();
        attrSP.setHeight("170px");
        attrSP.setWidth("300px");
        attrSP.add(attributeVp);
//        attrSP.addStyleName("border");
        allAttributesCb = new CheckBox(I18n.wszystkieAtrybuty.get());
        allAttributesCb.setValue(true);
        allAttributesCb.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                for (CheckBox cb : allAttributesCheckBoxes.values()) {
                    if (cb.isVisible()) {
                        cb.setValue(allAttributesCb.getValue());
                    }
                }
                ic.doIt();
            }
        });

        for (Map.Entry<String, String> s : allTextFieldsName.entrySet()) {
            attributeVp.add(createAttributeCb(s.getValue(), s.getKey()));
        }
        for (String name : attrNames) {
            String trimName = name.indexOf('.') < 0 ? name : name.split("\\.")[1];
            attributeVp.add(createAttributeCb(trimName, name));
        }

        attributeFilterVp.add(filterPanel(filterTb = new TextBox()));
        attributeFilterVp.add(getAllCheckboxFpWithStyle(allAttributesCb));
        attributeFilterVp.add(attrSP);
        return attributeFilterVp;
    }

    protected CheckBox createAttributeCb(String name, String formValue) {
        CheckBox cb = new CheckBox(name);
        cb.setTitle(formValue);
        cb.setValue(true);
        cb.setFormValue(formValue);
        allAttributesCheckBoxes.put(formValue, cb);//id
        cb.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                boolean all = true;
                for (CheckBox x : allAttributesCheckBoxes.values()) {
                    if (!x.getValue() && x.isVisible()) {
                        all = false;
                        break;
                    }
                }
                allAttributesCb.setValue(all);
//                searchId = null;
//                doSearch();
                ic.doIt();
            }
        });
        cb.addStyleName("checkboxAttr");
        return cb;
    }

    protected Pair<Set<String>, List<String>> getSelectedAttr(boolean isNormalSearchModeBtnSelected) {
        Set<String> selectedAttributeName = new HashSet<String>();
        List<String> selectedOtherFilter = new ArrayList<String>();//name,descr,attachment,joinattr
//        Set<Integer> selectedNodeKindIds = new HashSet<Integer>();

        if (isNormalSearchModeBtnSelected) {
            for (CheckBox cb : allAttributesCheckBoxes.values()) {
                if (cb.getValue()) {
                    String formValue = cb.getFormValue();
                    if (allTextFieldsName.containsKey(formValue)) {
                        selectedOtherFilter.add(formValue);
                    } else {
                        selectedAttributeName.add(formValue);
                    }
                }
            }
        }
        return new Pair<Set<String>, List<String>>(selectedAttributeName, selectedOtherFilter);
    }

    protected void filterAction() {
        if (attrNamesAndisAnythingNodeKindSelected == null) {
            reselectedAttributeCbxs(new Pair< Set<String>, Boolean>(attrNames, true));
        } else {
            reselectedAttributeCbxs(attrNamesAndisAnythingNodeKindSelected);
        }

        ic.doIt();
    }

    protected void reselectedAttributeCbxs(Pair< Set<String>, Boolean> attrNamesAndisAnythingNodeKindSelected) {
        this.attrNamesAndisAnythingNodeKindSelected = attrNamesAndisAnythingNodeKindSelected;
        String filtr = filterTb.getText();
        boolean isAnythingNodeKindSelected = attrNamesAndisAnythingNodeKindSelected.v2;

        boolean isAllAttributeSelected = true;
        for (CheckBox cb : allAttributesCheckBoxes.values()) {
            String formValue = cb.getFormValue();
            if (allTextFieldsName.containsKey(formValue)) {
                if (!isAnythingNodeKindSelected) {//sprawdzić
                    cb.setVisible(false);
                    cb.setValue(false);
                } else {
                    if (!cb.isVisible()) {
                        cb.setValue(true);
                        cb.setVisible(true);
                    }
                    cb.setVisible(true);
//                    cb.setValue(true);//zaznaczenie
                }
            } else if (attrNamesAndisAnythingNodeKindSelected.v1.contains(formValue)) {
                if (!cb.isVisible()) {
                    cb.setValue(true);
                    cb.setVisible(true);
                }
                cb.setVisible(true);
//                cb.setValue(true);//zaznaczenie
            } else {
                cb.setVisible(false);
                cb.setValue(false);
            }
            if (cb.isVisible()) {
                if (BaseUtils.isStrEmptyOrWhiteSpace(filtr) || cb.getHTML().toLowerCase().contains(filtr.toLowerCase())) {
                    if (!cb.isVisible()) {
                        cb.setValue(true);
                        cb.setVisible(true);
                    }
                    cb.setVisible(true);
//                    cb.setValue(true);//zaznaczenie
                    if (!cb.getValue()) {
                        isAllAttributeSelected = false;
                    }
                } else {
                    cb.setVisible(false);
                    cb.setValue(false);
                }
            }
        }

        allAttributesCb.setVisible(isAnythingNodeKindSelected);
        allAttributesCb.setValue(isAnythingNodeKindSelected && isAllAttributeSelected);

    }
       public void selectOrUnselectAllFilter(boolean check) {
        allAttributesCb.setValue(check);
        for (CheckBox cb : allAttributesCheckBoxes.values()) {
            cb.setValue(check);
            cb.setVisible(check);
        }
        filterTb.setText("");
    }

}
