/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.Timer;
import pl.fovis.client.mltx.history.MultiXHistoryManager;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.DoNothingAsyngCallback;

/**
 *
 * @author lbiegniewski
 */
public class bikMltxEntryPoint {

    public void init() {

        Timer keepSessionAliveTimer = new Timer() {
            @Override
            public void run() {
                BIKClientSingletons.setBrowserTitle();
                BIKClientSingletons.getService().keepSessionAlive(BIKClientSingletons.getLoggedUserLoginName(),
                        new DoNothingAsyngCallback<String>(I18n.bladPolaczenPrawdopoTrwaInstalac.get() /* I18N:  */));
            }
        };
        keepSessionAliveTimer.scheduleRepeating(10000);

        MultiXHistoryManager.init();
        // TF: ukrycie ładowania systemu
        bikcenterEntryPoint.hideLoadingView();
    }
}
