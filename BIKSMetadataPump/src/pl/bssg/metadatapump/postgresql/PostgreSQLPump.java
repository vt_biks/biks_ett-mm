/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.postgresql;

import commonlib.LameUtils;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.AdhocPartialBeanCollector;
import pl.bssg.metadatapump.GenericNodeBean;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.common.ServerConfigBean;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class PostgreSQLPump extends MetadataPumpBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    public final static String TYPE_SERVER = "PostgresServer";
    public final static String TYPE_DATABASE = "PostgresDatabase";
    public final static String TYPE_SCHEMA = "PostgresSchema";
    public final static String TYPE_TABLE = "PostgresTable";
    public final static String TYPE_VIEW = "PostgresView";
    public final static String TYPE_FUNCTION = "PostgresFunction";
    public final static String TYPE_COLUMN = "PostgresColumn";
    public final static String TYPE_FOLDER = "PostgresFolder";
    public final static String SCHEMA_QUERY = "select nspname as schema_name from pg_namespace where nspname not in ('information_schema', 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'pg_catalog')";
    public final static String TABLE_QUERY = "select cl.relname as table_name, nm.nspname as table_schema, des.description\n"
            + "from pg_class as cl \n"
            + "inner join pg_namespace as nm on nm.oid = cl.relnamespace \n"
            + "left join pg_description as des on des.objoid = cl.oid and des.objsubid = 0\n"
            + "where cl.relkind = 'r' and nm.nspname not in ('pg_catalog', 'information_schema', 'pg_toast')";
    public final static String VIEW_QUERY = "select cl.relname as view_name, nm.nspname as view_schema, pg_get_viewdef(cl.oid) as definition, des.description\n"
            + "from pg_class as cl \n"
            + "inner join pg_namespace as nm on nm.oid = cl.relnamespace \n"
            + "left join pg_description as des on des.objoid = cl.oid and des.objsubid = 0\n"
            + "where cl.relkind = 'v' and nm.nspname not in ('pg_catalog', 'information_schema')";
    public final static String FUNCTION_QUERY = "select proname || '_' || f.oid as specific_name, proname as routine_name, n.nspname as routine_schema, pg_get_functiondef(f.oid) as definition, des.description\n"
            + "from pg_proc f\n"
            + "inner join pg_namespace n on f.pronamespace = n.oid\n"
            + "left join pg_description as des on des.objoid = f.oid and des.objsubid = 0\n"
            + "where n.nspname not in ('pg_catalog', 'information_schema')";
//    public final static String COLUMN_QUERY = "select table_schema, table_name, column_name, ordinal_position, column_default, is_nullable, case when data_type in ('numeric') and numeric_precision is not null and numeric_scale is not null then data_type || '(' || numeric_precision || ',' || numeric_scale || ')' when data_type in ('character varying', 'varchar', 'character', 'char') and character_maximum_length is not null then data_type || '(' || character_maximum_length || ')' else data_type end as column_type from information_schema.columns where table_schema not in ('pg_catalog', 'information_schema') order by table_schema, table_name, ordinal_position";
    public final static String COLUMN_QUERY = "select table_schema, table_name, column_name, ordinal_position, column_default, is_nullable, case when data_type in ('numeric') and numeric_precision is not null and numeric_scale is not null then data_type || '(' || numeric_precision || ',' || numeric_scale || ')' when data_type in ('character varying', 'varchar', 'character', 'char') and character_maximum_length is not null then data_type || '(' || character_maximum_length || ')' else data_type end as column_type\n"
            + "from (\n"
            + "	select nc.nspname as table_schema,\n"
            + "	    c.relname as table_name,\n"
            + "	    a.attname as column_name,\n"
            + "	    a.attnum as ordinal_position,\n"
            + "	    pg_get_expr(ad.adbin, ad.adrelid) as column_default,\n"
            + "	    case when (a.attnotnull or ((t.typtype = 'd') and t.typnotnull)) then 'NO'\n"
            + "	    else 'YES' end as is_nullable,\n"
            + "	    case when (t.typtype = 'd') then\n"
            + "		    case when ((bt.typelem <> (0)) and (bt.typlen = (-1))) then 'ARRAY'\n"
            + "			 when (nbt.nspname = 'pg_catalog') then format_type(t.typbasetype, NULL)\n"
            + "			 else 'USER-DEFINED'\n"
            + "		    end else \n"
            + "		    case when ((t.typelem <> (0)) and (t.typlen = (-1))) then 'ARRAY'\n"
            + "			when (nt.nspname = 'pg_catalog') then format_type(a.atttypid, NULL)\n"
            + "			else 'USER-DEFINED'\n"
            + "		    end end as data_type,\n"
            + "	    information_schema._pg_char_max_length(information_schema._pg_truetypid(a.*, t.*), information_schema._pg_truetypmod(a.*, t.*)) as character_maximum_length,\n"
            + "	    information_schema._pg_numeric_precision(information_schema._pg_truetypid(a.*, t.*), information_schema._pg_truetypmod(a.*, t.*)) as numeric_precision,\n"
            + "	    information_schema._pg_numeric_scale(information_schema._pg_truetypid(a.*, t.*), information_schema._pg_truetypmod(a.*, t.*)) as numeric_scale\n"
            + "	from (((((pg_attribute a\n"
            + "	     left join pg_attrdef ad on a.attrelid = ad.adrelid and a.attnum = ad.adnum)\n"
            + "	     inner join (pg_class c\n"
            + "	     inner join pg_namespace nc on c.relnamespace = nc.oid) on a.attrelid = c.oid)\n"
            + "	     inner join (pg_type t\n"
            + "	     inner join pg_namespace nt on t.typnamespace = nt.oid) on a.atttypid = t.oid)\n"
            + "	     left join (pg_type bt\n"
            + "	     inner join pg_namespace nbt on bt.typnamespace = nbt.oid) on t.typtype = 'd' and t.typbasetype = bt.oid)\n"
            + "	     left join (pg_collation co\n"
            + "	     inner join pg_namespace nco on co.collnamespace = nco.oid) on a.attcollation = co.oid and (nco.nspname <> 'pg_catalog' or co.collname <> 'default'))\n"
            + "	where nc.nspname not in ('pg_catalog', 'information_schema') and\n"
            + "	  ((((not pg_is_other_temp_schema(nc.oid) \n"
            + "	  and a.attnum > 0)\n"
            + "	  and (NOT a.attisdropped)) \n"
            + "	  and (c.relkind = ANY (ARRAY['r', 'v', 'f']))))) x";
    public final static String INDEX_QUERY = "select\n"
            + "i.relname as indname,\n"
            + "ns.nspname as schema,\n"
            + "c.relname as tabname,\n"
            + "a.attname as column, \n"
            + "a.attnum as position,\n"
            + "idx.indisunique as is_unique,\n"
            + "idx.indisprimary as is_primary_key,\n"
            + "idx.indisclustered as is_clustered\n"
            + "from pg_index as idx\n"
            + "inner join pg_class as i on i.oid = idx.indexrelid\n"
            + "inner join pg_class as c on c.oid = idx.indrelid\n"
            + "inner join pg_am as am on i.relam = am.oid\n"
            + "inner join pg_namespace as ns on ns.oid = i.relnamespace\n"
            + "inner join pg_attribute as a on a.attrelid = i.oid\n"
            + "where ns.nspname not in ('pg_toast', 'pg_catalog') and a.attnum > 0";
    public final static String FOREIGN_KEY_QUERY = "select con.conname as name,\n"
            + "    ns2.nspname as from_schema,\n"
            + "    cl2.relname as from_table,\n"
            + "    att2.attname as from_column,\n"
            + "    ns.nspname as to_schema,\n"
            + "    cl.relname as to_table,\n"
            + "    att.attname as to_column\n"
            + "from\n"
            + "   (select unnest(con1.conkey) as \"parent\",\n"
            + "	   unnest(con1.confkey) as \"child\", \n"
            + "           con1.confrelid, \n"
            + "           con1.conrelid,\n"
            + "           con1.conname\n"
            + "    from \n"
            + "        pg_class cl\n"
            + "        join pg_namespace ns on cl.relnamespace = ns.oid\n"
            + "        join pg_constraint con1 on con1.conrelid = cl.oid\n"
            + "	where con1.contype = 'f' and ns.nspname not in ('pg_toast', 'pg_catalog')\n"
            + "   ) con\n"
            + "   inner join pg_attribute att on att.attrelid = con.confrelid and att.attnum = con.child\n"
            + "   inner join pg_class cl on cl.oid = con.confrelid\n"
            + "   inner join pg_namespace ns on ns.oid = cl.relnamespace and ns.nspname not in ('pg_toast', 'pg_catalog', 'information_schema')\n"
            + "   inner join pg_attribute att2 on att2.attrelid = con.conrelid and att2.attnum = con.parent\n"
            + "   inner join pg_class cl2 on cl2.oid = con.conrelid\n"
            + "   inner join pg_namespace ns2 on ns2.oid = cl2.relnamespace and ns2.nspname not in ('pg_toast', 'pg_catalog', 'information_schema')";
//    public final static String DEPENDENCY_QUERY = "select view_schema, view_name, table_schema, table_name from information_schema.view_table_usage where view_schema not in ('pg_toast', 'pg_catalog', 'information_schema')";
    public final static String DEPENDENCY_QUERY = "select distinct \n"
            + "    nv.nspname as view_schema,\n"
            + "    v.relname as view_name,\n"
            + "    nt.nspname as table_schema,\n"
            + "    t.relname as table_name\n"
            + "from pg_namespace nv,\n"
            + "    pg_class v,\n"
            + "    pg_depend dv,\n"
            + "    pg_depend dt,\n"
            + "    pg_class t,\n"
            + "    pg_namespace nt\n"
            + "where (nv.oid = v.relnamespace) \n"
            + "  and (v.relkind = 'v') \n"
            + "  and (v.oid = dv.refobjid)\n"
            + "  and (dv.deptype = 'i')\n"
            + "  and (dv.objid = dt.objid)\n"
            + "  and (dv.refobjid <> dt.refobjid) \n"
            + "  and (dt.refobjid = t.oid)\n"
            + "  and (t.relnamespace = nt.oid)\n"
            + "  and (t.relkind = ANY (ARRAY['r', 'v', 'f']))\n"
            + "  and (nv.nspname not in ('pg_toast', 'pg_catalog', 'information_schema'))";
    public final static String DRIVER_CLASS = "org.postgresql.Driver";
    public final static int PACKAGE_SIZE = 100;
    public final static String POSTGRES_DEFAULT_DATABASE_NAME = "template1";
    public final static Map<String, String> typeFolderNameMap = new HashMap<String, String>();
    public final static Map<String, Integer> typeFolderVisualOrderMap = new HashMap<String, Integer>();

    static {
        typeFolderNameMap.put(TYPE_TABLE, "Tabele");
        typeFolderNameMap.put(TYPE_VIEW, "Widoki");
        typeFolderNameMap.put(TYPE_FUNCTION, "Funkcje");
        typeFolderVisualOrderMap.put(TYPE_TABLE, 1);
        typeFolderVisualOrderMap.put(TYPE_VIEW, 10);
        typeFolderVisualOrderMap.put(TYPE_FUNCTION, 20);
    }
    protected AdhocPartialBeanCollector<GenericNodeBean> beanCollector;
    protected AdhocPartialBeanCollector<PostgresDefinitionBean> beanDefinitionCollector;
    protected AdhocPartialBeanCollector<PostgresIndexBean> beanIndexCollector;
    protected AdhocPartialBeanCollector<PostgresForeignKeyBean> beanForeignKeyCollector;
    protected AdhocPartialBeanCollector<PostgresColumnBean> beanColumnsCollector;
    protected AdhocPartialBeanCollector<PostgresDependencyBean> beanDependencyCollector;
    protected Set<String> typeSet = new HashSet<String>();
    protected Set<String> tableSet = new HashSet<String>();
    protected Map<String, Set<String>> serverDatabases = new HashMap<String, Set<String>>();
//    protected Connection conn = null;

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_POSTGRESQL;
    }

    @Override
    protected Pair<Integer, String> startPump() {
        createCollectors();
        getMetadataFromServers();

        if (!beanCollector.wasSomethingAdded()) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "No connecting with PostgreSQL server or lack of databases!");
        }

        execNamedCommandWithCommit("updatePostgresKeys"); // update PK, FK
        execNamedCommandWithCommit("initBranchInAmgNode"); // init branch names

        if (logger.isInfoEnabled()) {
            logger.info("Update BIKS structure");
        }
        pumpLogger.pumpLog("Update BIKS structure", 80);
        execNamedCommandWithCommit("insertPostgresIntoBikNode", serverDatabases);
        pumpLogger.pumpLog("Insert PostgreSQL extradata", 90);
        execNamedCommandWithCommit("movePostgresMetadataExtradata", serverDatabases);
        if (logger.isInfoEnabled()) {
            logger.info("All right. Success!");
        }
        return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
    }

//    @Override
//    public String getPumpGroup() {
//        return PumpConstants.PUMP_GROUP_POSTGRESQL;
//    }
    protected void createCollectors() {
        beanCollector = new AdhocPartialBeanCollector<GenericNodeBean>(PACKAGE_SIZE, adhocDao, "insertIntoGenericTable");
        beanDefinitionCollector = new AdhocPartialBeanCollector<PostgresDefinitionBean>(PACKAGE_SIZE, adhocDao, "insertPostgresDefinitionMetadata");
        beanColumnsCollector = new AdhocPartialBeanCollector<PostgresColumnBean>(PACKAGE_SIZE, adhocDao, "insertPostgresColumnMetadata");
        beanIndexCollector = new AdhocPartialBeanCollector<PostgresIndexBean>(PACKAGE_SIZE, adhocDao, "insertPostgresIndexMetadata");
        beanForeignKeyCollector = new AdhocPartialBeanCollector<PostgresForeignKeyBean>(PACKAGE_SIZE, adhocDao, "insertPostgresForeignKeyMetadata");
        beanDependencyCollector = new AdhocPartialBeanCollector<PostgresDependencyBean>(PACKAGE_SIZE, adhocDao, "insertPostgresDependencyMetadata");
    }

    protected void flushCollectors() {
        beanCollector.flush();
        beanDefinitionCollector.flush();
        beanColumnsCollector.flush();
        beanIndexCollector.flush();
        beanForeignKeyCollector.flush();
        beanDependencyCollector.flush();
    }

    protected void optAddTypeFolderIfNeeded(String servName, String databaseName, String schema, String objectType, String folderKey) {
        if (!typeSet.contains(folderKey)) {
            beanCollector.add(new GenericNodeBean(folderKey, servName + "|" + databaseName + "|" + schema + "|", TYPE_FOLDER, typeFolderNameMap.get(objectType), null, typeFolderVisualOrderMap.get(objectType)));
            typeSet.add(folderKey);
        }
    }

    protected void getMetadataFromServers() {
        // get connection properties
        typeSet.clear();
        serverDatabases.clear();
        List<ServerConfigBean> servers = adhocDao.createBeansFromNamedQry("getDBConfigForDB", ServerConfigBean.class, PumpConstants.SOURCE_NAME_POSTGRESQL, instanceName);
        Boolean useBasicModeInDBConnectors = BaseUtils.tryParseBoolean((String) execNamedQuerySingleVal("getAppPropValue", true, "val", "useBasicModeInDBConnectors"), false);
        // clear old values
        execNamedCommandWithCommit("deletePostgresTempTables");
        int serversStepCount = 80 / servers.size();
        int miniStep = serversStepCount / 2;
        int actualServerNumber = 0;
        int actualStep = 0;
        for (ServerConfigBean serv : servers) {
            actualStep = 0;
            String serverNameWithSuffix = serv.name;
            String serverName = PumpUtils.getServerNameWithoutSuffix(serverNameWithSuffix);
            String serverBranchName = serverName + "|";
            Connection masterConn = null;
            Set<String> databases = new HashSet<String>();
            try {
                String serverUrl = "jdbc:postgresql://" + serv.server + (serv.port != null ? ":" + serv.port : "");
                List<String> databasesToFilter = BaseUtils.splitBySep(serv.databaseFilter, ",", true);
                if (!databasesToFilter.isEmpty()) {
                    for (String db : databasesToFilter) {
                        databases.add(db);
                    }
                } else { // connect to system database (template1) and get databases list
//                StringBuilder url = new StringBuilder();
//                url.append("jdbc:postgresql://").append(serv.server);//.append("/").append(POSTGRES_DATABASE_NAME);
                    masterConn = getConnection(DRIVER_CLASS, serverUrl + "/" + POSTGRES_DEFAULT_DATABASE_NAME, serv.user, serv.password);
                    if (masterConn == null) {
                        pumpLogger.pumpLog("No connecting with server: " + serv.server, null);
                        continue;
                    }
                    Statement masterStatment = masterConn.createStatement();

                    // get databases
                    pumpLogger.pumpLog(serverNameWithSuffix + ": loading: databases", actualServerNumber * serversStepCount + miniStep * (actualStep++));
                    ResultSet retDbs = masterStatment.executeQuery("select datname from pg_database where datistemplate = false");
                    while (retDbs.next()) {
                        String db = retDbs.getString("datname");
                        if (databasesToFilter.isEmpty()) { // nie ma filtru na bazy danych
                            databases.add(db);
                        } else {
                            for (String dbF : databasesToFilter) {
                                if (dbF.contains(db)) {
                                    databases.add(db);
                                    break;
                                }
                            }
                        }
//                    System.out.println("Database = " + db);
                    }
                    closeConnection(masterConn);
                    masterConn = null;
                }
                if (!databases.isEmpty()) {
                    execNamedCommandWithCommit("addServerIfNotExists", serverName, TYPE_SERVER);
                }
                // set databases for server
                execNamedCommandWithCommit("setDatabasesForServer", serv.id, BaseUtils.mergeWithSepEx(databases, ","));

                //loading objects from database
                pumpLogger.pumpLog(serverNameWithSuffix + ": loading databases obejcts", actualServerNumber * serversStepCount + miniStep * (actualStep++));
                for (String database : databases) {
                    String databaseBranchName = serverBranchName + database + "|";
                    Connection dbConn = null;
                    tableSet.clear();
                    try {
                        dbConn = getConnection(DRIVER_CLASS, serverUrl + "/" + database, serv.user, serv.password);
                        Statement dbStatment = dbConn.createStatement();
                        beanCollector.add(new GenericNodeBean(databaseBranchName, serverBranchName, TYPE_DATABASE, database, null));
                        // get schemas
                        ResultSet schemasResults = dbStatment.executeQuery(SCHEMA_QUERY);
                        while (schemasResults.next()) {
                            String schema = schemasResults.getString("schema_name");
                            beanCollector.add(new GenericNodeBean(databaseBranchName + schema + "|", databaseBranchName, TYPE_SCHEMA, schema, null));
                        }
                        // get tables
                        ResultSet tablesResults = dbStatment.executeQuery(TABLE_QUERY);
                        while (tablesResults.next()) {
                            String schema = tablesResults.getString("table_schema");
                            String table = tablesResults.getString("table_name");
                            String description = tablesResults.getString("description");
                            String parentKey = databaseBranchName + schema + "|$" + TYPE_TABLE + "|";
                            String objectBranch = databaseBranchName + schema + "|" + table + "|";
                            tableSet.add(objectBranch);
                            optAddTypeFolderIfNeeded(serverName, database, schema, TYPE_TABLE, parentKey);
                            beanCollector.add(new GenericNodeBean(objectBranch, parentKey, TYPE_TABLE, table, description));
                        }
                        // get views
                        ResultSet views = dbStatment.executeQuery(VIEW_QUERY);
                        while (views.next()) {
                            String schema = views.getString("view_schema");
                            String view = views.getString("view_name");
                            String description = views.getString("description");
                            String definition = views.getString("definition");
                            String parentKey = databaseBranchName + schema + "|$" + TYPE_VIEW + "|";
                            String objectBranch = databaseBranchName + schema + "|" + view + "|";
                            tableSet.add(objectBranch);
                            optAddTypeFolderIfNeeded(serverName, database, schema, TYPE_VIEW, parentKey);
                            beanCollector.add(new GenericNodeBean(objectBranch, parentKey, TYPE_VIEW, view, description));
                            if (!BaseUtils.isStrEmptyOrWhiteSpace(definition)) {
                                beanDefinitionCollector.add(new PostgresDefinitionBean(serverName, database, schema, view, definition));
                            }
                        }
                        // get functions
                        ResultSet functions = dbStatment.executeQuery(FUNCTION_QUERY);
                        while (functions.next()) {
                            String schema = functions.getString("routine_schema");
                            String function = functions.getString("routine_name");
                            String uniqueFunctionName = functions.getString("specific_name");
                            String definition = functions.getString("definition");
                            String description = functions.getString("description");
                            String parentKey = databaseBranchName + schema + "|$" + TYPE_FUNCTION + "|";
                            optAddTypeFolderIfNeeded(serverName, database, schema, TYPE_FUNCTION, parentKey);
                            beanCollector.add(new GenericNodeBean(databaseBranchName + schema + "|" + uniqueFunctionName + "|", parentKey, TYPE_FUNCTION, function, description));
                            if (!BaseUtils.isStrEmptyOrWhiteSpace(definition)) {
                                beanDefinitionCollector.add(new PostgresDefinitionBean(serverName, database, schema, uniqueFunctionName, definition));
                            }
                        }
                        // basic mode - kończymy pobieranie
                        if (useBasicModeInDBConnectors) {
                            System.out.println("Basic mode POSTGRESQL! Instance: " + instanceName);
                            addToSuccessDBServer(serverName, databases, actualServerNumber);
                            continue;
                        }
                        // get columns
                        ResultSet columns = dbStatment.executeQuery(COLUMN_QUERY);
                        while (columns.next()) {
                            //table_schema, table_name, column_name, ordinal_position, column_default, is_nullable, column_type
                            String schema = columns.getString("table_schema");
                            String table = columns.getString("table_name");
                            String column = columns.getString("column_name");
                            int ordinalPosition = columns.getInt("ordinal_position");
                            String defaultValue = columns.getString("column_default");
                            String isNullFromString = columns.getString("is_nullable");
                            boolean isNullable = isNullFromString != null && isNullFromString.toLowerCase().equals("yes");
                            String type = columns.getString("column_type");
                            String objectParentBranch = databaseBranchName + schema + "|" + table + "|";
                            if (tableSet.contains(objectParentBranch)) {
                                beanColumnsCollector.add(new PostgresColumnBean(column, schema, table, database, serverName, ordinalPosition, null, isNullable, type, defaultValue));
                                beanCollector.add(new GenericNodeBean(objectParentBranch + column + "|", objectParentBranch, TYPE_COLUMN, column, null, ordinalPosition));
                            }
                        }
                        // get indices
                        ResultSet indices = dbStatment.executeQuery(INDEX_QUERY);
                        while (indices.next()) {
                            // indname, schema, tabname, column, position, is_unique, is_primary_key, is_clustered
                            String indname = indices.getString("indname");
                            String schema = indices.getString("schema");
                            String tabname = indices.getString("tabname");
                            String column = indices.getString("column");
                            int ordinalPosition = indices.getInt("position");
                            boolean isUnique = indices.getBoolean("is_unique");
                            boolean isPrimary = indices.getBoolean("is_primary_key");
                            boolean isClustered = indices.getBoolean("is_clustered");
                            beanIndexCollector.add(new PostgresIndexBean(indname, serverName, schema, database, tabname, column, ordinalPosition, isUnique, isClustered ? "CLUSTERED" : "NONCLUSTERED", isPrimary));
                        }
                        // get foreign keys
                        ResultSet foreignKeys = dbStatment.executeQuery(FOREIGN_KEY_QUERY);
                        while (foreignKeys.next()) {
                            // name, from_schema, from_table, from_column, to_schema, to_table, to_column
                            String name = foreignKeys.getString("name");
                            String fromSchema = foreignKeys.getString("from_schema");
                            String fromTable = foreignKeys.getString("from_table");
                            String fromColumn = foreignKeys.getString("from_column");
                            String toSchema = foreignKeys.getString("to_schema");
                            String toTable = foreignKeys.getString("to_table");
                            String toColumn = foreignKeys.getString("to_column");
                            beanForeignKeyCollector.add(new PostgresForeignKeyBean(name, null, serverName, database, fromSchema, fromTable, fromColumn, toSchema, toTable, toColumn));
                        }
                        // get dependencies
                        ResultSet dependencies = dbStatment.executeQuery(DEPENDENCY_QUERY);
                        while (dependencies.next()) {
                            // view_schema, view_name, table_schema, table_name
                            String viewSchema = dependencies.getString("view_schema");
                            String viewName = dependencies.getString("view_name");
                            String tableSchema = dependencies.getString("table_schema");
                            String tableName = dependencies.getString("table_name");
                            beanDependencyCollector.add(new PostgresDependencyBean(serverName, database, viewSchema, viewName, tableSchema, tableName));
                        }
                    } catch (Exception e) {
                        System.out.println("Error: " + e.getMessage());
                        pumpLogger.pumpLog("Error with connect to database: " + database + " for server: " + serverNameWithSuffix, null);
                    } finally {
                        if (dbConn != null) {
                            closeConnection(dbConn);
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
                pumpLogger.pumpLog(serverNameWithSuffix + " - error: " + e.getMessage(), null);
            } finally {
                if (masterConn != null) {
                    closeConnection(masterConn);
                }
            }
            addToSuccessDBServer(serverName, databases, actualServerNumber);
        }
    }

    protected void addToSuccessDBServer(String serverName, Set<String> databases, int actualServerNumber) {
        Set<String> setOnMap = serverDatabases.get(serverName);
        if (setOnMap == null) {
            setOnMap = new HashSet<String>();
        }
        setOnMap.addAll(databases);
        serverDatabases.put(serverName, setOnMap);
        actualServerNumber++;
        flushCollectors();
    }
}
