exec sp_check_version '1.0.68';
go

delete from aaa_universe_column;
delete from aaa_universe_obj_tables; 
delete from aaa_universe_table; 
delete from aaa_universe_obj; 
delete from aaa_universe_class; 
delete from aaa_universe; 
delete from aaa_universe_connetion; 
delete from aaa_universe_connetion_networklayer; 

drop table aaa_universe_column;
drop table aaa_universe_obj_tables; 
drop table aaa_universe_table; 
drop table aaa_universe_obj; 
drop table aaa_universe_class; 
drop table aaa_universe; 
drop table aaa_universe_connetion; 
drop table aaa_universe_connetion_networklayer; 

CREATE TABLE aaa_universe_connetion_networklayer(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
CREATE TABLE aaa_universe_connetion(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[connetion_name] [varchar](1000) NOT NULL,
	[server] [varchar](255) NULL,
	[user_name] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[database_source] [varchar](500) NULL,
	[connetion_networklayer_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[connetion_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[aaa_universe_connetion]  WITH CHECK ADD FOREIGN KEY([connetion_networklayer_id])
REFERENCES [dbo].[aaa_universe_connetion_networklayer] ([id])
GO

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

CREATE TABLE aaa_universe(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[unique_id] [varchar](300) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[description] [varchar](max) NULL,
	[file_name] [varchar](255) NOT NULL,
	[author] [varchar](120) NULL,
	[universe_connetion_id] [int] NOT NULL,
	[path] [varchar](500) NOT NULL,
	[global_props_si_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UNIQ_unique_id_path_aaa_universe] UNIQUE NONCLUSTERED 
(
	[unique_id] ASC,
	[path] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[aaa_universe]  WITH CHECK ADD FOREIGN KEY([universe_connetion_id])
REFERENCES [dbo].[aaa_universe_connetion] ([id])
GO

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

CREATE TABLE aaa_universe_class(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[si_id] [int] NOT NULL,
	[parent_id] [int] NULL,
	[name] [varchar](255) NOT NULL,
	[description] [varchar](max) NULL,
	[universe_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UNIQ_unique_si_id_aaa_universe_class] UNIQUE NONCLUSTERED 
(
	[si_id] ASC,
	[universe_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[aaa_universe_class]  WITH CHECK ADD FOREIGN KEY([universe_id])
REFERENCES [dbo].[aaa_universe] ([id])
GO

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

CREATE TABLE aaa_universe_obj(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[description] [varchar](max) NULL,
	[text_of_select] [varchar](max) NULL,
	[type] [varchar](255) NOT NULL,
	[universe_class_id] [int] NULL,
	[si_id] [int] NOT NULL,
	[parent_id] [int] NULL,
	[qualification] [varchar](50) NULL,
	[aggregate_function] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UNIQ_unique_si_id_aaa_universe_obj] UNIQUE NONCLUSTERED 
(
	[si_id] ASC,
	[universe_class_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[aaa_universe_obj]  WITH CHECK ADD FOREIGN KEY([universe_class_id])
REFERENCES [dbo].[aaa_universe_class] ([id])
GO

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

CREATE TABLE aaa_universe_table(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[their_id] [int] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[is_alias] [int] NOT NULL,
	[is_derived] [int] NOT NULL,
	[original_table] [int] NULL,
	[sql_of_derived_table] [varchar](max) NULL,
	[sql_of_derived_table_with_alias] [varchar](max) NULL,
	[universe_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UNIQ_unique_their_id_aaa_universe_table] UNIQUE NONCLUSTERED 
(
	[their_id] ASC,
	[universe_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[aaa_universe_table]  WITH CHECK ADD FOREIGN KEY([universe_id])
REFERENCES [dbo].[aaa_universe] ([id])
GO

ALTER TABLE [dbo].[aaa_universe_table]  WITH CHECK ADD CHECK  (([is_alias]=(1) OR [is_alias]=(0)))
GO

ALTER TABLE [dbo].[aaa_universe_table]  WITH CHECK ADD CHECK  (([is_derived]=(1) OR [is_derived]=(0)))
GO

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

CREATE TABLE aaa_universe_column(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[universe_table_id] [int] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[type] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UNIQ_unique_name_id_aaa_universe_column] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[universe_table_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[aaa_universe_column]  WITH CHECK ADD FOREIGN KEY([universe_table_id])
REFERENCES [dbo].[aaa_universe_table] ([id])
GO

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

CREATE TABLE aaa_universe_obj_tables(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[universe_table_id] [int] NOT NULL,
	[universe_obj_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[universe_table_id] ASC,
	[universe_obj_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[aaa_universe_obj_tables]  WITH CHECK ADD FOREIGN KEY([universe_table_id])
REFERENCES [dbo].[aaa_universe_table] ([id])
GO

ALTER TABLE [dbo].[aaa_universe_obj_tables]  WITH CHECK ADD FOREIGN KEY([universe_obj_id])
REFERENCES [dbo].[aaa_universe_obj] ([id])
GO

exec sp_update_version '1.0.68', '1.0.69';
go