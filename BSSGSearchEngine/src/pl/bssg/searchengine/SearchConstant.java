/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.searchengine;

import org.apache.lucene.analysis.Analyzer;
//import org.apache.lucene.analysis.morfologik.MorfologikAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

/**
 *
 * @author ctran
 */
public class SearchConstant {

    public static final int UNLIMITED_RESULT_NUM = 999999;

    public static final String IS_DELETED = "@is_deleted";
    public static final String DOC_ID = "@doc_id";
//    private static Analyzer analyzer = new MorfologikAnalyzer();
    private static final Analyzer analyzer = new StandardAnalyzer();

    public static Analyzer getAnalyzer() {
        return analyzer;
    }
}
