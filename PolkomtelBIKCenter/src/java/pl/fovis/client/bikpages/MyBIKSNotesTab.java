/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.IGridNodeAwareBeanPropsExtractor;
import pl.fovis.common.NoteBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author beata
 */
public class MyBIKSNotesTab extends MyBiksTabWithExtractor<NoteBean> /*implements IMyBIKSTab*/ {

    @Override
    public String getCaption() {
        return I18n.mojeKomentarze.get() /* I18N:  */;
    }

    @Override
    public String getButtonStyle() {
        return "labelLink-note";
    }

    @Override
    protected IGridNodeAwareBeanPropsExtractor<NoteBean> getExtractor() {
        return new NoteBeanExtractor();
    }

    @Override
    protected Request callServiceToGetData(AsyncCallback<List<NoteBean>> callback) {
        return BIKClientSingletons.getService().getNotesByUser(callback);
    }

    @Override
    protected String setTextWhenListEmpty() {
        return I18n.brakKomentarzy.get() /* I18N:  */;
    }
}
