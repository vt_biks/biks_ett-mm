/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author wezyr
 */
public class NewLookUtils {

    public static void setupClassAndId(Widget tlp, String name) {
        tlp.setStylePrimaryName(name);
        GWTUtils.setElementAttribute(tlp.getElement(), "id" /* I18N: no */, name + "Id" /* I18N: no */);
        // GWTUtils.setStyleAttribute(tlp, "id", name + "Id");
    }

    public static void setupClassOnTabs(TabLayoutPanel tlp) {
        int wc = tlp.getWidgetCount();
        for (int i = 0; i < wc; i++) {
            tlp.getTabWidget(i).getParent().addStyleName("tabLook");
            // String style = tlp.getTabWidget(i).getParent().getStyleName();
        }
    }

    public static void makeCustomPushButton(PushButton btn) {
        GWTUtils.setElementAttribute(btn.getElement(), "id" /* I18N: no */, "myCustomPushButton");
        // DOM.setElementAttribute(btn.getElement(), "disabled", "");
        // DOM.setElementAttribute(btn.getElement(), "style", "float:" + direction + ";");

        // DOM.setElementProperty(btn.getElement(), "disabled", "false");
        // DOM.removeElementAttribute(btn.getElement(), "disabled");
    }

    public static void onHoverImagePushButton(PushButton btn, String image) {
        btn.getUpHoveringFace().setImage(new Image(image));
    }

    public static PushButton newCustomPushButton(String caption, ClickHandler clickHandler) {
        PushButton res = new PushButton(caption);
        NewLookUtils.makeCustomPushButton(res);
        res.addClickHandler(clickHandler);
        return res;
    }

    public static FlexTable mltxBackground(Widget body) {
        FlexTable mmrp = new FlexTable();
        mmrp.setStyleName("bfpl");
        mmrp.getFlexCellFormatter().setHorizontalAlignment(0, 0, HasHorizontalAlignment.ALIGN_CENTER);
        FlexTable mft = new FlexTable();
        mmrp.setWidget(0, 0, mft);
        mft.setStyleName("multixRegistrationPanel");
//        mft.setWidth("560px");
        mft.setWidget(0, 0, body);
        return mmrp;
    }
}
