/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import pl.fovis.client.bikpages.IBikPage;
import pl.trzy0.foxy.commons.annotations.CreatorPackage;
import simplelib.IDynamicClassCreator;

/**
 *
 * @author pmielanczuk
 */
@CreatorPackage("bikpages")
public interface IBikPageCreator extends IDynamicClassCreator<IBikPage<Integer>> {
}
