/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.IGridBeanAction;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.PushButtonWithHideText;

/**
 *
 * @author acichy
 */
public abstract class MyBIKSGridBase<T> implements IPaginatedItemsDisplayer<T> {

    protected Map<Integer, BikCustomButton> buttons = new LinkedHashMap<Integer, BikCustomButton>();
    protected Map<Integer, HTML> titles = new LinkedHashMap<Integer, HTML>();
    protected FlexTable gp = new FlexTable();
    protected int columnsCnt = 0;
    protected VerticalPanel activeTab = new VerticalPanel();
    protected IPaginatedItemsProvider<T> itemsProvider;
    protected List<T> allItems = new ArrayList<T>();
    protected List<T> filteredItems;
    protected int firstItemToDisplayIdx;

    protected abstract String getIconUrl(T n);

    //ww->mybiks: tego isEnabled chyba nikt nie używa, po co to miało być?
    protected boolean isEnabled() {
        // nie uzywane nie zmieniam
        return BIKClientSingletons.isEffectiveExpertLoggedIn();
    }

    public void rebuildTab(boolean refetch, boolean resetPosition) {
        if (refetch) {
            itemsProvider.refetchItems(resetPosition);
        } else {
            itemsProvider.recalcItemsToDisplay(resetPosition);
        }
    }

    public void setPaginatedItemsProvider(IPaginatedItemsProvider<T> paginator) {
        this.itemsProvider = paginator;
    }

    public Widget build(List<T> itemsToDisplay, int firstItemToDisplayIdx, List<T> filteredItems, List<T> allItems) {
        this.allItems = allItems;
        this.filteredItems = filteredItems;
        this.firstItemToDisplayIdx = firstItemToDisplayIdx;
        addItemsToGrid(itemsToDisplay);
        return activeTab;
    }

    public abstract Widget addItemsToGrid(List<T> itemsToDisplay);

    protected PushButton createActionBtn(final IGridBeanAction<T> act, String actionIconUrl, final T t) {
        PushButton btn = new PushButton(new Image(actionIconUrl));
        btn.addStyleName("followBtn");
        btn.setTitle(act.getActionIconHint(t));
        btn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                act.executeAction(t);
            }
        });

        btn.setVisible(act.isEnabled(t));
        return btn;
    }

    //ww->mybiks: "dodaj wiersz do elementu"? jak to rozumieć?
    // domyślam się, że chodziło o "dodaj wiersz dla elementu",
    // ale co - tylko jeden wiersz dla elementu? a może czasem dwa?
    protected abstract void createRowFromBean(int row, T n); //wiersz głowny

//    protected abstract Widget getWidgetToRowExpanded(T n); //jakas inna nazwe! -> to jest ten co sie pokazuje po kliknieciu dartdown
    protected abstract void setUpGridColumnsWidths(FlexTable gp);

    protected abstract void addOrRefreshTitles(int row, T n);

    protected boolean isIgnoreImageVisible() {
        return false;
    }

    protected PushButtonWithHideText addIgnoreImage(T n) {
        return null;
    }

    protected String getBackgroundStyleForRowInGrid(int rowNr, int markChanges) {
        return "RelatedObj-oneObj-" + (rowNr % markChanges == 0 ? "grey" /* I18N: no */ : "white" /* I18N: no */);
    }
}
