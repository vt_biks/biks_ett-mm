exec sp_check_version '1.0.29';
go


update bik_tree
set code = 'Taxonomy' + convert(varchar, id) where tree_kind = 1
go


alter table bik_tree alter column code varchar(255) not null
go


exec sp_update_version '1.0.29', '1.0.30';
go


