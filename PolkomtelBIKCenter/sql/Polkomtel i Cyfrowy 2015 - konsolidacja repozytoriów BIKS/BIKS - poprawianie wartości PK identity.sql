  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!


----------------------------------------------------------
----------------------------------------------------------
----------------------------------------------------------

-- brakuj�ce FK

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_node_name_chunk' and Name = 'node_id' and ReferencedTableName = 'bik_node')
alter table bik_node_name_chunk add constraint fk_bik_node_name_chunk_node_id foreign key (node_id) references bik_node (id);
go


if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_node_name_chunk' and Name = 'tree_id' and ReferencedTableName = 'bik_tree')
alter table bik_node_name_chunk add constraint fk_bik_node_name_chunk_tree_id foreign key (tree_id) references bik_tree (id);
go


----------------------------------------------------------
----------------------------------------------------------
----------------------------------------------------------


if  exists (select * from sys.objects where object_id = object_id(N'[dbo].[fn_index_info]') and type in (N'fn', N'if', N'tf', N'fs', N'ft'))
drop function [dbo].[fn_index_info]
go

create function [dbo].[fn_index_info] (@table_name_patt varchar(max))
returns @tab table (table_name sysname not null, index_name sysname not null, cols varchar(max) not null, include_cols varchar(max) null, 
  idx_level tinyint not null, has_identity_col tinyint not null, has_fk_col tinyint not null)
as
begin

  declare @fk_cols table (table_name sysname not null, col_name sysname not null, unique (table_name, col_name));
  
  insert into @fk_cols
  select distinct table_name, name from dbo.vw_ww_foreignkeys where Table_Name like @table_name_patt;

  declare @cur cursor;

  set @cur = cursor for 
  select 
       tablename = t.name,
       indexname = ind.name,
       --indexid = ind.index_id,
       --columnid = ic.index_column_id,
       columnname = col.name,
       --ind.*,
       --ic.*,
       --col.*
       col.is_identity,
       ind.is_primary_key,
       ind.is_unique,
       ind.is_unique_constraint,
       ic.is_included_column
  from 
       sys.indexes ind 
  inner join 
       sys.index_columns ic on  ind.object_id = ic.object_id and ind.index_id = ic.index_id 
  inner join 
       sys.columns col on ic.object_id = col.object_id and ic.column_id = col.column_id 
  inner join 
       sys.tables t on ind.object_id = t.object_id 
  where 
       --ind.is_primary_key = 1
       --and ind.is_unique = 0 
       --and ind.is_unique_constraint = 0 
       -- and 
       t.is_ms_shipped = 0 
       --and t.name = 'bik_fts_stemmed_word'
       and t.name like @table_name_patt
       --and (ind.is_unique = 1 or ind.is_unique_constraint = 1)
  order by 
       t.name, ind.name, ind.index_id, ic.index_column_id 
  ;

  open @cur;

  declare @table_name sysname, @index_name sysname, @col_name sysname, @is_identity tinyint,
    @is_primary_key tinyint,
    @is_unique tinyint,
    @is_unique_constraint tinyint, @is_included_column tinyint;
  
  declare @is_fk_col tinyint, @prev_table_name sysname = '', @prev_index_name sysname = '', @idx_col_names varchar(max) = null,
    @include_cols varchar(max) = null, @idx_level tinyint = null, @has_identity_col tinyint = null,
    @has_fk_col tinyint = null;

  fetch next from @cur into @table_name, @index_name, @col_name, @is_identity, @is_primary_key,
    @is_unique, @is_unique_constraint, @is_included_column;

  while @@fetch_status = 0
  begin
    if @prev_table_name <> @table_name or @prev_index_name <> @index_name
    begin
      if @idx_col_names is not null
      begin
        --print @prev_table_name + ' <' + @prev_index_name + '> (' + @idx_col_names + ')';
        insert into @tab (table_name, index_name, cols, include_cols, idx_level, has_identity_col, has_fk_col)
        values (@prev_table_name, @prev_index_name, @idx_col_names, @include_cols, @idx_level, @has_identity_col, @has_fk_col);
      end;
            
      select @idx_col_names = null, @include_cols = null, @has_identity_col = 0, @has_fk_col = 0;
      
      set @idx_level = case when @is_primary_key <> 0 then 3
                            when @is_unique_constraint <> 0 then 2
                            when @is_unique <> 0 then 1
                            else 0 end;
    end;
    
    if @is_included_column = 0
    begin   
      set @idx_col_names = case when @idx_col_names is null then '' else @idx_col_names + ', ' end + @col_name;

      set @is_fk_col = case when exists(select 1 from @fk_cols where table_name = @table_name and col_name = @col_name) then 1 else 0 end;
      
      if @is_fk_col <> 0 set @has_fk_col = 1;
      
      if @is_identity <> 0 set @has_identity_col = 1;    
    end
    else
      set @include_cols = case when @include_cols is null then '' else @include_cols + ', ' end + @col_name;
    
    select @prev_table_name = @table_name, @prev_index_name = @index_name;

    fetch next from @cur into @table_name, @index_name, @col_name, @is_identity, @is_primary_key,
      @is_unique, @is_unique_constraint, @is_included_column;
  end;

  --if @idx_col_names <> '' print @prev_table_name + ' <' + @prev_index_name + '> (' + @idx_col_names + ')';
  insert into @tab (table_name, index_name, cols, include_cols, idx_level, has_identity_col, has_fk_col)
  values (@prev_table_name, @prev_index_name, @idx_col_names, @include_cols, @idx_level, @has_identity_col, @has_fk_col);
  
  return;
end;
go


----------------------------------------------------------
----------------------------------------------------------
----------------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_fix_int_pk_identity_vals]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_fix_int_pk_identity_vals]
go

/*
  Kroki realizacji:
  
  -- 0.  wyznaczy� nazw� kolumny pk na @table_name
  -- 1.  za�o�y� tabelk� klona (pk jako int, inne kolumny jak lec�)
  -- 2.  doda� not null na kolumnie pk w tabelce klonie
  -- 3.  do�o�y� primary key constraint na kolumnie pk w klonie
  -- 4.  wy��czenie FK w innych tabelach do tabeli @table_name
  -- 5.  switch z @table_name na klona
  -- 6.  update warto�ci pk na klonie
  -- 7.  wy��czenie indeks�w (disable) na @table_name - zostaje tylko primary key
  -- 8.  switch z klona na @table_name
  -- 9.  reseed identity na @table_name
  -- 10. usun�� tabel� klon!!!
  -- 11. wy��czenie trigger�w na tabelach w kt�rych b�d� zmiany FK
  -- 12. wy��czenie indeks�w na tabelach odwo�uj�cych si� do @table_name
  -- 13. update zawarto�ci FK w tabelach odwo�uj�cych si� do @table_name
  -- 14. rebuild indeks�w na @table_name
  -- 15. rebuild indeks�w na tabelach odwo�uj�cych si� do @table_name
  -- 16. w��czenie FK w tabelach odwo�uj�cych si� do @table_name
  -- 17. w��czenie trigger�w na tabelach w kt�rych by�y zmiany FK

*/

create procedure [dbo].[sp_fix_int_pk_identity_vals](@table_name sysname, @val_fix_sql varchar(max), @diag_level int = 1) as
begin
  set nocount on;

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: start for table: ' + @table_name;

  declare @err int, @rc int;

  -- declare @table_name sysname = 'bik_node';
  declare @pk_col sysname = (select cols from dbo.fn_index_info(replace(replace(@table_name, '_', '[_]'), '%', '[%]')) where has_identity_col = 1 and idx_level = 3 and charindex(',', cols) = 0);
  
  if @pk_col is null begin
    print 'table ' + @table_name + ' has no primary key with identity';
    return;
  end; 

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: @pk_col = ' + @pk_col;
  
  create table #ident_val_fix (old_val int not null primary key, new_val int not null unique);
  
  declare @sql_txt nvarchar(max) = N'insert into #ident_val_fix ' + @val_fix_sql;

  exec(@sql_txt);

  select @err = @@error, @rc = @@rowcount;
  if @err <> 0 begin
    print 'error occurred on insert into #ident_val_fix, @err = ' + cast(@err as varchar(20));
    return @err;
  end;
  
  declare @ivf_row_cnt int = (select count(1) from #ident_val_fix);
    
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: #ident_val_fix created, number of rows: ' + cast(@ivf_row_cnt as varchar(20));

  set @sql_txt = N'select @count = count(*) from ' + @table_name;
  declare @tab_row_cnt int;
  exec sp_executesql @sql_txt, N'@count int output', @tab_row_cnt output;

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: number of rows in ' + @table_name + ': ' + cast(@tab_row_cnt as varchar(20));

  if @tab_row_cnt <> @ivf_row_cnt begin
    print 'row counts of #ident_val_fix and ' + @table_name + ' differ, error!';
    return -1;
  end;

  set @sql_txt = N'select @count = count(*) from ' + @table_name + ' t left join #ident_val_fix f on t.' + @pk_col + ' = f.old_val where f.old_val is null';
  declare @missing_ids_cnt int;
  exec sp_executesql @sql_txt, N'@count int output', @missing_ids_cnt output;

  if @missing_ids_cnt <> 0 begin
    print 'there are missing values for PK in #ident_val_fix, number of missing values: ' + cast(@missing_ids_cnt as varchar(20)) + ', error!';
    return -2;
  end;

  --select * from #ident_val_fix;
  
  select Table_Name, Name as fk_column_name, ForeignKey_Name as fk_name, ReferencedTableName as ref_table_name 
  into #ident_fk_col
  --select * 
  from vw_ww_foreignkeys
  where (ReferencedTableName = @table_name and ReferencedColumn = @pk_col) or Table_Name = @table_name;
  
  select @err = @@error, @rc = @@rowcount;
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: number of FKs referencing ' + @table_name + ': ' + cast(@rc as varchar(20));
  
  --select * from #ident_fk_col;  
  
  -- indeksy (inne ni� PK) na naszej tabeli
  select 
    t.name as table_name,
    ind.name as idx_name -- , *
  into #ident_idx
  from 
    sys.indexes ind inner join 
    sys.tables t on ind.object_id = t.object_id 
  where 
    t.name = @table_name
    and ind.is_disabled = 0
    and ind.is_primary_key = 0
  --order by t.name, ind.name
  ;
  
  select @err = @@error, @rc = @@rowcount;
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: number of non-PK idxes on ' + @table_name + ': ' + cast(@rc as varchar(20));

  declare @clone_table_name sysname = 'tmp_fix_int_pk_' + substring(@table_name, 1, 60) + '_' + replace(newid(), '-', '_');

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: clone table name: ' + @clone_table_name;  
  
  declare @cols_exprs_for_clone varchar(max) = 'cast(10 as int) as ' + @pk_col;
  
  select @cols_exprs_for_clone = @cols_exprs_for_clone + ', ' + name
  from sys.columns where object_id = object_id(@table_name) and name <> @pk_col;
  
  set @sql_txt = 'select ' + @cols_exprs_for_clone + ' into ' + @clone_table_name + ' from ' + @table_name + ' where 1 = 0;
  alter table ' + @clone_table_name + ' alter column ' + @pk_col + ' int not null;
  alter table ' + @clone_table_name + ' add constraint pk_' + @clone_table_name + ' primary key (' + @pk_col + ');';
  exec(@sql_txt);
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: clone table created and structure fixed';  

  set @sql_txt = null;
  
  select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ';
  ' end + 'alter table ' + table_name + ' nocheck constraint ' + fk_name
  from
  #ident_fk_col;
  
  if @sql_txt is not null exec(@sql_txt);

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: FKs referencing ' + @table_name + ' disabled (if any)';  
  
  set @sql_txt = 'alter table ' + @table_name + ' switch to ' + @clone_table_name + ';';
  exec(@sql_txt);
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: table data switched to clone';  

  set @sql_txt = 'update c set ' + @pk_col + ' = f.new_val from ' + @clone_table_name + ' c inner join #ident_val_fix f on c.' + @pk_col + ' = f.old_val';
  exec(@sql_txt);

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: PK values in ' + @table_name + ' fixed to new vals';  
 
  create table #ident_disabled_idx (table_name sysname not null, idx_name sysname not null, unique (table_name, idx_name));
 
  set @sql_txt = null;
  
  select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ';
  ' end + 'alter index ' + idx_name + ' on ' + @table_name + ' disable'
  from
  #ident_idx;
  
  if @sql_txt is not null exec(@sql_txt);

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: idxes on ' + @table_name + ' disabled (if any)';  

  insert into #ident_disabled_idx (table_name, idx_name) select @table_name, idx_name from #ident_idx;
  
  set @sql_txt = 'alter table ' + @clone_table_name + ' switch to ' + @table_name + ';';
  exec(@sql_txt);

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: data switched from clone back to ' + @table_name;  
  
  set @sql_txt = 'DBCC CHECKIDENT (''' + @table_name + ''');';
  exec(@sql_txt);

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: identity on ' + @table_name + ' reseeded';  

  declare @trigger_cnt int;

  create table #ident_trigger (table_name sysname not null, trigger_name sysname not null, unique (table_name, trigger_name));

  insert into #ident_trigger (table_name, trigger_name)
  select t.name as table_name, tr.name as trigger_name
  from sys.triggers tr
  inner join sys.tables t on t.object_id = tr.parent_id
  where tr.is_disabled = 0 and t.name in (select table_name from #ident_fk_col where ref_table_name = @table_name);

  select @err = @@error, @rc = @@rowcount;
  
  set @trigger_cnt = @rc;
  
  if @trigger_cnt <> 0 begin
    set @sql_txt = null;
    
    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ';
    ' end + 'disable trigger ' + trigger_name + ' on ' + table_name
    from
    #ident_trigger;
    
    exec(@sql_txt);  
  end;
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: after disabling triggers (count: ' + cast(@trigger_cnt as varchar(20)) + ')';

  declare @cur cursor;
  set @cur = cursor for select table_name, fk_column_name from #ident_fk_col where ref_table_name = @table_name;

  declare @cur_tn sysname, @cur_cn sysname;
  
  open @cur;
  
  fetch next from @cur into @cur_tn, @cur_cn;
  
  while @@fetch_status = 0 begin
    if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: before fix FK vals in ' + @cur_tn + '.' + @cur_cn;  

    declare @idxes_on_tn_cn table (idx_name sysname not null primary key);
        
    delete from @idxes_on_tn_cn;
    
    declare @idx_cnt_on_tn_cn int, @dup_idx_cnt_on_tn_cn int;
    
    insert into @idxes_on_tn_cn    
    select 
      distinct ind.name
    from 
      sys.indexes ind inner join 
      sys.index_columns ic on  ind.object_id = ic.object_id and ind.index_id = ic.index_id inner join 
      sys.columns col on ic.object_id = col.object_id and ic.column_id = col.column_id inner join 
      sys.tables t on ind.object_id = t.object_id 
    where 
      t.name = @cur_tn and col.name = @cur_cn and ind.is_primary_key = 0;

    set @idx_cnt_on_tn_cn = @@rowcount;

    delete from ni
    from @idxes_on_tn_cn ni left join #ident_disabled_idx ai on ai.table_name = @cur_tn and ai.idx_name = ni.idx_name
    where ai.table_name is not null;

    set @dup_idx_cnt_on_tn_cn = @@rowcount;

    insert into #ident_disabled_idx (table_name, idx_name)
    select @cur_tn, ni.idx_name
    from @idxes_on_tn_cn ni;

    if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]:     number of idxes on table ' + @cur_tn + ' with column ' + @cur_cn + ': '
      + cast(@idx_cnt_on_tn_cn as varchar(20)) + ', not yet disabled cnt: ' + cast(@idx_cnt_on_tn_cn - @dup_idx_cnt_on_tn_cn as varchar(20));  
    
    set @sql_txt = null;
    
    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ';
    ' end + 'alter index ' + idx_name + ' on ' + @cur_tn + ' disable'
    from @idxes_on_tn_cn;
    
    if @sql_txt is not null exec(@sql_txt);

    if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]:     idxes on ' + @cur_tn + ' disabled (if any new)';  

    select @sql_txt = 'update t set ' + @cur_cn + ' = f.new_val from ' + @cur_tn + ' t inner join #ident_val_fix f on t.' + @cur_cn + ' = f.old_val';
    exec(@sql_txt);

    if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: after fix FK vals in ' + @cur_tn + '.' + @cur_cn;  

    fetch next from @cur into @cur_tn, @cur_cn;
  end;


/*
  set @sql_txt = null;
  
  select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ';
  ' end + 'update t set ' + fk_column_name + ' = f.new_val from ' + table_name + ' t inner join #ident_val_fix f on t.' + fk_column_name + ' = f.old_val'
  from
  #ident_fk_col;
  
  if @sql_txt is not null exec(@sql_txt);

*/

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: all FKs values fixed (if any)';  

  set @sql_txt = null;
  
/*
  select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ';
  ' end + 'alter index ' + idx_name + ' on ' + @table_name + ' rebuild'
  from
  #ident_idx;
*/
  
  declare @disabled_idx_cnt int = (select count(1) from #ident_disabled_idx);
  
  if @disabled_idx_cnt > 0 begin  
    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ';
    ' end + 'alter index ' + idx_name + ' on ' + table_name + ' rebuild'
    from
    #ident_disabled_idx;

    exec(@sql_txt);
  end;
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: all disabled idxes are rebuilt (idx cnt: ' + cast(@disabled_idx_cnt as varchar(20)) + ')';  
  
  set @sql_txt = null;
  
  select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ';
  ' end + 'alter table ' + table_name + ' with check check constraint ' + fk_name
  from
  #ident_fk_col;
  
  if @sql_txt is not null exec(@sql_txt);

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: FKs enabled (if any)';  

  if @trigger_cnt <> 0 begin
    set @sql_txt = null;
    
    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ';
    ' end + 'enable trigger ' + trigger_name + ' on ' + table_name
    from
    #ident_trigger;
    
    exec(@sql_txt);  
  end;
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: after enabling triggers (count: ' + cast(@trigger_cnt as varchar(20)) + ')';

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ':[sp_fix_int_pk_identity_vals]: all done';  
end;
go


  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!
  -- 10. usun�� tabel� klon!!!


/*

select max(id) from bik_node

select count(1) from bik_node



exec sp_fix_int_pk_identity_vals 'bik_node', 'select id, id + 10000000 from bik_node';
-- 3:40


exec sp_fix_int_pk_identity_vals 'bik_tree', 'select id, id - 100 from bik_tree';
-- 1:02


exec sp_fix_int_pk_identity_vals 'bik_node_kind', 'select id, id + 300 from bik_node_kind';


select * from bik_node_kind


select * from bik_tree


select count(1) from bik_node

select count(1) from bik_tree

select * from vw_ww_foreignkeys where ReferencedTableName = 'bik_tree'

exec sp_fix_int_pk_identity_vals 'tmp_fix_ident_a', 'select id, id + 100 from tmp_fix_ident_a /*where id <> 50 union all select 1000, 1000*/';

select * from tmp_fix_ident_a
order by id

select * from tmp_fix_ident_b b left join tmp_fix_ident_a a on b.a_id = a.id
order by b.id


insert into tmp_fix_ident_a (code, descr)
select top 1 '�ukasz' + cast(cast(substring(code, 7, len(code)) as int) + 1 as varchar(20)), descr
from tmp_fix_ident_a where code like '�ukasz%' order by cast(substring(code, 7, len(code)) as int) desc


exec sp_fix_int_pk_identity_vals 'tmp_fix_ident_a', 'select id, id + 20 from tmp_fix_ident_a';

*/


/*


  select *
  from vw_ww_foreignkeys
  where ForeignKey_Name = 'FK__bik_node__node_k__7FBC4DCC'


FK__bik_node__node_k__7FBC4DCC

*/


/*

SELECT  
       TAB.name as Table_Name 
     , TRIG.name as Trigger_Name
     , TRIG.is_disabled  --or objectproperty(object_id('TriggerName'), 'ExecIsTriggerDisabled')
FROM [sys].[triggers] as TRIG 
inner join sys.tables as TAB 
on TRIG.parent_id = TAB.object_id 

*/

--select len(replace(newid(), '-', '_'))

--exec sp_fix_int_pk_identity_vals 'bik_node', 'select id, id + 1000000 from bik_node';

/*
select node_kind_id, nk.code, count(*) cnt
from bik_node n inner join bik_node_kind nk on n.node_kind_id = nk.id
group by node_kind_id, nk.code
order by cnt desc
*/
