/*
declare @tab table (node_kind_id int not null)

insert into @tab
*/
    
    select
    bn.node_kind_id, bnk.caption,
    count(*) as cnt
    from bik_node bn
    inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    where bn.is_deleted = 0 and (bn.linked_node_id is null or bn.disable_linked_subtree <> 0)
    and bn.tree_id in (1,2,3,4,8,12,15,14,16,6,7,13,18)
    and bn.node_kind_id in (60,8,11,10,9,61,62,12,48,3,32,39,41,40,42,4,31,34,37,33,5,49,6,56,55,53,25,43,58,57,2,29,17,21,22,24,44,54,19,16,14,46,45,23,13)
       -- ) bn --
    group by
    bn.node_kind_id, bnk.caption
    order by bnk.caption
