/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.client;

import java.io.Serializable;
import pl.trzy0.foxy.commons.annotations.CreatorPackage;
import simplelib.INamedTypedBeanPropsBeanWrapperCreator;

/**
 *
 * @author pmielanczuk
 */
@CreatorPackage(value = "/pl/bssg/biks/adm/common")
public interface IAdmBeanWrapper extends INamedTypedBeanPropsBeanWrapperCreator<Serializable> {
}
