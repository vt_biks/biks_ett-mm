/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.treeandlist;

import java.util.Collection;

/**
 *
 * @author wezyr
 */
public interface ITreeRowAndBeanStorage<IDT, TR, BEAN> extends ITreeRowStorage<IDT, TR> {

    public BEAN getBeanById(IDT nodeId);

    public TR registerAndConvertBeanToRow(BEAN bean);

    public Collection<BEAN> getAllBeans();
}
