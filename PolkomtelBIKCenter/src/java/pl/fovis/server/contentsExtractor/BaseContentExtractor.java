/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.contentsExtractor;

import java.util.Set;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public abstract class BaseContentExtractor implements IContentsExtractor {

    protected Set<String> acceptedExtensions;

    public BaseContentExtractor(String fileExtensions) {
        if ("*".equals(fileExtensions)) {
            acceptedExtensions = null;
        } else {
            acceptedExtensions = BaseUtils.splitUniqueBySep(fileExtensions, "|", true);
        }
    }

    @Override
    public boolean canExtractFromFile(String fullFilePath) {
        if (acceptedExtensions == null) {
            return true;
        }
        fullFilePath = fullFilePath.replace("\\", "/");
        String ext = BaseUtils.extractFileExtFromFullPath(fullFilePath, false);
        return acceptedExtensions.contains(ext);
    }
}
