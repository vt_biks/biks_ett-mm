/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.fovis.common.i18npoc.I18n;
import static pl.fovis.foxygwtcommons.AdminConfigBaseWidget.createButton;
import static pl.fovis.foxygwtcommons.AdminConfigBaseWidget.createLabel;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuationWithReturn;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class AdminConfigDQMConnectionsWidget extends AdminConfigManyServersBase<List<ConnectionParametersDQMConnectionsBean>, ConnectionParametersDQMConnectionsBean> {

    protected TextBox tbxServer;
    protected TextBox tbxName;
    protected ListBox lbSourceType;
    protected Label lblJdbcTemplate;
    protected TextBox tbxUser;
    protected TextBox tbxPw;

    protected TextBox tbxInstance;
    protected TextBox tbxDatabaseName;
    protected TextBox tbxPort;
    protected Label instanceLbl;
    protected Label portLbl;
    protected Label databaseNameLbl;
    protected VerticalPanel parametersVp;
    protected int serverId;

    @Override
    protected void activateForServer(String id) {

        ConnectionParametersDQMConnectionsBean bean = serversMap.get(id);
        tbxServer.setText(bean.server);
        tbxName.setText(bean.name);
        tbxUser.setText(bean.user);
        tbxPw.setText(bean.password);
        tbxInstance.setText(bean.instance);
        tbxDatabaseName.setText((bean.databaseName));
        tbxPort.setText(BaseUtils.safeToStringNotNull(bean.port));
        serverId = bean.id;
        for (int i = 0; i < lbSourceType.getItemCount(); i++) {
            if (lbSourceType.getItemText(i).trim().equalsIgnoreCase(bean.sourceType.trim())) {
                lbSourceType.setSelectedIndex(i);
                show(lbSourceType.getItemText(i));
                break;
            }
        }

    }

    @Override
    protected void buildRightWidget(CellPanel panelRight) {

        parametersVp = new VerticalPanel();
        createLabel(I18n.nazwa.get() + ":", panelRight);
        tbxName = createTextBox(panelRight, false);
        createLabel(I18n.serwer.get() + ":", panelRight);
        tbxServer = createTextBox(panelRight, false);
        createLabel(I18n.uzytkownik.get() + ":", panelRight);
        tbxUser = createTextBox(panelRight);
        createLabel(I18n.haslo.get() + ":", panelRight);
        tbxPw = createPasswordTextBox(panelRight);
        HorizontalPanel hp = new HorizontalPanel();
        Label driverClass = createLabel(I18n.konektor.get() + ": ", hp);
        driverClass.addStyleName("defBottomLine");

        Set<String> sourceType = new HashSet<String>();
        sourceType.add(PumpConstants.SOURCE_NAME_ORACLE);
        sourceType.add(PumpConstants.SOURCE_NAME_MSSQL);
        sourceType.add(PumpConstants.SOURCE_NAME_POSTGRESQL);
        sourceType.add(PumpConstants.SOURCE_NAME_TERADATA);
        lbSourceType = createListBoxEx(sourceType, hp);
        lbSourceType.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                show(lbSourceType.getItemText(lbSourceType.getSelectedIndex()));
                tbxPort.setText("");
                tbxDatabaseName.setText("");
                tbxInstance.setText("");
            }
        });
        panelRight.add(hp);
        panelRight.add(lblJdbcTemplate = new Label());
        panelRight.add(parametersVp);

        instanceLbl = createLabel(I18n.instancja.get() + ":", parametersVp);
        tbxInstance = createTextBox(parametersVp, false);
        databaseNameLbl = createLabel(I18n.bazaDanych.get() + ":", parametersVp);
        tbxDatabaseName = createTextBox(parametersVp, false);
        portLbl = createLabel(I18n.port.get() + ":", parametersVp);
        tbxPort = createTextBox(parametersVp, false);
        show(lbSourceType.getItemText(lbSourceType.getSelectedIndex()));
        createButton(I18n.zapiszZmiany.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final ConnectionParametersDQMConnectionsBean bean = new ConnectionParametersDQMConnectionsBean();
                bean.id = Integer.parseInt(getValueForSelectedIndex());
                bean.name = tbxName.getText();
                bean.server = tbxServer.getText();
                bean.sourceType = lbSourceType.getSelectedItemText();
                bean.user = tbxUser.getText();
                bean.password = tbxPw.getText();
                bean.instance = tbxInstance.getText();
                bean.port = BaseUtils.tryParseInteger(tbxPort.getText());
                bean.databaseName = tbxDatabaseName.getText();
                BIKClientSingletons.getService().updateDQMConnections(bean, new StandardAsyncCallback<Void>() {

                    @Override
                    public void onSuccess(Void result) {

                        serversMap.put(String.valueOf(bean.id), bean);
                        serverNames.remove(listbox.getItemText(getSelectedIndex()));
                        serverNames.add(bean.name);
                        listbox.setItemText(getSelectedIndex(), bean.name);
                        activateForServer(String.valueOf(bean.id));

                        BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get());

                    }
                });

            }
        }, panelRight);
        createTestConnectionWidget(panelRight, new IParametrizedContinuationWithReturn<Void, Integer>() {

            @Override
            public Integer doIt(Void param) {
                return serverId;
            }
        });
        panelRight.setWidth("100%");
    }

    protected void show(String selectedText) {

        boolean isSqlServer = selectedText.equals(PumpConstants.SOURCE_NAME_MSSQL);
        boolean isOracle = selectedText.equals(PumpConstants.SOURCE_NAME_ORACLE);
        boolean isPostgres = selectedText.equals(PumpConstants.SOURCE_NAME_POSTGRESQL);
        tbxInstance.setVisible(isSqlServer || isOracle);
        instanceLbl.setVisible(isSqlServer || isOracle);
        tbxDatabaseName.setVisible(!isOracle);
        databaseNameLbl.setVisible(!isOracle);
        tbxPort.setVisible(isOracle || isPostgres);
        portLbl.setVisible(isOracle || isPostgres);
    }

    @Override
    protected void callServiceToAddNewServer(String source, String serverName, boolean needSeparateSchedule, AsyncCallback<ConnectionParametersDQMConnectionsBean> asyncCallback) {
        BIKClientSingletons.getService().addDQMConnections(serverName, asyncCallback);

    }

    @Override
    protected void callServiceToDeleteServer(int id, String serverName, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().deleteDQMConnections(id, asyncCallback);
    }

    @Override
    protected void callServiceToTestConnection(int id, AsyncCallback<Pair<Integer, String>> asyncCallback) {
        BIKClientSingletons.getService().runDQMTestConnection(id, null, asyncCallback);
    }
}
