﻿exec sp_check_version '1.0.98.2';
go

----------------------------------
----------------------------------
----------------------------------

-- indeks bez stop-listy
-- tutaj "do" nie przeszkadza!
if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val'))
exec('drop fulltext index on bik_searchable_attr_val')
go

declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_searchable_attr_val') and is_primary_key = 1)
exec('CREATE FULLTEXT INDEX ON bik_searchable_attr_val (value, fixed_value) key index ' + @pk_name + ' with stoplist = off')
go


alter table bik_searchable_attr_val add tree_id int null
go

alter table bik_searchable_attr_val add node_kind_id int null
go


-- PM: to trwa u mnie 01:34 - czyli 94 sekundy
update bik_searchable_attr_val
set tree_id = n.tree_id, node_kind_id = n.node_kind_id
from bik_node n
where n.id = bik_searchable_attr_val.node_id
go


delete from bik_searchable_attr_val 
-- select * 
from bik_searchable_attr_val av inner join bik_node n on av.node_id = n.id
inner join bik_searchable_attr a on av.attr_id = a.id
where a.node_kind_id <> n.node_kind_id or n.is_deleted = 1
go

-- select count(*) from bik_searchable_attr_val

exec sp_set_app_prop 'search_order_by_exprs', 'max_search_weight desc, bn.search_rank + bnk.search_rank desc, bn.vote_sum desc, sum_combined_rank desc'
go


-- zmiana nazwy klucza głównego (indeksu) na bik_searchable_attr_val - z nazwy losowej (domyślnej gdy się jawnie nie poda)
-- na nazwę jawną i ładną: PK_bik_searchable_attr_val - potrzebne m. in. do hintów w zapytaniach do wyszukiwania
declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_searchable_attr_val') and is_primary_key = 1)
EXEC ('sp_rename N''bik_searchable_attr_val.' + @pk_name + ''', N''PK_bik_searchable_attr_val'', N''INDEX''');
GO


alter table bik_searchable_attr add search_weight int null
go

alter table bik_searchable_attr add caption varchar(255) null
go

update bik_searchable_attr
set search_weight = coalesce(ap.search_weight, 1), caption = coalesce(ap.caption, a.name)
from bik_searchable_attr a left join bik_searchable_attr_props ap on a.name = ap.name
go

alter table bik_searchable_attr alter column search_weight int not null
go

alter table bik_searchable_attr alter column caption varchar(255) not null
go

alter table bik_searchable_attr_val alter column tree_id int not null
go

alter table bik_searchable_attr_val alter column node_kind_id int not null
go

-- czas wykonania: 02:51
-- drop index idx_bik_searchable_attr_val_nk_tr  on bik_searchable_attr_val
-- w nowym podejściu ten indeks wydaje się zbędny - rozważyć usunięcie niebawem, jak się skończą
-- stabilizacja i testy wyszukiwarki
CREATE NONCLUSTERED INDEX idx_bik_searchable_attr_val_nk_tr
ON [dbo].[bik_searchable_attr_val] ([node_kind_id],[tree_id])
INCLUDE ([id],[attr_id],[node_id],[value])
GO

-- ten indeks się przyda m. in. w kolejnym update - zamiast ponad 9 minut bez indeksu
-- ten update będzie trwał pół minuty (przy 1 760 007 aktualizowanych wierszy)
create index idx_bik_searchable_attr_name on bik_searchable_attr (name)
go


update bik_searchable_attr_val
set attr_id = (select min(id) from bik_searchable_attr a_m where a_m.name = a.name)
from bik_searchable_attr a where a.id = bik_searchable_attr_val.attr_id
go

delete 
--select * 
from bik_searchable_attr where id not 
in (select attr_id from bik_searchable_attr_val)
go


declare @idx_name sysname
select @idx_name = i.name
-- o.name as table_name, c.name as column_name, i.name as index_name, i.type_desc as index_type_desc, i.is_primary_key, i.is_unique, i.is_unique_constraint 
from sys.indexes i 
inner join sys.index_columns ic on i.index_id = ic.index_id and ic.object_id = i.object_id
inner join sys.columns c on ic.object_id = c.object_id and ic.column_id = c.column_id
inner join sys.objects o on i.object_id = o.object_id
where o.name = 'bik_searchable_attr' and c.name = 'node_kind_id'
--exec ('drop index ' + @idx_name + ' on bik_searchable_attr')
exec ('alter table bik_searchable_attr drop constraint ' + @idx_name)
go

declare @fk_name sysname
select @fk_name = foreignKey_name from dbo.vw_ww_foreignkeys
where Table_Name = 'bik_searchable_attr' and name = 'node_kind_id'
exec ('alter table bik_searchable_attr drop constraint ' + @fk_name)
go


--select * from bik_searchable_attr
alter table bik_searchable_attr drop column node_kind_id
go


declare @attr_id int = (select id from bik_searchable_attr where name = 'sql_of_derived_table_with_alias')
delete from bik_searchable_attr_val where attr_id = @attr_id
delete from bik_searchable_attr where id = @attr_id
go


alter table bik_searchable_attr_val drop column value_importance
go


alter table bik_searchable_attr drop column type
go

-- select * from bik_searchable_attr

declare @attr_id int = (select id from bik_searchable_attr where name = 'database_enigme')
delete from bik_searchable_attr_val where attr_id = @attr_id
delete from bik_searchable_attr where id = @attr_id
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_verticalize_node_attrs_inner') and type in (N'P', N'PC'))
drop procedure dbo.sp_verticalize_node_attrs_inner
go

create procedure dbo.sp_verticalize_node_attrs_inner(@optNodeFilter varchar(max))
as
begin
  exec sp_verticalize_node_attrs_one_table 'bik_node', 'id', 'name, descr', null, 0, @optNodeFilter

  exec sp_verticalize_node_attrs_metadata @optNodeFilter
end
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------
-- poprzednia wersja w pliku "alter db for v1.0.88.12 ww.sql", 
-- tutaj dodajemy with stoplist = off do tworzenia FULLTEXT INDEX na bik_searchable_attr_val

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs
go

create procedure [dbo].[sp_verticalize_node_attrs](@optNodeFilter varchar(max))
as
begin
  if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val'))
  exec('drop fulltext index on bik_searchable_attr_val')

  exec sp_verticalize_node_attrs_inner @optNodeFilter

  declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_searchable_attr_val') and is_primary_key = 1)
  exec('CREATE FULLTEXT INDEX ON bik_searchable_attr_val (value, fixed_value) key index ' + @pk_name + ' with stoplist = off')
end
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-- poprzednia wersja w pliku alter db for v1.0.88.13 tf.sql
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_metadata]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_metadata
go

create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_extradata', 'node_id', 'author, owner, cuid, guid, ruid', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @optNodeFilter
  --exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_column', 'table_id', 'name', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_engine, database_source, connetion_networklayer_name, user_name, server', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @optNodeFilter
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table', null, @optNodeFilter
end
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

if OBJECT_ID (N'dbo.fn_calc_searchable_attr_val_weight', N'FN') is not null
    drop function dbo.fn_calc_searchable_attr_val_weight;
go

create function dbo.fn_calc_searchable_attr_val_weight(@attr_search_weight int, @node_search_rank int, @kind_search_rank int, @positive_vote_cnt int)
returns int as
begin
  return (@attr_search_weight + @node_search_rank + @kind_search_rank) * 100 + @positive_vote_cnt;
end
go


-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-- poprzednia wersja pochodzi z pliku: alter db for v1.0.88.12 ww.sql

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_one_table]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_one_table
go

create procedure [dbo].[sp_verticalize_node_attrs_one_table](@source varchar(max), @nodeIdCol sysname, @cols varchar(max),
  @properNames varchar(max), @valueImportance int, @optNodeFilter varchar(max))
as
begin
  set nocount on
  
  declare @diags_level int = 0 -- 0 oznacza brak logowania

  -- diag
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': start'

  --------------
  -- 1. rozbijamy nazwy atrybutów - nazwy kolumn w źródle i nazwy właściwe (opcjonalne)
  --------------

  declare @attrPropNamesTab table (idx int primary key, name sysname not null)
  
  insert into @attrPropNamesTab (idx, name)
  select idx, str from dbo.fn_split_by_sep(@properNames, ',', 7)
  
  declare @attrNamesTab table (name sysname not null primary key, 
    proper_name varchar(255) not null, search_weight int not null,
    attr_id int null
  )
  
  insert into @attrNamesTab (name, proper_name, search_weight,
    attr_id)
  select aaa.str, coalesce(apnt.name, aaa.str), coalesce(a.search_weight, 1),
    a.id
  from dbo.fn_split_by_sep(@cols, ',', 7) aaa left join @attrPropNamesTab apnt on aaa.idx = apnt.idx
    left join 
      bik_searchable_attr a on a.name = coalesce(apnt.name, aaa.str)

  --------------
  -- 2. znamy nazwy atrybutów i node_kindy, teraz uzupełniamy bik_attribute, ew. poprawiamy tam typy
  --------------
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed uzupełnieniem searchable attrs'
  
  insert into bik_searchable_attr (name, caption, search_weight)
  select ant.proper_name, ant.proper_name, 1
  from @attrNamesTab ant
  where ant.attr_id is null  
  
  update @attrNamesTab set attr_id = a.id
  from @attrNamesTab aaa inner join bik_searchable_attr a on aaa.attr_id is null and a.name = aaa.name
  
  --------------
  -- 3. wrzucamy wartości atrybutów do tabeli pomocniczej
  --------------

  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przygotowanie do wrzucania do @attrValsTab'
  
  declare @attrValsTab table (attr_id int not null, node_id int not null, value varchar(max) null, 
    --type int not null, 
    search_weight int not null, tree_id int not null, node_kind_id int not null)
  
  declare attrCur cursor for select name, --attr_type, 
  proper_name, search_weight, attr_id from @attrNamesTab
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed open attrCur'

  open attrCur
  
  declare @attrName sysname, @attrType int, @attrProperName varchar(255), @searchWeight int, @attr_id int
  declare @attrValSql varchar(max)
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed pierwszym fetch next from attrCur'

  fetch next from attrCur into @attrName, --@attrType, 
    @attrProperName, @searchWeight, @attr_id
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed pętlą'

  while @@fetch_status = 0
  begin
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': start pętli dla pola: ' + @attrName + ' (' + @attrProperName + ')'
    
    set @attrValSql = 
    'select attr_id, node_id, case when is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''' then null else value end, search_weight,' +
    ' tree_id, node_kind_id from (' +
    'select ' + cast(@attr_id as varchar(20)) + ' as attr_id, x.' + @nodeIdCol + ' as node_id, x.' + @attrName + ' as value, ' + --', a.type, ' + 
    --cast(@searchWeight as varchar(20)) + ' * (n.search_rank + nk.search_rank) * 100 + n.vote_sum as search_weight, ' +
    'dbo.fn_calc_searchable_attr_val_weight(' + cast(@searchWeight as varchar(20)) + ', n.search_rank, nk.search_rank, n.vote_sum) as search_weight, ' + 
    + 'case when n.is_deleted = 0 and (n.linked_node_id is null or n.disable_linked_subtree <> 0) then 0 else 1 end is_deleted,' + 
      ' n.tree_id, n.node_kind_id' +
      ' from ' + @source + ' as x inner join bik_node n on n.id = x.' + @nodeIdCol +
      ' inner join bik_node_kind nk on n.node_kind_id = nk.id' +
      --' inner join bik_searchable_attr a on n.node_kind_id = a.node_kind_id and a.name = ''' + @attrProperName + '''' +
      --' where x.' + @attrName + ' is not null' +
      case when @optNodeFilter is null then '' else ' where ' + @optNodeFilter end +
      ' ) x'
    if @diags_level > 0 print 'sql for attr=' + @attrName + ' is: 
    ' + @attrValSql
  
    insert into @attrValsTab (attr_id, node_id, value, --type, 
    search_weight, tree_id, node_kind_id)
    exec (@attrValSql)
  
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed końcem iteracji pętli dla pola: ' + @attrName
    
    fetch next from attrCur into @attrName, --@attrType, 
      @attrProperName, @searchWeight, @attr_id
  end
    
  close attrCur
  
  deallocate attrCur
  
  --------------
  -- 5. aktualizacja bik_searchable_attr_val - na podstawie wyciągniętych wartości
  --------------
  
  if @diags_level > 0 begin
    declare @avtCnt int = (select count(*) from @attrValsTab)
    print cast(sysdatetime() as varchar(23)) + ': za pętlą, kursor zamknięty, liczba wartości razem: ' +
      cast(@avtCnt as varchar(20)) + ', przed delete'
  end
  
  declare @delCnt int
  delete from bik_searchable_attr_val
  from @attrValsTab avt
  where avt.value is null and bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id
  set @delCnt = @@rowcount
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po delete, rowcnt=' + cast(@delCnt as varchar(20)) + ', przed update'
  end

  update bik_searchable_attr_val
  set value = avt.value, 
    fixed_value = case when bik_searchable_attr_val.value <> avt.value then dbo.fn_normalize_text_for_fts(avt.value) else fixed_value end,
    search_weight = avt.search_weight
  from @attrValsTab avt
  where avt.value is not null and bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id and
     (bik_searchable_attr_val.value <> avt.value or bik_searchable_attr_val.search_weight <> avt.search_weight)
  
  declare @updRowCnt int = @@rowcount
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po update, row count: ' +
      cast(@updRowCnt as varchar(20)) + ', przed insert'
  end
  
  --------------
  -- 6. dorzucenie nowych wartości
  --------------
  
  insert into bik_searchable_attr_val (node_id, attr_id, value, fixed_value, search_weight, tree_id, node_kind_id)
  select avt.node_id, avt.attr_id, avt.value, dbo.fn_normalize_text_for_fts(avt.value), avt.search_weight, avt.tree_id, avt.node_kind_id
  from @attrValsTab avt left join bik_searchable_attr_val av on
  avt.attr_id = av.attr_id and avt.node_id = av.node_id
  where av.id is null and avt.value is not null

  declare @insRowCnt int = @@rowcount

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po insert, row count: ' +
      cast(@insRowCnt as varchar(20)) + ', KONIEC!'
  end
  
  -- select * from @attrValsTab
  
  --declare @sql varchar(max) = 'select ' + @nodeIdCol + ', ' + @normalCols + ', ' + @identCols + ' from ' + @source
  --print @sql
  
  --select * from @attrNamesTab
  --select * from @attrNodeKindsTab ankt inner join bik_node_kind nk on ankt.node_kind_id = nk.id
end
go

-------------------------------------------------------------------------------------------
------------------------------------
------------------------------------
-- kod definiujący drzewo wyszukiwarki (TreeOfTrees inaczej Modrzew)
-- poprzednia wersja w pliku "alter db for v1.0.97.1 bf.sql"
-- poniżej wyrzucany jest węzeł dla kindy Keyword w zakładce Glossary
-- '$Glossary', '@', '&Keyword' - wypada ta linia

delete from bik_node where tree_id = dbo.fn_tree_id_by_code('TreeOfTrees')
go

exec sp_add_menu_node null, 'Mój BIKS', '#MyBIKS'
exec sp_add_menu_node null, 'Biblioteka', 'metadata'
exec sp_add_menu_node 'metadata', 'SAP BO', 'sapbo'
exec sp_add_menu_node 'sapbo', '@', '$Reports'
exec sp_add_menu_node 'sapbo', '@', '$ObjectUniverses'
exec sp_add_menu_node 'sapbo', '@', '$Connections'
exec sp_add_menu_node 'metadata', '@', '$Teradata'
exec sp_add_menu_node null, '@', '$Glossary'
exec sp_add_menu_node null, 'Kategoryzacje', '@Taxonomy'
exec sp_add_menu_node null, 'Dokumenty', '$Documents'
exec sp_add_menu_node null, 'Blogi', '$Blogs'
exec sp_add_menu_node null, 'Społeczność BI', '$Users'
exec sp_add_menu_node null, 'Baza Wiedzy', '$Articles'
exec sp_add_menu_node null, 'Admin', '#Admin'
exec sp_add_menu_node null, 'Szukaj', '#Search'

exec sp_add_menu_node '$Teradata', '@', '&TeradataOwner'
exec sp_add_menu_node '&TeradataOwner', '@', '&TeradataSchema'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataView'
exec sp_add_menu_node '&TeradataView', '@', '&TeradataColumn'
exec sp_add_menu_node '@Taxonomy', '@', '&TaxonomyEntity'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataTable'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumn'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumnPK'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumnIDX'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataProcedure'
exec sp_add_menu_node '$Reports', '@', '&ReportFolder'
exec sp_add_menu_node '&ReportFolder', '@', '&Webi'
exec sp_add_menu_node '&ReportFolder', '@', '&FullClient'
exec sp_add_menu_node '&Webi', '@', '&ReportQuery'
exec sp_add_menu_node '&ReportQuery', '@', '&Dimension$Reports'
exec sp_add_menu_node '&ReportQuery', '@', '&Measure'
exec sp_add_menu_node '&ReportQuery', '@', '&Detail'
exec sp_add_menu_node '&ReportFolder', '@', '&Flash'
exec sp_add_menu_node '&ReportFolder', '@', '&Excel'
exec sp_add_menu_node '&ReportFolder', '@', '&Hyperlink'
exec sp_add_menu_node '&ReportFolder', '@', '&Powerpoint'
exec sp_add_menu_node '&ReportFolder', '@', '&Pdf'
exec sp_add_menu_node '&ReportFolder', '@', '&CrystalReport'
exec sp_add_menu_node '$ObjectUniverses', '@', '&UniversesFolder'
exec sp_add_menu_node '&UniversesFolder', '@', '&Universe'

exec sp_add_menu_node '&Universe', '@', '&UniverseDerivedTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseAliasTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseTable'

exec sp_add_menu_node '&Universe', '@', '&UniverseClass'
exec sp_add_menu_node '&UniverseClass', '@', '&Dimension'
exec sp_add_menu_node '&UniverseClass', '@', '&Measure'
exec sp_add_menu_node '&Dimension', '@', '&Detail'
exec sp_add_menu_node '&UniverseClass', '@', '&Filter'

exec sp_add_menu_node '$Connections', '@', '&ConnectionNetworkFolder'
exec sp_add_menu_node '&ConnectionNetworkFolder', '@', '&ConnectionEngineFolder'
exec sp_add_menu_node '&ConnectionEngineFolder', '@', '&DataConnection'

exec sp_add_menu_node '$Documents', '@', '&DocumentsFolder'
exec sp_add_menu_node '&DocumentsFolder', '@', '&Document'

exec sp_add_menu_node '$Users', '@', '&UsersGroup'
exec sp_add_menu_node '&UsersGroup', '@', '&User'
exec sp_add_menu_node '$Blogs', '@', '&Blog'

exec sp_add_menu_node '$Articles', '@', '&ArticleFolder'
exec sp_add_menu_node '&ArticleFolder', '@', '&Article'

--exec sp_add_menu_node '$Glossary', '@', '&Keyword'

exec sp_add_menu_node '$Glossary', '@', '&GlossaryCategory'

exec sp_add_menu_node '&GlossaryCategory', '@', '&Glossary'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryNotCorpo'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryVersion'

exec sp_add_menu_node '&Blog', '@', '&BlogEntry'
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go


----------------------------------
----------------------------------
----------------------------------

exec sp_verticalize_node_attrs null
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_update_searchable_attrs_weight_for_node') and type in (N'P', N'PC'))
drop procedure dbo.sp_update_searchable_attrs_weight_for_node
go

create procedure dbo.sp_update_searchable_attrs_weight_for_node(@node_id int)
as
begin
  declare @filterTxt varchar(1000) = 'n.id = ' + cast(@node_id as varchar(20))
  exec sp_verticalize_node_attrs_inner @filterTxt
end
go


----------------------------------
----------------------------------
----------------------------------

exec sp_update_version '1.0.98.2', '1.0.98.3';
go
