/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

/**
 *
 * @author tflorczak
 */
public interface IBean4TreeExtractor<BEAN, IDT> {

    public IDT extractId(BEAN b);

    public IDT extractParentId(BEAN b);

    public String extractName(BEAN b);

    public boolean extractHasNoChildren(BEAN b);

    public int extractVisualOrder(BEAN b);

    public boolean extractIsFolder(BEAN b);

    public String extractBranchIds(BEAN b);
}
