/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author tflorczak
 */
public class DataQualityTestBean implements Serializable {

    public Integer id;
    public Integer testNodeId;
    public String name;
    public String description;
    public String type;
    public int isActive;
    public int isDeleted;
    // conn params
    public String sourceName;
    public String serverName;
    public String databaseName;
    public String resultFileName;
    public Integer procedureNodeId;
    public Integer errorProcedureNodeId;
    public Integer databaseNodeId;
    public String procedureName;
    public String errorProcedureName;
    public String sqlText;
    public String errorSqlText;
    public List<DQMMultiSourceBean> multiSources;
    public Integer connectionId;
    public String connectionName;
//    public ConnectionParametersDBServerBean connParams;
    // attrs
    public String longName;
    public String benchmarkDefinition;
    public String samplingMethod;
    public String additionalInformation;
    public String dataScope;
    public Double errorThreshold;
    // daty ustawiamy z automatu
    public Date createDate;
    public Date modifyDate;
    public Date activateDate;
    public Date deactivateDate;
    // z DQC
    public Date suspendDate;
    public String expectedResult;
}
