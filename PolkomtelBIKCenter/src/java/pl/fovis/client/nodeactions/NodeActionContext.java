/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.Map;
import pl.fovis.client.bikpages.IBikTreePage;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.foxygwtcommons.treeandlist.ITreeGridForAction;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;

/**
 *
 * @author wezyr
 */
public class NodeActionContext implements INodeActionContext {

    protected TreeNodeBean node;
//    protected int treeId;
    protected INodeClipboard clipboard;
    protected IBikTreePage bikPage;
    protected EntityDetailsDataBean data;

    public NodeActionContext(TreeNodeBean node, IBikTreePage bikPage,
            EntityDetailsDataBean data) {
        this.node = node;
        this.bikPage = bikPage;
//        this.treeId = bikPage.getTreeId();
        this.clipboard = bikPage.getNodeClipboard();
        this.data = data;
    }

    @Override
    public TreeNodeBean getNode() {
        return node;
    }

    @Override
    public INodeClipboard getClipboard() {
        return clipboard;
    }

    @Override
    public void refreshAndRedisplayData() {
        bikPage.activatePage(true, null, false);
    }

    @Override
    public void redisplayData() {
        bikPage.activatePage(false, null, true);
    }

    @Override
    public void refreshRedisplayDataAndSelectRecord(Integer nodeId/*, boolean expand*/) {
        //bikPage.refreshRedisplayDataAndSelectRecord(nodeId/*, expand*/);
        bikPage.activatePage(true, nodeId, false);
    }

    @Override
    public int getTreeId() {
        return bikPage.getTreeBean().id;
    }

    @Override
    public String getTreeCode() {
        return bikPage.getTreeBean().code;
    }

    @Override
    public Integer getSelectedNodeId() {
        return bikPage.getSelectedNodeId();
    }

    @Override
    public EntityDetailsDataBean getNodeData() {
        return data;
    }

    @Override
    public void addTreeGridNode(TreeNodeBean node) {
        bikPage.getBikTree().addNode(node);
    }
//
//    public void setTreeGridNodeName(int nodeId, String name) {
//        bikPage.bikTree.getTreeGrid().setNodeName(nodeId, name);
//    }
//
//    public void deleteTreeGridNode(int nodeId) {
//        bikPage.bikTree.getTreeGrid().deleteNode(nodeId);
//    }
//
//    public void markAsCutTreeGridNode(int nodeId) {
//        bikPage.bikTree.getTreeGrid().markAsCutNode(nodeId);
//    }
//
//    public void markAsUncutTreeGridNode(int nodeId) {
//        bikPage.bikTree.getTreeGrid().markAsUncutNode(nodeId);
//    }
//
//    public
//
//    public void pasteTreeGridNode (int nodeId, int srcNodeId, boolean isCut) {
//        bikPage.bikTree.getTreeGrid().pasteNode(nodeId, srcNodeId, isCut);
//    }
////    public BaseBIKTreeWidget getBikTree() {
////        return bikPage.bikTree;
////    }
////
////    public IFoxyTreeGrid getTreeGrid() {
////        return bikPage.bikTree.getTreeGrid();
////    }
//
//    public int getChildCount(Integer nodeId) {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }
//
//    public int getIndexInSiblings(int nodeId) {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }

    @Override
    public ITreeGridForAction<Integer> getTreeGrid() {
        return bikPage.getBikTree().getTreeGrid();
    }

    @Override
    public ITreeRowAndBeanStorage<Integer, Map<String, Object>, TreeNodeBean> getTreeStorage() {
        return bikPage.getBikTree().getTreeStorage();
    }

    @Override
    public TreeNodeBean getCurrentNodeBean() {
        return bikPage.getBikTree().getCurrentNodeBean();
    }
}
