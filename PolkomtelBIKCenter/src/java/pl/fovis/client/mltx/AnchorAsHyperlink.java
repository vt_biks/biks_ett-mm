/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.mltx;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Anchor;

/**
 *
 * @author pmielanczuk
 */
public class AnchorAsHyperlink extends Anchor {

    public AnchorAsHyperlink(String text, final String targetHistoryToken) {
        super(text);
        addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                History.newItem(targetHistoryToken);
            }
        });
    }
}
