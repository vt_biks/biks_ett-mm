set nocount on

print cast(sysdatetime() as varchar(23)) + ': start'

        select
        bn.node_kind_id, bnk.caption,
        count(*) as cnt
        from bik_node bn
        inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
        where bn.is_deleted = 0 and (bn.linked_node_id is null or bn.disable_linked_subtree <> 0)
        and bn.tree_id in --        
        (1,2) --         (1,2,3,4,8,12,15,14,16,6,7,13,18)
        and bn.node_kind_id in --        (56,39) --        
        (14,42,43,10,62,61,40,54,55,9,56,53,41,39)
        group by
        bn.node_kind_id, bnk.caption
        order by bnk.caption

print cast(sysdatetime() as varchar(23)) + ': koniec'

--------------------------------------------------------
--------------------------------------------------------
--------------------------------------------------------

print cast(sysdatetime() as varchar(23)) + ': start'

select av.node_kind_id, bnk.caption, count(*) as cnt
from 
containstable(bik_searchable_attr_val, *, '"id_konto"') sr inner loop 
join
bik_searchable_attr_val av with (index(PK_bik_searchable_attr_val)) 
on av.id = sr.[key] inner join bik_node_kind bnk on av.node_kind_id = bnk.id
where    --and
        av.tree_id in -- (1,2) -- 
        (1,2,3,4,8,12,15,14,16,6,7,13,18)
         and 
        av.node_kind_id in -- (56,39) -- 
        (14,42,43,10,62,61,40,54,55,9,56,53,41,39)
        --and case when av.value like '%id[_]konto%' then 1 else 0 end = 1
        and av.value like '%id[_]konto%'
group by av.node_kind_id, bnk.caption

print cast(sysdatetime() as varchar(23)) + ': koniec'
