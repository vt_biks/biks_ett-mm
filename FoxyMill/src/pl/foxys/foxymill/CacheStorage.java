/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill;

import commonlib.LameUtils;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author wezyr
 */
public class CacheStorage {

    private static final boolean isLogMsgEnabled = false;
    private Map<Object, CacheItem> cacheMap = new HashMap<Object, CacheItem>();
    private Map<Object, Collection<Object>> keyToSubKeys = new HashMap<Object, Collection<Object>>();
    private Map<Object, Set<Object>> subKeyToKeys = new HashMap<Object, Set<Object>>();

    public CacheStorage() {
        if (isLogMsgEnabled) {
            logMsg("constructor CacheStorage()");
        }
    }

    private void logMsg(String msg) {
        if (!isLogMsgEnabled) {
            return;
        }
        System.out.println("#CacheStorage@" + LameUtils.getIdentityHashCode(this) + "#" +
                //"(keys=" + cacheMap.keySet() + ")# " +
                msg);
    }

    public CacheItem getItem(Object key) {
        //if (true) return null;
        CacheItem res = cacheMap.get(key);
        if (isLogMsgEnabled) {
            logMsg("getItem(" + key + "): hasItem? " + (res != null));
        }
        return res;
    }

    public void setItem(Object key, CacheItem item) {
        //cacheMap.put(key, item);
        if (isLogMsgEnabled) {
            logMsg("setItem(" + key + ")");
        }
        cacheMap.put(key, item);
    }

    public void invalidateItem(Object key) {
        CacheItem oldItem = cacheMap.remove(key);
        if (isLogMsgEnabled) {
            logMsg("invalidateItem(" + key + "), had item? " + (oldItem != null));
        }
        setCacheSubKeys(key, null);
    }

    public void invalidateSubItem(Object subKey) {
        Set<Object> keys = subKeyToKeys.remove(subKey);
        if (isLogMsgEnabled) {
            logMsg("invalidateSubItem(" + subKey + "): keys=" + keys);
        }
        if (keys != null) {
            for (Object key : keys) {
                invalidateItem(key);
            }
        }
    }

    public void setCacheSubKeys(Object key, Collection<Object> subKeys) {
        if (isLogMsgEnabled) {
            logMsg("setCacheSubKeys(" + key + "," + subKeys + ")");
        }
        Collection<Object> oldSubKeys = keyToSubKeys.get(key);
        if (oldSubKeys != null) {
            for (Object oldSubKey : oldSubKeys) {
                Set<Object> keys = subKeyToKeys.get(oldSubKey);
                if (keys != null) {
                    keys.remove(key);
                }
            }
        }
        if (subKeys != null) {
            for (Object newSubKey : subKeys) {
                Set<Object> keys = subKeyToKeys.get(newSubKey);
                if (keys == null) {
                    keys = new HashSet<Object>();
                    subKeyToKeys.put(newSubKey, keys);
                }
                keys.add(key);
            }
        }
        keyToSubKeys.put(key, subKeys);
    }
}
