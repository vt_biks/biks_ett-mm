/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.widgets.sbavc;

import simplelib.IDynamicClassCreator;

/**
 *
 * @author tflorczak
 */
public interface ISBAVCWidgetCreator extends IDynamicClassCreator<ISearchByAttrValueCriteriaWidget> {

}
