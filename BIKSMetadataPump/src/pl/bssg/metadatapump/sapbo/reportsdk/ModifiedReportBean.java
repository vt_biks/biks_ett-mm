/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.bssg.metadatapump.sapbo.reportsdk;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class ModifiedReportBean implements Serializable {

    public int id;
    public String name;
    public Date modified;

    public ModifiedReportBean() {
        
    }

    public ModifiedReportBean(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public ModifiedReportBean(int id, String name, Date modified) {
        this.id = id;
        this.name = name;
        this.modified = modified;
    }

    public ModifiedReportBean(String name, Date modified) {
        this.name = name;
        this.modified = modified;
    }

}
