/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.searchservice;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.lucene.document.Document;
import org.apache.tika.Tika;
import pl.bssg.searchengine.IDocCrawler;
import pl.bssg.searchengine.SearchUtils;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog.AttributeType;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;
import simplelib.SimpleDateUtils;

/**
 *
 * @author ctran
 */
public class BiksDocCrawler implements IDocCrawler {

//    private static final ILameLogger logger = LameLoggerFactory.getLogger(BiksDocCrawler.class);
    protected INamedSqlsDAO adhocDao;
    protected Date lastIndexedDate;
    protected Date actualDate;
    protected List<IDataProvider> dataProviders = new ArrayList<IDataProvider>();
    protected String dirForUpload;
    protected Tika tika = new Tika();
    protected Integer maxFileSize = 100000000;
    protected Set<String> fileExts = new HashSet<String>();
    protected boolean extractAllFileTypes = false;

    public BiksDocCrawler(INamedSqlsDAO adhocDao, String dirForUpload) {
        this.adhocDao = adhocDao;
        this.dirForUpload = dirForUpload;

        addDataProvider(createDeletedNodeDataProvider());
        addDataProvider(createNodeDataProvider());

        Integer maxFileSizeAppProp = BaseUtils.tryParseInteger(BeanConnectionUtils.<String>execNamedQuerySingleVal(adhocDao, "getAppPropValue", true, null, "fileContentsExtractor.maxFileSize"));
        if (maxFileSizeAppProp != null) {
            maxFileSize = maxFileSizeAppProp;
        }
        String fileExts = BeanConnectionUtils.<String>execNamedQuerySingleVal(adhocDao, "getAppPropValue", true, null, "fileContentsExtractor.tikaExts");
        if ("*".equals(fileExts)) {
            extractAllFileTypes = true;
        } else {
            this.fileExts.addAll(BaseUtils.splitBySep(fileExts, "|"));
        }
    }

    @Override
    public List<Document> next() {
        for (IDataProvider dataProvider : dataProviders) {
            List<Document> res = dataProvider.next();
            if (!BaseUtils.isCollectionEmpty(res)) {
                return res;
            }
        }
        return new ArrayList<Document>();
    }

    @Override
    public void setLastIndexedDate(Date lastIndexedDate) {
        this.lastIndexedDate = lastIndexedDate;
    }

    @Override
    public void setActualDate(Date actualDate) {
        this.actualDate = actualDate;
    }

    private void addDataProvider(IDataProvider dataProvider) {
        if (dataProvider == null) {
            throw new IllegalArgumentException("dataProvider is null");
        }
        dataProviders.add(dataProvider);
    }

    private IDataProvider createDeletedNodeDataProvider() {
        return new DataProviderBase(adhocDao, "getDeletedNodeDataProvider") {

            @Override
            protected void generateDocumentsFromContentAndAddToIndexingList(Map<String, Object> content, List<Document> indexingList) {
                String nodeIdStr = BaseUtils.safeToString(content.get("node_id"));
                Document doc = SearchUtils.createDocument(nodeIdStr);
                SearchUtils.markAsDeleted(doc, true);
                indexingList.add(doc);
            }
        };
    }

    private IDataProvider createNodeDataProvider() {
        return new DataProviderBase(adhocDao, "getNodeInfoToIndex") {

            @Override
            protected void generateDocumentsFromContentAndAddToIndexingList(Map<String, Object> content, List<Document> indexingList) {
                String nodeIdStr = BaseUtils.safeToString(content.get("node_id"));

                Document doc = SearchUtils.createDocument(nodeIdStr);
                SearchUtils.markAsDeleted(doc, false);

                String treeId = BaseUtils.safeToStringNotNull(content.get("tree_id"));
                SearchUtils.addTextField(doc, BiksSearchConstant.BIK_TREE_ID, treeId);

//                String name = BaseUtils.safeToStringNotNull(content.get("name")).trim().toLowerCase();
                String name = BaseUtils.safeToStringNotNull(content.get("name")).trim();
                SearchUtils.addTextField(doc, BiksSearchConstant.BIK_NODE_NAME, name);

//                String descr = BaseUtils.safeToStringNotNull(content.get("descr")).trim().toLowerCase();
                String descr = BaseUtils.safeToStringNotNull(content.get("descr")).trim();
                SearchUtils.addTextField(doc, BiksSearchConstant.BIK_NODE_DESC, descr);

                List<Map<String, Object>> sysAttrs = adhocDao.execNamedQueryCFN("getSystemAttribute", FieldNameConversion.ToLowerCase, BaseUtils.tryParseInteger(nodeIdStr));
                addAttribute2Doc(doc, sysAttrs);
                List<Map<String, Object>> attrs = adhocDao.execNamedQueryCFN("getAttribute", FieldNameConversion.ToLowerCase, BaseUtils.tryParseInteger(nodeIdStr));
                addAttribute2Doc(doc, attrs);

//                String attrValueStr = BaseUtils.safeToStringNotNull(content.get("node_attribute_value")).trim().toLowerCase();
//                SearchUtils.addTextField(doc, BiksSearchConstant.BIK_ATTRIBUTE_LINKED, attrValueStr);
//                String joinedObjAttrValueStr = BaseUtils.safeToStringNotNull(content.get("joined_obj_attribute_value")).trim().toLowerCase();
                String joinedObjAttrValueStr = BaseUtils.safeToStringNotNull(content.get("joined_obj_attribute_value")).trim();
                SearchUtils.addTextField(doc, BiksSearchConstant.BIK_JOINED_OBJ_ATTRIBUTE_LINKED, joinedObjAttrValueStr);

                List<String> fileNames = BaseUtils.splitBySep(BaseUtils.safeToStringNotNull(content.get("href")), "|");
                for (String fileName : fileNames) {
                    if (!BaseUtils.isStrEmptyOrWhiteSpace(fileName)) {
                        File file = new File(BaseUtils.ensureDirSepPostfix(dirForUpload) + fileName);
                        if (!file.exists()) {
                            System.out.println("indeksowanie: Brak pliku node_id = " + nodeIdStr);
                        } else if (file.length() < maxFileSize) {
                            try {
//                                String fileContent = tika.parseToString(file).trim().toLowerCase();
                                String fileContent = tika.parseToString(file).trim();
                                SearchUtils.addTextField(doc, BiksSearchConstant.BIK_FILE_CONTENT, fileContent);
                            } catch (Throwable ex) {
                                System.out.println("Plik " + fileName + " node_id=" + nodeIdStr + "został pominięty. Exception: " + ex);
                                ex.printStackTrace();
                            }
                        }
                    }
                }
                indexingList.add(doc);
            }

            private void addAttribute2Doc(Document doc, List<Map<String, Object>> attr) {
                for (Map<String, Object> m : attr) {
                    String attrName = (String) m.get("name");
                    attrName = "@" + attrName.replace(" ", "_").toLowerCase();
                    String attrType = (String) m.get("type_attr");
                    String value = (String) m.get("value");
                    boolean isNumber = ((Integer) m.get("display_as_number")) == 1 ? true : false;
                    if (isNumber) {
                        Object number = BaseUtils.tryParseNumber(value);
                        if (number != null) {
                            value = SearchUtils.convertDouble2String(SearchUtils.convertNumberAsDouble(number));
                        }
                    } else if (AttributeType.data.name().equals(attrType) || AttributeType.datetime.name().equals(attrType)) {
                        Date date = SimpleDateUtils.parseFromCanonicalString(value);
                        if (date != null) {
                            value = SearchUtils.convertDouble2String(1.0 * date.getTime());
                        }
                    }
                    SearchUtils.addTextField(doc, attrName, value);
//                    if ("@ldap.miejscowość".equals(attrName)) {
//                        System.out.println("Indexed miejscowosc: " + value);
//                    }
                }
            }
        };
    }

    @Override
    public void restartDataProviders() {
        for (IDataProvider dataProvider : dataProviders) {
            dataProvider.restart(lastIndexedDate, actualDate);
        }
    }
}
