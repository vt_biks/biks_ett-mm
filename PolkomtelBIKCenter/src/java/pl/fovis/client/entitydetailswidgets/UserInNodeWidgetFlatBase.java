/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.IJoinedObjSelectionManager;
import pl.fovis.client.dialogs.RolesDialog;
import pl.fovis.client.dialogs.TreeSelectorForJoinedObjectsDialogNew;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.JoinTypeRoles;
import pl.fovis.common.JoinedObjBasicInfoBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.UseInNodeSelectionBean;
import pl.fovis.common.UserRoleInNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.NewLookUtils;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.LameBag;

/**
 *
 * @author beata z drzewka użytkownicy
 */
public abstract class UserInNodeWidgetFlatBase/*<T>*/ extends EntityDetailsTabPaginationBase<UserRoleInNodeBean> {

    protected Map<Integer, String> idsAndNamesFromBranches = new LinkedHashMap<Integer, String>();

    @Override
    protected NodeAwareBeanExtractorBase<UserRoleInNodeBean> getBeanExtractor() {
        return new UserRoleInNodeBeanExtractor(hasAddJoinedButton, nodeId, idsAndNamesFromBranches);
    }

    @Override
    protected List<UserRoleInNodeBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
        this.idsAndNamesFromBranches = data.userRolesInNodes != null ? data.userRolesInNodes.v2 : null;
        Set<String> additionalsTreeKind = getAdditionalsTreeKind();
        String treeKindForSelected = getTreeKindForSelected();
        if (additionalsTreeKind == null) {
            return cddb.userRoleInNodeByTreeKind.get(treeKindForSelected);
        }
        additionalsTreeKind.add(treeKindForSelected);
        List<UserRoleInNodeBean> allItems = new ArrayList<UserRoleInNodeBean>();
        for (String kind : additionalsTreeKind) {
            List<UserRoleInNodeBean> lst = cddb.userRoleInNodeByTreeKind.get(kind);
            if (lst != null) {
                allItems.addAll(lst);
            }
        }
        return allItems;
    }

    protected IJoinedObjSelectionManager<Integer> getJoinedObjSelectionManager(final int roleForNodeId, final JoinTypeRoles joinTypeRole) {

        return new IJoinedObjSelectionManager<Integer>() {
            public JoinedObjBasicInfoBean getInfoForJoinedNodeById(Integer dstNodeId) {

                UserRoleInNodeBean u = cddb.userRoleInNodeByDstNodeId.get(dstNodeId);
                JoinedObjBasicInfoBean jb = new JoinedObjBasicInfoBean();
                //jb.srcId = getNodeId();
                jb.srcId = getData().node.linkedNodeId != null ? getData().node.linkedNodeId : getData().node.id;
                jb.type = false;
                jb.dstId = u.nodeId;
                jb.branchIds = u.branchIds;
                jb.inheritToDescendants = u.inheritToDescendants;
                return jb;
            }

            public Collection<Integer> getJoinedObjIds() {
                List<Integer> ids = new ArrayList<Integer>();
                if (!BaseUtils.isCollectionEmpty(items)) {
                    for (UserRoleInNodeBean item : items) {
                        //if (item.roleForNodeId == param.v1.roleForNodeId && item.isAuxiliary == (param.v1.joinTypeRole == JoinTypeRoles.Auxiliary)) {
                        if (item.roleForNodeId == roleForNodeId && item.isAuxiliary == (joinTypeRole == JoinTypeRoles.Auxiliary)) {
                            ids.add(extractor.getNodeId(item));
                        }
                    }
                }
                return ids;
            }

            public void refreshJoinedNodes(Integer forNodeId) {
                if (nodeId == forNodeId) {
                    refreshAndRedisplayData();
                }
            }
        };

    }

    protected void buildAndShowTreeSelectorForObjects(final Set<String> treeKinds,
            final String userName, final UseInNodeSelectionBean param, final String treeCode) {

        TreeSelectorForJoinedObjectsDialogNew treeSelectorForJoinedObjectsDialogNew = new TreeSelectorForJoinedObjectsDialogNew() {

            @Override
            protected Widget setOptionalWigetAfterHeadLabel() {
                PushButton changeRoleBtn = new PushButton(I18n.zmienRole.get() /* I18N:  */);
                NewLookUtils.makeCustomPushButton(changeRoleBtn);

                changeRoleBtn.addClickHandler(new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {

                        new RolesDialog().buildAndShowDialog(nodeId,/* treeKinds,*/ new IParametrizedContinuation<UseInNodeSelectionBean>() {

                                    @Override
                                    public void doIt(final UseInNodeSelectionBean param) {
                                        //zapisuje zwrocone parametry
                                        roleId = param.roleForNodeId;
                                        roleName = param.roleForNodeName;
                                        setHeadLabel();
                                        alreadyJoinedObjIds.clear();
                                        ancestorIdsOfJoinedObjs.clear();
                                        alreadyJoinedManager = getJoinedObjSelectionManager(roleId, param.joinTypeRole);

                                        buildAlreadyJoinedObjIds();
                                        fetchData();
                                    }
                                });

                    }
                });

                return changeRoleBtn;
            }

            @Override
            protected void refreshAndRedisplayData() {
                BIKClientSingletons.invalidateTabData(treeCode);
            }
        };

        treeSelectorForJoinedObjectsDialogNew.buildAndShowDialog(nodeId, userName,
                treeKinds, null, null,
                new IParametrizedContinuation<TreeNodeBean>() {
                    public void doIt(TreeNodeBean param) {
                    }
                },
                getJoinedObjSelectionManager(param.roleForNodeId, param.joinTypeRole), param);
    }

    protected BikCustomButton createAddJoinedButton(final Set<String> treeKind) {
        BikCustomButton addJoinedObjBtn = new BikCustomButton(I18n.dodajPowiazanie.get() /* I18N:  */, "Attachs" /* I18N: no */ + "-addAttachEntry",
                new IContinuation() {
                    public void doIt() {

                        TreeNodeBean currentNode = ctx.getCurrentNodeBean();
                        final String userName = currentNode.name;
                        final String treeCode = currentNode.treeCode;

                        //jeśli uzytkownik nie posiada przypisanej roli
                        if (currentNode == null || currentNode.id == nodeId
                        || BaseUtils.safeEquals(currentNode.treeKind, BIKGWTConstants.TREE_KIND_USERS)) {
                            new RolesDialog().buildAndShowDialog(nodeId,/* null,*/ new IParametrizedContinuation<UseInNodeSelectionBean>() {
                                        public void doIt(final UseInNodeSelectionBean param) {
                                            buildAndShowTreeSelectorForObjects(treeKind, userName, param, treeCode);
                                        }
                                    });
                        } else {
                            //pobranie id roli
                            TreeNodeBean roleNode = ctx.getTreeStorage().getBeanById(currentNode.parentNodeId);
                            UseInNodeSelectionBean unb = new UseInNodeSelectionBean();
                            //unb.roleForNodeId = roleNode.id;
                            unb.roleForNodeId = BaseUtils.tryParseInt(roleNode.objId);
                            unb.roleForNodeName = roleNode.name;

                            buildAndShowTreeSelectorForObjects(treeKind, userName, unb, treeCode);
                        }
                    }
                });

        addWidgetVisibledByState(addJoinedObjBtn);
        return addJoinedObjBtn;
    }

    @Override
    public boolean hasEditableControls() {
        return isInUserTree(getNodeTreeCode()) && isNodeKindCode(getNodeKindCode(), BIKGWTConstants.NODE_KIND_USER);
    }

    @Override
    protected boolean isChangedViewDescriptionInJoinedPanelVisible() {
        return true;
    }

    private HorizontalPanel filters;
    private FlowPanel checkBoxContainer;
    protected List<CheckBox> checkboxes = new ArrayList<CheckBox>();
    protected Set<String> activeKinds;

    protected void addCheckbox(String name, String caption, boolean active, final IContinuation contWhenChanged) {
        final CheckBox cb = new CheckBox(caption, true);
        cb.setStyleName("Filters-Checkbox");
        cb.setValue(active);
        cb.setName(name);
        //tymczasowa szerokość wstawiona tutaj.
        //cb.setWidth("155px");
        cb.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent ce) {
                contWhenChanged.doIt();
            }
        });
        checkboxes.add(cb);
    }

    protected void finalizeCheckboxes() {
        checkBoxContainer.clear();
        for (CheckBox cb : checkboxes) {
            checkBoxContainer.add(cb);
        }
    }

    protected int getFilterCnt() {
        return checkboxes.size();
    }

    protected boolean isFilterOn(int idx) {
        return checkboxes.get(idx).getValue();
    }

    protected String getFilterName(int idx) {
        return checkboxes.get(idx).getName();
    }

    protected void setFilterActive(int idx, boolean active) {
        checkboxes.get(idx).setValue(active);
    }

    protected void clearCheckboxes() {
        checkboxes.clear();
    }

    @Override
    protected void dataChanged() {
        recreateFilterCheckboxes();
        super.dataChanged();
    }

//    @Override
    protected void recreateFilterCheckboxes() {
//        if (ClientDiagMsgs.isShowDiagEnabled(BIKGWTConstants.DIAG_MSG_KIND$WW_joinedObjs)) {
//            ClientDiagMsgs.showDiagMsg(BIKGWTConstants.DIAG_MSG_KIND$WW_joinedObjs, "refreshDescendingNodesData: start, nodeId=" + getNodeId());
//        }

        LameBag<String> counts = new LameBag<String>();
        List<UserRoleInNodeBean> joinedObjsForMetadata = items;//getjoinedObjsForTreeKind();
        //new ArrayList<JoinedObjBean>();
//        for (JoinedObjBean joinedObjBean : data.joinedObjs) {
//            if (joinedObjBean.treeKind.equals(BIKGWTConstants.TREE_KIND_METADATA)) {
//                joinedObjsForMetadata.add(joinedObjBean);
//            }
//        }
        if (joinedObjsForMetadata != null) {
            for (UserRoleInNodeBean job : joinedObjsForMetadata) {
                counts.add(job.nodeKindCode);
            }
        }
        clearCheckboxes();
        addCheckbox("*", "<b>" + I18n.wszystko.get() /* I18N:  */ + "</b> (" + BaseUtils.collectionSizeFix(joinedObjsForMetadata) + ")",
                true, new IContinuation() {
                    public void doIt() {
                        boolean check = isFilterOn(0);
                        for (int i = 1; i < getFilterCnt(); i++) {
                            setFilterActive(i, check);
                        }
//                showDescendentNodes();
                        displayData();
                    }
                });

        for (String kind : counts.uniqueSet()) {

            addCheckbox(kind,
                    //"<Image style='vertical-align:middle' border='0' src='images/" + BIKClientSingletons.getNodeKindIconNameByCode(kind) + ".gif' width='16' height='16' />" //&nbsp;
                    //+ BIKClientSingletons.getNodeKindCaptionByCode(kind) + " (" + counts.getCount(kind) + ")"
                    BIKClientSingletons.getHtmlForNodeKindIconWithText(kind, null, BIKClientSingletons.getNodeKindCaptionByCode(kind) + " (" + counts.getCount(kind) + ")"), true,
                    new IContinuation() {
                        @Override
                        public void doIt() {
                            displayData();
//                            showDescendentNodes();
                        }
                    });
        }

        finalizeCheckboxes();
    }

    @Override
    protected void addOptionalFilterWidgets() {
        if (BaseUtils.collectionSizeFix(items) > 0) {
            filters = new HorizontalPanel();
            filters.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
            HTML lblFilters = new HTML(I18n.pokazTypyObiektowPowiazanych.get() /* I18N:  */ + ":");
            filters.add(lblFilters);
            filters.setWidth("100%");
            GWTUtils.setStyleAttribute(filters, "minHeight", "25px");
            filters.setCellWidth(lblFilters, "180px");
            checkBoxContainer = new FlowPanel();
            filters.add(checkBoxContainer);

            FlowPanel fpHelp = new FlowPanel();
            fpHelp.add(filters);
            fpHelp.setStyleName("RelatedObj-filters");
            mainJoinedObjVl.add(fpHelp);
            mainJoinedObjVl.setCellHeight(filters, "20px");

            recreateFilterCheckboxes();
        }
    }

    @Override
    protected boolean shouldShowItem(UserRoleInNodeBean e) {
        return activeKinds.contains(e.nodeKindCode);
    }

    @Override
    protected void prepareItemFiltering() {
        boolean allActive = true;
        activeKinds = new HashSet<String>();
        for (int i = 1; i < getFilterCnt(); i++) {
            if (isFilterOn(i)) {
                activeKinds.add(getFilterName(i));
            } else {
                allActive = false;
            }
        }
        setFilterActive(0, allActive);
    }

    @Override
    public boolean isEnabled() {
        String nodeKindCode = getNodeKindCode();
        return BIKConstants.NODE_KIND_USER.equals(nodeKindCode);
    }
}
