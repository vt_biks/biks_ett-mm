/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueBoxBase;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.bikwidgets.AttrCombobox;
import pl.fovis.client.bikwidgets.AttrDateTime;
import pl.fovis.client.bikwidgets.AttrWigetBase;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog.AttributeType;
import pl.fovis.client.entitydetailswidgets.IGridNodeAwareBeanPropsExtractor;
import pl.fovis.client.entitydetailswidgets.JoinedObjBeanExtractor;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.JoinedObjBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.trzy0.foxy.commons.FoxyCommonConsts;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author młydkowski
 */
public class TreeNodeDialog extends BaseActionOrCancelDialog {

    public static final int MAX_SCROLLPANEL_HEIGHT = 420;

    public TreeNodeDialog() {
        allowDuplicateJoinedObjs = BIKClientSingletons.allowDuplicateJoinedObjs();
    }

//    public TreeNodeDialog(boolean allowDuplicateJoinedObjs) {
//        this.allowDuplicateJoinedObjs = allowDuplicateJoinedObjs;
//    }
    private static enum ActionModeKind {

        New, Edit, Root
    };

    protected TextBox nameText;
//    protected Label status;
    private IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>> saveCont;
    private TreeNodeBean node;
    private TreeNodeBean parent;
    private ActionModeKind kind;
//    private String kind;
    protected List<String> keywords;
    private Label nodeLbl;
    private Label joinLabel;
    protected List<AttributeBean> requiredAttrs;
    protected FlexTable requiredAttrFt = new FlexTable();
    protected ScrollPanel requiredAttrsSp = new ScrollPanel(requiredAttrFt);
    protected CheckBox copyJoinedObjsCb;
    protected boolean allowDuplicateJoinedObjs = false;
    protected ScrollPanel parentJoinedObjsSp;
    protected Map<CheckBox, Integer> addingJoinedObjs = new HashMap<CheckBox, Integer>();
    protected Map<String, String> nodePath2NodeLink = new HashMap<String, String>();
    protected Map<String, AttributeBean> absMap = new HashMap<String, AttributeBean>();
    protected Map<String, Widget> mapWidget = new HashMap<String, Widget>();
    protected Map<String, List<CheckBox>> mapCbs = new HashMap<String, List<CheckBox>>();
    protected Map<String, List<RadioButton>> mapRbs = new HashMap<String, List<RadioButton>>();
    protected FlexTable table = new FlexTable();
    protected Grid gridIcons;
    protected int height = 0;
    protected String atrControlName = null;
//    protected ListBox treeKindCombo = new ListBox();
    protected int nameFieldWidth = 242;
    protected boolean isExistsTextArea = false; // Jeżeli nie ma to Enter zamyka okno, jak jest to nie!
//    protected TextBox tbDescription = new TextBox();

    @Override
    protected String getDialogCaption() {
        if (kind == ActionModeKind.Root //kind.equalsIgnoreCase(I18n.root.get() /* I18N:  */)
                ) {
//            return I18n.dodajeszNowyElementGlowny.get() /* I18N:  */;
            return I18n.dodajeszElementPod.get() /* I18N:  */ + " " + BIKClientSingletons.getNodeKindCaptionByCode(node.nodeKindCode);
        }
        if (kind == ActionModeKind.New //kind.equalsIgnoreCase(I18n.nowy2.get() /* I18N:  */)
                ) {
            return I18n.dodajeszElementPod.get() /* I18N:  */ + " " + BIKClientSingletons.getNodeKindCaptionByCode(node.nodeKindCode);
        }
//        if (kind == ActionModeKind.Edit
//                //kind.equalsIgnoreCase(I18n.edycja2.get() /* I18N:  */)
//                ) {
        return I18n.edytujeszNazweElementu.get() /* I18N:  */ + " "/*
                 //                     * + node.name + ""
                 //                     */;
//        }
//        return "zero" /* I18N: no */ + "! :" + I18n.d_noSingleLetter.get() /* I18N:  */;
    }

    private void refreshAssociationsSp() {
        if (parent != null) {
            BIKClientSingletons.getService().getJoinedObjs(parent.id, null, new StandardAsyncCallback<List<JoinedObjBean>>() {

                @Override
                public void onSuccess(final List<JoinedObjBean> result) {
                    final VerticalPanel vp = new VerticalPanel();

                    parentJoinedObjsSp.add(vp);
                    parentJoinedObjsSp.setHeight("120px");
                    final IGridNodeAwareBeanPropsExtractor extr = new JoinedObjBeanExtractor(false, parent.id);
                    Set<Integer> idsToFetch = new HashSet<Integer>();
                    for (JoinedObjBean bean : result) {

                        for (String s : BaseUtils.splitBySep(bean.branchIds, "|")) {
                            Integer id = BaseUtils.tryParseInteger(s.trim());
                            if (id != null) {
                                idsToFetch.add(id);
                            }
                        }
                    }

                    BIKClientSingletons.getService().getNodeNamesByIds(idsToFetch, new StandardAsyncCallback<Map<Integer, String>>() {
                        @Override
                        public void onSuccess(Map<Integer, String> id2Name) {
                            for (final JoinedObjBean bean : result) {
                                final HorizontalPanel hp = new HorizontalPanel();
                                CheckBox cb = new CheckBox();
                                hp.add(cb);
                                addingJoinedObjs.put(cb, bean.dstId);
                                if (extr.hasIcon()) {
                                    hp.add(BIKClientSingletons.createIconImg(extr.getIconUrl(bean), "16px", "16px", extr.getOptIconHint(bean)));
                                }

                                HTML nameWidget = new HTML();
                                String ancestorPath = "";
                                for (String s : BaseUtils.splitBySep(bean.branchIds, "|")) {
                                    Integer id = BaseUtils.tryParseInteger(s.trim());
                                    if (id != null) {
                                        ancestorPath += ("".equals(ancestorPath) ? "" : FoxyCommonConsts.RAQUO_STR_SPACED) + id2Name.get(id);
                                    }
                                }
                                String treeCode = extr.getTreeCode(bean);
                                String menuPathForTreeCode = BIKClientSingletons.getMenuPathForTreeCode(treeCode) + ancestorPath;
                                nameWidget.setTitle(menuPathForTreeCode);
                                ancestorPath = ancestorPath.substring(0, 100) + (ancestorPath.length() > 100 ? "..." : "");
                                if (BIKClientSingletons.isShowFullPathInJoinedObjsPane()) {
                                    nameWidget.setHTML(BIKClientSingletons.getTreeBeanByCode(treeCode).name + FoxyCommonConsts.RAQUO_STR_SPACED + ancestorPath);
                                } else {
                                    nameWidget.setHTML(BIKClientSingletons.getTreeBeanByCode(treeCode).name + FoxyCommonConsts.RAQUO_STR_SPACED + bean.dstName);
                                }
                                hp.add(nameWidget);
                                vp.add(hp);
                            }
                            if (result.size() == 0) {
                                table.removeRow(1);
                            }
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                            super.onFailure(caught);
                        }
                    });
                }
            });
        }
    }

    @Override
    protected String getActionButtonCaption() {
        if (kind == ActionModeKind.Root //kind.equalsIgnoreCase(I18n.root.get() /* I18N:  */)
                ) {
            return I18n.dodaj.get() /* I18N:  */;
        }
        if (kind == ActionModeKind.New //kind.equalsIgnoreCase(I18n.nowy2.get() /* I18N:  */)
                ) {
            return I18n.dodaj.get() /* I18N:  */;
        }
//        if (kind == ActionModeKind.Edit
//                //kind.equalsIgnoreCase(I18n.edycja2.get() /* I18N:  */)
//                ) {
        return I18n.zmien.get() /* I18N:  */;
//        }
//        return "zero" /* I18N: no */ + "! :" + I18n.d_noSingleLetter.get() /* I18N:  */;
    }

    protected void onEnterPressed() {
        if (!isExistsTextArea) {
            super.onEnterPressed();
        }
    }

    @Override
    protected void buildMainWidgets() {

//        status = new Label();
//        main.add(status);
//        int gridLength = allowDuplicateJoinedObjs ? 3 : 1;
        table.setCellSpacing(4);
        nodeLbl = new Label(I18n.nazwa.get() /* I18N:  */ + ":");
        nodeLbl.setStyleName("addNodeLbl");
        nodeLbl.setWidth("187px");
        nameText = new TextBox();
        nameText.setStyleName("addNode");
        nameText.setWidth(nameFieldWidth + "px");
        // height = height + 36;
        if (isNodeNameEditable()) {
            table.setWidget(0, 0, nodeLbl);
            HorizontalPanel hp = new HorizontalPanel();
            hp.add(nameText);
//            if (BIKConstants.NODE_KIND_META_MENU_ITEM.equals(node.nodeKindCode)) {
//                treeKindCombo.clear();
//                for (Entry<String, String> e : BIKClientSingletons.getTreeKindCaptionsMap().entrySet()) {
//                    treeKindCombo.addItem(e.getValue(), e.getKey());
//                }
//                treeKindCombo.addChangeHandler(new ChangeHandler() {
//
//                    @Override
//                    public void onChange(ChangeEvent event) {
//                        onSelectionTreeKindChanged();
//                    }
//
//                });
//                treeKindCombo.setItemSelected(0, true);
//                onSelectionTreeKindChanged();
//                hp.add(treeKindCombo);
//            }
            table.setWidget(0, 1, hp);
        }

        if (kind == ActionModeKind.Edit //kind.equalsIgnoreCase(I18n.edycja2.get() /* I18N:  */)
                ) {
            nameText.setText(this.node.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_BLOG_ENTRY) ? this.node.name.substring(20) : this.node.name);
        } else {
            nameText.setText(this.node.name);
        }

        //nameText.setWidth("300px");
        if (allowDuplicateJoinedObjs && !BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(node.treeKind)) {
            parentJoinedObjsSp = new ScrollPanel();
            table.setWidget(1, 0, joinLabel = new Label(I18n.powiazaniaZWyzszegoPoziomu.get()));
//            copyJoinedObjsCb.setVisible(allowDuplicateJoinedObjs && parent.linkedNodeId == null);
//            copyJoinedObjsCb.addClickHandler(new ClickHandler() {
//
//                @Override
//                public void onClick(ClickEvent event) {
//                    parentJoinedObjsSp.setVisible(copyJoinedObjsCb.getValue());
            //               }
//            });
            parentJoinedObjsSp.setVisible(true);
            table.setWidget(1, 1, parentJoinedObjsSp);
            refreshAssociationsSp();
        }
        if (!BIKConstants.TREE_KIND_META_BIKS.equals(node.treeKind)
                || BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG.equals(node.nodeKindCode)
                || BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE.equals(node.nodeKindCode)
                || BIKConstants.NODE_KIND_META_PRINT.equals(node.nodeKindCode)
                || BIKConstants.NODE_KIND_META_PRINTS_TEMPLATES.equals(node.nodeKindCode)) {
            main.add(table);
            main.setCellWidth(table, "100%");
        }
        if (!BaseUtils.isCollectionEmpty(requiredAttrs)) {
            //nameText.setWidth("99%");
            for (AttributeBean ab : requiredAttrs) {
                absMap.put(ab.atrName, ab);
            }
            refreshAttributeFt();
            requiredAttrFt.setWidth("100%");
            requiredAttrFt.setStyleName("gridJoinedObj");

            if (height > BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 57) {
                BIKClientSingletons.registerResizeSensitiveWidgetByHeight(requiredAttrsSp,
                        BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 57);
            }
            resizeRequiredAttrScrollPanel();
            main.setCellWidth(requiredAttrFt, "100%");
            main.add(requiredAttrsSp);
            requiredAttrsSp.scrollToTop();
            if (absMap.containsKey(BIKConstants.METABIKS_ATTR_ORIGINAL)
                    || absMap.containsKey(BIKConstants.METABIKS_ATTR_ATTR_NAME)
                    || absMap.containsKey(BIKConstants.METABIKS_ATTR_ATTR_CAT_NAME)) {
                nameText.setEnabled(false);
            }
        } else {
            nodeLbl.setWidth("100px");
        }
    }

//    private void onSelectionTreeKindChanged() {
//        TextBox wg = (TextBox) mapWidget.get(BIKConstants.METABIKS_ATTR_TREE_TYPE);
//        if (wg != null && BIKConstants.NODE_KIND_META_MENU_ITEM.equals(node.nodeKindCode)) {
//            wg.setValue(treeKindCombo.getSelectedValue());
//        }
//    }
    protected void resizeRequiredAttrScrollPanel() {
        if (requiredAttrFt.getOffsetHeight() != 0) {
            if (requiredAttrFt.getOffsetHeight() > MAX_SCROLLPANEL_HEIGHT) {
                requiredAttrsSp.setHeight(MAX_SCROLLPANEL_HEIGHT + "px");
            } else {
                if (!absMap.isEmpty()) {
                    requiredAttrsSp.setHeight(height + "px");
                } else {
                    requiredAttrsSp.setHeight(1 + "px");
                }
            }
            //wtf???
//            Window.alert("SP " + String.valueOf(requiredAttrsSp.getOffsetHeight()));
//            Window.alert("FT " + String.valueOf(requiredAttrFt.getOffsetHeight()));
//            Window.alert("SP w" + String.valueOf(requiredAttrsSp.getOffsetWidth()));
//            Window.alert("FT w" + String.valueOf(requiredAttrFt.getOffsetWidth()));
        } else {
            if (height > MAX_SCROLLPANEL_HEIGHT) {
                requiredAttrsSp.setHeight(MAX_SCROLLPANEL_HEIGHT + "px");
            } else {
                if (!absMap.isEmpty()) {
                    requiredAttrsSp.setHeight(height + "px");
                } else {
                    requiredAttrsSp.setHeight(1 + "px");
                }
            }
//            Window.alert("SP " + String.valueOf(requiredAttrsSp.getOffsetHeight()));
//            Window.alert("FT " + String.valueOf(requiredAttrFt.getOffsetHeight()));
//            Window.alert("SP w" + String.valueOf(requiredAttrsSp.getOffsetWidth()));
//            Window.alert("FT w" + String.valueOf(requiredAttrFt.getOffsetWidth()));
        }

        if (dialog != null) {
            recenterDialog();
        }
    }

    public void setNameFieldWidth(int attrWidth) {
        if (attrWidth >= nameFieldWidth) {
            nameFieldWidth = attrWidth;
        }
        nameText.setWidth(nameFieldWidth + "px");
    }

    protected String trimToDot(String attrName) {
        return attrName.indexOf('.') < 0 ? attrName : attrName.split("\\.")[1];
    }

    protected AttrWigetBase attributeWidget;
    protected Map<String, AttrWigetBase> awb = new HashMap<String, AttrWigetBase>();

    protected void refreshAttributeFt() {
        requiredAttrFt.clear();
        height = 0;
        int row = 0;
        for (final AttributeBean ab : absMap.values()) {
            Label nameLbl = new Label((trimToDot(ab.atrName))
                    + (!ab.isEmpty ? " *" : ""));
            HorizontalPanel attrNameNHintPanel = new HorizontalPanel();
            attrNameNHintPanel.add(BIKClientSingletons.createHintedHTML(nameLbl.getText(), null));
            requiredAttrFt.setWidget(row, 0, attrNameNHintPanel);

            String width = "185px";
//            requiredAttrFt.getCellFormatter().setWidth(row, 0, width);
//            requiredAttrFt.getCellFormatter().setWidth(row, 1, width);
            requiredAttrFt.getCellFormatter().addStyleName(row, 0, "tdPadding");
            requiredAttrFt.getCellFormatter().addStyleName(row, 1, "tdPadding");
            attrNameNHintPanel.setWidth(width);
            int attrWidth;
//            String atrLinValue = ab.atrLinValue;
//            if (atrLinValue != null) {
//                atrLinValue = ab.atrLinValue.substring(0, 100) + (ab.atrLinValue.length() > 100 ? "..." : "");
//            }
            String attrType = ab.typeAttr;
            String defaultValue = BaseUtils.safeToStringNotNull(ab.defaultValue);
            if (!BaseUtils.isStrEmptyOrWhiteSpace(defaultValue) //                    defaultValue != null
                    ) {
                ab.atrLinValue = defaultValue;
                absMap.put(ab.atrName, ab);
                defaultValue = defaultValue.substring(0, 100) + (defaultValue.length() > 100 ? "..." : "");
            }
            requiredAttrFt.setWidget(row, 1, new Label(defaultValue));

            if (AttributeType.shortText.name().equals(attrType)
                    || AttributeType.hyperlinkOut.name().equals(attrType)) {
                TextBox tbDescription = new TextBox();
                tbDescription.setValue(defaultValue);
                attrWidth = 242;
                //dla okienek metamodelu
                if (ab.atrName.startsWith("metaBiks")) {
                    tbDescription.setWidth("792px");
                } else {
                    tbDescription.setWidth(attrWidth + "px");
                }
                setNameFieldWidth(attrWidth - 6);
                tbDescription.setStyleName("addNode");
                requiredAttrFt.setWidget(row, 1, tbDescription);
//                tbDescription.setText(atrLinValue);
                mapWidget.put(ab.atrName, tbDescription);
                height = height + 34;
            } else if (AttributeType.calculation.name().equals(attrType)) {
                // nie ustawiamy wartości, wartość się sama policzy
                Label calcLbl = new Label(I18n.atrybutWiliczanyAutomatycznie.get());
                attrWidth = 242;
                calcLbl.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth);
                requiredAttrFt.setWidget(row, 1, calcLbl);
                height = height + 18;
            } else if (AttributeType.javascript.name().equals(attrType)) {
                // nie ustawiamy wartości, wartość się sama policzy
                Label jsLbl = new Label(I18n.javascript.get());
                attrWidth = 242;
                jsLbl.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth);
                requiredAttrFt.setWidget(row, 1, jsLbl);
                height = height + 18;
            } else if (AttributeType.data.name().equals(attrType)) {
                DateBox datePicker = new DateBox();
                datePicker.setStyleName("addAttr");
                BIKCenterUtils.setupDateBoxWithLimit(datePicker, ab.valueOpt);
                requiredAttrFt.setWidget(row, 1, datePicker);
                Date defaultValueDate = SimpleDateUtils.parseFromCanonicalString(defaultValue);
                if (defaultValueDate != null && SimpleDateUtils.getYear(defaultValueDate) > 1900) {
                    datePicker.setValue(defaultValueDate);
                    datePicker.getDatePicker().setCurrentMonth(defaultValueDate);
                }
                attrWidth = 250;
                datePicker.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth);
                mapWidget.put(ab.atrName, datePicker);
                height = height + 30;
            } else if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.datetime.name().equals(attrType)) {
                AttrDateTime dtw = new AttrDateTime("", ab.valueOpt, false);
                DateBox datePicker = (DateBox) dtw.getWidget();
                attrWidth = 250;
                datePicker.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth - 6);

                requiredAttrFt.setWidget(row, 1, datePicker);
                mapWidget.put(ab.atrName, datePicker);
                height = height + 28;

            } else if (AttributeType.combobox.name().equals(attrType) || AttributeType.comboBooleanBox.name().equals(attrType)) {
//             rb.clear();
//                List<RadioButton> rb = new ArrayList<RadioButton>();
                List<String> attrOpt = BaseUtils.splitBySep(ab.valueOpt, ",");
//                VerticalPanel vp = new VerticalPanel();
//                attrWidth = 242;
//                vp.setWidth(attrWidth + "px");
//                setNameFieldWidth(attrWidth);
//                ScrollPanel scollPanel = createScrolledPanel(vp, attrOpt.size());
//                if (BIKConstants.METABIKS_ATTR_ICON_NAME.equals(ab.atrName)) {
//                    gridIcons = BIKClientSingletons.createSelectIconView(attrOpt, BIKGWTConstants.DEFAULT_NODE_KIND_ICON_NAME);
//                    for (int i = 0; i < gridIcons.getRowCount(); i++) {
//                        for (int j = 0; j < gridIcons.getCellCount(i); j++) {
//                            if (gridIcons.getWidget(i, j) instanceof RadioButton) {
//                                rb.add((RadioButton) gridIcons.getWidget(i, j));
//                            }
//                        }
//                    }
//                    vp.add(gridIcons);
//                } else {
//                    for (String ao : attrOpt) {
//                        RadioButton radioButton = new RadioButton(BaseUtils.trimAndCompactSpaces(ab.atrName), ao);
//                        vp.add(radioButton);
////             if (isEditAttr && ao.equals(atrLinValue)) {
//                        radioButton.setValue(ao.equals(defaultValue));
//                        //}
//                        if (!attrOpt.get(0).equals(ao)) {
//                            height = height + 18;
//                        }
//                        rb.add(radioButton);
//                    }
//                    height = height + 26;
//                }
////                requiredAttrFt.setWidget(row, 1, scollPanel);
//                mapRbs.put(ab.atrName, rb);

//                attributeWidget = new AttrCombobox(ab.atrLinValue, attrOpt, ab.atrName, true, ab.defaultValue);
                attributeWidget = new AttrCombobox(ab.atrLinValue, attrOpt, ab.atrName, true, ab.defaultValue, new IParametrizedContinuation<Boolean>() {
                    @Override
                    public void doIt(Boolean param) {
                        awb.get(BIKConstants.METABIKS_ATTR_IS_EMPTY).setEnable(!param, BIKConstants.METABIKS_ATTR_IS_EMPTY);
                        awb.get(BIKConstants.METABIKS_ATTR_IS_REQUIRED).setEnable(!param, BIKConstants.METABIKS_ATTR_IS_REQUIRED);
                        awb.get(BIKConstants.METABIKS_ATTR_TYPE_ATTR).setEnable(!param, BIKConstants.METABIKS_ATTR_TYPE_ATTR);

                    }
                });
                awb.put(ab.atrName, attributeWidget);
                requiredAttrFt.setWidget(row, 1, attributeWidget.getWidget());
                height = height + attributeWidget.getWidgetHeight();
            } else if (AttributeType.selectOneJoinedObject.name().equals(attrType)) {
                final List<RadioButton> rb = new ArrayList<RadioButton>();
                final VerticalPanel vp = new VerticalPanel();
                ScrollPanel scollPanel = new ScrollPanel(vp);
                attrWidth = 322;
                vp.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth);
                if (parent != null) {
                    BIKClientSingletons.getService().getJoinedObjs(parent.id, null, new StandardAsyncCallback<List<JoinedObjBean>>() {

                        @Override
                        public void onSuccess(final List<JoinedObjBean> result) {
                            RadioButton ndRadioBtn = new RadioButton(BaseUtils.trimAndCompactSpaces(ab.atrName), I18n.nieDotyczy.get());
                            rb.add(ndRadioBtn);
                            vp.add(ndRadioBtn);
                            for (JoinedObjBean bean : result) {
                                final List<String> encodedValues = BaseUtils.splitBySep(bean.nodeMenuPath, ",");
                                final String nodeIdLink = encodedValues.get(0);
                                String caption = encodedValues.get(encodedValues.size() - 1);
                                RadioButton radioButton = new RadioButton(BaseUtils.trimAndCompactSpaces(ab.atrName), caption);
                                nodePath2NodeLink.put(caption, nodeIdLink);
                                vp.add(radioButton);
                                radioButton.setValue(true);
                                rb.add(radioButton);
                            }
                            resizeRequiredAttrScrollPanel();
                        }
                    });
                }
                requiredAttrFt.setWidget(row, 1, scollPanel);
                mapRbs.put(ab.atrName, rb);
                height = height + 18;
            } else if (AttributeType.checkbox.name().equals(attrType)) {
//             cb.clear();
                List<CheckBox> cb = new ArrayList<CheckBox>();

                VerticalPanel vp = new VerticalPanel();
                attrWidth = 242;
                vp.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth);
                List<String> attrOpt = BaseUtils.splitBySep(ab.valueOpt, ",");
                ScrollPanel scollPanel = createScrolledPanel(vp, attrOpt.size());
                List<String> defaultChecked = BaseUtils.splitBySep(defaultValue, ",", true);
                for (String ao : attrOpt) {
                    CheckBox checkBox = new CheckBox(ao);
                    vp.add(checkBox);
//             if (isEditAttr && attrOpt2.contains(ao)) {
//                    checkBox.setValue(true);
//             }
                    checkBox.setValue(defaultChecked.contains(ao));
                    cb.add(checkBox);
                    height = height + 19;
                }
                height = height + 12;
                requiredAttrFt.setWidget(row, 1, scollPanel);
                mapCbs.put(ab.atrName, cb);
//            } else if (AttributeType.longText.name().equals(attrType)) {
//                   HorizontalPanel hp = new HorizontalPanel();
//                 hp.add(createEditButton(ab, requiredAttrs, row));

            } else if (AttributeType.ansiiText.name().equals(attrType) || AttributeType.sql.name().equals(attrType)) {
                TextArea ansiiTextArea = new TextArea();
                VerticalPanel vp = new VerticalPanel();
                vp.add(ansiiTextArea);
                vp.setWidth("100%");
                ansiiTextArea.setStyleName("richTextArea");
//                ansiiTextArea.setWidth(width);
                attrWidth = 800;
                ansiiTextArea.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth - 6);
                ansiiTextArea.setHeight("150px");
                ansiiTextArea.setText(defaultValue);
                requiredAttrFt.setWidget(row, 1, vp);
                mapWidget.put(ab.atrName, ansiiTextArea);
                height = height + 165;
                isExistsTextArea = true;
            } else if (AttributeType.hyperlinkInMulti.name().equals(attrType)
                    || AttributeType.hyperlinkInMono.name().equals(attrType)
                    || AttributeType.permissions.name().equals(attrType)
                    || AttributeType.longText.name().equals(attrType)) {
                HorizontalPanel hp = new HorizontalPanel();
                String caption = "";
                if (!BaseUtils.isStrEmptyOrWhiteSpace(defaultValue)) {
                    int idx = defaultValue.indexOf(",");
//                    final String nodeIdLink = atrLinValue.substring(0, idx);
                    caption = defaultValue.substring(idx + 1, defaultValue.contains("|") ? defaultValue.indexOf("|") : defaultValue.length());
                }
//                Label lbl = new Label(caption);
                if (absMap.containsKey(BIKConstants.METABIKS_ATTR_ORIGINAL)
                        || (absMap.containsKey(atrControlName) && ab.atrName.equals(atrControlName))) {
                    nameText.setText(caption);
                }
                attrWidth = 242;
                hp.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth);
                if (AttributeType.longText.name().equals(attrType)) {
                    HTML html = new HTML(caption);
                    hp.add(html);
                } else {
                    Label lbl = new Label(caption);
                    hp.add(lbl);
                }
                hp.add(createEditButton(ab, requiredAttrs, row));
                requiredAttrFt.setWidget(row, 1, hp);
                mapWidget.put(ab.atrName, hp);
                height = height + 40;
            }

//            ft.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-white-middle" : "RelatedObj-oneObj-grey-middle");
            row++;
            requiredAttrFt.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
        }

        if (BIKConstants.TREE_KIND_META_BIKS.equals(node.treeKind)) {
            Widget wg = mapWidget.get(BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL);
            if (mapWidget.containsKey(BIKConstants.METABIKS_ATTR_TREE_KIND_CAPTION_PL)) {
                wg = mapWidget.get(BIKConstants.METABIKS_ATTR_TREE_KIND_CAPTION_PL);
            }
            if (mapWidget.containsKey(BIKConstants.METABIKS_ATTR_ATTR_CAT_NAME)) {
                wg = mapWidget.get(BIKConstants.METABIKS_ATTR_ATTR_CAT_NAME);
            }
            if (mapWidget.containsKey(BIKConstants.METABIKS_ATTR_ATTR_NAME)) {
                wg = mapWidget.get(BIKConstants.METABIKS_ATTR_ATTR_NAME);
            }
            final TextBox tb = (TextBox) wg;
            if (tb != null) {
                tb.addKeyUpHandler(new KeyUpHandler() {

                    @Override
                    public void onKeyUp(KeyUpEvent event) {
                        nameText.setText(tb.getText());
                    }
                });
            }
        }
//        if (BIKConstants.TREE_KIND_META_BIKS_MENU_CONF.equals(node.treeKind)) {
//            Widget wg = mapWidget.get(BIKConstants.METABIKS_ATTR_TREE_TYPE);
//            if (wg != null && BIKConstants.NODE_KIND_META_MENU_ITEM.equals(node.nodeKindCode)) {
//                TextBox tb = ((TextBox) wg);
//                tb.setEnabled(false);
//            }
//        }
    }

    protected ScrollPanel createScrolledPanel(VerticalPanel vp, int objsCount) {
        ScrollPanel scollPanel = new ScrollPanel(vp);
        if (objsCount > 20) {
            scollPanel.setHeight("90px");
        }
        return scollPanel;
    }

    protected Widget createEditButton(final AttributeBean attribute, final List<AttributeBean> attributes, int row) {
        if (attribute.typeAttr.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.calculation.name()) || attribute.typeAttr.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.javascript.name())) {
            // nie ma możliwości edycji atrybutów wyliczanych
            return new Label();
        }
        PushButton editButton = createEditButtonForAttribute(attribute, attributes, row);
        return editButton;
    }

    public PushButton createEditButtonForAttribute(final AttributeBean attribute,
            final List<AttributeBean> attributes, final int row) {

        IContinuation optOnClickCont = (new IContinuation() {

            @Override
            public void doIt() {
                showAttributeForm(attribute, attributes, row);
            }
        });

        return BIKClientSingletons.createEditButtonForAttribute(optOnClickCont);
    }

    public void showAttributeForm(final AttributeBean atr, List<AttributeBean> attributes, final int row) {
        final Map<String, String> attrNameToValue = new HashMap<String, String>();
        for (AttributeBean ab : attributes) {
            attrNameToValue.put(ab.atrName, BaseUtils.safeToStringNotNull(ab.defaultValue));
        }
        final Map<String, AttributeBean> map = new HashMap<String, AttributeBean>();
        map.put(atr.atrName, atr);
        BIKClientSingletons.getService().getNodesByLinkedKind(node.nodeKindId, BIKConstants.HYPERLINK, node.treeId, new StandardAsyncCallback<List<TreeNodeBean>>() {

            @Override
            public void onSuccess(List<TreeNodeBean> result) {

                new AddOrEditAttributeDialog(node.nodeKindId, BIKClientSingletons.getProperDynamicTreeIds().contains(node.treeId))
                        .buildAndShowDialog(map, attrNameToValue, atr, node == null ? null : node.treeId, I18n.dodajAtrybut.get(),
                                /*result*/ null, node.branchIds, node, new IParametrizedContinuation<AttributeBean>() {
                            public void doIt(AttributeBean param) {
                                absMap.put(param.atrName, param);

                                mapWidget.put(param.atrName, new Label(param.atrLinValue));
                                HorizontalPanel hp = new HorizontalPanel();
                                String caption = param.atrLinValue;

                                if (absMap.containsKey(BIKConstants.METABIKS_ATTR_ORIGINAL)
                                        || (absMap.containsKey(atrControlName) && param.atrName.equals(atrControlName))) {
                                    if ((AttributeType.hyperlinkInMulti.name().equals(param.typeAttr) || AttributeType.hyperlinkInMono.name().equals(param.typeAttr)
                                            || AttributeType.permissions.name().equals(param.typeAttr)) && !BaseUtils.isStrEmptyOrWhiteSpace(caption)) {
                                        int idx = caption.indexOf(",");
//                    final String nodeIdLink = atrLinValue.substring(0, idx);
                                        caption = caption.substring(idx + 1, caption.contains("|") ? caption.indexOf("|") : caption.length());
                                    }
                                    nameText.setText(caption);
                                }
                                if ((AttributeType.hyperlinkInMulti.name().equals(param.typeAttr) || AttributeType.hyperlinkInMono.name().equals(param.typeAttr)
                                        || AttributeType.permissions.name().equals(param.typeAttr)) && !BaseUtils.isStrEmptyOrWhiteSpace(caption)) {
                                    caption = BIKClientSingletons.getHyperlinkAttribute(param.atrLinValue);
                                }
                                param.defaultValue = caption;
                                if (AttributeType.longText.name().equals(param.typeAttr)) {
                                    HTML html = new HTML(caption);
                                    hp.add(html);
                                } else {
                                    Label lbl = new Label(caption);
                                    hp.add(lbl);
                                }
                                //int row;
                                hp.add(createEditButton(param, requiredAttrs, row));
                                requiredAttrFt.setWidget(row, 1, hp);
                            }
                        });
            }
        });
    }

    @Override
    protected void doAction() {
        node.name = nameText.getText().trim();
        if (allowDuplicateJoinedObjs) {
            node.addingJoinedObjs = new HashSet<Integer>();
            for (CheckBox cb : addingJoinedObjs.keySet()) {
                if (cb.getValue()) {
                    Integer dstNodeId = addingJoinedObjs.get(cb);
                    node.addingJoinedObjs.add(dstNodeId);
                }
            }
        }
        saveCont.doIt(new Pair<TreeNodeBean, Map<String, AttributeBean>>(node, absMap));
//        }
    }

    public void buildAndShowDialog(int nodeKindId, String nodeKindCode, TreeNodeBean nodeToEdit, TreeNodeBean parent, int treeId, List<String> keywords,
            List<AttributeBean> requiredAttrs, IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>> saveCont) {
        this.requiredAttrs = requiredAttrs;
        buildAndShowDialog(nodeKindId, nodeKindCode, nodeToEdit, parent, treeId, keywords, true, saveCont);
    }

    public void buildAndShowDialog(int nodeKindId, String nodeKindCode, TreeNodeBean nodeToEdit, TreeNodeBean parent, int treeId, List<String> keywords, IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>> saveCont) {
        buildAndShowDialog(nodeKindId, nodeKindCode, nodeToEdit, parent, treeId, keywords, true, saveCont);
    }

    public void buildAndShowDialog(int nodeKindId, String nodeKindCode, TreeNodeBean nodeToEdit, TreeNodeBean parent, int treeId, List<String> keywords, boolean getNodeKindFromParent, List<AttributeBean> requiredAttrs, IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>> saveCont) {
        if (!requiredAttrs.isEmpty() && requiredAttrs.get(0).atrName.startsWith("metaBiks")) {
            for (AttributeBean atrBean : requiredAttrs) {
                if (atrBean.atrName.equals("metaBiks.Nazwa atrybutu") || atrBean.atrName.equals("metaBiks.Nazwa typu obiektu")) {
                    AttributeBean nameAttrBean = atrBean;
                    requiredAttrs.remove(atrBean);
                    requiredAttrs.add(0, nameAttrBean);
                }
            }
        }
        this.requiredAttrs = requiredAttrs;
        buildAndShowDialog(nodeKindId, nodeKindCode, nodeToEdit, parent, treeId, keywords, getNodeKindFromParent, saveCont);
    }

    public void buildAndShowDialog(int nodeKindId, String nodeKindCode, TreeNodeBean nodeToEdit, TreeNodeBean parent, int treeId, List<String> keywords, boolean getNodeKindFromParent, IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>> saveCont) {
        this.saveCont = saveCont;
        this.keywords = keywords;

        if (nodeToEdit != null) {
            kind = ActionModeKind.Edit; //I18n.edycja2.get() /* I18N:  */;
            node = nodeToEdit;
        } else {
            node = new TreeNodeBean();
            if (parent != null) {
                this.parent = parent;
                node.parentNodeId = parent.id;
                node.objId = null;
                node.treeId = parent.treeId;
                node.linkedNodeId = null;
                node.nodeKindId = getNodeKindFromParent ? parent.nodeKindId : nodeKindId;
                node.nodeKindCode = getNodeKindFromParent ? parent.nodeKindCode : nodeKindCode;
                node.treeCode = parent.treeCode;
                node.treeName = parent.treeName;
                node.treeKind = parent.treeKind;
                node.hasNoChildren = true;
                node.branchIds = parent.branchIds;
                kind = ActionModeKind.New; //I18n.nowy2.get() /* I18N:  */;
            } else {
                node.parentNodeId = null;
                node.linkedNodeId = null;
                node.nodeKindId = nodeKindId;
                node.nodeKindCode = nodeKindCode;
                node.objId = null;
                node.treeId = treeId;
                node.hasNoChildren = true;
                kind = ActionModeKind.Root; //I18n.root.get() /* I18N:  */;
            }
        }
        if (parent != null) {
            atrControlName = BaseUtils.safeToStringDef(BIKClientSingletons.getNodeKindBeanByCode(node.nodeKindCode).attrControlNodeName, null);
        }
        super.buildAndShowDialog();
    }

    @Override
    protected boolean doActionBeforeClose() {
        String name = nameText.getText().trim();
//        if (node.treeCode != null) {
//            if ((node.treeKind.equals(BIKGWTConstants.TREE_KIND_GLOSSARY) || node.treeKind.equals(BIKGWTConstants.TREE_KIND_DICTIONARY)/*node.treeCode.equals(BIKGWTConstants.TREE_CODE_GLOSSARY) || node.treeCode.equals(BIKGWTConstants.TREE_CODE_SHORTCUTS_GLOSSARY) || node.treeCode.equals(BIKGWTConstants.TREE_CODE_BUSINESS_GLOSSARY)*/)
//                    && !node.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_GLOSSARY_CATEGORY)) {

        if (BaseUtils.isStrEmptyOrWhiteSpace(name)) {
            Window.alert(I18n.fillNameField.get());
            return false;
        }
        if (mustCheckNodeNameUniqueness(node) && (name.equals("") || (keywords.contains(stringTrimAndToLowerCase(name))
                && (node.name != null && !equalsLowerCaseString(name, node.name))))) {
            Window.alert(I18n.definicjOTakiejNazwieJuzIstnieje.get() /* I18N:  */);
//                    doAction();
            return false;
        }

//            Map<String, List<CheckBox>> mapCbs = new HashMap<String, List<CheckBox>>();
//    Map<String, List<RadioButton>> mapRbs = new HashMap<String, List<RadioButton>>();
        if (!mapCbs.isEmpty()) {
            for (Entry<String, List<CheckBox>> sw : mapCbs.entrySet()) {

                List<String> valueOpt = new ArrayList<String>();
                for (CheckBox c : sw.getValue()) {
                    if (c.getValue()) {
                        valueOpt.add(c.getText());
                    }
                }
                String nameW = sw.getKey();
                AttributeBean ab = absMap.get(nameW);
                ab.atrLinValue = BaseUtils.mergeWithSepEx(valueOpt, ",");
                if (BaseUtils.isStrEmptyOrWhiteSpace(ab.atrLinValue)) {
                    ab.atrLinValue = ab.defaultValue;
                }
            }

        }
        for (Map.Entry<String, AttrWigetBase> aw : awb.entrySet()) {
            String attributeName = aw.getKey();
            AttributeBean ab = absMap.get(attributeName);
            ab.atrLinValue = aw.getValue().getValue();
            absMap.put(attributeName, ab);
        }

        if (!mapRbs.isEmpty()) {
            for (Entry<String, List<RadioButton>> sw : mapRbs.entrySet()) {

                List<String> valueOpt = new ArrayList<String>();
                String nameW = sw.getKey();
                if (BIKConstants.METABIKS_ATTR_ICON_NAME.equalsIgnoreCase(nameW)) {
                    valueOpt.add(BIKClientSingletons.getSelectedIconFromView(gridIcons));
                } else {
                    for (RadioButton c : sw.getValue()) {
                        if (c.getValue()) {
                            valueOpt.add(c.getText());
                        }
                    }
                }
                AttributeBean ab = absMap.get(nameW);

                if (valueOpt.isEmpty()) {
                    valueOpt.add(ab.defaultValue);
                } else {
                    ab.atrLinValue = BaseUtils.mergeWithSepEx(valueOpt, ",");
                }
                if (AttributeType.selectOneJoinedObject.name().equalsIgnoreCase(ab.typeAttr)) {
                    ab.atrLinValue = (nodePath2NodeLink.containsKey(ab.atrLinValue) ? nodePath2NodeLink.get(ab.atrLinValue) + "," : "") + ab.atrLinValue;
                }
            }
        }

        if (!mapWidget.isEmpty()) {
            for (Entry<String, Widget> sw : mapWidget.entrySet()) {
                Widget w = sw.getValue();
                String nameW = sw.getKey();
                AttributeBean ab = absMap.get(nameW);

                if (w instanceof ValueBoxBase) {
                    ValueBoxBase t = (ValueBoxBase) w;
                    ab.atrLinValue = t.getText();
                } else if (w instanceof DateBox) {
                    DateBox d = (DateBox) w;
                    if (ab.typeAttr.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.data.name())) {
                        ab.atrLinValue = SimpleDateUtils.dateToString(d.getValue());
                    } else {
                        ab.atrLinValue = SimpleDateUtils.toCanonicalString(d.getValue());
                    }
//                    ab.atrLinValue = SimpleDateUtils.dateToString(d.getValue());
                    if (BaseUtils.isStrEmptyOrWhiteSpace(ab.atrLinValue)) {
                        ab.atrLinValue = ab.defaultValue;
                    }
                } else if (w instanceof Label) {
                    Label t = (Label) w;
                    ab.atrLinValue = t.getText();
                }
                absMap.put(sw.getKey(), ab);
            }
        }
        if (!BaseUtils.isCollectionEmpty(requiredAttrs)) {
            String emptyAttrs = "";
            boolean isFirst = true;
            String coma = "";
            for (AttributeBean ab : absMap.values()) {

                if (mustFillAttribute(ab)) {
//                    Window.alert(I18n.wypełnijAtryubuty.get() /* I18N:  */ + ": " + ab.atrName);
                    emptyAttrs = emptyAttrs + coma + trimToDot(ab.atrName);
                    if (isFirst) {
                        coma = ", ";
                        isFirst = false;
                    }
//                    return false;
                }
                if (BIKClientSingletons.showAddStatus()
                        && ab.atrName.equals(BIKConstants.METABIKS_ATTR_DEFAULT_VALUE) && absMap.get(BIKConstants.METABIKS_ATTR_STATUS_NAME).atrLinValue.equals(BIKConstants.YES)
                        && BaseUtils.isStrEmptyOrWhiteSpace(ab.atrLinValue)) {
                    emptyAttrs = emptyAttrs + coma + trimToDot(ab.atrName);
                }

//                if (BaseUtils.isStrEmptyOrWhiteSpace(ab.atrLinValue)) {
//                    ab.atrLinValue = null;
//                }
            }
            if (!BaseUtils.isStrEmptyOrWhiteSpace(emptyAttrs)) {
                Window.alert(I18n.wypełnijAtryubuty.get() + " " + emptyAttrs);
//                errorLbl.setText(I18n.wypełnijAtryubuty.get() + " " + emptyAttrs);
                return false;
            }
        }
//            }
//        }
        return true;
    }

    protected String stringTrimAndToLowerCase(String n1) {
        return n1.trim().toLowerCase();
    }

    protected boolean equalsLowerCaseString(String n1, String n2) {
        return stringTrimAndToLowerCase(n1).equals(stringTrimAndToLowerCase(n2));
    }

    protected boolean mustFillAttribute(AttributeBean ab) {
        if (!BIKConstants.TREE_KIND_META_BIKS.equals(node.treeKind) && !BIKConstants.TREE_KIND_META_BIKS_MENU_CONF.equals(node.treeKind)) {
            return BaseUtils.isStrEmptyOrWhiteSpace(ab.atrLinValue) && !ab.isEmpty
                    && !ab.typeAttr.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.calculation.name())
                    && !ab.typeAttr.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.javascript.name());
        } else {
            String optValue = ab.atrLinValue;
            if (BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE.equals(node.nodeKindCode)) {
                if (BIKConstants.METABIKS_ATTR_VALUE_OPT.equals(ab.atrName)) {//Lista wartosci jest pusty
                    String attrType = absMap.containsKey(BIKConstants.METABIKS_ATTR_TYPE_ATTR) ? absMap.get(BIKConstants.METABIKS_ATTR_TYPE_ATTR).atrLinValue : "";
                    boolean isCheckOrComboOrBooleanBoxOrDateType = !BaseUtils.isStrEmptyOrWhiteSpace(attrType)
                            && (BIKCenterUtils.isCheckboxOrComboBoxType(attrType) || BIKCenterUtils.isBooleanType(attrType) || BIKCenterUtils.isDateType(attrType));
                    if (isCheckOrComboOrBooleanBoxOrDateType && BaseUtils.isStrEmptyOrWhiteSpace(optValue)) {
                        return true;
                    }
                    if (BIKCenterUtils.isDateType(attrType)) {
                        List<String> l = BaseUtils.splitBySep(BIKClientSingletons.mergeWithSepExAndTrim(optValue), ",");
                        if (l.size() != 2) {
                            return true;
                        }
                        try {
                            if (SimpleDateUtils.parseFromCanonicalString(l.get(0)) == null || (SimpleDateUtils.parseFromCanonicalString(l.get(1)) == null)) {
                                return true;
                            }
                        } catch (Exception ex) {
                            return true;
                        }
                    }

                    if (BIKCenterUtils.isBooleanType(attrType) && (BaseUtils.isStrEmptyOrWhiteSpace(optValue) || BaseUtils.splitBySep(optValue, ",", true).size() != 2)) {
                        return true;
                    }
                }
                return false;
            }

            if (BaseUtils.isStrEmptyOrWhiteSpace(optValue)) {
                if (BIKConstants.NODE_KIND_META_NODE_KIND.equals(node.nodeKindCode)) {
                    return BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL.equals(ab.atrName) || BIKConstants.METABIKS_ATTR_ICON_NAME.equals(ab.atrName);
                }
                if (BIKConstants.NODE_KIND_META_TREE_KIND.equals(node.nodeKindCode)) {
                    return BIKConstants.METABIKS_ATTR_NODE_KIND_CAPTION_PL.equals(ab.atrName);
                }
                if (BIKConstants.NODE_KIND_META_ATTR_4_NODE_KIND.equals(node.nodeKindCode)
                        || BIKConstants.NODE_KIND_META_NODE_KIND_4_TREE_KIND.equals(node.nodeKindCode)
                        || BIKConstants.NODE_KIND_META_RELATED_NODE_KIND.equals(node.nodeKindCode)) {
                    return BIKConstants.METABIKS_ATTR_ORIGINAL.equals(ab.atrName);
                }
//                        || (!BIKConstants.METABIKS_ATTR_ATTR_DESC.equals(ab.atrName) && !BIKConstants.METABIKS_ATTR_DEFAULT_VALUE.equals(ab.atrName) && !BIKConstants.METABIKS_ATTR_OVERWRITE_TYPE_ATTR.equals(ab.atrName) && !BIKConstants.METABIKS_ATTR_ATTR_CHILDREN_KINDS.equals(ab.atrName) && !BIKConstants.METABIKS_ATTR_CHILDREN_KINDS.equals(ab.atrName));
            }
        }
        return false;
    }

    public static boolean mustCheckNodeNameUniqueness(TreeNodeBean node) {
        return node.treeKind != null && node.nodeKindCode != null
                && (node.treeKind.equals(BIKGWTConstants.TREE_KIND_GLOSSARY)
                || node.treeKind.equals(BIKGWTConstants.TREE_KIND_DICTIONARY))
                && !node.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_GLOSSARY_CATEGORY)
                && !node.nodeKindCode.equals(BIKConstants.NODE_KIND_LISA_FOLDER);
    }

    private boolean isNodeNameEditable() {
        boolean ok = BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG.equals(node.nodeKindCode)
                || BIKConstants.NODE_KIND_META_ATTRIBUTE_TYPE.equals(node.nodeKindCode)
                || BIKConstants.NODE_KIND_META_PRINT.equals(node.nodeKindCode)
                || BIKConstants.NODE_KIND_META_PRINTS_TEMPLATES.equals(node.nodeKindCode);
        ok = ok || (!BIKConstants.TREE_KIND_META_BIKS.equals(node.treeKind) /* && BaseUtils.isStrEmptyOrWhiteSpace(atrControlName)*/);
        return ok;
    }
}
