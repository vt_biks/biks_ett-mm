﻿exec sp_check_version '1.3.0.13';
go

delete from bik_lisa_dict_column where dictionary_name = 'Prod';
go

insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  16,  16,'id_cp_tier','','Id Cp Tier',   1,'I1',   0);                                  
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  17,  17,'id_typ_oplata','','Id Typ Oplata',   1,'CV',   0);                            
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  13,  13,'wartosc','','Wartosc',   1,'D ',   0);                                        
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  15,  15,'il_wystapien','','Ile Wystapien',   1,'I2',   0);                             
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  11,  11,'il_cykli','','Ile Cykli',   1,'I2',   0);                                     
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  14,  14,'id_typ_wartosc','','Id Typ Wartosc',   1,'I1',   0);                          
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',   8,   8,'id_system_zr','','Id System Zr',   1,'I ',   0);                              
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  12,  12,'id_typ_jednostki_miary','','Id Typ Jednostki Miary',   1,'CF',   0);          
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',   6,   6,'data_konca','','Data Konca',   1,'DA',   0);                                  
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  10,  10,'id_podtyp_usluga','','Id Podtyp Usluga',   1,'CF',   0);                      
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',   5,   5,'data_poczatku','','Data Poczatku',   1,'DA',   0);                            
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',   9,   9,'id_typ_usluga','','Id Typ Usluga',   1,'CF',   0);                            
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  29,  29,'id_update','','Id Update',   1,'I ',   0);                                    
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',   7,   7,'id_prod_zr','','ID PROD Zr',   1,'CV',   0);                                  
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',   3,   3,'id_typ_prod','','Id Typ Prod',   1,'CF',   0);                                
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',   4,   4,'nazwa_prod','','nazwa_prod',   1,'CV',   0);                                  
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  26,  26,'id_podtyp_prod','','Id Podtyp Prod',   1,'I1',   0);                          
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  28,  28,'id_load','','Id Load',   1,'I ',   0);                                        
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  24,  24,'id_typ_kupon','','Id Typ Kupon',   1,'CF',   0);                              
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  27,  27,'anu_wsk','','Anu Wskaznik',   1,'I1',   0);                                   
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  21,  21,'id_typ_jednostki_miary_cena','','Id Typ Jednostki Miary cena',   1,'CF',   0);
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',   2,   2,'id_prod_nadrz','','Id Prod_Nadrz',   1,'CF',   0);                            
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  19,  19,'il_miesiecy_plan_taryfowy','','Ile Miesiecy Plan Taryfowy',   1,'I1',   0);   
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',   1,   1,'id_prod','','Id Prod',   1,'CF',   1);                                        
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  18,  18,'id_typ_plan_taryfowy','','Id Typ Plan Taryfowy',   1,'I1',   0);              
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  25,  25,'data_stat','','Data Stat',   1,'DA',   0);                                    
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  23,  23,'id_sprzet_marka','','Id Sprzet Marka',   1,'I2',   0);                        
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  22,  22,'cena_domyslna','','Cena Domyslna',   1,'D ',   0);                            
 insert into bik_lisa_dict_column (dictionary_name, column_id, sequence, name_in_db, description, title_to_display, is_active, data_type, is_main_key) 
 values ('Prod',  20,  20,'id_plan_taryfowy_nast','','Id Plan Taryfowy Nast',   1,'CF',   0);            

go

exec sp_update_version '1.3.0.13', '1.3.0.14';
go

