/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.user.client.ui.Widget;
import pl.fovis.common.lisa.LisaDictionaryValue;
import simplelib.IContinuation;

/**
 *
 * @author tbzowka
 */
public interface WidgetWithValue {

    public Widget getWidget();

    public void setValue(Object value);

    public LisaDictionaryValue getValue();

    public void setEditable(boolean b);

    public void addChangeHandler(IContinuation con);

    public void setStyleName(String styleName);    
}
