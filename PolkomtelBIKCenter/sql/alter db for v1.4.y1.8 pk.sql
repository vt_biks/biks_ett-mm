﻿exec sp_check_version '1.4.y1.8';
go

insert into bik_admin (param, value) values('lisateradata.schema', 'DB_SBX_NAS');
go

exec sp_update_version '1.4.y1.8', '1.4.y1.9';
go