/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.ICheckEnabledByDesign;
import pl.fovis.client.NodeRefreshingCallback;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.SimpleDateUtils;

/**
 *
 * @author mmastalerz
 */
public abstract class GenericBeanExtractorBase<T> implements IGridGenericBeanPropsExtractor<T> {

    public IContinuation dataChangedHandler;
    public List<T> allBeans;
    protected boolean isAddJoinedButtonEnabled;

    public GenericBeanExtractorBase(boolean isAddJoinedButtonEnabled) {
        this.isAddJoinedButtonEnabled = isAddJoinedButtonEnabled;
    }

    public void setAllBeansList(List<T> allBeans) {
        this.allBeans = allBeans;
    }

    protected void deleteBeanFromList(T b, String msg) {
        allBeans.remove(b);
        refreshView(msg);
    }

    protected void refreshView(String msg) {
        if (dataChangedHandler != null) {
            dataChangedHandler.doIt();
            BIKClientSingletons.showInfo(msg);
        }
    }

    public void setDataChangedHandler(IContinuation handler) {
        this.dataChangedHandler = handler;
    }
    protected List<IGridBeanAction<T>> actions = new ArrayList<IGridBeanAction<T>>();

    protected void execActJoinSimilarNode(T b) {
        //no-op
    }

    protected void execActDelete(T b) {
        //no-op
    }

    protected void execActEdit(T b) {
        //no-op
    }

    public Double getOptSimilarityScore(T b) {
        return null;
    }

    protected IGridBeanAction<T> actDelete = new SimpleGridBeanAction<T>() {
        public String getActionIconUrl(T b) {
            if (getOptSimilarityScore(b) != null) {
                return getIconUrlByIconName("plus_button");
            }

            return getIconUrlByIconName("trash_gray");
        }

        public String getActionIconHint(T b) {
            return getOptSimilarityScore(b) != null ? I18n.dodajPowiazanie.get() : I18n.usun.get() /* I18N:  */;
        }

        public void executeAction(final T b) {

            if (getOptSimilarityScore(b) != null) {
                execActJoinSimilarNode(b);
                return;
            }

            HTML element = new HTML(
                    I18n.czyNaPewnoUsunac.get() /* I18N:  */ + ": <div class='Delete-Element'>" + (hasIcon() ? ("<" + "img src" /* I18N: no */ + "=\"" + getIconUrl(b) + "\" " + "style=\"width: 16px; height" /* I18N: no */ + ": 16px;\"/>") : "") + " <b>" + getNameAsHtml(b) + "</b></div>");
            element.setStyleName("komunikat" /* I18N: no:css-class */);

            new ConfirmDialog().buildAndShowDialog(element, I18n.usun.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            execActDelete(b);
                        }
                    });
        }

        public boolean isVisible(T b) {
            return isDeleteBtnVisible2Wtf(b) && isDeleteBtnVisible(b);
        }

        public boolean isEnabled(T b) {
            if (getOptSimilarityScore(b) != null) {
                return isAddJoinedButtonEnabled;
            }
            return isActDeleteBtnEnabled(b);
//            return /*BIKClientSingletons.isEffectiveExpertLoggedIn()*/ isEffectiveExpertLoggedInEx(b) && isDeleteBtnEnabled(b);
        }

        public IGridBeanAction.TypeOfAction getActionType() {
            return IGridBeanAction.TypeOfAction.Delete;
        }
    };
    protected IGridBeanAction<T> actEdit = new SimpleGridBeanAction<T>() {
        public String getActionIconUrl(T b) {
            return getIconUrlByIconName("edit_gray");
        }

        public String getActionIconHint(T b) {
            return I18n.edytuj.get() /* I18N:  */;
        }

        public void executeAction(T b) {
            execActEdit(b);
        }

        public boolean isEnabled(T b) {
            return isEditBtnEnabled(b);
        }

        public boolean isVisible(T b) {
            return true;
        }

        public IGridBeanAction.TypeOfAction getActionType() {
            return IGridBeanAction.TypeOfAction.Edit;
        }
    };

    protected boolean isEditBtnEnabled(T b) {
        return isEffectiveExpertLoggedInEx(b)/*BIKClientSingletons.isEffectiveExpertLoggedIn()*/;
    }

    public boolean isDeleteBtnVisible2Wtf(T b) {
        return true;
    }

    public boolean isDeleteBtnVisible(T b) {
        return true;
    }

    public boolean isDeleteBtnEnabled(T b) {
        return true;
    }

    public boolean hasIcon() {
        return true;
    }

    public String getIconUrl(T b) {
        return null;
    }

    protected List<IGridBeanAction<T>> createActionList() {
        List<IGridBeanAction<T>> res = new ArrayList<IGridBeanAction<T>>();
        res.add(actEdit);
        res.add(actDelete);
        return res;
    }

    protected List<IGridBeanAction<T>> getActionList() {
        if (actions == null || actions.isEmpty()) {
            actions = createActionList();
        }
        return actions;
    }

    public static String getIconUrlByIconName(String iconName) {
        return "images" /* I18N: no */ + "/" + iconName + "." + "gif" /* I18N: no */;
    }

//    public String getOptIconHint(T b) {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }
    public static HTML createDateAdded(Date dateAdded, String style) {
        HTML dateAddedHTML = new HTML(SimpleDateUtils.toCanonicalString(dateAdded));
        dateAddedHTML.setStyleName(style/*"dqc-dateAdded"*/);
        return dateAddedHTML;
    }

    public int getAddColCnt() {
        return 0;
    }

    public Object getAddColVal(T b, int addColIdx) {
        //no-op
        return null;
    }

    @Override
    public boolean createGridColumnNames(FlexTable gp) {
        return false;
    }

    public int getActionCnt() {
        return getActionList().size();
    }

    protected AsyncCallback<Void> getDoNothingCallback() {
        return //new NodeRefreshingCallback<Void>(currentNodeId, msgOnSuccess);
                BIKClientSingletons.getDoNothingCallback();
    }

    protected AsyncCallback<Void> getRefreshingCallback(int nodeId) {
        return new NodeRefreshingCallback<Void>(nodeId, null);
    }

    public IGridBeanAction<T> getActionByIdx(int idx) {
        return getActionList().get(idx);
    }

    public String getOptIconHint(T b) {
        return null;
    }

    protected abstract String getName(T b);

    public String getNameAsHtml(T b) {
        String name = getName(b);
        if (name == null) {
            return null;
        }
        return BaseUtils.encodeForHTMLTag(name/*getName(b)*/);
    }

    @Override
    public String getOptAddColTitle(T b, int addColIdx) {
        return null;
    }

    public boolean isEffectiveExpertLoggedInEx(T b) {
        if (getTreeCode(b) == null) {
            return false;
        }
        TreeBean tb = BIKClientSingletons.getTreeBeanByCode(getTreeCode(b));
        if (tb == null) {
            return false;
        }
        
        return BIKClientSingletons.isNodeActionEnabledByCustomRightRoles("EditNodeDetails",
                tb.id, null, new ICheckEnabledByDesign() {

                    @Override
                    public boolean isEnabledByDesign() {
                        return BIKClientSingletons.isEffectiveExpertLoggedIn();
                    }
                });
    }

    public boolean hasSelectedRowStyle(T b) {
        return false;
    }

    public boolean isActDeleteBtnEnabled(T b) {
        return isEffectiveExpertLoggedInEx(b) && isDeleteBtnEnabled(b);
    }
}
