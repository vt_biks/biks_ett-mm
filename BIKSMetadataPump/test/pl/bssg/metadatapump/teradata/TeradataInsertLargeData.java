/*a
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.teradata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import pl.bssg.metadatapump.PumpUtils;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class TeradataInsertLargeData {

    public static void main(String[] args) {
        Connection conn = PumpUtils.getConnection("com.teradata.jdbc.TeraDriver", "jdbc:teradata://192.168.167.75", "dbc", "dbc");
        String sql = "insert into DB_SBX_NAS.test(col) values(?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            Date newDate = SimpleDateUtils.newDate(2016, 01, 01);
            for (int i = 0; i < 10; i++) {

                for (int j = 0; j < 1000; j++) {
                    newDate = SimpleDateUtils.addDays(newDate, 1);
                    ps.setDate(1, new java.sql.Date(newDate.getTime()));
                    ps.addBatch();
                }
                ps.executeBatch();
            }
        } catch (Exception e) {
            System.out.println("Error = " + e.getLocalizedMessage());
        }
//        try {
//            Statement createStatement = conn.createStatement();
//            ResultSet executeQuery = createStatement.executeQuery("select col from DB_SBX_NAS.test");
//            while (executeQuery.next()) {
//                Date date = executeQuery.getDate("col");
//                System.out.println("Value = " + date.toString());
//            }
//            createStatement.close();
//            conn.close();
//        } catch (SQLException ex) {
//            System.out.println("Error = " + ex.getLocalizedMessage());
//        }
    }

}
