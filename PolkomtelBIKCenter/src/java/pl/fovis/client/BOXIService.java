/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.gwtservlet.DisableCallLoggingForServiceImplMethod;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;
import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import pl.bssg.metadatapump.common.ConnectionParametersDynamicAxBean;
import pl.bssg.metadatapump.common.ConnectionParametersFileSystemBean;
import pl.bssg.metadatapump.common.ConnectionParametersJdbcBean;
import pl.bssg.metadatapump.common.ConnectionParametersPlainFileBean;
import pl.bssg.metadatapump.common.MsSqlExPropConfigBean;
import pl.bssg.metadatapump.common.SAPBOObjectBean;
import pl.bssg.metadatapump.common.SAPBOScheduleBean;
import pl.bssg.metadatapump.common.SchedulePumpBean;
import pl.fovis.client.dialogs.ImportTreeLogDialog.TreeImportLogStatus;
import pl.fovis.common.AppPropBean;
import pl.fovis.common.ArticleBean;
import pl.fovis.common.AttachmentBean;
import pl.fovis.common.AttrHintBean;
import pl.fovis.common.AttrSearchableBean;
import pl.fovis.common.AttributeAndTheirKindsBean;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.AttributeValueInNodeBean;
import pl.fovis.common.AuditBean;
import pl.fovis.common.BIAArchiveLogBean;
import pl.fovis.common.BIArchiveConfigBean;
import pl.fovis.common.BISystemsConnectionsParametersBean;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.BikRoleForNodeBean;
import pl.fovis.common.BiksSearchResult;
import pl.fovis.common.BlogBean;
import pl.fovis.common.ChangedRankingBean;
import pl.fovis.common.ConnectionParametersADBean;
import pl.fovis.common.ConnectionParametersAuditBean;
import pl.fovis.common.ConnectionParametersBIAdminBean;
import pl.fovis.common.ConnectionParametersConfluenceBean;
import pl.fovis.common.ConnectionParametersDQCBean;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.fovis.common.ConnectionParametersErwinBean;
import pl.fovis.common.ConnectionParametersProfileBean;
import pl.fovis.common.ConnectionParametersSASBean;
import pl.fovis.common.ConnectionParametersSapBWBean;
import pl.fovis.common.ConnectionParametersSapBoBean;
import pl.fovis.common.ConnectionParametersTeradataBean;
import pl.fovis.common.DataSourceStatusBean;
import pl.fovis.common.DroolsConfigBean;
import pl.fovis.common.DroolsLogBean;
import pl.fovis.common.DynamicTreesBigBean;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.ErwinDataModelProcessObjectsRel;
import pl.fovis.common.ErwinDiagramDataBean;
import pl.fovis.common.FvsChangeExBean;
import pl.fovis.common.HelpBean;
import pl.fovis.common.HomePageBean;
import pl.fovis.common.JoinTypeRoles;
import pl.fovis.common.JoinedObjAttributeLinkedBean;
import pl.fovis.common.JoinedObjBean;
import pl.fovis.common.JoinedObjMode;
import pl.fovis.common.MailLogBean;
import pl.fovis.common.MyObjectsBean;
import pl.fovis.common.NameDescBean;
import pl.fovis.common.NewDefinitionDataBean;
import pl.fovis.common.NewsBean;
import pl.fovis.common.NodeAuthorBean;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.NodeModificationLogEntryBean;
import pl.fovis.common.NoteBean;
import pl.fovis.common.ObjectInFvsChangeBean;
import pl.fovis.common.ObjectInFvsChangeExBean;
import pl.fovis.common.ObjectInHistoryBean;
import pl.fovis.common.ParametersAnySqlBean;
import pl.fovis.common.PrintTemplateBean;
import pl.fovis.common.RightRoleBean;
import pl.fovis.common.RoleForNodeBean;
import pl.fovis.common.SapBoUniverseColumnBean;
import pl.fovis.common.SearchFullResult;
import pl.fovis.common.SearchFullResult.IndexCrawlStatus;
import pl.fovis.common.StatisticsBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeKindBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.TreeNodeBranchBean;
import pl.fovis.common.TutorialBean;
import pl.fovis.common.UserBean;
import pl.fovis.common.UserExtBean;
import pl.fovis.common.UserExtradataBean;
import pl.fovis.common.UserInNodeBean;
import pl.fovis.common.UserObjectBean;
import pl.fovis.common.ValidationExceptionBiks;
import pl.fovis.common.VoteBean;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import pl.fovis.foxygwtcommons.ServiceMethodCallException;
import simplelib.BeanWithIntIdAndNameBase;
import simplelib.LameRuntimeException;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
@RemoteServiceRelativePath("boxiservice")
public interface BOXIService extends RemoteService {

    public EntityDetailsDataBean getBikEntityDetailsData(int nodeId, Integer linkingParentId, boolean fetchExtraData);

    public void performUserLogout();

    // jedna metoda do logowania, działa w 3 trybach:
    // 1. klasyczne:    login != null && password != null && domain == null
    // 2. SSO zwykłe:   login == password == domain == null
    // 3. SSO domenowe: login != null && password != null && domain != null
    // w wyniku dostajemy nazwę bazy dbName:
    // 1. gdy dbName == null -> logowanie się nie udało
    // 2. dla trybu SingleBiks dostaniemy dbName == ""
    // drugi element wyniku (boolean) określa czy użytkownik jest zablokowany (false -> disabled)
    // na bazie, do której chciał się dobić (czyli określonej przez pierwszy element)
    public Pair<String, Boolean> performUserLogin(String login, String password, String domain);

//    public SystemUserBean performBikUserLogin(String login, String pwd);
//
//    public void performUserLoginBySSO();
//
//    public SystemUserBean performUserLoginBySSOLoginAndDomain(String login, String password, String domain);
//    public String getLoggedUserName();
//    public SystemUserBean getLoggedUserBean();
    public boolean addToFvs(List<Integer> nodeId);

    public boolean removeFromFvs(int nodeId);

    public List<ObjectInFvsChangeBean> getFavouritesForUser();

    public List<SapBoUniverseColumnBean> getAllAboutTable(int tableId);

    public void addToHist(int nodeId);

    public int addFrequentlyAskedQuestions(int nodeId, String question, String answer);

    public void updateFrequentlyAskedQuestions(int id, String question, String answer);

    public void deleteFrequentlyAskedQuestions(int id);

    public List<ObjectInHistoryBean> getFromHistory();

    public void createTree(Map<String, String> langToName, String treeKind, String parentMenuCode, Integer parentId, Integer visualOrder, Integer isAutoObjId,
            Map<String, String> optMenuSubGroup, String optServerTreeFileName);

    public Pair<Integer, List<String>> createTreeNode(Integer parentNodeId, int nodeKindId, String name, String objId, int treeId, Integer linkedNodeId);

    public Pair<Integer, List<String>> createTreeNodeWithAttr(Integer parentNodeId, int nodeKindId, String name, String objId, int treeId, Integer linkedNodeId, Set<Integer> addingJoinedObjs, Map<String, AttributeBean> requiredAttrs);

    public List<String> updateTreeNode(TreeNodeBean node);

    public List<String> updateTreeNodeAndChangeFvs(final TreeNodeBean node);

    public List<String> deleteTreeNode(TreeNodeBean node) throws LameRuntimeException;

    public List<TreeNodeBean> getTrimmedTree(int treeId);

    public List<TreeNodeBean> getTreeNodes(int treeId);

    public Pair<Integer, List<String>> addBikDocument(int parentNodeId, String caption, String href, String nodeKindCode);

    public int addBikDocumentAdHoc(String caption, String href, String nodeKindCode);

    public void addBikEntityAttribute(String name, String value, int siId);

    public void addBikEntityAttrAssociateRoleWithAnObj(String name, String value, int siId);

    public List<UserBean> getBikAllUsers();

    public void addBikUser(String name, String mail, String phone, String desc, int nodeId, String loginAD);

    public void addBikEntityNote(String title, String body, int nodeId);

    public List<String> updateBikUser(int id, String oldLoginName, String loginName, String name, String password, String mail, String phone, String desc);

    public Pair<Integer, List<String>> addBikBlogEntry(int parentNodeId, String content, String subject);

    public List<String> editBikBlogEntry(int id, String content, String subject, int nodeId);

    public void addBikEntry(int cuId, int id, boolean inheritToDescendants);

    public void deleteBikLinkedEntry(int cuId, int id);

    public ArticleBean getBikBizDefForNodeId(int siId);

    public AttachmentBean getBikDocumentForSiId(int siId);

//    public List<JoinedObjBean> getBikArticle(int siId);
    public void addBikArticle(String title, String body, Set<String> tags, int official, int node, String version);

    public List<String> deleteBikArticle(int id, int nodeId);

    public void deleteBikNote(int id);

    public void editBikEntityNote(int id, String title, String body);

    public void deleteBikLinkedMeta(Integer cuId, int id);

    public void deleteBikLinkedAtr(Integer cuId, int id, String value);

    public void editBikEntityAttribute(int id, int cuId, String atrLinValue);

    public List<String> editBikEntityAttrAssociateRoleWithAnObj(int id, int cuId, String atrLinValue, List<String> rightRolesBefore);

    public Pair<Integer, List<DataSourceStatusBean>> getBikDataSourceStatuses(int pageNum, int pageSize);

    public List<TreeNodeBean> getBikAllTreeNodes();

    public List<TreeNodeBean> getBikAllTreeNodesExcludingOneTree(int treeId);

    public List<TreeNodeBean> getBikAllRootTreeNodesExcludingOneTree(int treeId, Integer nodeKindId, String relationType, String attributeName, List<Integer> parentNodeIdsDirectOverride /*, boolean isHideDictionaryRootTab*/);//tymczasowe do ukrycia słowników

    public void addBikJoinedObject(int srcNodeId, int dstNodeId,
            boolean inheritToDescendants);

    public void deleteBikJoinedObjs(int srcNodeId, int dstNodeId);

//    public StartupDataBean getStartupData();
    public Pair<Integer, List<String>> createTreeNodeBiz(TreeNodeBean repo, ArticleBean param, int action, String nodeKindCode);

    public List<TreeNodeBean> getBikDocumentTreeNodes();

    public List<TreeNodeBean> getBikRolesTreeNodes(boolean onlyTopRoles);

    public List<TreeNodeBean> getBikDocumentRootTreeNodes();

    public DynamicTreesBigBean getBikTreeForData();

    public List<String> getBikAllArts(int treeId);

    public void updateBikTree(int id, Map<String, String> langToName, String parentMenuCode, int visualOrder, int isAutoObjId,
            Map<String, String> optMenuSubGroup, String treeKind, String optServerLinkFileName);

    public void addBikTree(String name, int nodeKindId);

    public void deleteTree(Integer parseInt);

    public TreeKindBean addOrUpdateTreeType(List<TreeKindBean> tkbs, Integer kindId);

    public void sendMail(String subject, String body, String... recipients);

    public void sendMailWithFiles(String subject, String body, Map<String, String> embeddedFiles, String... recipients);

    public void sendMailWithCCAndFiles(String subject, String body, Map<String, String> embeddedFiles, String[] recipients, String[] cc);

    public Integer deleteJoinedObjectWithRoleFromUser(int nodeId, int roleInKindId, int inheritToDescendants, boolean isAuxiliary, int userNodeId);

    public boolean addUserInNode(int nodeId, Integer userId, int roleForNodeId, int inheritToDescendants, boolean isAuxiliary, JoinTypeRoles joinTypeRoles);

    public void addVote(int nodeId, int value);

    public void deleteLoggedUserVoteForNode(int nodeId);

    public List<TreeNodeBean> getBikAllJoinableTreeNodes();

    public List<TreeNodeBean> getBikAllJoinableRootTreeNodes(Set<String> treeKind, boolean noLinkedNodes, Integer nodeKindId, String relationType, Integer treeId);

    public UserBean bikUserByNodeId(int nodeId);

    public UserBean bikUserByLogin(String loginName);

    public Pair<Integer, List<String>> addBikTreeNodeUser(TreeNodeBean repo, UserExtBean user);

    public List<BikRoleForNodeBean> getBikRolesForNode();

    public int updateMetapediaArt(int id, String subject, String body, int official, int nodeId,
            int action);

    public BlogBean getBikEntryByNodeId(int nodeId);

    public void addBikAuthorsBlog(int nodeId, int userId, boolean isAdmin);

    public void deleteBikAuthorsBlog(int nodeId, int userId);

    public NewDefinitionDataBean getNewDefinitionData(int treeId);

    public void runTeradataPump();

    public void runTeradataDataModelPump();

    public void runErwinDataModelPump();

    public void runOraclePump(String instanceName);

    public void runPostgresPump(String instanceName);

    public void runProfilePump();

    public void runMSSQLPump(String instanceName);

    public void runJdbcPump(String instanceName);

    public void runPlainFilePump(String instanceName);

    public ConnectionParametersPlainFileBean addPlainFileConfig(String serverName);

    public void setPlainFileConnectionParameters(ConnectionParametersPlainFileBean bean);

    public Pair<Integer, String> runPlainFileTestConnection(int id);

    public void deletePlainFileConfig(String sourceName, int id, String serverName);

    public void runFileSystemPump(String instanceName);

    public void runDynamicAxPump(String instanceName);

    public void runDQCPump();

    public void runADPump();

    public void runSAPBWPump();

    public void runConfluencePump();

    public void runSapBOPump(String instanceName, String source);

    public void runSapBOReportPump(String instanceName, String source);

    public void runSapBODesignerPump(String instanceName, String source);

    public void runSapBOIDTPump(String instanceName);

    public void runSapBOPumpAll(String instanceName, String source);

    public void runPermissionsPump(String sqlName);

    public void runAll();

    public Pair<Integer, String> runSapBoTestConnection(int id);

    public Pair<Integer, String> runTeradataTestConnection();

    public Pair<Integer, String> runDQCTestConnection();

    public Pair<Integer, String> runADTestConnection();

    public Pair<Integer, String> runSASTestConnection();

    public Pair<Integer, String> runSapBWTestConnection();

    public Pair<Integer, String> runConfluenceTestConnection();

    public Pair<Integer, String> runMsSqlTestConnection(int id, String optObjsForGrantsTesting);

    public Pair<Integer, String> runOracleTestConnection(int id, String optObjsForGrantsTesting);

    public Pair<Integer, String> runPostgresTestConnection(int id, String optObjsForGrantsTesting);

    public Pair<Integer, String> runProfileTestConnection();

    public Pair<Integer, String> runJdbcTestConnection(int id);

    public Pair<Integer, String> runFileSystemTestConnection(int id);

    public List<String> getActiveSystemInstance(String source);

    public void updateBikAuthorsBlogToAdmin(int nodeId, int userId);

    public String setUserAvatar(String avatar);

    public Pair<Integer, List<String>> promoteMetapediaArt(TreeNodeBean repo, int id, String subject, String body, int official, int nodeId,
            int action, String versions);

    public void setSapBoConnectionParameters(ConnectionParametersSapBoBean bean);

    public void setTeradataConnectionParameters(ConnectionParametersTeradataBean bean);

    public void setDQCConnectionParameters(ConnectionParametersDQCBean bean);

    public void setADConnectionParameters(ConnectionParametersADBean bean);

    public void setOracleConnectionParameters(ConnectionParametersDBServerBean bean);

    public void setPostgresConnectionParameters(ConnectionParametersDBServerBean bean);

    public void setProfileConnectionParameters(ConnectionParametersProfileBean bean);

    public void setMsSqlConnectionParameters(ConnectionParametersDBServerBean bean);

    public void setMsSqlExPropParameters(MsSqlExPropConfigBean bean);

    public void setSASConnectionParameters(ConnectionParametersSASBean bean);

    public void setSapBWConnectionParameters(ConnectionParametersSapBWBean bean);

    public void setConfluenceConnectionParameters(ConnectionParametersConfluenceBean bean);

    public ConnectionParametersSapBoBean addBOConfig(String sourceName, String configName);

    public ConnectionParametersDBServerBean addDBConfig(String sourceName, String configName, boolean needSeparateSchedule, String treeCode);

    public void deleteBOConfig(int configId);

    public void deleteDBConfig(int configId, String sourceName, String serverName);

    public List<ConfluenceObjectBean> getConfluenceRootObjects();

    public List<ConfluenceObjectBean> getConfluenceChildrenObjects(String parentId, String parentKind);

    public List<ConfluenceObjectBean> getConfluenceFiltredObjects(final String val, String ticket);

    public List<SystemUserGroupBean> getGroupRootObjects();

    public List<SystemUserGroupBean> getGroupChildrenObjects(String objId, int id);

    public void refreshConfluenceObject(int nodeId);

    public void saveConfluenceObjects(Map<String, String> objectsIds, Integer parentId, int tree_id);

    public void publishArticleToConfluence(String biksUrlBase, String articleName, String articleBody, String confluenceLogin, String confluencePassword, String parentObjId, String space, Integer parentNodeId, int treeId);

    public Map<String, List<String>> getMSSQLServerAndDatabasesForBOConnection();

    public Set<String> getOracleServerForBOConnection();

    public void setMSSQLServerAndDatabasesForBOConnection(int nodeId, String serverName, String databaseName, String schemaName);

    public void setOracleServerForBOConnection(int nodeId, String serverName);

    public Integer getLastSourceStatus(String source);

    public List<TreeNodeBean> getOneSubtreeLevelOfTree(int treeId, Integer parentNodeId, boolean noLinkedNodes,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, boolean calcHasNoChildren, Integer nodeKindId, String relationType, String attributeName, Integer outerTreeId);

    public Map<Integer, List<Integer>> getBikNodeAncestorIdsMulti(Collection<Integer> nodeIds);

    public List<Integer> getBikNodeAncestorIds(int nodeId);

    public List<TreeNodeBean> getSubtreeLevelsByParentIds(Collection<Integer> parentNodeIds,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String optRelationType, String optAttributeName, Integer nodeKindId);

    public List<TreeNodeBean> getBikFilteredNodes(int treeId, String val, final Integer optSubTreeRootNodeId,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode,
            String ticket, Integer optNodeKindId, String optRelationType, String optAttributeName);

    public List<SystemUserGroupBean> getUserGroupFilteredNodes(String val, String ticket);

    public List<AppPropBean> setEditableAppProps(Map<String, String> modifiedOrNewProps);

    // offset liczony od 0 (domyślna wartość)
    public SearchFullResult newSearch(String text, int offset, int limit,
            Collection<Integer> optTreeIds,
            Collection<Integer> optKindIdsToSearch,
            Collection<Integer> optKindIdsToExclude,
            boolean calcKindAndCounts,
            Collection<ISearchByAttrValueCriteria> optSBAVCs,
            String ticket, String opExpr,
            boolean useLucene);

    public List<AttributeAndTheirKindsBean> getBikAttributeAndTheirKinds();

    public List<NodeKindBean> getNodeKinds();

    public List<NoteBean> getNotesByUser();

    public void addNewRole(String caption, int attrCategoryId);

    public void updateRole(int id, String caption, int attrCategoryId);

    public void deleteRole(int id);

    public void updateDescription(NameDescBean nameDesc, TreeNodeBean node);

    public void updateDescriptionForMetadata(NameDescBean nameDesc, TreeNodeBean node);

    public Pair<Integer, Boolean> addLinkUser(int linkedId, String name, int parentNodeId, int treeId);

    public List<AttrHintBean> getAttributes();

    public void updateAttrHint(String name, String hint, int isBuiltIn);

    public void changeInheritance(int idBikJoinedObj, int inherit);

    public int getCountOfLinkedElements(int idAtr);

    public BISystemsConnectionsParametersBean getBISystemsConnectionsParameters();

//    public List<SystemUserBean> getAllBikSystemUser();
    public Integer addBikSystemUser(String loginName, String password, boolean isDisabled, Integer userId, Set<String> userRightRoleCodes,
            Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries, List<Integer> usersGroupsIds, boolean doNotFilterHtml) throws ValidationExceptionBiks;

    public void updateBikSystemUser(int id, String loginName, String password, boolean isDisabled, Integer userId, String systemUserName,
            Set<String> userRightRoleCodes, Set<Integer> treeIds, boolean isSendMails, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries,
            List<Integer> usersGroupsIds, boolean doNotFilterHtml) throws ValidationExceptionBiks;

    public void deleteSystemUser(Integer systemUserId);

    public void updateBikSystemUser(int id, String loginName, String password, boolean isSendMails);

    public List<TreeNodeBean> getBikUserTreeNodes(Integer optUpSelectedNodeId);

    public List<TreeNodeBean> getNotConnectedUsers();

    public List<TreeNodeBean> getBikUserRootTreeNodes(Integer optUpSelectedNodeId);

    public Map<Integer, List<Integer>> getAttributeDict();

    public List<RoleForNodeBean> getRoleForNode();

    public List<String> getAncestorNodePathByNodeId(int nodeId);

    public void updateItemInCategory(int itemId, int categoryId, String name, String attrType, String valueOpt, boolean displayAsNumber, boolean usedInDrools);

    public void updateJoinedObjsForNode(int nodeId, boolean noLinkedNodes, Map<Integer, JoinedObjMode> directOverride, Map<Integer, Pair<JoinedObjMode, Boolean>> subNodesOverride);

    public Integer addOrUpdateCategory(Integer id, String name);

    public Integer addAttributeDict(String itemName, int categoryId, String typeAttr, String attrValueList, boolean displayAsNumber, boolean usedInDrools);

    public List<AttributeBean> getAttrDefAndCategory();

    public List<AttributeBean> getAttrDefBuiltAndCategory();

    public void addOrDeleteAttributeDictForKind(Map<Integer, Integer> atrIdAndCnt, Set<Integer> selectedNodeKindIds);

    public void modifySystemAttrVisibleForKind(Map<Integer, Integer> atrIdAndCnt, Set<Integer> selectedNodeKindIds);

    public void deleteAttributeDict(int atrId);

    public Map<Integer, String> getNodeNamesByIds(Collection<Integer> nodeIds);

    public List<TreeNodeBean> getBikFilteredNodesWithCondidtion(String val, final Integer optSubTreeRootNodeId, Collection<Integer> treeIds, boolean noLinkedNodes,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String ticket, Integer optNodeKindId, String optRelationType, String optAttributeName);

    public IndexCrawlStatus getFullTextCrawlCompleteStatus(boolean useLucene);

    public Set<Integer> getAlreadyJoinedUserIds();

    public boolean cancelDBJob(String ticket);

//    public List<SystemUserBean> getAdminOrExpertBikSystemUser();
    public List<VoteBean> getVotesByUser();

    public void deleteCategory(int id);

    public boolean isOldPasswordTrue(String password, int id);

    public void setNodeVisualOrder(int nodeId, int visualOrder);

    public boolean updateUserRolesForNode(int nodeId, boolean noLinkedNodes, Map<Integer, JoinedObjMode> directOverride, Map<Integer, Pair<JoinedObjMode, Boolean>> subNodesOverride, int roleId, Set<String> treeKind, boolean isAuxiliary, JoinTypeRoles joinTypeRole, Collection<Integer> alreadyJoinedNode);

    public void resetVisualOrderForNodes(Integer parentId, int treeId);

    @DisableCallLoggingForServiceImplMethod
    public String keepSessionAlive(String dummy);

    @DisableCallLoggingForServiceImplMethod
    public ChangedRankingBean getChangedRanking(long timeStamp);

    public Pair<String, String> getAppVersion();

    public Pair<Map<String, AttributeBean>, Map<String, List<TreeNodeBean>>> getBikAttributesNameAndType(Integer nodeId, Integer nodeKindId, String relationType, Integer treeId, Map<String, List<Integer>> usedLink);

    public HelpBean getHelpDescr(String helpKey, String caption);

    public HelpBean getMultipleHelpDescr(String helpKey, String caption, String lang);

    public void addOrUpdateBikHelp(HelpBean hb);

    public List<HelpBean> getHelpHints();

    public void updateBikHelpHint(HelpBean hb);

    public Map<Integer, Integer> getDescendantsCntInMainRoles(int nodeId);

    public boolean deleteUserInNode(int nodeId, Integer userId, int roleInKindId);

    public Integer getDescendantsCntInMainRoleByUserRoleAndTreeKind(int userId, int roleForNodeId, Set<String> treeKindForSelected);

    public List<NewsBean> getNewsByUser();

    public NewsBean markNewsAsReadAndGetNew(int newsId);

    public NewsBean addNews(NewsBean news);

//    public NewsBean getNewsToEdit(int id);
    public void updateNews(NewsBean news);

    public void deleteNews(int newsId);

    public void insertBIKStatisticsEveryTimeOneDay(String counterName, String parameter);

    public List<ObjectInFvsChangeBean> getChangedDescendantsObjectInFvs(int nodeId, int userId);

    public void insertBIKStatisticsTabAndNode(String caption, String description, String parametr);

    public void insertBIKStatisticsExtTabAndNode(int nodeId, String branchIds);

    public List<StatisticsBean> getStatisticsDict();

    public void deleteAllChangedObjectInFvsInFvs(int nodeId);

    public void deleteChangedObjectInFvs(int nodeId);

    public void deleteAllChangedObjectForUser();

    public void copyAllRolesUser(int userIdFrom, int userIdTo, Boolean isMoveMain, Boolean isCopyAuxiliary);

    public List<String> getAllUserLogged();

    public List<TutorialBean> getTutorial();

    public void markTutorialAsRead(int tutorialId);

    public List<NodeAuthorBean> getRankUsers();

    public List<HomePageBean> getHomePageHints();

    public Set<String> getShortCodesForMyBIKSTabsWithNewItems();

    public Pair<Integer, List<SchedulePumpBean>> getSchedule(int pageNum, int pageSize);

    public void setSchedule(List<SchedulePumpBean> bean);

    public Pair<Integer, String> getLastInstanceForRaport(String reportId, int nodeId);

    public List<String> getSearchHints();

    public List<SystemUserBean> getAllBikSystemUserWithFilter(String ticket, String textOfFilter,
            Boolean isDisabled, Boolean hasCommunityUser, RightRoleBean rightRole,
            Set<Integer> selectedAuthorTreeIDs, Integer optCustomRightRoleId, boolean showUserInRoleFromGroup);

    public List<TreeNodeBean> getChildrenNodeBeansByParentId(int parentNodeId);

    public List<String> deleteTreeNodeWithTheUserRole(int id, String branchIds);

    public List<String> updateTreeNodeWithTheUserRole(int id, String name);

    public Pair<Pair<Integer, Integer>, List<String>> createTreeNodeWithTheUserRole(Integer parentNodeId, int nodeKindId, String name, String objId, int treeId);

    public List<TreeNodeBean> getBikUserRolesTreeNodes(Integer optUpSelectedNodeId, String showOnlyRoleByCode, boolean useLazyLoading);

    public boolean deleteBadLinkedUsersUnderRole();

    public void resetPumpThread();

    public Set<String> getAvailableConnectors();

    public Set<String> getIcons();

    public void changeToBranchOrLeaf(int nodeId, boolean changeBranchToleaf);

    public void setBODesignerPath(String path, String source);

    public void setReportSDKPath(String path);

    public void setPath(String path, String pathName);

    public Map<String, TreeKindBean> getTranslatedTreeKinds(int treeKindId);

    public Map<String, String> getTranslatedTrees(int treeId);

    public void updateTutorial(int id, String title, String text);

    public ErwinDiagramDataBean getErwinListTables(String areaId);

    public List<ErwinDataModelProcessObjectsRel> getDataModelProcessObjectsRel(int nodeId, String areaName);

    public void setConnectionParametersErwinBean(ConnectionParametersErwinBean bean);

    public void setPermissionsConnectionParameters(ParametersAnySqlBean bean);

    public Integer getNodeId(String objId, String treeCode);

    public List<NodeKindBean> getNodeKindsIconsForTree(int treeId, String treeKind);

    public void setNodeKindsIconsForTree(int treeId, List<NodeKindBean> icons);

    public Integer findAnticipatedNodeOfTree(int treeId, Integer optAncestorNodeId, String anticipatedNodeName);

    public List<AttrSearchableBean> getAttributesBySelectedTreeNode(Collection<Integer> treeIds, Set<Integer> kindsToSearchFor);

    public Integer updateConfluenceNode(TreeNodeBean node);

    public List<MyObjectsBean> getMyObjects();

    public List<UserObjectBean> getUsersObjects();

    public void registerNodeActionCodes(Set<String> codes);

    public Set<TreeNodeBranchBean> getRutEntries();

    public void updateRutEntries(Set<Pair<Integer, Integer>> newRutEntries);

    public String getLoggedUserName();

    public List<NodeModificationLogEntryBean> getModifiedNodesFromLog(int treeId, long timeStamp);

    public void registerChangeInTrees(Long serverTimeStamp, Set<Integer> treeIds);

    public void importCSVDataToTree(final String treeCode, final String filePath);

    public void forSerializationOnly() throws ServiceMethodCallException;

    public SystemUserGroupBean getSystemUsersGroup(int groupId);

    public void updateSystemUserGroup(int groupId, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries);

    public void updateProfileInRole(int groupId, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries);

    public ConnectionParametersJdbcBean addJdbcConfig(String sourceName, String serverName);

    public void setJdbcConnectionParameters(ConnectionParametersJdbcBean bean);

    public void deleteJdbcConfig(String sourceName, int id, String serverName);

    public ConnectionParametersFileSystemBean addFileSystemConfig(String serverName);

    public void setFileSystemConnectionParameters(ConnectionParametersFileSystemBean bean);

    public void deleteFileSystemConfig(String sourceName, int id, String serverName);

    public List<TreeNodeBean> getRootTreeNodes4ApplyForExtendedAccess(Set<String> treeCodes);

//    public List<AttributeUsageBean> getAttributeUsages();
    public void calculateAttributeUsages();

    public List<TreeNodeBean> getAttributeUsageTreeNodes();

    public List<AttributeValueInNodeBean> getAttributeUsingNodes(TreeNodeBean nodeBean);

    public void setCrrLabelsOnNode(Set<String> labels, int nodeId);

    public Set<String> getCrrLabelCodes();

    public String performFullSearch(String query, int allCnt, Set<Integer> treeIds,
            Set<Integer> kindsToSearchFor, Set<Integer> kindIdsToExclude,
            boolean calcKindCounts, List<ISearchByAttrValueCriteria> sbavcs,
            String ticket, String opExpr,
            boolean useLucene);

    public void clearSearchResultsCache(String ticketForFullSearch);

    public List<SystemUserBean> getADUsers(String optfilter);

    public SystemUserBean getOrCreateSystemUserBeanByLogin(String loginName, String optSsoDomain);

    public List<FvsChangeExBean> getFavouritesExForUser();

    public List<ObjectInFvsChangeExBean> getChangedDescendantsObjectInFvsEx(int nodeId, int userId);

    public Pair<List<ConnectionParametersSapBoBean>, ConnectionParametersBIAdminBean> getBIAdminConnectionParameters();

    public void setBIAdminConnectionParameters(ConnectionParametersBIAdminBean bean);

    public List<SAPBOScheduleBean> getBIASchedules(SAPBOScheduleBean filter);

    public void pauseBIAInstances(List<Integer> instancesToPause);

    public void resumeBIAInstances(List<Integer> instancesToResume);

    public void deleteBIAInstances(List<Integer> instancesToDelete);

    public void rescheduleBIAInstances(List<Integer> instancesToReschedule, SAPBOScheduleBean param);

    public List<FvsChangeExBean> getSuggestedElementsForLoggedUser();

    public void setBIAdminArchiveConfig(BIArchiveConfigBean bean);

    public BIArchiveConfigBean getBIAdminArchiveConfig();

    public List<TreeNodeBean> getBikTreeInBIAdmin(BikNodeTreeOptMode extraMode);

    public List<SAPBOObjectBean> getBIAObjects(SAPBOObjectBean filter);

    public List<SAPBOObjectBean> getBIAUsers();

    public void stopBIAQuery();

    public String runBIArchiveTestConnection();

    public void changeBIANameAndDescr(Set<SAPBOObjectBean> objs, Integer manageType);

    public void changeOwnerBIAObjects(List<Integer> objs, SAPBOObjectBean newName);

    public Pair<Integer, List<BIAArchiveLogBean>> getBIAArchiveLogs(int pageNum, int pageSize);

    public List<ObjectInFvsChangeBean> getSimilarFavouriteWithSimilarityScore(int nodeId);

    public ConnectionParametersAuditBean getBIAuditDatabaseConnectionParameters();

    public void setBIAuditDatabaseConnectionParameters(ConnectionParametersAuditBean bean);

    public List<AuditBean> getBIAAuditInformation();

    public Pair<List<UserInNodeBean>, Map<Integer, String>> getUsersInNode(int nodeId, String branchIds);

    public List<AttachmentBean> getAttachments(int nodeId, Integer linkingParentId);

    public List<BlogBean> getBlogs(int nodeId, Integer linkingParentId);

    public List<BlogBean> getUserBlogs(int nodeId);

    public List<NoteBean> getNotes(int nodeId);

    public List<JoinedObjBean> getJoinedObjs(int nodeId, Integer linkingParentId);

    public Pair<Integer, String> runBIAAuditTestConnection();

    public String getBIAInstanceTreeCode(String typeName);

    public ConnectionParametersDQMConnectionsBean addDQMConnections(String name);

    public void deleteDQMConnections(int id);

    public void updateDQMConnections(ConnectionParametersDQMConnectionsBean cpDQM);

    public Pair<Integer, String> runDQMTestConnection(int id, String optObjsForGrantsTesting);

    public int addJoinedObjAttribute(String name, String type, String valueOptList, String valueOptTo, boolean usedInDrools);

    public List<AttributeBean> getJoinedObjAttribute();

    public void deleteJoinedObjAttribute(int attrId);

    public void updateJoinedObjAttribute(int itemId, String name, String type, String valueOptList, String valueOptTo, boolean usedInDrools);

    public List<JoinedObjAttributeLinkedBean> getJoinedAttributesLinked(int joinedObjId);

    public Pair<List<JoinedObjAttributeLinkedBean>, Map<String, AttributeBean>> getJoinedAttributesLinkedAndoinedObjAttributes(Integer joinedObjId);

    public void addJoinedAttributeLinked(int joinedObjId, List<JoinedObjAttributeLinkedBean> joal);

    public ConnectionParametersDynamicAxBean addDynamicAxConfig(String serverName);

//    public List<Integer> getBindedTreeId(Integer currentTreeId);
    public void deleteTmpFiles(Collection<String> tmpFileName2Delete);

    public DroolsConfigBean getDroolsConfig();

    public void setDroolsConfig(DroolsConfigBean bean);

    public String resetDrools();

    public Pair<Integer, List<DroolsLogBean>> getDroolsLogs(int pageNum, int pageSize);

    public boolean checkTreeAttributesCompatibilitiy(String treeKind, String serverTreeFileName);

    public boolean checkJoinedObjsAttributesCompatibilitiy(Map<Integer, String> bindingTreeIdWithFile);

    public void importLinksAndJoinedObjs2Tree(Integer treeId, Map<Integer, String> bindingTreeIdWithFile);

    public void calculateObjIds(Integer treeId, Set<Integer> allTreeIds);

    public List<String> getImportTreeLog(Integer treeId);

    public void setTreeImportStatus(int treeId, TreeImportLogStatus status);

    public Integer addSystemUserGroup(String name, Integer parentId);

    public Integer addSystemRole(String name, String desc, Integer parentId);

    public void addSystemUserToGroup(String groupName, List<Integer> userIds);

    public Integer renameUserGroup(SystemUserGroupBean sub);

    public void deleteUserGroup(int id);

    public List<Integer> getUsersGroups(int userId);

    public void copyPermissionFromUser(int userId, int groupId);

    public List<TreeBean> getImportableTrees();

    public List<AttributeBean> getManagmentAttrubutes();

    public void updateAttributes(AttributeBean ab);

    public List<UserExtradataBean> getADLogin(String query);

    public UserExtradataBean getInfoFromADByLogin(String loginAd);

    public Set<Integer> checkAutoObj4Tree(Set<Integer> allTreeIds);

    public void importTreeWithLinkedAndJoinedObjs(int treeId, Integer isIncremental, String serverFileName2ImportTree, String serverMetadataFileName, Map<Integer, String> bindingTreeIdWithFile);

    public BiksSearchResult search(String searchMode, Integer searchId, String text, Set<Integer> selectedTreeIds, Set<String> selectedAttributeName, List<String> selectedOtherFilter, Set<Integer> nodeKindIds, int pageNum, int pageSize) throws LameRuntimeException;

    public void automaticFillAttributesForNode(Set<TreeNodeBranchBean> tnb, AttributeBean attrs, boolean addAttribute);

    public void automaticUpdateJoinedObjsForNodeInner(int srcId, Set<TreeNodeBranchBean> tnbs);

    public void automatiAddJoinedAttributeLinked(int nodeId, Set<TreeNodeBranchBean> tnbs, List<JoinedObjAttributeLinkedBean> joal);

    public void automaticDeleteAttributesForNode(Set<TreeNodeBranchBean> tnb, AttributeBean atr);

    public List<AttributeBean> getRequiredAttrs(int nodeKindId);

    public List<AttributeBean> getRequiredAttrsWithVisOrder(int nodeKindId);

    public String compileJasper(Integer nodeId, String reportName, String params);

    public String getNodeAssociationScript(Integer nodeId);

    public void updateJoinedObjsFromParent(Integer nodeId, Set<Integer> dstId2Add, Set<Integer> dstId2Delete);

    public String getConfigurationTree(String appProp);

    public void changeConfigurationTree(String appProp, String treeCode) throws LameRuntimeException;

//    public void implementConfigTree(Map<Integer, Integer> attrDictConfMap, Map<Integer, Integer> attrConfMap, Map<Integer, Integer> nodeKindConfMap, Map<Integer, Integer> treeKindConfMap) throws LameRuntimeException;
    public void implementConfigTree(Map<Integer, Integer> attrDictConfMap, Set<Integer> attrDictIds2Delete, Map<Integer, Integer> attrDefConfMap, Set<Integer> attrDefIds2Delete, /*Map<Integer, Integer> attrConfMap, Set<Integer> attrIds2Delete, */ Map<Integer, Integer> nodeKindConfMap, Set<Integer> nodeKindIds2Delete, Map<Integer, Integer> treeKindConfMap, Set<Integer> treeKindIds2Delete) throws LameRuntimeException;

    public void buildConfigTree(boolean deleteOldTree) throws LameRuntimeException;

//    public void buildTmpConfigTree();
    public Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>> getAttributeDictOverView() throws LameRuntimeException;

    public Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>> getAttributeDefOverView() throws LameRuntimeException;

    public Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>> getNodeKindsOverView() throws LameRuntimeException;

    public Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>> getTreeKindsOverView() throws LameRuntimeException;

    public List<TreeNodeBean> getNodesByLinkedKind(int srcNodeKindId, String relationType, int treeId);

    public String compileReport(int nodeId, String reportURL) throws LameRuntimeException;

    public List<TreeNodeBean> getBikAllJoinableNodeListByLinkedKind(int srcNodeKindId, int treeId, String relationType);

    public List<TreeNodeBean> getKindsForLinkNodeKind(Integer srcNodeKindId, int treeId, String relationType);

    public List<TreeNodeBean> getInnerJoinKindsForLinkNodeKindAndHyperlink(Integer nodeKindIdOuter, Integer nodeKindIdInner, int treeIdOuter, int treeIdInner, Pair<String, String> relationTypes, String optAttributeName);

    public Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>> getAttribute4NodeKindOverView() throws LameRuntimeException;

//    public void changeMenuConfigTree(String treeCode) throws LameRuntimeException;
//    public String getMenuConfigurationTree();
    public void buildMenuConfigTree(boolean deleteOldTree) throws LameRuntimeException;

    public void grantRightsFromContextForUser(Map<SystemUserBean, List<RightRoleBean>> map, int treeId, Integer nodeId, String rightsProc);

    public void grantRightsFromContextForUserNewRightsSys(List<SystemUserBean> usersList, int nodeId, int roleId, String rightsProc);

    public Map<RightRoleBean, List<SystemUserBean>> getAssignedToNodeUsers(int treeId, Integer nodeId);

    public List<RightRoleBean> getAllUserRoles();

    public Map<SystemUserBean, List<RightRoleBean>> getUsersMappedToRoles(String textOfFilter, Boolean isDisabled,
            Boolean hasCommunityUser, RightRoleBean rightRole, Set<Integer> selectedAuthorTreeIDs, Integer optCustomRightRoleId, boolean showUserInRoleFromGroup);

    public List<SystemUserGroupBean> getAllUserGroups();

    public List<SystemUserGroupBean> getRolesAssociatedWithObj(String nodeKindCode);

    public void grantRightsFromContextForGroup(Map<RightRoleBean, List<SystemUserGroupBean>> rightToGroupMap, int treeId, Integer nodeId, String rightsProc);

    public Set<String> getCurrentUserCustomRightRoleInGroup(int userId);

    public Integer isDeletedNode(Integer nodeId);

    public void implementConfigTree() throws LameRuntimeException;

    public void implementMenuConfigTree() throws LameRuntimeException;

    public List<TreeNodeBean> getAllTreeNodes(Integer treeId);

    public List<TreeNodeBean> getAllTreeNodesToExport(Integer treeId, String branchId);

    public void addSubmenu(Integer parentMenuId, String name);

    public void runScript(Integer nodeId, String script);

    public ParametersAnySqlBean addAnySql(String sourceName, String serverName);

    public void setAnySqlParameters(ParametersAnySqlBean bean);

    public void deleteAnySql(String sourceName, int id, String serverName);

    public Pair<Integer, List<MailLogBean>> getMailLogs(int pageNum, int pageSize);

    public void updateRoleDescription(NameDescBean nameDesc, Integer id);

    public String getRoleDescr(Integer id);

    public List<SystemUserBean> getAllUsersWithRightsInCurrentNode(int node_id, int group_id);

    public List<TreeNodeBean> getAttributeOrKindObjectToConfigPrintTemplate(Integer nodeId, Integer nodeIdObjectKind, String nodeKindCode, String nodeKindCodeFromAttrLis, String parentName);

    public Integer createTreeNodesAttribute(Integer parentNodeId, List<Integer> directOverride, int nodeKindId);

    public Map<String, PrintTemplateBean> getPrintTemplates(String nodeKindCode);

    public void resetVisualOrder(Integer parentId, int treeId);

    public void setStatus(int nodeId, Map<String, AttributeBean> statusAttrs);
}
