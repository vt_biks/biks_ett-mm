/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.text.Collator;
import java.util.Locale;
import simplelib.IStringComparator;

/**
 *
 * @author tflorczak
 */
public class JavaStringComparator implements IStringComparator {

    private static final Collator collatorInstance = Collator.getInstance(Locale.getDefault());

    public int compare(String source, String target) {
        return collatorInstance.compare(source, target);
    }
//    public static Collator getInstance() {
//        return collatorInstance;
//    }
}
