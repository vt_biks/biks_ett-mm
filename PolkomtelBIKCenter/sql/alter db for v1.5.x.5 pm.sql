exec sp_check_version '1.5.x.5';
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
-- 1. pomocnicza funkcja escape-uj�ca sql-stringi
-- 2. poprawka to tzw. kibla translacyjnego
--      z atrybutami wyszukiwalnymi (brakowa�o t�umacze�)
-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fn_escape_and_quote_str]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [fn_escape_and_quote_str]
GO

create function fn_escape_and_quote_str(@str varchar(max)) returns varchar(max) as
begin
  return case when @str is null then 'null' else '''' + replace(@str, '''', '''''') + '''' end
end
go


/*

  teraz mo�na po kozacku takie co� sobie pisa�:

select 'insert into bik_help (help_key, caption, text_help, is_hidden, lang) values
(' +  dbo.fn_escape_and_quote_str(help_key) + ', ' +
  dbo.fn_escape_and_quote_str(caption) + ', ' +
  dbo.fn_escape_and_quote_str(text_help) + ', ' + cast(is_hidden as varchar(max)) + ', ' + 
  dbo.fn_escape_and_quote_str(lang) + ')'
from  bik_help where lang = 'en'

  pomocne!

*/

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

insert into bik_translation (code, lang, kind, txt) values ('Nazwa', 'en', 'adef', 'Name')
insert into bik_translation (code, lang, kind, txt) values ('Opis/Tre��', 'en', 'adef', 'Descr./Body')
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

exec sp_update_version '1.5.x.5', '1.5.x.6';
go
