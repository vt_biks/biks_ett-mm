﻿exec sp_check_version '1.2.4';
go

create table aaa_oracle (
	--id int not null identity(1,1),
	name varchar(50) not null,
	type varchar(30) not null,
	branch_names varchar(1000) not null,
	parent_branch_names varchar(1000) null,
	descr varchar(max) null,
	extra_info varchar(max) null,
	visual_order int not null default(0)
);
go

create table aaa_oracle_index (
	--id int not null identity(1,1),
	name varchar(50) not null,
	server varchar(500) null,
	owner varchar(50) not null,
	table_name varchar(50) not null,
	column_name varchar(50) not null,
	column_position int null,
	unique_flag varchar(20) null,
	descend varchar(20) null,
	constraint_type varchar(30) null
);
go

create table bik_oracle_extradata (
	node_id int not null primary key references bik_node (id),
	definition_sql varchar(max) null
);
go

create table bik_oracle_index (
	table_branch_names varchar(900) not null,
	column_name varchar(50) not null,
	name varchar(100) not null,
	column_position int not null,
	unique_flag varchar(20) null,
	descend varchar(20) null,
	constraint_type varchar(30) null
);
go

alter table bik_mssql
add visual_order int not null default(0);
go

insert into bik_data_source_def(description)
values('Oracle')
go

declare @oracleId int;
select @oracleId = id from bik_data_source_def where description = 'Oracle'

insert into bik_schedule(source_id, hour, minute, interval, is_schedule, priority)
values(@oracleId, 0, 0, 1, 0, 2)

alter table bik_db_server
add port int null;
go

insert into bik_db_server(source_id, name, server, instance, db_user, db_password, is_active, port)
values ((select id from bik_data_source_def where description='Oracle'), 'My First Server', '', 'orcl', '', '', 0, 1521)


insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
values('OracleServer', 'Serwer Oracle', 'oracleServer', 'Metadata', 0, 8, 0)

insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
values('OracleOwner', 'Schemat Oracle', 'oracleSchema', 'Metadata', 0, 9, 0)

insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
values('OracleTable', 'Tabela Oracle', 'oracleTable', 'Metadata', 0, 8, 0)

insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
values('OracleView', 'Widok Oracle', 'oracleView', 'Metadata', 0, 8, 0)

insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
values('OracleProcedure', 'Procedura Oracle', 'oracleProcedure', 'Metadata', 0, 8, 0)

insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
values('OracleColumn', 'Kolumna Oracle', 'oracleColumn', 'Metadata', 0, 7, 0)

insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
values('OracleColumnPK', 'Kolumna Oracle PK', 'oracleColumnPK', 'Metadata', 0, 7, 0)

insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
values('OracleColumnIDX', 'Kolumna Oracle IDX', 'oracleColumnIDX', 'Metadata', 0, 7, 0)

declare @OracleCode varchar(100) = 'Oracle';
declare @OracleCodeBase varchar(100) = @OracleCode;
declare @num int = 1;
while exists(select 1 from bik_tree where code = @OracleCode)
begin
	set @num = @num + 1;
	set @OracleCode = @OracleCodeBase + cast(@num as varchar(20))
end

-- jesli utworzono drzewo kategoryzacji o kodzie 'Oracle' to zmieniam jego nazwe na inna (z numerkiem)
if exists(select 1 from bik_tree where code = @OracleCodeBase)
begin
	update bik_tree
	set code = @OracleCode
	where code = @OracleCodeBase
end

insert into bik_tree (name, node_kind_id, code, tree_kind, is_hidden, is_in_ranking, branch_level_for_statistics, is_built_in)
values ('Oracle', null, 'Oracle', 'Metadata', 1, 0, 1, 0)

insert into bik_icon(name)
select distinct icon_name from bik_node_kind a 
left join bik_icon b on b.name = a.icon_name
where b.id is null

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')  

exec sp_add_menu_node 'databases', '@', '$Oracle'
exec sp_add_menu_node '$Oracle', '@', '&OracleServer'
exec sp_add_menu_node '&OracleServer', '@', '&OracleOwner'
exec sp_add_menu_node '&OracleOwner', '@', '&OracleTable'
exec sp_add_menu_node '&OracleOwner', '@', '&OracleView'
exec sp_add_menu_node '&OracleOwner', '@', '&OracleProcedure'
exec sp_add_menu_node '&OracleView', '@', '&OracleColumn'
exec sp_add_menu_node '&OracleTable', '@', '&OracleColumn'
exec sp_add_menu_node '&OracleTable', '@', '&OracleColumnPK'
exec sp_add_menu_node '&OracleTable', '@', '&OracleColumnIDX'

exec sp_node_init_branch_id @tree_id, null
go



if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_oracle]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_oracle]
go

create procedure [dbo].[sp_insert_into_bik_node_oracle]
as
begin

	declare @oracle_tree_id int;
	select @oracle_tree_id = id from bik_tree where code='Oracle'

	-- update
	update bik_node 
	set is_deleted = 0, descr = orcl.descr, visual_order = orcl.visual_order
	from aaa_oracle orcl
	where bik_node.tree_id = @oracle_tree_id
	and bik_node.obj_id = orcl.branch_names
	and bik_node.linked_node_id is null

	-- insert nowych
	insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id, visual_order)
	select null, bnk.id, orcl.name, orcl.descr, @oracle_tree_id, orcl.branch_names, orcl.visual_order
	from aaa_oracle orcl
	inner join bik_node_kind bnk on orcl.type = bnk.code
	left join (bik_node bn inner join bik_tree bt on bn.tree_id = bt.id and bn.tree_id = @oracle_tree_id) on bn.obj_id = orcl.branch_names
	where bn.id is null
	order by orcl.branch_names

	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join aaa_oracle as orcl on bik_node.obj_id = orcl.branch_names
	inner join bik_node bk on bk.obj_id = orcl.parent_branch_names 
	where bik_node.tree_id = @oracle_tree_id
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.tree_id = @oracle_tree_id
	and bk.is_deleted = 0
	and bk.linked_node_id is null

	-- usunięcie zbędnych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join aaa_oracle orcl on bik_node.obj_id = orcl.branch_names
	where orcl.name is null
	and tree_id = @oracle_tree_id
	and is_deleted = 0
	and linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @oracle_tree_id 
	and bn2.is_deleted = 1

	exec sp_node_init_branch_id @oracle_tree_id, null
	
end;
go



if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_oracle_extradata]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_oracle_extradata]
go

create procedure [dbo].[sp_insert_into_bik_node_oracle_extradata]
as
begin

	delete from bik_oracle_extradata
	delete from bik_oracle_index
	
	insert into bik_oracle_extradata(node_id, definition_sql)
	select bn.id, extra_info from aaa_oracle orcl
	inner join bik_node bn on orcl.branch_names = bn.obj_id
	where bn.tree_id = (select id from bik_tree where code = 'Oracle')
	and bn.is_deleted = 0
	and orcl.extra_info is not null
	and orcl.type in ('OracleProcedure', 'OracleView')
	
	insert into bik_oracle_index(table_branch_names, column_name, name, column_position, unique_flag, descend, constraint_type)
	select server + '|' + owner + '|' + table_name + '|' as table_branch_names, column_name, name, column_position, unique_flag, descend, constraint_type 
	from aaa_oracle_index
	
end;
go

-- poprzednia wersja w pliku alter db for v1.2.3.2 tf.sql
-- dodanie do wyszukiwarki extradaty z Oracle
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_metadata]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_metadata
go

create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
	-- SAP BO
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_extradata', 'node_id', 'author, owner, cuid, guid, ruid', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_engine, database_source, connetion_networklayer_name, user_name, server', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_schedule', 'node_id', 'owner, creation_time, nextruntime, expire, format, parameters', null, @optNodeFilter
	
	declare @obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as si_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''DataConnection'', ''Webi'', ''Flash'', ''CrystalReport'', ''Universe'', ''Excel'', ''FullClient'', ''Pdf'', ''Hyperlink'', ''Powerpoint'',
		''ReportFolder'', ''UniversesFolder'', ''ConnectionFolder'', ''ReportSchedule''))'
	exec sp_verticalize_node_attrs_one_metadata_table @obj_id_sql, 'id', 'si_id', null, @optNodeFilter
	
	-- Oracle
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_oracle_extradata', 'node_id', 'definition_sql', null, @optNodeFilter
	
	-- MS SQL
	declare @mssql_tree_id int = (select id from bik_tree where code = 'MSSQL')
	declare @mssql_sql varchar(max) = '(select bn.id, ms.definition_text from bik_mssql ms 
		inner join bik_node bn on bn.obj_id = ms.branch_names
		where bn.tree_id = ' +  cast(@mssql_tree_id as varchar(20)) + '
		and bn.is_deleted = 0
		and linked_node_id is null
		and ms.definition_text is not null)'
	exec sp_verticalize_node_attrs_one_metadata_table @mssql_sql, 'id', 'definition_text', null, @optNodeFilter
	  
	-- DQC
	declare @tree_id int = (select id from bik_tree where code = 'DQC')
	declare @inact_nk_id int = (select id from bik_node_kind where code = 'DQCTestInactive'),
    @succ_nk_id int = (select id from bik_node_kind where code = 'DQCTestSuccess'),
    @fail_nk_id int = (select id from bik_node_kind where code = 'DQCTestFailed')

	declare @dqc_src_sql varchar(max) = '(select cast(dt.long_name as varchar(max)) as long_name, cast(dt.sampling_method as varchar(max)) as sampling_method, cast(dt.verified_attributes as varchar(max)) as verified_attributes,cast(dt.expected_result as varchar(max)) as expected_result,cast(dt.logging_details as varchar(max)) as logging_details, cast(dt.benchmark_definition as varchar(max)) as benchmark_definition, cast(dt.results_object as varchar(max)) as results_object, cast(dt.additional_information as varchar(max)) as additional_information, n.id as node_id
		from bik_dqc_test dt inner join bik_node n on dt.__obj_id = n.obj_id and n.node_kind_id in (' +
		cast(@inact_nk_id as varchar(20)) + ',' + cast(@succ_nk_id as varchar(20)) + ',' + cast(@fail_nk_id as varchar(20)) + ') and tree_id = ' + cast(@tree_id as varchar(20)) + '
		where n.linked_node_id is null and n.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @dqc_src_sql, 'node_id', 'long_name, sampling_method, verified_attributes,expected_result,logging_details, benchmark_definition, results_object, additional_information', null, @optNodeFilter
	
	-- AD
	declare @ad_src_sql varchar(max) = '(select bu.email, coalesce(bu.phone_num, ad.telephone_number) as phone_num,
		ad.physical_delivery_office_name,ad.postal_code, ad.manager, 
		ad.description, ad.department, ad.title, ad.mobile, 
		ad.display_name, bu.node_id
		from bik_user bu
		join bik_node bn on bn.id = bu.node_id and bn.is_deleted = 0
		left join bik_system_user bsu on bsu.user_id = bu.id 
		left join bik_active_directory ad 
		on (bu.login_name_for_ad = ad.s_a_m_account_name or bsu.login_name = ad.s_a_m_account_name))'
	exec sp_verticalize_node_attrs_one_metadata_table @ad_src_sql, 'node_id', 'email, phone_num, physical_delivery_office_name,postal_code,manager, description, department, title, mobile, display_name', null, @optNodeFilter
	
	-- SAP BW query extradata
	declare @bwreports_id int = (select id from bik_tree where code = 'BWReports')
	declare @query_nk_id int = (select id from bik_node_kind where code = 'BWBEx')
	declare @sapbw_query_src_sql varchar(max) = '(select q.update_time, q.owner, q.last_edit, bn.id as node_id
		from bik_sapbw_query q inner join bik_node bn on q.obj_name = bn.obj_id and bn.node_kind_id = ' +
		cast(@query_nk_id as varchar(20)) + ' and bn.tree_id = ' + cast(@bwreports_id as varchar(20)) + '
		where bn.linked_node_id is null and bn.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @sapbw_query_src_sql, 'node_id', 'update_time, owner, last_edit', null, @optNodeFilter
	
	-- SAP BW providers, query and areas - technical id
	declare @bw_obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id=bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWBEx'', ''BWArea'', ''BWMPRO'', ''BWCUBE'', ''BWISET'', ''BWODSO''))'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_obj_id_sql, 'id', 'technical_id', null, @optNodeFilter
	
	-- SAP BW objects - technical id
	declare @bw_objs_id_sql varchar(max) = '(select bn.id, obj.obj_name as obj_technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id=bnk.id
		inner join bik_sapbw_object obj on obj.provider + ''|'' + obj.obj_name=bn.obj_id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWUni'',''BWKyf'', ''BWTim'', ''BWDpa'', ''BWCha'')
		and obj.provider_parent is null)'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_objs_id_sql, 'id', 'obj_technical_id', null, @optNodeFilter
	
end;
go

-- poprzednia wersja w pliku alter db for v1.1.6.6 tf.sql
-- poprawienie / optymalizacja procedury
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_mssql]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_mssql]
go

create procedure [dbo].[sp_insert_into_bik_node_mssql]
as
begin

	declare @mssql_tree_id int;
	select @mssql_tree_id = id from bik_tree where code='MSSQL'

	-- update
	update bik_node 
	set is_deleted = 0, descr = mssql.extra_info, visual_order = mssql.visual_order
	from bik_mssql mssql
	where bik_node.tree_id = @mssql_tree_id
	and bik_node.obj_id = mssql.branch_names
	and bik_node.linked_node_id is null

	-- insert nowych
	insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id, visual_order)
	select null, bnk.id, ms.name, ms.extra_info, @mssql_tree_id, ms.branch_names, ms.visual_order
	from bik_mssql ms
	inner join bik_node_kind bnk on ms.type = bnk.code
	left join (bik_node bn inner join bik_tree bt on bn.tree_id = bt.id and bn.tree_id = @mssql_tree_id) on bn.obj_id = ms.branch_names
	where bn.id is null
	order by ms.branch_names

	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join bik_mssql as ms on bik_node.obj_id = ms.branch_names
	inner join bik_node bk on bk.obj_id = ms.parent_branch_names 
	where bik_node.tree_id = @mssql_tree_id
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.tree_id = @mssql_tree_id
	and bk.is_deleted = 0
	and bk.linked_node_id is null

	-- usunięcie zbędnych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join bik_mssql on bik_node.obj_id = bik_mssql.branch_names
	where bik_mssql.name is null
	and tree_id = @mssql_tree_id
	and is_deleted = 0
	and linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @mssql_tree_id 
	and bn2.is_deleted = 1

	exec sp_node_init_branch_id @mssql_tree_id,null

end;
go


-- poprzednia wersja w: alter db for v1.2.3.3 tf.sql
-- poprawienie / optymalizacja procedury
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbw]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbw]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbw](@tree_code varchar(255))
as
begin
	declare @diag_level int = 0;
	declare @AreaNodeKind int;
	declare @BExNodeKind int;
	declare @CUBENodeKind int;
	declare @MPRONodeKind int;
	declare @ODSONodeKind int;
	declare @ISETNodeKind int;
	declare @ObjectsFolderNodeKind int;
	declare @UNINodeKind int;
	declare @TIMNodeKind int;
	declare @KYFNodeKind int;
	declare @DPANodeKind int;
	declare @CHANodeKind int;
	declare @tree_id int;
	
	select @AreaNodeKind = id from bik_node_kind where code = 'BWArea'
	select @BExNodeKind = id from bik_node_kind where code = 'BWBEx'
	select @CUBENodeKind = id from bik_node_kind where code = 'BWCUBE'
	select @MPRONodeKind = id from bik_node_kind where code = 'BWMPRO'
	select @ODSONodeKind = id from bik_node_kind where code = 'BWODSO'
	select @ISETNodeKind = id from bik_node_kind where code = 'BWISET'
	select @ObjectsFolderNodeKind = id from bik_node_kind where code = 'BWObjsFolder'
	select @UNINodeKind = id from bik_node_kind where code = 'BWUni'
	select @TIMNodeKind = id from bik_node_kind where code = 'BWTim'
	select @KYFNodeKind = id from bik_node_kind where code = 'BWKyf'
	select @DPANodeKind = id from bik_node_kind where code = 'BWDpa'
	select @CHANodeKind = id from bik_node_kind where code = 'BWCha'
	select @tree_id = id from bik_tree where code = @tree_code;
	
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'
	
	create table #tmpNodes (si_id varchar(900) primary key, si_parentid varchar(1000), si_kind varchar(100), si_name varchar(1000), descr varchar(max), visual_order int default 0);
	create table #tmpNodesLinked (si_id varchar(900), si_parentid varchar(1000));
	
	-- wrzucanie obszarów inf.
	insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
	select obj_name, parent, @AreaNodeKind, descr, null, -1 from bik_sapbw_area 
		
	if(@tree_code = 'BWReports')
	begin
		-- wrzucanie obszaru na zapytania nieprzypisane
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		values('$$BIKS_TEMP_ARCHIWUM$$', null, @AreaNodeKind, '(Archiwum)', 'Obszar gromadzący zapytania BEx, dla których nie odnaleziono dostawcy informacji. Obszar został wygenerowany automatycznie i nie znajduje się w repozytorium SAP BW.',100)

		-- wrzucanie zapytań BEx
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr)
		select convert(varchar(200),obj_name), coalesce((select ar.obj_name from bik_sapbw_provider pv left join bik_sapbw_area ar on pv.info_area=ar.obj_name where pv.obj_name = q.provider),'$$BIKS_TEMP_ARCHIWUM$$'), @BExNodeKind, case when rtrim(descr) <> '' then rtrim(descr) else obj_name end,'' from bik_sapbw_query q
	end;
	if(@tree_code = 'BWProviders')
	begin
		-- wrzucanie obszaru na dostawców nieprzypisanych
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		values('$$BIKS_TEMP_ARCHIWUM$$', null, @AreaNodeKind, '(Archiwum)', 'Obszar gromadzący dostawców informacji, dla których nie odnaleziono obszaru informacji. Obszar został wygenerowany automatycznie i nie znajduje się w repozytorium SAP BW.',100)

		-- wrzucanie folderów na cechy i wskaźniki
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		select obj_name + '|objectsFolder|', obj_name, @ObjectsFolderNodeKind, 'Cechy i wskaźniki', 'Folder zawierający obiekty informacji', -1 from bik_sapbw_provider

		-- wrzucanie dostawców informacji
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order) -- visual_order zgodny z tym w SAP GUI
		select obj_name, coalesce(area,'$$BIKS_TEMP_ARCHIWUM$$'), case type when 'MPRO' then @MPRONodeKind
																			when 'CUBE' then @CUBENodeKind
																			when 'ODSO' then @ODSONodeKind
																			when 'ISET' then @ISETNodeKind end,  
			case when rtrim(descr) <> '' then rtrim(descr) else obj_name end, null, case type when 'CUBE' then 1
																							when 'ISET' then 2
																							when 'MPRO' then 3
																							when 'ODSO' then 4 end  
		from bik_sapbw_provider xx left join (select obj_name as area from bik_sapbw_area) yy on xx.info_area = yy.area
		
		-- wrzucanie obiektów
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		select provider + '|' + obj_name, provider + '|objectsFolder|' ,case type when 'CHA' then @CHANodeKind 
																				  when 'UNI' then @UNINodeKind
																				  when 'KYF' then @KYFNodeKind
																				  when 'TIM' then @TIMNodeKind
																				  when 'DPA' then @DPANodeKind
																				  else @CHANodeKind end, 
																				  descr, null, case type when 'CHA' then 1
																										 when 'KYF' then 2
																										 when 'TIM' then 3
																										 when 'UNI' then 4
																										 when 'DPA' then 5
																										 else 6 end 
		from bik_sapbw_object 
		where provider_parent is null
		
		-- wrzucanie przepływów
		insert into #tmpNodesLinked(si_id,si_parentid)
		select obj_name, parent from bik_sapbw_flow
	end;
	
	-- usuwanie pustych folderów
	declare @rc int = 1

	while @rc > 0 
	begin
		delete from #tmpNodes
		where si_kind in (@AreaNodeKind, @ObjectsFolderNodeKind) and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	
	-- wdrażnie danych z tabeli tymczasowej to bik_node
	
	-- update istniejących
	update bik_node 
	set /*node_kind_id = #tmpNodes.si_kind,*/ name = ltrim(rtrim(#tmpNodes.si_name)), descr = #tmpNodes.descr,
		visual_order = #tmpNodes.visual_order, is_deleted = 0
	from #tmpNodes
	where bik_node.tree_id = @tree_id
	and bik_node.obj_id = #tmpNodes.si_id 
	
	-- wrzucanie nowych
	insert into bik_node(name,node_kind_id,tree_id,descr,obj_id, visual_order)
	select ltrim(rtrim(#tmpNodes.si_name)), #tmpNodes.si_kind, @tree_id, #tmpNodes.descr, #tmpNodes.si_id, #tmpNodes.visual_order
	from #tmpNodes --inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	--left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id and bik_node.linked_node_id is null
	left join (bik_node bn inner join bik_tree bt on bn.tree_id = bt.id and bn.tree_id = @tree_id and bn.linked_node_id is null) on bn.obj_id = #tmpNodes.si_id
	where bn.id is null;
	
	-- wrzucanie podlinkowanych
	insert into bik_node(name,parent_node_id,node_kind_id,tree_id,obj_id, linked_node_id, disable_linked_subtree)
	select lin.name, par.id, lin.node_kind_id, lin.tree_id, tmp.si_id, lin.id, 1 from #tmpNodesLinked tmp 
	left join bik_node lin on lin.obj_id = tmp.si_id and lin.is_deleted = 0 and lin.tree_id = @tree_id and lin.linked_node_id is null
	left join bik_node par on par.obj_id = tmp.si_parentid and par.is_deleted = 0 and par.tree_id = @tree_id and par.linked_node_id is null
	left join bik_node oryg on oryg.obj_id = tmp.si_id and oryg.is_deleted = 0 and oryg.tree_id = @tree_id and oryg.linked_node_id is not null
	where oryg.id is null and par.id is not null and lin.id is not null
	
	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join #tmpNodes as pt on bik_node.tree_id = @tree_id and bik_node.obj_id = pt.si_id
	inner join bik_node bk on bk.tree_id = @tree_id and bk.obj_id = pt.si_parentid 
	where bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.is_deleted = 0
	and bk.linked_node_id is null
	
	-- usuwanie starych
	update bik_node
	set is_deleted = 1
	from bik_node
	left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id
	where #tmpNodes.si_id is null
	and bik_node.tree_id = @tree_id
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @tree_id 
	and bn2.is_deleted = 1
	
	-- usuwanie starych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join #tmpNodesLinked on bik_node.obj_id = #tmpNodesLinked.si_id 
	where #tmpNodesLinked.si_id is null 
	and bik_node.tree_id = @tree_id
	and bik_node.linked_node_id is not null
	
	
	drop table #tmpNodes;
	drop table #tmpNodesLinked;
	
	exec sp_node_init_branch_id @tree_id, null;
	
end;
go

exec sp_update_version '1.2.4', '1.2.4.1';
go