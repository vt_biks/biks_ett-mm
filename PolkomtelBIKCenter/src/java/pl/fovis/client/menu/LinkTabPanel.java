/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.menu;

import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikcenterEntryPoint;
import pl.fovis.client.bikpages.TreeBikPage;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.client.dialogs.FileUploadDialogSimple;
import pl.fovis.client.dialogs.TreeSelector2ExportDialog;
import pl.fovis.client.nodeactions.BiksMainBranchPopUpMenu;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.FoxyFileUpload;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.OneListBoxDialog;
import pl.fovis.foxygwtcommons.dialogs.OneTextBoxDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author wezyr
 */
public final class LinkTabPanel extends VerticalPanel {

    public static final String TAB_STYLE_ACTIVE = "active" /* I18N: no */;
    protected HorizontalPanel tabs = new HorizontalPanel();
    protected Widget currentContent;
    protected List<Widget> contentWidgets = new ArrayList<Widget>();
    protected List<Widget> tabWidgets = new ArrayList<Widget>();
    protected int selectedTabIdx = -1;
    protected List<SelectionHandler<Integer>> selectionHandlers = new ArrayList<SelectionHandler<Integer>>();
    protected FlowPanel fpHelp = new FlowPanel();
    protected BikCustomButton exportCsvBtn, importCsvBtn, doitBtn;
    protected FlowPanel btnFp;
    protected String tabId;
    protected ListBox actLb;
//    protected PopupMenuDisplayer popMenu = new PopupMenuDisplayer();

    public LinkTabPanel(String tabId) {
        this.tabId = tabId;
        //add(tabs);
        //setSize("100%", "25px");

        fpHelp.setStyleName("crazyMadaFacka");

        HorizontalPanel hp = new HorizontalPanel();
        hp.setStyleName("helpHorizontal");

        hp.add(tabs);
        btnFp = new FlowPanel();

        boolean isVisibleExportImportBtn = isVisibleExportImportBtn(tabId);

        addBtnsToFp(isVisibleExportImportBtn);
        hp.add(btnFp);
        fpHelp.add(hp);
        add(fpHelp);
        setCellHeight(fpHelp, "21px");
        tabs.setStyleName("mainTabLook");
    }

    public void add(Widget contentWidget, final String tabText) {
        Label tabLabel = new Label(tabText);
        tabLabel.setStyleName("tabLook");
        add(contentWidget, tabLabel);
    }

//    protected void addPoupMenu(final ContextMenuEvent event) {
//        event.preventDefault();
//        event.stopPropagation();
////        popMenu = new PopupMenuDisplayer();
//        popMenu.clearMenuItems();
//        final TreeBean treeBean = BIKClientSingletons.getTreeBeanById(BIKClientSingletons.getTreeIdOfCurrentTab());
//        if (BIKClientSingletons.isTreeKindDynamic(treeBean.treeKind)) {
//            if (treeBean.showAddMainBranchBtn) {
//
////                TreeBean tree = BIKClientSingletons.getTreeBeanById(BIKClientSingletons.getTreeIdOfCurrentTab());
//                for (final TreeBean tb : treeBean.nodeKindsToAddOnTreesAsMainBranch) {
//                    popMenu.addMenuItem(I18n.dodaj.get() + " " + tb.nodeKindCaption, new IContinuation() {
//                        @Override
//                        public void doIt() {
//                            buildTreeNodeDialog(tb, tb.requiredAttrs, null);
//                        }
//                    }
//                    );
//                }
//            }
//        } else {
////            final TreeBean treeBean = BIKClientSingletons.getTreeBeanById(BIKClientSingletons.getTreeIdOfCurrentTab());
//            String caption = BIKClientSingletons.getNodeKindCaptionByCode(treeBean.nodeKindCode);
//            popMenu.addMenuItem(I18n.dodaj.get() /* I18N:  */ + " "
//                    + (!treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_USERS) ? caption : I18n.grupeUzytkownikow.get() /* I18N:  */),
//                    new IContinuation() {
//                @Override
//                public void doIt() {
//
//                    BIKClientSingletons.getService().getRequiredAttrs(treeBean.nodeKindId, new StandardAsyncCallback<List<AttributeBean>>() {
//                        @Override
//                        public void onSuccess(final List<AttributeBean> result) {
//
//                            BIKClientSingletons.getService().getBikAllArts(treeBean.id, new StandardAsyncCallback<List<String>>() {
//                                @Override
//                                public void onSuccess(final List<String> keywords) {
//                                    buildTreeNodeDialog(treeBean, result, keywords);
//
//                                }
//                            });
//                        }
//                    });
//                }
//            });
//        }
//
//        if (isVisibleExportImportBtn(tabId)) {
//            popMenu.addMenuSeparator();
//            popMenu.addMenuItem(I18n.eksportGeneruj.get(), new IContinuation() {
//                @Override
//                public void doIt() {
//
//                    saveTree();
//                }
//            });
//        }
//        popMenu.showAtPosition(event.getNativeEvent().getClientX(), event.getNativeEvent().getClientY());
////        new BiksPopUpMenu(null, null, null, 0, 0).createAndShowPopup();
//    }
//    protected void buildTreeNodeDialog(final TreeBean treeBean, final List<AttributeBean> requiredAttrs, List<String> keywords) {
//        new TreeNodeDialog().buildAndShowDialog(treeBean.nodeKindId, treeBean.nodeKindCode, null,
//                null, treeBean.id, keywords,
//                requiredAttrs,
//                new IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>>() {
//            public void doIt(final Pair<TreeNodeBean, Map<String, AttributeBean>> param2) {
//                TreeNodeBean param = param2.v1;
//                BIKClientSingletons.getService().createTreeNodeWithAttr(param.parentNodeId,
//                        param.nodeKindId, param.name, param.objId, param.treeId, param.linkedNodeId, null,
//                        param2.v2,
//                        new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on" /* I18N: no */ + " createTreeNode") {
//                    @Override
//                    public void onSuccess(Pair<Integer, List<String>> result) {
//                        BIKClientSingletons.invalidateTabData(BIKClientSingletons.getTreeCodeById(BIKClientSingletons.getTreeIdOfCurrentTab()));
//                        BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get() /* I18N:  */);
//                    }
//                });
//            }
//        });
//
//    }
    public void add(Widget contentWidget, Widget tabWidget) {
        final int tabIdx = tabWidgets.size();

        contentWidgets.add(contentWidget);
        tabWidgets.add(tabWidget);
        tabs.add(tabWidget);
        GWTUtils.addOnClickHandler(tabWidget, new IContinuation() {

            public void doIt() {
                selectTab(tabIdx);
            }
        });

        if (selectedTabIdx == -1) {
            selectTab(0);
        }

        tabWidgets.get(tabIdx).sinkEvents(Event.ONCONTEXTMENU);
        tabWidgets.get(tabIdx).addHandler(
                new ContextMenuHandler() {
            @Override
            public void onContextMenu(ContextMenuEvent event) {
                event.preventDefault();
                event.stopPropagation();

                TreeBean treeBean = BIKClientSingletons.getTreeBeanById(BIKClientSingletons.getTreeIdOfCurrentTab());

                if (treeBean != null) {
                    if (selectedTabIdx == tabIdx) {

                        new BiksMainBranchPopUpMenu(tabId, new TreeBikPage(treeBean), event.getNativeEvent().getClientX(), event.getNativeEvent().getClientY()).createAndShowPopup();

//                        addPoupMenu(event);
                    }

                }
            }
        }, ContextMenuEvent.getType());

    }

    @Override
    public Widget getWidget(int tabIdx) {
        return contentWidgets.get(tabIdx);
    }

    public void selectTab(int tabIdx, boolean fireEvents) {
        if (selectedTabIdx != -1) {

            if (currentContent != null) {
                remove(currentContent);
            }

            tabWidgets.get(selectedTabIdx).removeStyleName(TAB_STYLE_ACTIVE);
        }

        Widget contentWidget = contentWidgets.get(tabIdx);
        add(contentWidget);
        setCellHeight(contentWidget, "100%");
        setCellWidth(contentWidget, "100%");

        tabWidgets.get(tabIdx).addStyleName(TAB_STYLE_ACTIVE);

        this.currentContent = contentWidget;
        this.selectedTabIdx = tabIdx;

        if (fireEvents) {
            for (SelectionHandler<Integer> handler : selectionHandlers) {
                handler.onSelection(new MySelectionEvent<Integer>(selectedTabIdx));
            }
        }
    }

    public void selectTab(int tabIdx) {
        selectTab(tabIdx, true);
    }

    private void addAction2ListBox() {
//        if (!BIKClientSingletons.isConnectorAvailable(PumpConstants.SOURCE_NAME_PLAIN_FILE)) {
//            actLb.addItem(I18n.importDrzewa.get());
//        }
        actLb.addItem(I18n.eksportGeneruj.get());
//        actLb.addItem(I18n.eksportCsv.get());
//        if (BIKClientSingletons.isEnableFullImportExportTree()) {
//            actLb.addItem(I18n.eksportDowiazaniPowiazan.get());
//        }
//        actLb.addItem(I18n.tworzenieHTML.get());
//        actLb.addItem(I18n.eksportPowiazan.get());
    }

    protected static class MySelectionEvent<T> extends SelectionEvent<T> {

        public MySelectionEvent(T selectedItem) {
            super(selectedItem);
        }
    }

    public void addSelectionHandler(SelectionHandler<Integer> handler) {
        selectionHandlers.add(handler);
    }

//    private void exportLinkAndAssociations() {
//        BIKClientSingletons.getService().getBindedTreeId(BIKClientSingletons.getTreeIdOfCurrentTab(), new StandardAsyncCallback<List<Integer>>() {
//
//            @Override
//            public void onSuccess(final List<Integer> result) {
//                List<String> allTreeNames = new ArrayList<String>();
//                for (Integer treeId : result) {
//                    allTreeNames.add(BIKClientSingletons.getMenuPathForTreeId(treeId));
//                }
//                new OneListBoxDialog().buildAndShowDialog(I18n.wybierzDrzewo.get(), allTreeNames, new IParametrizedContinuation<String>() {
//
//                    @Override
//                    public void doIt(String param) {
//                        param = BaseUtils.trimAndCompactSpaces(param);
//                        for (Integer treeId : result) {
//                            if (param.equalsIgnoreCase(BaseUtils.trimAndCompactSpaces(BIKClientSingletons.getMenuPathForTreeId(treeId)))) {
//                                Window.open(generateExportLinkedFileLink(BIKClientSingletons.getTreeIdOfCurrentTab(), treeId, BIKConstants.TREE_EXPORT_ACTION_LINKED_ASSOCIATIONS), "_blank", "");
//                                break;
//                            }
//                        }
//                    }
//                });
//            }
//        });
//    }
//    private String generateExportLinkedFileLink(Integer currentTreeId, Integer bindedTreeId, String act) {
//        String treeName = BIKClientSingletons.getTreeBeanById(currentTreeId).name;
//        String bindedTreeName = BIKClientSingletons.getTreeBeanById(bindedTreeId).name;
//        StringBuilder sb = new StringBuilder(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet());
//        sb.append("?").append(BIKConstants.TREE_EXPORT_PARAM_NAME).append("=").append(BIKConstants.EXPORT_FORMAT_CSV).append("&")
//                .append(BIKConstants.TREE_EXPORT_ACTION_PARAM_NAME).append("=").append(act).append("&")
//                .append(BIKConstants.TREE_EXPORT_BINDING_PARAM_NAME).append("=").append(bindedTreeId).append("&")
//                .append(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME).append("=").append(currentTreeId).append("&")
//                .append(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME).append("=").append(BaseUtils.deAccentPolishChars(treeName).replace(" ", "_")).append("&")
//                .append(BIKConstants.TREE_EXPORT_BINDED_TREE_NAME_PARAM_NAME).append("=").append(BaseUtils.deAccentPolishChars(bindedTreeName).replace(" ", "_"));
//        return sb.toString();
//    }
    private String generateCreateDocumentLink(String depth, boolean numbering, Integer treeId, String act) {
        String treeName = BIKClientSingletons.getTreeBeanById(treeId).name;
        StringBuilder sb = new StringBuilder(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet());
        sb.append("?").append(BIKConstants.TREE_EXPORT_PARAM_NAME).append("=").append(BIKConstants.EXPORT_FORMAT_HTML).append("&")
                .append(BIKConstants.TREE_EXPORT_ACTION_PARAM_NAME).append("=").append(act).append("&")
                .append(BIKConstants.TREE_EXPORT_DEPTH_PARAM_NAME).append("=").append(depth).append("&")
                .append(BIKConstants.TREE_EXPORT_NUMBERING_PARAM_NAME).append("=").append(numbering ? "1" : "0").append("&")
                .append(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME).append("=").append(treeId).append("&")
                .append(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME).append("=").append(BaseUtils.deAccentPolishChars(treeName).replace(" ", "_"));
        return sb.toString();
    }

    private void createDocument() {
        final CheckBox addNumbering = new CheckBox(I18n.czyChceszDodacNumeracje.get());

        new OneTextBoxDialog() {

            @Override
            protected void buildMainWidgets() {
                super.buildMainWidgets(); //To change body of generated methods, choose Tools | Templates.
                main.add(addNumbering);
            }
        }.buildAndShowDialog(I18n.glebokoscDrzewa.get(), "3", new IParametrizedContinuation<String>() {

            @Override
            public void doIt(String param) {
                Integer depth = BaseUtils.tryParseInteger(param, null);
                if (depth == null) {
                    BIKClientSingletons.showError(I18n.bladWeryfikacji.get());
                } else {
                    Window.open(generateCreateDocumentLink(param, addNumbering.getValue(), BIKClientSingletons.getTreeIdOfCurrentTab(), BIKConstants.TREE_EXPORT_ACTION_CREATE_DOCUMENT), "_blank", "");
                }
            }
        });
    }

    protected void addBtnsToFp(boolean isVisibleExportImportBtn) {
        btnFp.clear();
        TreeBean tb = BIKClientSingletons.getTreeBeanByCode(tabId);
        if (tb == null || "DictionaryDWH".equalsIgnoreCase(tb.code)) {
            btnFp.add(createHelpBtn()); //Pomoc dla zakładek nie drzewiastych np MOJ BIKS, WYSZUKIWARKA
        }

//        if (isVisibleExportImportBtn) {
//            btnFp.add(doitBtn = new BikCustomButton("" /* I18N:  */, "doitBtn" /* I18N: no */, new IContinuation() {
//
//                @Override
//                public void doIt() {
//                    String act = actLb.getSelectedItemText();
//                    if (I18n.tworzenieHTML.get().equalsIgnoreCase(act)) {
//                        createDocument();
//                    } else if (I18n.eksportGeneruj.get().equalsIgnoreCase(act)) {
//                        saveTree();
//                    }
//                }
//            }));
//            actLb = new ListBox();
//            addAction2ListBox();
//            actLb.addStyleName("actionListBox");
//
//            btnFp.add(actLb);
//
//        }
    }

    protected BikCustomButton createHelpBtn() {
        BikCustomButton help = new BikCustomButton(""/*I18n.pomoc.get()*/ /* I18N:  */, "help" /* I18N: no */,
                new IContinuation() {

            public void doIt() {
                bikcenterEntryPoint.helpButtonClick();
            }
        });
        return help;
    }

    public final void importCsv() {
//        importCsvBtn = new BikCustomButton(I18n.importDrzewa.get() /* I18N:  */, "importCsv" /* I18N: no */,
//                new IContinuation() {

//                    @Override
//                    public void doIt() {
        FileUploadDialogSimple fuDialog = new FileUploadDialogSimple();
        fuDialog.buildAndShowDialog(new IParametrizedContinuation<FoxyFileUpload>() {

            @Override
            public void doIt(FoxyFileUpload param) {
                final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.importDanychDoDrzewa.get(), true);
                BIKClientSingletons.getService().importCSVDataToTree(BIKClientSingletons.getTabSelector().getCurrentTab().getId(), param.getServerFileName(), new StandardAsyncCallback<Void>("Error in importCSVDataToTree") {

                    @Override
                    public void onSuccess(Void result) {
                        infoDialog.hideDialog();
                        BIKClientSingletons.invalidateTabData(BIKClientSingletons.getTreeCodeById(BIKClientSingletons.getTreeIdOfCurrentTab()));
                        BIKClientSingletons.showInfo(I18n.zaimportowanoDaneDoDrzewa.get());
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        infoDialog.hideDialog();
                        super.onFailure(caught);
                    }
                });
            }
        }, Arrays.asList(BIKClientSingletons.allowedFileExtensionsList()));
//                    }
//                });
    }

    public final void exportTreeCsv() {
//        exportCsvBtn = new BikCustomButton(I18n.eksportCsv.get() /* I18N:  */, "exportCsv" /* I18N: no */,
//                new IContinuation() {

//                    public void doIt() {
        new OneListBoxDialog().buildAndShowDialog(I18n.wybierzFormatExportu.get(), Arrays.<String>asList(BIKConstants.EXPORT_FORMAT_XLSX, BIKConstants.EXPORT_FORMAT_CSV), new IParametrizedContinuation<String>() {

            @Override
            public void doIt(String param) {
                Window.open(generateExportTreeFileLink(param, BIKClientSingletons.getTreeIdOfCurrentTab()), "_blank", "");
            }
        });
//                    }
//                });
    }

    public void saveTree() {
        //        Window.open(generateSaveTreeLink(BIKClientSingletons.getTreeIdOfCurrentTab()), "_blank", "");
        TreeSelector2ExportDialog dialog = new TreeSelector2ExportDialog(BIKClientSingletons.getTreeIdOfCurrentTab());
        dialog.buildAndShowDialog();
    }

    protected String generateExportTreeFileLink(String exportFormat, int treeId) {
        String treeName = BIKClientSingletons.getTreeBeanById(treeId).name;
        StringBuilder sb = new StringBuilder(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet());
        sb.append("?").append(BIKConstants.TREE_EXPORT_PARAM_NAME).append("=").append(exportFormat).append("&")
                .append(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME).append("=").append(treeId).append("&")
                .append(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME).append("=").append(BaseUtils.deAccentPolishChars(treeName).replace(" ", "_"));
        return sb.toString();
    }

    protected boolean isVisibleExportImportBtn(String tabId) {
        TreeBean tb = BIKClientSingletons.getTreeBeanByCode(tabId);

        if (tb == null) {
            return false;
        }
//        return true;

        return (BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_IMPORT_TREE, tb.id)
                || BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_TO_HTML, tb.id)
                || BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_TO_PDF, tb.id))
                && !BIKClientSingletons.disableImportExport()
                && !"DictionaryDWH".equalsIgnoreCase(tb.code);
    }

    public void setExportAndImportBtnVsible(String tabId) {
        boolean isVisible = isVisibleExportImportBtn(tabId);
        if (doitBtn != null) {
            doitBtn.setVisible(isVisible);
        }
        if (actLb != null) {
            actLb.clear();
            if (isVisible) {
                addAction2ListBox();
            }
        } else {
            if (isVisible) {
                addBtnsToFp(isVisible);
            }
        }
    }
}
