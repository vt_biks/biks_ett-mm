/*
 * CommandBatcher.java
 *
 * Created on 10 sierpieĹ„ 2006, 16:19
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.simpledblib;

import commonlib.LameUtils;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.driver.OraclePreparedStatement;
import simplelib.LameRuntimeException;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class OracleCommandBatcher {

    static ILameLogger logger = LameLoggerFactory.getLogger(OracleCommandBatcher.class);
    //LameLogger.getClassLogger();
//    public interface BatchCommand {
//        void run() throws SQLException;
//        //void writeAsBytes(OutputStream outStrm) throws IOException;
//        String runSeparatedText();
//    }
    static private final int MAX_PENDING_COMMANDS = 5000;
    static private final int RUN_COMMANDS_AT_ONCE = 1000;
    private int totalCount = 0;
    private Connection conn;
    //private Statement stmt = null;
    private OraclePreparedStatement prepStmt = null;
    private ArrayList<Object[]> pendingCommandsParams;
//    private String fileName;
    private boolean appendToFile = false;
    private int commandPatternCount = 0;

    /** Creates a new instance of CommandBatcher */
    public OracleCommandBatcher(Connection conn,
            String stmtText //, String fileName
            ) throws SQLException {
        this.conn = conn;
        if (conn != null) {
            this.prepStmt = (OraclePreparedStatement) conn.prepareStatement(stmtText);
        //this.stmt = conn.createStatement();
//        this.fileName = fileName;

        }
        this.pendingCommandsParams = new ArrayList<Object[]>();
    }

    public void addParams(Object[] params) throws SQLException//, IOException
    {
        pendingCommandsParams.add(params);

        if (pendingCommandsParams.size() >= MAX_PENDING_COMMANDS) {
            runPending(false);
        }
    }

//    private void storePending() throws IOException {
//        OutputStream os = new FileOutputStream(fileName, appendToFile);
//        for (int i = 0; i < pendingCommandsParams.size(); i++) {
//            //os.write(pendingCommandsParams.get(i).getRawData());
//            pendingCommandsParams.get(i).writeAsBytes(os);
//        }
//
//        os.close();
//        appendToFile = true;
//    }
    public void runPending() throws SQLException//, IOException
    {
        runPending(false);
    }

    public void runPending(boolean beQuite) throws SQLException//, IOException
    {
        if (conn != null) {
            execPending(beQuite);
        }
//        if (fileName != null)
//            storePending();

        if (pendingCommandsParams.size() >= MAX_PENDING_COMMANDS) {
            pendingCommandsParams.trimToSize();
        }

        pendingCommandsParams.clear();
    }

    private void execPending(boolean beQuite) throws SQLException {
        commandBuffer.clear();
        if (!beQuite) {
            if (logger.isInfoEnabled()) {
                logger.info("runPending: start, command count = " + pendingCommandsParams.size());
            }
        }
        boolean autoCommit = conn.getAutoCommit();
        conn.setAutoCommit(false);
        try {

            int oneBatchIdx = 0, pendingSize = pendingCommandsParams.size();

            while (oneBatchIdx < pendingSize) {
                if (!beQuite) {
                    if (logger.isInfoEnabled()) {
                        logger.info("runPending: clearBatch, oneBatchIdx = " + oneBatchIdx);
                    }
                }

                //stmt.clearBatch();
                prepStmt.clearBatch();

                if (!beQuite) {
                    if (logger.isInfoEnabled()) {
                        logger.info("runPending: run commands loop, oneBatchIdx = " + oneBatchIdx);
                    }
                }
                for (int i = 0; i < RUN_COMMANDS_AT_ONCE && oneBatchIdx < pendingSize; i++) {
                    Object[] params = pendingCommandsParams.get(oneBatchIdx);
                    pendingCommandsParams.set(oneBatchIdx, null);
                    //if (bc != null)
                    //    bc.run();

                    for (int parIdx = 0; parIdx < params.length; parIdx++) {
                        prepStmt.setObject(parIdx + 1, params[parIdx]);
                    }

                    prepStmt.addBatch();

                    oneBatchIdx++;
                    totalCount++;
                    if (totalCount % 30000 == 0) {
                        LameUtils.runGc();
                    }
                }

                if (!beQuite) {
                    if (logger.isInfoEnabled()) {
                        logger.info("runPending: exec batch, oneBatchIdx = " + oneBatchIdx);
                    }
                }

                try {
                    //stmt.executeBatch();
                    prepStmt.executeBatch();
                } catch (Exception ex) {
                    if (!conn.getAutoCommit()) {
                        conn.rollback();
                        if (!beQuite) {
                            if (logger.isInfoEnabled()) {
                                logger.info("runPending: manual rollback, oneBatchIdx = " + oneBatchIdx);
                            }
                        }
                    }
                    logger.error("error in one of commands\n" + LameUtils.mergeWithSepEx(commandBuffer, "\n"));
                    throw new LameRuntimeException(ex);
                }

                if (!conn.getAutoCommit()) {
                    conn.commit();
                    if (!beQuite) {
                        if (logger.isInfoEnabled()) {
                            logger.info("runPending: manual commit, oneBatchIdx = " + oneBatchIdx);
                        }
                    }
                } else if (!beQuite) {
                    if (logger.isInfoEnabled()) {
                        logger.info("runPending: autocommitted, oneBatchIdx = " + oneBatchIdx);
                    }
                }
            }

        } finally {
            conn.setAutoCommit(autoCommit);
        }

        if (!beQuite) {
            if (logger.isInfoEnabled()) {
                logger.info("runPending: done");
            }
        }
    }
    private List<String> commandBuffer = new ArrayList<String>();

    public void runCommand(String command) throws SQLException {
        //logger.info("add command: " + command);
        commandBuffer.add(command);
        //stmt.addBatch(command);
        prepStmt.addBatch(command);
    }

    public int registerCommandPattern() {
        return ++commandPatternCount;
    }

//    public static void batchFromFile(String fileName, Connection conn,
//            String tableToReplace, String newTable) throws IOException, SQLException {
//        FileInputStream fis = new FileInputStream(fileName);
//        PrimAsBytesReader r = new PrimAsBytesReader(fis);
//        List<CommandSerializer> css = new ArrayList<CommandSerializer>();
//        CommandBatcher cb = new CommandBatcher(conn, null);
//
//        int cntr = 0;
//
//        for (;; cntr++) {
//            if (cntr % 1000 == 0)
//                logger.info("batchFromFile: cntr = " + cntr);
//            Integer csi = (Integer) r.readObject();
//            if (csi == null)
//                break;
//            if (csi < 0) {
//                if (-csi - 1 != css.size())
//                    throw new StreamCorruptedException("unproper new command serializer index " + csi + ", css.size(): " + css.size());
//
//                String commandString = (String) r.readObject();
//                if (tableToReplace != null)
//                    commandString = commandString.replace(tableToReplace, newTable);
//
//                css.add(new CommandSerializer(cb, commandString, (String) r.readObject()));
//                csi = -csi;
//            }
//            if (csi < 1 || csi > css.size())
//                throw new StreamCorruptedException("unproper command serializer index " + csi + ", css.size(): " + css.size());
//            CommandSerializer cs = css.get(csi - 1);
//            Object[] args = r.readObjects(cs.argCount());
//            cs.addCommand(args);
//        }
//        cb.runPending();
//    }
//    private class PlainBatchCommandParams implements BatchCommandParams {
//        
//        protected String commandText;
//        protected boolean runSeparated;
//        
//        protected PlainBatchCommand(String commandText, boolean runSeparated) {
//            this.commandText = commandText;
//            this.runSeparated = runSeparated;
//        }
//        
//        public void run() throws SQLException {
//            runCommand(commandText);
//        }
//        
////        public void writeAsBytes(OutputStream outStrm) throws IOException {
////            PrimAsBytesWriter w = new PrimAsBytesWriter(outStrm);
////            w.writeObject(0);
////            w.writeObject(commandText);
////        }
//        
//        public String runSeparatedText() {
//            return runSeparated ? commandText : null;
//        }
//    }
//    
//    public void addCommand(String commandText, boolean runSeparated) throws SQLException//, IOException
//    {
//        add(new PlainBatchCommand(commandText, runSeparated));
//    }
//    
//    public void addCommand(String commandText) throws SQLException//, IOException
//    {
//        addCommand(commandText, false);
//    }
    public void addCommandParams(Object[] params) throws SQLException//, IOException
    {
        addParams(params);
    }
}
