/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

import java.util.Map;
import simplelib.BaseUtils.Projector;
import simplelib.INamedPropsRenameStrategy;

/**
 *
 * @author wezyr
 */
public class ExtraPropsQuery<K, T> {

    public String qryTxt;
    public String keysSubstr = "$keys$";
    public Projector<Map<String, Object>, K> pkProj;
    public Projector<T, K> fkProj;
    public INamedPropsRenameStrategy propsRenameStrategy; // optional, can be null
    public boolean pkNullable = false;

    public ExtraPropsQuery(Projector<T, K> fkProj, String qryTxt,
            Projector<Map<String, Object>, K> pkProj) {
        this.qryTxt = qryTxt;
        this.pkProj = pkProj;
        this.fkProj = fkProj;
    }
}
