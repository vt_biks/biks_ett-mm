/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import pl.trzy0.foxy.serverlogic.FoxyMillModelBaseEx;
import pl.foxys.foxymill.ReturnFromRenderingException;
import commonlib.LameUtils;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.IRenderable;
import pl.foxys.foxymill.ParserTask;
import pl.foxys.foxymill.renderctx.RenderCtx;
import pl.foxys.foxymill.tags.ITag;
import pl.trzy0.foxy.serverlogic.FoxyMillSrcFilesSpec;
import commonlib.ILameResourceProvider;
import pl.foxys.foxymill.FallThroughToLogonPageException;
import pl.foxys.foxymill.GlobalDefsBuilder;
import pl.foxys.foxymill.RerenderWithNewModelException;
import pl.foxys.foxymill.exprlang.mvel.MvelParsedExpression;
import pl.foxys.foxymill.templates.FoxyMillParser;
import pl.trzy0.foxy.serverlogic.TextProviderFromProperties;
import pl.trzy0.foxy.commons.templates.CachingNamedTemplateProvider;
import pl.trzy0.foxy.commons.templates.INamedTemplateProvider;
import pl.trzy0.foxy.serverlogic.FoxyUrlRewriter;
import pl.trzy0.foxy.serverlogic.IApplicationEnvironment;
import pl.trzy0.foxy.serverlogic.IFoxyService;
import simplelib.BaseUtils;
import simplelib.IPredicate;
import simplelib.LameRuntimeException;
import simplelib.MapHelper;
import simplelib.SimpleDateUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class FoxyMillBundle extends FoxyMillBundleData {

    private static final ILameLogger logger = //LameLoggerFactory.getLogger(FoxyMillBundle.class, LameLogLevel.Info);
            LameUtils.getMyLogger();
    private static final FoxyMillParser foxyMillParser = new FoxyMillParser();
    //public Properties props;
    private ILameResourceProvider resProvider;
    //private String propsFilePath;
    private String propsFilesDir;
    private String pageFilesDir;
    private String pageFilesDirWithSlash;
    private String globalFilesDir;
    private String errorMsg;
    private Set<String> enabledLoggerKinds;
    private IBundleDirector bundleDirector;
    protected IBundleDerivedLogic bundleLogic;
    private FoxyUrlRewriter urlRewriter;
    private IApplicationEnvironment appEnv;

    protected FoxyMillBundle() {
        // no public cons
    }

    public static FoxyMillBundle calcNewBoundle(FoxyMillBundle oldBoundle) {
        Date start = new Date();

        FoxyMillSrcFilesSpec newFilesSpec =
                FoxyMillSrcFilesSpec.calcChangedSpec(oldBoundle.filesSpec, oldBoundle.resProvider);

        Date changesFound = new Date();
//
//        if (logger.isDebugEnabled()) {
//            logger.debug("calcNewBoundle: find-changes time: "
//                    + SimpleDateUtils.getElapsedSecondsWithMilliPrec(start, changesFound)
//                    + ", changes found? " + (newFilesSpec != null));
//        }

        if (newFilesSpec == null) {
            return null;
        }

        FoxyMillBundle res = new FoxyMillBundle();
        res.generateFreshOne(oldBoundle, newFilesSpec);

        Date end = new Date();
        if (logger.isInfoEnabled()) {
            logger.info("calcNewBoundle: new boundle generated in: "
                    + SimpleDateUtils.getElapsedSecondsWithMilliPrec(changesFound, end)
                    + ", total time: " + SimpleDateUtils.getElapsedSecondsWithMilliPrec(start, end));
        }

        return res;
    }

    public void generateFreshOne(FoxyMillBundle oldBoundle, FoxyMillSrcFilesSpec newFilesSpec) {
        internalGenerate(oldBoundle.bundleDirector, oldBoundle.propsFilesDir, oldBoundle.pageFilesDir,
                oldBoundle.globalFilesDir, oldBoundle.resProvider, newFilesSpec,
                oldBoundle.urlRewriter, oldBoundle.appEnv);
    }

    public void internalGenerate(IBundleDirector bundleDirector,
            String propsFilesDir, String pageFilesDir, String globalFilesDir,
            ILameResourceProvider resProvider, FoxyMillSrcFilesSpec newFilesSpec,
            FoxyUrlRewriter urlRewriter,
            IApplicationEnvironment appEnv) {
        try {
            this.bundleDirector = bundleDirector;
            this.resProvider = resProvider;
            //this.propsFilePath = propsFilePath;
            this.propsFilesDir = propsFilesDir;
            this.pageFilesDir = pageFilesDir;
            this.globalFilesDir = globalFilesDir;
            this.urlRewriter = urlRewriter;
            this.appEnv = appEnv;

            this.pageFilesDirWithSlash = pageFilesDir + "/";

            //FoxyMillSrcFilesSpec spec
            this.filesSpec = newFilesSpec;

            readProperties();

            enabledLoggerKinds = BaseUtils.splitUniqueBySep(sysProps.getString("foxymill.logging.enabledKinds"), ",", true);

            this.renderEnv = FoxyMillUtils.makeRenderEnvForHTML(true, sysProps.getBool("disableKaszaning"));
            this.renderEnv.enabledLoggerPredicate = new IPredicate<String>() {

                public Boolean project(String val) {
                    return val == null || enabledLoggerKinds.contains(val);
                }
            };

            MvelParsedExpression.clearExprsInFiles();

            readGlobalFiles();
            readPageFiles();

//            System.out.println(">>>> mvel exprsInFiles start >>>>:\n\n" +
//                    MvelParsedExpression.getPrettyExprsInFiles() +
//                    "\n\n<<<< mvel exprsInFiles end <<<<");
            MvelParsedExpression.clearExprsInFiles();

            bundleLogic.init(this, urlRewriter, appEnv);
        } catch (Exception ex) {
            errorMsg = LameUtils.printExceptionToString(ex);
            if (logger.isErrorEnabled()) {
                logger.error("internal generate error", ex);
            }
        }
    }

    public void generateInitial(IBundleDirector bundleDirector,
            String propsFilesDir, String pageFilesDir, String globalFilesDir,
            ILameResourceProvider resProvider,
            FoxyUrlRewriter urlRewriter,
            IApplicationEnvironment appEnv) {
        internalGenerate(bundleDirector,
                propsFilesDir, pageFilesDir, globalFilesDir, resProvider, FoxyMillSrcFilesSpec.makeInitial(
                BaseUtils.paramsAsSet(pageFilesDir, globalFilesDir, propsFilesDir + "|.properties"),
                //BaseUtils.paramsAsSet(propsFilePath)
                Collections.<String>emptySet(), resProvider),
                urlRewriter, appEnv);
    }

//    public static Properties readPropsFileFromResProvider(ILameResourceProvider resProvider, String propsFilePath, boolean optional) {
//        Properties res = new Properties();
//        InputStream is = resProvider.gainResource(propsFilePath);
//
//        if (is == null) {
//            if (optional) {
//                return null;
//            } else {
//                throw new LameRuntimeException("cannot find props file: " + propsFilePath);
//            }
//        }
//
//        try {
//            try {
//                res.load(is);
//            } finally {
//                is.close();
//            }
//        } catch (Exception ex) {
//            throw new LameRuntimeException("cannot read props file: " + propsFilePath, ex);
//        }
//        return res;
//    }
    private Properties readPropsFile(String propsFilePath, boolean optional) {
        return LameUtils.readPropsFileFromResProvider(resProvider, propsFilePath, optional);
    }

    @SuppressWarnings("unchecked")
    private void readProperties() {
        Properties sysPropsAsProps = readPropsFile(propsFilesDir + "/sys-props.properties", false);
        Properties sysPropsCustom = readPropsFile(propsFilesDir + "/sys-props-custom.properties", true);

        if (sysPropsCustom != null) {
            sysPropsAsProps.putAll(sysPropsCustom);
        }

        sysProps = new MapHelper<String>((Map) sysPropsAsProps);
        bundleLogic = bundleDirector.createLogic(sysProps);
        //String langs = sysProps.getString("langs");
        //Iterable<String> langIter = BaseUtils.splitBySep(langs, ",", true);
        Iterable<String> langIter =
                bundleLogic.getSupportedClientLangs();

        textsProps = new HashMap<String, Properties>();
        tmplProviders = new HashMap<String, INamedTemplateProvider>();

        for (String lang : langIter) {
            Properties props = readPropsFile(propsFilesDir + "/texts_" + lang + ".properties", false);
            textsProps.put(lang, props);
            INamedTemplateProvider tmplProvider = new CachingNamedTemplateProvider(
                    new TextProviderFromProperties(props), foxyMillParser, true);
            tmplProviders.put(lang, tmplProvider);
        }
    }

    private String loadHtmlFile(String filePath) {
//        InputStream is = resProvider.gainResource(filePath);
//        if (is == null) {
//            throw new LameRuntimeException("no such file: " + filePath);
//        }
//        String pageContent = LameUtils.loadAsString(is, "UTF-8");
//        return pageContent;
        return LameUtils.gainReqStringFromProvider(resProvider, filePath, "UTF-8");
    }

    private //IRenderable
            List<ITag> readFMFile(String filePath) {
        try {
            String pageContent = loadHtmlFile(filePath);

            ParserTask pt = new ParserTask(filePath, pageContent);
            final List<ITag> tags = pt.read();
            FoxyMillUtils.prepareTags(tags, renderEnv);

            return tags;//FoxyMillUtils.wrapAsRenderable(tags, null);
        } catch (Exception ex) {
            throw new LameRuntimeException("error reading foxymill file '" + filePath + "'", ex);
        }
    }

    private Set<String> getFilesInDirBySpec(String dir) {
        return filesSpec.dirs.get(dir).fileLastMods.keySet();
    }

    private void readGlobalFiles() {
        Set<String> fileNames = getFilesInDirBySpec(globalFilesDir);

        Object model = new FoxyMillModelBaseEx(sysProps);

        globals = GlobalDefsBuilder.buildGlobals(resProvider, renderEnv, fileNames, model);

//        //SimpleRenderContext renderCtx = new SimpleRenderContext(new FoxyMillModelBaseEx(props));
//        IRenderCtx renderCtx = RenderCtx.makeRootRenderCtx(model);
//
//        List<ITag> allTags = new ArrayList<ITag>();
//
//        for (String fileName : fileNames) {
//            //IRenderable renderable
//            List<ITag> tags = readFMFile(fileName);
//            //renderable.render(renderCtx);
//            FoxyMillUtils.render(tags, renderCtx);
//            allTags.addAll(tags);
//        }
//
//        globals = renderCtx.getVars();
//
////        for (Object global : globals.values()) {
////            if (global instanceof ITag) {
////                ITag globalTag = (ITag) global;
////                globalTag.fixUp(globals);
////            }
////        }
//        FoxyMillUtils.fixupTags(allTags, globals);
    }

    private void readPageFiles() {
        boxes = new LinkedHashMap<String, IRenderable>();

        Set<String> fileNames = getFilesInDirBySpec(pageFilesDir);

        for (String fileName : fileNames) {
            String boxName = BaseUtils.dropFileExt(
                    BaseUtils.dropOptionalPrefix(fileName, pageFilesDirWithSlash));
            List<ITag> tags = readFMFile(fileName);
            FoxyMillUtils.fixupTags(tags, globals);
            boxes.put(boxName, FoxyMillUtils.wrapAsRenderable(tags, null));
        }
    }

    @SuppressWarnings("deprecation")
    protected String internalRenderIt(Object pageVal, Object model) {
        RenderCtx rc = RenderCtx.makeRootRenderCtx(
                model);

        String res;
        if (pageVal instanceof IRenderable) {
            IRenderable page = (IRenderable) pageVal;

            boolean abortOutput = false;
            try {
                page.render(rc);
            } catch (ReturnFromRenderingException ex) {
                abortOutput = ex.abortRendering;
            }
            res = abortOutput ? null : rc.getOutputStr();
        } else {
            res = BaseUtils.safeToString(pageVal);
        }

        return res;
    }

    public String renderPageFile(String pageName,
            HttpServletRequest request, HttpServletResponse response,
            IFoxyService foxyService, //Class<? extends FoxyMillHttpModel> modelClass,
            long fileSizeMax) {
        if (errorMsg != null) {
            return "<pre>" + errorMsg + "</pre>";
        }

        Date start = new Date();
        Map<String, Object> map = FoxyServletUtils.getParamsAsMapEx(request, fileSizeMax);
        map.remove("page");
        MapHelper<String> mh = new MapHelper<String>(map);

//        FoxyMillHttpModel model = null;
//        try {
//            model = modelClass.newInstance();
//        } catch (Exception ex) {
//            throw new LameRuntimeException("error creating model", ex);
//        }
//        model.init(request, response, boxes, foxyService,
//                pageName, mh, globals,
//                //props,
//                sysProps, textsProps,
//                renderEnv.cacheStorage, tmplProviders);

        Object model = bundleLogic.createPageModel(request, response,
                foxyService, pageName, mh //                boxes, globals, sysProps, textsProps,
                //                renderEnv.cacheStorage, tmplProviders
                );

//        RenderCtx rc = RenderCtx.makeRootRenderCtx(
//                model //        SimpleRenderContext rc = new SimpleRenderContext(
//                //                new FoxyMillHttpModel(
//                //                request, response, boxes, foxyService,
//                //                pageName, mh, globals,
//                //                //props,
//                //                sysProps, textsProps,
//                //                renderEnv.cacheStorage)
//                );
//
//        Object pageVal = globals.get("dispatchRequest");
//
//        String res;
//        if (pageVal instanceof IRenderable) {
//            IRenderable page = (IRenderable) pageVal;
//
//            boolean abortOutput = false;
//            try {
//                page.render(rc);
//            } catch (ReturnFromRenderingException ex) {
//                abortOutput = ex.abortRendering;
//            } catch (FallThroughToLogonPageException ex) {
//                Object fttl = globals.get("fallThroughToLogon");
//                if (fttl instanceof IRenderable) {
//                    IRenderable rrr = (IRenderable)fttl;
//
//                    if (logger.isInfoEnabled()) {
//                        logger.info("rendering page=" + pageName + ", requested fall-through to logon...");
//                    }
//
//                RenderCtx ftRc = RenderCtx.makeRootRenderCtx(
//                        model //        SimpleRenderContext rc = new SimpleRenderContext(
//                    );
//
//                }
//
//                if (model instanceof IPageRedirectionCapableModel) {
//                    IPageRedirectionCapableModel prcm = (IPageRedirectionCapableModel)model;
//                    prcm.sendRedirect(bundleLogic.getFallThroughLoginUrl());
//                    abortOutput = true;
//                } else {
//                    throw new LameRuntimeException("model is not " +
//                            "redirection capable but fall-through request " +
//                            "emerged while rendering", ex);
//                }
//            }
//            res = abortOutput ? null : rc.getOutputStr();
//        } else {
//            res = BaseUtils.safeToString(pageVal);
//        }

        Object dispatcher = globals.get("dispatchRequest");
        String res;
        try {
            res = internalRenderIt(dispatcher, model);
        } catch (FallThroughToLogonPageException ex) {
            //System.out.println("!!!! error !!!!");
            res = internalRenderIt(globals.get("fallThroughToLogon"), model);
        } catch (RerenderWithNewModelException ex) {
            //System.out.println("!!!! error 2 !!!!");
            Object nm = ex.proceedModel;
//            if (nm instanceof FoxyMillServletModelBase) {
//                FoxyMillServletModelBase fmsmb = (FoxyMillServletModelBase)nm;
//                fmsmb.setUrlMaker();
//            }
            res = internalRenderIt(dispatcher, nm);
        }

        Date end = new Date();

        if (logger.isInfoEnabled()) {
            logger.info("rendered page=" + pageName + ", time=" + SimpleDateUtils.getElapsedSecondsWithMilliPrec(start, end) + " s., "
                    + "params=" + map);
        }

        return res;
    }

    public String getError() {
        return errorMsg;
    }

    public ILameResourceProvider getResourceProvider() {
        return resProvider;
    }
}
