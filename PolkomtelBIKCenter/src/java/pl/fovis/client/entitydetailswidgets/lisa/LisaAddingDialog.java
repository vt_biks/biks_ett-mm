/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.dialogs.lisa.LisaDataType;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.DateLDV;
import pl.fovis.common.lisa.LisaConstants;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryValue;
import pl.fovis.common.lisa.LisaUtils;
import pl.fovis.common.lisa.StringLDV;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.SimpleDateUtils;

/**
 *
 * @author ctran
 */
public class LisaAddingDialog extends BaseActionOrCancelDialog {

    private final List<LisaDictColumnBean> columnInfo;
    private final FlexTable table = new FlexTable();
    private final Map<String, LisaDictionaryValue> record = new HashMap<String, LisaDictionaryValue>();

    public LisaAddingDialog(List<LisaDictColumnBean> columnInfo) {
        this.columnInfo = columnInfo;
    }

    @Override
    protected String getDialogCaption() {
        return I18n.dodajPozycje.get();
    }

    public void buildAndShowDialog() {
        super.buildAndShowDialog();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.dodaj.get();
    }

    @Override
    protected void buildMainWidgets() {
        DateTimeFormat dateFormat = DateTimeFormat.getFormat(LisaConstants.LISA_DATE_STING_FORMAT);
        for (LisaDictColumnBean columnBean : columnInfo) {
            if (!columnBean.isNotVisibleAccess()) {
                int rowCnt = table.getRowCount();
                table.setWidget(rowCnt, 0, new Label(columnBean.nameInDb));
                //System.out.println(columnBean.dataType + " " + LisaDataType.getValueFor(columnBean.dataType));
                switch (LisaDataType.getValueFor(columnBean.dataType)) {
                    case DA:
                        DateBox dateBox = new DateBox();
                        dateBox.setFormat(new DateBox.DefaultFormat(dateFormat));
                        dateBox.setValue(SimpleDateUtils.truncateDay(new Date()));
                        table.setWidget(rowCnt, 1, dateBox);
                        break;
                    case I1:
                    case I2:
                    case I8:
                    case I:
                        table.setWidget(rowCnt, 1, new TextBox());
                        break;
                    default:
                        table.setWidget(rowCnt, 1, new TextBox());
                }
            }
        }
        main.add(table);
    }

    @Override
    protected void doAction() {
        int i = 0;
        for (LisaDictColumnBean columnBean : columnInfo) {
            if (!columnBean.isNotVisibleAccess()) {
                switch (LisaDataType.getValueFor(columnBean.dataType)) {
                    case DA:
                        record.put(columnBean.nameInDb, new DateLDV(((DateBox) table.getWidget(i, 1)).getValue()));
                        break;
                    case I1:
                    case I2:
                    case I8:
                    case I:
                        record.put(columnBean.nameInDb, new StringLDV(((TextBox) table.getWidget(i, 1)).getValue()));
                        break;
                    default:
                        record.put(columnBean.nameInDb, new StringLDV(((TextBox) table.getWidget(i, 1)).getValue()));
                }
            }
        }
    }

    @Override
    protected String getCancelButtonCaption() {
        return I18n.anuluj.get() /*I18N:  *//*"Anuluj"*/;
    }

    public Map<String, LisaDictionaryValue> getData() {
        return record;
    }
}
