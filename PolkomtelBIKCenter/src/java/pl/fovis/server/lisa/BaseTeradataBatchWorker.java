/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.lisa;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import pl.fovis.common.lisa.LisaConstants;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaUtils;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public abstract class BaseTeradataBatchWorker {

    protected static final int BATCH_SIZE = 1000;
    protected PreparedStatement ps;
    protected List<LisaDictColumnBean> columnsList;
    protected List<LisaDictColumnBean> pkColumnsList;
    protected List<LisaDictColumnBean> nonPkColumnsList;
    protected int add2Batch = 0;

    public BaseTeradataBatchWorker(PreparedStatement ps, List<LisaDictColumnBean> columnsList) {
        this.columnsList = columnsList;
        this.pkColumnsList = LisaUtils.extractPkColumns(columnsList);
        this.nonPkColumnsList = LisaUtils.extractNonPkColumns(columnsList);
        this.ps = ps;
    }

    public void finish() throws SQLException {
        if (add2Batch > 0) {
            ps.executeBatch();
        }
        ps.close();
    }

    public void addBatch(Map<String, String> record) throws SQLException {
//        for (LisaDictColumnBean column : pkColumnsList) {
//            String value = record.get(column.nameInDb);
//            if (BaseUtils.isStrEmptyOrWhiteSpace(value)) {
//                //pk
//                return;
//            }
//        }
        parse2PreparedStatement(record);
        ps.addBatch();
        add2Batch++;
        if (add2Batch == BATCH_SIZE) {
            ps.executeBatch();
            ps.clearBatch();
            add2Batch = 0;
        }
    }

    protected void setParametr4PreparedStatement(PreparedStatement ps, int paramId, LisaDictColumnBean column, String value) throws SQLException {
        int dataType = column.sqlDataType;
        boolean isNull = (LisaConstants.IS_NULLABLE.equals(column.nullable.trim()) && (BaseUtils.isStrEmptyOrWhiteSpace(value)));
        if (isNull) {
            ps.setNull(paramId, dataType);
        } else {
            ps.setObject(paramId, LisaServerUtils.transcodeToLatin1(value.trim()), dataType);
        }
    }

    public abstract void parse2PreparedStatement(Map<String, String> record) throws SQLException;
}
