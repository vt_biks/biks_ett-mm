﻿exec sp_check_version '1.1.7.2';
go

create table bik_news(
	id int identity(1,1) not null primary key,
	title varchar(255) not null,
	text varchar(max) not null,
	author_id int not null references bik_system_user(id),
	date_added datetime not null default CURRENT_TIMESTAMP,
)
go

create table bik_news_readed_user(
	user_id int not null references bik_system_user(id),
	news_id int not null references bik_news(id)
)
go

alter table bik_system_user
add date_added datetime default SYSDATETIME()
go

update bik_system_user set date_added = SYSDATETIME()

exec sp_update_version '1.1.7.2', '1.1.7.3';
go
