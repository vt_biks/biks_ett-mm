/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import java.util.Date;
import java.util.List;
import simplelib.BaseUtils;
import simplelib.BeanWithIntegerIdBase;

/**
 *
 * @author tflorczak
 */
public class SAPBOScheduleBean extends BeanWithIntegerIdBase implements Comparable<SAPBOScheduleBean> {

    public Integer parentId;
    public String name;
    public String destination;
    public String emailAddresses; // email TO
    public String owner;
    public Date creationTime;
    public Date nextruntime;
    public Date expire;
    public int type; // once, hourly, dayly ... CeScheduleType.ONCE
    public int scheduleType;  // pending, success, failed, ... ISchedulingInfo.ScheduleStatus.PAUSED
    public int intervalMinutes;
    public int intervalHours;
    public int intervalDays;
    public int intervalMonths;
    public int intervalNthDay;
    public String format;
    public String parameters;
    // extended
    public String location;
    public Date started;
    public Date ended;
    public Date modifyTime;
    public String errorMessage;
    public int recurring;
    public String cuid;
    public Integer ancestorId;
    public List<String> reportsId;
    public List<String> ancestorsId;
    public List<Integer> connectionsNodeId;
    public List<String> reportsIdFromConnections;
    public String server;
    public String destinationPaths;
    public String kind; // SI_KIND, eg. Webi, Excel, ...

//    public String path; - nieużywane - jest zamiast tego location
    public Date modifyTimeReport;
    public String modifyTimeUniverse;
    public String universes;
    public String connections;
    public String databaseEngine;
    //
    public String emailFromAddresses;
    public String emailTitle;
    public String emailMessage;
    public String destinationUser;
    public String destinationPw;
    public String destinationFolder;

    public SAPBOScheduleBean() {
    }

    public Integer getId() {
        return id;
    }

    public Integer getAncestorId() {
        return ancestorId;
    }

    public String getCuid() {
        return cuid;
    }

    public Integer getParentId() {
        return parentId;
    }

    public String getName() {
        return name;
    }

    public String getDestination() {
        return destination;
    }

    public String getEmailAddresses() {
        return emailAddresses;
    }

    public String getOwner() {
        return owner;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public Date getNextruntime() {
        return nextruntime;
    }

    public Date getExpire() {
        return expire;
    }

    public int getType() {
        return type;
    }

    public int getScheduleType() {
        return scheduleType;
    }

    public int getIntervalMinutes() {
        return intervalMinutes;
    }

    public int getIntervalHours() {
        return intervalHours;
    }

    public int getIntervalDays() {
        return intervalDays;
    }

    public int getIntervalMonths() {
        return intervalMonths;
    }

    public int getIntervalNthDay() {
        return intervalNthDay;
    }

    public String getFormat() {
        return format;
    }

    public String getParameters() {
        return parameters;
    }

    public String getLocation() {
        return location;
    }

    public Date getStarted() {
        return started;
    }

    public Date getEnded() {
        if (scheduleType == 9 || scheduleType == 8 || scheduleType == 0) { //ISchedulingInfo.ScheduleStatus.PENDING, PAUSED, RUNNING:
            return null;
        }
        return ended;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getRecurring() {
        return recurring;
    }

    public Date getModify() {
        return modifyTime;
    }

//    public String getPath() {
//        return path;
//    }
    public String getKind() {
        return kind;
    }

    public Date getModifyTimeReport() {
        return modifyTimeReport;
    }

    public String getModifyTimeUniverse() {
        return modifyTimeUniverse;
    }

    public String getDatabaseEngine() {
        return databaseEngine;
    }

    public String getConnections() {
        return connections;
    }

    public String getUniverses() {
        return universes;
    }

    public String getServer() {
        return BaseUtils.coalesce(server, "");
    }

    /* Zwraca ścieżkę do folderu albo emaile do userów*/
    public String getDestinationPaths() {
        if (!BaseUtils.isStrEmptyOrWhiteSpace(destinationPaths)) {
            return destinationPaths;
        } else if (!BaseUtils.isStrEmptyOrWhiteSpace(emailAddresses)) {
            return emailAddresses;
        } else {
            return "";
        }
//        return !BaseUtils.isStrEmptyOrWhiteSpace(destinationPaths) ? destinationPaths : emailAddresses;
    }

    @Override
    public String toString() {
        return "SAPBOScheduleBean{" + "id=" + id + ", parentId=" + parentId + ", name=" + name + ", destination=" + destination + ", emailAddresses=" + emailAddresses + ", owner=" + owner + ", creationTime=" + creationTime + ", nextruntime=" + nextruntime + ", expire=" + expire + ", type=" + type + ", scheduleType=" + scheduleType + ", intervalMinutes=" + intervalMinutes + ", intervalHours=" + intervalHours + ", intervalDays=" + intervalDays + ", intervalMonths=" + intervalMonths + ", intervalNthDay=" + intervalNthDay + ", format=" + format + ", parameters=" + parameters + ", location=" + location + ", started=" + started + ", ended=" + ended + ", errorMessage=" + errorMessage + '}';
    }

    public String toCSVString() {
        return id + ";" + parentId + ";" + name + ";" + scheduleType + ";" + type + ";" + destination + ";" + emailAddresses + ";" + format + ";" + owner + ";";
    }

    @Override
    public int compareTo(SAPBOScheduleBean o) {
        return (o == null || o.id == null) ? -1 : -o.id.compareTo(id);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof SAPBOScheduleBean) {
            return id == ((SAPBOScheduleBean) o).id;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
