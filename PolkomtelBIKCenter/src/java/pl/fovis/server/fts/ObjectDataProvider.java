/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.FieldNameConversion;

/**
 *
 * @author pmielanczuk
 */
public class ObjectDataProvider {

    protected INamedSqlsDAO<Object> adhocDao;
    protected Map<Integer, String> attrIdToNameMap;

    public ObjectDataProvider(INamedSqlsDAO<Object> adhocDao) {
        this.adhocDao = adhocDao;
        readAttrs();
    }

    protected final void readAttrs() {
        List<Map<String, Object>> rows = adhocDao.execNamedQueryCFN("fts_readBikAttrs", FieldNameConversion.ToLowerCase);
        attrIdToNameMap = new HashMap<Integer, String>();
        for (Map<String, Object> row : rows) {
            attrIdToNameMap.put(adhocDao.sqlNumberToInt(row.get("id")), (String) row.get("name"));
        }
    }

    // size < 0 ---> full
    public Map<Integer, Map<String, Object>> getUnprocessedDataPack(int size) {
        List<Map<String, Object>> rows = adhocDao.execNamedQueryCFN("fts_getUnprocessedDataPack", FieldNameConversion.ToLowerCase, size);

        Map<Integer, Map<String, Object>> res = new HashMap<Integer, Map<String, Object>>();

        for (Map<String, Object> row : rows) {
            int attrId = adhocDao.sqlNumberToInt(row.get("attr_id"));
            int treeId = adhocDao.sqlNumberToInt(row.get("tree_id"));
            int nodeKindId = adhocDao.sqlNumberToInt(row.get("node_kind_id"));
            String attrName = attrIdToNameMap.get(attrId);

            if (attrName == null) {
                continue;
            }

            int objId = adhocDao.sqlNumberToInt(row.get("node_id"));

            Map<String, Object> vals = res.get(objId);

            if (vals == null) {
                vals = new HashMap<String, Object>();
                vals.put("tree_id", treeId);
                vals.put("node_kind_id", nodeKindId);
                res.put(objId, vals);
            }

            vals.put(attrName, row.get("value"));
        }

        return res;
    }

    public void startProcessingObj(int objId) {
        adhocDao.execNamedCommand("fts_startProcessingObj", objId);
    }

    public void markObjectAsProcessed(int objId) {
        adhocDao.execNamedCommand("fts_markObjectAsProcessed", objId);
    }

    public void markAllObjsUnprocessed() {
        adhocDao.execNamedCommand("fts_markAllObjsUnprocessed");
    }

    public void removeAllObjsFromProcessed() {
        adhocDao.execNamedCommand("fts_removeAllObjsFromProcessed");
    }
}
