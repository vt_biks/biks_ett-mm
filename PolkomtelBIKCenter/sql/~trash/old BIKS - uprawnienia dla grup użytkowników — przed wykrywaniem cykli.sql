if not exists(select * from sys.objects where object_id = object_id(N'bik_system_user_group') and type in (N'U'))
    create table bik_system_user_group (
        id int not null primary key identity(1,1),
        name varchar(512) not null,
        domain varchar(512) null,
        is_built_in tinyint not null default(1),
        is_deleted tinyint not null default(0),
        unique (domain, name)        
    );
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_system_group_in_group') and type in (N'U'))
    create table bik_system_group_in_group (
        id int not null primary key identity(1,1),
        group_id int not null references bik_system_user_group(id),
        member_group_id int not null references bik_system_user_group(id),
        is_deleted tinyint not null default(0),
        unique (group_id, member_group_id),        
        unique (member_group_id, group_id)        
    );
go

-- drop table bik_system_user_in_group

if not exists(select * from sys.objects where object_id = object_id(N'bik_system_user_in_group') and type in (N'U'))
    create table bik_system_user_in_group (
        id int not null primary key identity(1,1),
        user_id int not null references bik_system_user(id),
        group_id int not null references bik_system_user_group(id),
        is_deleted tinyint not null default(0),
        unique (group_id, user_id),        
        unique (user_id, group_id)        
    );
go


------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_custom_right_role_user_entry') and name = 'group_id')
 alter table bik_custom_right_role_user_entry add group_id int null references bik_system_user_group (id);
go


alter table bik_custom_right_role_user_entry alter column user_id int null;
go


------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants]
go

create procedure [dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants] as
begin
  set nocount on
  
  declare @diag_level int = 0
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': start!'

  if coalesce((select val from bik_app_prop where name = 'customRightRolesTreeSelector'), '') = ''
  begin
    if @diag_level > 0 print 'bik_app_prop[customRightRolesTreeSelector] is empty, nothing to do';
  
    return;
  end;
  
  truncate table bik_custom_right_role_action_branch_grant;

  -- specjalna akcja widoczno�ci w drzewie na zak�adkach - ShowInTree
  declare @show_in_tree_act_id int = (select id from bik_node_action where code = 'ShowInTree');

  declare @trees_for_roles_0 table (role_id int not null, tree_id int not null, unique (role_id, tree_id));

  insert into @trees_for_roles_0 (role_id, tree_id)
  select distinct crr.id as role_id, t.id as tree_id
  from
    bik_custom_right_role crr cross apply dbo.fn_split_by_sep(crr.tree_selector, ',', 7) x inner join bik_tree t on
      substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
      substring(x.str, 1, 1) <> '@' and t.code = x.str;  

  -- select * from bik_custom_right_role_user_entry
  
  if @diag_level > 1 select * from @trees_for_roles_0;
  
  declare @right_for_group_flat table (role_id int not null, group_id int not null, tree_id int null, node_id int null,
    unique(group_id, role_id, tree_id, node_id));

  declare @groups_to_process table (group_id int not null primary key);

  declare @processed_groups table (group_id int not null primary key);

  declare @unprocessed_groups table (group_id int not null primary key);
  
  insert into @unprocessed_groups (group_id) select id from bik_system_user_group;

  insert into @groups_to_process (group_id)
  select id
  from bik_system_user_group
  where id not in (select member_group_id from bik_system_group_in_group);
  
  while exists (select 1 from @groups_to_process)
  begin
    if @diag_level > 0
    begin
      declare @gtpstr varchar(max) = null;
      
      select @gtpstr = case when @gtpstr is null then '' else @gtpstr + ',' end + cast(group_id as varchar(20))
      from @groups_to_process;
      
      print 'groups to process: ' + @gtpstr;    
    end;
    
    insert into @right_for_group_flat (role_id, group_id, tree_id, node_id)
    select crrue.role_id, crrue.group_id, crrue.tree_id, crrue.node_id
    from @groups_to_process gtp inner join bik_custom_right_role_user_entry crrue on crrue.group_id = gtp.group_id;

    insert into @right_for_group_flat (role_id, group_id, tree_id, node_id)
    select distinct rfgf.role_id, gig.member_group_id, rfgf.tree_id, rfgf.node_id
    from @right_for_group_flat rfgf inner join bik_system_group_in_group gig on rfgf.group_id = gig.group_id
      inner join @groups_to_process gtp on gig.member_group_id = gtp.group_id
    where not exists 
      (select 1 from @right_for_group_flat rfgf2 
       where rfgf2.role_id = rfgf.role_id and rfgf2.group_id = gig.member_group_id and
         (rfgf2.tree_id is null and rfgf.tree_id is null or rfgf2.tree_id = rfgf.tree_id) and
         (rfgf2.node_id is null and rfgf.node_id is null or rfgf2.node_id = rfgf.node_id));

    insert into @processed_groups (group_id) select group_id from @groups_to_process;

    delete from @unprocessed_groups where group_id in (select group_id from @groups_to_process);

    delete from @groups_to_process;
         
    insert into @groups_to_process (group_id)
    select group_id
    from @unprocessed_groups
    where group_id not in 
      (select member_group_id from bik_system_group_in_group where group_id in (select group_id from @unprocessed_groups));
  end;
  
  if @diag_level > 0 and exists(select 1 from @unprocessed_groups)
  begin
    declare @ugstr varchar(max) = null;
    
    select @ugstr = case when @ugstr is null then '' else @ugstr + ',' end + cast(group_id as varchar(20))
    from @unprocessed_groups;
    
    print 'nothing new to process, but there are unprocessed groups: ' + @ugstr;    
  end;

  declare @right_for_user_flat table (role_id int not null, user_id int not null, tree_id int null, node_id int null,
    unique(user_id, role_id, tree_id, node_id));

  insert into @right_for_user_flat (user_id, role_id, tree_id, node_id)
  select distinct user_id, role_id, tree_id, node_id
  from
    (select uig.user_id, rfgf.role_id, rfgf.tree_id, rfgf.node_id
     from bik_system_user_in_group uig inner join @right_for_group_flat rfgf on uig.group_id = rfgf.group_id
     union
     select user_id, role_id, tree_id, node_id
     from bik_custom_right_role_user_entry crrue where user_id is not null) x;

  if @diag_level > 1 select * from bik_custom_right_role_user_entry;

  if @diag_level > 1 select *
  from --bik_custom_right_role_user_entry 
    @right_for_user_flat crrue left join
    @trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id;      

  insert into bik_custom_right_role_action_branch_grant (action_id, user_id, tree_id, branch_ids)
  select distinct @show_in_tree_act_id, crrue.user_id, coalesce(crrue.tree_id, rts.tree_id) as tree_id, --crrue.node_id,
    coalesce(n.branch_ids, '') as branch_ids
  from --bik_custom_right_role_user_entry 
    @right_for_user_flat crrue left join
    @trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id
  left join bik_node n on n.id = crrue.node_id
  where
    not exists -- nie dorzucamy wpisu, je�eli wyst�puje dla niego wpis nadrz�dny (na wy�szym poziomie w drzewie)
    (
      select --
        1
        --crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
        from --bik_custom_right_role_user_entry 
          @right_for_user_flat crrue2 left join
          @trees_for_roles_0 rts2 on crrue2.tree_id is null and rts2.role_id = crrue2.role_id
      left join bik_node n2 on n2.id = crrue2.node_id
      where crrue2.user_id = crrue.user_id and coalesce(n.branch_ids, '') like coalesce(n2.branch_ids, '') + '_%'
        and coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
        --and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
    );

  insert into bik_custom_right_role_action_branch_grant (action_id, user_id, tree_id, branch_ids)
  select distinct naicrr.action_id, crrue.user_id, coalesce(crrue.tree_id, rts.tree_id) as tree_id, --crrue.node_id,
    coalesce(n.branch_ids, '') as branch_ids
  from --bik_custom_right_role_user_entry 
    @right_for_user_flat crrue inner join bik_node_action_in_custom_right_role naicrr on crrue.role_id = naicrr.role_id left join
    @trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id
  left join bik_node n on n.id = crrue.node_id
  where
    naicrr.action_id <> @show_in_tree_act_id and
    not exists -- nie dorzucamy wpisu, je�eli wyst�puje dla niego wpis nadrz�dny (na wy�szym poziomie w drzewie)
    (
      select --
        1
        --crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
        from --bik_custom_right_role_user_entry 
          @right_for_user_flat crrue2 inner join bik_node_action_in_custom_right_role naicrr2 on crrue2.role_id = naicrr2.role_id left join
          @trees_for_roles_0 rts2 on crrue2.tree_id is null and rts2.role_id = crrue2.role_id
      left join bik_node n2 on n2.id = crrue2.node_id
      where naicrr2.action_id = naicrr.action_id and crrue2.user_id = crrue.user_id and coalesce(n.branch_ids, '') like coalesce(n2.branch_ids, '') + '_%'
        and coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
        --and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
    );
  
end;
go


--select * from bik_custom_right_role
-- select * from bik_system_user

delete from bik_custom_right_role_user_entry where group_id is not null;

delete from bik_system_group_in_group;

delete from bik_system_user_in_group;

delete from bik_system_user_group;

insert into bik_system_user_group (name, is_built_in)
values
  ('A', 0),
  ('B', 0);

insert into bik_system_group_in_group (group_id, member_group_id)
values 
  ((select id from bik_system_user_group where name = 'A'), (select id from bik_system_user_group where name = 'B'));

insert into bik_custom_right_role_user_entry (role_id, group_id)
values
  ((select id from bik_custom_right_role where code = 'DqmAdmin'), (select id from bik_system_user_group where name = 'A'));

insert into bik_system_user_in_group (user_id, group_id)
values
  ((select id from bik_system_user where login_name = 'aqq'), (select id from bik_system_user_group where name = 'B'));

exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;

select t.code, crrabg.branch_ids, crrabg.action_id, na.code
from bik_custom_right_role_action_branch_grant crrabg inner join bik_tree t on t.id = crrabg.tree_id
  inner join bik_node_action na on crrabg.action_id = na.id
where user_id = (select id from bik_system_user where login_name = 'aqq');

------------------------

-- select * from bik_node where id = 272200;


    declare @user_id int = (select id from bik_system_user where login_name = 'aqq'), @tree_id int, @branch_ids varchar(1000);

    select @tree_id = tree_id, @branch_ids = branch_ids
    from bik_node where id = 272200;

    select distinct action_id from bik_custom_right_role_action_branch_grant
    where user_id = @user_id and tree_id = @tree_id and @branch_ids like coalesce(branch_ids, '') + '%';    

go

------------------------

-- select * from bik_node where id = 272200;

    declare @user_id int = (select id from bik_system_user where login_name = 'aqq');

    declare @custom_Right_Roles_Tree_Selector varchar(8000) = (select val from bik_app_prop where name = 'customRightRolesTreeSelector');

    select tree_id, action_id from bik_custom_right_role_action_branch_grant
    where user_id = @user_id and branch_ids = '';
    

    select distinct t.id as tree_id, naicr.action_id
    from
    dbo.fn_split_by_sep(@custom_Right_Roles_Tree_Selector, ',', 7) x inner join bik_tree t on
    substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
    substring(x.str, 1, 1) <> '@' and t.code = x.str
    inner join
    (bik_custom_right_role_user_entry crrue left join
    (select distinct crr.id as role_id, t.id as tree_id from
    bik_custom_right_role crr cross apply dbo.fn_split_by_sep(crr.tree_selector, ',', 7) x inner join bik_tree t on
    substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
    substring(x.str, 1, 1) <> '@' and t.code = x.str
    ) rts on crrue.tree_id is null and rts.role_id = crrue.role_id
    ) on t.id = coalesce(crrue.tree_id, rts.tree_id)
    inner join bik_node_action_in_custom_right_role naicr on naicr.role_id = crrue.role_id
    where crrue.user_id = @user_id and crrue.node_id is null



select * from bik_app_prop
