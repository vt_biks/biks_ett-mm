exec sp_check_version '1.0.86.3';
go

declare @treeId int
declare @userIdTm int
declare @userIdGs int

declare @CategoryNodeKindId int
declare @nodeKindId int
--declare @treeId int
declare @parentNodeId int
declare @userId int
declare @parentNodeId2 int

select @CategoryNodeKindId=id from bik_node_kind where code='GlossaryCategory'
select @nodeKindId=id from bik_node_kind where code='Glossary'
select @treeId=id from bik_tree where code='Glossary'



insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
-- 
values(null,@CategoryNodeKindId,'RODOS',@treeId,null,null,null,null,0,0,'',0)
select  @parentNodeId =id from bik_node where name='RODOS' and tree_id = @treeId
exec sp_node_init_branch_id @treeId, null

/*----Artykuły---*/
insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
-- 
values(@parentNodeId,@CategoryNodeKindId,'Artykuły',@treeId,null,null,null,null,0,0,'',0)
select  @parentNodeId2 =id from bik_node where name='Artykuły' and tree_id = @treeId and parent_node_id in (select id from bik_node where name='RODOS')
exec sp_node_init_branch_id @treeId, null


insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
values(@parentNodeId2,@nodeKindId,'Wprowadzenie',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null


insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
-- 
values(@parentNodeId2,@nodeKindId,'Raport bazy abonenckiej',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Macierz migracji',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Cykl życia klienta',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Staże',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Analiza przeżycia',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Szeregi czasowe',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Prognoza',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Rozwój bazy produktowej',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Ranking',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

/*----Zdarzenia---*/
insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
 
values(@parentNodeId,@CategoryNodeKindId,'Zdarzenia',@treeId,null,null,null,null,0,0,'',0)
select  @parentNodeId2 =id from bik_node where name='Zdarzenia' and tree_id = @treeId and parent_node_id in (select id from bik_node where name='RODOS')
exec sp_node_init_branch_id @treeId, null


insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Aktywacja',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Deaktywacja',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Reaktywacja',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Zawieszenie',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Odwieszenie',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Migracja',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Zmiana',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null


/*----Miary----*/
insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
 
values(@parentNodeId,@CategoryNodeKindId,'Miary',@treeId,null,null,null,null,0,0,'',0)
select  @parentNodeId2 =id from bik_node where name='Miary' and tree_id = @treeId and parent_node_id in (select id from bik_node where name='RODOS')
exec sp_node_init_branch_id @treeId, null



insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Baza abonencka',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Liczba aktywacji',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Liczba deaktywacji (churn brutto)',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Liczba reaktywacji',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Liczba zawieszeń',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Liczba odwieszeń',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Liczba migracji',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Liczba zmian',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Przyrost brutto',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Churn netto',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
 values(@parentNodeId2,@nodeKindId,'Przyrost netto',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null


/*---Wymiary---*/
insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
 
values(@parentNodeId,@CategoryNodeKindId,'Wymiary',@treeId,null,null,null,null,0,0,'',0)
select  @parentNodeId2 =id from bik_node where name='Wymiary' and tree_id = @treeId and parent_node_id in (select id from bik_node where name='RODOS')
exec sp_node_init_branch_id @treeId, null


insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Czas',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Status',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Powód deaktywacji',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Plan cenowy',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Taryfa',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Linia produktowa',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Baza produktowa',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Metoda płatności',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Typ klienta',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Cykl billingowy',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Kontraktowy',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Komercyjny',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Sprzedaż',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Promocja',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Sprzęt',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Opiekun',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 values(@parentNodeId2,@nodeKindId,'Właściciel',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)  
values(@parentNodeId2,@nodeKindId,'Adres zamieszkania',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)  
values(@parentNodeId2,@nodeKindId,'Klient kluczowy',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Nowy klient',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)  
values(@parentNodeId2,@nodeKindId,'Typ procesu AA',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Segment kartosimowy',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Staż w bazie produktowej',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Staż w zawieszeniu',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdTm)
exec sp_node_init_branch_id @treeId, null

------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 
values(null,@CategoryNodeKindId,'ZAKRES',@treeId,null,null,null,null,0,0,'',0)
select  @parentNodeId =id from bik_node where name='ZAKRES' and tree_id = @treeId
exec sp_node_init_branch_id @treeId, null


/*----Zdarzenia zatrzymania---*/
insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 
values(@parentNodeId,@CategoryNodeKindId,'Zdarzenia zatrzymania',@treeId,null,null,null,null,0,0,'',0)
select  @parentNodeId2 =id from bik_node where name='Zdarzenia zatrzymania' and tree_id = @treeId and parent_node_id in (select id from bik_node where name='ZAKRES')
exec sp_node_init_branch_id @treeId, null


insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 values(@parentNodeId2,@nodeKindId,'Zdarzenie zatrzymania',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdGs)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 values(@parentNodeId2,@nodeKindId,'Zdarzenie podpisania umowy zatrzymaniowej',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdGs)
exec sp_node_init_branch_id @treeId, null


insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 values(@parentNodeId2,@nodeKindId,'Zdarzenie anulowania umowy zatrzymaniowej',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdGs)
exec sp_node_init_branch_id @treeId, null


/*----Miary---*/
insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 
values(@parentNodeId,@CategoryNodeKindId,'Miary',@treeId,null,null,null,null,0,0,'',0)
select  @parentNodeId2 =id from bik_node where name='Miary' and tree_id = @treeId and parent_node_id in (select id from bik_node where name='ZAKRES')
exec sp_node_init_branch_id @treeId, null


insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 values(@parentNodeId2,@nodeKindId,'Liczba zdarzeń zatrzymania',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdGs)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)  
values(@parentNodeId2,@nodeKindId,'Liczba zdarzeń podpisania umowy zatrzymaniowej',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdGs)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 values(@parentNodeId2,@nodeKindId,'Liczba zdarzeń anulowania umowy zatrzymaniowej',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('',0,scope_identity(),0,@userIdGs)
exec sp_node_init_branch_id @treeId, null


/*----Klienci---*/
insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 
values(@parentNodeId,@CategoryNodeKindId,'Klienci',@treeId,null,null,null,null,0,0,'',0)
select  @parentNodeId2 =id from bik_node where name='Klienci' and tree_id = @treeId and parent_node_id in (select id from bik_node where name='ZAKRES')
exec sp_node_init_branch_id @treeId, null


insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 values(@parentNodeId2,@nodeKindId,'Postpaid Indywidualni i "Mały" Biznes',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('Brak definicji',0,scope_identity(),0,@userIdGs)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 values(@parentNodeId2,@nodeKindId,'Postpaid "Duży Biznes"',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('Brak definicji',0,scope_identity(),0,@userIdGs)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in)
  values(@parentNodeId2,@nodeKindId,'Użytkownicy Mixplus',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) values('Brak definicji',0,scope_identity(),0,@userIdGs)
exec sp_node_init_branch_id @treeId, null

/*----Kanał sprzedaży---*/
insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 
values(@parentNodeId,@CategoryNodeKindId,'Inne',@treeId,null,null,null,null,0,0,'',0) 
select  @parentNodeId2 =id from bik_node where name='Inne' and tree_id = @treeId and parent_node_id in (select id from bik_node where name='ZAKRES')
exec sp_node_init_branch_id @treeId, null


insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 values(@parentNodeId2,@nodeKindId,'Kanał sprzedaży',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) 
 values('',0,scope_identity(),0,@userIdGs)
exec sp_node_init_branch_id @treeId, null

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted,indexVer,old_name,is_built_in) 
 values(@parentNodeId2,@nodeKindId,'Data zdarzenia zatrzymania',@treeId,null,null,null,null,0,0,'',0)
insert into bik_metapedia(body,official,node_id,def_status,user_id) 
 values('',0,scope_identity(),0,@userIdGs)
exec sp_node_init_branch_id @treeId, null

exec sp_update_version '1.0.86.3', '1.0.86.4';
go
