/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.deferredstringconverter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import simplelib.IStringMatcher;

/**
 *
 * @author pmielanczuk
 */
public class DeferredStringConverter {

    public static String convert(List<IDeferredRegExprReplacer<?, ?>> replacers, String str) {

        List<IStringMatcher> matchers = new ArrayList<IStringMatcher>();
        List<Integer> replacerIdxesForMatcher = new ArrayList<Integer>();
        int replacerCnt = replacers.size();

        for (int i = 0; i < replacerCnt; i++) {
            IDeferredRegExprReplacer<?, ?> r = replacers.get(i);

            IStringMatcher[] mms = r.makeMatchers(str);
            int mmsLen = mms.length;

            for (int j = 0; j < mmsLen; j++) {
                matchers.add(mms[j]);
                replacerIdxesForMatcher.add(i);
            }
        }

        final int msLength = matchers.size();

        boolean[] inactives = new boolean[msLength];
        int startPoses[] = new int[msLength];
        int endPoses[] = new int[msLength];

        int activeCnt = msLength;

        int lastEndPos = 0;

        List<Integer> chunkStarts = new ArrayList<Integer>();
        List<Integer> chunkEnds = new ArrayList<Integer>();
        List<Integer> replacementReplacerIdxes = new ArrayList<Integer>();
        List<List<Object>> replacementsColls = new ArrayList<List<Object>>();

        for (int i = 0; i < replacerCnt; i++) {
            replacementsColls.add(new ArrayList<Object>());
        }

        while (activeCnt > 0) {

            int bestMsIdx = -1;

            for (int i = 0; i < msLength; i++) {
                if (inactives[i]) {
                    continue;
                }

                final IStringMatcher currM = matchers.get(i);

                if (endPoses[i] == 0 || startPoses[i] < lastEndPos) {
                    boolean found = currM.find(lastEndPos);
                    if (!found) {
                        inactives[i] = true;
                        activeCnt--;
                        continue;
                    }
                    startPoses[i] = currM.start();
                    endPoses[i] = currM.end();
                }

                if (bestMsIdx == -1 || startPoses[bestMsIdx] > startPoses[i]) {
                    bestMsIdx = i;
                }
            }

            if (activeCnt == 0) {
                break;
            }

            chunkStarts.add(lastEndPos);
            chunkEnds.add(startPoses[bestMsIdx]);
            int replacerIdxForMatcher = replacerIdxesForMatcher.get(bestMsIdx);
            @SuppressWarnings("unchecked")
            IDeferredRegExprReplacer<IStringMatcher, ?> replacer = (IDeferredRegExprReplacer) replacers.get(replacerIdxForMatcher);
            Object replacement = replacer.getDeferredReplacement((IStringMatcher) matchers.get(bestMsIdx));
            replacementReplacerIdxes.add(replacerIdxForMatcher);
            replacementsColls.get(replacerIdxForMatcher).add(replacement);

            lastEndPos = endPoses[bestMsIdx];
            endPoses[bestMsIdx] = 0;
        }

        List<Iterator<String>> replaced = new ArrayList<Iterator<String>>();

        for (int i = 0; i < replacerCnt; i++) {
            IDeferredRegExprReplacer<?, ?> replacer = replacers.get(i);
            @SuppressWarnings("unchecked")
            Iterator<String> replacedStrs = replacer.resolveReplacements((Collection) replacementsColls.get(i)).iterator();
            replaced.add(replacedStrs);
        }

        StringBuilder sb = new StringBuilder();

        int chunkCnt = chunkStarts.size();
        for (int i = 0; i < chunkCnt; i++) {
            sb.append(str, chunkStarts.get(i), chunkEnds.get(i));
            Iterator<String> iter = replaced.get(replacementReplacerIdxes.get(i));
            sb.append(iter.next());
        }

        sb.append(str, lastEndPos, str.length());

        return sb.toString();
    }
}
