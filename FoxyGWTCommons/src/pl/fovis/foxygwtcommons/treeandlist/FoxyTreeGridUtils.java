/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.treeandlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import pl.fovis.foxygwtcommons.ITreeBroker;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public class FoxyTreeGridUtils {

    public static <IDT, BEAN> void registerBeansAndAddChildRows(ITreeRowAndBeanStorage<IDT, Map<String, Object>, BEAN> storage,
            IFoxyTreeGrid<IDT> tree, Iterable<BEAN> nodes, IDT parentId) {

        List<Map<String, Object>> childRows = new ArrayList<Map<String, Object>>();

        for (BEAN n : nodes) {
            Map<String, Object> row = storage.registerAndConvertBeanToRow(n);
            childRows.add(row);
        }

        tree.addChildNodes(tree.getStorage().getRowById(parentId), childRows);
    }

    public static <IDT, BEAN> void registerBeansAndAddRows(ITreeRowAndBeanStorage<IDT, Map<String, Object>, BEAN> storage,
            IFoxyTreeGrid<IDT> tree, Iterable<BEAN> nodes) {
        ITreeBroker<Map<String, Object>, IDT> treeBroker = storage.getTreeBroker();
        Map<IDT, List<Map<String, Object>>> nodesByParents = new HashMap<IDT, List<Map<String, Object>>>();
        for (BEAN n : nodes) {
            Map<String, Object> row = storage.registerAndConvertBeanToRow(n);
            IDT parentId = treeBroker.getParentId(row);
            BaseUtils.addToCollectingMapWithList(nodesByParents, parentId, row);
        }

        for (Entry<IDT, List<Map<String, Object>>> e : nodesByParents.entrySet()) {
            Map<String, Object> parentRow = storage.getRowById(e.getKey());
            tree.addChildNodes(parentRow, e.getValue());
        }
    }
}
