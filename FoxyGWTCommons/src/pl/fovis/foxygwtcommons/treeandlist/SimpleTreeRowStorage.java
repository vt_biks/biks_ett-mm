/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.treeandlist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import pl.fovis.foxygwtcommons.ITreeBroker;

/**
 *
 * @author wezyr
 */
public class SimpleTreeRowStorage<IDT, TN> implements ITreeRowStorage<IDT, TN> {
    
    protected ITreeBroker<TN, IDT> treeBroker;
    protected Map<IDT, TN> rowsMap = new LinkedHashMap<IDT, TN>();
    protected Map<IDT, List<TN>> childRows = new HashMap<IDT, List<TN>>();
    
    public SimpleTreeRowStorage() {
        // no-op, treeBroker must be set later!
    }
    
    public SimpleTreeRowStorage(ITreeBroker<TN, IDT> treeBroker) {
        this.treeBroker = treeBroker;
    }
    
    protected IDT getIdValOfRow(TN row) {
        return treeBroker.getNodeId(row);
    }
    
    public TN getRowById(IDT nodeId) {
        return rowsMap.get(nodeId);
    }
    
    public void addRow(TN row) {
        rowsMap.put(getIdValOfRow(row), row);
        
        IDT parentId = treeBroker.getParentId(row);
        
        List<TN> cr = childRows.get(parentId);
        if (cr == null) {
            cr = new LinkedList<TN>();
            childRows.put(parentId, cr);
        }
        cr.add(row);
    }
    
    public Collection<TN> getAllRows() {
        return rowsMap.values();
    }
    
    public ITreeBroker<TN, IDT> getTreeBroker() {
        return treeBroker;
    }
    
    public Collection<TN> getChildRows(TN row) {
        return childRows.get(row == null ? null : getIdValOfRow(row));
    }
    
    protected void collectDescendantAndSelfIds(IDT nodeId, Collection<IDT> ids) {
        if (nodeId == null) {
            return;
        }
        
        ids.add(nodeId);
        
        Collection<TN> crs = childRows.get(nodeId);
        if (crs != null) {
            for (TN cr : crs) {
                collectDescendantAndSelfIds(getIdValOfRow(cr), ids);
            }
        }
    }
    
    public void deleteRowById(IDT nodeId) {
        List<IDT> idsToDelete = new ArrayList<IDT>();
        
        collectDescendantAndSelfIds(nodeId, idsToDelete);
        
        rowsMap.keySet().removeAll(idsToDelete);
        childRows.keySet().removeAll(idsToDelete);        
    }
}
