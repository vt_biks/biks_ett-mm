/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import commonlib.LameUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class BasePumpExecutor extends BaseExecutor {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected Integer actualLogId;
    protected boolean isCancelled;
    protected IPumpLogger pLogger = new IPumpLogger() {
        @Override
        public void pumpLog(String str, Integer proc) {
            BasePumpExecutor.this.pumpLog(str, proc);
        }

        @Override
        public MssqlConnectionConfig getConnectionConfig() {
            return BasePumpExecutor.this.getConnectionConfig();
        }
    };
    protected String urlForPublicAccess;

    public BasePumpExecutor(String urlForPublicAccess) {
        this.urlForPublicAccess = urlForPublicAccess;
    }

    protected void pumpLog(String str, Integer proc) {
        insertLogToDB(str);
        if (proc != null) {
            updateMainLog(proc + " % - " + str, null);
        } else {
            updateMainLog(str, null);
        }
    }

    protected void insertLogToDB(String text) {
        execNamedCommand("insertLogToDB", text + " «usedMem: " + BaseUtils.padIntEx(LameUtils.usedMemory(), 0, ' ', " ") + "»", actualLogId);
    }

    protected void updateMainLogEx(String text) {
        execNamedCommand("updateMainLog", text, actualLogId);
    }

    protected void updateMainLog(String text, Integer status) {
        if (status != null) {
            updateMainLogWithStatus(text, status);
        } else {
            updateMainLogEx(text);
        }
    }

    protected void updateMainLogWithStatus(String text, int status) {
        execNamedCommand("updateMainLogWithStatus", text, actualLogId, status);
    }

    protected void insertLogSizeToDB() {
        Integer logSize = (Integer) execNamedQuerySingleVal("getLogSize", true, "size");
        insertLogToDB("Actual database log file size (in KB): " + (logSize * 8));
    }

    protected void updateLogsWithStatus(String text, int status) {
//        if (text.length() > 990) {
//            text = text.substring(0, 990);
//        }
        if (status == PumpConstants.LOG_LOAD_STATUS_ERROR) {
            text = "Error: " + text;
        }
        insertLogToDB(text);
        updateMainLogWithStatus(text, status);
    }

    protected void insertMemoryDiagsToLog(boolean startOfPump, String sourceName, boolean withAction) {
        String msg = "Using Java memory (" + (startOfPump ? "start" : "end") + "): "
                + LameUtils.getMemoryDiagInfoCompact(withAction, "; ");
        insertLogToDB(msg);
        if (logger.isStageEnabled()) {
            logger.stage("[loadId=" + actualLogId + ", source=" + sourceName + "] " + msg);
        }
    }
}
