select 
  w.id, w.word, count(distinct wioa.obj_id) as usage_in_obj_cnt
from bik_fts_word w inner join bik_fts_word_in_obj_attr wioa on w.id = wioa.word_id
where w.word like '% %'
group by w.id, w.word
order by usage_in_obj_cnt desc
;


select 
  sw.id, sw.word, min(w.word), count(distinct wioa.obj_id) as usage_in_obj_cnt
from 
  bik_fts_word w inner join 
  bik_fts_word_in_obj_attr wioa on w.id = wioa.word_id inner join
  bik_fts_word_stemming ws on w.id = ws.word_id inner join
  bik_fts_stemmed_word sw on sw.id = ws.stemmed_word_id
where sw.word like '% %'
group by sw.id, sw.word
order by usage_in_obj_cnt desc
;


/*

word -> stemmed_words

*/


select
  *, (select word from bik_fts_stemmed_word sw where sw.id = coalesce(ws1.stemmed_word_id, ws2.stemmed_word_id)) as stemmed_word
from
  (bik_fts_word w1 inner join bik_fts_word_stemming ws1 on w1.id = ws1.word_id) full outer join
  (bik_fts_word w2 inner join bik_fts_word_stemming ws2 on w2.id = ws2.word_id) on ws1.stemmed_word_id = ws2.stemmed_word_id
where
  w1.id < w2.id or (ws1.id is null or ws2.id is null)
order by stemmed_word
;


select
  *, (select count(*) from bik_fts_word_stemming ws2 where ws2.word_id = w.id) as other_stemming_count
from 
  bik_fts_stemmed_word sw inner join 
  bik_fts_word_stemming ws on sw.id = ws.stemmed_word_id inner join 
  bik_fts_word w on w.id = ws.word_id
where sw.word = 'aktywny'
order by sw.id, w.id





select
  *, (origin_flag + best_flag) * cnt as stemming_strength
from
(
select
  w.word, ws.origin_flag, sw.word as stemmed_word, count(*) as cnt, max(ws2.origin_flag) as best_flag
from
  bik_fts_word w inner join 
  bik_fts_word_stemming ws on w.id = ws.word_id inner join
  bik_fts_stemmed_word sw on ws.stemmed_word_id = sw.id inner join
  bik_fts_word_stemming ws2 on ws.stemmed_word_id = ws2.stemmed_word_id
where
  (len(sw.word) > 1 or ws.origin_flag <> 2)
  and ws2.origin_flag <> 2
group by
  w.id, w.word, ws.id, ws.origin_flag, sw.word
--order by cnt desc, sw.word
) x
order by stemming_strength desc, stemmed_word





select
  *
from
  bik_fts_word w cross apply
(
select
  top 1 *, (origin_flag + best_flag) * cnt as stemming_strength
from
(
select
  ws.word_id, ws.origin_flag, sw.word as stemmed_word, count(*) as cnt, max(ws2.origin_flag) as best_flag
from
  bik_fts_word_stemming ws inner join
  bik_fts_stemmed_word sw on ws.stemmed_word_id = sw.id inner join
  bik_fts_word_stemming ws2 on ws.stemmed_word_id = ws2.stemmed_word_id
where
  ws.word_id = w.id and
  (len(sw.word) > 1 or ws.origin_flag <> 2)
  and ws2.origin_flag <> 2
group by
  ws.word_id, ws.id, ws.origin_flag, sw.word
--order by cnt desc, sw.word
) x
order by stemming_strength desc
) x
where w.word like '%aktywn%'
order by cnt desc, stemmed_word




update wst
set 
  origin_flag = case when (
    select top 1 x.stemmed_word_id
    from
    (
      select
        ws.word_id, ws.origin_flag, sw.id as stemmed_word_id, sw.word as stemmed_word, count(*) as cnt, max(ws2.origin_flag) as best_flag
      from
        bik_fts_word_stemming ws inner join
        bik_fts_stemmed_word sw on ws.stemmed_word_id = sw.id inner join
        bik_fts_word_stemming ws2 on ws.stemmed_word_id = ws2.stemmed_word_id
      where
        ws.word_id = wst.word_id and
        (len(sw.word) > 1 or ws.origin_flag <> 2)
        and ws2.origin_flag <> 2
      group by
        ws.word_id, ws.id, ws.origin_flag, sw.id, sw.word
    ) x
    order by (origin_flag + best_flag) * (2 + cnt) desc
    ) = wst.stemmed_word_id then origin_flag | 32 else origin_flag & 31 end
from
  bik_fts_word_stemming wst
;  



select
  *
from bik_fts_word w inner join bik_fts_word_stemming ws on ws.word_id = w.id
inner join bik_fts_stemmed_word sw on sw.id = ws.stemmed_word_id
where ws.origin_flag >= 32 --and w.word = sw.word 
and w.word not like '% %' and w.word not like '%[_]%'
and w.word like '%aktywn%'
;


update sw
set weight = 
--select *, 
  case when sw.word like '% %' then 2 else 1 end *
  case when best_origin_flag >= 52 then 2
       when best_origin_flag >= 32 then 1.7
       when best_origin_flag >= 4 then 1.3
       else 1 end *
  1000.0 / (500 + usage_in_obj_cnt + total_usage_cnt) *
  case when len(sw.word) < 3 then 0.7 else 1 end-- as weight
-- select *
from
  bik_fts_stemmed_word sw inner join 
  (
  select 
    ws.stemmed_word_id, max(ws.origin_flag) as best_origin_flag, count(distinct wioa.obj_id) as usage_in_obj_cnt,
    count(*) total_usage_cnt
  from
    bik_fts_word_stemming ws inner join 
    bik_fts_word w on ws.word_id = w.id inner join 
    bik_fts_word_in_obj_attr wioa on w.id = wioa.word_id
  group by ws.stemmed_word_id
  --order by total_usage_cnt desc
  ) as x on sw.id = x.stemmed_word_id



update sw
set
  weight = case when w1 < 1 or w2 < 1 then case when w1 < w2 then w2 else w1 end else w1 + w2 end  
from
  bik_fts_stemmed_word sw inner join
  (
  select r.id, 
    (select weight from bik_fts_stemmed_word w where w.word = substring(r.word, 1, charindex(' ', r.word) - 1)) as w1,
    (select weight from bik_fts_stemmed_word w where w.word = substring(r.word, charindex(' ', r.word) + 1, len(r.word))) as w2
  from
    bik_fts_stemmed_word r
  ) x on x.id = sw.id  
where sw.word like '% %'  
;
  



where usage_in_obj_cnt > 1 and word like '%visiona%'

order by --usage_ratio desc
  weight desc
  



select
  *
from bik_fts_word w1 inner join bik_fts_word w2 on w2.word = substring(w1.word, 1, len(w1.word) - 3)
where w1.word not like '% %' and w1.word not like '%[_]%' and w1.word like '%owe'



select * from bik_fts_stemmed_word 
where len(word) < 3
order by weight desc

