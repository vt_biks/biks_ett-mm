/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import java.util.List;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class ContinuationBeanCollector<T> extends BeanCollectorBase<T> {

    protected IParametrizedContinuation<List<T>> conn;

    public ContinuationBeanCollector(int bufforSize, IParametrizedContinuation<List<T>> conn) {
        super(bufforSize);
        this.conn = conn;
    }

    @Override
    protected void insertListIntoDatabaseAndClear() {
        conn.doIt(list);
        list.clear();
    }
}
