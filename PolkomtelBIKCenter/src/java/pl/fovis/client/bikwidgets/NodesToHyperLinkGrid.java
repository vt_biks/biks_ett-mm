/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.common.TreeNodeBean;

/**
 *
 * @author bfechner
 */
public class NodesToHyperLinkGrid extends NodesGridBase {

    protected List<Integer> usedLink;

    public NodesToHyperLinkGrid(List<Integer> usedLink) {
        this.usedLink = usedLink;
    }
    public Map<TreeNodeBean, CheckBox> cbxs = new HashMap<TreeNodeBean, CheckBox>();

    protected Widget getSelectedWidget(final TreeNodeBean n) {

        CheckBox cb = new CheckBox();
        cb.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                tnb = n;

            }
        });
        if (usedLink != null && usedLink.contains(n.id)) {
            cb.setValue(true);
        }
        cbxs.put(n, cb);
        return cb;
    }
}
