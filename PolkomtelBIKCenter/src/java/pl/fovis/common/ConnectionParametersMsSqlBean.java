package pl.fovis.common;

import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import java.io.Serializable;
import java.util.List;
import pl.bssg.metadatapump.common.MsSqlExPropConfigBean;

/**
 *
 * @author pkobik
 */
public class ConnectionParametersMsSqlBean implements Serializable {

    public List<ConnectionParametersDBServerBean> servers;
    public MsSqlExPropConfigBean exProp;
}
