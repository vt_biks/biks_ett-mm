/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import simplelib.BeanWithIntIdAndNameBase;

/**
 *
 * @author ctran
 */
public class ConnectionParametersPlainFileBean extends BeanWithIntIdAndNameBase {

    public boolean isActive;
    public String sourceFilePath;
    public String sourceFolderPath;
    public boolean isRemote;
    public String domain;
    public String username;
    public String password;
    public Integer treeId;
    public String treeCode;
    public Integer isIncremental;
}
