-----------------------------------------------------------------
-----------------------------------------------------------------
-- czyszczenie tabeli bik_node where is_deleted = 1
-----------------------------------------------------------------
-----------------------------------------------------------------

-- select * from bik_node where is_deleted = 1

/*

-- bik_node <- bik_blog_linked
-- bik_node <- bik_blog <- bik_blog_linked
-- bik_node <- bik_blog <- bik_obj_in_body_node

insert into #to_del_bik_node ...
insert into #to_del_bik_blog ...
...
delete from bik_blog_linked where blog_id in (select id from #to_del_bik_blog)
delete from bik_blog_linked where node_id in (select id from #to_del_bik_node)

a <- b <- c <- d [<- e]
a <- d <- e

#0: a <- b
#0: a <- d
#1: b <- c
#1: d <- e
#2: c <- e

*/

set nocount on

if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#table_to_cleanup'))
drop table #table_to_cleanup
go

create table #table_to_cleanup (name sysname null, 
key_col_name sysname null,
ree_table_name sysname not null,
ree_fk_col_name sysname null,
min_lvl int null,
lvl int not null, 
max_lvl int null,
--delete_cmd varchar(max) null,
has_deps int null,
unique (name, key_col_name, ree_table_name, ree_fk_col_name))

declare @diag int = 0

insert into #table_to_cleanup (ree_table_name, lvl, min_lvl) values ('bik_node', 0, 0)

declare @lvl int = 0

while 1 = 1
begin
  if @diag > 0 print 'starting pass #' + cast(@lvl as varchar(10))
  if @diag > 1 begin
    print 'tables to cleanup on this lvl:'
    select ree_table_name from #table_to_cleanup where lvl = @lvl
  end
  
  declare @rc int

  declare @clean_me_before table (name sysname not null, 
    key_col_name sysname not null,
    ree_table_name sysname not null,
     ree_fk_col_name sysname not null)
  delete from @clean_me_before
  
  insert into @clean_me_before (name, key_col_name, ree_table_name, ree_fk_col_name)
  select distinct ReferencedTableName, ReferencedColumn, table_name, Name
  --select * 
  from vw_ww_foreignkeys
  where ReferencedTableName in (select ree_table_name from #table_to_cleanup where min_lvl = @lvl)
  --and table_name not in (select ree_table_name from #table_to_cleanup where name is not null)

  set @rc = @@rowcount

  if @diag > 1 begin
    print 'tables to clear before:'
    select ree_table_name from @clean_me_before
  end
    
  if @rc = 0 begin
    if @diag > 1 print 'nothing to add, break'
    break
  end

  set @lvl = @lvl + 1    

  insert into #table_to_cleanup (name, key_col_name, ree_table_name, ree_fk_col_name, lvl)
  select name, key_col_name, ree_table_name, ree_fk_col_name, @lvl
  from @clean_me_before

  update #table_to_cleanup set min_lvl = (select min(lvl) from #table_to_cleanup ttc where ttc.ree_table_name = #table_to_cleanup.ree_table_name)
  where lvl = @lvl
end

update #table_to_cleanup set max_lvl = (select max(lvl) from #table_to_cleanup ttc where #table_to_cleanup.ree_table_name = ttc.ree_table_name)

update #table_to_cleanup set has_deps = case when exists (select 1 from #table_to_cleanup ttc where ttc.name = #table_to_cleanup.ree_table_name) then 1 else 0 end

select * from #table_to_cleanup

select distinct 'create table #to_del_' + name + ' (' + key_col_name + ' int not null)'
from #table_to_cleanup
where name is not null

select 
'insert into #to_del_' + ree_table_name + ' select id from ' + ree_table_name + ' where ' + 
  case when name is null then 'is_deleted = 1'
       else ree_fk_col_name + ' in (select ' + key_col_name + ' from #to_del_' + name + ')' end
from #table_to_cleanup where has_deps = 1
order by lvl, case when ree_table_name = name then 0 else 1 end

print 'cleanup order:'
select --*, 
  'delete from ' + ree_table_name + ' where ' + 
  case when name is null then 'id in (select id from #to_del_bik_node)'
       else ree_fk_col_name + ' in (select ' + key_col_name + ' from #to_del_' + name + ')' end
from #table_to_cleanup
order by lvl desc
