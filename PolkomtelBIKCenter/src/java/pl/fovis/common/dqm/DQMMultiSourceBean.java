/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class DQMMultiSourceBean implements Serializable {

    public Integer id;
    public int testId;
    public int testNodeId;
    public int sourceId;
    public String sourceName;
    public String serverName;
    public String databaseName;
    public Integer databaseNodeId;
    public Integer dbServerId;
    public String sqlTest;
    public String codeName;
    public String dataScope;
    public String resultFileName;
    public String biksTableName;
    public String optTestGrantsPerObjects;
    //
    public Integer connectionId;
    public String connectionName;
}
