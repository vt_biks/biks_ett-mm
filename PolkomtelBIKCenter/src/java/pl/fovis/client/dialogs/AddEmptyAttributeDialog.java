/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.ListBox;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class AddEmptyAttributeDialog extends BaseActionOrCancelDialog {

    protected String caption;
    protected IParametrizedContinuation<AttributeBean> saveCont;
    protected AttributeBean attr = new AttributeBean();
    protected ListBox listBox;
    protected Map<String, AttributeBean> attrNamesAndType;

    @Override
    protected String getDialogCaption() {
        return caption;
    }

    public void buildAndShowDialog(String caption, Map<String, AttributeBean> attrNamesAndType, IParametrizedContinuation<AttributeBean> saveCont) {
        this.attrNamesAndType = attrNamesAndType;
        this.caption = caption;
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }

    @Override
    protected void doAction() {
        if (attr == null) {
            attr = new AttributeBean();
        }
        attr.atrName = listBox.getValue(listBox.getSelectedIndex());
        attr.atrLinValue = "";
        saveCont.doIt(attr);
    }

    @Override
    protected String getActionButtonCaption() {
              return I18n.zapisz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        listBox = new ListBox();
        List<String> attrlist = new ArrayList<String>();
        for (String value : attrNamesAndType.keySet()) {
            attrlist.add(value);
        }
        attrlist = sortedList(attrlist);
        for (String value : sortedList(attrlist)) {
            listBox.addItem(value);
        }
        main.add(listBox);
    }

    protected List<String> sortedList(List<String> attr) {

        Collections.sort(attr, new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                return String.CASE_INSENSITIVE_ORDER.compare(o1, o2);
            }
        });

        return attr;
    }
}
