/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.client.dialogs.TreeSelectorForConfluenceDialog;
import pl.fovis.common.i18npoc.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class NodeActionAddObjectFromConfluence extends NodeActionConfluenceBase {

    @Override
    public String getCaption() {
        return I18n.dodajObiektZConfluence.get();
    }

    @Override
    public void execute() {
        new TreeSelectorForConfluenceDialog().buildAndShowDialog(new IParametrizedContinuation<Map<String, String>>() {
            @Override
            public void doIt(Map<String, String> selectedNodes) {
                final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.pobieranieObiketowZConfluenceProszeCzekac.get(), true);
                BIKClientSingletons.getService().saveConfluenceObjects(selectedNodes, node != null ? node.id : null, treeBean.id, new AsyncCallback<Void>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        infoDialog.hideDialog();
                        BIKClientSingletons.showInfo(I18n.bladWZapisieObiektowZConfluence.get() /* I18N:  */);
                    }

                    @Override
                    public void onSuccess(Void result) {
                        infoDialog.hideDialog();
                        BIKClientSingletons.showInfo(I18n.zapisanoObiektyZConfluence.get() /* I18N:  */);
                        if (node == null) {
                            refreshAndRedisplayData();
                        } else {
                            refreshRedisplayDataAndSelectRecord(node.id);
                        }
                    }
                });
            }
        });
    }
}
