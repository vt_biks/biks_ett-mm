/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.erwin;

import commonlib.LameUtils;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pgajda
 */
public class ErwinStaXParser {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    static final String ENTITY_GROUPS = "Entity_Groups";
    static final String TABLE_ENTITY = "EntityProps";
    static final String TABLE_ATTRIBUTE_MAIN = "Attribute";
    static final String TABLE_COLUMN = "AttributeProps";
    static final String SHAPE_ENTITY = "ER_Model_ShapeProps";
    static final String RALATION_ENTITY = "RelationshipProps";
    static final String KEYGROUP_ENTITY = "Key_GroupProps";
    static final String KEYGROUPMEMBER_ENTITY = "Key_Group_MemberProps";
    static final String SUBJECT_AREA = "Subject_Area";
    static final String SUBJECT_AREA_ENTITY = "Subject_AreaProps";
    //static final String RELATION_SHAPE = "ER_Model_ShapeProps";
    //ER_Model_ShapePropsinSubjectAreaMain
    protected String fileName;
    protected List<ErwinTable> erwinTableList;
    protected List<ErwinShape> erwinShapeList;
    protected List<ErwinTableColumn> erwinColumnList;
    protected List<ErwinRelationship> erwinRelationshipList;
    protected List<ErwinKeyGroup> erwinKeyGroupList;
    protected List<ErwinKeyGroupMember> erwinKeyGroupMemberList;
    protected List<ErwinSubjectArea> erwinSubjectAreaList;

    public ErwinStaXParser(String fileName) {
        this.fileName = fileName;
        erwinTableList = new ArrayList<ErwinTable>();
        erwinShapeList = new ArrayList<ErwinShape>();
        erwinColumnList = new ArrayList<ErwinTableColumn>();
        erwinRelationshipList = new ArrayList<ErwinRelationship>();
        erwinKeyGroupList = new ArrayList<ErwinKeyGroup>();
        erwinKeyGroupMemberList = new ArrayList<ErwinKeyGroupMember>();
        erwinSubjectAreaList = new ArrayList<ErwinSubjectArea>();
    }

    public List<Object> readObject() {
        List<Object> list = new ArrayList<Object>();
        String elementName = null, attributeName = null, attributeAreaName = null, attributeAreaId = null;

        try {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            // Setup a new eventReader
            InputStream in = new FileInputStream(fileName);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
            XMLEvent event;

            IErwinXMLElement erwinElement = null;
            boolean inErwinTable = false;
            boolean inShapeEntity = false;
            boolean inColumnEntity = false;
            boolean inRelationship = false;
            boolean inKeyGroup = false;
            boolean inKeyGroupMember = false;
            boolean inSubjectArea = false;
            boolean inSubjectAreaMain = false;
            //boolean inEntityGroup = false;

            while (eventReader.hasNext()) {
                event = eventReader.nextEvent();

                if (event.isStartElement()) {
                    elementName = event.asStartElement().getName().getLocalPart();

                    if (elementName.equals(TABLE_ATTRIBUTE_MAIN)) {
                        Attribute at = event.asStartElement().getAttributeByName(new QName("name"));
                        attributeName = at.getValue();
                    } else if (elementName.equals(RALATION_ENTITY)) {
                        erwinElement = new ErwinRelationship();
                        erwinRelationshipList.add((ErwinRelationship) erwinElement);
                        inRelationship = true;
                    } else if (elementName.equals(KEYGROUP_ENTITY)) {
                        erwinElement = new ErwinKeyGroup();
                        erwinKeyGroupList.add((ErwinKeyGroup) erwinElement);
                        inKeyGroup = true;
                    } else if (elementName.equals(KEYGROUPMEMBER_ENTITY)) {
                        erwinElement = new ErwinKeyGroupMember();
                        erwinKeyGroupMemberList.add((ErwinKeyGroupMember) erwinElement);
                        if (erwinKeyGroupList.size() > 0) {
                            erwinElement.setPropertieValue("KeyGroupLongId",
                                    (String) erwinKeyGroupList.get(erwinKeyGroupList.size() - 1).getPropertieValue("Long_Id"));
                        }
                        inKeyGroupMember = true;
                    } else if (elementName.equals(TABLE_ENTITY)) {
                        erwinElement = new ErwinTable();
                        erwinTableList.add((ErwinTable) erwinElement);
                        inErwinTable = true;
                    } else if (elementName.equals(SHAPE_ENTITY)) {
                        erwinElement = new ErwinShape();
                        erwinShapeList.add((ErwinShape) erwinElement);
                        if (erwinSubjectAreaList.size() > 0) {
                            erwinElement.setPropertieValue("Area_Long_Id", attributeAreaId);
                            //(String) erwinSubjectAreaList.get(erwinSubjectAreaList.size() - 1).getPropertieValue("Long_Id").get(0)
                        }

                        inShapeEntity = true;
                    } else if (elementName.equals(TABLE_COLUMN)) {
                        erwinElement = new ErwinTableColumn();
                        erwinColumnList.add((ErwinTableColumn) erwinElement);
                        erwinElement.setPropertieValue("Name", attributeName);
                        if (erwinTableList.size() > 0) {
                            erwinElement.setPropertieValue("Table_Long_Id",
                                    (String) erwinTableList.get(erwinTableList.size() - 1).getPropertieValue("Long_Id"));
                        }
                        inColumnEntity = true;
                    } else if (elementName.equals(SUBJECT_AREA_ENTITY)) {
                        erwinElement = new ErwinSubjectArea();
                        erwinSubjectAreaList.add((ErwinSubjectArea) erwinElement);
                        inSubjectArea = true;
                    } else if (elementName.equals(SUBJECT_AREA)) {
                        Attribute at = event.asStartElement().getAttributeByName(new QName("name"));
                        attributeAreaName = at.getValue();
                        at = event.asStartElement().getAttributeByName(new QName("id"));
                        attributeAreaId = at.getValue();

                        inSubjectAreaMain = true;
                    } else if (inColumnEntity) {
                        setErwinElement(erwinElement, elementName, eventReader);
                        continue;
                    } else if (inErwinTable || inShapeEntity || inRelationship || inKeyGroup || inKeyGroupMember || inSubjectArea) {
                        setErwinElement(erwinElement, elementName, eventReader);
                    }
                }

                if (event.isEndElement()) {
                    elementName = event.asEndElement().getName().getLocalPart();

                    if (elementName.equals(TABLE_ENTITY)) {
                        inErwinTable = false;
                    } else if (elementName.equals(SHAPE_ENTITY)) {
                        //System.out.println("SHAPE_ENTITY END");
                        inShapeEntity = false;
                    } else if (elementName.equals(TABLE_COLUMN)) {
                        inColumnEntity = false;
                    } else if (elementName.equals(TABLE_ATTRIBUTE_MAIN)) {
                        inColumnEntity = false;
                    } else if (elementName.equals(RALATION_ENTITY)) {
                        inRelationship = false;
                    } else if (elementName.equals(KEYGROUP_ENTITY)) {
                        inKeyGroup = false;
                    } else if (elementName.equals(KEYGROUPMEMBER_ENTITY)) {
                        inKeyGroupMember = false;
                    } else if (elementName.equals(SUBJECT_AREA_ENTITY)) {
                        inSubjectArea = false;
                    } else if (elementName.equals(SUBJECT_AREA)) {
                        inSubjectAreaMain = false;
                    }
                }

                /*
                 if (inAttributeMainEntity && event.isStartElement()) {
                 System.out.println(event.asStartElement().getName());
                 Attribute at1 = event.asStartElement().getAttributeByName( new QName("name") );

                 //for (Iterator<Attribute> it = (Iterator<Attribute> )(event.asStartElement().getAttributes()); it.hasNext();) {
                 for (Iterator it = (event.asStartElement().getAttributes()); it.hasNext();) {
                 Attribute at = (Attribute) it.next();
                 System.out.println("atr==>" + at.getName() + "==>" + at.getValue());
                 }
                 }
                 */
            }

        } catch (FileNotFoundException e) {
            if (logger.isErrorEnabled()) {
                logger.error("File not found", e);
            }
        } catch (XMLStreamException e) {
            if (logger.isErrorEnabled()) {
                logger.error("XML Stream exception", e);
            }
        }

        return list;
    }

    protected void setErwinElement(IErwinXMLElement erwinElement, String elementName, XMLEventReader eventReader)
            throws XMLStreamException {
        XMLEvent event;
        String elementValue;

        if (erwinElement != null && erwinElement.checkIfReadPropertie(elementName)) {
            if (eventReader.hasNext()) {
                event = eventReader.nextEvent();
                if (event.isCharacters()) {
                    elementValue = event.asCharacters().getData();
                    //System.out.println("EL[" + elementName + "]==>" + elementValue);
                    erwinElement.setPropertieValue(elementName, elementValue);
                }
            }
        }
    }

    public List<ErwinTable> getErwinTableList() {
        return erwinTableList;
    }

    public List<ErwinShape> getErwinShapeList() {
        return erwinShapeList;
    }

    public List<ErwinTableColumn> getErwinTableColumnList() {
        return erwinColumnList;
    }

    public List<ErwinRelationship> getErwinRelationshipList() {
        return erwinRelationshipList;
    }

    public List<ErwinKeyGroup> getErwinKeyGroupList() {
        return erwinKeyGroupList;
    }

    public List<ErwinKeyGroupMember> getErwinKeyGroupMemberList() {
        return erwinKeyGroupMemberList;
    }

    public List<ErwinSubjectArea> getErwinSubjectAreaList() {
        return erwinSubjectAreaList;
    }

}
