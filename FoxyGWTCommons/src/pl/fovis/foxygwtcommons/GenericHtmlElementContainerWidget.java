/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author wezyr
 */
@SuppressWarnings("deprecation")
public abstract class GenericHtmlElementContainerWidget extends ComplexPanel {

//    public static <T extends GenericHtmlElementContainerWidget> T finishWrapingElementAsWidget(T widget) {
//
//        Element element = widget.getElement();
//
//        // Assert that the element is attached.
//        assert Document.get().getBody().isOrHasChild(element);
//
//        // Mark it attached and remember it for cleanup.
//        widget.onAttach();
//        RootPanel.detachOnWindowClose(widget);
//
//        return widget;
//    }
    public static <T extends GenericHtmlElementContainerWidget> T wrap(Element element, T widget) {
        // Assert that the element is attached.
        assert Document.get().getBody().isOrHasChild(element);

        widget.setElement(element);

        // Mark it attached and remember it for cleanup.
        widget.onAttach();
        RootPanel.detachOnWindowClose(widget);

        return widget;
    }

    protected GenericHtmlElementContainerWidget(Element ele) {
        if (ele != null) {
            setElement(ele);
        }
    }

    public GenericHtmlElementContainerWidget() {
        setElement(DOM.createElement(getElementNameFinal()));
    }

    protected final String getElementNameFinal() {
        return getElementName();
    }

    protected abstract String getElementName();

    public void setElementOnInit(Element ele) {
        setElement(ele);
    }

    @Override
    public void add(Widget w) {
        super.add(w, getElement());
    }

    public void insert(Widget w, int beforeIndex) {
        super.insert(w, getElement(), beforeIndex, true);
    }
}
