/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author wezyr
 */
public abstract class ResizeSensitiveWidgetHelper implements IResizeSensitiveWidget {

    protected IsWidget iw;

    public ResizeSensitiveWidgetHelper(IsWidget iw) {
        this.iw = iw;
    }

    @Override
    public abstract void setFixedHeight(int height);

    @Override
    public Widget asWidget() {
        return iw.asWidget();
    }
}
