/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.mltx;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.bikpages.LoginPageBase;
import pl.fovis.client.mltx.history.HistoryAction;
import pl.fovis.client.mltx.history.MultiXHistoryToken;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author pku
 */
public class MultixLoginPage extends LoginPageBase {

    public static HistoryAction getHistoryAction() {
        return new HistoryAction() {
            @Override
            public Widget performAction(String id) {
                return new MultixLoginPage();
            }
        };
    }

    public MultixLoginPage() {
        super();
    }

    @Override
    protected void addOptButtons(FlexTable ft, FlexTable.FlexCellFormatter flexCellFormatter, int row) {
        AnchorAsHyperlink registerLn = new AnchorAsHyperlink(I18n.rejestracja.get(), MultiXHistoryToken.REGISTRATION.hname);
        AnchorAsHyperlink passwordResetLn = new AnchorAsHyperlink(I18n.forgotPassword.get(), MultiXHistoryToken.PASSWORD_RESET_SEND.hname);
        ft.setWidget(row, 1, registerLn);
        flexCellFormatter.setHorizontalAlignment(row++, 1, HasHorizontalAlignment.ALIGN_RIGHT);
        ft.setWidget(row, 0, passwordResetLn);
        flexCellFormatter.setHorizontalAlignment(row, 0, HasAlignment.ALIGN_LEFT);
    }
}
