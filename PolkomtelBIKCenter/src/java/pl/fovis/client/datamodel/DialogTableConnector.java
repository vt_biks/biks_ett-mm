/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.datamodel;

import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.tecna.gwt.connectors.client.CornerPoint;
import pl.tecna.gwt.connectors.client.Diagram;
import pl.tecna.gwt.connectors.client.Point;
import pl.tecna.gwt.connectors.client.elements.Connector;
import pl.tecna.gwt.connectors.client.elements.Section;

/**
 *
 * @author pgajda
 */
public class DialogTableConnector {

    protected static int CONNECTOR_SHIFT = 2;

    protected Widget startPoint;
    protected Widget endPoint;
    protected DataModelConnector connection;
    protected Diagram diagram;
    protected String startPointComment;
    protected String endtPointComment;
    protected String relationName;
    protected String connectorComment;
    protected String startTableName;
    protected String endTableName;

    public DialogTableConnector(String relationName, Diagram diagram) {
        this.diagram = diagram;
        this.relationName = relationName;
        startPointComment = "";
        endtPointComment = "";
        connectorComment = "";
    }

    public void setStartTableName(String startTableName) {
        this.startTableName = startTableName;
    }

    public void setEndTableName(String endTableName) {
        this.endTableName = endTableName;
    }

    public void setConnectorComment(String connectorComment) {
        this.connectorComment = connectorComment;
    }

    public void setStartPointComment(String comment) {
        startPointComment = comment;
    }

    public void setEndPointComment(String comment) {
        endtPointComment = comment;
    }

    public String getStartPointComment() {
        return startPointComment;
    }

    public String getEndtPointComment() {
        return endtPointComment;
    }

    public void drawConnection() {
        if (startPoint == null || endPoint == null || diagram == null) {
            return;
        }

        int startLeft = startPoint.getAbsoluteLeft() - diagram.boundaryPanel.getAbsoluteLeft() - 7;
        int startTop = (startPoint.getAbsoluteTop() + (startPoint.getOffsetHeight() / 2)) - diagram.boundaryPanel.getAbsoluteTop();
        int startWidth = startPoint.getOffsetWidth() + 13;
        int endLeft = endPoint.getAbsoluteLeft() - diagram.boundaryPanel.getAbsoluteLeft() - 7;
        int endTop = (endPoint.getAbsoluteTop() + (endPoint.getOffsetHeight() / 2)) - diagram.boundaryPanel.getAbsoluteTop();
        int endWidth = endPoint.getOffsetWidth() + 13;

        if ((startLeft + startWidth) <= endLeft) {
            startLeft += startWidth + CONNECTOR_SHIFT;
        } else if (startLeft > (endLeft + endWidth)) {
            endLeft += endWidth + CONNECTOR_SHIFT;
        } else {
            startLeft = (startLeft - CONNECTOR_SHIFT >= 0) ? (startLeft - CONNECTOR_SHIFT) : startLeft;
            endLeft = (endLeft - CONNECTOR_SHIFT >= 0) ? (endLeft - CONNECTOR_SHIFT) : endLeft;
        }

        if (connection != null) {
            connection.removeFromDiagram(diagram);
            connection = null;
        }

        connection = new DataModelConnector(startLeft, startTop, endLeft, endTop);
        connection.startEndPoint.setTitle(relationName + ":" + startPointComment);
        connection.endEndPoint.setTitle(relationName + ":" + endtPointComment);
        connection.setConnectorComment(relationName + "\n" + startTableName + "(" + startPointComment.replaceAll("\n", " ") + ")"
                + " \u21D2 " + endTableName + "(" + endtPointComment.replaceAll("\n", " ") + ")");

        connection.showOnDiagram(diagram);

    }

    public void setStartPoint(Widget widget) {
        this.startPoint = widget;
    }

    public void setEndPoint(Widget widget) {
        this.endPoint = widget;
    }

    class DataModelConnector extends Connector {

        protected String connectorComment;

        public DataModelConnector(int startLeft, int startTop, int endLeft, int endTop) {
            super(startLeft, startTop, endLeft, endTop);
        }

        public void setConnectorComment(String comment) {
            this.connectorComment = comment;
        }

        @Override
        public void deselect() {
            this.isSelected = false;

            for (Section section : sections) {
                section.deselect();
                GWTUtils.setStyleAttribute(section, "boxShadow", "none");
            }
        }

        @Override
        public void select() {
            this.isSelected = true;

            for (Section section : sections) {
                section.select();
                GWTUtils.setStyleAttribute(section, "boxShadow", "2px 2px 7px blue");
            }
        }

        @Override
        public void drawSections(List<CornerPoint> cp, boolean isSelected) {
            super.drawSections(cp, isSelected);

            try {

                for (Section section : sections) {
                    if (connectorComment != null) {
                        section.setTitle(connectorComment);
                    }
                    GWTUtils.setElementAttribute(section, "borderColor", "#00BFFF");
                    if (!isSelected) {
                        GWTUtils.removeElementAttribute(section.getElement(), "boxShadow");
                    }
                }
            } catch (IllegalArgumentException e) {
                logCornerPointsData();
                this.calculateStandardPointsPositions();
            }
        }
    }

    class DataModelSection extends Section {

        public DataModelSection(Point startPoint, Point endPoint, Connector connector) throws IllegalArgumentException {
            super(startPoint, endPoint, connector);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void deselect() {

            if (getElement().getChildCount() != 0 && getElement().getChild(0) instanceof com.google.gwt.user.client.Element) {
                GWTUtils.setElementAttribute(this, "borderColor", "#FF0000");
            }

            // Deselect Section Decorations
            if (startPointDecoration != null) {
                this.startPointDecoration.deselect();
            }
            if (endPointDecoration != null) {
                this.endPointDecoration.deselect();
            }
        }
    }
}
