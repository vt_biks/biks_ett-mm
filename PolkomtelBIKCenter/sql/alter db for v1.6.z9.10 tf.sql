﻿exec sp_check_version '1.6.z9.10';
go

delete from bik_translation where kind = 'adef' and code = 'Źródło' 
insert into bik_translation(code, txt, lang, kind)
values ('Źródło', 'Konektor', 'pl', 'adef')
insert into bik_translation(code, txt, lang, kind)
values ('Źródło', 'Connector', 'en', 'adef')

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_dqm_log]') and name = 'data_read_secs')
begin
	alter table bik_dqm_log add data_read_secs int null;
end;
go

exec sp_update_version '1.6.z9.10', '1.6.z9.11';
go