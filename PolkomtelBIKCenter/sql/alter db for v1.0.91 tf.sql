﻿exec sp_check_version '1.0.91';
go
------------------------------------
------------------------------------

alter table aaa_query
add report_update datetime
go

alter table aaa_report_object
add report_update datetime
go

update aaa_query
set report_update=iw.SI_UPDATE_TS
from aaa_query aq
join INFO_WEBI iw on iw.si_id=aq.report_si_id

------------------------------------
------------------------------------

exec sp_update_version '1.0.91', '1.0.91.2';
go