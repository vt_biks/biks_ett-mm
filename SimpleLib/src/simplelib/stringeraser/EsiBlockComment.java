/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.stringeraser;

/**
 *
 * @author pmielanczuk
 */
public class EsiBlockComment extends ESISqlPrefixSuffixDelimitedString {

    public EsiBlockComment() {
        super("/*", "*/");
    }
}
