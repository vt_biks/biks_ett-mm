/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.*;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class TreeSelectorForUsersToCopyRole extends TreeSelectorForUsers {

    protected String userName;
    protected IParametrizedContinuation<Pair<TreeNodeBean, List<Boolean>>> saveCont;
    RadioButton MoveMRb = new RadioButton("typeM", I18n.przenies.get() /* I18N:  */);
    RadioButton LeaveRb = new RadioButton("typeM", I18n.pozostaw.get() /* I18N:  */);
    RadioButton CopyRb = new RadioButton("type" /* I18N: no */, I18n.kopiuj.get() /* I18N:  */);
    RadioButton MoveRb = new RadioButton("type" /* I18N: no */, I18n.przenies.get() /* I18N:  */);
    VerticalPanel vp;
//    protected Map<String,Boolean> joinedObjChangeAct= new HashMap<String,Boolean>();

    @Override
    protected Widget buildOptionalRightSide() {
        VerticalPanel rightVp = new VerticalPanel();
        vp = new VerticalPanel();
        rightVp.setWidth("200px"); //300
        rightVp.setStyleName("EntityDetailsPane selectRoleInDialogPane");
        main.setWidth("800px");

        ScrollPanel scroll = new ScrollPanel();
        vp.add(new HTML(I18n.powiazaniaWRoliGlownej.get() /* I18N:  */ + ":"));
        addStyleRadioButton(MoveMRb);
        MoveMRb.setValue(true);
        addStyleRadioButton(LeaveRb);

        vp.add(new HTML(I18n.powiazaniaWRoliPomocniczej.get() /* I18N:  */ + ":"));
        CopyRb.setValue(true);
        addStyleRadioButton(CopyRb);
        addStyleRadioButton(MoveRb);
        rightVp.add(vp);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(scroll,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 118);//77

        rightVp.add(scroll);

        return rightVp;
    }

    protected void addStyleRadioButton(RadioButton rb) {
        rb.setStyleName("AttrLbl");
        vp.add(rb);
    }

    public void buildAndShowDialog(int thisNodeId, String treeKind,
            IParametrizedContinuation<Pair<TreeNodeBean, List<Boolean>>> saveCont,
            IJoinedObjSelectionManager<Integer> alreadyJoinedManager, boolean isInheritToDescendantsCheckBoxVisible, Integer optUpSelectedNodeId, String userName) {
//        this.thisNodeId = thisNodeId;
//        this.treeKind = treeKind;
        this.isInheritToDescendantsCheckBoxVisible = isInheritToDescendantsCheckBoxVisible;
        this.optUpSelectedNodeId = optUpSelectedNodeId;
        this.userName = userName;
        this.saveCont = saveCont;
        super.buildAndShowDialog(thisNodeId, null, alreadyJoinedManager);
    }

    @Override
    protected void doAction() {
        List<Boolean> joinedObjChangeAct = new ArrayList<Boolean>();
        joinedObjChangeAct.add(MoveMRb.getValue());
        joinedObjChangeAct.add(CopyRb.getValue());

        saveCont.doIt(new Pair<TreeNodeBean, List<Boolean>>(selectedNode, joinedObjChangeAct));
    }

    @Override
    protected String getDialogCaption() {
        return I18n.kopiujPowiazania.get() /* I18N:  */;
    }
}
