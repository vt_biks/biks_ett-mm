/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

import java.io.Serializable;
import java.util.Date;
import pl.fovis.common.NoteBean;

/**
 *
 * @author tflorczak
 */
public class DQMEvaluationBean implements Serializable {

    public int id;
    public int nodeId;
    public String name;
    public int groupNodeId;
    public String groupName;
    public Date dateFrom;
    public Date dateTo;
    public Date createdDate;
    public int leaderNodeId;
    public String leaderName;
    public String prevQualityLevel;
    public String actualQualityLevel;
    public String significance;
    public String actionPlan;
    public int visualOrder;
    public Integer isAccepted;
    // w przypadku odrzucenia - komentarz
    public NoteBean note;
}
