﻿exec sp_check_version '1.4.y2';
go

----------------------------------------------------
----------------------------------------------------

declare @occurrenceId int;
select @occurrenceId = id from bik_attr_def where name = 'Występowanie w innych instancjach' and is_built_in = 1

declare @webi int = dbo.fn_node_kind_id_by_code('Webi');
declare @universe int = dbo.fn_node_kind_id_by_code('Universe');
declare @conenction int = dbo.fn_node_kind_id_by_code('DataConnection');
declare @reportFolder int = dbo.fn_node_kind_id_by_code('ReportFolder');
declare @universeFolder int = dbo.fn_node_kind_id_by_code('UniversesFolder');
declare @universeUNX int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
declare @connOlap int = dbo.fn_node_kind_id_by_code('CommonConnection');
declare @connRel int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
declare @deski int = dbo.fn_node_kind_id_by_code('FullClient');
declare @crystal int = dbo.fn_node_kind_id_by_code('CrystalReport');
declare @pdf int = dbo.fn_node_kind_id_by_code('Pdf');
declare @flash int = dbo.fn_node_kind_id_by_code('Flash');
declare @excel int = dbo.fn_node_kind_id_by_code('Excel');
declare @hyperlink int = dbo.fn_node_kind_id_by_code('Hyperlink');
declare @office int = dbo.fn_node_kind_id_by_code('Powerpoint');

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@webi,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@universe,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@conenction,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@reportFolder,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@universeFolder,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@universeUNX,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@connOlap,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@connRel,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@deski,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@crystal,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@pdf,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@flash,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@excel,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@hyperlink,@occurrenceId,1)

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@office,@occurrenceId,1)


-- poprzednia wersja w: alter db for v1.4.y1.17 tf.sql
-- dodanie połączeń między rap <-> conn,uni
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_universe_objects]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_joined_objs_universe_objects
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_universe_objects]
as
begin
	declare @repTree int = dbo.fn_get_bo_actual_report_tree_id();
	declare @uniTree int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connTree int = dbo.fn_get_bo_actual_connection_tree_id();
	declare @boServer int = dbo.fn_get_bo_actual_server_id();
	declare @webiKind int = dbo.fn_node_kind_id_by_code('Webi');
    declare @uniKind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcKind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');
    
    -- dodanie polaczen universe <-> connection
    exec sp_prepare_bik_joined_objs_tmp
	
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id, bnu.id, 1 from aaa_universe uni
	inner join aaa_universe_connection con on uni.universe_connetion_id = con.id
	inner join bik_node bn on bn.tree_id = @connTree and bn.name = con.connetion_name 
	inner join bik_node bnu on bnu.tree_id = @uniTree and bnu.obj_id = convert(varchar(30), uni.global_props_si_id)
	where bn.node_kind_id in (@dcKind, @dcNewKind)
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bnu.node_kind_id = @uniKind
	and bnu.is_deleted = 0
	and bnu.linked_node_id is null
	
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bnu.id, bn.id, 1 from aaa_universe uni
	inner join aaa_universe_connection con on uni.universe_connetion_id = con.id
	inner join bik_node bn on bn.tree_id = @connTree and bn.name = con.connetion_name 
	inner join bik_node bnu on bnu.tree_id = @uniTree and bnu.obj_id = convert(varchar(30), uni.global_props_si_id)
	where bn.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bnu.node_kind_id = @uniKind
	and bnu.is_deleted = 0
	and bnu.linked_node_id is null
	
	-- dodanie polaczen universe <-> conn OLAP
	if exists(select 1 from bik_sapbo_server ser 
		inner join bik_data_source_def def on ser.source_id = def.id
		where ser.id = @boServer and def.description = 'Business Objects 4.x') and exists(select 1 from sys.objects where type = 'U' and name = 'APP_DSL__METADATAFILE')
	begin
		declare @connColCode varchar(255) = 'SI_SL_UNIVERSE_TO_CONNECTIONS__'
		declare @num int = 1
		declare @connTableCodeBase varchar(255) = @connColCode
		set @connColCode = @connColCode + cast(@num as varchar(20))
		
		declare @table table (
			col_name varchar(max) not null
		);
		
		while exists(select 1 from sys.columns where name = @connColCode and Object_ID = Object_ID('APP_DSL__METADATAFILE'))
		begin
			insert into @table(col_name)
			values (@connColCode)
			
			set @num = @num + 1  
			set @connColCode = @connTableCodeBase + cast(@num as varchar(20))
		end
		
		declare curs cursor for 
		select col_name from @table order by col_name 

		open curs;
		declare @column varchar(50);

		fetch next from curs into @column;
		while @@fetch_status = 0
		begin
			declare @sql1 varchar(max);
			declare @sql2 varchar(max);
			set @sql1 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnu.id, bnc.id, 1 from APP_DSL__METADATAFILE uni
			inner join bik_node bnu on bnu.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),uni.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),uni.' + @column + ')
			where bnu.is_deleted = 0
			and bnc.is_deleted = 0'
			
			set @sql2 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnc.id, bnu.id, 1 from APP_DSL__METADATAFILE uni
			inner join bik_node bnu on bnu.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),uni.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),uni.' + @column + ')
			where bnu.is_deleted = 0
			and bnc.is_deleted = 0'

			exec(@sql1);
			exec(@sql2);
			fetch next from curs into @column;
		end
		close curs;
		deallocate curs;
	end
	
    exec sp_move_bik_joined_objs_tmp 'Universe,DSL.MetaDataFile', 'DataConnection,CCIS.DataConnection,CommonConnection'
    
    -- dodanie polaczen report <-> uni,conn
    -- tylko dla BO 4.0, gdyż nie działa Report SDK
	if exists(select 1 from bik_sapbo_server ser 
		inner join bik_data_source_def def on ser.source_id = def.id
		where ser.id = @boServer and def.description = 'Business Objects 4.x') and exists(select 1 from sys.objects where type = 'U' and name = 'INFO_WEBI')
	begin
	    
		exec sp_prepare_bik_joined_objs_tmp
	    
		---------------------
		declare @webiColCode varchar(255) = 'SI_UNIVERSE__'
		declare @numWebi int = 1
		declare @webiTableCodeBase varchar(255) = @webiColCode
		set @webiColCode = @webiColCode + cast(@numWebi as varchar(20))
		
		declare @tableWebi table (
			col_name varchar(max) not null
		);
		
		while exists(select 1 from sys.columns where name = @webiColCode and Object_ID = Object_ID('INFO_WEBI'))
		begin
			insert into @tableWebi(col_name)
			values (@webiColCode)
			
			set @numWebi = @numWebi + 1  
			set @webiColCode = @webiTableCodeBase + cast(@numWebi as varchar(20))
		end
		
		declare cursWebi cursor for 
		select col_name from @tableWebi order by col_name 

		open cursWebi;
		declare @columnWebi varchar(50);

		fetch next from cursWebi into @columnWebi;
		while @@fetch_status = 0
		begin
			declare @sqlWebi1 varchar(max);
			declare @sqlWebi2 varchar(max);
			declare @sqlWebi3 varchar(max);
			declare @sqlWebi4 varchar(max);
			
			set @sqlWebi1 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnu.id, bnc.id, 1 from INFO_WEBI web
			inner join bik_node bnu on bnu.tree_id = ' + cast(@repTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),web.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),web.' + @columnWebi + ')
			where bnu.is_deleted = 0 and bnc.is_deleted = 0
			and bnu.node_kind_id = ' + cast(@webiKind as varchar(30)) + ' and bnc.node_kind_id in (' + cast(@uniKind as varchar(30)) + ',' + cast(@uniNewKind as varchar(30)) + ')'
			
			set @sqlWebi2 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnc.id, bnu.id, 1 from INFO_WEBI web
			inner join bik_node bnu on bnu.tree_id = ' + cast(@repTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),web.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),web.' + @columnWebi + ')
			where bnu.is_deleted = 0 and bnc.is_deleted = 0
			and bnu.node_kind_id = ' + cast(@webiKind as varchar(30)) + ' and bnc.node_kind_id in (' + cast(@uniKind as varchar(30)) + ',' + cast(@uniNewKind as varchar(30)) + ')'
			
			set @sqlWebi3 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnj.id, bnu.id, 1 from INFO_WEBI web
			inner join bik_node bnu on bnu.tree_id = ' + cast(@repTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),web.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),web.' + @columnWebi + ')
			inner join bik_joined_objs obj on obj.src_node_id = bnc.id and obj.type = 1
			inner join bik_node bnj on obj.dst_node_id = bnj.id
			where bnu.is_deleted = 0 and bnc.is_deleted = 0 and bnj.is_deleted = 0
			and bnu.node_kind_id = ' + cast(@webiKind as varchar(30)) + ' and bnc.node_kind_id in (' + cast(@uniKind as varchar(30)) + ',' + cast(@uniNewKind as varchar(30)) + ')
			and bnj.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnj.node_kind_id in (' + cast(@dcKind as varchar(30)) + ',' + cast(@dcNewKind as varchar(30)) + ',' + cast(@dcNewOLAPKind as varchar(30)) + ')'

			set @sqlWebi4 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnu.id, bnj.id, 1 from INFO_WEBI web
			inner join bik_node bnu on bnu.tree_id = ' + cast(@repTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),web.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),web.' + @columnWebi + ')
			inner join bik_joined_objs obj on obj.src_node_id = bnc.id and obj.type = 1
			inner join bik_node bnj on obj.dst_node_id = bnj.id
			where bnu.is_deleted = 0 and bnc.is_deleted = 0 and bnj.is_deleted = 0
			and bnu.node_kind_id = ' + cast(@webiKind as varchar(30)) + ' and bnc.node_kind_id in (' + cast(@uniKind as varchar(30)) + ',' + cast(@uniNewKind as varchar(30)) + ')
			and bnj.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnj.node_kind_id in (' + cast(@dcKind as varchar(30)) + ',' + cast(@dcNewKind as varchar(30)) + ',' + cast(@dcNewOLAPKind as varchar(30)) + ')'

			exec(@sqlWebi1);
			exec(@sqlWebi2);
			exec(@sqlWebi3);
			exec(@sqlWebi4);
			
			--print @sqlWebi1
			--print @sqlWebi2
			--print @sqlWebi3
			--print @sqlWebi4
			
			fetch next from cursWebi into @columnWebi;
		end
		close cursWebi;
		deallocate cursWebi;
		---------------------
		
		exec sp_move_bik_joined_objs_tmp 'Webi', 'DataConnection,CCIS.DataConnection,CommonConnection,Universe,DSL.MetaDataFile'
	end
    
	-- dodanie polaczen universe objcets <-> uni,conn
    create table #tmpBranch (branch_id varchar(max) collate DATABASE_DEFAULT, node_id int);

    insert into #tmpBranch(branch_id,node_id)
    select bn.branch_ids, bn2.id from bik_node bn
    inner join bik_joined_objs jo on jo.src_node_id = bn.id
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    where bn.tree_id = @uniTree
    and bn.node_kind_id = @uniKind
    and bn.linked_node_id is null
    and bn.is_deleted = 0
    and bn2.tree_id = @connTree
    and bn2.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
    and bn2.linked_node_id is null
    and bn2.is_deleted = 0

    exec sp_prepare_bik_joined_objs_tmp

	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
	select bn.id, a.node_id, 1 from #tmpBranch a 
	inner join bik_node bn on bn.branch_ids like a.branch_id + '%'
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	where node_kind_id <> @uniKind
		
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
	select bn2.id, bn.id, 1 from bik_node bn 
	inner join bik_node bn2 on bn2.branch_ids like bn.branch_ids + '%'
	and bn2.is_deleted = 0
	and bn2.linked_node_id is null
	and bn2.node_kind_id <> @uniKind
	and bn.tree_id = @uniTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bn.node_kind_id = @uniKind

    exec sp_move_bik_joined_objs_tmp 'Universe,DSL.MetaDataFile,DataConnection,CCIS.DataConnection,CommonConnection', 'UniverseClass,Measure,Dimension,Detail,Filter,UniverseAliasTable,UniverseDerivedTable,UniverseTable,UniverseTablesFolder'
	
	drop table #tmpBranch
end;
go


-- poprzednia wersja w pliku: alter db for v1.4.y1.17 tf.sql
-- pozostawienie w niej tylko łączenia w ramach BO. Teradata ląduje w innej procedurze: [sp_insert_into_bik_joined_objs_teradata_connections]
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_metadata_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
	
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();

    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @universeNodeKind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');

    ----------   zapytanie  -->  universe    ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
    select bn.id as src_node_id, bnu.id as dst_node_id, 1 as type, 1 as inherit_to_descendants
    from bik_sapbo_query sbq
    inner join bik_node bn on sbq.node_id = bn.id
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind,@uniNewKind)
	where bn.tree_id = @reportTreeId
	and bn.is_deleted = 0

    ----------   zapytanie  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id as src_node_id, bnc.id as dst_node_id, 1 as type
    from bik_sapbo_query sbq
    inner join bik_node bn on sbq.node_id = bn.id
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
    and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
    and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	where bn.tree_id = @reportTreeId
	and bn.is_deleted = 0 

    ----------   universe  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnu.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
    and bnu.obj_id = convert(varchar(30),uni.si_id) 
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)

    ----------   raport  -->  universe   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnu.id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)

    ----------   raport  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnc.id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
    and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
    and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)

	----------   connection  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnc.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
    and bnu.obj_id = convert(varchar(30),uni.si_id)
    and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
    and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
    and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)

	----------   obiekt  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct ids.object_node_id, bn.parent_node_id, 1 
    from aaa_ids_for_report ids 
    inner join bik_node bn on ids.query_node_id = bn.id
    and bn.tree_id = @reportTreeId
    and bn.is_deleted = 0
    inner join bik_node bno on bno.id = ids.object_node_id
    and bno.tree_id = @universeTreeId
    and bno.is_deleted = 0
	
	----------   UniverseAliasTable  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bsut1.node_id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_sapbo_universe_table bsut1 on bsut.original_table = bsut1.their_id
	inner join bik_node bn on bn.id = bsut.node_id
	inner join bik_node bn2 on bn2.id = bsut1.node_id
	where bsut.is_alias = 1 and bn2.parent_node_id = bn.parent_node_id
	and bn.tree_id = @universeTreeId
	and bn2.tree_id = @universeTreeId

	-- usuniecie alias table - univ table
	exec sp_delete_bik_joined_objs_by_kinds_fast 'UniverseTable', 'UniverseAliasTable'
	
    exec sp_move_bik_joined_objs_tmp 'Webi,ReportQuery', 'Universe,DSL.MetaDataFile,DataConnection,CommonConnection,CCIS.DataConnection,Measure,Dimension,Detail,Filter'
	
end;
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_teradata_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_teradata_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_teradata_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
	
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();

    declare @teradataKind int = dbo.fn_tree_id_by_code('Teradata');
    declare @schemaKind int = dbo.fn_node_kind_id_by_code('TeradataSchema');
    declare @tabKind int = dbo.fn_node_kind_id_by_code('TeradataTable');
    declare @viewKind int = dbo.fn_node_kind_id_by_code('TeradataView');
    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @universeNodeKind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');
    
    ----------   obiekt  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_node obj on a.object_node_id = obj.id
    and obj.tree_id = @universeTreeId and obj.is_deleted = 0
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)

    ----------   obiekt  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_node obj on a.object_node_id = obj.id
    and obj.tree_id = @universeTreeId and obj.is_deleted = 0
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind

    ----------   universum  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type = 'Teradata' and b.is_derived = 0 
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Table/View  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type = 'Teradata' and b.is_derived = 0 
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   universum  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Schema  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   connection  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
    where b.type = 'Teradata' and b.is_derived = 0 and bn2.id is not null
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Schema  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
    where b.type = 'Teradata' and b.is_derived = 0 and bn2.id is not null
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Table/View  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   connection  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Table/View  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   raport  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   teradata Schema  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   raport  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   zapytanie  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   zapytanie  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId    
    
    ----------   UniverseTable  -->  teradata Table/View   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.name_for_teradata = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'
	inner join bik_node tab on tab.id = bsut.node_id
	and tab.tree_id = @universeTreeId
	and tab.is_deleted = 0

	----------   teradata Table/View  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bsut.node_id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.name_for_teradata = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'
	inner join bik_node tab on tab.id = bsut.node_id
	and tab.tree_id = @universeTreeId
	and tab.is_deleted = 0

	----------   UniverseTable  -->  teradata Schema   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.schema_name = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'
	inner join bik_node tab on tab.id = bsut.node_id
	and tab.tree_id = @universeTreeId
	and tab.is_deleted = 0

    exec sp_move_bik_joined_objs_tmp 'TeradataView,TeradataTable,TeradataSchema', 'Universe,DSL.MetaDataFile,DataConnection,CommonConnection,CCIS.DataConnection,Measure,Dimension,Detail,Filter,Webi,ReportQuery,UniverseAliasTable,UniverseDerivedTable,UniverseTable'
	
end;
go


----------------------------------------------------
----------------------------------------------------

exec sp_update_version '1.4.y2', '1.4.y2.1';
go
