﻿exec sp_check_version '1.0.87.7';
go
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
declare @uniqueName varchar(100);

select @uniqueName = 
so.name
from sys.objects so join sys.columns sc on so.parent_object_id = sc.object_id 
	join sys.tables st on sc.object_id = st.object_id
where so.type='UQ' and --sc.name = 'parent_id' and 
st.name = 'bik_role_in_node_kind'

if(not @uniqueName is null)
	exec ('alter table bik_role_in_node_kind drop constraint ' + @uniqueName)
go
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

alter table bik_role_in_node_kind
	add constraint UQ_bik_role_in_node_kind_role_id_node_kind_id unique(node_kind_id, role_id);
go


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
----------------------------- UPDACIKI OD TOMASZA------------------------------

update bik_node_kind set caption='Definicja robocza' where code = 'GlossaryNotCorpo'
update bik_node_kind set caption='Definicja korporacyjna' where code = 'Glossary'

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

EXEC sp_rename 'aaa_universe_filtr', 'aaa_universe_filter'
go

--------------------------------------------------------------
--------------------------------------------------------------

EXEC sp_rename 'aaa_universe_connetion', 'aaa_universe_connection'
go
--------------------------------------------------------------
--------------------------------------------------------------

EXEC sp_rename 'aaa_universe_connetion_networklayer', 'aaa_universe_connection_networklayer'
go
--------------------------------------------------------------
--------------------------------------------------------------


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_objects_from_Designer]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]
go

create procedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]
as
	declare @tree_id int;
	declare @kind_id int;
	declare @dimension_nki int, @measure_nki int, @detail_nki int, @filtr_nki int;
begin--begin procedure

	----wybranie id odpowiedniego drzewa
	select @tree_id = id
	from bik_tree
	where code = 'ObjectUniverses';	
	
	----wybranie id odpowiedniego kind'a
	select @kind_id = id
	from bik_node_kind
	where code = 'UniverseClass';
		
	create table #tmpNodes (si_id varchar(1000) , si_parentid varchar(1000), si_kind_id int, si_name varchar(max), si_description varchar(max));

	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Class
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), 
		case
			when uc.parent_id is null then convert(varchar, u.global_props_si_id)
			else convert(varchar, uc.parent_id) + '|' + convert(varchar, u.global_props_si_id) end as parent_id, 
		@kind_id, uc.name, uc.description
	from aaa_universe_class uc join aaa_universe u on u.id = uc.universe_id 
	
	----wybranie id odpowiedniego kind'a
	--detail
	select @detail_nki = id
	from bik_node_kind
	where code = 'Detail';
	--demension
	select @dimension_nki = id
	from bik_node_kind
	where code = 'Dimension';
	--measure
	select @measure_nki = id
	from bik_node_kind
	where code = 'Measure';
	--filtr
	select @filtr_nki = id
	from bik_node_kind
	where code = 'Filter';
	
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Obj
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), 
			case
				when uo.parent_id is null then convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id)
				else convert(varchar, uo.parent_id)+ '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id)
			end as parent_id, 
			case
				when uo.qualification = 'dsDetailObject' then @detail_nki
				when uo.qualification = 'dsMeasureObject' then @measure_nki
				when uo.qualification = 'dsDimensionObject' then @dimension_nki
				end as node_kind_id,
		 uo.name, uo.description
	from aaa_universe_obj uo join aaa_universe_class uc on uo.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id 
							 
	--wrzucam do tabeli tymczasowej wszystkie dane z Universe_filrt
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uf.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id),
		convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), @filtr_nki, uf.name, nullif(ltrim(rtrim(uf.description)), '') 
	from aaa_universe_filter uf join aaa_universe_class uc on uf.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id 
							 

	--aktualizacja istniejących węzłów w drzewie							
	update bik_node 
	set parent_node_id = null, node_kind_id = #tmpNodes.si_kind_id, name = #tmpNodes.si_name,
		is_deleted = 0, descr = #tmpNodes.si_description, tree_id = @tree_id
	from #tmpNodes 
	where bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id and bik_node.linked_node_id is null;
	
	--dorzucanie nowych węzłów w drzewie
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select #tmpNodes.si_kind_id, #tmpNodes.si_name, @tree_id, #tmpNodes.si_id, #tmpNodes.si_description
	from #tmpNodes left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
		and bik_node.tree_id = @tree_id
	where bik_node.id is null;	
	
	--uaktualnianie parentów w nodach
	declare @universe_kind_id int;
	select @universe_kind_id = id
	from bik_node_kind
	where code = 'Universe';
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id and bik_node.node_kind_id = pt.si_kind_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id 
		and  (
		 (pt.si_kind_id=@detail_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@dimension_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@measure_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@filtr_nki and bk.node_kind_id=@kind_id) or
		 (pt.si_kind_id=@kind_id and (bk.node_kind_id = @kind_id or bk.node_kind_id = @universe_kind_id)))
	where bik_node.linked_node_id is null;
		
	--usuwanie nodów
	update bik_node
	set is_deleted = 1
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and bik_node.node_kind_id in (@kind_id, @detail_nki, @dimension_nki, @measure_nki,
	  @filtr_nki);
	
	drop table #tmpNodes;
	--uzupełnianie danych w tabelach extra 
	--dla object
	create table #tmpObjExtra(text_of_select varchar(max),text_of_where varchar(max), type varchar(155), aggregate_function varchar(50), node_id int)
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select univ_obj.text_of_select, univ_obj.text_of_where, univ_obj.type, univ_obj.aggregate_function, bik_node.id
	from (
	select convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id) as obj_id, 
					uo.text_of_select, uo.text_of_where, uo.type, uo.aggregate_function
			from aaa_universe_obj uo join aaa_universe_class uc on uo.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id ) univ_obj 
			join bik_node on bik_node.obj_id = univ_obj.obj_id and bik_node.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)-- = @obj_node_kind_id;
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select null, univ_filtr.where_clause, univ_filtr.type, null, bik_node.id
	from (
		select convert(varchar, uf.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id) as obj_id,
			uf.where_clause, uf.type
		from aaa_universe_filter uf join aaa_universe_class uc on uf.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id ) univ_filtr
				 join bik_node on bik_node.obj_id = univ_filtr.obj_id and bik_node.node_kind_id = @filtr_nki
			
	update bik_sapbo_universe_object
	set text_of_select = #tmpObjExtra.text_of_select, text_of_where = #tmpObjExtra.text_of_where, type = #tmpObjExtra.type,  
		           aggregate_function = #tmpObjExtra.aggregate_function 
	from #tmpObjExtra join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id;
	
	insert into bik_sapbo_universe_object(text_of_select, text_of_where, type, aggregate_function, node_id)
	select #tmpObjExtra.text_of_select,#tmpObjExtra.text_of_where, #tmpObjExtra.type, #tmpObjExtra.aggregate_function, #tmpObjExtra.node_id 
	from #tmpObjExtra left join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id
	where bik_sapbo_universe_object.id is null;

	drop table #tmpObjExtra;
	
	--dla tabeli
	
	update bik_sapbo_universe_table
	set name = ut.name, is_alias = ut.is_alias, is_derived = ut.is_derived, original_table = ut.original_table, sql_of_derived_table = ut.sql_of_derived_table, 
		sql_of_derived_table_with_alias = ut.sql_of_derived_table_with_alias, their_id = ut.their_id, universe_node_id = bik_node.id, type = ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where linked_node_id is null and bik_node.node_kind_id = @universe_kind_id
	
	
	insert into bik_sapbo_universe_table(name, is_alias, is_derived, original_table, sql_of_derived_table, sql_of_derived_table_with_alias, their_id, universe_node_id, type)
	select ut.name, ut.is_alias, ut.is_derived, ut.original_table, ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, ut.their_id, 
					bik_node.id as universe_node_id, ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where  bik_node.linked_node_id is null and bsut.name is null and bik_node.node_kind_id = @universe_kind_id

	--usupełnianie name_for_teradata
	update bik_sapbo_universe_table
	set name_for_teradata = replace(name,'.', '|') + '|'
	where type = 'Teradata' and is_derived = 0 and is_alias = 0

	update bik_sapbo_universe_table
	set name_for_teradata = bsut.name_for_teradata
	from bik_sapbo_universe_table join bik_sapbo_universe_table bsut on bik_sapbo_universe_table.original_table = bsut.their_id and 
		bik_sapbo_universe_table.universe_node_id = bsut.universe_node_id
	where bik_sapbo_universe_table.type = 'Teradata' and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 1
	
	update bik_sapbo_universe_table
	set schema_name = left(name_for_teradata, charindex('|',name_for_teradata))
	where  type = 'Teradata' and is_derived = 0
	
	--dla kolumn
	update bik_sapbo_universe_column
	set name = uc.name, type = uc.type, table_id = bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and bik_node.node_kind_id = @universe_kind_id 
	
	insert into bik_sapbo_universe_column(name, type, table_id)
	select uc.name, uc.type, bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and bsuc.id is null and bik_node.node_kind_id = @universe_kind_id 
	
	delete 
	from bik_sapbo_universe_table
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and uc.id is null and bik_node.node_kind_id = @universe_kind_id
	
	delete 
	from bik_sapbo_universe_table
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where  bik_node.linked_node_id is null and ut.name is null and bik_node.node_kind_id = @universe_kind_id
	
	update bik_sapbo_universe_connection 
	set bik_sapbo_universe_connection.server = aaa_universe_connection.server, bik_sapbo_universe_connection.user_name = aaa_universe_connection.user_name,
		bik_sapbo_universe_connection.password = aaa_universe_connection.password, bik_sapbo_universe_connection.database_source = aaa_universe_connection.database_source, 
		bik_sapbo_universe_connection.connetion_networklayer_name = aaa_universe_connection_networklayer.name, bik_sapbo_universe_connection.node_id = bik_node.id, 
		bik_sapbo_universe_connection.type = aaa_universe_connection.type, bik_sapbo_universe_connection.database_enigme = aaa_universe_connection.database_enigme,
		bik_sapbo_universe_connection.client_number = aaa_universe_connection.client_number, bik_sapbo_universe_connection.language = aaa_universe_connection.language, 
		bik_sapbo_universe_connection.system_number = aaa_universe_connection.system_number, bik_sapbo_universe_connection.system_id = aaa_universe_connection.system_id
	from aaa_universe_connection join aaa_universe_connection_networklayer on aaa_universe_connection.connetion_networklayer_id = aaa_universe_connection_networklayer.id
		join bik_node on aaa_universe_connection.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		join bik_sapbo_universe_connection on  bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections'
	
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id, type, database_enigme, client_number, language, system_number, system_id)
	select aaa_universe_connection.server, aaa_universe_connection.user_name, aaa_universe_connection.password, aaa_universe_connection.database_source, 
		aaa_universe_connection_networklayer.name, bik_node.id,
		aaa_universe_connection.type, aaa_universe_connection.database_enigme, aaa_universe_connection.client_number, aaa_universe_connection.language,
		aaa_universe_connection.system_number, aaa_universe_connection.system_id
	from aaa_universe_connection join aaa_universe_connection_networklayer on aaa_universe_connection.connetion_networklayer_id = aaa_universe_connection_networklayer.id
		join bik_node on aaa_universe_connection.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		left join bik_sapbo_universe_connection on  bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections' and bik_sapbo_universe_connection.id is null;
		
	update bik_sapbo_extradata
	set statistic = u.statistic
	from bik_sapbo_extradata join bik_node bn on bn.id = bik_sapbo_extradata.node_id
		join bik_node_kind bnk on bn.node_kind_id = bnk.id
		join aaa_universe u on convert(varchar,u.global_props_si_id) = bn.obj_id
	where bnk.code = 'Universe'	
				
	exec sp_delete_linked_nodes_where_orignal_is_deleted;	
	exec sp_node_init_branch_id @tree_id, null;
end--end procedure

GO
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_add_converted_varchar_branch_after_insert_to_bik_node]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_add_converted_varchar_branch_after_insert_to_bik_node]
GO

create procedure [dbo].[sp_add_converted_varchar_branch_after_insert_to_bik_node]
as
begin
update aaa_universe_obj set obj_branch=convert(varchar(30),uo.si_id) + '|' +  convert(varchar(30),uc.si_id) + '|' + convert(varchar(30),u.global_props_si_id)
from aaa_universe_obj uo 
join aaa_universe_class uc on uo.universe_class_id=uc.id
join aaa_universe u on uc.universe_id=u.id

update aaa_universe_filter set filtr_branch=convert(varchar(30), uf.si_id) + '|' + convert(varchar(30), uc2.si_id) + '|' + convert(varchar(30), u3.global_props_si_id)
from aaa_universe_filter uf
left join aaa_universe_class uc2 on uf.universe_class_id = uc2.id
left join aaa_universe u3 on u3.id = uc2.universe_id

update aaa_universe_table set universe_branch=convert(varchar(30),u2.global_props_si_id)
from aaa_universe_table ut
join aaa_universe u2 on ut.universe_id=u2.id
end

GO
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_object_table]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_sapbo_object_table]
GO
create procedure [dbo].[sp_insert_into_bik_node_sapbo_object_table]
as
begin

declare @tree_id int;
select @tree_id = id from bik_tree where code='ObjectUniverses'
declare @universe_kind int;
select @universe_kind = id from bik_node_kind where code='Universe'
declare @filtr_kind int;
select @filtr_kind = id from bik_node_kind where code='Filter'
declare @detail_nki int;
select @detail_nki = id	from bik_node_kind where code = 'Detail';
declare @dimension_nki int;
select @dimension_nki = id from bik_node_kind where code = 'Dimension';
declare @measure_nki int;
select @measure_nki = id from bik_node_kind	where code = 'Measure';

insert into bik_sapbo_object_table
select *
from (
select case when uot.obj_type=1 then bn.id else bn3.id end as obj_node_ide, sut.id from aaa_universe_obj_tables uot
left join aaa_universe_obj uo on uot.universe_obj_id=uo.id
left join bik_node bn on bn.obj_id = uo.obj_branch
and bn.is_deleted=0
and bn.linked_node_id is null
and bn.tree_id=@tree_id
and bn.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)
join aaa_universe_table ut on uot.universe_table_id=ut.id
join bik_node bn2 on bn2.obj_id = ut.universe_branch
and bn2.is_deleted=0
and bn2.linked_node_id is null
and bn2.node_kind_id=@universe_kind
join bik_sapbo_universe_table sut on ut.name=sut.name
and sut.universe_node_id=bn2.id
left join aaa_universe_filter uf on uot.universe_obj_id=uf.id
left join bik_node bn3 on bn3.obj_id = uf.filtr_branch
and bn3.is_deleted=0
and bn3.linked_node_id is null
and bn3.tree_id=@tree_id
and bn3.node_kind_id=@filtr_kind
left join bik_sapbo_object_table bsot on bsot.object_node_id=case when uot.obj_type=1 then bn.id else bn3.id end and bsot.table_id=sut.id
where bsot.id is null
) x
where obj_node_ide is not null
end

GO
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
exec sp_update_version '1.0.87.7', '1.0.87.8';
go
-------------------------------------------------------------------------------
