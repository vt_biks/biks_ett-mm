/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

/**
 *
 * @author lbiegniewski
 */
public interface LisaDictionaryValue {

    public Object getValue();

    public void setValue(Object o);
}
