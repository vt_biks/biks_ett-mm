/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import simplelib.BeanWithIntIdBase;

/**
 *
 * @author tflorczak
 */
public class NodeKind4TreeKind extends BeanWithIntIdBase {

    public int treeKindId;
    public Integer dstNodeKindId;
    public Integer srcNodeKindId;
    public String relationType;
    public boolean isBranch;
    public boolean isLeaf;
//

    public NodeKind4TreeKind() {
    }

}
