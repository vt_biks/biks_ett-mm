﻿exec sp_check_version '1.8.4.11';
go

alter table bik_tree_kind alter column branch_node_kind_id int NULL
go

alter table bik_tree_kind alter column leaf_node_kind_id int NULL
go

exec sp_update_version '1.8.4.11', '1.8.4.12';
go