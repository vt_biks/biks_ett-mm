exec sp_check_version '1.0.4';
go


----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_check_version]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_check_version]
GO

create procedure [dbo].[sp_check_version] (@ver varchar(255))
as
	declare @version varchar(255)
begin	
	select @version = val from bik_app_prop;
	if(@version != @ver)
	begin
		raiserror(N'!!! Wrong version number %s. Current version is %s. !!!',--Message text
				20,
				-1,
				@ver, @version) with log;
				print N'Wrong number of version.Current version is %s';
	end
	
end
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_update_version]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_update_version]
GO

create procedure [dbo].[sp_update_version](@ver_current varchar(255), @ver_upper varchar(255))
as
	declare @version varchar(255)
begin
	exec sp_check_version @ver_current;
	
	update bik_app_prop set val=@ver_upper where name='bik_ver';	
end
GO


alter table bik_biz_def add node_id int references bik_node (id) not null;
GO


insert into bik_biz_def(name,node_id) select name,id from bik_node where tree_id=8;
GO

insert into  bik_biz_def_body (biz_def_id,body,def_status,user_id ,is_corpo) 
select a.id,b.body,0,1,0 from bik_biz_def a,aaa_mpart b,bik_node c where b.tree_node_id=c.obj_id and c.id=a.node_id and tree_id=8;

GO



----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

alter table aaa_teradata_object add branch_names varchar(8000)
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_teradata_init_branch_names]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_teradata_init_branch_names]
GO

create procedure sp_teradata_init_branch_names 
as
begin  
  set nocount on

  create table #ids (id int not null primary key);
  
  insert into #ids (id) 
  select ID from aaa_teradata_object where parent_id is null;
    
  create table #child_ids (id int not null primary key);
    
  while exists (select 1 from #ids)
  begin
    update aaa_teradata_object
    set branch_names = coalesce((select parent.branch_names from aaa_teradata_object as parent 
      where parent.id = aaa_teradata_object.parent_id), '')
    + name + '|'
    where aaa_teradata_object.id in (select id from #ids);
    
    truncate table #child_ids;
    
    insert into #child_ids (id)
    select ID from aaa_teradata_object where parent_id in (select id from #ids);
    
    truncate table #ids;
    
    insert into #ids (id) select ID from #child_ids;
  end
    
  drop table #ids;
  drop table #child_ids;
end
go

exec sp_teradata_init_branch_names
go

----------------------------------------------------------------------
----------------------------------------------------------------------

create table bik_teradata_extradata (
  node_id int not null primary key references bik_node (id),
  branch_names varchar(8000) not null unique
);
go

----------------------------------------------------------------------
update bik_tree
set tree_kind = 2
where name = 'Tezaurus'
go
----------------------------------------------------------------------
update bik_tree
set node_kind_id = (select id from bik_node_kind where code = 'Tezaurus')
where name = 'Tezaurus'
go

----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------


alter table bik_node add obj_id_str varchar(1000) null;
go

update bik_node set obj_id_str = CAST(obj_id as varchar(1000));
go

-- select * from bik_node

alter table bik_node drop column obj_id;
go

alter table bik_node add obj_id varchar(1000) null;
go

update bik_node set obj_id = obj_id_str;
go

alter table bik_node drop column obj_id_str;
go

----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

-- po zako�czeniu prac nad wersj� v1.0.5 odkomentowa� poni�sze

exec sp_update_version '1.0.4', '1.0.5';
go
