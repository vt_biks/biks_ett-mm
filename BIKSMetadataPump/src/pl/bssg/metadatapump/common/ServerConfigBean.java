/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

/**
 *
 * @author tflorczak
 */
public class ServerConfigBean {

    public int id;
    public String name;
    public String server;
    public Integer port;
    public String instance;
    public String user;
    public String password;
    public Boolean isActive;
    public String databaseFilter; // opt
    public String objectFilter; // opt
}
