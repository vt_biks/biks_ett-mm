/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib.logging;

import simplelib.logging.ILameLogger.LameLogLevel;

/**
 *
 * @author wezyr
 */
public class StdOutLameLoggerAppender extends LameLoggerAppenderFormattedMsgBase {

    public static final LameLogLevel stdOutMinLogLevel = LameLogLevel.Debug;

    private static boolean appenderAdded = false;

    public static synchronized void addAppender() {
        if (!appenderAdded) {
            LameLoggerFactory.addAppender(new StdOutLameLoggerAppender(stdOutMinLogLevel));
            appenderAdded = true;
        }
    }

    public StdOutLameLoggerAppender(LameLogLevel minLogLevel) {
        super(minLogLevel);
    }

    @Override
    protected void sendFormatedMsgToOutput(String formatedMsg) {
        System.out.println(formatedMsg);
    }
}
