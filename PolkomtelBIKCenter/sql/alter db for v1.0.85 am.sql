﻿exec sp_check_version '1.0.85';
go
--------------------------------------------------------------
--------------------------------------------------------------
create table aaa_universe_filtr(
	id int identity(1,1) not null primary key,
	si_id int not null,
	name varchar(255) not null,
	description varchar(max),
	where_clause varchar(max),
	type varchar(255) not null,
	universe_class_id int references aaa_universe_class(id)
)
go
--------------------------------------------------------------
--------------------------------------------------------------
alter table aaa_universe_filtr
	add constraint UNIQ_unique_si_id_universe_class_id unique(si_id, universe_class_id)
go
--------------------------------------------------------------
--------------------------------------------------------------
insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values('Filter', 'Filtr', 'filtr', 'Metadata', 0)
go
--------------------------------------------------------------
--------------------------------------------------------------
alter table bik_sapbo_universe_table
	add type varchar(255);
go
--------------------------------------------------------------
--------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_objects_from_Designer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_objects_from_Designer]
GO

create procedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]
as
	declare @tree_id int;
	declare @kind_id int;
	declare @dimension_nki int, @measure_nki int, @detail_nki int, @filtr_nki int;
begin--begin procedure

	----wybranie id odpowiedniego drzewa
	select @tree_id = id
	from bik_tree
	where code = 'ObjectUniverses';	
	
	----wybranie id odpowiedniego kind'a
	select @kind_id = id
	from bik_node_kind
	where code = 'UniverseClass';
		
	create table #tmpNodes (si_id varchar(1000) , si_parentid varchar(1000), si_kind_id int, si_name varchar(max), si_description varchar(max));

	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Class
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), 
		case
			when uc.parent_id is null then convert(varchar, u.global_props_si_id)
			else convert(varchar, uc.parent_id) + '|' + convert(varchar, u.global_props_si_id) end as parent_id, 
		@kind_id, uc.name, uc.description
	from aaa_universe_class uc join aaa_universe u on u.id = uc.universe_id 
	
	----wybranie id odpowiedniego kind'a
	--detail
	select @detail_nki = id
	from bik_node_kind
	where code = 'Detail';
	--demension
	select @dimension_nki = id
	from bik_node_kind
	where code = 'Dimension';
	--measure
	select @measure_nki = id
	from bik_node_kind
	where code = 'Measure';
	--filtr
	select @filtr_nki = id
	from bik_node_kind
	where code = 'Filter';
	
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Obj
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), 
			case
				when uo.parent_id is null then convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id)
				else convert(varchar, uo.parent_id)+ '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id)
			end as parent_id, 
			case
				when uo.qualification = 'dsDetailObject' then @detail_nki
				when uo.qualification = 'dsMeasureObject' then @measure_nki
				when uo.qualification = 'dsDimensionObject' then @dimension_nki
				end as node_kind_id,
		 uo.name, uo.description
	from aaa_universe_obj uo join aaa_universe_class uc on uo.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id 
							 
	--wrzucam do tabeli tymczasowej wszystkie dane z Universe_filrt
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uf.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id),
		convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), @filtr_nki, uf.name, uf.description
	from aaa_universe_filtr uf join aaa_universe_class uc on uf.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id 
							 

	--aktualizacja istniejących węzłów w drzewie							
	update bik_node 
	set parent_node_id = null, node_kind_id = #tmpNodes.si_kind_id, name = #tmpNodes.si_name,
		is_deleted = 0, descr = #tmpNodes.si_description, tree_id = @tree_id
	from #tmpNodes 
	where bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id and bik_node.linked_node_id is null;
	
	--dorzucanie nowych węzłów w drzewie
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select #tmpNodes.si_kind_id, #tmpNodes.si_name, @tree_id, #tmpNodes.si_id, #tmpNodes.si_description
	from #tmpNodes left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
		and bik_node.tree_id = @tree_id
	where bik_node.id is null;	
	
	--uaktualnianie parentów w nodach
	declare @universe_kind_id int;
	select @universe_kind_id = id
	from bik_node_kind
	where code = 'Universe';
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id and bik_node.node_kind_id = pt.si_kind_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id 
		and  (
		 (pt.si_kind_id=@detail_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@dimension_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@measure_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@filtr_nki and bk.node_kind_id=@kind_id) or
		 (pt.si_kind_id=@kind_id and (bk.node_kind_id = @kind_id or bk.node_kind_id = @universe_kind_id)))
	where bik_node.linked_node_id is null;
		
	--usuwanie nodów
	update bik_node
	set is_deleted = 1
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and bik_node.node_kind_id in (@kind_id, @detail_nki, @dimension_nki, @measure_nki,
	  @filtr_nki);
	
	drop table #tmpNodes;
	--uzupełnianie danych w tabelach extra 
	--dla object
	create table #tmpObjExtra(text_of_select varchar(max),text_of_where varchar(max), type varchar(155), aggregate_function varchar(50), node_id int)
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select univ_obj.text_of_select, univ_obj.text_of_where, univ_obj.type, univ_obj.aggregate_function, bik_node.id
	from (
	select convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id) as obj_id, 
					uo.text_of_select, uo.text_of_where, uo.type, uo.aggregate_function
			from aaa_universe_obj uo join aaa_universe_class uc on uo.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id ) univ_obj 
			join bik_node on bik_node.obj_id = univ_obj.obj_id and bik_node.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)-- = @obj_node_kind_id;
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select null, univ_filtr.where_clause, univ_filtr.type, null, bik_node.id
	from (
		select convert(varchar, uf.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id) as obj_id,
			uf.where_clause, uf.type
		from aaa_universe_filtr uf join aaa_universe_class uc on uf.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id ) univ_filtr
				 join bik_node on bik_node.obj_id = univ_filtr.obj_id and bik_node.node_kind_id = @filtr_nki
			
	update bik_sapbo_universe_object
	set text_of_select = #tmpObjExtra.text_of_select, text_of_where = #tmpObjExtra.text_of_where, type = #tmpObjExtra.type,  
		           aggregate_function = #tmpObjExtra.aggregate_function 
	from #tmpObjExtra join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id;
	
	insert into bik_sapbo_universe_object(text_of_select, text_of_where, type, aggregate_function, node_id)
	select #tmpObjExtra.text_of_select,#tmpObjExtra.text_of_where, #tmpObjExtra.type, #tmpObjExtra.aggregate_function, #tmpObjExtra.node_id 
	from #tmpObjExtra left join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id
	where bik_sapbo_universe_object.id is null;

	drop table #tmpObjExtra;
	
	--dla tabeli
	
	update bik_sapbo_universe_table
	set name = ut.name, is_alias = ut.is_alias, is_derived = ut.is_derived, original_table = ut.original_table, sql_of_derived_table = ut.sql_of_derived_table, 
		sql_of_derived_table_with_alias = ut.sql_of_derived_table_with_alias, their_id = ut.their_id, universe_node_id = bik_node.id, type = ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connetion uc on u.universe_connetion_id = uc.id
		join aaa_universe_connetion_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where linked_node_id is null and bik_node.node_kind_id = @universe_kind_id
	
	
	insert into bik_sapbo_universe_table(name, is_alias, is_derived, original_table, sql_of_derived_table, sql_of_derived_table_with_alias, their_id, universe_node_id, type)
	select ut.name, ut.is_alias, ut.is_derived, ut.original_table, ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, ut.their_id, 
					bik_node.id as universe_node_id, ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connetion uc on u.universe_connetion_id = uc.id
		join aaa_universe_connetion_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where  bik_node.linked_node_id is null and bsut.name is null and bik_node.node_kind_id = @universe_kind_id

	delete 
	from bik_sapbo_universe_table
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where  bik_node.linked_node_id is null and ut.name is null and bik_node.node_kind_id = @universe_kind_id
	
	--dla kolumn
	update bik_sapbo_universe_column
	set name = uc.name, type = uc.type, table_id = bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and bik_node.node_kind_id = @universe_kind_id 
	
	insert into bik_sapbo_universe_column(name, type, table_id)
	select uc.name, uc.type, bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and bsuc.id is null and bik_node.node_kind_id = @universe_kind_id 
	
	delete 
	from bik_sapbo_universe_table
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and uc.id is null and bik_node.node_kind_id = @universe_kind_id
	
	update bik_sapbo_universe_connection 
	set bik_sapbo_universe_connection.server = aaa_universe_connetion.server, bik_sapbo_universe_connection.user_name = aaa_universe_connetion.user_name,
		bik_sapbo_universe_connection.password = aaa_universe_connetion.password, bik_sapbo_universe_connection.database_source = aaa_universe_connetion.database_source, 
		bik_sapbo_universe_connection.connetion_networklayer_name = aaa_universe_connetion_networklayer.name, bik_sapbo_universe_connection.node_id = bik_node.id
	from aaa_universe_connetion join aaa_universe_connetion_networklayer on aaa_universe_connetion.connetion_networklayer_id = aaa_universe_connetion_networklayer.id
		join bik_node on aaa_universe_connetion.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		join bik_sapbo_universe_connection on  bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections'
	
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id)
	select aaa_universe_connetion.server, aaa_universe_connetion.user_name, aaa_universe_connetion.password, aaa_universe_connetion.database_source, 
		aaa_universe_connetion_networklayer.name, bik_node.id
	from aaa_universe_connetion join aaa_universe_connetion_networklayer on aaa_universe_connetion.connetion_networklayer_id = aaa_universe_connetion_networklayer.id
		join bik_node on aaa_universe_connetion.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		left join bik_sapbo_universe_connection on  bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections' and bik_sapbo_universe_connection.id is null;
				
	exec sp_delete_linked_nodes_where_orignal_is_deleted;
end--end procedure
GO
--------------------------------------------------------------
--------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node]
go
create procedure [dbo].[sp_insert_into_bik_node](@tree_code varchar(255))
as
	declare @table_folders varchar(255);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	--print cast(sysdatetime() as varchar(22)) + ': faza 1'
	
	select @tree_id = id 
	from bik_tree
	where code=@tree_code;

	create table #primaryIds(si_id varchar(1000) primary key);
	create table #secondaryIds(si_id varchar(1000) primary key);

	if(@tree_code!='Reports' and @tree_code!='Teradata')
	begin
		set @table_folders = ' APP_FOLDER where 1=1 ';
		
		if(@tree_code = 'Connections')
		begin
			set @kinds = '''MetaData.DataConnection''';
			exec('insert into #primaryIds
					select convert(varchar,SI_ID) as SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');
			exec('insert into #secondaryIds 
					select convert(varchar,SI_ID) as SI_ID
					from APP_UNIVERSE
					where SI_DATACONNECTION__1 in (select * from #primaryIds)');			
		end;
		if(@tree_code = 'ObjectUniverses')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select convert(varchar,SI_ID) as SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_code='Reports')
	begin
		-- wybranie folderów raportów bez raportów użytkowników
		set @table_folders = ' INFO_FOLDER where si_name != ''~Webintelligence''';
		--set @table_folders = ' INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 is null or SI_PATH__SI_FOLDER_NAME2 != ''User Folders''';
		set @kinds = '''Webi'', ''Flash'', ''CrystalReport'',''Excel'',''FullClient'',''Pdf'',''Hyperlink''
		,''Rtf'',''Txt'',''Powerpoint'',''Word''';
	end;
	
    declare @sql nvarchar(2000);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		--print cast(sysdatetime() as varchar(22)) + ': faza 4.1 - teradata przed sp_teradata_init_branch_names'
		exec sp_teradata_init_branch_names;
		--print cast(sysdatetime() as varchar(22)) + ': faza 4.2 - teradata po sp_teradata_init_branch_names'
		set @sql = N'select branch_names as SI_ID, 
											case 
												when SUBSTRING(branch_names, 1,len(branch_names) - len(name + ''|'')) =''''
													then null
												else SUBSTRING(branch_names, 1,len(branch_names) - len(name + ''|'')) end
												as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata
				/*where parent_id is not null or name like ''VD_US_%''*/';
	end;
	else
	begin
		if(@tree_code = 'Reports')
		-- sztuczny podzial na root folder i user folders dla raportów
		set @sql = N'select distinct SI_PARENT_FOLDER as SI_ID, (select si_id from INFO_FOLDER where SI_NAME=''User Folders'') as SI_PARENTID, SI_KIND, SI_PATH__SI_FOLDER_NAME1 as SI_NAME, NULL as DESCR
				 from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' 
				 and SI_NAME!=''~Webintelligence''

				 union all
				
				 select si_id, case
									when SI_PARENTID=0 and si_name!=''User Folders''
										then (select si_id from INFO_FOLDER where SI_NAME=''Root Folder'')
									when si_id in(select si_id from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 = ''User Folders'')
                                        then SI_PARENTID
									when SI_PARENTID in(select si_id from INFO_FOLDER)
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case
																			when SI_NAME=''Root Folder''
																				then ''Foldery korporacyjne''
																			when SI_NAME=''User Folders''
																				then ''Foldery użytkowników''
																			else SI_NAME end, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 								
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') and SI_INSTANCE = 0
				union all
				
				select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR
				from aaa_global_props where si_parentid in (select si_parentid from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' and SI_NAME!=''~Webintelligence'') and si_kind!=''Folder''
				';
		if(@tree_code = 'Connections')
			set @sql = N'select si_id, case
									when SI_NAME=''Connections''
										then 0
									when SI_PARENTID in(select si_id from ' + @table_folders +') 
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 
													
				' and SI_NAME!=''Dynamic Cascading Prompts''
				
				 union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') 
						
				union all		  

								select SI_ID, SI_DATACONNECTION__1 as SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
								from APP_UNIVERSE
								where SI_DATACONNECTION__1 in (select * from #primaryIds) and
									SI_KIND in('+ @kinds +')
	
								union all
								
								select SI_ID, SI_UNIVERSE__1 as SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
								from INFO_WEBI
								where SI_UNIVERSE__1 in (select * from #secondaryIds)
								and SI_KIND in('+ @kinds +')';

		if(@tree_code = 'ObjectUniverses')
			set @sql = N'select si_id, case 
									when SI_NAME=''Universes''
										then 0
									when SI_PARENTID in(select si_id from ' + @table_folders +') 
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 
													
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') 
				
				union all
			
								select SI_ID, SI_UNIVERSE__1 as SI_PARENTID, SI_KIND,  SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
								from INFO_WEBI
								where SI_UNIVERSE__1 in (select * from #primaryIds) and SI_KIND in ('+ @kinds +')';
	end;					

	--print cast(sysdatetime() as varchar(22)) + ': faza 6 - przed zapytaniem'
	print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(1000) primary key, si_parentid varchar(1000), si_kind varchar(max), si_name varchar(max), descr varchar(max));

	set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
				select si_id, si_parentid, dbo.fn_node_kind_code_for_metadata_type(si_kind), si_name, DESCR
			    from (' + @sql + ') xxx';
			   
	EXECUTE(@sql);

	--print cast(sysdatetime() as varchar(22)) + ': faza 6.5 - po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/

	declare @rc int = 1

	while @rc > 0 begin
	
	delete from #tmpNodes
	where si_kind = 'MetadataFolder' and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/	

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = #tmpNodes.si_name,
		is_deleted = 0, descr = #tmpNodes.descr, tree_id = @tree_id
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id /*and tree_id = @tree_id*/ and bik_node.linked_node_id is null
	
	--update podlinkowanych
	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = #tmpNodes.si_name,
		 descr = #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id /*and tree_id = @tree_id*/ and not bik_node.linked_node_id is null

	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, #tmpNodes.si_name, @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id 
	where bik_node.id is null;

	set nocount on;

	drop table #primaryIds;
	drop table #secondaryIds;
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id --and bik_node.tree_id = @tree_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid where bik_node.linked_node_id is null--and bk.tree_id = @tree_id 

	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in 
	('Measure', 'Dimension', 'Detail', 'UniverseClass', 'ReportQuery', 'Filter')
				
	drop table #tmpNodes

	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
end;

GO
--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

exec sp_update_version '1.0.85', '1.0.85.5';
go

