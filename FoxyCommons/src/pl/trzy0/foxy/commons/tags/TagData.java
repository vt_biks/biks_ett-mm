package pl.trzy0.foxy.commons.tags;

import java.util.HashMap;
import simplelib.LameRuntimeException;
import java.util.Map;
import simplelib.BaseUtils;
import simplelib.MapHelper;

/**
 *
 * @author wezyr
 */
public class TagData {

//    public static enum Kind {
//
//        TEXT, HTML /*, EXT*/ }
    private String name;
    //private Kind kind;
    private boolean isTextTag;
    private Map<String, String> attrs;
    private int levelDelta;
    public String sourceFileName;
    public int rowNum;
    public int colNum;

    public TagData(String name, boolean isTextTag, Map<String, String> attrs, int levelDelta, int rowNum, int colNum,
            String sourceFileName) {
        this.name = name;
        this.isTextTag = isTextTag;
        this.attrs = attrs;
        this.levelDelta = levelDelta;
        this.rowNum = rowNum;
        this.colNum = colNum;
        this.sourceFileName = sourceFileName;
    }

    public Map<String, String> getAttrs() {
        return attrs;
    }

    public String getReqAttrVal(String name) {
        String attrVal = getAttrVal(name);
        if (attrVal == null) {
            throw new LameRuntimeException("missing required attr " + name + " in tag: " + renderWithOutAttrs());
        }
        return attrVal;
    }

    public void invalidateRenderedTag() {
        renderedTag = null;
    }

    public void setAttrVal(String name, String val) {
        attrs.put(name, val);
        invalidateRenderedTag();
    }

    public String getAttrVal(String name) {
        return attrs != null ? attrs.get(name) : null;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

//    public Kind getKind() {
//        return kind;
//    }
    public boolean isTextTag() {
        return isTextTag;
    }

    public int getLevelDelta() {
        return levelDelta;
    }
    private String renderedTag;

    private void internalRender(StringBuilder sb, boolean disableAttrs) {
        String txt;
        if (!isTextTag()) {
            sb.append("<");
            if (getLevelDelta() == -1) {
                sb.append("/");
            }
            txt = getName();
        } else {
            txt = getName(); //LameUtils.encodeForHTMLTag(getName());
        }
        sb.append(txt);
        if (!isTextTag()) {
            if (!disableAttrs) {
                BaseUtils.renderHtmlAttrs(getAttrs(), sb);
            }
            if (getLevelDelta() == 0) {
                sb.append("/");
            }
            sb.append(">");
        }
    }

    protected String internalRender(boolean disableAttrs) {
        StringBuilder sb2 = new StringBuilder();
        internalRender(sb2, disableAttrs);
        return sb2.toString();
    }

    public void render(StringBuilder sb, boolean disableAttrs) {
        if (disableAttrs) {
            internalRender(sb, disableAttrs);
        } else {
            if (renderedTag == null) {
                renderedTag = internalRender(disableAttrs);
            }
            sb.append(renderedTag);
        }
    }

    public String renderWithOutAttrs() {
        return internalRender(true);
    }

    public String renderWithAttrs() {
        return internalRender(false);
    }

    public String getLocationDescr() {
        return "usage in file: " + sourceFileName +
                ", row: " + rowNum + ", col: " + colNum;
    }

    @SuppressWarnings(value = "unchecked")
    public TagData(MapHelper<String> mh) {
        this.name = mh.getString("n");
        this.isTextTag = mh.getBool("t");
        this.attrs = (Map) mh.getMap("a");
        this.levelDelta = mh.getInt("d");
    }

    public Map<String, Object> toCompactMap() {
        Map<String, Object> m = new HashMap<String, Object>();

//    private String name;
//    private boolean isTextTag;
//    private Map<String, String> attrs;
//    private int levelDelta;
        m.put("n", name);
        if (isTextTag) {
            m.put("t", isTextTag);
        }
        if (!isTextTag && attrs != null) {
            m.put("a", attrs);
        }
        if (levelDelta != 0) {
            m.put("d", levelDelta);
        }
        return m;
    }
}
