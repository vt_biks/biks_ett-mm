/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.plainfile;

import java.util.List;

/**
 *
 * @author ctran
 */
public class ImportedJoinedObj extends ImportedConnectionObj {

    public int isInherit;
    public int mainAttrId;
    public List<ImportedJoinedObjAttr> attrs;
}
