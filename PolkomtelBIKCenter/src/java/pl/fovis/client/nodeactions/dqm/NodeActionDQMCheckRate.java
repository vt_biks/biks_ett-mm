/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.dqm;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.dqm.AddOrEditDQMEvaluationDialog;
import pl.fovis.common.dqm.DQMEvaluationBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class NodeActionDQMCheckRate extends NodeActionDQMBase {

    @Override
    public boolean isEnabledByDesign() {
        return node != null && isNodeLinked() && isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_DQM_RATE/*, BIKGWTConstants.NODE_KIND_DQM_RATE_REJECTED*/);
    }

    @Override
    public String getCaption() {
        return I18n.skontrolujOkresowaOceneDanych.get();
    }

    @Override
    public void execute() {
        new AddOrEditDQMEvaluationDialog().buildAndShowDialog(data.dqmEvaluationBean, null, new IParametrizedContinuation<DQMEvaluationBean>() {

            @Override
            public void doIt(DQMEvaluationBean param) {
                BIKClientSingletons.getDQMService().setDQMEvaluationStatus(data.node.id, param.note, new StandardAsyncCallback<Void>("Error in setDQMEvaluationStatus") {

                    @Override
                    public void onSuccess(Void result) {
                        BIKClientSingletons.showInfo(I18n.skontrolowanoOkresowaOceneJakosci.get());
//                        refreshRedisplayDataAndSelectRecord(data.node.id);
                        refreshAndRedisplayData();
                    }
                });
            }
        }, true);
    }
}
