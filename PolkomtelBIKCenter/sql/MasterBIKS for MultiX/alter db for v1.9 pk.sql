------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- 1. usunięcie zbednych tabel
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_mltx_check_version '1.9'
go
------------------------------------------------------

drop table mltx_login;
go

drop table mltx_registration;
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_mltx_update_version '1.9', '1.10'
go