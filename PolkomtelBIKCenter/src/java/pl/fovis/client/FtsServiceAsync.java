/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Date;
import java.util.List;
import pl.fovis.common.SimilarNodeBean;
import pl.fovis.common.fts.SimilarWordsWithCounts;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public interface FtsServiceAsync {

    public void findSimilarObjects(int objId, double minRelevance, int maxResults, AsyncCallback<List<SimilarNodeBean>> callback);

    public void findSimilarObjectsByLucene(int objId, double minRelevance, int maxResults, AsyncCallback<List<SimilarNodeBean>> callback);

    public void findSimilarObjectsAndWords(int objId, double minRelevance, int maxResults, AsyncCallback<Pair<List<SimilarNodeBean>, List<SimilarWordsWithCounts>>> callback);

    public void getSimilarWordsWithCountsForObjPair(int objId1, int objId2, AsyncCallback<List<SimilarWordsWithCounts>> callback);

    public void changeWordWeight(String word, double weight, AsyncCallback<Void> callback);

    public void getDateDiagInfo(Date d, AsyncCallback<String> callback);
}
