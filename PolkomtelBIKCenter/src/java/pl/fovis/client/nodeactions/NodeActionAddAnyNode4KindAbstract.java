/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddAnyNodeForLinkKindDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.Dialogs;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author efedchenko
 */
public abstract class NodeActionAddAnyNode4KindAbstract extends NodeActionUpdatingBase {

    protected boolean addAsParent;
    protected Integer treeId;

    public NodeActionAddAnyNode4KindAbstract() {
    }

    public NodeActionAddAnyNode4KindAbstract(boolean addAsParent, int treeId) {
        this.addAsParent = addAsParent;
        this.treeId = treeId;
    }

    @Override
    public void execute() {

        Integer nodeKindId = null;
        String relationType = null;

        if (!addAsParent) {
            nodeKindId = node.nodeKindId;
            treeId = node.treeId;
            relationType = BIKConstants.CHILD;
        }

        BIKClientSingletons.getService().getKindsForLinkNodeKind(
                nodeKindId, treeId, relationType, new StandardAsyncCallback<List<TreeNodeBean>>() {

                    @Override
                    public void onSuccess(List<TreeNodeBean> result) {

                        if (result.isEmpty()) {
                            Dialogs.alertHtml(I18n.brakTypowWezlow.get(), I18n.addAnyNode.get(), null);
                        } else {
                            new AddAnyNodeForLinkKindDialog().buildAndShowDialog(
                                    !addAsParent ? node.nodeKindId : null, !addAsParent ? node.treeId : getTreeId(),
                                    !addAsParent ? isNodeFromDynamicTree() : true,
                                    result, node, new IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>>() {

                                        @Override
                                        public void doIt(Pair<TreeNodeBean, Map<String, AttributeBean>> p) {

                                            TreeNodeBean param = p.v1;

                                            BIKClientSingletons.getService().createTreeNodeWithAttr(addAsParent ? null : node.id /*param.parentNodeId*/,
                                                    param.nodeKindId, param.name, param.objId, treeId != null ? treeId : node.treeId, param.linkedNodeId, null,
                                                    p.v2,
                                                    new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on" /* I18N: no */ + " createTreeNode") {
                                                        @Override
                                                        public void onSuccess(Pair<Integer, List<String>> result) {
                                                            redisplayData(result.v1);
                                                        }
                                                    });
                                        }
                                    });
                        }
                    }
                });
    }

    @Override
    public boolean isEnabledByDesign() {
        return (!addAsParent ? isNodeFromDynamicTree() : true);
    }

    public abstract void redisplayData(Integer result);
}
