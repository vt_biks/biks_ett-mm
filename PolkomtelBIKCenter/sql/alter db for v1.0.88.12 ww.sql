exec sp_check_version '1.0.88.12';
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


IF EXISTS (SELECT name FROM sysobjects
      WHERE name = 'trg_bik_node_chunking' AND type = 'TR')
   DROP TRIGGER trg_bik_node_chunking
GO

CREATE TRIGGER trg_bik_node_chunking
ON bik_node
FOR insert, update, delete AS
begin
    -- diag: print cast(sysdatetime() as varchar(23)) + ': start!'

set nocount on

declare @diags_level int = 0;

declare @to_delete table (id int not null primary key);

declare @bik_node_name_chunk table (node_id int not null, tree_id int not null, chunk_txt varchar(3) not null)

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before calc @to_delete'
    end
   
insert into @to_delete (id)    
select d.id from deleted d left join inserted i on d.id = i.id
where i.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 0 and i.is_deleted = 1)
   
   declare @rowcnt int;
   
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before delete by ids'
      set @rowcnt = (select count(*) from @to_delete)
      print cast(sysdatetime() as varchar(23)) + ': before delete by ids, rowcnt=' + cast(@rowcnt as varchar(20))
    end
   
delete from bik_node_name_chunk where node_id in (select id from @to_delete
--select d.id from deleted d left join inserted i on d.id = i.id
--where i.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 0 and i.is_deleted = 1)
);

declare @to_insert table (id int not null, name varchar(max) not null, tree_id int not null);

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before calc @to_insert'
    end
   
insert into @to_insert (id, name, tree_id)
select i.id, i.name, i.tree_id from inserted i left join deleted d on d.id = i.id
where d.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 1 and i.is_deleted = 0);

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before declare cursor'
      set @rowcnt = (select count(*) from @to_insert)
      print cast(sysdatetime() as varchar(23)) + ': before declare cursor, rowcnt=' + cast(@rowcnt as varchar(20))
    end
   
declare nodes cursor for select id, name, tree_id from @to_insert;
/*select id, name, tree_id from bik_node where id in (
select i.id from inserted i left join deleted d on d.id = i.id
where d.name is null or i.name <> d.name or (d.is_deleted = 1 and i.is_deleted = 0)
)*/;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before open cursor'
    end
   
open nodes;

declare @id int, @name varchar(8000), @tree_id int;

  if @diags_level > 0 begin
  -- diag:
    print cast(sysdatetime() as varchar(23)) + ': before first fetch'
  end
 
fetch next from nodes into @id, @name, @tree_id;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before loop'
    end
   
    declare @cnt int = 0;
   
while @@fetch_status = 0
begin
  insert into @bik_node_name_chunk (node_id, chunk_txt, tree_id)
  select distinct @id, chunk_str, @tree_id
  from dbo.fn_split_string(@name, 3);
 
    if @diags_level > 1 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': in loop after insert'
    end
   
  set @cnt = @cnt + 1;
 
  if (@cnt % 1000 = 0) begin
 
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': in loop, before insert into bik_node_name_chunk, cnt:' + cast(@cnt as varchar(10))
    end
   
    insert into bik_node_name_chunk (node_id, chunk_txt, tree_id)
    select node_id, chunk_txt, tree_id
    from @bik_node_name_chunk
   
    delete from @bik_node_name_chunk
  end;   
 
  if @diags_level > 1 begin
     -- diag:
     print cast(sysdatetime() as varchar(23)) + ': in loop before fetch next'
  end
 
  fetch next from nodes into @id, @name, @tree_id;
  if @diags_level > 1 begin
    -- diag:
    print cast(sysdatetime() as varchar(23)) + ': in loop after fetch next'
  end
 
end;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': after loop, before insert into bik_node_name_chunk, @cnt=' + cast(@cnt as varchar(10))
    end
   
    insert into bik_node_name_chunk (node_id, chunk_txt, tree_id)
    select node_id, chunk_txt, tree_id
    from @bik_node_name_chunk
     
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': closing, deallocating cursor'
    end
   
close nodes;

deallocate nodes;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': done, stop!'
    end
end
go


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

-- select * from syscolumns where name = 'attr_type' and id = object_id('bik_attribute_linked')

if exists (select 1 from syscolumns where name = 'attr_type' and id = object_id('bik_attribute_linked'))
exec('delete from bik_attribute_linked from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id where a.attr_type <> 0')
go

exec sp_drop_column_if_exists_with_opt_default 'bik_attribute', 'attr_type'
go
exec sp_drop_column_if_exists_with_opt_default 'bik_attribute_linked', 'fixed_value'
go
exec sp_drop_column_if_exists_with_opt_default 'bik_attribute_linked', 'original_value'
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

-- drop table bik_searchable_attr_props

if object_id('bik_searchable_attr_props') is null
create table bik_searchable_attr_props (
  id int not null identity primary key,
  name varchar(255) not null unique,
  caption varchar(255) not null,
  search_weight int not null default 1,
  type int not null default 1 -- 1: to be normalized, 2: identifier (not normalizable)
)
go

if not exists(select 1 from bik_searchable_attr_props) begin
insert into bik_searchable_attr_props (name, caption, search_weight, type)
values 
('name', 'Nazwa', 10000, 1),
('descr', 'Opis/Tre��', 100, 1),
('guid', 'GUID', 1, 2)
end
go


-- drop table bik_searchable_attr

if object_id('bik_searchable_attr') is null
create table bik_searchable_attr (
  id int not null identity primary key,
  name varchar(255) not null,
  node_kind_id int not null references bik_node_kind (id) on delete cascade,
  type int not null, -- 0: manual, 1: automatic, 2: automatic identifier
  --attr_props_id int null references bik_searchable_attr_props (id),
  unique (node_kind_id, name)
)
go


-- drop table bik_searchable_attr_val

if object_id('bik_searchable_attr_val') is null
create table bik_searchable_attr_val (
  id int not null identity primary key,
  attr_id int not null references bik_searchable_attr (id) on delete cascade,
  node_id int not null references bik_node (id) on delete cascade,
  value varchar(max) not null,
  fixed_value varchar(max) null,
  value_importance int not null,
  --attr_props_id int null references bik_searchable_attr_props (id),
  search_weight int not null,
  unique (attr_id, node_id)
)
go


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

if OBJECT_ID (N'dbo.fn_normalize_text_for_fts', N'FN') is not null
    drop function dbo.fn_normalize_text_for_fts;
go

create function dbo.fn_normalize_text_for_fts(@txt varchar(max))
returns varchar(max)
as
begin
  if @txt is null return null

  set @txt = ltrim(rtrim(@txt))
  if @txt = '' return ''
  
  set @txt = replace(@txt, '_', ' ')
  set @txt = replace(@txt, '(', ' ')
  set @txt = replace(@txt, '/', ' ')
  set @txt = replace(@txt, ')', ' ')
  set @txt = replace(@txt, '\', ' ')
  set @txt = replace(@txt, '[', ' ')
  set @txt = replace(@txt, ']', ' ')
  set @txt = replace(@txt, '*', ' ')
  set @txt = replace(@txt, '"', ' ')
  set @txt = replace(@txt, '''', ' ')

  set @txt = ltrim(rtrim(@txt))
  --if @txt = '' set @txt = null
  
  return @txt
end
go

/*

select dbo.fn_normalize_text_for_fts('ala_ma [kota]')

*/

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


if object_id('[dbo].[fn_split_by_sep]') is not null
  drop function [dbo].[fn_split_by_sep]
go

-- @trimOpt = 0 -> do not trim substr
--          = 1 -> do ltrim substr
--          = 2 -> do rtrim substr
--          = 3 -> do full trim substr
--          & 4 <> 0 -> skip empty substr
CREATE FUNCTION [dbo].[fn_split_by_sep] (@str varchar(max), @sep varchar(max), @trimOpt int)
RETURNS @tab TABLE (str varchar(max) not null, idx int not null primary key)
AS
BEGIN
  declare @pos int = 1;
  declare @len int = DATALENGTH(@str);
  declare @seplen int = DATALENGTH(@sep)
  declare @pos2 int
  declare @substr varchar(max)
  declare @idx int = 1
  
  while @pos <= @len begin
    set @pos2 = charindex(@sep, @str, @pos)
    
    if @pos2 = 0 set @pos2 = @len + 1
    
    set @substr = substring(@str, @pos, @pos2 - @pos)
    
    if @trimOpt & 1 <> 0 set @substr = ltrim(@substr)
    if @trimOpt & 2 <> 0 set @substr = rtrim(@substr)
    
    if @trimOpt & 4 = 0 or DATALENGTH(@substr) <> 0 -- @substr <> '' - to nie zadzia�a dla spacji
    begin
      insert into @tab (str, idx) values (@substr, @idx)
      set @idx = @idx + 1
    end
    
    set @pos = @pos2 + @seplen
  end

  RETURN;
END;
GO


/*

select charindex('a', 'ala ma kota', 2)

select charindex('x', 'ala ma kota', 2)

select len(' ')

select case when len(' ') <> 0 then 'jest znak spacji' else 'brak spacji' end,
  case when datalength(' ') <> 0 then 'jest znak spacji' else 'brak spacji' end,
  case when ' ' <> '' then 'jest znak spacji' else 'brak spacji' end
  
select * from dbo.fn_split_by_sep('ala ama kota', 'a', 7)

select * from dbo.fn_split_by_sep('', ',', 7)

select * from dbo.fn_split_by_sep(null, ',', 7)

*/

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_one_table]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_one_table
go

create procedure [dbo].[sp_verticalize_node_attrs_one_table](@source varchar(max), @nodeIdCol sysname, @cols varchar(max),
  @properNames varchar(max), @valueImportance int, @optNodeFilter varchar(max))
as
begin
  set nocount on
  
  declare @diags_level int = 0 -- 0 oznacza brak logowania

  -- diag
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': start'

  declare @attrPropNamesTab table (idx int primary key, name sysname not null)
  
  insert into @attrPropNamesTab (idx, name)
  select idx, str from dbo.fn_split_by_sep(@properNames, ',', 7)
  
  declare @attrNamesTab table (name sysname not null primary key, attr_type int not null, 
    proper_name varchar(255) not null, search_weight int not null)
  
  insert into @attrNamesTab (name, attr_type, proper_name, search_weight)
  select aaa.str, coalesce(ap.type, 1), coalesce(apnt.name, aaa.str), coalesce(ap.search_weight, 1)
  from dbo.fn_split_by_sep(@cols, ',', 7) aaa left join @attrPropNamesTab apnt on aaa.idx = apnt.idx
    left join bik_searchable_attr_props ap on ap.name = coalesce(apnt.name, aaa.str)

  /*
  declare @type1Cnt int = @@rowcount

  insert into @attrNamesTab (name, attr_type, proper_name)
  select aaa.str, 2, coalesce(apnt.name, aaa.str)
  from dbo.fn_split_by_sep(@identCols, ',', 7) aaa left join @attrPropNamesTab apnt on aaa.idx + @type1Cnt = apnt.idx
  */
  
  -- mamy ju� nazwy atrybut�w i typy w tabelce, trzeba sprawdzi� teraz o jakie node_kindy chodzi

  -- diag
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed wyznaczeniem kind�w'
  
  declare @attrNodeKindsTab table (node_kind_id int not null)
  
  declare @attrNodeKindsSql varchar(max) = 'select distinct n.node_kind_id ' + 
    'from bik_node as n inner join ' + @source + ' as x on n.id = x.' + @nodeIdCol +
    case when @optNodeFilter is null then '' else ' where ' + @optNodeFilter end

  insert into @attrNodeKindsTab (node_kind_id)
  exec (@attrNodeKindsSql)

  -- znamy nazwy atrybut�w i node_kindy, teraz uzupe�niamy bik_attribute, ew. poprawiamy tam typy
  
  declare @nkCnt int = @@rowcount
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed uzupe�nieniem searchable attrs, nodeKindCnt=' +
    cast(@nkCnt as varchar(20))
  
  update bik_searchable_attr 
  set type = ant.attr_type
  from @attrNamesTab ant, @attrNodeKindsTab ankt
  where bik_searchable_attr.name = ant.proper_name and bik_searchable_attr.node_kind_id = ankt.node_kind_id
  
  insert into bik_searchable_attr (name, node_kind_id, type)
  select ant.proper_name, ankt.node_kind_id, ant.attr_type
  from @attrNamesTab ant cross join @attrNodeKindsTab ankt left join bik_searchable_attr a on 
    a.name = ant.proper_name and a.node_kind_id = ankt.node_kind_id
  where a.id is null
  
  -- wrzucamy warto�ci atrybut�w do tabeli pomocniczej
  
  declare @attrValsTab table (attribute_id int not null, node_id int not null, value varchar(max) not null, 
    type int not null, search_weight int not null)
  
  declare attrCur cursor for select name, attr_type, proper_name, search_weight from @attrNamesTab
  
  open attrCur
  
  declare @attrName sysname, @attrType int, @attrProperName varchar(255), @searchWeight int
  declare @attrValSql varchar(max)
  
  fetch next from attrCur into @attrName, @attrType, @attrProperName, @searchWeight
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed p�tl�'

  while @@fetch_status = 0
  begin
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': start p�tli dla pola: ' + @attrName + ' (' + @attrProperName + ')'
    
    set @attrValSql = 'select a.id, x.' + @nodeIdCol + ', x.' + @attrName + ', a.type, ' + cast(@searchWeight as varchar(20)) +
      ' from ' + @source + ' as x inner join bik_node n on n.id = x.' + @nodeIdCol +
      ' inner join bik_searchable_attr a on n.node_kind_id = a.node_kind_id and a.name = ''' + @attrProperName + '''' +
      ' where x.' + @attrName + ' is not null' +
      case when @optNodeFilter is null then '' else ' and ' + @optNodeFilter end
  
    --print 'sql for attr=' + @attrName + ' is: ' + @attrValSql
  
    insert into @attrValsTab (attribute_id, node_id, value, type, search_weight)
    exec (@attrValSql)
  
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed ko�cem iteracji p�tli dla pola: ' + @attrName
    
    fetch next from attrCur into @attrName, @attrType, @attrProperName, @searchWeight
  end
    
  close attrCur
  
  deallocate attrCur
  
  if @diags_level > 0 begin
    declare @avtCnt int = (select count(*) from @attrValsTab)
    print cast(sysdatetime() as varchar(23)) + ': za p�tl�, kursor zamkni�ty, liczba warto�ci razem: ' +
      cast(@avtCnt as varchar(20)) + ', przed update'
  end
  
  if @diags_level > 1 begin
    print 'rows to be updated'
    select *
    from bik_searchable_attr_val, @attrValsTab avt
    where avt.attribute_id = bik_searchable_attr_val.attr_id and avt.node_id = bik_searchable_attr_val.node_id
    and bik_searchable_attr_val.value_importance <= @valueImportance 
    and (case when bik_searchable_attr_val.fixed_value is null then 2 else 1 end <> avt.type or
    bik_searchable_attr_val.value <> avt.value)  
  end
  
  update bik_searchable_attr_val
  set value = avt.value, 
    fixed_value = case when avt.type = 2 then null else dbo.fn_normalize_text_for_fts(avt.value) end,
    value_importance = @valueImportance
  from @attrValsTab avt
  where avt.attribute_id = bik_searchable_attr_val.attr_id and avt.node_id = bik_searchable_attr_val.node_id
  and bik_searchable_attr_val.value_importance <= @valueImportance 
  and (case when bik_searchable_attr_val.fixed_value is null then 2 else 1 end <> avt.type or
    bik_searchable_attr_val.value <> avt.value or bik_searchable_attr_val.search_weight <> avt.search_weight)
  
  declare @updRowCnt int = @@rowcount
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po update, row count: ' +
      cast(@updRowCnt as varchar(20)) + ', przed insert'
  end
  
  insert into bik_searchable_attr_val (node_id, attr_id, value, fixed_value, value_importance, search_weight)
  select avt.node_id, avt.attribute_id, avt.value,   
  case when avt.type = 2 then null else dbo.fn_normalize_text_for_fts(avt.value) end,
  @valueImportance, avt.search_weight
  from @attrValsTab avt left join bik_searchable_attr_val av on
  avt.attribute_id = av.attr_id and avt.node_id = av.node_id
  where av.id is null

  declare @insRowCnt int = @@rowcount

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po insert, row count: ' +
      cast(@insRowCnt as varchar(20)) + ', KONIEC!'
  end
  
  -- select * from @attrValsTab
  
  --declare @sql varchar(max) = 'select ' + @nodeIdCol + ', ' + @normalCols + ', ' + @identCols + ' from ' + @source
  --print @sql
  
  --select * from @attrNamesTab
  --select * from @attrNodeKindsTab ankt inner join bik_node_kind nk on ankt.node_kind_id = nk.id
end
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

/*

select bn.*, sr.rnk as fts_rank
from
bik_node bn inner join 
  (select node_id, sum(sr.[rank] * av.search_weight) as rnk from bik_searchable_attr_val av inner join
     freetexttable(bik_searchable_attr_val, *, 'annapurna') sr on av.id = sr.[key]
     group by node_id) sr on sr.node_id = bn.id
where bn.is_deleted = 0 and bn.linked_node_id is null

*/

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_one_metadata_table]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_one_metadata_table
go

create procedure [dbo].sp_verticalize_node_attrs_one_metadata_table(@source varchar(max), @nodeIdCol sysname, 
  @cols varchar(max), @properNames varchar(max), @optNodeFilter varchar(max))
as
begin
  exec sp_verticalize_node_attrs_one_table @source, @nodeIdCol, @cols, @properNames, 0, @optNodeFilter
end
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_metadata]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_metadata
go

create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_extradata', 'node_id', 'author, owner, guid', null, @optNodeFilter

  exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @optNodeFilter
end
go


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs
go

create procedure [dbo].[sp_verticalize_node_attrs](@optNodeFilter varchar(max))
as
begin
  if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val'))
  exec('drop fulltext index on bik_searchable_attr_val')

  exec sp_verticalize_node_attrs_one_table 'bik_node', 'id', 'name, descr', null, 0, @optNodeFilter

  exec sp_verticalize_node_attrs_metadata @optNodeFilter

  declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_searchable_attr_val') and is_primary_key = 1)
  exec('CREATE FULLTEXT INDEX ON bik_searchable_attr_val (value, fixed_value) key index ' + @pk_name)
end
go


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_update_searchable_attr_val]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_update_searchable_attr_val
go

create procedure [dbo].sp_update_searchable_attr_val(@nodeId int, @properAttrName varchar(255),
  @valueImportance int, @value varchar(max))
as
begin
  declare @source varchar(max) = '(select ' + cast(@nodeId as varchar(20)) + ' as node_id, 
    ''' + replace(@value, '''', '''''') + ''' as val)'
  
  declare @nodeFilter varchar(max) = 'n.id = ' + cast(@nodeId as varchar(20))
    
  exec sp_verticalize_node_attrs_one_table @source, 'node_id', 'val', @properAttrName, @valueImportance, 
    @nodeFilter
end
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

EXECUTE sp_set_app_prop 'mustRunStripHtmlTagsFromRichTextFields', '1'
GO

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

/*

select top 10 * from bik_node

select * from bik_node_kind


exec sp_update_searchable_attr_val 1, 'xxx', 1, 1, 'ala ma kota

 a kot_ma_al� i ''psa'''

select top 10 * from bik_searchable_attr_val av inner join bik_searchable_attr a on av.attr_id = a.id
where a.name = 'name'

*/

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

/*

exec sp_verticalize_node_attrs null


sp_verticalize_node_attributes

truncate table bik_searchable_attr_val
delete from bik_searchable_attr


select count(*) from bik_node where descr is not null

exec sp_verticalize_node_attrs_one_table 'bik_node', 'id', 'name, descr', null

exec sp_verticalize_node_attrs_one_table 'bik_sapbo_extradata', 'node_id', 'author, owner', 'guid'

exec sp_verticalize_node_attrs_one_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null


exec [sp_verticalize_node_attributes] 'bik_sapbo_extradata', 'node_id', 'author, owner', 'guid'

exec [sp_verticalize_node_attributes] 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null


select * from bik_searchable_attr_val av inner join bik_searchable_attr a on av.attr_id = a.id


select * from bik_attribute

select a.id, x.node_id, x.guid, a.type from bik_sapbo_extradata as x inner join bik_node n on n.id = x.node_id inner join bik_searchable_attribute a on n.node_kind_id = a.node_kind_id and a.name = 'guid' where x.guid is not null



-----------------------
-----------------------
-----------------------

select * from bik_sapbo_extradata
select * from bik_sapbo_universe_table
select * from bik_sapbo_universe_connection
select * from bik_sapbo_universe_object
select * from bik_sapbo_query
select * from bik_sapbo_universe_column

select count(*) from bik_sapbo_extradata
select count(*) from bik_sapbo_universe_table
select count(*) from bik_sapbo_universe_connection
select count(*) from bik_sapbo_universe_object
select count(*) from bik_sapbo_query
select count(*) from bik_sapbo_universe_column

select * from bik_sapbo_universe_table
where coalesce(sql_of_derived_table, '') <> coalesce(sql_of_derived_table_with_alias, '')


select * from bik_sapbo_query


*/

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


/*

1. w nazwie def. korp.
2. w nazwie innego obiektu
3. w opisie/tre�ci def. korp.
4. w opisie/tre�ci innego obiektu



select top 20 bn.id as node_id,
    bn.name,
    bnk.code as node_kind_code,
    bnk.caption as node_kind_caption,
    bt.name as tree_name,
    bt.code as tree_code,
    bn.vote_sum,
    bn.vote_cnt,
    bn.fts_rank
    from (
    
    select bn.*, sr.rnk as fts_rank
    from
    bik_node bn inner join
    (select node_id, sum(sr.[rank] * coalesce(ap.search_weight, 1)) as rnk from bik_searchable_attr_val av inner join
    freetexttable(bik_searchable_attr_val, *, 'Deaktywacja') sr on av.id = sr.[key]
    inner join bik_searchable_attr a on av.attr_id = a.id
    left join bik_searchable_attr_props ap on a.name = ap.name
    group by node_id) sr on sr.node_id = bn.id
    where bn.is_deleted = 0 and (bn.linked_node_id is null or bn.disable_linked_subtree <> 0)

    ) as bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    inner join bik_tree bt on bn.tree_id = bt.id
    order by 
    vote_val desc, fts_rank desc
    --bn.search_rank + bnk.search_rank desc, bn.fts_rank desc
    
    --select * from bik_app_prop
    

*/

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

exec sp_verticalize_node_attrs null

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

exec sp_update_version '1.0.88.12', '1.0.88.13';
go
