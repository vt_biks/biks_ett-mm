CREATE TABLE [dbo].[bik_attachment](
	[id] [bigint] IDENTITY(1,1) NOT NULL primary key,
	[node_id] [int] NOT NULL references bik_node(id),
	[href] [varchar](max) NULL,
	[caption] [varchar](max) NULL);