/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

/**
 *
 * @author pmielanczuk
 */
public class DoNothingAsyngCallback<T> extends StandardAsyncCallback<T> {

    public static DoNothingAsyngCallback<Void> doNothingCallback = new DoNothingAsyngCallback<Void>();

    public DoNothingAsyngCallback() {
        super(null);
    }

    public DoNothingAsyngCallback(String errorMsg) {
        super(errorMsg);
    }

    public void onSuccess(T result) {
        // no-op
    }

    @SuppressWarnings("unchecked")
    public static <T> DoNothingAsyngCallback<T> getDoNothingCallback() {
        return (DoNothingAsyngCallback<T>) doNothingCallback;
    }
}
