exec sp_check_version '1.6.z5.1';
go

if not exists(select * from sys.objects where object_id = object_id(N'[bik_confluence_missing_joined_node]') and type in (N'U'))
begin 
	create table bik_confluence_missing_joined_node (
		conf_node_id int not null,
		dst_tree_code varchar(255) null,
		dst_tree_id int null,
		opt_dst_subnode_name varchar(max) null,
		opt_dst_subnode_parent_id int null,
		opt_dst_branch_ids_like varchar(max) null,
		dst_node_id int null
	);
end;
go

exec sp_update_version '1.6.z5.1', '1.6.z5.2';
go