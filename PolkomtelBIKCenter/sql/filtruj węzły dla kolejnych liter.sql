/*
exec sp_filter_bik_nodes 'k', 16, null


select id from bik_node where id in (select node_id from bik_node_name_chunk
  where (chunk_txt like 'f%') and tree_id = 16) and name like '%f%' and is_deleted = 0
  
  
select node_id from bik_node_name_chunk
  where (chunk_txt like 'k%') and tree_id = 16  
  
*/

-- select * from bik_tree
declare @tree_id int = (select id from bik_tree where code = 'Documents' --'Glossary'
)
declare @c char = 'a'

while 1 = 1
begin
  exec sp_filter_bik_nodes @c, @tree_id, null
  
  if (@c = 'z') break
  select @c = char(ascii(@c)+1)
end

-- 4:01
