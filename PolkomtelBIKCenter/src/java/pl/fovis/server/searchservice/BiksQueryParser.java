/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.searchservice;

import java.util.Date;
import java.util.Set;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.QueryParser;
import pl.bssg.searchengine.SearchUtils;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.SimpleDateUtils;

/**
 *
 * @author ctran
 */
public class BiksQueryParser extends QueryParser {

    protected Set<String> numerFields;
    protected Set<String> dateFields;

    public BiksQueryParser(Set<String> numberFields, Set<String> dateFields, String defaultField, Analyzer analyzer) {
        super(defaultField, analyzer);
        this.numerFields = numberFields;
        this.dateFields = dateFields;
        for (String field : dateFields) {
            setDateResolution(field, DateTools.Resolution.DAY);
        }
    }

    @Override
    protected org.apache.lucene.search.Query newRangeQuery(String field, String part1, String part2, boolean startInclusive, boolean endInclusive) {
        if (numerFields.contains(field)) {
            Object num1 = BaseUtils.tryParseNumber(part1);
            Object num2 = BaseUtils.tryParseNumber(part2);
            if (num1 == null || num2 == null) {
                throw new LameRuntimeException("Nie poprawny format");
            }
            return super.newRangeQuery(field, SearchUtils.convertDouble2String(SearchUtils.convertNumberAsDouble(num1)), SearchUtils.convertDouble2String(SearchUtils.convertNumberAsDouble(num2)), startInclusive, endInclusive); //To change body of generated methods, choose Tools | Templates.
        } else if (dateFields.contains(field)) {
            Date date1 = SimpleDateUtils.parseFromCanonicalString(part1);
            Date date2 = SimpleDateUtils.parseFromCanonicalString(part2);
            if (date1 == null || date1 == null) {
                throw new LameRuntimeException("Nie poprawny format");
            }
            return super.newRangeQuery(field, SearchUtils.convertDouble2String(1.0 * date1.getTime()), SearchUtils.convertDouble2String(1.0 * date2.getTime()), startInclusive, endInclusive); //To change body of generated methods, choose Tools | Templates.
        }
        return super.newRangeQuery(field, part1, part2, startInclusive, endInclusive); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected org.apache.lucene.search.Query newTermQuery(org.apache.lucene.index.Term term) {
        if (numerFields.contains(term.field())) {
            Object num1 = BaseUtils.tryParseNumber(term.text());
            if (num1 == null) {
                throw new LameRuntimeException("Nie poprawny format");
            }

            return super.newTermQuery(new Term(term.field(), SearchUtils.convertDouble2String(SearchUtils.convertNumberAsDouble(num1)))); //To change body of generated methods, choose Tools | Templates.
        } else if (dateFields.contains(term.field())) {
            String text = term.text();
            Date date1 = SimpleDateUtils.parseFromCanonicalString(text);
            if (date1 == null) {
                throw new LameRuntimeException("Nie poprawny format");
            }

            return super.newTermQuery(new Term(term.field(), SearchUtils.convertDouble2String(1.0 * date1.getTime()))); //To change body of generated methods, choose Tools | Templates.
        }
        return super.newTermQuery(term); //To change body of generated methods, choose Tools | Templates.
    }

}
