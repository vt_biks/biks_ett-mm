exec sp_check_version '1.5.x';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
--
-- 1. za�o�enie opcjonalnych tabelek trybu MultiX
--
-- 2. ustawienie starego trybu pracy (SingleBIKS)
--
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

/*
-- te tabelki s� opcjonalne, wi�c do test�w mo�na usun��
drop table mltx_app_prop
go
drop table mltx_login
go
*/

create table mltx_app_prop (name varchar(255) not null primary key, val varchar(max))
go

create table mltx_login (login varchar(255) not null primary key, database_name sysname not null)
go


-------------------------------------------------------------------------
-------------------------------------------------------------------------
-- przestawienie tutaj na true lub 1 spowoduje prac� w trybie MultiX
insert into mltx_app_prop (name, val) values ('is_multi', 'false')
go


-------------------------------------------------------------------------
-------------------------------------------------------------------------
-- przyk�adowe dane dla MultiX oraz sprawdzenia w wa�nych tabelkach
/*

select * from mltx_login


update mltx_app_prop set val = 'true' where name = 'is_multi'

insert into mltx_login (login, database_name) values ('pm', 'bssg-demo-v1.4.y1.4')

insert into mltx_login (login, database_name) values ('pmielanczuk', 'bssg-demo-v1.4.y1.4')

insert into mltx_login (login, database_name) values ('Jan Kowalski', 'bssg-demo-v1.4.y1.4')


select * from bik_node where branch_ids is null and is_deleted = 0

select * from bik_system_user

select * from bik_right_role

select *
    from 
    bik_system_user bsu left join 
    bik_user_right bur on bsu.id = bur.user_id
    left join bik_right_role brr 
     on brr.id = bur.right_id
    
    
*/

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

exec sp_update_version '1.5.x', '1.5.x.1';
go
