/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.searchengine;

import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import morfologik.stemming.WordData;
import morfologik.stemming.polish.PolishStemmer;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class SearchUtils {

    public static final Double DOUBLE_MAX_INDEX = 1000000000000000.0;
    protected static final DecimalFormat df = new DecimalFormat("0000000000000000.000000");

    public static Document createDocument(String docIdStr) {
        Document doc = new Document();
        doc.add(new StringField(SearchConstant.DOC_ID, docIdStr, Field.Store.YES));

        return doc;
    }

    public static void markAsDeleted(Document doc, boolean isDeleted) {
        addTextField(doc, SearchConstant.IS_DELETED, isDeleted ? "true" : "false");
    }

    public static void addTextField(Document doc, String fieldName, String value) {
        doc.removeField(fieldName);
        if (!BaseUtils.isStrEmptyOrWhiteSpace(value)) {
//            doc.add(new TextField(fieldName, value.toLowerCase(), Field.Store.YES));
            doc.add(new TextField(fieldName, value, Field.Store.YES));            
        }
    }

    public static void addSubQuery(BooleanQuery.Builder queryBuilder, String term, boolean isNegative, String field) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(term)) {
            return;
        }
        if (!term.contains("*")) {
            List<String> tokens = SearchUtils.tokenizeString(SearchConstant.getAnalyzer(), term);
            if (term.contains(" ")) {
                PhraseQuery.Builder phraseQueryBuilder = new PhraseQuery.Builder();

                for (String phrase : tokens) {
                    phraseQueryBuilder.add(new Term(field, phrase));
                }
                phraseQueryBuilder.setSlop(1);
                if (isNegative) {
                    queryBuilder.add(phraseQueryBuilder.build(), BooleanClause.Occur.MUST_NOT);
                } else {
                    queryBuilder.add(phraseQueryBuilder.build(), BooleanClause.Occur.MUST);
                }
            } else {
                Query query;
                if (!term.endsWith("~")) {
                    query = new TermQuery(new Term(field, tokens.get(0)));
                } else {
                    query = new FuzzyQuery(new Term(field, term.substring(0, term.length() - 1)));
                }
                if (isNegative) {
                    queryBuilder.add(query, BooleanClause.Occur.MUST_NOT);
                } else {
                    queryBuilder.add(query, BooleanClause.Occur.MUST);
                }
            }
        } else {
            WildcardQuery theWildcardQuery = new WildcardQuery(new Term(field, term));
            if (isNegative) {
                queryBuilder.add(theWildcardQuery, BooleanClause.Occur.MUST_NOT);
            } else {
                queryBuilder.add(theWildcardQuery, BooleanClause.Occur.MUST);
            }
        }
    }

    public static Query parse(String txt, String field) {
        BooleanQuery.Builder builder = new BooleanQuery.Builder();

        boolean isStringMode = false;
        boolean isNegated = false;
        StringBuilder currentTerm = new StringBuilder();

        for (int i = 0; i < txt.length(); i++) {
//            char currentChar = Character.toLowerCase(txt.charAt(i));
            char currentChar = txt.charAt(i);            
            if (currentChar == '\"') {
                isStringMode = !isStringMode;
            } else {
                if (!isStringMode) {
                    switch (currentChar) {
                        case '-': {
                            if (currentTerm.length() == 0) {
                                isNegated = true;
                            } else {
                                currentTerm.append(currentChar);
                            }
                            break;
                        }
                        case '+':
                            if (currentTerm.length() == 0) {
                                isNegated = false;
                            } else {
                                currentTerm.append(currentChar);
                            }
                            break;
                        case ' ': {
                            addSubQuery(builder, currentTerm.toString(), isNegated, field);
                            currentTerm = new StringBuilder();
                            isNegated = false;
                            break;
                        }
                        default: {
                            currentTerm.append(currentChar);
                            break;
                        }
                    }
                } else {
                    currentTerm.append(currentChar);
                }
            }
        }

        if (currentTerm.length() > 0) {
            addSubQuery(builder, currentTerm.toString(), isNegated, field);
        }

        return builder.build();
    }

    public static List<String> tokenizeString(Analyzer analyzer, String string) {
        List<String> result = new ArrayList<String>();
        try {
            TokenStream stream = analyzer.tokenStream(null, new StringReader(string));
            stream.reset();
            while (stream.incrementToken()) {
                result.add(stream.getAttribute(CharTermAttribute.class).toString());
            }
            stream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public static void main(String[] args) {
        PolishStemmer stemmer = new PolishStemmer();

        String in = "Nie zabrakło oczywiście wpadek. Największym zaskoczeniem okazał się dla nas strój Katarzyny Zielińskiej, której ewidentnie o coś chodziło, ale wciąż nie wiemy o co.";
        for (String t : in.toLowerCase(new Locale("pl")).split("[\\s\\.\\,]+")) {
            System.out.println("> '" + t + "'");
            for (WordData wd : stemmer.lookup(t)) {
                System.out.print(
                        "  - "
                        + (wd.getStem() == null ? "<null>" : wd.getStem()) + ", "
                        + wd.getTag());
            }
            System.out.println();
        }
    }

    public static String convertDouble2String(Double value) {
        if (Math.abs(value) > DOUBLE_MAX_INDEX) {
            return null;
        }
        if (value < 0) {
            value = value + DOUBLE_MAX_INDEX;
        }
        return df.format(value);
    }

    public static Double convertNumberAsDouble(Object number) {
        return (number instanceof Integer) ? 1.0 * ((Integer) number) : ((Double) number);
    }
}
