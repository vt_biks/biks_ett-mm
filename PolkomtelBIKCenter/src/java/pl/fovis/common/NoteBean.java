/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author wezyr
 */
public class NoteBean implements Serializable {

    public int id;
    public String title;
    public String body;
    public int userId;
    public Date dateAdded;
    public String authorName;
    public String avatar;
//    public String loginName;
//    public boolean activ;
//    public String oldLoginName;
    public String nodeTreeCode;
    public int nodeId;
    public String nodeName;

    public NoteBean() {
    }

    public NoteBean(int id) {
        this.id = id;
    }
}
