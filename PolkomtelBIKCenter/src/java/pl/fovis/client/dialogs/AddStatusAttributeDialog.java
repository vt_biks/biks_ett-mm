/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class AddStatusAttributeDialog extends BaseActionOrCancelDialog {

    protected List<AttributeBean> attrs;
    protected FlexTable attrsFt = new FlexTable();
    protected Map<String, ListBox> attrLb = new HashMap<String, ListBox>();
    protected IParametrizedContinuation<Map<String, AttributeBean>> saveCont;

    @Override
    protected String getDialogCaption() {
        return I18n.ustawStatus.get();
    }

    public void buildAndShowDialog(List<AttributeBean> attrs, IParametrizedContinuation<Map<String, AttributeBean>> saveCont) {
        this.attrs = attrs;
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {
        int row = 0;

        for (AttributeBean ab : attrs) {
            Label atrName = new Label(ab.atrName);
            atrName.setStyleName("addNoteLbl");
            attrsFt.setWidget(row, 0, atrName);
            ListBox lbl = new ListBox();

            lbl.setStyleName("addAttr");
            lbl.setWidth("200px");
            List<String> attrOpt = BaseUtils.splitBySep(ab.valueOpt, ",");
            String value = ab.atrLinValue == null ? ab.defaultValue : ab.atrLinValue;
            int idx = 0;
            for (String val : attrOpt) {
                lbl.addItem(val);
                if (val.equals(value)) {
                    lbl.setSelectedIndex(idx);

                }
                idx++;
            }
            attrsFt.setWidget(row, 1, lbl);
            attrLb.put(ab.atrName, lbl);

            row++;
        }
        main.add(attrsFt);
    }

    @Override
    protected void doAction() {

        Map<String, AttributeBean> ax = new HashMap<String, AttributeBean>();
        for (AttributeBean ab : attrs) {
//            Window.alert("A " + ab.atrName);
            AttributeBean a = ab;
            a.atrLinValue = attrLb.get(a.atrName).getSelectedValue();
//            Window.alert(a.atrLinValue + " " + a.atrName);
            ax.put(a.atrName, a);
//            Window.alert(a.atrName);
        }
//        Window.alert("A");
        saveCont.doIt(ax);
    }

}
