/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.templates;

import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.CachedGenericResourceProvider;
import simplelib.IGenericResourceProvider;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class CachingNamedTemplateProvider extends CachedGenericResourceProvider<IParsedTemplate>
        implements INamedTemplateProvider {

    //private ITemplateParser parser;

    public CachingNamedTemplateProvider(IGenericResourceProvider<String> resProvider,
            final ITemplateParser parser, final boolean safeMode) {
        super(BaseUtils.compoundProvider(resProvider,
                new Projector<String, IParsedTemplate>() {

            public IParsedTemplate project(String val) {
                if (val == null) {
                    if (safeMode) {
                        return null;
                    }
                    throw new LameRuntimeException("cannot parse null");
                }
                return parser.parse("?", val);
            }
        }));
        //this.parser = parser;
    }

    public IParsedTemplate getTemplate(String name) {
        return gainResource(name);
    }
}
