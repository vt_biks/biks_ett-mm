package pl.fovis.client;

import simplelib.IContinuation;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author młydkowski
 */
public class EntityDetailsPaneForDialog extends EntityDetailsPane {

    private IContinuation optDoAfterDataFetched;
    private IContinuation optDoAfterSizeProbablyChanged;
    protected IContinuation refreshContinuation;

    public EntityDetailsPaneForDialog(IContinuation optDoAfterDataFetched,
            IContinuation optDoAfterSizeProbablyChanged, IContinuation refreshContinuation) {
        super(false);
        this.optDoAfterDataFetched = optDoAfterDataFetched;
        this.optDoAfterSizeProbablyChanged = optDoAfterSizeProbablyChanged;
        this.refreshContinuation = refreshContinuation;
        this.isFooterExpanded = !BIKClientSingletons.notExpandingJoinedObjsFooter();
    }

    @Override
    protected void subdetailsTabSelected() {
        super.subdetailsTabSelected();
        if (optDoAfterSizeProbablyChanged != null) {
            optDoAfterSizeProbablyChanged.doIt();
        }
    }

    @Override
    protected void dataFetched() {
        super.dataFetched();
        if (optDoAfterDataFetched != null) {
            optDoAfterDataFetched.doIt();
        }
    }

    @Override
    protected IContinuation getRefreshContinuation() {
        return refreshContinuation;
    }
}
