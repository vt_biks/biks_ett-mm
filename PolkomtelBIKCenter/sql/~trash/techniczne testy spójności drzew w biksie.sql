﻿-- przy upgrade bazy demo_orange, przed wykonaniem skryptu alter db for v1.1.9.15 bf.sql
--   problemy są z testami 2, 10, 13
-- po wykonaniu skryptu alter db for v1.1.9.15 bf.sql na bazie demo ogólne
--   problemy są z testami 2, 8, 9, 10, 13
-- poprawki do 2, 3 i 8 są w skrypcie alter db for v1.1.9.21 pm.sql
-- po wykonaniu skryptu poprawkowego alter db for v1.1.9.21 pm.sql
--   nadal są problemy z 9, 10, 13, doszło jeszcze 15!
-- po wykonaniu skryptu poprawkowego alter db for v1.1.9.24 pm.sql
--   nadal są problemy z 15
-- po wykonaniu skryptu poprawkowego alter db for v1.1.9.26 pm.sql
--   jest OK
-------------------------


-- 1. 
-------------------------
-- OK na bazie v1.1.9.6 z polkomtela!
-- OK w wersji v1.1.9.16 dla polkomtela
-------------------------
-------------------------
-- dzieco w innym drzewie niż rodzic - to błąd
select * 
from bik_node child inner join bik_node parent on child.parent_node_id = parent.id
where child.tree_id <> parent.tree_id
and child.is_deleted = 0
go


-- 2. naprawiane przez alter db for v1.1.9.21 pm.sql (fragment [TSB-FIX:2])
-------------------------
-- OK na bazie v1.1.9.6 z polkomtela!
-- OK w wersji v1.1.9.16 dla polkomtela
-------------------------
-------------------------
-- dziecko nie jest usunięte, a rodzic owszem - to błąd
select * 
from bik_node child inner join bik_node parent on child.parent_node_id = parent.id
where child.is_deleted = 0 and parent.is_deleted = 1
go


-- 3. poprawka w alter db for v1.1.9.21 pm.sql, fragment oznaczony [TSB-FIX:3]
-------------------------
-- OK na bazie v1.1.9.6 z polkomtela!
-- OK w wersji v1.1.9.16 dla polkomtela
-------------------------
-------------------------
-- ścieżka idków (branch_ids) jest pusta lub źle uzupełniona - to błąd
select *
from bik_node
where is_deleted = 0 and ltrim(rtrim(coalesce(branch_ids, ''))) not like '%' + 
  case when parent_node_id is not null then cast(parent_node_id as varchar(20)) + '|' else '' end +
  cast(id as varchar(20)) + '|'
go


-- 4. UWAGA: to zapytanie jest wyłączone, bo może się zdarzyć,
-- że nazwy są powielone!!!
-------------------------
-- tak się może zdarzyć, w metadanych, nie mamy na to wpływu
-- (znaleziono 4354 rekordów na bazie v1.1.9.6 z Polkomtela)
-- bez zmian (OK) w wersji v1.1.9.16 dla polkomtela
-------------------------
-------------------------
-- zduplikowane nazwy w ramach jednego rodzica - zazwyczaj to błąd
declare @treeOfTreesId int = (select id from bik_tree where code = 'TreeOfTrees')
select (select code from bik_tree where id = n.tree_id) as tree_code, 
(select code from bik_node_kind where id = n.node_kind_id) as node_kind_code,
* 
from bik_node n join 
(select case when parent_node_id is null then 't' + cast(tree_id as varchar(20)) else 'p' + cast(parent_node_id as varchar(20)) end as parent_id_fix, name
from bik_node where is_deleted = 0 and tree_id <> @treeOfTreesId 
 group by case when parent_node_id is null then 't' + cast(tree_id as varchar(20)) else 'p' + cast(parent_node_id as varchar(20)) end, name 
 having count(*) > 1) x on 
   case when n.parent_node_id is null then 't' + cast(n.tree_id as varchar(20)) else 'p' + cast(n.parent_node_id as varchar(20)) end = x.parent_id_fix
   and n.name = x.name
where 1 = 0 and n.is_deleted = 0 
order by x.parent_id_fix, n.name
go


-- 5. 
-------------------------
-- OK na bazie v1.1.9.6 z polkomtela!
-- OK w wersji v1.1.9.16 dla polkomtela
-------------------------
-- na bazie polkomtela v1.1.9.6 są przypadki w drzewie ObjectUniverses
-- (7550 rekordów), ale dla tego samego obj_id są różne node_kindy
-- a Tomek po tym poznaje (para obj_id / node_kind) unikalne obiekty
-------------------------
-------------------------
-- zduplikowane niepodlinkowane węzły z tym samym nie nullowym obj_id w tym samym drzewie - błąd
-- w modrzewiu taka duplikacja jest dopuszczalna (choć można jej unikać),
-- w metadanych - jeżeli typ węzła jest różny, to ujdzie (Tomek to rozpoznaje)
-- ale dla jednego typu węzła duplikacja obj_id jest groźna (np. w metadanych)
declare @treeOfTreesId int = (select id from bik_tree where code = 'TreeOfTrees')
declare @treeObjectUniversesId int = (select id from bik_tree where code = 'ObjectUniverses')
select 
  (select code from bik_tree where id = n.tree_id) as tree_code, 
  (select code from bik_node_kind where id = n.node_kind_id) as node_kind_code,
  n.* 
from bik_node n join 
  (select tree_id, obj_id
   from bik_node
   where is_deleted = 0 and obj_id is not null and linked_node_id is null
   group by tree_id, obj_id
   having count(*) > 1
   ) x on n.tree_id = x.tree_id and n.obj_id = x.obj_id
where n.is_deleted = 0 and n.linked_node_id is null and
  n.tree_id not in (@treeOfTreesId, @treeObjectUniversesId)
order by tree_code, n.obj_id, n.id
go


-- 6. 
-------------------------
-- OK na bazie v1.1.9.6 z polkomtela!
-- OK w wersji v1.1.9.16 dla polkomtela
-------------------------
-- zduplikowane niepodlinkowane węzły z tym samym nie nullowym obj_id
-- w ramach jednego node_kind_id w tym samym drzewie - błąd
-- tak może być tylko w modrzewiu
-------------------------
declare @treeOfTreesId int = (select id from bik_tree where code = 'TreeOfTrees')
select 
  (select code from bik_tree where id = n.tree_id) as tree_code, 
  (select code from bik_node_kind where id = n.node_kind_id) as node_kind_code,
  n.* 
from bik_node n join 
  (select tree_id, obj_id, node_kind_id
   from bik_node
   where is_deleted = 0 and obj_id is not null and linked_node_id is null
   group by tree_id, obj_id, node_kind_id
   having count(*) > 1
   ) x on n.tree_id = x.tree_id and n.obj_id = x.obj_id and n.node_kind_id = x.node_kind_id
where n.is_deleted = 0 and n.linked_node_id is null and
  n.tree_id <> @treeOfTreesId
order by tree_code, n.obj_id, n.id
go



-- 7. 
-------------------------
-- OK na bazie v1.1.9.6 z polkomtela!
-- OK w wersji v1.1.9.16 dla polkomtela
-------------------------
-------------------------
-------------------------
-- podlinkowany sam jest podlinkowanym (to nie oryginał) - błąd
select * from bik_node nj inner join bik_node no on nj.linked_node_id = no.id
where no.linked_node_id is not null and nj.is_deleted = 0
go


-- 8. poprawka w alter db for v1.1.9.21 pm.sql, fragment oznaczony [TSB-FIX:8]
-------------------------
-- OK na bazie v1.1.9.6 z polkomtela!
-- w wersji v1.1.9.16 dla polkomtela jest błąd
-- użytkownik Mirosław Golec - usunięty, ale jest nieusunięty
-- jako podlinkowany
-------------------------
-- skrypt do v1.1.9.22 powinien poprawić!
-------------------------
-------------------------
-------------------------
-- oryginał usunięty, podlinkowany węzeł nie - błąd
select * from bik_node nj inner join bik_node no on nj.linked_node_id = no.id
where no.is_deleted = 1 and nj.is_deleted = 0
go


-- 9. poprawka w alter db for v1.1.9.24 pm.sql, fragment oznaczony [TSB-FIX:9]
-------------------------
-- OK na bazie v1.1.9.6 z polkomtela!
-- źle w wersji v1.1.9.16 dla polkomtela
-------------------------
-- jest 756 błędnych rekordów - trzeba poprawić!
-------------------------
-------------------------
-------------------------
-- wyszukiwalne wartości atrybutów z innych drzew lub o innych typach
-- niż wynika z bik_node - błąd
select 
  (select code from bik_tree where id = n.tree_id) as tree_code_from_node,
  (select code from bik_node_kind where id = n.node_kind_id) as node_kind_code_from_node,
  (select code from bik_tree where id = sav.tree_id) as tree_code_from_sav,
  (select code from bik_node_kind where id = sav.node_kind_id) as node_kind_code_from_sav,
  * 
from bik_searchable_attr_val sav inner join bik_node n on sav.node_id = n.id
where sav.tree_id <> n.tree_id or sav.node_kind_id <> n.node_kind_id
go


-- 10. poprawka w alter db for v1.1.9.24 pm.sql, fragment oznaczony [TSB-FIX:10]
-------------------------
-- na bazie polkomtela v1.1.9.6 są 2 adresy mailowe w 
-- wyszukiwalnych wartościach atrybutów, powiązane z usuniętymi użytkownikami
-- (Tomasz Miazek-Mioduszewski, Elżbieta Grądziel)
-------------------------
-- w wersji v1.1.9.16 dla polkomtela ten sam błąd
-- trzeba poprawić!
-------------------------
-------------------------
-- wyszukiwalne wartości atrybutów dla usuniętych węzłów
select (select code from bik_tree where id = n.tree_id) as tree_code,
  (select code from bik_node_kind where id = n.node_kind_id) as node_kind_code,
  * 
from bik_searchable_attr_val sav left join bik_node n on sav.node_id = n.id
where n.is_deleted <> 0
go


-- 11.
-------------------------
-- OK na bazie v1.1.9.6 z polkomtela!
-- OK w wersji v1.1.9.16 dla polkomtela
-------------------------
-------------------------
-------------------------
-- wyszukiwalne wartości atrybutów dla podlinkowanych, ale nie automatycznych
-- automatyczne to takie z disable_linked_subtree = 1, więc sprawdzamy te
-- co mają disable_linked_subtree = 0
select (select code from bik_tree where id = n.tree_id) as tree_code,
  (select code from bik_node_kind where id = n.node_kind_id) as node_kind_code,
  * 
from bik_searchable_attr_val sav left join bik_node n on sav.node_id = n.id
where n.linked_node_id is not null and n.disable_linked_subtree = 0
go


--select * from bik_tree where id = 14
--select * from bik_tree where id = 29
--select * from bik_node_kind where id = 22


-- 12.
-------------------------
-- OK na bazie v1.1.9.16 polkomtela!
-------------------------
-------------------------
-------------------------
-- niezgodne drzewo w bik_name_node_chunked względem drzewa w bik_node
select 
  (select code from bik_tree where id = n.tree_id) as tree_code,
  (select code from bik_node_kind where id = n.node_kind_id) as node_kind_code,
  * 
from bik_node_name_chunk nnc inner join bik_node n on nnc.node_id = n.id
where nnc.tree_id <> n.tree_id
go


-- 13. poprawka w alter db for v1.1.9.24 pm.sql, fragment oznaczony [TSB-FIX:13]
-------------------------
-- źle na bazie v1.1.9.16 polkomtela!
-- w glosariuszu wersje i użytkownicy w Użytkownikach (wg ról)
-- mają wpisy w bik_name_node_chunked dla usuniętych rekordów z bik_node!
-- wydaje się, że trigger nie zadziałał...
-------------------------
-- do naprawy skryptem - usunięcie, plus przejrzenie kodu triggera
-- (kod triggera jednak wydaje się OK i działa OK)!
-------------------------
-------------------------
-- wpisy w bik_name_node_chunked dla usuniętych węzłów z bik_node
select 
  (select code from bik_tree where id = n.tree_id) as tree_code,
  (select code from bik_node_kind where id = n.node_kind_id) as node_kind_code,
  nnc.*, n.name, n.chunked_ver
--into tmp_bad_chunking
from bik_node_name_chunk nnc inner join bik_node n on nnc.node_id = n.id
where n.is_deleted = 1
order by tree_code, node_kind_code
go

--select * from tmp_bad_chunking


-- 14. poprawka w alter db for v1.1.9.26 pm.sql, fragment oznaczony [TSB-FIX:14]
-------------------------
-------------------------
-------------------------
-- użytkownicy dodani w roli, ale brak przypisanych obiektów do nich (w tej roli)
declare @user_roles_tree_id int = (select id from bik_tree where code = 'UserRoles')
declare @user_node_kind_id int = (select id from bik_node_kind where code = 'User')

select 
n.id, n.name as user_node_name, rn.name as role_node_name, count(u.id) as obj_cnt
--rn.name , n.*, *
from bik_node n inner join bik_node rn on n.parent_node_id = rn.id
left join (bik_user u inner join
bik_user_in_node uin on u.id = uin.user_id inner join 
bik_role_for_node rfn on uin.role_for_node_id = rfn.id
) on u.node_id = n.linked_node_id and n.parent_node_id = rfn.node_id
where n.linked_node_id is not null and n.is_deleted = 0
and n.node_kind_id = @user_node_kind_id
and u.id is null
and n.tree_id = @user_roles_tree_id
group by n.id, n.name, rn.name
order by count(u.id), n.name
go


-- 15. poprawka w alter db for v1.1.9.26 pm.sql, fragment oznaczony [TSB-FIX:15]
-------------------------
-------------------------
-------------------------
-- użytkownik powiązany do obiektu w roli, ale brak
-- tego użytkownika podłączonego pod rolę
select * 
from
bik_user u inner join
bik_node orgn on orgn.id = u.node_id inner join
bik_user_in_node uin on u.id = uin.user_id inner join 
bik_node objn on uin.node_id = objn.id inner join
bik_role_for_node rfn on uin.role_for_node_id = rfn.id inner join
bik_node roln on roln.id = rfn.node_id left join
bik_node un on un.linked_node_id = u.node_id and un.parent_node_id = rfn.node_id
  and un.is_deleted = 0
where orgn.is_deleted = 0 and objn.is_deleted = 0 and roln.is_deleted = 0 and un.id is null
go


-- 16.
-------------------------
-------------------------
-------------------------
-- user_in_node linkuje do podlinkowanego obiektu
select * from bik_user_in_node uin inner join bik_node n on uin.node_id = n.id
where n.linked_node_id is not null
go


-- 17.
-------------------------
-------------------------
-------------------------
-- użytkownik (bik_user) linkuje do podlinkowanego użytkownika lub wcale nie 
-- użytkownika (inny typ węzła)
declare @user_node_kind_id int = (select id from bik_node_kind where code = 'User')

select * from bik_user u inner join bik_node n on u.node_id = n.id
where n.linked_node_id is not null or n.node_kind_id <> @user_node_kind_id
go


-- 18.
-------------------------
-------------------------
-------------------------
-- w joined_objs są powiązania pomiędzy podlinkowanymi obiektami, a nie
-- oryginałami
select *
from bik_joined_objs j inner join bik_node ns on j.src_node_id = ns.id
inner join bik_node nd on j.dst_node_id = nd.id
where ns.linked_node_id is not null and ns.is_deleted = 0 or nd.linked_node_id is not null and nd.is_deleted = 0
go


-- 19.
-------------------------
-------------------------
-------------------------
-- użytkownik powiązany do obiektu w roli i podłączony pod rolę, ale z dziwnym typem
-- (node_kind inny niż User) albo w złym drzwie (powinno to być drzewo UserRoles)
declare @user_roles_tree_id int = (select id from bik_tree where code = 'UserRoles')
declare @user_node_kind_id int = (select id from bik_node_kind where code = 'User')

select * 
from
bik_user u inner join
bik_user_in_node uin on u.id = uin.user_id inner join 
bik_role_for_node rfn on uin.role_for_node_id = rfn.id left join
bik_node n on n.linked_node_id = u.node_id and n.parent_node_id = rfn.node_id
  and n.is_deleted = 0
where
  n.node_kind_id <> @user_node_kind_id or n.tree_id <> @user_roles_tree_id
go


-- 20. poprawione w alter db for v1.1.9.26 pm.sql oznaczenie [TSB-FIX:20]
-------------------------
-- role są w drzewie, ale brakuje ich w bik_role_for_node
-------------------------
declare @user_roles_tree_id int = (select id from bik_tree where code = 'UserRoles')
declare @role_node_kind_id int = (select id from bik_node_kind where code = 'UserRole')

select * 
from bik_node n left join bik_role_for_node rfn on rfn.node_id = n.id
where n.tree_id = @user_roles_tree_id
and n.is_deleted = 0 and n.node_kind_id = @role_node_kind_id
and rfn.id is null
go


-- 21. poprawione w alter db for v1.1.9.27 tf.sql oznaczenie [TSB-FIX:21]
-------------------------
-- atrybuty są usunięte, ale nie są usunięte ich powiązania do node_kindów
-------------------------    
select * from bik_attr_def def
inner join bik_attribute atr on atr.attr_def_id=def.id
where def.is_deleted = 1 and atr.is_deleted = 0
go
  

-- 22. poprawione w alter db for v1.1.9.27 tf.sql oznaczenie [TSB-FIX:22]
-------------------------
-- przypisane atrybuty są usunięte, a nie jest usunięte dowiązanie ich do konkretnego node'a
-------------------------
select * from bik_attribute atr
inner join bik_attribute_linked lin on lin.attribute_id=atr.id
where atr.is_deleted = 1 and lin.is_deleted = 0
go


-- 23. poprawione w alter db for v1.1.9.27 mm.sql oznaczenie [TSB-FIX:23]
-------------------------
-- błędne przypisane obj_id do roli
-------------------------
select * 
from bik_node bn join bik_role_for_node rfn on bn.id = rfn.node_id
where bn.obj_id <> rfn.id
go


-- 24. poprawione w alter db for v1.1.9.29 mm.sql oznaczenie [TSB-FIX:24]
-------------------------
-- błędne przypisane obj_id do roli
-------------------------
declare @tree_id int =  (select id from bik_tree where code = 'UserRoles')
declare @node_kind int = (select id from bik_node_kind where code = 'User')
declare @tab table (role_node_id int, user_node_id int, max_linked_node_id int )

select  br.name, br.id, bu.name, bu.id
	from bik_node bn
	join bik_node br on bn.parent_node_id = br.id
	join bik_node bu on bn.linked_node_id = bu.id
	where br.is_deleted = 0 and 
	bu.is_deleted = 0 and bn.is_deleted = 0
	and br.tree_id = @tree_id
	and bu.node_kind_id = @node_kind
	group by  br.name, br.id, bu.name, bu.id 
	having count(*)>1 
go

