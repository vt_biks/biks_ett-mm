/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.db.setdbperformance;

import commonlib.GlobalThreadStopWatch;

/**
 *
 * @author pmielanczuk
 */
public class Scenario implements Runnable {

    Step[] steps;

    public Scenario(Step... steps) {
        this.steps = steps;
    }

    public void run() {
        int cnt = steps.length;
        GlobalThreadStopWatch.startProcess("scenario start, steps: " + cnt, true);
        try {
            int stepNum = 0;
            for (Step s : steps) {
                GlobalThreadStopWatch.startProcessStage("step #" + stepNum);
                s.run();
                stepNum++;
            }
        } finally {
            GlobalThreadStopWatch.endProcess();
        }
    }
}
