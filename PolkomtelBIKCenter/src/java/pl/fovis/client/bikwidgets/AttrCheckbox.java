/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import simplelib.BaseUtils;

/**
 *
 * @author bfechner
 */
public class AttrCheckbox extends AttrWigetBase {

    protected List<CheckBox> cb = new ArrayList<CheckBox>();

    protected String atrLinValue;
    protected List<String> attrOpt;
    protected boolean isEditAttr;
    protected int height = 0;

    public AttrCheckbox(String atrLinValue, List<String> attrOpt, boolean isEditAttr) {

        this.atrLinValue = atrLinValue;
        this.attrOpt = attrOpt;
        this.isEditAttr = isEditAttr;
    }

    @Override
    public Widget getWidget() {
        cb.clear();
        VerticalPanel vp = new VerticalPanel();
        ScrollPanel scollPanel = createScrolledPanel(vp, attrOpt.size());
        List<String> attrOpt2 = BaseUtils.splitBySep(atrLinValue, ",", true);
        for (String ao : attrOpt) {
            CheckBox checkBox = new CheckBox(ao);
            vp.add(checkBox);
            if (isEditAttr && attrOpt2.contains(ao)) {
                checkBox.setValue(true);
            }
            cb.add(checkBox);
            height = height + 19;
        }
        height = height + 12;
        return scollPanel;
    }

    @Override
    public String getValue() {
        //   attrDescription = "";
        List<String> valueOpt = new ArrayList<String>();
        for (CheckBox c : cb) {
            if (c.getValue()) {
                valueOpt.add(c.getText());
            }
        }
        return BaseUtils.mergeWithSepEx(valueOpt, ", ");

        //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getWidgetHeight() {
        return height;
    }
}
