/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import pl.fovis.client.BIKGWTConstants;

/**
 *
 * @author tflorczak
 */
public class ConfluenceRoleWidgetFlat extends UserInNodeWidgetFlatBase {

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_CONFLUENCE;
    }

    public String getCaption() {
        return BIKGWTConstants.MENU_NAME_CONFLUENCE;
    }
}
