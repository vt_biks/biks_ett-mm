/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.io.Serializable;

/**
 *
 * @author wezyr
 */
public class SingleValue<T> implements Serializable {

    public T v;

    public SingleValue() {
        // no-op
    }

    public SingleValue(T v) {
        this.v = v;
    }

    @Override
    public String toString() {
        return "(" + v + ")";
    }

    @Override
    public int hashCode() {
        return v == null ? 0 : v.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof SingleValue) {
            SingleValue single2 = (SingleValue) obj;
            return BaseUtils.safeEquals(v, single2.v);
        } else {
            return false;
        }
    }
}
