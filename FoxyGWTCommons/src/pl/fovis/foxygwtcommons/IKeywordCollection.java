/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import java.util.Set;

/**
 *
 * @author tflorczak
 */
public interface IKeywordCollection {

    public Set<String> getKeywords();

    public void addKeyword(String tag);

    public boolean getType();
}
