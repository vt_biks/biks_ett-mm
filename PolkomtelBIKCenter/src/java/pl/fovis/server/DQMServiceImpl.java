/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import commonlib.UrlMakingUtils;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import pl.bssg.gwtservlet.AdhocSqlsName;
import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import pl.fovis.client.DQMService;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.ConnectionParametersDQMBean;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.fovis.common.ConnectionParametersProfileBean;
import pl.fovis.common.NoteBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.dqm.DQMDataErrorIssueBean;
import pl.fovis.common.dqm.DQMEvaluationBean;
import pl.fovis.common.dqm.DQMLogBean;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.fovis.common.dqm.DQMScheduleBean;
import pl.fovis.common.dqm.DataQualityRequestBean;
import pl.fovis.common.dqm.DataQualityTestBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.server.dqm.DQMDefProfileBean;
import pl.fovis.server.dqm.DQMErrorBean;
import pl.fovis.server.dqm.DQMTestBean;
import pl.fovis.server.dqm.DQMTestManager;
import pl.fovis.server.dqm.IBIKSDQMSubservice;
import pl.trzy0.foxy.commons.BeanConnectionDAOBase;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.CSVReader;
import simplelib.CSVWriter;
import simplelib.FieldNameConversion;
import simplelib.IContinuation;
import simplelib.ILameCollector;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
@AdhocSqlsName("adhocSqlsDQM.xml")
public class DQMServiceImpl extends BIKSServiceBaseForSingleBIKS implements DQMService, IBIKSDQMSubservice {

    protected static final int BATCH_SIZE = 1000;
    protected BOXIServiceImpl boxiService;
    //ww:dqmVsMltx to już jest w nadklasie (takie pole)
    //protected String dirForUpload;
    //ww:dqmVsMltx ze względu na wielobazowość (MultiX) nie można tego tak zrobić
    //private DQMTestManager dqmTestExecutor;

    public DQMServiceImpl(BOXIServiceImpl boxiService) {
        this.boxiService = boxiService;
    }

    @Override
    protected void initWithConfig(BIKCenterConfigObj configBean) {
        super.initWithConfig(configBean);
        //ww:dqmVsMltx to już jest w nadklasie (takie pole)
        //this.dirForUpload = configBean.dirForUpload;
        //ww:dqmVsMltx po co taka inicjalizacja tutaj? co ma to dać?
        //getDQMTestExecutor();

        initDQMExecutor();
//        List<DQMDefProfileBean> data = readDataFromDef3000TRFile("150413003103.txt");
//        saveDataDefProfileToDB(data, "150413003103.txt");
//
//        List<DQMDefProfileBean> data2 = readDataFromProfileFile("zsorb_20150413.txt");
//        saveDataDefProfileToDB(data2, "zsorb_20150413.txt");
//        System.out.println("Sprawdzam!!!!");
    }

    @PerformOnDefaultDb
    protected void initDQMExecutor() {

        if (isMultiXMode()) {
            return;
        }

        String activeConnectors = execNamedQuerySingleVal("getActiveConnectors", false, "val");
        boolean isDQMActive = BaseUtils.splitBySep(activeConnectors, ",").contains("DQM");
        //ww:dqmVsMltx tak nie można, bo nie może być jeden globalny dqmTestExecutor
        // w przypadku MultiXa (różne bazy)
//        if (dqmTestExecutor == null && isDQMActive) {
//            dqmTestExecutor = new DQMTestManager(this, dirForUpload);
//            dqmTestExecutor.setScheduler(getDQMSchedules());
//            new Thread(dqmTestExecutor).start();
//        }
        BOXIServiceImplData serviceImplData = getBOXIServiceImplDataForCurrentRequest();
        if (serviceImplData.dqmTestExecutor == null && isDQMActive) {
            serviceImplData.dqmTestExecutor = new DQMTestManager(this, dirForUpload);
            serviceImplData.dqmTestExecutor.setScheduler(getDQMSchedules());
            new Thread(serviceImplData.dqmTestExecutor).start();
        }
    }

    @PerformOnSingleBiksDb
    protected synchronized DQMTestManager getDQMTestExecutor() {
        //ww:dqmVsMltx tak nie można, bo nie może być jeden globalny dqmTestExecutor
        // w przypadku MultiXa (różne bazy)
//        if (dqmTestExecutor == null) {
//            initDQMExecutor();
//        }
//        return dqmTestExecutor;
//        BOXIServiceImplData serviceImplData = getBOXIServiceImplDataForCurrentRequest();
//        if (serviceImplData.dqmTestExecutor == null) {
//            initDQMExecutor();
//        }
//        return serviceImplData.dqmTestExecutor;

        return getBOXIServiceImplDataForCurrentRequest().dqmTestExecutor;
    }

    @Override
    public int addDQMTest(DataQualityTestBean bean, int parentNodeId/*, boolean isAddedToAllTestFolder*/) {
        Integer nodeId = execNamedQuerySingleValAsInteger("createDQMTest", false, "id", bean.name, parentNodeId);
        execNamedCommand("node_init_branch_id_node", nodeId);
        boxiService.updateNodeForFts(nodeId, bean.name, null);
        bean.testNodeId = nodeId;
        execNamedCommand("insertDQMTestExtradata", bean);
//        boxiService.addOrUpdateSpidHistoryForCurrentUser();
        execNamedCommand("updateFTSTestExtradata", nodeId);
        return nodeId;
    }

    @Override
    public void editDQMTest(DataQualityTestBean bean) {
        execNamedCommand("updateNodeName", bean.testNodeId, bean.name);
        boxiService.updateNodeForFts(bean.testNodeId, bean.name, null);
        execNamedCommand("editDQMTest", bean.testNodeId, bean);
//        boxiService.addOrUpdateSpidHistoryForCurrentUser();
        execNamedCommand("updateFTSTestExtradata", bean.testNodeId);
        // reharmonogramowanie
        setDQMExecutorScheduler();
    }

    @Override
    public List<String> deleteDQMTest(TreeNodeBean node) {
        List<String> res = boxiService.deleteTreeNode(node);
        execNamedCommand("deleteDQMTest", node.id);
        return res;
    }

    @Override
    public int assignDQMTestToGroup(int testId, int groupId, String name) {
        int id = execNamedQuerySingleValAsInteger("assignDQMTestToGroup", false, "id", testId, groupId);
        execNamedCommand("node_init_branch_id_node", id);
        boxiService.updateNodeForFts(id, name, null);
        return id;
    }

    @Override
    public void changeDQMTestActivity(int nodeId, boolean deactivateTest) {
        if (deactivateTest) { // deactive test
            execNamedCommand("deactivateDQMTest", nodeId);
        } else { // activate test
            execNamedCommand("activateDQMTest", nodeId);
        }
    }

    @Override
    public Set<Integer> getAlreadyAssignedDQMTest(int nodeId) {
        return execNamedQuerySingleColAsSet("getAlreadyAssignedDQMTest", null, nodeId);
    }

    @Override
    public Pair<Set<String>, Map<Integer, ConnectionParametersDQMConnectionsBean>> getDQMTestsNameAndOptConnections() {
        Set<String> tests = execNamedQuerySingleColAsSet("getDQMTestsName", "name");
        Map<Integer, ConnectionParametersDQMConnectionsBean> connectionsMap = null;
        if (BaseUtils.tryParseBoolean(getAppPropValueEx("useDqmConnectionMode", true), false)) {
            List<ConnectionParametersDQMConnectionsBean> connections = createBeansFromNamedQry("getDQMConnectionParameters", ConnectionParametersDQMConnectionsBean.class);
            connectionsMap = BaseUtils.makeBeanMap(connections);
        }
        return new Pair<Set<String>, Map<Integer, ConnectionParametersDQMConnectionsBean>>(tests, connectionsMap);
    }

    @Override
    public List<DataQualityTestBean> getDQMTests() {
        return createBeansFromNamedQry("getDQMTests", DataQualityTestBean.class);
    }

    @Override
    public DQMTestBean getDQMConfigForTest(int testId) {
        return createBeanFromNamedQry("getDQMConfigForTest", DQMTestBean.class, false, testId);
    }

    @Override
    public Pair<Integer, List<DQMLogBean>> getDQMLogs(int pageNum, int pageSize) {
        Integer rowCnt = execNamedQuerySingleValAsInteger("getDQMLogsCnt", true, null);
        if (rowCnt == null) {
            rowCnt = 0;
        }
        return new Pair<Integer, List<DQMLogBean>>(rowCnt, createBeansFromNamedQry("getDQMLogs", DQMLogBean.class,
                pageNum * pageSize + 1,
                (pageNum + 1) * pageSize
        ));
    }

    @Override
    public long insertDQMLog(int testId, Integer multiSourceId) {
        return (Long) execNamedQuerySingleVal("insertDQMLog", false, "id", testId, I18n.rozpoczynamTest.get(), multiSourceId);
    }

    @Override
    public void updateDQMLog(long logId, String descr, boolean isEnd) {
        execNamedCommand("updateDQMLog", logId, descr, isEnd);
    }

    @Override
    public long insertDQMRequest(DataQualityRequestBean bean) {
        return (Long) execNamedQuerySingleVal("insertDQMRequest", false, "id", bean);
    }

    @Override
    public void updateDQMRequest(DataQualityRequestBean bean) {
        execNamedCommand("updateDQMRequest", bean);
        execNamedCommand("updateDQMTest", bean.testId, bean.passed);
    }

    @Override
    public void runDQMTest(int testId) {
        getDQMTestExecutor().addTestToJobExecutorQueue(testId, null, true);
    }

    @Override
    public void deleteDQMRequest(long requestId) {
        execNamedCommand("deleteDQMRequest", requestId);
    }

    @Override
    public List<TreeNodeBean> getDQMTestsTreeNodes(Integer optUpSelectedNodeId) {
        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getDQMRootTreeNodes", TreeNodeBean.class, optUpSelectedNodeId);
        boxiService.translateSpecialTreeNodeBeans(tnb, false);
        return tnb;
    }

    @Override
    public List<TreeNodeBean> getBikAllRootTreeNodesForTreeCodes(Set<String> treeCodes) {
        setOptFilteringNodeActionCodeForCurrentRequestAddJoinedObjs();
        List<TreeNodeBean> tnb = createBeansFromNamedQry("getBikAllRootTreeNodesForTreeCodes", TreeNodeBean.class, treeCodes);
        boxiService.translateSpecialTreeNodeBeans(tnb, false);
        return tnb;
    }

    @Override
    public ConnectionParametersDBServerBean getDBParametersForId(Integer serverId, Integer connectionId) {
        if (connectionId == null) {
            return boxiService.getDBParametersForId(serverId);
        } else {
            return boxiService.getOneDQMConnectionParameters(connectionId);
        }
    }

    @Override
    public MssqlConnectionConfig getBIKSDBParameters() {
        return boxiService.getMssqlConnLoadCfg();
    }

    @Override
    public void execInAutoCommitMode(IContinuation whatToExec) {
        getAdhocDao().execInAutoCommitMode(whatToExec);
    }

    @Override
    public void insertErrorResults(List<DQMErrorBean> list) {
        execNamedCommand("insertErrorResults", list);
    }

    public void getDQMRequestErrors(long requestId, final ILameCollector<Map<String, Object>> collector) {
//        List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();
//        List<DQMErrorBean> rowsForRequest = createBeansFromNamedQry("getDQMRequestErrors", DQMErrorBean.class, requestId);
        getAdhocDao().execNamedQueryExCFN("getDQMRequestErrors", new IParametrizedContinuation<Iterator<Map<String, Object>>>() {

            @Override
            public void doIt(Iterator<Map<String, Object>> iter) {

                int actualRowNumber = -1;
                Map<String, Object> map = null;

                while (iter.hasNext()) {
                    Map<String, Object> dataRow = iter.next();

                    int row = sqlNumberInRowToInt(dataRow, "row");

                    if (map == null || actualRowNumber != row) {
                        if (map != null) {
                            collector.add(map);
                        }
                        map = new LinkedHashMap<String, Object>();
                        actualRowNumber = row;
                    }

                    String columnName = (String) dataRow.get("column_name");
                    String value = (String) dataRow.get("value");

                    map.put(columnName, value);
                }

                if (map != null) {
                    collector.add(map);
                }
            }
        }, FieldNameConversion.ToLowerCase, requestId);
    }

    public List<Map<String, Object>> getDQMRequestErrors(long requestId) {

        final List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();

        getDQMRequestErrors(requestId, new ILameCollector<Map<String, Object>>() {

            @Override
            public void add(Map<String, Object> item) {
                returnList.add(item);
            }
        });

//        List<DQMErrorBean> rowsForRequest = createBeansFromNamedQry("getDQMRequestErrors", DQMErrorBean.class, requestId);
//        int actualRowNumber = -1;
//        for (DQMErrorBean oneBean : rowsForRequest) {
//            if (actualRowNumber != oneBean.row) {
//                returnList.add(new LinkedHashMap<String, Object>());
//            }
//            Map<String, Object> map = returnList.get(oneBean.row);
//            map.put(oneBean.columnName, oneBean.value);
//            actualRowNumber = oneBean.row;
//        }
        return returnList;
    }

    @Override
    public ConnectionParametersProfileBean getProfileConnectionParameters() {
        return boxiService.readAdminBean("profile", ConnectionParametersProfileBean.class);
    }

    @Override
    public ConnectionParametersDQMBean getDQMConnectionParameters() {
        return boxiService.readAdminBean("dqm", ConnectionParametersDQMBean.class);
    }

    @Override
    public void setDQMConnectionParameters(ConnectionParametersDQMBean bean) {
        boxiService.setAdminBean(bean, "dqm");
        boxiService.setVisibleTree(BIKConstants.TREE_CODE_DQM, bean.isActive);
    }

    @Override
    public void deleteOldErrorData(int testId) {
        execNamedCommand("deleteOldErrorData", testId);
    }

    @Override
    public Pair<Integer, List<DQMScheduleBean>> getDQMSchedules(int pageNum, int pageSize) {
        Integer rowCnt = execNamedQuerySingleValAsInteger("getDQMScheduleCnt", true, null);
        if (rowCnt == null) {
            rowCnt = 0;
        }
        return new Pair<Integer, List<DQMScheduleBean>>(rowCnt, createBeansFromNamedQry("getDQMSchedules", DQMScheduleBean.class,
                pageNum * pageSize + 1,
                (pageNum + 1) * pageSize
        ));
    }

    public List<DQMScheduleBean> getDQMSchedules() {
        return getDQMSchedules(0, 1000).v2;
    }

    @Override
    public void setDQMSchedules(List<DQMScheduleBean> schedules) {
        for (DQMScheduleBean schedule : schedules) {
            execNamedCommand("setDQMSchedule", schedule);
        }
        setDQMExecutorScheduler();
    }

    protected void setDQMExecutorScheduler() {
        getDQMTestExecutor().setScheduler(getDQMSchedules());
    }

    @Override
    public boolean useFunctionInsteadSQLQuery() {
        String appPropValue = boxiService.getAppPropValue("dqmRunTestAsFunction");
        return BaseUtils.tryParseBoolean(appPropValue);
    }

    @Override
    public List<DataQualityRequestBean> getRequestsStartDateForTest(int testId) {
        return createBeansFromNamedQry("getRequestsStartDateForTest", DataQualityRequestBean.class, testId);
    }

    public DataQualityRequestBean getDqmTestRequest(long requestId) {
        return createBeanFromNamedQry("getDqmTestRequest", DataQualityRequestBean.class, false, requestId);
    }

    @Override
    public List<DQMMultiSourceBean> getDQMTestMultiSources(int nodeId) {
        return createBeansFromNamedQry("getDQMTestMultiSources", DQMMultiSourceBean.class, nodeId, null);
    }

    @Override
    public DQMMultiSourceBean getDQMTestMultiSource(int multiSourceId) {
        return createBeanFromNamedQry("getDQMTestMultiSources", DQMMultiSourceBean.class, true, null, multiSourceId);
    }

    @Override
    public String getSensitiveAppPropValue(String propName) {
        return boxiService.getSensitiveAppPropValue(propName);
    }

    @Override
    public IBeanConnectionEx<Object> getConnection() {
        @SuppressWarnings("unchecked")
        IBeanConnectionEx<Object> res = ((BeanConnectionDAOBase) getAdhocDao()).getConnection();
        return res;
    }

    @Override
    public boolean checkMultiSourceIsSuccessForTest(int testId) {
        List<String> results = execNamedQuerySingleColAsList("checkMultiSourceIsSuccessForTest", "result", testId);
        for (String result : results) {
            if (!BaseUtils.tryParseBoolean(result, true)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void dropTempTables(int testId) {
        Set<String> tempTables = execNamedQuerySingleColAsSet("getDQMTempTableNames", null, testId);
        execNamedCommand("dropDQMTempTables", tempTables);
    }

    @Override
    public void fixOneTimeSchedules(int testId) {
        execNamedCommand("fixOneTimeSchedules", testId);
    }

    @Override
    public void fixOneTimeOneMultiSourceSchedule(int testId, Integer multiSourceId) {
        execNamedCommand("fixOneTimeOneMultiSourceSchedule", testId, multiSourceId);
    }

    @Override
    public void setScheduleStatusFlag(int testId, Integer multiSourceId) {
        execNamedCommand("setScheduleStatusFlagStarted", testId, multiSourceId);
    }

    @Override
    public void updateRequestDataReadSecs(long logId, int dataReadSecs) {
        execNamedCommand("updateRequestDataReadSecs", logId, dataReadSecs);
    }

    @Override
    public DQMEvaluationBean
            getEvaluationPreInfo(int nodeId) {
        return createBeanFromNamedQry("getEvaluationPreInfo", DQMEvaluationBean.class, true, nodeId);
    }

    @Override
    public List<DQMEvaluationBean> getEvaluationPeriods(int nodeId) {
        return createBeansFromNamedQry("getEvaluationPeriods", DQMEvaluationBean.class, nodeId);
    }

    @Override
    public Integer saveEvaluation(DQMEvaluationBean bean) {
        if (bean.id == 0) { // add
            Integer nodeId = execNamedQuerySingleValAsInteger("addDQMEvaluation", false, "id", bean);
            boxiService.updateNodeForFts(nodeId, bean.name, null);
            return nodeId;
        } else { // edit
            execNamedCommand("editDQMEvaluation", bean);
            boxiService.updateNodeForFts(bean.nodeId, bean.name, null);
            return bean.nodeId;
        }
    }

    @Override
    public void setDQMEvaluationStatus(int nodeId, NoteBean comment) {
        boolean isAccepted = comment == null;
        if (isAccepted) {
            execNamedCommand("setDQMEvaluationStatus", nodeId, 1, BIKConstants.NODE_KIND_DQM_RATE_ACCEPTED);
            // usunięcie z folderu "Do zaakceptowania"
            execNamedCommand("deleteDQMacceptedEvaluation", nodeId);
        } else {
            execNamedCommand("setDQMEvaluationStatus", nodeId, 0, BIKConstants.NODE_KIND_DQM_RATE_REJECTED);
            // dodawanie komentarza w przypadku odrzucenia
            boxiService.addBikEntityNote(comment.title, comment.body, nodeId);
        }
    }

    @Override
    public Integer generateDQMReport(int parentNodeId, Date fromDate, Date toDate) {
        String reportName = SimpleDateUtils.dateToString(new Date()) + " " + I18n.raportJakosciDanych.get();
        Integer nodeId = execNamedQuerySingleValAsInteger("generateDQMReport", false, "id", parentNodeId, reportName, fromDate, toDate);
        boxiService.updateNodeForFts(nodeId, reportName, null);
        return nodeId;
    }

    @Override
    public DQMDataErrorIssueBean getDQMDataErrorIssuePreInfo(int nodeId) {
        //opiekun, nazwa grupy danych
        DQMDataErrorIssueBean bean = createBeanFromNamedQry("getDQMDataErrorIssuePreInfo", DQMDataErrorIssueBean.class, false, nodeId);
        bean.createdDate = new Date();
        bean.name = SimpleDateUtils.dateTimeToString(bean.createdDate, "-", false) + " " + I18n.zgloszenieBleduDanych.get() + "-" + bean.groupName;
        bean.errorStatus = 0;
        return bean;
    }

    @Override
    public Integer saveDataErrorIssue(DQMDataErrorIssueBean bean) {
        if (bean.id == 0) { // add
            Integer nodeId = execNamedQuerySingleValAsInteger("addDQMDataErrorIssue", false, "id", bean);
            boxiService.updateNodeForFts(nodeId, bean.name, null);
            return nodeId;
        } else { // edit
            execNamedCommand("editDQMDataErrorIssue", bean);
            boxiService.updateNodeForFts(bean.nodeId, bean.name, null);
            return bean.nodeId;
        }
    }

    @Override
    public DQMDataErrorIssueBean getDQMDataErrorIssueByNodeId(int nodeId) {
        return createBeanFromNamedQry("getDQMDataErrorIssue", DQMDataErrorIssueBean.class, false, nodeId, true);
    }

    @Override
    public void refreshDQMReport(int nodeId) {
        execNamedCommand("refreshDQMReport", nodeId);
    }

    @SuppressWarnings("unchecked")
    public Pair<String, List<Map<String, Object>>> getDQMDataToExport(int nodeId) {
        Map<String, Object> result = execNamedQuerySingleRowCFN("getDQMTypeToExport", false, FieldNameConversion.ToLowerCase, nodeId);
        String typeToExport = (String) result.get("code");
        String exportName = (String) result.get("name");
        if (BIKConstants.NODE_KIND_DQM_REPORT.equals(typeToExport)) { // raport
            return new Pair<String, List<Map<String, Object>>>(exportName, (List) execNamedQueryCFN("getDQMReportEvaluation", null, nodeId, false));
        } else if (BIKConstants.NODE_KIND_DQM_ERROR_OPENED.equals(typeToExport) || BIKConstants.NODE_KIND_DQM_ERROR_CLOSED.equals(typeToExport)) {
            return new Pair<String, List<Map<String, Object>>>(exportName, (List) execNamedQueryCFN("getDQMDataErrorIssue", null, nodeId, false));
        } else if (BIKConstants.NODE_KIND_DQM_ERROR_REGISTER.equals(typeToExport)) {
            return new Pair<String, List<Map<String, Object>>>(exportName, (List) execNamedQueryCFN("getAllDQMDataErrorIssuesInNode", null, nodeId, false));
        } else {// ocena
            return new Pair<String, List<Map<String, Object>>>(exportName, (List) execNamedQueryCFN("getDQMEvaluation", null, nodeId, false));
        }
    }

    public void saveDataDefProfileToDB(List<DQMDefProfileBean> data, String fileName) {
        execNamedCommand("saveDataDefProfileToDB", data, fileName);
    }

    public List<DQMDefProfileBean> readDataFromProfileFile(String fileName) {
        List<DQMDefProfileBean> rows = new ArrayList<DQMDefProfileBean>();
        String csvContent = LameUtils.loadAsString(BaseUtils.ensureDirSepPostfix(dirForUpload) + fileName, "ISO-8859-2");
        CSVReader reader = new CSVReader(csvContent, CSVReader.CSVParseMode.CONSIDER_QUOTES, '\t', true);
        List<Map<String, String>> dataFromFile = reader.readFullTableAsMaps();
        for (Map<String, String> row : dataFromFile) {
            Date ztjd;
            try {
                ztjd = new SimpleDateFormat("MM/dd/yy", Locale.ENGLISH).parse(row.get("ZTJD"));
            } catch (ParseException ex) {
                ztjd = null;
            }
            rows.add(new DQMDefProfileBean(fileName, 0, ztjd, Double.valueOf(row.get("TAMT")), row.get("CUSTACC"), row.get("CUSTAD1"), row.get("CUSTAD2"), row.get("CUSTAD3"), row.get("CUSTAD4"), row.get("BENACC"), row.get("BENAD1"), row.get("BENAD2"), row.get("BENAD3"), row.get("BENAD4"), row.get("REF1"), row.get("REF2"), row.get("REF3"), row.get("REF4")));
        }
        return rows;
    }

    public List<DQMDefProfileBean> readDataFromDef3000TRFile(String fileName) {
        String csvContent = LameUtils.loadAsString(BaseUtils.ensureDirSepPostfix(dirForUpload) + fileName, "ISO-8859-2");
        List<String> trans = BaseUtils.splitBySep(csvContent, "-}", true);
//        int i = 1;
        List<DQMDefProfileBean> rows = new ArrayList<DQMDefProfileBean>();
        for (String tran : trans) {
            if (!BaseUtils.isStrEmptyOrWhiteSpace(tran)) {
                DQMDefProfileBean newbean = new DQMDefProfileBean();
                newbean.fileName = fileName;
                newbean.isDefFile = 1;
                int indexOf32A = tran.indexOf(":32A:");
                newbean.ztjd = SimpleDateUtils.newDate(Integer.valueOf("20" + tran.substring(indexOf32A + 5, indexOf32A + 7)), Integer.valueOf(tran.substring(indexOf32A + 7, indexOf32A + 9)), Integer.valueOf(tran.substring(indexOf32A + 9, indexOf32A + 11)));
                int indexOfNR = tran.indexOf("\r\n", indexOf32A);
                newbean.tamt = Double.valueOf(tran.substring(indexOf32A + 14, indexOfNR).replace(",", "."));
                int indexOf50K = tran.indexOf(":50K:", indexOf32A);
                int indexOfNR2 = tran.indexOf("\r\n", indexOf50K);
                newbean.custacc = tran.substring(indexOf50K + 8, indexOfNR2);
                int indexOf5 = tran.indexOf(":5", indexOfNR2);
                String custaddr = tran.substring(indexOfNR2 + 2, indexOf5);
                List<String> addrs = BaseUtils.splitBySep(custaddr, "\r\n");
                if (addrs != null && addrs.size() > 0) {
                    newbean.custad1 = addrs.get(0);
                }
                if (addrs != null && addrs.size() > 1) {
                    newbean.custad2 = addrs.get(1);
                }
                if (addrs != null && addrs.size() > 2) {
                    newbean.custad3 = addrs.get(2);
                }
                if (addrs != null && addrs.size() > 3) {
                    newbean.custad4 = addrs.get(3);
                }
                int indexOf59 = tran.indexOf(":59:", indexOfNR2);
                int indexOfNR4 = tran.indexOf("\r\n", indexOf59);
                newbean.benacc = tran.substring(indexOf59 + 7, indexOfNR4);
                int indexOfSomething = tran.indexOf("\r\n:", indexOfNR4);
                String benaddr = tran.substring(indexOfNR4 + 2, indexOfSomething);
                List<String> benaddrs = BaseUtils.splitBySep(benaddr, "\r\n");
                if (benaddrs != null && benaddrs.size() > 0) {
                    newbean.benad1 = benaddrs.get(0);
                }
                if (benaddrs != null && benaddrs.size() > 1) {
                    newbean.benad2 = benaddrs.get(1);
                }
                if (benaddrs != null && benaddrs.size() > 2) {
                    newbean.benad3 = benaddrs.get(2);
                }
                if (benaddrs != null && benaddrs.size() > 3) {
                    newbean.benad4 = benaddrs.get(3);
                }
                int indexOf70 = tran.indexOf(":70:", indexOfNR4);
                int indexOfSomething2 = tran.indexOf("\r\n:", indexOf70);
                String titl = tran.substring(indexOf70 + 4, indexOfSomething2);
                List<String> titles = BaseUtils.splitBySep(titl, "\r\n");
                if (titles != null && titles.size() > 0) {
                    newbean.ref1 = titles.get(0);
                }
                if (titles != null && titles.size() > 1) {
                    newbean.ref2 = titles.get(1);
                }
                if (titles != null && titles.size() > 2) {
                    newbean.ref3 = titles.get(2);
                }
                if (titles != null && titles.size() > 3) {
                    newbean.ref4 = titles.get(3);
                }
                rows.add(newbean);
            }
        }
        return rows;
    }

    @Override
    public Pair<String, String> prepareFileToDownload(String exportType, Long longRequestId) {
        String ext = "." + exportType;
//        Long longRequestId = Long.valueOf(requestId);
        List<Map<String, Object>> content = getDQMRequestErrors(longRequestId);
        DataQualityRequestBean dqmTestRequest = getDqmTestRequest(longRequestId);
        String encodedTestName = UrlMakingUtils.encodeStringForUrlWW(dqmTestRequest.testName).replace("+", "_"/*"%20"*/);
        String fileName = encodedTestName + "_" + SimpleDateUtils.dateTimeToStringNoSep(dqmTestRequest.startTimestamp) + ext;
        // create file
        BufferedOutputStream fileOut = null;
        String systemFileName = null;
        File file = null;
        try {
            File dir = new File(dirForUpload);
            file = File.createTempFile("file_DQM_", ext, dir);

            systemFileName = file.getName();
            fileOut = new BufferedOutputStream(new FileOutputStream(file));
            // write to file
            XlsxWriter writer = new XlsxWriter();
            writer.writeContentAndCloseWorkBook(content, fileOut);
        } catch (FileNotFoundException ex) {
            fileName = null;
        } catch (IOException ex) {
            fileName = null;
        } finally {
            try {
                if (fileOut != null) {
                    fileOut.close();
                }
//                if (file != null) {
//                    file.delete();
//                }
            } catch (IOException ex) {
                fileName = null;
            }
        }
        return new Pair<String, String>(fileName, systemFileName);
    }

    private List<String> extractColumnName(List<Map<String, Object>> content) {
        List<String> allColumnNames = new ArrayList<String>();
        for (Map<String, Object> row : content) {
            for (String colName : row.keySet()) {
                allColumnNames.add(colName);
            }
            break;
        }
        return allColumnNames;
    }

    @Override
    public Pair<String, String> saveErrorDataOnDisk(String tmpTableName) {
//        System.out.println("saveErrorDataAsXlsx");
//        String fileNameXlsx = saveErrorDataAsXlsx(content);
//        System.out.println("saveErrorDataAsCsv");
//        String fileNameCsv = saveErrorDataAsCsv(content);
//        return new Pair<String, String>(fileNameXlsx, fileNameCsv);
        BufferedOutputStream xlsxFileOut = null;
        String xlsxFileName = null;
        File xlsxFile = null;
        String csvFileName = null;
        BufferedOutputStream csvFileOut = null;
        List<String> allColumnNames = removeAutoIncColumn(tmpTableName, execNamedQuerySingleColAsList("getColumnNameOfTable", "column_name", tmpTableName));

        
        CSVWriter csvWritter = null;
        XlsxWriter xlsxWriter = new XlsxWriter(allColumnNames);
        File csvFile = null;
        try {
            File dir = new File(dirForUpload);
            xlsxFile = File.createTempFile("file_DQM_", ".xlsx", dir);
            xlsxFileName = xlsxFile.getName();
            csvFile = File.createTempFile("file_DQM_", ".csv", dir);
            csvFileName = csvFile.getName();
            xlsxFileOut = new BufferedOutputStream(new FileOutputStream(xlsxFile));
            csvFileOut = new BufferedOutputStream(new FileOutputStream(csvFile));
            // write to file
            int pageNum = 0;
            List<Map<String, Object>> content = null;
            do {
                content = getDataToSave2DiskFromTmpTable(tmpTableName, allColumnNames, pageNum);
                if (!BaseUtils.isCollectionEmpty(content)) {
                    if (csvWritter == null) {
                        csvWritter = new CSVWriter(allColumnNames, content);
                    } else {
                        csvWritter.setDataToSave(content);
                    }
                    String text = (pageNum == 0 ? '\uFEFF' : "") + csvWritter.getText();
                    csvFileOut.write(text.getBytes(StandardCharsets.UTF_8));

                    
                    xlsxWriter.writeContent(content,true);
                    pageNum++;
                }
            } while (!BaseUtils.isCollectionEmpty(content));
            xlsxWriter.closeWorkbook(xlsxFileOut);
        } catch (Exception ex) {
            System.out.println("Error in save error file: " + ex.getMessage());
        } finally {
            try {
                if (csvFileOut != null) {
                    csvFileOut.close();
                }
                if (xlsxFileOut != null) {
                    xlsxFileOut.close();
                }
            } catch (IOException ex) {
                System.out.println("Error in close file: " + ex.getMessage());
            }
        }
        return new Pair<String, String>(xlsxFileName, csvFileName);
    }

    public String saveErrorDataAsXlsx(List<Map<String, Object>> content) {
        BufferedOutputStream fileOut = null;
        String systemFileName = null;
        File file = null;
        try {
            File dir = new File(dirForUpload);
            file = File.createTempFile("file_DQM_", ".xlsx", dir);
            systemFileName = file.getName();
            fileOut = new BufferedOutputStream(new FileOutputStream(file));
            // write to file
            XlsxWriter writer = new XlsxWriter();
            writer.writeContentAndCloseWorkBook(content, fileOut);
        } catch (Exception ex) {
            System.out.println("Error in save error file: " + ex.getMessage());
        } finally {
            try {
                if (fileOut != null) {
                    fileOut.close();
                }
            } catch (IOException ex) {
                System.out.println("Error in close file: " + ex.getMessage());
            }
        }
        return systemFileName;
    }

    public String saveErrorDataAsCsv(List<Map<String, Object>> content) {
        List<String> allColumnNames = new ArrayList<String>();
        for (Map<String, Object> row : content) {
            for (String colName : row.keySet()) {
                allColumnNames.add(colName);
            }
            break;
        }
        BufferedOutputStream fileOut = null;
        String systemFileName = null;
        File file = null;
        try {
            File dir = new File(dirForUpload);
            file = File.createTempFile("file_DQM_", ".csv", dir);
            systemFileName = file.getName();
            fileOut = new BufferedOutputStream(new FileOutputStream(file));
            // write to file
            CSVWriter writter = new CSVWriter(allColumnNames, content);
            String text = '\uFEFF' + writter.getText();
            fileOut.write(text.getBytes(StandardCharsets.UTF_8));
        } catch (Exception ex) {
            System.out.println("Error in save error file: " + ex.getMessage());
        } finally {
            try {
                if (fileOut != null) {
                    fileOut.close();
                }
            } catch (IOException ex) {
                System.out.println("Error in close file: " + ex.getMessage());
            }
        }
        return systemFileName;
    }

    @Override
    public int countRecordNumber(String tmpTableName) {
        return execNamedQuerySingleValAsInteger("countRecordNumber", false, null, tmpTableName);
    }

    @Override
    public void dropTmpTable(String tmpTableName) {
        execNamedCommand("dropTmpTable", tmpTableName);
    }

    private List<Map<String, Object>> getDataToSave2DiskFromTmpTable(String tmpTableName, List<String> columnNames, int pageNum) {
        return getAdhocDao().execNamedQueryCFN("getDataToSave2DiskFromTmpTable", FieldNameConversion.ToLowerCase, tmpTableName, columnNames, "id_" + tmpTableName, pageNum * BATCH_SIZE, (pageNum + 1) * BATCH_SIZE + 1);
    }

    private List<String> removeAutoIncColumn(String tmpTableName, List<Object> columnNames) {
        List<String> res = new ArrayList<String>();
        String autoIncColumnName = "id_" + tmpTableName;
        for (Object name : columnNames) {
            if (autoIncColumnName.equalsIgnoreCase(String.valueOf(name))) {
                continue;
            }
            res.add(String.valueOf(name).toLowerCase());
        }
        return res;
    }
}
