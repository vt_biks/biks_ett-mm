/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.ListBox;
import java.util.Arrays;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.TreeSelectorForLinkedNodesDialog;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.foxygwtcommons.richtexteditor.FoxyRichTextArea;
import pl.fovis.foxygwtcommons.richtexteditor.RichTextAreaFoxyToolbar;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author młydkowski
 */
public class MyRichTextAreaFoxyToolbar extends RichTextAreaFoxyToolbar {

    //protected String url;
    protected TreeNodeBean treeNode;
//    protected Range actualRange;

    public MyRichTextAreaFoxyToolbar(FoxyRichTextArea bikRichText/*
     * , int id
     */) {
        super(bikRichText, Arrays.asList(BIKClientSingletons.allowedFileExtensionsList()));

        super.createObjLink.addClickHandler(new EventHandler());
        super.createObjLink.setVisible(true);

        this.treeNode = new TreeNodeBean();
        bikRichText.setSize(Window.getClientWidth() * 0.9 + "px", Window.getClientHeight() * 0.85 - 100 + "px");
//        treeNode.id = id;
        if (BIKClientSingletons.isDeveloperMode()) {
            showHtml.setVisible(true);
        }
    }

    public MyRichTextAreaFoxyToolbar(FoxyRichTextArea bikRichText, boolean showTableOfContent) {
        this(bikRichText);
        tableCaption.setVisible(showTableOfContent);
    }

    private class EventHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            selectLinkedObj(/*
                     * formatter
                     */);
        }
    }

    private void selectLinkedObj(/*
             * final ExtendedFormatter formatter
             */) {
        //actualRange = bikRichText.getRange();
        saveCurrentRange();
        Integer treeId = null;
        Integer nodeId = null;
        if (treeNode != null) {
            nodeId = treeNode.id;
            treeId = treeNode.treeId;
        }

        new TreeSelectorForLinkedNodesDialog().buildAndShowDialog(treeId, nodeId, new IParametrizedContinuation<TreeNodeBean>() { //TreeSelectorForLinkedNodesDialogInBody ?
            public void doIt(TreeNodeBean selected) {
                String url = null;

                if (selected.treeCode != null) {
                    url = "#" + selected.treeCode;
                    if (selected.id != 0) {
                        url += "/" + selected.id;
                    }
                }

                if (url != null) {
                    //bikRichText.getSelection().setRange(actualRange);
                    restoreSavedRange();
                    if (getSelectedText().isEmpty()) {
                        formatter.insertHTML("<" + "a href" /* I18N: no */ + "=\"" + url + "\">" + selected.name + "</a>");
                    } else {
                        formatter.createLink(url);
                    }
                }
            }
        }, null);
    }

//    /**
//     * Native JavaScript that returns the selected text and position of the
//     * start *
//     */
//    public static native JsArrayString getSelection(Element elem) /*
//     * -{ var txt = ""; var pos = 0; if (elem.contentWindow.getSelection) { txt
//     * = elem.contentWindow.getSelection(); pos =
//     * elem.contentWindow.getSelection().getRangeAt(0).startOffset; } else if
//     * (elem.contentWindow.document.getSelection) { txt =
//     * elem.contentWindow.document.getSelection(); pos =
//     * elem.contentWindow.document.getSelection().getRangeAt(0).startOffset; }
//     * else if (elem.contentWindow.document.selection) { txt =
//     * elem.contentWindow.document.selection.createRange().text; pos =
//     * elem.contentWindow.document.selection.getRangeAt(0).startOffset; } return
//     * [""+txt,""+pos];
//    }-
//     */;
    @Override
    protected void addAdditionalViewMode(ListBox lb) {
        if (BIKClientSingletons.useProgramerEditMode()) {
            super.addAdditionalViewMode(lb);
        } else {
            //Włączony tryb programisty
        }
    }
}
