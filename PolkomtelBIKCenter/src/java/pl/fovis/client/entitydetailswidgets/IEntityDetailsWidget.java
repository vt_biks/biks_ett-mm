/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.ComputedDetailsDataBean;
import pl.fovis.client.nodeactions.INodeActionContext;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.TreeBean;
import simplelib.IContinuation;

/**
 *
 * @author wezyr
 */
public interface IEntityDetailsWidget {

    public void setup(boolean widgetsActive, TreeBean treeBean);

    public abstract void dataFetched(final INodeActionContext optCtx, EntityDetailsDataBean data,
            ComputedDetailsDataBean cddb);

    public abstract Widget buildWidgets();

    public void displayData();

    public abstract void refreshWidgetsState();

    public boolean hasContent();

    // czy ten subwidget umożliwia edycję - tzn. jak jest nawet pusty, to
    // należy go pokazać, bo jest np. przycisk [dodaj nowy]
    // oczywiście kwestia kto może edytować - ale to już jest poza samym
    // subwidgetem, on tylko informuje czy umożliwia czy nie edycję kiedykolwiek
    public boolean hasEditableControls();
    //public boolean isEnabledDeprecated(String code, int count);

    // przekazanie do subwidgetu informacji o możliwym podlinkowanym nodzie
    public void setIsLinked(boolean isLinked);

    // w przypadku widgetów, które potrzebują pobrać dane (których nie ma w EntityDetailsDataBean)
    public void optCallServiceToGetData(IContinuation con);

    public void optSetCurrentLinkingParentId(Integer currentLinkingParentId);
}
