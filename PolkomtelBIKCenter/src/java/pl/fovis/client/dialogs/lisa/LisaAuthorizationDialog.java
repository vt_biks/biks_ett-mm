/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tbzowka
 */
public class LisaAuthorizationDialog extends BaseActionOrCancelDialog {

    protected String lisaName;
    public TextBox usernameTb;
    public PasswordTextBox passwordPTB;
    protected Label usernameLbl, passwordLbl;
    private IParametrizedContinuation<List<String>> saveCont;

    @Override
    protected String getDialogCaption() {
        return I18n.logowanieDoTeradaty.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return "OK" /* i18n: no */;
    }

    @Override
    protected void buildMainWidgets() {

        usernameLbl = new Label(I18n.login.get() /* I18N:  */);
        passwordLbl = new Label(I18n.haslo.get() /* I18N:  */);

        usernameTb = new TextBox();
        passwordPTB = new PasswordTextBox();

        int row = 0;
        Grid grid = new Grid(3, 2);
        grid.setWidget(row, 0, usernameLbl);
        grid.setWidget(row++, 1, usernameTb);
        grid.setWidget(row, 0, passwordLbl);
        grid.setWidget(row++, 1, passwordPTB);
        grid.setWidget(row, 0, new Label(I18n.instancja.get()));
        grid.setWidget(row++, 1, new HTML("<b>" + lisaName + "</b>"));
        grid.setCellSpacing(2);
        main.add(grid);
    }

    @Override
    protected void doAction() {

        String username = usernameTb.getValue();
        String password = passwordPTB.getValue();
        List<String> authorizationData = new ArrayList<String>();
        authorizationData.add(username);
        authorizationData.add(password);

        saveCont.doIt(authorizationData);

    }

    public void buildAndShowDialog(String lisaName, IParametrizedContinuation<List<String>> saveCont) {
        this.saveCont = saveCont;
        this.lisaName = lisaName;
        super.buildAndShowDialog();
    }
}
