exec sp_check_version '1.0.86.1';

go

/*
declare @table_name sysname = 'bik_app_prop', @column_name sysname = 'is_sensitive'
DECLARE @default_constraint_name sysname
SELECT @default_constraint_name = object_name(cdefault)
FROM syscolumns
WHERE id = object_id(@table_name)
AND name = @column_name
--select @default_constraint_name
EXEC ('ALTER TABLE ' + @table_name + ' DROP CONSTRAINT ' + @default_constraint_name)
EXEC ('ALTER TABLE ' + @table_name + ' DROP column ' + @column_name)
go

--alter table bik_app_prop drop column is_sensitive;
*/


-- -1 - sensitive (do not send to client)
--  0 -> is not editable on client, 
--  1 -> is editable on client
alter table bik_app_prop add is_editable int not null default 0; 
go

insert into bik_app_prop (name, val) values ('treeGridSpeed', '2');
go

-- select * from bik_app_prop

update bik_app_prop
set is_editable = 1
where name in ('developerMode', 'allowedShowDiagKinds', 'treeGridSpeed')
go

-- update bik_app_prop set val = 'true' where name = 'developerMode'


------------------------------------------------------------------

declare @table_name sysname = 'bik_node', @column_name sysname = 'disable_linked_subtree'
DECLARE @default_constraint_name sysname
SELECT @default_constraint_name = object_name(cdefault)
FROM syscolumns
WHERE id = object_id(@table_name)
AND name = @column_name
--select @default_constraint_name
if @default_constraint_name is not null
  EXEC ('ALTER TABLE ' + @table_name + ' DROP CONSTRAINT ' + @default_constraint_name)
if exists(select 1 from syscolumns sc where id = OBJECT_ID(@table_name) and name = @column_name)
  EXEC ('ALTER TABLE ' + @table_name + ' DROP column ' + @column_name)
go


alter table bik_node add disable_linked_subtree int not null default 0;

go



------------------------------------------------------------------

IF EXISTS (SELECT name FROM sysobjects
      WHERE name = 'trg_bik_node_chunking' AND type = 'TR')
   DROP TRIGGER trg_bik_node_chunking
GO

CREATE TRIGGER trg_bik_node_chunking
ON bik_node
FOR insert, update, delete AS
begin
    -- diag: print cast(sysdatetime() as varchar(23)) + ': start!'

set nocount on

declare @diags_level int = 0;

declare @to_delete table (id int not null primary key);

declare @bik_node_name_chunk table (node_id int not null, tree_id int not null, chunk_txt varchar(3) not null)

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before calc @to_delete'
    end
   
insert into @to_delete (id)    
select d.id from deleted d left join inserted i on d.id = i.id
where i.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 0 and i.is_deleted = 1)
   
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before delete by ids'
    end
   
delete from bik_node_name_chunk where node_id in (select id from @to_delete
--select d.id from deleted d left join inserted i on d.id = i.id
--where i.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 0 and i.is_deleted = 1)
);

declare @to_insert table (id int not null, name varchar(max) not null, tree_id int not null);

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before calc @to_insert'
    end
   
insert into @to_insert (id, name, tree_id)
select i.id, i.name, i.tree_id from inserted i left join deleted d on d.id = i.id
where d.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 1 and i.is_deleted = 0);

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before declare cursor'
    end
   
declare nodes cursor for select id, name, tree_id from @to_insert;
/*select id, name, tree_id from bik_node where id in (
select i.id from inserted i left join deleted d on d.id = i.id
where d.name is null or i.name <> d.name or (d.is_deleted = 1 and i.is_deleted = 0)
)*/;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before open cursor'
    end
   
open nodes;

declare @id int, @name varchar(8000), @tree_id int;

  if @diags_level > 0 begin
  -- diag:
    print cast(sysdatetime() as varchar(23)) + ': before first fetch'
  end
 
fetch next from nodes into @id, @name, @tree_id;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before loop'
    end
   
    declare @cnt int = 0;
   
while @@fetch_status = 0
begin
  insert into @bik_node_name_chunk (node_id, chunk_txt, tree_id)
  select distinct @id, chunk_str, @tree_id
  from dbo.fn_split_string(@name, 3);
 
    if @diags_level > 1 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': in loop after insert'
    end
   
  set @cnt = @cnt + 1;
 
  if (@cnt % 1000 = 0) begin
 
    if @diags_level > 1 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': in loop, before insert into bik_node_name_chunk, cnt:' + cast(@cnt as varchar(10))
    end
   
    insert into bik_node_name_chunk (node_id, chunk_txt, tree_id)
    select node_id, chunk_txt, tree_id
    from @bik_node_name_chunk
   
    delete from @bik_node_name_chunk
  end;   
 
  if @diags_level > 1 begin
     -- diag:
     print cast(sysdatetime() as varchar(23)) + ': in loop before fetch next'
  end
 
  fetch next from nodes into @id, @name, @tree_id;
  if @diags_level > 1 begin
    -- diag:
    print cast(sysdatetime() as varchar(23)) + ': in loop after fetch next'
  end
 
end;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': after loop, before insert into bik_node_name_chunk, @cnt=' + cast(@cnt as varchar(10))
    end
   
    insert into bik_node_name_chunk (node_id, chunk_txt, tree_id)
    select node_id, chunk_txt, tree_id
    from @bik_node_name_chunk
     
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': closing, deallocating cursor'
    end
   
close nodes;

deallocate nodes;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': done, stop!'
    end
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_update_version '1.0.86.1', '1.0.86.2';

go

