exec sp_check_version '1.6.z5.4';
go

update bik_tree set is_in_ranking = 1 where tree_kind = 'Confluence'

exec sp_update_version '1.6.z5.4', '1.6.z5.5';
go
