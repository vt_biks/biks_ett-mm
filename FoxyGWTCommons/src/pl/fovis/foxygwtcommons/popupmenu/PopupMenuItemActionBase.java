/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.popupmenu;

/**
 *
 * @author tflorczak
 * @param <CTX>
 */
public abstract class PopupMenuItemActionBase<CTX> implements IPopupMenuItemAction<CTX> {

    protected CTX contextObject;

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getButtonCss() {
        return null;
    }

    @Override
    public void setup(CTX contextObject) {
        this.contextObject = contextObject;
    }
}
