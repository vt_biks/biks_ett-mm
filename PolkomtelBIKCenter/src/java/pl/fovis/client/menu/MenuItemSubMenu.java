/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.menu;

import java.util.List;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public class MenuItemSubMenu extends MenuItemBase implements IMenuItemSubMenu {

    private List<IMenuItem> subItems;

    public MenuItemSubMenu(String caption, IMenuItem... subItems) {
        this(caption, BaseUtils.arrayToArrayList(subItems));
    }

    public MenuItemSubMenu(String title, List<IMenuItem> subItems) {
        super(title);
        this.subItems = subItems;
    }

    public List<IMenuItem> getSubItems() {
        return subItems;
    }
}
