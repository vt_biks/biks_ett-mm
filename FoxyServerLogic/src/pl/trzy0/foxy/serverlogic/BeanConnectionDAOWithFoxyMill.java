/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import java.util.Collection;
import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.ParametrizedTextProviderByFoxyMillResource;
import pl.trzy0.foxy.commons.BeanConnectionDAOBase;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import simplelib.IGenericResourceProvider;
import simplelib.IResourceFromProvider;

/**
 *
 * @author wezyr
 */
public class BeanConnectionDAOWithFoxyMill extends BeanConnectionDAOBase<Object> {

    public BeanConnectionDAOWithFoxyMill(IBeanConnectionEx<Object> connection,
            IGenericResourceProvider<String> sqlTextsResProvider,
            Collection<String> fileNames,
            IDaoAwareNamedSqlsModel<Object> optNamedSqlsModel,
            IServerSupportedLocaleNameRetriever optServerSupportedLocaleNameRetriever) {
//        super(connection, null);

        innerSetup(connection, new ParametrizedTextProviderByFoxyMillResource(sqlTextsResProvider,
                fileNames,
                FoxyMillUtils.makeRenderEnv(true, true, this),
                null), optNamedSqlsModel, optServerSupportedLocaleNameRetriever);

//        if (optNamedSqlsModel == null) {
//            optNamedSqlsModel = new FoxyMillDaoAwareModel<Object>();
//        }
//
//        ParametrizedTextProviderByFoxyMillResource ptpbfmr =
//                new ParametrizedTextProviderByFoxyMillResource(sqlTextsResProvider,
//                fileNames,
//                FoxyMillUtils.makeRenderEnv(true, true, this),
//                optNamedSqlsModel);
//
//        super.namedSqls = ptpbfmr;
//        //RenderEnvironment re = ;
//
//        optNamedSqlsModel.setDao(this);
//        optNamedSqlsModel.setGlobals(ptpbfmr.getGlobals());
    }

    public BeanConnectionDAOWithFoxyMill(IBeanConnectionEx<Object> connection,
            IResourceFromProvider<String> sqlTextsRes,
            IDaoAwareNamedSqlsModel<Object> optNamedSqlsModel) {
//        super(connection, null);

        innerSetup(connection, new ParametrizedTextProviderByFoxyMillResource(sqlTextsRes,
                FoxyMillUtils.makeRenderEnv(true, true, this),
                null), optNamedSqlsModel, null);

//        if (optNamedSqlsModel == null) {
//            optNamedSqlsModel = new FoxyMillDaoAwareModel<Object>();
//        }
//
//        ParametrizedTextProviderByFoxyMillResource ptpbfmr = 
//                new ParametrizedTextProviderByFoxyMillResource(sqlTextsRes,
//                FoxyMillUtils.makeRenderEnv(true, true, this),
//                optNamedSqlsModel);
//
//        super.namedSqls = ptpbfmr;
//        //RenderEnvironment re = ;
//
//        optNamedSqlsModel.setDao(this);
//        optNamedSqlsModel.setGlobals(ptpbfmr.getGlobals());
    }

    private void innerSetup(IBeanConnectionEx<Object> connection,
            ParametrizedTextProviderByFoxyMillResource ptpbfmr,
            IDaoAwareNamedSqlsModel<Object> optNamedSqlsModel,
            IServerSupportedLocaleNameRetriever optServerSupportedLocaleNameRetriever) {
//        super.namedSqls = ptpbfmr;

        super.innerSetup(connection, ptpbfmr);

        if (optNamedSqlsModel == null) {
            optNamedSqlsModel = new FoxyMillDaoAwareModel<Object>();
        }
        ptpbfmr.setModel(optNamedSqlsModel);
        optNamedSqlsModel.setDao(this, optServerSupportedLocaleNameRetriever);
        optNamedSqlsModel.setGlobals(ptpbfmr.getGlobals());
    }
}
