/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.JoinedObjMode;
import pl.fovis.common.TreeNodeBean;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public abstract class NodesGridBase extends MyBIKSGridSingleRowBase<TreeNodeBean> implements IPaginatedItemsDisplayer<TreeNodeBean> {

    public TreeNodeBean tnb;
    public Map<Integer, JoinedObjMode> directOverride = new HashMap<Integer, JoinedObjMode>();   
   // Boolean == true -> all descendants, false -> children only
    public Map<Integer, Pair<JoinedObjMode, Boolean>> subNodesOverride = new HashMap<Integer, Pair<JoinedObjMode, Boolean>>();

    @Override
    protected String getIconUrl(TreeNodeBean n) {
        return n.iconName;
    }

    protected abstract Widget getSelectedWidget(final TreeNodeBean n);


    @Override
    protected void createRowFromBean(int row, final TreeNodeBean n) {
        int col = 0;

        gp.setWidget(row, col++, getSelectedWidget(n));
        gp.setWidget(row, col++, BIKClientSingletons.createIconForNodeKind(n.nodeKindCode, n.treeCode, null));
        Label l = new Label(n.name);
        l.setTitle(n.nodePath);
        gp.setWidget(row, col++, l);

      
    }

  

    @Override
    protected void setUpGridColumnsWidths(FlexTable gp) {
        gp.setWidth("100%");
        gp.getColumnFormatter().setWidth(0, "16px");
        gp.getColumnFormatter().setWidth(1, "16px");

    }

    @Override
    protected void addOrRefreshTitles(int row, TreeNodeBean n) {

    }

}
