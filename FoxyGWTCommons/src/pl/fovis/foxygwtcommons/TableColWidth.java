/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import pl.fovis.foxygwtcommons.i18n.I18n;

/**
 *
 * @author beata
 */
public class TableColWidth extends BaseActionOrCancelDialog {

    protected TextBox widthTb;
    protected String caption;
    protected Label widthLb;
    protected IParametrizedContinuation<String> saveCont;

    @Override
    protected String getDialogCaption() {
        return I18n.ustaw.get() /* I18N:  */+ " " + caption.toLowerCase() + " " +I18n.kolumny.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        widthTb = new TextBox();
        widthLb = new Label(caption);
        main.add(widthTb);
    }

    @Override
    protected void doAction() {

        saveCont.doIt(widthTb.getText());
    }

    @Override
    protected boolean doActionBeforeClose() {
        if (BaseUtils.tryParseInt(widthTb.getText()) <= 0) {
            Window.alert(I18n.blad.get() /* I18N:  */+ "! " + caption + " " +I18n.kolumnyMusiBycDodatniaLiczbaCalk.get() /* I18N:  */+ "!");
            return false;
        }

        return true;
    }

    public void buildAndShowDialog(IParametrizedContinuation<String> saveCont, String caption) {
        this.saveCont = saveCont;
        this.caption = caption;
        super.buildAndShowDialog();
    }
}
