/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Image;
import pl.fovis.common.ScheduleWidgetBean;

/**
 *
 * @author tflorczak
 */
public class DQMScheduleWidgetBean extends ScheduleWidgetBean {

    public Image icon;
    public int testNodeId;
    public String databaseName;
//    public String serverName;
//    public String instanceName;
//    public String fileName;
    public Integer multiSourceId;
    public String connectionName;
    public String lastLog;

    public DQMScheduleWidgetBean(Grid grid, int startTimeRow, Integer startTimeCol) {
        super(grid, startTimeRow, startTimeCol);
    }
}
