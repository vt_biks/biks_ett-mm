﻿exec sp_check_version '1.6.z3';
go


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_delete_childless_folders]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_delete_childless_folders
go

create procedure [dbo].sp_delete_childless_folders
as
begin

	declare @treeOfTrees int = dbo.fn_tree_id_by_code('TreeOfTrees');

	delete from bik_node
	from (
	select *, (select count(*) from bik_node where parent_node_id = bn.id) as childCount 
	from bik_node bn 
	where bn.tree_id = @treeOfTrees
	and substring(obj_id, 1, 1) not in ('#', '@', '$', '&')
	) x
	where bik_node.id = x.id and x.childCount = 0;

end;
go


exec sp_update_version '1.6.z3', '1.6.z3.1';
go