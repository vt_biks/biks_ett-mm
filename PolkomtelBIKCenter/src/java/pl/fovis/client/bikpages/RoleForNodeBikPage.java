/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.CategorizedRoleForNodeDictWidget;
import pl.fovis.foxygwtcommons.GWTUtils;

/**
 *
 * @author AMamcarz
 */
public class RoleForNodeBikPage extends BikPageMinimalBase {

    protected CategorizedRoleForNodeDictWidget roles;

    @Override
    protected Widget buildWidgets() {

        ScrollPanel main = new ScrollPanel();
        GWTUtils.setStyleAttribute(main, "padding" /* I18N: no */, "10px");
        main.setStyleName("EntityDetailsPane");
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(main,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 20);
        roles = new CategorizedRoleForNodeDictWidget();
        main.add(roles.buildWidgets());
        roles.displayData();
        return main;
    }

//    public String getCaptionDeprecated() {
//        return I18n.roleUzytkownikow.get() /* I18N:  */;
//    }
    public String getId() {
        return "admin:dict:role";
    }

    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
//        throw new UnsupportedOperationException("Not supported yet.");
        roles.displayData();
    }
}
