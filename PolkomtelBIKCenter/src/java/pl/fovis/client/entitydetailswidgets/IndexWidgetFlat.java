/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.bikwidgets.BikIcon;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.DBIndexBean;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class IndexWidgetFlat extends EntityDetailsTabWithItemsBase<DBIndexBean> {

    protected VerticalPanel gridPanel;
    protected FlexTable gp;
    protected int indexCount = 0;

    @Override
    public void displayData() {
        gridPanel.clear();
        if (items != null) {
            gp = new FlexTable();
            gp.setWidth("100%");
            gp.setStyleName("gridJoinedObj");
            createHeaders();
            String prevIndexName = null;
            String prevType = null;
            indexCount = 1;
            List<String> columnNames = new ArrayList<String>();
            for (int i = 0; i < items.size(); i++) {
                final DBIndexBean currentBean = items.get(i);
                String currIndexName = currentBean.name;
                if (i == 0) {
                    prevIndexName = currIndexName;
                }
                if (!currIndexName.equals(prevIndexName)) {
                    createRow(indexCount, prevIndexName, prevType, BaseUtils.mergeWithSepEx(columnNames, ", "));
                    indexCount++;
                    columnNames.clear();
                }
                columnNames.add(currentBean.columnName);
                StringBuilder sb = new StringBuilder();
                if (currentBean.isPrimaryKey) {
                    sb.append("Primary key, " /* I18N: no */);
                }
                if (currentBean.type != null) {
                    sb.append(currentBean.type).append(" (").append(currentBean.isUnique ? "unique" : "non unique").append(")");
                } else {
                    sb.append(currentBean.isUnique ? "unique" : "non unique");
                }
                if (currentBean.descend != null) {
                    sb.append(" ").append(currentBean.descend);
                }
                prevType = sb.toString();
                prevIndexName = currIndexName;
            }
            createRow(indexCount, prevIndexName, prevType, BaseUtils.mergeWithSepEx(columnNames, ", "));
            gp.setWidth("100%");
            gp.getColumnFormatter().setWidth(0, "5%");
            gp.getColumnFormatter().setWidth(1, "25%");
            gp.getColumnFormatter().setWidth(2, "30%");
            gp.getColumnFormatter().setWidth(3, "40%");
            gridPanel.add(gp);
        }
    }

    @Override
    public Integer getItemCount() {
        String prevIndexName = "";
        indexCount = 0;
        for (int i = 0; i < BaseUtils.safeGetCollectionSize(items); i++) {
            final DBIndexBean currentBean = items.get(i);
            String currIndexName = currentBean.name;
            if (!currIndexName.equals(prevIndexName)) {
                indexCount++;
            }
            prevIndexName = currIndexName;
        }
        return indexCount;
    }

    protected void createHeaders() {
        gp.setWidget(0, 1, new HTML("<B>" + I18n.nazwa.get() /* I18N:  */ + "</B>"));
        gp.setWidget(0, 2, new HTML("<B>" + I18n.typ.get() /* I18N:  */ + "</B>"));
        gp.setWidget(0, 3, new HTML("<B>" + I18n.kolumny.get() /* I18N:  */ + "</B>"));
    }

    protected void createRow(int inx, String name, String type, String columns) {
        gp.setWidget(inx, 0, BikIcon.createIcon(getIndexIconName(), "RelatedObj-ObjIcon"));
        gp.setWidget(inx, 1, new HTML(name));
        gp.setWidget(inx, 2, new HTML(type));
        gp.setWidget(inx, 3, new HTML(columns));
        gp.getRowFormatter().setStyleName(inx, inx % 2 == 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
    }

    @Override
    public Widget buildWidgets() {
        gridPanel = new VerticalPanel();
        gridPanel.setWidth("100%");
        return gridPanel;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected List<DBIndexBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
        return BaseUtils.addManyLists(data.teradataIndices/* data.mssqlIndices,data.oracleIndices*/);
    }

    @Override
    public String getCaption() {
        return BIKGWTConstants.MENU_NAME_INDICIES;
    }

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_METADATA;
    }

    protected String getIndexIconName() {
        return "images/index.gif" /* I18N: no */;
    }

    @Override
    public boolean isEnabled() {
        String nodeKindCode = getNodeKindCode();
//        @EDDBProp4NodeKind({BIKConstants.NODE_KIND_TERADATA_TABLE, BIKConstants.NODE_KIND_TERADATA_COLUMN_PK, BIKConstants.NODE_KIND_TERADATA_COLUMN_IDX})
        return BIKConstants.NODE_KIND_TERADATA_TABLE.equals(nodeKindCode) || BIKConstants.NODE_KIND_TERADATA_COLUMN_PK.equals(nodeKindCode) || BIKConstants.NODE_KIND_TERADATA_COLUMN_IDX.equals(nodeKindCode);
    }
}
