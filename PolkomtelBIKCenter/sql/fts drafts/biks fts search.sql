if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#tmp_bik_fts_search_word'))
  drop table #tmp_bik_fts_search_word;
  
create table #tmp_bik_fts_search_word (
  word varchar(800) not null primary key,
  weight decimal(9,8) not null
);


insert into #tmp_bik_fts_search_word
select 
  --sw.*,
  sw.word, max(case 
    when sw.origin_flag >= 16 then 1
    when sw.origin_flag >= 8 then 0.9
    when sw.origin_flag >= 4 then 0.7
    when sw.origin_flag >= 2 then 0.5
    else 0.4 end * a.weight)
from
  bik_fts_word_in_obj_attr winoa 
  inner join bik_fts_attr a on winoa.attr_id = a.id 
  inner join bik_fts_word w on winoa.word_id = w.id 
  inner join bik_fts_stemmed_word sw on w.id = sw.word_id
where obj_id = 113170
group by sw.word;


select                     
  *
from
(
select 
  --sw.*,
  winoa.obj_id, sum(case 
    when sw.origin_flag >= 16 then 1
    when sw.origin_flag >= 8 then 0.9
    when sw.origin_flag >= 4 then 0.7
    when sw.origin_flag >= 2 then 0.5
    else 0.4 end * a.weight * fsw.weight) as relevance
from
  bik_fts_word_in_obj_attr winoa 
  inner join bik_fts_attr a on winoa.attr_id = a.id 
  inner join bik_fts_word w on winoa.word_id = w.id 
  inner join bik_fts_stemmed_word sw on w.id = sw.word_id
  inner join #tmp_bik_fts_search_word fsw on fsw.word = sw.word
group by winoa.obj_id
) s inner join bik_node n on s.obj_id = n.id
order by relevance desc



select * from
(
select
  *, (select count(*) from bik_fts_word_in_obj_attr where word_id = w.id) as usage_cnt
from bik_fts_word w
) x
where usage_cnt > 1 and word like 'prof%'
order by usage_cnt


--select top 10 * from bik_joined_objs





select 
  sw.word, count(distinct winoa.obj_id) as usage_in_obj_cnt, 1.0 as weight
from
  bik_fts_word_in_obj_attr winoa
  inner join bik_fts_attr a on winoa.attr_id = a.id
  inner join bik_fts_word w on winoa.word_id = w.id
  inner join bik_fts_stemmed_word sw on w.id = sw.word_id
  inner join #tmp_bik_fts_search_word as fsw on fsw.word = sw.word
group by sw.word
order by usage_in_obj_cnt desc


select *
from
  bik_fts_word w 
  inner join bik_fts_stemmed_word sw on w.id = sw.word_id
where sw.word like '%a�' and sw.word not like '%[_]%'
--  or w.word = 'system'
order by sw.word, w.word  
  
  
select word, count(*) as cnt
from bik_fts_stemmed_word
group by word
order by cnt desc


