﻿exec sp_check_version '1.0.98.3';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

create table bik_attr_category(
	id int not null IDENTITY(1,1) primary key,
	name varchar(255) not null
);
go

insert into bik_attr_category(name) values('Ogólne');
go

create table bik_attr_def(
	id int not null IDENTITY(1,1) primary key,
	name varchar(255) not null,
	attr_category_id int not null references bik_attr_category(id)
);
go
declare @category_id int;

select @category_id = id
from bik_attr_category
where name = 'Ogólne';

insert into
bik_attr_def(name,attr_category_id)
select distinct(name),@category_id from bik_attribute; 
go

alter table bik_attribute add 
attr_def_id int references bik_attr_def(id);
go

update bik_attribute 
set bik_attribute.attr_def_id=
 bad.id from bik_attr_def bad join bik_attribute on bad.name=bik_attribute.name
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------
exec sp_update_version '1.0.98.3', '1.0.98.4';
go
