package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.common.ConnectionParametersMsSqlBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.IAdminConfigBaseWidget;

/**
 *
 * @author p
 */
public class AdminConfigMsSqlExWidget implements IAdminConfigBaseWidget<ConnectionParametersMsSqlBean> {

    protected AdminConfigMsSqlWidget adminWidget = new AdminConfigMsSqlWidget();
    protected AdminConfigMsSqlExtPropWidget extPropWidget = new AdminConfigMsSqlExtPropWidget();

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_MSSQL;
    }

    @Override
    public void populateWidgetWithData(ConnectionParametersMsSqlBean data) {
        adminWidget.populateWidgetWithData(data.servers);
        extPropWidget.populateWidgetWithData(data.exProp);
    }

    @Override
    public Widget buildWidget() {
        TabPanel tabPanel = new TabPanel();
        tabPanel.setAnimationEnabled(true);
        tabPanel.add(adminWidget.buildWidget(), I18n.serwery.get());
        tabPanel.add(extPropWidget.buildWidget(), extPropWidget.getSourceName());
        tabPanel.selectTab(0);

        return tabPanel;
    }
}
