﻿exec sp_check_version '1.2.1.5';
go


declare @connectors varchar(200);
select @connectors = val from bik_app_prop where name = 'availableConnectors'

update bik_app_prop
set val = @connectors + ',SAP BW'
where name = 'availableConnectors'


insert into bik_data_source_def(description)
values('SAP BW')
go

insert into bik_admin(param,value)
values ('sapbw.isActive', 1)

insert into bik_admin(param,value)
values ('sapbw.path', 'C:/Temp/ZTCT_META.CSV')

insert into bik_schedule(source_id, hour, minute, interval, date_started, is_schedule, priority)
values((select id from bik_data_source_def where description='SAP BW'), 20, 0, 1, null, 0, 10)
go

create table aaa_sapbw (
	info_cube varchar(max) null,
	ip_level int null,
	info_name varchar(max) null,
	obj_name varchar(max) null,
	file_type varchar(max) null,
	info_type varchar(max) null,
	parent varchar(max) null,
	info_descr varchar(max) null,
	obj_descr varchar(max) null,
	info_area varchar(max) null,
	obj_type varchar(max) null,
	query_update varchar(max) null,
	query_owner varchar(max) null,
	query_last_edit varchar(max) null
);
go

create table bik_sapbw_area (
	obj_name varchar(max) not null,
	descr varchar(max) null,
	parent varchar(max) null
);
go

create table bik_sapbw_provider (
	obj_name varchar(max) not null,
	descr varchar(max) null,
	type varchar(100) not null,
	info_area varchar(max) null
);
go

create table bik_sapbw_flow (
	obj_name varchar(max) not null,
	parent varchar(max) not null
);
go

create table bik_sapbw_query (
	obj_name varchar(max) not null,
	descr varchar(max) null,
	provider varchar(max) null,
	update_time varchar(max) null,
	owner varchar(max) null,
	last_edit varchar(max) null
);
go

create table bik_sapbw_object (
	obj_name varchar(max) not null,
	descr varchar(max) null,
	type varchar(100) null,
	provider varchar(max) null,
	provider_parent varchar(max) null
);
go

insert into bik_tree(name,node_kind_id,code,tree_kind,is_hidden,is_in_ranking)
values ('Raporty', null, 'BWReports', 'Metadata',0,0)

insert into bik_tree(name,node_kind_id,code,tree_kind,is_hidden,is_in_ranking)
values ('Dostawcy informacji', null, 'BWProviders', 'Metadata',0,0)

insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('BWArea', 'Obszar informacji', 'bwarea', 'Metadata', 1, 15)

insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('BWBEx', 'Zapytanie BEx', 'bwbex', 'Metadata', 1, 16)

insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('BWMPRO', 'Multidostawca', 'bwmpro', 'Metadata', 1, 14)

insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('BWCUBE', 'Kostka informacji', 'bwcube', 'Metadata', 1, 13)

insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('BWODSO', 'Obiekt DataStore', 'bwodso', 'Metadata', 1, 12)

insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('BWISET', 'Zbiór informacji', 'bwiset', 'Metadata', 1, 14)

insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('BWObjsFolder', 'Folder obiektów informacji', 'bwobject', 'Metadata', 1, 0)

insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('BWUni', 'Jednostka', 'bwuni', 'Metadata', 1, 9)

insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('BWTim', 'Czas', 'bwtim', 'Metadata', 1, 9)

insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('BWKyf', 'Wskaźnik', 'bwkyf', 'Metadata', 1, 9)

insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('BWCha', 'Charakterystyka', 'bwcha', 'Metadata', 1, 9)

insert into bik_node_kind(code, caption, icon_name,tree_kind,is_folder,search_rank)
values ('BWDpa', 'Pakiet Danych', 'bwdpa', 'Metadata', 1, 9)


exec sp_add_menu_node 'metadata', 'SAP BW', 'sapbw'
exec sp_add_menu_node 'sapbw', '@', '$BWReports'
exec sp_add_menu_node '$BWReports', '@', '&BWArea$BWReports'
exec sp_add_menu_node '&BWArea$BWReports', '@', '&BWBEx'
exec sp_add_menu_node 'sapbw', '@', '$BWProviders'
exec sp_add_menu_node '$BWProviders', '@', '&BWArea$BWProviders'
exec sp_add_menu_node '&BWArea$BWProviders', '@', '&BWMPRO'
exec sp_add_menu_node '&BWArea$BWProviders', '@', '&BWCUBE'
exec sp_add_menu_node '&BWArea$BWProviders', '@', '&BWISET'
exec sp_add_menu_node '&BWArea$BWProviders', '@', '&BWODSO'
exec sp_add_menu_node '&BWArea$BWProviders', '@', '&BWCha'
exec sp_add_menu_node '&BWArea$BWProviders', '@', '&BWUni'
exec sp_add_menu_node '&BWArea$BWProviders', '@', '&BWKyf'
exec sp_add_menu_node '&BWArea$BWProviders', '@', '&BWTim'
exec sp_add_menu_node '&BWArea$BWProviders', '@', '&BWDpa'

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go
-----------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbw]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbw]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbw](@tree_code varchar(255))
as
begin
	declare @diag_level int = 1;
	declare @AreaNodeKind int;
	declare @BExNodeKind int;
	declare @CUBENodeKind int;
	declare @MPRONodeKind int;
	declare @ODSONodeKind int;
	declare @ISETNodeKind int;
	declare @ObjectsFolderNodeKind int;
	declare @UNINodeKind int;
	declare @TIMNodeKind int;
	declare @KYFNodeKind int;
	declare @DPANodeKind int;
	declare @CHANodeKind int;

	declare @tree_id int;
	select @AreaNodeKind = id from bik_node_kind where code = 'BWArea'
	select @BExNodeKind = id from bik_node_kind where code = 'BWBEx'
	select @CUBENodeKind = id from bik_node_kind where code = 'BWCUBE'
	select @MPRONodeKind = id from bik_node_kind where code = 'BWMPRO'
	select @ODSONodeKind = id from bik_node_kind where code = 'BWODSO'
	select @ISETNodeKind = id from bik_node_kind where code = 'BWISET'
	select @ObjectsFolderNodeKind = id from bik_node_kind where code = 'BWObjsFolder'
	select @UNINodeKind = id from bik_node_kind where code = 'BWUni'
	select @TIMNodeKind = id from bik_node_kind where code = 'BWTim'
	select @KYFNodeKind = id from bik_node_kind where code = 'BWKyf'
	select @DPANodeKind = id from bik_node_kind where code = 'BWDpa'
	select @CHANodeKind = id from bik_node_kind where code = 'BWCha'
	select @tree_id = id from bik_tree where code = @tree_code;
	
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'
	
	create table #tmpNodes (si_id varchar(900) primary key, si_parentid varchar(1000), si_kind varchar(100), si_name varchar(1000), descr varchar(max), visual_order int default 0);
	create table #tmpNodesLinked (si_id varchar(900), si_parentid varchar(1000));
	
	-- wrzucanie obszarów inf.
	insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
	select obj_name, parent, @AreaNodeKind, descr, null, -1 from bik_sapbw_area 
		
	if(@tree_code='BWReports')
	begin
		-- wrzucanie obszaru na zapytania nieprzypisane
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		values('$$BIKS_TEMP_ARCHIWUM$$', null, @AreaNodeKind, '(Archiwum)', 'Obszar gromadzący zapytania BEx, dla których nie odnaleziono dostawcy informacji. Obszar został wygenerowany automatycznie i nie znajduje się w repozytorium SAP BW.',100)

		-- wrzucanie zapytań BEx
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr)
		select convert(varchar(200),obj_name), coalesce((select ar.obj_name from bik_sapbw_provider pv left join bik_sapbw_area ar on pv.info_area=ar.obj_name where pv.obj_name = q.provider),'$$BIKS_TEMP_ARCHIWUM$$'), @BExNodeKind, case when rtrim(descr) <> '' then rtrim(descr) else obj_name end,'' from bik_sapbw_query q
	end;
	if(@tree_code='BWProviders')
	begin
		-- wrzucanie obszaru na dostawców nieprzypisanych
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		values('$$BIKS_TEMP_ARCHIWUM$$', null, @AreaNodeKind, '(Archiwum)', 'Obszar gromadzący dostawców informacji, dla których nie odnaleziono obszaru informacji. Obszar został wygenerowany automatycznie i nie znajduje się w repozytorium SAP BW.',100)

		-- wrzucanie folderów na cechy i wskaźniki
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		select obj_name + '|objectsFolder|', obj_name, @ObjectsFolderNodeKind, 'Cechy i wskaźniki', 'Folder zawierający obiekty informacji', -1 from bik_sapbw_provider

		-- wrzucanie dostawców informacji
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order) -- visual_order zgodny z tym w SAP GUI
		select obj_name, coalesce(area,'$$BIKS_TEMP_ARCHIWUM$$'), case type when 'MPRO' then @MPRONodeKind
																			when 'CUBE' then @CUBENodeKind
																			when 'ODSO' then @ODSONodeKind
																			when 'ISET' then @ISETNodeKind end,  
			case when rtrim(descr) <> '' then rtrim(descr) else obj_name end, null, case type when 'CUBE' then 1
																							when 'ISET' then 2
																							when 'MPRO' then 3
																							when 'ODSO' then 4 end  
		from bik_sapbw_provider xx left join (select obj_name as area from bik_sapbw_area) yy on xx.info_area = yy.area
		
		-- wrzucanie obiektów
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		select provider + '|' + obj_name, provider + '|objectsFolder|' ,case type when 'CHA' then @CHANodeKind 
																				  when 'UNI' then @UNINodeKind
																				  when 'KYF' then @KYFNodeKind
																				  when 'TIM' then @TIMNodeKind
																				  when 'DPA' then @DPANodeKind
																				  else @CHANodeKind end, 
																				  descr, null, case type when 'CHA' then 1
																										 when 'KYF' then 2
																										 when 'TIM' then 3
																										 when 'UNI' then 4
																										 when 'DPA' then 5
																										 else 6 end 
		from bik_sapbw_object 
		where provider_parent is null
		
		-- wrzucanie przepływów
		insert into #tmpNodesLinked(si_id,si_parentid)
		select obj_name, parent from bik_sapbw_flow
	end;
	
	-- usuwanie pustych folderów
	declare @rc int = 1

	while @rc > 0 
	begin
		delete from #tmpNodes
		where si_kind in (@AreaNodeKind, @ObjectsFolderNodeKind) and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	
	-- wdrażnie danych z tabeli tymczasowej to bik_node
	
	-- update istniejących
	update bik_node 
	set node_kind_id = #tmpNodes.si_kind, name = ltrim(rtrim(#tmpNodes.si_name)), descr = #tmpNodes.descr,
		visual_order = #tmpNodes.visual_order, is_deleted = 0
	from #tmpNodes
	where bik_node.obj_id = #tmpNodes.si_id and (bik_node.tree_id = @tree_id or (bik_node.linked_node_id is not null and bik_node.tree_id in (select id from bik_tree where tree_kind='Taxonomy')))
	
	-- wrzucanie nowych
	insert into bik_node(name,node_kind_id,tree_id,descr,obj_id, visual_order)
	select ltrim(rtrim(#tmpNodes.si_name)), #tmpNodes.si_kind, @tree_id, #tmpNodes.descr, #tmpNodes.si_id, #tmpNodes.visual_order
	from #tmpNodes --inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id and bik_node.linked_node_id is null
	where bik_node.id is null;
	
	-- wrzucanie podlinkowanych
	insert into bik_node(name,parent_node_id,node_kind_id,tree_id,obj_id, linked_node_id, disable_linked_subtree)
	select lin.name, par.id, lin.node_kind_id, lin.tree_id, tmp.si_id, lin.id, 1 from #tmpNodesLinked tmp 
	left join bik_node lin on lin.obj_id = tmp.si_id and lin.is_deleted = 0 and lin.tree_id = @tree_id and lin.linked_node_id is null
	left join bik_node par on par.obj_id = tmp.si_parentid and par.is_deleted = 0 and par.tree_id = @tree_id and par.linked_node_id is null
	left join bik_node oryg on oryg.obj_id = tmp.si_id and oryg.is_deleted = 0 and oryg.tree_id = @tree_id and oryg.linked_node_id is not null
	where oryg.id is null and par.id is not null and lin.id is not null
	
	-- update parentów
	update bik_node 
	set parent_node_id= bk.id
	from bik_node 
	inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id
	inner join bik_node bk on bk.obj_id = pt.si_parentid 
	where bik_node.linked_node_id is null and bk.tree_id = @tree_id 
	
	-- usuwanie starych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id 
	where #tmpNodes.si_id is null and (bik_node.tree_id = @tree_id or (bik_node.linked_node_id is not null and bik_node.tree_id in (select id from bik_tree where tree_kind='Taxonomy')))
	
	-- usuwanie starych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join #tmpNodesLinked on bik_node.obj_id = #tmpNodesLinked.si_id 
	where #tmpNodesLinked.si_id is null and bik_node.linked_node_id is not null
	and bik_node.tree_id = @tree_id
	
	drop table #tmpNodes;
	drop table #tmpNodesLinked;
	
	exec sp_node_init_branch_id @tree_id, null;
	
end;
go


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_sapbw_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_sapbw_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_sapbw_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
    
    declare @BEx_kind int;
    declare @Cube_kind int;
    declare @Iset_kind int;
    declare @Mpro_kind int;
    declare @Odso_kind int;
    declare @Reports_tree_id int;
    declare @Providers_tree_id int;
    select @BEx_kind=id from bik_node_kind where code='BWBEx'    
    select @Cube_kind=id from bik_node_kind where code='BWCUBE'
    select @Iset_kind=id from bik_node_kind where code='BWISET'
    select @Mpro_kind=id from bik_node_kind where code='BWMPRO'
    select @Odso_kind=id from bik_node_kind where code='BWODSO'
    select @Reports_tree_id = id from bik_tree where code = 'BWReports'
    select @Providers_tree_id = id from bik_tree where code = 'BWProviders'

	---------- BEx query --> Provider -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, prov.id, 1 from bik_sapbw_query q
	inner join bik_node bn on bn.obj_id = convert(varchar(200),q.obj_name)
		and bn.is_deleted = 0 
		and bn.linked_node_id is null
		and bn.tree_id = @Reports_tree_id
	inner join bik_node prov on prov.obj_id = convert(varchar(200),q.provider)
		and prov.is_deleted = 0 
		and prov.linked_node_id is null
		and prov.tree_id = @Providers_tree_id
	
	---------- Provider --> BEx query -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select prov.id, bn.id, 1 from bik_sapbw_query q
	inner join bik_node bn on bn.obj_id = convert(varchar(200),q.obj_name)
		and bn.is_deleted = 0 
		and bn.linked_node_id is null
		and bn.tree_id = @Reports_tree_id
	inner join bik_node prov on prov.obj_id = convert(varchar(200),q.provider)
		and prov.is_deleted = 0 
		and prov.linked_node_id is null
		and prov.tree_id = @Providers_tree_id
	
	---------- Provider źródłowy --> Provider -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bn2.id, 1 from bik_sapbw_flow fl
    inner join bik_node bn on bn.obj_id=fl.obj_name 
		and bn.is_deleted = 0 
		and bn.linked_node_id is null 
		and bn.tree_id = @Providers_tree_id
	inner join bik_node bn2 on bn2.obj_id=fl.parent 
		and bn2.is_deleted = 0 
		and bn2.linked_node_id is null 
		and bn2.tree_id = @Providers_tree_id
		
	---------- Provider --> Provider źródłowy -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn2.id, bn.id, 1 from bik_sapbw_flow fl
    inner join bik_node bn on bn.obj_id=fl.obj_name 
		and bn.is_deleted = 0 
		and bn.linked_node_id is null 
		and bn.tree_id = @Providers_tree_id
	inner join bik_node bn2 on bn2.obj_id=fl.parent 
		and bn2.is_deleted = 0 
		and bn2.linked_node_id is null 
		and bn2.tree_id = @Providers_tree_id
	
    exec sp_move_bik_joined_objs_tmp 'BWBEx, BWCUBE, BWISET, BWMPRO, BWODSO', 'BWCUBE, BWISET, BWMPRO, BWODSO'
	
end;
go


insert into bik_attr_category(name,is_deleted,is_built_in)
values('SAP BW', 0, 1)

declare @categoryId int;
select @categoryId = id from bik_attr_category where name = 'SAP BW' and is_built_in = 1

insert into bik_attr_def(name, attr_category_id, is_deleted, is_built_in)
values('Nazwa techniczna', @categoryId, 0, 1)

insert into bik_attr_def(name, attr_category_id, is_deleted, is_built_in)
values('Osoba odpowiedzialna', @categoryId, 0, 1)

insert into bik_attr_def(name, attr_category_id, is_deleted, is_built_in)
values('Ostatnia zmiana', @categoryId, 0, 1)

insert into bik_attr_def(name, attr_category_id, is_deleted, is_built_in)
values('Zmieniony przez', @categoryId, 0, 1)

-- poprzednia wersja w pliku alter db for v1.2.0.1 tf.sql
-- dodanie wyszukiwania po atrybutach z bw
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_metadata]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_metadata
go

create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_extradata', 'node_id', 'author, owner, cuid, guid, ruid', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_engine, database_source, connetion_networklayer_name, user_name, server', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table', null, @optNodeFilter
  
	declare @tree_id int = (select id from bik_tree where code = 'DQC')
	declare @inact_nk_id int = (select id from bik_node_kind where code = 'DQCTestInactive'),
    @succ_nk_id int = (select id from bik_node_kind where code = 'DQCTestSuccess'),
    @fail_nk_id int = (select id from bik_node_kind where code = 'DQCTestFailed')

	declare @dqc_src_sql varchar(max) = '(select cast(dt.long_name as varchar(max)) as long_name, cast(dt.sampling_method as varchar(max)) as sampling_method, cast(dt.verified_attributes as varchar(max)) as verified_attributes,cast(dt.expected_result as varchar(max)) as expected_result,cast(dt.logging_details as varchar(max)) as logging_details, cast(dt.benchmark_definition as varchar(max)) as benchmark_definition, cast(dt.results_object as varchar(max)) as results_object, cast(dt.additional_information as varchar(max)) as additional_information, n.id as node_id
		from bik_dqc_test dt inner join bik_node n on dt.__obj_id = n.obj_id and n.node_kind_id in (' +
		cast(@inact_nk_id as varchar(20)) + ',' + cast(@succ_nk_id as varchar(20)) + ',' + cast(@fail_nk_id as varchar(20)) + ') and tree_id = ' + cast(@tree_id as varchar(20)) + '
		where n.linked_node_id is null and n.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @dqc_src_sql, 'node_id', 'long_name, sampling_method, verified_attributes,expected_result,logging_details, benchmark_definition, results_object, additional_information', null, @optNodeFilter
	
	declare @ad_src_sql varchar(max) = '(select bu.email, coalesce(bu.phone_num, ad.telephone_number) as phone_num,
		ad.physical_delivery_office_name,ad.postal_code, ad.manager, 
		ad.description, ad.department, ad.title, ad.mobile, 
		ad.display_name, bu.node_id
		from bik_user bu
		join bik_node bn on bn.id = bu.node_id and bn.is_deleted = 0
		left join bik_system_user bsu on bsu.user_id = bu.id 
		left join bik_active_directory ad 
		on (bu.login_name_for_ad = ad.s_a_m_account_name or bsu.login_name = ad.s_a_m_account_name))'
	exec sp_verticalize_node_attrs_one_metadata_table @ad_src_sql, 'node_id', 'email, phone_num, physical_delivery_office_name,postal_code,manager, description, department, title, mobile, display_name', null, @optNodeFilter
	
	declare @obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as si_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id=bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''DataConnection'', ''Webi'', ''Flash'', ''CrystalReport'', ''Universe'', ''Excel'', ''FullClient'', ''Pdf'', ''Hyperlink'', ''Powerpoint'',
		''ReportFolder'', ''UniversesFolder'', ''ConnectionFolder''))'
	exec sp_verticalize_node_attrs_one_metadata_table @obj_id_sql, 'id', 'si_id', null, @optNodeFilter
	
	-- SAP BW query extradata
	declare @bwreports_id int = (select id from bik_tree where code = 'BWReports')
	declare @query_nk_id int = (select id from bik_node_kind where code = 'BWBEx')
	declare @sapbw_query_src_sql varchar(max) = '(select q.update_time, q.owner, q.last_edit, bn.id as node_id
		from bik_sapbw_query q inner join bik_node bn on q.obj_name = bn.obj_id and bn.node_kind_id = ' +
		cast(@query_nk_id as varchar(20)) + ' and bn.tree_id = ' + cast(@bwreports_id as varchar(20)) + '
		where bn.linked_node_id is null and bn.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @sapbw_query_src_sql, 'node_id', 'update_time, owner, last_edit', null, @optNodeFilter
	
	-- SAP BW providers, query and areas - technical id
	declare @bw_obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id=bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWBEx'', ''BWArea'', ''BWMPRO'', ''BWCUBE'', ''BWISET'', ''BWODSO''))'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_obj_id_sql, 'id', 'technical_id', null, @optNodeFilter
	
	-- SAP BW objects - technical id
	declare @bw_objs_id_sql varchar(max) = '(select bn.id, obj.obj_name as obj_technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id=bnk.id
		inner join bik_sapbw_object obj on obj.provider + ''|'' + obj.obj_name=bn.obj_id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWUni'',''BWKyf'', ''BWTim'', ''BWDpa'', ''BWCha'')
		and obj.provider_parent is null)'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_objs_id_sql, 'id', 'obj_technical_id', null, @optNodeFilter
	
end;
go

exec sp_update_version '1.2.1.5', '1.2.2';
go