/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.serverlogic;

import pl.trzy0.foxy.commons.MailServerConfig;
import pl.trzy0.foxy.commons.PostgresConnectionConfig;

/**
 *
 * @author wezyr
 */
public class AppConfigObj extends ServletConfigObj {
    public PostgresConnectionConfig connConfig;
    public MailServerConfig mailServerConfig;
}
