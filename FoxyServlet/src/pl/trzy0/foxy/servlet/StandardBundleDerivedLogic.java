/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import java.util.Collections;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.trzy0.foxy.serverlogic.FoxyUrlRewriter;
import pl.trzy0.foxy.serverlogic.IApplicationEnvironment;
import pl.trzy0.foxy.serverlogic.IFoxyService;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.MapHelper;

/**
 *
 * @author wezyr
 */
public class StandardBundleDerivedLogic implements IBundleDerivedLogic {

    public static final String SUPPORTED_CLIENT_LANGS_KEYNAME = "supportedClientLangs";
    public static final String CLIENT_LANG_KEY_PREFIX = "clientLangFor.";
    protected MapHelper<String> sysProps;
    protected FoxyMillBundleData bundleData;
    private static String supportedClientLangsStr;
    private static Set<String> supportedClientLangs = Collections.<String>emptySet();
    private Set<String> languageSensitivePages;
    private FoxyUrlRewriter urlRewriter;
    private IApplicationEnvironment appEnv;

    public void preInit(MapHelper<String> sysProps) {
        this.sysProps = sysProps;
    }

    protected void customInit() {
        String lsp = getSysProp("languageSensitivePages", "");
        this.languageSensitivePages = BaseUtils.splitUniqueBySep(lsp, ",", true);
    }

    public void init(FoxyMillBundleData bundleData,
            FoxyUrlRewriter urlRewriter,
            IApplicationEnvironment appEnv) {
        this.bundleData = bundleData;
        this.urlRewriter = urlRewriter;
        this.appEnv = appEnv;

        customInit();
    }

    protected FoxyMillServletModelBase createRawPageModel() {
        return new FoxyMillServletModelBase();
    }

    public FoxyMillServletModelBase createPageModel(HttpServletRequest request, HttpServletResponse response,
            IFoxyService service, String pageName, MapHelper<String> params) {
        FoxyMillServletModelBase model = null;
        try {
            model = //new FoxyMillServletModelBase();
                    createRawPageModel();
        } catch (Exception ex) {
            throw new LameRuntimeException("error creating model", ex);
        }
        model.init(request, response, //bundleData.boxes,
                service,
                pageName, params,
                this, urlRewriter,
                appEnv //                bundleData.globals,
                //                //props,
                //                bundleData.sysProps, bundleData.textsProps,
                //                bundleData.renderEnv.cacheStorage, bundleData.tmplProviders
                );
        return model;
    }

    public String getSysProp(String name) {
        return getSysProp(name, null);
    }

    public String getSysProp(String name, String defVal) {
        return sysProps.getString(name, defVal);
    }

    public String getDefaultClientLang() {
        return getSysProp("defaultLang", "pl");
    }

    public Set<String> getSupportedClientLangs() {
        String scls = getSysProp(SUPPORTED_CLIENT_LANGS_KEYNAME);
        if (!BaseUtils.safeEquals(scls, supportedClientLangsStr)) {
            supportedClientLangsStr = scls;
            supportedClientLangs = BaseUtils.splitUniqueBySep(scls == null ? "" : scls, ",", true);
        }
        return supportedClientLangs;
    }

    public String getClientLangForContentLang(String paramLang) {
        String cliLang = sysProps.getString(CLIENT_LANG_KEY_PREFIX + paramLang, paramLang);
        if (!getSupportedClientLangs().contains(cliLang)) {
            cliLang = getDefaultClientLang();
        }
        return cliLang;
    }

    public boolean isLanguageSensitivePage(String pageName) {
        return languageSensitivePages.contains(pageName);
    }

    public String getMainPageName() {
        return getSysProp("mainPageName", "mainPage");
    }

    public String getFallThroughLoginUrl() {
        return getSysProp("loginPageName");
    }
}
