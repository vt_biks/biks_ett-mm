/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.Arrays;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.FileUploadDialogForPhoto;
import pl.fovis.foxygwtcommons.FoxyFileUpload;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author beata
 */
public class MyBIKSEditUserTab extends MyBIKSTabBase {

    protected HorizontalPanel mainHp;
    protected Label loginName;
    protected Image avatarImage;
    protected SystemUserBean user;
    protected PasswordTextBox password;
    protected PasswordTextBox passwordRepeat;
    protected PasswordTextBox passwordOld;
    protected BikCustomButton saveChangePassword;
    protected CheckBox isSendMailsCb;
    protected CheckBox changePassword;

    @Override
    public String getCaption() {
        return I18n.edycjaDanych.get() /* I18N:  */;
    }

    @Override
    public String getButtonStyle() {
        return "labelLink-profil";
    }

    @Override
    public void show(VerticalPanel vp) {
        vp.clear();
        mainHp = new HorizontalPanel();

        String avatar = BIKClientSingletons.getLoggedUserAvatar();
        HorizontalPanel hpBtn = new HorizontalPanel();
        Panel avatarAp = new AbsolutePanel();
        avatarAp.setStyleName("MyBIKAvatarPanelEditP");
        avatarAp.add(avatarImage = BIKClientSingletons.createAvatarImage(avatar, 120, 120, "images/noavatar.jpg" /* I18N: no */, "MyBIKAvatarImgEdit", null));
        if (avatar != null && avatar.startsWith("file_")) {
            hpBtn.add(makeAvatarActionButton(I18n.zmienZdjecie.get() /* I18N:  */, I18n.zmienionoZdjecie.get() /* I18N:  */, false, vp));
            hpBtn.add(makeAvatarActionButton(I18n.usun.get() /* I18N:  */, I18n.usunietoZdjecie.get() /* I18N:  */, true, vp));
        } else {
            hpBtn.add(makeAvatarActionButton(I18n.dodajZdjecie.get() /* I18N:  */, I18n.dodanoZdjecie.get() /* I18N:  */, false, vp));
        }

        VerticalPanel editAvatarVp = new VerticalPanel();
        editAvatarVp.setStyleName("MyBIKAvatarPanelEdit");
        editAvatarVp.add(avatarAp);
        editAvatarVp.add(hpBtn);
        buildGrid();
        mainHp.add(editAvatarVp);
        vp.add(mainHp);
    }

    protected void buildGrid() {
        user = BIKClientSingletons.getLoggedUserBean();
        FlexTable grid = new FlexTable();
        grid.setStyleName("MyBIKAvatarPanelEditUser");
        int row = 0;
        changePassword = new CheckBox(I18n.zmienHaslo.get());
        changePassword.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                setEnablePassword(changePassword.getValue());
            }
        });
        boolean isSendEmailNotifications = BIKClientSingletons.isSendEmailNotifications();
        changePassword.setVisible(isSendEmailNotifications);

        grid.setWidget(row, 0, new Label(I18n.login.get() /* I18N:  */));
        grid.setWidget(row++, 1, loginName = new Label());
        if (user != null) {
            loginName.setText(BaseUtils.safeToString(user.loginName));
            if (!user.isDomainUser) {
                grid.getFlexCellFormatter().setColSpan(row, 0, 2);
                grid.setWidget(row++, 0, changePassword);

                grid.setWidget(row, 0, new Label(I18n.biezaceHaslo.get() /* I18N:  */ + ":"));
                grid.setWidget(row++, 1, passwordOld = new PasswordTextBox());
                grid.setWidget(row, 0, new Label(I18n.haslo.get() /* I18N:  */));
                grid.setWidget(row++, 1, password = new PasswordTextBox());
                grid.setWidget(row, 0, new Label(I18n.powtorzHaslo.get() /* I18N:  */));
                grid.setWidget(row++, 1, passwordRepeat = new PasswordTextBox());
                setEnablePassword(false);
                if (!isSendEmailNotifications) {
                    grid.setWidget(6, 1, saveChangePassword = saveButton(I18n.zapiszZmiany.get() /* I18N:  */, I18n.zmodyfikowanoDane.get() /* I18N:  */, false));
                }
            }
            if (isSendEmailNotifications) {
                isSendMailsCb = new CheckBox(I18n.wysylajPowiadomieniaMailowe.get());
                isSendMailsCb.setValue(user.isSendMails);
                grid.getFlexCellFormatter().setColSpan(row, 0, 2);
                grid.setWidget(row++, 0, isSendMailsCb);
                grid.setWidget(6, 1, saveChangePassword = saveButton(I18n.zapiszZmiany.get() /* I18N:  */, I18n.zmodyfikowanoDane.get() /* I18N:  */, false));
            }

        }
        mainHp.add(grid);
    }

    protected void setEnablePassword(boolean isEnable) {
        if (BIKClientSingletons.isSendEmailNotifications()) {
            passwordOld.setEnabled(isEnable);
            password.setEnabled(isEnable);
            passwordRepeat.setEnabled(isEnable);
        }
    }

    protected BikCustomButton makeAvatarActionButton(String btnName, final String reactionName, final boolean ifDelete, final VerticalPanel vp) {
        BikCustomButton actionBtn;
        actionBtn = new BikCustomButton(btnName, ifDelete ? "delTrashBtn" : "Attachs" /* I18N: no */ + "-addAttachEntry",
                new IContinuation() {
                    @Override
                    public void doIt() {
                        if (ifDelete) {
                            new ConfirmDialog().buildAndShowDialog("<br>" + I18n.czyNaPewnoUsunacZdjecie.get() /* I18N:  */ + "?<br/>" + "<br><center>" + avatarImage + "<center/><br/>",
                                    new IContinuation() {
                                        @Override
                                        public void doIt() {
                                            updateUserAvatar(null, reactionName, vp);
                                        }
                                    });
                        } else {
                            FileUploadDialogForPhoto fud = new FileUploadDialogForPhoto();
                            fud.buildAndShowDialog(new IParametrizedContinuation<FoxyFileUpload>() {
                                @Override
                                public void doIt(final FoxyFileUpload param) {
                                    updateUserAvatar(param.getServerFileName(), reactionName, vp);
                                }
                            }, Arrays.asList(BIKClientSingletons.allowedFileExtensionsList()));
                        }
                    }
                });
        return actionBtn;
    }

    protected void updateUserAvatar(final String avatar, final String whatToDo, final VerticalPanel vp) {
        BIKClientSingletons.getService().setUserAvatar(avatar,
                new StandardAsyncCallback<String>() {
                    @Override
                    public void onSuccess(String result) {
                        BIKClientSingletons.showInfo(whatToDo);
                        BIKClientSingletons.setLoggedUserAvatar(result);
                        if (myBIKSPage != null) {
                            myBIKSPage.refreshAvatarImg();
                        }
                        show(vp);
                    }
                });
    }

    protected BikCustomButton saveButton(String btnName, final String reactionName, final boolean ifDelete) {
        BikCustomButton actionBtn = new BikCustomButton(btnName, ifDelete ? "delTrashBtn" : "Attachs" /* I18N: no */ + "-addAttachEntry",
                new IContinuation() {
                    @Override
                    public void doIt() {

                        final int userId = user.id;
                        final boolean isSendMails = BIKClientSingletons.isSendEmailNotifications() ? isSendMailsCb.getValue() : false;
                        if (BIKClientSingletons.isSendEmailNotifications() && !changePassword.getValue()) {
                            updateBikSystemUser(userId, user.loginName, null, isSendMails);
                            
                           BIKClientSingletons.getLoggedUserBean().isSendMails=isSendMails;
                        } else if (!password.getValue().equals(passwordRepeat.getValue())) {
                            Window.alert(I18n.haslaNieSaTakieSame.get() /* I18N:  */);
                        } else if (BaseUtils.isStrEmptyOrWhiteSpace(password.getValue())) {
                            Window.alert(I18n.hasloNieMozeBycPuste.get() /* I18N:  */);
                        } else {
                            BIKClientSingletons.getService().isOldPasswordTrue(passwordOld.getValue(), userId, new StandardAsyncCallback<Boolean>() {
                                @Override
                                public void onSuccess(Boolean result) {
                                    if (result) {

                                        updateBikSystemUser(userId, user.loginName, password.getValue(), isSendMails);
                                        user.isSendMails = isSendMails;

//                                        BIKClientSingletons.setIsSendMail(isSendMails);
//                                        BIKClientSingletons.iss
//                                        BIKClientSingletons.getLoggedUserBean().isSendMails = isSendMails;
                                    } else {
                                        Window.alert(I18n.podaneBiezaceHasloJestBledne.get() /* I18N:  */ + ".");
                                    }
                                }
                            });
                        }
                    }
                }
        );
        return actionBtn;
    }

    protected void updateBikSystemUser(int userId, String loginName, String newPassword, boolean isSendMails) {
        BIKClientSingletons.getService().updateBikSystemUser(userId, loginName, newPassword, isSendMails, new StandardAsyncCallback<Void>("Failure on" /* I18N: no */ + " updateBikSystemUser") {
                    @Override
                    public void onSuccess(Void result) {
                        BIKClientSingletons.showInfo(I18n.zmodyfikowanoDane.get() /* I18N:  */);
                        clearPasswordTextBox(passwordOld, password, passwordRepeat);
                    }
                });
    }

    protected void clearPasswordTextBox(PasswordTextBox... passwords) {
        for (PasswordTextBox passwordTb : passwords) {
            passwordTb.setValue("");
        }
    }

    @Override
    public IMyBIKSPage.ReachableMyBIKSTabCode getOptTabCode() {
        return IMyBIKSPage.ReachableMyBIKSTabCode.EditUser;
    }
}
