﻿exec sp_check_version '1.6.z9.2';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_attribute_linked]') and name = N'idx_bik_attribute_linked_node_id')
begin
	drop index [idx_bik_attribute_linked_node_id] ON [dbo].[bik_attribute_linked];
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_attribute_linked]') and name = N'idx_bik_attribute_linked_n_a_v')
begin
	create unique index idx_bik_attribute_linked_n_a_v on bik_attribute_linked (node_id, attribute_id);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_attribute_linked]') and name = N'idx_bik_attribute_linked_a_n_v')
begin
	create unique index idx_bik_attribute_linked_a_n_v on bik_attribute_linked (attribute_id, node_id);
end;
go

update bik_app_prop set val = 'images/santa2.png|12.06|01.06|-120|8;images/easter.png|03.20|04.20|-51|3;images/pl.png|04.29|05.06|-46|3;'
where name = 'logoOverlay'
go

if not exists(select * from bik_app_prop where name = 'renderAttributesAsHTML')
begin
	insert into bik_app_prop (name, val, is_editable)
	values ('renderAttributesAsHTML', 'false', 1)
end;
go



if not exists(select * from bik_app_prop where name = 'dumHeaderMsg')
begin
	insert into bik_app_prop (name, val, is_editable)
	values ('dumHeaderMsg', '', 1),
	('dumAvailableOptions', '', 1),
	('dumFooterMsg', '', 1),
	('dumMailTo', '', 1),
	('dumMailTitle', '', 1),
	('dumMailBody', '', 1)
end;
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------


exec sp_update_version '1.6.z9.2', '1.6.z9.3';
go
