exec sp_check_version '1.1.9.18';
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

declare @treeOfTreesId int = (select id from bik_tree where code = 'TreeOfTrees')
update bik_node set visual_order = id where tree_id = @treeOfTreesId
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------
--ww: poprzednia wersja w alter db for v1.1.9.16 pm.sql
-- warunek na parent_node_id by� z�y!
-- nie mo�na pisa� po prostu parent_node_id = @parent_node_id, bo
-- przecie� parent_node_id mo�e by� nullem!
-- musi by�: (@parent_node_id is null and parent_node_id is null or @parent_node_id is not null and parent_node_id = @parent_node_id)
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_add_menu_node]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_add_menu_node]
GO

create procedure [dbo].[sp_add_menu_node](@parent_obj_id varchar(255), @name varchar(255), @obj_id varchar(255))
as
begin
  declare @parent_node_id int = dbo.fn_node_id_by_tree_code_and_obj_id('TreeOfTrees', @parent_obj_id)
  declare @tree_of_trees_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
  declare @visual_order int = coalesce((select max(visual_order) from bik_node 
    where (@parent_node_id is null and parent_node_id is null or @parent_node_id is not null and parent_node_id = @parent_node_id) and
    is_deleted = 0 and tree_id = @tree_of_trees_id), 0) + 1

  insert into bik_node (parent_node_id, node_kind_id, tree_id, name, obj_id, visual_order)
  values (@parent_node_id, dbo.fn_node_kind_id_by_code('MenuItem'), @tree_of_trees_id, @name, @obj_id, @visual_order)
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_update_version '1.1.9.18', '1.1.9.19';
go
