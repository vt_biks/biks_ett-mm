/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib.logging;

import java.util.Collection;
import simplelib.logging.ILameLogger.LameLogLevel;

/**
 *
 * @author wezyr
 */
public interface ILameLoggerAppender {
    public LameLogLevel getMinLogLevel();
    //public void sendMsgToOutput(LameLoggerMsg msg);
    public void sendMsgsToOutput(Collection<LameLoggerMsg> msgs,
            LameLogLevel minLogLevelInMsgs, LameLogLevel maxLogLevelInMsgs);
}
