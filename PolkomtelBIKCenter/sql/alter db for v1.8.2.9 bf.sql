﻿exec sp_check_version '1.8.2.9';
go

declare @metaTreeKindId int = (select id from bik_tree_kind where code='metaBIKS')
exec sp_add_node_kind_relation @metaTreeKindId, 'metaBiksNodeKindInRelation', 'metaBiksBuiltInNodeKind', 'Hyperlink'

exec sp_update_version '1.8.2.9', '1.8.2.10';
go
 