/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.TreeSelectorForUsers;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.UserBean;
import pl.fovis.common.UserInNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author beata
 */
public class UserInNodeBeanExtractor extends NodeAwareBeanExtractorBase<UserInNodeBean> {//do specjalistow

    protected int currentNodeId;
    protected String pathStr;
    protected Map<Integer, String> nodeNamesById;

    public UserInNodeBeanExtractor(boolean isAddJoinedButtonEnabled, int currentNodeId, Map<Integer, String> nodeNamesById) {
        super(isAddJoinedButtonEnabled);
        this.currentNodeId = currentNodeId;
        this.nodeNamesById = nodeNamesById;
    }

    public Integer getNodeId(UserInNodeBean b) {
        return b.userNodeId;
    }

    public String getTreeCode(UserInNodeBean b) {
        return b.treeCode;
    }

    @Override
    public String getNameAsHtml(UserInNodeBean b) {
        return getNameWithOptStarAsHtml(b.userName + (b.isDisabled > 0 ? " (" + I18n.uzytkownik.get() + " " + I18n.wylaczony.get() + ")" : ""), BIKClientSingletons.isStarredBIKUser(b.userId));
    }

    @Override
    protected String getName(UserInNodeBean b) {
        throw new UnsupportedOperationException(I18n.toSieNigdyNieWywola.get() /* I18N:  */);
//        return null;
    }

    protected String getNodeKindCode(UserInNodeBean b) {
        return BIKClientSingletons.getNodeKindCodeById(b.userInRoleNodeKindId);
    }

    public String getBranchIds(UserInNodeBean b) {
        return b.userInRoleBranchIds;//b.userBranchIds;
    }

    @Override
    public String getIconUrl(UserInNodeBean b) {
        String avatarUrl;
        if (b.avatar != null) {
            avatarUrl = BIKClientSingletons.fixFileResourceUrl(b.avatar);
//            if (b.avatar.startsWith("file_")) {
//                avatarUrl = FoxyFileUpload.getWebAppUrlPrefixForUploadedFile("fsb?get=") + b.avatar;
//            } else {
//                avatarUrl = b.avatar;
//            }
        } else {
            avatarUrl = getIconUrlByNodeKindCode(getNodeKindCode(b), getTreeCode(b));
        }

        return avatarUrl;
    }

    @Override
    public int getAddColCnt() {
        return 1;
//        return BIKClientSingletons.isDescriptionInJoinedPanelVisible() ? 2 : 1;
    }

    @Override
    public Object getAddColVal(UserInNodeBean b, int addColIdx) {
//        if (BIKClientSingletons.isDescriptionInJoinedPanelVisible() && addColIdx == 0) {
//            return getDescription(b.descrPlainUser);
//        }

        List<Integer> nodeIdsFromBranch = BIKCenterUtils.splitBranchIdsEx(b.userInRoleBranchIds, false);
        List<String> nodeNames = new ArrayList<String>();
        for (Integer id : nodeIdsFromBranch) {
            if (nodeNamesById.get(id) != null && b.userInRoleNodeId != id) {
                nodeNames.add(nodeNamesById.get(id));
            }
        }
        pathStr = BaseUtils.mergeWithSepEx(nodeNames, BIKGWTConstants.RAQUO_STR_SPACED);
        return pathStr + (b.isAuxiliary ? " (" + I18n.pomocniczy.get() + ")" : "") // " (" + (!b.isAuxiliary ? I18n.glowny.get() /* I18N:  */ : I18n.pomocniczy.get() /* I18N:  */) + ")"
                + (b.inheritToDescendants /*== 0*/ ? " *" : "");
    }

    @Override
    protected List<IGridBeanAction<UserInNodeBean>> createActionList() {
        List<IGridBeanAction<UserInNodeBean>> res = new ArrayList<IGridBeanAction<UserInNodeBean>>();
//        res.add(nodeAwareActEdit);
        res.add(actFollow);
        res.add(actDelete);
        return res;
    }

    @Override
    public boolean isDeleteBtnVisible2Wtf(UserInNodeBean b) {
        return b.nodeId == currentNodeId;
    }

    @Override
    public boolean isDeleteBtnVisible(UserInNodeBean b) {
        return !isUsingOnDialog && !BaseUtils.safeEquals(b.roleCode, BIKGWTConstants.USER_KIND_CODE_AUTHOR) && !b.isAutomaticJoined;
    }

    @Override
    protected void execActEdit(final UserInNodeBean b) {

        BIKClientSingletons.getService().getBikAllUsers(
                new StandardAsyncCallback<List<UserBean>>("Failure on" /* I18N: no */ + " getUsers") {
                    public void onSuccess(List<UserBean> result) {

                        final Map<Integer, UserBean> users = new HashMap<Integer, UserBean>();
                        for (UserBean r : result) {
                            users.put(r.nodeId, r);
                        }
                        final TreeSelectorForUsers tsfjod = new TreeSelectorForUsers();

                        tsfjod.buildAndShowDialog(currentNodeId, //"Users" /* I18N: no */,
                                new IParametrizedContinuation<TreeNodeBean>() {
                                    public void doIt(TreeNodeBean param) {
                                        execSetUserInNodeAndRefresh(b, users.get(BaseUtils.nullToDef(param.linkedNodeId, param.id)),
                                                BaseUtils.booleanToInt(tsfjod.isInheritToDescendantsChecked()), I18n.zmienionoUzytkownika.get() /* I18N:  */);
                                    }
                                }, null, true, null);
                    }
                });
    }

    protected void execSetUserInNodeAndRefresh(UserInNodeBean uinb, UserBean ub, int inheritToDescendants, String notification) {
//        BIKClientSingletons.getService().deleteUserInNode(currentNodeId, uinb.userId, uinb.roleForNodeId, getDoNothingCallback());
        BIKClientSingletons.getService().deleteUserInNode(currentNodeId, uinb.userId, uinb.roleForNodeId, new StandardAsyncCallback<Boolean>(notification) {
            public void onSuccess(Boolean result) {
                BIKClientSingletons.getTabSelector().invalidateNodeData(currentNodeId);
                BIKClientSingletons.refreshAndRedisplayDataUsersTree(result);
            }
        });
        if (ub == null) {
            deleteBeanFromList(uinb, notification);
        } else {
            uinb.loginName = ub.loginName;
            uinb.avatar = ub.avatar;
            uinb.userName = ub.name;
            uinb.userNodeId = ub.nodeId;

            refreshView(notification);
        }
    }

    @Override
    protected void execActDelete(final UserInNodeBean b) {
        execSetUserInNodeAndRefresh(b, null, 0, I18n.usunietoPowiazanie.get() /* I18N:  */);
    }

    @Override
    protected void execActFollow(final UserInNodeBean b) {
        BIKClientSingletons.getTabSelector().submitNewTabSelection(getTreeCode(b), b.userInRoleNodeId, true);
    }

    @Override
    public boolean hasNameHints() {
        return false;
    }

    @Override
    public int howManyTimesNameColumnWiderThanOther() {
        return 2;
    }

    @Override
    public String getOptExtraStyleForRow(UserInNodeBean b) {
        return BaseUtils.safeToStringNotNull(super.getOptExtraStyleForRow(b)) + (b.isDisabled > 0 ? " userDisabled" : "");
    }

}
