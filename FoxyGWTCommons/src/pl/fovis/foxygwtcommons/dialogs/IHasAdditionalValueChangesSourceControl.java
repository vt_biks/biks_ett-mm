/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author pmielanczuk
 */
public interface IHasAdditionalValueChangesSourceControl {

    public Widget getAdditionalValueChangesSourceControl();
}
