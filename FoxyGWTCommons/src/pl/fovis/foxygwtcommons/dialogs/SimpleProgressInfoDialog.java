/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import pl.fovis.foxygwtcommons.DialogCaption;

/**
 *
 * @author pmielanczuk
 * Dialog bez przycisków, bez zamykania itp. - ma na celu pokazanie okienka,
 * które tylko pokazuje prace w toku i nie można go zamykać
 * (ma pod spodem glass)
 */
public class SimpleProgressInfoDialog extends BaseActionOrCancelDialog {

    protected String optCaption;
    protected String htmlOrTxt;
    protected boolean isPlainText;

    @Override
    protected String getDialogCaption() {
        return optCaption;
    }

    @Override
    protected String getActionButtonCaption() {
        return null;
    }

    @Override
    protected void buildMainWidgets() {
        main.add(isPlainText ? new Label(htmlOrTxt) : new HTML(htmlOrTxt));
    }

    @Override
    protected void doAction() {
        // no-op
    }

    @Override
    protected boolean isKeyEscapeCloseWindow() {
        return false;
    }

    public void buildAndShowDialog(String optCaption, String htmlOrTxt, boolean isPlainText) {
        this.optCaption = optCaption;
        this.htmlOrTxt = htmlOrTxt;
        this.isPlainText = isPlainText;
        super.buildAndShowDialog();
    }

    @Override
    public void hideDialog() {
        super.hideDialog();
    }

    @Override
    protected HorizontalPanel buildAndAddBottomWidgets() {
        return null;
    }

    @Override
    protected DialogCaption getDialogCaptionElem(HorizontalPanel bottomBtns) {
        return new DialogCaption() {
            {
                captionHtml = new HTML(optCaption);
                fl.add(captionHtml);
                fl.addStyleName("Caption" /* I18N: no */);
            }
        };
    }

    @Override
    protected boolean doActionBeforeClose() {
        return false;
    }

    @Override
    protected boolean doCancelBeforeClose() {
        return false;
    }
}
