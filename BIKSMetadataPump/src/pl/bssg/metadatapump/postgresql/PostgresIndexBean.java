/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.postgresql;

/**
 *
 * @author tflorczak
 */
public class PostgresIndexBean {

    public String name;
    public String server;
    public String schema;
    public String databaseName;
    public String tableName;
    public String columnName;
    public int columnPosition;
    public boolean isUnique;
    public String type;
    public boolean isPrimaryKey;

    public PostgresIndexBean(String name, String server, String schema, String databaseName, String tableName, String columnName, int columnPosition, boolean isUnique, String type, boolean isPrimaryKey) {
        this.name = name;
        this.server = server;
        this.schema = schema;
        this.databaseName = databaseName;
        this.tableName = tableName;
        this.columnName = columnName;
        this.columnPosition = columnPosition;
        this.isUnique = isUnique;
        this.type = type;
        this.isPrimaryKey = isPrimaryKey;
    }
}
