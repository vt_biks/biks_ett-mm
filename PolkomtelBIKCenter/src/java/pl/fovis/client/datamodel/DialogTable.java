/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.datamodel;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author pgajda
 */
public class DialogTable extends DecoratorPanel //implements MouseDownHandler, MouseUpHandler, MouseOverHandler, MouseMoveHandler, MouseOutHandler 
{

    protected Label tableName;
    protected VerticalPanel panel;
    protected FlexTable columnsTable;
    protected List<DialogTableConnector> tableconnectors = new ArrayList<DialogTableConnector>();
    protected HashMap<String, DialogTable.ColumnTable> tableColumnsWiget = new HashMap<String, DialogTable.ColumnTable>();
    protected DataModel dataModel;
    protected int positionLeft;
    protected int positionTop;

    public DialogTable(DataModel dataModel) {
        super();
        this.dataModel = dataModel;
        initComponents();
    }

    public DialogTable(DataModel dataModel, String tableName, String tableComment, String objId) {
        this(dataModel);
        setComponents(tableName, tableComment, objId);

    }

    public void setPosition(int x, int y) {
        positionLeft = x;
        positionTop = y;
    }

    public int getPositionLeft() {
        return positionLeft;
    }

    public int getPositionTop() {
        return positionTop;
    }

    private void initComponents() {
        setStyleName("dialogTable");
        DOM.sinkEvents(getElement(), DOM.getEventsSunk(getElement()) | Event.MOUSEEVENTS);

        panel = new VerticalPanel();
        panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        setWidget(panel);

        tableName = new Label();
        tableName.setStyleName("dialogTitle");
        tableName.setWordWrap(false);
        tableName.getElement().getStyle().setFontSize(DataModel.FONT_SIZE * dataModel.getScale(), Style.Unit.PX);

        panel.add(tableName);
        panel.setCellWidth(tableName, "100%");

        columnsTable = new FlexTable();
        columnsTable.setStyleName("columnsTable");
        columnsTable.setCellPadding(0);
        columnsTable.setCellSpacing(0);
        columnsTable.setWidth("100%");
        panel.add(columnsTable);
        panel.setCellWidth(columnsTable, "100%");

        dataModel.getWindowController().getPickupDragController().makeDraggable(this, tableName);
        dataModel.getWindowController().getPickupDragController().setBehaviorDragStartSensitivity(1);
    }

    public void addTableConnector(DialogTableConnector dtc) {
        tableconnectors.add(dtc);
    }

    protected void doOnDragEnd() {
        for (DialogTableConnector dtc : tableconnectors) {
            dtc.drawConnection();
        }
    }

    public void setActive(boolean isActive) {
        addStyleDependentName("active");
    }

    private void setComponents(String tableName, String tableComment, String objId) {
        setTableTitle(tableName, tableComment, objId);
    }

    public void setTableTitle(String tableName, String tableComment, final String objId) {
        this.tableName.setText(tableName);
        this.tableName.setTitle(tableName + (tableComment != null ? (" : " + tableComment) : ""));
        if (objId != null) {
            this.tableName.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    changeNode(objId);
                }
            });
        }

    }

    public String getTableName() {
        return tableName.getText();
    }

    public Widget addTableColumn(String colLongId, String colName, String colComment, final String objId, boolean isActive) {

        DialogTable.ColumnTable html = new DialogTable.ColumnTable(SafeHtmlUtils.htmlEscape(colName));
        html.setStyleName("dialogTableColumn");
        if (isActive) {
            html.addStyleDependentName("active");
        }
        html.setWordWrap(false);
        html.setTitle(colName + (colComment != null ? (" : " + colComment) : ""));
        html.getElement().getStyle().setFontSize(DataModel.FONT_SIZE * dataModel.getScale(), Style.Unit.PX);
        html.getElement().getStyle().setMarginBottom(-3 - (1 - dataModel.getScale()) * DataModel.FONT_SIZE / 2.0, Style.Unit.PX);
        html.getElement().getStyle().setMarginTop(-3 - (1 - dataModel.getScale()) * DataModel.FONT_SIZE / 2.0, Style.Unit.PX);

        if (objId != null) {
            html.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    changeNode(objId);
                }
            });
        }

        columnsTable.setWidget(columnsTable.getRowCount(), 0, html);
        tableColumnsWiget.put(colLongId, html);
        return html;
    }

    protected void changeNode(String objId) {

        BIKClientSingletons.getService().getNodeId(objId, BIKConstants.TREE_CODE_TERADATADATAMODEL,
                new StandardAsyncCallback<Integer>("error in" /* I18N: no:err-fail-in */ + " sDialogTable addClickHandler") {
                    @Override
                    public void onSuccess(Integer result) {
                        if (result != null) {
                            BIKClientSingletons.getTabSelector().submitNewTabSelection("TeradataDataModel", result, true);
                        }
                    }
                }
        );
    }

    public Widget getColumnWidget(String columntLongId) {
        return tableColumnsWiget.get(columntLongId);
    }

    public String getColumnName(String columntLongId) {
        DialogTable.ColumnTable ct = tableColumnsWiget.get(columntLongId);
        if (ct != null) {
            return ct.getColumnTableName();
        }
        return "";
    }

    class ColumnTable extends HTML {

        protected String columnName;

        public ColumnTable(String columnName) {
            super(columnName);
            this.columnName = columnName;
        }

        public String getColumnTableName() {
            return columnName;
        }
    }
}
