exec sp_check_version '1.0.41';

create table bik_authors(
	id int not null IDENTITY(1,1) primary key,
	node_id int not null references bik_node(id),
	user_id int not null references bik_user(id),
	constraint uk_bik_authors unique (node_id, user_id)
);
go

exec sp_update_version '1.0.41', '1.0.42';
go
