/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill.tags;

import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.exprlang.IParsedExpression;
import pl.foxys.foxymill.renderctx.IRenderCtx;

/**
 *
 * @author wezyr
 */
public class IfTag extends TagBase {

    private IParsedExpression condExpr;
    protected boolean followedByElse;
    
    @Override
    protected void prepareSelf() {
        condExpr = attrToReqExpr("cond"); //exprLang.parseExpression(cond);
    }

    public void internalRender(//IRenderContext
            IRenderCtx renderCtx) {
        boolean bVal = FoxyMillUtils.evalCondExpr(condExpr, renderCtx);
        if (followedByElse) {
            renderCtx.pushIfCondVal(bVal);
        }
        if (bVal) {
            FoxyMillUtils.render(subTags, renderCtx);
        }
    }
}
