/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class DataQualityRequestBean implements Serializable {

    public long id;
    public long ordinalId;
    public Long caseCount;
    public Long errorCount;
    public Date startTimestamp;
    public int testId;
    public int testNodeId;
    public Double errorPercentage;
    public int passed;
    public Date endTimestamp;
    public int isManual;
    public int isDeleted;
    public String xlsxFileName;
    public String csvFileName;
    // z testu
    public Double errorThreshold;
    // czy można pobrać plik z błędami
    public boolean hasErrorsFile;
    // nazwa testu
    public String testName;

    public DataQualityRequestBean() {
    }

    public DataQualityRequestBean(int testId, int testNodeId, int isManual, Date startTimestamp) {
        this.testId = testId;
        this.testNodeId = testNodeId;
        this.isManual = isManual;
        this.startTimestamp = startTimestamp;
    }

}
