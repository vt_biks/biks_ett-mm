﻿declare @zIluOstatnichDni varchar(10) = '60';

declare @sqltext varchar(max) = '';

select @sqltext = @sqltext + case when @sqltext = '' then '' else 'union' end + '
            select us.login_name as logowali
            from [' + name + ']..bik_system_user us
            inner join [' + name + ']..bik_statistic_ext ex on ex.user_id = us.id
            where ex.event_datetime >= dateadd(day, -' + @zIluOstatnichDni + ', getdate())
            union
            select us.login_name
            from [' + name + ']..bik_system_user us
            inner join [' + name + ']..bik_statistic ex on ex.user_id = us.id
            where ex.event_datetime >= dateadd(day, -' + @zIluOstatnichDni + ', getdate())
            union
            select us.login_name
            from [' + name + ']..bik_system_user us
            inner join [' + name + ']..bik_object_in_history ex on ex.user_id = us.id
            where ex.date_added >= dateadd(day, -' + @zIluOstatnichDni + ', getdate())
            '
from sys.databases where name not in ('master', 'tempdb', 'model', 'msdb', 'MasterBIKS')



declare @temp_tab_name_base sysname = 'create table ##biks_raport_dla_lukasza (login varchar(max) collate DATABASE_DEFAULT)';
exec(@temp_tab_name_base)

set @sqltext = 'insert into ##biks_raport_dla_lukasza(login) ' + @sqltext;
exec(@sqltext)

exec ('select b.* from ##biks_raport_dla_lukasza as a
inner join MasterBIKS..mltx_registered_user as b on a.login collate DATABASE_DEFAULT = b.email collate DATABASE_DEFAULT')


drop table ##biks_raport_dla_lukasza