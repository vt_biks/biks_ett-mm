/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author wezyr
 */
public interface IRegExpMatcher extends IStringMatcher {

//    public boolean find();
//
//    public int start();
//
//    public int end();
//---
//    public int start(int grp);
//    public int end(int grp);
    public String group(int grp);

    public String replaceAll(String replacement);
}
