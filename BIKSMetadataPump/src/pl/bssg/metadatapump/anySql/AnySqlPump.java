/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.anySql;

import java.util.List;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.common.ParametersAnySqlBean;
import pl.bssg.metadatapump.common.PumpConstants;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class AnySqlPump extends MetadataPumpBase {

    protected List<ParametersAnySqlBean> configs;

    @Override
    protected Pair<Integer, String> startPump() {
        pumpLogger.pumpLog("Start: " + PumpConstants.SOURCE_NAME_BIKS_SQL, 10);
        try {
            configs = adhocDao.createBeansFromNamedQry("getAnySqlForSqlName", ParametersAnySqlBean.class, instanceName);
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, " Error: " + e.getMessage());
        }

        for (ParametersAnySqlBean config : configs) {
            try {
                if (BaseUtils.isStrEmptyOrWhiteSpace(config.query)) {
                    return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Empty query");
                }
                adhocDao.execNamedCommand("execAnySql", config.query);
            } catch (Exception e) {
                return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, e.getMessage());
            }
        }
        return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_BIKS_SQL;
    }

}
