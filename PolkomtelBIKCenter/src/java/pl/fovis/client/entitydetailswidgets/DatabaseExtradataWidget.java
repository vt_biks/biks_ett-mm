/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.BikIcon;
import static pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase.createFollowCH;
import static pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase.createHeaders;
import static pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase.createPostSpacing;
import static pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase.createRow;
import static pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase.setColumnWidth;
import pl.fovis.common.DBColumnBean;
import pl.fovis.common.DBDependencyBean;
import pl.fovis.common.DBDependentObjectBean;
import pl.fovis.common.DBForeignKeyBean;
import pl.fovis.common.DBIndexBean;
import pl.fovis.common.DBPartitionBean;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class DatabaseExtradataWidget extends EntityDetailsWidgetFlatBase {

    protected VerticalPanel mainVp;
    protected IMainHeaderSharedTable imhst;
    public final static String CLUSTRED_INDEX_TYPE = "CLUSTERED";
    public final static String NON_CLUSTRED_INDEX_TYPE = "NONCLUSTERED";

    @Override
    public Widget buildWidgets() {
        mainVp = new VerticalPanel();
        mainVp.setWidth("100%");
        return mainVp;
    }

    @Override
    public void displayData() {
        mainVp.clear();
        List<DBColumnBean> columnExtradata = getColumnExtradata();
        List<DBForeignKeyBean> foreignKeyExtradata = getForeignKeyExtradata();
        List<DBPartitionBean> partitionExtradata = getPartitionExtradata();
        List<DBDependencyBean> dependencyExtradata = getDependencyExtradata();
        List<DBDependentObjectBean> dependentObjectExtradata = getDependentObjectExtradata();
        List<DBIndexBean> indexExtradata = getIndexExtradata();
        boolean hasPartitionExtradata = !BaseUtils.isCollectionEmpty(partitionExtradata);
        boolean isTableOrView = isTableOrView();

        if (hasPartitionExtradata) {
            DBPartitionBean column = partitionExtradata.get(0); // partycjonowanie możliwe tylko po 1 kolumnie
            HorizontalPanel hp = new HorizontalPanel();
            hp.add(new HTML("<B>" + I18n.partycjonowanePo.get() + "&nbsp;</B>"));
            hp.add(BIKClientSingletons.createLinkAsAnchor(column.columnName, column.treeCode, column.nodeId));
            mainVp.add(hp);
            mainVp.add(createPostSpacing());
        }

        if (!BaseUtils.isCollectionEmpty(columnExtradata)) {
            FlexTable gp = new FlexTable();
            gp.setStyleName("gridJoinedObj");
//            createHeaders(gp, I18n.nazwa.get(), I18n.typDanych.get(), I18n.allowNulls.get(), I18n.identity.get(), I18n.domyslnie.get(), I18n.opis.get());
            createHeaders(gp, I18n.nazwa.get(), I18n.typDanych.get(), I18n.allowNulls.get(),/* I18n.identity.get(), I18n.domyslnie.get(),*/ I18n.opis.get());
            int indexCount = 1;
            for (DBColumnBean column : columnExtradata) {
                Widget nameWidget = new HTML(column.name);
                if (isTableOrView) {
                    nameWidget = BIKClientSingletons.createLinkAsAnchor(column.name, column.treeCode, column.nodeId);
                }
                createRow(gp, indexCount++, true, isTableOrView,
                        column.seedValue != null ? new InlineHTML("<img src='images/" + column.icon + ".gif' " + "style=\"width: 16px; height: 16px;\" border=\"0\" class=\"treeIcon\"/><img src='images/star.png' style=\"width: 9px; height: 9px; margin-left: -9px\" border=\"0\" class=\"treeIconP\"/>") : BikIcon.createIcon("images/" + column.icon + ".gif" /* I18N: no */, "RelatedObj-ObjIcon"),
                        nameWidget,
                        new HTML(column.type),
                        BikIcon.createIcon(!column.isNullable ? "images/delete.png" /* I18N: no */ : "images/check2.png" /* I18N: no */, "RelatedObj-ObjIcon"),
                        //new HTML(column.seedValue != null ? column.seedValue + " - " + column.incrementValue : ""),
                        //new HTML(column.defaultField),
                        new HTML(column.description),
                        BIKClientSingletons.createActionButton("link_arrows", I18n.przejdz.get() /* I18N:  */, createFollowCH(column.treeCode, column.nodeId)));
            }
            gp.setWidth("100%");
//            setColumnWidth(gp, 2, 15, 10, 5, 10, 10, 48);
            if (isTableOrView) {
                setColumnWidth(gp, 2, 25, 10, 5, /*10, 10,*/ 56, 2);
            } else {
                setColumnWidth(gp, 2, 25, 10, 5, /*10, 10,*/ 58);
            }

            if (isTableOrView) {
                mainVp.add(createHeader(I18n.columns.get()));
            }
            mainVp.add(gp);
            mainVp.add(createPostSpacing());
        }

        if (!BaseUtils.isCollectionEmpty(indexExtradata)) {
            FlexTable gp = new FlexTable();
            gp.setStyleName("gridJoinedObj");
            createHeaders(gp, hasPartitionExtradata, I18n.nazwa.get(), I18n.typ.get(), I18n.kolumny.get(), I18n.partycjonowanyPo.get());
            List<DBIndexBean> indexes = createIndexRows(indexExtradata);
            int indexCount = 1;
            for (DBIndexBean index : indexes) {
                createRow(gp, indexCount++, true, hasPartitionExtradata,
                        BikIcon.createIcon(index.icon, "RelatedObj-ObjIcon"),
                        new HTML(index.name),
                        new HTML(index.type),
                        new HTML(index.columnName), // kolumny, oddzielone przecinkiem
                        new HTML(index.partitionColumnName));
            }
            gp.setWidth("100%");
            if (hasPartitionExtradata) {
                setColumnWidth(gp, 2, 25, 25, 33, 15);
            } else {
                setColumnWidth(gp, 2, 28, 25, 45);
            }

            mainVp.add(createHeader(I18n.indexy.get()));
            mainVp.add(gp);
            mainVp.add(createPostSpacing());
        }

        if (!BaseUtils.isCollectionEmpty(foreignKeyExtradata)) {
            FlexTable gp = new FlexTable();
            gp.setStyleName("gridJoinedObj");
            createHeaders(gp, I18n.nazwa.get(), I18n.columns.get()/*, I18n.opis.get()*/);
            int indexCount = 1;
            for (DBForeignKeyBean foreignKey : foreignKeyExtradata) {
                HorizontalPanel hp = new HorizontalPanel();
                hp.add(new HTML(foreignKey.fkColumn + "&nbsp;->&nbsp;"));
                hp.add(BIKClientSingletons.createLinkAsAnchor("[" + foreignKey.pkSchema + "].[" + foreignKey.pkTable + "]", foreignKey.pkTreeCode, foreignKey.pkTableNodeId));
                hp.add(BIKClientSingletons.createLinkAsAnchor(".[" + foreignKey.pkColumn + "]", foreignKey.pkTreeCode, foreignKey.pkColumnNodeId));
                createRow(gp, indexCount++,
                        BikIcon.createIcon(getFKColumnIconName(), "RelatedObj-ObjIcon"),
                        new HTML(foreignKey.name),
                        hp/*,
                 new HTML(foreignKey.description)*/);
            }
            gp.setWidth("100%");
//            setColumnWidth(gp, 5, 25, 40, 30);
            setColumnWidth(gp, 2, 33, 65);

            mainVp.add(createHeader(I18n.kluczeObce.get()));
            mainVp.add(gp);
            mainVp.add(createPostSpacing());
        }

        if (!BaseUtils.isCollectionEmpty(dependencyExtradata)) {
            FlexTable gp = new FlexTable();
            gp.setStyleName("gridJoinedObj");
            createHeaders(gp, I18n.nazwaObiektu.get(), I18n.typ.get());
            int indexCount = 1;
            for (DBDependencyBean dep : dependencyExtradata) {
                createRow(gp, indexCount++,
                        BikIcon.createIcon("images/" + dep.typeAsIconName + ".gif" /* I18N: no */, "RelatedObj-ObjIcon"),
                        BIKClientSingletons.createLinkAsAnchor("[" + dep.owner + "].[" + dep.name + "]", dep.treeCode, dep.nodeId),
                        new HTML(dep.type),
                        BIKClientSingletons.createActionButton("link_arrows", I18n.przejdz.get() /* I18N:  */, createFollowCH(dep.treeCode, dep.nodeId)));
            }
            gp.setWidth("100%");
            setColumnWidth(gp, 2, 74, 22, 2);

            mainVp.add(createHeader(I18n.dependencies.get()));
            mainVp.add(gp);
            mainVp.add(createPostSpacing());
        }

        if (!BaseUtils.isCollectionEmpty(dependentObjectExtradata)) {
            FlexTable gp = new FlexTable();
            gp.setStyleName("gridJoinedObj");
            createHeaders(gp, I18n.nazwaObiektu.get(), I18n.typ.get());
            int indexCount = 1;
            for (DBDependentObjectBean dep : dependentObjectExtradata) {
                HorizontalPanel depHP = new HorizontalPanel();
                depHP.add(BIKClientSingletons.createLinkAsAnchor("[" + dep.sourceOwner + "].[" + dep.sourceName + "]", dep.treeCode, dep.sourceNodeId));
                if (dep.type == 1) { // dla FK
                    depHP.add(BIKClientSingletons.createLinkAsAnchor(".[" + dep.sourceColumn + "]", dep.treeCode, dep.sourceColumnNodeId));
                }
                depHP.add(new HTML("&nbsp;->&nbsp;[" + dep.dstOwner + "].[" + dep.dstName + "]"));
                if (dep.type == 1) { // dla FK
                    depHP.add(new HTML(".[" + dep.dstColumn + "]"));
                }
                createRow(gp, indexCount++,
                        BikIcon.createIcon(dep.type == 1 ? getFKColumnIconName() : "images/" + dep.sourceIcon + ".gif" /* I18N: no */, "RelatedObj-ObjIcon"),
                        depHP,
                        new HTML(dep.type == 0 ? I18n.obiektyZalezneDep.get() : I18n.obiektyZalezneFK.get()),
                        BIKClientSingletons.createActionButton("link_arrows", I18n.przejdz.get() /* I18N:  */, createFollowCH(dep.treeCode, dep.sourceNodeId)));
            }
            gp.setWidth("100%");
            setColumnWidth(gp, 2, 77, 19, 2);

            mainVp.add(createHeader(I18n.obiektyZalezne.get()));
            mainVp.add(gp);
            mainVp.add(createPostSpacing());
        }

        addAdditionaryRow();
    }

    @Override
    public boolean hasContent() {
        EntityDetailsDataBean data = getData();
        return treeIdFilter() && (data.dbColumnExtradata != null || data.dbForeignKeyExtradata != null || data.dbPartitionExtradata != null || data.dbDependencyExtradata != null || data.dbIndices != null);
    }

    protected List<DBIndexBean> createIndexRows(List<DBIndexBean> indexExtradata) {
        List<DBIndexBean> resultList = new ArrayList<DBIndexBean>();
        if (indexExtradata.isEmpty()) {
            return resultList;
        }
        String prevIndexName = indexExtradata.get(0).name;
        String prevType = null;
        String prevIcon = null;
        String prevPartition = "";
        boolean shouldLoadNames = true;
        List<String> columns = new ArrayList<String>();
        for (DBIndexBean dBIndexBean : indexExtradata) {
            if (!dBIndexBean.name.equals(prevIndexName)) {
                resultList.add(new DBIndexBean(prevIndexName, prevType, BaseUtils.mergeWithSepEx(columns, ", "), prevPartition, prevIcon));
                columns.clear();
                prevPartition = "";
                shouldLoadNames = true;
            }
            columns.add(dBIndexBean.columnName);
            if (dBIndexBean.partitionOrdinal != 0) {
                prevPartition = dBIndexBean.columnName;
            }
            if (shouldLoadNames) {
                StringBuilder sb = new StringBuilder();
                if (dBIndexBean.isPrimaryKey) {
                    sb.append("Primary key, " /* I18N: no */);
                }
                String uniqueFlag = dBIndexBean.isUnique ? "Unique" : "Non-Unique";
                if (dBIndexBean.type != null) {
                    sb.append(dBIndexBean.type).append(" (").append(uniqueFlag).append(")");
                } else {
                    sb.append(uniqueFlag);
                }
                if (!BaseUtils.isAnyStringEmptyOrWhiteSpace(dBIndexBean.descend)) {
                    sb.append(" ").append(dBIndexBean.descend);
                }
                prevType = sb.toString();
                prevIndexName = dBIndexBean.name;
                prevIcon = CLUSTRED_INDEX_TYPE.equals(dBIndexBean.type) ? getClusteredIndexIconName() : getNonClusteredIndexIconName();
                shouldLoadNames = false;
            }
        }
        resultList.add(new DBIndexBean(prevIndexName, prevType, BaseUtils.mergeWithSepEx(columns, ", "), prevPartition, prevIcon));
        return resultList;
    }

    protected void addAdditionaryRow() {
        // no op
    }

    protected List<DBColumnBean> getColumnExtradata() {
        return getData().dbColumnExtradata;
    }

    protected List<DBForeignKeyBean> getForeignKeyExtradata() {
        return getData().dbForeignKeyExtradata;
    }

    protected List<DBPartitionBean> getPartitionExtradata() {
        return getData().dbPartitionExtradata;
    }

    protected List<DBDependencyBean> getDependencyExtradata() {
        return getData().dbDependencyExtradata;
    }

    protected List<DBDependentObjectBean> getDependentObjectExtradata() {
        return getData().dbDependentObjectsExtradata;
    }

    protected List<DBIndexBean> getIndexExtradata() {
        return getData().dbIndices;
    }

    protected boolean isTableOrView() {
        return false;
    }

    protected String getFKColumnIconName() {
        return "images/mssqlColumnFK.gif";
    }

    protected String getClusteredIndexIconName() {
        return "images/mssqlClustredIndex.gif";
    }

    protected String getNonClusteredIndexIconName() {
        return "images/mssqlNonClustredIndex.gif";
    }

    protected boolean treeIdFilter() {
        return true;
    }
}
