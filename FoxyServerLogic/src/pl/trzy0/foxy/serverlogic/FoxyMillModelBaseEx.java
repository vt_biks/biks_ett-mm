/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import java.util.HashMap;
import pl.foxys.foxymill.IExceptionHandlingModel;
import pl.foxys.foxymill.IRenderingAbortAwareModel;
import pl.foxys.foxymill.IRerenderCapableModel;
import simplelib.RawString;
import simplelib.BaseUtils;
import simplelib.JsonConverter;
import simplelib.MapHelper;
import simplelib.StringEscapeUtils;

/**
 *
 * @author wezyr
 */
public class FoxyMillModelBaseEx implements IRenderingAbortAwareModel, IRerenderCapableModel, IExceptionHandlingModel {
//    private Properties props;

    public MapHelper<String> sysProps;
    protected Boolean returnFromRenderingFlag;
    private MapHelper<String> renderVars;
    protected Object rerenderModel;
    protected String forceDispatcherToGlobalName;
    
//    public FoxyMillModelBaseEx(Properties props) {
//        this.props = props;
//    }
//
    public FoxyMillModelBaseEx() {
    }

    public FoxyMillModelBaseEx(MapHelper<String> sysProps) {
        init(sysProps);
    }

    public Object raw(String str) {
        return new RawString(str);
    }

    public void init(MapHelper<String> sysProps) {
        this.sysProps = sysProps;
    }

    public String getSysProp(String name) {
        return sysProps.getString(name);
    }

    public String getSysProp(String name, String defVal) {
        return sysProps.getString(name, defVal);
    }

//    public String render(IRenderable r, Map<String, Object> params) {
//        return FoxyMillUtils.renderWithParams(r, params, this);
//    }
    public <T> T nullToDef(T... vals) {
        for (T val : vals) {
            if (val != null) {
                return val;
            }
        }
        return null;
    }

    public boolean isStrEmpty(String s) {
        return BaseUtils.isStrEmpty(s);
    }

    public boolean isStrEmptyOrWhiteSpace(String s) {
        return BaseUtils.isStrEmptyOrWhiteSpace(s);
    }

    public String strEmptyOrWhiteSpaceToDef(String s, String def) {
        return BaseUtils.isStrEmptyOrWhiteSpace(s) ? def : s;
    }

    public void abortRendering() {
        returnFromRenderingFlag = true;
    }

    public void finishRendering() {
        // set to false (not null)
        returnFromRenderingFlag = false;
    }

    // 3 values are recognized:
    // null  -> do nothing, proceed with rendering
    // true  -> abort and discard generated content
    // false -> finish but keep already generated content
    public Boolean getReturnFromRenderingFlag() {
        return returnFromRenderingFlag;
    }

    private void ensureRenderVars() {
        if (renderVars == null) {
            renderVars = new MapHelper<String>(new HashMap<String, Object>());
        }
    }

    public MapHelper<String> getRenderVars() {
        ensureRenderVars();
        return renderVars;
    }

    public void setRenderVar(String name, Object val) {
        ensureRenderVars();
        renderVars.helperAsMap().put(name, val);
    }

    public void exec(Object ooo) {
        // no-op
    }

    public Object formatAnyBody(String body, boolean isRichtext) {
        if (body == null) {
            return null;
        }

        return raw(isRichtext ? body
                : BaseUtils.encodeForHTMLWithNewLinesAsBRs(body));
    }

    public Object getRerenderModel() {
        return rerenderModel;
    }

    public Object escapeStringForJSNoQuotes(String s) {
        return raw(StringEscapeUtils.escapeJavaScript(s));
    }

    public Object escapeForJS(Object o) {
        return raw(JsonConverter.convertObject(o));
    }

    public Object escapeJSWithDQ(Object o) {
        return raw("\"" + StringEscapeUtils.escapeJavaScript(BaseUtils.safeToString(o)) + "\"");
    }

    public void setHandleExceptionGlobalName(String globalName) {
        forceDispatcherToGlobalName = globalName;
    }

    public Object getRerenderOnExceptionModel() {
        return forceDispatcherToGlobalName != null ? this : null;
    }

    public String getForceDispatcherToGlobalName() {
        String res = forceDispatcherToGlobalName;
        forceDispatcherToGlobalName = null;
        return res;
    }
}
