/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import commonlib.UrlMakingUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import static pl.bssg.gwtservlet.FoxyGWTServiceServlet.isIeBrowser;
import pl.fovis.common.BIKConstants;
import simplelib.BaseUtils;
import simplelib.CSVWriter;
import simplelib.ILameCollector;
import simplelib.IParametrizedContinuation;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author bfechner
 */
public abstract class ExportManagerBase implements IExportManager {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    // do logowania z podklas
//    protected static final ILameLogger baseLogger = logger;
    protected List<String> allColumnNames;

    protected abstract Pair<String, List<Map<String, Object>>> getFileNamePrefixAndContent(HttpServletRequest req);

    protected abstract String getFileFormat(HttpServletRequest req);

    @Override
    public void handleExportRequest(HttpServletRequest req, HttpServletResponse resp) {
        //dla csv i xlsx
        String fileFormat = getFileFormat(req);
        ServletOutputStream output = null;
        try {
            output = resp.getOutputStream();
            req.setCharacterEncoding("utf8");
            if (BIKConstants.EXPORT_FORMAT_XLSX.equals(fileFormat)) {
                Pair<String, List<Map<String, Object>>> dataToExport = getFileNamePrefixAndContent(req);
                writeXlsxFormat(req, resp, dataToExport.v1, dataToExport.v2);
            } else if (BIKConstants.EXPORT_FORMAT_CSV.equals(fileFormat)) {

                Pair<String, IParametrizedContinuation<ILameCollector<Map<String, Object>>>> dataProducerEx = optGetFileNamePrefixAndContentProducer(req);

                if (dataProducerEx != null) {
                    writeCsvFormatWithProducer(req, resp, dataProducerEx.v2, dataProducerEx.v1, null);
                } else {
                    Pair<String, List<Map<String, Object>>> dataToExport = getFileNamePrefixAndContent(req);
                    writeCsvFormat(req, resp, dataToExport.v1, dataToExport.v2);
                }
            } else if (BIKConstants.EXPORT_FORMAT_HTML.equals(fileFormat)) {
                writeHtmlFormat(req, resp);
            } else if (BIKConstants.EXPORT_FORMAT_ZIP.equals(fileFormat)) {
                writeZipFormat(req, resp);
            } else if (BIKConstants.EXPORT_FORMAT_PDF.equals(fileFormat)) {
                writePdfFormat(req, resp);
            } else if (BIKConstants.JASPER_EXPORT_FORMAT_HTML.equals(fileFormat)) {
                writeJasperFormat(req, resp);
            } else {
                throw new Exception("File format is not supported - " + fileFormat);
            }
        } catch (Exception e) {
            System.out.println("Error when creating content and generate file: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    System.out.println("Error when closing ServletOutputStream: " + e.getMessage());
                }
            }
        }
    }

    public void writeXlsxFormat(final HttpServletRequest req, final HttpServletResponse resp, String filePrefix, List<Map<String, Object>> data) throws IOException {
        String contentDispositionVal = getContentDispositionVal(filePrefix, req, ".xlsx");
//            String contentType = "application/vnd.ms-excel"; // xwykle xls
        String contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        resp.setContentType(contentType);
        resp.setHeader("Content-disposition", contentDispositionVal);
        ServletOutputStream output = resp.getOutputStream();
        XlsxWriter writer = new XlsxWriter();
        writer.writeContentAndCloseWorkBook(data, output);
//        loadAllColumNames(data);
//        XSSFWorkbook wb = new XSSFWorkbook();
//        XSSFSheet sheet = wb.createSheet();
//        sheet.setAutobreaks(true);
//        sheet.setDefaultColumnWidth(20); // ?
//        int rowNum = 1;
//        createRowHead(sheet, data);
//        for (Map<String, Object> row : data) {
//            createRow(rowNum++, sheet, row);
//        }
//        ServletOutputStream output = resp.getOutputStream();
//        wb.write(output);
//        wb.close();
//        output.flush();
    }

    protected boolean isMemUsageLoggingEnabled() {
        return logger.isInfoEnabled();
    }

    protected void logMemUsageWithMessage(String msg) {
        logger.info(msg + ", memUsage=" + LameUtils.getMemoryDiagInfoCompact(false, ", "));
    }

    protected Pair<String, IParametrizedContinuation<ILameCollector<Map<String, Object>>>> optGetFileNamePrefixAndContentProducer(HttpServletRequest req) {
        return null; // not supported by default
    }

    public void writeCsvFormatWithProducer(final HttpServletRequest req, final HttpServletResponse resp,
            //INamedSqlsDAO adhocDao, NamedSqlCallParams nscp,
            IParametrizedContinuation<ILameCollector<Map<String, Object>>> processDataCont,
            String filePrefix, final List<String> optColNames) throws IOException {

        if (isMemUsageLoggingEnabled()) {
            logMemUsageWithMessage("handleCsvExportRequestLowMem: start");
        }

        final StringBuilder sb = new StringBuilder("\uFEFF");
        String contentDispositionVal = getContentDispositionVal(filePrefix, req, ".csv");
        String contentType = "text/csv";
        resp.setContentType(contentType);
        resp.setHeader("Content-disposition", contentDispositionVal);
        final ServletOutputStream output = resp.getOutputStream();
//        output.write(text.getBytes(Charset.forName("UTF-8")));
//        output.write(text.getBytes(Charset.forName("UTF-8")));

        ILameCollector<Map<String, Object>> collector = new ILameCollector<Map<String, Object>>() {

            CSVWriter writer = null;
            ILameCollector<Map<String, Object>> delegate = null;

            @Override
            public void add(Map<String, Object> item) {

                if (writer == null) {
                    List<String> colNames = optColNames != null ? optColNames : new ArrayList<String>(item.keySet());
                    writer = new CSVWriter(colNames, null);
                    delegate = writer.makeCollector(sb);
                }

                delegate.add(item);

                if (sb.length() > 20000) {
                    try {
                        output.write(sb.toString().getBytes(StandardCharsets.UTF_8));
                        output.flush();
                    } catch (IOException ex) {
                        throw new LameRuntimeException("error in collector.add", ex);
                    }
                    sb.setLength(0);
                }
            }
        };

//        adhocDao.execNamedQueryExCFN(nscp, collector, null);
        processDataCont.doIt(collector);

        if (sb.length() > 0) {
            output.write(sb.toString().getBytes(StandardCharsets.UTF_8));
            output.flush();
        }

        if (isMemUsageLoggingEnabled()) {
            logMemUsageWithMessage("handleCsvExportRequestLowMem: end");
        }
    }

    public void writeCsvFormat(final HttpServletRequest req, final HttpServletResponse resp, String filePrefix, List<Map<String, Object>> data) throws IOException {

        if (isMemUsageLoggingEnabled()) {
            logMemUsageWithMessage("handleCsvExportRequest: start");
        }

        loadAllColumNames(data);

        if (isMemUsageLoggingEnabled()) {
            logMemUsageWithMessage("handleCsvExportRequest: after loadAllColumNames");
        }

        CSVWriter writter = new CSVWriter(allColumnNames, data);

        if (isMemUsageLoggingEnabled()) {
            logMemUsageWithMessage("handleCsvExportRequest: CSVWriter created");
        }

        String text = '\uFEFF' + writter.getText();

        if (isMemUsageLoggingEnabled()) {
            logMemUsageWithMessage("handleCsvExportRequest: after writter.getText(), text.length=" + text.length());
        }

        String contentDispositionVal = getContentDispositionVal(filePrefix, req, ".csv");
        String contentType = "text/csv";
        resp.setContentType(contentType);
        resp.setHeader("Content-disposition", contentDispositionVal);
        ServletOutputStream output = resp.getOutputStream();
//        output.write(text.getBytes(Charset.forName("UTF-8")));
//        output.write(text.getBytes(Charset.forName("UTF-8")));
        output.write(text.getBytes(StandardCharsets.UTF_8));
        output.flush();

        if (isMemUsageLoggingEnabled()) {
            logMemUsageWithMessage("handleCsvExportRequest: end");
        }
    }

    public static String getContentDispositionVal(String filePrefix, final HttpServletRequest req, String fileSuffix) {
        return getContentDispositionValNew(filePrefix, req, fileSuffix, false);
    }

    public static String getContentDispositionValNew(String filePrefix, final HttpServletRequest req, String fileSuffix, boolean encode) {
        String contentDispositionVal = "attachment; filename";
        String fileName = filePrefix + fileSuffix;
//        fileName = fileName.substring(0, 200);

        if (encode) {
            fileName = UrlMakingUtils.encodeStringForUrlWW(fileName).replace("+", "%20");
        }
        boolean isIeBrowser = isIeBrowser(req);
        if (isIeBrowser) {
            contentDispositionVal += "=\"" + fileName + "\"";
        } else {
//            contentDispositionVal += "*=UTF-8\'\'" + fileName + "\"";
            contentDispositionVal += "=\"" + fileName + "\"";
        }
        return contentDispositionVal;
    }

    protected void createRowHead(XSSFSheet sheet, List<Map<String, Object>> data) {
        XSSFRow rowHead = sheet.createRow((short) 0);

        for (int nCol = 0; nCol < allColumnNames.size(); nCol++) {
            rowHead.createCell(nCol).setCellValue(String.valueOf(allColumnNames.get(nCol)));
        }
    }

    protected void createRow(int rowNum, XSSFSheet sheet, Map<String, Object> row) {
        XSSFRow rowHead = sheet.createRow(/*(short) */rowNum);
        for (Entry<String, Object> e : row.entrySet()) {
            String colName = e.getKey();
            rowHead.createCell(allColumnNames.indexOf(colName)).setCellValue(BaseUtils.safeToStringNotNull(e.getValue()));
        }
    }

    private void loadAllColumNames(List<Map<String, Object>> data) {
        allColumnNames = new ArrayList<String>();
        for (Map<String, Object> row : data) {
            for (String colName : row.keySet()) {
                if (allColumnNames.indexOf(colName) < 0) {
                    allColumnNames.add(colName);
                }
            }
        }
    }

    protected void writeHtmlFormat(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    }

    protected void writeJasperFormat(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    }

    protected void writePdfFormat(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    }

    protected List<String> getFiles2Zip(HttpServletRequest req) {
        //null
        return new ArrayList<String>();
    }

    private void writeZipFormat(HttpServletRequest req, HttpServletResponse resp) {
        String fileNamePrefix = req.getParameter(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME);
        String contentDispositionVal = getContentDispositionVal(fileNamePrefix, req, ".zip");
        String contentType = "application/octet-stream";
        resp.setContentType(contentType);
        resp.setHeader("Content-disposition", contentDispositionVal);

        List<String> files2Zip = null;
        try {
            ZipOutputStream zipStream = new ZipOutputStream(resp.getOutputStream());
            files2Zip = getFiles2Zip(req);
            for (String fileToSend : files2Zip) {
                // We give a name to the file

                fileToSend.replace("\\", "/");
                String fileName = fileToSend.substring(fileToSend.lastIndexOf("/") + 1);
                zipStream.putNextEntry(new ZipEntry(fileName));
                InputStream fileContent = new FileInputStream(fileToSend);

                byte[] buffer = new byte[4096];
                int readBytesCount = 0;
                while ((readBytesCount = fileContent.read(buffer)) >= 0) {
                    zipStream.write(buffer, 0, readBytesCount);
                }
                zipStream.closeEntry();
            }
            zipStream.close();

            for (String fileToSend : files2Zip) {
                File file = new File(fileToSend);
                if (file.exists()) {
                    file.delete();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
