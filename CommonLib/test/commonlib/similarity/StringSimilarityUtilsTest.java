/*
 * StringSimilarityUtilsTest.java
 * JUnit based test
 *
 * Created on 4 wrzesie� 2007, 18:36
 */

package commonlib.similarity;

import junit.framework.*;

/**
 *
 * @author wezyr
 */
public class StringSimilarityUtilsTest extends TestCase {
    
    public StringSimilarityUtilsTest(String testName) {
        super(testName);
    }

    public void testCalcDamerauLevenshteinDistance() {
        assertEquals(1, StringSimilarityUtils.calcDamerauLevenshteinDistance("A", "B"));
        assertEquals(2, StringSimilarityUtils.calcDamerauLevenshteinDistance("A", "BC"));
        assertEquals(1, StringSimilarityUtils.calcDamerauLevenshteinDistance("michael", "micheal"));
        assertEquals(0, StringSimilarityUtils.calcDamerauLevenshteinDistance("ABC", "ABC"));
        assertEquals(7, StringSimilarityUtils.calcDamerauLevenshteinDistance("ABC", "VWXYZ0A"));
        assertEquals(1, StringSimilarityUtils.calcDamerauLevenshteinDistance("TO", "OT"));
        assertEquals(1, StringSimilarityUtils.calcDamerauLevenshteinDistance("TOC", "OTC"));
        assertEquals(3, StringSimilarityUtils.calcDamerauLevenshteinDistance("TOC", "OST"));
    }
}
