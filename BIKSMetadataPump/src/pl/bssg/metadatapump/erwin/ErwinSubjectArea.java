/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.erwin;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author pgajda
 */
public class ErwinSubjectArea extends ErwinXMLElementListOfString {

    public ErwinSubjectArea() {
        propertiesMap = new HashMap<String, ArrayList<String>>() {
            {
                put("Name", null);
                put("Long_Id", null);
                put("Object_Order", null);
                put("Auto_Attached_Objects_Ref", null);
                put("User_Attached_Objects_Ref", null);
            }
        };
    }
}
