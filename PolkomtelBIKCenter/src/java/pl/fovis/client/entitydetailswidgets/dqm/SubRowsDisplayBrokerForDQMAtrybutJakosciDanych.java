/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import static pl.fovis.client.BIKClientSingletons.getNodeKindBeanByCode;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.entitydetailswidgets.ISubRowsDisplayBroker;
import pl.fovis.common.dqm.DataQualityGroupBean;
import pl.fovis.common.dqm.DqmGrupaDanychPotomkowieBean;
import simplelib.IParametrizedContinuationWithReturn;

/**
 *
 * @author pmielanczuk
 */
public class SubRowsDisplayBrokerForDQMAtrybutJakosciDanych implements ISubRowsDisplayBroker {

    // ikonka, nazwa, strzałka
//    protected final int[] widths = {2, 96, 2};
    protected final int[] widths = {100};
    protected Map<Integer, String> data;
    protected List<Integer> ids;
    protected DqmGrupaDanychPotomkowieBean dqmGrupaDanychPotomkowie;
    protected String iconName = getNodeKindBeanByCode(BIKGWTConstants.NODE_KIND_DQM_GROUP).iconName;

    public SubRowsDisplayBrokerForDQMAtrybutJakosciDanych(DqmGrupaDanychPotomkowieBean dqmGrupaDanychPotomkowie) {

        this.dqmGrupaDanychPotomkowie = dqmGrupaDanychPotomkowie;

        if (dqmGrupaDanychPotomkowie != null) {
            this.data = dqmGrupaDanychPotomkowie.atrybutyJakosciDanych;
            this.ids = new ArrayList<Integer>(data.keySet());
        }
    }

    @Override
    public int getAttrCount() {
        return widths.length;
    }

    @Override
    public int getAttrWidth(int attrIdx) {
        return widths[attrIdx];
    }

    @Override
    public String getAttrCaption(int attrIdx) {
        return null;
    }

    @Override
    public int getRowCount() {
        return ids == null ? 0 : ids.size();
    }

    protected Widget createNameWidget(int id, String name) {
//        return BIKClientSingletons.createLinkAsAnchor(name, BIKGWTConstants.TREE_CODE_DQM, id);

//        DisclosurePanel dp = new DisclosurePanel();
//        dp.setHeader(BIKClientSingletons.createLinkAsAnchor(name, BIKGWTConstants.TREE_CODE_DQM, id));
//        dp.setOpen(true);
        VerticalPanel vp = new VerticalPanel();
        HorizontalPanel hp = new HorizontalPanel();
        vp.add(hp);
        //hp.add(BikIcon.createIcon("images/" + iconName + ".gif" /* I18N: no */, "RelatedObj-ObjIcon"));
        Widget attrNameLink = BIKClientSingletons.createLinkAsAnchor(name, BIKGWTConstants.TREE_CODE_DQM, id);
        attrNameLink.addStyleName("dqmAttrNameLink");
        hp.add(attrNameLink);
//        Widget actBtn = BIKClientSingletons.createActionButton("link_arrows", I18n.przejdz.get() /* I18N:  */,
//                EntityDetailsWidgetFlatBase.createFollowCH(BIKGWTConstants.TREE_CODE_DQM, id));
//        hp.add(actBtn);
//        hp.setCellHorizontalAlignment(actBtn, HasHorizontalAlignment.ALIGN_RIGHT);

        FlowPanel fp = new FlowPanel();
        fp.addStyleName("dqmTestsInAttr");
        vp.add(fp);

//        dp.setContent(new Label("aqq xxx!!!"));
        // na serwerze trzeba rozpoznawać, że nodem jest atrybut jakości danych i wtedy pobierać
        // dodatkową strukturę dla testów pod tym atrybutem
        List<DataQualityGroupBean> elems = dqmGrupaDanychPotomkowie.testyDlaAtrybutow.get(id);
//        dp.setContent(new GroupWidgetBuilder(elems, true).buildWidget());

        GroupWidgetBuilder.showManyInContainer(elems, fp, true);

        return vp;
    }

    @Override
    public Widget[] makeWidgetsForRow(int rowIdx) {
        Widget[] widgets = new Widget[widths.length];

        int id = ids.get(rowIdx);
        String name = data.get(id);
        int colIdx = 0;

//        widgets[colIdx++] = BikIcon.createIcon("images/" + iconName + ".gif" /* I18N: no */, "RelatedObj-ObjIcon");
//        widgets[colIdx++] = createNameWidget(id, name);
//        widgets[colIdx++] = BIKClientSingletons.createActionButton("link_arrows", I18n.przejdz.get() /* I18N:  */,
//                EntityDetailsWidgetFlatBase.createFollowCH(BIKGWTConstants.TREE_CODE_DQM, id));
        widgets[colIdx++] = createNameWidget(id, name);

        return widgets;
    }

    private static IParametrizedContinuationWithReturn<Integer, String> rowStyleCalc = new IParametrizedContinuationWithReturn<Integer, String>() {

        @Override
        public String doIt(Integer param) {
            return "dqmAtrybutJakosciDanychRow";
        }
    };

    @Override
    public IParametrizedContinuationWithReturn<Integer, String> getRowStyleCalc() {
        return rowStyleCalc;
    }
}
