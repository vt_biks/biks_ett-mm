/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.client.dialogs.BlogAddOrEditDialog;
import pl.fovis.common.BlogBean;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class NodeActionAddBlogEntry extends NodeActionUpdatingBase {

    @Override
    public boolean isEnabledByDesign() {
        return isNodeNotLinkedAnyhowNorReadOnlyTreeAndBlogNodeAndUserInAuthors();
    }

//    @Override
//    protected boolean isBlogNodeEditableForLoggedUser() {
//        return isLoggedUserInBlogAuthors(false);
//    }
//
//    @Override
//    protected Collection<String> getEditableNodeKinds() {
//        return Collections.singletonList("Blog");
//    }
    public String getCaption() {
        return I18n.dodajWpis.get() /* I18N:  */;
    }

    public void execute() {
        if (!isSocialUserLoggedInWithInfo()) {
            return;
        }

        new BlogAddOrEditDialog().buildAndShowDialog(null, new IParametrizedContinuation<BlogBean>() {
            public void doIt(BlogBean param) {
                BIKClientSingletons.getService().addBikBlogEntry(node.id, param.content, param.subject/*, node.branchIds*/, new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on" /* I18N: no */+ " addBlogEntry") {
                    public void onSuccess(Pair<Integer, List<String>> result) {
                        BIKClientSingletons.showInfo(I18n.dodanoWpis.get() /* I18N:  */);
                        refreshRedisplayDataAndSelectRecord(result.v1/*, false*/);
                        refreshAndRedisplayDataByTreeCode(result.v2);
                    }
                });
            }
        });
    }
}
