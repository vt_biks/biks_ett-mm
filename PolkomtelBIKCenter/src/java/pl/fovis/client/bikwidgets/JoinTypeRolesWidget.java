/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.JoinTypeRoles;
import pl.fovis.common.UserInNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author beata
 */
public class JoinTypeRolesWidget {

//    public Map<RadioButton, JoinTypeRoles> joinTypeRoleRadioBtnMap = new HashMap<RadioButton, JoinTypeRoles>();
    public Map<JoinTypeRoles, RadioButton> joinTypeRoleRadioBtnMap = new EnumMap<JoinTypeRoles, RadioButton>(JoinTypeRoles.class);
    public RadioButton auxiliary;
    protected RadioButton primaryAct;
    protected VerticalPanel joinTypeVp;
    protected VerticalPanel joinTypeMainVp;
    protected List<UserInNodeBean> usersInNodeBean;
    protected Integer nodeId;
    protected Map<Integer, Integer> rolesUnderCnt;
    public CheckBox primaryReplace = new CheckBox();
    protected HTML userInRoleHTML = new HTML();
    protected FlowPanel userInRoleVp = new FlowPanel();
    protected CheckBox userInRoleCb = new CheckBox();
    protected Set<String> treeKindForSelected;
    protected FlowPanel mainFP = new FlowPanel();

    public JoinTypeRolesWidget(int nodeId, Set<String> treeKindForSelected) {
        this.nodeId = nodeId;
        this.treeKindForSelected = treeKindForSelected;
    }

    public JoinTypeRolesWidget(List<UserInNodeBean> usersInNodeBean, Integer nodeId, Map<Integer, Integer> rolesUnderCnt) {
        this.usersInNodeBean = usersInNodeBean; //z dziedziczenia i dla danego noda
        this.nodeId = nodeId;
        this.rolesUnderCnt = rolesUnderCnt; // z potomków
    }

    public FlowPanel buildJointTypeRoleWidget(int markedNode, boolean isInheritToDescendantsChecked) {
        joinTypeVp = new VerticalPanel();
        joinTypeMainVp = new VerticalPanel();
//        FlowPanel mainFP = new FlowPanel();
        mainFP.clear();
        mainFP.setStyleName("Roles" /* i18n: no:css-class */);

        Label typeLb = new Label(I18n.wybierzTypPowiazania.get() /* I18N:  */ + ":");
        typeLb.setStyleName("AttrHeadLbl");
        typeLb.addStyleName("rightTreeLbl");
        mainFP.add(typeLb);

//        joinTypeVp.addStyleName("selectTypeRolesScroll");
        joinTypeMainVp.addStyleName("selectRolesScroll");
        buildRolePanelWithData(markedNode, isInheritToDescendantsChecked);
//        fp.setHeight("225px");//
        mainFP.add(joinTypeVp);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(mainFP,
                BIKClientSingletons.ORIGINAL_TREE_GRID_PANEL_HEIGHT - 4);
        return mainFP;
    }

    protected boolean isMultiSelected() {
        return BaseUtils.isMapEmpty(rolesUnderCnt); //(nodeId == null);
    }

    public void buildRolePanelWithData(int markedNode, boolean isInheritToDescendantsChecked) {
        buildRolePanelWithData(markedNode, isInheritToDescendantsChecked, null);
    }

    public void buildRolePanelWithData(final int markedNode, boolean isInheritToDescendantsChecked, Integer roleCnt) {
//        boolean isMultiSelected = (nodeId == null);
        joinTypeVp.clear();
        joinTypeMainVp.clear();
        joinTypeRoleRadioBtnMap.clear();
        primaryAct = new RadioButton("joinType", I18n.glownyAAktualniUstawionNaGlowneZ.get() /* I18N:  */ + " " /* JoinTypeRoles.PrimaryAct.caption*/ + (isMultiSelected() ? I18n.tychObiektach.get() /* I18N:  */ : I18n.tymObiekcie.get() /* I18N:  */));
        auxiliary = new RadioButton("joinType", JoinTypeRoles.Auxiliary.caption);
        primaryAct.setStyleName("AttrRb");
        auxiliary.setStyleName("AttrRb");
        if (isRoleInJoinedRoles(markedNode)) {
            primaryAct.addClickHandler(new ClickHandler() {//bf-> primary zaznacza pierwszy pod nim
                public void onClick(ClickEvent event) {
                    //ww->prim2: powinna być mapa <JoinTypeRoles, RadioButton> albo zmienna po prostu
                    // przechowująca radioButton dla PrimaryOldReplace...
                    joinTypeRoleRadioBtnMap.get(JoinTypeRoles.PrimaryOldReplace).setValue(true);
                }
            });

            joinTypeVp.add(primaryAct);//inny tekst z drzewka użytkownikow inny z reszty???
            String text = (isMultiSelected() ? I18n.wybranychObiektach.get() /* I18N:  */ : I18n.wybranymObiekcie.get() /* I18N:  */);
            String textWithInInherit = text + " " + I18n.iObiektachPodrzednych.get() /* I18N:  */;
            //ww->prim2: text + " i obiektach podrzędnych" - jest copy + paste 2 razy poniżej...
            // powinno to być na zmiennej i bez powtarzania takiego wyrażenia dwa razy.
            joinTypeMainVp.add(createJoinTypeRoleRbtn(JoinTypeRoles.PrimaryOldReplace, "joinType2", textWithInInherit, markedNode));
            joinTypeMainVp.add(createJoinTypeRoleRbtn(JoinTypeRoles.PrimaryOldDelete, "joinType2", textWithInInherit, markedNode));// w wybranym obiekcie i obiektach podrzędnych

            if (isMultiSelected() || (isInheritToDescendantsChecked && isRoleInDescendants(markedNode))) {//z drzewka user
                FlowPanel hp = new FlowPanel();
                final RadioButton primaryOldNotReplaceRb = createJoinTypeRoleRbtn(JoinTypeRoles.PrimaryOldNotReplace, "joinType2", text, markedNode);
                hp.add(primaryOldNotReplaceRb);
                primaryReplace.setText(JoinTypeRoles.SelectedPrimaryOldReplace.caption);
//                primaryReplace = new CheckBox(JoinTypeRoles.SelectedPrimaryOldReplace.caption);
                primaryReplace.addClickHandler(new ClickHandler() {
                    public void onClick(ClickEvent event) {
                        if (primaryReplace.getValue()) {
                            primaryOldNotReplaceRb.setValue(true);
                            primaryAct.setValue(true);
                            auxiliary.setValue(false);
                        }
                        if (primaryOldNotReplaceRb.getValue()) {
                            callServiceAndSetUserInRoleCntTxt(null, markedNode);
//                            BIKClientSingletons.getService().getDescendantsCntInMainRoleByUserRoleAndTreeKind(nodeId, markedNode, treeKindForSelected, new StandardAsyncCallback<Integer>() {
//                                public void onSuccess(Integer result) {
//                                    if (result != 0) {
//                                        setTextCntUserInRole(result, null);
//                                    }
//                                }
//                            });
                        }
                    }
                });

                primaryReplace.setTitle(I18n.zamienNaPomocniczeLubUsun.get() /* I18N:  */ + " ");
                primaryReplace.setStyleName("AttrRb");

                hp.add(primaryReplace);
                joinTypeMainVp.add(hp);
            }
            joinTypeVp.add(joinTypeMainVp);
            joinTypeVp.add(auxiliary);
            auxiliary.addClickHandler(new ClickHandler() {//bf-> auxiliary zdejmuje z primary bo ten sam typ rb, i poniżej z rreszty
                public void onClick(ClickEvent event) {

                    //ww->prim2: powinna być mapa <JoinTypeRoles, RadioButton>, takich pętli by tu
                    // nie było...

                    joinTypeRoleRadioBtnMap.get(JoinTypeRoles.Auxiliary);

                    for (RadioButton r : joinTypeRoleRadioBtnMap.values()) {
                        if (r != joinTypeRoleRadioBtnMap.get(JoinTypeRoles.Auxiliary)) {
                            r.setValue(false);
                        }
                    }
                    primaryReplace.setValue(false);
                    userInRoleVp.setVisible(false);
                }
            });
            userInRoleVp.add(userInRoleHTML);
            userInRoleVp.add(userInRoleCb);
            joinTypeVp.add(userInRoleVp);
            userInRoleVp.setVisible(false);
            joinTypeRoleRadioBtnMap.put(JoinTypeRoles.Auxiliary, auxiliary);

        } else {
            joinTypeVp.add(createJoinTypeRoleRbtn(JoinTypeRoles.Primary, "joinType", markedNode));
            joinTypeVp.add(createJoinTypeRoleRbtn(JoinTypeRoles.Auxiliary, "joinType", markedNode));
        }
    }

    //ww->prim2: String... opt gdy potrzebny jest tylko jeden taki parametr?
    // to się inaczej robi - przeciąża się metodę, jedna ma ten parametr (opt)
    // druga go nie ma i woła tę pierwszą z nullem
    // tak jak jest teraz, to można przekazać kilka parametrów na opt - ale po co???
    // po drugie: nazwa opt jest zła - nic nie mówi, poza tym że to parametr opcjonalny
    // ale po co on jest???
    public RadioButton createJoinTypeRoleRbtn(final JoinTypeRoles jt, final String name, int markedNode) {
        return createJoinTypeRoleRbtn(jt, name, null, markedNode);
    }

    public RadioButton createJoinTypeRoleRbtn(final JoinTypeRoles jt, final String name, String optText, final int markedNode) {
        RadioButton rb = new RadioButton(name, jt.caption + (optText != null ? " " + optText : ""));
        rb.setStyleName("AttrRb");
        joinTypeRoleRadioBtnMap.put(jt, rb);
        if (JoinTypeRoles.PrimaryOldReplace == jt || JoinTypeRoles.PrimaryOldDelete == jt || JoinTypeRoles.PrimaryOldNotReplace == jt) {//bf--> zaznacza główny.
            //if (name.equals("joinType2")) {
            rb.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    if (primaryAct != null) {
                        primaryAct.setValue(true);
                        if (JoinTypeRoles.PrimaryOldNotReplace != jt) {
                            primaryReplace.setValue(false);
                        }
                    }

                    if (isMultiSelected()) {
                        callServiceAndSetUserInRoleCntTxt(jt, markedNode);
                    }
                }
            });
        } else {
            userInRoleVp.setVisible(false);
        }
        return rb;
    }

    protected void callServiceAndSetUserInRoleCntTxt(final JoinTypeRoles jt, final int markedNode) {
        if (treeKindForSelected != null) {
            BIKClientSingletons.getService().getDescendantsCntInMainRoleByUserRoleAndTreeKind(nodeId,
                    markedNode, treeKindForSelected, new StandardAsyncCallback<Integer>() {
                public void onSuccess(Integer result) {
                    if (result != 0) {
                        setTextCntUserInRole(result, jt);
                    }
                }
            });
        }
    }

    protected void setTextCntUserInRole(Integer cnt, JoinTypeRoles jt) {
        userInRoleVp.setVisible(true);
        userInRoleHTML.setHTML(I18n.uzytkownPosiadaObiektowPowiazan.get() /* I18N:  */ + ": " + cnt
                + "<br/> " + I18n.czyDlaJuzIstniejaPowiazanWykonac.get() /* I18N:  */ + ": ");
        String text = getJoinTypeRoleSelectedRb().caption;

        if (jt != null && JoinTypeRoles.PrimaryOldNotReplace != jt) {
            text += " " + I18n.wybranycObiektacIObiektacPodrzed.get() /* I18N:  */;

        } else {
            text += " " + I18n.wybranychObiektach.get() /* I18N:  */ + " " + (primaryReplace.getValue() ? JoinTypeRoles.SelectedPrimaryOldReplace.caption : I18n.usun2.get() /* I18N:  */);
        }
        userInRoleCb.setText(text);
        userInRoleCb.setStyleName("AttrRb");
    }

    protected boolean isRoleInDescendants(int roleId) {
        //ww->prim2: po co to sprawdzanie > 0? czy w tej mapie muszą siedzieć wpisy
        // z kluczem i wartością == 0 pod kluczem (może nie powinno ich być w mapie?)
        // a co z wartościami < 0 - są dopuszczalne? taka konstrukcja kodu każe
        // mi przypuszczać, że wartości < 0 mogą się po coś pojawiać...
        return rolesUnderCnt != null && rolesUnderCnt.get(roleId) != null;
//                rolesUnderCnt.containsKey(roleId)
//                && rolesUnderCnt.get(roleId) > 0;
    }

    protected boolean isRoleInJoinedRoles(int roleId) {
        //jeżeli ma role w potomkach (a sam jej jeszcze nie ma)
        //ww->prim2: co oznacza roleId <= 0? może być ujemne, może być 0?
        // czy nie powinno być sprawdzanie na NULLa - o ile roleId jest opcjonalne
        if (roleId > 0 && isRoleInDescendants(roleId)) {
            return true;
        }
        if (usersInNodeBean == null) {
            return true;
        }
        //jeżeli ma role sam lub dziedziczy z góry
        for (UserInNodeBean ub : usersInNodeBean) {
            if (roleId == ub.roleForNodeId && !ub.isAuxiliary) {
                return true;
            }
        }
        return false;
    }

    public boolean isUserInRoleSelected() {
        return userInRoleCb.getValue();
    }

    public JoinTypeRoles getJoinTypeRoleSelectedRb() {
        for (Entry< JoinTypeRoles, RadioButton> rb : joinTypeRoleRadioBtnMap.entrySet()) {
            if (rb.getValue().getValue()) {
                return rb.getKey();
            }
        }
        return null;
    }

    public boolean isJoinTypeRoleRbSelected() {
        if (!auxiliary.getValue() && getJoinTypeRoleSelectedRb() == null) {
            Window.alert(I18n.wybierzTypPowiazania.get() /* I18N:  */);
            return false;
        }
        return true;
    }

    public void setVisibleMainFP(boolean isVisible) {
        mainFP.setVisible(isVisible);
    }
}
