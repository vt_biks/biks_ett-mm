/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.common.UserBean;

/**
 *
 * @author bfechner
 */
public class PickUserDialog extends PickUserDialogBase<UserBean> {

    @Override
    protected String getName(UserBean b) {
        return b.name;
    }

    @Override
    protected Integer getId(UserBean b) {
        return b.id;
    }
}
