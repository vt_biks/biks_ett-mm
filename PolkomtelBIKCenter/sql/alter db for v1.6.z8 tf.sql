﻿-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-----------------   Implementacja testów dla Profile  -------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
exec sp_check_version '1.6.z8';
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_dqm_log') and type in (N'U'))
begin
	create table bik_dqm_log (
		id bigint not null identity(1,1) primary key,
		test_id int not null references bik_dqm_test(id),
		start_time datetime not null,
		finish_time datetime null,
		description varchar(max) null
	);
end;
go

if not exists (select * from bik_app_prop where name = 'dqmRunTestAsFunction')
begin
	insert into bik_app_prop(name, val,is_editable)
	values ('dqmRunTestAsFunction', 'false', 0)
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_dqm_test]') and name = 'database_node_id')
begin
	alter table bik_dqm_test add database_node_id int null references bik_node(id);
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_dqm_test]') and name = 'sql_text')
begin
	alter table bik_dqm_test add sql_text varchar(max) null;
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_dqm_test]') and name = 'error_sql_text')
begin
	alter table bik_dqm_test add error_sql_text varchar(max) null;
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_dqm_test]') and name = 'result_file_name')
begin
	alter table bik_dqm_test add result_file_name varchar(512) null;
end;
go

if not exists(select * from bik_attr_def where is_built_in = 1 and name = 'Zapytanie testujące')
begin
	declare @categoryId int = (select id from bik_attr_category where name = 'DQM' and is_built_in = 1);

	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Zapytanie testujące', @categoryId, 1, '', 'shortText')
		
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Zapytanie zwracające błędne dane', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Baza danych', @categoryId, 1, '', 'shortText')
	
	insert into bik_attr_def (name, attr_category_id, is_built_in, hint, type_attr)
	values ('Prefiks nazwy pliku na SFTP', @categoryId, 1, '', 'shortText')


	insert into bik_translation(code, txt, lang, kind)
	values ('Zapytanie testujące', 'Test query', 'en', 'adef')
	
	insert into bik_translation(code, txt, lang, kind)
	values ('Zapytanie zwracające błędne dane', 'Error returning query', 'en', 'adef')
	
	insert into bik_translation(code, txt, lang, kind)
	values ('Baza danych', 'Database name', 'en', 'adef')
	
	insert into bik_translation(code, txt, lang, kind)
	values ('Prefiks nazwy pliku na SFTP', 'SFTP file prefix', 'en', 'adef')
	
	
	declare @DQMTestInactive int = dbo.fn_node_kind_id_by_code('DQMTestInactive');
    declare @DQMTestSuccess int = dbo.fn_node_kind_id_by_code('DQMTestSuccess');
    declare @DQMTestFailed int = dbo.fn_node_kind_id_by_code('DQMTestFailed');
    declare @DQMTestNotStarted int = dbo.fn_node_kind_id_by_code('DQMTestNotStarted');
    declare @attrTestQuery int = (select id from bik_attr_def where name = 'Zapytanie testujące' and is_built_in = 1);
    declare @attrErrorTestQuery int = (select id from bik_attr_def where name = 'Zapytanie zwracające błędne dane' and is_built_in = 1);
    declare @attrDatabase int = (select id from bik_attr_def where name = 'Baza danych' and is_built_in = 1);
    declare @sftpprefix int = (select id from bik_attr_def where name = 'Prefiks nazwy pliku na SFTP' and is_built_in = 1);
    
    insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrTestQuery, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrTestQuery, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrTestQuery, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrTestQuery, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrErrorTestQuery, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrErrorTestQuery, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrErrorTestQuery, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrErrorTestQuery, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @attrDatabase, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @attrDatabase, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @attrDatabase, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @attrDatabase, 1)
	
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestInactive, @sftpprefix, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestSuccess, @sftpprefix, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestFailed, @sftpprefix, 1)
	insert into bik_attr_system(node_kind_id, attr_id, is_visible)
	values(@DQMTestNotStarted, @sftpprefix, 1)
	
end;
go

-- poprzednia wersja w pliku: alter db for v1.6.z7.1 tf.sql
-- fix na left joina dla procedure node id
if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_verticalize_node_attrs_dqm_test') and type in (N'P', N'PC'))
drop procedure dbo.sp_verticalize_node_attrs_dqm_test
go

create procedure dbo.sp_verticalize_node_attrs_dqm_test(@optNodeFilter varchar(max))
as
begin
	set nocount on
	
	declare @sqlText varchar(max) = '(select te.test_node_id, te.description, te.long_name, te.benchmark_definition, te.sampling_method, te.additional_information, te.error_threshold, te.create_date, te.modify_date, te.activate_date, te.deactivate_date, def.description as source_name, bn1.name as proc_name, bn2.name as err_proc_name, te.sql_text, te.error_sql_text, db.name as database_name, te.result_file_name
		from bik_dqm_test as te 
		inner join bik_data_source_def as def on def.id = te.source_id
		left join bik_node as bn1 on bn1.id = te.procedure_node_id
		left join bik_node as bn2 on bn2.id = te.error_procedure_node_id
		left join bik_node as db on te.database_node_id = db.id
		where te.is_deleted = 0)';
	
	exec sp_verticalize_node_attrs_one_table @sqlText, 'test_node_id', 'description, long_name, benchmark_definition, sampling_method, additional_information, error_threshold, create_date, modify_date, activate_date, deactivate_date, proc_name, err_proc_name, source_name, sql_text, error_sql_text, database_name, result_file_name', 'Opis testu, Nazwa biznesowa, Definicja testu, Metoda próbkowania, Informacje dodatkowe, Próg błędu, Data utworzenia, Data modyfikacji, Data aktywacji, Data dezaktywacji, Funkcja testująca, Funkcja zwracająca błędne dane, Źródło, Zapytanie testujące, Zapytanie zwracające błędne dane, Baza danych, Prefiks nazwy pliku na SFTP', @optNodeFilter
end
go

declare @treeOfTreeId int = (select id from bik_tree where code = 'TreeOfTrees');
update bik_node set is_deleted = 1 where tree_id = @treeOfTreeId and obj_id = '#admin:dqm:config' and is_deleted = 0


exec sp_update_version '1.6.z8', '1.6.z8.1';
go