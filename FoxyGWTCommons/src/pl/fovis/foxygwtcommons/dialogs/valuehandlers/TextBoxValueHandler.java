/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs.valuehandlers;

import com.google.gwt.user.client.ui.TextBoxBase;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public class TextBoxValueHandler implements IWidgetValueHandler<TextBoxBase> {

    protected boolean doTrimValues;

    public TextBoxValueHandler(boolean doTrimValues) {
        this.doTrimValues = doTrimValues;
    }

    public Object getValueFromWidget(TextBoxBase widget, Class targetValueClass, GetValueExtraInfo extraInfo) {
        String val = widget.getValue();
        return doTrimValues ? BaseUtils.emptyStringOrWhiteSpaceToNull(val) : BaseUtils.emptyStrToDef(val, null);
    }

    public void setValueToWidget(TextBoxBase widget, Object value, SetValueExtraInfo extraInfo) {
//        Window.alert("TextBoxValueHandler.setValueToWidget: val=" + value + ", propName=" + extraInfo.propName);
        widget.setValue(BaseUtils.safeToString(value));
    }

    public boolean isWidgetClassHandled(Object widget) {
        boolean res = widget instanceof TextBoxBase;
//        Window.alert("TextBoxValueHandler.isWidgetClassHandled: widget.class=" + widget.getClass() + ", res=" + res);
        return res;
    }
}
