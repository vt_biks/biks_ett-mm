/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import pl.fovis.client.entitydetailswidgets.IEntityDetailsWidgetTabReady;

/**
 *
 * @author AMamcarz
 */
public interface IEntityDetailsWidgetsContainer {

    public void refreshTabCaption(IEntityDetailsWidgetTabReady act);
}
