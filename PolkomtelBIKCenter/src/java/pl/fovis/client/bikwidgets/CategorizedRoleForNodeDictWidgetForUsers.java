/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;
import java.util.*;
import java.util.Map.Entry;
import pl.fovis.common.RoleForNodeBean;
import pl.fovis.common.UserInNodeBean;
import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.IContinuation;

/**
 *
 * @author AMamcarz
 */
public class CategorizedRoleForNodeDictWidgetForUsers extends CategorizedRoleForNodeDictWidget {
    //mapa checkboksow

    protected Map<Integer, RadioButton> radioButtonsMap = new HashMap<Integer, RadioButton>();
    //ostatni zaznaczony checkbox
//    protected int lastMarked = 0;
    protected IContinuation selectionChangedCont;
    protected List<UserInNodeBean> roles;

    public CategorizedRoleForNodeDictWidgetForUsers(List<UserInNodeBean> roles) {
        this.roles = roles;
    }

    @Override
    protected Widget createOptCheckboxForItemWidget(final int attrId) {
//        CheckBox cb = new CheckBox();
        final RadioButton cb = new RadioButton("type" /* I18N: no */, "");

        cb.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
//                resetAllCheckBoxes(attrId);
                if (selectionChangedCont != null) {
                    selectionChangedCont.doIt();
                }

            }
        });
        radioButtonsMap.put(attrId, cb);
        return cb;
    }

    @Override
    protected List<RoleForNodeBean> deleteEmptyCategories(List<RoleForNodeBean> result) {
        List<RoleForNodeBean> resultWithoutEmptyCategories = new ArrayList<RoleForNodeBean>();
        for (RoleForNodeBean role : result) {
            if (role.caption != null) {
                resultWithoutEmptyCategories.add(role);
            }
        }
        return resultWithoutEmptyCategories;
    }

    //odznacz poprzedni checkbox
    //ustaw teraz na niego wskazanie że jest zaznaczony
//    protected void resetAllCheckBoxes(int withOut) {
//        if (lastMarked != 0 && lastMarked != withOut) {
//            radioButtonsMap.get(lastMarked).setValue(false);
//        }
//        if (lastMarked == withOut && !radioButtonsMap.get(lastMarked).getValue()) {
//            lastMarked = 0;
//        } else {
//            lastMarked = withOut;
//        }
//    }
    public int markedNode() {
        for (Entry<Integer, RadioButton> rb : radioButtonsMap.entrySet()) {
            if (rb.getValue().getValue()) {
                return rb.getKey();
            }
        }
        return 0;//lastMarked;
    }

    @Override
    protected BikCustomButton createAddItemButton(final int categoryId) {
        return null;
    }

    @Override
    protected PushButton createDeleteButton(final int attrOrRoleId, final String name) {
        return null;
    }

    @Override
    protected BikCustomButton createAddCategoryButton() {
        return null;
    }

    @Override
    protected PushButton createEditButton(final IContinuation showEditDialogCont) {
        return null;
    }

    public void setOnSelectionChanged(IContinuation selectionChangedCont) {
        this.selectionChangedCont = selectionChangedCont;
    }

    @Override
    protected PushButton createDeleteButtonForCategory(final int categoryId) {
        return null;
    }
}
