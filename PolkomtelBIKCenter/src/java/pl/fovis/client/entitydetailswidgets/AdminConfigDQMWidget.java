/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.ConnectionParametersDQMBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class AdminConfigDQMWidget extends AdminConfigBIKBaseWidget<ConnectionParametersDQMBean> {

    protected CheckBox chbIsActive;
    protected PushButton btnSave;
    protected TextBox errorData;
    protected AdminConfigDQMConnectionsWidget acWidget = new AdminConfigDQMConnectionsWidget();

    @Override
    protected void buildInnerWidget() {
        if (BIKClientSingletons.useDqmConnectionMode()) {
            TabPanel tabPanel = new TabPanel();
            tabPanel.setAnimationEnabled(true);
            tabPanel.add(generalVp(), I18n.dqmOgolne.get());
            tabPanel.add(acWidget.buildWidget(), I18n.dqmPolaczenia.get());
            tabPanel.selectTab(0);
            main.add(tabPanel);
        } else {
            main.add(generalVp());
        }
    }

    protected VerticalPanel generalVp() {
        VerticalPanel vp = new VerticalPanel();
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
            }
        }, false, vp);
        createLine(vp);
        createLabel(I18n.trzymajBledneDaneDlaOstatnichWykonan.get(), vp);
//        createLabel("Keep error data for last requests");
        errorData = createTextBox(vp);
        createLine(vp);
        btnSave = createButton(I18n.zapisz.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ConnectionParametersDQMBean bean = new ConnectionParametersDQMBean();
                bean.isActive = chbIsActive.getValue() ? 1 : 0;
                bean.lastErrorRequests = BaseUtils.tryParseInteger(errorData.getValue());
                if (bean.lastErrorRequests == null) {
                    new SimpleInfoDialog().buildAndShowDialog(I18n.wpiszPoprawneWartosci.get(), null, null);
                } else {
                    BIKClientSingletons.getDQMService().setDQMConnectionParameters(bean, new StandardAsyncCallback<Void>("Error in setDQMConnectionParameters") {

                        @Override
                        public void onSuccess(Void result) {
                            BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get() /* I18N:  */);
                        }
                    });
                }
            }
        }, vp);
        return vp;
    }

    @Override
    public void populateWidgetWithData(ConnectionParametersDQMBean data) {
        errorData.setValue(String.valueOf(data.lastErrorRequests));
        chbIsActive.setValue(data.isActive == 1);
        acWidget.populateWidgetWithData(data.connections);
        setWidgetsEnabled(chbIsActive.getValue());
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_DQM;
    }
}
