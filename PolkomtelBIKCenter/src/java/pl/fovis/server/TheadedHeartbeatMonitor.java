/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import java.net.HttpURLConnection;
import java.net.URL;
import simplelib.logging.ILameLogger;

/**
 *
 * @author mmastalerz
 */
public class TheadedHeartbeatMonitor implements Runnable {

    protected HttpURLConnection connection = null;
    protected String url;
    protected int timeout;
    private static final ILameLogger logger = LameUtils.getMyLogger();

    public void setConnectionData(String url, int timeout) {
        this.url = url;
        this.timeout = timeout;
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(timeout * 1000);
                URL u = new URL(url);
                connection = (HttpURLConnection) u.openConnection();
                connection.setRequestMethod("HEAD" /* I18N: no */);
                int code = connection.getResponseCode();

            } catch (Exception ex) {
                if (logger.isInfoEnabled()) {
                    logger.info("  TheadedHeartbeatMonitor  " + ex);
                }

            }
        }

    }
}
