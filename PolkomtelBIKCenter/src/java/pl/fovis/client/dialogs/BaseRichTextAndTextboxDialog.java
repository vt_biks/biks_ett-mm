/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.client.bikwidgets.BikRichTextArea;
import pl.fovis.client.bikwidgets.MyRichTextAreaFoxyToolbar;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.richtexteditor.FoxyRichTextArea;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public abstract class BaseRichTextAndTextboxDialog extends BaseActionOrCancelDialog {

    protected TextBox txtTitle = new TextBox();
    protected BikRichTextArea richTextArea = new BikRichTextArea();
    protected TextArea textArea = new TextArea();
    protected Label lblViewMode = new Label(I18n.podglad.get());
    protected Label lblTitle;
    protected Label lblArea;
    protected MyRichTextAreaFoxyToolbar toolbar;
    protected Grid grid;
    protected String title;
    protected String content;
    protected boolean isTitleEditEnabled = true;

    protected abstract void innerDoAction(String title, String body);

    protected FoxyRichTextArea getRichTextArea() {
        return richTextArea;
    }

    @Override
    protected void buildMainWidgets() {
        Grid mainGrid = new Grid(2, 2);
        mainGrid.setCellSpacing(4);

        txtTitle.setMaxLength(150);
        txtTitle.setStyleName("addBlogTitle");
        txtTitle.setWidth("471px");

        addLabelsTitle();

        lblTitle.setStyleName("addBlogLbl");
        lblArea.setStyleName("addBlogLbl");

        richTextArea.setSize("100%", "14em");
        toolbar = new MyRichTextAreaFoxyToolbar(richTextArea/*, record.nodeId*/, showTableOfContent());
//        toolbar.setWidth("100%");
        toolbar.setStyleName("richTextAreaToolbar");
        HorizontalPanel hp = new HorizontalPanel();
        hp.setStyleName("richTextAreaP");
        hp.add(toolbar);

        grid = new Grid(2, 1);
        grid.setWidget(0, 0, hp);
        grid.setWidget(1, 0, richTextArea);

        FlowPanel flowPanel = new FlowPanel();
        flowPanel.add(grid);
        flowPanel.setStyleName("richTextArea");

        txtTitle.setText(title != null ? title : "");
        txtTitle.setEnabled(isTitleEditEnabled);
        richTextArea.setHTML(BaseUtils.safeToString(content != null ? content : ""));

        mainGrid.setWidget(0, 0, lblTitle);
        mainGrid.setWidget(0, 1, txtTitle);
        mainGrid.setWidget(1, 0, lblArea);
        mainGrid.setWidget(1, 1, flowPanel);

        main.add(mainGrid);
    }

    protected void addLabelsTitle() {
        lblTitle = new Label(I18n.tytul.get() /* I18N:  */ + ":");
        lblArea = new Label(I18n.tresc.get() /* I18N:  */ + ":");
    }

//    @Override
//    protected void doCancel() {
//        repairSelectionBeforeClose();
//    }
//    //ww->close: przy actionClicked i cancelClicked woła się hideDialog...
//    // czy to wywołanie repairSelectionBeforeClose nie powinno być przeniesione
//    // tam (wołane stamtąd) zamiast z dwóch miejsc? może wtedy doAction by
//    // nie było potrzebne?
//    protected void doAction() {
//        doAction();
////        repairSelectionBeforeClose();
//    }
//    //ww->close: jeżeli doAction wołane jest tylko wewnątrz doAction, to powinno
//    // nazywać się internalDoAction albo doAction - wtedy jest to jasne
//    // bez komentarza
//    protected abstract void doAction();
    @Override
    public void hideDialog() {
        super.hideDialog();

        repairSelectionBeforeClose();
    }

    //ww->close: co to robi? po co to jest? proszę o opis, bo nie wiadomo czy
    // to jest potrzebne
    protected void repairSelectionBeforeClose() {
        //ww->close: getBasicFormatter jest deprecated - widać, bo skreślone...
        // jak się wejdzie do kodu źródłowego tej metody, to widać, że:
        // @deprecated use {@link #getFormatter()} instead
        // chyba wystarczy zmienić na getFormatter i nie będzie ostrzeżenia?
        toolbar.richText.getFormatter()/*getBasicFormatter()*/.selectAll();
    }

    public void buildAndShowDialog(String title, String content, boolean isTitleEditEnabled) {
        this.title = title;
        this.content = content;
        this.isTitleEditEnabled = isTitleEditEnabled;
        super.buildAndShowDialog();
    }

    public void buildAndShowDialog(String title, String content) {
        buildAndShowDialog(title, content, true);
    }

    @Override
    protected final void doAction() {
        innerDoAction(txtTitle.getText(), toolbar.isHtmlMode() ? richTextArea.getHTML() : richTextArea.getText());
    }

    @Override
    protected boolean doActionBeforeClose() {
        if (BaseUtils.isStrEmptyOrWhiteSpace(txtTitle.getText())/* || BaseUtils.isHTMLTextEmpty(area.getHTML()*/) {
//            String alertText = I18n.polaTytulITrescNieMogaBycPuste.get() /* I18N:  */;
            String alertText = I18n.poleTytulNieMozeBycPuste.get() /* I18N:  */;
            Window.alert(alertText);
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected final String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }

    protected boolean showTableOfContent() {
        return true;
    }
}
