/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.StartupDataBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.UserExtBean;
import pl.fovis.common.ValidationExceptionBiks;
import pl.trzy0.foxy.serverlogic.IServerSupportedLocaleNameRetriever;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public interface IBIKSSystemUsersSubservice extends IServerSupportedLocaleNameRetriever {

//    public ConnectionParametersADBean getADConnectionParameters();
    public String getAvatarFieldForAd();

    public SystemUserBean bikSystemUserByLogin(String login, String avatarField);

    public void setLoggedUserBeanInSessionData(SystemUserBean user);

    public void createSocialUserFromADData(String loginName, String optSsoDomain, int systemUserId);

    public void maybeSetModifiedTreesInfoInResponse();

    public SystemUserBean getLoggedUserBean();

    public StartupDataBean getStartupData();

    public Pair<Integer, List<String>> addBikTreeNodeUser(TreeNodeBean repo, UserExtBean user);

    public void updateBikSystemUser(int id, String loginName, String password, boolean isDisabled, Integer userId, String systemUserName, Set<String> userRightRoleCodes, 
            Set<Integer> treeIds, boolean isSendMails, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries,List<Integer>  usersGroupsIds, boolean doNotFilterHtml) throws ValidationExceptionBiks;

    public Integer addBikSystemUserInternal(boolean performSecurityChecks, String loginName, String password,
            boolean isDisabled, Integer userId, Set<String> userRightRoleCodes, Map<Integer, Set<Pair<Integer, Integer>>> customRightRoleUserEntries,List<Integer>  usersGroupsIds, boolean doNotFilterHtml, String rightsProc) throws ValidationExceptionBiks;

    public SystemUserBean getOrCreateSystemUserBeanByLogin(String loginName, String optSsoDomain);

    public Pair<String, SystemUserBean> performUserLoginEx(String login, String password, String domain);
}
