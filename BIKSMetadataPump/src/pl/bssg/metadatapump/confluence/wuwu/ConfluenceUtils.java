/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence.wuwu;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.common.LinkToBikNodeBean;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.IRegExpMatcher;
import simplelib.IRegExpPattern;
import simplelib.LameRuntimeException;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public class ConfluenceUtils {

    public static final String HREF_REGEXPR = "[ \\t\\n\\r]+href[ \\t\\n\\r]*=[ \\t\\n\\r]*[\"']([^\"']*)[\"']";
    protected static final IRegExpPattern HREF_PATT = ConfluenceUtils.compileRegExpr(HREF_REGEXPR);

    public static IRegExpPattern compileRegExpr(String regExpr) {
        return BaseUtils.getRegExpMatcherFactory().compile(regExpr, true, true);
    }

    public static void updateJoinedObjsForLinksToBikNodes(INamedSqlsDAO<Object> metadataPumpAdhocDao,
            String urlBase, Map<Integer, String> nodeIdToDescrMap) {

        List<LinkToBikNodeBean> list = extractLinksToBikNodes(urlBase, nodeIdToDescrMap);

        if (list.isEmpty()) {
            return; // nie można wywołać poniższego bo nie przyjmuje pustej listy
        }

        // założenie jest takie, że list nie może być puste, więc sprawdzanie na początku
        // czy nodeIdToDescrMap.isEmpty() ma sens i nam to załatwia
        int batchSize = 32;
        for (int i = 0; i < (list.size() - 1) / batchSize + 1; i++) {
            final List<LinkToBikNodeBean> l = list.subList(i * batchSize, Math.min(i * batchSize + batchSize, list.size()));

            metadataPumpAdhocDao.execNamedCommand("updateJoinedObjsForLinksToBikNodes", l);
        }
    }

    public static void addAuthorsToRanking(INamedSqlsDAO<Object> metadataPumpAdhocDao) {
        metadataPumpAdhocDao.execNamedCommand("confluenceAddAuthorsToRanking");
    }

    public static List<LinkToBikNodeBean> extractLinksToBikNodes(String urlBase, Map<Integer, String> nodeIdToDescrMap) {
        List<LinkToBikNodeBean> list = new ArrayList<LinkToBikNodeBean>();

        for (Map.Entry<Integer, String> e : nodeIdToDescrMap.entrySet()) {
            int nodeId = e.getKey();
            String descr = e.getValue();

            extractLinksToBikNodesFromSingleHtml(urlBase, list, nodeId, descr);
        }

        return list;
    }

    // urlBase nie zawiera '#', tniemy potem aż znajdziemy pierwszy #
    public static void extractLinksToBikNodesFromSingleHtml(String urlBase,
            Collection<LinkToBikNodeBean> links, int srcObjId, String html) {

//        System.out.println("extractLinksToBikNodesFromSingleHtml: urlBase=[" + urlBase + "], html:\n"
//                + html);
        IRegExpMatcher m = HREF_PATT.matcher(html);

        boolean addedAnything = false;

        while (m.find()) {

            String url = m.group(1).trim();
//            System.out.println("extractLinksToBikNodesFromSingleHtml: found url=[" + url + "]");

            String urlRest = BaseUtils.dropPrefixOrGiveNull(url, urlBase);

            if (urlRest == null) {
                continue;
            }

            int hashPos = urlRest.indexOf("#");
            if (hashPos < 0) {
                continue;
            }

            urlRest = urlRest.substring(hashPos + 1);

            try {
                urlRest = URLDecoder.decode(urlRest, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                throw new LameRuntimeException(ex);
            }

            Pair<String, String> treeCodeAndId = BaseUtils.splitString(urlRest, "/");
            final String idAndOptNameStr = treeCodeAndId.v2;
            if (idAndOptNameStr == null) {
                continue;
            }
            Pair<String, String> idAndOptName = BaseUtils.splitString(idAndOptNameStr, "|");

            Integer nodeId = BaseUtils.tryParseInteger(idAndOptName.v1);

            if (nodeId == null && idAndOptName.v2 == null) {
                continue;
            }

//            String decodedName;
//            try {
//                decodedName = idAndOptName.v2 == null ? null : URLDecoder.decode(idAndOptName.v2, "UTF-8");
//            } catch (UnsupportedEncodingException ex) {
//                throw new LameRuntimeException(ex);
//            }
            final LinkToBikNodeBean linkToBikNodeBean = new LinkToBikNodeBean(srcObjId, treeCodeAndId.v1, nodeId, idAndOptName.v2);

            links.add(linkToBikNodeBean);

//            System.out.println("extractLinksToBikNodesFromSingleHtml: added link-bean=[" + linkToBikNodeBean + "]");
            addedAnything = true;
        }

        if (!addedAnything) {
            links.add(new LinkToBikNodeBean(srcObjId, null, null, null));
        }
    }
}
