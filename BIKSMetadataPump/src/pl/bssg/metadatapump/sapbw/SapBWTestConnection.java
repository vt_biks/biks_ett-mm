/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbw;

import java.io.File;
import pl.bssg.metadatapump.MetadataTestConnectionBase;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class SapBWTestConnection extends MetadataTestConnectionBase {

    protected String path;

    public SapBWTestConnection(MssqlConnectionConfig mcfg) {
        super(mcfg);
    }

    @Override
    public Pair<Integer, String> testConnection() {
        path = execNamedQuerySingleVal("getConfigValue", false, "value", "sapbw.path");

        File file = new File(path);
        if (!file.exists()) {
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Wrong path or file does not exists!");
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, PumpConstants.PUMP_TEST_CONNECTION_OK);
    }

}
