package pl.trzy0.foxy.commons;

public interface IFoxyAsyncCallback {

    public String getLabel();

    public void setLabel(String label);

    public void onSuccess(String txt);

    public void onFailure(String msg, Throwable cause);
}
