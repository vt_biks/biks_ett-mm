/*

  0. ka�dy w�ze� mo�e by�:
  0.1. nieodwiedzony
  0.2. w trakcie odwiedzin
  0.3. odwiedzony

  1. oznacz wszystkie w�z�y jako nieodwiedzone
  
  2. lp = 1
  
  3. dla ka�dego w�z�a w:
  3.1. je�eli w jest odwiedzony - kontynuuj p�tl�

  PROCEDURA ODWIED�(w)
  3.2. oznacz w jako w trakcie odwiedzin
  3.3. w.lp = lp
  3.4. w.minlp = lp
  3.4. lp += 1
  
  3.5. dla ka�dego v - czyli w�z�a do kt�rego jest kraw�d� z w:
  
  3.5.1. je�eli v jest w trakcie odwiedzin -> 
  3.5.1.1. w.minlp = min(w.minlp, v.lp)
  
  3.5.2. je�eli v jest nieodwiedzony ->
  3.5.2.1. wywo�aj ODWIED�(v)
  3.5.2.2. w.minlp = min(w.minlp, v.minlp)
  
  3.6. je�eli w.lp = w.minlp ->
  3.6.1. dla wszystkich w�z��w, kt�re s� w trakcie odwiedzin i maj� lp >= w.lp
  3.6.1.1. oznacz jako odwiedzone, ustaw minlp = w.minlp (nale�� do klastra w�z�a w)       
     
  --------------   
     
  stos: <w, v, f>, gdzie:
    w - nadrz�dny w�ze�, mo�e by� null
    v - aktualny w�ze� do przetworzenia   
    f - flaga okre�laj�ca typ przetwarzania w p�tli (0, 1)
  mo�liwe postaci warto�ci na stosie
    <w, v, 0> - wywo�anie ODZIED�(w)
      je�eli w = null to jest inicjalny poziom rekurencji, w przeciwnym razie, to jest wywo�anie rekurencyjne
      ODWIED�(v) w trakcie wywo�ania ODWIED�(w)
    <w, v, 1> - w�a�ciwa p�tla po w�z�ach osi�galnych z w�z�a w
    <w, v, 2> - po przej�ciu wewn�trznej p�tli dla v, nale�y zbada� czy v robi klaster,
      a potem ew. (gdy w <> null) uaktualni� w.minlp - bo w�a�nie sko�czy�o si� rekurencyjne wywo�anie
      ODWIED�(v) w trakcie wywo�ania ODWIED�(w)
    
  1. oznacz wszystkie w�z�y jako nieodwiedzone
  
  2. lp = 1
    
  3. dla wszystkich w�z��w w, umie�� na stosie <null, w, 0>
  
  4. dop�ki stos nie jest pusty:
  4.1. zdejmij <w, v, f> ze stosu

  4.2. je�eli f = 0 ->
  4.2.1. oznacz v jako w trakcie odwiedzin
  4.2.2. v.lp = lp
  4.2.3. v.minlp = lp
  4.2.4. lp += 1
  4.2.5. wrzu� na stos <w, v, 2>
  4.2.6. wrzu� na stos <v, q, 1> - dla ka�dego q osi�galnego bezpo�rednio z v

  4.3. je�eli f = 1 ->
  4.3.1. je�eli v jest w trakcie odwiedzin ->
  4.3.1.1. w.minlp = min(w.minlp, v.minlp)
  4.3.2. je�eli v jest nieodwiedzone ->
  4.3.2.1. wrzu� na stos <w, v, 0>

  4.4. je�eli f = 2 (czyli wszystkie dzieci v s� przetworzone w wewn. p�tli) ->
  4.4.1. je�eli v.lp = v.minlp ->
  4.4.1.1. dla ka�dego w�z�a q:
  4.4.1.1.1. je�eli q.lp >= v.lp i q jest w trakcie odwiedzin ->
  4.4.1.1.1.1. oznacz q jako odwiedzony, ustaw q.minlp = v.lp
  4.4.2. je�eli w <> null ->
  4.4.2.1. w.minlp = min(w.minlp, v.minlp)
  
       
*/


if exists(select * from sys.objects where object_id = object_id(N'itscc_graph_edge') and type in (N'U'))
  drop table itscc_graph_edge;
go

if exists(select * from sys.objects where object_id = object_id(N'itscc_graph_node') and type in (N'U'))
  drop table itscc_graph_node;
go

create table itscc_graph_node (node_id varchar(17) not null primary key);
go

create table itscc_graph_edge (src_node_id varchar(17) not null references itscc_graph_node(node_id),
  dst_node_id varchar(17) not null references itscc_graph_node(node_id),
  unique (src_node_id, dst_node_id), unique (dst_node_id, src_node_id));
go


-------------------------------------------------------
-------------------------------------------------------
-------------------------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_itscc_init_data]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_itscc_init_data]
go

create procedure [dbo].[sp_itscc_init_data](@multiplyDataCnt int) as
begin
  set nocount on

  declare @start_time datetime = getdate();

  print cast(sysdatetime() as varchar(23)) + ': [sp_itscc_init_data] preparing data, @multiplyDataCnt: ' + cast(@multiplyDataCnt as varchar(20));

  delete from itscc_graph_edge;
  delete from itscc_graph_node;

  insert into itscc_graph_node (node_id) values
  ('0'), ('A'), ('B'), ('C'), ('D'), ('E'), ('X'), ('Y'), ('Z'),
  ('A0'), ('A1'), ('A2'), ('A3'), ('A4'), ('A5'), ('A6'), ('A7');

  insert into itscc_graph_edge (src_node_id, dst_node_id) values
  ('A', 'B'),
  ('B', 'C'),
  ('C', 'A'),
  ('A', 'D'),
  ('D', '0'),
  ('0', 'E'),
  ('E', 'C'),
  ('Z', 'Y'),
  ('Y', 'X'),
  ('X', 'Z'),
  ('A', 'X'),
  ('Z', '0'),
  ('A0', 'A1'),
  ('A1', 'A2'),
  ('A1', 'A3'),
  ('A2', 'A4'),
  ('A3', 'A4'),
  ('A3', 'A7'),
  ('A4', 'A5'),
  ('A5', 'A6'),
  ('A2', 'A'),
  ('A5', 'A0'),
  --('A7', 'Z')
  ('Z', 'A1');

  -------------------------------------------------------

  declare @nodes table (node_id varchar(17) not null primary key);
  
  insert into @nodes select node_id from itscc_graph_node;
  
  declare @edges table (src_node_id varchar(17) not null, dst_node_id varchar(17) not null);
  
  insert into @edges select src_node_id, dst_node_id from itscc_graph_edge;

  --declare @multiplyDataCnt int = 100;

  declare @extra_random_edge_cnt int = cast(@multiplyDataCnt * 6 * rand() as int);

  declare @sep varchar(17) = '$';

  declare @prefix varchar(17);

  while @multiplyDataCnt > 0
  begin
    set @prefix = @sep + cast(@multiplyDataCnt as varchar(17)) + @sep;

    insert into itscc_graph_node (node_id)
    select @prefix + node_id
    from 
      --itscc_graph_node where node_id not like @sep + '%';
      @nodes;
      
    insert into itscc_graph_edge (src_node_id, dst_node_id)
    select @prefix + src_node_id, @prefix + dst_node_id
    from 
      --itscc_graph_edge where src_node_id not like @sep + '%';
      @edges;
      
    set @multiplyDataCnt -= 1;
  end;

  print cast(sysdatetime() as varchar(23)) + ': [sp_itscc_init_data] nodes and edges multiplicated, adding extra edges: ' +
    cast(@extra_random_edge_cnt as varchar(20));

  create table #numbered_nodes (idx int not null identity primary key, node_id varchar(17) not null);
  
  insert into #numbered_nodes (node_id) select node_id from itscc_graph_node;

  declare @node_cnt int = (select count(*) from itscc_graph_node);

  while @extra_random_edge_cnt > 0
  begin
    declare @src_idx int = cast(rand() * @node_cnt + 1 as int);
    declare @dst_idx int = cast(rand() * @node_cnt + 1 as int);

    declare @src_node_id varchar(17) = (select node_id from #numbered_nodes where idx = @src_idx);
    declare @dst_node_id varchar(17) = (select node_id from #numbered_nodes where idx = @dst_idx);
    
    --declare @src_node_id varchar(17) = (select top 1 node_id from itscc_graph_node order by newid());
    --declare @dst_node_id varchar(17) = (select top 1 node_id from itscc_graph_node order by newid());

    if @src_node_id = @dst_node_id or exists (select 1 from itscc_graph_edge where src_node_id = @src_node_id and dst_node_id = @dst_node_id)
      continue;

    insert into itscc_graph_edge (src_node_id, dst_node_id)
    values (@src_node_id, @dst_node_id);

    set @extra_random_edge_cnt -= 1;
  end;

  declare @end_time datetime = getdate();

  declare @edge_cnt int = (select count(*) from itscc_graph_edge);
  
  print cast(sysdatetime() as varchar(23)) + ': [sp_itscc_init_data] data prepared, nodes: ' + cast(@node_cnt as varchar(20)) +
  ', edges: ' + cast(@edge_cnt as varchar(20)) +
  ', time taken: ' + cast(datediff(ms, @start_time, @end_time) as varchar(30)) + ' ms';
end;
go


-------------------------------------------------------
-------------------------------------------------------
-------------------------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_itscc_calc_scc_v1]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_itscc_calc_scc_v1]
go

create procedure [dbo].[sp_itscc_calc_scc_v1] as
begin
  set nocount on;

  declare @diag_level int = 0;

  if @diag_level >= 0 print cast(sysdatetime() as varchar(23)) + ': [sp_itscc_calc_scc_v1] start';

  declare @idx int = 1;

  declare @stack table (i int not null identity primary key, w_id varchar(17) null, v_id varchar(17) not null, f int not null, unique (w_id, v_id, f));

  insert into @stack (w_id, v_id, f) select null, node_id, 0 from itscc_graph_node;

  declare @node_state table (node_id varchar(17) not null primary key, state int not null, idx int not null, lowlink int not null,
    unique(idx, state, node_id));

  insert into @node_state (node_id, state, idx, lowlink) select node_id, 0, 0, 0 from itscc_graph_node;

  declare @pass int = 0;

  while exists (select 1 from @stack)
  begin
    if @diag_level > 0
      print '
    ----------------------------- pass #' + cast(@pass as varchar(20)) + ' -----------------------------

    ';
    if @diag_level > 1
    begin
      select * from @stack order by i;
      select * from @node_state order by node_id;
    end;
    
    declare @max_i int = (select max(i) from @stack);
    declare @w_id varchar(17), @v_id varchar(17), @f int;
    declare @v_state int, @v_idx int, @v_lowlink int;
    
    select @w_id = w_id, @v_id = v_id, @f = f from @stack where i = @max_i;
    delete from @stack where i = @max_i;
    
    select @v_state = state, @v_idx = idx, @v_lowlink = lowlink from @node_state where node_id = @v_id;

    if @diag_level > 0
    print '@w_id = ' + coalesce(@w_id, '<null>') + ', @v_id = ' + @v_id + ', @f = ' + cast(@f as varchar(20)) +
      ', @v_state = ' + cast(@v_state as varchar(20)) + ', @v_idx = ' + cast(@v_idx as varchar(20)) + ', @v_lowlink = ' + cast(@v_lowlink as varchar(20));

    if @f = 0
    begin
      if @v_state = 2 continue;
      
      update @node_state set state = 1, idx = @idx, lowlink = @idx where node_id = @v_id;
      
      set @idx += 1;
      
      insert into @stack (w_id, v_id, f) values (@w_id, @v_id, 2);
      
      insert into @stack (w_id, v_id, f) select @v_id, dst_node_id, 1 from itscc_graph_edge where src_node_id = @v_id;
      
      --print 'stack after inserts:';    
      --select * from @stack order by i;
            
    end else if @f = 1
    begin
      /*
        4.3. je�eli f = 1 ->
        4.3.1. je�eli v jest w trakcie odwiedzin ->
        4.3.1.1. w.minlp = min(w.minlp, v.minlp)
        4.3.2. je�eli v jest nieodwiedzone ->
        4.3.2.1. wrzu� na stos <w, v, 0>
      */

      if @v_state = 1
        update @node_state set lowlink = case when lowlink > @v_lowlink then @v_lowlink else lowlink end where node_id = @w_id;
      else if @v_state = 0
        insert into @stack (w_id, v_id, f) values (@w_id, @v_id, 0);
          
    end else if @f = 2
    begin
      /*
        4.4. je�eli f = 2 (czyli wszystkie dzieci v s� przetworzone w wewn. p�tli) ->
        4.4.1. je�eli v.lp = v.minlp ->
        4.4.1.1. dla ka�dego w�z�a q:
        4.4.1.1.1. je�eli q.lp >= v.lp i q jest w trakcie odwiedzin ->
        4.4.1.1.1.1. oznacz q jako odwiedzony, ustaw q.minlp = v.lp
        4.4.2. je�eli w <> null ->
        4.4.2.1. w.minlp = min(w.minlp, v.minlp)
      */
      
      if @v_idx = @v_lowlink
        update @node_state set state = 2, lowlink = @v_idx where idx >= @v_idx and state = 1;
      
      if @w_id is not null
        update @node_state set lowlink = case when lowlink > @v_lowlink then @v_lowlink else lowlink end where node_id = @w_id;        
    end;

    set @pass += 1;
    
    --if @pass > 5 break;
  end;


  if @diag_level >= 0 print cast(sysdatetime() as varchar(23)) + ': [sp_itscc_calc_scc_v1] finished after ' + cast(@pass as varchar(20)) + ' passes'

  select * from @node_state;
end;
go

-------------------------------------------------------
-------------------------------------------------------
-------------------------------------------------------

/*

exec sp_itscc_init_data 2000;
exec sp_itscc_calc_scc_v1;

*/
