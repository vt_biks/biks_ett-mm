/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons.templates;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public class TemplateUtils {
    
    public static String render(IParsedTemplate pt, Map<String, Object> vars, Object model) {
        if (pt == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        pt.render(sb, vars, model);
        return sb.toString();
    }

    public static String render(INamedTemplateProvider tp, String templateName,
            Map<String, Object> vars, Object model) {
        return render(tp, templateName, vars, model, false);
    }

    public static String render(INamedTemplateProvider tp, String templateName,
            Map<String, Object> vars, Object model, boolean allowNoTemplate) {
        IParsedTemplate pt = tp.getTemplate(templateName);

        if (allowNoTemplate && pt == null) {
            return null;
        }
        
        return render(pt, vars, model);
    }
}
