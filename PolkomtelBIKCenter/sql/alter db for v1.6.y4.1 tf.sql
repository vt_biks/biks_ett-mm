﻿exec sp_check_version '1.6.y4.1';
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')   
declare @appProp int = (select id from bik_node where tree_id = @tree_id and obj_id  = '#admin:dict:bikAppProps')
update bik_node set is_deleted = 1 where id = @appProp

if not exists (select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[bik_joined_objs]') and name = N'idx_bik_joined_objs_src_node_id_dst_node_id_inherit_to_descendants')
begin
	create unique index idx_bik_joined_objs_src_node_id_dst_node_id_inherit_to_descendants on bik_joined_objs(src_node_id, dst_node_id, inherit_to_descendants);
end;
go

if not exists (select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[bik_joined_objs]') and name = N'idx_bik_joined_objs_dst_node_id_src_node_id_inherit_to_descendants')
begin
	create unique index idx_bik_joined_objs_dst_node_id_src_node_id_inherit_to_descendants on bik_joined_objs(dst_node_id, src_node_id, inherit_to_descendants);
end;
go

exec sp_update_version '1.6.y4.1', '1.6.y4.2';
go
