/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.foxygwtcommons.grid.IBeanProvider;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

public class ChangableMetadataBeanProvider implements IBeanProvider<LisaDictColumnBean> {

    public Integer categoryId;

    ChangableMetadataBeanProvider(int nodeId) {
        categoryId = nodeId;
    }

    public void getBeans(int offset, int limit, Iterable<String> sortCols, Iterable<String> descendingCols,
            final IParametrizedContinuation<Pair<? extends Iterable<LisaDictColumnBean>, Integer>> showBeansCont) {

        BIKClientSingletons.getLisaService().getDictionaryColumnsByNodeId(categoryId, new StandardAsyncCallback<List<LisaDictColumnBean>>() {

            public void onSuccess(List<LisaDictColumnBean> result) {
                Pair<List<LisaDictColumnBean>, Integer> p = new Pair<List<LisaDictColumnBean>, Integer>();
                p.v1 = result;
                p.v2 = result.size();

                showBeansCont.doIt(p);
            }
        });

    }
}
