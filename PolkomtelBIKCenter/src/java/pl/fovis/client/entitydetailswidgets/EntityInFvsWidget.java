/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.SimilarFavouriteDialog;
import pl.fovis.common.ObjectInFvsChangeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.PushButtonWithHideText;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author młydkowski
 */
public class EntityInFvsWidget extends EntityDetailsWidgetFlatBase {

    protected FlowPanel fp;
    protected IContinuation refreshContinuation;
//    protected BikCustomButton btn;
//    public static final Set<String> gainAccessForSiKinds =
//            BaseUtils.splitUniqueBySep(I18n.raportWebiRaportFlashSwiatObiekt_wtf.get() /* i18n:  */, ",", true);
    protected List<Integer> nodeIds = new ArrayList<Integer>();

    @Override
    public void refreshWidgetsState() {
        // szczególny przypadek - budujemy subwidgety przy każdym odświeżeniu
        displayData();
    }

    @Override
    public Widget buildWidgets() {
        fp = new FlowPanel();
        fp.setStyleName("BizDef-toFav");
        return fp;
    }

    @Override
    public void displayData() {
        fp.clear();
        //this.data = data;

        final Boolean isInFvs = getData().isInFav;
//        final List<ObjectInFvsChangeBean> similarFavouriteWithSimilarityScore = getData().similarFavouriteWithSimilarityScore;
        final PushButtonWithHideText btn = new PushButtonWithHideText(createImage(isInFvs));
        btn.addClickHandler(new ClickHandler() {

            List<ObjectInFvsChangeBean> checkedSimilarFFvs = new ArrayList<ObjectInFvsChangeBean>();
            Boolean isObjInFvs = isInFvs;

            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().getSimilarFavouriteWithSimilarityScore(nodeId, new StandardAsyncCallback<List<ObjectInFvsChangeBean>>("Error in getSimilarFavouriteWithSimilarityScore") {

                    @Override
                    public void onSuccess(List<ObjectInFvsChangeBean> similarFavouriteWithSimilarityScore) {
                        if (!BaseUtils.isCollectionEmpty(similarFavouriteWithSimilarityScore)) {
                            for (ObjectInFvsChangeBean sf : similarFavouriteWithSimilarityScore) {
                                if (nodeIds.contains(sf.nodeId)) {
                                    checkedSimilarFFvs.add(sf);
                                }
                            }
                            similarFavouriteWithSimilarityScore.removeAll(checkedSimilarFFvs);
                        }
                        nodeIds.clear();
                        if (!isObjInFvs && !BaseUtils.isCollectionEmpty(similarFavouriteWithSimilarityScore)) {
                            new SimilarFavouriteDialog().buildAndShowDialog(nodeId, getNodeName(), getNodeKindCode(), getNodeTreeCode(), similarFavouriteWithSimilarityScore, new IParametrizedContinuation<List<Integer>>() {

                                @Override
                                public void doIt(List<Integer> saveCont) {
                                    nodeIds.add(nodeId);
                                    nodeIds.addAll(saveCont);

                                    BIKClientSingletons.getService().addToFvs(nodeIds, new StandardAsyncCallback<Boolean>() {

                                        @Override
                                        public void onSuccess(Boolean result) {
                                            isObjInFvs = refreshImgAndShowMessageFvs(isObjInFvs, result, btn);
                                        }
                                    });
                                }
                            });
                        } else {
                            AsyncCallback<Boolean> myCallback = new StandardAsyncCallback<Boolean>() {

                                @Override
                                public void onSuccess(final Boolean result) {
                                    isObjInFvs = refreshImgAndShowMessageFvs(isObjInFvs, result, btn);
                                }

                            };

                            if (isObjInFvs) {
                                BIKClientSingletons.getService().removeFromFvs(nodeId, myCallback);
                            } else {
                                nodeIds.add(nodeId);
                                BIKClientSingletons.getService().addToFvs(nodeIds, myCallback);
                            }
                        }
                    }
                });
            }
        });
        btn.setTitle(setTitle(isInFvs)/* ? "Usuń z ulubionych" : "Dodaj do ulubionych"*/);
        btn.setStyleName("inFvs");
        btn.setVisible(BIKClientSingletons.isUserLoggedIn());

        fp.add(btn);

    }

    protected boolean refreshImgAndShowMessageFvs(Boolean isObjInFvs, boolean result, PushButtonWithHideText btn) {
        String message = I18n.obiekt.get() /* I18N:  */ + " " + (isObjInFvs ? I18n.usunietoZ.get() /* I18N:  */ : I18n.dodanoDo.get() /* I18N:  */) + " " + I18n.ulubionych.get() /* I18N:  */;

        isObjInFvs = result;
        if (isWidgetRunFromDialog()) {
            Window.alert(message);
        }
        btn.setTitle(setTitle(isObjInFvs));
        btn.getUpFace().setImage(createImage(isObjInFvs));
        BIKClientSingletons.showInfo(message);
        if (refreshContinuation != null) {
            refreshContinuation.doIt();
        }
        return isObjInFvs;
    }

    protected String setTitle(boolean isInFvs) {
        return isInFvs ? I18n.usunZUlubionych.get() /* I18N:  */ : I18n.dodajDoUlubionych.get() /* I18N:  */;
    }

    protected Image createImage(boolean isInFvs) {
        return new Image("images" /* I18N: no */ + "/" + (isInFvs ? "fav.gif" /* I18N: no */ : "addToFav." + "gif" /* I18N: no */));
    }

    public boolean isEnabledDeprecated(String code, int count) {
        return true;
    }

    public void setRefreshContinuation(IContinuation refreshContinuation) {
        this.refreshContinuation = refreshContinuation;
    }
}
