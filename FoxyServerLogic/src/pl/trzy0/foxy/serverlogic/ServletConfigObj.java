package pl.trzy0.foxy.serverlogic;

public class ServletConfigObj {

    public String httpBaseAddr;
    public String httpBaseAddrForServlet;
    public String httpBaseAddrForResource;
    public String dirForUpload;
    public boolean usesSubdomains;
}
