/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class MsSqlExPropConfigBean implements Serializable {

    public String exPropDatabase;
    public String exPropTable;
    public String exPropView;
    public String exPropProcedure;
    public String exPropScalarFunction;
    public String exPropTableFunction;
    public String exPropColumn;
}
