﻿exec sp_check_version '1.1.1.1';
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

-- naprawianie podlinkowanych - prostujemy na zwykłe
-- rzuca błędem: nie moze updatowac na te same wartosc
/*update bik_joined_objs 
set 
  src_node_id = coalesce(ns.linked_node_id, ns.id),
  dst_node_id = coalesce(nd.linked_node_id, nd.id)
-- select coalesce(ns.linked_node_id, ns.id), coalesce(nd.linked_node_id, nd.id)
from 
bik_joined_objs jo inner join bik_node ns on jo.src_node_id = ns.id inner join bik_node nd on nd.id = jo.dst_node_id
where (ns.linked_node_id is not null or nd.linked_node_id is not null)
--and jo.type = 0 
and ns.is_deleted = 0
and not exists (select 1 from bik_joined_objs joj where src_node_id = coalesce(ns.linked_node_id, ns.id) and dst_node_id = coalesce(nd.linked_node_id, nd.id))
go*/

-- naprawa podlinkowanych: reczna
create table #tmp(
		src_node_id int, 
		dst_node_id int,
		src_node_id_new int, 
		dst_node_id_new int,
		type int,
		inherit_to_descendants int 
)
go
--napchanie tabelki danymi
insert into #tmp(src_node_id, dst_node_id, src_node_id_new, dst_node_id_new, type, inherit_to_descendants)
select src_node_id, dst_node_id, coalesce(ns.linked_node_id, ns.id), coalesce(nd.linked_node_id, nd.id), type, inherit_to_descendants 
from 
	bik_joined_objs jo inner join bik_node ns on jo.src_node_id = ns.id inner join bik_node nd on nd.id = jo.dst_node_id
where (ns.linked_node_id is not null or nd.linked_node_id is not null)
	and ns.is_deleted = 0
	and not exists (select 1 from bik_joined_objs joj where src_node_id = coalesce(ns.linked_node_id, ns.id) and dst_node_id = coalesce(nd.linked_node_id, nd.id))
go

--usunięcie z bik_joned_obj
delete from bik_joined_objs
from  
	bik_joined_objs jo inner join #tmp on jo.src_node_id = #tmp.src_node_id and jo.dst_node_id = #tmp.dst_node_id
go

--dodanie na nowo
insert into bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
select distinct src_node_id_new, dst_node_id_new, type, inherit_to_descendants
from #tmp
go

drop table #tmp
go


---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- naprawiamy obsługę podlinkowanych - po pierwsze pozwalamy na takie powiązania (o ile w drzewku widać podlinkowane),
-- po drugie - przełączamy je na oryginał

-- poprzednia wersja w: alter db for v1.0.99.2 ww.sql
if object_id('sp_update_Joined_Objs_For_Node') is not null
  drop procedure sp_update_Joined_Objs_For_Node
go

create procedure sp_update_Joined_Objs_For_Node(@src_id int, @no_linked_nodes int, @joined_obj_modes bik_joined_obj_modes_table_type readonly) as
begin
  set nocount on
  declare @diags_level int = 1 -- 0 oznacza brak logowania

  declare @tmp_joined_objs table (dst_node_id int not null primary key, type int not null, inherit_to_descendants int not null)
  
  declare @to_join table (dst_id int not null, mode int null, level int not null, branch_ids varchar(2000) not null)

  insert into @to_join (dst_id, mode, level, branch_ids)
  select t.dst_id, t.mode, t.level, n.branch_ids
  from @joined_obj_modes t inner join bik_node n on t.dst_id = n.id

  declare to_join cursor for 
  select dst_id, mode, level, branch_ids from @to_join
  order by branch_ids

  open to_join

  declare @dst_id int, @mode int, @level int, @branch_ids varchar(2000)

  fetch next from to_join into @dst_id, @mode, @level, @branch_ids

  while @@fetch_status = 0
  begin
    if @diags_level > 0 begin
      print 'row in loop: @dst_id=' + cast(@dst_id as varchar(20)) + ', @mode=' + cast(@mode as varchar(20)) + ', @branch_ids=' + @branch_ids
    end
  
    declare @br_ids_for_like varchar(3000) = @branch_ids + '%'

    if @level = 0 -- this node only (@dst_id)
    begin
      delete from @tmp_joined_objs
      where dst_node_id = @dst_id

  	  if @mode is not null begin
  	    declare @linked_node_id_single int = (select linked_node_id from bik_node where id = @dst_id)
  	  
        insert into @tmp_joined_objs (dst_node_id, type, inherit_to_descendants)
        values (coalesce(@linked_node_id_single, @dst_id), case when @mode = 0 then -1 else 0 end, case when @mode = 1 then 1 else 0 end)
        if @diags_level > 0 begin
          print 'row in loop: inserted single'
        end        
      end
    end else if @level = 1 -- direct subnodes only
    begin
        delete from @tmp_joined_objs
        where dst_node_id in (select id from bik_node where parent_node_id = @dst_id)

  	  if @mode is not null begin
  	    insert into @tmp_joined_objs (dst_node_id, type, inherit_to_descendants)
	    select coalesce(n.linked_node_id, n.id), case when @mode = 0 then -1 else 0 end, case when @mode = 1 then 1 else 0 end
	    from bik_node n left join bik_joined_objs jo on n.id = jo.dst_node_id and jo.src_node_id = @src_id
	    where n.parent_node_id = @dst_id and (jo.id is null or jo.type = 0) and n.is_deleted = 0 and (@no_linked_nodes = 0 or n.linked_node_id is null)
      end
    end else -- all subdones
    begin
      delete from @tmp_joined_objs
      where dst_node_id <> @dst_id and dst_node_id in (select id from bik_node where branch_ids like @br_ids_for_like)

	  if @mode is not null begin
        insert into @tmp_joined_objs (dst_node_id, type, inherit_to_descendants)
        select coalesce(n.linked_node_id, n.id), case when @mode = 0 then -1 else 0 end, case when @mode = 1 then 1 else 0 end
        from bik_node n left join bik_joined_objs jo on n.id = jo.dst_node_id and jo.src_node_id = @src_id
        where n.id <> @dst_id and n.branch_ids like @br_ids_for_like and (jo.id is null or jo.type = 0) and n.is_deleted = 0 and (@no_linked_nodes = 0 or n.linked_node_id is null)
      end
    end
    fetch next from to_join into @dst_id, @mode, @level, @branch_ids
  end

  close to_join;

  deallocate to_join;

  if @diags_level > 0 begin
    select * from @tmp_joined_objs
  end

  declare @src_branch_ids varchar(2000) = (select branch_ids from bik_node where id = @src_id)

  declare @fixed_dst_node_tab table (node_id int not null primary key)

  insert into @fixed_dst_node_tab (node_id)
  select distinct jo.dst_node_id from bik_joined_objs jo inner join bik_node n on jo.dst_node_id = n.id
  where src_node_id in (
  select ancestor_id from (
  select cast(str as int) as ancestor_id, idx 
  from dbo.fn_split_by_sep(@src_branch_ids, '|', 7)
  ) x 
  where ancestor_id <> @src_id
  ) and jo.inherit_to_descendants = 1 and n.is_deleted = 0

  insert into @fixed_dst_node_tab (node_id)
  select dst_node_id from bik_joined_objs where src_node_id = @src_id and type = 1

  delete from bik_joined_objs where dst_node_id in (select dst_node_id from @tmp_joined_objs) and src_node_id = @src_id and
  dst_node_id not in (select node_id from @fixed_dst_node_tab)

  insert into bik_joined_objs (src_node_id, dst_node_id, type, inherit_to_descendants)
  select @src_id, dst_node_id, 0, inherit_to_descendants
  from @tmp_joined_objs
  where type <> -1 and dst_node_id not in (select node_id from @fixed_dst_node_tab) and dst_node_id <> @src_id

  declare @fixed_src_node_tab table (node_id int not null primary key)

  insert into @fixed_src_node_tab (node_id)
  select jo.src_node_id from bik_joined_objs jo inner join @tmp_joined_objs t on jo.src_node_id = t.dst_node_id
  where jo.dst_node_id = @src_id and jo.type = 1

  delete from bik_joined_objs where src_node_id in (select dst_node_id from @tmp_joined_objs) and dst_node_id = @src_id and
  src_node_id not in (select node_id from @fixed_src_node_tab)

  insert into bik_joined_objs (src_node_id, dst_node_id, type, inherit_to_descendants)
  select dst_node_id, @src_id, 0, inherit_to_descendants
  from @tmp_joined_objs
  where type <> -1 and dst_node_id not in (select node_id from @fixed_src_node_tab) and dst_node_id <> @src_id
end
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

/*
-- do zastanowienia co zrobić z tymi przypadkami - tu są ręczne powiązania, ale są niesymetryczne!

select * from bik_joined_objs jo inner join bik_node ns on jo.src_node_id = ns.id inner join bik_node nd on nd.id = jo.dst_node_id
where jo.type = 0 and not exists (select 1 from bik_joined_objs jo2 where jo2.src_node_id = jo.dst_node_id and jo2.dst_node_id = jo.src_node_id)
and ns.is_deleted = 0
and nd.is_deleted = 0
*/

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

exec sp_update_version '1.1.1.1', '1.1.1.2';
go
