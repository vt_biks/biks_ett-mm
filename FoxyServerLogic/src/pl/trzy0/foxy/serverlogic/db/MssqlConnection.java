/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import com.microsoft.sqlserver.jdbc.SQLServerXADataSource;
import commonlib.connectionpool.ConnectionPoolManagerViaDataSource;
import java.sql.Connection;
import pl.trzy0.foxy.serverlogic.IConnectionDialectHelper;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class MssqlConnection extends PooledJdbcConnectionBase<MssqlConnectionConfig> {

    public MssqlConnection() {
    }

    public MssqlConnection(MssqlConnectionConfig config) {
        initConnectionFinal(config);
    }

    @Override
    protected IConnectionDialectHelper createDialectHelper() {
        return new SqlValueFormatterCurlyTimeStamp();
    }

    @Override
    public void initConnection(MssqlConnectionConfig cfg) {
        SQLServerXADataSource ds = new SQLServerXADataSource();
        Pair<String, String> p = BaseUtils.splitString(cfg.server, ":");
        ds.setServerName(p.v1);
        Integer portNum = BaseUtils.tryParseInteger(p.v2);
        if (portNum != null) {
            ds.setPortNumber(portNum);
        }
        if (!BaseUtils.isStrEmptyOrWhiteSpace(cfg.instance)) {
            ds.setInstanceName(cfg.instance);
        }
        boolean integrated = cfg.user == null;
        ds.setIntegratedSecurity(integrated);
        if (!integrated) {
            ds.setUser(cfg.user);
            ds.setPassword(cfg.password);
        }
        ds.setDatabaseName(cfg.database);

        this.cpm = //new ConnectionPoolManagerByDataSource(ds);
                //new ConnectionPoolManagerByPooledConnection(ds);
                //new ConnectionPoolManagerMini2(ds);
                new ConnectionPoolManagerViaDataSource(ds, 30, 30, new IParametrizedContinuation<Connection>() {
                    public void doIt(Connection param) {
                        execCommand("SET NOCOUNT ON");
                    }
                });
    }

    public static String quoteIdent(String ident) {
        return ident == null ? null : "[" + ident.replace("]", "]]") + "]";
    }
}
