/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.Grid;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class PickIconDialog extends BaseActionOrCancelDialog {

    protected Grid grid;
    protected String selectedIcon;
    protected IParametrizedContinuation<String> con;
    protected Set<String> icons;

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzIkone.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.wybierz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        grid = BIKClientSingletons.createSelectIconView(icons, selectedIcon);

        main.add(grid);
    }

    @Override
    protected void doAction() {
        selectedIcon = BIKClientSingletons.getSelectedIconFromView(grid);
        con.doIt(selectedIcon);
    }

    public void buildAndShowDialog(String selectedIcon, Set<String> icons, IParametrizedContinuation<String> con) {
        this.con = con;
        this.icons = icons;
        this.selectedIcon = (selectedIcon == null ? BIKGWTConstants.DEFAULT_NODE_KIND_ICON_NAME : selectedIcon);
        super.buildAndShowDialog();
    }
}
