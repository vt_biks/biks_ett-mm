begin tran

/*
  select * from bik_node where tree_id = 14
  select * from bik_tree
*/

insert into bik_node (parent_node_id, node_kind_id, name, tree_id)
values (32390, 22, 'wwX', 14)

declare @node_id int 

select @node_id = @@identity

/*
set transaction isolation level read uncommitted
set transaction isolation level read committed

exec sp_node_init_branch_id 14, null

insert into bik_user (login_name, name, password, email, node_id, user_kind_id)
values ('wwZ', 'ww', 'w', 'w', @node_id, 4)
*/

select * from bik_node (nolock) where tree_id = 14


-- rollback tran
