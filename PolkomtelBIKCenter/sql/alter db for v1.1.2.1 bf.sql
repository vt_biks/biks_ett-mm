﻿exec sp_check_version '1.1.2.1';
go

insert into bik_app_prop(name,val,is_editable) values('openReportLinkTemplate','http://Nazwa_servera:8080/OpenDocument/opendoc/openDocument.jsp?iDocID={si_id}',1)
insert into bik_app_prop(name,val,is_editable) values('openReportForNodeKinds','Webi,FullClient,Flash,Excel,Hyperlink,Powerpoint,Pdf,CrystalReport',1)

exec sp_update_version '1.1.2.1', '1.1.2.2';
go

