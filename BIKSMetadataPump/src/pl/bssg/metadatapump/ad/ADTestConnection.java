/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.ad;

import com4j.COM4J;
import com4j.Variant;
import com4j.typelibs.activeDirectory.IADs;
import com4j.typelibs.ado20.ClassFactory;
import com4j.typelibs.ado20._Command;
import com4j.typelibs.ado20._Connection;
import com4j.typelibs.ado20._Recordset;
import java.util.List;
import pl.bssg.metadatapump.MetadataTestConnectionBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.StackTraceUtil;

/**
 *
 * @author tflorczak
 */
public class ADTestConnection extends MetadataTestConnectionBase {

    public ADTestConnection(MssqlConnectionConfig mcfg) {
        super(mcfg);
    }

    @Override
    public Pair<Integer, String> testConnection() {
        ADBean config = PumpUtils.readAdminBean("ad", ADBean.class, adhocDao);
        List<String> domains = BaseUtils.splitBySep(config.domains, ";", true);
        if (BaseUtils.isCollectionEmpty(domains)) {
            IADs rootDSE = COM4J.getObject(IADs.class, "LDAP://rootDSE", null);
            String defaultNamingContext = (String) rootDSE.get("defaultNamingContext");
            domains.add(defaultNamingContext);
        }
        boolean isAllOK = true;
        StringBuilder domainTestBuilder = new StringBuilder();
        _Connection con = ClassFactory.createConnection();
        con.provider("ADsDSOObject");
        con.open("Active Directory Provider", ""/*default*/, ""/*default*/, -1/*default*/);
        _Command cmd = ClassFactory.createCommand();
        cmd.activeConnection(con);
        for (String domain : domains) {
            try {
                cmd.commandText("<LDAP://" + domain + ">;(&(objectCategory=person)(objectclass=user)(!(objectclass=inetOrgPerson)));distinguishedName;subTree");
                _Recordset rs = cmd.execute(null, Variant.getMissing(), -1/*default*/);
                if (rs == null || rs.eof()) { // No results
                    throw new LameRuntimeException("No Users in AD?!");
                } else {
                    rs.close();
                }
                domainTestBuilder.append(domain).append(" = OK\n");
            } catch (Exception e) {
                isAllOK = false;
                domainTestBuilder.append(domain).append(" = Error! Message: ").append(StackTraceUtil.getCustomStackTrace(e)).append("\n");
            }
        }
        con.close();
        return new Pair<Integer, String>(isAllOK ? PumpConstants.TEST_CONNECTION_SUCCESS : PumpConstants.TEST_CONNECTION_FAIL, domainTestBuilder.toString());
    }
}
