/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.ftshelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import morfologik.stemming.PolishStemmer;
import morfologik.stemming.WordData;
import org.getopt.stempel.Stemmer;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class OtherTermFormsOraclePl extends OtherTermFormsOracleBase {

    private final Stemmer stempelStemmer = new Stemmer();
    private final PolishStemmer dictStemmer = new PolishStemmer();
    public static final int OTFO_PL_VERSION = 1;

    private OtherTermFormsOraclePl() {
        init();
    }

    private void init() {
        addConnectedTerms("debil", "lepper", "palant", "kretyn", "imbecyl", "głupek", "głąb");
//        addConnectedTerms("kot", "koty", "kota", "kotów", "koci", "kocia");
//        addConnectedTerms("morda", "mordę", "mordy", "mórd");
//        addConnectedTerms("java", "javie", "javy", "dżawa");
//        addConnectedTerms("chomik", "chomika", "chomiki", "chomikach", "chomików");
    }

    private synchronized List<WordData> getDictStemmedWordsRaw(String srcWord) {
        return dictStemmer.lookup(srcWord);
    }

    @Override
    protected synchronized List<String> getDictStemmedWords(String srcWord) {
        try {
            List<WordData> words = getDictStemmedWordsRaw(srcWord); //stemmer.lookup(srcWord);
            List<String> res = new ArrayList<String>(words.size());

            for (WordData word : words) {
                res.add(word.getStem().toString());
            }

            return res;
        } catch (Exception ex) {
            System.out.println("getStemmedWords: srcWord=" + srcWord);
            throw new LameRuntimeException("getStemmedWords: srcWord=" + srcWord, ex);
        }
    }

    @Override
    protected List<String> getAlgoStemmedWords(String srcWord) {
        String normTerm = stempelStemmer.stem(srcWord, true);
        if (normTerm.equals(srcWord)) {
            return null;
        }
        return Collections.singletonList(normTerm);
    }

    private static class SingletonHolder {

        private static final OtherTermFormsOraclePl INSTANCE = new OtherTermFormsOraclePl();
    }

    public static OtherTermFormsOraclePl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    protected int getVersionNumber() {
        return OTFO_PL_VERSION;
    }
}
