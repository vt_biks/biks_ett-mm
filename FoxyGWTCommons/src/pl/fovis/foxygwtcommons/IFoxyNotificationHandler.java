/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

/**
 *
 * @author pmielanczuk
 */
public interface IFoxyNotificationHandler {

    public void showInfo(String msg);

    public void showWarning(String err);

    public boolean isInfoModal();

    public boolean isWarningModal();
}
