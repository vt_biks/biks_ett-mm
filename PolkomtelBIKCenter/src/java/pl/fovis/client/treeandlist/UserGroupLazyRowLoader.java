/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.IPageDataFetchBroker;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeGrid;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;

/**
 *
 * @author tflorczak
 */
public class UserGroupLazyRowLoader extends AbstractLazyRowLoader<SystemUserGroupBean, String> {

    public UserGroupLazyRowLoader(IFoxyTreeGrid<String> treeGrid, ITreeRowAndBeanStorage<String, Map<String, Object>, SystemUserGroupBean> storage,
            BikNodeTreeOptMode optExtraMode, IPageDataFetchBroker dataFetchBroker, boolean calcHasNoChildren) {
        super(treeGrid, storage, optExtraMode, null, dataFetchBroker, calcHasNoChildren);
    }

    @Override
    public Request getOneSubtreeLevelOfTree(SystemUserGroupBean bean, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, boolean calcHasNoChildren, AsyncCallback<List<SystemUserGroupBean>> asyncCallback) {
        return BIKClientSingletons.getService().getGroupChildrenObjects(bean.objId, bean.id, asyncCallback);
    }

    @Override
    public Request getBikNodeAncestorIds(String nodeId, AsyncCallback<List<String>> asyncCallback) {
        // niewykorzystane
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Request getSubtreeLevelsByParentIds(Collection<String> parentNodeIds, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, AsyncCallback<List<SystemUserGroupBean>> asyncCallback) {
        // niewykorzystane
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public IBean4TreeExtractor<SystemUserGroupBean, String> createBeanExtractor() {
        return new GroupBeanExtractor();
    }
}
