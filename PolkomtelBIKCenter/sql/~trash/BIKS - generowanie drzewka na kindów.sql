--/*

select * from bik_app_prop


select sql_txt--, min(min_branch_ids) as min_min
from
(
select 
  tree_code, parent_code, code,
  'exec sp_add_menu_node ''' + 
  case
    when tree_kind = 'Taxonomy' then '@Taxonomy' 
    when parent_code is null then '$' + tree_code else '&' + parent_code end
   + ''', ''@'', ''&' + code + '''' as sql_txt, min_branch_ids
from
(select 
  t.code as tree_code, pnk.code as parent_code, nk.code,
  t.tree_kind,
   min(n.branch_ids) as min_branch_ids
from bik_node n inner join bik_node_kind nk on n.node_kind_id = nk.id 
left join bik_node pn on n.parent_node_id = pn.id
left join bik_node_kind pnk on pn.node_kind_id = pnk.id
inner join bik_tree t on n.tree_id = t.id
where n.is_deleted = 0 and (pn.id is null or pn.node_kind_id <> n.node_kind_id)
  and (n.linked_node_id is null or n.disable_linked_subtree = 1)
  and t.code <> 'TreeOfTrees'
group by 
  t.code, pnk.code, nk.code, t.tree_kind
) x
) y
group by sql_txt
order by min(min_branch_ids)


select * from bik_tree

select * from bik_node where tree_id = 17
order by branch_ids

select * from bik_node_kind


--*/



delete from bik_node where tree_id = dbo.fn_tree_id_by_code('TreeOfTrees')
go

exec sp_add_menu_node null, 'My BIKS', '#MyBIKS'
exec sp_add_menu_node null, 'Metadane', 'metadata'
exec sp_add_menu_node 'metadata', 'SAP BO', 'sapbo'
exec sp_add_menu_node 'sapbo', '@', '$Reports'
exec sp_add_menu_node 'sapbo', '@', '$ObjectUniverses'
exec sp_add_menu_node 'sapbo', '@', '$Connections'
exec sp_add_menu_node 'metadata', '@', '$Teradata'
exec sp_add_menu_node null, '@', '$Glossary'
exec sp_add_menu_node null, 'Kategoryzacja', '@Taxonomy'
exec sp_add_menu_node null, 'Dokumenty', '$Documents'
exec sp_add_menu_node null, 'Blogi', '$Blogs'
exec sp_add_menu_node null, 'Użytkownicy', '$Users'
exec sp_add_menu_node null, 'FAQ', '$FAQ'
exec sp_add_menu_node null, 'Admin', '#Admin'
exec sp_add_menu_node null, 'Szukaj', '#Search'

exec sp_add_menu_node '$Teradata', '@', '&TeradataSchema'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataView'
exec sp_add_menu_node '&TeradataView', '@', '&TeradataColumn'
exec sp_add_menu_node '@Taxonomy', '@', '&TaxonomyEntity'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataTable'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumn'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataProcedure'
exec sp_add_menu_node '$Reports', '@', '&ReportFolder'
exec sp_add_menu_node '&ReportFolder', '@', '&Webi'
exec sp_add_menu_node '&ReportFolder', '@', '&FullClient'
exec sp_add_menu_node '&Webi', '@', '&ReportQuery'
exec sp_add_menu_node '&ReportQuery', '@', '&Dimension$Reports'
exec sp_add_menu_node '&ReportQuery', '@', '&Measure'
exec sp_add_menu_node '&ReportQuery', '@', '&Detail'
exec sp_add_menu_node '&ReportFolder', '@', '&Flash'
exec sp_add_menu_node '&ReportFolder', '@', '&Excel'
exec sp_add_menu_node '&ReportFolder', '@', '&Hyperlink'
exec sp_add_menu_node '&ReportFolder', '@', '&Powerpoint'
exec sp_add_menu_node '&ReportFolder', '@', '&Pdf'
exec sp_add_menu_node '&ReportFolder', '@', '&CrystalReport'
exec sp_add_menu_node '$ObjectUniverses', '@', '&UniversesFolder'
exec sp_add_menu_node '&UniversesFolder', '@', '&Universe'

exec sp_add_menu_node '&Universe', '@', '&UniverseDerivedTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseAliasTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseTable'

exec sp_add_menu_node '&Universe', '@', '&UniverseClass'
exec sp_add_menu_node '&UniverseClass', '@', '&Dimension'
exec sp_add_menu_node '&UniverseClass', '@', '&Measure'
exec sp_add_menu_node '&Dimension', '@', '&Detail'
exec sp_add_menu_node '&UniverseClass', '@', '&Filter'

exec sp_add_menu_node '$Connections', '@', '&ConnectionEngineFolder'
exec sp_add_menu_node '&ConnectionEngineFolder', '@', '&ConnectionUserFolder'

exec sp_add_menu_node '&ConnectionUserFolder', '@', '&DataConnection'

exec sp_add_menu_node '$Documents', '@', '&DocumentsFolder'
exec sp_add_menu_node '&DocumentsFolder', '@', '&Document'

exec sp_add_menu_node '$Users', '@', '&UsersGroup'
exec sp_add_menu_node '&UsersGroup', '@', '&User'
exec sp_add_menu_node '$Blogs', '@', '&Blog'

exec sp_add_menu_node '$FAQ', '@', '&CategoriesOfQuestions'
exec sp_add_menu_node '&CategoriesOfQuestions', '@', '&Question'

exec sp_add_menu_node '$Glossary', '@', '&Keyword'

exec sp_add_menu_node '$Glossary', '@', '&GlossaryCategory'

exec sp_add_menu_node '&GlossaryCategory', '@', '&Glossary'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryNotCorpo'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryVersion'

exec sp_add_menu_node '&Blog', '@', '&BlogEntry'
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go
