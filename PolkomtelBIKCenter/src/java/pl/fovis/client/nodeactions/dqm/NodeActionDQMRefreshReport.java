/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.dqm;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.IContinuation;

/**
 *
 * @author tflorczak
 */
public class NodeActionDQMRefreshReport extends NodeActionDQMBase {

    @Override
    public boolean isEnabledByDesign() {
        return node != null && isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_DQM_REPORT);
    }

    @Override
    public String getCaption() {
        return I18n.odswiezRaport.get();
    }

    @Override
    public void execute() {
        new ConfirmDialog().buildAndShowDialog(I18n.czyChceszOdswiezycRaportZaOkres.get(), I18n.odswiez.get(), new IContinuation() {

            @Override
            public void doIt() {
                BIKClientSingletons.getDQMService().refreshDQMReport(data.node.id, new StandardAsyncCallback<Void>("error in refreshDQMReport") {

                    @Override
                    public void onSuccess(Void result) {
                        BIKClientSingletons.showInfo(I18n.odswiezonoRaport.get());
                        refreshRedisplayDataAndSelectRecord(data.node.id);
                    }
                });
            }
        });
    }
}
