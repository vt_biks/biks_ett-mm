/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.tags.htmltags;

import java.util.Collection;
import java.util.Map;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public class RenderableTagUtils {

    public static void renderTag(StringBuilder sb, IRenderableTag tag) {
        String tagName = tag.getTagName();

        if (tag.isTextTag()) {
            sb.append(tagName);
        } else {
            sb.append("<").append(tagName);
            Map<String, String> attrs = tag.getTagAttrs();

            BaseUtils.renderHtmlAttrs(attrs, sb);
            if (tag.isClosedInPlace()) {
                sb.append(" />");
            } else {
                sb.append(">");
                Collection<IRenderableTag> subTags = tag.getRenderableSubTags();
                if (subTags != null) {
                    for (IRenderableTag subTag : subTags) {
                        renderTag(sb, subTag);
                    }
                }
                sb.append("</").append(tagName).append(">");
            }
        }
    }
}
