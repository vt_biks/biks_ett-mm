/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.caches;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 *
 * @author wezyr
 */
public abstract class FoxyObjectSupplierBase<K, V> implements IFoxyObjectSupplier<K, V> {

    public abstract V getByKey(K key);

    public Set<String> getAlternativeKeyNames() {
        return null;
    }

    public boolean isNullUniqueForKey(String keyName) {
        return false;
    }

    public Object getObjectKeyValue(V obj, String keyName) {
        return null;
    }

    public Collection<V> getManyByKeys(Collection<K> keys) {
        List<V> res = new ArrayList<V>();
        for (K key : keys) {
            res.add(getByKey(key));
        }
        return res;
    }

    public K getKeyValue(V obj) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public V getByAltKeyVal(String keyName, Object val) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Collection<V> getManyByAltKeyVals(String keyName, Collection<Object> vals) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
