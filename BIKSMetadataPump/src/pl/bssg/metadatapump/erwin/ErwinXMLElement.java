/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.erwin;

import java.util.Map;

/**
 *
 * @author pgajda
 */
public abstract class ErwinXMLElement<V> implements IErwinXMLElement<V>  {

    protected Map<String, V> propertiesMap;
  
    public String getElementName() {
        return "EntityProps";
    }

    public boolean checkIfReadPropertie(String propertyName) {
        return propertiesMap.containsKey(propertyName);
    }    
    
    public V getPropertieValue(String key) {
        return  propertiesMap.get(key);
    }

    public void setPropertieValues(String key, V values){
        propertiesMap.put(key, values);
    }
}
