/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public interface INamedTemplateRenderer {
    public String render(String templateName, Map<String, Object> vars, Object model);
}
