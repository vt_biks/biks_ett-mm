/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.grid;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.foxygwtcommons.i18n.I18n;

/**
 *
 * @author pmielanczuk
 */
public class BeanGridPager<T> extends PagerBase implements IsWidget, IBeanGridEventsListener<T>, IGridDisplayWithPager {

    protected BeanGrid<T> beanGrid;
    protected Boolean isUnknownPageCountModeEnabled = false;

    public BeanGridPager(BeanGrid<T> beanGrid) {
        super();
        this.beanGrid = beanGrid;
        beanGrid.addEventsListener(this);
    }

    //ww: pageNum < 0 -> go to last page
    //tf: last? maybe first? :)
    @Override
    public void navigate(int pageNum) {
        if (pageNum < 0) {
            pageNum = 0;
        } else if (!isUnknownPageCountModeEnabled) {
            int maxPageNum = getMaxPageNum();
            if (pageNum > maxPageNum) {
                pageNum = maxPageNum;
            }
        }
        doSomethingOptBeforeNavigate();
        beanGrid.setPageNum(pageNum);
    }

    protected void doSomethingOptBeforeNavigate() {
        // NO OP
    }

    public int getPageNum() {
        return beanGrid.getCurrPage();
    }

    @Override
    public Widget asWidget() {
        return hp;
    }

    public void setUknownPageCountMode(Boolean unknownPageCount) {
        this.isUnknownPageCountModeEnabled = unknownPageCount;
        lastPageBtn.setVisible(!unknownPageCount);
        beanGrid.setIsUnknownPageCountModeEnabled(unknownPageCount);
    }

    @Override
    public void pageChanged() {
        int currPage = beanGrid.getCurrPage();
        boolean prevEnabled = currPage > 0;
        firstPageBtn.setEnabled(prevEnabled);
        prevPageBtn.setEnabled(prevEnabled);
        boolean nextEnabled;
        if (!isUnknownPageCountModeEnabled) {
            nextEnabled = currPage < getMaxPageNum();
            lastPageBtn.setEnabled(nextEnabled);
            pageInfoLbl.setText(
                    //                I18nParametrizedMessages.substitute(MainMessages.stronaXzY, currPage + 1, getMaxPageNum() + 1)
                    I18n.strona0Z1.format(currPage + 1, getMaxPageNum() + 1) //                ParametrizedMessages.stronaXzY(currPage + 1, getMaxPageNum() + 1)
            //MainMessages.strona.get() /* I18N:  */+ " " + (currPage + 1) + " " +MainMessages.z_noSingleLetter.get() /* I18N:  */+ " " + (getMaxPageNum() + 1)
            );
        } else {
            nextEnabled = beanGrid.getNumberOfVisibleRows() == beanGrid.getPageSize();
            nextPageBtn.setEnabled(nextEnabled);
            pageInfoLbl.setText("Strona " + (currPage + 1));
        }
        nextPageBtn.setEnabled(nextEnabled);
    }

    @Override
    public void oneBeanChanged(T bean) {
        // no-op
    }

    public int getMaxPageNum() {
        return (beanGrid.getBeanCount() - 1) / beanGrid.getPageSize();
    }

    @Override
    public void beansChanged() {
        pageChanged();
    }

    public void setVisible(boolean visible) {
        hp.setVisible(visible);
    }
}
