/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.templates;

import pl.trzy0.foxy.commons.ITextProvider;

/**
 *
 * @author wezyr
 */
public class PlainNamedTemplateProvider implements INamedTemplateProvider {

    private ITemplateParser parser;
    private ITextProvider textProvider;

    public PlainNamedTemplateProvider(ITextProvider textProvider, ITemplateParser parser) {
        this.parser = parser;
        this.textProvider = textProvider;
    }

    protected String getTemplateFileName(String name) {
        return "template:" + name;
    }

    public IParsedTemplate getTemplate(String name) {
        return parser.parse(getTemplateFileName(name), textProvider.getText(name));
    }
}
