/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.MuppetMerger;
import commonlib.i18nsupport.JavaCurrentLocaleNameProvider;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.bssg.biks.adm.server.AppAndDBVerInfoServiceImpl;
import pl.bssg.gwtservlet.FoxyGWTServiceServlet;
import pl.bssg.gwtservlet.IFoxyServiceConfigurator;
import pl.fovis.client.VersionInfoService;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.dqm.DQMConstanst;
import pl.fovis.server.dqm.DQMExportManager;
import pl.fovis.server.dqm.DQMExportReportManager;
import pl.fovis.server.fts.FtsServiceImpl;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.serverlogic.IDaoAwareNamedSqlsModel;
import pl.trzy0.foxy.serverlogic.IFoxyServiceOnServer;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author pmielanczuk
 */
public class BOXIServiceServlet extends FoxyGWTServiceServlet<BIKCenterConfigObj, BOXISessionData, BiksSharedSessionObj> {

    public static final String FOXYMILL_ROOT_PATH = "/WEB-INF/FoxyMill/";
    public static final String FOXYMILL_ADHOC_SQLS_PATH = FOXYMILL_ROOT_PATH + "adhocSqls.html" /* I18N: no */;
    protected VersionInfoService versionInfoService = new VersionInfoServiceImpl();
    protected BOXIServiceImpl boxiService = new BOXIServiceImpl(versionInfoService);
    protected MultiKSServiceImpl multiXService = new MultiKSServiceImpl(/*boxiService*/);
    protected LisaServiceImpl lisaService = new LisaServiceImpl(boxiService);
    protected DQMServiceImpl dqmService = new DQMServiceImpl(boxiService);
    protected FtsServiceImpl ftsService = new FtsServiceImpl(boxiService);
    protected DQMExportManager dqmExportManager;
    protected DQMExportReportManager dqmExportReportManager;
    protected TreeExportManager treeExportManager;
    protected SearchResultsExportManager searchResultsExportManager;
    protected RssExportManager rssExportManager;
    protected BIAInstanceExportManager biaInstanceManager;
    protected BIAAuditExportManager biaAdutiManager;
    protected BIAObjectManagerExportManager biaObjectManager;
    //    {
//        multiXService.setStartupSubservice(boxiService);
//        multiXService.setUserCreationSubserrvice(boxiService);
//    }

    @Override
    public synchronized void init(ServletConfig config) throws ServletException {
        super.init(config);
        initAdditionalManagers();
//        System.out.println("INIT! Instance: " + System.identityHashCode(this) + ", class: " + this.getClass().getCanonicalName());
        boxiService.setDqmService(dqmService);

        ServletContext context = getServletContext();
        String imagesPath = context.getRealPath("images");
        String additonalIconsDir = getAdditionalIconDir();

        if (!BaseUtils.isStrEmptyOrWhiteSpace(additonalIconsDir)) {
            System.out.println("Kopiowanie dodatkowych ikonek....");
            try {
                File src = new File(additonalIconsDir);
                File dst = new File(imagesPath);
                //TODO: aktualizowac definicje atrybutu metaBIKS.Ikona
                copyFolder(src, dst);
                System.out.println("Nowe ikony zostały skopiowane");
                boxiService.updateMetaIconAttribute();
            } catch (Throwable ex) {
                ex.printStackTrace();
                System.out.println("Nie można skopiować ikonek z foldera " + additonalIconsDir);
            }
        }
    }

    protected void copyFolder(File src, File dst) throws FileNotFoundException, IOException {
        if (src.isDirectory()) {
            if (!dst.exists()) {
                dst.mkdir();
            }
            String[] files = src.list();
            for (String file : files) {
                File srcFile = new File(src, file);
                File dstFile = new File(dst, file);
                copyFolder(srcFile, dstFile);
            }
        } else {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);
            byte[] buffer = new byte[1024];

            int length = 0;
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }
            in.close();
            out.close();
            System.out.println("File copied from " + src + " to " + dst);
        }
    }

    protected String getAdditionalIconDir() {
        if (MuppetMerger.hasPropAssignableTo(cfg.getClass(), "dirForIcons", String.class)) {
            return (String) MuppetMerger.getProperty(cfg, "dirForIcons");
        }
        return null;
    }

    @Override
    protected void registerServiceImpl(Object serviceImpl) {
        super.registerServiceImpl(serviceImpl);
        if (serviceImpl instanceof BIKSServiceBase) {
            BIKSServiceBase bsb = (BIKSServiceBase) serviceImpl;
            bsb.setCommonSubservices(boxiService, multiXService);
        }
    }

//    @Override
//    public String processCall(String payload) throws SerializationException {
//
//        String signature = SerializabilityUtil.getSerializationSignature(BikNodeTreeOptMode.class,
//                getSerializationPolicy("http://localhost:8080/PolkomtelBIKCenter/pl.fovis.bikcenter/", "6426FD05C09CDF902195FFC5C29F660C"));
//        System.out.println("signature: " + signature + " for 6426FD05C09CDF902195FFC5C29F660C");
//
//        return super.processCall(payload);
//    }
    @Override
    protected void registerAdditionalServiceImpls() {
        super.registerAdditionalServiceImpls();
        registerServiceImpl(new ServerSideI18nSupportingServiceImpl());
        registerServiceImpl(versionInfoService);
        registerServiceImpl(lisaService);
        registerServiceImpl(dqmService);
//        registerServiceImpl(ftsService);
        registerServiceImpl(multiXService);
        registerServiceImpl(new AppAndDBVerInfoServiceImpl());
    }

//    @Override
//    protected String makeFullPathToAdhocSqlsOfService(String adhocSqlsWebPath, String serviceSimpleName) {
//        if (("BOXI" /* i18n: no */).equals(serviceSimpleName)) {
//            return FOXYMILL_ADHOC_SQLS_PATH;
//        }
//        return super.makeFullPathToAdhocSqlsOfService(adhocSqlsWebPath, serviceSimpleName);
//    }
    @Override
    protected IFoxyServiceConfigurator<BIKCenterConfigObj, BOXISessionData, BiksSharedSessionObj> getServiceConfigurator(ServletContext servletContext) {
        return new IFoxyServiceConfigurator<BIKCenterConfigObj, BOXISessionData, BiksSharedSessionObj>() {
            public String getAdhocSqlsWebPath() {
                return FOXYMILL_ROOT_PATH;
            }

            public Class<BIKCenterConfigObj> getConfigBeanClass() {
                return BIKCenterConfigObj.class;
            }

            public Class<BOXISessionData> getSessionDataClass() {
                return BOXISessionData.class;
            }

            public IFoxyServiceOnServer<BIKCenterConfigObj, BOXISessionData, BiksSharedSessionObj> getServiceImpl() {
                return boxiService;
            }

            public String getAppName() {
                return "bikcenter" /* I18N: no */;
            }

            public IBeanConnectionEx<Object> createDBConnection(BIKCenterConfigObj cfg) {
                return new MssqlConnection(cfg.connConfig);
            }
        };
    }

    @Override
    @SuppressWarnings("unchecked")
    protected IDaoAwareNamedSqlsModel<Object> getOptNamedSqlsModel() {
        return new BIKSDaoAwareModelForFoxyMill(boxiService);
    }

    @Override
    protected boolean processFileHandlingRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        boolean isDQMExport = !BaseUtils.isStrEmptyOrWhiteSpace(req.getParameter(DQMConstanst.DQM_EXPORT_PARAM_NAME));
        boolean isDQMReportExport = !BaseUtils.isStrEmptyOrWhiteSpace(req.getParameter(DQMConstanst.DQM_EXPORT_REPORT_PARAM_NAME));
        boolean isTreeExport = !BaseUtils.isStrEmptyOrWhiteSpace(req.getParameter(BIKConstants.TREE_EXPORT_PARAM_NAME));
        boolean isSearchResultsExport = !BaseUtils.isStrEmptyOrWhiteSpace(req.getParameter(BIKConstants.SEARCH_RESULTS_EXPORT_PARAM_NAME));
        boolean isRss = req.getParameter(BIKConstants.RSS) != null;
        boolean isBIAdmin = !BaseUtils.isStrEmptyOrWhiteSpace(req.getParameter(BIKConstants.BIAINSTANCE_RESULTS_EXPORT_PARAM_NAME));
        boolean isBIAAudit = !BaseUtils.isStrEmptyOrWhiteSpace(req.getParameter(BIKConstants.BIA_AUDIT_RESULTS_EXPORT_PARAM_NAME));
        boolean isBIAObjectManager = !BaseUtils.isStrEmptyOrWhiteSpace(req.getParameter(BIKConstants.BIA_OBJECT_RESULTS_EXPORT_PARAM_NAME));

        String serverFileName = req.getParameter("get");

//        super.allowedExst = BIKClientSingletons.allowedFileExtensionsList() != null ? Arrays.asList(BIKClientSingletons.allowedFileExtensionsList()) : null;

        if (isDQMExport) {
            processExportingRequest(req, resp, dqmExportManager);
            return true;
        }
        if (isDQMReportExport) {
            processExportingRequest(req, resp, dqmExportReportManager);
            return true;
        }
        if (isTreeExport) {
            processExportingRequest(req, resp, treeExportManager);
            return true;
        }
        if (isSearchResultsExport) {
            JavaCurrentLocaleNameProvider.setCurrentLocaleName(getCurrentLocaleNameFromRequest(boxiService, req));
            try {
                processExportingRequest(req, resp, searchResultsExportManager);
            } finally {
                JavaCurrentLocaleNameProvider.setCurrentLocaleName(null);
            }
            return true;
        }
        if (isRss) {
            processExportingRequest(req, resp, rssExportManager);
            return true;
        }
        if (isBIAdmin) {
            processExportingRequest(req, resp, biaInstanceManager);
        }
        if (isBIAAudit) {
            processExportingRequest(req, resp, biaAdutiManager);
        }

        if (isBIAObjectManager) {
            processExportingRequest(req, resp, biaObjectManager);
        }
        boolean ret = super.processFileHandlingRequest(req, resp);
        if (!BaseUtils.isStrEmptyOrWhiteSpace(serverFileName) && serverFileName.startsWith(BIKConstants.NAS_FILE_PREFIX)) {
            //sciaga pliku slownika modulu NAS
            File file = new File(dirForUpload, serverFileName);
            if (file.exists()) {
                file.delete();
            }
        }
        return ret;
    }

    protected void processExportingRequest(final HttpServletRequest req, final HttpServletResponse resp, final IExportManager manager) {
        runCustomSessionAwareCode(req, resp, new IContinuation() {
            @Override
            public void doIt() {
                manager.handleExportRequest(req, resp);
            }
        });
    }

    protected void initAdditionalManagers() {
        dqmExportManager = new DQMExportManager(dqmService);
        dqmExportReportManager = new DQMExportReportManager(dqmService);
        treeExportManager = new TreeExportManager(boxiService);
        searchResultsExportManager = new SearchResultsExportManager(boxiService);
        rssExportManager = new RssExportManager(boxiService);
        biaInstanceManager = new BIAInstanceExportManager(boxiService);
        biaAdutiManager = new BIAAuditExportManager(boxiService);
        biaObjectManager = new BIAObjectManagerExportManager(boxiService);
    }

    @Override
    protected BiksSharedSessionObj createSharedSessionObj() {
        return new BiksSharedSessionObj();
    }

    protected boolean isAppConfigurated() {
        return false;
    }
}
