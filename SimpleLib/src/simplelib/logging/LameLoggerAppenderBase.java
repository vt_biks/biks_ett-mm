/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.logging;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import simplelib.logging.ILameLogger.LameLogLevel;

/**
 *
 * @author wezyr
 */
public abstract class LameLoggerAppenderBase implements ILameLoggerAppender {

    private LameLogLevel minLogLevel;

    public static LameLogLevel calcProperMinLogLevel(LameLogLevel minLogLevel) {
        return LameLogLevel.getHigherLevel(minLogLevel, LameLoggerFactory.globalMinLogLevel);
    }

    public LameLoggerAppenderBase(LameLogLevel minLogLevel) {
        this.minLogLevel = calcProperMinLogLevel(minLogLevel);
    }

    public LameLogLevel getMinLogLevel() {
        return minLogLevel;
    }

    protected boolean isMsgLevelInRange(LameLoggerMsg msg) {
        return !msg.level.isLowerLevelThan(getMinLogLevel());
    }

    protected Collection<LameLoggerMsg> filterMsgs(Collection<LameLoggerMsg> msgs,
            LameLogLevel minLogLevelInMsgs, LameLogLevel maxLogLevelInMsgs) {
        if (maxLogLevelInMsgs.isLowerLevelThan(getMinLogLevel())) {
            return null;
        }

        if (!minLogLevelInMsgs.isLowerLevelThan(getMinLogLevel())) {
            return msgs;
        }

        List<LameLoggerMsg> res = new ArrayList<LameLoggerMsg>();
        for (LameLoggerMsg msg : msgs) {
            if (isMsgLevelInRange(msg)) {
                res.add(msg);
            }
        }
        return res;
    }
}
