/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import java.util.List;
import pl.bssg.metadatapump.common.ConnectionParametersJdbcBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.PushButtonWithHideText;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuationWithReturn;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class AdminConfigGeneralJdbcWidget extends AdminConfigManyServersBase<List<ConnectionParametersJdbcBean>, ConnectionParametersJdbcBean> {

    protected TextBox tbxName;
    protected TextBox tbxJdbcUrl;
    protected TextArea tbxQuery;
    protected TextArea tbxJoinedQuery;
    protected ListBox lbSelectedDriver;
    protected Label lblJdbcTemplate;
    protected CheckBox chbIsActive;
    protected TextBox tbxUser;
    protected TextBox tbxPw;
    protected RadioButton load2TreeBtn;
    protected RadioButton load2FilesBtn;
    protected TextBox tbxFolderPath;
    protected TextArea tbxAdhocSql;
//    protected CheckBox chbPw;
    protected ConnectionParametersJdbcBean activatedJdbcBean;

    @Override
    protected void activateForServer(String id) {
        activatedJdbcBean = serversMap.get(id);
        tbxName.setText(activatedJdbcBean.name);
        tbxJdbcUrl.setText(activatedJdbcBean.jdbcUrl);
        tbxQuery.setText(activatedJdbcBean.query);
        tbxJoinedQuery.setText(activatedJdbcBean.queryJoined);
        chbIsActive.setValue(activatedJdbcBean.isActive);
        tbxUser.setText(activatedJdbcBean.user);
        tbxPw.setText(activatedJdbcBean.password);
        tbxFolderPath.setText(activatedJdbcBean.folderPath);
        tbxAdhocSql.setText(activatedJdbcBean.adhocSql);
        if (BaseUtils.isStrEmptyOrWhiteSpace(activatedJdbcBean.folderPath)) {
            load2FilesBtn.setValue(Boolean.FALSE);
            load2TreeBtn.setValue(Boolean.TRUE);
        } else {
            load2TreeBtn.setValue(Boolean.FALSE);
            load2FilesBtn.setValue(Boolean.TRUE);
        }
        lblJdbcTemplate.setText(I18n.szablon.get() + ": " + activatedJdbcBean.jdbcSourceTemplate);
        for (int i = 0; i < lbSelectedDriver.getItemCount(); i++) {
            if (lbSelectedDriver.getItemText(i).equalsIgnoreCase(activatedJdbcBean.jdbcSourceName)) {
                lbSelectedDriver.setSelectedIndex(i);
                break;
            }
        }
        setWidgetsEnabled(chbIsActive.getValue());
        onLoadOptionChanged();
    }

    protected void onLoadOptionChanged() {
        tbxQuery.setEnabled(load2TreeBtn.getValue());
        tbxJoinedQuery.setEnabled(load2TreeBtn.getValue());
        tbxFolderPath.setEnabled(load2FilesBtn.getValue());
        tbxAdhocSql.setEnabled(load2FilesBtn.getValue());
    }

    protected void createCommonConfig(CellPanel panelRight) {
        createLabel(I18n.nazwa.get() + ":", panelRight);
        tbxName = createTextBox(panelRight, false);
        tbxName.setEnabled(false);
        createLabel(I18n.uzytkownik.get() + ":", panelRight);
        tbxUser = createTextBox(panelRight);
        createLabel(I18n.haslo.get() + ":", panelRight);
        tbxPw = createPasswordTextBox(panelRight);
//        chbPw = createAndAddCheckBox(I18n.ukryjHaslo.get(), new ValueChangeHandler<Boolean>() {
//            @Override
//            public void onValueChange(ValueChangeEvent<Boolean> event) {
//                tbxPw.getElement().setAttribute("type" /* I18N: no */, chbPw.getValue() ? "password" /* I18N: no */ : "text" /* I18N: no */);
//            }
//        }, panelRight);
        HorizontalPanel hp = new HorizontalPanel();
        Label driverClass = createLabel(I18n.klasaSterownika.get() + ": ", hp);
        driverClass.addStyleName("defBottomLine");
        lbSelectedDriver = createListBoxEx(BIKClientSingletons.getJdbcSourceTypes(), hp);
        lbSelectedDriver.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                lblJdbcTemplate.setText(I18n.szablon.get() + ": " + BIKClientSingletons.getJdbcSourceTypeBean(lbSelectedDriver.getItemText(lbSelectedDriver.getSelectedIndex())).jdbcSourceTemplate);
            }
        });
        panelRight.add(hp);
        panelRight.add(lblJdbcTemplate = new Label());
        Label jdbcLbl = new Label(I18n.jdbcUrl.get() + ":");
        jdbcLbl.addStyleName("topMarginForAdminLabel");
        panelRight.add(jdbcLbl);
        tbxJdbcUrl = createTextBox(panelRight, true);

        createLine(panelRight);
    }

    protected void createLoadOptions(CellPanel panelRight) {
        tbxQuery = createTextArea("100px");
        tbxJoinedQuery = createTextArea("50px");
        tbxAdhocSql = createTextArea("150px");
        load2TreeBtn = new RadioButton("load-option", "<b>" + I18n.zasilenieDoDrzewa.get() + "</b>", true);
        load2TreeBtn.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                onLoadOptionChanged();
            }
        });
        load2FilesBtn = new RadioButton("load-option", "<b>" + I18n.zasilenieDoPlikow.get() + "</b>", true);
        load2FilesBtn.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                onLoadOptionChanged();
            }
        });

        panelRight.add(load2TreeBtn);
        HorizontalPanel queryPanel = new HorizontalPanel();
        queryPanel.add(new Label(I18n.zapytanieSQL.get() + ":"));
        queryPanel.add(createHintBtn(I18n.jdbcZapytanie.get()));
        panelRight.add(queryPanel);
        panelRight.add(tbxQuery);
        widgetList.add(tbxQuery);
        HorizontalPanel joinedQueryPanel = new HorizontalPanel();
        joinedQueryPanel.add(new Label(I18n.zapytanieSQLDlaPowiazanychObiektow.get() + ":"));
        joinedQueryPanel.add(createHintBtn(I18n.jdbcPowiazaniaZapytanie.get()));
        panelRight.add(joinedQueryPanel);
        panelRight.add(tbxJoinedQuery);
        widgetList.add(tbxJoinedQuery);
        panelRight.add(load2FilesBtn);
        panelRight.add(new Label(I18n.sciezkaDoKatalogu.get() + " :"));
        panelRight.add(tbxFolderPath = createTextBox());
        panelRight.add(new Label(I18n.zapytaniePobierajaceDane.get() + " :"));
        panelRight.add(tbxAdhocSql);
        widgetList.add(tbxAdhocSql);
    }

    @Override
    protected void buildRightWidget(CellPanel panelRight) {
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
                onLoadOptionChanged();
            }
        }, false, panelRight);
        createCommonConfig(panelRight);
        createLoadOptions(panelRight);

        createButton(I18n.zapiszZmiany.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (BaseUtils.isStrEmptyOrWhiteSpace(tbxPw.getText())) {
                    Window.alert(I18n.hasloNieMozeBycPuste.get());

                }else
                if (serverNames.contains(tbxName.getText()) && !tbxName.getText().equals(listbox.getItemText(getSelectedIndex()))) {
                    Window.alert(I18n.istniejeJuzTakaNazwaSerwera.get() /* I18N:  */);
                } else {
                    activatedJdbcBean.name = tbxName.getText();
                    ConnectionParametersJdbcBean selectedType = BIKClientSingletons.getJdbcSourceTypeBean(lbSelectedDriver.getItemText(lbSelectedDriver.getSelectedIndex()));
                    activatedJdbcBean.jdbcSourceId = selectedType.jdbcSourceId;
                    activatedJdbcBean.jdbcDriverName = selectedType.jdbcDriverName;
                    activatedJdbcBean.jdbcUrl = tbxJdbcUrl.getText();
                    activatedJdbcBean.isActive = chbIsActive.getValue();
                    activatedJdbcBean.user = tbxUser.getText();
                    activatedJdbcBean.password = tbxPw.getText();
                    if (load2TreeBtn.getValue()) {
                        activatedJdbcBean.query = tbxQuery.getText();
                        activatedJdbcBean.queryJoined = tbxJoinedQuery.getText();
                        activatedJdbcBean.folderPath = "";
                        activatedJdbcBean.adhocSql = "";
                    } else {
                        activatedJdbcBean.query = "";
                        activatedJdbcBean.queryJoined = "";
                        activatedJdbcBean.folderPath = tbxFolderPath.getText();;
                        activatedJdbcBean.adhocSql = tbxAdhocSql.getText();
                    }
                    callServiceToSaveData(activatedJdbcBean, new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " runTestConnection: " + getSourceName()) {

                                @Override
                                public void onSuccess(Void result) {
//                                    serverNames.remove(listbox.getItemText(getSelectedIndex()));
//                                    serverNames.add(activatedJdbcBean.name);
                                    resultTest.setText("");
                                    listbox.setItemText(getSelectedIndex(), activatedJdbcBean.name);
                                    BIKClientSingletons.showInfo(I18n.zmienionoParametryDoPolaczenia.get() /* I18N:  */ + ": " + getSourceName());
                                }
                            });
                }
            }
        }, panelRight);
        createTestConnectionWidget(panelRight, new IParametrizedContinuationWithReturn<Void, Integer>() {

            @Override
            public Integer doIt(Void param) {
                return activatedJdbcBean.id;
            }
        });
        panelRight.setWidth("100%");
    }

    protected TextArea createTextArea(String height) {
        TextArea area = new TextArea();
        area.setWidth("490px");
        area.setHeight(height);
        return area;
    }

    protected PushButtonWithHideText createHintBtn(final String text) {
        PushButtonWithHideText howTOBtn = new PushButtonWithHideText(new Image("images/hint.gif" /* I18N: no */));
        howTOBtn.setStyleName("BizDef-vWidget");
        howTOBtn.addStyleName("adminIconMargin");
        howTOBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                new SimpleInfoDialog().buildAndShowDialog(text, I18n.jakNapisacZapytanie.get(), null);
            }
        });
        howTOBtn.setTitle(I18n.jakNapisacZapytanie.get());
        return howTOBtn;
    }

    protected void callServiceToSaveData(ConnectionParametersJdbcBean bean, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().setJdbcConnectionParameters(bean, asyncCallback);
    }

    @Override
    protected void callServiceToAddNewServer(String sourceName, String serverName, boolean needSeparateSchedule, AsyncCallback<ConnectionParametersJdbcBean> asyncCallback) {
        BIKClientSingletons.getService().addJdbcConfig(sourceName, serverName, asyncCallback);
    }

    @Override
    protected void callServiceToDeleteServer(int id, String serverName, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().deleteJdbcConfig(getSourceName(), id, serverName, asyncCallback);
    }

    @Override
    protected void callServiceToTestConnection(int id, AsyncCallback<Pair<Integer, String>> asyncCallback) {
        BIKClientSingletons.getService().runJdbcTestConnection(id, asyncCallback);
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_JDBC;
    }

    @Override
    protected boolean needSeparateSchedule() {
        return true;
    }
}
