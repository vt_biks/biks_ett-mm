/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.ftp;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpATTRS;

/**
 *
 * @author tflorczak
 */
public class SFTP4JFoxyFTPFile implements IFoxyFTPFile {

    protected ChannelSftp.LsEntry file;
    protected SftpATTRS attrs;

    public SFTP4JFoxyFTPFile(ChannelSftp.LsEntry file) {
        this.file = file;
        this.attrs = file.getAttrs();
    }

    @Override
    public String getName() {
        return file.getFilename();
    }

    @Override
    public boolean isDirectory() {
        return attrs.isDir();
    }

    @Override
    public long getSize() {
        return attrs.getSize();
    }
}
