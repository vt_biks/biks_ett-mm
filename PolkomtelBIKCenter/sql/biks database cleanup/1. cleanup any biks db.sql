-----------------------------------------------------------------
-----------------------------------------------------------------
-- �mieci z PGE
-----------------------------------------------------------------
-----------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_przygotujDane_Raport3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_przygotujDane_Raport3]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FinalizujDaneRaportow]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FinalizujDaneRaportow]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_dane_raport1]'))
DROP VIEW [dbo].[vw_dane_raport1]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_dane_raport2]'))
DROP VIEW [dbo].[vw_dane_raport2]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_ozn_kontr_agr]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_ozn_kontr_agr]
GO

-----------------------------------------------------------------
-----------------------------------------------------------------
-- UWAGA -- UWAGA -- UWAGA -- UWAGA -- UWAGA -- UWAGA --
--
-- UWAGA: na du�ej bazie z Polkomtela skrypt mo�e dzia�a�
-- powy�ej p� godziny! Co wi�cej generuje si� plik LOG
-- rozmiaru > 18 GB !!!
--
-- UWAGA -- UWAGA -- UWAGA -- UWAGA -- UWAGA -- UWAGA --
--
-- czyszczenie tabelek (truncate) kt�re nie maj� prefiksu:
-- 1. BIK_
-- 2. AMI_
-- 3. ani MLTX_
-----------------------------------------------------------------
-----------------------------------------------------------------

set nocount on

if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#table_to_trunc'))
drop table #table_to_trunc
go

create table #table_to_trunc (name sysname not null primary key, lvl int not null default 0)

insert into #table_to_trunc (name) 
select t.name
from sys.objects t
  left join sys.extended_properties as ep on ep.class = 1 and
  ep.major_id = t.object_id and ep.minor_id = 0 and ep.name = 'microsoft_database_tools_support'
where t.is_ms_shipped = 0 and ep.major_id is null
  and t.type = 'U'
  and not (t.name like 'bik[_]%' or t.name like 'ami[_]%' or t.name like 'mltx[_]%')

if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#table_rel'))
drop table #table_rel
go

create table #table_rel (referee_name sysname not null, referenced_name sysname not null,
  unique (referee_name, referenced_name),
  unique (referenced_name, referee_name))

insert into #table_rel (referee_name, referenced_name)
select distinct ree.name, red.name
from vw_ww_foreignkeys fk inner join #table_to_trunc ree on fk.Table_Name = ree.name collate DATABASE_DEFAULT
  inner join #table_to_trunc red on fk.referencedtablename = red.name collate DATABASE_DEFAULT
where ree.name <> red.name
  
declare @pass_num int = 0

while exists(select * from #table_to_trunc)
begin

  update #table_to_trunc
  set lvl = 1
  where name in (select referenced_name from #table_rel)

  declare @sql_txt varchar(max) = ''

  declare @single_cmd varchar(max) = case when @pass_num = 0 then 'truncate table' else 'delete from' end

  select @sql_txt = @sql_txt + @single_cmd + ' ' + name + '
'
  --t.* 
  from #table_to_trunc ttt
  where lvl = 0

  print '
-----------
pass #' + cast(@pass_num as varchar(10)) + ', executing commands:
' + @sql_txt
  --select len(@sql_txt) as length, @sql_txt

  exec(@sql_txt)

  delete from #table_rel where referee_name in (select name from #table_to_trunc where lvl = 0)

  delete from #table_to_trunc
  where lvl = 0

  update #table_to_trunc
  set lvl = 0

  set @pass_num = @pass_num + 1

  -- select * from #table_to_trunc
  -- select * from #table_rel
end
go

-----------------------------------------------------
-----------------------------------------------------
-- obiekty mltx - zb�dne
-----------------------------------------------------
-----------------------------------------------------
/*
drop table mltx_login
drop table mltx_registration
drop table mltx_app_prop
drop table mltx_database
drop table mltx_database_template
go

*/