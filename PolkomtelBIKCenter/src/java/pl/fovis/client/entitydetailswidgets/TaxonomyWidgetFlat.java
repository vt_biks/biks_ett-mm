/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import java.util.List;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.JoinedObjBean;
import pl.fovis.common.JoinedObjsTabKind;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author tflorczak
 */
public class TaxonomyWidgetFlat extends JoinedObjWidgetFlatBase<JoinedObjBean> {

    @Override
    protected List<JoinedObjBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
//        return cddb.taxonomyJOBs;
        return cddb.jobs.get(JoinedObjsTabKind.Taxonomy);
    }

    @Override
    public String getCaption() {
        return /*BIKGWTConstants.MENU_NAME_KNOWLEDGE + "<br>" + BIKGWTConstants.MENU_NAME_TAXONOMY*/ I18n.powiazania.get();
    }

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_DYNAMIC_TREE;
    }
}
