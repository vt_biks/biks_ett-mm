/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.fts;

import java.io.Serializable;

/**
 *
 * @author pmielanczuk
 */
public class StemmedWordBean implements Serializable {

    public int wordId;
    public String word;
    public int originFlag;

    public StemmedWordBean() {
        // aby się dało serializować
    }

    public StemmedWordBean(int wordId, String word, int originFlag) {
        this.wordId = wordId;
        this.word = word;
        this.originFlag = originFlag;
    }
}
