/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class KeywordPickerDialogBase extends BaseActionOrCancelDialog {

    protected ListBox listBox;
    protected List<String> tagsArray;
    protected TextBox textBox;
    protected IParametrizedContinuation<TextWithInherit> onKeywordSelected;
    protected VerticalPanel dialogContents;
    private HTML lblSearch;
    private Label lblListBox;
    protected CheckBox inheritToDescendantsCb;

    protected void startDataFetch() {
        // no-op
    }

    protected void addNewTagToDB(String tag) {
        // no-op
    }

    public void populateWithKeywords(Collection<String> keywords) {
        tagsArray = new ArrayList<String>(keywords);
        listBox.clear();
        for (String k : tagsArray) {
            listBox.addItem(k);
        }
        if (listBox.getItemCount() > 0) {
            listBox.setSelectedIndex(0);
        }
    }

    public void buildAndShow(IParametrizedContinuation<TextWithInherit> onKeywordSelected) {
        this.onKeywordSelected = onKeywordSelected;
        super.buildAndShowDialog();
    }

    private void addNewTag(IParametrizedContinuation<TextWithInherit> onKeywordSelected) {
        if (!textBox.getText().trim().equals("")) {
            if (!tagsArray.contains(textBox.getText())) {
                addNewTagToDB(textBox.getText());
                // w addNewTagToDB przy on success naley wywolac doId -  onKeywordSelected.doIt(textBox.getText());
            } else {
                onKeywordSelected.doIt(new TextWithInherit(textBox.getText(), inheritToDescendantsCb.getValue()));
            }
            cancelClicked();
        } else {
            Window.alert(I18n.poleNieMozeBycPuste.get() /* I18N:  */ + ".");
        }
    }

    @Override
    protected String getDialogCaption() {
        // return "Wybierz artykuł do podłączenia";//"Dodaj nowy tag";
        return I18n.powiazPojecieBiznesowe.get() /* I18N:  */; // może dodaj pojęcie biznesowe?
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.wybierz.get() /* I18N:  */;//"Dodaj";
    }

    @Override
    protected void buildMainWidgets() {
        Grid grid = new Grid(2, 2);
        grid.setCellSpacing(4);

        lblSearch = new HTML(I18n.szukajLubDodaj.get() /* I18N:  */ + ":");
        lblSearch.setStyleName("KPDlblSearch");
        lblListBox = new Label(I18n.pojeciaBiznesowe.get() /* I18N:  */ + ":");
        lblListBox.setStyleName("KPDlblListBox");

        actionBtn.setEnabled(false);

        listBox = new ListBox();
        listBox.setVisibleItemCount(5);
        listBox.setWidth("480px");
        startDataFetch();
        final PushButton button = new PushButton(new Image("images/plus_large.png" /* I18N: no */));

        dialogContents = new VerticalPanel();
        // dialogContents.setSpacing(4);

        HorizontalPanel search = new HorizontalPanel();
        search.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
        // search.setSpacing(4);
        textBox = new TextBox();
        textBox.setWidth("450px");
        textBox.addKeyUpHandler(new KeyUpHandler() {

            public void onKeyUp(KeyUpEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_DOWN && listBox.getItemCount() > 0) {
                    listBox.setFocus(true);
                    if (listBox.getItemCount() > 0) {
                        listBox.setSelectedIndex(0);
                        actionBtn.setEnabled(true);
                    }
                } else if ((event.getNativeKeyCode() == KeyCodes.KEY_ENTER) && (!textBox.getText().equals(""))) {
                    addNewTag(onKeywordSelected);
                    actionBtn.setEnabled(false);
                } else {
                    String searchTxtLower = textBox.getText().toLowerCase();
                    listBox.clear();
                    // Collections.sort(tagsArray);
                    for (String k : tagsArray) {
                        String kLower = k.toLowerCase();
                        if (kLower.indexOf(searchTxtLower) != -1) {
                            listBox.addItem(k); // zwraca -1 gdy nie ma go
                        }
                    }
                    listBox.setVisibleItemCount(5);
                    actionBtn.setEnabled(false);
                }
            }
        });

        listBox.addDoubleClickHandler(new DoubleClickHandler() {

            public void onDoubleClick(DoubleClickEvent event) {
                actionClicked();
            }
        });

        listBox.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
                if (listBox.getSelectedIndex() != -1) {
                    actionBtn.setEnabled(true);
                }
            }
        });

        search.add(textBox);

        button.setTitle(I18n.dodajNowyTag.get() /* I18N:  */);
        button.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                addNewTag(onKeywordSelected);
            }
        });
        Style style = button.getElement().getStyle();
        style.setMargin(0, Unit.PX);
        style.setPadding(4, Unit.PX);
        GWTUtils.setStyleAttribute(button.getElement(), "background" /* I18N: no */, "none" /* I18N: no */);
        style.setBorderWidth(0, Unit.PX);
        search.add(button);
        search.setCellWidth(button, "20px");

        // listBox.setWidth("100%");
        listBox.addKeyDownHandler(new KeyDownHandler() {

            @Override
            public void onKeyDown(KeyDownEvent event) {
                if ((listBox.getItemCount() > 0) && (listBox.getSelectedIndex() == 0) && (event.getNativeKeyCode() == KeyCodes.KEY_UP)) {
                    textBox.setFocus(true);
                }
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    onKeywordSelected.doIt(new TextWithInherit(listBox.getItemText(listBox.getSelectedIndex()), inheritToDescendantsCb.getValue()));
                    cancelClicked();
                }
            }
        });
        listBox.addDoubleClickHandler(new DoubleClickHandler() {

            public void onDoubleClick(DoubleClickEvent event) {
//                onKeywordSelected.doIt(new TextWithInherit(listBox.getItemText(listBox.getSelectedIndex()), inheritToDescendantsCb.getValue()));
//                cancelClicked();
                actionBtn.setEnabled(false);
            }
        });
        listBox.setVisibleItemCount(7);
        //dialogContents.add(search);
        //dialogContents.add(listBox);
        //main.add(dialogContents);
        textBox.setStyleName("KPDtextBox");
        listBox.setStyleName("KPDlistBox");

        grid.setWidget(0, 0, lblSearch);
        grid.setWidget(0, 1, search);
        grid.setWidget(1, 0, lblListBox);
        grid.setWidget(1, 1, listBox);
        main.add(grid);
        HorizontalPanel hp = new HorizontalPanel();
        hp.add(inheritToDescendantsCb = new CheckBox());
        hp.add(new Label(I18n.pokazujWObiektachPodrzednych.get() /* I18N:  */));
        main.add(hp);
    }

    @Override
    protected void doAction() {
        if (listBox.getItemCount() == 0) {
            if (!textBox.getText().trim().equals("")) {
                addNewTagToDB(textBox.getText());
//                onKeywordSelected.doIt(textBox.getText());
            }
        } else {
            if (listBox.getSelectedIndex() == -1) { // widoczna jest lista ale nic nie jest zaznaczone
                for (int i = 0; i < listBox.getItemCount(); i++) {
                    if (listBox.getItemText(i).equals(textBox.getText())) {
//                        onKeywordSelected.doIt(listBox.getItemText(i)); // czy warto?
                        //cancelClicked();
                        return;
                    }
                }
                if (!textBox.getText().trim().equals("")) {
                    addNewTagToDB(textBox.getText());
//                    onKeywordSelected.doIt(textBox.getText());
                }
            } else { // cos jest zaznaczone
                onKeywordSelected.doIt(new TextWithInherit(listBox.getItemText(listBox.getSelectedIndex()), inheritToDescendantsCb.getValue()));
            }

        }
//        cancelClicked();
    }
}
