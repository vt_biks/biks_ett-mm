/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

/**
 *
 * @author wezyr
 */
public class MssqlConnectionConfig implements Cloneable {

    public String server;
    public String instance;
    public String database;
    public String user; // null -> use integrated login
    public String password;
    public int poolSize = 30;
    public int timeout = 10;

    public MssqlConnectionConfig() {
    }

    public MssqlConnectionConfig(String server, String instance, String database, String user, String password) {
        this.server = server;
        this.instance = instance;
        this.database = database;
        this.user = user;
        this.password = password;
    }

    @Override
    public MssqlConnectionConfig clone() {
        try {
            return (MssqlConnectionConfig) super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException("what a surprise! not cloneable?", ex);
        }
    }

    @Override
    public String toString() {
        return "MssqlConnectionConfig{" + "server=" + server + ", instance=" + instance + ", database=" + database + ", user=" + user + ", poolSize=" + poolSize + ", timeout=" + timeout + '}';
    }
}
