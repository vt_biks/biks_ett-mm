
exec sp_check_version '1.0.45';
go
create table aaa_universe_connetion_networklayer(
id int identity(1,1) not null primary key,
name varchar(255) not null unique,
)
go

create table aaa_universe_connetion(
id int identity(1,1) not null primary key,
connetion_name varchar(1000) not null unique,
server varchar(255),
user_name varchar(255), 
password varchar(255),
database_source varchar(500),
connetion_networklayer_id int not null references aaa_universe_connetion_networklayer(id)
)
go

create table aaa_universe(
id int identity(1,1) not null primary key,
unique_id varchar(300) not null,
name varchar(255) not null,
description varchar(max),
file_name varchar(255) not null,
author varchar(120),
universe_connetion_id int not null references aaa_universe_connetion(id)
)
go

create table aaa_universe_class(
id int identity(1,1) not null primary key,
si_id int not null unique,
parent_id int references aaa_universe_class(si_id), 
name varchar(255) not null,
description varchar(max),
universe_id int not null references aaa_universe(id)
)
go

create table aaa_universe_obj(
id int identity(1,1) not null primary key,
name varchar(255) not null,
description varchar(max),
text_of_select varchar(max) not null,
type varchar(255) not null,
universe_class_id int references aaa_universe_class(id)
)
go

create table aaa_universe_table(
id int identity(1,1) not null primary key,
their_id int not null unique,
name varchar(255) not null,
is_alias int not null check(is_alias in (0,1)),
is_derived int not null check(is_derived in (0,1)),
original_table int references aaa_universe_table(their_id),
sql_of_derived_table varchar(max),
sql_of_derived_table_with_alias varchar(max),
)
go

create table aaa_universe_column(
id int identity(1,1) not null primary key,
universe_table_id int not null references aaa_universe_table(id),
name varchar(255) not null,
type varchar(255) not null,
)
go

insert into bik_data_source_def(description)
values('BO Universe Designer')
go

declare @nameFK varchar(255);
select @nameFK = 
fk.ForeignKey_Name
from sysobjects so inner join syscolumns sc on so.id = sc.id
 left join vw_ww_foreignkeys fk on fk.table_name = so.name and fk.name = sc.name
where  so.name like 'aaa_universe_class'
and fk.ReferencedColumn = 'si_id'
print @nameFK
exec ('alter table aaa_universe_class drop constraint ' + @nameFK)
go

declare @uniqueName varchar(100);

select @uniqueName = so.name
from sys.objects so join sys.columns sc on so.parent_object_id = sc.object_id 
join sys.tables st on sc.object_id = st.object_id
where so.type='UQ' and sc.name = 'parent_id' and st.name = 'aaa_universe_class'
print @uniqueName

exec ('alter table aaa_universe_class drop constraint ' + @uniqueName)
go

alter table aaa_universe_class
add constraint UNIQ_unique_si_id_aaa_universe_class unique(si_id, universe_id)
go

/*alter table aaa_universe_class
add foreign key(parent_id) references aaa_universe_class(si_id)
go*/

alter table aaa_universe_obj
add si_id int not null unique
go

alter table aaa_universe_obj
add parent_id int references aaa_universe_obj(si_id)
go


declare @nameFK varchar(255);
select @nameFK = 
fk.ForeignKey_Name
from sysobjects so inner join syscolumns sc on so.id = sc.id
 left join vw_ww_foreignkeys fk on fk.table_name = so.name and fk.name = sc.name
where  so.name like 'aaa_universe_obj'
and fk.ReferencedColumn = 'parent_id'
print @nameFK
exec ('alter table aaa_universe_obj drop constraint ' + @nameFK)
go

declare @uniqueName varchar(100);

select @uniqueName = 
so.name
from sys.objects so join sys.columns sc on so.parent_object_id = sc.object_id 
join sys.tables st on sc.object_id = st.object_id
where so.type='UQ' and sc.name = 'si_id' and st.name = 'aaa_universe_obj'
print @uniqueName

exec ('alter table aaa_universe_obj drop constraint ' + @uniqueName)
go

alter table aaa_universe_obj
add constraint UNIQ_unique_si_id_aaa_universe_obj unique(si_id, universe_class_id)
go

alter table aaa_universe_obj
add foreign key(parent_id) references aaa_universe_obj(si_id)--pamietać o zmianie
go

alter table aaa_universe_obj
alter column text_of_select varchar(max);
go

alter table aaa_universe
	add path varchar(500) not null 
go

alter table aaa_universe
add constraint UNIQ_unique_id_path_aaa_universe unique(unique_id, path)
go

alter table aaa_universe_obj
add qualification varchar(50);
go

alter table aaa_universe_obj
add aggregate_function varchar(50);
go

declare @nameFK varchar(255);
select @nameFK = 
fk.ForeignKey_Name
from sysobjects so inner join syscolumns sc on so.id = sc.id
 left join vw_ww_foreignkeys fk on fk.table_name = so.name and fk.name = sc.name
where  so.name like 'aaa_universe_table'
and fk.ReferencedColumn = 'original_table'
print @nameFK
exec ('alter table aaa_universe_table drop constraint ' + @nameFK)
go

declare @uniqueName varchar(100);
select @uniqueName = 
so.name
from sys.objects so join sys.columns sc on so.parent_object_id = sc.object_id 
join sys.tables st on sc.object_id = st.object_id
where so.type='UQ' and sc.name = 'their_id' and st.name = 'aaa_universe_table'
print @uniqueName

exec ('alter table aaa_universe_table drop constraint ' + @uniqueName)
go

alter table aaa_universe_table
add universe_id int references aaa_universe(id);
go
alter table aaa_universe_table
add constraint UNIQ_unique_their_id_aaa_universe_table unique(their_id, universe_id)
go


/*alter table aaa_universe_table
add foreign key(original_table) references aaa_universe_table(their_id)--pamietać o zmianie
go*/

alter table aaa_universe_column
add constraint UNIQ_unique_name_id_aaa_universe_column unique(name, universe_table_id)
go

create table aaa_universe_obj_tables(
	id int identity(1,1) not null primary key,
	universe_table_id int not null references aaa_universe_table(id),
	universe_obj_id int not null references aaa_universe_obj(id),
	unique(universe_table_id, universe_obj_id),
)
go

alter table aaa_universe
add global_props_si_id int references aaa_global_props(si_id)
go


 exec sp_update_version '1.0.45', '1.0.46';
 go

