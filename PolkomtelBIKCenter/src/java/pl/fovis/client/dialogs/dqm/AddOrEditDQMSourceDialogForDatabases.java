/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import java.util.HashMap;
import java.util.Map;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class AddOrEditDQMSourceDialogForDatabases extends AddOrEditDQMSourceDialogBase {

    protected Label databaseNameLbl;
    protected TextBox fileNameTbx;
    protected Integer databaseNodeId;
    protected Map<Integer, SourceType> indexToSourceTypeMap;
    protected Label fileNameLbl;
    protected String serverName;

    @Override
    protected boolean doActionBeforeClose() {
        boolean isProfileSelected = getSelectedSource().equals(SourceType.Profile);
        if (BaseUtils.isStrEmptyOrWhiteSpace(codeNameTbx.getText())) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.nazwaKodowaJestPusta.get(), I18n.blad.get(), null);
            return false;
        }
        if (!isProfileSelected && databaseNodeId == null) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.wybierzBazeDanych.get(), I18n.blad.get(), null);
            return false;
        }
        if (!isProfileSelected && BaseUtils.isStrEmptyOrWhiteSpace(queryArea.getText())) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.zapytaniePobierajaceDaneNieMozeBycPuste.get(), I18n.blad.get(), null);
            return false;
        }
        if (isProfileSelected && BaseUtils.isStrEmptyOrWhiteSpace(fileNameTbx.getText())) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.nazwaPlikuNaSFTPNieMozeBycPusta.get(), I18n.blad.get(), null);
            return false;
        }
        return true;
    }

    @Override
    protected void fillLbx() {
        indexToSourceTypeMap = new HashMap<Integer, SourceType>();
        int index = 0;
        for (SourceType value : SourceType.values()) {
            sourceLbx.addItem(value.getName());
            indexToSourceTypeMap.put(index++, value);
        }
        sourceLbx.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                setSelectedSourceWidget();
                cleanWidgets();
            }
        });
    }

    @Override
    protected void innerWidget() {

        FlexTable databaseFT = new FlexTable();
        FlexTable profileFT = new FlexTable();
        HorizontalPanel databaseHP = new HorizontalPanel();
        databaseHP.add(databaseNameLbl = new Label(I18n.brak.get()));
        PushButton selectDatabaseBtn = new PushButton(I18n.edytuj.get());
        NewLookUtils.makeCustomPushButton(selectDatabaseBtn);
        selectDatabaseBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new TreeSelectorForDQMDatabase().buildAndShowDialog(nodeId, indexToSourceTypeMap.get(sourceLbx.getSelectedIndex()).getTreeCode(), new IParametrizedContinuation<TreeNodeBean>() {

                    @Override
                    public void doIt(TreeNodeBean param) {
                        databaseNameLbl.setText(param.name);
                        databaseNodeId = param.id;
                        serverName = BaseUtils.getFirstItems(param.objId, "|", 1);
                    }
                });
            }
        });
        databaseHP.add(selectDatabaseBtn);
        databaseFT.setHTML(0, 0, I18n.bazaDanych.get());
        databaseFT.setWidget(0, 1, databaseHP);
        databaseFT.getColumnFormatter().setWidth(0, "180px");
        databaseVP.add(databaseFT);
        databaseVP.add(new Label(I18n.zapytaniePobierajaceDane.get()));
        databaseVP.add(queryArea = new TextArea());
        queryArea.setWidth("380px");
        queryArea.setHeight("200px");
        profileFT.setHTML(0, 0, I18n.plikZDanymi.get());
        profileFT.setHTML(1, 0, I18n.nazwaPlikuZDanymiNaFTP.get());
        profileFT.setWidget(0, 1, fileNameTbx = new TextBox());
        fileNameTbx.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                updateLabelTexts();
            }
        });
        fileNameTbx.setWidth("200px");
        profileFT.setWidget(1, 1, fileNameLbl = new Label(""));
        profileFT.getColumnFormatter().setWidth(0, "180px");
        profileVP.add(profileFT);
        main.add(databaseVP);
        main.add(profileVP);
    }

    @Override
    protected int getSelectedIdx() {
        databaseNameLbl.setText(!BaseUtils.isStrEmptyOrWhiteSpace(bean.databaseName) ? bean.databaseName : "");
        databaseNodeId = bean.databaseNodeId;
        fileNameTbx.setText(!BaseUtils.isStrEmptyOrWhiteSpace(bean.resultFileName) ? bean.resultFileName : "");
        int index = 0;
        for (Map.Entry<Integer, SourceType> entrySet : indexToSourceTypeMap.entrySet()) {
            String sourceName = entrySet.getValue().getSourceName();
            if (sourceName.equals(bean.sourceName.trim())) {
                index = entrySet.getKey();
                break;
            }
        }
        return index;
    }

    protected void doActionOpt(DQMMultiSourceBean bean) {
        bean.sourceName = sourceLbx.getValue(sourceLbx.getSelectedIndex());
        bean.serverName = serverName;
        bean.databaseName = databaseNameLbl.getText();
        bean.resultFileName = fileNameTbx.getText();
        bean.databaseNodeId = databaseNodeId;
    }

    protected void cleanWidgetsOpt() {
        databaseNodeId = null;
        databaseNameLbl.setText("");
        fileNameTbx.setText("");
//        sqlTestText = "";
//        queryArea.setText("");
    }

    protected void updateLabelTexts() {
        String text = fileNameTbx.getText().trim();
        boolean isStrEmpty = BaseUtils.isStrEmptyOrWhiteSpace(text);
        fileNameLbl.setText(isStrEmpty ? "" : text + TEST_FILE_SUFFIX);
    }

    protected void setSelectedSourceWidget() {
        SourceType type = indexToSourceTypeMap.get(sourceLbx.getSelectedIndex());
        boolean isProfileSelected = type.equals(SourceType.Profile);
        databaseVP.setVisible(!isProfileSelected);
        profileVP.setVisible(isProfileSelected);
    }

    protected SourceType getSelectedSource() {
        return indexToSourceTypeMap.get(sourceLbx.getSelectedIndex());
    }

    @Override
    protected String getName() {
        return I18n.konektor.get();
    }
}
