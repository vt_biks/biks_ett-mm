/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.JasperReportExportWidget;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class NodeActionGetJasperReport extends NodeActionUpdatingBase {

    @Override
    public boolean isEnabledByDesign() {
        return BIKClientSingletons.showExportJasperReports() && !isMetaBiks()
                && !BaseUtils.isStrEmptyOrWhiteSpace(BIKClientSingletons.getNodeKindById(node.nodeKindId).jasperReports);
    }

    @Override
    public String getCaption() {
        return I18n.pobierzRaport.get();
    }

    @Override
    public void execute() {
        final String jasperReports = BIKClientSingletons.getNodeKindById(node.nodeKindId).jasperReports;
        Map<String, String> rp = new HashMap<String, String>();
        List<String> reports = BaseUtils.splitBySep(jasperReports, "|", true);
        for (String r : reports) {
            Pair<String, String> nameToLocation = BaseUtils.splitString(r, ",");
            if (nameToLocation.v2 != null) {
                rp.put(nameToLocation.v2, nameToLocation.v1);
            }
        }
        new JasperReportExportWidget().buildAndShowDialog(node.id, rp, new IParametrizedContinuation<String>() {

            @Override
            public void doIt(String param) {

            }
        });
    }

}
