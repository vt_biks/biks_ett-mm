﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- dodanie disableGuestMode
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_check_version '1.6.z6';
go

if not exists(select 1 from bik_app_prop where name = 'disableGuestMode')
begin
	insert into bik_app_prop(name, val, is_editable)
	values ('disableGuestMode', 'false', 1)
end;

exec sp_update_version '1.6.z6', '1.6.z6.1';
go