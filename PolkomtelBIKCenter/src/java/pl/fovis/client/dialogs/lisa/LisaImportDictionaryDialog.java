/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.client.entitydetailswidgets.lisa.LisaAuthorizedRequest;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.ImportOverviewBean;
import pl.fovis.common.lisa.LisaConstants.ImportMode;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryBean;
import pl.fovis.common.lisa.LisaUtils;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IContinuation;

/**
 *
 * @author ctran
 */
public class LisaImportDictionaryDialog extends BaseActionOrCancelDialog {

    protected LisaDictionaryBean selectedDict;
    protected List<LisaDictColumnBean> allColumnsOfSelectedDict;
    protected List<String> headerNames;
    protected List<TextBox> listNames = new ArrayList<TextBox>();
    protected String serverFileName;
    protected RadioButton overrideImportModeRb, addingImportModeTb, deleteImportModeTb;
    protected Map<String, String> mappedHeaders = new HashMap<String, String>();
    protected IContinuation cancelAction;

    @Override
    protected String getDialogCaption() {
        return I18n.konfiguracjaImportu.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.analizuj.get();
    }

    @Override
    protected void buildMainWidgets() {
        main.add(createImportModePanel());
        main.add(createColumnNamesMappingPanel());
    }

    @Override
    protected void doAction() {
        for (int i = 0; i < headerNames.size(); i++) {
            mappedHeaders.put(headerNames.get(i), listNames.get(i).getText());
        }
        new LisaAuthorizedRequest<ImportOverviewBean>(selectedDict.lisaId, selectedDict.lisaName) {
            private BIKSProgressInfoDialog infoDialog;

            @Override
            protected void performServerAction(AsyncCallback<ImportOverviewBean> callback) {
                infoDialog = new BIKSProgressInfoDialog();

                if (checkMapping()) {
                    infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaPrzygotowanieImportu.get(), true);
                    BIKClientSingletons.getLisaService().analyseDictWithImportedFile(selectedDict.lisaId, selectedDict.nodeId, mappedHeaders, serverFileName, getImportMode(), callback);
                }
            }

            @Override
            public void onActionSuccess(ImportOverviewBean ret) {
                infoDialog.hideDialog();
                new LisaImportOverviewDialog().buildAndShowDialog(serverFileName, ret, LisaUtils.extractPkColumns(allColumnsOfSelectedDict), new IContinuation() {

                    @Override
                    public void doIt() {
                        callService2Import();
                    }
                }, cancelAction);
            }

            @Override
            public void doSomethingOptOnFailure() {
                infoDialog.hideDialog();
            }

            @Override
            public void onActionFailure(Throwable caught) {
                super.onActionFailure(caught); //To change body of generated methods, choose Tools | Templates.
                infoDialog.hideDialog();
                callService2ClearCache();
            }
        }.setNodeId(selectedDict.nodeId).go();
    }

    private boolean checkMapping() {
        if (mappedHeaders.size() != allColumnsOfSelectedDict.size()) {
            BIKClientSingletons.showWarning(I18n.liczbyKolumnyNieSieZgadzaja.get());
            return false;
        }
        for (LisaDictColumnBean column : allColumnsOfSelectedDict) {
            if (!mappedHeaders.containsValue(column.nameInDb)) {
                BIKClientSingletons.showWarning(I18n.brakKolumn.get());
                return false;
            }
        }
        return true;
    }

    private ImportMode getImportMode() {
        if (addingImportModeTb.getValue()) {
            return ImportMode.ADD;
        } else if (overrideImportModeRb.getValue()) {
            return ImportMode.OVERWRITE;
        } else if (deleteImportModeTb.getValue()) {
            return ImportMode.DELETE;
        }
        throw new UnsupportedOperationException("Nie ma takiego trybu importu");
    }

    private void callService2ClearCache() {
        //kasowac temporalny plik, rekordy w bik_lisa_dict_import_cahce
        BIKClientSingletons.getLisaService().clearLisaImportCache(serverFileName, new AsyncCallback<Void>() {

            @Override
            public void onFailure(Throwable caught) {
                BIKClientSingletons.showWarning("clearLisaImportCache");
            }

            @Override
            public void onSuccess(Void result) {
                BIKClientSingletons.showInfo(I18n.gotowe.get());
            }
        });
    }

    private void callService2Import() {
        new LisaAuthorizedRequest<Void>(selectedDict.lisaId, selectedDict.lisaName) {
            private BIKSProgressInfoDialog infoDialog;

            @Override
            protected void performServerAction(AsyncCallback<Void> callback) {
                infoDialog = new BIKSProgressInfoDialog();
                infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaPrzygotowanieImportu.get(), true);
                BIKClientSingletons.getLisaService().importDictionary(selectedDict.lisaId, selectedDict.nodeId, mappedHeaders, serverFileName, getImportMode(), callback);
            }

            @Override
            public void onActionSuccess(Void ret) {
                infoDialog.hideDialog();
                callService2ClearCache();
            }

            @Override
            public void doSomethingOptOnFailure() {
                infoDialog.hideDialog();
            }

            @Override
            public void onActionFailure(Throwable caught) {
                infoDialog.hideDialog();
                super.onActionFailure(caught); //To change body of generated methods, choose Tools | Templates.
                callService2ClearCache();
            }
        }.setNodeId(selectedDict.nodeId).go();
    }

    private VerticalPanel createImportModePanel() {
        VerticalPanel vp = new VerticalPanel();
        vp.add(new Label(I18n.trybImportu.get()));
        vp.add(overrideImportModeRb = new RadioButton("importType", I18n.nadpisywanie.get()));
        vp.add(addingImportModeTb = new RadioButton("importType", I18n.dodawanie.get()));
        vp.add(deleteImportModeTb = new RadioButton("importType", I18n.usuwanie.get()));
        addingImportModeTb.setValue(true);

        return vp;
    }

    private ScrollPanel createColumnNamesMappingPanel() {
        ScrollPanel scrollPanel = new ScrollPanel();
        scrollPanel.setHeight("400px");
        scrollPanel.setWidth("400px");
        Grid grid = new Grid(headerNames.size() + 1, 2);
        int row = 0;
        grid.setWidget(row, 0, new HTML("<b>" + I18n.nazwaWPliku.get() + "</b>"));
        grid.setWidget(row++, 1, new HTML("<b>" + I18n.nazwaKolumny.get() + "</b>"));
        for (int i = 0; i < headerNames.size(); i++) {
            TextBox tb = new TextBox();
            listNames.add(tb);
            tb.setText((i < allColumnsOfSelectedDict.size() ? allColumnsOfSelectedDict.get(i).nameInDb : ""));
            String styleName = grid.getRowFormatter().getStyleName(row);
            styleName += ((styleName.length() > 0) ? " " : "") + ((row % 2 == 1) ? "odd" : "even");
            grid.getRowFormatter().setStyleName(row, styleName);

            grid.setWidget(row, 0, new Label(headerNames.get(i)));
            grid.setWidget(row++, 1, tb);
        }
        scrollPanel.add(grid);
        return scrollPanel;
    }

    public void buildAndShowDialog(LisaDictionaryBean selectedDict, List<LisaDictColumnBean> allColumnsOfSelectedDict, List<String> headerNames, String serverFileName) {
        this.selectedDict = selectedDict;
        this.allColumnsOfSelectedDict = allColumnsOfSelectedDict;
        this.headerNames = headerNames;
        this.serverFileName = serverFileName;

        cancelAction = new IContinuation() {

            @Override
            public void doIt() {
                callService2ClearCache();
            }
        };
        super.buildAndShowDialog();
    }
}
