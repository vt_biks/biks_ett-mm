/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.ftp;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPFile;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pmielanczuk
 */
public class FTP4JFoxyFTPClient implements IFoxyFTPClient {

    protected FTPClient client;
    protected String host;
    protected int port;
    protected String user;
    protected String password;
    protected String currentDirectory;

    @Override
    public void connectAndLogin(String host, int port, String user, String password) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;

        client = new FTPClient();
        try {
            client.connect(host, port);
            client.login(user, password);
            //client.setAutoNoopTimeout(...);
            this.currentDirectory = client.currentDirectory();
            //Utils.info("FTP Client connectAndLogin: current DIR=" + currentDirectory);

        } catch (Exception ex) {
            throw new FoxyFTPException("error in connectAndLogin", ex);
        }
    }

    protected void innerChangeDir(String dir) {
        try {
            currentDirectory = dir;
            client.changeDirectory(dir);
        } catch (Exception ex) {
            throw new FoxyFTPException(ex);
        }
    }

    protected void reconnectAndResetCurrentDir() {
        String oldCurDir = currentDirectory;
        try {
            if (client.isConnected()) {
                client.disconnect(false);
            }
        } catch (Exception ex) {
            // no-op: silent
        }

        connectAndLogin(host, port, user, password);
        innerChangeDir(oldCurDir);
    }

    protected void changeDirWithReconnectOnError(String currDir) {
        try {
            innerChangeDir(currDir);
        } catch (Exception ex) {
            System.out.println("error in innerChangeDir: dir=" + currDir + ", will try to reconnect");
            reconnectAndResetCurrentDir();
        }
    }

    @Override
    public void changeDirectory(String dir) {
        changeDirWithReconnectOnError(dir);
//        try {
//            innerChangeDir(dir);
//        } catch (Exception ex) {
//            throw new FoxyFTPException(ex);
//        }
    }

    @Override
    public String getCurrentDirectory() {
        try {
            return client.currentDirectory();
        } catch (Exception ex) {
            throw new FoxyFTPException(ex);
        }
    }

    protected List<IFoxyFTPFile> innerGetFiles(String optFileMask) {
        try {
            FTPFile[] files = null;
            int retryNum = 0;

            while (files == null) {
                try {
                    files = client.list(optFileMask);
                } catch (Exception ex) {
                    retryNum++;
                    if (retryNum > 5) {
                        throw ex;
                    } else {
                        System.out.println("error in list, retrying #" + retryNum);
                        if (retryNum % 2 == 0) {
                            changeDirWithReconnectOnError(currentDirectory);
                        }
                    }
                }
            }

            List<IFoxyFTPFile> res = new ArrayList<IFoxyFTPFile>(files.length);

            for (FTPFile file : files) {
                res.add(new FTP4JFoxyFTPFile(file));
            }

            return res;
        } catch (Exception ex) {
            throw new FoxyFTPException(ex);
        }
    }

    @Override
    public void disconnect() {
        if (client.isConnected()) {
            //System.out.println("DISCONNECTING");
            try {
                client.disconnect(true);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        } else {
            //System.out.println("NOT CONNECTED");
        }
    }

    @Override
    public void download(String remoteFileName, String localFileName) {
        try {

            int retryNum = 0;
            boolean success = false;

            while (!success) {

                try {
                    //System.out.println("*** CLIENT TYPE=" + client.getType());
                    client.setType(FTPClient.TYPE_BINARY);

                    File newFile = new File(localFileName);
                    client.download(remoteFileName, newFile);

                    long remoteSize = client.fileSize(remoteFileName);
                    long localSize = newFile.length();

                    if (remoteSize != localSize) {
                        //newFile.delete();
                        System.out.println("FTP SIZE ERROR! Remote file: " + remoteFileName + ", remote size: "
                                + remoteSize + ", local file: " + localFileName + ", local size: " + localSize + ". Local file not REMOVED!");
                    }

                    success = true;

                } catch (Exception ex) {
                    retryNum++;
                    if (retryNum > 5) {
                        throw ex;
                    } else {
                        System.out.println("error in download, retrying #" + retryNum);
                        if (retryNum % 2 == 0) {
                            changeDirWithReconnectOnError(currentDirectory);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw new FoxyFTPException(ex);
        }
    }

    @Override
    public List<IFoxyFTPFile> getFiles(String fileMask) {
        return innerGetFiles(fileMask);
    }

    @Override
    public List<IFoxyFTPFile> getFiles() {
        return innerGetFiles(null);
    }
//    public String getCurrentDir() {
//        return currentDirectory;
//    }
//    public static void main(String[] args) {
//        Integer a = 100;
//        Integer b = new Integer(a + 1);
//        b = b - 1;
//        if (a == b) {
//            System.out.println("ROWNE! a=" + a + ", b=" + b);
//        } else {
//            System.out.println("RӯNE a=" + a + ", b=" + b);
//        }
//    }
}
