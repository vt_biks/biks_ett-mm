﻿exec sp_check_version '1.1.1.2';
go
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---dodanie kolumny avatar do użytkownika systemowego
alter table bik_system_user
	add avatar varchar(500)
go

--przepisanie avatarów z użytkowników społecznościowych do systemowych
update  bik_system_user 
set bik_system_user.avatar = bik_user.avatar
from bik_system_user join bik_user on bik_system_user.user_id = bik_user.id
go

--usuniecie avatara ze spolecznościowego użytkownika
alter table bik_user
	drop column avatar
go
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
--usuniecie uniqa z kolymny login_name ze spolecznościowego użytkownika
declare @unique_name varchar(100);

select @unique_name = constraint_name
from information_schema.key_column_usage 
where table_name = 'bik_user' and column_name = 'login_name'

if @unique_name is not null
	exec('alter table bik_user drop constraint ' + @unique_name)
go
--usuniecie login_name ze spolecznościowego użytkownika
alter table bik_user
	drop column login_name   
go
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
--usuniecie password ze spolecznościowego użytkownika
alter table bik_user
	drop column password
go
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
--usuniecie klucza do bik_department id
declare @nameFK varchar(255);

select @nameFK = fk.ForeignKey_Name
from sys.objects so inner join sys.columns sc on so.object_id = sc.object_id
	left join vw_ww_foreignkeys fk on fk.table_name = so.name and fk.name = sc.name
where  so.name like 'bik_user' and sc.name = 'department_id'
	
if @nameFK is not null	
	exec ('alter table bik_user drop constraint ' + @nameFK)
	
--usuniecie kolumny departament_id z bik_user
if exists(select 1 from syscolumns sc where id = object_id('bik_user') and name = 'department_id')
	exec ('alter table bik_user drop column department_id')
go
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
--usunięcie tabeli bik_def_department
drop table bik_def_department 
go
--usunięcie tabeli bik_department
drop table bik_department 
go
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
exec sp_update_version '1.1.1.2', '1.1.1.3';
go