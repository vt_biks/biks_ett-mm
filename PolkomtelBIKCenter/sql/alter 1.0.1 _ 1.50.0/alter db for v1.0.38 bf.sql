exec sp_check_version '1.0.38';

delete from bik_metapedia_keyword
go

delete from bik_metapedia_linked
go

-- tu mialam jakos dziwnie po update (potrzebny mi byl CK_bik_tree_tree_kind a miałam CK_bik_tree_tree_id)
alter table bik_tree drop constraint CK_bik_tree_tree_id
go

alter table bik_tree add constraint CK_bik_tree_tree_kind check(tree_kind in (0, 1, 2, 3, 4,5))
go

insert into bik_node_kind values ('BlogEntry','Wpis blogowy')
go

insert into bik_node_kind values ('Blog','Blog')
go

insert into bik_tree select 'Blogi',5,id,'Blogs' from bik_node_kind where code='Blog'
go

alter table bik_blog
add node_id int references bik_node(id) null default null
go

exec sp_update_version '1.0.38', '1.0.39';
go