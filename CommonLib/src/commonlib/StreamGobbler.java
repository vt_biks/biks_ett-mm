/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class StreamGobbler extends Thread {

    public static final String NEW_LINE_SEP = "\n";

    private static final ILameLogger logger = LameUtils.getMyLogger();
    private InputStream is;
    private String type;
    private String errorMsg;
    private String outputMsg;
    private boolean useStandardOutput;

    public String getErrorMsg() {
        return errorMsg;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public StreamGobbler(InputStream is, String type) {
        this(is, type, false);
    }

    public StreamGobbler(InputStream is, String type, boolean useStandardOutput) {
        this.is = is;
        this.type = type;
        this.useStandardOutput = useStandardOutput;
    }

    @Override
    public void run() {
        try {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            StringBuilder sb = new StringBuilder();
            StringBuilder lines = new StringBuilder();

            while ((line = br.readLine()) != null) {
                if (useStandardOutput) {
                    System.out.println(type + ">" + line);
                } else {
                    if (logger.isInfoEnabled()) {
                        logger.info(type + ">" + line);
                    }
                }
                sb.append(line);
                lines.append(line).append(NEW_LINE_SEP);
            }
            if (type.equals("ERROR")) {
                String trimedTmpString = sb.toString().trim();
                if (!trimedTmpString.equals("")) {
                    errorMsg = trimedTmpString;
                }
            }
            outputMsg = lines.toString();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
