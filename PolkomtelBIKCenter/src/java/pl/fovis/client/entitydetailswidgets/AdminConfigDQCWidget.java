/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.ConnectionParametersDQCBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class AdminConfigDQCWidget extends AdminConfigBIKBaseWidget<ConnectionParametersDQCBean> {

    protected TextBox tbxServer;
    protected TextBox tbxInstance;
    protected TextBox tbxDB;
    protected TextBox tbxUser;
    protected TextBox tbxPw;
    protected TextBox tbxGroupFilter;
    protected TextBox tbxActiveFilter;
//    protected CheckBox chbPw;
    protected CheckBox chbInactive;
    protected CheckBox chbCheckActivity;
    protected PushButton btnSave;
    protected CheckBox chbIsActive;

    @Override
    public void buildInnerWidget() {
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
            }
        }, false);
        createLine();
        createLabel(I18n.serwer.get() + ":");
        tbxServer = createTextBox();
        createLabel(I18n.instancja.get() + ":");
        tbxInstance = createTextBox();
        createLabel(I18n.bazaDanych.get() + ":");
        tbxDB = createTextBox();
        createLabel(I18n.uzytkownik.get() + ":");
        tbxUser = createTextBox();
        createLabel(I18n.haslo.get() + ":");
        tbxPw = createPasswordTextBox();
//        chbPw = createAndAddCheckBox(I18n.ukryjHaslo.get(), new ValueChangeHandler<Boolean>() {
//            @Override
//            public void onValueChange(ValueChangeEvent<Boolean> event) {
//                if (chbPw.getValue()) {
//                    tbxPw.getElement().setAttribute("type" /* I18N: no */, "password" /* I18N: no */);
//                } else {
//                    tbxPw.getElement().setAttribute("type" /* I18N: no */, "text" /* I18N: no */);
//                }
//            }
//        });
        createLabel(I18n.activeRequestForTest.get());
        tbxActiveFilter = createTextBox();
        chbInactive = createAndAddCheckBoxWithOutHandler(I18n.showInactiveTests.get());
        chbCheckActivity = createAndAddCheckBoxWithOutHandler(I18n.sprawdzajAktywnoscTestu.get());
        createLabel(I18n.ignorujGrupyTestow.get());
        tbxGroupFilter = createTextBox();
        createLine();
        btnSave = createButton(I18n.zapisz.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ConnectionParametersDQCBean bean = new ConnectionParametersDQCBean();
                bean.server = tbxServer.getText();
                bean.instance = tbxInstance.getText();
                bean.database = tbxDB.getText();
                bean.user = tbxUser.getText();
                bean.password = tbxPw.getText();
                bean.activeFilter = BaseUtils.tryParseInteger(tbxActiveFilter.getText(), -1);
                bean.groupFilter = tbxGroupFilter.getText();
                bean.withInactiveTests = chbInactive.getValue() ? 1 : 0;
                bean.checkTestActivity = chbCheckActivity.getValue() ? 1 : 0;
                bean.isActive = chbIsActive.getValue() ? 1 : 0;
                BIKClientSingletons.getService().setDQCConnectionParameters(bean, new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " setDQCConnectionParameters") {
                            @Override
                            public void onSuccess(Void result) {
                                BIKClientSingletons.showInfo(I18n.zmienionParametrDoPolaczenZDQC.get() /* I18N:  */);
                            }
                        });
            }
        });
        createTestConnectionWidget(I18n.testujPolaczenie.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().runDQCTestConnection(new StandardAsyncCallback<Pair<Integer, String>>("Error in" /* I18N: no */ + " runDQCTestConnection") {
                            @Override
                            public void onSuccess(Pair<Integer, String> result) {
                                setResultTestConnection(result);
                            }
                        });
            }
        });
    }

    @Override
    public void populateWidgetWithData(ConnectionParametersDQCBean data) {
        tbxServer.setText(data.server);
        tbxInstance.setText(data.instance);
        tbxDB.setText(data.database);
        tbxUser.setText(data.user);
        tbxPw.setText(data.password);
        tbxActiveFilter.setText(data.activeFilter.toString());
        tbxGroupFilter.setText(data.groupFilter);
        chbInactive.setValue(data.withInactiveTests == 1);
        chbCheckActivity.setValue(data.checkTestActivity == 1);
        chbIsActive.setValue(data.isActive == 1);
        setWidgetsEnabled(chbIsActive.getValue());
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_DQC;
    }
}
