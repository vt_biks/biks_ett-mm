/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.common.AuditBean;
import pl.fovis.common.ConnectionParametersAuditBean;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.logging.ILameLogger;

/**
 *
 * @author bfechner
 */
public class SAPBOAuditConnector {

    protected ConnectionParametersAuditBean connection;
    private static final ILameLogger logger = LameUtils.getMyLogger();

    public SAPBOAuditConnector(ConnectionParametersAuditBean connection) {
        this.connection = connection;
    }

    public List<AuditBean> getData(List<AuditBean> auditBeans) {

        MssqlConnection mssqlConnection = new MssqlConnection(
                new MssqlConnectionConfig(connection.server, connection.instance, connection.dbName, connection.login, connection.password));

        mssqlConnection.setDbStatementExecutionWarnThresholdMillis(-1);

        List<AuditBean> dataFromAuditDatabase = mssqlConnection.createBeansFromQry(
                "select  *, "
                + "(select COUNT(*) from ADS_STATUS_STR INNER JOIN ADS_EVENT ON (ADS_EVENT.Event_Type_ID = ADS_STATUS_STR.Event_Type_ID  AND  ADS_EVENT.Status_ID = ADS_STATUS_STR.Status_ID  AND  ADS_STATUS_STR.Language = 'EN') "
                + "INNER JOIN ADS_OBJECT_TYPE_STR ON (ADS_EVENT.Object_Type_ID=ADS_OBJECT_TYPE_STR.Object_Type_ID  AND  ADS_OBJECT_TYPE_STR.Language = 'EN')  "
                + "WHERE "
                + "( "
                + "ADS_OBJECT_TYPE_STR.Object_Type_Name  IN  ( 'Web Intelligence','WebIntelligence'  ) AND ADS_STATUS_STR.Status_Name  IN  ( 'Viewed Successfully')) and x.cuid=ADS_EVENT.Object_ID and DATEADD(Month, -1, getdate())<ADS_EVENT.Start_Time "
                + ") as view_cnt, "
                + "(select MAX(ADS_EVENT.Start_Time) from ADS_STATUS_STR INNER JOIN ADS_EVENT ON (ADS_EVENT.Event_Type_ID = ADS_STATUS_STR.Event_Type_ID  AND  ADS_EVENT.Status_ID = ADS_STATUS_STR.Status_ID  AND  ADS_STATUS_STR.Language = 'EN') "
                + "INNER JOIN ADS_OBJECT_TYPE_STR ON (ADS_EVENT.Object_Type_ID=ADS_OBJECT_TYPE_STR.Object_Type_ID  AND  ADS_OBJECT_TYPE_STR.Language = 'EN')  "
                + "WHERE "
                + "( "
                + "ADS_OBJECT_TYPE_STR.Object_Type_Name  IN  ( 'Web Intelligence','WebIntelligence'  ) AND ADS_STATUS_STR.Status_Name  IN  ( 'Viewed Successfully')) and x.cuid=ADS_EVENT.Object_ID  "
                + ") as view_last "
                + ",(select COUNT(*) from ADS_STATUS_STR INNER JOIN ADS_EVENT ON (ADS_EVENT.Event_Type_ID = ADS_STATUS_STR.Event_Type_ID  AND  ADS_EVENT.Status_ID = ADS_STATUS_STR.Status_ID  AND  ADS_STATUS_STR.Language = 'EN') "
                + "INNER JOIN ADS_OBJECT_TYPE_STR ON (ADS_EVENT.Object_Type_ID=ADS_OBJECT_TYPE_STR.Object_Type_ID  AND  ADS_OBJECT_TYPE_STR.Language = 'EN')  "
                + "WHERE "
                + "( "
                + "ADS_OBJECT_TYPE_STR.Object_Type_Name  IN  ( 'Web Intelligence','WebIntelligence'  ) AND ADS_STATUS_STR.Status_Name  IN  ( 'Refreshed Successfully')) and x.cuid=ADS_EVENT.Object_ID  and DATEADD(Month, -1, getdate())<ADS_EVENT.Start_Time "
                + ") as refresh_cnt, "
                + "(select MAX(ADS_EVENT.Start_Time) from ADS_STATUS_STR INNER JOIN ADS_EVENT ON (ADS_EVENT.Event_Type_ID = ADS_STATUS_STR.Event_Type_ID  AND  ADS_EVENT.Status_ID = ADS_STATUS_STR.Status_ID  AND  ADS_STATUS_STR.Language = 'EN') "
                + "INNER JOIN ADS_OBJECT_TYPE_STR ON (ADS_EVENT.Object_Type_ID=ADS_OBJECT_TYPE_STR.Object_Type_ID  AND  ADS_OBJECT_TYPE_STR.Language = 'EN')  "
                + "WHERE "
                + "( "
                + "ADS_OBJECT_TYPE_STR.Object_Type_Name  IN  ( 'Web Intelligence','WebIntelligence'  ) AND ADS_STATUS_STR.Status_Name  IN  ( 'Refreshed Successfully')) and x.cuid=ADS_EVENT.Object_ID  "
                + ") as refresh_last "
                + " ,(select top 1 ADS_EVENT.User_Name from ADS_STATUS_STR INNER JOIN ADS_EVENT ON (ADS_EVENT.Event_Type_ID = ADS_STATUS_STR.Event_Type_ID  AND  ADS_EVENT.Status_ID = ADS_STATUS_STR.Status_ID  AND  ADS_STATUS_STR.Language = 'EN') INNER JOIN ADS_OBJECT_TYPE_STR ON (ADS_EVENT.Object_Type_ID=ADS_OBJECT_TYPE_STR.Object_Type_ID  AND  ADS_OBJECT_TYPE_STR.Language = 'EN')  WHERE ( ADS_OBJECT_TYPE_STR.Object_Type_Name  IN  ( 'Web Intelligence','WebIntelligence'  ) AND ADS_STATUS_STR.Status_Name  IN  ( 'Refreshed Successfully')) and x.cuid=ADS_EVENT.Object_ID  order by ADS_EVENT.Start_Time desc"
                + ") as user_refresh_last  "
                + " ,(select top 1 ADS_EVENT.User_Name from ADS_STATUS_STR INNER JOIN ADS_EVENT ON (ADS_EVENT.Event_Type_ID = ADS_STATUS_STR.Event_Type_ID  AND  ADS_EVENT.Status_ID = ADS_STATUS_STR.Status_ID  AND  ADS_STATUS_STR.Language = 'EN') INNER JOIN ADS_OBJECT_TYPE_STR ON (ADS_EVENT.Object_Type_ID=ADS_OBJECT_TYPE_STR.Object_Type_ID  AND  ADS_OBJECT_TYPE_STR.Language = 'EN')  WHERE ( ADS_OBJECT_TYPE_STR.Object_Type_Name  IN  ( 'Web Intelligence','WebIntelligence'  ) AND ADS_STATUS_STR.Status_Name  IN  ( 'Viewed Successfully')) and x.cuid=ADS_EVENT.Object_ID  order by ADS_EVENT.Start_Time desc"
                + ") as user_view_last  "
                + "from  (select  ADS_EVENT.Object_ID as cuid,object_folder_path as path from  "
                + "ADS_STATUS_STR INNER JOIN ADS_EVENT ON (ADS_EVENT.Event_Type_ID = ADS_STATUS_STR.Event_Type_ID  AND  ADS_EVENT.Status_ID = ADS_STATUS_STR.Status_ID  AND  ADS_STATUS_STR.Language = 'EN') "
                + "INNER JOIN ADS_OBJECT_TYPE_STR ON (ADS_EVENT.Object_Type_ID=ADS_OBJECT_TYPE_STR.Object_Type_ID  AND  ADS_OBJECT_TYPE_STR.Language = 'EN')  "
                + "WHERE "
                + "( "
                + "ADS_OBJECT_TYPE_STR.Object_Type_Name  IN  ( 'Web Intelligence','WebIntelligence'  ) "
                + "AND "
                + "ADS_STATUS_STR.Status_Name  IN  ( 'Viewed Successfully' ,'Refreshed Successfully')) "
                + "group by ADS_EVENT.Object_ID,object_folder_path) x "
                + "", AuditBean.class);
        Map<String, AuditBean> cuidAndDataFromAuditDatabaseMap = new HashMap<String, AuditBean>();
        for (AuditBean a : dataFromAuditDatabase) {
            cuidAndDataFromAuditDatabaseMap.put(a.cuid, a);
        }

        for (AuditBean ab : auditBeans) {
            AuditBean aud = cuidAndDataFromAuditDatabaseMap.get(ab.cuid);
            if (aud != null) {
                ab.refreshCnt = aud.refreshCnt;
                ab.refreshLast = aud.refreshLast;
                ab.viewCnt = aud.viewCnt;
                ab.viewLast = aud.viewLast;
                ab.userRefreshLast = aud.userRefreshLast;
                ab.userViewLast = aud.userViewLast;
            }
        }
        return auditBeans;
    }
}
