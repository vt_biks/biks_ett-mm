/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons.tags;

import java.util.Collection;

/**
 *
 * @author wezyr
 */
public interface ITagLibrary<T> extends ITagStructureOracle {
    public T createTag(T parentTag, TagData tagData);
    public void setSubTags(T tag, Collection<T> subTags);
}
