/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.IGridNodeAwareBeanPropsExtractor;
import pl.fovis.common.VoteBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author beata
 */
public class MyBIKSVoteTab extends MyBiksTabWithExtractor<VoteBean> {

    @Override
    public String getCaption() {
        return I18n.mojeOceny.get() /* I18N:  */;
    }

    @Override
    public String getButtonStyle() {
        return "labelLink-vote";
    }

    @Override
    protected IGridNodeAwareBeanPropsExtractor<VoteBean> getExtractor() {
        return new VoteBeanExtractor();
    }

    @Override
    protected Request callServiceToGetData(AsyncCallback<List<VoteBean>> callback) {
        return BIKClientSingletons.getService().getVotesByUser(callback);
    }

    @Override
    protected String setTextWhenListEmpty() {
        return I18n.brakOcen.get() /* I18N:  */;
    }
}
