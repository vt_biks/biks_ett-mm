/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.popupmenu;

import simplelib.IContinuation;

/**
 *
 * @author tflorczak
 */
public abstract class FoxyPopUpMenu<CTX> implements IFoxyPopUpMenu<CTX> {

    protected PopupMenuDisplayer popMenu = new PopupMenuDisplayer();
    protected CTX contextObject;
    protected int clientX;
    protected int clientY;

    public FoxyPopUpMenu(CTX contextObject, int clientX, int clientY) {
        this.contextObject = contextObject;
        this.clientX = clientX;
        this.clientY = clientY;
    }

    public void createAndShowPopup() {
        addActions();
        popMenu.showAtPosition(clientX, clientY);
    }

    @Override
    public void addAction(final IPopupMenuItemAction<CTX> act) {
        act.setup(contextObject);

        if (!act.isEnabled()) {
            return;
        }

        IContinuation execAction = new IContinuation() {
            @Override
            public void doIt() {
                act.execute();
            }
        };
        String css = act.getButtonCss();
        popMenu.addMenuItem(act.getCaption(), execAction, css);
    }

    @Override
    public void addSeparator() {
        popMenu.addMenuSeparator();
    }

    protected abstract void addActions();

    @Override
    public CTX getContextObject() {
        return contextObject;
    }

    public static <CTX> void createAndShow(CTX contextObject, int clientX, int clientY, final IPopUpMenuBuilder<CTX> menuBuilder) {
        new FoxyPopUpMenu<CTX>(contextObject, clientX, clientY) {
            @Override
            protected void addActions() {
                menuBuilder.addActions(this);
            }
        }.createAndShowPopup();
    }
}
