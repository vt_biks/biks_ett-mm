﻿exec sp_check_version '1.0.97.1';
go

update bik_node_kind set caption='Folder artykułów',icon_name='articleFolder',code='ArticleFolder',tree_kind='Articles'
where code='CategoriesOfQuestions'
update bik_node_kind set caption='Artykuł',icon_name='article',code='Article',tree_kind='Articles'
where code='Question'
update bik_tree set code='Articles' ,tree_kind='Articles' ,name='Artykuły' where code='FAQ'
go
-------------------------------------------------------------------------------------------
delete from bik_node where tree_id = dbo.fn_tree_id_by_code('TreeOfTrees')
go

exec sp_add_menu_node null, 'Mój BIKS', '#MyBIKS'
exec sp_add_menu_node null, 'Biblioteka', 'metadata'
exec sp_add_menu_node 'metadata', 'SAP BO', 'sapbo'
exec sp_add_menu_node 'sapbo', '@', '$Reports'
exec sp_add_menu_node 'sapbo', '@', '$ObjectUniverses'
exec sp_add_menu_node 'sapbo', '@', '$Connections'
exec sp_add_menu_node 'metadata', '@', '$Teradata'
exec sp_add_menu_node null, '@', '$Glossary'
exec sp_add_menu_node null, 'Kategoryzacje', '@Taxonomy'
exec sp_add_menu_node null, 'Dokumenty', '$Documents'
exec sp_add_menu_node null, 'Blogi', '$Blogs'
exec sp_add_menu_node null, 'Społeczność BI', '$Users'
exec sp_add_menu_node null, 'Baza Wiedzy', '$Articles'
exec sp_add_menu_node null, 'Admin', '#Admin'
exec sp_add_menu_node null, 'Szukaj', '#Search'

exec sp_add_menu_node '$Teradata', '@', '&TeradataOwner'
exec sp_add_menu_node '&TeradataOwner', '@', '&TeradataSchema'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataView'
exec sp_add_menu_node '&TeradataView', '@', '&TeradataColumn'
exec sp_add_menu_node '@Taxonomy', '@', '&TaxonomyEntity'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataTable'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumn'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumnPK'
exec sp_add_menu_node '&TeradataTable', '@', '&TeradataColumnIDX'
exec sp_add_menu_node '&TeradataSchema', '@', '&TeradataProcedure'
exec sp_add_menu_node '$Reports', '@', '&ReportFolder'
exec sp_add_menu_node '&ReportFolder', '@', '&Webi'
exec sp_add_menu_node '&ReportFolder', '@', '&FullClient'
exec sp_add_menu_node '&Webi', '@', '&ReportQuery'
exec sp_add_menu_node '&ReportQuery', '@', '&Dimension$Reports'
exec sp_add_menu_node '&ReportQuery', '@', '&Measure'
exec sp_add_menu_node '&ReportQuery', '@', '&Detail'
exec sp_add_menu_node '&ReportFolder', '@', '&Flash'
exec sp_add_menu_node '&ReportFolder', '@', '&Excel'
exec sp_add_menu_node '&ReportFolder', '@', '&Hyperlink'
exec sp_add_menu_node '&ReportFolder', '@', '&Powerpoint'
exec sp_add_menu_node '&ReportFolder', '@', '&Pdf'
exec sp_add_menu_node '&ReportFolder', '@', '&CrystalReport'
exec sp_add_menu_node '$ObjectUniverses', '@', '&UniversesFolder'
exec sp_add_menu_node '&UniversesFolder', '@', '&Universe'

exec sp_add_menu_node '&Universe', '@', '&UniverseDerivedTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseAliasTable'
exec sp_add_menu_node '&Universe', '@', '&UniverseTable'

exec sp_add_menu_node '&Universe', '@', '&UniverseClass'
exec sp_add_menu_node '&UniverseClass', '@', '&Dimension'
exec sp_add_menu_node '&UniverseClass', '@', '&Measure'
exec sp_add_menu_node '&Dimension', '@', '&Detail'
exec sp_add_menu_node '&UniverseClass', '@', '&Filter'

exec sp_add_menu_node '$Connections', '@', '&ConnectionNetworkFolder'
exec sp_add_menu_node '&ConnectionNetworkFolder', '@', '&ConnectionEngineFolder'
exec sp_add_menu_node '&ConnectionEngineFolder', '@', '&DataConnection'

exec sp_add_menu_node '$Documents', '@', '&DocumentsFolder'
exec sp_add_menu_node '&DocumentsFolder', '@', '&Document'

exec sp_add_menu_node '$Users', '@', '&UsersGroup'
exec sp_add_menu_node '&UsersGroup', '@', '&User'
exec sp_add_menu_node '$Blogs', '@', '&Blog'

exec sp_add_menu_node '$Articles', '@', '&ArticleFolder'
exec sp_add_menu_node '&ArticleFolder', '@', '&Article'

exec sp_add_menu_node '$Glossary', '@', '&Keyword'

exec sp_add_menu_node '$Glossary', '@', '&GlossaryCategory'

exec sp_add_menu_node '&GlossaryCategory', '@', '&Glossary'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryNotCorpo'
exec sp_add_menu_node '&GlossaryCategory', '@', '&GlossaryVersion'

exec sp_add_menu_node '&Blog', '@', '&BlogEntry'
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
exec sp_node_init_branch_id @tree_id, null
go


------------------------------------
------------------------------------


exec sp_update_version '1.0.97.1', '1.0.97.2';
go
