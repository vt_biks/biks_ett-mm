/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.NodeRefreshingCallback;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public class EditCrrLabelsDialog extends BaseActionOrCancelDialog {

    protected Set<String> allCrrLabels;
    protected int nodeId;
    protected String nodeCrrLabels;
//    protected List<CheckBox> checkboxes;
    protected Set<String> selectedLabels = new HashSet<String>();

    @Override
    protected String getDialogCaption() {
        return I18n.edycjaObszarowUprawnien.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {
        Map<String, Boolean> currLabs = new HashMap<String, Boolean>();

        Set<String> currLabsWithInheritanceInfo = BaseUtils.splitUniqueBySep(nodeCrrLabels, ",", true);

        for (String lab : currLabsWithInheritanceInfo) {
            boolean isInherited = BaseUtils.strHasSuffix(lab, "*", false);

            if (isInherited) {
                lab = BaseUtils.dropOptionalSuffix(lab, "*");
            }

            currLabs.put(lab, isInherited);
        }

        for (final String lab : allCrrLabels) {
            final CheckBox cb = new CheckBox(lab);
            final boolean isCurrent = currLabs.containsKey(lab);
            cb.setValue(isCurrent, false);
            if (isCurrent) {
                selectedLabels.add(lab);
            }
            cb.setEnabled(!isCurrent || !currLabs.get(lab));
            cb.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    BaseUtils.setAddOrRemoveItem(selectedLabels, lab, cb.getValue());
                }
            });
            main.add(cb);
        }
    }

    @Override
    protected void doAction() {
        BIKClientSingletons.getService().setCrrLabelsOnNode(selectedLabels, nodeId, new NodeRefreshingCallback<Void>(nodeId, I18n.zmienionoObszaryUprawnien.get()));
    }

    public void buildAndShowDialog(Set<String> allCrrLabels, int nodeId, String nodeCrrLabels) {
        this.allCrrLabels = allCrrLabels;
        this.nodeId = nodeId;
        this.nodeCrrLabels = nodeCrrLabels;

        buildAndShowDialog();
    }
}
