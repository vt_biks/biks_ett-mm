/*
 * SimilarityContainer2.java
 *
 * Created on 31 sierpie� 2007, 19:31
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib.similarity;

import simplelib.LameBag;
import commonlib.LameUtils;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class SimilarityContainer3 implements SimilarityContainerIntf {
    
    private static Locale PolishLocale = new Locale("pl");
    
    private LameBag<String> exactWords = new LameBag<String>();
    private Map<Integer, Map<String, List<String>>> partialWordsCluster;
    //private Map<String, List<String>> partialWords;
    private double minSimilarity;
    
    public SimilarityContainer3(double minSimilarity) {
        this.minSimilarity = minSimilarity;
        //this.partialWords = new HashMap();//[partCount]; //HashMap<String, List<String>>
        this.partialWordsCluster = new HashMap<Integer, Map<String, List<String>>>();
    }
    
    protected String normalizeWord(String word) {
        return word.toLowerCase(PolishLocale);
    }
    
    protected String makePartial(String word, int partNo, int partNo2) {
        char buff[] = new char[word.length()];
        int outPos = 0;
        
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            if (i != partNo && i != partNo2)
                buff[outPos++] = c;
        }
        
        String res = new String(buff, 0, outPos);
        //System.out.println("makePartial: word=" + word + ", partNo=" + partNo + ", res=" + res);
        return res;
    }

    @SuppressWarnings("unchecked")
    public static <IM extends Map<String, C>, C extends Collection<String>> void addToCollectingMapOfMap(Map<Integer, IM> map, Integer k, String ik, String iv) {    
        IM im = map.get(k);
        if (im == null) {
            im = (IM) new HashMap();
            map.put(k, im);
        }
        LameUtils.addToCollectingMap(im, ik, iv);
    }
    
    public void add(String word) {
        String normalizedWord = normalizeWord(word);
        
        if (exactWords.addEx(word) > 1)
        //if (exactWords.contains(word))
            return;
        //exactWords.add(normalizedWord);
        
//        Map<String, List<String>>[] partialWords = partialWordsMap.get(normalizedWord.length());
//        if (partialWords == null) {
//            partialWords = newPartialWords();
//            partialWordsMap.put(normalizedWord.length(), partialWords);
//        }
        
        //LameUtils.addToCollectingMap(partialWords, normalizedWord, normalizedWord);
        //Map<Integer, Map<String, List<String>>>
        addToCollectingMapOfMap(partialWordsCluster, new Integer(-1), normalizedWord, normalizedWord);
        if (normalizedWord.length() > 1) {
            for (int i = 0; i < normalizedWord.length(); i++) {
                String partialWord = makePartial(normalizedWord, i, -1);
                //System.out.println("normalized: " + normalizedWord + ", partial: " + partialWord);
                addToCollectingMapOfMap(partialWordsCluster, i, partialWord, normalizedWord);
//            List<String> fullWords = partialWords[i].get(partialWord);
//            if (fullWords == null) {
//                fullWords = new ArrayList<String>();
//                partialWords[i].put(partialWord, fullWords);
//            }
//            fullWords.add(normalizedWord);
            }
        }
    }
    
    public int getWordCount() {
        return exactWords.size();
    }
    
    public double calcSimilarity(String w1, String w2) {
        //return w1.equals(w2) ? 100.0 : 95.0;
        
        int cnt1 = exactWords.getCount(w1);
        int cnt2 = exactWords.getCount(w2);
        double penalty = 1.0;
        
        if (cnt1 + cnt2 > 60)
            penalty = 1.3;
        else if (cnt1 + cnt2 > 30)
            penalty = 1.15;
        //System.out.println("w1=" + w1 + ", cnt1=" + cnt1 + ", w2=" + w2 + ", cnt2=" + cnt2);
        
        int maxL = Math.max(w1.length(), w2.length());
        int dist = StringSimilarityUtils.calcDamerauLevenshteinDistance(w1, w2);
        if (w1.charAt(0) != w2.charAt(0))
            dist += 1;
        if (dist > 1) 
            dist = dist * 2 - 1;
        return 100 - dist * (maxL > 4 ? 3 : maxL == 4 ? 4 : maxL == 3 ? 5 : 6) *
                penalty;
        
        //return (maxL - dist) * 100.0 / maxL;
        
//        return //100.0;
//                20.0 * LetterTupleSimilarity.calcRankForStrings(w1, w2, 2, false) +
//                30.0 * LetterTupleSimilarity.calcRankForStrings(w1, w2, 2, true) +
//                50.0 * LetterTupleSimilarity.calcRankForStrings(w1, w2, 1, false);
    }
    
    //protected 
    
    int calcCnt;
        
    public Map<String, Double> findSimilar(String word) {
        String normalizedWord = normalizeWord(word);
        
        calcCnt = 0;
                
        Map<String, Double> candidates = new HashMap<String, Double>();
        
        if (normalizedWord.length() > 1) {
            for (int i = 0; i < normalizedWord.length(); i++) {
                findSingle(normalizedWord, candidates, i);

//                String partialWord = makePartial(normalizedWord, i, -1);
//                Map<String, List<String>> partialWords = partialWordsCluster.get(i);
//                List<String> fullWords = partialWords.get(partialWord);
//                if (fullWords != null) {
//                    for (String w : fullWords) {
//                        calcCnt++;
//                        double similarity = calcSimilarity(normalizedWord, w);
//                        if (similarity >= minSimilarity)
//                            candidates.put(w, similarity);
//                    }
//                }
            }
        }

        findSingle(normalizedWord, candidates, -1);
                
        //System.out.println("findSimilar: " + word + ", calcCnt=" + calcCnt + ", res=" + candidates);
        return candidates;
    }

    private void findSingle(final String normalizedWord, final Map<String, Double> candidates,
            int clusterIdx) {
        int minIdx, maxIdx;
        String partialWord;
        
        if (clusterIdx == -1) {
            // clusterIdx == -1 -> szukamy pod -1 i od 0 do nw.length - 1
            minIdx = 0;
            maxIdx = normalizedWord.length();
            partialWord = normalizedWord;
        } else {
            // clusterIdx >= 0 -> szukamy pod -1 i od clusterIdx-1 do clusterIdx+1
            minIdx = //clusterIdx; //
                    clusterIdx > 0 ? clusterIdx - 1 : 0;
            maxIdx = //clusterIdx; //
                    clusterIdx + 1;
            partialWord = makePartial(normalizedWord, clusterIdx, -1);
        }
        
        findPrimitive(normalizedWord, partialWord, candidates, -1);
        for (int i = minIdx; i <= maxIdx; i++)
            findPrimitive(normalizedWord, partialWord, candidates, i);
    }
    
    private void findPrimitive(final String normalizedWord, String partialWord, final Map<String, Double> candidates,
            int clusterIdx) {
        Map<String, List<String>> partialWords = partialWordsCluster.get(clusterIdx);
        if (partialWords == null)
            return;
        List<String> fullWords = partialWords.get(partialWord);
        if (fullWords != null) {
            for (String w : fullWords) {
                calcCnt++;
                double similarity = calcSimilarity(normalizedWord, w);
                if (similarity >= minSimilarity)
                    candidates.put(w, similarity);
            }
        }
    }
}
