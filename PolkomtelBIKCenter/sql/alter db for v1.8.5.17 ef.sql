exec sp_check_version '1.8.5.17';
GO

UPDATE bik_translation
SET txt = 'U�ytkownik niepubliczny'
WHERE txt = 'U�ytkownik nie publiczny'

IF NOT EXISTS (
		SELECT 1
		FROM bik_attr_def
		WHERE name = 'metaBiks.Przypisana rola'
		)
	INSERT INTO bik_attr_def (
		name
		,attr_category_id
		,is_deleted
		,is_built_in
		,type_attr
		,display_as_number
		,visual_order
		,in_drools
		)
	VALUES (
		'metaBiks.Przypisana rola'
		,(
			SELECT id
			FROM bik_attr_category
			WHERE name = 'metaBiks'
			)
		,0
		,0
		,'ansiiText'
		,0
		,0
		,0
		)

IF NOT EXISTS (
		SELECT 1
		FROM bik_attribute
		WHERE node_kind_id = (
				SELECT id
				FROM bik_node_kind
				WHERE code = 'metaBiksNodeKind'
				)
		)
	INSERT INTO bik_attribute (
		node_kind_id
		,attr_def_id
		,is_deleted
		,is_required
		,is_visible
		,is_empty
		,is_public
		)
	VALUES (
		(
			SELECT id
			FROM bik_node_kind
			WHERE code = 'metaBiksNodeKind'
			)
		,(
			SELECT id
			FROM bik_attr_def
			WHERE name = 'metaBiks.Przypisana rola'
			)
		,0
		,0
		,1
		,1
		,1
		)

IF NOT EXISTS (
		SELECT 1
		FROM bik_attribute
		WHERE node_kind_id = (
				SELECT id
				FROM bik_node_kind
				WHERE code = 'metaBiksBuiltInNodeKind'
				)
		)
	INSERT INTO bik_attribute (
		node_kind_id
		,attr_def_id
		,is_deleted
		,is_required
		,is_visible
		,is_empty
		,is_public
		)
	VALUES (
		(
			SELECT id
			FROM bik_node_kind
			WHERE code = 'metaBiksBuiltInNodeKind'
			)
		,(
			SELECT id
			FROM bik_attr_def
			WHERE name = 'metaBiks.Przypisana rola'
			)
		,0
		,0
		,1
		,1
		,1
		)

IF NOT EXISTS (
		SELECT 1
		FROM bik_app_prop
		WHERE name = 'newRightsSys'
		)
	INSERT INTO bik_app_prop (
		name
		,val
		,is_editable
		)
	VALUES (
		'newRightsSys'
		,'false'
		,1
		)

EXEC sp_add_menu_node 'admin:dict'
	,'Konfiguracja uprawnie�'
	,'#admin:dict:konfUpr'

-- nowy system uprawnie� nie zawiera drzewa grup ale domy�lnie mamy wy��czony nowy system
UPDATE bik_node
SET is_deleted = 0
WHERE name = 'Grupy u�ytkownik�w'
	AND obj_id = '#admin:dict:sysGroupUsers'

UPDATE bik_node
SET is_deleted = 1
WHERE name = 'Konfiguracja uprawnie�'
	AND obj_id = '#admin:dict:konfUpr'

ALTER TABLE bik_custom_right_role_user_entry

ALTER COLUMN role_id INT NULL

IF NOT EXISTS (
		SELECT 1
		FROM sys.columns
		WHERE Name = N'description'
			AND Object_ID = Object_ID(N'dbo.bik_system_user_group')
		)
	ALTER TABLE bik_system_user_group ADD description VARCHAR(max)

IF NOT EXISTS (
		SELECT 1
		FROM bik_node_action
		WHERE code = 'GrantRightsNewRightsSys'
		)
BEGIN
	INSERT INTO bik_node_action (code)
	VALUES ('GrantRightsNewRightsSys')

	DECLARE @actionId INT;

	SELECT @actionId = id
	FROM bik_node_action
	WHERE code = 'GrantRightsNewRightsSys'

	INSERT INTO bik_node_action_in_custom_right_role (
		action_id
		,role_id
		)
	SELECT DISTINCT @actionId
		,role_id
	FROM bik_node_action_in_custom_right_role
	WHERE action_id = (
			SELECT id
			FROM bik_node_action
			WHERE code = 'AddAnyNode'
			)
END

--alter procedure kt�ry daj mo�liwo�� mie� w polu role_id w bik_custom_right_role_user_entry nulle
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants] @diag_level INT = 0
AS
BEGIN
	SET NOCOUNT ON

	--declare @diag_level int = 1
	DECLARE @log_msg VARCHAR(max);

	IF @diag_level > 0
	BEGIN
		SET @log_msg = cast(sysdatetime() AS VARCHAR(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: start!';

		EXEC sp_log_msg @log_msg;
	END;

	IF coalesce((
				SELECT val
				FROM bik_app_prop
				WHERE name = 'customRightRolesTreeSelector'
				), '') = ''
	BEGIN
		IF @diag_level > 0
		BEGIN
			SET @log_msg = 'bik_app_prop[customRightRolesTreeSelector] is empty, nothing to do';

			EXEC sp_log_msg @log_msg;
		END;

		RETURN;
	END;

	-- 1. 
	-- zak�adamy, �e zosta�y zidentyfikowane silnie powi�zane komponenty (strongly connected components, scc)
	-- i poprawnie wyliczone jest bik_system_user_group.scc_root_id
	-- je�eli dana grupa nie tworzy cyklu z innymi, to jest ona jedynym elementem swojego scc, wi�c 
	-- bik_system_user_group.scc_root_id = bik_system_user_group.id
	SELECT DISTINCT scc_root_id AS scc_id
	INTO #rcrrabg_scc
	FROM bik_system_user_group
	WHERE is_deleted = 0;

	SELECT DISTINCT pg.scc_root_id AS parent_scc_id
		,cg.scc_root_id AS child_scc_id
	INTO #rcrrabg_rel
	FROM bik_system_group_in_group gig
	INNER JOIN bik_system_user_group pg ON gig.group_id = pg.id
	INNER JOIN bik_system_user_group cg ON gig.member_group_id = cg.id
	WHERE gig.is_deleted = 0
		AND pg.is_deleted = 0
		AND cg.is_deleted = 0
		AND pg.scc_root_id <> cg.scc_root_id;

	TRUNCATE TABLE bik_custom_right_role_action_branch_grant;

	-- specjalna akcja widoczno�ci w drzewie na zak�adkach - ShowInTree
	DECLARE @show_in_tree_act_id INT = (
			SELECT id
			FROM bik_node_action
			WHERE code = 'ShowInTree'
			);

	IF EXISTS (
			SELECT 1
			FROM tempdb..sysobjects
			WHERE id = object_id('tempdb..##rcrrabg_trees_for_roles_0')
			)
		DROP TABLE ##rcrrabg_trees_for_roles_0;

	CREATE TABLE ##rcrrabg_trees_for_roles_0 (
		role_id INT NOT NULL
		,tree_id INT NOT NULL
		,UNIQUE (
			role_id
			,tree_id
			)
		);

	/*
  declare ##rcrrabg_trees_for_roles_0 table (role_id int not null, tree_id int not null, unique (role_id, tree_id));
  */
	INSERT INTO ##rcrrabg_trees_for_roles_0 (
		role_id
		,tree_id
		)
	SELECT DISTINCT crr.id AS role_id
		,t.id AS tree_id
	FROM bik_custom_right_role crr
	CROSS APPLY dbo.fn_split_by_sep(crr.tree_selector, ',', 7) x
	INNER JOIN bik_tree t ON substring(x.str, 1, 1) = '@'
		AND t.tree_kind = substring(x.str, 2, len(x.str))
		OR substring(x.str, 1, 1) <> '@'
		AND t.code = x.str;

	-- select * from bik_custom_right_role_user_entry
	IF @diag_level > 1
		SELECT *
		FROM ##rcrrabg_trees_for_roles_0;

	DECLARE @right_for_group_flat TABLE (
		role_id INT
		,group_id INT NOT NULL
		,tree_id INT NULL
		,node_id INT NULL
		,UNIQUE (
			group_id
			,role_id
			,tree_id
			,node_id
			)
		);
	DECLARE @groups_to_process TABLE (group_id INT NOT NULL PRIMARY KEY);
	DECLARE @processed_groups TABLE (group_id INT NOT NULL PRIMARY KEY);
	DECLARE @unprocessed_groups TABLE (group_id INT NOT NULL PRIMARY KEY);

	INSERT INTO @unprocessed_groups (group_id) --select id from bik_system_user_group;
	SELECT scc_id
	FROM #rcrrabg_scc;

	INSERT INTO @groups_to_process (group_id)
	/*select id
  from bik_system_user_group
  where id not in (select member_group_id from bik_system_group_in_group);*/
	SELECT scc_id
	FROM #rcrrabg_scc
	WHERE scc_id NOT IN (
			SELECT child_scc_id
			FROM #rcrrabg_rel
			);

	SELECT scc_id
		,scc_id AS ancestor_scc_id
	INTO #rcrrabg_ancestors_flat
	FROM #rcrrabg_scc;

	WHILE EXISTS (
			SELECT 1
			FROM @groups_to_process
			)
	BEGIN
		IF @diag_level > 0
		BEGIN
			DECLARE @gtpstr VARCHAR(max) = NULL;

			SELECT @gtpstr = CASE 
					WHEN @gtpstr IS NULL
						THEN ''
					ELSE @gtpstr + ','
					END + cast(group_id AS VARCHAR(20))
			FROM @groups_to_process;

			SET @log_msg = 'groups to process: ' + @gtpstr;

			EXEC sp_log_msg @log_msg;
		END;

		INSERT INTO #rcrrabg_ancestors_flat (
			scc_id
			,ancestor_scc_id
			)
		SELECT DISTINCT gtp.group_id
			,af.ancestor_scc_id
		FROM @groups_to_process gtp
		INNER JOIN #rcrrabg_rel r ON gtp.group_id = r.child_scc_id
		INNER JOIN #rcrrabg_ancestors_flat af ON r.parent_scc_id = af.scc_id;

		INSERT INTO @processed_groups (group_id)
		SELECT group_id
		FROM @groups_to_process;

		DELETE
		FROM @unprocessed_groups
		WHERE group_id IN (
				SELECT group_id
				FROM @groups_to_process
				);

		DELETE
		FROM @groups_to_process;

		INSERT INTO @groups_to_process (group_id)
		SELECT group_id
		FROM @unprocessed_groups
		WHERE group_id NOT IN
			--(select member_group_id from bik_system_group_in_group where group_id in (select group_id from @unprocessed_groups));
			(
				SELECT child_scc_id
				FROM #rcrrabg_rel
				WHERE parent_scc_id IN (
						SELECT group_id
						FROM @unprocessed_groups
						)
				);
	END;

	IF @diag_level > 0
		AND EXISTS (
			SELECT 1
			FROM @unprocessed_groups
			)
	BEGIN
		DECLARE @ugstr VARCHAR(max) = NULL;

		SELECT @ugstr = CASE 
				WHEN @ugstr IS NULL
					THEN ''
				ELSE @ugstr + ','
				END + cast(group_id AS VARCHAR(20))
		FROM @unprocessed_groups;

		SET @log_msg = 'nothing new to process, but there are unprocessed groups: ' + @ugstr;

		EXEC sp_log_msg @log_msg;
	END;

	-- poni�sze nie wy�apuje sytuacji gdy istnieje wpis nadrz�dny i podrz�dny 
	-- (w sensie w�z��w w tym samym drzewie, dla tej samej roli) - wrzucane s� oba wpisy
	INSERT INTO @right_for_group_flat (
		role_id
		,group_id
		,tree_id
		,node_id
		)
	SELECT DISTINCT crrue.role_id
		,cg.id
		,crrue.tree_id
		,crrue.node_id
	FROM #rcrrabg_ancestors_flat af
	INNER JOIN bik_system_user_group ag ON af.ancestor_scc_id = ag.scc_root_id
	INNER JOIN bik_custom_right_role_user_entry crrue ON crrue.group_id = ag.id
	INNER JOIN bik_system_user_group cg ON af.scc_id = cg.scc_root_id
	WHERE ag.is_deleted = 0;

	IF EXISTS (
			SELECT 1
			FROM tempdb..sysobjects
			WHERE id = object_id('tempdb..##rcrrabg_right_for_user_flat')
			)
		DROP TABLE ##rcrrabg_right_for_user_flat;

	CREATE TABLE ##rcrrabg_right_for_user_flat (
		role_id INT
		,user_id INT NOT NULL
		,tree_id INT NULL
		,node_id INT NULL
		,UNIQUE (
			user_id
			,role_id
			,tree_id
			,node_id
			)
		);

	/*
  declare ##rcrrabg_right_for_user_flat table (role_id int not null, user_id int not null, tree_id int null, node_id int null,
    unique(user_id, role_id, tree_id, node_id));
  */
	IF (
			SELECT val
			FROM bik_app_prop
			WHERE name = 'newRightsSys'
			) = 'true'
		INSERT INTO ##rcrrabg_right_for_user_flat (
			user_id
			,role_id
			,tree_id
			,node_id
			)
		SELECT DISTINCT user_id
			,role_id
			,tree_id
			,node_id
		FROM (
			SELECT uig.user_id
				,rfgf.role_id
				,rfgf.tree_id
				,rfgf.node_id
			FROM bik_system_user_in_group uig
			INNER JOIN @right_for_group_flat rfgf ON uig.group_id = rfgf.group_id
				AND uig.is_deleted = 0
			
			UNION
			
			SELECT user_id
				,(
					CASE 
						WHEN bc.group_id IS NULL
							THEN crrue.role_id
						ELSE bc.role_id
						END
					) 'role_id'
				,tree_id
				,node_id
			FROM bik_custom_right_role_user_entry crrue
			LEFT JOIN (
				SELECT role_id
					,group_id
				FROM bik_custom_right_role_user_entry
				WHERE role_id IS NOT NULL
				) bc ON crrue.group_id = bc.group_id
			WHERE crrue.user_id IS NOT NULL
			) x;
	ELSE
		INSERT INTO ##rcrrabg_right_for_user_flat (
			user_id
			,role_id
			,tree_id
			,node_id
			)
		SELECT DISTINCT user_id
			,role_id
			,tree_id
			,node_id
		FROM (
			SELECT uig.user_id
				,rfgf.role_id
				,rfgf.tree_id
				,rfgf.node_id
			FROM bik_system_user_in_group uig
			INNER JOIN @right_for_group_flat rfgf ON uig.group_id = rfgf.group_id
				AND uig.is_deleted = 0
			
			UNION
			
			SELECT user_id
				,role_id
				,tree_id
				,node_id
			FROM bik_custom_right_role_user_entry crrue
			WHERE user_id IS NOT NULL
			) x;

	IF @diag_level > 1
		SELECT *
		FROM bik_custom_right_role_user_entry;

	IF @diag_level > 1
		SELECT *
		FROM --bik_custom_right_role_user_entry 
			##rcrrabg_right_for_user_flat crrue
		LEFT JOIN ##rcrrabg_trees_for_roles_0 rts ON crrue.tree_id IS NULL
			AND rts.role_id = crrue.role_id;

	IF @diag_level > 1
	BEGIN
		SET @log_msg = cast(sysdatetime() AS VARCHAR(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: before semi-final insert';

		EXEC sp_log_msg @log_msg;
	END;

	INSERT INTO bik_custom_right_role_action_branch_grant (
		action_id
		,user_id
		,tree_id
		,branch_ids
		)
	SELECT DISTINCT @show_in_tree_act_id
		,crrue.user_id
		,coalesce(crrue.tree_id, rts.tree_id) AS tree_id
		,--crrue.node_id,
		coalesce(n.branch_ids, '') AS branch_ids
	FROM --bik_custom_right_role_user_entry 
		##rcrrabg_right_for_user_flat crrue
	LEFT JOIN ##rcrrabg_trees_for_roles_0 rts ON crrue.tree_id IS NULL
		AND rts.role_id = crrue.role_id
	LEFT JOIN bik_node n ON n.id = crrue.node_id
	WHERE NOT EXISTS -- nie dorzucamy wpisu, je�eli wyst�puje dla niego wpis nadrz�dny (na wy�szym poziomie w drzewie)
		(
			SELECT --
				1
			--crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
			FROM --bik_custom_right_role_user_entry 
				##rcrrabg_right_for_user_flat crrue2
			LEFT JOIN ##rcrrabg_trees_for_roles_0 rts2 ON crrue2.tree_id IS NULL
				AND rts2.role_id = crrue2.role_id
			LEFT JOIN bik_node n2 ON n2.id = crrue2.node_id
			WHERE crrue2.user_id = crrue.user_id
				AND coalesce(n.branch_ids, '') LIKE coalesce(n2.branch_ids, '') + '_%'
				AND coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
				--and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
			);

	IF @diag_level > 1
	BEGIN
		SET @log_msg = cast(sysdatetime() AS VARCHAR(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: before final insert';

		EXEC sp_log_msg @log_msg;
	END;

	INSERT INTO bik_custom_right_role_action_branch_grant (
		action_id
		,user_id
		,tree_id
		,branch_ids
		)
	SELECT DISTINCT naicrr.action_id
		,crrue.user_id
		,coalesce(crrue.tree_id, rts.tree_id) AS tree_id
		,--crrue.node_id,
		coalesce(n.branch_ids, '') AS branch_ids
	FROM --bik_custom_right_role_user_entry 
		##rcrrabg_right_for_user_flat crrue
	INNER JOIN bik_node_action_in_custom_right_role naicrr ON crrue.role_id = naicrr.role_id
	LEFT JOIN ##rcrrabg_trees_for_roles_0 rts ON crrue.tree_id IS NULL
		AND rts.role_id = crrue.role_id
	LEFT JOIN bik_node n ON n.id = crrue.node_id
	WHERE naicrr.action_id <> @show_in_tree_act_id
		AND NOT EXISTS -- nie dorzucamy wpisu, je�eli wyst�puje dla niego wpis nadrz�dny (na wy�szym poziomie w drzewie)
		(
			SELECT --
				1
			--crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
			FROM --bik_custom_right_role_user_entry 
				##rcrrabg_right_for_user_flat crrue2
			INNER JOIN bik_node_action_in_custom_right_role naicrr2 ON crrue2.role_id = naicrr2.role_id
			LEFT JOIN ##rcrrabg_trees_for_roles_0 rts2 ON crrue2.tree_id IS NULL
				AND rts2.role_id = crrue2.role_id
			LEFT JOIN bik_node n2 ON n2.id = crrue2.node_id
			WHERE naicrr2.action_id = naicrr.action_id
				AND crrue2.user_id = crrue.user_id
				AND coalesce(n.branch_ids, '') LIKE coalesce(n2.branch_ids, '') + '_%'
				AND coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
				--and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
			);

	IF @diag_level > 0
	BEGIN
		SET @log_msg = cast(sysdatetime() AS VARCHAR(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: end';

		EXEC sp_log_msg @log_msg;
	END;
END;
GO

exec sp_update_version '1.8.5.17'
	,'1.8.5.18';
GO


