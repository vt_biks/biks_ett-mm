﻿exec sp_check_version '1.1.0.4';
go
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- poprzednia wersja w pliku: "alter db for v1.0.86.6 ww.sql"
if  exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[fn_bik_node_name_chunk_search_sql]') and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	drop function [dbo].[fn_bik_node_name_chunk_search_sql]
go
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
create function [dbo].[fn_bik_node_name_chunk_search_sql] (@substr varchar(1000), @tree_ids_filter varchar(8000))--@opt_tree_id int)
returns varchar(8000)
WITH EXECUTE AS CALLER
as
begin
  declare @sql varchar(8000) = 'select node_id from bik_node_name_chunk
  where (';

  if len(@substr) > 3 begin

  declare @substr_chunks table (chunk_str varchar(3) not null);
  
  insert into @substr_chunks
  select chunk_str from (
  select distinct top 5 chunk_str, len(chunk_Str) as cln from dbo.fn_split_string(@substr, 3) 
  where len(chunk_str) = 3 order by len(chunk_str) desc
  ) x
  
  declare bnncs cursor for select chunk_str from @substr_chunks;
  
  open bnncs;
  
  declare @chunk_str varchar(3);
  
  fetch next from bnncs into @chunk_str
  
  declare @is_first int = 1;
  
  while @@fetch_status = 0 begin
    if @is_first = 0 begin
      set @sql = @sql + ' or ';
    end else begin
      set @is_first = 0;
    end

	set @chunk_str = replace(@chunk_str, '''', '''''');
  
    set @sql = @sql + 'chunk_txt = ''' + @chunk_str + ''''
  
    fetch next from bnncs into @chunk_str
  end;
  end else begin
	set @chunk_str = replace(@substr, '''', '''''');
  
    if len(@chunk_str) < 3 begin
      set @sql = @sql + 'chunk_txt like ''' + @chunk_str + '%'''
    end else begin
      set @sql = @sql + 'chunk_txt = ''' + @chunk_str + ''''
    end;
  end  
  set @sql = @sql + ')'
	/*
	--zmiana
	if @opt_tree_id is not null begin
		set @sql = @sql + ' and tree_id = ' + cast(@opt_tree_id as varchar(20));
	end;
  */
  if @tree_ids_filter is not  null
  begin
	set @sql = @sql + ' and ' + @tree_ids_filter;
  end
  if len(@substr) > 3 begin
  set @sql = @sql + ' group by node_id having count(*) = ' + cast((select count(*) from @substr_chunks) as varchar(10))
  end
  
  return @sql;
end;

go
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- poprzednia wersja w pliku: "alter db for v1.0.86.6 ww.sql"
if exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_filter_bik_nodes]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_filter_bik_nodes]
go
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
create procedure [dbo].[sp_filter_bik_nodes] (@txt varchar(max), @tree_ids_filter varchar(8000) /*@optTreeId int*/, @opt_bik_node_filter varchar(max))
as
begin
  set nocount on
  
  declare @diag_level int = 0
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': start!'
  
declare @sql varchar(8000);

set @sql = 'select id from bik_node where id in (' + dbo.fn_bik_node_name_chunk_search_sql(@txt, @tree_ids_filter)--@optTreeId) 
  + ') and name like ''%' + replace(@txt, '''', '''''') + '%'' and is_deleted = 0' +
  case when @opt_bik_node_filter is null then '' else ' and (' + @opt_bik_node_filter + ')' end +
  case when @tree_ids_filter is null then '' else ' and (' + @tree_ids_filter + ')' end
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': have sql: sql=' + @sql
  print @sql
-- ostatnio wrzucone
declare @t table (id int not null primary key, parent_node_id int null);
-- parent_node_ids ostatnio wrzuconych - te będziemy wrzucać teraz
declare @p table (id int not null primary key);
-- wszystkie
declare @a table (id int not null primary key);

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': before exec sql'

  insert into @p (id)
  exec(@sql)

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': sql executed'
  
  while exists (select 1 from @p) begin
    insert into @a select id from @p;

    delete from @t;

    insert into @t (id, parent_node_id) select id, parent_node_id from bik_node where id in (select id from @p);

    delete from @p;

    insert into @p 
    select distinct parent_node_id 
    from @t 
    where parent_node_id is not null and parent_node_id not in (select id from @a);
  end;

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': after loop'

select bn.*,
    bnk.caption as node_kind_caption,
    bnk.code as node_kind_code,
    bnk.is_folder as is_folder,
    bt.name as tree_name, bt.tree_kind, bt.code as tree_code
    , case when disable_linked_subtree = 0 and exists 
        (select 1 from bik_node where is_deleted = 0 and parent_node_id = coalesce(bn.linked_node_id, bn.id)) then 0 else 1 end as has_no_children
    from bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    inner join bik_tree bt on bn.tree_id = bt.id
    where bn.id in (select id from @a)
    order by bn.name

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': after final select'
end;
go

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- ww: przeindeksowanie do filtrowania artykułów bazy wiedzy
declare @articles_tree_id int = (select id from bik_tree where code = 'Articles')
update bik_node set chunked_ver = chunked_ver + 1 where tree_id = @articles_tree_id
go

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
exec sp_update_version '1.1.0.4', '1.1.0.5';
go
