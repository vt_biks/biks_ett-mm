/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

/**
 *
 * @author bfechner
 */
public interface ITreeBuilder<DN, TN, ID> extends ITreeBroker<DN, ID> {

    public TN createNode(DN n, TN p/*, List<Integer> list*/);
}
