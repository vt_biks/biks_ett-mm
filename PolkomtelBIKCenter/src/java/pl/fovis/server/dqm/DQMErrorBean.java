/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

/**
 *
 * @author tflorczak
 */
public class DQMErrorBean {

    public long requestId;
    public int row;
    public int columnOrdinalPosition;
    public String columnName;
    public String value;

    public DQMErrorBean() {
    }

    public DQMErrorBean(long requestId, int row, int columnOrdinalPosition, String columnName, String value) {
        this.requestId = requestId;
        this.row = row;
        this.columnOrdinalPosition = columnOrdinalPosition;
        this.columnName = columnName;
        this.value = value;
    }
}
