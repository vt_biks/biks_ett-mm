/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueBoxBase;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.AttrCombobox;
import pl.fovis.client.bikwidgets.AttrDateTime;
import pl.fovis.client.bikwidgets.AttrWigetBase;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.JoinedObjBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author efedchenko
 */
public abstract class AbstractAddAnyNodeDialog extends BaseActionOrCancelDialog {

    protected ListBox addNodeListBox = new ListBox();
    protected TreeNodeBean node;
    protected List<String> attrList = new ArrayList<String>();
    protected boolean addingEventDialog;
    protected final HTML imgHtml = new HTML();
    protected boolean atrContainsValues;
    protected boolean isEnabledForEdition = true;
    protected List<AttributeBean> attrListForViewing;
    protected IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>> saveCont;

    protected TextBox tbxName;
    protected List<TreeNodeBean> nodeKinds;
    protected Integer selectedNodeKindId;
    protected Label errorLbl = new Label();
    protected FlexTable requiredAttrFt = new FlexTable();
    protected Map<String, Widget> mapWidget = new HashMap<String, Widget>();
    protected Map<String, List<RadioButton>> mapRbs = new HashMap<String, List<RadioButton>>();

    protected ScrollPanel requiredAttrsSp = new ScrollPanel(requiredAttrFt);
    protected Map<String, String> nodePath2NodeLink = new HashMap<String, String>();
    protected Map<String, List<CheckBox>> mapCbs = new HashMap<String, List<CheckBox>>();
    protected List<AttributeBean> requiredAttrs = new ArrayList<AttributeBean>();
    protected TreeNodeBean parent;
    protected VerticalPanel vp = new VerticalPanel();
    protected Integer nodeKindId;
    protected Integer treeId;
    protected boolean isNodeFromDynamicTree;
    protected FlexTable rbBoxContainer = new FlexTable();
    protected boolean sepLine1Exists;
    protected boolean sepLine2Exists;
    protected Widget separationLine1 = new HTML("<hr color = #8faed6>");
    protected Widget separationLine2 = new HTML("<hr color = #8faed6>");
    public static final int MAX_SCROLLPANEL_HEIGHT = 420;
    protected String defaultValue;
    protected String atrLinValue;
    protected int nameFieldWidth = 235;
    protected String atrControlName = null;

    protected int height = 0;
    protected Map<String, AttributeBean> absMap = new HashMap<String, AttributeBean>();
    protected boolean isExistsTextArea = false;

    @Override
    protected String getActionButtonCaption() {
        return I18n.dodaj.get();
    }

    @Override
    protected void buildMainWidgets() {
        someObjectsAreAvailable();
    }

    public Widget getCaption() {
        Label nameLbl = new Label(I18n.podajNazwe.get());
        nameLbl.setStyleName("addNodeLbl");
        nameLbl.setWidth(getTopLabelWidth());
        return nameLbl;
    }

    public void someObjectsAreAvailable() {
        vp.add(errorLbl);
        errorLbl.setStyleName("testStyle");

        displayAttributes();
        vp.add(rbBoxContainer);

        FlexTable table = new FlexTable();
        table.setCellSpacing(5);
        tbxName = new TextBox();
        tbxName.setWidth(nameFieldWidth + "px");
        tbxName.setStyleName("addNode");
        table.setWidget(0, 0, getCaption());
        table.setWidget(0, 1, tbxName);
        vp.add(table);
        if (!sepLine1Exists) {
            vp.add(separationLine1);
            sepLine1Exists = true;
            separationLine1.setVisible(false);
        } else {
            separationLine1.setVisible(true);
        }
        if (addingEventDialog) {
            rbBoxContainer.setVisible(false);
            addNodeListBox.setSelectedIndex(1);
            if (!sepLine2Exists) {
                vp.add(separationLine2);
                separationLine2.setVisible(true);
                sepLine2Exists = true;
            }
            customOnChangeEvent();
        }
        //vp.add(requiredAttrsSp);
        main.add(vp);
    }

    public void displayAttributes() {
        if (node != null && node.isRegistry) {
            addingEventDialog = true;
        }

        Label nodeType = new Label(I18n.wybierzTypWezla.get());
        nodeType.setStyleName("addNodeLbl");
        nodeType.setWidth(getTopLabelWidth());

        rbBoxContainer.setCellSpacing(5);
        rbBoxContainer.setWidget(0, 0, nodeType);
        rbBoxContainer.setWidget(0, 1, addNodeListBox);
        rbBoxContainer.setWidget(0, 2, imgHtml);
        addNodeListBox.addItem("Proszę wybrać typ węzła");
        if (BaseUtils.isCollectionEmpty(attrListForViewing)) {
            for (final TreeNodeBean nk : nodeKinds) {
                addNodeListBox.addItem(BaseUtils.encodeForHTMLTag(nk.nodeKindCaption));
            }

            addNodeListBox.addChangeHandler(new ChangeHandler() {

                @Override
                public void onChange(ChangeEvent event) {
                    customOnChangeEvent();
                }
            });
        }
        addNodeListBox.setVisibleItemCount(1);
        addNodeListBox.setStyleName("addAttr");
        addNodeListBox.getElement().getFirstChildElement().setAttribute("disabled", "disabled");
    }

    protected void onEnterPressed() {
        if (!isExistsTextArea) {
            super.onEnterPressed();
        }
    }

    public String getTopLabelWidth() {
        String lblWidth = "187px";
        return lblWidth;
    }

    public void setNameFieldWidth(int attrWidth) {
        if (attrWidth > nameFieldWidth) {
            nameFieldWidth = attrWidth;
        }
        tbxName.setWidth(nameFieldWidth + "px");
    }

    public void customOnChangeEvent() {
        if (!BaseUtils.isCollectionEmpty(attrListForViewing)) {
            //selectedNodeKindId = ma nie być null, a tak nie jest potrzebny
            requiredAttrs = attrListForViewing;
            selectedNodeKindId = 0;
        } else {
            selectedNodeKindId = nodeKinds.get(addNodeListBox.getSelectedIndex() - 1).nodeKindId;
            requiredAttrs = nodeKinds.get(addNodeListBox.getSelectedIndex() - 1).requiredAttrs;
            imgHtml.setHTML("<" + "img style='vertical-align:text-top' border='0' src='images" /* I18N: no */ + "/"
                    + nodeKinds.get(addNodeListBox.getSelectedIndex() - 1).iconName + "." + "gif' width='16' " + "height" /* I18N: no */ + "='16' />");
        }
        atrControlName = (selectedNodeKindId == 0 ? null : BaseUtils.safeToStringDef(BIKClientSingletons.getNodeKindById(selectedNodeKindId).attrControlNodeName, null));

        requiredAttrFt.clear();
        mapCbs.clear();
        mapRbs.clear();
        mapWidget.clear();
        requiredAttrFt.removeAllRows();
        absMap.clear();
        separationLine1.setVisible(false);
        if (sepLine2Exists) {
            separationLine2.setVisible(false);
            sepLine2Exists = false;
        }

        if (!BaseUtils.isCollectionEmpty(requiredAttrs)) {

            for (AttributeBean ab : requiredAttrs) {
                absMap.put(ab.atrName, ab);
            }
            refreshAttributeFt();
            requiredAttrFt.setWidth("100%");
            requiredAttrFt.setStyleName("gridJoinedObj");

            vp.setCellWidth(requiredAttrFt, "100%");
            vp.add(requiredAttrsSp);
            requiredAttrsSp.scrollToTop();
            resizeRequiredAttrScrollPanel();
            if (!sepLine2Exists) {
                vp.add(separationLine2);
                separationLine2.setVisible(true);
                sepLine2Exists = true;
            }
            separationLine1.setVisible(true);
            sepLine1Exists = true;

        } else {
            separationLine1.setVisible(false);
            sepLine1Exists = true;
            if (sepLine2Exists) {
                separationLine2.setVisible(false);
            }
            resizeRequiredAttrScrollPanel();
        }
        if (addingEventDialog) {
            if (height > MAX_SCROLLPANEL_HEIGHT) {
                requiredAttrsSp.setHeight(MAX_SCROLLPANEL_HEIGHT + "px");
            } else {
                if (!absMap.isEmpty()) {
                    requiredAttrsSp.setHeight(height + "px");
                } else {
                    requiredAttrsSp.setHeight(1 + "px");
                }
            }
        }
        if (dialog != null) {
            recenterDialog();
        }
    }
    protected AttrWigetBase attributeWidget;
    protected Map<String, AttrWigetBase> awb = new HashMap<String, AttrWigetBase>();

    protected void refreshAttributeFt() {
        fillAttributeFields();

        requiredAttrFt.clear();

        height = 0;
        int row = 0;

        for (final AttributeBean ab : absMap.values()) {

            Label nameLbl = new Label(trimToDot(ab.atrName)
                    //                    (ab.atrName.indexOf('.') < 0 ? ab.atrName : ab.atrName.split("\\.")[1])
                    + (!ab.isEmpty ? " *" : ""));
            HorizontalPanel attrNameNHintPanel = new HorizontalPanel();
            attrNameNHintPanel.add(BIKClientSingletons.createHintedHTML(nameLbl.getText(), null));
            requiredAttrFt.setWidget(row, 0, attrNameNHintPanel);
            String width = "185px";
//            requiredAttrFt.getCellFormatter().setWidth(row, 0, width);
//            requiredAttrFt.getCellFormatter().setWidth(row, 1, width);
            requiredAttrFt.getCellFormatter().addStyleName(row, 0, "tdPadding");
            requiredAttrFt.getCellFormatter().addStyleName(row, 1, "tdPadding");
            attrNameNHintPanel.setWidth(width);
            defaultValue = ab.defaultValue;
            atrLinValue = ab.atrLinValue;
            if (atrContainsValues) {
                if (atrLinValue != null) {
                    atrLinValue = ab.atrLinValue.substring(0, 100) + (ab.atrLinValue.length() > 100 ? "..." : "");
                }
            } else {
                if (!BaseUtils.isStrEmptyOrWhiteSpace(defaultValue)) {
                    //  defaultValue = ab.defaultValue.substring(0, 100) + (ab.defaultValue.length() > 100 ? "..." : "");
                    ab.atrLinValue = defaultValue;
                    absMap.put(ab.atrName, ab);
                    defaultValue = defaultValue.substring(0, 100) + (defaultValue.length() > 100 ? "..." : "");
                }
            }
            requiredAttrFt.setWidget(row, 1, new Label(chooseAtrValue()));
            String attrType = ab.typeAttr;
            int attrWidth;

            if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.shortText.name().equals(attrType)
                    || AddOrEditAdminAttributeWithTypeDialog.AttributeType.hyperlinkOut.name().equals(attrType)) {
                TextBox tbDescription = new TextBox();
                attrWidth = 242;
                tbDescription.setWidth(attrWidth + "px");  //8px mniej od szerokości ansiitext'u dlatego że style 'addNode' dodaje do każdego border'a 4px
                setNameFieldWidth(attrWidth - 6);
                tbDescription.setStyleName("addNode");
                requiredAttrFt.setWidget(row, 1, tbDescription);
                tbDescription.setText(chooseAtrValue());
                if (!isEnabledForEdition) {
                    tbDescription.setEnabled(false);
                }
                mapWidget.put(ab.atrName, tbDescription);
                height = height + 34;

            } else if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.calculation.name().equals(attrType)) {
                // nie ustawiamy wartości, wartość się sama policzy
                Label calcLbl = new Label(I18n.atrybutWiliczanyAutomatycznie.get());
                attrWidth = 242;
                calcLbl.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth);
                requiredAttrFt.setWidget(row, 1, calcLbl);
                height = height + 18;
            } else if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.javascript.name().equals(attrType)) {
                // nie ustawiamy wartości, wartość się sama policzy
                Label jsLbl = new Label(I18n.javascript.get());
                attrWidth = 242;
                jsLbl.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth);
                requiredAttrFt.setWidget(row, 1, jsLbl);
                height = height + 18;
            } else if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.data.name().equals(attrType)) {
                DateBox datePicker = new DateBox();
                BIKCenterUtils.setupDateBoxWithLimit(datePicker, ab.valueOpt);
                requiredAttrFt.setWidget(row, 1, datePicker);
                Date atrLinValueDate = SimpleDateUtils.parseFromCanonicalString(chooseAtrValue());
                if (atrLinValueDate != null && SimpleDateUtils.getYear(atrLinValueDate) > 1900) {
                    datePicker.setValue(atrLinValueDate);
                    datePicker.getDatePicker().setCurrentMonth(atrLinValueDate);
                }
                attrWidth = 250;
                datePicker.setStyleName("addAttr");
                datePicker.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth - 6);
                if (!isEnabledForEdition) {
                    datePicker.setEnabled(false);
                }
                mapWidget.put(ab.atrName, datePicker);
                height = height + 28;
            } else if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.datetime.name().equals(attrType)) {
                AttrDateTime dtw = new AttrDateTime("", ab.valueOpt, false);
                DateBox datePicker = (DateBox) dtw.getWidget();
                attrWidth = 250;
                datePicker.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth - 6);
                if (!isEnabledForEdition) {
                    datePicker.setEnabled(false);
                }
                requiredAttrFt.setWidget(row, 1, datePicker);
                mapWidget.put(ab.atrName, datePicker);
                height = height + 28;

            } else if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.combobox.name().equals(attrType) || AddOrEditAdminAttributeWithTypeDialog.AttributeType.comboBooleanBox.name().equals(attrType)) {
//             rb.clear();
//                List<RadioButton> rb = new ArrayList<RadioButton>();
                List<String> attrOpt = BaseUtils.splitBySep(ab.valueOpt, ",");
//                VerticalPanel vp = new VerticalPanel();
//                attrWidth = 242;
//                vp.setWidth(attrWidth + "px");
//                setNameFieldWidth(attrWidth);
//                ScrollPanel scollPanel = createScrolledPanel(vp, attrOpt.size());
//                List<String> defaultChecked = BaseUtils.splitBySep(chooseAtrValue(), ",", true);
//                height = height + 26;
//                for (String ao : attrOpt) {
//                    RadioButton radioButton = new RadioButton(BaseUtils.trimAndCompactSpaces(ab.atrName), ao);
//                    vp.add(radioButton);
//                    if (defaultChecked.contains(ao)) {
//                        radioButton.setValue(true);
//                    }
//                    if (!isEnabledForEdition) {
//                        radioButton.setEnabled(false);
//                    }
//                    rb.add(radioButton);
//                    if (!attrOpt.get(0).equals(ao)) {
//                        height = height + 18;
//                    }
//                }
//                requiredAttrFt.setWidget(row++, 1, scollPanel);
//                mapRbs.put(ab.atrName, rb);

                attributeWidget = new AttrCombobox(atrLinValue, attrOpt, ab.atrName, false, ab.defaultValue, isEnabledForEdition);
                awb.put(ab.atrName, attributeWidget);
                requiredAttrFt.setWidget(row, 1, attributeWidget.getWidget());

            } else if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.selectOneJoinedObject.name().equals(attrType)) {
                final List<RadioButton> rb = new ArrayList<RadioButton>();
                final VerticalPanel vp = new VerticalPanel();
                ScrollPanel scollPanel = new ScrollPanel(vp);
                attrWidth = 322;
                vp.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth);
                if (parent != null) {
                    BIKClientSingletons.getService().getJoinedObjs(parent.id, null, new StandardAsyncCallback<List<JoinedObjBean>>() {

                        @Override
                        public void onSuccess(final List<JoinedObjBean> result) {
                            RadioButton ndRadioBtn = new RadioButton(BaseUtils.trimAndCompactSpaces(ab.atrName), I18n.nieDotyczy.get());
                            rb.add(ndRadioBtn);
                            vp.add(ndRadioBtn);
                            for (JoinedObjBean bean : result) {
                                final List<String> encodedValues = BaseUtils.splitBySep(bean.nodeMenuPath, ",");
                                final String nodeIdLink = encodedValues.get(0);
                                String caption = encodedValues.get(encodedValues.size() - 1);
                                RadioButton radioButton = new RadioButton(BaseUtils.trimAndCompactSpaces(ab.atrName), caption);
                                nodePath2NodeLink.put(caption, nodeIdLink);
                                vp.add(radioButton);
                                radioButton.setValue(true);
                                if (!isEnabledForEdition) {
                                    radioButton.setEnabled(false);
                                }
                                rb.add(radioButton);
                            }
                            resizeRequiredAttrScrollPanel();
                        }
                    });
                }
                requiredAttrFt.setWidget(row, 1, scollPanel);
                mapRbs.put(ab.atrName, rb);
                height = height + 18;
            } else if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.checkbox.name().equals(attrType)) {
//             cb.clear();
                List<CheckBox> cb = new ArrayList<CheckBox>();

                VerticalPanel vp = new VerticalPanel();
                attrWidth = 242;
                vp.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth);
                List<String> attrOpt = BaseUtils.splitBySep(ab.valueOpt, ",");
                ScrollPanel scollPanel = createScrolledPanel(vp, attrOpt.size());
                List<String> defaultChecked = BaseUtils.splitBySep(chooseAtrValue(), ",", true);
                for (String ao : attrOpt) {
                    CheckBox checkBox = new CheckBox(ao);
                    vp.add(checkBox);
                    if (defaultChecked.contains(ao)) {
                        checkBox.setValue(true);
                    }
                    if (!isEnabledForEdition) {
                        checkBox.setEnabled(false);
                    }
                    cb.add(checkBox);
                    height = height + 19;
                }
                requiredAttrFt.setWidget(row, 1, scollPanel);
                mapCbs.put(ab.atrName, cb);
            } else if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.ansiiText.name().equals(attrType)
                    || AddOrEditAdminAttributeWithTypeDialog.AttributeType.sql.name().equals(attrType)) {
                TextArea ansiiTextArea = new TextArea();
                isExistsTextArea = true;
                VerticalPanel vp = new VerticalPanel();
                vp.add(ansiiTextArea);
                vp.setWidth("100%");
                ansiiTextArea.setStyleName("richTextArea");
//                ansiiTextArea.setWidth(width);
                attrWidth = 800;
                ansiiTextArea.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth - 6);
                ansiiTextArea.setHeight("150px");
                ansiiTextArea.setText(chooseAtrValue());
                if (!isEnabledForEdition) {
                    ansiiTextArea.setEnabled(false);
                }
                requiredAttrFt.setWidget(row, 1, vp);
                mapWidget.put(ab.atrName, ansiiTextArea);
                height = height + 160;
            } else if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.hyperlinkInMulti.name().equals(attrType)
                    || AddOrEditAdminAttributeWithTypeDialog.AttributeType.hyperlinkInMono.name().equals(attrType)
                    || AddOrEditAdminAttributeWithTypeDialog.AttributeType.permissions.name().equals(attrType)
                    || AddOrEditAdminAttributeWithTypeDialog.AttributeType.longText.name().equals(attrType)) {
                HorizontalPanel hp = new HorizontalPanel();
                String caption = "";

                if (!BaseUtils.isStrEmptyOrWhiteSpace(ab.atrLinValue)) {
                    int idx = ab.atrLinValue.indexOf(",");
//                    final String nodeIdLink = atrLinValue.substring(0, idx);
                    caption = ab.atrLinValue.substring(idx + 1, ab.atrLinValue.contains("|") ? ab.atrLinValue.indexOf("|") : ab.atrLinValue.length());
                }
                if (absMap.containsKey(atrControlName) && ab.atrName.equals(atrControlName)) {
                    tbxName.setText(caption);
                }
//                if (!BaseUtils.isStrEmptyOrWhiteSpace(ab.atrLinValue)) {
//                    int idx = ab.atrLinValue.indexOf("_");
//                    final String nodeIdLink = atrLinValue.substring(0, idx);
//                    caption = chooseAtrValue().substring(idx + 1, ab.atrLinValue.length());
//                }
                attrWidth = 242;
                hp.setWidth(attrWidth + "px");
                setNameFieldWidth(attrWidth);
                if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.longText.name().equals(attrType)) {
                    HTML html = new HTML(caption);
                    hp.add(html);
                } else {
                    Label lbl = new Label(caption);
                    hp.add(lbl);
                }
//                Label lbl = new Label(caption);
//                hp.add(lbl);
                if (isEnabledForEdition) {
                    hp.add(createEditButton(ab, requiredAttrs, row));
                }
                requiredAttrFt.setWidget(row, 1, hp);
                mapWidget.put(ab.atrName, new Label(ab.atrLinValue));
                height = height + 33;
            }
//            ft.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-white-middle" : "RelatedObj-oneObj-grey-middle");
            row++;
            requiredAttrFt.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
        }
    }

    protected String chooseAtrValue() {
        if (atrContainsValues) {
            return atrLinValue;
        } else {
            return defaultValue;
        }
    }

    protected ScrollPanel createScrolledPanel(VerticalPanel vp, int objsCount) {
        ScrollPanel scollPanel = new ScrollPanel(vp);
        if (objsCount > 30) {
            scollPanel.setHeight("90px");
        }
        return scollPanel;
    }

    protected void resizeRequiredAttrScrollPanel() {
        if (requiredAttrFt.getOffsetHeight() > MAX_SCROLLPANEL_HEIGHT) {
            requiredAttrsSp.setHeight(MAX_SCROLLPANEL_HEIGHT + "px");
        } else {
            if (!absMap.isEmpty()) {
                requiredAttrsSp.setHeight(requiredAttrFt.getOffsetHeight() + "px");
            } else {
                requiredAttrsSp.setHeight(1 + "px");
            }
        }
        if (dialog != null) {
            recenterDialog();
        }
    }

    @Override
    protected boolean doActionBeforeClose() {

        if (!mapCbs.isEmpty()) {

            for (Map.Entry<String, List<CheckBox>> sw : mapCbs.entrySet()) {

                List<String> valueOpt = new ArrayList<String>();
                for (CheckBox c : sw.getValue()) {
                    if (c.getValue()) {
                        valueOpt.add(c.getText());
                    }
                }
                String nameW = sw.getKey();
                AttributeBean ab = absMap.get(nameW);
                ab.atrLinValue = BaseUtils.mergeWithSepEx(valueOpt, ",");

                if (BaseUtils.isStrEmptyOrWhiteSpace(ab.atrLinValue)) {
                    ab.atrLinValue = ab.defaultValue;
                }
            }

        }

        for (Map.Entry<String, AttrWigetBase> aw : awb.entrySet()) {
            String attributeName = aw.getKey();
            AttributeBean ab = absMap.get(attributeName);
            ab.atrLinValue = aw.getValue().getValue();

            absMap.put(attributeName, ab);
        }

        if (!mapRbs.isEmpty()) {

            for (Map.Entry<String, List<RadioButton>> sw : mapRbs.entrySet()) {

                List<String> valueOpt = new ArrayList<String>();
                for (RadioButton c : sw.getValue()) {
                    if (c.getValue()) {
                        valueOpt.add(c.getText());
                    }
                }
                String nameW = sw.getKey();
                AttributeBean ab = absMap.get(nameW);
                ab.atrLinValue = BaseUtils.mergeWithSepEx(valueOpt, ",");
                if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.selectOneJoinedObject.name().equalsIgnoreCase(ab.typeAttr)) {
                    ab.atrLinValue = (nodePath2NodeLink.containsKey(ab.atrLinValue) ? nodePath2NodeLink.get(ab.atrLinValue) + "," : "") + ab.atrLinValue;
                }

            }
        }
        if (!mapWidget.isEmpty()) {

            for (Map.Entry<String, Widget> sw : mapWidget.entrySet()) {
                Widget w = sw.getValue();
                String nameW = sw.getKey();
                AttributeBean ab = absMap.get(nameW);

                if (w instanceof ValueBoxBase) {
                    ValueBoxBase t = (ValueBoxBase) w;
                    ab.atrLinValue = t.getText();
                } else if (w instanceof DateBox) {
                    DateBox d = (DateBox) w;
                    if (ab.typeAttr.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.data.name())) {
                        ab.atrLinValue = SimpleDateUtils.dateToString(d.getValue());
                    } else {
                        ab.atrLinValue = SimpleDateUtils.toCanonicalString(d.getValue());
                    }
                    if (BaseUtils.isStrEmptyOrWhiteSpace(ab.atrLinValue)) {
                        ab.atrLinValue = ab.defaultValue;
                    }
                } else if (w instanceof Label) {
                    Label t = (Label) w;
                    ab.atrLinValue = t.getText();
                }
                absMap.put(sw.getKey(), ab);
            }
        }

        if (!BaseUtils.isCollectionEmpty(requiredAttrs)) {
            String emptyAttrs = "";
            boolean isFirst = true;
            String coma = "";
            for (AttributeBean ab : absMap.values()) {
                if (BaseUtils.isStrEmptyOrWhiteSpace(ab.atrLinValue) && !ab.isEmpty
                        && !ab.typeAttr.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.calculation.name())
                        && !ab.typeAttr.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.javascript.name())) {
                    emptyAttrs = emptyAttrs + coma + trimToDot(ab.atrName);
                    if (isFirst) {
                        coma = ", ";
                        isFirst = false;
                    }

//                    errorLbl.setText(I18n.wypełnijAtryubuty.get());
                }
            }
            if (!BaseUtils.isStrEmptyOrWhiteSpace(emptyAttrs)) {
                errorLbl.setText(I18n.wypełnijAtryubuty.get() + " " + emptyAttrs);
                return false;
            }
        }

        if (BaseUtils.isStrEmptyOrWhiteSpace(tbxName.getText())) {
            errorLbl.setText(I18n.uzupelniNazwe.get());
            return false;
        }
        if (selectedNodeKindId == null) {
            errorLbl.setText(I18n.zaznaczTypWezla.get());
            return false;
        }
        return true;
    }

    @Override
    protected void doAction() {
        TreeNodeBean tb = new TreeNodeBean();
        tb.name = tbxName.getText();
        tb.nodeKindId = selectedNodeKindId;//selectedBean.id;
        saveCont.doIt(new Pair<TreeNodeBean, Map<String, AttributeBean>>(tb, absMap));
    }

    protected Widget createEditButton(final AttributeBean attribute, final List<AttributeBean> attributes, int row) {
        if (attribute.typeAttr.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.calculation.name()) || attribute.typeAttr.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.javascript.name())) {
            // nie ma możliwości edycji atrybutów wyliczanych
            return new Label();
        }
        PushButton editButton = createEditButtonForAttribute(attribute, attributes, row);
        return editButton;
    }

    public PushButton createEditButtonForAttribute(final AttributeBean attribute,
            final List<AttributeBean> attributes, final int row) {

        IContinuation optOnClickCont = (new IContinuation() {

            @Override
            public void doIt() {
                showAttributeForm(attribute, attributes, row);
            }
        });

        return BIKClientSingletons.createEditButtonForAttribute(optOnClickCont);
    }

    public void showAttributeForm(final AttributeBean atr, List<AttributeBean> attributes, final int row) {
        final Map<String, String> attrNameToValue = new HashMap<String, String>();
        for (AttributeBean ab : attributes) {
            attrNameToValue.put(ab.atrName, ab.atrLinValue == null ? ab.defaultValue : ab.atrLinValue);
        }
        final Map<String, AttributeBean> map = new HashMap<String, AttributeBean>();
        map.put(atr.atrName, atr);

        if (isNodeFromDynamicTree) {
            BIKClientSingletons.getService().getNodesByLinkedKind(selectedNodeKindId, BIKConstants.HYPERLINK, treeId, new StandardAsyncCallback<List<TreeNodeBean>>() {

                @Override
                public void onSuccess(List<TreeNodeBean> result) {
                    addOrEditAttribute(map, attrNameToValue, atr, result, isNodeFromDynamicTree, row);
                }
            });

        } else {
            addOrEditAttribute(map, attrNameToValue, atr, null, isNodeFromDynamicTree, row);
        }
    }

    protected void addOrEditAttribute(Map<String, AttributeBean> map, Map<String, String> attrNameToValue,
            AttributeBean atr, List<TreeNodeBean> result, boolean isNodeFromDynamicTree, final int row) {

        new AddOrEditAttributeDialog(selectedNodeKindId, isNodeFromDynamicTree)
                .buildAndShowDialog(map, attrNameToValue, atr, treeId/* node == null ? null : node.treeId*/, I18n.dodajAtrybut.get(), node,
                        new IParametrizedContinuation<AttributeBean>() {
                    public void doIt(AttributeBean param) {
                        absMap.put(param.atrName, param);

                        mapWidget.put(param.atrName, new Label(param.atrLinValue));
                        HorizontalPanel hp = new HorizontalPanel();
                        Label lbl;//= new Label();
                        //int row;
                        hp.add(createEditButton(param, requiredAttrs, row));
                        if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.hyperlinkInMulti.name().equals(param.typeAttr)
                                || AddOrEditAdminAttributeWithTypeDialog.AttributeType.hyperlinkInMono.name().equals(param.typeAttr)
                                || AddOrEditAdminAttributeWithTypeDialog.AttributeType.permissions.name().equals(param.typeAttr)) {
                            lbl = new Label(BIKClientSingletons.getHyperlinkAttribute(param.atrLinValue));
                            hp.add(lbl);
                        } else if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.longText.name().equals(param.typeAttr)) {
                            HTML html = new HTML(param.atrLinValue);
                            hp.add(html);
                        } else {
                            lbl = new Label(param.atrLinValue);
                            hp.add(lbl);
                        }

                        requiredAttrFt.setWidget(row, 1, hp);

                    }
                });

    }

    protected String trimToDot(String attrName) {
        return attrName.indexOf('.') < 0 ? attrName : attrName.split("\\.")[1];
    }

    protected abstract void fillAttributeFields();

}
