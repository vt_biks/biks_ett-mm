/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill;

import commonlib.LameUtils;
import java.util.*;
import pl.foxys.foxymill.tags.*;
import pl.trzy0.foxy.commons.tags.StringStreamParser;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.TextFilePosition;

/**
 *
 * @author wezyr
 */
public class ParserTask {

    private static enum NextTagKind {

        Eof, StartTag, EndTag, SpecialTag, StartComment, StartCommentEatLeftWS
    };
    public static final String TAG_NAMESPACE = "f";
    public static final String TAG_NAMESPACE_EX = TAG_NAMESPACE + ":";
    public static final String TAG_CLOSE_NAMESPACE_EX = "/" + TAG_NAMESPACE + ":";
    public static final String TAG_OPEN_START = "<" + TAG_NAMESPACE + ":";
    public static final String TAG_END = ">";
    public static final String TAG_CLOSE_START = "</" + TAG_NAMESPACE + ":";
    public static final String TAG_SPECIAL_START = "%{";
    public static final String TAG_SPECIAL_END = "}%";
    public static final String TAG_COMMENT_START = "<!--%";
    public static final String TAG_COMMENT_START_EAT_LEFT_WS = "<!--<%";
    public static final String TAG_COMMENT_END = "%-->";
    public static final String TAG_COMMENT_END_EAT_RIGHT_WS = "%>-->";
    private String txt;
    private int pos;
    private TextFilePosition filePos;
    private String fileName;

    //private List<ITag> result;
    public ParserTask(String txt) {
        this.txt = txt;
    }

    public ParserTask(String fileName, String txt) {
        this.txt = txt;
        this.fileName = fileName;
    }

    public List<ITag> read() {
        pos = 0;
        filePos = TextFilePosition.initialPos(fileName);

        List<ITag> res;
        try {
            res = innerReadTags();
        } catch (Exception ex) {
            throw new LameRuntimeException("unchecked exception while parsing file: " + fileName
                    + ", near: " + getCurrentTFP(), ex);
        }

        if (pos < txt.length()) {
            raiseError("unexpected end of tag reached");
        }

        return res;
    }

    // start tag
    // end tag
    // special tag
    private List<ITag> innerReadTags() {
        List<ITag> res = new ArrayList<ITag>();

        ITag prevTag = null;

        while (!eof()) {
            int startPos = pos;
            NextTagKind ntk = readPlainText();
            String plainTextStr = null;
            if (pos > startPos) {
                res.add(new PlainTextTag(plainTextStr = txt.substring(startPos, pos)));
            }
            if (ntk == NextTagKind.Eof) {
                break;
            }
            TextFilePosition tfp = getCurrentTFP();
            if (ntk == NextTagKind.SpecialTag) {
                // find close
                int endSpecialPos = txt.indexOf(TAG_SPECIAL_END, pos + TAG_SPECIAL_START.length());
                if (endSpecialPos == -1) {
                    raiseError("unclosed special tag");
                }

                Map<String, String> attrs = new HashMap<String, String>();
                int sp = pos + TAG_SPECIAL_START.length();
                if (txt.charAt(sp) == ':') {
                    int sp2 = txt.indexOf(':', sp + 1);
                    if (sp2 > sp) {
                        attrs.put("exprLang", txt.substring(sp + 1, sp2));
                        sp = sp2 + 1;
                    }
                }
                attrs.put("expr", txt.substring(sp, endSpecialPos));
                InsertTag it = new InsertTag();
                it.init(attrs, null, null, tfp);
                res.add(it);
                pos = endSpecialPos + TAG_SPECIAL_END.length();
            } else if (ntk == NextTagKind.StartComment || ntk == NextTagKind.StartCommentEatLeftWS) {
                boolean eatLeftWS = ntk == NextTagKind.StartCommentEatLeftWS;
                consumeStr(eatLeftWS ? TAG_COMMENT_START_EAT_LEFT_WS : TAG_COMMENT_START);
                int endCommentPos1 = BaseUtils.firstPos(txt.indexOf(TAG_COMMENT_END, pos), txt.length());
                int endCommentPos2 = BaseUtils.firstPos(txt.substring(pos, endCommentPos1).indexOf(TAG_COMMENT_END_EAT_RIGHT_WS, 0),
                        txt.length()) + pos;
                endCommentPos1 = BaseUtils.firstPos(endCommentPos1, endCommentPos2);
                if (endCommentPos1 == -1) {
                    raiseError("unclosed comment tag");
                }
                boolean eatRightWS = endCommentPos1 == endCommentPos2;
                pos = endCommentPos1 + (eatRightWS ? TAG_COMMENT_END_EAT_RIGHT_WS : TAG_COMMENT_END).length();
                if (eatRightWS) {
                    skipWhitespace();
                }
                if (eatLeftWS && plainTextStr != null) {
                    String eatenPTS = eatWSFromLeft(plainTextStr);
                    BaseUtils.removeLastItem(res);
                    if (!eatenPTS.isEmpty()) {
                        res.add(new PlainTextTag(eatenPTS));
                    }
                }
            } else if (ntk == NextTagKind.StartTag) {
                consumeStr(TAG_OPEN_START);
                String tagName = readIdent();
                Map<String, String> attrs = new LinkedHashMap<String, String>();
                boolean endsInPlace = true;
                while (!eof()) {
                    skipWhitespace();
                    if (consumeStr("/>")) {
                        break;
                    }
                    if (consumeStr(">")) {
                        endsInPlace = false;
                        break;
                    }
                    String attrName = readIdent();
                    skipWhitespace();
                    if (!consumeStr("=")) {
                        raiseError("expected = after attr name");
                    }
                    skipWhitespace();
                    String attrVal = getQuotedString();
//                    if (attrs == null) {
//                        attrs = new LinkedHashMap<String, String>();
//                    }
                    attrs.put(attrName, attrVal);
                }
                List<ITag> subTags = endsInPlace ? null : innerReadTags();
                res.add(createTag(tagName, attrs, subTags, prevTag, tfp));
                if (!endsInPlace) {
                    if (eof()) {
                        raiseError("expected end tag for: " + tagName + " but reached eof");
                    }
                    pos += 1 + TAG_CLOSE_NAMESPACE_EX.length();
                    String closeTagName = readIdent();

                    if (!closeTagName.equals(tagName)) {
                        raiseError("close tag " + closeTagName + " does not match start tag " + tagName);
                    }
                    if (!consumeStr(">")) {
                        raiseError("missing > at close of tag " + closeTagName);
                    }
                }
            } else if (ntk == NextTagKind.EndTag) {
                break;
            } else {
                raiseError("unknown next tag kind: " + ntk.name());
            }

            prevTag = BaseUtils.getLastItem(res, false);
        }
        return res;
    }

    private ITag createTag(String tagName, Map<String, String> attrs, List<ITag> subTags,
            ITag prevTag, TextFilePosition tfp) {
        String classNameForTag = "pl.foxys.foxymill.tags." + BaseUtils.capitalize(tagName) + "Tag";
        try {
            Class<TagBase> c = (Class<TagBase>) //Class.forName(classNameForTag);
                    LameUtils.<TagBase>safeGetClassForName(classNameForTag);

            TagBase tag = c == null ? new CustomTag(tagName) : c.newInstance();

            tag.init(attrs, subTags, prevTag, tfp);
            return tag;
//        } catch (NoClassDefFoundError ncEx) {
//            CustomTag ct = new CustomTag();
//            ct.init(attrs, subTags, ct, tfp);
//            return ct;
        } catch (Exception ex) {
            throw new LameRuntimeException("error creating tag name: " + tagName + ", class name: " + classNameForTag, ex);
        }
    }

    private String getQuotedString() {
        //checkProperCurrPos();
        if (eof()) {
            raiseError("expected quoted string (attr value)");
        }
        char quotChar = txt.charAt(pos);
        if (quotChar != '"' && quotChar != '\'') {
            raiseError("expected quotchar (\' or \")");
        }
        pos++;
        int endPos = txt.indexOf(quotChar, pos);
        if (endPos < 0) {
            raiseError("unexpected end of content inside quoted string");
        }
        String ret = txt.substring(pos, endPos);
        pos = endPos + 1;
        return ret;
    }

    private boolean consumeStr(String str) {
        if (txt.startsWith(str, pos)) {
            pos += str.length();
            return true;
        }
        return false;
    }

    private static boolean isWhitespace(char c) {
        return (c <= ' ');
    }

    private void skipWhitespace() {
        while (!eof() && isWhitespace(txt.charAt(pos))) {
            pos++;
        }
    }

    private String readIdent() {
        int i = pos;
        while (i < txt.length()) {
            char c = txt.charAt(i);
            if (StringStreamParser.isIdentCharFast(c)) {
                i++;
            } else {
                break;
            }
        }

        if (i == pos) {
            raiseError("empty ident");
        }
        String ret = txt.substring(pos, i);
        pos = i;
        return ret;
    }

    private void raiseError(String msg) {
        throw new LameRuntimeException("error while parsing: " + msg + "\n at " + getCurrentTFP());
    }

    private NextTagKind readPlainText() {
        while (!eof()) {
            char c = txt.charAt(pos);
            if (c == '<') {
                // check for start of comment
                if (txt.startsWith(TAG_COMMENT_START, pos)) {
                    return NextTagKind.StartComment;
                }
                if (txt.startsWith(TAG_COMMENT_START_EAT_LEFT_WS, pos)) {
                    return NextTagKind.StartCommentEatLeftWS;
                }
                // check for start tag or end tag
                if (txt.startsWith(TAG_NAMESPACE_EX, pos + 1)) {
                    return NextTagKind.StartTag;
                }
                if (txt.startsWith(TAG_CLOSE_NAMESPACE_EX, pos + 1)) {
                    return NextTagKind.EndTag;
                }
            } else if (txt.startsWith(TAG_SPECIAL_START, pos)) {
                // check for special tag
                return NextTagKind.SpecialTag;
            }
            pos++;
        }
        return NextTagKind.Eof;
    }

    public static List<ITag> parse(String txt) {
        return parse("?", txt);
    }

    public static List<ITag> parse(String fileName, String content) {
        ParserTask pt = new ParserTask(fileName, content);
        return pt.read();
    }

    private boolean eof() {
        return pos >= txt.length();
    }

    private TextFilePosition getCurrentTFP() {
        TextFilePosition res = TextFilePosition.moveTo(filePos, txt, pos);
        filePos = res;
        return res;
    }

    private String eatWSFromLeft(String plainTextStr) {
        int nonWSIdx = plainTextStr.length() - 1;
        while (nonWSIdx >= 0 && isWhitespace(plainTextStr.charAt(nonWSIdx))) {
            nonWSIdx--;
        }
        return plainTextStr.substring(0, nonWSIdx + 1);
    }
//    static Map<Class<? extends ITag>, Class<? extends ITag>> managerClassMapping = new HashMap();
//
//    static {
//        managerClassMapping.put(ElseTag.class, CustomTag.class);
//    }
//
//    public static ITag createManagerForTypeAndFields(Class<ITag> managerType, String... str) {
//        try {
//            Class<? extends ITag> fixedManType = managerClassMapping.get(managerType);
//            if (fixedManType == null) {
//                fixedManType = managerType;
//            }
//
//            ITag man = fixedManType.newInstance();
//            //....
//            return man;
//        } catch (Exception ex) {
//            throw new RuntimeException("podkreślmy wyjątkowość javy!");
//        }
//    }
}
