/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.IPaginatedItemsDisplayer;
import pl.fovis.client.bikwidgets.MyBIKSHistoryGrid;
import pl.fovis.common.ObjectInHistoryBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class MyBIKSHistoryTabNew extends MyBIKSTabWithPaginatorBase<ObjectInHistoryBean> {

    @Override
    protected void callServiceToGetData(AsyncCallback<List<ObjectInHistoryBean>> callback) {
        BIKClientSingletons.getService().getFromHistory(callback);
    }

    @Override
    protected IPaginatedItemsDisplayer<ObjectInHistoryBean> createPaginatedItemsDisplayer() {
        return new MyBIKSHistoryGrid();
    }

    @Override
    public String getCaption() {
        return I18n.historia.get() /* I18N:  */;
    }

    @Override
    public String getButtonStyle() {
        return "labelLink-toHist";
    }

    @Override
    protected int getPageSize() {
        return 20;
    }

    @Override
    protected String getCaptionWhenListEmpty() {
        return I18n.brakHistorii.get() /* I18N:  */;
    }
}
