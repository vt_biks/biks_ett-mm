/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.DataDictionaryForTreeKindDialog;
import pl.fovis.client.dialogs.DataDictionaryForTreeKindDialog.TreeUpdateBean;
import pl.fovis.client.entitydetailswidgets.GenericBeanGridWidget;
import pl.fovis.common.DynamicTreesBigBean;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.i18nsupport.I18nMessage;

/**
 *
 * @author beata
 */
public class DataDictionaryPage extends BikPageMinimalBase implements IBikPageForAppAdmin {

    protected VerticalPanel mainVp;
    protected VerticalPanel vp;
    protected HorizontalPanel mainHp;
    protected List<TreeBean> allTrees;
    protected List<TreeBean> dynamicTrees;
    protected List<MenuNodeBean> menuNodes;

    @Override
    protected Widget buildWidgets() {
        mainHp = new HorizontalPanel();
        mainHp.setWidth("100%");
        mainHp.setHeight("100%");
        mainVp = new VerticalPanel();
        vp = new VerticalPanel();
        HorizontalPanel hp = new HorizontalPanel();
        hp.add(createAddButton());
        hp.add(createRefreshButton());
        mainVp.add(hp);
        show(vp);
        vp.setWidth("100%");
        mainVp.setWidth("100%");

        ScrollPanel scPanelFvsm = new ScrollPanel(vp);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(scPanelFvsm, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 18);
        scPanelFvsm.setStyleName("EntityDetailsPane");
        mainVp.add(scPanelFvsm);
        mainHp.add(mainVp);
        return mainHp;
    }

    public String getId() {
        return "admin:dict:trees";
    }

    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        //no-op
    }

    protected Request callServiceToGetData(AsyncCallback<DynamicTreesBigBean> callback) {
        return BIKClientSingletons.getService().getBikTreeForData(callback);
    }

    protected BikCustomButton createAddButton() {
        return new BikCustomButton(I18n.dodajDrzewo.get() /* I18N:  */, "Tree" /* I18N: no */ + "-addTree",
                new IContinuation() {
                    public void doIt() {
                        addTreeKind();
                    }
                });
    }

    public static void showConfirmationAndMaybeReloadAfterTreeOperation(I18nMessage msg) {
//        if (Window.confirm(msg.get())) {
//            Window.Location.reload();
//        }

        BIKClientSingletons.showInfo(I18n.trwaPostepowanie.get());
        String msgStr = msg.get();

        new ConfirmDialog().buildAndShowDialog(new HTML(BaseUtils.encodeForHTMLWithNewLinesAsBRs(msgStr)), pl.fovis.foxygwtcommons.i18n.I18n.potwierdz.get(),
                "OK", I18n.anuluj.get(),
                new IParametrizedContinuation<Boolean>() {

                    @Override
                    public void doIt(Boolean param) {
                        if (param) {
                            Window.Location.reload();
                        }
                    }
                });
    }

    private void addTreeKind() {
        new DataDictionaryForTreeKindDialog().buildAndShowDialog(null,
                menuNodes, allTrees, null,
                new IParametrizedContinuation<TreeUpdateBean>() {
                    public void doIt(final TreeUpdateBean param) {
                        BIKClientSingletons.showInfo(I18n.trwaPostepowanie.get());
                        BIKClientSingletons.getService().createTree(param.name, param.treeKind,
                                param.parentMenuCode, param.parentId, param.visualOrder, param.isAutoObjId,
                                param.optMenuSubGroup, param.serverTreeFileName,/* param.serverLinkFileName,*/
                                new StandardAsyncCallback<Void>(I18n.bladTworzeniaNowegoDrzewa.get() /* I18N: no:err-fail-in */) {
                                    public void onSuccess(Void result) {
                                        BIKClientSingletons.showInfo(I18n.dodanoDrzewo.get() /* I18N:  */);
                                        show(vp);

//                                        if (!BaseUtils.isStrEmpty(param.parentMenuCode)) {
                                        showConfirmationAndMaybeReloadAfterTreeOperation(I18n.poDodaniuDrzewa);
//                                        BIKClientSingletons.callService2DeleteTmpFiles(param.tmpFileName2Delete);

//                                        }
                                    }

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        super.onFailure(caught); //To change body of generated methods, choose Tools | Templates.
//                                        BIKClientSingletons.callService2DeleteTmpFiles(param.tmpFileName2Delete);
                                    }
                                });
                    }
                });
    }

    public void show(final VerticalPanel vp) {
        callServiceToGetData(new StandardAsyncCallback<DynamicTreesBigBean>() {
            @Override
            public void onSuccess(DynamicTreesBigBean result) {
                allTrees = result.allTrees;
                menuNodes = result.menuNodes;
                dynamicTrees = BIKClientSingletons.getDynamicTrees(allTrees);
//                dynamicTrees = allTrees;

//                for (TreeBean tb : allTrees) {
//                    String code = tb.treeKind;
//                    if (BIKClientSingletons.getTreeKindCaptionsMap().containsKey(code) && tb.isBuiltIn == 0) {
//                        dynamicTrees.add(tb);
//                    }
//                }
                vp.clear();
                GenericBeanGridWidget<TreeBean> gw = new GenericBeanGridWidget<TreeBean>(true, new TreeBeanExtractor(menuNodes, allTrees), true);
//                NodeAwareBeanGridWidget<TreeBean> gw = new NodeAwareBeanGridWidget<TreeBean>(true, new TreeBeanExtractor(menuNodes, allTrees), null);
                gw.setDataChangedHandler(new IContinuation() {
                    @Override
                    public void doIt() {
                        show(vp);
                    }
                });
                vp.add(gw.buildGrid(dynamicTrees, dynamicTrees, 0, dynamicTrees.size()));
            }
        });
    }

    private IsWidget createRefreshButton() {
        return new BikCustomButton(I18n.odswiez.get() /* I18N:  */, "Tree" /* I18N: no */ + "-refreshTree",
                new IContinuation() {
                    public void doIt() {
                        show(vp);
                    }
                });
    }
}
