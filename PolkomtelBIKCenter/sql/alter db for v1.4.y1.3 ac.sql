
exec sp_check_version '1.4.y1.3';
go
----------------------------------------------------
----------------------------------------------------
----------------------------------------------------
-- T�umaczenia tutoriali i tekst�w powitalnych
----------------------------------------------------
----------------------------------------------------
----------------------------------------------------


delete from bik_tutorial_translation
go

insert into bik_tutorial_translation (lang, code, movie, title, txt) values  
('en', 'Introduction', null, 'Introduction', '<p>BI Knowledge System is a tool that allows mangement of knowledge associated with Business Intelligence systems. </p>

<p></p>

<p></p>

<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The software consists of six modules arranged in tabs. (Figure 1.1).</p>

<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>


<p></p>

<div align="center"><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_721498659056413611.png"><br></p>


    <blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 1.1. BI Knowledge System tabs.</p>

    </blockquote></div>
<p></p>


<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><b>
        <i>My BIKS </i> </b>module (Figure 1.2) is responsible for user personalization and includes a logged on user''s individual data.</p>


<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>


<p></p>

<p></p>

<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_2117911334507805228.png"><br></p>


<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 1.2. Exemplary view of <i>My BIKS</i> module.</p>

</blockquote>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><b><i>Library </i></b>module.</p>
<p></p>
<p></p>

<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Most of the BI Knowledge System application modules look just like this. The left side of the screen is a, so called, tree panel which contains the structure of folders (Figure 1.3).</p>

<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>

<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "> <img src="fsb?get=file_2635585063280849574.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 1.3. Exemplary view of BI Knowledge System tree panel.</p>
</blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The right side is divided into two parts. </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The upper part makes the properties panel (Figure 1.4).</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_2976391287963252081.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 1.4. Exemplary view of BI Knowledge System properties panel.</p>
</blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The lower part - association panel (Figure 1.5).</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br> </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_3948927694432622596.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 1.5. Exemplary view of BI Knowledge System association panel</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The <i>Library </i>module functions as a metadata repository. It contains metadata from external data sources such as reporting systems and data warehouses.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The application is flexible and it�s possible to develop available components. The <i>DQC</i> tab which contains quality tests and reports updates is an example of such development.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Selecting an object in the tree panel displays information involving the name and attributes of the selected object in the properties panel (Figure. 1.4). The upper right corner of the properties panel contains icons for previewing descriptions, issuing positive or negative evaluation of the viewed content, generating links to the previewed view and the addition of the selected object to your favourites list. </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The association panel (Figure 1.5) displays all defined connections or relationships between the selected node and other objects located in the BI Knowledge System. For example <i>Market Analysis</i> report relies on three definitions in the <i>Glossary module</i>.</p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The <i><b>Glossary </b></i>module (Figure 1.6) is a glossary of terms used in the corporation. It�s tree panel consists of folders grouping definitions which are represented by a book icon.</p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_4110265429617590639.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 1.6. Exemplary view of BI Knowledge System <i>Glossary</i> module.</p>
    <p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The <i><b>Knowledge Base</b></i> module includes practical knowledge related to company activity which can be contained in a document repository. The repository contains articles (marked with the letter <i>A</i>), text files, excel files, pdf files and other formats (Figure 1.7). </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_4027897527534158886.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 1.7. Exemplary view of BI Knowledge System <i>Knowledge Base Documentation </i> module tree panel.</p>
</blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The knowledge contained in the BI Knowledge System can be ordered freely using categorization. Icons shown in Figure 1.8  are business areas to which other elements of the BI Knowledge System (for example: earlier discussed reports, articles or definitions) are linked.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_6415667199040655208.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 1.8. Exemplary view of BI Knowledge System <i>Knowledge Base Categorization</i> module tree panel.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The next tab is the <b><i>BI community</i></b> management module. It provides a layer of communication between users. This is where a user can keep track of expert blogs (Figure 1.9). </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "> <img src="fsb?get=file_3274573943471788358.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 1.9. Exemplary view of BI Knowledge System BI <i>Community Blogs</i> module.</p>
</blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The module also contains business information and contact details of all users of the system and presents their assignment to specific groups (Figure 1.10).</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_4182576243418955294.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 1.10. Exemplary view of BI Knowledge System BI <i>Community Users</i> module.</p>
</blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The last module is the Search Engine. It is slightly different than the other tabs (Figure 1.11) and allows for searching through items in the BI Knowledge System application.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "> <br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_1249694903457633910.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 1.11. Exemplary view of the <i>Search</i> tab.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Each tab has a help buton which when pressed displays a help window with a description of the current tab (Figure 1.12).</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "> <img src="fsb?get=file_5116687448430065016.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 1.12. Exemplary view of the <i>Search</i> tab.</p>
</blockquote><p></p>
')  

insert into bik_tutorial_translation (lang, code, movie, title, txt) values  
('en', 'MyBIKS', null, 'My BIKS', '<p></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">This material describes functionalities available in <i>My BIKS module</i>.</p>
<p></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><i>My BIKS</i> startup view allows insight into items marked as favorites (Figure 2.1). The order of items in the list is determined by the date individual elements were added.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><img src="fsb?get=file_591179126895182485.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">Fig. 2.1. Exemplary <i>My BIKS</i> module startup view.</p>
    <p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><br></p>
</blockquote><p></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">If the content of one of your favorite sites changes the application informs you about it. Objects within which the change occurred are at the beginning of the list of Favorites. </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_2399200714291124745.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 2.2. Exemplary view of favourite objects in <i>My BIKS</i> module.</p>
    <p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
</blockquote><p></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Title of the modified object is marked in bold and information if an item was added, modified, or removed from this object can be found in parenthesis next to it. Clicking on the gray arrow displays details of the modification..
By pressing the GO TO button marked by green arrows we can go to the place in the application where the modified object is located and see changes that have occurred in it. You can also preview the changes useing the magnifying glass icon. You can narrow the view to only those objects that have been modified. You can also immediately mark all items as read (Figure 2.2).
</p>
<p></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Other features of the <i>My BIKS</i> tab are available in the panel on the left side of the screen (Figure 2.3).</p>
<p></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_695995149653057555.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig.2.3. Functionalities of <i>My BIKS</i> tab.</p>
    <p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
</blockquote><p></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">These include reading news posted by the administrator (Figure 2.4). New, unread news is marked with a <i>New</i> in red lettering and the title is in bold.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_4957577705642414.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 2.4. Exemplary view of <i>My BIKS</i> module - News.</p>
    <p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
</blockquote><p></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><i>My BIKS</i> also allows to trace the history of viewed objects, view their comments and reviews. </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><i>Data Edition</i> functionality is used to change the password and to enter and change the avatar (Figure 2.5).</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_46256438273299643.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 2.5. View of the <i>Data Edition</i> functionality.</p>
</blockquote><p></p>
')  

insert into bik_tutorial_translation (lang, code, movie, title, txt) values  
('en', 'Search', null, 'Search', '<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Searching for elements in Knowledge System BI application is done using the <i>Search</i> tab. To find an object simply type the desired sequence of characters in the search field. The search engine returns all objects in the system which have the requested string in their name, description, attributes or content. </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">For example (Figure 3.1) typeing <i>services</i> as a search string returns 22 results.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "> <img src="fsb?get=file_258602651074562753.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 3.1. Exemplary view of the <i>Search</i> tab.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">There can be up to 20 objects on a sigle page. Moving between the pages is done by clicking arrows in desired direction. We can also manage the view of search results by unchecking or checking selected checkboxes. It is a function which allows limiting the view.</p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The tree panel gives us the possibility to limit our search (Figure 3.2). It contains the structure of objects in the BI Knowledge System which allows you to search within the context of each object in the application. The sequence of characters entered in the search field will be searched only in those modules whose checkboxes are checked.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_4329481256905337962.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 3.2. Filtering search terms.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Searching for the term <i>import</i> in all BI Knowledge System modules returns 13 elements. After searching for it only in reports the search is limited to one report.  Searching the <i>Glossary</i> (Figure 3.3) displays only those results that are in the selected area.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_952796681054714999.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 3.3. Exemplary view of the <i>Search</i> tab.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Next, let''s look at how to enter characters in the search box. For example, entering the term "analysis" returns 23 records that match the search conditions. Adding an additional term to the search field e.g <i>market</i> narrows the search to 14 elements which in their titles or content have the words <i>market</i> and <i>analysis</i> (Figure 3.4).</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_5478699526871258296.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Fig. 3.4. Exemplary view of the <i>Search</i> tab.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Typing the terms in quotes narrows the search even more. Only items containing the complete phrase: "market analysis" will be displayed. </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Typing an asterisk "*" in the search field  replaces any number of characters. All objects in the BI Knowledge System will be shown. By using filters (checkboxes) all objects of the selected type can be shown. This allows us to count the number of reports or universes easily.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The application also enables you to filter data displayed in the tree panel. Filtering works the same way on all tabs. </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_5424909523394251327.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 3.5. Filtering data displayed in the tree panel on the example of the <i>Glossary</i>.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Just enter the desired string in the search field at the bottom of the tree panel then press filter. This will narrow the view to folders that contain objects that meet the filter conditions. Clicking on the <i>Clear all</i> icon takes us back to the startup view.</p>
<p></p>
')  

insert into bik_tutorial_translation (lang, code, movie, title, txt) values  
('en', 'AppNavigation', null, 'Navigation', '<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">This material describes the main principles of navigating through the BI Knowledge System. It describes the navigation between objects in different modules and presents each tab''s functionalities.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Navigating through objects in most BI Knowledge System modules is similar. You move through the tree panel by useing a drop-down structure of objects visible in the panel.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">In the <i>Library</i> tab you navigate through the drop-dowm structure of metadata from reporting systems, data warehouses and <i>DQC</i> tabs.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The <i>Glossary</i> Module contains definitions of terms used in a corporation. Let''s look at the definitions given in the <i>Banking</i> folder (Figure 4.1).  Corporate definitions are marked in blue. Red tells us about the lack of corporate definition for a term, but after expanding the node we see that there is a working definition for it. Corporate definitions may also have sub-versions e.g. <i>departmental</i>.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_4626611914712451385.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 4.1. Definition symbols/icons in the <i>Glossary module.</i></p>
</blockquote><p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The <i>Knowledge Base</i> module in addition to articles and documents is also categorization. An example of such categorization may be an organizational structure of a company to which other BI Knowledge System elements are linked. The linked elements are a shortcut to individual objects. This allows you to store a single object in several structures. Clicking on the link will display information about the linked object in the properties and <i>Knowledge Base</i> connections panels. We can also use a <i>go to original</i> command available under the mouse''s right-click (Figure 4.2). This functionality takes us to the place in application where the object is located.<br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br> </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_5617052295874371910.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 4.2. An exemplary view of the <i>Knowledge Base module.</i></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Let''s go to the <i>Glossary</i> tab and look at the properties panel. It shows information about the currently selected object. By using the drop-down button you can increase the area of the properties panel making it easier to browse its displayed contents (Figure 4.3). Pressing the drop-down button again takes you back to the previous view.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><strike><img src="fsb?get=file_1314330186371209212.png"></strike><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 4.3. An exemplary view of the <i>Glossary</i> module.</p>
</blockquote><p dir="ltr" style="text-align: justify; margin-top: 0pt; margin-bottom: 0pt; "> </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Clicking on the magnifying glass placed in the upper right corner of the properties panel allows you to view the description of the object. Remaining icons feed the list of objects displayed in the <i>My BIKS</i> tab. </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">In the case of a BI report an additional <i>Open in SAP BO</i> icon appears in the properties panel (Figure 4.4). It allows users with the appropriate permissions to open the report on a BusinessObjects platform.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_934442184500741669.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 4.4. An exemplary view of the <i>Library</i> module.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Let''s go to the <i>Knowledge Base</i> module. The contents of most objects are displayed in the properties panel immediately after you select an object in the tree panel but to see the content of a desired document you have to click on the <i>view document</i> icon (Figure 4.5) which takes us to the content of the selected document.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_2667409129821720156.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 4.5. Exemplary view of the <i>Knowledge Base</i> module.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The associations panel allows free movement between tabs and their arbitrary switching. Each tab contains a list of objects associated with the element selected in the tree (Fig. 4.6).</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">For example, for the <i>market analysis</i> report the <i>Library</i> tab presents:</p>
<p></p>
<p></p>
<ul><li>database schema </li>
    <li>individual tables that contain report data, </li>
    <li>the universe on which the inquiries were built</li>
    <li>the connection which the report uses to connect with the data base. </li>
</ul><p><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_1721596858429561559.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 4.6. Exemplary view of the association panel.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The <i>BI Society Users</i> tab allows us to find out who is responsible for the content of this report or who is an expert or specialist in this field.  </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The last tab are <i>comments</i>. This tab allows you to view posted comments and add new comments to the read content (Figure 4.6). You add comments by clicking on the <i>add comments</i> icon. When a dialog box appears you enter the title and content of the comment you want to add. After completing these fields changes have to be saved. </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "> <img src="fsb?get=file_1677071176514219986.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 4.7. Exemplary view of the <i>Library</i> module.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The added comment appears in the tab. The pencil icon allows you to edit comments and clicking on the trash icon will delete the entry. Keep in mind that you can edit and delete only your own comments.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p></p>
')  

insert into bik_tutorial_translation (lang, code, movie, title, txt) values  
('en', 'UseCases', null, 'Use cases', '<p align="left"></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">This material presents practical exercises. It shows solutions to various problems with the BI Knowledge System application on the example of specific use cases.</p>
<p dir="ltr" style="text-align: justify; margin-top: 0pt; margin-bottom: 0pt; " align="left"> </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">Let''s assume that we need to know which universe a report is based on. Let''s find this information for <i>OFE</i> report as an example. </p>
<p align="left"></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">The question can be answered in several ways by using BIKS.</p>
<p align="left"></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">Reports come from the BI platform so their metadata are in the <i>Library</i> tab. Therefore let''s filter the tab''s tree panel and locate the desired report. The <i>OFE</i> report is now displayed in the tree panel. We now select the report and move on to analyse the links panel. Universe is the element we search for. Information on universes in the BI Knowledge System is also are arranged in the Library tab. Therefore we need to open the Library tab in the associations panel. This will show us a list of all objects in the tab associated with the <i>OFE</i> report. Since we are looking only for the universe we have to uncheck all checkboxes and select only universes. <i>eFashion</i> universe is the result of our search.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><img src="fsb?get=file_3261276149311785744.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">Figure 5.1. Searching for associations between a universe and a report.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><br></p>
</blockquote><p align="left"></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">Another way to find the <i>OFE</i> report''s universe is the search engine (Figure 5.2). First we search for the report. We check the report checkbox in the tree panel. With this selection only reports will be searched. We enter <i>OFE</i> in the search box and press the search button. Our search result is the required report. By using the preview icon we display the report''s details (description and associations). Next we proceed as in the previous case. We can also go to the place where the report is located and follow the same steps. Such steps can also answer other questions.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><img src="fsb?get=file_8037105180868049910.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">Figure 5.2. Searching for a report and its dependence on universe.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><br></p>
</blockquote><p align="left"></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">For example: which business definition does this report concern or who is the person responsible for this report. By expanding the node in the tree panel you get information on objects which make up the query (Figure 5.3).</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><img src="fsb?get=file_6954196202339410245.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">Figure 5.2. An exemplary view of the <i>Library</i> tab''s tree panel.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><br></p>
</blockquote><p align="left"></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">Another information we may need is information on how many reports support a given business process. Business processes are defined in the <i>Knowledge Base</i>. Therefore we go to <i>Knowledge Base</i> -> <i>Categorization</i> -> <i>Business Processes</i> tab. Let''s search for the production process. It is supported by two reports.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><img src="fsb?get=file_7335017106467420572.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">Rys. 5.4. Wyszukiwanie zale�no�ci mi�dzy procesem produkcji i raportami.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><br></p>
</blockquote><p align="left"></p>
<p align="left"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">Now, let''s suppose that we had read the document and we want to contact the person who is responsible for its contents. We go to the <i>Knowledge Base</i> -> <i>Documentation</i> tab and look for our document. Next, we choose the document we are interested in from the list of items and click on the <i>BI Society</i> -> <i>Users</i> tab in the associations panel. The person responsible for the document is <i>John Smith</i>.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><img src="fsb?get=file_683670160096150487.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">Figure 5.5. Searching for a person responsible for a given document.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left"><br></p>
</blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; " align="left">If we go to information on the user contact information and other business information will be displayed. Furthermore, by going to the <i>Knowledge Base Documentation</i> tab in the associations panel we can view a list of all documents for which John Smith is responsible.</p>
<p align="left"></p>
')  

insert into bik_tutorial_translation (lang, code, movie, title, txt) values  
('en', 'Analysis', null, 'Analysis', '<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">This material shows how to perform an impact and dependency analysis using BI Knowledge System.</p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><b>Impact analysis</b></p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">The impact analysis is useful when we make changes to source data.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Any change made to source data has impact on elements that are based on that data. These may include e.g. universes and reports.</p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">For example let''s suppose we make a change in any of the database tables. In this case it is necessary to know how many universes and reports will the change of the table affect. </p>
<p dir="ltr" style="text-align: left; margin-top: 0pt; margin-bottom: 0pt;">We begin from finding the table that we are going to modify. Then we enter the name of our table (e.g.Sales) in the <i>Library</i> -> <i>Database</i> tab (Figure 6.1). Then, we select the table. In the properties panel we see <i>show SQL</i> icon through which we can view the contents of the SQL query and in the associations panel in the Library tab we can conclude that the modification of this table will affect two universes and eight reports.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "> <img src="fsb?get=file_4834933759343140039.png"><span style="text-align: center; "><br></span></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><span style="text-align: center; ">Figure 6.1. An example of impact analysis.</span></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><span style="text-align: center; "><br></span></p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">After narrowing the view using checkboxes we get specific information on which reports and universe it will be. We can also see detailed information about the selected item, go to it or end the analysis at this stage.</p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><b>Dependency analysis</b></p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">We perform a dependency analysis when we need knowledge on the relationships between individual elements. </p>
<p dir="ltr" style="text-align: left; margin-top: 0pt; margin-bottom: 0pt;">For example, we want to know from which tables and definition our report is dependant. First, we look for the report whose dependencies we want to analyse. Then, in the associations panel we filter by the desired elements. We can see that it depends on three tables in the database (Figure 6.2). To see what definition it depends on we need to go to the <i>Glossary</i> tab.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_6928169107420198419.png"><br></p>
<blockquote><p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 6.1. Sample analysis of dependencies.</p>
</blockquote><p></p>
')  

insert into bik_tutorial_translation (lang, code, movie, title, txt) values  
('en', 'AddNews', null, 'Adding news', '<p></p>
An expert can perform steps shown in the film for all content; an author can add, edit and delete only the content he was given authorization to.
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">We add news in the <i>My BIKS</i> tab. In order to do this we need to choose the <i>News</i> tab. The procedure of adding news is similar to adding a definition (see: BI Knowledge System - adding and editing definitions). It is added by using the <i>Add news</i> icon (Figure 7.2). Pressing the icon opens a window where you enter the title and content of the news and then save your changes.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_145412341034124112.png"><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Figure 7.2. Exemplary view of the <i>My BIKS module.</i></p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">The added news is displayed in the profile of every BIKS user. The title in bold and a <i>New</i> symbol next to the news means that it has not yet been read. If someone modifies an entry it is treated as new and goes to the beginning of the list. With the help of the edit button available on the right side of the screen we can edit the added news or delete it by clicking on the trash icon.</p>
<p></p>
')  

insert into bik_tutorial_translation (lang, code, movie, title, txt) values  
('en', 'AddAttributes', null, 'Adding attributes', '<p>This range of functionalities is assigned to users with author or expert competencies. </p>
<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Attributes are added to elements descriptions visible in the tree panel. This is done by using the <i>Add Attribute</i> icon visible in the lower left corner of the properties panel (Figure 7.1). The presence of this icon is optional as not every object must have attributes. The administrator decides of its existence. After clicking on the icon a box with a drop-down list appears and a name of the attribute you want to add needs to be chosen.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br> </p>
<blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_6871046713386902091.png"><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Figure 8.1. Exemplary view of the <i>Library module</i>.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">There are different types of attributes:</p>
<p></p>
<p></p>
<ol style="margin-top: 0pt; margin-bottom: 0pt; "><li style="list-style-type: decimal; background-color: transparent; vertical-align: baseline; margin-left: 24px; "><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Date - where you select a date from the calendar.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_1249575244390942717.png"><br></p>
</li></ol><p></p>
<p></p>
<ol start="2" style="margin-top: 0pt; margin-bottom: 0pt; "><li style="list-style-type: decimal; background-color: transparent; vertical-align: baseline; margin-left: 24px; "><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Long text - the procedure for adding an attribute is the same as for adding a definition.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_9200887028459721139.png"><br></p>
</li></ol><p></p>
<p></p>
<ol start="3" style="margin-top: 0pt; margin-bottom: 0pt; "><li style="list-style-type: decimal; background-color: transparent; vertical-align: baseline; margin-left: 24px; "><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Short text - to be filled in the visible field.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_8016308125063520185.png"><br></p>
</li></ol><p></p>
<p></p>
<ol start="4" style="margin-top: 0pt; margin-bottom: 0pt; "><li style="list-style-type: decimal; background-color: transparent; vertical-align: baseline; margin-left: 24px; "><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Short text - to be filled in the visible field.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_3250066301854180652.png"><br></p>
</li></ol><p></p>
<p></p>
<ol start="5" style="margin-top: 0pt; margin-bottom: 0pt; "><li style="list-style-type: decimal; background-color: transparent; vertical-align: baseline; margin-left: 24px; "><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">External Link -  where we enter a URL in the visible field. </p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_1078010854952363223.png"><br></p>
</li></ol><p></p>
<p></p>
<ol start="6" style="margin-top: 0pt; margin-bottom: 0pt; "><li style="list-style-type: decimal; background-color: transparent; vertical-align: baseline; margin-left: 24px; "><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">A single choice field.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_6952492918251341573.png"><br></p>
</li></ol><p></p>
<p></p>
<ol start="7" style="margin-top: 0pt; margin-bottom: 0pt; "><li style="list-style-type: decimal; background-color: transparent; vertical-align: baseline; margin-left: 24px; "><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">A multiple choice field.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_2701716298698190420.png"><br></p>
</li></ol><p></p>
<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">After filling in the content or selecting the right selection we save the attribute. The added attribute can not be removed.</p>
<p></p>
')  

insert into bik_tutorial_translation (lang, code, movie, title, txt) values  
('en', 'UpdateContent', null, 'Updating content', '<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Modifying the BI Knowledge System modules is possible with options available under mouse''s right-click and icons available in this version of permissions.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">A right-click on the name of the report while in the <i>Library</i> tab allows you to enter or modify an existing description of the item. </p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">You can change the name of an item, edit the description or add a new directory in the same way while in the Glossary tab. The <i>green plus</i> at the bottom of the tree panel allows you to add an item to the highest level of the tree. You can add definitions to existing folders. Clicking the <i>Add definition</i> option shows a window where you enter the title of the new definition and its content (Figure 8.1). A detailed description of the text editor''s functionalities can be found in <i>BI Knowledge System - Creating and editing definitions</i> material. After filling in these elements you need to determine <i>the version of the definition.</i>  </p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">For the created definitions you can add different versions using the right mouse button. The title of the version is completed automatically. It is only necessary to fill in the content and choose the version of the definition. Once the changes are approved a message that tells us under what business term the definition will be added appears, if everything is correct, the message should be approved. </p>
A user with <i>expert</i> rank can manage the display order of elements in the list. Options such as <i>Move up, Move to top, Move down, Move to bottom or Sort alphabetically</i> are used for that (Figure 8.1). Options available under the right mouse click enable you to <i>cut</i> elements from one tree location and <i>paste</i> them to another as well as <i>delete</i> tree items. <p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_466508107012903056.png"><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Figure 9.1. Exemplary view of the <i>Glossary</i> module.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Managing the <i>Knowledge Base</i> tab is similar. You can add <i>articles</i> in the same way as definitions, except that articles do not have versions. An additional functionality here is <i>adding documents</i> (Figure 8.2). Documents are added by searching the the disk space. The loaded file is automatically saved on the BIKS server.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_3124683827742223719.png"><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Figure 9.2. Exemplary view of the <i>Knowledge Base</i> module.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">In the <i>Categorization</i> tab you can create business areas and assign them to other elements of any BIKS module. We can expand the tree structure by adding to existing areas more business sub-areas. We can link elements to existing areas by clicking on <i>add link</i>. This functionality is shown in <i>BI Knowledge System - linking elements</i> movie tutorial.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">The <i>BI Society</i> module allows <i>expert blogging</i>. Blog entries are added just like definitions. Users can add entries to blogs which they are authors of. Each blog can have multiple authors. </p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">The <i>BI Society</i> tab allows also managing system users. We can create groups and function sub-groups and assign users to them. We link users using the assign to group command. This functionality is shown in the <i>BI Knowledge System - linking elements</i> movie tutorial. </p>
<p></p>')  

insert into bik_tutorial_translation (lang, code, movie, title, txt) values  
('en', 'LinkElements', null, 'Linking elements together', '<p></p>
<p>This material explains how link elements. Linking elements in BIKS functions in two places. The first place is a <i>Categorization tab</i> in the <i>Knowledge Base module</i> where you can link an item to existing business areas. The second place is the <i>Users tab</i> in the <i>BI Society module</i>. This functionality allows linking a user to selected functional groups.</p>
<p></p>
<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><b>Linking categorizations</b></p>
<p></p>
<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">In order to link an element to an existing area you must use the add link command which displays an add links window (Figure 9.1). The window shows the structure of all BIKS areas. At this point the item you want to assign to the selected area should be pointed. You can find it by clicking on the tree or using filtering options. Each element may be linked to several business areas or may not occur in any of them.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "> <img src="fsb?get=file_8843591372194217517.png"><br></p>
    <p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Figure 10.1. Exemplary view of the <i>Knowledge Base</i> module.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">The <i>go to original</i> option takes us to the source location of this element in BIKS where we are able to edit information and delete items. Deleting the original will remove the login from anywhere it is linked while removing the element from a business area works only within a given area.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_944689372123618294.png"><br></p>
    <p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Figure 10.2. Using the <i>Go to original</i> option.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><b>Linking users</b></p>
<p></p>
<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Users are linked with the assigne to <i>group command</i> (Figure 9.3). In the <i>add a link window</i> you need to specify the user you want to assign to the selected group. You can find him on the list of users or choose from one of the groups. Each user can be linked to several groups or may not appear in any.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br> </p>
<blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_1800774126066880125.png"><br></p>
    <p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Figure 10.3. Assigning users to groups.</p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">The <i>Go to original</i> option takes you to a list of users where you are able to edit and delete user information (Figure 9.4). Deleting the original will remove login from any location where it is linked while removing the user from a group works only within the group.</p>
<blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_6795477607094489301.png"><br></p>
    <p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Figure 10.4. Using the <i>Go to original</i> option.</p>
</blockquote><br><p></p>
')  

insert into bik_tutorial_translation (lang, code, movie, title, txt) values  
('en', 'AddEditDefinitions', null, 'Adding/Updating definitions', '<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">This material contains information on the possibilities of BIKS text editor with examples of use.</p>
<p></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">We add definitions using the <i>add definition</i> function available under the right mouse button. After selecting this option an editing window appears. You can enter a text manually or paste a previously copied text into the editor. </p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">The editor allows (Figure 10.1) <i>bold text, underlining, italics, strikethrough</i>. The <i>remove formatting</i> button allows you to remove these effects. We may also decide on the alignment of the text. </p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">There is also an option of <i>indexing</i> available. It is realized by two icons. A subscripts and a superscript can be inserted. We can also separate parts of text by inserting <i>a horizontal line. Numbering</i> and <i>bulleting</i> of the text is available.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">We can add images in two ways. The first is to add an image from a www by entering it''s URL. The second is to insert an image from disc space via searching.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Another functionality is the possibility to add tables. First the number of columns and rows in a table must be declared, then the blanks filled with content. The editor makes it possible to modify the size of the table.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">We can also insert a <i>code</i>. In this area text formatting is similar to formatting text in a queries editor. This serves to highlight fragments containing elements of the code.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">At any time we can return to edit mode. </p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">An additional functionality of the editor is creating links. A part of a definition''s text can be a link to a website. To achieve this we mark the selected text and type the URL to which we want to invoke to. It is also possible to connect part of text with any element contained in the BIKS application. To do this we need to select a text and look for the element we want to link with it. Selected fragments will have a different font color which will suggest that they are so called references. In a Review mode the highlighted fragments move to a previously declared location. The list of references to BIKS objects is automatically added under a <i>See Also</i> headline. Linking text with BIKS objects is added automatically in the associations panel. </p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">If a definition contains a lot of text we can insert <i>a table of contents</i>, which moves to the desired fragment in the text. First we must select headwords to appear in the table of contents and using the <i>insert header</i> icon make them headlines. This changes the formatting of the selected text. After selecting all the elements we set the cursor at the point we want to insert the <i>table of contents</i> and click on the icon with table of contents, then save the changes.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">With the table of contents we can easily and quickly access the part of text we are interested in.</p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_115296931263397198.png"><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; ">Figure 11.1. Text editor functionalities.</p>
</blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p></p>
')  

insert into bik_tutorial_translation (lang, code, movie, title, txt) values  
('en', 'AddConnections', null, 'Adding connections', '<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><span style="text-indent: 35.4pt; ">This functionality is assigned to users with author or expert privileges. An expert can perform the steps described in the material on all BIKS sites while an author has only the possibility to add and remove associations added from his account.</span></p>
<p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Let''s get to the associations panel. In the lower right corner of the panel there is an icon for <i>adding associations</i>. It is on each of the BIKS tabs except MY BIKS and the search engine. With this icon we can create associations between any BIKS objects. <br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<blockquote><p><img src="fsb?get=file_2259989709457017299.png"><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 12.1. Exemplary view of the Knowledge Base module.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Clicking this button displays a window that contains the same structure of the tree panel as the module to which we want to add an association. Then we need to find the element we want to associate with the previously selected object. We can find the elements manually or use filtering. We select a definition by clicking the left mouse button on a blue square visible near it. The object name highlights in red, which means that a new association has been added but it is not yet saved. 
After saving a new association it is added in the associations panel. The association gives us the possibility to move freely between related elements, regardless of which one we''re currently viewing.
<br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Let us analyze creating an association on the example of associating the Import report with a definition. First, select the report in the Library tab''s tree panel. Then, select the <i>glossary tab</i> in the associations panel and click <i>add association</i>. A window with the rendition of the tree of the Glossary appears (Figure 11.2). We can see that the <i>Pharmaceutical line of business</i> folder is highlighted in blue. After expanding this node sub-folders are shown, two of them are also different from the others <i>(Pharmacology and Copyright)</i>. This means that the defined associations are inside these objects. The associated definitions are <i>The original medical product</i> and <i>Trademark</i>. </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_7858988150616634127.png"><br></p>
    <p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 12.2. An exemplary view of an <i>add an association window</i>.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">We can add a number of new associations, change or delete others at one time. Names of added and modified definitions highlight in red and icons appear in the blue checkbox next to the definition (Figure 12.3).
    Nazwa obiektu pod�wietli�a si� na czerwono, co  oznacza, �e zosta�o dodane nowe po��czenie, ale nie jest ono jeszcze  zapisane. </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Po  zapisaniu nowego po��czenia zostaje ono dodane w panelu powi�za�.  Po��czenie daje nam mo�liwo�� swobodnego poruszania si� pomi�dzy  powi�zanymi elementami z bez wzgl�du na to na kt�ry z nich aktualnie  przegl�damy. Mo�emy jednorazowo doda� kilka nowych powi�za�, a pozosta�e zmieni� lub usun��. Nazwy dodanych i zmodyfikowanych definicji pod�wietlaj� si� na czerwono, a w niebieskim checkboxie obok definicji pojawiaj� si� ikony (Rys. 12.3).</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">&nbsp;&nbsp;</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_9092106840652749669.png"><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 12.3. An exemplary view of an add an association window.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Explanation of all icons and ways of displaying the names of objects are available under the help button (Figure 12.4)</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">&nbsp;&nbsp;</p>
<blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_926981779669492461.png"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 12.4. An exemplary view of the help window.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
</blockquote><p></p>
<p></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">After selecting the desired associations we save the changes. Associations are added to the associations panel immediately.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Another example of an association is associating a user with another BIKS object. In order to choose a user we need to start from the<i> BI Society</i> tab. Associating a user with another element requires determining the role that the user accepts against the item first. We then select the user we want to associate from the list of users and click add association. The application then asks us to select the type and sort of the association (Figure 11.5). The user may be <i>A Specialist, A Sponsor, An expert</i> or may be <i>responsible for a given element.</i> After choosing the role we proceed to choose the type of association. The association can be of two types:<i> Primary</i> or <i>Auxiliary</i>. When choosing the <i>Primary</i> type of association we have to define sub-options assigned to those kinds of associations.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
<blockquote><p dir="ltr" style="margin-top: 0pt; margin-bottom: 0pt; "><img src="fsb?get=file_233881372375386719.png"></p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">Figure 12.5. Type and sort of association selection window.</p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; "><br></p>
</blockquote><p dir="ltr" style="text-align: center; margin-top: 0pt; margin-bottom: 0pt; "> </p>
<p dir="ltr" style="text-align: left;margin-top: 0pt; margin-bottom: 0pt; ">This allows us to manage roles of other users assigned to that definition. A detailed description of these options can be found by clicking on the <i>Help</i> icon. After defining the role and type of association we click <i>next</i>. Remaining steps proceed as in the previously discussed case.</p>
<p></p>
')  
go

-- select * from bik_translation where kind = 'aprop' and lang = 'en'

delete from bik_translation where kind = 'aprop' and lang = 'en'
go

insert into bik_translation (code, txt, lang, kind) values ('welcomeMsgSimpleUser', '<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto; line-height:14.4pt"><p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto; line-height:14.4pt"><span style="font-size:12.0pt;mso-bidi-font-size:14.0pt; font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;; color:#094F8B;mso-fareast-language:PL">You are logged on as: <b>A regular user</b></span>
    <span style="font-family: Tahoma, sans-serif; "><o:p></o:p></span>
</p>
<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto; line-height:14.4pt"><span style="font-size:12.0pt;mso-bidi-font-size:14.0pt; font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;; color:#094F8B;mso-fareast-language:PL">Your privileges allow:</span>
    <span style="font-family: Tahoma, sans-serif; "><o:p></o:p></span>
</p>
<ul type="disc">  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL">Browsing data in the system</span>
        <span style="font-size:10.0pt;      mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:      &quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
    </li>
    <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL">Refreshing data in the system.</span>
        <span style="font-size:10.0pt;      mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:      &quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
    </li>
    <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL">Adding comments and ratings to content in the system.</span>
        <span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
    </li>
    <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL">Searching data in the system.</span>
        <span style="font-size:10.0pt;      mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:      &quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
    </li>
</ul></p>','en' , 'aprop')


insert into bik_translation (code, txt, lang, kind) values ('welcomeMsgExpertUser', '<p></p>
<p class="MsoNormal"></p>
<p class="MsoNormal"></p>
<p class="MsoNormal" style="background-color: white; background-position: initial initial; background-repeat: initial initial; "></p>
<p class="MsoNormal" style="background-color: white; background-position: initial initial; background-repeat: initial initial; "></p>
<p class="MsoNormal" style="background-color: white; background-position: initial initial; background-repeat: initial initial; "><p class="MsoNormal" style="background-color: white; background-position: initial initial; background-repeat: initial initial; "><b><span style="font-size:12.0pt; mso-bidi-font-size:14.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family: &quot;Times New Roman&quot;;color:#094F8B;mso-fareast-language:PL">You are logged on as: <b>Expert </b></span>
</b><span style="font-family: Tahoma, sans-serif; "><o:p></o:p></span>
</p>
  <p class="MsoNormal" style="background-color: white; background-position: initial initial; background-repeat: initial initial; "><b><span style="font-size:12.0pt; mso-bidi-font-size:14.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family: &quot;Times New Roman&quot;;color:#094F8B;mso-fareast-language:PL">Your privileges allow: </span>
</b><span style="font-family: Tahoma, sans-serif; "><o:p></o:p></span>
</p>
  <ul type="disc">  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Browsing data in the system.</span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Refreshing data in the system.</span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Adding comments and ratings to content in the system.</span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;      font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;      mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold"> Adding news. </span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Searching data in the system.</span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold"> Defining associations between various elements of the system.</span>
<span style="font-size:10.0pt;mso-bidi-font-size:      12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;      mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Adding descriptions and objects to system modules.</span>
<span style="font-size:10.0pt;mso-bidi-font-size:      12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;      mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Deleting elements contained in the system (except for blogging content posted by other users).</span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
</li>
  <li class="MsoNormal" style="color:#094F8B;mso-margin-top-alt:auto;mso-margin-bottom-alt:      auto;line-height:14.4pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt"><span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Arial Unicode MS&quot;;mso-fareast-language:PL;      mso-bidi-font-weight:bold">Editing content in the system (with the exception of blogging content posted by other users).</span>
<span style="font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;      mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:PL"><o:p></o:p></span>
</li>
 </ul></p>
<ul type="disc"> </ul><p></p>
<ul type="disc"> </ul><p></p>
<ul type="disc"> </ul><p></p>
<p></p>
<p></p>
<p></p>' ,'en' , 'aprop')


insert into bik_translation (code, txt, lang, kind) values ('myBIKSDemoMsg', '<h3>Welcome to BI Knowledge System - demo version</h3>
  BI Knowledge System is a tool that allows managing knowledge associated with Business Intelligence systems. This demo version of the application is a fully functional program that allows normal operation of the system and allows users to explore the benefits of its use. Changes saved by a user visiting the BI Knowledge System demo version will be removed at the end of each working day. All data contained in it is exemplary. The content of descriptions of objects may not be true and the correlation of users'' names is coincidental.
' ,'en' , 'aprop')


insert into bik_translation (code, txt, lang, kind) values ('myBIKSGeneralMsg', '<h3>Welcome to BI Knowledge System</h3> BI Knowledge System is a tool that allows managing knowledge associated with Business Intelligence systems.  Allows collection of specific knowledge related to the use of BI in the company in one place and in such a way that the lack of access to people with expertise does not cause stopping processes associated with decision-making. BI Knowledge System also helps to build a community of BI users giving them the opportunity of unfettered communication and exchange of knowledge.
' ,'en' , 'aprop')

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

exec sp_update_version '1.4.y1.3', '1.4.y1.4';
go
