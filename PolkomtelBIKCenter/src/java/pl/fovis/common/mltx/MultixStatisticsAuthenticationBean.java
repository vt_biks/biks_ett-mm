/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.mltx;

import java.io.Serializable;

/**
 *
 * @author lbiegniewski
 */
public class MultixStatisticsAuthenticationBean implements Serializable {

    public String login;
    public String pass;

    public MultixStatisticsAuthenticationBean() {
    }

    public MultixStatisticsAuthenticationBean(String login, String pass) {
        this.login = login;
        this.pass = pass;
    }
}
