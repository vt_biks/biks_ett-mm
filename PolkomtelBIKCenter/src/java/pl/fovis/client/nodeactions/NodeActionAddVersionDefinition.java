/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.ArticleBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.Pair;

/**
 *
 * @author beata
 */
public class NodeActionAddVersionDefinition extends NodeActionAddOrPromoteDefBase {

    @Override
    protected void callService(ArticleBean art, int action/* Set<String> depsToConnect*/, AsyncCallback<Pair<Integer, List<String>>> refreshAfterAction) {
        BIKClientSingletons.getService().createTreeNodeBiz(node, art,
                action, getNodeKindCode(), refreshAfterAction);
    }

    @Override
    public String getCaption() {
        return I18n.dodajWersjeDefinicji.get() /* I18N:  */;
    }

    @Override
    public boolean isEnabledByDesign() {
        return (isNodeNotLinkedAnyhowNorReadOnlyTreeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_GLOSSARY)
                || isNodeNotLinkedAnyhowNorReadOnlyTreeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_GLOSSARY_NOT_CORPO)) && isBikUserLoggedIn();
    }

//    @Override
//    protected boolean isEnabledWhenNotLinked() {
//        return node != null && treeBean.treeKind.equals("Glossary") &&
//                nodeKind.code.equals("GlossaryCategory");
//    }
    @Override
    protected String getActionSuccessMsg() {
        return I18n.dodanoNowaWersjeDefinicji.get() /* I18N:  */;
    }

    @Override
    protected String getNodeKindCode() {
        return BIKGWTConstants.NODE_KIND_GLOSSARY_VERSION;
    }

    @Override
    protected String getSubject() {
        return node.name;
    }

    @Override
    protected boolean isNewDefinition() {
        return true;
    }
}
