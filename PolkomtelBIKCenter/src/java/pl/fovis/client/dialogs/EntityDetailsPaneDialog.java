/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.EntityDetailsPaneForDialog;
import pl.fovis.client.ITabSelector;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Pair;

/**
 *
 * @author mgraczkowski
 *
 * #250 -Dziwnie długo trwa pokazanie dialogu podglądu obiektu - tego na lupce.
 * Po kliknięciu w lupkę dialog powinien pokazać się szybciej: Dzieje się tak,
 * ponieważ: - długie wczytywanie powiązanych z bazy, gdy jest ich duza ilość
 * (np Cellman) - niepotrzebne budowanie widget-ów, które nie są wyświetlane (w
 * MHW) ( i najprawdopodobniej dlatego dialog.center(); w
 * BaseActionOrCancelDialog trwa długo.)
 *
 */
public class EntityDetailsPaneDialog extends BaseActionOrCancelDialog {

    private int nodeId;
    private String tabId;
    protected Integer linkingParentId;
    protected Integer linkedNodeId;
    protected IContinuation refreshContinuation;

    @Override
    protected String getDialogCaption() {
        return I18n.informacjeSzczegolowe.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.przejdz.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaptionIcon() {
        return "link_arrows_white." + "gif" /* I18N: no */;
    }

    @Override
    protected String getCancelButtonCaption() {
        return I18n.zamknij.get() /* I18N:  */;
    }

    protected ITabSelector<Integer> getTabSelector() {
        return BIKClientSingletons.getTabSelector();
    }

    @Override
    protected void firstShowCentered() {
        //no-op
    }

    protected void callProperFirstShowCentered() {
        super.firstShowCentered();
    }

    protected void doAfterDataFetched() {
        callProperFirstShowCentered();
    }
    protected EntityDetailsPaneForDialog edpw;

    @Override
    protected void buildMainWidgets() {
        edpw = new EntityDetailsPaneForDialog(GWTUtils.wrapAsDeferred(new IContinuation() {

            public void doIt() {
                //recenterDialog();
                //callProperFirstShowCentered();
                doAfterDataFetched();
                edpw.setHeightBodyEdp();
                if (edpw.data != null && edpw.data.node != null && edpw.data.node.name != null) {
                    dialog.setHTML(BaseUtils.encodeForHTMLTag(edpw.data.node.name) + ": " + getDialogCaption());
                }
//                Scheduler.get().scheduleDeferred(new ScheduledCommand() {
//
//                    public void execute() {
//                        recenterDialog();
//                    }
//                });
            }
        }),
                GWTUtils.wrapAsDeferred(new IContinuation() {

                    public void doIt() {
                        recenterDialog();
                        if (!BaseUtils.isStrEmptyOrWhiteSpace(BIKClientSingletons.visScript)) {
                            ScriptInjector.fromString(BIKClientSingletons.visScript).setWindow(ScriptInjector.TOP_WINDOW).inject();
                        }
//                        ScriptInjector.fromString("var container = document.getElementById('gwt-debug-chart-div'); var label20151231 = {content: '1.4157'}; var label20160630 = {content: '1.0003'}; var label20161231 = {content: '0.9644'}; var label20170221 = {content: '0.7123'};  var items = [{x:'2017-02-21',y:0.7123,label:label20170221},{x:'2016-12-31',y:0.9644,label:label20161231},{x:'2016-06-30',y:1.0003,label:label20160630},{x:'2015-12-31',y:1.4157,label:label20151231}]; var dataset = new vis.DataSet(items);var options = {start:'2015-06-30',end: '2020-12-31'};var graph2d = new vis.Graph2d(container, dataset, options);").setWindow(ScriptInjector.TOP_WINDOW).inject();
                    }
                }), refreshContinuation);

        Pair<Integer, Integer> sizes = getSizeForScrollPanel();
        edpw.setWidgetHeight(sizes.v2 - 54);

        edpw.setNodeIdAndFetchData(linkedNodeId != null ? linkedNodeId : nodeId, linkingParentId);
        Widget widget = edpw.getWidget();

        ScrollPanel scPanel = new ScrollPanel(widget);
//        scPanel.setHeight(Window.getClientHeight() * 0.92  - 50 + "px");
//        scPanel.setWidth(Window.getClientWidth() * 0.97 + "px");

        scPanel.setPixelSize(sizes.v1, sizes.v2 - 50);
        //widget.setPixelSize(sizes.v1, sizes.v2 - 50);

        //setScrollPanelSize(scPanel, 50);
        //widget.setHeight( "px");
        //setScrollPanelSize(widget, 50);
        // widget.setWidth("800px");
        main.add(scPanel);
        //dialog.center();
    }

    @Override
    protected void doAction() {
        getTabSelector().submitNewTabSelection(tabId, nodeId, true);
    }

    public void buildAndShowDialog(int nodeId, Integer linkingParentId, String tabId) {
        this.nodeId = nodeId;
        this.linkingParentId = linkingParentId;
        this.tabId = tabId;
        super.buildAndShowDialog();
        if (refreshContinuation != null) {
            refreshContinuation.doIt();
        }
    }

    public void buildAndShowDialog(int nodeId, Integer linkingParentId, Integer linkedNodeId, String tabId) {
        this.linkedNodeId = linkedNodeId;
        buildAndShowDialog(nodeId, linkingParentId, tabId);
    }

    //ww: nie rób focusa wcale
    @Override
    protected void tryFocusFirstWidget() {
        // no-op
    }

    public void setRefreshContinuation(IContinuation refreshContinuation) {
        this.refreshContinuation = refreshContinuation;
    }

    @Override
    protected Pair<Integer, Integer> getSizeForScrollPanel() {

        Pair<Integer, Integer> weightHight = BIKClientSingletons.customDialogPercentSize();
        if (weightHight == null) {
            return super.getSizeForScrollPanel();
        }

        return new Pair<Integer, Integer>((int) (Window.getClientWidth() * weightHight.v1 / 100),
                (int) (Window.getClientHeight() * weightHight.v2 / 100));
    }
}
