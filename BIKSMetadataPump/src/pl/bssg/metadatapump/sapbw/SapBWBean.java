/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbw;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class SapBWBean implements Serializable {

    public String infoCube;
    public Integer iplevel;
    public String infoName;
    public String objName;
    public String fileType;
    public String infoType;
    public String parent;
    public String infoDescr;
    public String objDescr;
//    public String endLevel; // technical field - ignore
    public String infoArea;
    public String objType;
    public String queryUpdate;
    public String queryOwner;
    public String queryLastEdit;

    public SapBWBean() {
    }

//    INFOCUBE,IPLEVEL,PARTCUBE,IOBJNM,FILETYPE,TCTBWOTYPE,INFOPROVUP,INFOPROVTXT,IOBJTXT,ENDLEVEL,INFOAREA,IOBJTYPE,QUERYUPDATE,QUERYOWNER,QUERYEDIT
    public SapBWBean(String infoCube, Integer iplevel, String infoName, String objName, String fileType, String infoType, String parent, String infoDescr, String objDescr, String infoArea, String objType, String queryUpdate, String queryOwner, String queryLastEdit) {
        this.infoCube = infoCube;
        this.iplevel = iplevel;
        this.infoName = infoName;
        this.objName = objName;
        this.fileType = fileType;
        this.infoType = infoType;
        this.parent = parent;
        this.infoDescr = infoDescr;
        this.objDescr = objDescr;
        this.infoArea = infoArea;
        this.objType = objType;
        this.queryUpdate = queryUpdate;
        this.queryOwner = queryOwner;
        this.queryLastEdit = queryLastEdit;
    }
}
