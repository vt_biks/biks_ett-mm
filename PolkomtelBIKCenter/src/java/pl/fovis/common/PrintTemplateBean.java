/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author bfechner
 */
public class PrintTemplateBean implements Serializable {

    public String template;
    public Integer maxLevel;
    public boolean showAuthor;
    public boolean showPageNumber;
    public boolean showNewLine;
    public Integer templateNodeId;
}
