/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.foxygwtcommons.BasePickerDialog;
import java.util.Map;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BlogAuthorBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class PickAuthorBlogDialog extends BasePickerDialog<BlogAuthorBean> {

    @Override
    protected String getDialogCaption() {
        return I18n.zmienAdministratoraBloga.get() /* I18N:  */;
    }

    @Override
    protected void setupListGridRecord(BlogAuthorBean item, Map<String, Object> record) {
        record.put("name" /* I18N: no */, BIKCenterUtils.getBikUserNodeName(item.userName, item.userName/*, item.activ, item.oldLoginName*/));//bf:-->
        record.put("id" /* I18N: no */, Integer.toString(item.userId));
    }

    @Override
    protected String[] createFields() {
        return createFields("name=" /* i18n: no */ + I18n.autor.get() /* I18N:  */, "id=id" /* I18N: no */);
    }

    @Override
    protected String getIdFieldName() {
        return "id" /* I18N: no */;
    }

    @Override
    protected String getListBoxLabelCaption() {
        return I18n.autorzy.get() /* I18N:  */+ ":";
    }
}
