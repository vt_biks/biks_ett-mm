/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.CheckBox;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;

/**
 *
 * @author ctran
 */
public abstract class SearchResultsExportOptionsDialog extends SelectExportOptionsDialog {

    protected CheckBox nameCb = new CheckBox(I18n.nazwa.get());
    protected CheckBox nodeKindCb = new CheckBox(I18n.typWezla.get());
    protected CheckBox descrCb = new CheckBox(I18n.opis.get());
    protected CheckBox objPathCb = new CheckBox(I18n.sciezkaDoObiektu.get());
    protected CheckBox evaluationCb = new CheckBox(I18n.oceny.get());
    protected CheckBox appearanceCb = new CheckBox(I18n.wystepowanie.get());
    protected String ticketForFullSearch;
    protected String query;

    public SearchResultsExportOptionsDialog(String ticketForFullSearch, String query) {
        this.ticketForFullSearch = ticketForFullSearch;
        this.query = query;
    }

    @Override
    public void buildAdditionalOptions() {
        main.add(nameCb);
        nameCb.setValue(true);
        nameCb.setEnabled(false);
        main.add(nodeKindCb);
        main.add(descrCb);
        main.add(objPathCb);
        main.add(evaluationCb);
        main.add(appearanceCb);
    }

    @Override
    public String generateDownloadLink() {
        StringBuilder sb = new StringBuilder(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet());
        sb.append("?").append(BIKConstants.SEARCH_RESULTS_EXPORT_PARAM_NAME).append("=").append(selectFormatLb.getItemText(selectFormatLb.getSelectedIndex())).append("&")
                .append(BIKConstants.SEARCH_PHRASE_PARAM_NAME).append("=").append(query.replace(" ", "%20")).append("&")
                .append(BIKConstants.SEARCH_REQUEST_TICKET_PARAM_NAME).append("=").append(ticketForFullSearch).append("&")
                .append(BIKConstants.EXPORT_OPTIONS_PARAM_NAME).append("=").append(BIKConstants.EXPORT_OPTIONS_NAME)
                .append(nodeKindCb.getValue() ? "," + BIKConstants.EXPORT_OPTIONS_NODE_KIND : "")
                .append(descrCb.getValue() ? "," + BIKConstants.EXPORT_OPTIONS_DESCR : "")
                .append(objPathCb.getValue() ? "," + BIKConstants.EXPORT_OPTIONS_OBJ_PATH : "")
                .append(evaluationCb.getValue() ? "," + BIKConstants.EXPORT_OPTIONS_EVALUATION : "")
                .append(appearanceCb.getValue() ? "," + BIKConstants.EXPORT_OPTIONS_APPEARANCE : "");
        return sb.toString();
    }
}
