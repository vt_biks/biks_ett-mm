/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.mailing;

import commonlib.LameUtils;
import commonlib.ThreadedLameLoggingEnvironment;
import javax.mail.MessagingException;
import pl.trzy0.foxy.commons.FoxyRuntimeException;
import pl.trzy0.foxy.commons.MailServerConfig;
import simplelib.IContinuationWithReturn;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class MailSenderThread extends Thread {

    private static final long SLEEP_FOR_NEW_MAIL_MILLIS = 3 * 1000;//3 * 60 * 1000;
    private static ILameLogger logger = LameLoggerFactory.getLogger(MailSenderThread.class);
    private IMailingBizLogic bizLogic;
    private MailServerConfig mailServerConfig;

    static {
        ThreadedLameLoggingEnvironment.initLameLoggingEnvironment();
    }

    public MailSenderThread(MailServerConfig mailServerConfig, IMailingBizLogic bizLogic) {
        this.bizLogic = bizLogic;
        this.mailServerConfig = mailServerConfig;
        //if (logger.isDebugEnabled()) logger.debug("host=" + mailServerConfig.host + " .. ssl=" + mailServerConfig.ssl);
    }

    protected void sendMyMail(String recipient, String subject, String message) {
        String[] sendTo0 = LameUtils.splitBySep(recipient, ",").toArray(new String[0]);
        String[] sendTo = new String[sendTo0.length];

        for (int i = 0; i < sendTo0.length; i++) {
            String mailAddr = sendTo0[i];
            int pos = mailAddr.lastIndexOf("#");
            if (pos > -1) {
                mailAddr = mailAddr.substring(0, pos);
            }
            sendTo[i] = mailAddr;
        }

        try {
            SendMail.sendMessage(
                    mailServerConfig.host, mailServerConfig.port,
                    mailServerConfig.user, mailServerConfig.password,
                    sendTo, subject, message,
                    mailServerConfig.fromAddress, mailServerConfig.ssl,
                    null);
        } catch (MessagingException ex) {
            throw new FoxyRuntimeException("error sending mail to: " + recipient, ex);
        }
    }

    private void singlePass() {
        //bizLogic.debugCheckIfThereIsAnyMailToBeSend("mailThread");

        MailToSendBean mail = null;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("BEFORE bizLogic.getMailToSend()");
            }
            mail = bizLogic.getMailToSend(!logger.isDebugEnabled());
            if (logger.isDebugEnabled()) {
                logger.debug("AFTER bizLogic.getMailToSend(), has mail: " + (mail != null));
            }
        } catch (Exception e) {
            bizLogic.finishWorkUnit(false);
            e.printStackTrace();
        }
        if (mail == null) {
            try {
                bizLogic.finishWorkUnit(true);
                //wait(SLEEP_FOR_NEW_MAIL_MILLIS);
                //PostgresConnection.recycleThreadConnection();
                sleep(SLEEP_FOR_NEW_MAIL_MILLIS);
            } catch (InterruptedException ex) {
                if (logger.isWarnEnabled()) {
                    logger.warn("%%% mail thread interrupted %%%");
                }
                System.out.println(Thread.currentThread().getName() + ": --- %%% mail thread interrupted %%% ---");
                Thread.currentThread().interrupt();
                System.out.println(Thread.currentThread().getName() + ": --- mail thread: isInterrupted=" + Thread.currentThread().isInterrupted() + " %%% ---");
                return;
            }
        } else {
            String errMsg = null;
            try {
                try {
                    if (logger.isDebugEnabled()) {
                        logger.debug("send mail starts");
                    }
                    sendMyMail(mail.recipient, mail.subject, mail.message);
                } catch (Exception ex) {
                    errMsg = ex.toString(); //ex.getClass().getName() + ": " + ex.getMessage();
                    ex.printStackTrace();
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("send mail done, errMsg=" + errMsg);
                }
                try {
                    bizLogic.setMailSendStatus(mail.id, errMsg);
                } catch (Exception ex2) {
                    errMsg = ex2.toString(); //ex2.getClass().getName() + ": " + ex2.getMessage();
                    ex2.printStackTrace();
                }
            } finally {
                bizLogic.finishWorkUnit(//errMsg == null
                        true);
            }
            if (errMsg != null) {
                logger.error("error in mail sender thread: " + errMsg);
            }
        }
    }

    @Override
    public void run() {
        if (logger.isInfoEnabled()) {
            logger.info("------ mail thread starting (run) ------");
        }
//        try {
//            sleep(10000);
//        } catch (InterruptedException ex) {
//        }

        IContinuationWithReturn<Void> c = new IContinuationWithReturn<Void>() {

            public Void doIt() {
                singlePass();
                return null;
            }
        };

        while (!Thread.interrupted()) {
            LameLoggerFactory.runWithoutLogging(true, c);
        }

        if (logger.isInfoEnabled()) {
            logger.info("------ mail thread finished ------");
        }
    }
}
