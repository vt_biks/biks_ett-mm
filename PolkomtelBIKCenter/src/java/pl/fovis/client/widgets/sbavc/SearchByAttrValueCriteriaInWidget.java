/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.widgets.sbavc;

import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import pl.fovis.common.sbavc.SearchByAttrValueCriteriaIn;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class SearchByAttrValueCriteriaInWidget extends SearchByAttrValueCriteriaWidgetBase {

    protected Widget inWidget;

    @Override
    protected void createOptWidget() {
        row.setWidget(0, 4, inWidget = createWidgetBox());
    }

    @Override
    public ISearchByAttrValueCriteria provideBean() {
        return (isValidInput() ? new SearchByAttrValueCriteriaIn(getSelectedAttrId(), getSelectedAttrCaption(), getWidgetValueAsString(inWidget), negationChbx.getValue()) : null);
    }

    @Override
    protected boolean isValidInput() {
        String value = getWidgetValueAsString(inWidget);
        if (manager.isDisplayedAsNumber(getSelectedAttrId())) {
            List<String> values = BaseUtils.splitBySep(value, ",");
            for (String s : values) {
                if (BaseUtils.tryParseNumber(s) == null) {
                    return false;
                }
            }
        }
        return true;
    }

}
