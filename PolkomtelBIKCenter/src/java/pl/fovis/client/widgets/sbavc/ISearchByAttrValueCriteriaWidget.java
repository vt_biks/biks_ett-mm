/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.widgets.sbavc;

import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.common.AttrSearchableBean;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;

/**
 *
 * @author tflorczak
 */
public interface ISearchByAttrValueCriteriaWidget {

    public void setup(SearchByAttrValueCriteriaManager manager, List<AttrSearchableBean> attrs);

    public void populateWidgets(int attrId, boolean isNegation, int criteriumIndex);

    public int getSelectedAttrId();

    public boolean isNegation();

    public int getCriteriumIndex();

    public boolean populateAttributeListBox(List<AttrSearchableBean> attrs);

    public ISearchByAttrValueCriteria provideBean();

    public Widget getWidget();

    public void setCriteriaId(int id);

}
