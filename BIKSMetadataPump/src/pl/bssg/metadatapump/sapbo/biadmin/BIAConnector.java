/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.biadmin;

import com.businessobjects.sdk.plugin.desktop.webi.IWebi;
import com.businessobjects.sdk.plugin.desktop.webi.IWebiFormatOptions;
import com.crystaldecisions.sdk.exception.SDKException;
import com.crystaldecisions.sdk.framework.CrystalEnterprise;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.framework.ISessionMgr;
import com.crystaldecisions.sdk.occa.infostore.CeScheduleType;
import com.crystaldecisions.sdk.occa.infostore.IDestination;
import com.crystaldecisions.sdk.occa.infostore.IDestinationPlugin;
import com.crystaldecisions.sdk.occa.infostore.IDestinations;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import com.crystaldecisions.sdk.occa.infostore.ISchedulingInfo;
import com.crystaldecisions.sdk.plugin.destination.diskunmanaged.IDiskUnmanagedOptions;
import com.crystaldecisions.sdk.plugin.destination.smtp.ISMTPOptions;
import com.crystaldecisions.sdk.properties.IProperties;
import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static pl.bssg.metadatapump.common.PumpConstants.BO_CONN_KINDS;
import static pl.bssg.metadatapump.common.PumpConstants.BO_REPORT_KINDS;
import static pl.bssg.metadatapump.common.PumpConstants.BO_UNIV_KINDS;
import pl.bssg.metadatapump.common.SAPBOObjectBean;
import pl.bssg.metadatapump.common.SAPBOScheduleBean;
import static pl.bssg.metadatapump.sapbo.biadmin.ObjectInfo.getBaseInfoForObject;
import static pl.bssg.metadatapump.sapbo.biadmin.ObjectInfo.getInfoForObject;
import static pl.bssg.metadatapump.sapbo.biadmin.ReportScheduleInfo.getOneSchedule;
import pl.bssg.metadatapump.sapbo.javasdk.SapBOPump;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.LameRuntimeException;
import simplelib.SimpleDateUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class BIAConnector implements IBIAConnector {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected String host;
    protected String userName;
    protected String password;
    protected IEnterpriseSession enterpriseSession = null;
    protected IInfoStore iStore;
    protected Integer rootFolderId;
    protected Integer userFolderId;
    protected Integer univFolderId;
    protected Integer connFolderId;
    protected boolean stopQuering = false;

    public BIAConnector(String host, String user, String password) {
        this.host = host;
        this.userName = user;
        this.password = password;
    }

    @Override
    public void initAndLogin() {
        if (logger.isInfoEnabled()) {
            logger.info("Trying to login...");
        }
        try {
            ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
            enterpriseSession = sessionMgr.logon(userName, password, host, "secEnterprise");
            iStore = (IInfoStore) enterpriseSession.getService("InfoStore");
            IInfoObjects repCorpoFolder = iStore.query("select si_id from ci_infoobjects where si_cuid = '" + SapBOPump.FOLDER_CUID_ROOT + "'");
            rootFolderId = ((IInfoObject) repCorpoFolder.get(0)).getID();
            IInfoObjects repUserFolder = iStore.query("select si_id from ci_infoobjects where si_cuid = '" + SapBOPump.FOLDER_CUID_USER + "'");
            userFolderId = ((IInfoObject) repUserFolder.get(0)).getID();
            IInfoObjects univFolder = iStore.query("select si_id from ci_appobjects where si_cuid = '" + SapBOPump.FOLDER_CUID_UNIVERSES + "'");
            univFolderId = ((IInfoObject) univFolder.get(0)).getID();
            IInfoObjects connFolder = iStore.query("select si_id from ci_appobjects where si_cuid = '" + SapBOPump.FOLDER_CUID_CONNECTIONS + "'");
            connFolderId = ((IInfoObject) connFolder.get(0)).getID();
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in initAndLogin", e);
            }
        }
    }

    @Override
    public void logout() {
        if (enterpriseSession != null) {
            enterpriseSession.logoff();
        }
    }

    @Override
    public boolean isServiceInit() {
        return iStore != null;
    }

    @Override
    public void pauseInstances(List<Integer> instancesToPause) {
        setFlagsForObjects(instancesToPause, ISchedulingInfo.ScheduleFlags.PAUSE);
    }

    @Override
    public void resumeInstances(List<Integer> instancesToResume) {
        setFlagsForObjects(instancesToResume, ISchedulingInfo.ScheduleFlags.RESUME);
    }

    @Override
    public void deleteInstances(List<Integer> instancesToDelete) {
        if (BaseUtils.isCollectionEmpty(instancesToDelete)) {
            return;
        }
        String incestances = BaseUtils.mergeWithSepEx(instancesToDelete, ",");
        String queryString = "SELECT * FROM CI_INFOOBJECTS WHERE SI_INSTANCE = 1 AND SI_ID in (" + incestances + ")";
        try {
            IInfoObjects boInfoObjects = iStore.query(queryString);
            for (Object boInfoObject : boInfoObjects) {
                IInfoObject boIInfoObject = (IInfoObject) boInfoObject;
                boIInfoObject.deleteNow();
                boInfoObjects.delete(boIInfoObject);
            }
            iStore.commit(boInfoObjects);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in deleteInstances: " + incestances, e);
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void rescheduleInstances(List<Integer> instancesToReschedule, SAPBOScheduleBean params) {
        if (BaseUtils.isCollectionEmpty(instancesToReschedule)) {
            return;
        }
        String incestances = BaseUtils.mergeWithSepEx(instancesToReschedule, ",");
        String queryString = "SELECT * FROM CI_INFOOBJECTS WHERE SI_INSTANCE = 1 AND SI_ID in (" + incestances + ")";
        try {
            IInfoObjects boInfoObjects = iStore.query(queryString);
            for (Object boInfoObject : boInfoObjects) {
                IInfoObject boIInfoObject = (IInfoObject) boInfoObject;

                ISchedulingInfo schedulingInfo = boIInfoObject.getSchedulingInfo();
                if (params == null) {
                    schedulingInfo.setType(CeScheduleType.ONCE);
                    schedulingInfo.setRightNow(true);
                    schedulingInfo.setEndDate(SimpleDateUtils.addDays(new Date(), 1));
                } else { // są inne parametry
                    // cykl
                    switch (params.type) {
                        case -1: // now
                            schedulingInfo.setType(CeScheduleType.ONCE);
                            schedulingInfo.setRightNow(true);
                            schedulingInfo.setEndDate(SimpleDateUtils.addDays(new Date(), 1));
                            break;
                        case 1: // once
                            schedulingInfo.setType(CeScheduleType.ONCE);
                            schedulingInfo.setBeginDate(params.started);
                            schedulingInfo.setEndDate(params.ended);
                            break;
                        case 2: // dayly
                            schedulingInfo.setType(CeScheduleType.DAILY);
                            schedulingInfo.setIntervalDays(params.intervalDays);
                            schedulingInfo.setBeginDate(params.started);
                            schedulingInfo.setEndDate(params.ended);
                            break;
                        case 4: // monthly
                            schedulingInfo.setType(CeScheduleType.MONTHLY);
                            schedulingInfo.setIntervalMonths(params.intervalMonths);
                            schedulingInfo.setBeginDate(params.started);
                            schedulingInfo.setEndDate(params.ended);
                            break;
                    }
                    // format
                    String kind = boIInfoObject.properties().getString("SI_KIND");
                    int formatType = 1;
                    if ("Webi".equals(kind)) {
                        IWebi boReport = (IWebi) boIInfoObject;
                        IWebiFormatOptions boReportFormatOptions = boReport.getWebiFormatOptions();
                        if (params.format.equals("Pdf")) {
                            formatType = IWebiFormatOptions.CeWebiFormat.PDF;
                        } else if (params.format.equals("Excel")) {
                            formatType = IWebiFormatOptions.CeWebiFormat.EXCEL;
                        } else {
                            formatType = IWebiFormatOptions.CeWebiFormat.Webi;
                        }
                        boReportFormatOptions.setFormat(formatType);
                    }
                    // destination
                    if ("Smtp".equals(params.destination)) {
                        IDestinationPlugin boDestinationPlugin = (IDestinationPlugin) iStore.query("Select Top 1 * from CI_SYSTEMOBJECTS Where SI_NAME = 'CrystalEnterprise.SMTP'").get(0);
                        ISMTPOptions smtpOptions = (ISMTPOptions) boDestinationPlugin.getScheduleOptions();
                        smtpOptions.setSenderAddress(params.emailFromAddresses);
                        smtpOptions.setSubject(params.emailTitle);
                        smtpOptions.setMessage(params.emailMessage);
                        List recipients = smtpOptions.getToAddresses();
                        List<String> rec = BaseUtils.splitBySep(params.emailAddresses.replace(",", ";"), ";");
                        for (String oneRec : rec) {
                            if (oneRec.contains("@")) {
                                recipients.add(oneRec);
                            }
                        }
                        IDestinations boDestinations = schedulingInfo.getDestinations();
                        IDestination boDestination = boDestinations.add("CrystalEnterprise.Smtp");
                        boDestination.setFromPlugin(boDestinationPlugin);
                    } else if ("DiskUnmanaged".equals(params.destination)) {
                        IDestinationPlugin boDestinationPlugin = (IDestinationPlugin) iStore.query("SELECT TOP 1 * FROM CI_SYSTEMOBJECTS WHERE SI_NAME='CrystalEnterprise.DiskUnmanaged'").get(0);
                        IDiskUnmanagedOptions diskUnmanagedOptions = (IDiskUnmanagedOptions) boDestinationPlugin.getScheduleOptions();
                        diskUnmanagedOptions.setUserName(params.destinationUser);
                        diskUnmanagedOptions.setPassword(params.destinationPw);
                        List listDestination = diskUnmanagedOptions.getDestinationFiles();
                        listDestination.add(params.destinationFolder);
                        IDestinations boDestinations = schedulingInfo.getDestinations();
                        IDestination boDestination = boDestinations.add("CrystalEnterprise.DiskUnmanaged");
                        boDestination.setFromPlugin(boDestinationPlugin);
                    }
                }
                while (!schedulingInfo.getDependencies().isEmpty()) {
                    schedulingInfo.getDependencies().clear();;
                }
            }
            iStore.schedule(boInfoObjects);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in rescheduleInstances: " + incestances, e);
            }
        }
    }

    protected void setFlagsForObjects(List<Integer> instances, int flag) {
        if (BaseUtils.isCollectionEmpty(instances)) {
            return;
        }
        String incestances = BaseUtils.mergeWithSepEx(instances, ",");
        String queryString = "SELECT * FROM CI_INFOOBJECTS WHERE SI_INSTANCE = 1 AND SI_ID in (" + incestances + ")";
        try {
            IInfoObjects boInfoObjects = iStore.query(queryString);
            for (Object boInfoObject : boInfoObjects) {
                IInfoObject boIInfoObject = (IInfoObject) boInfoObject;
                boIInfoObject.getSchedulingInfo().setFlags(flag);
            }
            iStore.commit(boInfoObjects);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in setFlagsForObjects: " + incestances, e);
            }
        }
    }

    @Override
    public void changeBIANameAndDescr(Set<SAPBOObjectBean> objs, Integer manageType) {
        try {
            for (SAPBOObjectBean obj : objs) {
                IInfoObjects results = iStore.query("select * from " + getSapTableName(manageType) + " where si_id = " + obj.id);
                IInfoObject infoObj = (IInfoObject) results.get(0);
                infoObj.setTitle(obj.name);
                infoObj.setDescription(obj.description);
                iStore.commit(results);
            }
        } catch (SDKException ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in changeBIAOwner", ex);
            }
            throw new LameRuntimeException("Error in changeBIAProperties", ex);
        }
    }

    @Override
    public void changeBIAOwner(List<Integer> objs, final SAPBOObjectBean newOwner) {
        changeBIAProperties(objs, newOwner, new IParametrizedContinuation<IInfoObject>() {

            @Override
            public void doIt(IInfoObject param) {
                IProperties properties = param.properties();
                properties.setProperty("SI_OWNERID", newOwner.id);
                properties.setProperty("SI_OWNER", newOwner.name);
            }
        });
    }

    protected void changeBIAProperties(List<Integer> objs, SAPBOObjectBean newObj, IParametrizedContinuation<IInfoObject> param) {
        try {
            Integer siID = null;
            String whereString = "SI_ID in (" + BaseUtils.mergeWithSepEx(objs, ",") + ")";
            while (true) {
                StringBuilder sbQuery = new StringBuilder("select top 100 * from ");
                sbQuery.append(getSapTableName(newObj.manageType));
                if (siID == null) {
                    sbQuery.append(" where ").append(whereString);
                } else {
                    sbQuery.append(" where (si_id > ").append(siID).append(") AND ").append(whereString);
                }
                sbQuery.append(" order by si_id");
                String queryText = sbQuery.toString();
                if (logger.isInfoEnabled()) {
                    logger.info("Executing query: " + queryText);
                }
                IInfoObjects results = iStore.query(queryText);

                for (int i = 0; i < results.size(); i++) {
                    IInfoObject obj = (IInfoObject) results.get(i);
                    param.doIt(obj);
                }
                iStore.commit(results);
                // pobieramy top 1000
                if (results.size() < 100 || objs.size() >= 1000) {
                    break;
                }
            }
        } catch (SDKException ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in changeBIAOwner", ex);
            }
            throw new LameRuntimeException("Error in changeBIAProperties", ex);
        }
    }

    @Override
    public List<SAPBOObjectBean> getBIAUsers() {
        try {
            List<SAPBOObjectBean> objs = new ArrayList<SAPBOObjectBean>();
            IInfoObjects users = iStore.query("select * from ci_systemobjects where si_kind = 'User'");

            for (Object user : users) {
                IInfoObject obj = (IInfoObject) user;
                SAPBOObjectBean infoForObject = getInfoForObject(obj, iStore);
                objs.add(infoForObject);
            }
            return objs;
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in getBIAUsers", ex);
            }
            throw new LameRuntimeException("Error in getBIAUsers", ex);
        }
    }

    @Override
    public List<SAPBOObjectBean> getBIAReports() {
        SAPBOObjectBean filter = new SAPBOObjectBean();
        filter.manageType = 1;
        filter.kindsToFilter = new ArrayList<String>(BO_REPORT_KINDS);
        return getBIAObjectsInner(filter);
    }

    @Override
    public List<SAPBOObjectBean> getBIAUniverses() {
        SAPBOObjectBean filter = new SAPBOObjectBean();
        filter.manageType = 2;
        filter.kindsToFilter = new ArrayList<String>(BO_UNIV_KINDS);
        return getBIAObjectsInner(filter);
    }

    @Override
    public List<SAPBOObjectBean> getBIAConnections() {
        SAPBOObjectBean filter = new SAPBOObjectBean();
        filter.manageType = 3;
        filter.kindsToFilter = new ArrayList<String>(BO_CONN_KINDS);
        return getBIAObjectsInner(filter);
    }

    @Override
    public List<SAPBOObjectBean> getBIAObjects(SAPBOObjectBean filter) {
        return getBIAObjectsInner(filter);
    }

    protected String getSapTableName(int manageType) {
        String sapTableName = null;
        switch (manageType) {
            case 1:
                sapTableName = "ci_infoobjects";
                break;
            case 2:
            case 3:
                sapTableName = "ci_appobjects";
                break;
            case 4:
                sapTableName = "ci_systemobjects";
                break;
        }
        return sapTableName;
    }

    /**
     *
     * @param filter - Bean with filter values, added to where clause
     * @return list of SAP BI Objects
     */
    public List<SAPBOObjectBean> getBIAObjectsInner(SAPBOObjectBean filter) {
        allowQuering();
        try {
            String whereQuery = createObjectsWhereString(filter);
            if (logger.isInfoEnabled()) {
                logger.info("Getting objects...");
            }
            List<SAPBOObjectBean> objs = new ArrayList<SAPBOObjectBean>();
            String sapTableName = getSapTableName(filter.manageType);
            Integer siID = null;
            while (true) {
                StringBuilder sbQuery = new StringBuilder("select top 100 * from ");
                sbQuery.append(sapTableName);
                if (siID == null) {
                    sbQuery.append(" where ").append(whereQuery);
                } else {
                    sbQuery.append(" where (si_id > ").append(siID).append(") AND ").append(whereQuery);
                }
                sbQuery.append(" order by si_id");
                String queryText = sbQuery.toString();
                if (logger.isInfoEnabled()) {
                    logger.info("Executing query: " + queryText);
                }
                IInfoObjects results = iStore.query(queryText);

                for (int i = 0; i < results.size(); i++) {
                    IInfoObject obj = (IInfoObject) results.get(i);
                    SAPBOObjectBean infoForObject = getInfoForObject(obj, iStore);
                    objs.add(infoForObject);
                    siID = obj.getID();
                }
                if (results.size() < 100 /*|| objs.size() >= 1000*/) {
                    break;
                }
                if (stopQuering) {
                    allowQuering();
                    System.out.println("Stopping BO Query. Last Query: " + queryText);
                    objs.clear();
                    return objs;
                }
            }
//            if (!isBasicMode) {
            // get location
            Map<Integer, SAPBOObjectBean> objMap = createFolderMap(sapTableName, false);
            for (SAPBOObjectBean obj : objs) {
                StringBuilder locationBuilder = new StringBuilder();
                fixLocation(obj.name, obj.parentId, objMap, locationBuilder, true);
                obj.location = locationBuilder.toString();
            }
//            }
            return objs;
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in getBIAObjects", ex);
            }
            throw new LameRuntimeException("Error in getBIAObjects", ex);
        }
    }

    protected Map<Integer, SAPBOObjectBean> createFolderMap(String sapTableName, boolean withWebi) {
        Map<Integer, SAPBOObjectBean> objMap = new HashMap<Integer, SAPBOObjectBean>();
        Integer siID = null;
        String whereQuery = "SI_KIND in ('Folder', 'FavoritesFolder'" + (withWebi ? ", 'Webi', 'Publication', 'Excel', 'FullClient', 'Pdf') and SI_INSTANCE = 0" : ")");
        try {
            while (true) {
                StringBuilder sbQuery = new StringBuilder("select top 100 * from ");
                sbQuery.append(sapTableName);
                if (siID == null) {
                    sbQuery.append(" where ").append(whereQuery);
                } else {
                    sbQuery.append(" where (si_id > ").append(siID).append(") AND ").append(whereQuery);
                }
                sbQuery.append(" order by si_id");
                String queryText = sbQuery.toString();
                if (logger.isInfoEnabled()) {
                    logger.info("createFolderMap - executing query: " + queryText);
                }
                IInfoObjects results = iStore.query(queryText);

                for (int i = 0; i < results.size(); i++) {
                    IInfoObject obj = (IInfoObject) results.get(i);
                    SAPBOObjectBean infoForObject = getBaseInfoForObject(obj, iStore);
                    objMap.put(infoForObject.id, infoForObject);
                    siID = obj.getID();
                }
                if (results.size() < 100 /*|| objs.size() >= 1000*/) {
                    break;
                }
            }
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in createFolderMap", ex);
            }
            throw new LameRuntimeException("Error in createFolderMap", ex);
        }
        return objMap;
    }

    protected void fixLocation(String name, Integer parentId, Map<Integer, SAPBOObjectBean> objMap, StringBuilder sb, boolean dropLastName) {
        SAPBOObjectBean parent = objMap.get(parentId);
        if (parent != null) {
            fixLocation(parent.name, parent.parentId, objMap, sb, false);
            if (!dropLastName) {
                sb.append("/").append(name);
            }
        } else {
            sb.append("/").append(name);
        }
    }

    @Override
    public List<SAPBOObjectBean> getBIAJobs() {
        List<SAPBOObjectBean> beansToReturn = new ArrayList<SAPBOObjectBean>();
        try {
            IInfoObjects results = iStore.query("select top 100 * from ci_infoobjects where si_kind = 'LCMJob' order by SI_UPDATE_TS desc");
            for (int i = 0; i < results.size(); i++) {
                IInfoObject obj = (IInfoObject) results.get(i);
                beansToReturn.add(getBaseInfoForObject(obj, iStore));
            }
            return beansToReturn;
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in getBIAJobs", ex);
            }
            throw new LameRuntimeException("Error in getBIAJobs", ex);
        }
    }

    @Override
    public void deleteBIAJobs(String prefix) {
        try {
            IInfoObjects results = iStore.query("select top 100 * from ci_infoobjects where si_kind = 'LCMJob' and si_name like '" + prefix + "%'");
            for (Object boInfoObject : results) {
                IInfoObject boIInfoObject = (IInfoObject) boInfoObject;
                boIInfoObject.deleteNow();
                results.delete(boIInfoObject);
            }
            iStore.commit(results);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in deleteBIAJobs", ex);
            }
            throw new LameRuntimeException("Error in deleteBIAJobs", ex);
        }
    }

    @Override
    public void cancelQuering() {
        stopQuering = true;
    }

    protected void allowQuering() {
        stopQuering = false;
    }

    @Override
    public List<SAPBOScheduleBean> getBIASchedules(SAPBOScheduleBean filter) {
        allowQuering();
        try {
            String whereQuery = createInstanceWhereString(filter);
            if (logger.isInfoEnabled()) {
                logger.info("Getting schedules...");
            }
            List<SAPBOScheduleBean> schedules = new ArrayList<SAPBOScheduleBean>();
            Integer siID = null;
            while (true) {
                String queryText = "select top 100 * from ci_infoobjects"
                        + (siID == null ? " where " + whereQuery : (" where (si_id > " + siID + ") AND " + whereQuery)) + " order by si_id";
                if (logger.isInfoEnabled()) {
                    logger.info("Executing query: " + queryText);
                }
                IInfoObjects results = iStore.query(queryText);

                for (int i = 0; i < results.size(); i++) {
                    IInfoObject obj = (IInfoObject) results.get(i);
                    schedules.add(getOneSchedule(obj, iStore));
                    siID = obj.getID();
                }
                // pobieramy top 1000
                if (results.size() < 100 /*|| schedules.size() >= 1000*/) {
                    break;
                }
                if (stopQuering) {
                    allowQuering();
                    System.out.println("Stopping BO Query. Last Query: " + queryText);
                    schedules.clear();
                    return schedules;
                }
            }
            // get location
            Map<Integer, SAPBOObjectBean> objMap = createFolderMap("ci_infoobjects", true);
            for (SAPBOScheduleBean obj : schedules) {
                StringBuilder locationBuilder = new StringBuilder();
                fixLocation(obj.name, obj.parentId, objMap, locationBuilder, true);
                obj.location = locationBuilder.toString();
            }
            return schedules;
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in getBIASchedules", ex);
            }
            throw new LameRuntimeException("Error in getBIASchedules", ex);
        }
    }

    protected String createInstanceWhereString(SAPBOScheduleBean filter) {
        StringBuilder sb = new StringBuilder("SI_INSTANCE = 1 and SI_UNDER_FAVORITES is null and si_kind in ('Webi', 'Excel', 'Publication', 'Pdf', 'FullClient')");// and si_kind = 'Webi'");
        switch (filter.scheduleType) {
            case -1:
                break;
            default:
                sb.append(" and SI_SCHEDULE_STATUS = ").append(filter.scheduleType);
                break;
        }
        switch (filter.recurring) {
            case -1:
                break;
            default:
                sb.append(" and SI_RECURRING = ").append(filter.recurring);
                break;
        }
        if (!BaseUtils.isStrEmptyOrWhiteSpace(filter.owner)) {
            sb.append(" and SI_OWNER = '").append(filter.owner).append("'");
        }
        if (filter.id != null && filter.id != -1) {
            sb.append(" and SI_ID = ").append(filter.id);
        }
        if (filter.parentId != null && filter.parentId != -1) {
            sb.append(" and SI_PARENTID = ").append(filter.parentId);
        }
        if (filter.ancestorId != null && filter.ancestorId != -1) {
            sb.append(" and SI_ANCESTOR = ").append(filter.ancestorId);
        }
        if (filter.intervalHours != -1 && filter.intervalHours != 0) {
            Date now = new Date();
            Date dateFilter = SimpleDateUtils.addHours(now, -filter.intervalHours);
            sb.append(getFilterStartDateWhereString(dateFilter));
            sb.append(getFilterEndDateWhereString(now));
//            sb.append(" and SI_UPDATE_TS > '").append(SimpleDateUtils.toCanonicalString(dateFilter)).append("'");
        }
        if (filter.started != null) {
            sb.append(getFilterStartDateWhereString(filter.started));
        }
        if (filter.ended != null) {
            sb.append(getFilterEndDateWhereString(filter.ended));
//            sb.append(" and SI_STARTTIME < '").append(SimpleDateUtils.toCanonicalString(filter.ended)).append("'");
        }

        if (!BaseUtils.isCollectionEmpty(filter.reportsId)) {
            sb.append(" and SI_PARENTID in (").append(BaseUtils.mergeWithSepEx(filter.reportsId, ",")).append(")");
        }

        if (!BaseUtils.isCollectionEmpty(filter.ancestorsId)) {
            sb.append(" and SI_ANCESTOR in (").append(BaseUtils.mergeWithSepEx(filter.ancestorsId, ",")).append(")");
        }

        if (!BaseUtils.isCollectionEmpty(filter.reportsIdFromConnections)) {
            sb.append(" and SI_PARENTID in (").append(BaseUtils.mergeWithSepEx(filter.reportsIdFromConnections, ",")).append(")");
        }
        return sb.toString();
    }

    protected String getFilterStartDateWhereString(Date dateFilter) {
        StringBuilder sb = new StringBuilder();
        sb.append(" and ((SI_STARTTIME is not null and  SI_STARTTIME > '").append(SimpleDateUtils.toCanonicalString(dateFilter)).append("')")
                .append(" or (SI_NEXTRUNTIME is not null and SI_NEXTRUNTIME > '").append(SimpleDateUtils.toCanonicalString(dateFilter)).append("'))");
        return sb.toString();
    }

    protected String getFilterEndDateWhereString(Date dateFilter) {
        StringBuilder sb = new StringBuilder();
        sb.append(" and ((SI_STARTTIME is not null and  SI_STARTTIME < '").append(SimpleDateUtils.toCanonicalString(dateFilter)).append("')")
                .append(" or (SI_NEXTRUNTIME is not null and SI_NEXTRUNTIME < '").append(SimpleDateUtils.toCanonicalString(dateFilter)).append("'))");
        return sb.toString();
    }

    private String createObjectsWhereString(SAPBOObjectBean filter) {
        StringBuilder sb = new StringBuilder("SI_INSTANCE = 0"); // and SI_UNDER_FAVORITES is null
        switch (filter.manageType) {
            case 1:
                sb.append(" and SI_KIND in ('Folder', 'FavoritesFolder', 'Webi', 'CrystalReport', 'Pdf', 'FullClient', 'Rtf', 'Txt', 'Flash', 'Hyperlink', 'Excel', 'Powerpoint', 'Word')");// and SI_ANCESTOR in (").append(rootFolderId).append(", ").append(userFolderId).append(")");
                break;
            case 2:
                sb.append(" and SI_KIND in ('Folder', 'Universe', 'DSL.MetaDataFile') and SI_ANCESTOR in (").append(univFolderId).append(")");
                break;
            case 3:
                sb.append(" and SI_KIND in ('Folder', 'MetaData.DataConnection', 'CCIS.DataConnection', 'CommonConnection', 'Connection') and SI_ANCESTOR in (").append(connFolderId).append(")");
                break;
            case 4:
                sb.append(" and SI_KIND in ('UserGroup')");
                break;
        }
        if (filter.id != null && filter.id != -1) {
            sb.append(" and SI_ID = ").append(filter.id);
        }
        if (filter.parentId != null && filter.parentId != -1) {
            sb.append(" and SI_ANCESTOR = ").append(filter.parentId);
        }
        if (!BaseUtils.isCollectionEmpty(filter.kindsToFilter)) {
            sb.append(" and SI_KIND in (");
            int i = 0;
            for (String kind : filter.kindsToFilter) {
                if (i != 0) {
                    sb.append(",");
                }
                sb.append("'").append(kind).append("'");
                i++;
            }
            sb.append(")");
        }
        if (!BaseUtils.isStrEmptyOrWhiteSpace(filter.owner)) {
            sb.append(" and SI_OWNER = '").append(filter.owner).append("'");
        }
        if (!BaseUtils.isStrEmptyOrWhiteSpace(filter.name)) {
            sb.append(" and SI_NAME ");
            if (filter.name.contains("*") || filter.name.contains("%")) {
                sb.append("like '").append(filter.name.replace("*", "%")).append("'");
            } else {
                sb.append("= '").append(filter.name).append("'");
            }
        }
        return sb.toString();
    }
}
