/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import pl.bssg.gwtservlet.WithoutAdhocSqls;
import pl.fovis.client.VersionInfoService;
import pl.fovis.common.BIKConstants;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
@WithoutAdhocSqls
public class VersionInfoServiceImpl extends BIKSServiceBase implements VersionInfoService {

    public Pair<String, String> getAppVersion() {
        String version = execNamedQuerySingleVal("getVersionAppProps", true, "val" /* I18N: no */);
        return new Pair<String, String>(BIKConstants.APP_VERSION, version);
    }
}
