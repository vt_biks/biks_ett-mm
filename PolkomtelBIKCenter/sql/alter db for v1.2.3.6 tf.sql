﻿exec sp_check_version '1.2.3.6';
go

alter table bik_tree
add is_built_in int not null default 0
go

update bik_tree
set is_built_in = 1
where code in ('Documents', 'Blogs', 'Glossary')

-- update Dokumentacja
declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')  

exec sp_add_menu_node 'knowledge', 'Dokumentacja', '@Documents'

declare @documentsInstanceId int;
declare @documentsTemplateId int;
select @documentsInstanceId = id from bik_node where tree_id = @tree_id and obj_id = '$Documents'
select @documentsTemplateId = id from bik_node where tree_id = @tree_id and obj_id = '@Documents'

update bik_node
set parent_node_id = @documentsTemplateId
where parent_node_id = @documentsInstanceId
and tree_id = @tree_id

update bik_node
set parent_node_id = @documentsTemplateId
where id = @documentsInstanceId

update bik_node
set name = '@', visual_order = 0
where id = @documentsInstanceId

update bik_node
set visual_order = 0
where id = @documentsTemplateId

exec sp_node_init_branch_id @tree_id, null
go

-- update Blogi
declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')

update bik_node
set obj_id = '@Blogs'
where obj_id = '$Blogs'
and tree_id = @tree_id

exec sp_add_menu_node 'community', '@', '$Blogs'

update bik_node
set visual_order = 0
where obj_id = '$Blogs'
and tree_id = @tree_id

exec sp_node_init_branch_id @tree_id, null
go

-- update Glosariusz
declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')

update bik_node
set obj_id = '@Glossary', name = 'Glosariusz'
where obj_id = '$Glossary'
and tree_id = @tree_id

exec sp_add_menu_node '@Glossary', '@', '$Glossary'

declare @GlossaryVO int;
select @GlossaryVO = visual_order 
from bik_node 
where obj_id = '@Glossary'
and tree_id = @tree_id

update bik_node
set visual_order = @GlossaryVO
where obj_id = '$Glossary'
and tree_id = @tree_id

exec sp_node_init_branch_id @tree_id, null
go

exec sp_update_version '1.2.3.6', '1.2.3.7';
go