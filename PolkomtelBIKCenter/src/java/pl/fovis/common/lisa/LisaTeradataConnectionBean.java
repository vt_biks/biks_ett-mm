/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

import java.io.Serializable;

/**
 *
 * @author tbzowka
 */
public class LisaTeradataConnectionBean implements Serializable {
    public String serverName;
    public String port;
    public String dbName;
    public String charset;
    public String transactionType;    
    public String schemaName;
    public String defaultUnassignedCategoryName;
}
