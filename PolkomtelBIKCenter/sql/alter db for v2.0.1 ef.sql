exec sp_check_version '2.0.1';
GO

IF NOT EXISTS (
		SELECT 1
		FROM bik_app_prop
		WHERE name = 'hideActionMoveInTree'
		)
	INSERT INTO bik_app_prop (
		name
		,val
		,is_editable
		)
	VALUES (
		'hideActionMoveInTree'
		,'false'
		,1
		)

exec sp_update_version '2.0.1'
	,'2.0.2';
GO