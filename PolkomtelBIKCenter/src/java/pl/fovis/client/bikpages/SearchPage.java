/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.PaginationWidgetsHelper;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BiksSearchResult;
import pl.fovis.common.SearchNodeKindsFilterWidget;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class SearchPage extends BikPageMinimalBase {

    public static final String SEARCH_PAGE_TAB_ID = "SearchPage";
    public static TextBox queryTb;
    protected VerticalPanel main = new VerticalPanel();
    protected PushButton searchBtn;
//    protected RadioButton queryTxtRb = new RadioButton("queryType", I18n.szukanaFraza.get());
//    protected RadioButton queryWildcardRb = new RadioButton("queryType", I18n.maska.get());
    protected Label foundItemsHeader = new Label();
    protected FlexTable foundItemsGrid = new FlexTable();
    protected PaginationWidgetsHelper pagger;

//    protected Integer totalCnt = 0;
    protected Integer searchId;
    protected RadioButton normalSearchModeBtn, advancedSearchModeBtn;
    protected ListBox returnedResultNum;
    protected SearchTreeFilterWidget treeFilters;//= new SearchTreeFilterWidget();
    public SearchNodeKindsFilterWidget nodeKindsFilter;
    public SearchAttributesNormalSearchModeFilter attributesFilter;
    protected SearchAttributeAdvancedSearchModFilter attributesPbFilter;

    private void doSearch() {
        if (searchId == null) {
            pagger.pageNum = 0;
        }
        Pair<Set<String>, List<String>> selectedAttrAndOther = attributesFilter.getSelectedAttr(normalSearchModeBtn.getValue());
        String query = BIKClientSingletons.getGlobalSearchTextBox().getText();

        if (!BaseUtils.isStrEmptyOrWhiteSpace(query)) {
            final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
            infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaWyszukiwanie.get(), true);
            String searchMode = normalSearchModeBtn.getValue() ? BIKConstants.SEARCH_MODE_NORMAL : BIKConstants.SEARCH_MODE_ADVANCED;

            BIKClientSingletons.getService().search(searchMode, searchId, query, treeFilters.getSelectedTreeIds(), selectedAttrAndOther.v1, selectedAttrAndOther.v2, nodeKindsFilter.getSelectedNodeKindIds(), pagger.pageNum, pagger.pageSize, new StandardAsyncCallback<BiksSearchResult>() {

                @Override
                public void onSuccess(BiksSearchResult result) {
                    infoDialog.hideDialog();
                    updateSearchResultBlock(result);
                }

                @Override
                public void onFailure(Throwable caught) {

                    infoDialog.hideDialog();
//                    super.onFailure(caught);
                    if (normalSearchModeBtn.getValue()) {
                        BIKClientSingletons.showWarning("Trwa indeksowanie. Spróbuj ponownie za chwilę");
                    } else {
                        BIKClientSingletons.showWarning("Trwa indeksowanie lub występuje błąd składni zapytania. Spróbuj ponownie za chwilę");
                    }
//                    brak indeksu wyszukiwarki, nie można wyszukiwać. Najprawdopodobniej trwa zasilanie, poczekaj aż zostanie ukończone lub skontaktuj się z administratorem

                }
            });
        } else {
            foundItemsHeader.setText("");
            foundItemsGrid.removeAllRows();
        }
    }

    private void updateSearchResultBlock(BiksSearchResult result) {
        treeFilters.updateCheckBoxes(result.treeCnt);

        int totalCnt = treeFilters.getTotalCnt();

        pagger.setPageCntByItemCnt(totalCnt);

        String foundItemsHeaderTxt;

        if (totalCnt == 0) {
            foundItemsHeaderTxt = I18n.brakWynikowDla.get() + ": [" + result.query + "], " + I18n.wpiszInnaFrazeLubWybierzInneZakl.get();
        } else {
            int lastDisplayedItemNum = Math.min((pagger.pageNum + 1) * pagger.pageSize, totalCnt);

            foundItemsHeaderTxt = I18n.wynikiDla.get() + ": [" + result.query + "], " + I18n.pozycje.get() + ": "
                    //                    + (pagger.pageNum * pagger.pageSize + 1) + "-" + lastDisplayedItemNum
                    + " (" + I18n.lacznie.get() /* I18N:  */ + ": " + totalCnt + ")";
        }
        foundItemsHeader.setText(foundItemsHeaderTxt);
        pagger.refresh();

        if (!result.searchId.equals(searchId)) {
//        if (result.searchId == searchId) {
            foundItemsGrid.removeAllRows();
//            Window.alert("reset");
        }
        searchId = result.searchId;

        int row = foundItemsGrid.getRowCount();
        int hc = 0;
        if (row == 0) {
            //init grid
            foundItemsGrid.getFlexCellFormatter().setColSpan(row, hc, 3);
            foundItemsGrid.setText(row, hc++, I18n.akcje.get());

//        foundItemsGrid.setText(row, hc++,  I18n.oceny.get() ) ;
//        foundItemsGrid.getCellFormatter().getElement(row, hc - 1).setTitle(I18n.uzytecznosc.get() );
//        foundItemsGrid.setText(row, hc++, I18n.wystepowanie.get()  );
//            foundItemsGrid.getFlexCellFormatter().setColSpan(row, hc, 2);
            foundItemsGrid.setText(row, hc++, I18n.nazwaObiektu.get());

            foundItemsGrid.setText(row, hc++, I18n.opis.get());
            foundItemsGrid.setText(row, hc++, I18n.atrybuty.get());
            foundItemsGrid.getRowFormatter().setStyleName(row, "related-obj-header-row");

            row++;
        }
        for (TreeNodeBean item : result.items) {
            int col = 0;
            foundItemsGrid.setWidget(row, col++, BIKClientSingletons.createLookupButton(item.treeCode, item.id, null, true));
            foundItemsGrid.setWidget(row, col++, BIKClientSingletons.createFollowButton(item.treeCode, item.id, true));
            foundItemsGrid.setWidget(row, col++, BIKClientSingletons.createIconForNodeKind(item.nodeKindCode, item.treeCode, item.nodeKindCaption));
            GWTUtils.setStyleAttribute(foundItemsGrid.getFlexCellFormatter().getElement(row, col - 1), "width" /* I18N: no */, "16px");
            BikCustomButton btn = BIKClientSingletons.createLabelLookupButton(item.name, item.treeCode, item.id, null, true);
            btn.setTitle(item.treeName + ":" + item.nodePath);
            foundItemsGrid.setWidget(row, col++, /*new Label(item.treeName + ": " + item.nodePath)*/ btn);

            foundItemsGrid.setWidget(row, col++, new Label(BaseUtils.getTrimStrToLength(item.descr, 200)));
            foundItemsGrid.setWidget(row, col++, new Label(item.attributes));

//                int allVoteCnt = item.voteCnt + item.voteSum;
//                foundItemsGrid.setText(row, col++, (allVoteCnt != 0) ? ("" + item.voteSum + " " + I18n.z_noSingleLetter.get() /* I18N:  */ + " " + allVoteCnt) : "(" + I18n.brakOcen2.get() /* I18N:  */ + ")");
//                if (allVoteCnt != 0) {
//                    foundItemsGrid.getCellFormatter().getElement(row, col - 1).setTitle(item.voteSum + " " + I18n.z_noSingleLetter.get() /* I18N:  */ + " " + allVoteCnt + " " + I18n.osobUznaloToZaUzyteczne.get() /* I18N:  */);
//                }
            //foundItemsGrid.getCellFormatter().getElement(row, col - 1).setTitle("głosów: " + item.voteCnt /*+ ", zgodność frazy: " + item.ftsRank*/);
            //foundItemsGrid.setText(row, col++, item.voteSum + " / " + item.voteCnt);
//                Pair<String, String> p = getAttrListShortAndFull(item.foundInAttrCaptions);
//                foundItemsGrid.setText(row, col++, p.v1);
//                foundItemsGrid.getCellFormatter().getElement(row, col - 1).setTitle(p.v2);
//            foundItemsGrid.setWidget(row, col++, BIKClientSingletons.createLookupButton(item.treeCode, item.id, null, true));
//            GWTUtils.setStyleAttribute(foundItemsGrid.getFlexCellFormatter().getElement(row, col - 1), "width" /* I18N: no */, "16px");
            for (int colNr = 0; colNr <= 2; colNr++) {
                GWTUtils.setStyleAttribute(foundItemsGrid.getFlexCellFormatter().getElement(row, colNr), "width" /* I18N: no */, "16px");
            }
            GWTUtils.setStyleAttribute(foundItemsGrid.getFlexCellFormatter().getElement(row, 3), "width" /* I18N: no */, "250px");

            foundItemsGrid.getRowFormatter().setStyleName(row, row % 2 != 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
            row++;
        }

//        BIKClientSingletons.showInfo(I18n.wynikiDla.get() /* I18N:  */ + ": " + totalCnt);
    }
    ScrollPanel resultSP;

    @Override
    protected Widget buildWidgets() {
        main.clear();
        main.add(buildQueryBlock());
//        main.add(buildFilterBlock());
        main.add(buildFilterBlock());
        HorizontalPanel foundItemsHeaderHp = new HorizontalPanel();
        foundItemsHeaderHp.setStyleName("clearBoth searchPageBigTxtP paddingTop8px");
        foundItemsHeaderHp.add(foundItemsHeader);
        main.add(foundItemsHeaderHp);

        resultSP = (ScrollPanel) buildSearchResultBlock();
        main.add(resultSP/*buildSearchResultBlock()*/);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(resultSP, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 318);

//        main.add(pagger.paginationHp);
        main.setWidth("100%");
        ScrollPanel scPanelFvsm = new ScrollPanel(main);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(scPanelFvsm, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT);
        scPanelFvsm.setStyleName("EntityDetailsPane");
        return scPanelFvsm;

//         return createMainScrollPanel(main, null);
//        return main;
    }

    @Override
    public String getId() {
        return SEARCH_PAGE_TAB_ID;
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        searchId = null;
        doSearch();
    }

    private IsWidget buildQueryBlock() {
        HorizontalPanel w = new HorizontalPanel();
        w.setStyleName("queryBlock");
        w.setSpacing(8);
//        queryTxtRb.setValue(true);
//        w.add(queryTxtRb);
//        w.add(queryWildcardRb);
        VerticalPanel vp = new VerticalPanel();
        queryTb = new TextBox();
        queryTb.setWidth("830px");
        searchBtn = new PushButton(I18n.szukaj.get());
        searchBtn.setHTML("<img  style=\"margin: 0px 4px 0px 0px; border: 0px none; float: left; padding: 0px 3px 0px 3px;\" src=\"images/loupe_search.png" + "\"/>" + searchBtn.getHTML());
        NewLookUtils.makeCustomPushButton(searchBtn);
        BIKClientSingletons.setupAdditionalSearchTextBoxEvents(queryTb, searchBtn);
        searchBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                searchId = null;
                doSearch();
            }
        });
        vp.add(queryTb);
        HorizontalPanel hp = new HorizontalPanel();
        hp.add(normalSearchModeBtn = new RadioButton("searchMode", I18n.zwykle.get()));
        hp.add(advancedSearchModeBtn = new RadioButton("searchMode", I18n.zaawansowane.get()));
        normalSearchModeBtn.setValue(true);
        searchModeBtnClickHandler();
        vp.add(hp);
        w.add(vp);
        w.add(searchBtn);
//        w.add(createReturnedResultNumListBox());
        return w;
    }

    private IsWidget buildSearchResultBlock() {
//        pagger = new PaginationWidgetsHelper(BaseUtils.tryParseInteger(returnedResultNum.getSelectedItemText(), 10)) {
//
//            @Override
//            protected void pageNumChanged() {
//                doSearch();
//            }
//        };
        pagger = new PaginationWidgetsHelper(50) {

            @Override
            protected void pageNumChanged() {
                doSearch();
            }
        };
        pagger.init();
        final ScrollPanel sp = new ScrollPanel();
//        sp.setHeight("650px");
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(sp, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 108);
//        VerticalPanel w = new VerticalPanel();
//        w.setWidth("100%");
//        foundItemsHeaderHp.add(pagger.paginationHp);
//        foundItemsHeaderHp.setCellHorizontalAlignment(pagger.paginationHp, HasHorizontalAlignment.ALIGN_RIGHT);

        foundItemsGrid.setStyleName("gridJoinedObj");
        foundItemsGrid.setWidth("100%");

//        sp.add(foundItemsHeaderHp);
        sp.add(foundItemsGrid);

//        sp.add(w);
        sp.addScrollHandler(new ScrollHandler() {

            @Override
            public void onScroll(ScrollEvent event) {
                if (sp.getMaximumVerticalScrollPosition() == sp.getVerticalScrollPosition()) {
                    if (!pagger.isLastPage()) {
                        pagger.pageNum++;
                        doSearch();
                    } else {
                        BIKClientSingletons.showInfo(I18n.koniecWyszukiwania.get());
                    }
                }
            }
        });

        return sp;
    }

    protected DisclosurePanel advancedDisclosure = new DisclosurePanel(I18n.filtr.get());
    boolean isFilterVisible = false;
    BikCustomButton filtrBtn;

    protected IsWidget buildFilterBlock() {
        final HorizontalPanel filterBlockHP = new HorizontalPanel();
//        filterBlockHP.setStyleName("queryBlock");
//        filterBlockHP.addStyleName("queryBlockW");
        treeFilters = new SearchTreeFilterWidget(new IContinuation() {

            @Override
            public void doIt() {

                nodeKindsFilter.reselectedAndFilteredNodeKindsVp(treeFilters.getNodeKindIdsInTrees()/* param*/ /*filtrKindTb.getText()*/);
                attributesFilter.reselectedAttributeCbxs(nodeKindsFilter.getAttrNamesAndIsAnythingNodeKindSelected());
                attributesPbFilter.reselectedAttributeCbxs(nodeKindsFilter.getAttrNamesAndIsAnythingNodeKindSelected());
                searchId = null;
                doSearch();
            }
        });

        filterBlockHP.add(treeFilters.buildWidget());

        nodeKindsFilter = new SearchNodeKindsFilterWidget(new IContinuation() {

            @Override
            public void doIt() {
                attributesFilter.reselectedAttributeCbxs(nodeKindsFilter.getAttrNamesAndIsAnythingNodeKindSelected());
                attributesPbFilter.reselectedAttributeCbxs(nodeKindsFilter.getAttrNamesAndIsAnythingNodeKindSelected());
                searchId = null;
                doSearch();
            }
        }, treeFilters.getNodeKindIdsInTrees().v1);

        filterBlockHP.add(nodeKindsFilter.buildWidget());
        attributesFilter = new SearchAttributesNormalSearchModeFilter(new IContinuation() {

            @Override
            public void doIt() {
                searchId = null;
                doSearch();
            }
        }, nodeKindsFilter.getAttrNamesAndIsAnythingNodeKindSelected().v1);

        filterBlockHP.add(attributesFilter.buildWidget());
        attributesPbFilter = new SearchAttributeAdvancedSearchModFilter(new IParametrizedContinuation<String>() {

            @Override
            public void doIt(String param) {
                if (!BaseUtils.isStrEmptyOrWhiteSpace(param)) {
                    String txt = queryTb.getText() + param;
                    queryTb.setText(txt);
                    BIKClientSingletons.setGlobalSearchTb(txt);
                }
            }
        }, nodeKindsFilter.getAttrNamesAndIsAnythingNodeKindSelected().v1);
        filterBlockHP.add(attributesPbFilter.buildWidget());
        attributesPbFilter.attributeFilterVp.setVisible(false);

//        DisclosurePanel advancedDisclosure = new DisclosurePanel(I18n.filtr.get());
        filterBlockHP.setVisible(false);
        VerticalPanel vp = new VerticalPanel();
        HorizontalPanel hp = new HorizontalPanel();
        hp.setWidth("100%");
        vp.setStyleName("queryBlock");
        vp.addStyleName("queryBlockW");
        filtrBtn = new BikCustomButton(I18n.filtr.get(), "dartAtSearch", new IContinuation() {

            @Override
            public void doIt() {

                filtrBtn.setStyleName(!isFilterVisible ? "dartDownSearch" : "dartAtSearch");
                filterBlockHP.setVisible(!isFilterVisible);
                isFilterVisible = !isFilterVisible;
//                filterBlockHP.setHeight(isFilterVisible ? "400px" : "0px");
                if (isFilterVisible) {
                    BIKClientSingletons.registerResizeSensitiveWidgetByHeight(resultSP, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 370);
                } else {
                    BIKClientSingletons.registerResizeSensitiveWidgetByHeight(resultSP, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 118);
                }
            }
        });
        hp.add(filtrBtn);
        hp.add(selectOrUnselectAllBtn(I18n.zaznaczWszystko.get(), true));
        hp.add(selectOrUnselectAllBtn(I18n.odznaczWszystko.get(), false));
        vp.add(hp);
        vp.add(filterBlockHP);
        return vp;
    }

    protected PushButton selectOrUnselectAllBtn(String caption, final boolean check) {
        PushButton selectOrUnselectBtn = new PushButton(caption);
        NewLookUtils.makeCustomPushButton(selectOrUnselectBtn);
        selectOrUnselectBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                treeFilters.selectOrUnselectAllFilter(check);
                nodeKindsFilter.selectOrUnselectAllFilter(check);
                attributesFilter.selectOrUnselectAllFilter(check);
                attributesPbFilter.selectOrUnselectAllFilter(check);
            }
        });
        return selectOrUnselectBtn;
    }

//  
    private IsWidget createReturnedResultNumListBox() {
        returnedResultNum = new ListBox();
        returnedResultNum.addItem("20");
        returnedResultNum.addItem("50");
        returnedResultNum.addItem("100");
        returnedResultNum.addItem("150");
        returnedResultNum.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                int n = BaseUtils.tryParseInteger(returnedResultNum.getSelectedItemText(), 12);
                pagger.pageSize = n;
                pagger.init();
                doSearch();
            }
        });
        returnedResultNum.setSelectedIndex(0);
        return returnedResultNum;
    }

    private void setFiterAtributePanelVisible() {
//        nodeKindsFilterVp.setVisible(normalSearchModeBtn.getValue());
        attributesFilter.attributeFilterVp.setVisible(normalSearchModeBtn.getValue());
        attributesPbFilter.attributeFilterVp.setVisible(!normalSearchModeBtn.getValue());
//        advancedDisclosure.setVisible(normalSearchModeBtn.getValue());
//        fiterAtributePanel.setVisible(normalSearchModeBtn.getValue());
    }

    private void searchModeBtnClickHandler() {
        normalSearchModeBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {

                setFiterAtributePanelVisible();
                queryTb.setText("");
                BIKClientSingletons.clearGlobalSearchTb();
                searchId = null;
                doSearch();

            }
        });
        advancedSearchModeBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                setFiterAtributePanelVisible();
                queryTb.setText("");
                BIKClientSingletons.clearGlobalSearchTb();
                searchId = null;
                doSearch();
            }
        });
    }

    protected PushButton createPushButtonOnImage(String imageUrl, String title, ClickHandler clickHandler) {
        PushButton pBtn = new PushButton(new Image(imageUrl), clickHandler);
        pBtn.setTitle(title);
        pBtn.setStyleName("SystemUserPageFilterPushButton");
        return pBtn;
    }
}
