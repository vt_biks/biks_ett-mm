/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author tflorczak
 */
public class CSVWriter {

    protected List<String> allColumnNames;
    protected List<Map<String, Object>> dataToSave;
    protected char separator;
    protected boolean addHeaderRow;
    protected boolean addSeparatorAsLastCharInLine;
    protected final static char END_LINE_CHAR = '\n';

    public List<String> loadColumnNames(List<Map<String, Object>> dataToSave) {
        List<String> allColumnNames = new ArrayList<String>();
        for (Map<String, Object> row : dataToSave) {
            for (String colName : row.keySet()) {
                if (allColumnNames.indexOf(colName) < 0) {
                    allColumnNames.add(colName);
                }
            }
        }
        return allColumnNames;
    }

    public CSVWriter(List<Map<String, Object>> dataToSave) {
        this(null, dataToSave);
    }

    public CSVWriter(List<String> allColumnNames, List<Map<String, Object>> dataToSave) {
        this(allColumnNames, dataToSave, CSVReader.DEFAULT_SEPARATOR);
    }

    public CSVWriter(List<String> allColumnNames, List<Map<String, Object>> dataToSave, char separator) {
        this(allColumnNames, dataToSave, separator, true, false);
    }

    public CSVWriter(List<String> allColumnNames, List<Map<String, Object>> dataToSave, char separator, boolean addHeaderRow, boolean addSeparatorAsLastCharInLine) {
        this.allColumnNames = allColumnNames == null ? loadColumnNames(dataToSave) : allColumnNames;
        this.dataToSave = dataToSave;
        this.separator = separator;
        this.addHeaderRow = addHeaderRow;
        this.addSeparatorAsLastCharInLine = addSeparatorAsLastCharInLine;
    }

    protected void mergeRowWithSep(StringBuilder sb, Map<String, Object> row) {
        mergeRowWithSep(sb, row, false);
    }

    protected void mergeRowWithSep(StringBuilder sb, Map<String, Object> row, boolean isKeyVal) {
        boolean isFirstCol = true;
//        for (Map.Entry<String, Object> entrySet : row.entrySet()) {
//            if (isFirstRow) {
//                isFirstRow = false;
//            } else {
//                sb.append(separator);
//            }
//            String val = String.valueOf(entrySet.getValue());
//            if (isKeyVal) {
//                val = entrySet.getKey();
//            }
//            fixAndAppend(sb, val);
//        }
        for (int i = 0; i < allColumnNames.size(); i++) {
            if (isFirstCol) {
                isFirstCol = false;
            } else {
                sb.append(separator);
            }
            fixAndAppend(sb, BaseUtils.safeToStringNotNull(row.get(allColumnNames.get(i))));
        }
    }

    protected void fixAndAppend(StringBuilder sb, String val) {
        String valAfterFix = (val == null ? "" : val.replace("\"", CSVReader.DOUBLE_QUOTE_STRING));
        sb.append(CSVReader.STRING_QUOTE_CHAR).append(valAfterFix).append(CSVReader.STRING_QUOTE_CHAR);
    }

    public ILameCollector<Map<String, Object>> makeCollector(final StringBuilder sb) {
        return new ILameCollector<Map<String, Object>>() {
            boolean isHeaderAdded = false;

            @Override
            public void add(Map<String, Object> row) {
                if (addHeaderRow && !isHeaderAdded) {
                    createHeader(sb);
//                mergeRowWithSep(sb, row, true);
                    addEndLineChars(sb);
                    isHeaderAdded = true;
                }
                mergeRowWithSep(sb, row);
                addEndLineChars(sb);
            }
        };
    }

    public String getTextViaCollector() {
        StringBuilder sb = new StringBuilder();
        ILameCollector<Map<String, Object>> collector = makeCollector(sb);
        for (Map<String, Object> row : dataToSave) {
            collector.add(row);
        }
        return sb.toString();
    }

    public String getText() {
        StringBuilder sb = new StringBuilder();
        boolean isHeaderAdded = false;
        for (Map<String, Object> row : dataToSave) {
            if (addHeaderRow && !isHeaderAdded) {
                createHeader(sb);
//                mergeRowWithSep(sb, row, true);
                addEndLineChars(sb);
                isHeaderAdded = true;
            }
            mergeRowWithSep(sb, row);
            addEndLineChars(sb);
        }
        return sb.toString();
    }

    protected void createHeader(StringBuilder sb) {
        fixAndAppend(sb, allColumnNames.get(0));
        for (int i = 1; i < allColumnNames.size(); i++) {
            sb.append(separator);
            fixAndAppend(sb, allColumnNames.get(i));
        }
    }

    protected void addEndLineChars(StringBuilder sb) {
        if (addSeparatorAsLastCharInLine) {
            sb.append(separator);
        }
        sb.append(END_LINE_CHAR);
    }

//    public static void main(String[] args) {
//        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
//        Map<String, String> map = new LinkedHashMap<String, String>();
//        Map<String, String> map2 = new LinkedHashMap<String, String>();
//        map.put("imie", "Ala");
//        map.put("nazwisko", "Mohammed\"cka");
//        map.put("wiek", "13");
//        map2.put("imie", "Kaśka\"ala\"kaczka");
//        map2.put("nazwisko", "Brocka");
//        map2.put("wiek", "26");
//        list.add(map);
//        list.add(map2);
//        CSVWriter writter = new CSVWriter(list);
//        String text = writter.getText();
//        System.out.println("text = " + text);
//    }
    public void setDataToSave(List<Map<String, Object>> dataToSave) {
        this.dataToSave = dataToSave;
    }
}
