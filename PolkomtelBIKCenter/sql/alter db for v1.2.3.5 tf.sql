﻿exec sp_check_version '1.2.3.5';
go

create index idx_bik_attachment_node_id on bik_attachment (node_id)
go
 
create index idx_bik_attribute_linked_node_id on bik_attribute_linked (node_id)
go
 
create index idx_bik_blog_node_id on bik_blog (node_id)
go
 
create index idx_bik_frequently_asked_questions_node_id on bik_frequently_asked_questions (node_id)
go
 
create index idx_bik_metapedia_node_id on bik_metapedia (node_id)
go
 
create index idx_bik_note_node_id on bik_note (node_id)
go
 
create index idx_bik_object_author_node_id on bik_object_author (node_id)
go
 
create index idx_bik_object_excluded_from_statistic_node_id on bik_object_excluded_from_statistic (node_id)
go
 
create index idx_bik_object_in_fvs_change_node_id on bik_object_in_fvs_change (node_id)
go
 
create index idx_bik_object_in_history_node_id on bik_object_in_history (node_id)
go
 
create index idx_bik_role_for_node_node_id on bik_role_for_node (node_id)
go
 
create index idx_bik_sapbo_extradata_node_id on bik_sapbo_extradata (node_id)
go
 
create index idx_bik_specialist_node_id on bik_specialist (node_id)
go
 
create index idx_bik_statistic_ext_clicked_node_id on bik_statistic_ext (clicked_node_id)
go
 
create index idx_bik_user_in_node_node_id on bik_user_in_node (node_id)
go

exec sp_update_version '1.2.3.5', '1.2.3.6';
go