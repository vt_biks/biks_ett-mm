﻿exec sp_check_version '1.4.y1.2';
go

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------


-- poprzednia wersja w: alter db for v1.3.0.10 tf.sql
-- fix dla != , ~webinteligence% i coaselsce
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node]
go

create procedure [dbo].[sp_insert_into_bik_node](@tree_code varchar(255))
as
	declare @table_folders varchar(255);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	declare @diag_level int = 0;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'

	create table #primaryIds(si_id varchar(5000) collate DATABASE_DEFAULT primary key);

	if(@tree_code <> 'Reports' and @tree_code <> 'Teradata')
	begin
		set @table_folders = ' APP_FOLDER ';
		if(@tree_code = 'Connections')
		begin
			set @kinds = '''MetaData.DataConnection''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_METADATA__DATACONNECTION
					where SI_KIND in (' + @kinds + ') and SI_OBJECT_IS_CONTAINER=0');			
		end;
		if(@tree_code = 'ObjectUniverses')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_UNIVERSE
					where SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_code='Reports')
	begin
		-- wybranie folderw raportów bez raportów użytkowników
		set @table_folders = ' INFO_FOLDER where si_name not like ''~Webintelligence%''';
		--set @table_folders = ' INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 is null or SI_PATH__SI_FOLDER_NAME2 != ''User Folders''';
		set @kinds = '''Webi'', ''Flash'', ''CrystalReport'',''Excel'',''FullClient'',''Pdf'',''Hyperlink''
		,''Rtf'',''Txt'',''Powerpoint'',''Word''';
	end;
	
    declare @sql nvarchar(max);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		select @tree_id = id from bik_tree where code = @tree_code;
		set @sql = N'select branch_names as SI_ID, parent_branch_names	as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata';
	end;
	if(@tree_code = 'Reports')
	begin
		set @tree_id = dbo.fn_get_bo_actual_report_tree_id();
		
		declare @rootFolderName varchar(max) = null, @userFolderName varchar(max) = null;
		select @rootFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$RootFolder' and is_deleted = 0
		select @userFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$UserFolder' and is_deleted = 0
		
		-- sztuczny podzial na root folder i user folders dla raportów
		set @sql = N'select distinct convert(varchar(30),SI_PARENT_FOLDER) as SI_ID, ''#BO$UserFolder'' as SI_PARENTID, SI_KIND, SI_PATH__SI_FOLDER_NAME1 as SI_NAME, NULL as DESCR
				 from INFO_FOLDER where SI_PATH__SI_FOLDER_ID2 in (select si_id from INFO_FOLDER where SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c'') 
				 and SI_NAME not like ''~Webintelligence%'' 
				
				 union all
				
				 select case when SI_CUID = ''ASHnC0S_Pw5LhKFbZ.iA_j4'' then ''#BO$RootFolder'' when SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c'' then ''#BO$UserFolder'' else convert(varchar(30),si_id) end as si_id, case
									when SI_PARENTID = 0 and SI_CUID <> ''AWigQI18AAZJoXfRHLzWJ2c''
										then ''#BO$RootFolder''
									when si_id in (select si_id from INFO_FOLDER where SI_PATH__SI_FOLDER_ID2 in (select si_id from INFO_FOLDER where SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c''))
										then convert(varchar(30),SI_PARENTID)
									when SI_PARENTID in(select si_id from INFO_FOLDER)
										then convert(varchar(30),SI_PARENTID)
									else ''0'' end as SI_PARENTID, SI_KIND, case
																			when SI_CUID = ''ASHnC0S_Pw5LhKFbZ.iA_j4''
																				then coalesce(' + coalesce(QUOTENAME(substring(@rootFolderName,1,128),''''),'null') + ',SI_NAME)
																			when SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c''
																				then coalesce(' + coalesce(QUOTENAME(substring(@userFolderName,1,128), ''''),'null') + ',SI_NAME)
																			else SI_NAME end, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + '
				
				 union all
				
				 select convert(varchar(30),SI_ID), convert(varchar(30),SI_PARENTID), SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') and SI_INSTANCE = 0
				
				union all
				
				select convert(varchar(30),SI_ID), convert(varchar(30),SI_PARENTID), SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR
				from aaa_global_props where si_parentid in (select si_parentid from INFO_FOLDER where SI_PATH__SI_FOLDER_ID2 in (select si_id from INFO_FOLDER where SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c'') and SI_NAME <> ''~Webintelligence'') and si_kind <> ''Folder''
				
				union all
				
				select convert(varchar(30),id), convert(varchar(30),parent_id), ''ReportSchedule'', name, null
				from aaa_report_schedule sch
				inner join aaa_global_props gp on gp.si_id = sch.parent_id';
	end;
	if(@tree_code = 'Connections')
	begin
		set @tree_id = dbo.fn_get_bo_actual_connection_tree_id();
		
		declare @conenctionFolderName varchar(max) = null;
		select @conenctionFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$ConnectionFolder' and is_deleted = 0
		
		set @sql = N'select ''#BO$ConnectionFolder'' as si_id, ''0'' as si_parentid, si_kind, coalesce(' + coalesce(QUOTENAME(substring(@conenctionFolderName,1,128), ''''),'null') + ',SI_NAME) as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR from app_folder where SI_CUID = ''AZVXOgKIBEdOkiXzUuybja4''
		
			 union all
			 
			 select convert(varchar(30),SI_ID), ''#BO$ConnectionFolder'', SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
			 from APP_METADATA__DATACONNECTION
			 where SI_KIND in ('+ @kinds +') and SI_OBJECT_IS_CONTAINER = 0';
	end;
	if(@tree_code = 'ObjectUniverses')
	begin
		set @tree_id = dbo.fn_get_bo_actual_universe_tree_id();
		
		declare @universeFolderName varchar(max) = null;
		select @universeFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$UniverseFolder' and is_deleted = 0
		
		set @sql = N'select case when SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'' then ''#BO$UniverseFolder'' else convert(varchar(30),si_id) end as si_id, case 
								when SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk''
									then ''0''
								when SI_PARENTID in (select si_id from app_folder where SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'') 
									then ''#BO$UniverseFolder''
								when SI_PARENTID in(select si_id from ' + @table_folders +') 
									then convert(varchar(30),SI_PARENTID)
								else ''0'' end as SI_PARENTID, SI_KIND, case when SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'' then coalesce(' + coalesce(QUOTENAME(substring(@universeFolderName,1,128), ''''),'null') + ',SI_NAME) else SI_NAME end as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
			 from ' + @table_folders + '
			 
			 union all
			 
			 select convert(varchar(30),SI_ID), case when SI_PARENTID in (select si_id from app_folder where SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'') then ''#BO$UniverseFolder'' else convert(varchar(30),SI_PARENTID) end, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
			 from aaa_global_props
			 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
					and SI_KIND in ('+ @kinds +')';
	end;					

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed zapytaniem'
	print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(1000) collate DATABASE_DEFAULT primary key, si_parentid varchar(1000) collate DATABASE_DEFAULT, si_kind varchar(max) collate DATABASE_DEFAULT, si_name varchar(max) collate DATABASE_DEFAULT, descr varchar(max) collate DATABASE_DEFAULT);
	if(@tree_code = 'Reports')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ReportFolder'' 
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	if(@tree_code = 'Connections')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ConnectionFolder''
										when si_kind=''MetaData.DataConnection'' then ''DataConnection''
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	if(@tree_code = 'ObjectUniverses')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''UniversesFolder'' else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	if(@tree_code = 'Teradata')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, si_kind, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po stworzeniu drugiego @sql'
			   
	EXECUTE(@sql);

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);
	
	-- fix na foldery główne
	if(@tree_code = 'Reports' or @tree_code = 'ObjectUniverses' or @tree_code = 'Connections')
	begin
		delete from #tmpNodes
		where si_parentid = '0' and si_id not in ('#BO$RootFolder', '#BO$UserFolder', '#BO$UniverseFolder', '#BO$ConnectionFolder')
		
		declare @rc1 int = 1
		while @rc1 > 0 begin
			delete from #tmpNodes
			where si_parentid <> '0' and not exists(select 1 from #tmpNodes g where g.si_id = #tmpNodes.si_parentid)
			
			set @rc1 = @@ROWCOUNT
		end;
	end;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po fixie rootów dla #tmpNodes'
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/

	declare @rc int = 1

	while @rc > 0 begin
	
	delete from #tmpNodes
	where (si_kind = 'UniversesFolder' or si_kind = 'ConnectionFolder' or si_kind = 'ReportFolder') and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/
	
	declare @report_tree_id int = dbo.fn_get_bo_actual_report_tree_id();

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updatetami'

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.descr, tree_id = @tree_id
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.tree_id = @tree_id and bik_node.obj_id = #tmpNodes.si_id and bik_node.linked_node_id is null
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed update podlinkowanych'
	--update podlinkowanych
	update bik_node 
	set /*parent_node_id = null, */node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		 descr = #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and (tree_id = @report_tree_id or tree_id in (select id from bik_tree where tree_kind in (select code from bik_tree_kind where allow_linking = 1))) and bik_node.linked_node_id is not null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed insertami'
	
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id
	where bik_node.id is null;

	set nocount on;

	drop table #primaryIds;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updateami parentow'
	

	update bik_node 
	set parent_node_id = bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id
	inner join bik_node bk on bk.tree_id = @tree_id and bk.obj_id = pt.si_parentid where bik_node.linked_node_id is null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed usuwaniem'
	
	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in 
	('Measure', 'Dimension', 'Detail', 'UniverseClass', 'ReportQuery', 'Filter', 'UniverseColumn', 'UniverseTable', 'UniverseAliasTable', 'UniverseDerivedTable', 'UniverseTablesFolder', 'ConnectionEngineFolder', 'ConnectionNetworkFolder')
				
	drop table #tmpNodes
	
	-- fix na is_built_in
	update bik_node
	set is_built_in = 1
	where tree_id = @tree_id
	and obj_id in ('#BO$RootFolder', '#BO$UserFolder', '#BO$UniverseFolder', '#BO$ConnectionFolder')
	and is_built_in = 0
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po usuwaniu i po dropie'

	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': koniec'
end;
go


if OBJECT_ID (N'dbo.fn_discard_quote_for_string', N'FN') is not null
    drop function dbo.fn_discard_quote_for_string;
go

create function dbo.fn_discard_quote_for_string(@string varchar(max))
returns varchar(max)
as
begin
  return replace(replace(replace(replace(@string, '''', ''), '"',''),']',''),'[','')
end
go


if OBJECT_ID (N'dbo.fn_get_string_to_n_char', N'FN') is not null
    drop function dbo.fn_get_string_to_n_char;
go

create function dbo.fn_get_string_to_n_char(@string varchar(max), @char varchar(max), @n int)
returns varchar(max)
as
begin
	declare @pos int = 1;
	declare @len int = DATALENGTH(@string);
	declare @pos2 int;
	declare @idx int = 1;
	declare @returnString varchar(max);
	
	while @pos <= @len begin
		set @pos2 = charindex(@char, @string, @pos)
		if @pos2 = 0 break;
		if @idx = @n
		begin
			set @returnString = substring(@string, 1, @pos2)
			break;
		end
		else
		begin
			set @idx = @idx + 1;
			set @pos = @pos2 + 1;
		end
	end
	return @returnString;
end
go


-- poprzednia wersja w pliku: alter db for v1.2.4.1 tf.sql
-- fix dla drzewa MSSQL
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_mssql_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
    
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	
    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');    
    declare @universe_kind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @mssqlTree int = dbo.fn_tree_id_by_code('MSSQL');

	---------- universe table --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ut.node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- universe table --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ut.node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- object --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- object --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- query --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- query --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- report --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- report --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId

	---------- universe --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- universe --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null


	
	---------- mssql table --> universe table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, ut.node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted=0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- mssql table --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bno on bno.id = ot.object_node_id
	and bno.tree_id = @universeTreeId
	and bno.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- mssql table --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql table --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id = @connection_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null

	---------- mssql DB --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id = @universe_kind
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bno on bno.id = ot.object_node_id
	and bno.tree_id = @universeTreeId
	and bno.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null		
	
    exec sp_move_bik_joined_objs_tmp '@MSSQL', '@Reports, @ObjectUniverses, @Connections'
    
end;
go


----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

exec sp_update_version '1.4.y1.2', '1.4.y1.3';
go
