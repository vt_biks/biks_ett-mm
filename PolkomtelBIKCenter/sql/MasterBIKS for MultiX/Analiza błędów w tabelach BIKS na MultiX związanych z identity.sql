if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##tmp'))
  exec('drop table ##tmp');

create table ##tmp (db_name sysname, tab_name sysname, id_col_name sysname null, max_id_val bigint null, curr_ident_val bigint null);

declare @cur1 cursor 

set @cur1 = cursor for --select name from master.sys.databases where name like '%biks%';
  select database_name from [mltx_database];
  --select database_name from (values ('biks_plusbank3')) x(database_name);
  
open @cur1;

declare @db_name sysname;

fetch next from @cur1 into @db_name;

while @@fetch_status = 0 begin

  print 'db name: ' + @db_name;
  
  declare @sql_txt varchar(max) = '
  insert into ##tmp (db_name, tab_name, id_col_name)
  select ' + quotename(@db_name, '''') + ', tab_name, id_col_name from
  (
  select t.name as tab_name, (select top 1 name from ' + quotename(@db_name) + '.sys.columns where object_id = t.object_id and is_identity = 1) as id_col_name
  from ' + quotename(@db_name) + '.sys.tables t
  where t.name like ''bik[_]%'' or t.name like ''dbau[_]%''
  ) x where id_col_name is not null';
  
  --print @sql_txt;
  
  exec(@sql_txt);
  
  fetch next from @cur1 into @db_name;
end;

deallocate @cur1;

declare @cur2 cursor;

set @cur2 = cursor for select db_name, tab_name, id_col_name from ##tmp;

open @cur2;

declare @tab_name sysname, @id_col_name sysname;

fetch next from @cur2 into @db_name, @tab_name, @id_col_name;

while @@fetch_status = 0 begin
  
  declare @sql_txt2 varchar(max) = '
  update ##tmp 
    set max_id_val = (select max(' + quotename(@id_col_name) + ') from ' + quotename(@db_name) + '..' + quotename(@tab_name) + '), 
    curr_ident_val = IDENT_CURRENT(''' + quotename(@db_name) + '..' + quotename(@tab_name) + ''')
  where db_name = ' + quotename(@db_name, '''') + ' and tab_name = ' + quotename(@tab_name, '''');

  --print @sql_txt2;

  exec(@sql_txt2);
  
  fetch next from @cur2 into @db_name, @tab_name, @id_col_name;
end;

deallocate @cur2;

select * from ##tmp where max_id_val is not null and max_id_val > curr_ident_val;

-- select quotename('ala''ma', '''')

-- select * from biks_plusbank3.sys.tables

/*

delete from bik_keyword;

insert into bik_keyword (body) values ('��w');

set identity_insert bik_keyword on;

insert into bik_keyword (id, body) values (10, 'aqq'), (1, 'pies'), (2, 'kot');

set identity_insert bik_keyword off;

insert into bik_keyword (body) values ('chomik');

select * from bik_keyword

*/
