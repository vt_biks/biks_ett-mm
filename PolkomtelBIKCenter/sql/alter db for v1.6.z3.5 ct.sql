exec sp_check_version '1.6.z3.5';
go

if not exists(select 1 from bik_admin where param = 'sapbo4.idtPath')
  insert into bik_admin(param, value) values('sapbo4.idtPath', '')
if not exists(select 1 from bik_admin where param = 'sapbo4.clientToolsPath')
  insert into bik_admin(param, value) values('sapbo4.clientToolsPath', '')
go

exec sp_update_version '1.6.z3.5', '1.6.z3.6';
go