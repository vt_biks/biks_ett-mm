/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.bssg.metadatapump.sapbo.reportsdk;

/**
 *
 * @author tflorczak
 */
public class ReportObjectBean {

    public int queryNodeId;
    public String universeCUID;
    public String universeName;
    public String objectName;
    public String className;
    public int reportSiId;

    public ReportObjectBean(int queryNodeId, String universeCUID, String universeName, String objectName, String className, int reportSiId) {
        this.queryNodeId = queryNodeId;
        this.universeCUID = universeCUID;
        this.universeName = universeName;
        this.objectName = objectName;
        this.className = className;
        this.reportSiId = reportSiId;
    }
}
