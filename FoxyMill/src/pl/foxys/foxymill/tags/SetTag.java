/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.tags;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import pl.foxys.foxymill.CacheItem;
import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.IRenderable;
import pl.foxys.foxymill.IRenderableWithParamSignature;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import pl.trzy0.foxy.commons.templates.ParamInfo;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public class SetTag extends TagWithExpr implements IRenderableWithParamSignature {

    private String var;
    private String renderStr;
    private boolean doRender;
    private Map<String, ParamInfo> params;
    //private IParsedExpression expr;

    @Override
    protected void renderWithExprVal(//IRenderContext
            IRenderCtx renderCtx, Object val) {
        Object res = null;

        if (parsedExpr != null) {
            res = val;
            if (res instanceof IRenderable && doRender) {
                IRenderable r = (IRenderable) res;
                CacheItem cacheItem = FoxyMillUtils.renderToCacheItem(//renderCtx,
                        renderCtx.getRootModel(), new HashMap<String, Object>(),
                        r);
                res = cacheItem;
//                res = FoxyMillUtils.renderWithParams(r, null, renderCtx//.getEvalContext()
//                        );
            }
        } else {
            IRenderable r = FoxyMillUtils.wrapAsRenderable(subTags, null, params);
            if (doRender) {
                CacheItem cacheItem = FoxyMillUtils.renderToCacheItem(//renderCtx,
                        renderCtx.getRootModel(), renderCtx.getVars(),
                        r);
                res = cacheItem;
//                res = new RawString(FoxyMillUtils.renderWithParams(r, renderCtx.getVars(),
//                        renderCtx//.getEvalContext()
//                        ));
            } else {
                res = r;
            }
        }
        renderCtx.getVars().put(var, res);
    }

    @Override
    protected void initSelf() {
        var = //attrs.get("var");
                getReqAttrVal("var");
        renderStr = attrs.get("render");
        doRender = BaseUtils.safeEquals(renderStr, "true");

        String paramsStr = attrs.get("params");
        params = new LinkedHashMap<String, ParamInfo>();
        if (paramsStr != null) {
            Set<String> ps = BaseUtils.splitUniqueBySep(paramsStr, ",", true);
            for (String p : ps) {
                ParamInfo pi = new ParamInfo();
                pi.name = p;
                pi.type = Object.class;
                pi.required = true;
                params.put(p, pi);
            }
        }


//        String exprStr = attrs.get("expr");
//        if (exprStr == null && subTags == null) {
//            throw new LameRuntimeException("set tag must have expr attr or subTags");
//        }
    }

    @Override
    protected boolean isExprAttrRequired() {
        return subTags == null;
    }

    public Collection<String> getParamNames() {
        return params.keySet();
    }

    public ParamInfo getParamInfo(String paramName) {
        return params.get(paramName);
    }
//    @Override
//    protected void prepareSelf() {
//        expr = attrToExpr("expr");
//    }
}
