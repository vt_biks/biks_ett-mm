IF OBJECT_ID (N'dbo.fn_KEYWORD_ID', N'FN') IS NOT NULL
    DROP FUNCTION dbo.fn_KEYWORD_ID;
GO

CREATE FUNCTION dbo.fn_KEYWORD_ID(@body varchar(2000))
RETURNS int
WITH EXECUTE AS CALLER
AS
BEGIN  
  return (select ID from aaa_keyword where body = @body);
END;
GO


IF OBJECT_ID (N'dbo.fn_TREE_NODE_ID', N'FN') IS NOT NULL
    DROP FUNCTION dbo.fn_TREE_NODE_ID;
GO

CREATE FUNCTION dbo.fn_TREE_NODE_ID(@tree_num int, @caption varchar(1000))
RETURNS int
WITH EXECUTE AS CALLER
AS
BEGIN  
  return (select ID from aaa_tree_node where tree_num = @tree_num and caption = @caption);
END;
GO

IF OBJECT_ID (N'dbo.fn_USER_ID', N'FN') IS NOT NULL
    DROP FUNCTION dbo.fn_USER_ID;
GO

CREATE FUNCTION dbo.fn_USER_ID(@name varchar(1000))
RETURNS int
WITH EXECUTE AS CALLER
AS
BEGIN  
  return (select ID from aaa_user where name = @name);
END;
GO


insert into aaa_tree_node (tree_num, caption, parent_id, id_path) values (1, 'fandoplex', null, '?1');
insert into aaa_tree_node (tree_num, caption, parent_id, id_path) values (1, 'zbyszek', dbo.fn_TREE_NODE_ID(1, 'fandoplex'), '?2');
insert into aaa_tree_node (tree_num, caption, parent_id, id_path) values (1, 'asia', dbo.fn_TREE_NODE_ID(1, 'fandoplex'), '?6');
insert into aaa_tree_node (tree_num, caption, parent_id, id_path) values (1, 'wezyr', dbo.fn_TREE_NODE_ID(1, 'asia'), '?3');
insert into aaa_tree_node (tree_num, caption, parent_id, id_path) values (1, 'damian', dbo.fn_TREE_NODE_ID(1, 'zbyszek'), '?4');
insert into aaa_tree_node (tree_num, caption, parent_id, id_path) values (1, 'bssg', dbo.fn_TREE_NODE_ID(1, 'zbyszek'), '?5');

-- select * from aaa_tree_node
-- delete from aaa_tree_node where caption = 'wezyr' and tree_num = 1

insert into aaa_entity_in_tree_node (tree_node_id, si_id) values (dbo.fn_TREE_NODE_ID(1, 'fandoplex'), 8930);
insert into aaa_entity_in_tree_node (tree_node_id, si_id) values (dbo.fn_TREE_NODE_ID(1, 'wezyr'), 10351);
insert into aaa_entity_in_tree_node (tree_node_id, si_id) values (dbo.fn_TREE_NODE_ID(1, 'wezyr'), 10352);
insert into aaa_entity_in_tree_node (tree_node_id, si_id) values (dbo.fn_TREE_NODE_ID(1, 'wezyr'), 10780);
insert into aaa_entity_in_tree_node (tree_node_id, si_id) values (dbo.fn_TREE_NODE_ID(1, 'damian'), 7360);
insert into aaa_entity_in_tree_node (tree_node_id, si_id) values (dbo.fn_TREE_NODE_ID(1, 'damian'), 7361);


    select SI_ID, SI_KIND, SI_NAME, SI_PARENTID from aaa_global_props
    where SI_PARENTID in (7361) and SI_KIND in ('Folder', 'Universe', 'Webi', 'Pdf', 'Flash', 'CristalReports','MetaData.DataConnection')
