set nocount on


if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#has_new_to_del'))
drop table #has_new_to_del
go

create table #has_new_to_del (table_name sysname not null primary key)

insert into #has_new_to_del (table_name) values ('bik_node')

if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##ww_td_bik_node'))
drop table ##ww_td_bik_node
go

create table ##ww_td_bik_node (id int not null primary key)
insert into ##ww_td_bik_node (id) select id from bik_node where is_deleted = 1

insert into ##ww_td_bik_node (id) select id from bik_node
where is_deleted = 0 and tree_id in (select id from bik_tree where is_hidden = 1)


if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#table_to_del_rows'))
drop table #table_to_del_rows
go

create table #table_to_del_rows (table_name sysname not null primary key)

declare @drop_tmp_txt varchar(max) = ''

select @drop_tmp_txt = @drop_tmp_txt + 'drop table ' + name + '
'
-- object_id('tempdb..' + name), * 
from tempdb.sys.objects
where object_id('tempdb..' + name) is not null and name like '##ww[_]td[_]%'
and object_id('tempdb..' + name) not in (
  select tmp_tab_id from (select object_id('tempdb..##ww_td_' + table_name) as tmp_tab_id from #has_new_to_del) x
  where tmp_tab_id is not null)

--select @drop_tmp_txt
exec(@drop_tmp_txt)

declare @step int = 0

while exists(select 1 from #has_new_to_del)
begin
  declare @table_name sysname = (select top 1 table_name from #has_new_to_del)
  
  print '-- step #' + cast(@step as varchar(10)) + ', table name: ' + @table_name
  
  if not exists(select 1 from #table_to_del_rows where table_name = @table_name)
  insert into #table_to_del_rows (table_name) values (@table_name)
  
  delete from #has_new_to_del where table_name = @table_name
  
  --set @lvl = @lvl + 1

  declare @sql_txt varchar(max) = ''

  select 
  @sql_txt = @sql_txt + '
' +
  'if not exists(select 1 from tempdb.sys.objects where object_id = object_id(''tempdb..##ww_td_' + Table_Name + ''')) exec(''create table ' + tmp_tab_name + ' (' + pk_typed + ', primary key (' + pk_untyped + '))'')
' +  
    'insert into ' + tmp_tab_name + ' (' + pk_untyped +     
    ') select ' + 'ree.' + replace(pk_untyped, ',', ',ree.') +
    ' from ' + table_name + ' ree inner join ##ww_td_' + ReferencedTableName + ' red on ' + join_cond + ' left join ' + tmp_tab_name + ' tt on ' +
    dbo.fn_ww_self_join_cond(pk_untyped, 'ree', 'tt') + ' where ' + replace('tt.' + pk_untyped, ',', ' is null and tt.') + ' is null
' + 
'if @@rowcount <> 0 and not exists(select 1 from #has_new_to_del where table_name = ''' + table_name + ''') insert into #has_new_to_del (table_name) values (''' + table_name + ''')'
  from (
  select
  table_name, dbo.fn_ww_table_key_cols(table_name, 1, 2) as pk_typed,
  dbo.fn_ww_table_key_cols(table_name, 0, 2) as pk_untyped,
  dbo.fn_ww_fk_join_cond(ForeignKey_Name, 'ree', 'red') as join_cond,
  case when exists(select 1 from tempdb.sys.objects where object_id = object_id('tempdb..##ww_td_' + Table_Name)) then 1 else 0 end
    as has_tmp_tab,
  '##ww_td_' + Table_Name as tmp_tab_name, ReferencedTableName
  from (
  select distinct foreignkey_name, Table_Name, ReferencedTableName
  from vw_ww_foreignkeys
  where ReferencedTableName = --'bik_node' --
    @table_name
  ) x
  ) y  
  
  --print @sql_txt
  
  exec(@sql_txt)
  
  set @step = @step + 1
end

declare @nocheck_constraints varchar(max) = ''
declare @check_constraints varchar(max) = ''
declare @delete_rows varchar(max) = ''

select @nocheck_constraints = @nocheck_constraints +
'alter table ' + table_name + ' nocheck constraint all
',
@check_constraints = @check_constraints +
'alter table ' + table_name + ' check constraint all
',
@delete_rows = @delete_rows +
'delete from ' + table_name + ' from ##ww_td_' + table_name + ' as td where ' +
   dbo.fn_ww_self_join_cond(dbo.fn_ww_table_key_cols(table_name, 0, 2), table_name, 'td') + '
'
from #table_to_del_rows

print '
-- nocheck constraints'
--print @nocheck_constraints
exec(@nocheck_constraints)

print '
-- delete rows'
--print @delete_rows
exec(@delete_rows)

print '
-- check constraints'
--print @check_constraints
exec(@check_constraints)
