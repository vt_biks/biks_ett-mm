/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import commonlib.UrlMakingUtils;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.dqm.DQMConstanst;
import pl.fovis.server.DQMServiceImpl;
import pl.fovis.server.ExportManagerBase;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class DQMExportReportManager extends ExportManagerBase {

    protected DQMServiceImpl service;

    public DQMExportReportManager(DQMServiceImpl service) {
        this.service = service;
    }

    @Override
    protected Pair<String, List<Map<String, Object>>> getFileNamePrefixAndContent(HttpServletRequest req) {
        String reportId = req.getParameter(DQMConstanst.DQM_EXPORT_REPORT_PARAM_NAME);
        Integer intReportId = Integer.valueOf(reportId);
        Pair<String, List<Map<String, Object>>> res = service.getDQMDataToExport(intReportId);
        res.v1 = UrlMakingUtils.encodeStringForUrlWW(res.v1).replace("+", "%20");
        return res;
    }

    @Override
    protected String getFileFormat(HttpServletRequest req) {
        return BIKConstants.EXPORT_FORMAT_XLSX;
    }

}
