﻿exec sp_check_version '1.0.86';
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_add_converted_varchar_branch_after_insert_to_bik_node]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_add_converted_varchar_branch_after_insert_to_bik_node]
GO

create procedure sp_add_converted_varchar_branch_after_insert_to_bik_node
as
begin
update aaa_universe_obj set obj_branch=convert(varchar(30),uo.si_id) + '|' +  convert(varchar(30),uc.si_id) + '|' + convert(varchar(30),u.global_props_si_id)
from aaa_universe_obj uo 
join aaa_universe_class uc on uo.universe_class_id=uc.id
join aaa_universe u on uc.universe_id=u.id

update aaa_universe_filtr set filtr_branch=convert(varchar(30), uf.si_id) + '|' + convert(varchar(30), uc2.si_id) + '|' + convert(varchar(30), u3.global_props_si_id)
from aaa_universe_filtr uf
left join aaa_universe_class uc2 on uf.universe_class_id = uc2.id
left join aaa_universe u3 on u3.id = uc2.universe_id

update aaa_universe_table set universe_branch=convert(varchar(30),u2.global_props_si_id)
from aaa_universe_table ut
join aaa_universe u2 on ut.universe_id=u2.id
end
go

exec sp_add_converted_varchar_branch_after_insert_to_bik_node
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_object_table]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_sapbo_object_table]
GO

create procedure sp_insert_into_bik_node_sapbo_object_table
as
begin

declare @tree_id int;
select @tree_id = id from bik_tree where code='ObjectUniverses'
declare @universe_kind int;
select @universe_kind = id from bik_node_kind where code='Universe'
declare @filtr_kind int;
select @filtr_kind = id from bik_node_kind where code='Filter'
declare @detail_nki int;
select @detail_nki = id	from bik_node_kind where code = 'Detail';
declare @dimension_nki int;
select @dimension_nki = id from bik_node_kind where code = 'Dimension';
declare @measure_nki int;
select @measure_nki = id from bik_node_kind	where code = 'Measure';

insert into bik_sapbo_object_table
select *
from (
select case when uot.obj_type=1 then bn.id else bn3.id end as obj_node_ide, sut.id from aaa_universe_obj_tables uot
left join aaa_universe_obj uo on uot.universe_obj_id=uo.id
left join bik_node bn on bn.obj_id = uo.obj_branch
and bn.is_deleted=0
and bn.linked_node_id is null
and bn.tree_id=@tree_id
and bn.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)
join aaa_universe_table ut on uot.universe_table_id=ut.id
join bik_node bn2 on bn2.obj_id = ut.universe_branch
and bn2.is_deleted=0
and bn2.linked_node_id is null
and bn2.node_kind_id=@universe_kind
join bik_sapbo_universe_table sut on ut.name=sut.name
and sut.universe_node_id=bn2.id
left join aaa_universe_filtr uf on uot.universe_obj_id=uf.id
left join bik_node bn3 on bn3.obj_id = uf.filtr_branch
and bn3.is_deleted=0
and bn3.linked_node_id is null
and bn3.tree_id=@tree_id
and bn3.node_kind_id=@filtr_kind
left join bik_sapbo_object_table bsot on bsot.object_node_id=case when uot.obj_type=1 then bn.id else bn3.id end and bsot.table_id=sut.id
where bsot.id is null
) x
where obj_node_ide is not null
end
go

----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
--------------------Zamiana tezaurus -> glosariusz-----------------
update bik_node_kind set code='Glossary', caption='Glosariusz',
icon_name='glosariusz',tree_kind='Glossary' where code='Tezaurus' 

update bik_node_kind set code='GlossaryCategory',tree_kind='Glossary' where code=
'TezaurusCategory'

update bik_node_kind set tree_kind ='Glossary' where tree_kind='Tezaurus'

update bik_tree set name='Glosariusz',code='Glossary',tree_kind='Glossary'
where name='Tezaurus'
go

---------------------Czystki Glosariusza-----------------

delete from bik_node where linked_node_id in (select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id where bt.code='Glossary')
delete from bik_object_in_fvs  where node_id in  (select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id where bt.code='Glossary')
delete from bik_object_in_history  where node_id in  (select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id where bt.code='Glossary')
delete from bik_joined_objs where src_node_id in (select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id where bt.code='Glossary')
or dst_node_id in (select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id where bt.code='Glossary')
delete from bik_attribute_linked where node_id in (select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id where bt.code='Glossary')
delete from bik_node_vote where node_id in (select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id where bt.code='Glossary')
delete from bik_user_in_node  where node_id in (select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id where bt.code='Glossary')
delete from bik_note  where node_id in (select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id where bt.code='Glossary')
delete from bik_specialist where node_id in (select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id where bt.code='Glossary')
delete from bik_obj_in_body_node where linked_node_id in (select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id where bt.code='Glossary')
delete from bik_def_department
delete from bik_biz_def_body
delete from bik_biz_def
delete from bik_metapedia 
delete from bik_blog_linked
delete from bik_keyword_linked where node_id in (select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id where bt.code='Glossary') 
delete from bik_node where tree_id in (select id from bik_tree where code='Glossary')
go


------------------------Słowa kluczowe-------------------

insert into bik_node(parent_node_id,node_kind_id,name,tree_id,linked_node_id,branch_ids,obj_id,descr,is_deleted)
select NULL,id,'Słowa kluczowe',(select id from bik_tree where code='Glossary'),NULL,NULL,NULL,NULL,0 
from bik_node_kind where code='GlossaryCategory'

update bik_node set branch_ids=convert(varchar, id) + '|' where name='Słowa kluczowe'
go

--------------------------------------------------------------
exec sp_update_version '1.0.86', '1.0.86.1';
go
