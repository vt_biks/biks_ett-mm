/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.http.client.Request;
import pl.fovis.client.BIKClientSingletons;

/**
 *
 * @author wezyr
 */
public class PageDataFetchBroker implements IPageDataFetchBroker {

    // null oznacza, że nie odbywa się odczyt danych w tej chwili
    protected TreeDataRequestKind treeDataRequestKind;
    // czy aktualnie wyświetlane są jakieś dane (!= null)
    // jeżeli przefiltrowane, to isFullDataFetched == false
    // w przeciwnym razie (isFullDataFetched == true) - są to pełne dane
    protected Boolean isFullDataFetched = null;
    protected Request fetchRequest;
    protected String fetchDataRequestTicket;

    @Override
    public void dataFetchStarting(TreeDataRequestKind treeDataRequestKind, Request fetchRequest, String optRequestTicket) {
        // Additional można zawołać tylko gdy dane są już wczytane i nie jest właśnie
        // wykonywany żaden request albo jest to inny Additional
        if (treeDataRequestKind == TreeDataRequestKind.Additional
                && (this.isFullDataFetched == null
                || this.treeDataRequestKind != null
                && this.treeDataRequestKind != TreeDataRequestKind.Additional)) {
            BIKClientSingletons.showWarning("cannot call additional request" /* I18N: no */);
            throw new RuntimeException("cannot call additional request" /* I18N: no */);
        }

//        if (treeDataRequestKind != TreeDataRequestKind.Additional || this.treeDataRequestKind != TreeDataRequestKind.Additional) {
        dataFetchCancel();
//        }

        //this.isFullDataRequested = isFullData;
        this.treeDataRequestKind = treeDataRequestKind;
        this.fetchRequest = fetchRequest;
        this.fetchDataRequestTicket = optRequestTicket;
    }

    public void dataFetchCancel() {
        if (fetchRequest != null && fetchRequest.isPending()) {
            if (fetchDataRequestTicket != null) {
                BIKClientSingletons.cancelServiceRequest(fetchDataRequestTicket);
                fetchDataRequestTicket = null;
            }
            fetchRequest.cancel();
            fetchRequest = null;
        }
        //isFullDataRequested = null;
        treeDataRequestKind = null;
    }

    public void dataFetchFinished(boolean success) {
        if (success) {
            if (treeDataRequestKind != treeDataRequestKind.Additional) {
                isFullDataFetched = treeDataRequestKind == treeDataRequestKind.Full;
            }
        }
        //this.isFullDataRequested = null;
        treeDataRequestKind = null;
        fetchRequest = null;
        fetchDataRequestTicket = null;
    }

    public TreeDataRequestKind getRequestInProgressKind() {
        return treeDataRequestKind;
    }

    public Boolean isFullDataFetched() {
        return this.isFullDataFetched;
    }
}
