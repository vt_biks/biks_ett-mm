package pl.fovis.foxygwtcommons.i18n;

import simplelib.i18nsupport.I18nMessage;
import simplelib.i18nsupport.I18nMessagePlEn;
import simplelib.i18nsupport.I18nMessages;

public class I18n implements I18nMessages<I18nMessagePlEn> {

    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.dialogs.OneListBoxDialog
    public static final I18nMessage bRAKWARTOSCI = new I18nMessagePlEn("BRAK WARTOŚCI", "No values") /* I18N:  */; // #1
    // is translated
    // multiple occurrences: 2
    // pl.fovis.foxygwtcommons.TableColWidth
    // pl.fovis.foxygwtcommons.FoxyClientSingletons
    public static final I18nMessage blad = new I18nMessagePlEn("Błąd", "Error") /* I18N:  */; // #2
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.AddTableDialog
    public static final I18nMessage bladIloscWierszyIKolumnMusiBycDo = new I18nMessagePlEn("Błąd! Ilość wierszy i kolumn musi być dodatnią liczbą całkowitą! Maksymalna liczba kolumn to 20; wierszy", "Error! Number of rows and columns must be positive. The maximum number for columns: 20 and for rows") /* I18N:  */; // #3
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.FoxyClientSingletons
    public static final I18nMessage bladKomunikacji = new I18nMessagePlEn("Błąd komunikacji", "Error in communication") /* I18N:  */; // #4
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.FoxyFileUpload
    public static final I18nMessage brakPodanegoPlikuNieMaCoWysylac = new I18nMessagePlEn("Brak podanego pliku, nie ma co wysyłać", "No file selected, nothing to send") /* I18N:  */; // #5
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage brakTabeli = new I18nMessagePlEn("Brak tabeli", "No table") /* I18N:  */; // #6
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.StandardAsyncCallback
    public static final I18nMessage cosPoszloNieTak = new I18nMessagePlEn("Coś poszło nie tak", "Something went wrong") /* I18N:  */; // #7
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage dodajKolumneZLewej = new I18nMessagePlEn("Dodaj kolumnę z lewej", "Add a column to the left") /* I18N:  */; // #8
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage dodajKolumneZPrawej = new I18nMessagePlEn("Dodaj kolumnę z prawej", "Add a column to the right") /* I18N:  */; // #9
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage dodajNaglowkiH1PojawiaSieOneWSpi = new I18nMessagePlEn("Dodaj nagłówki (H1). Pojawią się one w spisie treści", "Add headers (H1). They will appear in the table of contents") /* I18N:  */; // #10
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.KeywordPickerDialogBase
    public static final I18nMessage dodajNowyTag = new I18nMessagePlEn("Dodaj nowy tag", "Add new tag") /* I18N:  */; // #11
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage dodajWierszPonizej = new I18nMessagePlEn("Dodaj wiersz poniżej", "Add row below") /* I18N:  */; // #12
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage dodajWierszPowyzej = new I18nMessagePlEn("Dodaj wiersz powyżej", "Add row above") /* I18N:  */; // #13
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.FileUploadDialogForPhoto
    public static final I18nMessage dodajZdjecie = new I18nMessagePlEn("Dodaj zdjęcie", "Add photo") /* I18N:  */; // #14
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.dialogs.validators.WidgetValueValidatorLength
    public static final I18nMessage dopuszczalneNajwyzej = new I18nMessagePlEn("Dopuszczalne najwyżej", "Allowed a maximum of") /* I18N:  */; // #15
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.AddTableDialog
    public static final I18nMessage iloscKolumn = new I18nMessagePlEn("Ilość kolumn", "Number of columns") /* I18N:  */; // #16
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.AddTableDialog
    public static final I18nMessage iloscWierszy = new I18nMessagePlEn("Ilość wierszy", "Number of rows") /* I18N:  */; // #17
    // is translated
    // multiple occurrences: 2
    // pl.fovis.foxygwtcommons.FoxyClientSingletons
    // pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog
    public static final I18nMessage informacja = new I18nMessagePlEn("Informacja", "Info") /* I18N:  */; // #18
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.TableColWidth
    public static final I18nMessage kolumny = new I18nMessagePlEn("kolumny", "of column") /* I18N: set xxx of column */; // #19
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.TableColWidth
    public static final I18nMessage kolumnyMusiBycDodatniaLiczbaCalk = new I18nMessagePlEn("kolumny musi być dodatnią liczbą całkowitą", "of column must be positive number") /* I18N:  */; // #20
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage nieZnalezioZadnychPozycjiSpisuTr = new I18nMessagePlEn("Nie znaleziono żadnych pozycji spisu treści", "No table of contents items found") /* I18N:  */; // #21
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.FoxyFileUpload
    public static final I18nMessage plikWyslany = new I18nMessagePlEn("Plik wysłany", "File sent") /* I18N:  */; // #22
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage podajAdresURL = new I18nMessagePlEn("Podaj adres URL", "Enter URL") /* I18N:  */; // #23
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage podajAdresURLDlaObrazka = new I18nMessagePlEn("Podaj adres URL dla obrazka", "Enter an image URL") /* I18N:  */; // #24
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.KeywordPickerDialogBase
    public static final I18nMessage pojeciaBiznesowe = new I18nMessagePlEn("Pojęcia biznesowe", "Business terms") /* I18N:  */; // #25
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage pokazHTML = new I18nMessagePlEn("Pokaż HTML", "Show HTML") /* I18N:  */; // #26
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.KeywordPickerDialogBase
    public static final I18nMessage pokazujWObiektachPodrzednych = new I18nMessagePlEn("Pokazuj w obiektach podrzędnych", "Show in descendant objects") /* I18N:  */; // #27
    // is translated
    // multiple occurrences: 2
    // pl.fovis.foxygwtcommons.dialogs.validators.WidgetValueValidatorRequired
    // pl.fovis.foxygwtcommons.KeywordPickerDialogBase
    public static final I18nMessage poleNieMozeBycPuste = new I18nMessagePlEn("Pole nie może być puste", "Field cannot be empty") /* I18N:  */; // #28
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.dialogs.ConfirmDialog
    public static final I18nMessage potwierdz = new I18nMessagePlEn("Potwierdź", "Confirm") /* I18N:  */; // #29
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.KeywordPickerDialogBase
    public static final I18nMessage powiazPojecieBiznesowe = new I18nMessagePlEn("Powiąż pojęcie biznesowe", "Associate business term") /* I18N:  */; // #30
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage spisTresci = new I18nMessagePlEn("Spis treści", "Table of contents") /* I18N:  */; // #31
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.grid.BeanGridPager
    public static final I18nMessage strona0Z1 = new I18nMessagePlEn("Strona {0} z {1}", "Page {0} of {1}") /* I18N:  */; // #32
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage szerokosc = new I18nMessagePlEn("Szerokość", "Width") /* I18N:  */; // #33
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.BasePickerDialog
    public static final I18nMessage szukaj = new I18nMessagePlEn("Szukaj", "Search") /* I18N:  */; // #34
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.KeywordPickerDialogBase
    public static final I18nMessage szukajLubDodaj = new I18nMessagePlEn("Szukaj lub dodaj", "Search or add") /* I18N:  */; // #35
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.dialogs.ConfirmDialog
    public static final I18nMessage tak = new I18nMessagePlEn("Tak", "Yes") /* I18N:  */; // #36
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.TableColWidth
    public static final I18nMessage ustaw = new I18nMessagePlEn("Ustaw", "Set") /* I18N:  */; // #37
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage ustawSzerokosc = new I18nMessagePlEn("Ustaw szerokość", "Set width") /* I18N:  */; // #38
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.grid.StandardBeanDisplay
    public static final I18nMessage usun = new I18nMessagePlEn("Usuń", "Delete") /* I18N:  */; // #39
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage usunKolumne = new I18nMessagePlEn("Usuń kolumnę", "Remove column") /* I18N:  */; // #40
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage usunWiersz = new I18nMessagePlEn("Usuń wiersz", "Remove row") /* I18N:  */; // #41
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.FoxyClientSingletons
    public static final I18nMessage warning = new I18nMessagePlEn("Ostrzeżenie", "Warning") /* I18N:  */; // #42
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.ViewURLLink
    public static final I18nMessage wcisnijCtrlCAbySkopiowaLinkDoSch = new I18nMessagePlEn("Wciśnij Ctrl+C aby skopiować link do schowka,<br /> następnie możesz go przesłać np. mailem, na czacie itp", "Press Ctrl+C to copy link to clipboard,<br /> then you can send it via mail, chat etc") /* I18N:  */; // #43
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.dialogs.FoxyDialogSerializer
    public static final I18nMessage wprowadzWartoscNieJestLiczbaCalk = new I18nMessagePlEn("Wprowadzona wartość nie jest liczbą całkowitą", "Value is not integer number") /* I18N:  */; // #44
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.dialogs.FoxyDialogSerializer
    public static final I18nMessage wprowadzonaWartoscNieJestLiczba = new I18nMessagePlEn("Wprowadzona wartość nie jest liczbą", "Value is not a number") /* I18N:  */; // #45
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.AddTableDialog
    public static final I18nMessage wstaw = new I18nMessagePlEn("Wstaw", "Insert") /* I18N: table */; // #46
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.AddTableDialog
    public static final I18nMessage wstawTabele = new I18nMessagePlEn("Wstaw tabelę", "Insert table") /* I18N:  */; // #47
    // is translated
    // multiple occurrences: 3
    // pl.fovis.foxygwtcommons.dialogs.OneListBoxDialog
    // pl.fovis.foxygwtcommons.KeywordPickerDialogBase
    // pl.fovis.foxygwtcommons.BasePickerDialog
    public static final I18nMessage wybierz = new I18nMessagePlEn("Wybierz", "Select") /* I18N:  */; // #48
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.dialogs.validators.WidgetValueValidatorLength
    public static final I18nMessage wymaganePrzynajmniej = new I18nMessagePlEn("Wymagane przynajmniej", "Required at least") /* I18N:  */; // #49
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.FoxyFileUpload
    public static final I18nMessage wyslij = new I18nMessagePlEn("Wyślij", "Send") /* I18N:  */; // #50
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.dialogs.StandardAsyncCallbackForDialog
    public static final I18nMessage wystapilBladPodczasWykonywaAkcji = new I18nMessagePlEn("Wystąpił błąd podczas wykonywania akcji.\n"
            + " Przejrzyj dane, popraw jeśli trzeba i spróbuj ponownie", "Error while executing action.\n"
            + " Check entered data, fix if needed and try again") /* I18N:  */; // #51
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.FoxyFileUpload
    public static final I18nMessage wysylaniePlikuWToku = new I18nMessagePlEn("Wysyłanie pliku w toku", "Upload in progress") /* I18N:  */; // #52
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.FileUploadDialogForPhoto
    public static final I18nMessage zaladuj = new I18nMessagePlEn("Załaduj", "Upload") /* I18N:  */; // #53
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog
    public static final I18nMessage zamknij = new I18nMessagePlEn("Zamknij", "Close") /* I18N:  */; // #54
    // is translated
    // multiple occurrences: 2
    // pl.fovis.foxygwtcommons.dialogs.OneTextBoxDialog
    // pl.fovis.foxygwtcommons.TableColWidth
    public static final I18nMessage zapisz = new I18nMessagePlEn("Zapisz", "Save") /* I18N:  */; // #55
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.grid.StandardBeanDisplay
    public static final I18nMessage zmien = new I18nMessagePlEn("Zmień", "Edit") /* I18N:  */; // #56
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.dialogs.validators.WidgetValueValidatorLength
    public static final I18nMessage znakowWprowadzono = new I18nMessagePlEn("znaków, wprowadzono", "chars, entered") /* I18N:  */; // #57
    // is translated
    // single occurrence
    // pl.fovis.foxygwtcommons.RichTextAreaFoxyToolbar
    public static final I18nMessage zobaczTezSpanA_noMaybeIdent = new I18nMessagePlEn("Zobacz też", "See also") /* I18N: no?maybe-ident */; // #58
    // not translated
    // no occurrences
    public static final I18nMessage added = new I18nMessagePlEn("added") /* i18n:  */; // #59
    // not translated
    // no occurrences
    public static final I18nMessage allowed = new I18nMessagePlEn("allowed") /* i18n:  */; // #60
    // not translated
    // no occurrences
    public static final I18nMessage arial = new I18nMessagePlEn("Arial") /* i18n:  */; // #61
    // not translated
    // no occurrences
    public static final I18nMessage beanStringPropNameObjectValue = new I18nMessagePlEn("bean, String propName, Object value") /* i18n:  */; // #62
    // not translated
    // no occurrences
    public static final I18nMessage beanStringPropNameReturn_noMaybeIdent = new I18nMessagePlEn("bean, String propName) {\n"
            + "                return") /* i18n: no?maybe-ident */; // #63
    // not translated
    // no occurrences
    public static final I18nMessage beforeGenerateClassBody_noMaybeIdent = new I18nMessagePlEn("before generateClassBody") /* i18n: no?maybe-ident */; // #64
    // not translated
    // no occurrences
    public static final I18nMessage black = new I18nMessagePlEn("Czarny", "Black") /* i18n:  */; // #65
    // not translated
    // no occurrences
    public static final I18nMessage blue = new I18nMessagePlEn("Niebieski", "Blue") /* i18n:  */; // #66
    // not translated
    // no occurrences
    public static final I18nMessage bolder = new I18nMessagePlEn("bolder") /* i18n:  */; // #67
    // not translated
    // no occurrences
    public static final I18nMessage broker = new I18nMessagePlEn("broker") /* i18n:  */; // #68
    // not translated
    // no occurrences
    public static final I18nMessage brokerNewINamedTypedPropsBeanBro_noMaybeIdent = new I18nMessagePlEn("broker = "
            + "new INamedTypedPropsBeanBroker") /* i18n: no?maybe-ident */; // #69
    // not translated
    // no occurrences
    public static final I18nMessage brokersNewLinkedHashMap_noMaybeIdent = new I18nMessagePlEn("brokers = new LinkedHashMap") /* i18n: no?maybe-ident */; // #70
    // not translated
    // no occurrences
    public static final I18nMessage butHasNoDefaultConstrucCannotBeI = new I18nMessagePlEn("but has no default constructor, cannot be instantiated dynamically. to suppress this warning annotate class with @DisableDynamicClassCreation") /* i18n:  */; // #71
    // not translated
    // no occurrences
    public static final I18nMessage cannotFindClass = new I18nMessagePlEn("cannot find class") /* i18n:  */; // #72
    // not translated
    // no occurrences
    public static final I18nMessage cannotFindPackageForImplemen = new I18nMessagePlEn("cannot find package for implementations") /* i18n:  */; // #73
    // not translated
    // no occurrences
    public static final I18nMessage cannotGetDynamicClassCreatorAsGe = new I18nMessagePlEn("cannot get dynamic class creator as generic type") /* i18n:  */; // #74
    // not translated
    // no occurrences
    public static final I18nMessage cannotGetParametrOfDynamicClassC = new I18nMessagePlEn("cannot get parametrization of dynamic class creator for class") /* i18n:  */; // #75
    // not translated
    // no occurrences
    public static final I18nMessage ccc = new I18nMessagePlEn("ccc") /* i18n:  */; // #76
    // not translated
    // no occurrences
    public static final I18nMessage class2_ = new I18nMessagePlEn("class") /* i18n:  */; // #77
    // not translated
    // no occurrences
    public static final I18nMessage classBroker = new I18nMessagePlEn("class, broker") /* i18n:  */; // #78
    // not translated
    // no occurrences
    public static final I18nMessage classCreator = new I18nMessagePlEn("class, creator") /* i18n:  */; // #79
    // not translated
    // no occurrences
    public static final I18nMessage classEsInThisPackage = new I18nMessagePlEn("class(es) in this package") /* i18n:  */; // #80
    // not translated
    // no occurrences
    public static final I18nMessage classEsOfTotal = new I18nMessagePlEn("class(es) of total") /* i18n:  */; // #81
    // not translated
    // no occurrences
    public static final I18nMessage class_ = new I18nMessagePlEn("Class") /* i18n:  */; // #82
    // not translated
    // no occurrences
    public static final I18nMessage clear = new I18nMessagePlEn("Clear") /* i18n:  */; // #83
    // not translated
    // no occurrences
    public static final I18nMessage clearNotSupported = new I18nMessagePlEn("clear not supported") /* i18n:  */; // #84
    // not translated
    // no occurrences
    public static final I18nMessage cls = new I18nMessagePlEn("cls") /* i18n:  */; // #85
    // not translated
    // no occurrences
    public static final I18nMessage courierNew = new I18nMessagePlEn("Courier New") /* i18n:  */; // #86
    // not translated
    // no occurrences
    public static final I18nMessage createReturnNew = new I18nMessagePlEn("create() { return new") /* i18n:  */; // #87
    // not translated
    // no occurrences
    public static final I18nMessage createStringNameIObjectCreator_noMaybeIdent = new I18nMessagePlEn("create(String name) { "
            + "IObjectCreator") /* i18n: no?maybe-ident */; // #88
    // not translated
    // no occurrences
    public static final I18nMessage createsSubclassesOf = new I18nMessagePlEn("creates subclasses of") /* i18n:  */; // #89
    // not translated
    // no occurrences
    public static final I18nMessage creator = new I18nMessagePlEn("creator") /* i18n:  */; // #90
    // not translated
    // no occurrences
    public static final I18nMessage creatorCreatorsGetNameIfCreatorN_noMaybeIdent = new I18nMessagePlEn("creator = creators.get(name);"
            + "if (creator == null") /* i18n: no?maybe-ident */; // #91
    // not translated
    // no occurrences
    public static final I18nMessage creatorNew = new I18nMessagePlEn("creator = new") /* i18n:  */; // #92
    // not translated
    // no occurrences
    public static final I18nMessage creatorNewIObjectCreator_noMaybeIdent = new I18nMessagePlEn("creator = new IObjectCreator") /* i18n: no?maybe-ident */; // #93
    // not translated
    // no occurrences
    public static final I18nMessage creatorsNewLinkedHashMap_noMaybeIdent = new I18nMessagePlEn("creators = new LinkedHashMap") /* i18n: no?maybe-ident */; // #94
    // not translated
    // no occurrences
    public static final I18nMessage db = new I18nMessagePlEn("Db") /* i18n:  */; // #95
    // not translated
    // no occurrences
    public static final I18nMessage declared = new I18nMessagePlEn("declared") /* i18n:  */; // #96
    // not translated
    // no occurrences
    public static final I18nMessage descendantS = new I18nMessagePlEn("descendant(s") /* i18n:  */; // #97
    // not translated
    // no occurrences
    public static final I18nMessage disallowed = new I18nMessagePlEn("disallowed") /* i18n:  */; // #98
    // not translated
    // no occurrences
    public static final I18nMessage doneGenerateClassBodyBeforeCommi_noMaybeIdent = new I18nMessagePlEn("done generateClassBody, before commit") /* i18n: no?maybe-ident */; // #99
    // not translated
    // no occurrences
    public static final I18nMessage doneGenerateOutClassName_noMaybeIdent = new I18nMessagePlEn("done generate, outClassName") /* i18n: no?maybe-ident */; // #100
    // not translated
    // no occurrences
    public static final I18nMessage dynamicClassCreator = new I18nMessagePlEn("dynamic class creator") /* i18n:  */; // #101
    // not translated
    // no occurrences
    public static final I18nMessage errorForGetResourceAsStreamBaseP_noMaybeIdent = new I18nMessagePlEn("error for getResourceAsStream, basePath") /* i18n: no?maybe-ident */; // #102
    // not translated
    // no occurrences
    public static final I18nMessage extendWithDescendantsDoneAdded_noMaybeIdent = new I18nMessagePlEn("extendWithDescendants done, added") /* i18n: no?maybe-ident */; // #103
    // not translated
    // no occurrences
    public static final I18nMessage font = new I18nMessagePlEn("Czcionka", "Font") /* i18n:  */; // #104
    // not translated
    // no occurrences
    public static final I18nMessage foundClassesInPackage = new I18nMessagePlEn("found classes in package") /* i18n:  */; // #105
    // not translated
    // no occurrences
    public static final I18nMessage foundResources = new I18nMessagePlEn("found resources") /* i18n:  */; // #106
    // not translated
    // no occurrences
    public static final I18nMessage g_noSingleLetter = new I18nMessagePlEn("g") /* i18n: no?single-letter */; // #107
    // not translated
    // no occurrences
    public static final I18nMessage generateStartsTypeName = new I18nMessagePlEn("generate starts, typeName") /* i18n:  */; // #108
    // not translated
    // no occurrences
    public static final I18nMessage georgia = new I18nMessagePlEn("Georgia") /* i18n:  */; // #109
    // not translated
    // no occurrences
    public static final I18nMessage green = new I18nMessagePlEn("Zielony", "Green") /* i18n:  */; // #110
    // not translated
    // no occurrences
    public static final I18nMessage has = new I18nMessagePlEn("has") /* i18n:  */; // #111
    // not translated
    // no occurrences
    public static final I18nMessage iNamedTypedPropsBeanBrokerBCGetB_noMaybeIdent = new I18nMessagePlEn("INamedTypedPropsBeanBroker<BC> getBroker(Class<BC> beanClass) {\n"
            + "INamedTypedPropsBeanBroker<BC> broker = (INamedTypedPropsBeanBroker)brokers.get(beanClass);\n"
            + "if (broker == null") /* i18n: no?maybe-ident */; // #112
    // not translated
    // no occurrences
    public static final I18nMessage iNamedTypedPropsBeanBrokerExtend_noMaybeIdent = new I18nMessagePlEn("INamedTypedPropsBeanBroker<? extends") /* i18n: no?maybe-ident */; // #113
    // not translated
    // no occurrences
    public static final I18nMessage iNamedTypedPropsBeanCreatorIWrap_noMaybeIdent = new I18nMessagePlEn("INamedTypedPropsBean> creator = (IWrapperCreator)creators.get(cls);"
            + "if (creator == null") /* i18n: no?maybe-ident */; // #114
    // not translated
    // no occurrences
    public static final I18nMessage iWrapperCreatorExtends_noMaybeIdent = new I18nMessagePlEn("IWrapperCreator<? extends") /* i18n: no?maybe-ident */; // #115
    // not translated
    // no occurrences
    public static final I18nMessage i_noSingleLetter = new I18nMessagePlEn("i") /* i18n: no?single-letter */; // #116
    // not translated
    // no occurrences
    public static final I18nMessage ifResNull = new I18nMessagePlEn("if (res == null") /* i18n:  */; // #117
    // not translated
    // no occurrences
    public static final I18nMessage im = new I18nMessagePlEn("im") /* i18n:  */; // #118
    // not translated
    // no occurrences
    public static final I18nMessage impliedBy = new I18nMessagePlEn("implied by") /* i18n:  */; // #119
    // not translated
    // no occurrences
    public static final I18nMessage initialLineNumber = new I18nMessagePlEn("initial line number") /* i18n:  */; // #120
    // not translated
    // no occurrences
    public static final I18nMessage inputStreamIsNullForFile = new I18nMessagePlEn("input stream is null for file") /* i18n:  */; // #121
    // not translated
    // no occurrences
    public static final I18nMessage intfIDynamicMethodInvokerMustBeP_noMaybeIdent = new I18nMessagePlEn("intf IDynamicMethodInvoker must be parametrized") /* i18n: no?maybe-ident */; // #122
    // not translated
    // no occurrences
    public static final I18nMessage is = new I18nMessagePlEn("is") /* i18n:  */; // #123
    // not translated
    // no occurrences
    public static final I18nMessage isAssignableTo = new I18nMessagePlEn("is assignable to") /* i18n:  */; // #124
    // not translated
    // no occurrences
    public static final I18nMessage isGenericType = new I18nMessagePlEn("is generic type") /* i18n:  */; // #125
    // not translated
    // no occurrences
    public static final I18nMessage isNotClassTypeName_noMaybeIdent = new I18nMessagePlEn("is not class-type, name") /* i18n: no?maybe-ident */; // #126
    // not translated
    // no occurrences
    public static final I18nMessage lastPointsI_noMaybeIdent = new I18nMessagePlEn("lastPoints[i") /* i18n: no?maybe-ident */; // #127
    // not translated
    // no occurrences
    public static final I18nMessage lb = new I18nMessagePlEn("Lb") /* i18n:  */; // #128
    // not translated
    // no occurrences
    public static final I18nMessage m_noSingleLetter = new I18nMessagePlEn("m") /* i18n: no?single-letter */; // #129
    // not translated
    // no occurrences
    public static final I18nMessage mergedPath = new I18nMessagePlEn("merged path") /* i18n:  */; // #130
    // not translated
    // no occurrences
    public static final I18nMessage myType = new I18nMessagePlEn("my type") /* i18n:  */; // #131
    // not translated
    // no occurrences
    public static final I18nMessage noBrokerForClass = new I18nMessagePlEn("no broker for class") /* i18n:  */; // #132
    // not translated
    // no occurrences
    public static final I18nMessage noCreatorForClass = new I18nMessagePlEn("no creator for class") /* i18n:  */; // #133
    // not translated
    // no occurrences
    public static final I18nMessage noCreatorForName = new I18nMessagePlEn("no creator for name") /* i18n:  */; // #134
    // not translated
    // no occurrences
    public static final I18nMessage noResourceDirAnnotationFor = new I18nMessagePlEn("no ResourceDir annotation for") /* i18n:  */; // #135
    // not translated
    // no occurrences
    public static final I18nMessage noResourceWithName = new I18nMessagePlEn("no resource with name") /* i18n:  */; // #136
    // not translated
    // no occurrences
    public static final I18nMessage nx = new I18nMessagePlEn("nx") /* i18n:  */; // #137
    // not translated
    // no occurrences
    public static final I18nMessage ny = new I18nMessagePlEn("ny") /* i18n:  */; // #138
    // not translated
    // no occurrences
    public static final I18nMessage nz = new I18nMessagePlEn("nz") /* i18n:  */; // #139
    // not translated
    // no occurrences
    public static final I18nMessage object = new I18nMessagePlEn("Object") /* i18n:  */; // #140
    // not translated
    // no occurrences
    public static final I18nMessage overridePublicObjectGetPropValue_noMaybeIdent = new I18nMessagePlEn("Override\n"
            + "            public Object getPropValue") /* i18n: no?maybe-ident */; // #141
    // not translated
    // no occurrences
    public static final I18nMessage p_noSingleLetter = new I18nMessagePlEn("p") /* i18n: no?single-letter */; // #142
    // not translated
    // no occurrences
    public static final I18nMessage packagePath = new I18nMessagePlEn("package path") /* i18n:  */; // #143
    // not translated
    // no occurrences
    public static final I18nMessage packagePathForClass = new I18nMessagePlEn("package path for class") /* i18n:  */; // #144
    // not translated
    // no occurrences
    public static final I18nMessage parametrizationForClass = new I18nMessagePlEn("parametrization for class") /* i18n:  */; // #145
    // not translated
    // no occurrences
    public static final I18nMessage paths = new I18nMessagePlEn("paths") /* i18n:  */; // #146
    // not translated
    // no occurrences
    public static final I18nMessage point = new I18nMessagePlEn("point") /* i18n:  */; // #147
    // not translated
    // no occurrences
    public static final I18nMessage populateWithDescendaTestForNode_noMaybeIdent = new I18nMessagePlEn("populateWithDescendants: test for node") /* i18n: no?maybe-ident */; // #148
    // not translated
    // no occurrences
    public static final I18nMessage populateWithDescendantsForNode_noMaybeIdent = new I18nMessagePlEn("populateWithDescendants: for node") /* i18n: no?maybe-ident */; // #149
    // not translated
    // no occurrences
    public static final I18nMessage privateStaticFinalMap = new I18nMessagePlEn("private static final Map") /* i18n:  */; // #150
    // not translated
    // no occurrences
    public static final I18nMessage privateStaticMapStringStringReso = new I18nMessagePlEn("private static Map<String, String> resources = new LinkedHashMap<String, String") /* i18n:  */; // #151
    // not translated
    // no occurrences
    public static final I18nMessage progress = new I18nMessagePlEn("progress") /* i18n:  */; // #152
    // not translated
    // no occurrences
    public static final I18nMessage props = new I18nMessagePlEn("props") /* i18n:  */; // #153
    // not translated
    // no occurrences
    public static final I18nMessage publicBCExtends = new I18nMessagePlEn("public <BC extends") /* i18n:  */; // #154
    // not translated
    // no occurrences
    public static final I18nMessage publicINamedTypedPropsBeanCreate_noMaybeIdent = new I18nMessagePlEn(""
            + "public INamedTypedPropsBean create") /* i18n: no?maybe-ident */; // #155
    // not translated
    // no occurrences
    public static final I18nMessage publicSetStringGetNames = new I18nMessagePlEn("public Set<String> getNames") /* i18n:  */; // #156
    // not translated
    // no occurrences
    public static final I18nMessage publicSetStringGetNamesReturnCre_noMaybeIdent = new I18nMessagePlEn("public Set<String> getNames() { return creators.keySet") /* i18n: no?maybe-ident */; // #157
    // not translated
    // no occurrences
    public static final I18nMessage publicString = new I18nMessagePlEn("public String") /* i18n:  */; // #158
    // not translated
    // no occurrences
    public static final I18nMessage publicStringGetStringName = new I18nMessagePlEn("public String get(String name") /* i18n:  */; // #159
    // not translated
    // no occurrences
    public static final I18nMessage public_noMaybeIdent = new I18nMessagePlEn(""
            + "public") /* i18n: no?maybe-ident */; // #160
    // not translated
    // no occurrences
    public static final I18nMessage radius = new I18nMessagePlEn("radius") /* i18n:  */; // #161
    // not translated
    // no occurrences
    public static final I18nMessage red = new I18nMessagePlEn("Czerwony", "red") /* i18n:  */; // #162
    // not translated
    // no occurrences
    public static final I18nMessage returnBroker_noMaybeIdent = new I18nMessagePlEn(""
            + "return broker") /* i18n: no?maybe-ident */; // #163
    // not translated
    // no occurrences
    public static final I18nMessage returnCreatorCreateSrc_noMaybeIdent = new I18nMessagePlEn(""
            + "return creator.create(src") /* i18n: no?maybe-ident */; // #164
    // not translated
    // no occurrences
    public static final I18nMessage returnCreatorCreate_noMaybeIdent = new I18nMessagePlEn("return creator.create") /* i18n: no?maybe-ident */; // #165
    // not translated
    // no occurrences
    public static final I18nMessage returnRes = new I18nMessagePlEn("return res") /* i18n:  */; // #166
    // not translated
    // no occurrences
    public static final I18nMessage returnResourcesKeySet_noMaybeIdent = new I18nMessagePlEn("return resources.keySet") /* i18n: no?maybe-ident */; // #167
    // not translated
    // no occurrences
    public static final I18nMessage return_ = new I18nMessagePlEn("return") /* i18n:  */; // #168
    // not translated
    // no occurrences
    public static final I18nMessage row = new I18nMessagePlEn("row") /* i18n:  */; // #169
    // not translated
    // no occurrences
    public static final I18nMessage sb = new I18nMessagePlEn("Sb") /* i18n:  */; // #170
    // not translated
    // no occurrences
    public static final I18nMessage self = new I18nMessagePlEn("self") /* i18n:  */; // #171
    // not translated
    // no occurrences
    public static final I18nMessage setInputControlValueUndefineForI_noMaybeIdent = new I18nMessagePlEn("setInputControlValue undefined for inputControl class") /* i18n: no?maybe-ident */; // #172
    // not translated
    // no occurrences
    public static final I18nMessage showDiagMsgNotAllowedForKind = new I18nMessagePlEn("showDiagMsg not allowed for kind") /* i18n:  */; // #173
    // not translated
    // no occurrences
    public static final I18nMessage srcIfSrcNullReturnNullClassClsSr_noMaybeIdent = new I18nMessagePlEn("src) { "
            + "  if (src == null) return null;"
            + "  Class cls = src.getClass();"
            + "IWrapperCreator") /* i18n: no?maybe-ident */; // #174
    // not translated
    // no occurrences
    public static final I18nMessage srcReturnNew = new I18nMessagePlEn("src) { return new") /* i18n:  */; // #175
    // not translated
    // no occurrences
    public static final I18nMessage st_noMaybeIdent = new I18nMessagePlEn(""
            + "st") /* i18n: no?maybe-ident */; // #176
    // not translated
    // no occurrences
    public static final I18nMessage static_ = new I18nMessagePlEn("static") /* i18n:  */; // #177
    // not translated
    // no occurrences
    public static final I18nMessage stringIObjectCreator_noMaybeIdent = new I18nMessagePlEn("String, IObjectCreator") /* i18n: no?maybe-ident */; // #178
    // not translated
    // no occurrences
    public static final I18nMessage stringResResourcesGetName_noMaybeIdent = new I18nMessagePlEn("String res = resources.get(name") /* i18n: no?maybe-ident */; // #179
    // not translated
    // no occurrences
    public static final I18nMessage ta = new I18nMessagePlEn("Ta") /* i18n:  */; // #180
    // not translated
    // no occurrences
    public static final I18nMessage tb = new I18nMessagePlEn("Tb") /* i18n:  */; // #181
    // not translated
    // no occurrences
    public static final I18nMessage testForNode = new I18nMessagePlEn("test for node") /* i18n:  */; // #182
    // not translated
    // no occurrences
    public static final I18nMessage thisClassIsAlreadyCreated = new I18nMessagePlEn("this class is already created") /* i18n:  */; // #183
    // not translated
    // no occurrences
    public static final I18nMessage throwNewLameRuntimeException_noMaybeIdent = new I18nMessagePlEn("throw new LameRuntimeException") /* i18n: no?maybe-ident */; // #184
    // not translated
    // no occurrences
    public static final I18nMessage timesNewRoman = new I18nMessagePlEn("Times New Roman") /* i18n:  */; // #185
    // not translated
    // no occurrences
    public static final I18nMessage trebuchet = new I18nMessagePlEn("Trebuchet") /* i18n:  */; // #186
    // not translated
    // no occurrences
    public static final I18nMessage type = new I18nMessagePlEn("type") /* i18n:  */; // #187
    // not translated
    // no occurrences
    public static final I18nMessage typeArgOfDynamicClassCreatorForC = new I18nMessagePlEn("type arg of dynamic class creator for class") /* i18n:  */; // #188
    // not translated
    // no occurrences
    public static final I18nMessage typeArgsButShouldHaveJust = new I18nMessagePlEn("typeArgs, but should have just") /* i18n:  */; // #189
    // not translated
    // no occurrences
    public static final I18nMessage unableToFindClass = new I18nMessagePlEn("unable to find class") /* i18n:  */; // #190
    // not translated
    // no occurrences
    public static final I18nMessage unknown = new I18nMessagePlEn("unknown") /* i18n:  */; // #191
    // not translated
    // no occurrences
    public static final I18nMessage verdana = new I18nMessagePlEn("Verdana") /* i18n:  */; // #192
    // not translated
    // no occurrences
    public static final I18nMessage white = new I18nMessagePlEn("Biały", "White") /* i18n:  */; // #193
    // not translated
    // no occurrences
    public static final I18nMessage widget = new I18nMessagePlEn("widget") /* i18n:  */; // #194
    // not translated
    // no occurrences
    public static final I18nMessage willExtendWithDescendants_noMaybeIdent = new I18nMessagePlEn("will extendWithDescendants") /* i18n: no?maybe-ident */; // #195
    // not translated
    // no occurrences
    public static final I18nMessage withMsg_noMaybeIdent = new I18nMessagePlEn(""
            + "with msg") /* i18n: no?maybe-ident */; // #196
    // not translated
    // no occurrences
    public static final I18nMessage wrapper = new I18nMessagePlEn("Wrapper") /* i18n:  */; // #197
    // not translated
    // no occurrences
    public static final I18nMessage wrapperGetPropClassStaticPropNam_noMaybeIdent = new I18nMessagePlEn("Wrapper.getPropClassStatic(propName);\n"
            + "            }\n"
            + "\n"
            + "            @Override\n"
            + "            public Set<String> getPropNames() {\n"
            + "                return") /* i18n: no?maybe-ident */; // #198
    // not translated
    // no occurrences
    public static final I18nMessage wrapperGetPropValueStaticBeanPro_noMaybeIdent = new I18nMessagePlEn("Wrapper.getPropValueStatic(bean, propName);\n"
            + "            }\n"
            + "\n"
            + "            @Override\n"
            + "            public void setPropValue") /* i18n: no?maybe-ident */; // #199
    // not translated
    // no occurrences
    public static final I18nMessage wrapperSetPropValueStaticBeanPro_noMaybeIdent = new I18nMessagePlEn("Wrapper.setPropValueStatic(bean, propName, value);\n"
            + "            }\n"
            + "\n"
            + "            @Override\n"
            + "            public Class getPropClass(String propName) {\n"
            + "                return") /* i18n: no?maybe-ident */; // #200
    // not translated
    // no occurrences
    public static final I18nMessage wrapperSrc = new I18nMessagePlEn("Wrapper(src") /* i18n:  */; // #201
    // not translated
    // no occurrences
    public static final I18nMessage x_noSingleLetter = new I18nMessagePlEn("x") /* i18n: no?single-letter */; // #202
    // not translated
    // no occurrences
    public static final I18nMessage xxx = new I18nMessagePlEn("xxx") /* i18n:  */; // #203
    // not translated
    // no occurrences
    public static final I18nMessage yellow = new I18nMessagePlEn("Żółty", "Yellow") /* i18n:  */; // #204
//    public static final I18nMessage black = new I18nMessagePlEn("Czarny","Black");
//    public static final I18nMessage blue = new I18nMessagePlEn("Niebieski","Blue");
    public static final I18nMessage bold = new I18nMessagePlEn("Pogrubienie", "Toggle Bold");
    public static final I18nMessage color = new I18nMessagePlEn("Kolor", "Color");
    public static final I18nMessage createLink = new I18nMessagePlEn("Utwórz łącze", "Create Link");
//    public static final I18nMessage font = new I18nMessagePlEn("Czcionka","Font");
//    public static final I18nMessage green = new I18nMessagePlEn("Zielony","Green");
    public static final I18nMessage hr = new I18nMessagePlEn("Wstaw poziomą linię", "Insert Horizontal Rule");
    public static final I18nMessage indent = new I18nMessagePlEn("Zwiększ wcięcie", "Indent Right");
    public static final I18nMessage insertImage = new I18nMessagePlEn("Wstaw obrazek", "Insert Image");
    public static final I18nMessage italic = new I18nMessagePlEn("Kursywa", "Toggle Italic");
    public static final I18nMessage justifyCenter = new I18nMessagePlEn("Wyrównaj do środka", "Center");
    public static final I18nMessage justifyLeft = new I18nMessagePlEn("Wyrównaj do lewej", "Left Justify");
    public static final I18nMessage justifyRight = new I18nMessagePlEn("Wyrównaj do prawej", "Right Justify");
    public static final I18nMessage large = new I18nMessagePlEn("Duży", "Large");
    public static final I18nMessage medium = new I18nMessagePlEn("Średni", "Medium");
    public static final I18nMessage normal = new I18nMessagePlEn("Normalny", "Normal");
    public static final I18nMessage ol = new I18nMessagePlEn("Wstaw listę numerowaną", "Insert Ordered List");
    public static final I18nMessage outdent = new I18nMessagePlEn("Zmniejsz wcięcie", "Indent Left");
//    public static final I18nMessage red = new I18nMessagePlEn("Czerwony","Red");
    public static final I18nMessage removeFormat = new I18nMessagePlEn("Usuń formatowanie", "Remove Formatting");
    public static final I18nMessage removeLink = new I18nMessagePlEn("Usuń łącze", "Remove Link");
    public static final I18nMessage size = new I18nMessagePlEn("Rozmiar", "Size");
    public static final I18nMessage small = new I18nMessagePlEn("Mały", "Small");
    public static final I18nMessage strikeThrough = new I18nMessagePlEn("Przekreślenie", "Toggle Strikethrough");
    public static final I18nMessage subscript = new I18nMessagePlEn("Indeks dolny", "Toggle Subscript");
    public static final I18nMessage superscript = new I18nMessagePlEn("Indeks górny", "Toggle Superscript");
    public static final I18nMessage ul = new I18nMessagePlEn("Wstaw punktory", "Insert Unordered List");
    public static final I18nMessage underline = new I18nMessagePlEn("Podkreślenie", "Toggle Underline");
//    public static final I18nMessage white = new I18nMessagePlEn("Biały","White");
    public static final I18nMessage xlarge = new I18nMessagePlEn("Bardzo duży", "X-Large");
    public static final I18nMessage xsmall = new I18nMessagePlEn("Bardzo mały", "X-Small");
    public static final I18nMessage xxlarge = new I18nMessagePlEn("Największy", "XX-Large");
    public static final I18nMessage xxsmall = new I18nMessagePlEn("Najmniejszy", "XX-Small");
//    public static final I18nMessage yellow = new I18nMessagePlEn("Żółty","Yellow");
    public static final I18nMessage insertTable = new I18nMessagePlEn("Wstaw/Edytuj tabelę", "Insert table");
    public static final I18nMessage code = new I18nMessagePlEn("Wstaw kod", "Insert code");
    public static final I18nMessage caption = new I18nMessagePlEn("Wstaw nagłówek", "Insert heading");
    public static final I18nMessage tableCaption = new I18nMessagePlEn("Spis treści", "Table of contents");
    public static final I18nMessage createObjLink = new I18nMessagePlEn("Utwórz łącze do obiektu", "Create obj link");
    public static final I18nMessage insertMyImage = new I18nMessagePlEn("Wstaw obrazek z pliku", "Upload and insert image");
    public static final I18nMessage programista = new I18nMessagePlEn("Programista", "Programmer");
    public static final I18nMessage wrongFileExtension = new I18nMessagePlEn("Plik z takim rozszerzeniem nie może być załadowany", "File with this extension can not be loaded");

    /*
     i18n categories counts:
     null: 156
     <--proper-->: 204
     <--proper:no?-->: 46
     <--proper:regular-->: 158
     <-all->: 419
     [translated]: 58
     [unused]: 146
     no: 208
     no!: 1
     no!ident: 4
     no!not-msg: 1
     no:px-dims: 1
     no?maybe-ident: 41
     no?single-letter: 5
     set xxx of column: 1
     table: 1
     */
}
