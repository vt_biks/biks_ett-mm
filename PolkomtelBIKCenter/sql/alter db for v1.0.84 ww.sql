exec sp_check_version '1.0.84';
go

--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

IF OBJECT_ID (N'dbo.fn_split_string', N'TF') IS NOT NULL
    DROP FUNCTION dbo.fn_split_string;
GO

CREATE FUNCTION dbo.fn_split_string (@str varchar(max), @chunk_len int)
RETURNS @tab TABLE (chunk_str varchar(100))
WITH EXECUTE AS CALLER
AS
BEGIN
  declare @pos int = 1;
  declare @len int = len(@str);
  
  while @pos <= @len begin
    insert into @tab (chunk_str) values (substring(@str, @pos, @chunk_len));
    set @pos = @pos + 1;
  end

  RETURN;
END;
GO


--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

create table bik_node_name_chunk (node_id int not null, tree_id int not null, chunk_txt varchar(3) not null)
go

create unique index idx_bik_node_name_chunk_ctn on bik_node_name_chunk (chunk_txt, tree_id, node_id);
go

create unique index idx_bik_node_name_chunk_cn on bik_node_name_chunk (chunk_txt, node_id);
go

create index idx_bik_node_name_chunk_n on bik_node_name_chunk (node_id);
go

alter table bik_node add is_chunked int not null default 0;
go

/*
create index idx_bik_node_chunked on bik_node (is_chunked);
go

create index idx_bik_node_tree_chunked on bik_node (tree_id, is_chunked);
go
*/

create index idx_bik_node_branch_ids on bik_node (branch_ids)
go


IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[bik_node]') AND name = N'idx_bik_node_chunked')
DROP INDEX [idx_bik_node_chunked] ON [dbo].[bik_node]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[bik_node]') AND name = N'idx_bik_node_tree_chunked')
DROP INDEX idx_bik_node_tree_chunked ON [dbo].[bik_node]
GO


exec sp_rename 'bik_node.is_chunked', 'chunked_ver', 'COLUMN'
go


--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

IF OBJECT_ID (N'dbo.fn_bik_node_name_chunk_search_sql', N'FN') IS NOT NULL
    DROP FUNCTION dbo.fn_bik_node_name_chunk_search_sql;
GO

CREATE FUNCTION dbo.fn_bik_node_name_chunk_search_sql (@substr varchar(1000), @opt_tree_id int)
RETURNS varchar(8000)
WITH EXECUTE AS CALLER
AS
BEGIN
  declare @sql varchar(8000) = 'select node_id from bik_node_name_chunk
  where (';

  if len(@substr) > 3 begin

  declare @substr_chunks table (chunk_str varchar(3) not null);
  
  insert into @substr_chunks
  select chunk_str from (
  select distinct top 5 chunk_str, len(chunk_Str) as cln from dbo.fn_split_string(@substr, 3) 
  where len(chunk_str) = 3 order by len(chunk_str) desc
  ) x
  
  declare bnncs cursor for select chunk_str from @substr_chunks;
  
  open bnncs;
  
  declare @chunk_str varchar(3);
  
  fetch next from bnncs into @chunk_str
  
  declare @is_first int = 1;
  
  while @@fetch_status = 0 begin
    if @is_first = 0 begin
      set @sql = @sql + ' or ';
    end else begin
      set @is_first = 0;
    end

	set @chunk_str = replace(@chunk_str, '''', '''''');
  
    set @sql = @sql + 'chunk_txt = ''' + @chunk_str + ''''
  
    fetch next from bnncs into @chunk_str
  end;
  end else begin
	set @chunk_str = replace(@substr, '''', '''''');
  
    if len(@chunk_str) < 3 begin
      set @sql = @sql + 'chunk_txt like ''' + @chunk_str + '%'''
    end else begin
      set @sql = @sql + 'chunk_txt = ''' + @chunk_str + ''''
    end;
  end
  
  set @sql = @sql + ')'

  if @opt_tree_id is not null begin
    set @sql = @sql + ' and tree_id = ' + cast(@opt_tree_id as varchar(20));
  end;
  
  if len(@substr) > 3 begin
  set @sql = @sql + ' group by node_id having count(*) = ' + cast((select count(*) from @substr_chunks) as varchar(10))
  end
  
  RETURN @sql;
END;
GO

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------


IF EXISTS (SELECT name FROM sysobjects
      WHERE name = 'trg_bik_node_chunking' AND type = 'TR')
   DROP TRIGGER trg_bik_node_chunking
GO

CREATE TRIGGER trg_bik_node_chunking
ON bik_node
FOR insert, update, delete AS
begin
	-- diag: print cast(sysdatetime() as varchar(23)) + ': start!'

set nocount on

declare @diags_level int = 0;

declare @to_delete table (id int not null primary key);

declare @bik_node_name_chunk table (node_id int not null, tree_id int not null, chunk_txt varchar(3) not null)

	if @diags_level > 0 begin
	-- diag: 
	  print cast(sysdatetime() as varchar(23)) + ': before calc @to_delete'
	end
	
insert into @to_delete (id) 	
select d.id from deleted d left join inserted i on d.id = i.id
where i.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 0 and i.is_deleted = 1)
	
	if @diags_level > 0 begin
	-- diag: 
	  print cast(sysdatetime() as varchar(23)) + ': before delete by ids'
	end
	
delete from bik_node_name_chunk where node_id in (select id from @to_delete
--select d.id from deleted d left join inserted i on d.id = i.id
--where i.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 0 and i.is_deleted = 1)
);

declare @to_insert table (id int not null, name varchar(max) not null, tree_id int not null);

	if @diags_level > 0 begin
	-- diag: 
	  print cast(sysdatetime() as varchar(23)) + ': before calc @to_insert'
    end
    
insert into @to_insert (id, name, tree_id)
select i.id, i.name, i.tree_id from inserted i left join deleted d on d.id = i.id
where d.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 1 and i.is_deleted = 0);

	if @diags_level > 0 begin
	-- diag: 
	  print cast(sysdatetime() as varchar(23)) + ': before declare cursor'
	end
	
declare nodes cursor for select id, name, tree_id from @to_insert;
/*select id, name, tree_id from bik_node where id in (
select i.id from inserted i left join deleted d on d.id = i.id
where d.name is null or i.name <> d.name or (d.is_deleted = 1 and i.is_deleted = 0)
)*/;

	if @diags_level > 0 begin
	-- diag: 
	  print cast(sysdatetime() as varchar(23)) + ': before open cursor'
	end
	
open nodes;

declare @id int, @name varchar(8000), @tree_id int;

  if @diags_level > 0 begin
  -- diag: 
    print cast(sysdatetime() as varchar(23)) + ': before first fetch'
  end
  
fetch next from nodes into @id, @name, @tree_id;

	if @diags_level > 0 begin
	-- diag: 
	  print cast(sysdatetime() as varchar(23)) + ': before loop'
	end
	
	declare @cnt int = 0;
	
while @@fetch_status = 0
begin
  insert into @bik_node_name_chunk (node_id, chunk_txt, tree_id)
  select distinct @id, chunk_str, @tree_id 
  from dbo.fn_split_string(@name, 3);
  
	if @diags_level > 1 begin
	-- diag: 
	  print cast(sysdatetime() as varchar(23)) + ': in loop after insert'
	end
	
  set @cnt = @cnt + 1;
  
  if (@cnt % 1000 = 0) begin
  
    if @diags_level > 1 begin
    -- diag: 
      print cast(sysdatetime() as varchar(23)) + ': in loop, before insert into bik_node_name_chunk, cnt:' + cast(@cnt as varchar(10))
    end
	
	insert into bik_node_name_chunk (node_id, chunk_txt, tree_id) 
	select node_id, chunk_txt, tree_id
	from @bik_node_name_chunk
	
	delete from @bik_node_name_chunk
  end;    
  
  if @diags_level > 1 begin
     -- diag: 
     print cast(sysdatetime() as varchar(23)) + ': in loop before fetch next'
  end
  
  fetch next from nodes into @id, @name, @tree_id;
  if @diags_level > 1 begin
	-- diag: 
	print cast(sysdatetime() as varchar(23)) + ': in loop after fetch next'
  end
  
end;

	if @diags_level > 0 begin
	-- diag: 
	  print cast(sysdatetime() as varchar(23)) + ': after loop, before insert into bik_node_name_chunk, @cnt=' + cast(@cnt as varchar(10))
	end
	
	insert into bik_node_name_chunk (node_id, chunk_txt, tree_id) 
	select node_id, chunk_txt, tree_id
	from @bik_node_name_chunk
	  
	if @diags_level > 0 begin
	-- diag: 
	  print cast(sysdatetime() as varchar(23)) + ': closing, deallocating cursor'
	end
	
close nodes;

deallocate nodes;

	if @diags_level > 0 begin
	-- diag: 
	  print cast(sysdatetime() as varchar(23)) + ': done, stop!'
	end
end
go


--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_filter_bik_nodes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_filter_bik_nodes]
GO

create procedure [dbo].[sp_filter_bik_nodes] (@txt varchar(max), @optTreeId int, @optExtraFilterCondition varchar(max))
as
begin
  set nocount on
  
declare @sql varchar(8000);

set @sql = 'select id from bik_node where id in (' + dbo.fn_bik_node_name_chunk_search_sql(@txt, @optTreeId) 
  + ') and name like ''%' + replace(@txt, '''', '''''') + '%'' and is_deleted = 0' +
  case when @optExtraFilterCondition is null then '' else ' and (' + @optExtraFilterCondition + ')' end

-- ostatnio wrzucone
declare @t table (id int not null primary key, parent_node_id int null);
-- parent_node_ids ostatnio wrzuconych - te b�dziemy wrzuca� teraz
declare @p table (id int not null primary key);
-- wszystkie
declare @a table (id int not null primary key);

  insert into @p (id)
  exec(@sql)

  while exists (select 1 from @p) begin
    insert into @a select id from @p;

    delete from @t;

    insert into @t (id, parent_node_id) select id, parent_node_id from bik_node where id in (select id from @p);

    delete from @p;

    insert into @p 
    select distinct parent_node_id 
    from @t 
    where parent_node_id is not null and parent_node_id not in (select id from @a);
  end;

  --select * from @a

select bn.*,
    bnk.caption as node_kind_caption,
    bnk.code as node_kind_code,
    bnk.is_folder as is_folder,
    bt.name as tree_name, bt.tree_kind, bt.code as tree_code
    , case when exists 
        (select 1 from bik_node where is_deleted = 0 and parent_node_id = coalesce(bn.linked_node_id, bn.id)) then 0 else 1 end as has_no_children
    from bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    inner join bik_tree bt on bn.tree_id = bt.id
    where bn.id in (select id from @a)
    order by bn.id
end;
go


--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

truncate table bik_node_name_chunk
go

disable trigger trg_bik_node_chunking on bik_node;
go

update bik_node set chunked_ver = 0
go

enable trigger trg_bik_node_chunking on bik_node;
go



declare @node_id int, @node_id_max int, @done_cnt int = 0;

select @node_id = min(id) - 1 from bik_node where is_deleted = 0;

while 1 = 1 begin

--if @done_cnt >= 1500000 break;

select @node_id_max = max(id) 
from (select top 50000 id
from bik_node where id > @node_id and is_deleted = 0 order by id) xxx;

--select @node_id, @node_id_max

if @node_id_max is null break;

update bik_node set chunked_ver = chunked_ver + 1 where is_deleted = 0 and id > @node_id and id <= @node_id_max;

set @done_cnt = @done_cnt + (select count(*) from bik_node where is_deleted = 0 and id > @node_id and id <= @node_id_max);

set @node_id = @node_id_max;

end
go


--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

exec sp_update_version '1.0.84', '1.0.84.5';
go
