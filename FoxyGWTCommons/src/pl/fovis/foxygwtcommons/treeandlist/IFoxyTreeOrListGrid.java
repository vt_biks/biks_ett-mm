/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.treeandlist;

import com.google.gwt.user.client.ui.Widget;
import java.util.Map;
import simplelib.IParametrizedContinuation;
import simplelib.IParametrizedContinuationWithReturn;

/**
 *
 * @author mgraczkowski
 */
public interface IFoxyTreeOrListGrid<IDT> {

    // "siName=Nazwa", "siKind=Rodzaj", "link=Przejdź", "icon=Ikonka"
    public void setFields(String... fieldDef);

    public void defineButtonField(String fieldName, String upImage, String downImage,
            IParametrizedContinuation<Map<String, Object>> handler,
            IParametrizedContinuationWithReturn<Map<String, Object>, Boolean> stateHandler);

    public void defineIconField(String fieldName);

    public void defineFieldWidth(String fieldName, int width);

    public void refreshWidgetsState();

    public void addSelectionChangedHandler(IParametrizedContinuation<Map<String, Object>> handler);

    public void addRightClickHandler(IFoxyTreeOrListGridRightClickHandler<Map<String, Object>> handler);

    public void addDoubleClickHandler(IParametrizedContinuation<Map<String, Object>> handler);

    public void setEmptyMessage(String message);

    public void selectRecord(IDT id);

    // musi wywołać handler wybierania rekordu, niezależnie czy aktualnie
    // wybrany jest inny rekord czy już ten sam
    public void selectFirstRecord();

    public Widget buildWidget();

    // ww: nieużywane, usuwam z interfejsu
    // wydaje się, że nie będzie nam to nigdy potrzebne
    //public void defineFieldHidden(String fieldName, boolean hidden);

    public void setHeight(int value);

    public Map<String, Object> getSelectedRecord();

    //public Map<String, Object> getSelectedRowData();

    public String getIdFieldName();
}
