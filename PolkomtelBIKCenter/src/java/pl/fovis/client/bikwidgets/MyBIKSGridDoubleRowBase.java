/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.BaseUtils;

/**
 *
 * @author bfechner
 */
public abstract class MyBIKSGridDoubleRowBase<T> extends MyBIKSGridBase<T> {

    //ww->mybiks: czy chodziło o "Up"Style? a nie "at"Style?
    protected String dartAtStyle() {
        return "dartAt";
    }

    //ww->mybiks: dlaczego to jest w klasie bazowej? wydawało mi się, że
    // strzałki są potrzebne tylko czasem
    protected String dartDownStyle() {
        return "dartDown";
    }

    protected void initializeStartListsInner(List<T> result, int i) {
        buttons.put(i, new BikCustomButton("  ", dartAtStyle(), null));
        T r = result.get(i);
        addOrRefreshTitles(i, r);
        addClickHandlerDarts(i, r);
    }

    protected void initializeStartLists(List<T> result) {
        //ww->mybiks: to bardzo ciekawy kod, ciekawe co robi...
        // trudno zrozumieć, jakieś "listy startowe"?
        buttons.clear();
        titles.clear();
        for (int i = 0; i < result.size(); i++) {
            initializeStartListsInner(result, i);
        }
    }

    protected abstract ClickHandler createClickHandlerForRow(final int row, final T n);

    //ww->mybiks: wydaje mi się, że "darts" to takie "strzałki" typu "rzutki", "lotki"
    // jakimi rzuca się do tarczy, a strzałki ekranowe to raczej "arrows"?
    protected void addClickHandlerDarts(final int row, final T n) {
        //ww->mybiks: createClickHandlerForRow w dwóch kolejnych liniach
        // powinno być na zmiennej - a tak, tworzą się dwa identyczne
        // handlery kliknięcia, PO CO?
        ClickHandler ch = createClickHandlerForRow(row, n);
        buttons.get(row).addClickHandler(ch);
        titles.get(row).addClickHandler(ch);
    }

    protected abstract void createRowFromBean(int row, T n); //wiersz głowny

    public Widget addItemsToGrid(List<T> itemsToDisplay) {
        activeTab.clear();
        gp.clear();
        activeTab.setWidth("100%");
        int newsCnt = BaseUtils.collectionSizeFix(itemsToDisplay);
        if (newsCnt > 0) {
            gp.setWidth("100%");
            gp.setStyleName("gridJoinedObj");
            initializeStartLists(itemsToDisplay);
            int row = 0;
            for (T r : itemsToDisplay) {
                createRowFromBean(row, r);
                addRowExpandedToItem(row, r);

                gp.getRowFormatter().setVisible(row + 1, false);
                row = row + 2;
            }
            setUpGridColumnsWidths(gp);
            activeTab.add(gp);
        }
        return activeTab;
    }

    protected void addRowExpandedToItem(int row, T n) {
        gp.setWidget(row + 1, 0, getWidgetToRowExpanded(n));
        gp.getFlexCellFormatter().setColSpan(row + 1, 0, columnsCnt);
        //ww->mybiks: o konstrukcji typu "row == 0 || row % 4 == 0" wypowiedziałem
        // się już w innym miejscu, przykro, że jest ona kopi-pejstowana w kilka miejsc
        // (ta konstrukcja, bo mojej opinii oszczędzę hańby kopi-pejstu).
        gp.getRowFormatter().setStyleName(row + 1, getBackgroundStyleForRowInGrid(row, 4));
    }

    //ww->mybiks: addWidgetToRow czyli "dodaj widget do wiersza"? co to znaczy, co to robi?
    // chyba dodaje widgety (nie jeden) dla wiersza
    protected void addWidgetsToRow(int row, T n) {

        addOrRefreshTitles(row / 2, n);
        gp.setWidget(row, columnsCnt++, isDartDownVisible(n) ? buttons.get(row / 2) : new Label());
        gp.setWidget(row, columnsCnt++, new Image(getIconUrl(n)));
        gp.setWidget(row, columnsCnt++, titles.get(row / 2));
        if (isIgnoreImageVisible()) {
            gp.setWidget(row, columnsCnt++, isDartDownVisible(n) ? addIgnoreImage(n) : new Label());
        }

    }

    protected abstract Widget getWidgetToRowExpanded(T n); //jakas inna nazwe! -> to jest ten co sie pokazuje po kliknieciu dartdown

    protected boolean isDartDownVisible(T n) {
        return true;
    }
}
