/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.dialogs.AddNoteDialog;
import pl.fovis.common.NoteBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.dqm.DQMEvaluationBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class AddOrEditDQMEvaluationDialog extends BaseActionOrCancelDialog {

    protected boolean isNewEvaluation;
    protected DQMEvaluationBean bean;
    protected List<DQMEvaluationBean> periods;
    protected IParametrizedContinuation<DQMEvaluationBean> saveCont;
    protected Label lblDataGroup;
    protected Label lblLeader;
    protected TextArea taeActionPlan;
    protected ListBox lbxPrevQualityLevel;
    protected ListBox lbxActualQualityLevel;
    protected Label lblSignificanceLevel;
    protected Integer leaderNodeId;
    protected DateBox dpCreateDate;
    protected ListBox lbxReportPeriod;
//    protected DateBox dpFromDate;
//    protected DateBox dpToDate;
    protected PushButton selectLeaderBtn;
    protected boolean isViewMode;
    protected NoteBean optNote;
    protected PushButton rejectBtn;
    protected Map<String, DQMEvaluationBean> periodsMap;

    @Override
    protected void addAdditionalButtons(HorizontalPanel hp) {
        rejectBtn = NewLookUtils.newCustomPushButton(I18n.odrzuc.get() /* I18N:  */, new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        NoteBean bean = new NoteBean(-1);
                        bean.title = SimpleDateUtils.dateToString(new Date()) + " - " + I18n.odrzuconoOceneJakosciDanych.get();
                        bean.body = I18n.proszePoprawicOcene.get();
                        new AddNoteDialog().buildAndShowDialog(bean, new IParametrizedContinuation<NoteBean>() {
                            @Override
                            public void doIt(NoteBean param) {
                                optNote = param;
                                actionClicked();
                            }
                        });
                    }
                });
        hp.add(rejectBtn);
        rejectBtn.setVisible(isViewMode);
    }

    @Override
    protected String getDialogCaption() {
        if (isViewMode) {
            return I18n.kontrolaOkresowejOcenyJakosci.get();
        }
        return isNewEvaluation ? I18n.dodajOcene.get() : I18n.edytujOcene.get();
    }

    @Override
    protected String getActionButtonCaption() {
        if (isViewMode) {
            return I18n.akceptuj.get();
        }
        return isNewEvaluation ? I18n.dodaj.get() : I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {
        VerticalPanel mainPanel = new VerticalPanel();
        periodsMap = new HashMap<String, DQMEvaluationBean>();
//        HorizontalPanel hp = new HorizontalPanel();
        DateTimeFormat dateFormat = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_SHORT);
//        hp.add(new HTML("&nbsp;" + I18n.okresOd.get() + "&nbsp;&nbsp;"));
//        hp.add(dpFromDate = new DateBox());
//        dpFromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
//        dpFromDate.setWidth("100px");
//        hp.add(new HTML("&nbsp;&nbsp;" + I18n.okresDo.get() + "&nbsp;&nbsp;"));
//        hp.add(dpToDate = new DateBox());
//        dpToDate.setFormat(new DateBox.DefaultFormat(dateFormat));
//        dpToDate.setWidth("100px");
//        mainPanel.add(hp);
//        mainPanel.add(new HTML("<hr/>"));
        FlexTable mainFT = new FlexTable();
        mainFT.setText(0, 0, I18n.grupaDanych.get());
        mainFT.setText(1, 0, I18n.istotnoscGrupyDanych.get());
        mainFT.setText(2, 0, I18n.opiekunGrupyDanych.get());
        mainFT.setText(3, 0, I18n.okresSprawozdawczy.get());
        mainFT.setText(4, 0, I18n.dataSporzadzenia.get());
        mainFT.setText(5, 0, I18n.poprzedniPoziomJakosci.get());
        mainFT.setText(6, 0, I18n.aktualnyPoziomJakosci.get());
        mainFT.setText(7, 0, I18n.planDzialania.get());
        mainFT.setWidget(0, 1, lblDataGroup = new Label());
        mainFT.setWidget(1, 1, lblSignificanceLevel = new Label());
        lblDataGroup.setWidth("200px");
        lblDataGroup.setWordWrap(true);
        lblSignificanceLevel.setWidth("200px");
        lblSignificanceLevel.setWordWrap(true);
        HorizontalPanel hpLeader = new HorizontalPanel();
        hpLeader.add(lblLeader = new Label());

        selectLeaderBtn = new PushButton(new Image("images/edit_yellow.png" /* I18N: no */), new Image("images/edit_yellow.png" /* I18N: no */));
        selectLeaderBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new TreeSelectorForDQMLeader().buildAndShowDialog(null, new IParametrizedContinuation<TreeNodeBean>() {

                    @Override
                    public void doIt(TreeNodeBean param) {
                        lblLeader.setText(param.name);
                        leaderNodeId = param.linkedNodeId != null ? param.linkedNodeId : param.id;
                    }
                });
            }
        });
        selectLeaderBtn.setStyleName("admin-btn");
        selectLeaderBtn.setWidth("16px");
        selectLeaderBtn.setTitle(I18n.wybierzUzytkownika.get() /* I18N:  */);
        hpLeader.add(selectLeaderBtn);
        mainFT.setWidget(2, 1, hpLeader);
        lbxReportPeriod = new ListBox();
        lbxReportPeriod.setWidth("204px");
        mainFT.setWidget(3, 1, lbxReportPeriod);
        HorizontalPanel hpCreateDate = new HorizontalPanel();
        hpCreateDate.add(dpCreateDate = new DateBox());
        dpCreateDate.setWidth("200px");
        dpCreateDate.setFormat(new DateBox.DefaultFormat(dateFormat));
        dpCreateDate.setValue(new Date());
        mainFT.setWidget(4, 1, hpCreateDate);
        lbxPrevQualityLevel = new ListBox();
        lbxActualQualityLevel = new ListBox();
        lbxPrevQualityLevel.setWidth("204px");
        lbxActualQualityLevel.setWidth("204px");
        lbxPrevQualityLevel.addItem("");
        if (bean != null && BaseUtils.isStrEmptyOrWhiteSpace(bean.prevQualityLevel)) {
            lbxActualQualityLevel.addItem("");
        }

        for (QualityLevel value : QualityLevel.values()) {
            lbxPrevQualityLevel.addItem(value.name);
            lbxActualQualityLevel.addItem(value.name);
        }
//        for (SignificanceLevel value : SignificanceLevel.values()) {
//            lbxSignificanceLevel.addItem(value.name);
//        }
        mainFT.setWidget(5, 1, lbxPrevQualityLevel);
        mainFT.setWidget(6, 1, lbxActualQualityLevel);
        mainFT.setWidget(7, 1, taeActionPlan = new TextArea());
        taeActionPlan.setHeight("80px");
        taeActionPlan.setWidth("200px");
        mainPanel.add(mainFT);
        fillWidgets();
        setOptWidgetVisible();
        if (isViewMode) {
            setWidgetsDisabled();
        }
        main.add(mainPanel);
    }

    @Override
    protected void doAction() {
        bean.actionPlan = taeActionPlan.getValue();
        bean.actualQualityLevel = lbxActualQualityLevel.getValue(lbxActualQualityLevel.getSelectedIndex());
        bean.prevQualityLevel = lbxPrevQualityLevel.getValue(lbxPrevQualityLevel.getSelectedIndex());
//        bean.significance = bean.significance;
        bean.createdDate = dpCreateDate.getValue();
        bean.leaderNodeId = leaderNodeId;
        bean.name = SimpleDateUtils.dateToString(dpCreateDate.getValue()) + " " + I18n.ocenaJakosciDanych.get() + " - " + bean.groupName;
        bean.note = optNote;
        if (periods != null) {
            DQMEvaluationBean selectedPeriod = periodsMap.get(lbxReportPeriod.getValue(lbxReportPeriod.getSelectedIndex()));
            bean.dateFrom = selectedPeriod.dateFrom;
            bean.dateTo = selectedPeriod.dateTo;
        }
        saveCont.doIt(bean);
    }

    @Override
    protected boolean doActionBeforeClose() {
//        Date periodFrom = dpFromDate.getValue();
//        Date periodTo = dpToDate.getValue();
//        if (periodFrom == null) {
//            new SimpleInfoDialog().buildAndShowDialog(I18n.okresOdNieMozeBycPusty.get(), I18n.blad.get(), null);
//            return false;
//        }
//        if (periodTo == null) {
//            new SimpleInfoDialog().buildAndShowDialog(I18n.okresDoNieMozeBycPusty.get(), I18n.blad.get(), null);
//            return false;
//        }
//        if (periodFrom.after(periodTo)) {
//            new SimpleInfoDialog().buildAndShowDialog(I18n.bladWOkresachObowiazywania.get(), I18n.blad.get(), null);
//            return false;
//        }
        if (leaderNodeId == null || leaderNodeId == 0) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.wybierzOpiekunaGrupDanych.get(), I18n.blad.get(), null);
            return false;
        }
        return true;
    }

    public void buildAndShowDialog(DQMEvaluationBean bean, List<DQMEvaluationBean> periods, IParametrizedContinuation<DQMEvaluationBean> saveCont, boolean isViewMode) {
        this.bean = bean;
        this.periods = periods;
        this.isViewMode = isViewMode;
        this.isNewEvaluation = bean == null || bean.id == 0;
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }

    protected int getIndexByText(ListBox lbx, String text) {
        int itemCount = lbx.getItemCount();
        for (int i = 0; i < itemCount; i++) {
            if (lbx.getItemText(i).equals(text)) {
                return i;
            }
        }
        return 0;
    }

    protected void setOptWidgetVisible() {
        selectLeaderBtn.setVisible(leaderNodeId == null || leaderNodeId == 0);
        lbxReportPeriod.setEnabled(periods != null);
    }

    protected void fillWidgets() {
        if (periods == null) {
            lbxReportPeriod.addItem(SimpleDateUtils.dateToString(bean.dateFrom) + " - " + SimpleDateUtils.dateToString(bean.dateTo));
        } else {
            int index = 0;
            periodsMap.clear();
            for (DQMEvaluationBean period : periods) {
                lbxReportPeriod.addItem(SimpleDateUtils.dateToString(period.dateFrom) + " - " + SimpleDateUtils.dateToString(period.dateTo), String.valueOf(index));
                periodsMap.put(String.valueOf(index), period);
                index++;
            }
        }
        if (bean != null && bean.id != 0) {
            taeActionPlan.setValue(bean.actionPlan);
            lbxActualQualityLevel.setSelectedIndex(getIndexByText(lbxActualQualityLevel, bean.actualQualityLevel));
            dpCreateDate.setValue(bean.createdDate);
//            dpFromDate.setValue(bean.dateFrom);
//            dpToDate.setValue(bean.dateTo);
        }
        lbxPrevQualityLevel.setSelectedIndex(getIndexByText(lbxPrevQualityLevel, bean.prevQualityLevel));
        lblDataGroup.setText(bean.groupName);
        lblSignificanceLevel.setText(bean.significance);
        lblLeader.setText(bean.leaderName != null ? bean.leaderName : "");
        leaderNodeId = bean.leaderNodeId;
    }

    @Override
    protected void onEnterPressed() {
        // NO ACTION
    }

    protected void setWidgetsDisabled() {
//        dpFromDate.setEnabled(false);
//        dpToDate.setEnabled(false);
        dpCreateDate.setEnabled(false);
        taeActionPlan.setEnabled(false);
        lbxActualQualityLevel.setEnabled(false);
        lbxPrevQualityLevel.setEnabled(false);
//        lbxSignificanceLevel.setEnabled(false);
        selectLeaderBtn.setVisible(false);
    }

    enum QualityLevel {

        NISKI("Niski"), SREDNI("Średni"), WYSOKI("Wysoki");

        protected String name;

        private QualityLevel(String name) {
            this.name = name;
        }
    }
//    enum SignificanceLevel {
//
//        SREDNIA("Średnia"), DUZA("Duża");
//
//        protected String name;
//
//        private SignificanceLevel(String name) {
//            this.name = name;
//        }
//    }
}
