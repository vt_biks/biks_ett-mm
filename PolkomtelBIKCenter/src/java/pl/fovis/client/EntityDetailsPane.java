/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.bikpages.IBikTreePage;
import pl.fovis.client.bikpages.TreeBikPage;
import pl.fovis.client.entitydetailswidgets.AttachmentsRolesWidgetFlat;
import pl.fovis.client.entitydetailswidgets.AttachmentsWidgetFlat;
import pl.fovis.client.entitydetailswidgets.BlogsWidget;
import pl.fovis.client.entitydetailswidgets.ConfluenceRoleWidgetFlat;
import pl.fovis.client.entitydetailswidgets.ConfluenceWidgetFlat;
import pl.fovis.client.entitydetailswidgets.DescendantNodesSubTreeWidgetFlat;
import pl.fovis.client.entitydetailswidgets.DictionaryRolesWidgetFlat;
import pl.fovis.client.entitydetailswidgets.DictionaryWidgetFlat;
import pl.fovis.client.entitydetailswidgets.GlossaryRolesWidgetFlat;
import pl.fovis.client.entitydetailswidgets.GlossaryWidgetFlat;
import pl.fovis.client.entitydetailswidgets.IEntityDetailsWidget;
import pl.fovis.client.entitydetailswidgets.IEntityDetailsWidgetTabReady;
import pl.fovis.client.entitydetailswidgets.IndexWidgetFlat;
import pl.fovis.client.entitydetailswidgets.MainHeaderWidget;
import pl.fovis.client.entitydetailswidgets.MetadataRolesWidgetFlat;
import pl.fovis.client.entitydetailswidgets.MetadataWidgetFlat;
import pl.fovis.client.entitydetailswidgets.NodeHistoryWidgetFlat;
import pl.fovis.client.entitydetailswidgets.NoteWidgetFlat;
import pl.fovis.client.entitydetailswidgets.SpecialistWidget;
import pl.fovis.client.entitydetailswidgets.TaxonomyRoleWidgetFlat;
import pl.fovis.client.entitydetailswidgets.TaxonomyWidgetFlat;
import pl.fovis.client.entitydetailswidgets.UserBlogsWidget;
import pl.fovis.client.entitydetailswidgets.UserNoteWidgetFlat;
import pl.fovis.client.entitydetailswidgets.dqm.DQCRequestChartWidgetFlat;
import pl.fovis.client.entitydetailswidgets.dqm.DQCRequestForTestWidgetFlat;
import pl.fovis.client.entitydetailswidgets.dqm.DataQualityRoleWidgetFlat;
import pl.fovis.client.entitydetailswidgets.dqm.DataQualityWidgetFlat;
import pl.fovis.client.nodeactions.INodeActionContext;
import pl.fovis.client.nodeactions.NodeActionContext;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.JoinedObjBean;
import pl.fovis.common.JoinedObjsTabKind;
import pl.fovis.common.TreeBean;
import pl.fovis.common.UserRoleInNodeBean;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.PushButtonWithHideText;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import static pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog.isShowDiagDialogSizingEnabled;
import static pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog.showDiagDialogSizingMsgS;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author wezyr
 */
public class EntityDetailsPane implements IEntityDetailsWidgetsContainer {

    protected IBikTreePage bikPage;
    protected ScrollPanel ultimateContainer = new ScrollPanel();
    protected FlowPanel mainFp = new FlowPanel();
//    protected TabPanel dsp;//= new NewLookTabLayoutPanel(46, Unit.PX, "innerTab");;
    protected Integer currentNodeId = null;
    public EntityDetailsDataBean data;
    private List<IEntityDetailsWidget> subWidgets = new ArrayList<IEntityDetailsWidget>();
    private List<IEntityDetailsWidget> staticSubWidgets = new ArrayList<IEntityDetailsWidget>();
    protected boolean forceDataRefresh;
    private int selectedTab = -1;
    protected boolean widgetsActive;
    protected IContinuation dataFetchedCallback;
    protected Integer currentLinkingParentId;
    protected FlowPanel arrowFp;
    protected BikCustomButton arrowUpOrDownBtn;
    protected PushButtonWithHideText arrowLeftBtn;
    protected PushButtonWithHideText arrowRightBtn;
    protected boolean isFooterExpanded = false;
    protected MainHeaderWidget mhw;
    protected HorizontalPanel captionsHp;
    protected FlowPanel captionsPositionFp;
    protected FlowPanel joinedFp;
    protected boolean autoSize = true;
    protected int widgetHeight = BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT;
    protected String optWidgetHeightStr;
    protected ComputedDetailsDataBean cddb;
    protected INodeActionContext ctx;
    protected List<IEntityDetailsWidgetTabReady> displayedTabs = new ArrayList<IEntityDetailsWidgetTabReady>();
    protected Map<Class, Widget> joinedWidgetByClass = new HashMap<Class, Widget>();
    protected List<Widget> joinedWidget = new ArrayList< Widget>();
    protected Map<IEntityDetailsWidgetTabReady, HTML> captionWidgets = new HashMap<IEntityDetailsWidgetTabReady, HTML>();
    protected Set<IEntityDetailsWidgetTabReady> candidateTabsToOptRender = new HashSet<IEntityDetailsWidgetTabReady>();
    //    protected IEntityDetailsWidget selectedWidget = null;

    public EntityDetailsPane(TreeBikPage bikPage) {
        this(true);
        this.bikPage = bikPage;
    }

    public EntityDetailsPane(boolean widgetsActive) {
        this.widgetsActive = widgetsActive;

        Window.addResizeHandler(new ResizeHandler() {
            @Override
            public void onResize(ResizeEvent event) {
                setVisibleArrowBtn();
            }
        });
    }

    public Widget getWidget() {
        return mainFp;
    }

    //nieużywane?
    public void refreshWidgetsState() {
        for (IEntityDetailsWidget edw : subWidgets) {
            edw.refreshWidgetsState();
        }
    }

    public void refreshAndRedisplayData() {
        refreshData();
    }

    public void setNodeIdAndFetchData(Integer nodeId, Integer linkingParentId) {
        setNodeId(nodeId, linkingParentId);

        refreshAndRedisplayData();
    }

    protected void setupClearForNewNodeId() {
        joinedWidget.clear();
        joinedFp.clear();
        subWidgets.clear();
        joinedWidgetByClass.clear();
        displayedTabs.clear();
        captionsHp.clear();
        captionWidgets.clear();
        candidateTabsToOptRender.clear();
        setupCtx();
    }

    protected void setupCtx() {
        ctx = bikPage == null || data == null ? null : new NodeActionContext(data.node, bikPage, data);
    }

    protected String prevNodeKindCode = data == null || data.node == null ? null : data.node.nodeKindCode;

    protected void dataFetched() {
//        setupCtx();
        setupClearForNewNodeId();
        mhw.dataFetched(ctx, data, cddb);
        mhw.displayData();
        mhw.refreshWidgetsState();

        BIKClientSingletons.getService().getJoinedObjs(currentNodeId, currentLinkingParentId, new StandardAsyncCallback<List<JoinedObjBean>>("Error in getJoinedObjs") {

            @Override
            public void onSuccess(List<JoinedObjBean> result) {
                IEntityDetailsWidget selectedWidget = null;

                String currentNodeKindCode = data == null || data.node == null ? null : data.node.nodeKindCode;

                if (BaseUtils.safeEquals(currentNodeKindCode, prevNodeKindCode) || !BaseUtils.safeEqualsAny(currentNodeKindCode,
                        BIKGWTConstants.NODE_KIND_DQM_TEST_FAILED,
                        BIKGWTConstants.NODE_KIND_DQM_TEST_INACTIVE,
                        BIKGWTConstants.NODE_KIND_DQM_TEST_NOT_STARTED,
                        BIKGWTConstants.NODE_KIND_DQM_TEST_SUCCESS
                )) {
                    //poprzednia zaznaczona
                    if (selectedTab > -1 && !displayedTabs.isEmpty()) {
                        selectedWidget = displayedTabs.get(selectedTab);
                    }
                }

                prevNodeKindCode = currentNodeKindCode;

                setupClearForNewNodeId();
                subWidgets.addAll(staticSubWidgets);

                calcComputedDetailsDataBean(result);

                addTabWidget(new TaxonomyWidgetFlat());
                addTabWidget(new TaxonomyRoleWidgetFlat());

                addTabWidget(new GlossaryWidgetFlat());
                addTabWidget(new GlossaryRolesWidgetFlat());

                // Dodać ukrywanie zakładek dla nieaktywnych systemów BI
                addTabWidget(new DQCRequestForTestWidgetFlat());
                addTabWidget(new DQCRequestChartWidgetFlat());

                addTabWidget(new ConfluenceWidgetFlat());
                addTabWidget(new ConfluenceRoleWidgetFlat());

                addTabWidget(new DictionaryWidgetFlat());
                addTabWidget(new DictionaryRolesWidgetFlat());

                addTabWidget(new AttachmentsWidgetFlat());
                addTabWidget(new AttachmentsRolesWidgetFlat());

                addTabWidget(new BlogsWidget());
                addTabWidget(new UserBlogsWidget());

                addTabWidget(new SpecialistWidget());

                addTabWidget(new NoteWidgetFlat());
                addTabWidget(new UserNoteWidgetFlat());

                addTabWidget(new IndexWidgetFlat());

                addTabWidget(new MetadataWidgetFlat());
                addTabWidget(new MetadataRolesWidgetFlat());

                addTabWidget(new DataQualityWidgetFlat());
                addTabWidget(new DataQualityRoleWidgetFlat());
                if (BIKClientSingletons.isEnableNodeHistory()) {
                    addTabWidget(new NodeHistoryWidgetFlat());
                }

//        addTabWidget(new ColumnWidgetFlat()//, BIKGWTConstants.MENU_NAME_COLUMNS, (data.columns == null) ? 0 : data.columns.size()
                if (!widgetsActive && data.hasChildren) {
                    addTabWidget(new DescendantNodesSubTreeWidgetFlat()//, BIKGWTConstants.MENU_NAME_DESCENDANT_NODES, null
                    );
                }
                getDataForCandidateTabs();

                arrowFp.clear();
                arrowFp.add(createArrowLeftButton());
                arrowFp.add(createArrowButton());
                arrowFp.add(createArrowRightButton());

                if (displayedTabs.isEmpty() /*
                         * && !(BIKClientSingletons.isUserLoggedIn())
                         */) {
                    arrowFp.setVisible(false);
                } else {
                    arrowFp.setVisible(true);
                    joinedFp.setVisible(isFooterExpanded);

                    if (selectedWidget != null && joinedWidgetByClass.containsKey(selectedWidget.getClass())) {
                        selectedTab = joinedWidget.indexOf(joinedWidgetByClass.get(selectedWidget.getClass()));

                    } else {
                        selectedTab = 0;
                    }

                    setStyleCaption(selectedTab, captionsHp.getWidget(selectedTab));
                    displayDataOfActiveTab(displayedTabs.get(selectedTab));
                }
                setHeightBodyEdp();

                arrowUpOrDownBtn.setStyleName(isFooterExpanded ? "arrowDown" : "arrowUp");
            }
        });
    }

    protected TreeBean getOptTreeBean() {
        return bikPage == null ? null : bikPage.getTreeBean();
    }

    protected int getSelectId() {
        return selectedTab < displayedTabs.size() ? selectedTab : 0;
    }

    protected void calcComputedDetailsDataBean(List<JoinedObjBean> result) {

        cddb = new ComputedDetailsDataBean();
//        cddb.metadataJOBs = new ArrayList<JoinedObjBean>();
//        cddb.confluenceJOBs = new ArrayList<JoinedObjBean>();
//        cddb.dqcJOBs = new ArrayList<JoinedObjBean>();
//        cddb.taxonomyJOBs = new ArrayList<JoinedObjBean>();

        cddb.jobs = new EnumMap<JoinedObjsTabKind, List<JoinedObjBean>>(JoinedObjsTabKind.class);
        for (JoinedObjsTabKind jotk : JoinedObjsTabKind.values()) {
            cddb.jobs.put(jotk, new ArrayList<JoinedObjBean>());
        }

        cddb.jobsByDstNodeId = new LinkedHashMap<Integer, JoinedObjBean>();
        cddb.nodeNamesById = new LinkedHashMap<Integer, String>();

        cddb.userRoleInNodeByDstNodeId = new HashMap<Integer, UserRoleInNodeBean>();
        cddb.userRoleInNodeByTreeKind = new HashMap<String, List<UserRoleInNodeBean>>();
        for (JoinedObjBean joinedObjBean : result) {
            cddb.jobsByDstNodeId.put(joinedObjBean.dstId, joinedObjBean);
            cddb.nodeNamesById.put(joinedObjBean.dstId, joinedObjBean.dstName);
            JoinedObjsTabKind jotk = null;

            if (joinedObjBean.treeKind.equals(BIKGWTConstants.TREE_KIND_METADATA)) {
                jotk = JoinedObjsTabKind.Metadata;
            } else if (joinedObjBean.treeKind.equals(BIKGWTConstants.TREE_KIND_CONFLUENCE)) {
                jotk = JoinedObjsTabKind.Confluence;
            } else if (BIKClientSingletons.isTreeKindDynamic(joinedObjBean.treeKind)) {
                jotk = JoinedObjsTabKind.Taxonomy;
            } else if (joinedObjBean.treeKind.equals(BIKGWTConstants.TREE_KIND_QUALITY_METADATA)) {
                jotk = JoinedObjsTabKind.Dqc;
            } else if (joinedObjBean.treeKind.equals(BIKGWTConstants.TREE_KIND_GLOSSARY)) {
                jotk = JoinedObjsTabKind.Glossary;
            } else if (BaseUtils.safeEqualsAny(joinedObjBean.treeKind, BIKGWTConstants.TREE_KIND_DICTIONARY, BIKGWTConstants.TREE_KIND_DICTIONARY_DWH)) {
                jotk = JoinedObjsTabKind.Dictionary;
            }

            if (jotk != null) {
                cddb.jobs.get(jotk).add(joinedObjBean);
            }
        }
        if (data.userRolesInNodes != null) {
            for (UserRoleInNodeBean us : data.userRolesInNodes.v1) {
                cddb.userRoleInNodeByDstNodeId.put(us.nodeId, us);
                String treeKindAfterFix = BIKClientSingletons.getDynamicTreeKindForTreeKind(us.treeKind);
                if (!cddb.userRoleInNodeByTreeKind.containsKey(treeKindAfterFix)) {
                    cddb.userRoleInNodeByTreeKind.put(treeKindAfterFix, new ArrayList<UserRoleInNodeBean>());
                }
                cddb.userRoleInNodeByTreeKind.get(treeKindAfterFix).add(us);
            }
        }
    }

    public void setHeightBodyEdp() {

        if (isShowDiagDialogSizingEnabled()) {
            showDiagDialogSizingMsgS("setHeightBodyEdp: start, mhw=" + (mhw != null ? "not " : "") + "null");
        }

        if (mhw == null) {
            return;
        }

        int heightBodyEdp = BIKClientSingletons.ORIGINAL_EDP_BODY + 36;
        final int height = isFooterExpanded && !displayedTabs.isEmpty()
                ? (/*widgetsActive ? heightBodyEdp + 28 :*/heightBodyEdp + 28)
                : 36;
        mhw.setHeightScPanel(height, mainFp);
    }

    protected void refreshData() {

        mainFp.setVisible(currentNodeId != null);

        if (currentNodeId == null) {
            setupClearForNewNodeId();
            return;
        }

        final AsyncCallback<EntityDetailsDataBean> callback2 = new StandardAsyncCallback<EntityDetailsDataBean>("Refresh data failed" /* i18n: no */) {
            @Override
            public void onSuccess(EntityDetailsDataBean result) {

                if (result == null) {
                    currentNodeId = null;
                    mainFp.setVisible(false);

                    setupClearForNewNodeId();
                    return;
                }

                if (!BaseUtils.safeEquals(result.node.id, currentNodeId)) {
                    return;
                }
                EntityDetailsPane.this.data = result;

                if (dataFetchedCallback != null) {
                    dataFetchedCallback.doIt();
                }
                dataFetched();
            }
        };

        BIKClientSingletons.getService().getBikEntityDetailsData(currentNodeId, currentLinkingParentId, widgetsActive, callback2);
    }

    protected Widget addAndBuildSubWidget(IEntityDetailsWidget subWidget) {
        subWidgets.add(subWidget);
        Widget res = subWidget.buildWidgets();
        joinedWidgetByClass.put(subWidget.getClass(), res);
        joinedWidget.add(res);
        return res;
    }

    protected void subdetailsTabSelected() {
        //no-op
    }

    protected void displayDataOfActiveTab(IEntityDetailsWidgetTabReady act) {
        joinedFp.clear();
        joinedFp.add(joinedWidgetByClass.get(act.getClass()));
        act.displayData();
        act.refreshWidgetsState();
        subdetailsTabSelected();
    }

    protected boolean isWidgetBuilt = false;

    protected void buildWidgets() {

        isWidgetBuilt = true;

        arrowFp = new FlowPanel();
        captionsHp = new HorizontalPanel();
        captionsPositionFp = new FlowPanel();
        joinedFp = new FlowPanel();

        GWTUtils.setStyleAttribute(mainFp, "padding" /* I18N: no */, "0px 0 0px 0");
        mainFp.setStyleName("EntityDetailsPane");
        setMainFpHeight();
        mhw = new MainHeaderWidget();
        mhw.setup(widgetsActive, getOptTreeBean());
        mhw.setBikPage(bikPage);
        mhw.setRefreshContinuation(getRefreshContinuation());

        captionsPositionFp.add(captionsHp);
        captionsPositionFp.setStyleName("secedpHP");
        mainFp.add(addAndBuildSubWidget(mhw));

        mainFp.add(arrowFp);
        mainFp.add(captionsPositionFp);
        mainFp.add(joinedFp);
        joinedFp.setWidth("100%");
        captionsHp.setStyleName("innerTab");

        staticSubWidgets.addAll(subWidgets);
    }

    protected void setMainFpHeight() {
        if (autoSize) {
            BIKClientSingletons.registerResizeSensitiveWidgetByHeight(mainFp, widgetHeight);
        } else {
            mainFp.setHeight(optWidgetHeightStr == null ? widgetHeight + "px" : optWidgetHeightStr);
        }

        if (BaseActionOrCancelDialog.isShowDiagDialogSizingEnabled()) {
            BaseActionOrCancelDialog.showDiagDialogSizingMsgS("setMainFpHeight: done, autoSize=" + autoSize
                    + ", widgetHeight=" + widgetHeight
                    + ", optWidgetHeightStr=" + optWidgetHeightStr);
        }
    }

    public void setForceDataRefresh(boolean val) {
        this.forceDataRefresh = val;
    }

    public void setWidgetHeight(String sizeStr) {
        optWidgetHeightStr = sizeStr;
        autoSize = false;
    }

    public void setWidgetHeight(int size) {
        widgetHeight = size;
        autoSize = false;
    }

//    protected ITabSelector<Integer> getTabSelector() {
//        return BIKClientSingletons.getTabSelector();
//    }
    public void setDataFetchedCallback(IContinuation dataFetchedCallback) {
        this.dataFetchedCallback = dataFetchedCallback;
    }

    protected String getSubWidgetCaption(IEntityDetailsWidgetTabReady act) {
        String caption = act.getCaption();
        int cnt = BaseUtils.nullToDef(act.getItemCount(), -1);
        return caption + (cnt <= 0 ? "" : (caption.contains("<br>") ? "" : "<br>") + " <b>(" + cnt + ")</b>");
    }

    protected void addTabWidget(final IEntityDetailsWidgetTabReady act) {
        if (hideTabsForMM(act.getCaption())) {
            return;
        }
        act.setup(widgetsActive, getOptTreeBean());
        act.dataFetched(ctx, data, cddb);
        act.optSetCurrentLinkingParentId(currentLinkingParentId);

        String treeKindForSelected = act.getTreeKindForSelected();
        if (treeKindForSelected/*act.getTreeKindForSelected()*/ != null && BIKClientSingletons.isTreeHidden(treeKindForSelected)) {//bf:-->[zakladki] ukrywanie zakładek menu dolne
            return;
        }

        if (!act.hasContent() && act.isEnabled()) {
//            Window.alert("Nie ma kontentu!!!!!: " + act.getCaption());
            candidateTabsToOptRender.add(act);
        }
//        boolean canEdit = widgetsActive && act.canCurrentUserEdit();
        boolean canEdit = widgetsActive && act.isEditonEnabled();
        if (!act.isEnabled() || (!act.hasContent() && (!canEdit || !act.hasEditableControls()))) {
            return;
        }

//        Window.alert("Dodajemy!!!!!: " + act.getCaption());
        displayedTabs.add(act);
        addAndBuildSubWidget(act);
        captionsHp.add(createCaption(act));
        act.setEntityDetailsWidgetsContainer(this);
    }

    protected boolean hideTabsForMM(String actCapt) {
        boolean hideDefinedTabs = actCapt.equals("Notes") || actCapt.equals("Notatki")
                || actCapt.equals("Metadane") || actCapt.equals("Metadata")
                || actCapt.equals("Users") || actCapt.equals("Użytkownicy")
                || actCapt.equals("Comments") || actCapt.equals("Komentarze");
        return BIKClientSingletons.isHideElementsForMM() && hideDefinedTabs;
    }

    protected HTML createCaption(final IEntityDetailsWidgetTabReady act) {
        final HTML caption = new HTML(getSubWidgetCaption(act));
        caption.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                displayDataOfActiveTab(act);
                setStyleCaption(getSelectId(), caption);
                selectedTab = displayedTabs.indexOf(act);
            }
        });
        captionWidgets.put(act, caption);
        return caption;
    }

    protected void setStyleCaption(int selectedId, Widget caption) {
        captionsHp.getWidget(selectedId).removeStyleName("active" /* I18N: no */);
        DOM.getParent(captionsHp.getWidget(selectedId).getElement()).removeClassName("active" /* I18N: no */);
        caption.addStyleName("active" /* I18N: no */);
        DOM.getParent(caption.getElement()).setClassName("active" /* I18N: no */);
    }

    @Override
    public void refreshTabCaption(IEntityDetailsWidgetTabReady act) {
        HTML widgetCaption = captionWidgets.get(act);
        if (widgetCaption != null) {
            widgetCaption.setHTML(getSubWidgetCaption(act));
        }
//        dsp.getTabBar().setTabHTML(displayedTabs.indexOf(act), getSubWidgetCaption(act));
    }

    protected void setVisibleArrowBtn() {
        if (captionsHp != null) {
            int leftPosition = parsePosition(captionsHp.getElement().getStyle().getLeft());
            if (arrowLeftBtn != null) {
                arrowLeftBtn.setVisible(leftPosition < 0);
            }
            if (arrowRightBtn != null) {
                arrowRightBtn.setVisible(captionsPositionFp.getOffsetWidth() - leftPosition < captionsHp.getOffsetWidth());
            }
            setHeightBodyEdp();
        }
    }

    protected BikCustomButton createArrowButton() {
        arrowUpOrDownBtn = new BikCustomButton("   ", "arrowDown", new IContinuation() {
            @Override
            public void doIt() {
                isFooterExpanded = !isFooterExpanded;
                arrowUpOrDownBtn.setStyleName(isFooterExpanded ? "arrowDown" : "arrowUp");
                joinedFp.setVisible(isFooterExpanded);
                setHeightBodyEdp();
            }
        });
        return arrowUpOrDownBtn;
    }

    protected PushButtonWithHideText createArrowRightButton() {
        arrowRightBtn = new PushButtonWithHideText(new Image("images/arrowr.gif" /* I18N: no */));
        arrowRightBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                int leftPosition = parsePosition(captionsHp.getElement().getStyle().getLeft());
                if (captionsPositionFp.getOffsetWidth() - leftPosition < captionsHp.getOffsetWidth()) {
                    setPositionCapionsHp(-50);
                }
            }
        });
        arrowRightBtn.setStyleName("arrowRight");
        setVisibleArrowBtn();
        return arrowRightBtn;
    }

    protected PushButtonWithHideText createArrowLeftButton() {
        arrowLeftBtn = new PushButtonWithHideText(new Image("images/arrowl.gif" /* I18N: no */));
        arrowLeftBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                int leftPosition = parsePosition(captionsHp.getElement().getStyle().getLeft());
                if (leftPosition < 0) {
                    setPositionCapionsHp(50);
                }
            }
        });
        arrowLeftBtn.setStyleName("arrowLeft");
        setVisibleArrowBtn();
        return arrowLeftBtn;
    }

    protected void setPositionCapionsHp(final int d) {

        int leftPosition = parsePosition(captionsHp.getElement().getStyle().getLeft());
        captionsHp.getElement().getStyle().setLeft(leftPosition + d, Unit.PX);
        setVisibleArrowBtn();
    }

    private static int parsePosition(String positionString) {
        int i = 0;
        int len = positionString.length();

        //szukamy pierwszego znaku nie cyfry i nie minusa
        // może takie nie być wcale
        while (i < len) {
            char c = positionString.charAt(i);
            if (c != '-' && (c < '0' || c > '9')) {
                break;
            }
            i++;
        }
        // obcinamy do samych poprawnych znaków cyfrowych
        // lub weźmie całość jak wszystkie poprawne
        String s = positionString.substring(0, i);
        Integer p = null;
        if (!BaseUtils.isStrEmptyOrWhiteSpace(s)) {
            p = BaseUtils.tryParseInteger(s);
        }
        return p == null ? 0 : p;
    }

    protected IContinuation getRefreshContinuation() {
        return null;
    }

    protected void getDataForCandidateTabs() {
        for (final IEntityDetailsWidgetTabReady tab : candidateTabsToOptRender) {
            tab.optCallServiceToGetData(new IContinuation() {

                @Override
                public void doIt() {
                    if (!displayedTabs.contains(tab) && candidateTabsToOptRender.contains(tab)) {
                        candidateTabsToOptRender.remove(tab);
//                        Window.alert("Dodaje: " + tab.getCaption());
                        addTabWidget(tab);
                    } else {
                        refreshTabCaption(tab);
                    }
                    tab.displayData();
                    setVisibleArrowBtn();
                }
            });
        }
    }

    public void setNodeId(Integer nodeId, Integer linkingParentId) {
        forceDataRefresh = false;
//        if (currentNodeId == null) {
//            buildWidgets();
//        }
        if (!isWidgetBuilt) {
            buildWidgets();
        }
        this.currentNodeId = nodeId;
        this.currentLinkingParentId = linkingParentId;
    }
}
