/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.teradata;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
    public class TeradataBean implements Serializable {

    public int dataLoadLogId;
    public String type;
    public String name;
    public String extraInfo;
    public String branchNames;
    public String parentBranchNames;
    public String requestText;

    public TeradataBean(int dataLoadLogId, String type, String name, String extraInfo, String branchNames, String parentBranchNames, String requestText) {
        this.dataLoadLogId = dataLoadLogId;
        this.type = type;
        this.name = name;
        this.extraInfo = extraInfo;
        this.branchNames = branchNames;
        this.parentBranchNames = parentBranchNames;
        this.requestText = requestText;
    }

    @Override
    public String toString() {
        return "TeradataBean{ dataLoadLogId=" + dataLoadLogId + ", type=" + type + ", name=" + name + ", extraInfo=" + extraInfo + ", branchNames=" + branchNames + ", parentBranchNames=" + parentBranchNames /*+ ", requestText=" + requestText*/ + '}';
    }
}
