exec sp_check_version '1.1.9.19';
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

--ww: usuwamy z menu (modrzew) drzewa, kt�re wylecia�y z bik_tree

declare @treeOfTreesId int = (select id from bik_tree where code = 'TreeOfTrees')
delete from bik_node  
from bik_node n left join bik_tree t on substring(n.obj_id, 2, len(n.obj_id)) = t.code
where n.tree_id = @treeOfTreesId and n.obj_id like '$%'
and t.id is null
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_update_version '1.1.9.19', '1.1.9.20';
go
