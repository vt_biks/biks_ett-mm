/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.util.List;
import simplelib.BeanWithIntIdBase;

/**
 *
 * @author mgraczkowski
 */
public class TreeBean extends BeanWithIntIdBase {

    public String code;
    public String name;
    public String treeKind;
    public Integer nodeKindId;
    public Integer isBuiltIn;
    public String nameInLang;
    public String lang;
    public String treePath;
    //---
    public String nodeKindCode;
    //---
    public boolean isAccessibleByGrants;

    public int importStatus;
    public int isAutoObjId;
    public List<Integer> nodeKindIds;
    public boolean showAddMainBranchBtn;
    public List<TreeBean> nodeKindsToAddOnTreesAsMainBranch;
    public String nodeKindCaption;
    public List<AttributeBean> requiredAttrs;
}
