/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.IJoinedObjSelectionManager;
import pl.fovis.client.dialogs.RolesDialog;
import pl.fovis.client.dialogs.TreeSelectorForJoinedObjectsDialogNew;
import pl.fovis.client.dialogs.TreeSelectorForUsers;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.JoinTypeRoles;
import pl.fovis.common.JoinedObjBasicInfoBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.UseInNodeSelectionBean;
import pl.fovis.common.UserRoleInNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pgajda
 */
public class NodeActionLinkUserToRole extends NodeActionUpdatingBase {

    protected static enum NodeType {

        NODE, USER, ALL
    };
    protected NodeType nodeType;

    public String getCaption() {

        switch (nodeType) {
            case NODE:
                return I18n.przypiszDoRoli.get() /* I18N:  */;
            case USER:
                return I18n.dodajPowiazanie.get() /* I18N:  */;
            default:
                //tu nigdy nie wejdzie, ale musi coś zwrócić
                return "";
        }
    }

    protected IJoinedObjSelectionManager<Integer> getJoinedObjSelectionManager(final int nodeId, final int roleForNodeId,
            final JoinTypeRoles joinTypeRole, final Map<Integer, UserRoleInNodeBean> userRoleIiNodeByDstNodeId) {

        return new IJoinedObjSelectionManager<Integer>() {
            public JoinedObjBasicInfoBean getInfoForJoinedNodeById(Integer dstNodeId) {

                UserRoleInNodeBean u = userRoleIiNodeByDstNodeId.get(dstNodeId);
                JoinedObjBasicInfoBean jb = new JoinedObjBasicInfoBean();
                jb.srcId = nodeId;
                jb.type = false;
                jb.dstId = u.nodeId;
                jb.branchIds = u.branchIds;
                jb.inheritToDescendants = u.inheritToDescendants;

                return jb;
            }

            public Collection<Integer> getJoinedObjIds() {
                List<Integer> joinedObjIds = new ArrayList<Integer>();
                for (UserRoleInNodeBean uinb : userRoleIiNodeByDstNodeId.values()) {
                    if (uinb.roleForNodeId == roleForNodeId) {
                        joinedObjIds.add(uinb.nodeId);
                    }
                }
                return joinedObjIds;
            }

            public void refreshJoinedNodes(Integer forNodeId) {
                if (nodeId == forNodeId) {
                    int pos = BIKClientSingletons.getTabSelector().getCurrentTabIdAndSelectedVal().v2;
                    refreshRedisplayDataAndSelectRecord(pos);
                }
            }
        };

    }

    protected void buildAndShowTreeSelectorForObjects(final int nodeId, final String treeKind, final int treeId,
            final String userName, final UseInNodeSelectionBean param, final Map<Integer, UserRoleInNodeBean> userRoleIiNodeByDstNodeId) {

        TreeSelectorForJoinedObjectsDialogNew treeSelectorForJoinedObjectsDialogNew = new TreeSelectorForJoinedObjectsDialogNew() {

            @Override
            protected Collection<Integer> getTreeIdsForFiltering() {
                return BIKClientSingletons.getProperTreeIdsWithoutOneTree(treeId);
            }

            @Override
            protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
                if (useLazyLoading) {
                    getService().getBikAllRootTreeNodesExcludingOneTree(treeId,null,null,null,null/*,BIKClientSingletons.isHideDictionaryRootTab()-tymczasowe do ukrycia słowników,*/, callback);
                } else {
                    getService().getBikAllTreeNodesExcludingOneTree(treeId, callback);
                }
            }

            @Override
            protected Widget setOptionalWigetAfterHeadLabel() {
                PushButton changeRoleBtn = new PushButton(I18n.zmienRole.get() /* I18N:  */);
                NewLookUtils.makeCustomPushButton(changeRoleBtn);

                changeRoleBtn.addClickHandler(new ClickHandler() {
                    public void onClick(ClickEvent event) {

                        new RolesDialog().buildAndShowDialog(nodeId, /*treeKind,*/ new IParametrizedContinuation<UseInNodeSelectionBean>() {
                                    public void doIt(final UseInNodeSelectionBean param) {
                                        //zapisuje zwrocone parametry
                                        roleId = param.roleForNodeId;
                                        roleName = param.roleForNodeName;
                                        setHeadLabel();
                                        alreadyJoinedObjIds.clear();
                                        ancestorIdsOfJoinedObjs.clear();
                                        alreadyJoinedManager = getJoinedObjSelectionManager(nodeId, roleId, param.joinTypeRole, userRoleIiNodeByDstNodeId);

                                        buildAlreadyJoinedObjIds();
                                        fetchData();
                                    }
                                });

                    }
                });

                return changeRoleBtn;
            }
        };

        treeSelectorForJoinedObjectsDialogNew.buildAndShowDialog(nodeId, userName,
                BaseUtils.paramsAsSet(treeKind), null, null,
                //                                Collections.<String>singleton(treeKind), null, null,
                new IParametrizedContinuation<TreeNodeBean>() {
                    public void doIt(TreeNodeBean param) {
                    }
                },
                getJoinedObjSelectionManager(nodeId, param.roleForNodeId, param.joinTypeRole, userRoleIiNodeByDstNodeId), param);
    }

    protected void prepareBuildAndShowTreeSelectorForObjects(final int nodeId,
            final String treeKind,
            final int treeId,
            final String userName,
            final UseInNodeSelectionBean unb) {

        AsyncCallback<EntityDetailsDataBean> callback = new StandardAsyncCallback<EntityDetailsDataBean>("Refresh data failed" /* i18n: no */) {
            public void onSuccess(EntityDetailsDataBean result) {

                Map<Integer, UserRoleInNodeBean> userRoleIiNodeByDstNodeId = new HashMap<Integer, UserRoleInNodeBean>();

                for (UserRoleInNodeBean uinb : result.userRolesInNodes.v1) {
                    userRoleIiNodeByDstNodeId.put(uinb.nodeId, uinb);
                }

                buildAndShowTreeSelectorForObjects(nodeId, treeKind,
                        treeId, userName, unb, userRoleIiNodeByDstNodeId);
            }
        };

        BIKClientSingletons.getService().getBikEntityDetailsData(nodeId, null, false, callback);
    }

    public void execute() {

        if (nodeType == NodeType.NODE) {
            TreeSelectorForUsers tsfjod = new TreeSelectorForUsers();

            tsfjod.buildAndShowDialog(0, //"Users" /* I18N: no */,
                    new IParametrizedContinuation<TreeNodeBean>() {
                        public void doIt(final TreeNodeBean param) {
                            final UseInNodeSelectionBean unb = new UseInNodeSelectionBean();
                            unb.roleForNodeId = BaseUtils.tryParseInt(node.objId);
                            unb.roleForNodeName = node.name;

                            prepareBuildAndShowTreeSelectorForObjects(param.linkedNodeId != null ? param.linkedNodeId : param.id, param.treeKind, param.treeId, param.name, unb);
                        }
                    }, null, false, 0);
        } else {

            if (node.linkedNodeId == null) {
                //trzeba wybrać rolę
                new RolesDialog().buildAndShowDialog(node.id,/* node.treeKind,*/ new IParametrizedContinuation<UseInNodeSelectionBean>() {
                            public void doIt(final UseInNodeSelectionBean param) {
                                //buildAndShowTreeSelectorForObjects(treeKind, userName, param);
                                prepareBuildAndShowTreeSelectorForObjects(node.linkedNodeId != null ? node.linkedNodeId : node.id,
                                        node.treeKind,
                                        node.treeId,
                                        node.name,
                                        param);
                            }
                        });

            } else {
                //rolę szczytuję z drzewka
                if (node.parentNodeId == null) {
                    return;
                }

                TreeNodeBean roleNode = ctx.getTreeStorage().getBeanById(node.parentNodeId);
                UseInNodeSelectionBean unb = new UseInNodeSelectionBean();
                //unb.roleForNodeId = roleNode.id;
                unb.roleForNodeId = BaseUtils.tryParseInt(roleNode.objId);
                unb.roleForNodeName = roleNode.name;

                prepareBuildAndShowTreeSelectorForObjects(node.linkedNodeId != null ? node.linkedNodeId : node.id,
                        node.treeKind,
                        node.treeId,
                        node.name,
                        unb);
            }
        }
    }

    @Override
    public boolean isEnabledByDesign() {
        Map<Integer, String> roleNodeAndCode = data.rolesNodeIdAndCode;
        String roleCode = roleNodeAndCode != null ? roleNodeAndCode.get(node.id) : null;
        if (isNodeOneOfTheKind(true, BIKGWTConstants.NODE_KIND_USERS_ROLE) && isAdminOrExpertOrAuthorOrEditor()
                && (!isNodeBuiltIn()
                || (roleCode != null && roleCode.equals(BIKGWTConstants.ROLE_CODE_EDITOR)))) {
            nodeType = NodeType.NODE;
            return true;
        } else if (isAdminOrExpertOrAuthorOrEditor() && isInUserRolesTree() && !isNodeFolder()) {
            roleCode = roleNodeAndCode.get(node.parentNodeId);
            if (roleCode != null && roleCode.equals(BIKGWTConstants.USER_KIND_CODE_AUTHOR)) {
                return false;
            }
            nodeType = NodeType.USER;
            return true;
        } else {
            nodeType = NodeType.ALL;
        }

        return false;

    }
}
