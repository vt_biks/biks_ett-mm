﻿exec sp_check_version '1.0.89.7';
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

alter table aaa_query
add report_si_id int not null default 0
go

alter table aaa_report_object
add report_si_id int not null default 0
go


update aaa_query
set report_si_id=iw.si_id
from aaa_query aq join bik_node bn on aq.report_node_id=bn.id
join INFO_WEBI iw on bn.obj_id=convert(varchar(30), iw.si_id)


update aaa_report_object
set report_si_id=iw.si_id
from aaa_report_object ro join aaa_query aq on aq.id=ro.query_id
join bik_node bn on aq.report_node_id=bn.id
join INFO_WEBI iw on bn.obj_id=convert(varchar(30), iw.si_id)

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

exec sp_update_version '1.0.89.7', '1.0.90';
go