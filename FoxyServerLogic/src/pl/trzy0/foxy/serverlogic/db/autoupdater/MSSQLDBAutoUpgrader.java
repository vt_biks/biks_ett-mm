/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db.autoupdater;

import pl.trzy0.foxy.commons.IBeanConnectionEx;
import simplelib.stringeraser.StringContentEraser;
import simplelib.stringeraser.StringContentEraser.IScriptBatchCollector;

/**
 *
 * @author pmielanczuk
 */
public abstract class MSSQLDBAutoUpgrader extends GenericDBAutoUpgrader {

    public MSSQLDBAutoUpgrader(String appVer, String alterSqlsDir, IBeanConnectionEx<Object> conn) {
        super(appVer, alterSqlsDir, conn);
    }

//    @Override
//    protected void executeAlterScriptBatch(String batchText) {
//        //super.executeAlterScriptBatch(batchText); //To change body of generated methods, choose Tools | Templates.
//        System.out.println("no op - now...");
//    }
    @Override
    protected void executeAlterScript(String alterScript) {
//        System.out.println("first char as int=" + Integer.toHexString(alterScript.charAt(0)) + "\n");
//        System.out.println("alter script:\n" + alterScript);
        StringContentEraser.splitMSSQLScript(alterScript, new IScriptBatchCollector() {
            protected int batchNum = 1;
            protected int batchCount;
            protected int lineCount;

            public void initialInfo(int batchCount, int lineCount) {
                this.batchCount = batchCount;
                this.lineCount = lineCount;
                logExecution("file has " + batchCount + " parts, total lines: " + lineCount);
            }

            public void collect(String batch, int startLine, int endLine) {
                logExecution("executing batch #" + batchNum + "/" + batchCount + " part, lines: " + (startLine + 1) + " to " + endLine + " (of total: " + lineCount + ")");
                executeAlterScriptBatch(batch);
                batchNum++;
            }
        });
    }
//    protected String getCurrentDBVer() {
//        return (String) conn.execOneRowQry("select val from bik_app_prop where name = 'bik_ver'", false,
//                IRawConnection.FieldNameConversion.ToLowerCase).get("val");
//    }
//    private static void testEncodingSingle(String fileName, String encoding) {
//        String dir = "";
////        String dir = "C:\\temp\\BIKS-alter-db-sql-scripts\\";
//        System.out.println("\nfile=\"" + fileName + "\" as " + encoding + ":");
//        System.out.println(LameUtils.loadAsString(dir + fileName, encoding).substring(0, 2000));
//    }
//
//    private static void testEncoding(String fileName) {
//        testEncodingSingle(fileName, "utf8");
//        testEncodingSingle(fileName, "cp1250");
//        testEncodingSingle(fileName, "utf16");
//        testEncodingSingle(fileName, "*cp1250");
//    }
//
//    public static void main(String[] args) {
//
//        LameLoggerFactory.setOneConfigEntry("LameUtils", LameLogLevel.Debug);
//        System.out.println("system encoding=" + LameUtils.getDefaultSystemEncoding());
////        testEncoding("zgj-ansi.txt");
////        testEncoding("zgj-utf8.txt");
////        testEncoding("zgj-utf8 no bom.txt");
////        testEncoding("zgj-utf.txt");
////        testEncoding("zgj-utf-be.txt");
//        testEncoding("C:\\Users\\pmielanczuk\\Downloads\\ps_product.csv");
//        testEncoding("C:\\Users\\pmielanczuk\\Downloads\\ps_product_nowy.csv");
////        testEncoding("alter db for v1.4.y1.6 ac.sql");
////        testEncoding("alter db for v1.1.8.2 bf.sql");
//    }
//    public static void main(String[] args) {
//        MssqlConnectionConfig cfg = new MssqlConnectionConfig("localhost", "SQLEXPRESS", "BIKS_OLD", "sa", "MSsql123");
//        MssqlConnection conn = new MssqlConnection(cfg);
//
//        MSSQLDBAutoUpgrader updtr = new MSSQLDBAutoUpgrader("1.5.z", "C:/temp/BIKS-alter-db-sql-scripts", conn);
//        updtr.execute();
//    }
}
