/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import pl.trzy0.foxy.commons.templates.IParamSignature;
import simplelib.FieldNameConversion;
import simplelib.IContinuation;
import simplelib.ILameCollector;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author wezyr
 */
public interface INamedSqlsDAO<BeanBase> extends IFoxyTransactionalPerformer {

    public <T extends BeanBase> List<T> createBeansFromNamedQry(String name, Class<T> beanClass,
            Object... args);

    public <T extends BeanBase> T createBeanFromNamedQry(String name, Class<T> beanClass,
            boolean allowNoResult, Object... args);

    @Deprecated
    public Map<String, Object> execNamedQuerySingleRow(String name,
            boolean allowNoResult, Object... args);

    public Map<String, Object> execNamedQuerySingleRowCFN(String name,
            boolean allowNoResult, FieldNameConversion optConversion, Object... args);

    public int execNamedCommand(String name, Object... args);

    @Deprecated
    public List<Map<String, Object>> execNamedQuery(String name, Object... args);

    public List<Map<String, Object>> execNamedQueryCFN(String name,
            FieldNameConversion optConversion, Object... args);

//    public <C extends ILameCollector<Map<String, Object>>>  C execQry(String name,
//            boolean maxOneRow, boolean minOneRow,
//            C rowCollector);
    @Deprecated
    public <C extends ILameCollector<Map<String, Object>>> C execNamedQueryEx(String name,
            boolean maxOneRow, boolean minOneRow, C rowCollector,
            Object... args);

    public <C extends ILameCollector<Map<String, Object>>> C execNamedQueryExCFN(String name,
            boolean maxOneRow, boolean minOneRow, C rowCollector,
            FieldNameConversion optConversion, Object... args);

    // null if no such named query
    public IParamSignature getNamedQuerySignature(String name);

    public Integer sqlNumberToInt(Object numVal);

    //ww: do debugowania, sprawdzania itp.
    public String provideNamedSqlText(String name, Object... args);

    public void execInAutoCommitMode(IContinuation whatToExec);

    public IBeanConnectionThreadHandle getCurrentThreadHandle();

    public void setCurrentDatabase(String dbName);

    public String getCurrentDatabase();

    //------------------------------------------------------
    public String provideNamedSqlText(NamedSqlCallParams nscp);

    public <T extends BeanBase> List<T> createBeansFromNamedQry(NamedSqlCallParams nscp, Class<T> beanClass);

    public <T extends BeanBase> T createBeanFromNamedQry(NamedSqlCallParams nscp, Class<T> beanClass);

    public Map<String, Object> execNamedQuerySingleRowCFN(NamedSqlCallParams nscp,
            FieldNameConversion optConversion);

    public int execNamedCommand(NamedSqlCallParams nscp);

    public List<Map<String, Object>> execNamedQueryCFN(NamedSqlCallParams nscp,
            FieldNameConversion optConversion);

    public <C extends ILameCollector<Map<String, Object>>> C execNamedQueryExCFN(NamedSqlCallParams nscp,
            C rowCollector, FieldNameConversion optConversion);

    public void execNamedQueryExCFN(String name,
            IParametrizedContinuation<Iterator<Map<String, Object>>> processCont,
            FieldNameConversion optConversion, Object... args);

    public IParametrizedNamedTextProvider getParametrizedNamedTextProvider();
}
