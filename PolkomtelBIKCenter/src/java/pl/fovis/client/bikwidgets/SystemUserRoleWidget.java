/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.SystemUserDialog;
import pl.fovis.common.CustomRightRoleBean;
import pl.fovis.common.TreeNodeBranchBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author Eugeniusz
 */
public class SystemUserRoleWidget extends SystemGroupWidget {

    protected Map<CustomRightRoleBean, CheckBox> checkBxRoleList = new HashMap();
    protected HTML descr;
    protected HTMLPanel htmlPanel;

    protected void buildMainWidgets() {
        main = new VerticalPanel();
        main.setSpacing(10);
        main.setWidth("100%");
        main.add(new HTML("<h1>" + userGroup.name + "</h1></br>"));

        descr = new HTML(userGroup.description);
        htmlPanel = new HTMLPanel("");
        htmlPanel.add(descr);

        main.add(createLabelWithStyle(I18n.profilUprawnien.get()));
        grid = new FlexTable();
        grid.addStyleName("sysUserDialog");
        grid.setWidth("300px");
        addRightRolesToGrid(0);
        main.add(htmlPanel);
        htmlPanel.setStyleName("BizDef-descr");
        main.add(grid);
        saveBtn = new PushButton(I18n.zapisz.get());
        NewLookUtils.makeCustomPushButton(saveBtn);
        saveBtn.setEnabled(false);
        saveBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                Map<Integer, Set<Pair<Integer, Integer>>> flatCrrEntries = makeFlatCrrEntries();
                // exec sp_recalculate_Custom_Right_Role_Tree_Selectors_Ex;

                BIKClientSingletons.getService().updateProfileInRole(userGroup.id, flatCrrEntries, new StandardAsyncCallback<Void>() {

                    @Override
                    public void onSuccess(Void result) {
                        BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get());
                    }
                });
            }
        });
        main.add(saveBtn);
    }

    protected int addRightRolesToGrid(int row) {
        List<CustomRightRoleBean> crrbs = BIKClientSingletons.getCustomRightRoles();
        List<CustomRightRoleBean> crrbs2 = new ArrayList();
        crrbs2.addAll(crrbs);
        for (CustomRightRoleBean crr : crrbs) {
            if (crr.code.equals("Administrator") || crr.code.equals("RegularUser") || crr.code.equals("AppAdmin") || crr.code.equals("Expert") || crr.code.equals("NonPublicUser")) {
                crrbs2.remove(crr);
            }
        }
        boolean disabled = false;
        for (CustomRightRoleBean crr : crrbs2) {

            final CustomRightRoleBean crrF = crr;
            final CheckBox cb = createCheckBoxWithStyle();
            if (!BaseUtils.isCollectionEmpty(customRightRoleUserEntries.get(crr.id))) {
                cb.setValue(true);
                cb.setEnabled(true);
                for (CheckBox cbIt : checkBxRoleList.values()) {
                    cbIt.setEnabled(false);
                }
                disabled = true;
            } else if (disabled) {
                cb.setEnabled(false);
            }
            FlowPanel flowPanel = new FlowPanel();
            flowPanel.add(cb);

            checkBxRoleList.put(crr, cb);
            cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

                @Override
                public void onValueChange(ValueChangeEvent<Boolean> event) {
                    if (event.getValue()) {
                        saveBtn.setEnabled(true);
                        customRightRoleUserEntries.put(crrF.id, SystemUserDialog.MARKER_FOR_CUSTOM_ROLES_WITHOUT_SELECTOR);
                        for (Map.Entry<CustomRightRoleBean, CheckBox> mapPair : checkBxRoleList.entrySet()) {
                            if (mapPair.getKey().id != crrF.id) {
                                mapPair.getValue().setEnabled(false);
                            }
                        }
                    } else {
                        saveBtn.setEnabled(false);
                        customRightRoleUserEntries.remove(crrF.id);
                        for (Map.Entry<CustomRightRoleBean, CheckBox> mapPair : checkBxRoleList.entrySet()) {
                            mapPair.getValue().setEnabled(true);
                        }
                    }

                }
            });
//            }
            flowPanel.setWidth("60px");
            boolean isRegularUserCrr = BaseUtils.safeEquals(crr.code, "RegularUser");
            cb.setVisible(!isRegularUserCrr);
            addRowToGrid(crr.caption, flowPanel, row++, isRegularUserCrr ? null : "customRightRole");
        }
        return row;
    }

    protected Map<Integer, Set<Pair<Integer, Integer>>> makeFlatCrrEntries() {
        Map<Integer, Set<Pair<Integer, Integer>>> res = new HashMap<Integer, Set<Pair<Integer, Integer>>>();
        for (Map.Entry<Integer, Set<TreeNodeBranchBean>> e : customRightRoleUserEntries.entrySet()) {
            Set<Pair<Integer, Integer>> ents = new HashSet<Pair<Integer, Integer>>();
            if (e.getKey() != 0) {
                for (TreeNodeBranchBean tnbb : e.getValue()) {
                    ents.add(new Pair<Integer, Integer>(tnbb.treeId, tnbb.nodeId));
                }
                res.put(e.getKey(), ents);
            }
        }
        return res;
    }
}
