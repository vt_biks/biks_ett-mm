/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.IPageDataFetchBroker;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeGrid;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;

/**
 *
 * @author tflorczak
 */
public class ConfluenceLazyRowLoader extends AbstractLazyRowLoader<ConfluenceObjectBean, String> {

    public ConfluenceLazyRowLoader(IFoxyTreeGrid<String> treeGrid,
            ITreeRowAndBeanStorage<String, Map<String, Object>, ConfluenceObjectBean> storage,
            BikNodeTreeOptMode optExtraMode,
            IPageDataFetchBroker dataFetchBroker, boolean calcHasNoChildren) {
        super(treeGrid, storage, optExtraMode, null, dataFetchBroker, calcHasNoChildren);
    }

    @Override
    public Request getOneSubtreeLevelOfTree(ConfluenceObjectBean bean, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, boolean calcHasNoChildren, AsyncCallback<List<ConfluenceObjectBean>> asyncCallback) {
        return BIKClientSingletons.getService().getConfluenceChildrenObjects(bean.id, bean.kind, asyncCallback);
    }

    @Override
    public Request getBikNodeAncestorIds(String nodeId, AsyncCallback<List<String>> asyncCallback) {
        // niewykorzystane
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Request getSubtreeLevelsByParentIds(Collection<String> parentNodeIds, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, AsyncCallback<List<ConfluenceObjectBean>> asyncCallback) {
        // niewykorzystane
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public IBean4TreeExtractor<ConfluenceObjectBean, String> createBeanExtractor() {
        return new ConfluenceBean4TreeExtractor();
    }
}
