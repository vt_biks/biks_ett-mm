/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.sbavc;

/**
 *
 * @author tflorczak
 */
public abstract class SearchByAttrValueCriteriaSingleValue extends SearchByAttrValueCriteriaBase {

    public String value;

    public SearchByAttrValueCriteriaSingleValue() {
    }

    public SearchByAttrValueCriteriaSingleValue(int attrId, String attrName, String value, boolean isNegation) {
        super(attrId, attrName, isNegation);
        this.value = value;
    }
}
