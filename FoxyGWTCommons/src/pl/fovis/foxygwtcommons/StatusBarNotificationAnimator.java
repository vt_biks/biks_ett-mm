/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.core.client.Duration;
import com.google.gwt.user.client.ui.Label;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author pmielanczuk
 */
public class StatusBarNotificationAnimator implements IFoxyNotificationHandler {
    //ww: nie jest final aby konkretny projekt mógł to zmienić 
    // (nadpisać przy starcie entry-point)

    public static int MESSAGE_FADE_OUT_DURATION_MILLIS = 2000;
    public static int MESSAGE_FADE_OUT_DELAY_MILLIS = 3000;
    protected Map<Label, Animation> infoWidgetAnimations = new HashMap<Label, Animation>();
    protected Label notificationWidget;
    protected Label errorWidget;

    public StatusBarNotificationAnimator(Label notificationWidget, Label errorWidget) {
        this.notificationWidget = notificationWidget;
        this.errorWidget = errorWidget;
    }

    protected void showAppMessage(String info, boolean isError) {
        String diagMsgKind = ClientDiagMsgs.APP_MSG_KIND + ":" + (isError ? "error" /* I18N: no */ : "info" /* I18N: no */);
        if (ClientDiagMsgs.isShowDiagEnabled(diagMsgKind)) {
            ClientDiagMsgs.showDiagMsg(diagMsgKind, info);
        }

        final Label label = isError ? errorWidget : notificationWidget;

        Animation ani = getInfoWidgetAnimation(label);

        if (ani != null) {
            ani.cancel();
        }

        label.setText(info);
        label.setVisible(true);
        label.getElement().getStyle().setOpacity(1);

        ani = new Animation() {
            @Override
            protected void onUpdate(double progress) {
                label.getElement().getStyle().setOpacity(1 - progress);
                if (progress == 1) {
                    label.setVisible(false);
                    setInfoWidgetAnimation(label, null);
                }
            }
        };

        setInfoWidgetAnimation(label, ani);

        ani.run(MESSAGE_FADE_OUT_DURATION_MILLIS, Duration.currentTimeMillis() + MESSAGE_FADE_OUT_DELAY_MILLIS);
    }

    protected Animation getInfoWidgetAnimation(Label w) {
        return infoWidgetAnimations.get(w);
    }

    protected void setInfoWidgetAnimation(Label w, Animation ani) {
        infoWidgetAnimations.put(w, ani);
    }

    public void showInfo(String msg) {
        showAppMessage(msg, false);
    }

    public void showWarning(String err) {
        showAppMessage(err, true);
    }

    public boolean isInfoModal() {
        return false;
    }

    public boolean isWarningModal() {
        return false;
    }
}
