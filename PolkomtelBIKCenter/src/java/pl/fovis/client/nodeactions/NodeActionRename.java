/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.TreeNodeDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class NodeActionRename extends NodeActionUpdatingBase {

    public String getCaption() {
        return I18n.zmienNazwe.get() /* I18N:  */;
    }

    public void execute() {

        if (TreeNodeDialog.mustCheckNodeNameUniqueness(node)) {
            BIKClientSingletons.getService().getBikAllArts(node.treeId, new StandardAsyncCallback<List<String>>() {
                public void onSuccess(final List<String> keywords) {
                    buildAndShowDialog(keywords);
                }
            });
        } else {
            buildAndShowDialog(null);
        }
    }

    protected void buildAndShowDialog(final List<String> keywords) {
        new TreeNodeDialog().buildAndShowDialog(node.nodeKindId, node.nodeKindCode, node, null, node.treeId, keywords, new IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>>() {
            @Override
            public void doIt(Pair<TreeNodeBean, Map<String, AttributeBean>> param2) {
                TreeNodeBean param = param2.v1;
                BIKClientSingletons.getService().updateTreeNode(node, new StandardAsyncCallback<List<String>>("Failure on" /* I18N: no */ + " updateTreeNode") {
                            @Override
                            public void onSuccess(List<String> result) {
                                BIKClientSingletons.showInfo(I18n.zmienionoNazwe.get() /* I18N:  */);
                                //refreshRedisplayDataAndSelectRecord(node.id, false);
                                refreshAndRedisplayDataByTreeCode(result);
                                if (result == null || result.isEmpty()) {
                                    redisplayData();
                                }
                            }
                        });
                ctx.getTreeGrid().setNodeName(node.id, node.name);
                maybeSortSiblingsWithDefaultOrder();

                //zmienic to odswiezanie zeby nie odswiezalo calego drzewka..
//                        if (node.nodeKindCode.equals("Glossary") || node.nodeKindCode.equals("GlossaryNotCorpo")) {
//                            List<String> nodeKindCode = new ArrayList<String>();
//                            nodeKindCode.add("Glossary");
//                            refreshAndRedisplayDataByTreeCode(nodeKindCode);
//                        }
            }
        });
    }

    @Override
    public boolean isEnabledByDesign() {
        if (node.nodeKindCode.startsWith(BIKConstants.METABIKS_PREFIX)) {
            return false;
        }
        //ww->authors: ostateczny tryb edycji wszystkiego, wszystko można edytować!
        if (isInUltimateEditMode()) {
            return true;
        }

        //ww: edycja węzłów wbudowanych, gratka dla adminka! ;-)
        //ww->authors: przerabiam, żeby też ekspert mógł to przeedytować
        if (isEffectiveExpert() && isNodeBuiltIn() && !isConfluenceObject()) {
            return true;
        }

        if (isConfluenceObject()) {
            return false;
        }

        if (isInDQMAdminMode()) {
            return !isDQMTest() && !isNodeBuiltIn();
        }

        if (isNodeOneOfTheKind(true, BIKGWTConstants.NODE_KIND_USER, BIKGWTConstants.NODE_KIND_BLOG_ENTRY, BIKGWTConstants.NODE_KIND_GLOSSARY_VERSION /* "User", "BlogEntry", "GlossaryVersion"*/, BIKGWTConstants.NODE_KIND_USERS_ROLE)) {
            return false;
        }

        if (isNodeOneOfTheKind(false, BIKConstants.NODE_KIND_LISA_FOLDER)) {
            return true;
        }

//        if (isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_BLOG/*"Blog"*/)) {
//            // 1. dla niewbudowanych i niepodlinkowanych oraz
//            // 2. musi być adminem bloga
//            return isNodeNotLinkedAnyhowNorBuiltinNorReadOnlyNode()
//                    && isLoggedUserInBlogAuthors(true);
//        } else {
//            return isNodeNotLinkedAnyhowNorBuiltinNorReadOnlyNode();
//        }
        if (BIKClientSingletons.isLoggedUserAuthorOfTree(node.treeId) || BIKClientSingletons.isLoggedUserCreatorOfTree(node.treeId)) {
            return true;
        }
        return isNodeNotLinkedAnyhowNorBuiltinNorReadOnlyNode();
    }
}
