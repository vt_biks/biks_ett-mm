/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.basewidgets;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author pmielanczuk
 *
 * @param <W> - class of main widget
 *
 */
public abstract class IsWidgetBase<W extends Widget> implements IsWidget {

    protected W mainWidget;

    protected abstract W createMainWidget();

    protected abstract void fillMainWidget();

    protected void clearMainWidget() {
        // no-op
    }

    protected void initCreatedMainWidget() {
        // no-op
    }

    @Override
    public Widget asWidget() {
        if (mainWidget == null) {
            rebuildMainWidget();
        }
        return mainWidget;
    }

    protected void rebuildMainWidget() {
        if (mainWidget == null) {
            mainWidget = createMainWidget();
            initCreatedMainWidget();
        } else {
            clearMainWidget();
        }
        fillMainWidget();
    }
}
