/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.reportsdk;

import com.businessobjects.rebean.wi.DataSourceObject;
import com.businessobjects.rebean.wi.ObjectQualification;
import com.crystaldecisions.sdk.exception.SDKException;
import com.crystaldecisions.sdk.framework.CrystalEnterprise;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import com.crystaldecisions.sdk.plugin.CeKind;
import com.crystaldecisions.sdk.properties.IProperties;
import com.crystaldecisions.sdk.properties.IProperty;
import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.sapbo.javasdk.SapBOBean;
import pl.bssg.metadatapump.sapbo.javasdk.SapBOPump;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.SimpleDateUtils;
import simplelib.SingleValue;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public abstract class AbstractReportPump extends MetadataPumpBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected static final boolean STORE_DATA = true;
//    protected List<ReportObjectBean> reportObjectList = new LinkedList<ReportObjectBean>();
//    protected List<ReportQueryBean> reportQueryList = new LinkedList<ReportQueryBean>();
    protected static final String WHEREWI = "(SI_KIND=\'" + CeKind.WEBI + "\') AND (SI_INSTANCE=0)";
    protected static final String WHEREFC = "(SI_KIND=\'" + CeKind.FullClient + "\') AND (SI_INSTANCE=0)";
    protected static final String WHERECOUNT = "(SI_KIND=\'" + CeKind.FullClient + "\' OR SI_KIND=\'" + CeKind.WEBI + "\') AND (SI_INSTANCE=0)";
    protected Map<Integer, ModifiedReportBean> candidateReportsMap = new HashMap<Integer, ModifiedReportBean>();
    protected Map<Integer, String> oldReportsMap = new HashMap<Integer, String>();
    protected List<Integer> reportsToDelete = new ArrayList<Integer>();
    protected List<ModifiedReportBean> reportsToAdd = new ArrayList<ModifiedReportBean>();
    protected List<Integer> reportTmpList = new ArrayList<Integer>();
    protected IEnterpriseSession enterpriseSession;
    protected IInfoStore iStore;
    protected boolean isExtraDetailsLoggingEnable;
    protected boolean openAllReportsForReportSDK;
    protected boolean ignoreRefreshOnOpen;
    protected boolean purgeWebi;

    protected Integer getCount(IInfoStore infoStore) {
        IInfoObjects objects = null;
        try {
            objects = infoStore.query("SELECT COUNT(SI_ID) FROM CI_INFOOBJECTS WHERE " + WHEREWI);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in getCount (Report SDK). Message: " + ex.getMessage(), ex);
            }
        }
        IInfoObject user = (IInfoObject) objects.get(0);
        IProperty property = user.properties().getProperty("SI_AGGREGATE_COUNT");
        IProperties properties = (IProperties) property.getValue();
        return (Integer) properties.getProperty("SI_ID").getValue();
    }

    protected String getClassName(DataSourceObject obj) {
        if (obj.getQualification() != ObjectQualification.CLASS) {
            return getClassName((DataSourceObject) obj.getParent());
        }
        return obj.getName();
    }

    protected void createCandidateReportsList() throws SDKException {
        Integer si_ID = null;
        while (true) {
            String sQuery = "select top 100 * from CI_INFOOBJECTS WHERE " + (si_ID == null ? "SI_NAME='~Webintelligence' " : "(si_id > " + si_ID + ") AND (SI_NAME='~Webintelligence')") + " ORDER BY SI_ID";
            IInfoObjects webiRaport = (IInfoObjects) iStore.query(sQuery);
            for (int i = 0; i < webiRaport.size(); i++) {
                IInfoObject iObj = (IInfoObject) webiRaport.get(i);
                si_ID = iObj.getID();//properties().getInt("SI_ID");
                reportTmpList.add(si_ID);
            }
            if (webiRaport.size() < 100) {
                break;
            }
        }
        String whereString = createWhereString();
        Integer siID = null;
        while (true) {
            String sQuery = "SELECT top 100 * FROM CI_INFOOBJECTS " + (siID == null ? (whereString != null ? " WHERE " + whereString : "") : "WHERE (si_id > " + siID + ")" + (whereString != null ? " AND " + whereString : "")) + " ORDER BY SI_ID";
            IInfoObjects webiRaport = (IInfoObjects) iStore.query(sQuery);
            for (int i = 0; i < webiRaport.size(); i++) {
                IInfoObject iObj = (IInfoObject) webiRaport.get(i);
                siID = iObj.getID();//properties().getInt("SI_ID");
                candidateReportsMap.put(iObj.getID(), new ModifiedReportBean(iObj.getID(), iObj.getTitle(), iObj.getUpdateTimeStamp()));
            }
            if (webiRaport.size() < 100) {
                break;
            }
        }
    }

    protected void openAndGetObjectsFromReports() throws SDKException {
        Integer queryCount = execNamedQuerySingleVal("getMaxQueryId", true, "ide");
        if (queryCount == null) {
            queryCount = 0;
        }
        SingleValue<Integer> queryCnt = new SingleValue<Integer>(queryCount);
        for (int i = 0; i < reportsToAdd.size(); i++) {
            ModifiedReportBean actualReport = reportsToAdd.get(i);
            String reportName = actualReport.name;
            int reportId = actualReport.id;
            Date modified = actualReport.modified;
            Integer reportNodeId = execNamedQuerySingleVal("getReportNodeId", true, "id", Integer.toString(reportId));
            if (reportNodeId != null) {
                openReportAndSaveMetadata(reportName, reportId, i, queryCnt, reportNodeId, modified);
            } else {
                safeLog("Report has not been opened: " + reportName + ", id: " + reportId + ". Report does not exists in BIKS", null);
            }
        }
    }

    private String createWhereString() {
        StringBuilder string = new StringBuilder();
        if (reportTmpList.size() > 0) {
            string.append("(SI_PARENTID NOT IN (");
            for (Integer siId : reportTmpList) {
                string.append(siId).append(", ");
            }
            string.delete(string.length() - 2, string.length());
            string.append(")) AND ").append(WHEREWI);
        } else {
            string = null;
        }
        return string != null ? string.toString() : null;
    }

    public String getPumpGroup() {
        return PumpConstants.PUMP_GROUP_BO;
    }

    @Override
    protected Integer insertMainLog() {
        return getLastStartLog(getSourceName());
    }

    @Override
    protected Pair<Integer, String> startPump() {
        execNamedCommandWithCommit("createBOInstanceFunctions", getSourceName(), instanceName);
        reportTmpList.clear();
        candidateReportsMap.clear();
        reportsToDelete.clear();
        reportsToAdd.clear();

        SapBOBean configBean = getAdhocDao().createBeanFromNamedQry("getBOConnectionParamatersForSource", SapBOBean.class, true, getSourceName(), instanceName);

        try {
            pumpLogger.pumpLog("Getting connection parameters", null);
            isExtraDetailsLoggingEnable = BaseUtils.tryParseBoolean((String) execNamedQuerySingleVal("getAppPropValue", true, "val", "isExtraDetailsLoggingForReportSDKEnable"), false);
            openAllReportsForReportSDK = BaseUtils.tryParseBoolean((String) execNamedQuerySingleVal("getAppPropValue", true, "val", "openAllReportsForReportSDK"), false);
            ignoreRefreshOnOpen = BaseUtils.tryParseBoolean((String) execNamedQuerySingleVal("getAppPropValue", true, "val", "ignoreRefreshOnOpenForReportSDK"), false);
            purgeWebi = BaseUtils.tryParseBoolean((String) execNamedQuerySingleVal("getAppPropValue", true, "val", "purgeWebiForReportSDK"), false);

            enterpriseSession = CrystalEnterprise.getSessionMgr().logon(configBean.userName, configBean.password, configBean.server, SapBOPump.AUTHENTICATION_METHOD);
            iStore = (IInfoStore) enterpriseSession.getService("", "InfoStore");
            pumpLogger.pumpLog("Do optional initialize", null);
            doOptInitialize();
            pumpLogger.pumpLog("Create candidate reports list", null);
            createCandidateReportsList();
            if (STORE_DATA) {
                execNamedCommandWithCommit("prepareMultiInstanceTablesForReportSDK");
            }
            if (openAllReportsForReportSDK) {
                execNamedCommandWithCommit("deleteAllReportsData");
            }

            List<ModifiedReportBean> oldReports = getAdhocDao().createBeansFromNamedQry("getModifiedReportInfo", ModifiedReportBean.class);
            for (ModifiedReportBean modifiedReportBean : oldReports) {
                String oldDate = SimpleDateUtils.toCanonicalString(modifiedReportBean.modified);
                oldReportsMap.put(modifiedReportBean.id, oldDate);
            }
            for (Integer oldInt : oldReportsMap.keySet()) {
                if (!candidateReportsMap.containsKey(oldInt)) { // raport zostal usuniety
                    reportsToDelete.add(oldInt);
                }
            }
            for (Integer newInt : candidateReportsMap.keySet()) {
                if (!oldReportsMap.containsKey(newInt)) { // jest nowy raport
                    reportsToAdd.add(new ModifiedReportBean(newInt, candidateReportsMap.get(newInt).name, candidateReportsMap.get(newInt).modified));
                } else { // sprawdzamy czas modyfikacji
                    if (!SimpleDateUtils.toCanonicalString(candidateReportsMap.get(newInt).modified).equals(oldReportsMap.get(newInt))) { // data się zmieniła
                        reportsToAdd.add(new ModifiedReportBean(newInt, candidateReportsMap.get(newInt).name, candidateReportsMap.get(newInt).modified));
                        reportsToDelete.add(newInt);
                    }
                }
            }
            reportsToDelete.add(0); // usuwamy stare
//            System.out.println("Reports to delete: " + reportsToDelete);
//            System.out.println("Reports to add: " + reportsToAdd.size());
            if (STORE_DATA) {
                execNamedCommandWithCommit("deleteOldReports", reportsToDelete); // usuwamy stare
            }
            pumpLogger.pumpLog("Open and get objects from reports", null);
            openAndGetObjectsFromReports(); //otwieramy i wrzucamy nowe
            if (STORE_DATA) {
//                Integer treeId = execNamedQuerySingleVal("getIdTreeReports", true, "id");
                pumpLogger.pumpLog("Delete and fix bad tables", 92);
                execNamedCommandWithCommit("deleteReportsIds");
                if (shouldFixBadCUID()) {
                    execNamedCommandWithCommit("fixBadCuids");
                }
                pumpLogger.pumpLog("Insert and update report query", 93);
                execNamedCommandWithCommit("insertReportQueriesToBikNode");
                pumpLogger.pumpLog("Insert and update report objects", 94);
                execNamedCommandWithCommit("updateReportObjects");
                execNamedCommandWithCommit("insertReportObjectsToBikNode");
//                pumpLogger.pumpLog("Wykonywanie procedury deleteIsDeletedLinkedNodes", 95);
//                execNamedCommandWithCommit("deleteIsDeletedLinkedNodes");
//                pumpLogger.pumpLog("Wykonywanie procedury sp_node_init_branch_id", 96);
//                execNamedCommandWithCommit("sp_node_init_branch_id", treeId);
                pumpLogger.pumpLog("Insert extra info for query", 97);
                execNamedCommandWithCommit("insertQueryExtraData");
                pumpLogger.pumpLog("Adding associations between metadata", 98);
                execNamedCommandWithCommit("insertIntoJOMetadataConnections"); // for BO
                execNamedCommandWithCommit("insertIntoJOTeradataMetadataConnections"); // for Teradata
                execNamedCommandWithCommit("insertIntoJOMSSQLMetadataConnections"); // for MS SQL
                execNamedCommandWithCommit("insertIntoJOOracleMetadataConnections"); // for Oracle
                execNamedCommandWithCommit("sapboJoinContentWithUsers"); // for BIKS users

                execNamedCommandWithCommit("finishMultiInstanceTablesForReportSDK");
//                execNamedCommandWithCommit("updateReportsUpdateTS");
            }
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, "Success");
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error: " + e.getMessage(), e);
            }
            throw new LameRuntimeException(e.getMessage(), e);
        } finally {
            if (enterpriseSession != null) {
                enterpriseSession.logoff();
            }
        }
    }

    protected void safeLog(String text, Integer progress) {
        safeLogWithDetails(text, progress, true);
    }

    protected void safeLogWithDetails(String text, Integer progress, Boolean isExtraLoggingEnable) {
        if (STORE_DATA && isExtraLoggingEnable != null && isExtraLoggingEnable) {
            pumpLogger.pumpLog(text, progress);
        }
    }

    protected void doOptInitialize() throws Exception {
        // NO OP
    }

    // tylko dla BO 3.1
    protected boolean shouldFixBadCUID() {
        return true;
    }

    protected abstract void openReportAndSaveMetadata(String reportName, int reportObjId, int reportCount, SingleValue<Integer> queryCount, Integer reportNodeId, Date reportModifiedTime);
}
