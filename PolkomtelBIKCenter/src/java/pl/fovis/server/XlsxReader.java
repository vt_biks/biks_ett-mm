/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import pl.trzy0.foxy.commons.FoxyRuntimeException;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class XlsxReader {

//    private static final int READ_SIZE = 10000;
    protected List<String> headerNames = new ArrayList<String>();
    protected List<Map<String, String>> content = new ArrayList<Map<String, String>>();
    protected Map<String, String> refHeaderMap = new HashMap<String, String>();
    protected FileInputStream fis = null;

    public XlsxReader(File xlsxFile, final Map<String, String> mappedHeaders) throws FoxyRuntimeException {
        try {
            OPCPackage pkg = OPCPackage.open(fis = new FileInputStream(xlsxFile));
            XSSFReader reader = new XSSFReader(pkg);
            StylesTable styles = reader.getStylesTable();
            ReadOnlySharedStringsTable sharedStrings = new ReadOnlySharedStringsTable(pkg);
            ContentHandler handler = new XSSFSheetXMLHandler(styles, sharedStrings, new XSSFSheetXMLHandler.SheetContentsHandler() {

                private Map<String, String> row;
                private int currentRow;

                @Override
                public void startRow(int rowNum) {
                    row = new HashMap<String, String>();
                    currentRow = rowNum;
                }

                @Override
                public void endRow(int i) {
                    if (currentRow > 0) {
                        content.add(row);
                    }
//                System.out.println("");
                }

                @Override
                public void cell(String cellReference, String value, XSSFComment xssfc) {
//                System.out.println("cellReference = " + cellReference);
                    if (currentRow == 0) {
                        //czyta header
                        headerNames.add(value);
                        refHeaderMap.put(getColumnName(cellReference), value);
                    } else {
                        if (mappedHeaders == null /* || currentRow > READ_SIZE */) {
                            throw new FoxyRuntimeException("Throw when read headers only - HACK do zatrzymania czytnika");
                        }
                        String header = refHeaderMap.get(getColumnName(cellReference));
//                    System.out.println(header);
                        String nameInDb = mappedHeaders.get(header);
                        if (nameInDb == null) { //nie czyta komorki jak nie potrzebna
                            return;
                        }
                        row.put(nameInDb, BaseUtils.isStrEmptyOrWhiteSpace(value) ? null : value.trim());
                    }
                }

                @Override
                public void headerFooter(String text, boolean isHeader, String tagName) {
                }

                private String getColumnName(String cellReference) {
                    String columnName = "";
                    int i = 0;
                    while (i < cellReference.length()) {
                        char c = cellReference.charAt(i++);
                        if ('A' <= c && c <= 'Z') {
                            columnName += c;
                        } else {
                            break;
                        }
                    }
                    return columnName;
                }
            }, true);

            XMLReader parser = XMLReaderFactory.createXMLReader();
            parser.setContentHandler(handler);
            try {
                parser.parse(new InputSource(reader.getSheetsData().next()));
            } catch (FoxyRuntimeException ex) {
            }
        } catch (Exception ex) {
            throw new FoxyRuntimeException(ex.getMessage());
        }
    }
//
//    public void close() throws IOException {
//        wb.close();
//    }
//
//    public List<Map<String, String>> readContent(Map<String, String> mappedHeaders, Integer start, Integer limit) {
//        List<Map<String, String>> lines = new ArrayList<Map<String, String>>();
//        Integer finish = start + limit;
//        if (finish > getLineNum()) {
//            finish = getLineNum();
//        }
//        for (int i = start; i < finish; i++) {
//            Map<String, String> map = new HashMap<String, String>();
//            Row row = mainSheet.getRow(i);
//            for (int cellNum = 0; cellNum < headerNames.size(); cellNum++) {
//                Cell cell = row.getCell(cellNum);
//                String headerName = headerNames.get(cellNum);
//                String nameInDb = mappedHeaders.get(headerName);
//                String value = cell.getStringCellValue();
//                map.put(nameInDb, value);
//            }
//            lines.add(map);
//        }
//        return lines;
//    }
//

    public int getLineNum() {
        return content.size();
    }

    public static void main(String[] args) throws Exception {
//        final XlsxReader reader = new XlsxReader(new File("C:/Chung/BSSG-2016-01-13.xlsx"), null);
//        System.out.println("Liczba kolumn: " + reader.headerNames.size());
//        Map<String, String> map = new HashMap<String, String>();
//        for (String header : reader.headerNames) {
//            map.put(header, header);
//        }
//        System.out.println("Reading content");
//        long start = System.currentTimeMillis();
//        XlsxReader reader2 = new XlsxReader(new File("C:/Chung/BSSG-2016-01-13.xlsx"), map);
//        System.out.println("Done reading content:" + (System.currentTimeMillis() - start) / 1000.0);
//        List<Map<String, String>> content = reader2.getContent();
////        String sql = "insert into db_sbx_nas.test_prod2 ( \"id_prod\",\"id_prod_nadrz\",\"id_typ_prod\",\"nazwa_prod\",\"id_prod_wersja\",\"data_poczatku\",\"data_konca\",\"id_prod_zr\",\"id_system_zr\",\"id_typ_usluga\",\"id_podtyp_usluga\",\"il_cykli\",\"id_typ_jednostki_miary\",\"wartosc\",\"id_typ_wartosc\",\"il_wystapien\",\"id_cp_tier\",\"id_typ_oplata\",\"id_typ_plan_taryfowy\",\"il_miesiecy_plan_taryfowy\",\"id_plan_taryfowy_nast\",\"id_typ_jednostki_miary_cena\",\"cena_domyslna\",\"oplata_stala\",\"id_sprzet_marka\",\"id_typ_kupon\",\"data_stat\",\"id_podtyp_prod\",\"blokada_wsk\",\"priorytet_wsk\",\"wartosc_proc_upustu\",\"kwota_odnowienia_od\",\"suma_odnowienia\",\"anu_wsk\",\"wsk_in\",\"strefa_czasowa_typ\",\"strefa_czasowa_dnia\",\"okres_powt\",\"id_typ_zuzycia\",\"alokacja_wsk\",\"id_kod_gl\",\"efaktura_wsk\",\"id_load\",\"id_update\") values (cast(? as char(11)),cast(? as char(11)),cast(? as char(10)),cast(? as char(255)),cast(? as char(30)),cast(? as date),cast(? as date),cast(? as char(100)),cast(? as byteint),cast(? as char(2)),cast(? as char(2)),cast(? as smallint),cast(? as char(5)),cast(? as decimal(18,2)),cast(? as byteint),cast(? as integer),cast(? as byteint),cast(? as char(1)),cast(? as byteint),cast(? as byteint),cast(? as char(11)),cast(? as char(5)),cast(? as decimal(18,2)),cast(? as decimal(18,2)),cast(? as smallint),cast(? as char(2)),cast(? as date),cast(? as smallint),cast(? as char(1)),cast(? as smallint),cast(? as decimal(10,6)),cast(? as smallint),cast(? as decimal(18,2)),cast(? as byteint),cast(? as byteint),cast(? as char(50)),cast(? as char(50)),cast(? as char(50)),cast(? as integer),cast(? as char(1)),cast(? as char(10)),cast(? as byteint),cast(? as integer),cast(? as integer));";
//        final String sql = "insert into db_sbx_nas.test_prod2 ( \"id_prod\",\"id_prod_nadrz\",\"id_typ_prod\",\"nazwa_prod\",\"id_prod_wersja\",\"data_poczatku\",\"data_konca\",\"id_prod_zr\",\"id_system_zr\",\"id_typ_usluga\",\"id_podtyp_usluga\",\"il_cykli\",\"id_typ_jednostki_miary\",\"wartosc\",\"id_typ_wartosc\",\"il_wystapien\",\"id_cp_tier\",\"id_typ_oplata\",\"id_typ_plan_taryfowy\",\"il_miesiecy_plan_taryfowy\",\"id_plan_taryfowy_nast\",\"id_typ_jednostki_miary_cena\",\"cena_domyslna\",\"oplata_stala\",\"id_sprzet_marka\",\"id_typ_kupon\",\"data_stat\",\"id_podtyp_prod\",\"blokada_wsk\",\"priorytet_wsk\",\"wartosc_proc_upustu\",\"kwota_odnowienia_od\",\"suma_odnowienia\",\"anu_wsk\",\"wsk_in\",\"strefa_czasowa_typ\",\"strefa_czasowa_dnia\",\"okres_powt\",\"id_typ_zuzycia\",\"alokacja_wsk\",\"id_kod_gl\",\"efaktura_wsk\",\"id_load\",\"id_update\") values (";
//        String url = "jdbc:teradata://192.168.167.75/TMODE=ANSI,CHARSET=UTF8";
//        Class.forName("com.teradata.jdbc.TeraDriver");
//        Connection conn = DriverManager.getConnection(url, "dbc", "dbc");
//        int i = 0;
//        try {
//            Statement st = conn.createStatement();
//            ResultSet rs = st.executeQuery("select count(1) from db_sbx_nas.prod");
//            rs.next();
//            System.out.println("prod count = " + rs.getInt(1));
//            PreparedStatement ps = conn.prepareStatement(sql);
//            conn.setAutoCommit(false);
//            int packSize = 100;
//            int added = 0;
//            start = System.currentTimeMillis();
//            for (i = 0; i < READ_SIZE; i++) {
//                Map<String, String> record = content.get(i);
//                for (int colId = 0; colId < reader2.headerNames.size(); colId++) {
//                    String header = reader2.headerNames.get(colId);
//                    String value = record.get(header);
//                    ps.setObject(colId + 1, value);
//                }
//                ps.addBatch();
//                added++;
//                if (added == packSize) {
//                    ps.executeUpdate();
//                    ps.clearBatch();
//                    System.out.println("Pack " + i / packSize);
//                }
//            }
//            conn.commit();
//            System.out.println("Done insert content:" + (System.currentTimeMillis() - start) / 1000.0);
//        } catch (SQLException ex) {
//            System.out.println("i = " + i);
//            ex.printStackTrace();
//        }
//        StringBuilder sb = new StringBuilder();
//        sb.append(BaseUtils.mergeWithSepEx(content, ";", new BaseUtils.Projector<Map<String, String>, String>() {
//
//            @Override
//            public String project(final Map<String, String> record) {
//                return sql + BaseUtils.mergeWithSepEx(reader.headerNames, ",", new BaseUtils.Projector<String, String>() {
//
//                    @Override
//                    public String project(String colName) {
//                        String value = record.get(colName);
//                        if (value == null || BaseUtils.isStrEmptyOrWhiteSpace(value)) {
//                            value = "null";
//                        } else {
//                            value = "'" + value + "'";
//                        }
//
//                        switch (LisaDataType.getValueFor(column.dataType)) {
//                            case AT:
//                                value = "cast(" + value + " as time)";
//                            case BF:
//                            case BV:
//                                return "TO_BYTE(cast(" + value + " as integer))";
//                            case D:
//                                return "cast(" + value + " as decimal(" + column.decimalTotalDigits + "," + column.decimalFractionalDigits + "))";
//                            case DA:
//                                return "cast(" + value + " as date)";
//                            case F:
//                                return "cast(" + value + " as float)";
//                            case I1:
//                                return "cast(" + value + " as byteint)";
//                            case I2:
//                                return "cast(" + value + " as smallint)";
//                            case I8:
//                                return "cast(" + value + " as bigint)";
//                            case I:
//                                return "cast(" + value + " as integer)";
//                            case TS:
//                                return "cast(cast(" + value + " as date) as timestamp)";
//                            default:
//                                return "cast(" + value + " as char(" + column.columnLength + "))";
//                        }
//                    }
//                }) + ")";
//            }
//        }));
    }

    public List<String> getHeaderNames() {
        return headerNames;
    }

    public List<Map<String, String>> getContent() {
        return content;
    }

    public void close() {
        if (fis != null) {
            try {
                fis.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
