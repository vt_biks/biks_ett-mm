/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import java.util.Set;

/**
 *
 * @author wezyr
 */
public interface IMapHelperWrapper<T> {
    public Object getPropValue(T name);
    public Set<T> getPropNames();
}
