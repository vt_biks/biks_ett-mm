/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author LKI
 */
public class UserRegistrationDataBean implements Serializable {

    public String email;
    public String password;
    public String company;
    public String name;
    public String lastName;
    public String token;
    public Integer templateId;
    public String attention;
    public String position;
    public String phoneNum;
}
