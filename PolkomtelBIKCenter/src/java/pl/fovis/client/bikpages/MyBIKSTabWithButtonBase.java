/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import java.util.List;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.GWTUtils;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author bfechner
 */
public abstract class MyBIKSTabWithButtonBase<T> extends MyBIKSTabWithPaginatorBase<T> {
    
    protected CheckBox filterItemsCB = new CheckBox();
    protected BikCustomButton customHeaderBtn;
    
    @Override
    protected HorizontalPanel createOptHeaderWidget() {
        HorizontalPanel hp = new HorizontalPanel();
        
        GWTUtils.addWidgets(hp, createHeaderButton(), createFilterCheckBox());
        hp.setStyleName("MyBIK-Hp");
        return hp;
    }
    
    protected boolean isHeaderBtnEnabled(boolean forEmptyFilteredItems) {
        return false;
    }
    
    protected BikCustomButton createHeaderButton() {
        
        String btnCaption = getOptHeaderBtnCaption();
        if (btnCaption == null) {
            return null;
        }
        
        customHeaderBtn = new BikCustomButton(btnCaption, getHeaderBtnStyle(), new IContinuation() {
            public void doIt() {
                headerButtonClicked();
            }
        });
        
        customHeaderBtn.setVisible(false);
        return customHeaderBtn;
    }
    
    protected abstract List<T> filterAllItems();
    
    protected abstract String getFilterCheckBoxCaption();
    
    protected CheckBox createFilterCheckBox() {
        
        String filterCaption = getFilterCheckBoxCaption();
        
        if (filterCaption == null) {
            return null;
        }
        
        filterItemsCB.setText(filterCaption);
        filterItemsCB.setStyleName("News" /* i18n: no:css-class */ + "-lookNewsEntry");
        filterItemsCB.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                addOptionalFilterByKindWidgets(filterItemsCB.getValue() ? filterAllItems() : allItems);
                displayData();
                
            }
        });
        filterItemsCB.setVisible(false);
        return filterItemsCB;
    }
    
    protected abstract String getOptHeaderBtnCaption();
    
    protected abstract String getHeaderBtnStyle();
    
    protected abstract void headerButtonClicked();
    
    @Override
    public List<T> prepareAllItemsToDisplay() {
        List<T> filteredItems = filterAllItems();
        filteredItems = filteredItemsByKind(filteredItems);
        boolean isFilteredEmpty = BaseUtils.isCollectionEmpty(filteredItems);
        
        if (isFilteredEmpty) {
            // tylko oznacz w check-boksie, że widać wszystko
            filterItemsCB.setValue(false, false);
        }
        
        if (customHeaderBtn != null) {
            customHeaderBtn.setVisible(isHeaderBtnEnabled(isFilteredEmpty));
        }
        filterItemsCB.setVisible(!isFilteredEmpty);
        
        return filterItemsCB.getValue() ? filteredItems : filteredItemsByKind(allItems);
    }
    
    protected List<T> filteredItemsByKind(List<T> items) {
        return items;
    }
    
    @Override
    public void updateHasNewItems(boolean hasNewItems) {
        super.updateHasNewItems(hasNewItems);
        
        filterItemsCB.setVisible(hasNewItems || filterItemsCB.getValue());
        
        if (customHeaderBtn != null) {
            customHeaderBtn.setVisible(isHeaderBtnEnabled(!hasNewItems));
        }
    }
}
