﻿exec sp_check_version '1.0.65';

update bik_user
set
  activ = 0,
  old_login_name = u.login_name,
  login_name = '~' + convert(varchar, u.id)
from bik_user u
inner join bik_node n on n.id = u.node_id
where n.is_deleted = 1 and u.old_login_name is null
  
exec sp_update_version '1.0.65', '1.0.66';
go
