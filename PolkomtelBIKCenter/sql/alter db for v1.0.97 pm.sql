﻿exec sp_check_version '1.0.97';
go

----------------------------------
----------------------------------
----------------------------------

--drop index idx_bik_sapbo_universe_table_node_id on bik_sapbo_universe_table
--go


create unique index idx_bik_sapbo_universe_table_node_id on  bik_sapbo_universe_table (node_id)
go


create unique index idx_bik_sapbo_query_node_id on bik_sapbo_query (node_id)
go


----------------------------------
----------------------------------
----------------------------------

exec sp_update_version '1.0.97', '1.0.97.0.1';
go
