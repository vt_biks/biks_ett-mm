/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

/**
 *
 * @author pmielanczuk
 */
public class FvsChangeExBean extends ObjectInFvsBean {

    public String branchIds;
    public String treeCode;
    public String menuPath;
    public String namePath;
    public String iconName;
    public int cntDelete;
    public int cntNew;
    public int cntUpdate;
    public int cntJoinedObjChange;
    public int cntNewComments;
    public int cntVoteChange;
    public int cntDqcTestFails;
    public int cutParentId; 
    public int isPasted;
}
