/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.core.client.Scheduler.RepeatingCommand;

/**
 *
 * @author wezyr
 */
public abstract class RepeatingCommandFix implements RepeatingCommand {

    @Override
    public final boolean execute() {
        try {
            return doExecute();
        } catch (Exception ex) {
            FoxyClientSingletons.showWarning("error in repeating command" /* I18N: no */);
            ClientDiagMsgs.showException("error in repeating command", ex);
//            if (ClientDiagMsgs.isShowDiagEnabled(BIKConstants.DIAG_MSG_KIND$error)) {
//                ClientDiagMsgs.showDiagMsg(BIKConstants.DIAG_MSG_KIND$error, "error in repeating command: ex=" + ex);
//            }
            return false;
        }
    }

    public abstract boolean doExecute();
}
