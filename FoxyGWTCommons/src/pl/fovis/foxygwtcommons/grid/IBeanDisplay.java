/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.grid;

/**
 *
 * @author pmielanczuk
 */
public interface IBeanDisplay<T> {

//    public void setBeanGrid(IBeanGrid<T> beanGrid);
    
    public Object getHeader(String colName);

    public Object getValue(T bean, String colName);
}
