/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author efedchenko
 * wyświetla 'dodaj zdarzenie' w menu pod prawym przyciskiem
 * (zamiast 'dodaj węzel', jeśli isRegistry = 1 (rejestr))
 */
public class NodeActionAddEvent extends NodeActionAddAnyNode4KindAbstract {

    protected NodeKindBean nodeKindBean;

    public NodeActionAddEvent(NodeKindBean nodeKindBean) {
        this.nodeKindBean = nodeKindBean;
    }
    
    @Override
    public String getCaption() {
        return I18n.dodaj.get() + " " +  nodeKindBean.caption;
    }

    @Override
    public void redisplayData(Integer result) {
        BIKClientSingletons.showInfo(I18n.dodanoNoweZdarzenie.get() /* I18N:  */);
        refreshAndRedisplayData();
    }
}
