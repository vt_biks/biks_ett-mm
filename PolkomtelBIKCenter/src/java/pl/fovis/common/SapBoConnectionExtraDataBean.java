/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class SapBoConnectionExtraDataBean implements Serializable {

    public String server;
    public String userName;
    public String password;
    public String databaseSource;
    public String connectionNetworkLayer;
    public String type;
    public String databaseEngine;
    public Integer clientNumber;
    public String language;
    public String systemNumber;
    public String systemId;
    public String cuid;
    public Date creationTime;
    public Date updateTime;
    public String defaultSchemaName; // for MS SQL
    public String databaseName; // for MS SQL
    public String serverNameForOracle; // for Oracle
}