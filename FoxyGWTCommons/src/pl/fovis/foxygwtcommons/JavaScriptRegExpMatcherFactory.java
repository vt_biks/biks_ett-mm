package pl.fovis.foxygwtcommons;

import simplelib.BaseUtils;
import simplelib.IRegExpMatcherFactory;
import simplelib.IRegExpPattern;

public class JavaScriptRegExpMatcherFactory implements IRegExpMatcherFactory {

//    public IRegExpMatcher matcher(String pattern, String str) {
//        return //new JavaScriptRegExpMatcher(createRegExpr(pattern, ""), str);
//                compile(pattern, false, false).matcher(str);
//    }

    private static boolean isFactorySet = false;

    public static void setFactory() {
        if (!isFactorySet) {
            isFactorySet = true;
            BaseUtils.setRegExpMatcherFactory(new JavaScriptRegExpMatcherFactory());
        }
    }

    public IRegExpPattern compile(String pattern, boolean ignoreCase, boolean multiline) {
        return new JavaScriptRegExpPattern(pattern, ignoreCase, multiline);
    }
}
