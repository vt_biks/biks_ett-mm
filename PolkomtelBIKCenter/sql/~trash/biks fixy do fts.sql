select top 20 bn.id as node_id,
    bn.name,
    bnk.code as node_kind_code,
    bnk.caption as node_kind_caption,
    bt.name as tree_name,
    bt.code as tree_code,
    bn.vote_sum,
    bn.vote_cnt,
    bn.sum_combined_rank, bn.max_combined_rank, bn.sum_search_weight, bn.max_search_weight
    from (
    
    select bn.*, sr.sum_combined_rank, sr.max_combined_rank, sr.sum_search_weight, sr.max_search_weight
    from
    bik_node bn inner join
    (select node_id, 
    sum(sr.[rank] * coalesce(ap.search_weight, 1)) as sum_combined_rank, 
    max(sr.[rank] * coalesce(ap.search_weight, 1)) as max_combined_rank, 
    sum(coalesce(ap.search_weight, 1)) as sum_search_weight,
    max(coalesce(ap.search_weight, 1)) as max_search_weight
    from bik_searchable_attr_val av inner join
    freetexttable(bik_searchable_attr_val, *, 'Deaktywacja') sr on av.id = sr.[key]
    inner join bik_searchable_attr a on av.attr_id = a.id
    left join bik_searchable_attr_props ap on a.name = ap.name
    group by node_id) sr on sr.node_id = bn.id
    where bn.is_deleted = 0 and (bn.linked_node_id is null or bn.disable_linked_subtree <> 0)

    ) as bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    inner join bik_tree bt on bn.tree_id = bt.id
    order by 
    bn.max_search_weight desc, bn.search_rank + bnk.search_rank desc, bn.vote_sum desc, sum_combined_rank desc
        
    


select node_id, coalesce(ap.caption, a.name) as attr_name
from
  bik_searchable_attr_val av inner join bik_searchable_attr a on av.attr_id = a.id
  left join bik_searchable_attr_props ap on a.name = ap.name
where
  av.node_id in (841771, 841773, 841774, 841776) and
  freetext(av.*, 'Deaktywacja')
  
  
select * from bik_searchable_attr_props
  
  
      
/*

    select * from bik_node_kind where code like '%glossary%'

exec sp_set_app_prop 'search_order_by_exprs', 'bn.max_search_weight desc, bn.search_rank + bnk.search_rank desc, bn.vote_sum desc, sum_combined_rank desc'


update bik_node_kind set search_rank = 10 where code = 'Glossary'
update bik_node_kind set search_rank = 9 where code = 'GlossaryNotCorpo'
update bik_node_kind set search_rank = 8 where code = 'GlossaryVersion'

*/
