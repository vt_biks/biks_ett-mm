﻿exec sp_check_version '1.6.z8.5';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------


alter table bik_dqm_test alter column source_id int null;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_dqm_test_multi') and type in (N'U'))
begin
	create table bik_dqm_test_multi (
	  id int not null identity(1,1),
	  test_id int not null references bik_dqm_test(id),
	  test_node_id int not null references bik_node(id),
	  source_id int not null references bik_data_source_def(id),
	  database_node_id int null references bik_node(id), 
	  db_server_id int null,
	  sql_test varchar(max) null,
	  code_name varchar(512) not null,
	  result_file_name varchar(512) null,
	  is_deleted int not null default(0)
	);
end;
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

exec sp_update_version '1.6.z8.5', '1.6.z8.6';
go
