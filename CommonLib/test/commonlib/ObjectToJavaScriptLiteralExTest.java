/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package commonlib;

import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author wezyr
 */
public class ObjectToJavaScriptLiteralExTest extends TestCase {
    
    public ObjectToJavaScriptLiteralExTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    static class TestMuppet1 {
        public int field;
        public TestMuppet1(int field) {
            this.field = field;
        }
    }

    public void testConvertObjectMuppet() {
        TestMuppet1 tm1 = new TestMuppet1(10);
        String res = JsonConverterEx.convertObject(tm1);
        assertEquals("{\"field\":10}", res);
    }

    static class TestMuppet2 {
        public int field;
        public TestMuppet1 tm1;
        public List<TestMuppet1> list = new ArrayList<TestMuppet1>();
        
        public TestMuppet2(int field) {
            this.field = field;
            this.tm1 = new TestMuppet1(field + 100);
            list.add(tm1);
            list.add(new TestMuppet1(field - 1000));
        }
    }

    public void testConvertObjectMuppet2() {
        TestMuppet2 tm2 = new TestMuppet2(10);
        String res = JsonConverterEx.convertObject(tm2);
        assertEquals("{\"field\":10,\"tm1\":{\"field\":110},\"list\":[{\"field\":110},{\"field\":-990}]}", res);
    }
}
