/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.biadmin;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.HashMap;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.BikPageAdminConfigBase;
import pl.fovis.common.BIArchiveConfigBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class BIAArchiveManagerPage extends BikPageAdminConfigBase<BIArchiveConfigBean> {

    protected CheckBox chbIsArchiveActivated;
    protected CheckBox chbIsRemoteSaving;
    protected CheckBox chbIsGit;

    protected TextBox tbxSavedFolder;
    protected TextBox tbxUser;
    protected TextBox tbxPw;
    protected TextBox tbxDomain;
    protected TextBox tbxLcmCliPath;
//    protected TextBox tbxJobCuid;
    protected TextBox tbxLimit;
    protected TextBox tbxGitUsername;
    protected TextBox tbxGitPw;
    protected TextBox tbxFileSize;
    protected TextBox tbxGitRepository;
//    protected CheckBox chbRemoteSavingPw;
//    protected CheckBox chbGitPw;
    protected Label testResult;

    protected ListBox lbxFrequency;
    protected DateBox dbxStartDate;
    protected TextBox tbxStartHour;

    protected BIArchiveConfigBean bean;

    public static final String[] frequencyTxt = {I18n.codziennie.get(), I18n.co3Dni.get(), I18n.co5Dni.get(), I18n.co7Dni.get(), I18n.co14Dni.get(), I18n.co30Dni.get(), I18n.jednorazowo.get()};
    public static final Map<String, Integer> frequency2IdMap = new HashMap<String, Integer>();

    static {
        frequency2IdMap.put("1", 0);
        frequency2IdMap.put("3", 1);
        frequency2IdMap.put("5", 2);
        frequency2IdMap.put("7", 3);
        frequency2IdMap.put("14", 4);
        frequency2IdMap.put("30", 5);
        frequency2IdMap.put("0", 6);
    }

    @Override
    protected void buildInnerWidget() {
        chbIsArchiveActivated = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsArchiveActivated.getValue());
                changeStateAdditionalCheckbox();
            }
        }, false);
        createLaunchingSection();
        createLine(main, "100%");
        createLabel(I18n.sciezkaDoLcmCli.get());
        tbxLcmCliPath = createTextBox();
//        createLabel(I18n.jobCuid.get());
//        tbxJobCuid = createTextBox();
        createLabel(I18n.liczbaObiektowDoBackupowania.get());
        tbxLimit = createTextBox();
        createLabel(I18n.granicaRozmiaruPlikow.get());
        tbxFileSize = createTextBox();

        createLabel(I18n.folderZapisu.get());
        tbxSavedFolder = createTextBox();
        createRemoteSavingSection();

        createGitSection();

        createLine(main, "100%");
        createButton(I18n.zapisz.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                saveConfig();
            }
        });

        createButton(I18n.testujPolaczenie.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().runBIArchiveTestConnection(new StandardAsyncCallback<String>() {

                    @Override
                    public void onSuccess(String result) {
                        testResult.setText(result);
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        testResult.setText(I18n.blad.get());
                    }
                });
            }
        });
        testResult = createLabel("");
        calcServiceToGetData();
    }

    private void changeStateAdditionalCheckbox() {
        changeRemoteSavingState();
        changeGitState();
    }

    private void changeGitState() {
        boolean state = chbIsGit.getValue() && chbIsArchiveActivated.getValue();
        tbxGitUsername.setEnabled(state);
        tbxGitPw.setEnabled(state);
        tbxGitRepository.setEnabled(state);
    }

    private void changeRemoteSavingState() {
        boolean state = chbIsRemoteSaving.getValue() && chbIsArchiveActivated.getValue();
        tbxUser.setEnabled(state);
        tbxPw.setEnabled(state);
        tbxDomain.setEnabled(state);
    }

    protected ListBox createFrequencyBox(CellPanel cp) {
        ListBox listbox = new ListBox();
        listbox.addItem(frequencyTxt[0] /* I18N:  */, "1");
        listbox.addItem(frequencyTxt[1] /* I18N:  */, "3");
        listbox.addItem(frequencyTxt[2] /* I18N:  */, "5");
        listbox.addItem(frequencyTxt[3] /* I18N:  */, "7");
        listbox.addItem(frequencyTxt[4] /* I18N:  */, "14");
        listbox.addItem(frequencyTxt[5] /* I18N:  */, "30");
        listbox.addItem(frequencyTxt[6] /* I18N:  */, "0");
        cp.add(listbox);
        widgetList.add(listbox);
        return listbox;
    }

    private void saveConfig() {
        bean = new BIArchiveConfigBean();
        bean.isActive = chbIsArchiveActivated.getValue() ? 1 : 0;
        bean.savedFolder = tbxSavedFolder.getText();
        bean.user = tbxUser.getText();
        bean.pw = tbxPw.getText();
        bean.domain = tbxDomain.getText();
        bean.isRemoteSaving = chbIsRemoteSaving.getValue() ? 1 : 0;
        bean.useGit = chbIsGit.getValue() ? 1 : 0;
        bean.gitUsername = tbxGitUsername.getText();
        bean.gitPw = tbxGitPw.getText();
        bean.lcmCliPath = tbxLcmCliPath.getText();
        bean.startDate = dbxStartDate.getTextBox().getText();
        bean.frequency = lbxFrequency.getSelectedValue();
        bean.startHour = tbxStartHour.getText();
//        bean.jobCuid = tbxJobCuid.getText();
        bean.limit = tbxLimit.getText();
        bean.fileSize = BaseUtils.tryParseDouble(tbxFileSize.getText(), 0.0);
        bean.gitRepository = tbxGitRepository.getText();
//        if (chbIsArchiveActivateds.getValue()) {
//            if (BaseUtils.isStrEmptyOrWhiteSpace(bean.startDate)
//                    || BaseUtils.isStrEmptyOrWhiteSpace(bean.frequency)
//                    || BaseUtils.isStrEmptyOrWhiteSpace(bean.startHour)
//                    || BaseUtils.isStrEmptyOrWhiteSpace(bean.savedFolder)
//                    || BaseUtils.isStrEmptyOrWhiteSpace(bean.user)
//                    || BaseUtils.isStrEmptyOrWhiteSpace(bean.pw)
//                    || BaseUtils.isStrEmptyOrWhiteSpace(bean.domain)) {
//                BIKClientSingletons.showWarning(I18n.wszystkiePolaSaWymagane.get());
//                return;
//            }
        BIKClientSingletons.getService().setBIAdminArchiveConfig(bean, new StandardAsyncCallback<Void>() {

            @Override
            public void onSuccess(Void result) {
                BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get());
            }
        });
//        }
    }

    @Override
    public String getId() {
        return "admin:biadmin:archiveManager";
//        return "";
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
    }

    @Override
    public boolean isPageEnabled() {
        return super.isPageEnabled() && BIKClientSingletons.isBIAdminEnabled();
    }

    @Override
    public void populateWidgetWithData(BIArchiveConfigBean data) {
        //NO-OP: nie jest uzywane
    }

    @Override
    public Widget createMainWidget() {
        TabPanel tabPanel = new TabPanel();
//        tabPanel.setAnimationEnabled(true);
        tabPanel.add(buildLogTab(), I18n.logi.get());
        tabPanel.add(buildConfigTab(), I18n.konfiguracja.get());
        tabPanel.selectTab(0);
        tabPanel.setWidth("100%");
        return tabPanel;
    }

    private IsWidget buildLogTab() {
        BIAArchiveLogPage page = new BIAArchiveLogPage();
        Widget widget = page.buildWidgets();
        page.activatePage(true, null, true);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(widget, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - BIKClientSingletons.SCROLL_PANEL_SPACING);
        return widget;
    }

    private IsWidget buildConfigTab() {
        ScrollPanel sp = new ScrollPanel();
//        sp.setHeight("500px");
        sp.add(buildWidget());
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(sp, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - BIKClientSingletons.SCROLL_PANEL_SPACING);
//        sp.setStyleName("EntityDetailsPane");
        return sp;
    }

    private void createLaunchingSection() {
        HorizontalPanel hp = new HorizontalPanel();
        createLabel(I18n.dataRozpoczecia.get(), hp);
        dbxStartDate = createDateBox(hp);
        dbxStartDate.setWidth("125px");
//        dbxStartDate.getTextBox().getElement().setAttribute("readonly", "true");
        createLabel(I18n.godzina.get() + " (hh:mm)", hp);
        tbxStartHour = createTextBox(hp);
        tbxStartHour.setWidth("50px");
        createLabel(I18n.uruchamiany.get(), hp);
        lbxFrequency = createFrequencyBox(hp);
        setHorizontalPanelAlignmentMiddle(hp);
        main.add(hp);
        //        tbxStartHour.setWidth("100px");
    }

    private void setHorizontalPanelAlignmentMiddle(HorizontalPanel hp) {
        hp.addStyleName("tableAsInlineDiv");
        for (int i = 0; i < hp.getWidgetCount(); i++) {
            hp.setCellVerticalAlignment(hp.getWidget(i), HasVerticalAlignment.ALIGN_MIDDLE);
        }
    }

    private void createRemoteSavingSection() {
        HorizontalPanel hp = new HorizontalPanel();
        chbIsRemoteSaving = createCheckBox(I18n.zdalnie.get(), new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                changeRemoteSavingState();
            }
        }, true, hp);
        createLabel(I18n.domena.get() + ":", hp);
        tbxDomain = createTextBox(hp);
        tbxDomain.setWidth("100px");
        createLabel(I18n.uzytkownik.get() + ":", hp);
        tbxUser = createTextBox(hp);
        tbxUser.setWidth("100px");
        createLabel(I18n.haslo.get() + ":", hp);
        tbxPw = createPasswordTextBox(hp);
        tbxPw.setWidth("100px");
//        chbRemoteSavingPw = createAndAddCheckBox(I18n.ukryjHaslo.get(), new ValueChangeHandler<Boolean>() {
//            @Override
//            public void onValueChange(ValueChangeEvent<Boolean> event) {
//                tbxPw.getElement().setAttribute("type" /* I18N: no */, chbRemoteSavingPw.getValue() ? "password" /* I18N: no */ : "text" /* I18N: no */);
//            }
//        }, hp);
        setHorizontalPanelAlignmentMiddle(hp);
        main.add(hp);
    }

    private void createGitSection() {
        HorizontalPanel hp = new HorizontalPanel();
        chbIsGit = createCheckBox(I18n.gitRepozytorium.get(), new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                changeGitState();
            }
        }, true);
        tbxGitRepository = createTextBox();
        createLabel(I18n.uzytkownik.get(), hp);
        tbxGitUsername = createTextBox(hp);
        tbxGitUsername.setWidth("100px");
        createLabel(I18n.haslo.get(), hp);
        tbxGitPw = createPasswordTextBox(hp);
        tbxGitPw.setWidth("100px");
//        chbGitPw = createAndAddCheckBox(I18n.ukryjHaslo.get(), new ValueChangeHandler<Boolean>() {
//            @Override
//            public void onValueChange(ValueChangeEvent<Boolean> event) {
//                tbxGitPw.getElement().setAttribute("type" /* I18N: no */, chbGitPw.getValue() ? "password" /* I18N: no */ : "text" /* I18N: no */);
//            }
//        }, hp);
        setHorizontalPanelAlignmentMiddle(hp);
        main.add(hp);
    }

    private void calcServiceToGetData() {
        BIKClientSingletons.getService().getBIAdminArchiveConfig(new StandardAsyncCallback<BIArchiveConfigBean>() {

            @Override
            public void onSuccess(BIArchiveConfigBean result) {
                chbIsArchiveActivated.setValue(result.isActive > 0);
                tbxSavedFolder.setText(result.savedFolder);
                tbxUser.setText(result.user);
                tbxPw.setText(result.pw);
                tbxDomain.setText(result.domain);
                chbIsGit.setValue(result.useGit > 0);
                chbIsRemoteSaving.setValue(result.isRemoteSaving > 0);
                tbxLcmCliPath.setText(result.lcmCliPath);
//                tbxJobCuid.setText(result.jobCuid);
                tbxLimit.setText(result.limit);
                tbxGitRepository.setText(result.gitRepository);
                tbxGitUsername.setText(result.gitUsername);
                tbxGitPw.setText(result.gitPw);
                dbxStartDate.getTextBox().setText(result.startDate);
                tbxStartHour.setText(result.startHour);
                lbxFrequency.setSelectedIndex(frequency2IdMap.get(result.frequency));
                tbxFileSize.setText(BaseUtils.safeToStringNotNull(result.fileSize));
                setWidgetsEnabled(result.isActive > 0);
                changeStateAdditionalCheckbox();
            }
        });
    }
}
