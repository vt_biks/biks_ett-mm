/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.biadmin;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.common.SAPBOObjectBean;
import static pl.fovis.client.bikpages.biadmin.BIAObjectManagerPage.KIND_TO_ICON_MAP;
import pl.fovis.client.dialogs.ReplaceDialog;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.dialogs.OneTextBoxDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class BIAChangeNameDialog extends BaseActionOrCancelDialog {

    protected IParametrizedContinuation<Set<SAPBOObjectBean>> saveCont;
    protected Set<SAPBOObjectBean> objectsToManage;
    protected Map<Integer, TextBox> textBoxMap = new HashMap<Integer, TextBox>();

    @Override
    protected String getDialogCaption() {
        return I18n.objectZmienNazwe.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zmien.get();
    }

    @Override
    protected void buildMainWidgets() {
        HorizontalPanel toolsPanel = new HorizontalPanel();
        toolsPanel.setSpacing(3);
        toolsPanel.add(new Label(I18n.wykonajDlaKazdego.get()));
        // Prefiks button
        PushButton addPrefixBtn = new PushButton(I18n.objectDodajPrefiks.get());
        NewLookUtils.makeCustomPushButton(addPrefixBtn);
        addPrefixBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new OneTextBoxDialog().buildAndShowDialog(I18n.objectDodajPrefiks.get(), null, new IParametrizedContinuation<String>() {

                    @Override
                    public void doIt(String param) {
                        if (!BaseUtils.isStrEmptyOrWhiteSpace(param)) {
                            for (TextBox value : textBoxMap.values()) {
                                value.setValue(param + value.getValue());
                            }
                        }
                    }
                });
            }
        });
        toolsPanel.add(addPrefixBtn);
        // Postfiks button
        PushButton addPostfixBtn = new PushButton(I18n.objectDodajPostfiks.get());
        NewLookUtils.makeCustomPushButton(addPostfixBtn);
        addPostfixBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new OneTextBoxDialog().buildAndShowDialog(I18n.objectDodajPostfiks.get(), null, new IParametrizedContinuation<String>() {

                    @Override
                    public void doIt(String param) {
                        if (!BaseUtils.isStrEmptyOrWhiteSpace(param)) {
                            for (TextBox value : textBoxMap.values()) {
                                value.setValue(value.getValue() + param);
                            }
                        }
                    }
                });
            }
        });
        toolsPanel.add(addPostfixBtn);
        // Replace button
        PushButton replaceBtn = new PushButton(I18n.zamien.get());
        NewLookUtils.makeCustomPushButton(replaceBtn);
        replaceBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new ReplaceDialog().buildAndShowDialog(I18n.zamien.get(), new IParametrizedContinuation<Pair<String, String>>() {

                    @Override
                    public void doIt(Pair<String, String> param) {
                        for (TextBox value : textBoxMap.values()) {
                            value.setValue(value.getValue().replace(param.v1, param.v2));
                        }
                    }
                });
            }
        });
        toolsPanel.add(replaceBtn);
        // object table
        FlexTable objectTable = new FlexTable();
        objectTable.setCellSpacing(10);

        createObjectTableHeader(objectTable);
        populateObjectTable(objectTable);

        ScrollPanel scrollTablePanel = new ScrollPanel(objectTable);
        scrollTablePanel.setHeight("500px");
        main.add(toolsPanel);
        main.add(scrollTablePanel);
    }

    protected void createObjectTableHeader(FlexTable objectTable) {
        int actualRow = 0;
        objectTable.setWidget(actualRow, 0, new HTML("<b>" + I18n.scheduleID.get() + "</b>"));
        objectTable.setWidget(actualRow, 1, new HTML("<b></b>"));
        objectTable.setWidget(actualRow, 2, new HTML("<b>" + I18n.scheduleNazwa.get() + "</b>"));
        objectTable.setWidget(actualRow, 3, new HTML("<b>" + I18n.objectNowaNazwa.get() + "</b>"));
    }

    protected void populateObjectTable(FlexTable objectTable) {
        int actualRow = 1;
        for (SAPBOObjectBean object : objectsToManage) {
            objectTable.setWidget(actualRow, 0, new Label(String.valueOf(object.id)));
            objectTable.setWidget(actualRow, 1, new Image("images/" + KIND_TO_ICON_MAP.get(object.type) + ".gif"));
            objectTable.setWidget(actualRow, 2, new Label(object.name));
            TextBox tbx = new TextBox();
            tbx.setWidth("300px");
            tbx.setValue(object.name);
            objectTable.setWidget(actualRow, 3, tbx);
            textBoxMap.put(object.id, tbx);
            actualRow++;
        }
    }

    @Override
    protected void doAction() {
        for (SAPBOObjectBean object : objectsToManage) {
            TextBox tbxDescr = textBoxMap.get(object.id);
            object.name = tbxDescr.getValue();
        }
        saveCont.doIt(objectsToManage);
    }

    public void buildAndShowDialog(Set<SAPBOObjectBean> objects, IParametrizedContinuation<Set<SAPBOObjectBean>> saveCont) {
        this.objectsToManage = objects;
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }
}
