/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.bikwidgets.SystemGroupDialog;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BIKRightRoles;
import pl.fovis.common.CustomRightRoleBean;
import pl.fovis.common.RightRoleBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.TreeNodeBranchBean;
import pl.fovis.common.UserBean;
import pl.fovis.common.ValidationExceptionBiks;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author AMamcarz
 */
public class SystemUserDialog extends BaseActionOrCancelDialog {
    
    @SuppressWarnings("unchecked")
//    protected static final Set<Pair<Integer, Integer>> MARKER_FOR_CUSTOM_ROLES_WITHOUT_SELECTOR = BaseUtils.<Pair<Integer, Integer>>paramsAsSet(new Pair<Integer, Integer>());
    public static final Set<TreeNodeBranchBean> MARKER_FOR_CUSTOM_ROLES_WITHOUT_SELECTOR = BaseUtils.<TreeNodeBranchBean>paramsAsSet(new TreeNodeBranchBean());
    protected static final String NO_USER_TEXT = "(" + I18n.brak3.get() /* I18N:  */ + ")";
    private TextBox loginName;
    private TextBox systemUserName;
    private PasswordTextBox password;
    private PasswordTextBox passwordRepeat;
    private FlexTable grid;
    protected SystemUserBean systemUser;
    private Label status;
    private IParametrizedContinuation<SystemUserBean> saveCont;
    protected CheckBox isDisabled;
    protected Map<String, CheckBox> rightRolesCB = new LinkedHashMap<String, CheckBox>();
    protected Label userSpolem;
//    protected Label userAd;
    protected Button setUser;
    private Integer userId = null;
    protected PushButton deleteBtn;
    protected PushButton editBtn;
    protected PushButton editGroupBtn;
//    protected PushButton editAdBtn;
//    protected PushButton deleteAdBtn;
    protected FlowPanel socialUserFp;
//    protected FlowPanel adUSerFp;
    protected Set<Integer> alreadyJoinedUsers = new HashSet<Integer>();
    protected CheckBox isSendMailsCb;
    //protected List<String> allUsers = new ArrayList<String>();
    protected Map<Integer, UserBean> users = new HashMap<Integer, UserBean>();
    protected Map<String, RightRoleBean> mapOfAllRightRoles = new LinkedHashMap<String, RightRoleBean>();
    protected TreesDialog authorDialog;
    protected CheckBox authorIsEnabled;
    protected int authorPanelRowNum;
    protected Map<Integer, Set<TreeNodeBranchBean>> customRightRoleUserEntries = new HashMap<Integer, Set<TreeNodeBranchBean>>();
    protected List<Integer> userGroupsIds;
    protected boolean doNotFilterHtml;
    protected final CheckBox filterHtmlBx = createCheckBoxWithStyle();
    protected boolean newRightsSys;
    
    @Override
    protected String getDialogCaption() {
        return systemUser != null ? I18n.edytujDane.get() /* I18N:  */ : I18n.dodajUzytkownika.get() /* I18N:  */;
    }
    
    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }
    
    protected Label createLabelWithStyle(String title) {
        Label tempLabel = new Label(title);
        tempLabel.setStyleName("addAttrLbl");
        return tempLabel;
    }
    
    protected CheckBox createCheckBoxWithStyle() {
        CheckBox tempCheckBox = new CheckBox();
        tempCheckBox.setStyleName("Filters-Checkbox");
        return tempCheckBox;
    }
    
    protected TextBox createTextBoxWithStyle() {
        TextBox tempTextBox = new TextBox();
        tempTextBox.setStyleName("adminTextBox");
        return tempTextBox;
    }
    
    protected PasswordTextBox createPasswordTextBoxWithStyle() {
        PasswordTextBox tempPasswordTextBox = new PasswordTextBox();
        tempPasswordTextBox.setStyleName("adminTextBox");
        return tempPasswordTextBox;
    }
    
    protected void addRowToGrid(String name, Widget w, int row) {
        addRowToGrid(name, w, row, null);
    }
    
    protected void addRowToGrid(String name, Widget w, int row, String optRowStyleName) {
        grid.setWidget(row, 0, createLabelWithStyle(name));
        if (w != null) {
            grid.setWidget(row, 1, w);
        }
        if (optRowStyleName != null) {
            grid.getRowFormatter().addStyleName(row, optRowStyleName);
        }
    }
    
    protected int addRightRolesToGrid(int row) {
        mapOfAllRightRoles = BIKClientSingletons.getMapOfAllRightRolesByCode();
        authorPanelRowNum = -1;
        
        boolean isSysAdminLoggedIn = BIKClientSingletons.isSysAdminLoggedInEx(BIKConstants.ACTION_OTHER_ACTIONS_SYS_ADMIN);
        
        for (String code : mapOfAllRightRoles.keySet()) {

            //ww: tylko SysAdmin może nadać komuś uprawnienie SysAdmin!
            // innymi słowy - AppAdmin tego zrobić nie może, może komuś nadać
            // tylko AppAdmina, Experta, itd., ale nie SysAdmina
            if (!isSysAdminLoggedIn && BIKRightRoles.isSysAdminRightRoleCode(code)) {
                continue;
            }
            
            CheckBox cb = createCheckBoxWithStyle();
            cb.setValue(false);
            rightRolesCB.put(code, cb);
            RightRoleBean rrb = mapOfAllRightRoles.get(code);
            
            FlowPanel flowPanel = new FlowPanel();
            flowPanel.add(cb);
            if (code.equals(BIKGWTConstants.USER_KIND_CODE_AUTHOR)) {
                authorPanelRowNum = row;
                flowPanel.add(createAutorEditButton());
                
                authorIsEnabled = cb;
                cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
                    @Override
                    public void onValueChange(ValueChangeEvent<Boolean> event) {
                        if (event.getValue()) {
                            createAndShowAuthorDialog();
                        }
                    }
                });
                
            }
            
            if (rrb != null) {
                //addRowToGrid(rrb.caption, cb, row++);
//                addRowToGrid(rrb.caption, flowPanel, row++);
                addRowToGrid(BIKRightRoles.getRightRoleCaptionByCode(code), flowPanel, row++);
            }
        }
        
        if (systemUser == null || systemUser.userId == null) {
            setRowVisibleInGrid(authorPanelRowNum, false);
        }
        List<CustomRightRoleBean> crrbs = BIKClientSingletons.getCustomRightRoles();
        List<CustomRightRoleBean> tempList = new ArrayList();
        if(BIKClientSingletons.newRightsSysByAppProps()){
        if (newRightsSys) {
            for (CustomRightRoleBean tempCrrb : crrbs) {
                if (tempCrrb.code.equals("Expert") || tempCrrb.code.equals("Administrator") || tempCrrb.code.equals("AppAdmin")) {
                    tempList.add(tempCrrb);
                }
            }
        }
        }else{
            tempList.addAll(crrbs);
        }
        
        for (CustomRightRoleBean crr : tempList) {
            
            if (!isSysAdminLoggedIn && BIKRightRoles.isSysAdminRightRoleCode(crr.code)) {
                continue;
            }
            
            final CustomRightRoleBean crrF = crr;
            
            final CheckBox cb = createCheckBoxWithStyle();
            cb.setValue(!BaseUtils.isCollectionEmpty(customRightRoleUserEntries.get(crr.id)));
            
            FlowPanel flowPanel = new FlowPanel();
            flowPanel.add(cb);
            
            if (!BaseUtils.isStrEmptyOrWhiteSpace(crr.treeSelector) && crr.selectorMode != 0) {
                
                cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
                    
                    @Override
                    public void onValueChange(ValueChangeEvent<Boolean> event) {
                        if (event.getValue()) {
                            showEditRoleRestrictionsSelectorDialog(cb, crrF);
                        } else {
                            customRightRoleUserEntries.remove(crrF.id);
                        }
                    }
                });
                
                flowPanel.add(createEditRoleRestrictionsButton(new IContinuation() {
                    
                    @Override
                    public void doIt() {
                        showEditRoleRestrictionsSelectorDialog(cb, crrF);
                    }
                }));
            } else {
                cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
                    
                    @Override
                    public void onValueChange(ValueChangeEvent<Boolean> event) {
                        if (event.getValue()) {
                            customRightRoleUserEntries.put(crrF.id, MARKER_FOR_CUSTOM_ROLES_WITHOUT_SELECTOR);
                        } else {
                            customRightRoleUserEntries.remove(crrF.id);
                        }
                    }
                });
            }
            
            boolean isRegularUserCrr = BaseUtils.safeEquals(crr.code, "RegularUser");
            
            cb.setVisible(!isRegularUserCrr);
            
            addRowToGrid(crr.caption, flowPanel, row++, isRegularUserCrr ? null : "customRightRole");
        }
        
        return row;
    }
    
    protected void showEditRoleRestrictionsSelectorDialog(final CheckBox hasObjectsInRoleCb, final CustomRightRoleBean crr) {
        final Set<TreeNodeBranchBean> prev = customRightRoleUserEntries.get(crr.id);
        hasObjectsInRoleCb.setValue(!BaseUtils.isCollectionEmpty(prev));
        
        new TreeSelectorForRoleRestrictionsDialog().buildAndShowDialog(crr.caption,
                BIKClientSingletons.treeSelectorToTreeCodes(crr.treeSelector), prev, new IParametrizedContinuation<Set<TreeNodeBranchBean>>() {
                    
                    @Override
                    public void doIt(Set<TreeNodeBranchBean> param) {
                        customRightRoleUserEntries.put(crr.id, param);
                        hasObjectsInRoleCb.setValue(!param.isEmpty());
                    }
                });
    }
    
    @Override
    protected void buildMainWidgets() {
        status = new Label();
        status.setStyleName("status" /* i18n: no:css-class */);
        main.add(status);
        grid = new FlexTable();
        grid.addStyleName("sysUserDialog");
        grid.setWidth("100%");
        
        int row = 0;
        addRowToGrid((BIKClientSingletons.isMultiXMode() ? I18n.email.get() : I18n.login.get()) + "(*)", loginName = createTextBoxWithStyle(), row++);
        if (BIKClientSingletons.isMultiXMode()) {
            loginName.setEnabled(false);
        }
        if (systemUser != null) {
            addRowToGrid(I18n.nazwa.get() /* I18N:  */, systemUserName = createTextBoxWithStyle(), row++);
        }
        
        password = new PasswordTextBox();
        passwordRepeat = new PasswordTextBox();
        if (!BIKClientSingletons.isMultiXMode()) {
            addRowToGrid(I18n.haslo.get() /* I18N:  */, password, row++);
            addRowToGrid(I18n.powtorzHaslo.get() /* I18N:  */, passwordRepeat, row++);
        }

        //uzytkownik spolecznosciowy
        socialUserFp = new FlowPanel();
        socialUserFp.setStyleName("Admin-fp");
        userSpolem = new Label();
        userSpolem.setStyleName("admin-lbl");
        socialUserFp.add(userSpolem);
        socialUserFp.add(createEditButton());
        socialUserFp.add(createDeleteButton());
        
        addRowToGrid(I18n.uzytkownikSpol2.get() /* I18N:  */ + ".", socialUserFp, row++);
//        adUSerFp = new FlowPanel();
//        adUSerFp.setStyleName("Admin-fp");
//        userAd = new Label();
//        userAd.setStyleName("admin-lbl");
//        adUSerFp.add(userAd);
//        adUSerFp.add(createEditAdButton());
//        adUSerFp.add(createDeleteAdButton());
//        userAd.setText(NO_USER_TEXT);
//
//        addRowToGrid(I18n.loginZAD.get() /* I18N:  */, adUSerFp, row++);
        if (!newRightsSys) {
            addRowToGrid(I18n.uprawnienia.get() /* I18N:  */, null, row++);
        }
        addRowToGrid(I18n.doNotFilterHtml.get(), createFilterHtmlBox(), row++);
        
        row = addRightRolesToGrid(row);
        
        addRowToGrid(I18n.wylaczony.get() /* I18N:  */, isDisabled = createCheckBoxWithStyle(), row++, "sysUsrDialogDisabled");
        deleteBtn.setVisible(userId != null);
        userSpolem.setText(NO_USER_TEXT);
        
        if (systemUser != null) {
            userSpolem.setText(systemUser.userName != null ? systemUser.userName : NO_USER_TEXT);
//            userAd.setText(systemUser.loginNameForAd != null ? systemUser.loginNameForAd : NO_USER_TEXT);
            loginName.setValue(BaseUtils.safeToString(systemUser.loginName));
            systemUserName.setValue(systemUser.userId != null ? systemUser.userName : BaseUtils.safeToString(systemUser.systemUserName));
            password.setValue(null);
            passwordRepeat.setValue(null);
            isDisabled.setValue(systemUser.isDisabled);
            if (systemUser.userRightRoleCodes != null) {
                for (String code : systemUser.userRightRoleCodes) {
                    rightRolesCB.get(code).setValue(true);
                }
            }
            if (systemUser.userId != null) {
                systemUserName.setEnabled(false);
            }
        }
        if (BIKClientSingletons.isSendEmailNotifications()) {
            isSendMailsCb = new CheckBox();
            isSendMailsCb.setValue(systemUser != null ? systemUser.isSendMails : true);
            addRowToGrid(I18n.wysylajPowiadomieniaMailowe.get(), isSendMailsCb, row++);
        }
        
        addRowToGrid(I18n.nalezyDoGrupy.get() /* I18N:  */, createEditGrupuButton(), row++);
        
        main.add(grid);
    }
    
    @Override
    protected void doAction() {
        throw new UnsupportedOperationException("Not supported yet" /* I18N: no */ + ".");
    }
    
    protected Map<Integer, Set<Pair<Integer, Integer>>> makeFlatCrrEntries() {
        Map<Integer, Set<Pair<Integer, Integer>>> res = new HashMap<Integer, Set<Pair<Integer, Integer>>>();
        for (Entry<Integer, Set<TreeNodeBranchBean>> e : customRightRoleUserEntries.entrySet()) {
            Set<Pair<Integer, Integer>> ents = new HashSet<Pair<Integer, Integer>>();
            for (TreeNodeBranchBean tnbb : e.getValue()) {
                ents.add(new Pair<Integer, Integer>(tnbb.treeId, tnbb.nodeId));
            }
            res.put(e.getKey(), ents);
        }
        return res;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    protected boolean doActionBeforeClose() {
        if (BIKClientSingletons.isMultiXMode()) {
            boolean oldIsEmailValid = systemUser == null || BaseUtils.isEmailValid(systemUser.loginName);
            boolean newIsEmailValid = BaseUtils.isEmailValid(loginName.getText());
            
            if (oldIsEmailValid && !newIsEmailValid) {
                status.setText(I18n.niepoprawnyAdresEmail.get());
                return false;
            }
        }
        
        if (systemUser == null && BaseUtils.isStrEmptyOrWhiteSpace(password.getValue())) {
            status.setText(I18n.hasloNieMozeBycPuste.get() /* I18N:  */);
            return false;
        }
        
        if (systemUser != null && BaseUtils.isStrEmptyOrWhiteSpace(systemUserName.getValue())) {
            status.setText(I18n.nazwaNieMozeBycPusta.get() /* I18N:  */);
            return false;
        }
        
        if (!BaseUtils.safeEquals(password.getValue(), (passwordRepeat.getValue()))) {
            status.setText(I18n.haslaNieSaTakieSame.get() /* I18N:  */);
            return false;
        } else if (loginName.getText().equals("")) {
            status.setText(I18n.poleLoginJestWymagane.get() /* I18N:  */);
            return false;
        } else {
            final SystemUserBean usr = new SystemUserBean();
            if (systemUser != null) {
                usr.id = systemUser.id;
            }
            usr.loginName = loginName.getValue();
            usr.isDisabled = isDisabled.getValue();
            usr.userRightRoleCodes = new LinkedHashSet<String>();
            for (String code : rightRolesCB.keySet()) {
                if (rightRolesCB.get(code).getValue()) {
                    usr.userRightRoleCodes.add(code);
                }
            }
            if (usr.userRightRoleCodes.isEmpty()) {
                usr.userRightRoleCodes = null;
            }
            usr.userName = userSpolem.getText();
            usr.doNotFilterHtml = doNotFilterHtml;
            if (systemUser != null) {
                usr.systemUserName = systemUserName.getValue();
            }
            if (userSpolem.getText().equals("") || userSpolem.getText().equals(NO_USER_TEXT)) {
                usr.userId = null;
            } else {
                if (alreadyJoinedUsers.contains(userId)) {
                    status.setText(I18n.uzytkownJuzPolaczonWybierzInnego.get() /* I18N:  */ + ".");
                    return false;
                }
                usr.userId = userId;
            }
            
            if (authorDialog != null) {
                Collection<Integer> authorTreeIDs = authorDialog.getCheckedTreeIds();
                Collection<Integer> creatorTreeIDs = authorDialog.getCheckedTreeIds();
                
                if (authorIsEnabled.getValue() && authorTreeIDs.isEmpty()) {
                    status.setText(I18n.proszeWybracDrzewaKtorychUzytkow.get() /* I18N:  */);
                    return false;
                }
                
                if (authorIsEnabled.getValue() && creatorTreeIDs.isEmpty()) {
                    status.setText(I18n.proszeWybracDrzewaKtorychUzytkow.get() /* I18N:  */);
                    return false;
                }
                
                if (authorTreeIDs.isEmpty()) {
                    if (usr.userAuthorTreeIDs != null) {
                        usr.userAuthorTreeIDs.clear();
                    }
                } else {
                    if (usr.userAuthorTreeIDs == null) {
                        usr.userAuthorTreeIDs = new HashSet(authorTreeIDs);
                    } else {
                        usr.userAuthorTreeIDs.clear();
                        usr.userAuthorTreeIDs.addAll(authorTreeIDs);
                    }
                }
            } else {
                if (systemUser != null) {
                    usr.userAuthorTreeIDs = systemUser.userAuthorTreeIDs;
                }
            }
            
            usr.isSendMails = BIKClientSingletons.isSendEmailNotifications() ? isSendMailsCb.getValue() : false;
            usr.usersGroupsIds = userGroupsIds;
            Map<Integer, Set<Pair<Integer, Integer>>> flatCrrEntries = makeFlatCrrEntries();
            
            final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
            infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.pleaseWait.get(), true);
            if (systemUser == null) {
                BIKClientSingletons.getService().addBikSystemUser(usr.loginName, password.getValue(), usr.isDisabled, usr.userId, usr.userRightRoleCodes,
                        flatCrrEntries, usr.usersGroupsIds, doNotFilterHtml,
                        new UserDialogCallback<Integer>("Failure on" /* I18N: no */ + " addBikSystemUser") {
                            @Override
                            public void onSuccess(Integer result) {
                                usr.systemUserName = usr.loginName;
                                usr.id = result;
                                usr.customRightRoleUserEntries = customRightRoleUserEntries;
                                saveCont.doIt(usr);
                                infoDialog.hideDialog();
                                hideDialog();
                                BIKClientSingletons.showInfo(I18n.dodanoUzytkownika.get() /* I18N:  */);
                            }
                            
                            @Override
                            public void onFailure(Throwable caught) {
                                infoDialog.hideDialog();
                                super.onFailure(caught);
                            }
                        }
                );
            } else {
                //przesyłamy wartość "int" przy nadaniu uprawnień, żeby można było rozróżnić skąd nadane uprawnienia (kon - kontekst, int - interfejs, sql - sqlką)
                BIKClientSingletons.getService().updateBikSystemUser(systemUser.id, usr.loginName, password.getValue(), usr.isDisabled, usr.userId, usr.systemUserName, usr.userRightRoleCodes,
                        usr.userAuthorTreeIDs, usr.isSendMails, flatCrrEntries, usr.usersGroupsIds, doNotFilterHtml,
                        new UserDialogCallback<Void>("Failure on" /* I18N: no */ + " updateBikSystemUser") {
                            @Override
                            public void onSuccess(Void result) {
                                SystemUserBean systemUserBean = BIKClientSingletons.getLoggedUserBean();
                                if (systemUserBean.id == systemUser.id) {
                                    if ((systemUserBean.userId == null && usr.userId != null)
                                    || (systemUserBean.userId != null && usr.userId == null)
                                    || systemUserBean.userId != usr.userId) {
                                        systemUserBean.userId = usr.userId;
                                    }
                                }
                                
                                usr.customRightRoleUserEntries = customRightRoleUserEntries;
                                saveCont.doIt(usr);
                                infoDialog.hideDialog();
                                hideDialog();
                                BIKClientSingletons.showInfo(I18n.zmodyfikowanoUzytkownika.get() /* I18N:  */);
                            }
                            
                            @Override
                            public void onFailure(Throwable caught) {
                                infoDialog.hideDialog();
                                super.onFailure(caught);
                            }
                        });
            }
            return false;
        }
    }
    
    public void buildAndShowDialog(SystemUserBean user, Set<Integer> alreadyJoinedUsers, IParametrizedContinuation<SystemUserBean> saveCont,
            boolean hasRut) {
        this.saveCont = saveCont;
        this.systemUser = user;
        userId = user != null ? user.userId : null;
//        this.allUsers = allUsers;
        this.alreadyJoinedUsers = alreadyJoinedUsers;
//        this.listKind = listKind;
        this.customRightRoleUserEntries = BaseUtils.safeNewHashMap(systemUser == null ? null : systemUser.customRightRoleUserEntries);
        this.newRightsSys = BIKClientSingletons.newRightsSysByAppProps();
        if (hasRut && systemUser == null) {
            BIKClientSingletons.getService().getRutEntries(new StandardAsyncCallback<Set<TreeNodeBranchBean>>() {
                
                @Override
                public void onSuccess(Set<TreeNodeBranchBean> result) {
                    customRightRoleUserEntries.put(BIKClientSingletons.customRightRoleRegularUserId, result);
                    SystemUserDialog.super.buildAndShowDialog();
                }
            });
        } else {
            super.buildAndShowDialog();
        }
    }
    
    protected PushButton createDeleteButton() {
        deleteBtn = new PushButton(new Image("images/trash_gray.gif" /* I18N: no */), new Image("images/trash_red.png" /* I18N: no */));
        deleteBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                new ConfirmDialog().buildAndShowDialog(I18n.czyNaPewnoUsunac.get() /* I18N:  */ + "?", new IContinuation() {
                            public void doIt() {
                                userSpolem.setText(NO_USER_TEXT);
                                systemUserName.setEnabled(true);
                                userId = null;
                                deleteBtn.setVisible(false);
                                if (authorIsEnabled != null) {
                                    authorIsEnabled.setValue(false);
                                }
                                setRowVisibleInGrid(authorPanelRowNum, false);
                            }
                        });
            }
        });
        deleteBtn.setStyleName("admin-btn");
        deleteBtn.setWidth("16px");
        deleteBtn.setTitle(I18n.usun.get() /* I18N:  */);
        return deleteBtn;
    }
    
    protected PushButton createEditButton() {
        editBtn = new PushButton(new Image("images/edit_yellow.png" /* I18N: no */), new Image("images/edit_yellow.png" /* I18N: no */));
        editBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().getBikAllUsers(
                        new StandardAsyncCallback<List<UserBean>>("Failure on" /* I18N: no */ + " getBikAllUsers") {
                            @Override
                            public void onSuccess(List<UserBean> result) {
                                
                                final Map<Integer, UserBean> users = new HashMap<Integer, UserBean>();
                                for (UserBean r : result) {
                                    users.put(r.nodeId, r);
                                }
                                TreeSelectorForNotConnectedUsers tsfjod = new TreeSelectorForNotConnectedUsers();
                                
                                tsfjod.buildAndShowDialog(0, //"Users" /* I18N: no */,
                                        new IParametrizedContinuation<TreeNodeBean>() {
                                            @Override
                                            public void doIt(TreeNodeBean param) {
                                                UserBean u = users.get(BaseUtils.nullToDef(param.linkedNodeId, param.id));
                                                userId = u.id;
                                                userSpolem.setText(u.name);
                                                systemUserName.setText(u.name);
                                                systemUserName.setEnabled(false);
                                                deleteBtn.setVisible(true);
                                                status.setText("");
                                                setRowVisibleInGrid(authorPanelRowNum, true);
                                            }
                                        }, null, false, null);
                            }
                        });
            }
        });
        editBtn.setStyleName("admin-btn");
        editBtn.setWidth("16px");
        editBtn.setTitle(I18n.edytuj.get() /* I18N:  */);
        return editBtn;
    }
    
    protected PushButton createEditGrupuButton() {
        editGroupBtn = new PushButton(new Image("images/edit_yellow.png" /* I18N: no */), new Image("images/edit_yellow.png" /* I18N: no */));
        editGroupBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                
                if (systemUser == null) {
                    new SystemGroupDialog().buildAndShowDialog(new ArrayList<Integer>(), new IParametrizedContinuation<List<Integer>>() {
                        
                        @Override
                        public void doIt(List<Integer> param) {
                            userGroupsIds = param;
                        }
                    });
                } else {
                    BIKClientSingletons.getService().getUsersGroups(systemUser.id, new StandardAsyncCallback<List<Integer>>() {
                        
                        @Override
                        public void onSuccess(List<Integer> result) {
                            new SystemGroupDialog().buildAndShowDialog(result, new IParametrizedContinuation<List<Integer>>() {
                                
                                @Override
                                public void doIt(List<Integer> param) {
                                    userGroupsIds = param;
                                    BIKClientSingletons.invalidateTabData("admin:dict:sysGroupUsers");
                                }
                            });
                        }
                    });
                }
                
            }
        });
        editGroupBtn.setStyleName("admin-btn");
        editGroupBtn.setWidth("16px");
        editGroupBtn.setTitle(I18n.edytuj.get() /* I18N:  */);
        return editGroupBtn;
    }
    
    protected PushButton createAutorEditButton() {
        return createEditRoleRestrictionsButton(new IContinuation() {
            
            @Override
            public void doIt() {
                createAndShowAuthorDialog();
            }
        });
    }
    
    protected PushButton createEditRoleRestrictionsButton(final IContinuation onClick) {
        PushButton editorEditBtn;
        Image img = new Image("images/edit_yellow.png" /* I18N: no */);
        editorEditBtn = new PushButton(img, img);
        editorEditBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                onClick.doIt();
            }
        });
        
        editorEditBtn.setStyleName("admin-btn");
        editorEditBtn.setWidth("16px");
        editorEditBtn.setTitle(I18n.edytuj.get() /* I18N:  */);
        return editorEditBtn;
    }
    
    protected void createAndShowAuthorDialog() {
        if (authorDialog == null) {
            authorDialog = new TreesDialog(systemUser.userAuthorTreeIDs) {
                @Override
                protected void doAction() {
                    authorIsEnabled.setValue(!getCheckedTreeIds().isEmpty());
                }
            };
            authorDialog.buildAndShowDialog();
        } else {
            authorDialog.showDialog();
        }
        
    }
    
    protected void setRowVisibleInGrid(int romNum, boolean visible) {
        if (romNum > 0 && romNum < grid.getRowCount()) {
            grid.getRowFormatter().setVisible(romNum, visible);
        }
    }
    
    protected Widget createFilterHtmlBox() {
        if (systemUser != null && systemUser.doNotFilterHtml) {
            filterHtmlBx.setValue(true);
        }
        filterHtmlBx.addClickHandler(new ClickHandler() {
            
            @Override
            public void onClick(ClickEvent event) {
                if (filterHtmlBx.getValue() == true) {
                    doNotFilterHtml = true;
                } else {
                    doNotFilterHtml = false;
                }
            }
        });
        return filterHtmlBx;
    }

//    private Widget createEditAdButton() {
//        editAdBtn = new PushButton(new Image("images/edit_yellow.png" /* I18N: no */), new Image("images/edit_yellow.png" /* I18N: no */));
//        editAdBtn.addClickHandler(new ClickHandler() {
//            @Override
//            public void onClick(ClickEvent event) {
//            }
//        });
//        editAdBtn.setStyleName("admin-btn");
//        editAdBtn.setWidth("16px");
//        editAdBtn.setTitle(I18n.edytuj.get() /* I18N:  */);
//        return editAdBtn;
//    }
//
//    private IsWidget createDeleteAdButton() {
//        deleteAdBtn = new PushButton(new Image("images/edit_yellow.png" /* I18N: no */), new Image("images/edit_yellow.png" /* I18N: no */));
//        deleteAdBtn.addClickHandler(new ClickHandler() {
//            @Override
//            public void onClick(ClickEvent event) {
//            }
//        });
//        deleteAdBtn.setStyleName("admin-btn");
//        deleteAdBtn.setWidth("16px");
//        deleteAdBtn.setTitle(I18n.edytuj.get() /* I18N:  */);
//        return deleteAdBtn;
//    }
    private abstract class UserDialogCallback<T> extends StandardAsyncCallback<T> {
        
        public UserDialogCallback(String standardErrMessage) {
            super(standardErrMessage);
        }
        
        @Override
        public void onFailure(Throwable caught) {
            if (caught instanceof ValidationExceptionBiks) {
                status.setText(caught.getMessage());
            } else {
                super.onFailure(caught);
            }
        }
    }
}
