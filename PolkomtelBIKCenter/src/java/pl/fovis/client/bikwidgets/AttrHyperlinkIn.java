/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog.AttributeType;
import pl.fovis.client.dialogs.TreeSelectorForLinkedNodesDialog;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.JoinedObjBasicGenericInfoBean;
import pl.fovis.common.JoinedObjBasicInfoBean;
import pl.fovis.common.JoinedObjMode;
import pl.fovis.common.TreeNodeBean;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class AttrHyperlinkIn extends TreeSelectorForLinkedNodesDialog /* implements IAttrWidget*/ {

//    protected Map<Integer, JoinedObjMode> directOverride;//= new HashMap<IDT, JoinedObjMode>();
    protected Map<String, List<TreeNodeBean>> allItems;
//    protected Integer treeId;
    protected String attrType;
    protected TextBox tbDescription;
    protected String selectedText;
    protected String atrName;
    protected boolean isNodeFromDynamicTree;

    public AttrHyperlinkIn(String selectedText,
            Map<String, List<TreeNodeBean>> allItems, Integer treeId, String attrType, String atrName,
            boolean isNodeFromDynamicTree,Integer nodeKindId) {
//        this.directOverride = directOverride;
        this.allItems = allItems;
        this.treeId = treeId;
        this.selectedText = selectedText;
        this.atrName = atrName;
        this.nodeKindId=nodeKindId;
        this.isNodeFromDynamicTree = isNodeFromDynamicTree;

    }

//    @Override
    public Widget getWidget() {
        VerticalPanel vp = new VerticalPanel();
//            if (AttributeType.hyperlinkInMulti.name().equals(attrType)) {
        directOverride.clear();
        if (allItems != null && !allItems.isEmpty()) {
            List<TreeNodeBean> tnbs = allItems.get(selectedText);
            if (!BaseUtils.isCollectionEmpty(tnbs)) {
                for (TreeNodeBean tb : tnbs) {
                    directOverride.put(tb.id, JoinedObjMode.Single);
                }
            }
        }

//            }
        if (treeId == null) {
            treeId = 0;
        }

        buildTreeMainWidgets(vp);
        if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.hyperlinkInMono.name().equals(attrType)) {
            bikTree.addSelectionChangedHandler(new IParametrizedContinuation<Map<String, Object>>() {
                public void doIt(Map<String, Object> param) {
                    tbDescription.setText(bikTree.getCurrentNodeId() + "_" + bikTree.getCurrentNodeBean().treeCode + "," + bikTree.getCurrentNodeBean().name);
                }
            });
        }
        vp.setStyleName("richTextArea");
        vp.setWidth("100%");

        return vp;
    }

    public String getValue() {
        String attrDescription = null;
        if (AttributeType.hyperlinkInMono.name().equals(attrType)) {
            attrDescription = tbDescription.getText();
        } else if (AttributeType.hyperlinkInMulti.name().equals(attrType)
                ||AttributeType.permissions.name().equals(attrType)
                /* || AttributeType.hyperlinkInMono.name().equals(attrType)*//*&& isNodeFromDynamicTree*/) {

            String txt = "";
            for (Entry<Integer, JoinedObjMode> e : directOverride.entrySet()) {
                TreeNodeBean node = bikTree.getTreeStorage().getBeanById(e.getKey());
                //  if (node != null) {
                if (node != null) {
                    if (e.getValue().equals(JoinedObjMode.Single)) {
                        txt = txt + node.id + "_" + node.treeCode + "," + node.name.replace("|", "I");
                        if (!BIKConstants.METABIKS_ATTR_ORIGINAL.equals(atrName)) {
                            txt = txt + "|";
                        }
                    }
                } else if (node == null) {
                    for (TreeNodeBean tb : allItems.get(atrName)) {
                        if (tb != null && tb.id == e.getKey()) {
                            txt = txt + tb.id + "_" + tb.treeCode + "," + tb.name.replace("|", "I");
                            if (!BIKConstants.METABIKS_ATTR_ORIGINAL.equals(atrName)) {
                                txt = txt + "|";
                            }
                        }
                    }
                }
            }
            attrDescription = txt;
        }
        return attrDescription;
    }

    protected JoinedObjBasicGenericInfoBean getJoinedObjBasicInfo(Integer dstNodeId) {
        if (allItems == null || allItems.isEmpty()) {
            return null;
        }
        JoinedObjBasicInfoBean job = null;
//        int selectedIdx = listBox.getSelectedIndex() >= 0 ? listBox.getSelectedIndex() : 0;
//        String selectedText = listBox.getItemText(selectedIdx);

        List<TreeNodeBean> tbs = allItems.get(selectedText);
        if (tbs != null && !tbs.isEmpty()) {
            for (TreeNodeBean tb : tbs) {
                if (tb.id == dstNodeId) {
                    job = new JoinedObjBasicInfoBean();
                    job.srcId = nodeId;
                    job.dstId = tb.id;
                    job.branchIds = tb.branchIds;
                    job.dstName = tb.name;
                    job.type = false;
                    job.inheritToDescendants = false;
                }
            }
        }

        return job;
    }

    protected void buildAlreadyJoinedObjIds() {

        if (allItems != null && !allItems.isEmpty()) {
//            int selectedIdx = listBox.getSelectedIndex() >= 0 ? listBox.getSelectedIndex() : 0;
//            String selectedText = listBox.getItemText(selectedIdx);

            List<TreeNodeBean> tbs = allItems.get(selectedText);
            if (tbs != null && !tbs.isEmpty()) {
                for (TreeNodeBean tb : tbs) {
                    alreadyJoinedObjIds.add(tb.id);
                }

                for (Integer id : alreadyJoinedObjIds) {
                    JoinedObjBasicGenericInfoBean jobi = getJoinedObjBasicInfo(id);
                    splitBranchIdsIntoColl(jobi.branchIds, true, ancestorIdsOfJoinedObjs);
                }
            }
        }
    }

    protected boolean isInMultiSelectMode() {
        if (treeId == null) {
            return true;
        }
        return AttributeType.hyperlinkInMulti.name().equals(attrType)
        ||AttributeType.permissions.name().equals(attrType);
    }

    @Override
    protected boolean isGrouppingByMenuEnabled() {
        return false;
    }

    protected boolean isStandardJoinedObjMode() {
        return false;
    }

    @Override
    protected boolean isNodeKindIdInLinkedKind(TreeNodeBean tb) {

        return getNodeKindId() == null ? false : !tb.isFixed;
    }

    @Override
    protected Integer getNodeKindId() {
        return isNodeFromDynamicTree ? nodeKindId : null;
    }

    protected String getRelationType() {
        return BIKConstants.HYPERLINK;
    }
}
