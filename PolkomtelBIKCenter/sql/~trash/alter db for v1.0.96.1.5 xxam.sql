﻿exec sp_check_version '1.0.96.1.5';
go
----------------------------------------------------------------------------------------------------------	
-----------------------------------------------------	-----------------------------------------------------	
if exists (select * from sys.objects where object_id = object_id(N'aaa_additional_descr') and type in (N'U'))
	drop table aaa_additional_descr
go
-----------------------------------------------------	-----------------------------------------------------	
if exists (select * from sys.objects where object_id = object_id(N'aaa_attribute') and type in (N'U'))
	drop table aaa_attribute
go


--Usuwanie zbiorowo kluczy obcych

declare @count int = 1;
while @count>0
begin

		exec N'alter table ' + @tableName + N' drop constraint ' + (  
																	select top(1) name
																	from sys.foreign_keys 
																	where parent_object_id = object_id(@tableName));
		set @count = @@rowcount
end




----------------------------------------------------------------------------------------------------------	
-----------------------------------------------------	-----------------------------------------------------	
exec sp_update_version '1.0.96.1.5', '1.0.96.2';
go