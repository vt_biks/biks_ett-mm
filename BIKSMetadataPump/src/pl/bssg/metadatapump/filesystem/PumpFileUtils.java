/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.filesystem;

import commonlib.LameUtils;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;

/**
 *
 * @author ctran
 */
public class PumpFileUtils {

    public static final String SMB_PROTOCOL = "smb:";

    public static String fixPathIfNessecary(String remoteFilePath) {
        if (!BaseUtils.isStrEmptyOrWhiteSpace(remoteFilePath)
                && (remoteFilePath.startsWith("/") || remoteFilePath.startsWith("\\"))
                && !remoteFilePath.toLowerCase().startsWith(SMB_PROTOCOL)) {
            return "smb:" + remoteFilePath; // for example: smb://fandoplex/kosz/file.txt
//            return "file:" + remoteFilePath; // for example: smb://fandoplex/kosz/file.txt file:///C:/Users/ctran/Downloads/
        }
        return remoteFilePath;
    }

    public static String getOriginalPath(String smbFilePath) {
        if (!BaseUtils.isStrEmptyOrWhiteSpace(smbFilePath) && smbFilePath.startsWith(SMB_PROTOCOL)) {
            smbFilePath = smbFilePath.substring(4);
        }
        return smbFilePath != null ? smbFilePath.replace("/", "\\") : null;
    }

    public static String write2TempFile(String dirForUpload, String fileExt, IFileDelegator file2Download) throws IOException {
        return LameUtils.write2TempFile(dirForUpload, fileExt, file2Download.getInputStream());
    }

    public static void write2SmbFile(NtlmPasswordAuthentication auth, String smbFilePath, InputStream inputStream) throws IOException {
        SmbFile smbFile = new SmbFile(smbFilePath, auth);
        LameUtils.copy2File(smbFile.getOutputStream(), inputStream);
    }

    public static InputStream readFileAsInputStream(String filePath, boolean isRemote, NtlmPasswordAuthentication optAuth) {
        try {
            if (isRemote) {
                SmbFile file = new SmbFile(fixPathIfNessecary(filePath), optAuth);
                return new SmbFileInputStream(file);
            } else {
                return new FileInputStream(filePath);
            }
        } catch (Exception ex) {
            throw new LameRuntimeException("File " + filePath + " does not exists or can't read this file:" + ex.getMessage());
        }
    }

    public static String loadAsString(String sourceFilePath, boolean isRemote, NtlmPasswordAuthentication optAuth) {
        return LameUtils.loadAsString(readFileAsInputStream(sourceFilePath, isRemote, optAuth), "*Cp1250,ISO-8859-2,UTF-8");
    }
}
