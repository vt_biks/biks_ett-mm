/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.filesystem;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import simplelib.LameRuntimeException;

/**
 *
 * @author ctran
 */
public class LocalFileDelegator implements IFileDelegator {

    public File file;

    public LocalFileDelegator(String path) {
        this(new File(path));
    }

    public LocalFileDelegator(File file) {
        this.file = file;
    }

    @Override
    public boolean isDirectory() {
        return file.isDirectory();
    }

    @Override
    public Integer getFileCount() {
        return file.list().length;
    }

    @Override
    public IFileDelegator[] listFiles() {
        return convert2Array(file.listFiles());
    }

    @Override
    public String getPath() {
        return file.getPath();
    }

    @Override
    public String getFolderName() {
        return file.getName();
    }

    @Override
    public IFileDelegator[] listFiles(String wildcard) {
        FileFilter fileFilter = new WildcardFileFilter(wildcard);
        return convert2Array(file.listFiles(fileFilter));
    }

    @Override
    public long lastModified() {
        return file.lastModified();
    }

    @Override
    public String getName() {
        return file.getName();
    }

    @Override
    public long length() {
        return file.length();
    }

    @Override
    public InputStream getInputStream() {
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    private IFileDelegator[] convert2Array(File[] listFiles) {
        IFileDelegator[] ret = new IFileDelegator[listFiles.length];
        for (int i = 0; i < listFiles.length; i++) {
            ret[i] = new LocalFileDelegator(listFiles[i]);
        }
        return ret;
    }
}
