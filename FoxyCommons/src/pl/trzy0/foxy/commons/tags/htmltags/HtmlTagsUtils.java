/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.tags.htmltags;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.trzy0.foxy.commons.tags.TagData;
import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.MapHelper;

/**
 *
 * @author wezyr
 */
public class HtmlTagsUtils {

    public static List<IHtmlTag> createTagsFromCompactMaps(final String fileName,
            Collection<Map<String, Object>> maps) {
        return createTagsFromCompactMapsInner(fileName, null, maps);
    }

    protected static List<IHtmlTag> createTagsFromCompactMapsInner(final String fileName,
            final IHtmlTag parentTag,
            Collection<Map<String, Object>> maps) {
        if (maps == null || maps.size() == 0) {
            return null;
        }

        return BaseUtils.projectToList(maps, new Projector<Map<String, Object>, IHtmlTag>() {

            public IHtmlTag project(Map<String, Object> val) {
                MapHelper<String> mh = new MapHelper<String>(val);
                return createTagFromCompactMapInner(fileName, parentTag, mh);
            }
        });
    }

    public static IHtmlTag createTagFromCompactMap(String fileName, MapHelper<String> mh) {
        return createTagFromCompactMapInner(fileName, null, mh);
    }

    @SuppressWarnings("unchecked")
    public static IHtmlTag createTagFromCompactMapInner(String fileName,
            IHtmlTag parentTag,
            MapHelper<String> mh) {
        String name = mh.getString("n");
        boolean isTextTag = mh.getInt("t") != 0;
        Map<String, String> attrs = (Map) mh.getMap("a");
        boolean isOpenTag = mh.getInt("o") != 0;
        int levelDelta = isOpenTag ? 1 : 0;

        TagData tagData = new TagData(name, isTextTag, attrs, levelDelta, 0, 0, fileName);

        IHtmlTag tag = HtmlTagLibrary.createHtmlTag(parentTag, tagData);

        List<IHtmlTag> subTags = createTagsFromCompactMapsInner(fileName, tag, mh.getCollection("s"));

        tag.setSubTags(subTags);

        return tag;
    }

    public static List<Map<String, Object>> tagsToCompactMaps(Collection<IHtmlTag> tags) {
        if (tags == null || tags.size() == 0) {
            return null;
        }
        return BaseUtils.projectToList(tags, new Projector<IHtmlTag, Map<String, Object>>() {

            public Map<String, Object> project(IHtmlTag val) {
                return tagToCompactMap(val);
            }
        });
    }

    public static Map<String, Object> tagToCompactMap(IHtmlTag tag) {
        Map<String, Object> res = new HashMap<String, Object>();

        res.put("n", tag.getName());

        if (tag.isTextTag()) {
            res.put("t", 1);
        }

        if (!tag.isTextTag() && !tag.isClosedInPlace()) {
            res.put("o", 1);
        }

        if (tag.getAttrs() != null && tag.getAttrs().size() > 0) {
            res.put("a", tag.getAttrs());
        }

        List<Map<String, Object>> compactSubTags = tagsToCompactMaps(tag.getSubTags());
        if (compactSubTags != null) {
            res.put("s", compactSubTags);
        }

        return res;
    }

    public static void renderHtmlTags(Collection<IHtmlTag> tags, StringBuilder sb) {
        if (tags == null) {
            return;
        }
        for (IHtmlTag tag : tags) {
            renderHtmlTag(tag, sb);
        }
    }

    public static void renderHtmlTag(IHtmlTag tag, StringBuilder sb) {
        if (tag.isTextTag()) {
            sb.append(tag.getName());
        } else {
            Map<String, String> attrsOverride = tag.getAttributes();
            String innerHtml = BaseUtils.safeMapGet(attrsOverride, null);

            sb.append("<").append(tag.getName());
            Map<String, String> attrs = tag.getAttrs();

            if (!BaseUtils.isMapEmpty(attrsOverride) && !BaseUtils.isMapEmpty(attrs)) {
                Map<String, String> attrs2 = new HashMap<String, String>(attrs);
                attrs2.putAll(attrsOverride);
                attrs = attrs2;
            } else if (!BaseUtils.isMapEmpty(attrsOverride)) {
                attrs = attrsOverride;
            }

            BaseUtils.renderHtmlAttrs(attrs, sb);
            if (tag.isClosedInPlace() && innerHtml == null) {
                sb.append(" />");
            } else {
                sb.append(">");
                if (innerHtml != null) {
                    sb.append(innerHtml);
                } else {
                    renderHtmlTags(tag.getSubTags(), sb);
//                    Collection<IHtmlTag> subTags = tag.getSubTags();
//                    if (subTags != null) {
//                        for (IHtmlTag subTag : subTags) {
//                            renderHtmlTag(subTag, sb);
//                        }
//                    }
                }
                sb.append("</").append(tag.getName()).append(">");
            }
        }
    }
}
