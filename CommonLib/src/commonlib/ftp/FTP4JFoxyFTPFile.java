/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.ftp;

import it.sauronsoftware.ftp4j.FTPFile;

/**
 *
 * @author pmielanczuk
 */
public class FTP4JFoxyFTPFile implements IFoxyFTPFile {

    protected FTPFile file;

    public FTP4JFoxyFTPFile(FTPFile file) {
        this.file = file;
    }

    @Override
    public String getName() {
        return file.getName();
    }

    @Override
    public boolean isDirectory() {
        return file.getType() == FTPFile.TYPE_DIRECTORY;
    }
    
    @Override
    public long getSize() {
        return file.getSize();
    }
}
