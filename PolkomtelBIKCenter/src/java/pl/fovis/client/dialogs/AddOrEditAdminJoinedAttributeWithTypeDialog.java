/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.TextBox;
import java.util.List;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class AddOrEditAdminJoinedAttributeWithTypeDialog extends AddOrEditAdminAttributeWithTypeDialog {

    protected List<AttributeBean> actualJoinedAttributes;
    protected TextBox tbValueFrom, tbValueTo;

    public void buildAndShowDialog(String dialogCaption, AttributeBean ab, List<AttributeBean> actualJoinedAttributes, IParametrizedContinuation<AttributeBean> saveCont) {
        this.actualJoinedAttributes = actualJoinedAttributes;
        this.saveCont = saveCont;
        this.ab = ab;
        this.name = dialogCaption;
        super.buildAndShowDialog();
    }

    public static enum AttributeType {

        definedValue(I18n.zdefiniowanaWartość.get()),
        shortText(I18n.krotkiTekst.get() /* I18N:  */),
        data(I18n.data.get() /* I18N:  */),
        combobox(I18n.poleJednokrotnegoWyboru.get() /* I18N:  */),
        checkbox(I18n.poleWielokrotnegoWyboru.get() /* I18N:  */);
        public String caption;

        AttributeType(String c) {
            this.caption = c;
        }
    }

    @Override
    protected void fillAttrLbx(boolean isEditAttr) {
        int i = 0;
        for (AttributeType e : AttributeType.values()) {
            attrTypeListBox.addItem(e.caption);
            if (isEditAttr && (e.name()).equals(ab.typeAttr.trim())) {
                attrTypeListBox.setSelectedIndex(i);
            }
            nameToValAT.put(e.caption, e.name());
            i++;
        }
    }

    @Override
    protected boolean isAttributeInCategory() {
        return false;
    }

    @Override
    protected boolean isAttributeAlreadyExists() {

        if (getSelectedAttrType().equals(AttributeType.definedValue.name())) {
            if (BaseUtils.isStrEmptyOrWhiteSpace(tbValueFrom.getText())) {
                errorMssg.setHTML(I18n.podajWartoscDlaWezlaZrodlowego.get() /* I18N:  */ + ".");
            }
            if (BaseUtils.isStrEmptyOrWhiteSpace(tbValueTo.getText())) {
                errorMssg.setHTML(I18n.podajWartoscDlaWezlaDocelowego.get() /* I18N:  */ + ".");
            }
        }
        String newCaption = tbValue.getValue();
        if (ab == null || !ab.atrName.equals(newCaption)) {
            for (AttributeBean attrHintBean : actualJoinedAttributes) {
                String newCaptionLowerCased = newCaption.toLowerCase();
                String attrName = attrHintBean.atrName;
                //ww: tłumaczenie tu nie może być, bo nie tłumaczymy atr. dodatkowych!
//                String attrNameTranslated = BIKClientSingletons.getTranslatedAttributeName(attrName);
                if (attrName.toLowerCase().equals(newCaptionLowerCased)) {
                    errorMssg.setHTML(I18n.atrybutOPodanejNazwieJuzIstnieje.get() /* I18N:  */ + ".");
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected boolean isShortTextType(String name) {
        return false;
    }

    @Override
    protected void optTxbToDefinedValue(String valueFrom, String valueTo) {
        optVpToDefinedValue.setHeight("50px");
        optVpToDefinedValue.add(addHTMLAndsetStyle(I18n.wartoscDlaWezlaZrodlowego.get()));
        tbValueFrom = addTextBoxAndsetStyle(valueFrom);
        optVpToDefinedValue.add(tbValueFrom);
        optVpToDefinedValue.add(addHTMLAndsetStyle(I18n.wartoscDlaWezlaDocelowego.get()));
        tbValueTo = addTextBoxAndsetStyle(valueTo);
        optVpToDefinedValue.add(tbValueTo);
        setValueOptVisible();
        main.add(optVpToDefinedValue);
    }

    @Override
    protected void setValueOptVisible() {
        optVpToDefinedValue.setVisible(getSelectedAttrType().equals(AttributeType.definedValue.name()));
    }

    protected void optDoAction(AttributeBean atrb) {
        if (getSelectedAttrType().equals(AttributeType.definedValue.name())) {
            atrb.valueOpt = tbValueFrom.getText();
            atrb.valueOptTo = tbValueTo.getText();
        }
    }

}
