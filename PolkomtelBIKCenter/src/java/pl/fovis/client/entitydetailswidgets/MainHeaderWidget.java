/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.HistoryManager;
import pl.fovis.client.ICheckEnabledByDesign;
import pl.fovis.client.bikpages.IBikTreePage;
import pl.fovis.client.dialogs.AddParentJoinedObjs2Node;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.client.dialogs.EditCrrLabelsDialog;
import pl.fovis.client.dialogs.HelpDialog;
import pl.fovis.client.dialogs.LuceneViewAndJoinSimilarNodesDialog;
import pl.fovis.client.dialogs.ShowAssignedUsers4NodeDialog;
import pl.fovis.client.dialogs.ShowAssociationDialog;
import pl.fovis.client.dialogs.ViewHTMLInFullScreenDialog;
import pl.fovis.client.entitydetailswidgets.dqm.DQCTestWidget;
import pl.fovis.client.entitydetailswidgets.dqm.DQMDataErrorIssueWidget;
import pl.fovis.client.entitydetailswidgets.dqm.DQMEvaluationWidget;
import pl.fovis.client.entitydetailswidgets.dqm.DQMTestWidget;
import pl.fovis.client.entitydetailswidgets.dqm.DataQualityTestGroupWidget;
import pl.fovis.client.entitydetailswidgets.dqm.SubRowsExtradataWidgetForDqmAtrybutyJakosciDanych;
import pl.fovis.client.entitydetailswidgets.lisa.LisaDictionaryContentDisplayedByDataGrid;
import pl.fovis.client.entitydetailswidgets.lisa.LisaDictionaryMetadataContent;
import pl.fovis.client.nodeactions.NodeActionJumpToLinkedNode;
import pl.fovis.common.ArticleBean;
import pl.fovis.common.AuthorBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BlogBean;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.HelpBean;
import pl.fovis.common.LinkedAlsoBean;
import pl.fovis.common.RightRoleBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.PushButtonWithHideText;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.ViewURLLink;
import pl.fovis.foxygwtcommons.VisualMetricsUtils;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author wezyr tu trzeba porządek zrobić zadanie 224 z nieoficialnej listy
 */
public class MainHeaderWidget extends EntityDetailsWidgetFlatBase implements IMainHeaderSharedTable {

    public static final String ACTION_DOWNLOAD_DOCUMENT = "DownloadDocument";
    public static final String ACTION_SHOW_DATE_AND_AUTHOR = "ShowDateAndAuthor";
    public static final String ACTION_SHOW_REFRESH_PAGE_IN_CONFLUENCE = "#ShowRefreshPageInConfluence";
    public static final String ACTION_EDIT_CRR_LABELS = "+EditCrrLabels";
    public static final String FILE_PROTOCOL = "file:";

    protected IBikTreePage bikPage;
    protected FlowPanel vp;
    protected VerticalPanel titleVp;
    protected HTML descr;
    protected String properDescr;
    protected HTMLPanel htmlPanel;
    protected HTML version;
    protected HTML titleLbl;
    protected HorizontalPanel mainHp;
    protected VerticalPanel mainVp;
    protected NodeActionJumpToLinkedNode linkedNodeAction;
//    protected BikCustomButton linkedNodeBtn;
    protected PushButtonWithHideText linkedNodeBtn;
    protected /* FlowPanel*/ HorizontalPanel mainLbl;
    protected FlowPanel bottomFp;
//    protected HorizontalPanel titleHpp;
//    protected FlowPanel bizDefLb1;
    protected FlowPanel bottomLbl;
//    protected FlowPanel space;
    protected FlowPanel innerLinksGlos;
    protected FlowPanel pathAncestorLbl;
//    protected HTML author;
    protected HTML dateAdded = new HTML();
    protected HTML author = new HTML();
    protected HTML lookAttachments;
    protected FlowPanel conUniv;
//    protected FlowPanel table;
    protected FlowPanel attributesFP;
    protected FlowPanel attributesBeforeDescrFP;
    protected FlowPanel childrenExtradata;
    protected FlowPanel childrenExtradataPB;
    protected FlowPanel genericObjectsExtradata;
    protected FlowPanel teradataExtradata;
    protected FlowPanel dqmTestExtradata;
    protected FlowPanel dqmEvaluationExtradata;
    protected FlowPanel dqmDataErrorIssueExtradata;
    protected FlowPanel dqcTestExtradata;
    protected FlowPanel dqcTestGroupExtradata;
    protected FlowPanel sapbwExtradata;
    protected FlowPanel userExtradata;
    protected FlowPanel sapBoScheduleExtradata;
    protected FlowPanel lisaDictionary;
    protected FlowPanel lisaDictionaryMetadata;
    protected FlowPanel mssqlExtradata;
    protected FlowPanel dbDefinitionExtradata;
    protected FlowPanel mssqlColumnExtradata;
    protected FlowPanel postgresExtradata;
    protected FlowPanel oracleExtradata;
    protected FlowPanel dataModel;
    protected FlowPanel profileExtradata;
    //    protected HTML userKindLbl;
//    protected HTML userDataLbl;
//    protected HTML userDescrLbl;
//    protected Image userAvatarLbl;
    protected List<IEntityDetailsWidget> subWidgets = new ArrayList<IEntityDetailsWidget>();
    protected BikCustomButton deleteBtn;
    protected Label label;
    protected VerticalPanel vpsp;
    protected IEntityDetailsWidget reportConnUnivWidget;
//    protected BikCustomButton nodeKindHelpBtn;
    protected PushButtonWithHideText nodeKindHelpBtn;
    protected PushButtonWithHideText lookupDescrBtn;
    protected PushButtonWithHideText showAssociationBtn;
    protected PushButtonWithHideText addParentJoinedObjsBtn;
    protected PushButtonWithHideText rightsBtn;
//    protected BikCustomButton lookupDescrBtn;
    private IEntityDetailsWidget attributeWidgetBeforeDescr;
    private IEntityDetailsWidget attributeWidget;
    private IEntityDetailsWidget childrenExtradataWidget;
    private IEntityDetailsWidget childrenExtradataWidgetPB;
    private IEntityDetailsWidget genericNodeObjectsWidget;
    private IEntityDetailsWidget teradataWidget;
    private IEntityDetailsWidget mssqlWidget;
    private IEntityDetailsWidget dbDefinitionWidget;
    private IEntityDetailsWidget mssqlColumnWidget;
    private IEntityDetailsWidget postgresWidget;
    private IEntityDetailsWidget oracleWidget;
    private IEntityDetailsWidget dqcTestWidget;
    private IEntityDetailsWidget dqmTestWidget;
    private IEntityDetailsWidget dqmEvaluationWidget;
    private IEntityDetailsWidget dqmDataErrorIssueWidget;
    private IEntityDetailsWidget dqcTestGroupWidget;
    private IEntityDetailsWidget sapbwWidget;
    private IEntityDetailsWidget userExtradataWidget;
    private IEntityDetailsWidget sapBoScheduleWidget;
    private IEntityDetailsWidget lisaDictContentWidget;
    private IEntityDetailsWidget lisaDictMetadataContentWidget;
    private IEntityDetailsWidget profileWidget;
    private IEntityDetailsWidgetWithContlolWiget dataModelWidget;
    protected VerticalPanel defVp = new VerticalPanel();
    protected ScrollPanel scPanelDef = new ScrollPanel(defVp);
    protected HTML pathAncestor;
//    protected int row = 0;
//    protected FlexTable ft;// =new FlexTable();
    protected MainHeaderSharedTable ftImpl;// =new FlexTable();
    protected MainHeaderSharedTable ft0Impl;// =new FlexTable();
    protected MainHeaderSharedTable ftImplAttr;// =new FlexTable();
    protected HTML linkedToReportBOHtml;
    protected HTML linkedToPageInConfluence;
    protected HTML refreshPageInConfluence;
    protected HorizontalPanel titleHp = new HorizontalPanel();
    protected IContinuation refreshContinuation;
    protected Map<Integer, Pair<String, String>> innerLinks;
//    protected PushButtonWithHideText eksportBtn;

    protected Widget addAndBuildSubWidget(IEntityDetailsWidget subWidget) {
        subWidget.setup(widgetsActive, treeBean);
        subWidgets.add(subWidget);

        Widget res = subWidget.buildWidgets();

        return res;
    }

    @Override
    protected void extractAndAssignExtraPropsFromFetchedData() {
        for (IEntityDetailsWidget edw : subWidgets) {
            // tf: niepotrzebne jest cddb
            edw.dataFetched(ctx, getData(), null/*cddb*/);
        }
    }

    private static native void writeToDocument(Document doc, String html) /*-{
     doc.open();
     doc.write(html);
     doc.close();
     }-*/;

    public static native int getDocHeight(Document D) /*-{
     //var D = document;
     return Math.max(
     D.body.scrollHeight, D.documentElement.scrollHeight,
     D.body.offsetHeight, D.documentElement.offsetHeight,
     D.body.clientHeight, D.documentElement.clientHeight
     );
     }-*/;

    public static native int getDocWidth(Document D) /*-{
     //var D = document;
     return Math.max(
     D.body.scrollWidth, D.documentElement.scrollWidth,
     D.body.offsetWidth, D.documentElement.offsetWidth,
     D.body.clientWidth, D.documentElement.clientWidth
     );
     }-*/;

    public static void initFrameOnLoadViaGWT(IFrameElement frame) {
        final Document frameDoc = frame.getContentDocument();
        //ww: odejmujemy tutaj 20px na ewentualny scroll pionowy
        // pewnie to powinno być zrobione lepiej - sprytniej, ale nie chcę
        // się teraz nad tym głowić
        frame.setAttribute("width", (getDocWidth(frameDoc) - 20) + "px");
        frame.setAttribute("height", getDocHeight(frameDoc) + "px");
    }

    public static native void exportInitFrameOnLoad() /*-{
     $wnd.initFrameOnLoad = $entry(@pl.fovis.client.entitydetailswidgets.MainHeaderWidget::initFrameOnLoadViaGWT(Lcom/google/gwt/dom/client/IFrameElement;));
     }-*/;

    static {
        exportInitFrameOnLoad();
    }

    protected void showEditPrivilegeLabelsDialog() {
        getService().getCrrLabelCodes(new StandardAsyncCallback<Set<String>>() {

            @Override
            public void onSuccess(Set<String> result) {
                new EditCrrLabelsDialog().buildAndShowDialog(result, nodeId, getData().crrLabels);
            }
        });
    }

    protected HTML createOpenRawFileLink(final String protocol, String link) {
//        link = protocol + link.replace("\\", "/");
//        HTML label = new HTML(link);
        HTML openLink = new HTML("<a href=\"" + link + "\" target=\"_blank\">" + link + "</a>");
//        label.addClickHandler(new ClickHandler() {
//
//            @Override
//            public void onClick(ClickEvent event) {
//                Window.open(link, "_blank", "");
//            }
//        });
        return openLink;
    }

    @Override
    public void displayData() {
//        row = 0;
//        ft.clear();
        ft0Impl.clear();
        ftImpl.clear();
        ftImplAttr.clear();
        titleHp.clear();
        final EntityDetailsDataBean data = getData();
        //System.out.println(data.node.descr);
        String nodeKindCode = getNodeKindCode();
        //Webi,FullClient,Flash,Excel,Hyperlink,Powerpoint,Pdf,CrystalReport

        if (data.crrLabels != null) {
//            addTranslatedRow("Obszary uprawnień", data.crrLabels);
            ICheckEnabledByDesign cebd = new ICheckEnabledByDesign() {

                @Override
                public boolean isEnabledByDesign() {
                    return BIKClientSingletons.isEffectiveExpertLoggedIn() || BIKClientSingletons.isNodeAuthorOrEditorLoggedIn(data);
                }
            };
            boolean canEdit = BIKClientSingletons.isNodeActionEnabledByCustomRightRoles(ACTION_EDIT_CRR_LABELS, data, cebd)
                    || BIKClientSingletons.isNodeActionEnabledByCustomRightRoles(ACTION_EDIT_CRR_LABELS, cebd);
            if (canEdit) {
                ft0Impl.addTranslatedRow(I18n.obszaryUprawnien.get(), data.crrLabels, BIKClientSingletons.createEditButtonForAttribute(new IContinuation() {

                    @Override
                    public void doIt() {
                        //Window.alert("EDIT Obszary uprawnień!");
                        showEditPrivilegeLabelsDialog();
                    }
                }));
            } else {
                ft0Impl.addTranslatedRow(I18n.obszaryUprawnien.get(), data.crrLabels);
            }
        }

        boolean showOpenReportLink
                = BIKClientSingletons.getOpenReportLink(data.node.treeId) != null
                && BaseUtils.collectionContainsFix(BIKClientSingletons.appPropsBean.openReportForNodeKindsSet, getNodeKindCode());
        boolean showOpenInConfluenceLink = data.confluenceExtradata != null && data.confluenceExtradata.url != null;
        boolean showRefreshPageConfluence = BaseUtils.safeEqualsAny(nodeKindCode, BIKConstants.NODE_KIND_CONFLUENCE_PAGE, BIKConstants.NODE_KIND_CONFLUENCE_BLOG);

        linkedToReportBOHtml.setHTML(showOpenReportLink ? "<img src='images/infoView.gif' border='0'/>" : "");
        linkedToPageInConfluence.setHTML(showOpenInConfluenceLink ? "<img src='images/confluenceSpace.gif' border='0' style=\'padding-top:3px\'/>" : "");
        refreshPageInConfluence.setHTML(showRefreshPageConfluence ? "<img src='images/clear_filter_16.gif' border='0' style=\'padding-top:3px\'/>" : "");

        properDescr = data.properDescr;
        if (nodeKindCode.equals(BIKConstants.NODE_KIND_GLOSSARY_NOT_CORPO)) {
            properDescr = I18n.brakWersjiKorporacyjnej.get();
        }

        innerLinks = data.innerLinks;
        innerLinksGlos.setVisible(!BaseUtils.isMapEmpty(innerLinks));
//        lookAttachments.setVisible(BIKClientSingletons.isDocumentNodeKind(nodeKindCode));//nodeKindCode.equals(BIKGWTConstants.NODE_KIND_DOCUMENT));
        // dodatkowe parsowanie dla treści z Confluence
        if (nodeKindCode.equals(BIKGWTConstants.NODE_KIND_CONFLUENCE_PAGE) || nodeKindCode.equals(BIKGWTConstants.NODE_KIND_CONFLUENCE_BLOG) || nodeKindCode.equals(BIKGWTConstants.NODE_KIND_CONFLUENCE_SPACE)) {

            if (descr.isAttached()) {
                prepareIFrameForConfluenceContent(data);
            } else {
                descr.setHTML("...");
                descr.addAttachHandler(new AttachEvent.Handler() {
                    @Override
                    public void onAttachOrDetach(AttachEvent event) {
                        if (event.isAttached()) {
                            prepareIFrameForConfluenceContent(data);
                        }
                    }
                });
            }

//            frame.setAttribute("height", getDocHeight(frameDoc) + "px");
        } else {
            //ct-blad obrazka
//            descr.setHTML(BIKClientSingletons.replaceDescrAndSanitize(properDescr, widgetsActive ? "" : "$", innerLinks, nodeId, getNodeTreeCode()));
//            descr.setHTML(new SafeHtml() {
//
//                @Override
//                public String asString() {
////                    return BIKClientSingletons.replaceDescrAndSanitize(data.properDescr, widgetsActive ? "" : "$", data.innerLinks, nodeId, getNodeTreeCode());
//                    return properDescr;
//                }
//            });
            SafeHtmlBuilder builder = new SafeHtmlBuilder();
            descr.setHTML(builder.appendHtmlConstant(properDescr == null ? "" : properDescr).toSafeHtml());
        }
        //raporty JASPER
        BIKClientSingletons.parseDynamicNodeDesc(nodeId, descr);
        //ct: Test jasper opis
//        for (AttributeBean bean : data.attributes) {
//            if ("Jasper".equals(bean.atrName)) {
//                BIKClientSingletons.getService().compileJasper(nodeId, bean.atrLinValue, new StandardAsyncCallback<String>() {
//
//                    @Override
//                    public void onSuccess(String result) {
//                        descr.setHTML(result);
//                    }
//                });
//            }
//        }

        scPanelDef.scrollToTop();

        ArticleBean thisBizDef = data.thisBizDef;
        boolean isBizDef = thisBizDef != null;
        version.setVisible(isBizDef);

        if (!BaseUtils.isMapEmpty(innerLinks) && !widgetsActive && properDescr != null) {
            htmlPanel.add(descr);
            BIKClientSingletons.replaceHrefInnerLinksFromDescrToInlineLabel(innerLinks, htmlPanel);
        }
        String nodeKindCaptionByCode = getNodeKindCaption();//BIKClientSingletons.getNodeKindCaptionByCode(nodeKindCode);

        BlogBean blogEntry = data.blogEntry;
        AuthorBean authorBean = data.author;
        boolean isBlogDef = blogEntry != null;
        boolean isObjectWithAuthor = !data.node.treeKind.equals(BIKGWTConstants.TREE_KIND_METADATA)
                && authorBean != null;
//        ConfluenceObjectBean confluenceBean = data.confluenceExtradata;
//        boolean isConfluenceAuthor = data.node.treeKind.equals(BIKGWTConstants.TREE_KIND_CONFLUENCE) && confluenceBean != null && confluenceBean.authorFullName != null;
        author.setVisible(false);
        dateAdded.setVisible(false);

        if (isObjectWithAuthor) {
            setAuthorAndDateAdded(authorBean.authorSystemId, BaseUtils.safeToStringNotNull(authorBean.authorName), authorBean.dateAdded, data);
        }
//        if (isConfluenceAuthor) {
//            setAuthorAndDateAdded(-1, confluenceBean.authorFullName, confluenceBean.publishDate);
//        }
        if (isBlogDef) {
            setTitileAsHtml(I18n.wpis.get() /* I18N:  */, blogEntry.subject);
            setAuthorAndDateAdded(blogEntry.userId, blogEntry.userName, blogEntry.dateAdded, data);
        } else if (isBizDef) {
//            titleLbl.setHTML("<b>"
//                    + (!data.node.nodeKindCode.equals("GlossaryNotCorpo")
//                    ? BIKClientSingletons.getNodeKindCaptionByCode(data.node.nodeKindCode) : "Definicja korporacyjna")
//                    + ": </b>" + data.thisBizDef.subject.replace("(" + data.thisBizDef.versions + ")", ""));
            setTitileAsHtml((!nodeKindCode.equals(BIKGWTConstants.NODE_KIND_GLOSSARY_NOT_CORPO) ? nodeKindCaptionByCode : I18n.definicjaKorporacyjna.get() /* I18N:  */),
                    thisBizDef.subject.replace("(" + thisBizDef.versions + ")", ""));

            int isOfficial = thisBizDef.official;
            String defWersions;
            if (isOfficial == 1) {
                defWersions = I18n.korporacyjna.get() /* I18N:  */;
            } else if (isOfficial == 0) {
                defWersions = I18n.robocza.get() /* I18N:  */;
            } else {
                defWersions = I18n.inna.get() /* I18N:  */;
            }
            version.setHTML(I18n.wersja.get() /* I18N:  */ + " " + defWersions);
        } else {
            String nodeName = data.node.name;
            final boolean isRawFileNode = BIKConstants.NODE_KIND_RAW_FILE.equals(getNodeKindCode());
            boolean showDownloadLinks = BIKClientSingletons.isNodeActionEnabledByCustomRightRoles(ACTION_DOWNLOAD_DOCUMENT, data, new ICheckEnabledByDesign() {

                @Override
                public boolean isEnabledByDesign() {
                    return (data.thisDocument != null || (isRawFileNode && data.rawFileDownloadLink != null)) && (BIKClientSingletons.isLoggedUserHasPrivilege2PublicAttr(data.node));
                }
            });
            if (/*data.thisDocument != null*/showDownloadLinks) {
                String href = isRawFileNode ? data.rawFileDownloadLink : data.thisDocument.href;
                String captionFileName = isRawFileNode ? nodeName : data.thisDocument.caption;
                setTitle(nodeKindCaptionByCode, BIKClientSingletons.createLinkEx(BaseUtils.encodeForHTMLTag(nodeName), href, captionFileName));
                lookAttachments.setHTML(BIKClientSingletons.createLinkEx(I18n.zobaczDokument.get(), href, captionFileName));
                addRow(null, null, lookAttachments);
            } else {
                setTitileAsHtml(nodeKindCaptionByCode, nodeName);//titleLbl.setHTML("<b>" + nodeKindCaptionByCode + ": </b>" + data.node.name);
            }
            if (isRawFileNode) {
                addRow(I18n.sciezkaDoPliku.get(), data.filePath /*createOpenRawFileLink(FILE_PROTOCOL, data.filePath)*/);
                addRow(I18n.dataModyfikaji.get(), SimpleDateUtils.toCanonicalString(new Date(data.lastModified)));
            }
            if (BIKConstants.NODE_KIND_DOCUMENTS_FOLDER.equals(getNodeKindCode()) && data.node.treeCode.startsWith(BIKConstants.TREE_CODE_FILE_SYSTEM)) {
                addRow(I18n.sciezkaDoKatalogu.get(), data.folderPath/*createOpenRawFileLink(FILE_PROTOCOL, data.folderPath)*/);
            }
        }
        List<LinkedAlsoBean> alsoLinked = data.alsoLinked;
        if (!BaseUtils.isCollectionEmpty(alsoLinked)) { // występowanie w innych grupach dla podlinkowanych

            Collections.sort(alsoLinked, new Comparator<LinkedAlsoBean>() {
                @Override
                public int compare(LinkedAlsoBean o1, LinkedAlsoBean o2) {
                    String link1 = BaseUtils.safeToStringDef(o1.ancestorNodePathByNodeId.toString(), "");
                    String link2 = BaseUtils.safeToStringDef(o2.ancestorNodePathByNodeId.toString(), "");
                    return link1.compareToIgnoreCase(link2);
                }
            });

            final FlowPanel fp = new FlowPanel();
//            String treeCode = treeBean == null ? null : treeBean.code;

            for (final LinkedAlsoBean bean : alsoLinked) {
                if (bean.treeCode != null && (bean.treeCode.equals(BIKConstants.TREE_CODE_DQM) || bean.treeCode.equals(BIKConstants.TREE_CODE_DQM_DOKUMENTACJA))) {
                    continue;
                }
//                if (BaseUtils.safeEquals(bean.treeCode, treeCode)) {
                HorizontalPanel row = new HorizontalPanel();
                row.setSpacing(0);
                HTML labelBtn = new HTML(bean.caption);
                labelBtn.setTitle(setAncestorPath(bean.ancestorNodePathByNodeId));
                if (widgetsActive) {
                    labelBtn.addClickHandler(new ClickHandler() {
                        @Override
                        public void onClick(ClickEvent event) {
                            int linkedNodeId = bean.linkedNodeId;
                            if (linkedNodeId != getNodeId()) {
                                BIKClientSingletons.getTabSelector().submitNewTabSelection(bean.treeCode, linkedNodeId, true);
                            }
                        }
                    });
                    labelBtn.setStyleName("btnPointer");
                }
                row.add(BIKClientSingletons.createIconImgByCode(bean.icon));
                row.add(labelBtn);
                fp.add(row);
//                }
            }
            if (fp.getWidgetCount() > 0) {
//                addTranslatedRow(I18n.wystepowanieW_yes.get() /* I18N:  */ + " " + (BaseUtils.safeEquals(treeCode, BIKGWTConstants.TREE_CODE_USER_ROLES) ? I18n.rolach.get() /* I18N:  */ : I18n.grupach.get() /* I18N:  */), null, fp);
                addTranslatedRow(I18n.wystepowanie.get(), null, fp);
            }
        }
        titleLbl.setTitle("(" + "id" /* I18N: no */ + ": " + data.node.id + ")");
        bottomLbl.setVisible(data.user == null && !nodeKindCode.equals(BIKGWTConstants.NODE_KIND_USERS_GROUP));
        titleHp.add(titleLbl);

        if (data.user != null) {
//            addTranslatedRow("E-mail", data.user.email);
//            addTranslatedRow("Telefon", data.user.phoneNum);
//            userAvatarLbl = getImage(data.user.avatar, "UsersGroup");
            if (BIKClientSingletons.isStarredBIKUser(data.user.id)) {
                titleHp.add(BIKClientSingletons.createIconImgEx("images/star.png" /* I18N: no */, "16", "16", I18n.dodanychWpisow.get() /* I18N:  */ + ": " + data.user.cnt + "", "RankIcon"));
            }
        }

        if (ctx != null) {
            linkedNodeAction.setup(ctx);
            if (linkedNodeAction.isEnabledByDesign()) {
                linkedNodeBtn.setVisible(true);
                bottomLbl.setVisible(false);
            } else {
                linkedNodeBtn.setVisible(false);
            }
        } else {
            if (!isWidgetRunFromDialog()) {
                linkedNodeBtn.setVisible(false);
            } else {
                linkedNodeBtn.setVisible(true);
            }

//            bottomLbl.setVisible(false);
        }
        for (IEntityDetailsWidget edw : subWidgets) {
            edw.setIsLinked(linkedNodeAction.isEnabledByDesign());
            // tf: optymalizacja - wywołujemy displayData tylko jeśli ma content!
            if (edw.hasContent()) {
                edw.displayData();
            }
        }
        descr.setVisible(!BaseUtils.isStrEmptyOrWhiteSpace(descr.getHTML()));
        attributesFP.setVisible(attributeWidget.hasContent());
        attributesBeforeDescrFP.setVisible(attributeWidgetBeforeDescr.hasContent());
        childrenExtradata.setVisible(childrenExtradataWidget.hasContent());
        childrenExtradataPB.setVisible(childrenExtradataWidgetPB.hasContent());
        genericObjectsExtradata.setVisible(genericNodeObjectsWidget.hasContent());
        conUniv.setVisible(reportConnUnivWidget.hasContent());
        teradataExtradata.setVisible(teradataWidget.hasContent());
        mssqlExtradata.setVisible(mssqlWidget.hasContent());
        dbDefinitionExtradata.setVisible(dbDefinitionWidget.hasContent());
        mssqlColumnExtradata.setVisible(mssqlColumnWidget.hasContent());
        postgresExtradata.setVisible(postgresWidget.hasContent());
        oracleExtradata.setVisible(oracleWidget.hasContent());
        dqmTestExtradata.setVisible(dqmTestWidget.hasContent());
        dqmEvaluationExtradata.setVisible(dqmEvaluationWidget.hasContent());
        dqmDataErrorIssueExtradata.setVisible(dqmDataErrorIssueWidget.hasContent());
        dqcTestExtradata.setVisible(dqcTestWidget.hasContent());
        dqcTestGroupExtradata.setVisible(dqcTestGroupWidget.hasContent());
        dataModel.setVisible(dataModelWidget.hasContent());
        sapbwExtradata.setVisible(sapbwWidget.hasContent());
        userExtradata.setVisible(userExtradataWidget.hasContent());
        sapBoScheduleExtradata.setVisible(sapBoScheduleWidget.hasContent());
        lisaDictionary.setVisible(lisaDictContentWidget.hasContent());
        lisaDictionaryMetadata.setVisible(lisaDictMetadataContentWidget.hasContent());
        profileExtradata.setVisible(profileWidget.hasContent());
        lookupDescrBtn.setVisible(!BaseUtils.isHTMLTextEmpty(data.properDescr) && widgetsActive && !data.node.treeKind.equals(BIKConstants.TREE_KIND_CONFLUENCE));
        rightsBtn.setVisible(!BIKClientSingletons.hideRightsBtn());
        if (!widgetsActive) {
            pathAncestor.setHTML("<b>" + BIKClientSingletons.getMenuPathForTreeCode(data.node.treeCode) + setAncestorPath(data.ancestorNodePath) + "</b>");
        }
        if (dataModelWidget.hasContent()) {
            titleVp.clear();
            titleVp.add(dataModelWidget.buildContlolWidgets());
        }

        boolean showRefreshPageInConfluence = BIKClientSingletons.isNodeActionEnabledByCustomRightRoles(ACTION_SHOW_REFRESH_PAGE_IN_CONFLUENCE, data, new ICheckEnabledByDesign() {

            @Override
            public boolean isEnabledByDesign() {
                return BIKClientSingletons.isEffectiveExpertLoggedIn() || BIKClientSingletons.isNodeAuthorOrEditorLoggedIn(getData());
            }
        });
        refreshPageInConfluence.setVisible(showRefreshPageInConfluence);
//        if (eksportBtn != null) {
////            Window.alert(BaseUtils.isStrEmptyOrWhiteSpace(BIKClientSingletons.getNodeKindById(nodeKindId).jasperReports) + "");
//            eksportBtn.setVisible(!BaseUtils.isStrEmptyOrWhiteSpace(BIKClientSingletons.getNodeKindById(nodeKindId).jasperReports));
//        }
    }

    @Override
    public Widget buildWidgets() {
//        Window.alert("called buildWidgets()");
        vp = new FlowPanel();
        vp.setStyleName("BizDef");

        titleLbl = new HTML();
        version = new HTML();
        descr = new HTML();
        mainHp = new HorizontalPanel();
        mainVp = new VerticalPanel();

        mainHp.setWidth("100%");
        mainVp.setWidth("100%");
        pathAncestor = new HTML();
        linkedNodeAction = new NodeActionJumpToLinkedNode();
        linkedNodeBtn = new PushButtonWithHideText(new Image("images/link_arrows.gif" /* I18N: no */));
        linkedNodeBtn.setStyleName("BizDef-vWidget");
        linkedNodeBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                if (!isWidgetRunFromDialog()) {
                    linkedNodeAction.execute();
                } else {
                    BaseActionOrCancelDialog.hideAllDialogs();
                    BIKClientSingletons.getTabSelector().submitNewTabSelection(getNodeTreeCode(), nodeId, true);
                }

            }
        });
        linkedNodeBtn.setTitle(I18n.przejdzDoOryginalu.get() /* I18N:  */);
        linkedToReportBOHtml = new HTML();
        linkedToReportBOHtml.setStyleName("BizDef-vWidget");
        linkedToReportBOHtml.setTitle(I18n.otworzWSAPBO.get() /* I18N:  */);
        linkedToReportBOHtml.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                EntityDetailsDataBean data = getData();
//                SapBoExtraDataBean extradata = data.sapBoExtraData;
//                if (extradata != null && extradata.webiDocProperties != null) {
//                    List<Map<String, String>> extractXmlNodes = XMLNodeLameExtractor.extractXmlNodes(extradata.webiDocProperties, BaseUtils.paramsAsSet("webi_prop", "webi_prompt"), true, true);
//                    for (Map<String, String> map : extractXmlNodes) {
//                        if (map.containsValue("is_refresh_on_open") && map.get("value").equals("true")) {
//                            openInNewWindow(data.node.objId);
//                            return;
//                        }
//                    }
//                }
                BIKClientSingletons.getService().getLastInstanceForRaport(data.node.objId, data.node.id, new StandardAsyncCallback<Pair<Integer, String>>("Error in" /* I18N: no */ + " getLastInstanceForRaport") {
                    @Override
                    public void onSuccess(Pair<Integer, String> result) {
                        if (result == null) { // brak dostepu do raportu
                            SimpleInfoDialog dialog = new SimpleInfoDialog();
                            dialog.buildAndShowDialog(I18n.nieMaszUprawnieDoWyswietlTegoRap.get() /* I18N:  */ + ".", I18n.brakDostepu.get() /* I18N:  */ + "!", "OK" /* I18N: no */);
                        }
                        EntityDetailsDataBean data = getData();
                        String reportID;
                        Integer instanceFromServer = result.v1;
                        if (instanceFromServer != null && !instanceFromServer.equals(0)) {
                            reportID = instanceFromServer.toString();
                        } else {
                            reportID = data.node.objId;
                        }
                        openInNewWindow(data.node.treeId, reportID);
                    }
                });
            }
        });
        linkedToPageInConfluence = new HTML();
        linkedToPageInConfluence.setStyleName("BizDef-vWidget");
        linkedToPageInConfluence.setTitle(I18n.otworzWConfluence.get() /* I18N:  */);
        linkedToPageInConfluence.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                EntityDetailsDataBean data = getData();
                if (data.confluenceExtradata != null && data.confluenceExtradata.url != null) {
                    Window.open(data.confluenceExtradata.url, "_blank", "");
                }
            }
        });
        refreshPageInConfluence = new HTML();
        refreshPageInConfluence.setStyleName("BizDef-vWidget");
        refreshPageInConfluence.setTitle(I18n.odswiezObiektZConfluencea.get() /* I18N:  */);
        refreshPageInConfluence.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                //BIKClientSingletons.showInfo(I18n.odswiezanieObiektuZConfluence.get());
                final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.pobieranieObiketowZConfluenceProszeCzekac.get(), true);

                final EntityDetailsDataBean data = getData();
                BIKClientSingletons.getService().updateConfluenceNode(data.node, new StandardAsyncCallback<Integer>("Error in refreshConfluenceObject") {

                    @Override
                    public void onSuccess(Integer result) {
                        infoDialog.hideDialog();
                        BIKClientSingletons.showInfo(I18n.odswiezonoObiektZConfluence.get());

                        if (result != null) {
                            BIKClientSingletons.getTabSelector().getCurrentTab().activatePage(true, data.node.id, false);
                        } else {
                            BIKClientSingletons.getTabSelector().getCurrentTab().activatePage(true, null, false);
                        }
//                        refreshAndRedisplayData();

                        //BIKClientSingletons.getTabSelector().invalidateNodeData(getData().node.id);
                    }
                });
//                BIKClientSingletons.getService().refreshConfluenceObject(data.node.id, new StandardAsyncCallback<Void>("Error in refreshConfluenceObject") {
//                    public void onSuccess(Void result) {
//                        infoDialog.hideDialog();
//                        BIKClientSingletons.getTabSelector().invalidateNodeData(getData().node.id);
//                        BIKClientSingletons.showInfo(I18n.odswiezonoObiektZConfluence.get());
//                    }
//                });
            }
        });

//        refreshPageInConfluence.setVisible(BIKClientSingletons.isEffectiveExpertLoggedIn() || BIKClientSingletons.isNodeAuthorOrEditorLoggedIn(getData()));
        lookupDescrBtn = new PushButtonWithHideText(new Image("images/loupe.gif" /* I18N: no */));
        lookupDescrBtn.setStyleName("BizDef-vWidget");
        lookupDescrBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                EntityDetailsDataBean data = getData();
                Map<Integer, Pair<String, String>> innerLinks = data.innerLinks;
                new ViewHTMLInFullScreenDialog().buildAndShowDialog(I18n.opis.get() /* I18N:  */, null,
                        /*BIKClientSingletons.replaceDescrAndSanitize(data.properDescr, widgetsActive ? "" : "$", innerLinks, nodeId, getNodeTreeCode())*/ properDescr,
                        innerLinks, nodeId);
            }
        });
        lookupDescrBtn.setTitle(I18n.pokazOpis.get() /* I18N:  */);
        lookupDescrBtn.setVisible(false);

        rightsBtn = new PushButtonWithHideText(new Image("images/uprawnienia.png"));
        rightsBtn.setStyleName("BizDef-vWidget");
        rightsBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final TreeNodeBean node = getData().node;
                BIKClientSingletons.getService().getAssignedToNodeUsers(node.treeId, nodeId, new StandardAsyncCallback<Map<RightRoleBean, List<SystemUserBean>>>() {

                    @Override
                    public void onSuccess(Map<RightRoleBean, List<SystemUserBean>> result) {
                        new ShowAssignedUsers4NodeDialog(result).buildAndShowDialog();
                    }
                });
            }
        });
        rightsBtn.setTitle(I18n.usersBtn.get());
        nodeKindHelpBtn = new PushButtonWithHideText(new Image("images/hint.gif" /* I18N: no */));

        nodeKindHelpBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                final TreeNodeBean node = getData().node;
                final String helpKey = node.nodeKindCode + ":"
                        + (BIKClientSingletons.isLoggedUserAuthorOfTree(node.treeId) || BIKClientSingletons.isBranchAuthorLoggedIn(node.branchIds)
                        ? "author"
                        : ((BIKClientSingletons.isLoggedUserCreatorOfTree(node.treeId) || BIKClientSingletons.isBranchCreatorLoggedIn(node.branchIds))
                        ? "creator"
                        : (BIKClientSingletons.isLoggedUserIsNonPublicUserInTree(node.treeId) || BIKClientSingletons.isBranchNonPublicUserLoggedIn(node.branchIds)
                        ? "Non public user" : "user")));
                BIKClientSingletons.getService().getHelpDescr(helpKey, null, new StandardAsyncCallback<HelpBean>() {
                    @Override
                    public void onSuccess(HelpBean result) {
                        new HelpDialog().buildAndShowDialog(result, helpKey, node.nodeKindCaption);
                    }
                });
            }
        });

//        nodeKindHelpBtn = new BikCustomButton(I18n.pomoc.get() /* I18N:  */, "help" /* I18N: no */,
//                new IContinuation() {
//            @Override
//            public void doIt() {
//                final TreeNodeBean node = getData().node;
//                final String helpKey = node.nodeKindCode + ":"
//                        + (BIKClientSingletons.isLoggedUserAuthorOfTree(node.treeId) || BIKClientSingletons.isBranchAuthorLoggedIn(node.branchIds)
//                        ? "author"
//                        : ((BIKClientSingletons.isLoggedUserCreatorOfTree(node.treeId) || BIKClientSingletons.isBranchCreatorLoggedIn(node.branchIds))
//                        ? "creator"
//                        : (BIKClientSingletons.isLoggedUserIsNonPublicUserInTree(node.treeId) || BIKClientSingletons.isBranchNonPublicUserLoggedIn(node.branchIds)
//                        ? "Non public user" : "user")));
//                BIKClientSingletons.getService().getHelpDescr(helpKey, null, new StandardAsyncCallback<HelpBean>() {
//                    @Override
//                    public void onSuccess(HelpBean result) {
//                        new HelpDialog().buildAndShowDialog(result, helpKey, node.nodeKindCaption);
//                    }
//                });
//            }
//        });
        nodeKindHelpBtn.setStyleName("helpPb");
        nodeKindHelpBtn.setTitle(I18n.pomoc.get() /* I18N:  */);
        nodeKindHelpBtn.setVisible(true);
        if (!BIKClientSingletons.isHideElementsForMM()) {
            showAssociationBtn = new PushButtonWithHideText(new Image("images/association.png"));
            showAssociationBtn.setStyleName("BizDef-vWidget");
            showAssociationBtn.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    new ShowAssociationDialog().buildAndShowDialog(nodeId);
                }
            });
            showAssociationBtn.setTitle(I18n.pokazPowiazania.get() /* I18N:  */);

            addParentJoinedObjsBtn = new PushButtonWithHideText(new Image("images/network_connections.png"));
            addParentJoinedObjsBtn.setStyleName("BizDef-vWidget");
            addParentJoinedObjsBtn.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    if (ctx != null && ctx.getNode().parentNodeId != null) {
                        new AddParentJoinedObjs2Node().buildAndShowDialog(ctx.getNode(), new IParametrizedContinuation<Pair<Set<Integer>, Set<Integer>>>() {

                            @Override
                            public void doIt(Pair<Set<Integer>, Set<Integer>> param) {
                                Set<Integer> dst2Add = param.v1;
                                Set<Integer> dst2Delete = param.v2;
                                BIKClientSingletons.getService().updateJoinedObjsFromParent(ctx.getNode().id, dst2Add, dst2Delete, new StandardAsyncCallback<Void>() {

                                    @Override
                                    public void onSuccess(Void result) {
                                        refreshAndRedisplayData();
                                    }
                                });
                            }
                        });
                    }
                }
            });
            addParentJoinedObjsBtn.setTitle(I18n.powiazaniaZWyzszegoPoziomu.get() /* I18N:  */);
        }
        linkedNodeBtn.setVisible(false);
        mainLbl = new HorizontalPanel();
        bottomFp = new FlowPanel();
        bottomFp.setStyleName("tittle-icon");
        pathAncestorLbl = new FlowPanel();
        bottomLbl = new FlowPanel();
        innerLinksGlos = new FlowPanel();
        lookAttachments = new HTML();

        conUniv = new FlowPanel();
        attributesFP = new FlowPanel();
        attributesBeforeDescrFP = new FlowPanel();
        childrenExtradata = new FlowPanel();
        childrenExtradataPB = new FlowPanel();
        genericObjectsExtradata = new FlowPanel();
        teradataExtradata = new FlowPanel();
        mssqlExtradata = new FlowPanel();
        dbDefinitionExtradata = new FlowPanel();
        mssqlColumnExtradata = new FlowPanel();
        postgresExtradata = new FlowPanel();
        oracleExtradata = new FlowPanel();
        dqmTestExtradata = new FlowPanel();
        dqmEvaluationExtradata = new FlowPanel();
        dqmDataErrorIssueExtradata = new FlowPanel();
        dqcTestExtradata = new FlowPanel();
        dqcTestGroupExtradata = new FlowPanel();
        sapbwExtradata = new FlowPanel();
        userExtradata = new FlowPanel();
        sapBoScheduleExtradata = new FlowPanel();
        lisaDictionary = new FlowPanel();
        profileExtradata = new FlowPanel();

        lisaDictionaryMetadata = new FlowPanel();
        dataModel = new FlowPanel();
        vpsp = new VerticalPanel();

//        ft = new FlexTable();
//        ft.setWidth("100%");
//        ft.setStyleName("gridJoinedObj");
//        ft.getColumnFormatter().setWidth(0, "250px");
//        ft.getColumnFormatter().setWidth(2, "16px");
//        ft.getColumnFormatter().setWidth(3, "16px");
        ft0Impl = new MainHeaderSharedTable(this);
        ftImpl = new MainHeaderSharedTable(this);
        //to display attributes in different header table second MainHeaderSharedTable is needed
        ftImplAttr = new MainHeaderSharedTable(this);

        titleLbl.setStyleName("BizDef-titleLbl");
        version.setStyleName("BizDef-version");
        descr.setStyleName("BizDef-descr");
        innerLinksGlos.setStyleName("BizDef-innerLinks");
        mainLbl.setStyleName(widgetsActive ? "BizDef-mainLbl" : "BizDefDialog-mainLbl");
        pathAncestorLbl.setStyleName("BizDef-mainLbl");
        conUniv.setStyleName("BizDef-conUniv");
        lookAttachments.setStyleName("lookupTrashBtn");
        dqmTestExtradata.setStyleName("BizDef-conUniv");
        dqmEvaluationExtradata.setStyleName("BizDef-conUniv");
        dqmDataErrorIssueExtradata.setStyleName("BizDef-conUniv");
        dqcTestExtradata.setStyleName("BizDef-conUniv");
        sapbwExtradata.setStyleName("BizDef-conUniv");
        userExtradata.setStyleName("BizDef-conUniv");
        sapBoScheduleExtradata.setStyleName("BizDef-conUniv");
        profileExtradata.setStyleName("BizDef-conUniv");

//        mainLbl.add(titleLbl);
//        titleH.add(titleLbl);
        mainLbl.add(titleHp);
        EntityInFvsWidget fvsWidget = new EntityInFvsWidget();
        fvsWidget.setRefreshContinuation(refreshContinuation);
        bottomLbl.add(addAndBuildSubWidget(fvsWidget));
        attributesFP.add(addAndBuildSubWidget(attributeWidget = new AttributeWidget(ftImplAttr)));
        attributesBeforeDescrFP.add(addAndBuildSubWidget(attributeWidgetBeforeDescr = new AttributeWidget(ft0Impl, true)));
        childrenExtradata.add(addAndBuildSubWidget(childrenExtradataWidget = new ChildrenExtradataWidget(bikPage, widgetsActive)));

        childrenExtradataPB.add(addAndBuildSubWidget(childrenExtradataWidgetPB
                = new SubRowsExtradataWidgetForDqmAtrybutyJakosciDanych()));

        genericObjectsExtradata.add(addAndBuildSubWidget(genericNodeObjectsWidget = new GenericNodeObjectsWidget()));
        innerLinksGlos.add(addAndBuildSubWidget(new InnerLinksWidgetFlat()));
        conUniv.add(addAndBuildSubWidget(reportConnUnivWidget = new SAPBOExtradataWidget(this)));//dlugo 400
        teradataExtradata.add(addAndBuildSubWidget(teradataWidget = new TeradataExtradataWidget()));
        mssqlExtradata.add(addAndBuildSubWidget(mssqlWidget = new MssqlShowSQLWidget()));
        dbDefinitionExtradata.add(addAndBuildSubWidget(dbDefinitionWidget = new DbShowSQLWidget()));
        mssqlColumnExtradata.add(addAndBuildSubWidget(mssqlColumnWidget = new SqlServerExtradataWidget(this)));
        postgresExtradata.add(addAndBuildSubWidget(postgresWidget = new PostgresExtradataWidget()));
        oracleExtradata.add(addAndBuildSubWidget(oracleWidget = new OracleExtradataWidget()));
        dqmTestExtradata.add(addAndBuildSubWidget(dqmTestWidget = new DQMTestWidget(this)));
        dqmEvaluationExtradata.add(addAndBuildSubWidget(dqmEvaluationWidget = new DQMEvaluationWidget(this)));
        dqmDataErrorIssueExtradata.add(addAndBuildSubWidget(dqmDataErrorIssueWidget = new DQMDataErrorIssueWidget(this)));
        dqcTestExtradata.add(addAndBuildSubWidget(dqcTestWidget = new DQCTestWidget(this)));
        dqcTestGroupExtradata.add(addAndBuildSubWidget(dqcTestGroupWidget = new DataQualityTestGroupWidget()));
        sapbwExtradata.add(addAndBuildSubWidget(sapbwWidget = new SapBWWidget(this)));
        userExtradata.add(addAndBuildSubWidget(userExtradataWidget = new UserExtradataWidget(this)));
        sapBoScheduleExtradata.add(addAndBuildSubWidget(sapBoScheduleWidget = new SapBoScheduleWidget(this)));
        //lisaDictionary.add(addAndBuildSubWidget(lisaDictContentWidget = new LisaDictionaryContent()));
        lisaDictionary.add(addAndBuildSubWidget(lisaDictContentWidget = new LisaDictionaryContentDisplayedByDataGrid()));
        lisaDictionaryMetadata.add(addAndBuildSubWidget(lisaDictMetadataContentWidget = new LisaDictionaryMetadataContent()));
        profileExtradata.add(addAndBuildSubWidget(profileWidget = new ProfileExtradataWidget(this)));
        dataModel.add(addAndBuildSubWidget(dataModelWidget = new DataModelWidget(/*this*/) {
            @Override
            protected void setScrollPositionInDetailWidget(int left, int top) {
                setScrollPositionPanelDef(left, top);
            }
        }));

        bottomFp.add(nodeKindHelpBtn);
        bottomFp.add(bottomLbl);
        bottomFp.add(linkedNodeBtn);

        //if (!ifWidgetRunFromDialog())
        {
            PushButtonWithHideText labelLinkBtn = new PushButtonWithHideText(new Image("images/share.gif" /* I18N: no */));
//             PushButtonWithHideText labelLinkBtn = new PushButtonWithHideText(new Image("images/share.png" /* I18N: no */));
            labelLinkBtn.setStyleName("BizDef-vWidget");
            labelLinkBtn.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    new ViewURLLink().buildAndShowDialog(I18n.adresURL.get() /* I18N:  */, HistoryManager.getFullUrl(getNodeTreeCode(), nodeId + ""), new IParametrizedContinuation<String>() {
                        public void doIt(String param) {
//                        no-op
                        }
                    });
                }
            });
            labelLinkBtn.setTitle(I18n.polecZnajomemu.get() /* I18N:  */);

            bottomFp.add(labelLinkBtn);
            addWidgetVisibledByState(labelLinkBtn);
        }

//        if (BIKClientSingletons.showExportJasperReports()) {
//            eksportBtn = new PushButtonWithHideText(new Image("images/export.png" /* I18N: no */));
//            eksportBtn.setStyleName("BizDef-vWidget");
//            eksportBtn.addClickHandler(new ClickHandler() {
//                public void onClick(ClickEvent event) {
////&& !BaseUtils.isStrEmptyOrWhiteSpace(jasperReports)
//                    final String jasperReports = BIKClientSingletons.getNodeKindById(nodeKindId).jasperReports;
//                    Map<String, String> rp = new HashMap<String, String>();
//                    List<String> reports = BaseUtils.splitBySep(jasperReports, "|", true);
//                    for (String r : reports) {
//                        Pair<String, String> nameToLocation = BaseUtils.splitString(r, ",");
//                        if (nameToLocation.v2 != null) {
//                            rp.put(nameToLocation.v2, nameToLocation.v1);
//                        }
//                    }
//                    new JasperReportExportWidget().buildAndShowDialog(nodeId, rp, new IParametrizedContinuation<String>() {
//
//                        @Override
//                        public void doIt(String param) {
//
//                        }
//                    });
//                }
//
//            });
//
//            eksportBtn.setTitle(I18n.eksport.get() /* I18N:  */);
//
//            bottomFp.add(eksportBtn);
//
//            addWidgetVisibledByState(eksportBtn);
//        }
//        labelLinkBtn.setEnabled(widgetsActive); // zawsze widoczny
        EntityVoteValsWidget voteWidget = new EntityVoteValsWidget();
        Widget evvw = addAndBuildSubWidget(voteWidget);
        bottomFp.add(evvw);
        bottomFp.add(linkedToReportBOHtml);
        bottomFp.add(refreshPageInConfluence);
        bottomFp.add(linkedToPageInConfluence);
        if (!BIKClientSingletons.isHideElementsForMM()) {
            bottomFp.add(showAssociationBtn);
            bottomFp.add(addParentJoinedObjsBtn);
        }
        bottomFp.add(rightsBtn);
        bottomFp.add(lookupDescrBtn);

        if (widgetsActive && BIKClientSingletons.suggestSimilarNodes()) {
            //Button btnSuggestSimilarNodes = new Button("#=@?", new ClickHandler() {
            PushButtonWithHideText btnSuggestSimilarNodes = new PushButtonWithHideText(new Image("images/magic-wand.png" /* I18N: no */));
            btnSuggestSimilarNodes.setStyleName("BizDef-vWidget");
            btnSuggestSimilarNodes.setTitle(I18n.zobaczSugerowanePowiazania.get());

            btnSuggestSimilarNodes.addClickHandler(
                    new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
//                            new ViewAndJoinSimilarNodesDialog().buildAndShowDialog(nodeId, getNodeName(),
                    new LuceneViewAndJoinSimilarNodesDialog().buildAndShowDialog(nodeId, getNodeName(),
                            getNodeKindCaption(),
                            new IContinuation() {

                        @Override
                        public void doIt() {
                            refreshAndRedisplayData();
                        }
                    });
                }
            });
            bottomFp.add(btnSuggestSimilarNodes);
        }

        mainLbl.add(bottomFp);

        evvw.setStyleName(
                "BizDef-toFav");
        addWidgetVisibledByState(evvw);

        HorizontalPanel titleWithButtonsHp = new HorizontalPanel();
        //VerticalPanel titleVp = new VerticalPanel();
        titleVp = new VerticalPanel();

        mainVp.add(conUniv);
        mainVp.add(childrenExtradata);
        mainVp.add(childrenExtradataPB);
        mainVp.add(teradataExtradata);
        mainVp.add(mssqlColumnExtradata);
        mainVp.add(postgresExtradata);
        mainVp.add(oracleExtradata);
        mainVp.add(dqmTestExtradata);
        mainVp.add(dqmEvaluationExtradata);
        mainVp.add(dqmDataErrorIssueExtradata);
        mainVp.add(dqcTestExtradata);
        mainVp.add(dqcTestGroupExtradata);
        mainVp.add(sapbwExtradata);
        mainVp.add(userExtradata);
        mainVp.add(sapBoScheduleExtradata);
        mainVp.add(lisaDictionary);
        mainVp.add(lisaDictionaryMetadata);
        mainVp.add(dataModel);
        mainVp.add(mssqlExtradata);
        mainVp.add(dbDefinitionExtradata);
        mainVp.add(profileExtradata);

        dataModel.setSize("100%", "100%");
        mainVp.add(genericObjectsExtradata);
        mainHp.add(mainVp);
        pathAncestorLbl.setVisible(!widgetsActive);
        pathAncestorLbl.add(pathAncestor);

        titleVp.add(pathAncestorLbl);

        titleVp.add(mainLbl);
        titleVp.setWidth("100%");
        titleWithButtonsHp.setWidth("100%");
        titleWithButtonsHp.add(titleVp);

        vp.add(titleWithButtonsHp);

        defVp.setStyleName(
                "BizDef");
        defVp.add(version);

        defVp.add(attributesBeforeDescrFP);
        htmlPanel = new HTMLPanel("");

        htmlPanel.add(descr);

        defVp.add(htmlPanel);

        FlowPanel hp2 = new FlowPanel();

        hp2.add(descr);

        if (!BIKClientSingletons.showDescriptionAfterAttributes()) {
            defVp.add(hp2);
        }

        defVp.add(innerLinksGlos);

        if (BIKClientSingletons.showAttributesBeforeChildrenExtradataWidget()) {
//            defVp.add(ft);
            defVp.add(ftImpl);
            defVp.add(ft0Impl);
            defVp.add(ftImplAttr);
            defVp.add(attributesFP);
            defVp.add(mainHp);
            if (BIKClientSingletons.showDescriptionAfterAttributes()) {
                defVp.add(hp2);
            }
        } else {
            defVp.add(mainHp);
//            defVp.add(ft);
            defVp.add(ftImpl);
            defVp.add(ft0Impl);
            defVp.add(ftImplAttr);
            defVp.add(attributesFP);
            if (BIKClientSingletons.showDescriptionAfterAttributes()) {
                defVp.add(hp2);
            }
        }
        defVp.add(dateAdded);
        defVp.add(author);

//        scPanelDef = new ScrollPanel(defVp);
        scPanelDef.setStyleName("BizDef-Scroll");
        vp.add(scPanelDef);
        titleLbl.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                scPanelDef.scrollToTop();
            }
        });

        return vp;
    }

    @Override
    public void refreshWidgetsState() {
        for (IEntityDetailsWidget edw : subWidgets) {
            edw.refreshWidgetsState();
        }
    }

    protected void addEntitySapBoExtraDataProp(StringBuilder sb, String caption, String val) {
        if (!BaseUtils.isStrEmptyOrWhiteSpace(val)) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append("<b>").append(caption).append(": </b>").append(val);
        }
    }

    public boolean isEnabledDeprecated(String code, int count) {
        return true;
    }

    public void setHeightScPanel(int height, FlowPanel secStk) {
        final int secStkHeight = VisualMetricsUtils.getHeight(secStk);
        final int dynamicHeightForLowerWidget = VisualMetricsUtils.getDynamicHeightForLowerWidget(secStk, scPanelDef);
        final int newHeight = dynamicHeightForLowerWidget - height;

        if (height > 0 && (secStkHeight - height) > 200) {
            secStk.setStyleName("EntityDetailsPane");

            if (BaseActionOrCancelDialog.isShowDiagDialogSizingEnabled()) {
                BaseActionOrCancelDialog.showDiagDialogSizingMsgS("setHeightScPanel: setResizeSensitiveCustomWidgetHeight");
            }

            BIKClientSingletons.getGUIResizer().setResizeSensitiveCustomWidgetHeight(scPanelDef, newHeight);
        } else {
            if (newHeight /* -4 */ > 0) {
                scPanelDef.setHeight(newHeight + "px");
            } else {
                scPanelDef.setHeight(50 + "px");
            }
        }
        if (lisaDictContentWidget instanceof LisaDictionaryContentDisplayedByDataGrid) {
            ((LisaDictionaryContentDisplayedByDataGrid) lisaDictContentWidget).resizeDataGrid(scPanelDef.getOffsetHeight() - 80, scPanelDef.getOffsetWidth());
        }

        if (BaseActionOrCancelDialog.isShowDiagDialogSizingEnabled()) {
            BaseActionOrCancelDialog.showDiagDialogSizingMsgS("setHeightScPanel: done, height=" + height
                    + ", secStkHeight=" + secStkHeight
                    + ", dynamicHeightForLowerWidget=" + dynamicHeightForLowerWidget
                    + ", newHeight=" + newHeight
                    + ", final scPanelDef.height=" + VisualMetricsUtils.getHeight(scPanelDef));
        }
    }

    protected String setAncestorPath(List<String> ancestorNodePath) {
        String path = BaseUtils.mergeWithSepEx(ancestorNodePath, BIKGWTConstants.RAQUO_STR_SPACED);
        return BaseUtils.encodeForHTMLTag(path);
    }

    @Override
    public void addTranslatedRow(String nameId, Object value, Widget... actWidget) {
        ftImpl.addTranslatedRow(nameId, value, actWidget);
    }

    //wrzuacamy (s,s) lub(s,s,w,w) lub(s,null,w) lub (null,null,w)
    @Override
    public void addRow(String nameToDisplay, boolean isVisible, Object value, Widget... actWidget) {
        ftImpl.addRow(nameToDisplay, isVisible, value, actWidget);
    }

    public void addRow(String nameToDisplay, Object value, Widget... actWidget) {
        ftImpl.addRow(nameToDisplay, true, value, actWidget);
    }

//    public void addTranslatedRow(String nameId, Object value, Widget... actWidget) {
//        String nameToDisplay = BIKClientSingletons.getTranslatedAttributeNameOrNullForHidden(getData().node.nodeKindId, nameId);
//        addRow(nameToDisplay, value, nameId != null && nameToDisplay == null, actWidget);
//    }
//    //wrzuacamy (s,s) lub(s,s,w,w) lub(s,null,w) lub (null,null,w)
//
//    public void addRow(String nameToDisplay, Object value, Widget... actWidget) {
//        addRow(nameToDisplay, value, false, actWidget);
//    }
//
//    public void addRow(String nameToDisplay, Object value, boolean hideWidgets, Widget... actWidget) {
////        String nameToDisplay = BIKClientSingletons.getTranslatedAtribute(getData().node.nodeKindId, nameId);
//        int col = 0;
//        if (nameToDisplay != null) {
//            String valueStr = BaseUtils.safeToString(value);
//            if (!BaseUtils.isStrEmpty(nameToDisplay)) {
//                if (!BaseUtils.isStrEmpty(valueStr)) {
//
//                    Widget valuelbl;
//                    if (value instanceof Widget) {
//                        valuelbl = (Widget) value;
//                    } else {
//                        valuelbl = new Label(valueStr);
//                        valuelbl.setStyleName("gp-html");
//                    }
//
//                    ft.setWidget(row, col++, createHintedHTML(nameToDisplay, null));
//                    ft.setWidget(row, col++, valuelbl);
//                } else {
//                    if (actWidget.length > 0) {
//                        ft.setWidget(row, col++, createHintedHTML(nameToDisplay, null));
//                    }
//                }
//            }
//        }
//        if (actWidget.length > 0 && !hideWidgets) {
//            for (Widget w : actWidget) {
//                ft.getCellFormatter().setStyleName(row, col, row % 2 == 0 ? "RelatedObj-oneObj-white-middle" : "RelatedObj-oneObj-grey-middle");
//                ft.setWidget(row, col++, w);
//            }
//        }
//        if (col > 0) {
//            while (col < 4) {
//                ft.setWidget(row, col++, new Label());
//            }
//            ft.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
//            ft.getCellFormatter().setStyleName(row, 0, row % 2 == 0 ? "RelatedObj-oneObj-white-top" : "RelatedObj-oneObj-grey-top");
//            row++;
//        }
//    }
    //type: zazwyczaj podawany caption node_kind-a , czasem cos innego
    protected void setTitileAsHtml(String type, String name) {
        setTitle(type, BaseUtils.encodeForHTMLTag(name));
    }

    protected void setTitle(String type, String name) {
        titleLbl.setHTML((BIKClientSingletons.isShowTitleInEntityHeader() ? "<b>" + type + ":</b> " : "") + name);
    }

    protected void openInNewWindow(int treeId, String instanceID) {
        Window.open(BIKClientSingletons.getOpenReportLink(treeId).replace("{si_id}", instanceID), "_blank", "");
    }

    public void setRefreshContinuation(IContinuation refreshContinuation) {
        this.refreshContinuation = refreshContinuation;
    }

    public void setAuthorAndDateAdded(Integer authorId, final String authorName, final Date date, EntityDetailsDataBean data) {
        dateAdded.setHTML("<br/>" + I18n.dataDodania.get() /* I18N:  */ + ": " + SimpleDateUtils.toCanonicalString(date));
        dateAdded.setStyleName("BizDef-author");
        if (!BaseUtils.isStrEmptyOrWhiteSpace(authorName)) {
            author.setHTML(I18n.autor.get() /* I18N:  */ + ": " + BaseUtils.encodeForHTMLTag(authorName)
                    + (BIKClientSingletons.isStarredSystemUser(authorId)
                    ? " <img src=\"images/star.png\" style=\"width: 12px; height: 12px;\">" : ""));
            author.setStyleName("BizDef-author");
        }
        boolean showAuthorAndData = BIKClientSingletons.isNodeActionEnabledByCustomRightRoles(ACTION_SHOW_DATE_AND_AUTHOR, data, new ICheckEnabledByDesign() {

            @Override
            public boolean isEnabledByDesign() {
                return date != null && authorName != null;
            }
        });
        dateAdded.setVisible(showAuthorAndData);
        author.setVisible(showAuthorAndData);
    }

    protected void prepareIFrameForConfluenceContent(EntityDetailsDataBean data) {
        //            properDescr = BIKClientSingletons.sanitizeForConfluenceContent(properDescr);
        String frameId = Document.get().createUniqueId();
        descr.setHTML("<iframe scrolling='no' width='100%' height='0' frameBorder='0' id='" + frameId + "'"
                + " onload=\"initFrameOnLoad(this); \""
                + "></iframe>");
        Element frameEle = GWTUtils.getElementById(frameId);
        IFrameElement frame = IFrameElement.as(frameEle);
        final Document frameDoc = frame.getContentDocument();
        writeToDocument(frameDoc, data.node.descr);
    }

//    @Override
//    protected Panel createHintedHTML(final String hintName, String text) {
//
//        HorizontalPanel wrapper = new HorizontalPanel();
//        HTML html = new HTML("<b>" + hintName + ":</b> ");
//        wrapper.add(html);
//
//        Widget btn = BIKClientSingletons.createHintButton(hintName);
//
//        wrapper.add(btn);
//
//        BIKClientSingletons.setupDisplayOnHover(btn, new IContinuationWithReturn<Boolean>() {
//            @Override
//            public Boolean doIt() {
//                return BIKClientSingletons.attrHasHint(hintName);
//            }
//        });
//
//        if (text != null) {
//            wrapper.add(new HTML(text));
//        }
//        wrapper.setStyleName("gp-html");
//        hintButtonsByHintName.put(hintName, btn);
//        hintWrappersByHintName.put(hintName, wrapper);
//        return wrapper;
//    }
    public void setScrollPositionPanelDef(int left, int top) {
        scPanelDef.setHorizontalScrollPosition(left);
        scPanelDef.setVerticalScrollPosition(top);
    }

    @Override
    public void clear() {
        ftImpl.clear();
        ft0Impl.clear();
        ftImplAttr.clear();
    }

    public void setBikPage(IBikTreePage bikPage) {
        this.bikPage = bikPage;
    }

    @Override
    public void setColumnWidthForNonActiveWidget() {
        //just empty method
    }

}
