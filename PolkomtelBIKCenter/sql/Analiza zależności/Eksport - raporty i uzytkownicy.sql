﻿IF OBJECT_ID('tempdb..#xxx') IS NOT NULL 
drop table #xxx
go

select bn.name, 
		bn.id, 
		bn.parent_node_id,
		cast('' as varchar(max)) as rap_parent_path, 
		bn.parent_node_id as rap_ancestor_id
into #xxx
from bik_node bn
inner join bik_node_kind bnk on bn.node_kind_id=bnk.id
inner join bik_tree bt on bn.tree_id=bt.id
where bn.linked_node_id is null
and bn.is_deleted = 0
and bnk.code in ('Webi', 'Flash', 'CrystalReport', 'Excel', 'FullClient', 'Pdf', 'Hyperlink', 'Rtf', 'Txt', 'Powerpoint', 'Word')
and bt.code = 'Reports'

while 1 = 1 begin
  declare @rc int = null

  update #xxx
  set rap_parent_path = bn.name + '\' + rap_parent_path, rap_ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #xxx.rap_ancestor_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go

declare @usersForNode table (
	node_id int not null,
	user_name varchar(max) not null,
	user_role varchar(max)  not null,
	role_type int not null
);

--- kursor
declare kurs cursor for 
select id, parent_node_id from #xxx 

open kurs;
declare @id int;
declare @parentId int;

fetch next from kurs into @id, @parentId;
while @@fetch_status = 0
	begin
	
	insert into @usersForNode(node_id,user_name,user_role,role_type)
	select distinct @id, u.name, role.caption, uin.is_auxiliary from bik_user_in_node uin
	inner join bik_user u on uin.user_id = u.id
	inner join bik_role_for_node role on role.id = uin.role_for_node_id
	inner join bik_node bnu on bnu.id=u.node_id
	inner join bik_node rol on rol.id=role.node_id
	left join @usersForNode old on old.node_id = @id and old.user_name = u.name and old.user_role = role.caption and old.role_type = uin.is_auxiliary
	where uin.node_id = @id
	and bnu.is_deleted = 0
	and bnu.linked_node_id is null 
	and rol.is_deleted = 0
	and rol.linked_node_id is null
	and old.node_id is null
	
	while @parentId is not null
	begin

		insert into @usersForNode(node_id,user_name,user_role,role_type)
		select distinct @id, u.name, role.caption, uin.is_auxiliary from bik_user_in_node uin
		inner join bik_user u on uin.user_id = u.id
		inner join bik_role_for_node role on role.id = uin.role_for_node_id
		inner join bik_node bnu on bnu.id=u.node_id
		inner join bik_node rol on rol.id=role.node_id
		left join @usersForNode old on old.node_id = @id and old.user_name = u.name and old.user_role = role.caption and old.role_type = uin.is_auxiliary
		where uin.node_id = @parentId
		and uin.inherit_to_descendants = 1
		and bnu.is_deleted = 0
		and bnu.linked_node_id is null 
		and rol.is_deleted = 0
		and rol.linked_node_id is null
		and old.node_id is null
		
		select @parentId = parent_node_id from bik_node where id = @parentId
	end
	
	fetch next from kurs into @id, @parentId;
	end
close kurs;
deallocate kurs;

select rap_parent_path as 'Sciezka do raportu', name as 'Raport', user_name as 'Nazwa użytkownika', user_role + ' (' + case when role_type = 0 then 'Główny)' else 'Pomocniczy)' end as 'Rola użytkownika' 
from #xxx rep left join @usersForNode users on rep.id = users.node_id
where rap_parent_path like 'Foldery korporacyjne%'
order by 1,2,3,4

drop table #xxx;