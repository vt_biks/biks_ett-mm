﻿exec sp_check_version '1.8.5.1';
GO

INSERT INTO bik_app_prop (
	name
	,val
	,is_editable
	)
VALUES (
	'showDescriptionAfterAttributes'
	,'true'
	,1
	)

exec sp_update_version '1.8.5.1'
	,'1.8.5.2';
GO


