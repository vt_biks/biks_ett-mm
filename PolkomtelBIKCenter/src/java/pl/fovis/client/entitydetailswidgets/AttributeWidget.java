/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog.AttributeType;
import pl.fovis.client.dialogs.EntityDetailsPaneDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author AMamcarz
 */
public class AttributeWidget extends EntityDetailsWidgetFlatBase {

    private VerticalPanel mainAttrVp;
//    private VerticalPanel innerAttrVp;
//    protected Map<String, Widget> hintBtns = new HashMap<String, Widget>();
    IMainHeaderSharedTable imhst;
    protected boolean beforeDescr;

    public AttributeWidget(IMainHeaderSharedTable imhst) {

        this(imhst, false);
    }

    public AttributeWidget(IMainHeaderSharedTable imhst, boolean beforeDescr) {
        this.imhst = imhst;
        this.beforeDescr = beforeDescr;
    }

    protected Map<String, List<Integer>> usedLink = new HashMap<String, List<Integer>>();
    protected List<AttributeBean> attributes = new ArrayList<AttributeBean>();
    protected Map<String, String> attrNameAndValue = new HashMap<String, String>();

    @Override
    public void displayData() {
        usedLink.clear();
        attrNameAndValue.clear();
//        hintBtns.clear();
//        innerAttrVp.clear();

        BIKClientSingletons.getService().getRequiredAttrsWithVisOrder(nodeKindId, new StandardAsyncCallback<List<AttributeBean>>() {

            @Override
            public void onSuccess(List<AttributeBean> result) {

                boolean isDQMTree = treeBean != null ? treeBean.code.equals(BIKConstants.TREE_CODE_DQM) : false;
                boolean isShowAttributeButtonsOnTheLeft = isDQMTree ? false : BIKClientSingletons.isShowAttributeButtonsOnTheLeft();
                attributes = BIKClientSingletons.filtrPublicAttributes(getData().node, getData().attributes);

                if (!BaseUtils.isCollectionEmpty(attributes) && !BaseUtils.isCollectionEmpty(result)) {
                    for (AttributeBean listAb : result) {
                        for (AttributeBean ab : attributes) {
                            if (listAb.atrName.equals(ab.atrName)) {
                                ab.visualOrder = listAb.visualOrder;
                            }
                        }
                    }
                    Collections.sort(attributes, new Comparator<AttributeBean>() {
                        @Override
                        public int compare(AttributeBean atrB1, AttributeBean atrB2) {
                            return Integer.compare(atrB1.visualOrder, atrB2.visualOrder);
                        }
                    });
                }

                int attributeCnt = BaseUtils.collectionSizeFix(attributes);

                if (attributeCnt > 0) {
                    /* tylko dla drzewa meta ?*/
                    if (BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(getTreeKind())) {

                        for (AttributeBean atr : attributes) {
                            attrNameAndValue.put(atr.atrName, atr.atrLinValue);
                        }

                    }
                    for (AttributeBean atr : attributes) {
                        if (BIKClientSingletons.showAddStatus() && atr.isStatus) {
                            continue;
                        }

                        int atrVisualOrder = atr.visualOrder;

                        if (beforeDescr != (atrVisualOrder < 0)) {
                            continue;
                        }

                        String attrType = atr.typeAttr;
                        String atrLinValue = atr.atrLinValue;
                        String attrName = atr.atrName.indexOf('.') < 0 ? atr.atrName : atr.atrName.split("\\.")[1];
                        boolean displayAsNumber = atr.displayAsNumber;
                        Widget w = new Widget();
                        if (AttributeType.javascript.name().equals(attrType)) {
                            w = new HTML();
                            w.getElement().setId("chart-div");
                            w.setHeight("500px");
                        } else {
                            w = BIKClientSingletons.createAttributeView(attrType, atrLinValue, displayAsNumber);
                        }

                        if (AddOrEditAdminAttributeWithTypeDialog.AttributeType.longText.name().equals(attrType)) {
                            BIKClientSingletons.parseDynamicNodeDesc(nodeId, w);
                        }
                        if (AttributeType.javascript.name().equals(attrType)) {
                            final String script = atrLinValue;

                            if (widgetsActive) {
                                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                                    @Override
                                    public void execute() {
                                        ScriptInjector.fromString(script).setWindow(ScriptInjector.TOP_WINDOW).inject();
                                    }
                                });
                                BIKClientSingletons.visScript = "";
                            } else {//ct: pozakac jako MainHeaderWidget w Dialog
                                imhst.setColumnWidthForNonActiveWidget();
                                BIKClientSingletons.visScript = script;
                            }
                        }

                        //change atr.isPublic type to boolean
                        if (atr.isPublic == 1) {
                            if (((AttributeType.hyperlinkInMulti.name().equals(attrType) || AttributeType.permissions.name().equals(attrType))
                                    && !BaseUtils.isStrEmptyOrWhiteSpace(atrLinValue) /*|| AttributeType.hyperlinkInMono.name().equals(attrType)*/)) {
                                addRowWhenAttributeTypeIsHyperlinkInMulti(atr, atrLinValue, attrName, w, attrType, displayAsNumber, isShowAttributeButtonsOnTheLeft);
                            } else {
                                addRowOtherAttribute(atr, attrName, isShowAttributeButtonsOnTheLeft, w);
                            }
                        } else if (atr.isPublic == 0 && (checkNonPublicUserRights() || checkAuthorRights() || checkCreatorRights() || BIKClientSingletons.isEffectiveExpertLoggedIn())) {
                            if (((AttributeType.hyperlinkInMulti.name().equals(attrType) || AttributeType.permissions.name().equals(attrType))
                                    && !BaseUtils.isStrEmptyOrWhiteSpace(atrLinValue) /*|| AttributeType.hyperlinkInMono.name().equals(attrType)*/)) {
                                addRowWhenAttributeTypeIsHyperlinkInMulti(atr, atrLinValue, attrName, w, attrType, displayAsNumber, isShowAttributeButtonsOnTheLeft);
                            } else {
                                addRowOtherAttribute(atr, attrName, isShowAttributeButtonsOnTheLeft, w);
                            }
                        }
                    }
                }

                if (BIKConstants.NODE_KIND_META_PRINT_ATTRIBUT.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(nodeKindId))
                        || BIKConstants.NODE_KIND_META_PRINT_KIND.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(nodeKindId))
                        || BIKConstants.NODE_KIND_META_PRINTS_TEMPLATES.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(nodeKindId))
                        || BIKConstants.NODE_KIND_META_PRINT.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(nodeKindId))) {
                    actionBtn.setVisible(false);
                }

            }
        });
    }

    protected void addRowWhenAttributeTypeIsHyperlinkInMulti(AttributeBean atr, String atrLinValue, String attrName, Widget w, String attrType, boolean displayAsNumber, boolean isShowAttributeButtonsOnTheLeft) {
        List<Integer> links = new ArrayList<Integer>();
        List<String> linkValues = BaseUtils.splitBySep(atrLinValue, "|");
        boolean isFirstRow = true;
        for (String link : linkValues) {
            if (!BaseUtils.isStrEmptyOrWhiteSpace(link)) {
                List<Widget> actWidgets = new ArrayList<Widget>();
                links.add(BaseUtils.tryParseInteger(BaseUtils.splitBySep(link, "_").get(0)));
                AttributeBean atr2 = atr;
                atr2.atrLinValue = link;
                w = BIKClientSingletons.createAttributeView(attrType, link, displayAsNumber);
                if (widgetsActive) {

                    if (isShowAttributeButtonsOnTheLeft) {
                        addRowWhenAttributeTypeIsHyperlinkAndBtnShowOnOnTheLeft(attrName, actWidgets, atr, atrLinValue, link, w, isFirstRow);
                    } else {
                        addRowWhenAttributeTypeIsHyperlinkAndBtnShowOnOnTheRight(attrName, actWidgets, atr, atrLinValue, link, w, isFirstRow);
                    }
                } else {
                    if (!isFirstRow) {
                        actWidgets.add(new Label(""));
                        actWidgets.add(w);
                    }
                    imhst.setColumnWidthForNonActiveWidget();
                    imhst.addRow(attrName, atr.isVisible(), w/*atr.atrLinValue*/, actWidgets.toArray(new Widget[actWidgets.size()]));
                }
                if (isFirstRow) {
                    isFirstRow = false;
                    attrName = "";
                }
            }
        }
        usedLink.put(atr.atrName, links);
    }

    protected void addRowWhenAttributeTypeIsHyperlinkAndBtnShowOnOnTheLeft(String attrName, List<Widget> actWidgets, AttributeBean atr, String atrLinValue, String link, Widget w, boolean isFirstRow) {
        AttributeBean atrWithSplitLineValue = atr;
        atrWithSplitLineValue.atrLinValue = link;

        boolean isDeleteButtonEnable = checkAuthorRights() || isUserCreatedSpecifiedObject(atr) || BIKClientSingletons.isEffectiveExpertLoggedIn();
        boolean isEditButtonEnable = checkAuthorRights() || checkCreatorRights() || BIKClientSingletons.isEffectiveExpertLoggedIn();
        if (isFirstRow) {
            if (isDeleteButtonEnable) {
                actWidgets.add(createLinkDeleteButton(atrLinValue, nodeId, atrWithSplitLineValue.id, atrWithSplitLineValue.typeAttr, atrWithSplitLineValue.atrLinValue, atrWithSplitLineValue.displayAsNumber, atrWithSplitLineValue.isRequired));
            } else {
                actWidgets.add(new HTML(""));
            }
            if (isEditButtonEnable) {
                actWidgets.add(createEditButton(nodeId, atrWithSplitLineValue));
            } else {
                actWidgets.add(new HTML(""));
            }
        } else {
            actWidgets.add(new Label(""));
            actWidgets.get(0).getElement().setAttribute("width", "0");
            actWidgets.add(new Label(""));
            actWidgets.add(new Label(""));
        }

        HorizontalPanel hp = new HorizontalPanel();
        hp.add(createLookupButton(atrWithSplitLineValue));
        hp.add(createFollowButton(atrWithSplitLineValue));
        hp.add(w);
        if (!isFirstRow) {
            actWidgets.add(new Label(""));
            /* pusta nazwa atrybutu*/

            actWidgets.add(hp);
        }
        imhst.addRow(attrName, atr.isVisible(), hp/*atr.atrLinValue*/, actWidgets.toArray(new Widget[actWidgets.size()]));
    }

    protected void addRowWhenAttributeTypeIsHyperlinkAndBtnShowOnOnTheRight(String attrName, List<Widget> actWidgets, AttributeBean atr, String atrLinValue, String link, Widget w, boolean isFirstRow) {
        AttributeBean atrWithSplitLineValue = atr;
        atrWithSplitLineValue.atrLinValue = link;
        if (!isFirstRow) {
            actWidgets.add(new Label(""));
            actWidgets.add(w);
            actWidgets.add(createLookupButton(atr));
            actWidgets.add(createFollowButton(atrWithSplitLineValue));
            actWidgets.add(new Label(" "));
            actWidgets.add(new Label(" "));
        } else {
            actWidgets.add(createLookupButton(atr));
            actWidgets.add(createFollowButton(atrWithSplitLineValue));
            if (checkAuthorRights() || checkCreatorRights() || BIKClientSingletons.isEffectiveExpertLoggedIn()) {
                actWidgets.add(createEditButton(nodeId, atrWithSplitLineValue));
            }
            if (checkAuthorRights() || isUserCreatedSpecifiedObject(atr) || BIKClientSingletons.isEffectiveExpertLoggedIn()) {
                actWidgets.add(createLinkDeleteButton(atrLinValue, nodeId, atrWithSplitLineValue.id, atrWithSplitLineValue.typeAttr, atrWithSplitLineValue.atrLinValue, atrWithSplitLineValue.displayAsNumber, atrWithSplitLineValue.isRequired));
            }
        }
        imhst.addRow(attrName, atr.isVisible(), w/*atr.atrLinValue*/, actWidgets.toArray(new Widget[actWidgets.size()]));

    }

    protected void addRowOtherAttribute(AttributeBean atr, String attrName, boolean isShowAttributeButtonsOnTheLeft, Widget w) {

        boolean isInTemplatesPrintObjects = BIKConstants.NODE_KIND_META_PRINT_ATTRIBUT.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(nodeKindId))
                || BIKConstants.NODE_KIND_META_PRINT_KIND.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(nodeKindId));
        if (widgetsActive) {
            if (isShowAttributeButtonsOnTheLeft) {
                List<Widget> l = new ArrayList<Widget>();
                if (checkAuthorRights() || isUserCreatedSpecifiedObject(atr) || BIKClientSingletons.isEffectiveExpertLoggedIn()) {
                    l.add(createDeleteButton(nodeId, atr.id, atr.typeAttr, atr.atrLinValue, atr.displayAsNumber, atr.isRequired));
                } else {
                    l.add(new Label(""));
                }
                if (!isInTemplatesPrintObjects && (checkAuthorRights() || checkCreatorRights() || BIKClientSingletons.isEffectiveExpertLoggedIn())) {
                    l.add(createEditButton(nodeId, atr));
                } else {
                    l.add(new Label(""));
                }
                HorizontalPanel vp = new HorizontalPanel();
                if (AttributeType.hyperlinkInMono.name().equals(atr.typeAttr)) {
                    vp.add(createLookupButton(atr));
                    vp.add(createFollowButton(atr));
                } else {
                    Label emptyLbl = new Label(" ");
                    emptyLbl.setWidth("56px");
                    vp.add(emptyLbl);
                }

                vp.add(w);
//                imhst.addRow(atr.atrName.indexOf('.') < 0 ? atr.atrName : atr.atrName.split("\\.")[1], atr.isVisible(), w/*atr.atrLinValue*/, l.toArray(new Widget[l.size()]));
                imhst.addRow(attrName, atr.isVisible(), vp/*atr.atrLinValue*/, l.toArray(new Widget[l.size()]));
            } else {
                List<Widget> l = new ArrayList<Widget>();
                l.add(createLookupButton(atr));
                l.add(createFollowButton(atr));
                if (!isInTemplatesPrintObjects && (checkAuthorRights() || checkCreatorRights() || BIKClientSingletons.isEffectiveExpertLoggedIn())) {
                    l.add(createEditButton(nodeId, atr));
                }
                if (checkAuthorRights() || isUserCreatedSpecifiedObject(atr) || BIKClientSingletons.isEffectiveExpertLoggedIn()) {
                    l.add(createDeleteButton(nodeId, atr.id, atr.typeAttr, atr.atrLinValue, atr.displayAsNumber, atr.isRequired));
                }

//                imhst.addRow(atr.atrName.indexOf('.') < 0 ? atr.atrName : atr.atrName.split("\\.")[1], atr.isVisible(), w/*atr.atrLinValue*/, l.toArray(new Widget[l.size()]));
                imhst.addRow(attrName, atr.isVisible(), w/*atr.atrLinValue*/, l.toArray(new Widget[l.size()]));
            }
        } else {
            imhst.setColumnWidthForNonActiveWidget();
            imhst.addRow(attrName, atr.isVisible(), w/*atr.atrLinValue*/);
        }
    }

    protected boolean checkNonPublicUserRights() {
        return BIKClientSingletons.checkNonPublicUserRights(getData().node.treeId, getData().node.branchIds);
    }

    protected boolean checkAuthorRights() {
        return BIKClientSingletons.checkAuthorRights(getData().node.treeId, getData().node.branchIds);
    }

    protected boolean checkCreatorRights() {
        return BIKClientSingletons.checkCreatorRights(getData().node.treeId, getData().node.branchIds);
    }

    protected boolean isUserCreatedSpecifiedObject(AttributeBean atr) {
        return checkCreatorRights() && atr.creatorId == BIKClientSingletons.getLoggedUserBean().id;
    }

    public static String getHTMLInnerText(String atrLinValue) {
        int idxS = atrLinValue.indexOf(RENDER_AS_HTML_VAL_PREFIX);
        int idxL = atrLinValue.lastIndexOf(RENDER_AS_HTML_VAL_SUFFIX);
        return atrLinValue.substring(idxS + RENDER_AS_HTML_VAL_PREFIX.length(), idxL);
    }

    public static final String RENDER_AS_HTML_VAL_PREFIX = "<html>";
    public static final String RENDER_AS_HTML_VAL_SUFFIX = "</html>";

    @Override
    public Widget buildWidgets() {
        usedLink.clear();
        mainAttrVp = new VerticalPanel();
        if (!beforeDescr && widgetsActive) {

            mainAttrVp.setWidth("100%");
//            Window.alert(nodeKindId + " " + getData().node.nodeKindId);
            //widocznoscią guzika(isEditionEnabled) sterujemy w BIKClientSingleton.isNodeActionEnabledByCustomRightRoles
            if (!BIKClientSingletons.hideAddAttrBtnsOnRight() //
                    //                  && !(BIKConstants.NODE_KIND_META_PRINT_ATTRIBUT.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(nodenodeKindId))
                    //                || BIKConstants.NODE_KIND_META_PRINT_KIND.equalsIgnoreCase(BIKClientSingletons.getNodeKindCodeById(node.nodeKindId))
                    //                    )
                    ) {
                actionBtn = new BikCustomButton(I18n.dodajAtrybut.get() /* I18N:  */, "Attr" /* I18N: no */ + "-newAddAtrr",
                        new IContinuation() {
                    @Override
                    public void doIt() {
                        TreeNodeBean node = getData().node;
                        getForms().showAttributeForm(nodeId, node, null, node.treeId, BIKClientSingletons.filtrPublicAttributes(getData().node, getData().attributes), node.nodeKindId,
                                (BIKClientSingletons.isTreeKindDynamic(node.treeKind) && !BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(node.treeKind)), usedLink);
                    }
                });

                mainAttrVp.add(EntityDetailsWidgetFlatBase.createPostSpacing());
                mainAttrVp.add(actionBtn);

                mainAttrVp.add(EntityDetailsWidgetFlatBase.createPostSpacing());
            }
        }
        return mainAttrVp;
    }

    protected PushButton createLinkDeleteButton(final String atrLinValue, final int nodeId, final int paramNodeId, final String attrType, final String value, final boolean displayAsNumber, boolean isRequired) {
        PushButton button = new PushButton(new Image("images/trash_gray.gif" /* I18N: no */), new Image("images/trash_red.png" /* I18N: no */));
        button.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                HTML element = new HTML(
                        I18n.czyNaPewnoUsunacAtrybut.get() /* I18N:  */);
                element.setStyleName("komunikat" /* I18N: no:css-class */);
                VerticalPanel vp = new VerticalPanel();
                vp.add(element);
                vp.add(BIKClientSingletons.createAttributeView(attrType, value, displayAsNumber));
                new ConfirmDialog().buildAndShowDialog(vp, I18n.usun.get() /* I18N:  */, new IContinuation() {
                    @Override
                    public void doIt() {

                        final String atrLinValue2 = atrLinValue.replace(value + "|", "");
                        BIKClientSingletons.getService().editBikEntityAttribute(paramNodeId, nodeId, atrLinValue2,
                                new StandardAsyncCallback<Void>("Failure on" /* I18N: no */ + " deleteAttribute") {
                            @Override
                            public void onSuccess(Void result) {
                                if (BaseUtils.isStrEmptyOrWhiteSpace(atrLinValue2)) {
                                    BIKClientSingletons.getService().deleteBikLinkedAtr(nodeId, paramNodeId, value, new StandardAsyncCallback<Void>("Failure on" /* I18N: no */ + " deleteAttribute") {
                                        @Override
                                        public void onSuccess(Void result) {
                                            BIKClientSingletons.showInfo(I18n.usunietoAtrybut.get() /* I18N:  */);
                                            refreshAndRedisplayData();
                                        }
                                    });
                                } else {

                                    BIKClientSingletons.showInfo(I18n.usunietoAtrybut.get() /* I18N:  */);
                                    refreshAndRedisplayData();
                                }
                            }
                        });

//                                        new NodeRefreshingCallback<Void>(nodeId, I18n.edytowanoAtrybut.get() /* I18N:  */));
//                                BIKClientSingletons.getService().deleteBikLinkedAtr(nodeId, paramNodeId, value, new StandardAsyncCallback<Void>("Failure on" /* I18N: no */ + " deleteAttribute") {
//                                    @Override
//                                    public void onSuccess(Void result) {
//                                        BIKClientSingletons.showInfo(I18n.usunietoAtrybut.get() /* I18N:  */);
//                                        refreshAndRedisplayData();
//                                    }
//                                });
                    }
                });
            }
        });
        button.setStyleName("Attr" /* I18N: no */ + "-trashBtn");
//        button.setVisible(isAdminLoggedIn());
        //ww->authors: przerabiam na eksperta

        boolean isVIsible = isEditonEnabled() && !isRequired;
//        Window.alert(isVIsible + " " + isRequired + " " + isEditonEnabled());
        button.setVisible(isVIsible);
        button.setTitle(I18n.usun.get() /* I18N:  */);
        buttons.put(button, isRequired);
        return button;
    }

    protected PushButton createDeleteButton(final int nodeId, final int paramNodeId, final String attrType, final String value, final boolean displayAsNumber, boolean isRequired) {
        PushButton button = new PushButton(new Image("images/trash_gray.gif" /* I18N: no */), new Image("images/trash_red.png" /* I18N: no */));
        button.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                HTML element = new HTML(
                        I18n.czyNaPewnoUsunacAtrybut.get() /* I18N:  */);
                element.setStyleName("komunikat" /* I18N: no:css-class */);
                VerticalPanel vp = new VerticalPanel();
                vp.add(element);
                vp.add(BIKClientSingletons.createAttributeView(attrType, value, displayAsNumber));
                new ConfirmDialog().buildAndShowDialog(vp, I18n.usun.get() /* I18N:  */, new IContinuation() {
                    @Override
                    public void doIt() {
                        BIKClientSingletons.getService().deleteBikLinkedAtr(nodeId, paramNodeId, value, new StandardAsyncCallback<Void>("Failure on" /* I18N: no */ + " deleteAttribute") {
                            @Override
                            public void onSuccess(Void result) {
                                BIKClientSingletons.showInfo(I18n.usunietoAtrybut.get() /* I18N:  */);
                                refreshAndRedisplayData();
                            }
                        });

                    }
                });
            }
        });
        button.setStyleName("Attr" /* I18N: no */ + "-trashBtn");
//        button.setVisible(isAdminLoggedIn());
        //ww->authors: przerabiam na eksperta

        boolean isVIsible = isEditonEnabled() && !isRequired;
//        Window.alert(isVIsible + " " + isRequired + " " + isEditonEnabled());
        button.setVisible(isVIsible);
        button.setTitle(I18n.usun.get() /* I18N:  */);
        buttons.put(button, isRequired);
        return button;
    }

    protected Widget createLookupButton(AttributeBean attr) {
        if (!(AttributeType.hyperlinkInMulti.name().equals(attr.typeAttr) || AttributeType.permissions.name().equals(attr.typeAttr)
                || AttributeType.hyperlinkInMono.name().equals(attr.typeAttr))
                || (BaseUtils.isStrEmptyOrWhiteSpace(attr.atrLinValue))) {
            return new Label();
        }
        PushButton lookup = new PushButton(new Image("images/loupe.gif"));
//        lookup.addStyleName("imgButton");
        List<String> encodedValues = BaseUtils.splitBySep(attr.atrLinValue, ",");
        String nodeIdLink = encodedValues.get(0);
        final List<String> link = BaseUtils.splitBySep(nodeIdLink, "_");
        if (link.size() != 2) {
            Label l = new Label();
            l.setWidth("24px");
            return l;
        }
        lookup.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                EntityDetailsPaneDialog edpd = new EntityDetailsPaneDialog();
                edpd.buildAndShowDialog(BaseUtils.tryParseInteger(link.get(0)), null, link.get(1));
            }
        });

        lookup.setStyleName("Attr" /* I18N: no */ + "-lookupBtn");
        lookup.setTitle(I18n.podglad.get() /* I18N:  */);
        return lookup;
    }

    protected Widget createFollowButton(AttributeBean attr) {
        if (!(AttributeType.hyperlinkInMulti.name().equals(attr.typeAttr) || AttributeType.permissions.name().equals(attr.typeAttr)
                || AttributeType.hyperlinkInMono.name().equals(attr.typeAttr))
                || (BaseUtils.isStrEmptyOrWhiteSpace(attr.atrLinValue))) {
            return new Label();
        }
//        if (BaseUtils.isStrEmptyOrWhiteSpace(attr.atrLinValue)){
//            return new Label("");
//        }
        List<String> encodedValues = BaseUtils.splitBySep(attr.atrLinValue, ",");
        String nodeIdLink = encodedValues.get(0);

        final List<String> link2 = BaseUtils.splitBySep(nodeIdLink, "_");
        if (link2.size() != 2) {
            Label l = new Label();
            l.setWidth("32px");
            return l;
        }
        return createFollowButton(link2.get(1), BaseUtils.tryParseInt(link2.get(0)));
    }

    protected Widget createEditButton(final int nodeId, final AttributeBean attribute) {
        if (attribute.typeAttr.equals(AttributeType.calculation.name()) || attribute.typeAttr.equals(AttributeType.javascript.name())) {
            // nie ma możliwości edycji atrybutów wyliczanych
            return new Label();
        }

        if (attribute.atrName.equals(BIKConstants.METABIKS_ATTR_TYPE_ATTR)
                || attribute.atrName.equals(BIKConstants.METABIKS_ATTR_IS_REQUIRED)
                || attribute.atrName.equals(BIKConstants.METABIKS_ATTR_IS_EMPTY)) {
            if (BIKClientSingletons.showAddStatus() && attrNameAndValue.get(BIKConstants.METABIKS_ATTR_STATUS_NAME).equals(BIKConstants.YES)) {
                return new Label();
            }
        }

        PushButton editButton = createEditButtonForAttribute(nodeId, attribute);
        buttons.put(editButton, false);
        return editButton;
    }

    public PushButton createEditButtonForAttribute(final int nodeId1, final AttributeBean attribute) {

        IContinuation optOnClickCont = (super.isEditonEnabled() && attribute.isEditable) ? new IContinuation() {

            @Override
            public void doIt() {
                TreeNodeBean node = getData().node;
                getForms().showAttributeForm(nodeId1, node, attribute, node.treeId, BIKClientSingletons.filtrPublicAttributes(getData().node, getData().attributes), node.nodeKindId,
                        BIKClientSingletons.isTreeKindDynamic(node.treeKind) && !BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(node.treeKind), usedLink);
            }
        } : null;

        return BIKClientSingletons.createEditButtonForAttribute(optOnClickCont);
    }

    @Override
    public boolean isEditonEnabled() {
        return super.isEditonEnabled() && getData().countAttributeForKind > 0;
    }

//    public boolean isBtnEnable() {
//        return BIKClientSingletons.isNodeActionEnabledByCustomRightRoles(getOptNodeActionCode(), getData(), new ICheckEnabledByDesign() {
//
//            @Override
//            public boolean isEnabledByDesign() {
//                return isEditionEnabledByDesign();
//            }
//        });
//    }
    @Override
    protected boolean isEditionEnabledByDesign() {
        boolean res = BIKClientSingletons.isEffectiveExpertLoggedIn();

        return true;
    }
}
