/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.util.Set;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public class AppPropsBeanEx extends AppPropsBean {

    public Set<String> allowedShowDiagKindSet;
    public Set<String> disallowedShowDiagKindSet;
    public Set<String> openReportForNodeKindsSet;
    public DisabledUserApplyForAccountMsgsBean disabledUserApplyForAccountMsgsBean;

    public void calcExProps() {
        allowedShowDiagKindSet = BaseUtils.splitUniqueBySep(allowedShowDiagKinds, ",", true);
        disallowedShowDiagKindSet = BaseUtils.splitUniqueBySep(disallowedShowDiagKinds, ",", true);
        openReportForNodeKindsSet = BaseUtils.splitUniqueBySep(openReportForNodeKinds, ",", true);
        disabledUserApplyForAccountMsgsBean = new DisabledUserApplyForAccountMsgsBean(dumHeaderMsg, dumAvailableOptions, dumFooterMsg, dumMailTo, dumMailTitle, dumMailBody);
    }
}
