/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import commonlib.LameUtils;
import commonlib.ThreadedJobExecutor;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import pl.bssg.metadatapump.ad.ADPump;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.common.PumpGroupBean;
import pl.bssg.metadatapump.common.SchedulePumpBean;
import pl.bssg.metadatapump.confluence.ConfluencePump;
import pl.bssg.metadatapump.dqc.DQCPump;
import pl.bssg.metadatapump.filesystem.FileSystemPump;
import pl.bssg.metadatapump.jdbc.JdbcPump;
import pl.bssg.metadatapump.mssql.MsSqlPump;
import pl.bssg.metadatapump.oracle.OraclePump;
import pl.bssg.metadatapump.anySql.AnySqlPump;
import pl.bssg.metadatapump.plainfile.PlainFilePump;
import pl.bssg.metadatapump.postgresql.PostgreSQLPump;
import pl.bssg.metadatapump.profile.ProfilePump;
import pl.bssg.metadatapump.sapbo.designersdk.Designer4Pump;
import pl.bssg.metadatapump.sapbo.designersdk.DesignerPump;
import pl.bssg.metadatapump.sapbo.idtsdk.IDTPump;
import pl.bssg.metadatapump.sapbo.javasdk.SapBO4Pump;
import pl.bssg.metadatapump.sapbo.javasdk.SapBOPump;
import pl.bssg.metadatapump.sapbo.reportsdk.Report4Pump;
import pl.bssg.metadatapump.sapbo.reportsdk.ReportPump;
import pl.bssg.metadatapump.sapbw.SapBWPump;
import pl.bssg.metadatapump.teradata.TeradataPump;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.IContinuation;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class ThreadedPumpExecutor extends BasePumpExecutor implements Runnable {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    public static final long MILLIS_FOR_24H = 1000 * 60 * 60 * 24;
    public static final long MILLIS_FOR_MINUTE = 1000 * 60;
//    public static final long MILLIS_FOR_24H = 1000;
//    protected IBIKPumpTaskNew[] pumps;
    protected Set<String> crashedPumps = new LinkedHashSet<String>();
    protected List<Timer> timerList;
    protected Map<PumpGroupBean, List<Pair<String, String>>> pumpToSchedule; // Grupa -- > lista par <źródło, instancja>
    protected int actualPumpNumber;
    protected ThreadedJobExecutor jobExecutor;
    protected String dirForUpload;
    protected IContinuation doAfterVerticalize;

    public ThreadedPumpExecutor(String urlForPublicAccess, String dirForUpload,
            IContinuation doAfterVerticalize) {
        super(urlForPublicAccess);
        this.dirForUpload = dirForUpload;
        this.jobExecutor = new ThreadedJobExecutor();
        this.timerList = new ArrayList<Timer>();
        this.pumpToSchedule = new LinkedHashMap<PumpGroupBean, List<Pair<String, String>>>();
        this.doAfterVerticalize = doAfterVerticalize;
    }

    protected void shrinkLogsInDB() {
//        BeanConnectionUtils.execInAutoCommitMode(mc, new IContinuation() {
//
//            public void doIt() {
        execNamedCommand("shrinkDBLog");
//            }
//        });
    }

    protected void fixOneTimeSchedules() {
        execNamedCommand("fixOneTimeSchedules");
    }

    protected void addToCrashedPumps(String sourceGroup) {
        if (sourceGroup.equals(PumpConstants.PUMP_GROUP_BO)) { // tylko BO jest wieloetapowe!
            crashedPumps.add(sourceGroup);
        }
    }

    protected void verticalizeNodeAttrs() {
        try {
//            execNamedCommand("verticalizeNodeAttrs");
            if (doAfterVerticalize != null) {
                doAfterVerticalize.doIt();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new LameRuntimeException("Data load success, but error in indexing for search engine: " + e.getMessage());
        }
    }

    protected final void afterOnePump() {
        if (!isCancelled) {
            shrinkLogsInDB();
        }
        fixOneTimeSchedules();
    }

    protected final void afterLastPump() {
        if (!isCancelled) {
            verticalizeNodeAttrs();
        }
    }

    protected void executePumpTask(final List<IBIKPumpTaskNew> pumps) {
        actualPumpNumber = 0;
        for (final IBIKPumpTaskNew pump : pumps) {
            System.out.println(pump.getSourceName());
            if (isCancelled) {
                break;
            }

            actualPumpNumber++;
            final String pumpGroup = pump.getPumpGroup();
            if (!crashedPumps.contains(pumpGroup)) {
                try {
                    BeanConnectionUtils.execInAutoCommitMode(mc, new IContinuation() {
                        @Override
                        public void doIt() {
                            String source = "Undefined";
                            boolean withAction = true;
                            try {
                                actualLogId = pump.setConnectionAndLog(urlForPublicAccess, dirForUpload, mc, adhocDao, pLogger);
                                source = pump.getSourceName();
                                execNamedCommand("addOrUpdateSpidHistoryForDataSource", source);
                                withAction = pump.isActionAfterNeeded();
                                insertMemoryDiagsToLog(true, source, withAction);
                                Pair<Integer, String> pumpResult = pump.pump();
                                insertLogSizeToDB();
                                afterOnePump();
                                boolean isError = pumpResult.v1 == PumpConstants.LOG_LOAD_STATUS_ERROR;
                                if (actualPumpNumber == pumps.size() && !(pumps.size() == 1 && isError)) {
                                    pumpLog("Data indexing for search engine", 99);
                                    afterLastPump();
                                }
                                if (isError) {
                                    addToCrashedPumps(pumpGroup);
                                    updateLogsWithStatus(pumpResult.v2, PumpConstants.LOG_LOAD_STATUS_ERROR);
                                } else {
                                    insertLogToDB(pumpResult.v2);
                                    updateMainLog(pumpResult.v2, pumpResult.v1);
                                }
                            } catch (Exception e) {
                                if (logger.isErrorEnabled()) {
//                                logger.error("Error in Metadata data load. Message: " + e.getMessage());
                                    logger.error("Error in metadata data load", e);
                                }
                                addToCrashedPumps(pumpGroup);
                                if (!Thread.currentThread().isInterrupted() && !isCancelled) {
                                    updateLogsWithStatus(e.getMessage(), PumpConstants.LOG_LOAD_STATUS_ERROR);
                                }
                            } finally {
                                if (!Thread.currentThread().isInterrupted() && !isCancelled) {
                                    insertMemoryDiagsToLog(false, source, withAction);
                                }
//                            mc.finishWorkUnit(!Thread.currentThread().isInterrupted() && !isCancelled); // zwolnienie połączenia
                            }
                        }
                    });
                } finally {
                    mc.finishWorkUnit(!Thread.currentThread().isInterrupted() && !isCancelled); // zwolnienie połączenia
                }
            }

        }
        crashedPumps.clear();
    }

    public synchronized void setScheduler(List<SchedulePumpBean> beans) {
        System.out.println("ThreadedPumpExecutor.setScheduler. Instance: " + this.toString() + " List size: " + beans.size());
        for (Timer timer : timerList) {
            timer.cancel();
            timer.purge();
        }
        timerList.clear();
        pumpToSchedule.clear();
        for (final SchedulePumpBean scheduleBean : beans) {
            if (scheduleBean.isSchedule) {
                PumpGroupBean groupBean = new PumpGroupBean(PumpUtils.getStartedDateForSchedule(scheduleBean), scheduleBean.interval);
                String trimedSource = scheduleBean.source.trim();
                if (pumpToSchedule.containsKey(groupBean)) {
                    pumpToSchedule.get(groupBean).add(new Pair<String, String>(trimedSource, scheduleBean.instanceName));
                } else {
                    LinkedList<Pair<String, String>> tmpList = new LinkedList<Pair<String, String>>();
                    tmpList.add(new Pair<String, String>(trimedSource, scheduleBean.instanceName));
                    pumpToSchedule.put(groupBean, tmpList);
                }
            }
        }
        for (Map.Entry<PumpGroupBean, List<Pair<String, String>>> entry : pumpToSchedule.entrySet()) {
            final List<IBIKPumpTaskNew> schedulePumpList = new LinkedList<IBIKPumpTaskNew>();
            Timer timer = new Timer();
            for (Pair<String, String> source : entry.getValue()) {
                if (source.v1.equals(PumpConstants.SOURCE_NAME_TERADATA)) {
                    schedulePumpList.add(new GenericPseudoPump(TeradataPump.class));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_SAPBO)) {
                    schedulePumpList.add(new GenericPseudoPump(SapBOPump.class, source.v2));
                    schedulePumpList.add(new GenericPseudoPump(DesignerPump.class, source.v2));
                    schedulePumpList.add(new GenericPseudoPump(ReportPump.class, source.v2));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_SAPBO4)) {
                    schedulePumpList.add(new GenericPseudoPump(SapBO4Pump.class, source.v2));
                    schedulePumpList.add(new GenericPseudoPump(Designer4Pump.class, source.v2));
                    schedulePumpList.add(new GenericPseudoPump(IDTPump.class, source.v2));
                    schedulePumpList.add(new GenericPseudoPump(Report4Pump.class, source.v2));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_DQC)) {
                    schedulePumpList.add(new GenericPseudoPump(DQCPump.class));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_MSSQL)) {
                    schedulePumpList.add(new GenericPseudoPump(MsSqlPump.class, source.v2));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_ORACLE)) {
                    schedulePumpList.add(new GenericPseudoPump(OraclePump.class, source.v2));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_POSTGRESQL)) {
                    schedulePumpList.add(new GenericPseudoPump(PostgreSQLPump.class, source.v2));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_AD)) {
                    schedulePumpList.add(new GenericPseudoPump(ADPump.class));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_SAPBW)) {
                    schedulePumpList.add(new GenericPseudoPump(SapBWPump.class));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_PROFILE)) {
                    schedulePumpList.add(new GenericPseudoPump(ProfilePump.class));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_CONFLUENCE)) {
                    schedulePumpList.add(new GenericPseudoPump(ConfluencePump.class));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_JDBC)) {
                    schedulePumpList.add(new GenericPseudoPump(JdbcPump.class, source.v2));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_FILE_SYSTEM)) {
                    schedulePumpList.add(new GenericPseudoPump(FileSystemPump.class, source.v2));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_PLAIN_FILE)) {
                    schedulePumpList.add(new GenericPseudoPump(PlainFilePump.class, source.v2));
                } else if (source.v1.equals(PumpConstants.SOURCE_NAME_BIKS_SQL)) {
                    schedulePumpList.add(new GenericPseudoPump(AnySqlPump.class,source.v2));
                }
            }
            int interval = entry.getKey().interval;
            Date startDate = entry.getKey().startDate;
            TimerTask task = new TimerTask() {

                @Override
                public void run() {
                    addPumpToJobExecutorQueue(schedulePumpList);
                }
            };
            if (interval == 0 && startDate != null) {
                timer.schedule(task, startDate);
            } else if (interval > 0) {
                timer.scheduleAtFixedRate(task, startDate, MILLIS_FOR_MINUTE * interval);
            }
            timerList.add(timer);
        }
    }

    public void addPumpToJobExecutorQueue(final List<IBIKPumpTaskNew> pump, final IContinuation contWhenDone) {
        jobExecutor.addJob(new Runnable() {
            @Override
            public void run() {
                executePumpTask(pump);
                contWhenDone.doIt();
            }
        });
        System.out.println("jobExecutor.addJob(). Actual pendingJobCnt: " + jobExecutor.getPendingJobCount());
    }

    public void addPumpToJobExecutorQueue(final List<IBIKPumpTaskNew> pump) {
        addPumpToJobExecutorQueue(pump, new IContinuation() {

            @Override
            public void doIt() {
                System.out.println("After job done. Actual pendingJobCnt: " + jobExecutor.getPendingJobCount());
            }
        });
    }

    public INamedSqlsDAO<Object> getPumpAdhocDao() {
        return adhocDao;
    }

    public void setConnectionConfig(MssqlConnectionConfig cfg) {
        this.connConfg = cfg;
    }

    @Override
    public void run() {
        initExecutor();
        jobExecutor.run();
    }

    public void cancelStatementIfRunning() {
        isCancelled = true;
        if (threadHandle != null) {
            threadHandle.cancelExecution();
        }
    }

    public void destroy() {
        if (timerList != null) {
            for (Timer t : timerList) {
                t.cancel();
            }
        }
    }
}
