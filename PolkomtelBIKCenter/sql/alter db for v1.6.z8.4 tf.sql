﻿exec sp_check_version '1.6.z8.4';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

declare @fk_name sysname
select @fk_name = foreignKey_name from dbo.vw_ww_foreignkeys where Table_Name = 'bik_dqm_test' and name = 'db_server_id'

if(@fk_name is not null)
begin
	exec ('alter table bik_dqm_test drop constraint ' + @fk_name)
end;
go

declare @fk_error_name sysname
select @fk_error_name = foreignKey_name from dbo.vw_ww_foreignkeys where Table_Name = 'bik_dqm_test' and name = 'error_db_server_id'

if(@fk_error_name is not null)
begin
	exec ('alter table bik_dqm_test drop constraint ' + @fk_error_name)
end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_data_load_log]') and name = 'server_name')
begin
	alter table bik_data_load_log add server_name varchar(max) null;
end;
go


-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

exec sp_update_version '1.6.z8.4', '1.6.z8.5';
go
