﻿exec sp_check_version '1.8.0.20';
go
 
if not exists (select 1 from bik_app_prop where name = 'maxChildrenExtradataLevel')
insert into bik_app_prop(name, val) values ('maxChildrenExtradataLevel', '1')
else update bik_app_prop set val = '1' where name = 'maxChildrenExtradataLevel'


if not exists (select 1 from bik_app_prop where name = 'defaultPackSize')
insert into bik_app_prop(name, val) values ('defaultPackSize', '10000')
else update bik_app_prop set val = '10000' where name = 'defaultPackSize'

alter table bik_object_author alter column user_id int null
exec sp_update_version '1.8.0.20', '1.8.0.21';
go

