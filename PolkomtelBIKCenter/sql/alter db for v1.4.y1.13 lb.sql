﻿exec sp_check_version '1.4.y1.13';
go

-- [LISA] opisanie unikanym obj_id głównego węzła w drzewie słowników 

declare @treeId int
select @treeId = id from bik_tree where code ='DictionaryDWH';
declare @dict_node_id int = (select top 1 id from bik_node where is_deleted = 0 and tree_id = @treeId and name = 'Słowniki DWH' and parent_node_id is null)
update bik_node set is_built_in =1, obj_id = 'DWHDictionaryRoot' where id = @dict_node_id


exec sp_update_version '1.4.y1.13', '1.4.y1.14';
go
