/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.client.dialogs.AddOrUpdateItemNameDialog;
import pl.fovis.common.RoleForNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author beata
 * Brak powiązania roli z kategorią (brak attrCategoryId i attrCategoryName)
 * zostawione jeszcze zeby ewentualnie odkryć w Adminie tą zakładkę, ale wtedy jest ona do przerobienia
 * (wyświetla  role ,ale edycyjne rzeczy nie poprawione)
 */
public class CategorizedRoleForNodeDictWidget extends CategorizedDictionaryWidgetBase<RoleForNodeBean> {

    @Override
    public void displayData() {

        BIKClientSingletons.getService().getRoleForNode(new StandardAsyncCallback<List<RoleForNodeBean>>() {
            public void onSuccess(List<RoleForNodeBean> result) {
                result = deleteEmptyCategories(result);
                displayFetchedData(result);
            }
        });
    }

    protected List<RoleForNodeBean> deleteEmptyCategories(List<RoleForNodeBean> result) {
        return result;
    }

    @Override
    protected String getAddItemButtonCaption() {
        return I18n.dodajNowaRole.get() /* I18N:  */;
    }

    @Override
    protected void updateItemInCategory(int itemId, String name, int categoryId) {
        BIKClientSingletons.getService().updateRole(itemId, name, categoryId, new StandardAsyncCallback<Void>("Failed in updating role name" /* I18N: no:err-fail-in */) {
            public void onSuccess(Void result) {
                BIKClientSingletons.showInfo(I18n.zmodyfikowanoRole.get() /* I18N:  */);
                refreshData();
            }
        });
    }

    @Override
    protected Widget createOptCheckboxForCategoryWidget(int catId) {
        return null;
    }

    @Override
    protected Widget createOptCheckboxForItemWidget(int attrId) {
        return null;
    }

    @Override
    protected int getCategoryIdForItem(RoleForNodeBean item) {
        // bf:   -1 zeby się role pokazały, w zapytaniu w ad-hoc tez -1
        //(ogolnie już cała ta funkcjinalność jest niepotrzebna, bo zakładka nie pokazuje sie już w adminie, dlatego nie przerabiałam bardziej).
        return -1;//item.attrCategoryId;
    }

    @Override
    protected void refreshData() {
        displayData();
    }

    @Override
    protected String getCategoryNameForItem(RoleForNodeBean item) {
        return "";//item.attrCategoryName;
    }

    @Override
    protected String getItemNameForItem(RoleForNodeBean item) {
        return item.caption;
    }

    @Override
    protected void showDeleteItemConfirmDialog(final int itemId) {

        new ConfirmDialog().buildAndShowDialog(I18n.czyNaPewnoUsunacRoleWrazUsuwaszZ.get() /* I18N:  */ + ".",
                new IContinuation() {
            public void doIt() {

                BIKClientSingletons.getService().deleteRole(itemId, new StandardAsyncCallback<Void>("Failure on" /* I18N: no */ + " deleteRole") {
                    public void onSuccess(Void result) {
                        BIKClientSingletons.showInfo(I18n.usunietoRole.get() /* I18N:  */);
                        refreshData();
                    }
                });
            }
        });
    }

    protected void showAddItemInCategoryDialog(final int categoryId) {
        new AddOrUpdateItemNameDialog().buildAndShowDialog(getAddItemButtonCaption(), null,
                getItemsNames(/*new ArrayList<RoleForNodeBean>(items.values())*/),
                new IParametrizedContinuation<String>() {
            public void doIt(String param) {
                addItemInCategory(categoryId, param);
                refreshData();
            }
        });
    }

    protected void addItemInCategory(int categoryId, String itemName) {
        BIKClientSingletons.getService().addNewRole(itemName, categoryId, new StandardAsyncCallback<Void>("Failed in adding new role" /* I18N: no:err-fail-in */) {
            public void onSuccess(Void result) {
                BIKClientSingletons.showInfo(I18n.dodanoNowaRole.get() /* I18N:  */);
                refreshData();
            }
        });
    }
}
