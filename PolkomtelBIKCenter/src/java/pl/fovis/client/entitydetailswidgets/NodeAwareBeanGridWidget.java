/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import static pl.fovis.client.BIKClientSingletons.getViewType;
import pl.fovis.client.ViewType;
import pl.fovis.client.bikpages.BikTreeWidget;
import pl.fovis.client.bikpages.PageDataFetchBroker;
import pl.fovis.client.bikwidgets.NodeNamePathsAsynchFetcher;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.trzy0.foxy.commons.FoxyCommonConsts;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author beata
 */
public class NodeAwareBeanGridWidget<T> extends GenericBeanGridWidget<T> {

    public static final String STYLE_DART_AT = "dartAt";
    public static final String STYLE_DART_DOWN = "dartDown";
    protected IGridNodeAwareBeanPropsExtractor<T> nodeAwareExtr;
//    protected Set<Integer> alreadySentNodeIdsForNames = new HashSet<Integer>();
//    protected Map<Integer, String> nodeNamesById;
//    protected Map<T, HTML> postponedAncestorPaths = new HashMap<T, HTML>();
    protected final NodeNamePathsAsynchFetcher nnpaf;

    protected Set<Integer> nonEmptyActionIdxes;

    public NodeAwareBeanGridWidget(boolean widgetsActive, IGridNodeAwareBeanPropsExtractor<T> extr,
            Map<Integer, String> nodeNamesById) {
        super(widgetsActive, extr);

        //pg: niepotrzebne, inicjowaie z super this.widgetsActive = widgetsActive;
        //pg: tu jest niepotrzebe(?? ) zdublowanie extr, w super jest to trzymane, ale na innej zmiennej
        this.nodeAwareExtr = extr;
//        this.nodeNamesById = nodeNamesByIdnnpaf;
        this.nnpaf = new NodeNamePathsAsynchFetcher(nodeNamesById);
        extr.setDataChangedHandler(new IContinuation() {
            public void doIt() {
                dataChanged();
            }
        });
    }

    protected String currentNodeName;

    public NodeAwareBeanGridWidget(boolean isWidgetRunFromDialog, boolean widgetsActive, IGridNodeAwareBeanPropsExtractor<T> extr, Map<Integer, String> nodeNamesById, String currentNodeName) {
        this(widgetsActive, extr, nodeNamesById);
        this.currentNodeName = currentNodeName;
        this.isWidgetRunFromDialog = isWidgetRunFromDialog;
    }

    @Override
    public Widget buildGrid(final List<T> allBeans,
            final List<T> beansToDisplay,
            final int pageNum, int packSize) {
        this.beansToDisplay = beansToDisplay;
        this.pageNum = pageNum;
        this.packSize = packSize;

        nodeAwareExtr.setAllBeansList(allBeans);
        nodeAwareExtr.setUsingOnDialog(isWidgetRunFromDialog);
        innerBuildGrid();

        return ft;
    }

    protected void addArrowAndTreeToRow(final Integer itemNodeId, FlexTable gp, int gridIdx, int col, IGridNodeAwareBeanPropsExtractor<T> extr) {
        String[] fields;
        if (isWidgetRunFromDialog) {
            fields = new String[2];
            fields[0] = "name" /* I18N: no */ + "=10000:Nazwa";
            fields[1] = "follow" /* I18N: no */ + "=32:Przejdź";
        } else if (nonEmptyActionIdxes.size() == 3) { // założenie, że kosz jest na trzecim miejscu
            fields = new String[4];
            fields[0] = "name" /* I18N: no */ + "=10000:Nazwa";
            fields[1] = "view" /* I18N: no */ + "=32:Podgląd";
            fields[2] = "follow" /* I18N: no */ + "=32:Przejdź";
            fields[3] = "empty" /* I18N: no */ + "=32:Puste"; // jeśli gdzieś występuje kosz, należy dodać na końcu pustą kolumnę
        } else {
            fields = new String[3];
            fields[0] = "name" /* I18N: no */ + "=10000:Nazwa";
            fields[1] = "view" /* I18N: no */ + "=32:Podgląd";
            fields[2] = "follow" /* I18N: no */ + "=32:Przejdź";
        }
        final BikTreeWidget bbTree = new BikTreeWidget(false,
                "wwNewTreePopup wwNewTree",
                new PageDataFetchBroker() {
                    {
                        isFullDataFetched = true;
                    }
                }, isWidgetRunFromDialog, fields);
        final Widget treeGridWidget = bbTree.getTreeGridWidget();
        treeGridWidget.setVisible(false);
        final BikCustomButton arrowBtn = new BikCustomButton("&" + "nbsp" /* I18N: no */ + ";", STYLE_DART_AT, null);
        arrowBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().getChildrenNodeBeansByParentId(itemNodeId, new StandardAsyncCallback<List<TreeNodeBean>>("Error in" /* I18N: no */ + " getNodeBeanById()") {
                            public void onSuccess(List<TreeNodeBean> result) {
                                if (arrowBtn.getStyleName().equals(STYLE_DART_AT) && !bbTree.isTreeInitialized()) { // nie(do)rozwinięte drzewo
                                    bbTree.initWithFullData(result, true, null, null);
                                }
                                arrowBtn.setStyleName(arrowBtn.getStyleName().equals(STYLE_DART_AT) ? STYLE_DART_DOWN : STYLE_DART_AT);
                                treeGridWidget.setVisible(!treeGridWidget.isVisible());
                            }
                        });
            }
        });
        //tf: wrzucenie strzalki do kolumny 0,
        // nie nadpiszemy żadnego widgetu, ponieważ zaczynamy iterować się od 1
        //gp.getColumnFormatter().setWidth(0, "26px");
        setColWidth(gridIdx, 0, 20);
        gp.setWidget(gridIdx, 0, arrowBtn);

        //gp.setHTML(gridIdx + 1, col - 2, "&nbsp;"); //ww: &nbsp;
        setColWidth(gridIdx + 1, col - 2, 20);
        gp.setWidget(gridIdx + 1, col - 1, treeGridWidget); // dodanie drzewka w kolejnym wierszu
        gp.getFlexCellFormatter().setColSpan(gridIdx + 1, col - 1, 2 + extr.getActionCnt() + extr.getAddColCnt()); // scalenie komórek dla "drzewkowego" wiersza
//        gp.getFlexCellFormatter().setWidth(gridIdx + 1, col - 1, "100%");
        setColWidth(gridIdx + 1, col - 1, null);
    }

    @Override
    protected void addBeansToGrid(final FlexTable gp, int listIdx, List<T> beansToBeAddedToGrid,
            int gridIdx, int packSize) {

        int listSize = beansToBeAddedToGrid.size();
//        postponedAncestorPaths.clear();
//        final Set<Integer> missingNodeIds = new HashSet<Integer>();
        if (nodeAwareExtr.createGridColumnNames(gp)) {
            gridIdx++;
        }
        // sprawdzenie, czy jakiś wiersz ma dzieci (pokazuje się strzałka do rozwijania) i ustawia startowy index columnowy
        int listIdxTmp = listIdx;
        int startColumnIndex = 0;
        for (int i = 0; i < packSize && listIdxTmp < listSize; i++) {
            T e = beansToBeAddedToGrid.get(listIdxTmp++);
            if (nodeAwareExtr.hasChildren(e)) {
                startColumnIndex = 1;
                break;
            }
        }

        nonEmptyActionIdxes = new HashSet<Integer>();

        listIdxTmp = listIdx;
        for (int i = 0; i < packSize && listIdxTmp < listSize; i++) {
            T item = beansToBeAddedToGrid.get(listIdxTmp++);

            for (int actIdx = 0; actIdx < extr.getActionCnt(); actIdx++) {
                final IGridBeanAction<T> act = extr.getActionByIdx(actIdx);

                String actionIconUrl = act.getActionIconUrl(item);

                if (actionIconUrl != null && act.isVisible(item)) {
                    nonEmptyActionIdxes.add(actIdx);
                }
            }
        }
        for (int i = 0; i < packSize && listIdx < listSize; i++) {
            T e = beansToBeAddedToGrid.get(listIdx++);
            if (nodeAwareExtr.isRowVisible(e, gridIdx)) {// dla Użytkownicy systemu
                int gridIdxAfterCalculate = gridIdx * 2; // ilość wierszy jest podwajana, ponieważ w kolejnych mogą się znajdować drzewka do rozklikania
                createRowFromBean(e, gridIdxAfterCalculate, gp, nodeAwareExtr, startColumnIndex);
                String styleForRow = gridIdx % 2 == 0 ? "RelatedObj-oneObj-greyEx" : "RelatedObj-oneObj-whiteEx";
                if (nodeAwareExtr.hasSelectedRowStyle(e)) {
                    styleForRow = "RelatedObj-oneObj-selected";
                    BIKClientSingletons.requestId = null;
                }

                String optExtraStyleForRow = nodeAwareExtr.getOptExtraStyleForRow(e);
                if (optExtraStyleForRow != null) {
                    styleForRow += " " + optExtraStyleForRow;
                }

                gp.getRowFormatter().setStyleName(gridIdxAfterCalculate, styleForRow);
                gp.getRowFormatter().setStyleName(gridIdxAfterCalculate + 1, styleForRow);
                gridIdx++;
            }
        }

        nnpaf.fetchAndSetCollected();
//        setTitlesForNameWidgets(missingNodeIds);

//        missingNodeIds.removeAll(alreadySentNodeIdsForNames);
//
//        if (!missingNodeIds.isEmpty()) {
//            alreadySentNodeIdsForNames.addAll(missingNodeIds);
//            BIKClientSingletons.getService().getNodeNamesByIds(missingNodeIds, new StandardAsyncCallback<Map<Integer, String>>() {
//                public void onSuccess(Map<Integer, String> result) {
//                    alreadySentNodeIdsForNames.removeAll(missingNodeIds);
//                    nodeNamesById.putAll(result);
//                    setTitlesForNameWidgets(null);
//                }
//
//                @Override
//                public void onFailure(Throwable caught) {
//                    alreadySentNodeIdsForNames.removeAll(missingNodeIds);
//                    super.onFailure(caught);
//                }
//            });
//        }
    }

    public void createRowFromBean(final T item, int gridIdx, FlexTable gp, IGridNodeAwareBeanPropsExtractor<T> extr, int startColumnIndex) {
        //tf: specjalnie zaczynamy iterować się po kolumnach od wartości 1,
        // ponieważ na 0 może pojawić się strzałka do rozklikiwania węzłów.
        // Jeśli strzałka się nie pojawi, to i tak kolumna 0 nie będzie wyświetlana (bo nic tam nie ma)
        int col = startColumnIndex;
        if (extr.hasIcon()) {
            //gp.getColumnFormatter().setWidth(col, "26px");
            setColWidth(gridIdx, col, 26);
            gp.setWidget(gridIdx, col++, BIKClientSingletons.createIconImgEx(extr.getIconUrl(item), "16px", "16px",
                    extr.getOptIconHint(item), "RelatedObj-ObjIconEx"));
        }

        if (startColumnIndex > 0) {
            gp.setHTML(gridIdx, 0, "&" + "nbsp" /* I18N: no */ + ";");
        }

        String itemName = extr.getNameAsHtml(item);
        boolean isDescriptionInJoinedPanelVisible = BIKClientSingletons.isDescriptionInJoinedPanelVisible();
        boolean isWithoutName = false; // tf: jesli nie ma pola name to traktujemy pierwszą kolumne jako name i robimy ja szersza
        if (itemName != null) {
            final Integer itemNodeId = extr.getNodeId(item);

            //gp.getCellFormatter().setWidth(gridIdx, col, "100%");
            setColWidth(gridIdx, col, isDescriptionInJoinedPanelVisible ? 1500 : null); //t

            if (itemNodeId != null) {
                //Widget nameWidget = BIKClientSingletons.createHtmlLabelLookupButton(itemName, extr.getTreeCode(item), itemNodeId, null, widgetsActive);
                final HTML nameWidget = BIKClientSingletons.createHtmlLabelLookupButton(itemName, extr.getTreeCode(item), itemNodeId, null, true, null, !isWidgetRunFromDialog);
                if (extr.hasChildren(item)) {
                    addArrowAndTreeToRow(itemNodeId, gp, gridIdx, col, extr); // dodanie strzałki i drzewka w kolejnym wierszu
                }
                gp.setWidget(gridIdx, col++, nameWidget);

//                postponedAncestorPaths.put(item, nameWidget);
                nnpaf.setOrCollect(extr.getBranchIds(item), new IParametrizedContinuation<Pair<String, String>>() {

                    @Override
                    public void doIt(Pair<String, String> param) {
                        String ancestorPath = param.v2;
                        if (!BaseUtils.isStrEmptyOrWhiteSpace(ancestorPath)) {
                            if (nodeAwareExtr.hasNameHints()) {
                                // dodanie ścieżki do menu
//                    nameWidget.setTitle(ancestorPath);
                                String treeCode = nodeAwareExtr.getTreeCode(item);
                                String menuPathForTreeCode = BIKClientSingletons.getMenuPathForTreeCode(treeCode) + ancestorPath;
                                nameWidget.setTitle(menuPathForTreeCode);
                                if (BIKClientSingletons.isShowFullPathInJoinedObjsPane()) {
                                    nameWidget.setHTML(BIKClientSingletons.getTreeBeanByCode(treeCode).name + FoxyCommonConsts.RAQUO_STR_SPACED + ancestorPath);
                                }
                            }
                        }
                    }
                });

            } else {
                gp.setHTML(gridIdx, col++, itemName);
            }
        } else {
            isWithoutName = true;
        }
        for (int i = 0; i < extr.getAddColCnt(); i++) {
            Object addColVal = extr.getAddColVal(item, i);
            IsWidget val = getCellValAsWidget(addColVal);

            if (val instanceof Image) {
                //gp.getColumnFormatter().setWidth(col, "26px");
                setColWidth(gridIdx, col, 32);
            } else if (val instanceof InlineLabel && ((InlineLabel) val).getText().equals("")) {
                setColWidth(gridIdx, col, 32);
            } else if (isWithoutName) {
                setColWidth(gridIdx, col, 10000);
                isWithoutName = false;
            } else {

                int columnWidth = (isDescriptionInJoinedPanelVisible ? 18500 : 10000) / (extr.getAddColCnt() + extr.howManyTimesNameColumnWiderThanOther());
                setColWidth(gridIdx, col, columnWidth);
            }
            if (isDescriptionInJoinedPanelVisible && i == 0 && addColVal != null && itemName != null) {

                if (BIKClientSingletons.enableAttributeJoinedObjs() && getViewType() == ViewType.AttributesView) {
                    val = BIKClientSingletons.createAtrributeLookupButton(addColVal.toString(), extr.getJoinedObjId(item), currentNodeName, extr.getNameAsHtml(item)/* extr.getCurrentNodeName()*/, widgetsActive, !isWidgetRunFromDialog);
                } else {
                    val = BIKClientSingletons.createHtmlLabelLookupButton(addColVal.toString(), extr.getTreeCode(item), extr.getNodeId(item), null, true, null, !isWidgetRunFromDialog);
                }
            }

            gp.setWidget(gridIdx, col++, val);
        }

        for (int i = 0; i < extr.getActionCnt(); i++) {
            if (!nonEmptyActionIdxes.contains(i)) {
                continue;
            }

            final IGridBeanAction<T> act = extr.getActionByIdx(i);

            String actionIconUrl = act.getActionIconUrl(item);

            if (actionIconUrl != null && act.isVisible(item)) {
                gp.setWidget(gridIdx, col, createActionBtn(act, actionIconUrl, item));
                setColWidth(gridIdx, col, 32);
            } else {
                gp.setHTML(gridIdx, col, "&" + "nbsp" /* I18N: no */ + ";");
            }
            col++;
        }
    }

//    protected void setTitlesForNameWidgets(/*Collection<Integer> missingNodeIds*/) {
//        Set<T> successfulItems = new HashSet<T>();
//        for (Entry<T, HTML> postponedAncestorPath : postponedAncestorPaths.entrySet()) {
//            T item = postponedAncestorPath.getKey();
//            String branchIds = nodeAwareExtr.getBranchIds(item);
//            HTML nameWidget = postponedAncestorPath.getValue();
//
//            String ancestorPath = BIKCenterUtils.buildAncestorPathByNodeNames(branchIds, nodeNamesById,
//                    missingNodeIds);
//            if (!BaseUtils.isStrEmptyOrWhiteSpace(ancestorPath)) {
//                if (nodeAwareExtr.hasNameHints()) {
//                    // dodanie ścieżki do menu
////                    nameWidget.setTitle(ancestorPath);
//                    String treeCode = nodeAwareExtr.getTreeCode(item);
//                    String menuPathForTreeCode = BIKClientSingletons.getMenuPathForTreeCode(treeCode) + ancestorPath;
//                    nameWidget.setTitle(menuPathForTreeCode);
//                    if (BIKClientSingletons.isShowFullPathInJoinedObjsPane()) {
//                        nameWidget.setHTML(BIKClientSingletons.getTreeBeanByCode(treeCode).name + FoxyCommonConsts.RAQUO_STR_SPACED + ancestorPath);
//                    }
//                }
//                successfulItems.add(item);
//            }
//        }
//        postponedAncestorPaths.keySet().removeAll(successfulItems);
//    }
}
