
--select * from sys.objects where name like 'bik[_]%' and type = 'u'

if  exists (select * from sys.objects where object_id = object_id(N'[dbo].[fn_index_info]') and type in (N'fn', N'if', N'tf', N'fs', N'ft'))
drop function [dbo].[fn_index_info]
go

create function [dbo].[fn_index_info] (@table_name_patt varchar(max))
returns @tab table (table_name sysname not null, index_name sysname not null, cols varchar(max) not null, include_cols varchar(max) null, 
  idx_level tinyint not null, has_identity_col tinyint not null, has_fk_col tinyint not null)
as
begin

  declare @fk_cols table (table_name sysname not null, col_name sysname not null, unique (table_name, col_name));
  
  insert into @fk_cols
  select distinct table_name, name from dbo.vw_ww_foreignkeys where Table_Name like @table_name_patt;

  declare @cur cursor;

  set @cur = cursor for 
  select 
       tablename = t.name,
       indexname = ind.name,
       --indexid = ind.index_id,
       --columnid = ic.index_column_id,
       columnname = col.name,
       --ind.*,
       --ic.*,
       --col.*
       col.is_identity,
       ind.is_primary_key,
       ind.is_unique,
       ind.is_unique_constraint,
       ic.is_included_column
  from 
       sys.indexes ind 
  inner join 
       sys.index_columns ic on  ind.object_id = ic.object_id and ind.index_id = ic.index_id 
  inner join 
       sys.columns col on ic.object_id = col.object_id and ic.column_id = col.column_id 
  inner join 
       sys.tables t on ind.object_id = t.object_id 
  where 
       --ind.is_primary_key = 1
       --and ind.is_unique = 0 
       --and ind.is_unique_constraint = 0 
       -- and 
       t.is_ms_shipped = 0 
       --and t.name = 'bik_fts_stemmed_word'
       and t.name like @table_name_patt
       --and (ind.is_unique = 1 or ind.is_unique_constraint = 1)
  order by 
       t.name, ind.name, ind.index_id, ic.index_column_id 
  ;

  open @cur;

  declare @table_name sysname, @index_name sysname, @col_name sysname, @is_identity tinyint,
    @is_primary_key tinyint,
    @is_unique tinyint,
    @is_unique_constraint tinyint, @is_included_column tinyint;
  
  declare @is_fk_col tinyint, @prev_table_name sysname = '', @prev_index_name sysname = '', @idx_col_names varchar(max) = null,
    @include_cols varchar(max) = null, @idx_level tinyint = null, @has_identity_col tinyint = null,
    @has_fk_col tinyint = null;

  fetch next from @cur into @table_name, @index_name, @col_name, @is_identity, @is_primary_key,
    @is_unique, @is_unique_constraint, @is_included_column;

  while @@fetch_status = 0
  begin
    if @prev_table_name <> @table_name or @prev_index_name <> @index_name
    begin
      if @idx_col_names is not null
      begin
        --print @prev_table_name + ' <' + @prev_index_name + '> (' + @idx_col_names + ')';
        insert into @tab (table_name, index_name, cols, include_cols, idx_level, has_identity_col, has_fk_col)
        values (@prev_table_name, @prev_index_name, @idx_col_names, @include_cols, @idx_level, @has_identity_col, @has_fk_col);
      end;
            
      select @idx_col_names = null, @include_cols = null, @has_identity_col = 0, @has_fk_col = 0;
      
      set @idx_level = case when @is_primary_key <> 0 then 3
                            when @is_unique_constraint <> 0 then 2
                            when @is_unique <> 0 then 1
                            else 0 end;
    end;
    
    if @is_included_column = 0
    begin   
      set @idx_col_names = case when @idx_col_names is null then '' else @idx_col_names + ', ' end + @col_name;

      set @is_fk_col = case when exists(select 1 from @fk_cols where table_name = @table_name and col_name = @col_name) then 1 else 0 end;
      
      if @is_fk_col <> 0 set @has_fk_col = 1;
      
      if @is_identity <> 0 set @has_identity_col = 1;    
    end
    else
      set @include_cols = case when @include_cols is null then '' else @include_cols + ', ' end + @col_name;
    
    select @prev_table_name = @table_name, @prev_index_name = @index_name;

    fetch next from @cur into @table_name, @index_name, @col_name, @is_identity, @is_primary_key,
      @is_unique, @is_unique_constraint, @is_included_column;
  end;

  --if @idx_col_names <> '' print @prev_table_name + ' <' + @prev_index_name + '> (' + @idx_col_names + ')';
  insert into @tab (table_name, index_name, cols, include_cols, idx_level, has_identity_col, has_fk_col)
  values (@prev_table_name, @prev_index_name, @idx_col_names, @include_cols, @idx_level, @has_identity_col, @has_fk_col);
  
  return;
end;
go


select * from dbo.fn_index_info('bik[_]%')
where idx_level > 0 and has_identity_col = 0 and has_fk_col = 0
order by table_name, idx_level desc;
 
  -- case when idx_level = 3 then 1 else 0 end <> has_identity_col;

select * from bik_keyword


select * from bik_right_role


select * from bik_icon

select * from vw_ww_foreignkeys
where ReferencedTableName = 'bik_attr_category'


select * from bik_searchable_attr_props



select * from bik_attr_category


select * from bik_attr_def

select * from bik_attr_hint


select * from bik_help


select * from bik_statistic_dict


select * from bik_data_source_def


select * from bik_attr_def


select * from bik_node_kind

select * from bik_tree_kind tk inner join bik_tree t on tk.code = t.tree_kind


select * from bik_tutorial


select * from bik_active_directory

