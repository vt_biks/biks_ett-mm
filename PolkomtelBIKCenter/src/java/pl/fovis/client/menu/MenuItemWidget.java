/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.menu;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import pl.fovis.foxygwtcommons.ListItem;

/**
 *
 * @author BSSG-Stacjonarny
 */
public class MenuItemWidget extends ListItem {

    protected Anchor anc;

    public MenuItemWidget(String itemCaption, ClickHandler clickHandler) {
        anc = new Anchor(itemCaption);
        add(anc);
        anc.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                anc.setFocus(false);
            }
        });
        anc.addClickHandler(clickHandler);
    }

    public void setActive(boolean val) {
        setStyleName("active" /* I18N: no */, val);
        anc.setStyleName("active" /* I18N: no */, val);
    }

    public Anchor getAnchor() {
        return anc;
    }
}