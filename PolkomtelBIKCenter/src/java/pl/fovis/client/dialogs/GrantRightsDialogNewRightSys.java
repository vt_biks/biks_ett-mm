/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author ctran
 */
public class GrantRightsDialogNewRightSys extends BaseActionOrCancelDialog {

    protected TreeNodeBean node;
    protected Map<CheckBox, SystemUserBean> userBeanMap = new HashMap<CheckBox, SystemUserBean>();
    protected ArrayList<SystemUserBean> resultUsersList = new ArrayList<SystemUserBean>();
    protected ArrayList<SystemUserBean> resultUsersListToRemove = new ArrayList<SystemUserBean>();
    protected IParametrizedContinuation<List<SystemUserBean>> saveContUsers;
    protected SystemUserBean currUser = BIKClientSingletons.getLoggedUserBean();
    protected HorizontalPanel hpUsers = new HorizontalPanel();
    protected VerticalPanel vpUsersLeft = new VerticalPanel();
    protected VerticalPanel vpUsersMid = new VerticalPanel();
    protected VerticalPanel vpUsersRight = new VerticalPanel();
    protected VerticalPanel usersCheckBoxPanel = new VerticalPanel();
    protected VerticalPanel usersCheckBoxPanelLeft = new VerticalPanel();
    protected VerticalPanel usersCheckBoxPanelMid = new VerticalPanel();
    protected VerticalPanel usersCheckBoxPanelRight = new VerticalPanel();
    protected TextBox searchTxtBx = new TextBox();
    protected Label userNamesLbl;
    protected HTML htmlDesc;
    protected HTMLPanel htmlPanel;
    protected List<SystemUserBean> allUsers;
    protected SystemUserGroupBean sysUserGroupBean;
    protected boolean isHtmlEmpty = true;

    public GrantRightsDialogNewRightSys(TreeNodeBean node, SystemUserGroupBean sysUserGroupBean, List<SystemUserBean> allUsers,
            IParametrizedContinuation<List<SystemUserBean>> saveContUsers) {
        this.node = node;
        this.htmlDesc = new HTML(sysUserGroupBean.description);
        this.sysUserGroupBean = sysUserGroupBean;
        this.allUsers = allUsers;
        this.saveContUsers = saveContUsers;
    }

    @Override
    protected String getDialogCaption() {
        if (BIKClientSingletons.isAppAdminLoggedIn() || BIKClientSingletons.isSysAdminLoggedIn()) {
            return I18n.nadajUprawnieniaDlaRoli.get() + sysUserGroupBean.name;
        } else {
            return I18n.podgladUprawnien.get() + sysUserGroupBean.name;
        }
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {

        htmlPanel = new HTMLPanel("");
        htmlPanel.add(htmlDesc);
        ScrollPanel scHtml = new ScrollPanel();
        scHtml.add(htmlPanel);
        scHtml.setHeight("250px");
        htmlDesc.setStyleName("BizDef-descr");
        FlexTable ft = new FlexTable();
        int row = 0;
        ft.setWidget(row++, 0, scHtml);

        FlexTable ftLblAndSearch = new FlexTable();
        FlexTable ftSearch = new FlexTable();
        ftLblAndSearch.setWidget(0, 1, ftSearch);

        ft.setWidget(row++, 0, new HTML("<hr color = #8faed6>"));
        ft.setWidget(row++, 0, ftLblAndSearch);

        userNamesLbl = new Label(I18n.userNames.get() + ":");
        userNamesLbl.setWidth("520px");
        ftLblAndSearch.setWidget(0, 0, userNamesLbl);

        searchTxtBx.addKeyDownHandler(new KeyDownHandler() {

            @Override
            public void onKeyDown(KeyDownEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    seekForACoincidences();
                }
            }
        });
        ftSearch.setWidget(0, 0, searchTxtBx);
        searchTxtBx.setWidth("350px");
        PushButton searchBtn = new PushButton(new Image("images/searchBtn1.png"));
        searchBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                seekForACoincidences();
            }
        });

        ftSearch.setWidget(0, 1, searchBtn);

        ScrollPanel sp2 = new ScrollPanel(hpUsers);

        ft.setWidget(row++, 0, sp2);
        sp2.setHeight("350px");
        ft.setWidth("920px");

        displayUsers(allUsers);

//        if (!resultUsersList.isEmpty()) {
        super.actionBtn.setEnabled(false);
//        }
        main.add(ft);
    }

    public void addPanels() {
        hpUsers.setSpacing(2);
        hpUsers.add(vpUsersLeft);
        vpUsersLeft.setWidth("305px");
        hpUsers.add(vpUsersMid);
        vpUsersMid.setWidth("305px");
        hpUsers.add(vpUsersRight);
        vpUsersRight.setWidth("304px");
    }

    //TODO: search Doesnt Work
    protected void seekForACoincidences() {
        if (searchTxtBx.getValue().isEmpty()) {
            displayUsers(allUsers);
        } else {
            hpUsers.clear();
            List<SystemUserBean> tempSearchUserList = new ArrayList<SystemUserBean>();
            String enteredString = searchTxtBx.getValue();
            enteredString = enteredString.toLowerCase();

            for (SystemUserBean sub : allUsers) {
                if (sub != null) {
                    String userName = (sub.userName != null ? sub.userName : sub.loginName).toLowerCase();

                    if (userName.contains(enteredString)) {
                        tempSearchUserList.add(sub);
                    }
                }
            }
            displayUsers(tempSearchUserList);
        }
    }

    protected void displayUsers(List<SystemUserBean> allUsers) {
        hpUsers.clear();
        vpUsersLeft.clear();
        vpUsersMid.clear();
        vpUsersRight.clear();
        usersCheckBoxPanelLeft.clear();
        usersCheckBoxPanelMid.clear();
        usersCheckBoxPanelRight.clear();
        int splitter = allUsers.size() / 3;
        List<SystemUserBean> leftUserList = allUsers.subList(0, splitter + 1);
        List<SystemUserBean> midUserList = allUsers.subList(splitter + 1, splitter * 2 + 1);
        List<SystemUserBean> rightUserList = allUsers.subList(splitter * 2 + 1, allUsers.size());
        if (allUsers.size() == 2 || allUsers.size() == 3) {
            leftUserList = allUsers;
            midUserList = null;
            rightUserList = null;
        }
        if (!allUsers.isEmpty()) {
            if (!leftUserList.isEmpty()) {
                fillLeftUserPanel(leftUserList);
            }
            if (!midUserList.isEmpty()) {
                fillMidUserPanel(midUserList);
            }
            if (!rightUserList.isEmpty()) {
                fillRightUserPanel(rightUserList);
            }
        } else {
            usersCheckBoxPanelLeft.add(new Label(I18n.brakUzytkownikow.get()));
        }
        vpUsersLeft.add(usersCheckBoxPanelLeft);
        vpUsersMid.add(usersCheckBoxPanelMid);
        vpUsersRight.add(usersCheckBoxPanelRight);
        addPanels();
    }

    protected void fillLeftUserPanel(List<SystemUserBean> leftPanelUsers) {
        for (SystemUserBean sub : leftPanelUsers) {
            if (sub != null) {
                if (currUser.id != sub.id) {
                    final CheckBox cbUser = new CheckBox(sub.userName == null ? sub.loginName : sub.userName);
                    userBeanMap.put(cbUser, sub);
                    if (sub.userHasRightsInCurrentNode) {
                        cbUser.setValue(true);
                        resultUsersList.add(sub);
                    }
                    cbUser.addValueChangeHandler(addCustomValueChangeHandler(cbUser));
                    usersCheckBoxPanelLeft.add(cbUser);
                    if (!checkIfUserIsAdmin()) {
                        cbUser.setEnabled(false);
                    }
                }
            }
        }
    }

    protected void fillMidUserPanel(List<SystemUserBean> midPanelUsers) {
        for (SystemUserBean sub : midPanelUsers) {
            if (sub != null) {
                if (currUser.id != sub.id) {
                    final CheckBox cbUser = new CheckBox(sub.userName == null ? sub.loginName : sub.userName);
                    userBeanMap.put(cbUser, sub);
                    if (sub.userHasRightsInCurrentNode) {
                        cbUser.setValue(true);
                        resultUsersList.add(sub);
                    }
                    cbUser.addValueChangeHandler(addCustomValueChangeHandler(cbUser));
                    usersCheckBoxPanelMid.add(cbUser);
                    if (!checkIfUserIsAdmin()) {
                        cbUser.setEnabled(false);
                    }
                }
            }
        }
    }

    protected void fillRightUserPanel(List<SystemUserBean> rightPanelUsers) {
        for (SystemUserBean sub : rightPanelUsers) {
            if (sub != null) {
                if (currUser.id != sub.id) {
                    final CheckBox cbUser = new CheckBox(sub.userName == null ? sub.loginName : sub.userName);
                    userBeanMap.put(cbUser, sub);
                    if (sub.userHasRightsInCurrentNode) {
                        cbUser.setValue(true);
                        resultUsersList.add(sub);
                    }
                    cbUser.addValueChangeHandler(addCustomValueChangeHandler(cbUser));
                    usersCheckBoxPanelRight.add(cbUser);
                    if (!checkIfUserIsAdmin()) {
                        cbUser.setEnabled(false);
                    }
                }
            }
        }
    }

    protected ValueChangeHandler<Boolean> addCustomValueChangeHandler(final CheckBox cbU) {
        return new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                SystemUserBean bean = userBeanMap.get(cbU);
                if (event.getValue()) {
                    actionBtn.setEnabled(true);
                    resultUsersList.add(bean);
                    if (bean.userHasRightsInCurrentNode && !resultUsersListToRemove.isEmpty()) {
                        resultUsersListToRemove.remove(bean);
                    }
                } else {
                    if (resultUsersList.contains(bean)) {
                        resultUsersList.remove(bean);
                    }
                    if (bean.userHasRightsInCurrentNode) {
                        resultUsersListToRemove.add(bean);
                    }
                    actionBtn.setEnabled(true);
                }
            }
        };
    }

    protected boolean checkIfUserIsAdmin() {
        return BIKClientSingletons.isSysAdminLoggedIn() || BIKClientSingletons.isAppAdminLoggedIn();
    }

    @Override
    protected void doAction() {
        if (resultUsersList.isEmpty()) {
            BIKClientSingletons.showWarning(I18n.zaznaczUzytkownikow.get());
        } else {

            ArrayList<SystemUserBean> tempUsersList = new ArrayList<SystemUserBean>();
            tempUsersList.addAll(resultUsersList);
            for (SystemUserBean sub : tempUsersList) {
                if (sub.userHasRightsInCurrentNode) {
                    resultUsersList.remove(sub);
                }
                sub.userHasRightsInCurrentNode = false;
            }
            resultUsersList.addAll(resultUsersListToRemove);
            saveContUsers.doIt(resultUsersList);
        }
    }
}
