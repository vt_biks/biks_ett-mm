-- select * from bik_node_kind 
IF OBJECT_ID('tempdb..#misia_jest_masta_raporty_ze_sciezka_chce_i_basta') IS NOT NULL 
drop table #misia_jest_masta_raporty_ze_sciezka_chce_i_basta
go

select bn.id, bn.name, bnk.code as kind_code, bnk.caption as kind_caption, cast('' as varchar(max)) as parent_path, bn.parent_node_id as ancestor_id
into #misia_jest_masta_raporty_ze_sciezka_chce_i_basta
from bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
where 
bn.is_deleted = 0 and bn.linked_node_id is null and
bnk.code in ('Webi') -- je�eli potrzebujesz wi�cej typ�w raport�w - dopisujesz je tu, typy znajdziesz tu: select * from bik_node_kind 
go

while 1 = 1 begin
  declare @rc int = null

  update #misia_jest_masta_raporty_ze_sciezka_chce_i_basta
  set parent_path = bn.name + '\' + parent_path, ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #misia_jest_masta_raporty_ze_sciezka_chce_i_basta.ancestor_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go

select mjm.name, mjm.parent_path, mjm.parent_path + mjm.name as full_path, mjm.kind_code, mjm.kind_caption
  , unin.name as universe_name
from #misia_jest_masta_raporty_ze_sciezka_chce_i_basta mjm
left join (bik_joined_objs jo
inner join bik_node unin on unin.id = jo.dst_node_id and unin.is_deleted = 0
inner join bik_node_kind unink on unin.node_kind_id = unink.id and unink.code = 'Universe'
) on mjm.id = jo.src_node_id
--where unin.name is not null
