/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wezyr
 */
public class TextFilePositionTest {

    public TextFilePositionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMoveTo() {
        String txt = "ala ma kota\na kot co?\n";
        TextFilePosition init = TextFilePosition.initialPos("a");
        TextFilePosition p0 = TextFilePosition.moveTo(init, txt, 1);
        TextFilePosition p1 = TextFilePosition.moveTo(init, txt, 11);
        TextFilePosition p2 = TextFilePosition.moveTo(init, txt, 12);
        TextFilePosition p3 = TextFilePosition.moveTo(init, txt, 13);
        assertEquals(new Pair<Integer, Integer>(0, 1), p0.getRowAndCol());
        assertEquals(new Pair<Integer, Integer>(0, 11), p1.getRowAndCol());
        assertEquals(new Pair<Integer, Integer>(1, 0), p2.getRowAndCol());
        assertEquals(new Pair<Integer, Integer>(1, 1), p3.getRowAndCol());
    }
}