/*
 * StrUtils.java
 *
 * Created on 21 wrzesień 2007, 11:52
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author wezyr
 */
public class StrUtils {

    private StrUtils() {
    }

    public static Pair<Integer, Integer> leftSubstringCalcPos(int pos, int cnt, boolean includeCharAtPos) {
        int incFix = includeCharAtPos ? 1 : 0;
        int startPos = Math.max(pos - cnt + incFix, 0);
        int endPos = pos + incFix;
        return new Pair<Integer, Integer>(startPos, endPos);
    }

    public static String leftSubstring(String str, int pos, int cnt, boolean includeCharAtPos) {
        Pair<Integer, Integer> posPair = leftSubstringCalcPos(pos, cnt, includeCharAtPos);
        return str.substring(posPair.v1, posPair.v2);
    }

    public static String leftSubstring(StringBuilder sb, int pos, int cnt, boolean includeCharAtPos) {
        Pair<Integer, Integer> posPair = leftSubstringCalcPos(pos, cnt, includeCharAtPos);
        return sb.substring(posPair.v1, posPair.v2);
    }

    public static String capitalize(String s) {
        if (BaseUtils.isStrEmpty(s)) {
            return s;
        }
        return Character.toLowerCase(s.charAt(0)) + s.substring(1);
    }

    public static String leftStr(String s, int charCnt) {
        if (s == null) {
            return null;
        }
        if (s.length() < charCnt) {
            return s;
        }
        return s.substring(0, charCnt);
    }
}
