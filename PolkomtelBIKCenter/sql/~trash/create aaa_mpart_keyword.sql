/****** Object:  Table [dbo].[aaa_mpart_keyword]    Script Date: 03/10/2011 11:14:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[aaa_mpart_keyword](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mpart_id] [int] NOT NULL,
	[keyword_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uniq2] UNIQUE NONCLUSTERED 
(
	[mpart_id] ASC,
	[keyword_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[aaa_mpart_keyword]  WITH CHECK ADD FOREIGN KEY([keyword_id])
REFERENCES [dbo].[aaa_keyword] ([id])
GO

ALTER TABLE [dbo].[aaa_mpart_keyword]  WITH CHECK ADD FOREIGN KEY([mpart_id])
REFERENCES [dbo].[aaa_mpart] ([id])
GO


