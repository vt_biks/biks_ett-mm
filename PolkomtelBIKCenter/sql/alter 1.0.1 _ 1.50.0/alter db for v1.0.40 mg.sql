exec sp_check_version '1.0.40';

alter table bik_node_kind
add icon_name varchar(255) null
go

update bik_node_kind
set icon_name = 'webi'
where code = 'Webi'
go

update bik_node_kind
set icon_name = 'flash'
where code = 'Flash'
go

update bik_node_kind
set icon_name = 'universes'
where code = 'Universe'
go

update bik_node_kind
set icon_name = 'connection'
where code = 'MetaData.DataConnection'
go

update bik_node_kind
set icon_name = 'bfolder'
where code = '_BIZAREA_'
go

update bik_node_kind
set icon_name = 'folder'
where code = 'Folder'
go

update bik_node_kind
set icon_name = 'folder'
where code = 'DefaultFolder'
go

update bik_node_kind
set icon_name = 'scheme'
where code = 'SCHEMA'
go

update bik_node_kind
set icon_name = 'table'
where code = 'TABLE'
go

update bik_node_kind
set icon_name = 'attributes'
where code = 'COLUMN'
go

update bik_node_kind
set icon_name = 'table'
where code = 'VIEW'
go

update bik_node_kind
set icon_name = 'views'
where code = 'PROCEDURE'
go

update bik_node_kind
set icon_name = 'tezaurus'
where code = 'Tezaurus'
go

update bik_node_kind
set icon_name = 'key'
where code = 'Keyword'
go

update bik_node_kind
set icon_name = 'folder'
where code = 'TezaurusCategory'
go

update bik_node_kind
set icon_name = 'report'
where code = 'Document'
go

update bik_node_kind
set icon_name = 'userSpecialist'
where code = 'User'
go

update bik_node_kind
set icon_name = 'users_icon'
where code = 'UsersGroup'
go

update bik_node_kind
set icon_name = 'blog_icon'
where code = 'Blog'
go

update bik_node_kind
set icon_name = 'blogw_icon'
where code = 'BlogEntry'
go

alter table bik_tree
add tree_kind_code varchar(255) null
go

update bik_tree
set tree_kind_code = 'Metadata'
where tree_kind = 0
go

update bik_tree
set tree_kind_code = 'Taxonomy'
where tree_kind = 1
go

update bik_tree
set tree_kind_code = 'Tezaurus'
where tree_kind = 2
go

update bik_tree
set tree_kind_code = 'Documents'
where tree_kind = 3
go

update bik_tree
set tree_kind_code = 'Users'
where tree_kind = 4
go

update bik_tree
set tree_kind_code = 'Blogs'
where tree_kind = 5
go

alter table bik_tree
drop constraint CK_bik_tree_tree_kind
go


alter table bik_tree
drop column tree_kind
go

alter table bik_tree
add tree_kind varchar(255) null default null
go

update bik_tree
set tree_kind = tree_kind_code
go

alter table bik_tree
drop column tree_kind_code
go

exec sp_update_version '1.0.40', '1.0.41';
go
