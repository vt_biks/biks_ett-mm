/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author tflorczak
 */
public class ConnectionServerDialog extends BaseActionOrCancelDialog {

    protected ListBox lbServer;
    protected Set<String> servers;
    protected int nodeId;
    protected IContinuation continuation;

    @Override
    protected String getDialogCaption() {
        return I18n.ustawSerwerDlaPolaczenia.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.ustaw.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        main.add(new Label(I18n.wybierzSerwerOracle.get() /* I18N:  */+ ":"));
        main.add(lbServer = new ListBox());
        lbServer.setStyleName("loginAndPass");
        lbServer.setWidth("200px");
        for (String server : servers) {
            lbServer.addItem(server);
        }
    }

    @Override
    protected void doAction() {
        BIKClientSingletons.getService().setOracleServerForBOConnection(nodeId, lbServer.getItemText(lbServer.getSelectedIndex()), new StandardAsyncCallback<Void>("Error in" /* I18N: no */+ " setOracleServerForBOConnection") {

            public void onSuccess(Void result) {
                BIKClientSingletons.showInfo(I18n.zapisanoSerwerDlaPolaczenia.get() /* I18N:  */);
                continuation.doIt();
            }
        });
    }

    public void buildAndShowDialog(Set<String> servers, int nodeId, IContinuation continuation) {
        this.servers = servers;
        this.nodeId = nodeId;
        this.continuation = continuation;
        super.buildAndShowDialog();
    }
}
