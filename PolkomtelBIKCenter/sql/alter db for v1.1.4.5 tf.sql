﻿exec sp_check_version '1.1.4.5';
go
------------------------------------
------------------------------------



-- poprzednia wersja w: alter db for v1.1.4.3 tf.sql
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node]
go

create procedure [dbo].[sp_insert_into_bik_node](@tree_code varchar(255))
as
	declare @table_folders varchar(255);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	declare @diag_level int = 1;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'
	
	select @tree_id = id 
	from bik_tree
	where code=@tree_code;

	create table #primaryIds(si_id varchar(5000) primary key);

	if(@tree_code!='Reports' and @tree_code!='Teradata')
	begin
		set @table_folders = ' APP_FOLDER ';
		if(@tree_code = 'Connections')
		begin
			set @kinds = '''MetaData.DataConnection''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_METADATA__DATACONNECTION
					where SI_KIND in (' + @kinds + ') and SI_OBJECT_IS_CONTAINER=0');			
		end;
		if(@tree_code = 'ObjectUniverses')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select convert(varchar(30),SI_ID) as SI_ID  
					from APP_UNIVERSE
					where SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_code='Reports')
	begin
		-- wybranie folderw raportów bez raportów użytkowników
		set @table_folders = ' INFO_FOLDER where si_name != ''~Webintelligence''';
		--set @table_folders = ' INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 is null or SI_PATH__SI_FOLDER_NAME2 != ''User Folders''';
		set @kinds = '''Webi'', ''Flash'', ''CrystalReport'',''Excel'',''FullClient'',''Pdf'',''Hyperlink''
		,''Rtf'',''Txt'',''Powerpoint'',''Word''';
	end;
	
    declare @sql nvarchar(max);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		--print cast(sysdatetime() as varchar(22)) + ': faza 4.1 - teradata przed sp_teradata_init_branch_names'
		--exec sp_teradata_init_branch_names;
		if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': tworzenie pierwszego @sql'
		set @sql = N'select branch_names as SI_ID, parent_branch_names	as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata';
	end;
	else
	begin
		if(@tree_code = 'Reports')
		-- sztuczny podzial na root folder i user folders dla raportów
		set @sql = N'select distinct SI_PARENT_FOLDER as SI_ID, (select si_id from INFO_FOLDER where SI_NAME=''User Folders'') as SI_PARENTID, SI_KIND, SI_PATH__SI_FOLDER_NAME1 as SI_NAME, NULL as DESCR
				 from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' 
				 and SI_NAME!=''~Webintelligence'' 

				 union all
				
				 select si_id, case
									when SI_PARENTID=0 and si_name!=''User Folders''
										then (select si_id from INFO_FOLDER where SI_NAME=''Root Folder'')
									when si_id in(select si_id from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 = ''User Folders'')
                                        then SI_PARENTID
									when SI_PARENTID in(select si_id from INFO_FOLDER)
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case
																			when SI_NAME=''Root Folder''
																				then ''Foldery korporacyjne''
																			when SI_NAME=''User Folders''
																				then ''Foldery użytkowników''
																			else SI_NAME end, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 								
				' union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') and SI_INSTANCE = 0
				union all
				
				select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR
				from aaa_global_props where si_parentid in (select si_parentid from INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2=''User Folders'' and SI_NAME!=''~Webintelligence'') and si_kind!=''Folder''
				';
		if(@tree_code = 'Connections')
			set @sql = N'select 1 as si_id, 0 as si_parentid, ''ConnectionFolder'' as si_kind, ''Połączenia'' as si_name, '''' as descr
				
				 union all

				 select SI_ID, 1, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from APP_METADATA__DATACONNECTION
				 where SI_KIND in ('+ @kinds +') and SI_OBJECT_IS_CONTAINER=0';

		if(@tree_code = 'ObjectUniverses')
			set @sql = N'select si_id, case 
									when SI_NAME=''Universes''
										then 0
									when SI_PARENTID in(select si_id from ' + @table_folders +') 
										then SI_PARENTID
									else 0 end as SI_PARENTID, SI_KIND, case when SI_NAME=''Universes'' then ''Światy obiektów'' else SI_NAME end as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + 
													
				'union all

				 select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +')';
	end;					

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed zapytaniem'
	print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(1000) primary key, si_parentid varchar(1000), si_kind varchar(max), si_name varchar(max), descr varchar(max));
	if(@tree_code = 'Reports')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ReportFolder'' 
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'Connections')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ConnectionFolder''
										when si_kind=''MetaData.DataConnection'' then ''DataConnection''
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'ObjectUniverses')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''UniversesFolder'' else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	if(@tree_code = 'Teradata')
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, si_kind, si_name, DESCR
					from (' + @sql + ') xxx';

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po stworzeniu drugiego @sql'
			   
	EXECUTE(@sql);

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/

	declare @rc int = 1

	while @rc > 0 begin
	
	delete from #tmpNodes
	where (si_kind = 'UniversesFolder' or si_kind = 'ConnectionFolder' or si_kind = 'ReportFolder') and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/
	
	declare @report_tree_id int;
	select @report_tree_id=id from bik_tree where code='Reports'

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updatetami'

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.descr, tree_id = @tree_id
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and tree_id = @tree_id and bik_node.linked_node_id is null
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed update podlinkowanych'
	--update podlinkowanych
	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		 descr = #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and (tree_id = @report_tree_id or tree_id in (select id from bik_tree where tree_kind='Taxonomy')) and not bik_node.linked_node_id is null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed insertami'
	
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id
	where bik_node.id is null;

	set nocount on;

	drop table #primaryIds;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updateami parentow'
	

	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id --and bik_node.tree_id = @tree_id
	inner join bik_node bk on bk.obj_id = pt.si_parentid where bik_node.linked_node_id is null--and bk.tree_id = @tree_id 

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed usuwaniem'
	
	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in 
	('Measure', 'Dimension', 'Detail', 'UniverseClass', 'ReportQuery', 'Filter', 'UniverseColumn', 'UniverseTable', 'UniverseAliasTable', 'UniverseDerivedTable', 'UniverseTablesFolder', 'ConnectionEngineFolder', 'ConnectionNetworkFolder')
				
	drop table #tmpNodes
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po usuwaniu i po dropie'

	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': koniec'
end;
go

-- poprzednia wersja w: alter db for v1.1.4.3 tf.sql
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_objects_from_Designer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_objects_from_Designer]
GO

create procedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]
as
	declare @tree_id int;
	declare @kind_id int;
	declare @dimension_nki int, @measure_nki int, @detail_nki int, @filtr_nki int;
begin--begin procedure

	----wybranie id odpowiedniego drzewa
	select @tree_id = id
	from bik_tree
	where code = 'ObjectUniverses';	
	
	----wybranie id odpowiedniego kind'a
	select @kind_id = id
	from bik_node_kind
	where code = 'UniverseClass';
		
	create table #tmpNodes (si_id varchar(1000) , si_parentid varchar(1000), si_kind_id int, si_name varchar(max), si_description varchar(max));

	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Class
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select si_id_designer, si_parentid, @kind_id, name, description
	from aaa_universe_class  
	
	
	----wybranie id odpowiedniego kind'a
	--detail
	select @detail_nki = id
	from bik_node_kind
	where code = 'Detail';
	--demension
	select @dimension_nki = id
	from bik_node_kind
	where code = 'Dimension';
	--measure
	select @measure_nki = id
	from bik_node_kind
	where code = 'Measure';
	--filtr
	select @filtr_nki = id
	from bik_node_kind
	where code = 'Filter';
	
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Obj
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select obj_branch, si_parentid, 
			case
				when qualification = 'dsDetailObject' then @detail_nki
				when qualification = 'dsMeasureObject' then @measure_nki
				when qualification = 'dsDimensionObject' then @dimension_nki
				end as node_kind_id,
		 name, description
	from aaa_universe_obj
							 						 
	--wrzucam do tabeli tymczasowej wszystkie dane z Universe_filrt
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select uf.filtr_branch,	uc.si_id_designer, @filtr_nki, uf.name, nullif(ltrim(rtrim(uf.description)), '') 
	from aaa_universe_filter uf join aaa_universe_class uc on uf.universe_class_id = uc.id
	
	declare @folder_node_kind_id int;
	select @folder_node_kind_id = dbo.fn_node_kind_id_by_code('UniverseTablesFolder');

	--wrzucam katalogi do których są wrzucane tabele
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)
	select convert(varchar(10),global_props_si_id) + '|tableCatalog', convert(varchar(10),global_props_si_id), @folder_node_kind_id, 'Tabele'
	from aaa_universe 


	--wybranie id odpowiedniego kind'a
	declare @table_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseTable');
	declare @table_alias_node_kind_id int =  dbo.fn_node_kind_id_by_code('UniverseAliasTable');
	declare @table_derived_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseDerivedTable');
							 
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Table	
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)		
	select convert(varchar(10), their_id) + '|' + convert(varchar(1000), universe_branch), convert(varchar(1000), universe_branch) + '|tableCatalog', 
	case
				when is_alias = 1 then @table_alias_node_kind_id
				when is_derived = 1 then @table_derived_node_kind_id
				else @table_node_kind_id
				end as node_kind_id, name 
	from aaa_universe_table 
	
	declare @universe_column_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseColumn');
	
	----wrzucam do tymczasowej tabeli wszystkie dane z Universe_Column
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch) + '|' + uc.name, 
		convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch), @universe_column_node_kind_id, uc.name, REPLACE(REPLACE(uc.type,'ds',''),'Column','')
	from aaa_universe_table ut join aaa_universe_column uc on ut.id = uc.universe_table_id
	
	--aktualizacja istniejących węzłów w drzewie							
	update bik_node 
	set parent_node_id = null, node_kind_id = #tmpNodes.si_kind_id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.si_description, tree_id = @tree_id
	from #tmpNodes 
	where bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id and bik_node.linked_node_id is null;
	
	--dorzucanie nowych węzłów w drzewie
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select #tmpNodes.si_kind_id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.si_description
	from #tmpNodes left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
		and bik_node.tree_id = @tree_id
	where bik_node.id is null;	
	
	--uaktualnianie parentów w nodach
	declare @universe_kind_id int;
	select @universe_kind_id = id
	from bik_node_kind
	where code = 'Universe';
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id and bik_node.node_kind_id = pt.si_kind_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id 
		and  (
		 (pt.si_kind_id=@detail_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@dimension_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@measure_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@filtr_nki and bk.node_kind_id=@kind_id) or
		 (pt.si_kind_id=@universe_column_node_kind_id and (bk.node_kind_id=@table_node_kind_id or bk.node_kind_id=@table_alias_node_kind_id or bk.node_kind_id=@table_derived_node_kind_id)) or
		 (pt.si_kind_id=@kind_id and (bk.node_kind_id = @kind_id or bk.node_kind_id = @universe_kind_id))or
		 ((pt.si_kind_id=@table_node_kind_id or pt.si_kind_id=@table_alias_node_kind_id or pt.si_kind_id=@table_derived_node_kind_id) and bk.node_kind_id=@folder_node_kind_id)or
		 (pt.si_kind_id=@folder_node_kind_id and bk.node_kind_id=@universe_kind_id)
		 )
	where bk.linked_node_id is null and bik_node.parent_node_id is null;
		
	--usuwanie nodów
	update bik_node
	set is_deleted = 1
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and bik_node.node_kind_id in (@kind_id, @detail_nki, @dimension_nki, @measure_nki, -- do usuniecia tabele i kolumny i folder tabel
	  @filtr_nki);
	
	drop table #tmpNodes;
	--uzupełnianie danych w tabelach extra 
	--dla object
	create table #tmpObjExtra(text_of_select varchar(max),text_of_where varchar(max), type varchar(155), aggregate_function varchar(50), node_id int)
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select univ_obj.text_of_select, univ_obj.text_of_where, univ_obj.type, univ_obj.aggregate_function, bik_node.id
	from aaa_universe_obj univ_obj join bik_node on bik_node.obj_id = univ_obj.obj_branch 
		and bik_node.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)-- = @obj_node_kind_id;
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select null, uf.where_clause, uf.type, null, bik_node.id
	from aaa_universe_filter uf join bik_node on bik_node.obj_id = uf.filtr_branch and bik_node.node_kind_id = @filtr_nki
	
	update bik_sapbo_universe_object
	set text_of_select = #tmpObjExtra.text_of_select, text_of_where = #tmpObjExtra.text_of_where, type = #tmpObjExtra.type,  
		           aggregate_function = #tmpObjExtra.aggregate_function 
	from #tmpObjExtra join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id;
	
	insert into bik_sapbo_universe_object(text_of_select, text_of_where, type, aggregate_function, node_id)
	select #tmpObjExtra.text_of_select,#tmpObjExtra.text_of_where, #tmpObjExtra.type, #tmpObjExtra.aggregate_function, #tmpObjExtra.node_id 
	from #tmpObjExtra left join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id
	where bik_sapbo_universe_object.id is null;

	drop table #tmpObjExtra;
	
	--dla tabeli
	
	update bik_sapbo_universe_table
	set is_alias = ut.is_alias, is_derived = ut.is_derived, original_table = ut.original_table, sql_of_derived_table = ut.sql_of_derived_table, 
		sql_of_derived_table_with_alias = ut.sql_of_derived_table_with_alias, their_id = ut.their_id, node_id = bik_node.id, type = ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
	where linked_node_id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	
	insert into bik_sapbo_universe_table(is_alias, is_derived, original_table, sql_of_derived_table, sql_of_derived_table_with_alias, their_id, node_id, type)
	select ut.is_alias, ut.is_derived, ut.original_table, ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, ut.their_id, 
					bik_node.id as node_id, ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
		
	where  bik_node.linked_node_id is null and bsut.id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)

	--usupełnianie name_for_teradata
	update bik_sapbo_universe_table
	set name_for_teradata = replace(bik_node.name,'.', '|') + '|'
	from bik_sapbo_universe_table join bik_node on bik_sapbo_universe_table.node_id = bik_node.id
	where bik_sapbo_universe_table.type = 'Teradata' and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 0

	update bik_sapbo_universe_table
	set name_for_teradata = tabo.name_for_teradata
	from bik_sapbo_universe_table join bik_sapbo_universe_table tabo on bik_sapbo_universe_table.original_table = tabo.their_id 
	join bik_node bn on bn.id=bik_sapbo_universe_table.node_id
	join bik_node bn2 on bn2.id=tabo.node_id
	where bik_sapbo_universe_table.type = 'Teradata' and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 1 
	and bn2.parent_node_id=bn.parent_node_id
	
	update bik_sapbo_universe_table
	set schema_name = left(name_for_teradata, charindex('|',name_for_teradata))
	where  type = 'Teradata' and is_derived = 0
	
	--dla kolumn
	update bik_sapbo_universe_column
	set name = uc.name, type = uc.type, table_id = bik_node.id--bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			--join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
			join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bik_node.id--bsut.id
	where linked_node_id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	
	insert into bik_sapbo_universe_column(name, type, table_id)
	select uc.name, uc.type, bik_node.id--bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			--join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar(10), ut.their_id) + '|' + convert(varchar(1000), ut.universe_branch)
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id =bik_node.id--bsut.id
	where linked_node_id is null and bsuc.id is null  and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)

	--delete from bik_sapbo_universe_column i bik_sapbo_universe_column nie jest potrzebne bo w bik_node jest ustawiont isDeleted na 1:)
	
	delete from bik_sapbo_universe_connection;
	
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id, type, database_engine, client_number, language, system_number, system_id)
	select aaa_universe_connection.server, aaa_universe_connection.user_name, aaa_universe_connection.password, aaa_universe_connection.database_source, 
		aaa_universe_connection_networklayer.name, bik_node.id,
		aaa_universe_connection.type, aaa_universe_connection.database_enigme, aaa_universe_connection.client_number, aaa_universe_connection.language,
		aaa_universe_connection.system_number, aaa_universe_connection.system_id
	from aaa_universe_connection join aaa_universe_connection_networklayer on aaa_universe_connection.connetion_networklayer_id = aaa_universe_connection_networklayer.id
		join bik_node on aaa_universe_connection.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		left join bik_sapbo_universe_connection on bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections' and bik_sapbo_universe_connection.id is null;
	
	-- dodanie statystyk dla światów obiektów	
	update bik_sapbo_extradata
	set statistic = u.statistic
	from bik_sapbo_extradata join bik_node bn on bn.id = bik_sapbo_extradata.node_id
		join bik_node_kind bnk on bn.node_kind_id = bnk.id
		join aaa_universe u on convert(varchar,u.global_props_si_id) = bn.obj_id
	where bnk.code = 'Universe'	
	
	-- ustawianie visual order dla folderów tabel
	declare @nk_id int = (select id from bik_node_kind where code = 'UniverseTablesFolder')
	update bik_node set visual_order = -1 where node_kind_id = @nk_id
				
	exec sp_delete_linked_nodes_where_orignal_is_deleted;	
	exec sp_node_init_branch_id @tree_id, null;
end
go



exec sp_update_version '1.1.4.5', '1.1.4.6';
go
