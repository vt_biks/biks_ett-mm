/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.foxygwtcommons.treeandlist;

/**
 *
 * @author wezyr
 */
public interface ILazyRowLoader<ROW> {

    public boolean isLeafForSure(ROW row);

    public void loadSubRowsOnExpand(ROW row);
}
