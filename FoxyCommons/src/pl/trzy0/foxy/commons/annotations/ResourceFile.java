/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons.annotations;

/**
 *
 * @author wezyr
 */
public @interface ResourceFile {
    String value() default "";
    String ext() default "";
    String encoding() default "";
}
