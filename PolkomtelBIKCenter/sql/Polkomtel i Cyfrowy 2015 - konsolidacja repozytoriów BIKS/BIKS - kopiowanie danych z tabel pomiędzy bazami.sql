if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_copy_data_for_tables]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_copy_data_for_tables]
go

create procedure [dbo].[sp_copy_data_for_tables] @src_opt_ls_and_db_name varchar(max), @src_schema_name sysname = 'dbo', 
  @src_opt_table_name_filter_clause varchar(max) = null, @dst_table_name_prefix sysname = 'copy_', @diag_level int = 1 as
begin
  set nocount on;

  if @src_schema_name is null set @src_schema_name = '';

  declare @src_db_prefix varchar(max) = case when coalesce(@src_opt_ls_and_db_name, '') = '' then '' else @src_opt_ls_and_db_name + '.' end;

  declare @src_schema_prefix varchar(max) = case when @src_schema_name = '' then '' else @src_schema_name + '.' end;

  declare @sql_txt nvarchar(max);

  set @sql_txt = 'select name from ' + @src_db_prefix + 'sys.objects where type = ''u'' and (select name from ' + @src_db_prefix + 'sys.schemas where schema_id = objects.schema_id) = ''' + @src_schema_name + '''' +
    case when @src_opt_table_name_filter_clause is null then '' else ' and (' + @src_opt_table_name_filter_clause + ')' end;
  

  if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##cdtf_table_names'))
	exec('drop table ##cdtf_table_names');

  create table ##cdtf_table_names (name sysname not null primary key);

  set @sql_txt = 'insert into ##cdtf_table_names (name) ' + @sql_txt;
  exec(@sql_txt);

  -- select * from ##cdtf_table_names;

  declare @table_cnt int = (select count(1) from ##cdtf_table_names), @table_num int = 0;

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': number of tables to copy data: ' + cast(@table_cnt as varchar(20));

  declare @cur cursor;

  set @cur = cursor for select name from ##cdtf_table_names;

  declare @src_table_name sysname, @dst_table_name sysname, @rc int;

  open @cur;

  fetch next from @cur into @src_table_name;

  while @@fetch_status = 0 begin
  
    set @table_num += 1;
    
    if @diag_level > 1 print cast(sysdatetime() as varchar(23)) + ': before copy data of table #' + cast(@table_num as varchar(20)) + ': ' + @src_table_name;
	
	set @dst_table_name = @dst_table_name_prefix + @src_table_name;

	if object_id(@dst_table_name, 'u') is not null begin
	  set @sql_txt = 'drop table ' + @dst_table_name;
	  exec(@sql_txt);
	end;  

    set @sql_txt = 'select * into ' + @dst_table_name + ' from ' + @src_db_prefix + @src_schema_prefix + @src_table_name + '; set @rc = @@rowcount';
  
    --exec(@sql_txt);

	declare @start_time datetime = sysdatetime(), @end_time datetime;

	exec sp_executesql @sql_txt, N'@rc int output', @rc output

	set @end_time = sysdatetime();

    if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': done copy data of table #' + cast(@table_num as varchar(20)) + ': ' + @src_table_name + 
	  ', row count: ' + cast(@rc as varchar(20)) + ', time: ' + cast(datediff(ms, @start_time, @end_time) as varchar(20)) + ' ms';

    fetch next from @cur into @src_table_name;
  end;

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': finished';

  close @cur;
end;
go

-- exec sp_copy_data_for_tables 'biks_cp.biks_cyfrowy_2015', @src_opt_table_name_filter_clause = 'name in (''bik_node'', ''bik_tree'')';

-- exec sp_copy_data_for_tables 'biks_cp.biks_cyfrowy_2015'

-- exec sp_copy_data_for_tables '[biks_polkomtel_2012_10_22]'

--select objects.name, s.name from sys.objects inner join sys.schemas s on objects.schema_id = s.schema_id where objects.is_ms_shipped = 0 and objects.type = 'u'

