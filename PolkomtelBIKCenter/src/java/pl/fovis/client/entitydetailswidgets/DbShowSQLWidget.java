/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

/**
 *
 * @author tflorczak
 */
public class DbShowSQLWidget extends ShowSQLWidgetBase {

    @Override
    protected String getSQLTextFromData() {
        return getData().dbDefinition;
    }
}
