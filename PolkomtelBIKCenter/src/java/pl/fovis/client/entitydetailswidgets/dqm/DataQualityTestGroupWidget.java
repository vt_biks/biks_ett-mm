/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class DataQualityTestGroupWidget extends EntityDetailsWidgetFlatBase {

    protected FlowPanel panel;

    @Override
    public void displayData() {
        GroupWidgetBuilder.showManyInContainer(getData().dataQualityTestGroupExtradata, panel, BaseUtils.safeEquals(getNodeTreeCode(), BIKGWTConstants.TREE_CODE_DQM));
//        panel.clear();
//        List<DataQualityGroupBean> data = getData().dataQualityTestGroupExtradata;
//        if (data != null) {
//            Map<Integer, List<DataQualityGroupBean>> map = new LinkedHashMap<Integer, List<DataQualityGroupBean>>();
//            // napełnianie mapy
//            for (DataQualityGroupBean bean : data) {
//                if (!map.containsKey(bean.nodeId)) {
//                    map.put(bean.nodeId, new ArrayList<DataQualityGroupBean>());
//                }
//                map.get(bean.nodeId).add(bean);
//            }
//
//            // dodanie widgetów
//            for (Entry<Integer, List<DataQualityGroupBean>> wid : map.entrySet()) {
//                panel.add(new GroupWidgetBuilder(wid.getValue(), BaseUtils.safeEquals(getNodeTreeCode(), BIKGWTConstants.TREE_CODE_DQM)).buildWidget());
//            }
//        }
    }

    @Override
    public boolean hasContent() {
        return getData().dataQualityTestGroupExtradata != null;
    }

    @Override
    public Widget buildWidgets() {
        panel = new FlowPanel();
        panel.setStyleName("superBorderForChuck");
        return panel;
    }
}
