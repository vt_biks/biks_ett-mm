/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.drools;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.LogsPageBase;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.DroolsLogBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class DroolsLogPage extends LogsPageBase<DroolsLogBean> {

    @Override
    protected void callServiceToGetData(StandardAsyncCallback<Pair<Integer, List<DroolsLogBean>>> callback) {
        BIKClientSingletons.getService().getDroolsLogs(pwh.pageNum, pwh.pageSize, callback);
    }

    @Override
    protected NodeAwareBeanExtractorBase<DroolsLogBean> getDataExtractor() {
        return new DroolsLogBeanExtractor();
    }

    @Override
    public String getId() {
        return "admin:drools:logs";
    }
}
