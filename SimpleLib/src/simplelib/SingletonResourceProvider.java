/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author wezyr
 */
public class SingletonResourceProvider<T> implements IGenericResourceProvider<T> {

    private IResourceFromProvider<T> resource;

    public SingletonResourceProvider(IResourceFromProvider<T> resource) {
        this.resource = resource;
    }

    public T gainResource(String resourceName) {
        return resource.gainResource();
    }

    public long getLastModifiedOfResource(String resourceName) {
        return resource.getLastModifiedOfResource();
    }
}
