/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.JdbcTestConnectionBase;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.common.ServerConfigBean;
import pl.bssg.metadatapump.mssql.IMsSqlQueryNameProvider;
import pl.bssg.metadatapump.mssql.MsSql2000QueryNameProvider;
import pl.bssg.metadatapump.mssql.MsSql2005QueryNameProvider;
import pl.bssg.metadatapump.mssql.MsSql2008QueryNameProvider;
import pl.bssg.metadatapump.mssql.MsSqlBean;
import pl.fovis.common.ConnectionParametersAuditBean;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author bfechner
 */
public class BIAAuditDatabaseTestConnection extends JdbcTestConnectionBase {

//    protected int id;
    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected ServerConfigBean bean;
    protected String database;
    protected INamedSqlsDAO<Object> adhocDaoMSSQL;
//    protected String optObjsForGrantsTesting;
    ConnectionParametersAuditBean mcfg;

    public BIAAuditDatabaseTestConnection(ConnectionParametersAuditBean mcfg) {
        this.mcfg = mcfg;
    }

    @Override
    protected Pair<Integer, String> testConnectionInner() {
        String serverVersion = "";
        try {
            bean = new ServerConfigBean();
            bean.databaseFilter = mcfg.dbName;
            bean.instance = mcfg.instance;
            bean.server = mcfg.server;
            bean.user = mcfg.login;
            bean.password = mcfg.password;
            database = "master";
            List<String> databasesFilterDatabases = BaseUtils.splitBySep(bean.databaseFilter, ",", true);
            boolean notConnectingWithSystemDB = !databasesFilterDatabases.isEmpty();
            if (notConnectingWithSystemDB) {
                database = databasesFilterDatabases.get(0);
            }
            bce = new MssqlConnection(
                    new MssqlConnectionConfig(bean.server, bean.instance, database, bean.user, bean.password));
            if (bce == null) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connecting with MSSQL DB: " + database);
            }
            List<Map<String, Object>> execQry = bce.execQry("select cast(serverproperty('productversion') as char) as version");
            if (execQry == null || execQry.isEmpty()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No access to database: " + database);
            }
            // sprawdzam skladnie
            adhocDaoMSSQL = FoxyAppUtils.createNamedSqlsDAOFromJar(bce, ADHOC_SQLS_PATH);
            serverVersion = BeanConnectionUtils.execNamedQuerySingleVal(adhocDaoMSSQL, "getMSSQLServerVersion", false, "version");
            IMsSqlQueryNameProvider queryNameProvider;
            if (serverVersion.startsWith("8.")) { // 2000
                queryNameProvider = new MsSql2000QueryNameProvider();
            } else if (serverVersion.startsWith("9.")) { // 2005
                queryNameProvider = new MsSql2005QueryNameProvider();
            } else if (serverVersion.startsWith("10.")) { // 2008 & 2008 R2
                queryNameProvider = new MsSql2008QueryNameProvider();
            } else if (serverVersion.startsWith("11.")) { // 2012
                queryNameProvider = new MsSql2008QueryNameProvider();
            } else {
                queryNameProvider = new MsSql2008QueryNameProvider();
            }
            List<MsSqlBean> databases;
            if (notConnectingWithSystemDB) {
                databases = new ArrayList<MsSqlBean>();
                for (String dbF : databasesFilterDatabases) {
                    MsSqlBean msBean = new MsSqlBean();
                    msBean.name = dbF;
                    databases.add(msBean);
                }
            } else { // pobierz z bazy master
                databases = adhocDaoMSSQL.createBeansFromNamedQry(queryNameProvider.getDatabases(), MsSqlBean.class, BaseUtils.splitBySep(bean.databaseFilter, ","));
            }

            if (databases == null || databases.isEmpty()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Lack of database!");
            }
            Map<String, String> objectMap;
            try {
                objectMap = MetadataPumpBase.createObjectMapFromString(bean.objectFilter);
            } catch (Exception e) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Syntax error in the filter: " + e.getMessage());
            }
            for (MsSqlBean db : databases) {
                String optFilter = null;
                if (objectMap != null && objectMap.containsKey(db.name)) {
                    optFilter = objectMap.get(db.name);
                }
                try {
                    List<MsSqlBean> objects = adhocDaoMSSQL.createBeansFromNamedQry(queryNameProvider.getObjects(), MsSqlBean.class, db.name, optFilter);
                } catch (Exception e) {
                    return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Syntax error in the filter or lack of permissions to the database: " + db.name + (optFilter != null ? " - " + optFilter : " - lack of permission"));
                }
            }
            if (showGrantsInTestConnection()) {
                StringBuilder sbGrant = new StringBuilder();
                for (MsSqlBean db : databases) {
                    List<MsSqlBean> grants = adhocDaoMSSQL.createBeansFromNamedQry(queryNameProvider.getGrants(), MsSqlBean.class, bean.user, db.name);
                    sbGrant.append(db.name).append(":\n");
                    for (MsSqlBean grant : grants) {
                        sbGrant.append("   ").append(grant.name).append(" - ").append(grant.type).append("\n");
                    }
                }

//                String perObjTestingResult = BaseUtils.isStrEmptyOrWhiteSpace(optObjsForGrantsTesting) ? "" : "\nPer obj testing: \n" + optTestGrantsPerObjects(bce);
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, "OK, version: " + serverVersion + " \nGrants:\n" + sbGrant.toString());
            }
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in MS SQL Test Connection", e);
            }
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "No connecting with MSSQL DB! Error: " + e.getMessage());
        }
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, "OK, version: " + serverVersion);
    }
}
