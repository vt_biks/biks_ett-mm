/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import org.apache.lucene.analysis.core.LetterTokenizer;
import org.apache.lucene.util.AttributeFactory;

/**
 *
 * @author pmielanczuk
 */
public class MyLowerCaseTokenizer extends LetterTokenizer {

    public MyLowerCaseTokenizer() {
    }

    public MyLowerCaseTokenizer(AttributeFactory factory) {
        super(factory);
    }

    @Override
    protected int normalize(int c) {
        return Character.toLowerCase(c);
    }

    @Override
    protected boolean isTokenChar(int c) {
        return //Character.isLetter(c) ||
                Character.isLetterOrDigit(c);
    }
}
