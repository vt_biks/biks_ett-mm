/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.grid;

import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public interface IBeanProvider<T> {
    // offset >= 0
    // offset < 0 => offset = 0    
    // limit > 0 -> get partial data (at most give limit beans)
    // limit = 0 -> for checking bean count only
    // limit < 0 -> get full data

    public void getBeans(int offset, int limit, Iterable<String> sortCols,
            Iterable<String> descendingCols, IParametrizedContinuation<Pair<? extends Iterable<T>, Integer>> showBeansCont);
}
