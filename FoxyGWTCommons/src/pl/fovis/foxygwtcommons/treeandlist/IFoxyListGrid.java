/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.treeandlist;

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author mgraczkowski
 */
public interface IFoxyListGrid<IDT> extends IFoxyTreeOrListGrid<IDT> {

    public void setIdField(String idFieldName);

    public void setData(Collection<Map<String, Object>> rows);

    public void setDescending(int columnNumber);
//    public void setWidth(String width);
//
//    public void setHeight(String height);
}
