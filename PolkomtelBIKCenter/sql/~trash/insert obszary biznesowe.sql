-- select * from aaa_aux_tree_node
-- delete from aaa_aux_tree_node
-- delete from aaa_entity_in_tree_node

--set identity_insert aaa_aux_tree_node on
--go

insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (1, 1, null, '?1', 'Struktura organizacyjna', 1);
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (2, 2, 1, '?2', 'Polkomtel', 1);
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (3, 3, 2, '?3', 'Pion IT', 1);
 --Pion IT
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (14, 14, 3, '?4', 'Departament Projekt�w', 1);
--   Departament Projekt�w
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (15, 15, 2, '?5', 'Dzia� Zakup�w', 1);
--   Dzia� Zakup�w
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (4, 4, 2, '?6', 'Pion finansowy', 1);
-- Pion finansowy
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (13, 13, 4, '?7', 'Departament bud�etu i kontroli finansowej', 1);
--   Departament bud�etu i kontroli finansowej
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (5, 5, 2, '?8', 'Pion Techniczny', 1);
 --Pion Techniczny
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (8, 8, 5, '?9', 'Centrala', 1);
--   Centrala
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (9, 9, 5, '?10', 'Region 1', 1);
--   Region 1
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (10, 10, 5, '?11', 'Region 2', 1);
   --Region 2
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (11, 11, 5, '?12', 'Region 3', 1);
   --Region 3
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (12, 12, 5, '?13', 'Region 4', 1);
   --Region 4
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (6, 6, 2, '?14', 'Pion Marketingu', 1);
-- Pion Marketingu
insert into aaa_aux_tree_node (id, item_si_id, parent_id, id_path, caption, tree_num)
values (7, 7, 6, '?15', 'Departament Business Intelligence', 1);
--   Departament Business Intelligence
go

--set identity_insert aaa_aux_tree_node off
--go

---------------------------------
---------------------------------
---------------------------------

-- select max(id) + 1 from aaa_aux_tree_node 
-- select * from aaa_aux_tree_node 
-- delete from aaa_aux_tree_node where tree_num = 2
-- alter table aaa_aux_tree_node alter column id_path varchar(800) null
-- alter table aaa_aux_tree_node drop constraint UQ__aaa_tree__09416ACE7AE1BEBF

--set identity_insert aaa_aux_tree_node on
--go


insert into aaa_aux_tree_node (id, parent_id, id_path, caption, tree_num)
values (16, null, '?16', 'Systemy Informatyczne', 2);
--Systemy Informatyczne
insert into aaa_aux_tree_node (id, parent_id, id_path, caption, tree_num)
values (17, 16, '?17', 'DWH', 2);
-- DWH
insert into aaa_aux_tree_node (id, parent_id, id_path, caption, tree_num)
values (18, 17, '?18', 'DM_SPRZEDA�', 2);
   --DM_SPRZEDA�
insert into aaa_aux_tree_node (id, parent_id, id_path, caption, tree_num)
values (19, 17, '?19', 'DM_NC', 2);
   --DM_NC
insert into aaa_aux_tree_node (id, parent_id, id_path, caption, tree_num)
values (20, 17, '?20', 'DM_RODOS', 2);
   --DM_RODOS
insert into aaa_aux_tree_node (id, parent_id, id_path, caption, tree_num)
values (21, 16, '?21', 'SAP', 2);
-- SAP
insert into aaa_aux_tree_node (id, parent_id, id_path, caption, tree_num)
values (22, 16, '?22', 'SAPBW', 2);
 --SAPBW
insert into aaa_aux_tree_node (id, parent_id, id_path, caption, tree_num)
values (23, 16, '?23', 'S2K', 2);
 --S2K
insert into aaa_aux_tree_node (id, parent_id, id_path, caption, tree_num)
values (24, 16, '?24', 'ADA', 2);
-- ADA
insert into aaa_aux_tree_node (id, parent_id, id_path, caption, tree_num)
values (25, 16, '?25', 'CELLMAN', 2);
 -- CELLMAN
insert into aaa_aux_tree_node (id, parent_id, id_path, caption, tree_num)
values (26, 16, '?26', 'REMEDY', 2);
 --REMEDY
insert into aaa_aux_tree_node (id, parent_id, id_path, caption, tree_num)
values (27, 16, '?27', 'REKLAMACJE', 2);
 --REKLAMACJE
 
--set identity_insert aaa_aux_tree_node off
--go



---------------------------------
---------------------------------
---------------------------------

-- select max(id) + 1 from aaa_aux_tree_node 
-- select * from aaa_aux_tree_node 
-- delete from aaa_aux_tree_node where tree_num = 2
-- alter table aaa_aux_tree_node alter column id_path varchar(800) null
-- alter table aaa_aux_tree_node drop constraint UQ__aaa_tree__09416ACE7AE1BEBF
-- alter table aaa_aux_tree_node add descr varchar(max) null

--set identity_insert aaa_aux_tree_node on
--go


insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (30, null, 'Wska�niki', 3, '')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (31, 30, 'churn', 3, 'odej�cie klienta (klient�w), zaprzestanie korzystania z us�ugi danej firmy. Churn mo�e by� dzia�aniem formalnym � np. z�o�enie przez klienta wniosku o rezygnacj� z us�ug lub dzia�aniem nie formalnym � np. zaprzestanie p�atno�ci faktur bez wypowiedzenia umowy. Definiujemy ten wska�nik jako liczb� os�b, kt�ra przestala korzysta� z danego produktu czy us�ugi w okre�lonym czasie, podzielona przez �redni� liczb� og�lnych u�ytkownik�w wspomnianego produktu lub us�ugi. Zjawisko churn pokazuje przyrost lub spadek liczby og�lnej konsument�w danego produktu/us�ugi, jak r�wnie� �redni czas u�ytkowania (�ycia) danego produktu/us�ugi')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (32, 30, 'arpu', 3, '�redni miesi�czny przych�d z abonenta')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (33, 30, '  aktywacja', 3, '')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (34, 30, 'deaktywcja', 3, '')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (35, 30, 'rekatywacja', 3, '')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (36, null, 'Poj�cia biznesowe', 3, '')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (37, 36, 'MARKA', 3, '')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (38, 37, 'PLUS ', 3, 'system abonamentowy')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (39, 37, '  SIMPLUS ', 3, 'system przedp�acony')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (40, 37, '  MixPlus ', 3, 'system ��cz�cy elementy abonamentu i us�ugi przedp�aconej')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (41, 37, '  Sami Swoi ', 3, 'prosta telefonia kom�rkowa w systemie przedp�aconym')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (42, 37, '  iPlus', 3, 'bezprzewodowy dost�p do Internetu')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (43, 36, 'Us�ugi g�osowe', 3, '')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (44, 43, '  Po��czenia telefoniczne ', 3, 'w systemie abonamentowym (oferta dla klient�w indywidualnych, biznesowych i w systemach pre-paid.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (45, 43, 'Wideorozmowa', 3, '')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (46, 43, 'Us�uga Prefiks 1069 ', 3, ' jednoczesne korzystanie z pakietu us�ug telefonii mobilnej oraz telefonii stacjonarnej.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (47, 43, 'dost�p g�osowy ', 3, 'dost�p g�osowy do poczty elektronicznej i SMS.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (48, 36, 'US�UGI NIEG�OSOWE', 3, '')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (49, 48, 'SMS', 3, 'kr�tkie wiadomo�ci tekstowe,')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (50, 48, 'MMS', 3, 'wiadomo�ci multimedialne.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (51, 48, 'przesy�anie danych ', 3, 'przesy�anie danych w technologiach:GPRS,HSCSD,3G (GPRS, EDGE, UMTS).')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (52, 48, 'dost�p do Internetu', 3, 'dost�p do internetu r�wnie� z wykorzystaniem technologii 3G (GPRS, EDGE, UMTS):')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (53, 48, 'WAP, Internet', 3, 'WAP, Internet,')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (54, 48, 'poczta e-mail', 3, 'poczta e-mail')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (55, 36, 'ROAMING', 3, '')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (56, 36, 'TELEMETRIA I MONITORING', 3, '')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (57, 56, 'Monitoring operacyjny', 3, 'monitoring obiekt�w, przesy�ek.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (58, 56, 'Zarz�dzanie flot� samochodow�.', 3, '')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (59, 56, 'Monitoring medyczny', 3, 'przesiewowe badania s�uchu noworodk�w + telekardiologia.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (60, 56, 'Us�ugi lokalizacyjne', 3, 'np.: Pilot Plus, Wiem gdzie jeste�, wykorzystuj�ce kart� Plus Mega.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (61, 36, 'INFOTAINMENT', 3, 'INFORMACJE I ROZRYWKA')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (62, 61, 'M-banking:', 3, 'przy wsp�pracy z Pekao S.A., Inteligo, Raiffeisen, BZWBK, mBank.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (63, 61, 'Us�ugi informacyjne oparte o technik� SMS', 3, 'm.in. INFO Plus, PKP INFO, SMS Plus Portal, Hot News, Encyklopedia PWN, S�ownik pol-ang/ang-pol.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (65, 61, 'Rozrywka Plus', 3, 'gry SMS, Java, WAP, Graffiti Plus, loga i dzwonki, Randka Plus i Partner Plus.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (66, 61, 'Serwis Mobilny', 3, 'Telewizja Polska S.A., we wsp�pracy z sieci� Plus udost�pni�a pierwszy w Polsce mobilny serwis informacyjny. Aktualizowane co kilka godzin informacje wy�wietlane s� na ekranie telefonu kom�rkowego w postaci tekstu, fotografii, a tak�e sekwencji wideo.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (67, 36, 'OFERTA DLA BIZNESU', 3, '')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (68, 67, 'System Plus', 3, 'Zorganizowany system po��cze� mi�dzy telefonami kom�rkowymi nale��cymi do jednej firmy.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (69, 67, 'Strefa Biurowa ', 3, 'Wydzielony obszar b�d� kilku obszar�w, na kt�rych wszystkie po��czenia mi�dzy telefonami kom�rkowymi obj�te s� specjaln� taryfikacj� - tak� jak w Systemie Plus. Techniczne mo�liwo�ci pozwalaj� tak�e w��czy� do Strefy Biurowej po��czenia z telefon�w stacjonarnych na kom�rkowe.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (70, 67, 'Prefix 1069 ', 3, 'umo�liwia abonentom jednoczesne korzystanie z pakietu us�ug telefonii mobilnej oraz telefonii stacjonarnej.')
insert into aaa_aux_tree_node (id, parent_id, caption, tree_num, descr) values (71, 67, 'Telefonia konwergentna', 3, 'integracja telefonii stacjonarnej i mobilnej i stworzenie sp�jnego systemu ��czno�ci, obs�ugiwanego przez jednego operatora. W Plusie oparta jest na taryfach MaxProfit! oraz us�udze Prefix 1069.')


/*

--select '[' + rtrim(ltrim(' ala  ')) + ']'


set identity_insert aaa_tree_node on
go

insert into aaa_tree_node (id, parent_id, caption, tree_num)
select -1 * id, case when parent_id is null then null else -1 * parent_id end, ltrim(rtrim(caption)), tree_num
from aaa_aux_tree_node
where tree_num = 3

go

set identity_insert aaa_tree_node on
go

delete from aaa_mpart_linked
delete dbo.aaa_mpart_keyword
delete from aaa_mpart

alter table aaa_mpart add tree_node_id int not null

insert into aaa_mpart (subject, body, official, tree_node_id)
select caption, descr, 1, -1 * id
from aaa_aux_tree_node
where tree_num = 3
and ltrim(rtrim(isnull(descr, ''))) != ''

insert into aaa_mpart (subject, body, official, tree_node_id)
select caption, caption, 1, -1 * id
from aaa_aux_tree_node
where tree_num = 3 and id in (33,34,35)

and ltrim(rtrim(isnull(caption, ''))) in ('', '', '')


select distinct type
--* 
from aaa_teradata_object
where parent_id is null

select * from aaa_teradata_object where name like 'VD_US_%'

-- VD_US_DM_Sprzedaz

raport "Onephone_aktywacje_dziennie" --> podpi�� pod aktywacje

    select *
    from aaa_teradata_object
    where isnull(parent_id,'')='' and name like 'VD_US_%'

delete from dbo.aaa_blog_entry_entity

delete from dbo.aaa_blog_entry

delete from dbo.aaa_entity_keyword

delete from dbo.aaa_keyword

*/

create table aaa_joined_objs (
  id int identity(-100000,-1) not null primary key,
  src_id int not null,
  dst_id int not null,
  dst_type varchar(200) not null,
  dst_name varchar(2000) not null
);

create table aaa_additional_descr (
  id int identity not null primary key,
  siId int not null,
  descr varchar(max) not null
);

insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name)
values (-1013817, 15266, 'Universe', 'DWH_SPRZEDAZ__0106')


insert into aaa_additional_descr (siId, descr)
values (-1013817, 'Datamart dla departamentu sprzeda�y do monitorowania aktywacji, deaktywacji, reaktywacji po taryfach, us�ugach i wska�nikach churn.')


insert into aaa_additional_descr (siId, descr)
values (16149, 'Raport u�ywany przez BPKiA do monitorowania churn. Dostarczany w ka�dy wtorek do biura zarz�du.')

insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (-31, 22440, 'Folder', '/DWH/SPRZEDAZ')
insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (-31, 33640, 'Folder', '/SR DKPiNT/DWH/postpaid')
insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (-31, 16149, 'Webi', 'Onephone_aktywacje_dzienne')
insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (-31, 15266, 'Universe', 'DWH_SPRZEDAZ_0106')
insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (-31, -1013817, 'SCHEMA', 'VD_US_DM_Sprzedaz')
insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (16149, -31, 'Glosariusz', 'Churn')
insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (16149, 15266, 'Universe', 'DWH_SPRZEDAZ_0106')
insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (16149, -1013817, 'SCHEMA', 'VD_US_DM_Sprzedaz')
insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (16149, 22440, 'Folder', '/DWH/SPRZEDAZ')
insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (-1013817, 15266, 'Universe', 'DWH_SPRZEDAZ_0106')
insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (-1013817, 33640, 'Folder', '/SR DKPiNT/DWH/postpaid')
insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (-1013817, -31, 'Glosariusz', 'Churn')
insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (-1013817, 16149, 'Webi', 'Onephone_aktywacje_dzienne')
insert into aaa_joined_objs (src_id, dst_id, dst_type, dst_name) values (-1013817, 22440, 'Folder', '/DWH/SPRZEDAZ')
