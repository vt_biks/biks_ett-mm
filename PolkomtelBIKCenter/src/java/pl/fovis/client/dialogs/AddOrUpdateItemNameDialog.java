/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.TextBox;
import java.util.Collection;
import java.util.Set;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.OneTextBoxDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author AMamcarz
 */
public class AddOrUpdateItemNameDialog extends OneTextBoxDialog {

    protected IParametrizedContinuation<String> saveCont;
    protected Set<String> alreadyInUseInLowerCase;// = new ArrayList<String>();
    protected HTML errorMssg;

    @Override
    protected String getDialogCaption() {
        return name;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        main.add(errorMssg = new HTML());
        errorMssg.setStyleName("status" /* i18n: no:css-class */);
        tbValue = new TextBox();
        tbValue.setStyleName("addOUpItemTb");
        tbValue.setTitle(name);
        tbValue.setValue((initValue != null) ? initValue : "");

        main.add(tbValue);
    }

//    @Override
//    protected void doAction() {
//        String newCaption = tbValue.getValue();
//        if (!newCaption.equals("")) {
//
//            for(String oldCaption : alreadyInUseInLowerCase){
//                if(oldCaption.equals(newCaption)){
//                    return;
//                }
//            }
//            saveCont.doIt(tbValue.getValue());
//
//        }
//    }
    @Override
    protected boolean doActionBeforeClose() {
        //ww: pachnie to c&p z AttrOrRoleEditDialog
        //ww: pachnie to c&p z AddOrUpdateItemNameDialog
        //ww: pachnie to c&p z AddOrEditAdminAttributeWithTypeDialog
        String newCaption = tbValue.getValue();
        if (BaseUtils.isStrEmptyOrWhiteSpace(newCaption)) {
            errorMssg.setHTML(I18n.nazwaNieMozeBycPusta.get() /* I18N:  */ + ".");
            return false;
        }
        return doActionBeforeCloseInner();
    }

    protected boolean doActionBeforeCloseInner() {
        String newCaption = tbValue.getValue();
        if (alreadyInUseInLowerCase.contains(newCaption.toLowerCase())) {
            errorMssg.setHTML(I18n.takaNazwaJuzIstnieje.get() /* I18N:  */ + ".");
            return false;
        }
        saveCont.doIt(tbValue.getValue());
        return true;
    }

    public void buildAndShowDialog(String name, String caption, Collection<String> alreadyInUse, IParametrizedContinuation<String> saveCont) {
        this.name = name;
        this.initValue = caption;
        this.saveCont = saveCont;
        this.alreadyInUseInLowerCase = BaseUtils.toLowerCaseIterableToSet(alreadyInUse, true);
        super.buildAndShowDialog();
    }
}
