/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import java.util.LinkedHashMap;
import java.util.Map;
import pl.fovis.client.dialogs.ViewTutorialInFullScreenDialog;
import pl.fovis.common.TutorialBean;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class MyBIKSTutorialGrid extends MyBIKSGridSingleRowBase<TutorialBean> implements IPaginatedItemsDisplayer<TutorialBean> {

    protected Map<Integer, Image> images = new LinkedHashMap<Integer, Image>();
    protected Map<Integer, Image> imagesAccept = new LinkedHashMap<Integer, Image>();

    @Override
    protected String getIconUrl(TutorialBean n) {
        return "images/itutorial.gif" /* I18N: no */;
    }

    @Override
    protected void setUpGridColumnsWidths(FlexTable gp) {
        gp.setWidth("100%");
        gp.getColumnFormatter().setWidth(0, "18px");
        gp.getColumnFormatter().setWidth(1, "100%");
    }

    protected ClickHandler addClickHandler(final int row, final TutorialBean n) {
        return new ClickHandler() {
            public void onClick(ClickEvent event) {
                new ViewTutorialInFullScreenDialog().buildAndShowDialog(n,
                        new IParametrizedContinuation<TutorialBean>() {
                            public void doIt(final TutorialBean param) {
                                boolean isReaded = n.isReaded;
                                titles.get(row).setStyleName(!isReaded ? "Tutorial-Title" : "Tutorial-line");
                                images.get(row).setUrl(getIconUrl(n));
                                imagesAccept.get(row).setUrl("images" /* I18N: no */ + "/" + (!isReaded ? "empty" /* I18N: no */ : "accept" /* I18N: no */) + "." + "gif" /* I18N: no */);
                            }
                        });
            }
        };
    }

    @Override
    protected void addOrRefreshTitles(int row, final TutorialBean n) {
        final Image img = BikIcon.createIcon(getIconUrl(n), "RelatedObj-ObjIcon");
        final HTML title = new HTML((getCurrentRowGlobalIdx() + 1) + ". " + n.title);
        title.addClickHandler(addClickHandler(row, n));
        boolean isReaded = n.isReaded;
        title.setStyleName(!isReaded ? "Tutorial-Title" : "Tutorial-line");
        titles.put(row, title);
        images.put(row, img);
        imagesAccept.put(row, new Image("images" /* I18N: no */ + "/" + (!isReaded ? "empty" /* I18N: no */ : "accept" /* I18N: no */) + "." + "gif" /* I18N: no */));
    }

    @Override
    protected void createRowFromBean(int row, TutorialBean n) {
        addOrRefreshTitles(row, n);
        columnsCnt = 0;
        gp.setWidget(row, columnsCnt++, images.get(row));
        FlowPanel fp = new FlowPanel();
        fp.add(titles.get(row));
        fp.add(imagesAccept.get(row));
        gp.setWidget(row, columnsCnt++, fp);
        gp.getRowFormatter().setStyleName(row, getBackgroundStyleForRowInGrid(row, 2));
    }
}
