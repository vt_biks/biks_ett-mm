/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.biadmin;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import pl.bssg.metadatapump.common.SAPBOScheduleBean;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.FilterGridPageBase;
import static pl.fovis.client.bikpages.biadmin.BIAObjectManagerPage.KIND_TO_ICON_MAP;
import pl.fovis.client.bikwidgets.HeaderCheckbox;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.client.dialogs.TreeSelectorBIAInstanceConnectionsDialog;
import pl.fovis.client.dialogs.TreeSelectorBIAInstanceFoldersDialog;
import pl.fovis.client.dialogs.TreeSelectorBIAInstanceReportsDialog;
import pl.fovis.client.dialogs.biadmin.BIAInstanceScheduleDialog;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBranchBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class BIAInstanceManagerPage extends FilterGridPageBase<SAPBOScheduleBean> {

    protected PushButton pauseInstanceBtn;
    protected PushButton resumeInstanceBtn;
    protected PushButton deleteInstanceBtn;
    protected PushButton rescheduleInstanceBtn;
    protected BikCustomButton exportToExcelPb;

    protected PushButton clearFiltersBtn;
    protected PushButton stopQueryBtn;
    protected PushButton refreshBtn;
    //
    protected ListBox statusLbx;
    protected ListBox recurringLbx;
    protected ListBox dateLbx;
    protected TextBox instanceId;
    protected TextBox reportId;
    protected TextBox ownerName;
    protected TextBox folderId;
    protected DateBox startDate;
    protected DateBox endDate;
    protected SAPBOScheduleBean bean;

    protected HTML selectedReportsLbl;
    protected HTML selectedFoldersLbl;
    protected HTML selectedConnectionsLbl;

    protected List<String> reportsObjId;
    protected List<String> ancestorsObjId;
    protected List<Integer> connectionsNodeId;
    protected Set<TreeNodeBranchBean> selectedReportNodes;
    protected Set<TreeNodeBranchBean> selectedFolderNodes;
    protected Set<TreeNodeBranchBean> selectedConnectionNodes;

    @Override
    protected void optAddActionButtonsToPanel(HorizontalPanel actionPanel, HorizontalPanel actionHorizontalPanel) {
        actionPanel.add(pauseInstanceBtn);
        actionPanel.add(resumeInstanceBtn);
        actionPanel.add(rescheduleInstanceBtn);
        actionPanel.add(deleteInstanceBtn);
        actionHorizontalPanel.add(exportToExcelPb);
    }

    @Override
    protected void createActionButtons() {
        // pause
        pauseInstanceBtn = new PushButton(I18n.scheduleWstrzymaj.get());
        NewLookUtils.makeCustomPushButton(pauseInstanceBtn);
        pauseInstanceBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.scheduleTrwaWykonywanieAkcji.get(), true);
                BIKClientSingletons.getService().pauseBIAInstances(getSelectedObjectsIds(), new StandardAsyncCallback<Void>("Error in pauseBIAInstances") {

                    @Override
                    public void onSuccess(Void result) {
                        infoDialog.hideDialog();
                        BIKClientSingletons.showInfo(I18n.scheduleWstrzymanoInstancje.get());
                        refreshGrid();
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        infoDialog.hideDialog();
                        super.onFailure(caught);
                    }
                });
            }
        });
        //resume
        resumeInstanceBtn = new PushButton(I18n.scheduleWznow.get());
        NewLookUtils.makeCustomPushButton(resumeInstanceBtn);
        resumeInstanceBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.scheduleTrwaWykonywanieAkcji.get(), true);
                BIKClientSingletons.getService().resumeBIAInstances(getSelectedObjectsIds(), new StandardAsyncCallback<Void>("Error in resumeBIAInstances") {

                    @Override
                    public void onSuccess(Void result) {
                        infoDialog.hideDialog();
                        BIKClientSingletons.showInfo(I18n.scheduleWznowionoInstancje.get());
                        refreshGrid();
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        infoDialog.hideDialog();
                        super.onFailure(caught);
                    }
                });
            }
        });
        // reschedule
        rescheduleInstanceBtn = new PushButton(I18n.scheduleHarmonogramujPonownie.get());
        NewLookUtils.makeCustomPushButton(rescheduleInstanceBtn);
        rescheduleInstanceBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new BIAInstanceScheduleDialog().buildAndShowDialog(new IParametrizedContinuation<SAPBOScheduleBean>() {

                    @Override
                    public void doIt(SAPBOScheduleBean param) {
                        final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                        infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.scheduleTrwaWykonywanieAkcji.get(), true);
                        BIKClientSingletons.getService().rescheduleBIAInstances(getSelectedObjectsIds(), param, new StandardAsyncCallback<Void>("Error in rescheduleInstances") {

                            @Override
                            public void onSuccess(Void result) {
                                infoDialog.hideDialog();
                                BIKClientSingletons.showInfo(I18n.scheduleZaharmonogramowano.get());
                                refreshGrid();
                            }

                            @Override
                            public void onFailure(Throwable caught) {
                                infoDialog.hideDialog();
                                super.onFailure(caught);
                            }
                        });
                    }
                });

            }
        });
        // delete
        deleteInstanceBtn = new PushButton(I18n.scheduleUsun.get());
        NewLookUtils.makeCustomPushButton(deleteInstanceBtn);
        deleteInstanceBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.scheduleTrwaWykonywanieAkcji.get(), true);
                BIKClientSingletons.getService().deleteBIAInstances(getSelectedObjectsIds(), new StandardAsyncCallback<Void>("Error in deleteBIAInstances") {

                    @Override
                    public void onSuccess(Void result) {
                        infoDialog.hideDialog();
                        BIKClientSingletons.showInfo(I18n.scheduleUsunieto.get());
                        refreshGrid();
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        infoDialog.hideDialog();
                        super.onFailure(caught);
                    }
                });
            }
        });
        exportToExcelPb = createExportResultsBtn();
        exportToExcelPb.setVisible(false);
        exportToExcelPb.addStyleName("BIAdmin");
    }

    @Override
    protected void createFilters(VerticalPanel leftVp) {
        // Filtry
        FlexTable filterWidgets = new FlexTable();
        filterWidgets.setCellSpacing(5);
        // status
        filterWidgets.setWidget(0, 0, new Label(I18n.status.get()));
        filterWidgets.setWidget(0, 1, statusLbx = new ListBox());
        for (Status value : Status.values()) {
            statusLbx.addItem(value.getName(), String.valueOf(value.getValue()));
        }
        statusLbx.setWidth("160px");

        // cykliczny
        filterWidgets.setWidget(1, 0, new Label(I18n.scheduleCykliczny.get()));
        filterWidgets.setWidget(1, 1, recurringLbx = new ListBox());
        recurringLbx.addItem("<" + I18n.wszystkie.get() + ">", "-1");
        recurringLbx.addItem(I18n.tak.get(), "1");
        recurringLbx.addItem(I18n.nie.get(), "0");
        recurringLbx.setWidth("160px");
        // data
        filterWidgets.setWidget(2, 0, new Label(I18n.data.get()));
        filterWidgets.setWidget(2, 1, dateLbx = new ListBox());
        dateLbx.addItem(I18n.scheduleOstatniDzien.get(), "24");
        dateLbx.addItem(I18n.scheduleOstatniaGodzina.get(), "1");
        dateLbx.addItem(I18n.scheduleOstatniTydzien.get(), "168");
        dateLbx.addItem(I18n.scheduleOstatniMiesiac.get(), "720");
        dateLbx.addItem(I18n.scheduleZPrzedzialu.get(), "0");
        dateLbx.addItem("<" + I18n.wszystkie.get() + ">", "-1");
        dateLbx.setWidth("160px");
        dateLbx.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                int selectedValue = BaseUtils.tryParseInt(dateLbx.getSelectedValue());
                boolean isBetweenDate = selectedValue == 0;
                startDate.setEnabled(isBetweenDate);
                endDate.setEnabled(isBetweenDate);
                startDate.setValue(null);
                endDate.setValue(null);
            }
        });

        // start
        DateTimeFormat dateFormat = DateTimeFormat.getFormat("yyyy-MM-dd hh:mm:ss");
        filterWidgets.setWidget(3, 0, new Label(I18n.scheduleStart.get()));
        filterWidgets.setWidget(3, 1, startDate = new DateBox());
        startDate.setWidth("156px");
        startDate.setFormat(new DateBox.DefaultFormat(dateFormat));
        startDate.setEnabled(false);

        // end
        filterWidgets.setWidget(4, 0, new Label(I18n.scheduleKoniec.get()));
        filterWidgets.setWidget(4, 1, endDate = new DateBox());
        endDate.setFormat(new DateBox.DefaultFormat(dateFormat));
        endDate.setEnabled(false);
        endDate.setWidth("156px");
        // właściciel
        filterWidgets.setWidget(5, 0, new Label(I18n.scheduleWlasciciel.get()));
        filterWidgets.setWidget(5, 1, ownerName = new TextBox());
        ownerName.setText("*");
        ownerName.setWidth("152px");
        // id instancji
        filterWidgets.setWidget(6, 0, new Label(I18n.scheduleInstanceId.get()));
        filterWidgets.setWidget(6, 1, instanceId = new TextBox());
        instanceId.setText("*");
        instanceId.setWidth("152px");
        // id raportu
        filterWidgets.setWidget(7, 0, new Label(I18n.scheduleIDRaportu.get()));
        filterWidgets.setWidget(7, 1, reportId = new TextBox());
        reportId.setText("*");
        reportId.setWidth("152px");
        // id folderu
        filterWidgets.setWidget(8, 0, new Label(I18n.scheduleidFolderu.get()));
        filterWidgets.setWidget(8, 1, folderId = new TextBox());
        folderId.setText("*");
        folderId.setWidth("152px");
        //drzewko raportów

        HorizontalPanel rHp = new HorizontalPanel();
        rHp.setWidth("158px");
        rHp.add(selectedReportsLbl = new HTML(""));
        rHp.add(createFillterByReportsOrFoldersButton(Type.Reports));

        filterWidgets.setWidget(9, 0, new Label(I18n.scheduleReports.get()));
        filterWidgets.setWidget(9, 1, rHp);
        //drzewko folderow

        HorizontalPanel fHp = new HorizontalPanel();
        fHp.setWidth("158px");
        fHp.add(selectedFoldersLbl = new HTML(""));
        fHp.add(createFillterByReportsOrFoldersButton(Type.Folders));

        filterWidgets.setWidget(10, 0, new Label(I18n.scheduleFolders.get()));
        filterWidgets.setWidget(10, 1, fHp);

        HorizontalPanel cHp = new HorizontalPanel();
        cHp.setWidth("100%");
        cHp.setWidth("100%");
        cHp.add(selectedConnectionsLbl = new HTML(""));
        cHp.add(createFillterByReportsOrFoldersButton(Type.Connections));

        filterWidgets.setWidget(11, 0, new Label(I18n.scheduleConnections.get()));
        filterWidgets.setWidget(11, 1, cHp);

        refreshBtn = new PushButton(I18n.odswiez.get());
        NewLookUtils.makeCustomPushButton(refreshBtn);
        refreshBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                refreshGrid();
            }
        });

        HorizontalPanel buttonToolsPanel = new HorizontalPanel();
        buttonToolsPanel.setSpacing(0);
        clearFiltersBtn = new PushButton(I18n.deleteFilters.get());
        NewLookUtils.makeCustomPushButton(clearFiltersBtn);
        clearFiltersBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                clearFilters();
            }
        });
        stopQueryBtn = new PushButton(I18n.przerwij.get());
        NewLookUtils.makeCustomPushButton(stopQueryBtn);
        stopQueryBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                stopQueryBtn.setVisible(false);
                BIKClientSingletons.getService().stopBIAQuery(new StandardAsyncCallback<Void>("Error in stopBIAQuery") {

                    @Override
                    public void onSuccess(Void result) {
//                        BIKClientSingletons.showInfo(I18n.);
                    }
                });
            }
        });
        stopQueryBtn.setVisible(false);
        buttonToolsPanel.add(clearFiltersBtn);
        buttonToolsPanel.add(stopQueryBtn);
        filterWidgets.setWidget(12, 0, refreshBtn);
        filterWidgets.setWidget(12, 1, buttonToolsPanel);
        leftVp.add(filterWidgets);
//        leftVp.add(refreshBtn);
    }

    private BikCustomButton createExportResultsBtn() {
        return new BikCustomButton(I18n.eksportWynikow.get() /* I18N:  */, "importCsv" /* I18N: no */,
                new IContinuation() {

                    @Override
                    public void doIt() {
                        StringBuilder sb = new StringBuilder(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet());
                        sb.append("?").append(BIKConstants.BIAINSTANCE_RESULTS_EXPORT_PARAM_NAME).append("=").append(BIKConstants.EXPORT_FORMAT_XLSX).append("&")
                        .append(BIKConstants.BIAINSTANCE_PHRASE_PARAM_NAME).append("=").append(I18n.biInstances.get())
                        .append("&").append(BIKConstants.BIAINSTANCE_SCHEDULE_TYPE).append("=").append(bean.scheduleType)
                        .append("&").append(BIKConstants.BIAINSTANCE_RECURRING).append("=").append(bean.recurring)
                        .append("&").append(BIKConstants.BIAINSTANCE_ID).append("=").append(bean.id)
                        .append("&").append(BIKConstants.BIAINSTANCE_PARENT_ID).append("=").append(bean.parentId)
                        .append("&").append(BIKConstants.BIAINSTANCE_ANCESTOR_ID).append("=").append(bean.ancestorId)
                        .append("&").append(BIKConstants.BIAINSTANCE_INTERVAL_HOURS).append("=").append(bean.intervalHours);
                        if (bean.owner != null) {
                            sb.append("&").append(BIKConstants.BIAINSTANCE_OWNER).append("=").append(bean.owner);
                        }
                        if (bean.started != null) {
                            String startDate = SimpleDateUtils.toCanonicalStringWithMillis(bean.started);
                            sb.append("&").append(BIKConstants.BIAINSTANCE_STARTED).append("=").append(startDate);
                        }
                        if (bean.ended != null) {
                            sb.append("&").append(BIKConstants.BIAINSTANCE_ENDED).append("=").append(SimpleDateUtils.toCanonicalStringWithMillis(bean.ended));
                        }
                        if (!BaseUtils.isCollectionEmpty(bean.reportsId)) {
                            sb.append("&").append(BIKConstants.BIAINSTANCE_REPORTS).append("=").append(BaseUtils.mergeWithSepEx(bean.reportsId, ","));
                        }
                        if (!BaseUtils.isCollectionEmpty(bean.ancestorsId)) {
                            sb.append("&").append(BIKConstants.BIAINSTANCE_FOLDERS).append("=").append(BaseUtils.mergeWithSepEx(bean.ancestorsId, ","));
                        }
                        if (!BaseUtils.isCollectionEmpty(bean.connectionsNodeId)) {
                            sb.append("&").append(BIKConstants.BIAINSTANCE_CONNECTIONS).append("=").append(BaseUtils.mergeWithSepEx(bean.connectionsNodeId, ","));
                        }

                        Window.open(sb.toString(), "_blank", "");
                    }
                });
    }

    @Override
    protected void refreshActionButtons() {
        Set<SAPBOScheduleBean> selectedSet = selectionModel.getSelectedSet();
        boolean rowsSelected = !BaseUtils.isCollectionEmpty(selectedSet);
        pauseInstanceBtn.setVisible(rowsSelected);
        resumeInstanceBtn.setVisible(rowsSelected);
        rescheduleInstanceBtn.setVisible(rowsSelected);
        deleteInstanceBtn.setVisible(rowsSelected);
    }

    protected void refreshGrid() {
        stopQueryBtn.setVisible(true);
//        SAPBOScheduleBean
        bean = new SAPBOScheduleBean();
        bean.scheduleType = BaseUtils.tryParseInt(statusLbx.getSelectedValue());
        bean.recurring = BaseUtils.tryParseInt(recurringLbx.getSelectedValue());
        bean.id = BaseUtils.tryParseInteger(instanceId.getText(), -1);
        bean.parentId = BaseUtils.tryParseInteger(reportId.getText(), -1);
        String ownerText = ownerName.getText();
        bean.owner = BaseUtils.safeEqualsStr(ownerText, "*", true) ? null : ownerText;
        bean.ancestorId = BaseUtils.tryParseInteger(folderId.getText(), -1);
        int selectedDateFilter = BaseUtils.tryParseInt(dateLbx.getSelectedValue());
        bean.intervalHours = BaseUtils.tryParseInt(dateLbx.getSelectedValue());
        if (selectedDateFilter == 0) {
            bean.started = SimpleDateUtils.parseFromCanonicalString(startDate.getTextBox().getValue());
            bean.ended = SimpleDateUtils.parseFromCanonicalString(endDate.getTextBox().getValue());
        }
        bean.reportsId = reportsObjId;
        bean.ancestorsId = ancestorsObjId;
        bean.connectionsNodeId = connectionsNodeId;

        showLoadingWidget();
        refreshBtn.setEnabled(false);
        clearFiltersBtn.setEnabled(false);
        BIKClientSingletons.getService().getBIASchedules(bean, new StandardAsyncCallback<List<SAPBOScheduleBean>>("Error in getBIASchedules") {

            @Override
            public void onSuccess(List<SAPBOScheduleBean> result) {
                stopQueryBtn.setVisible(false);
                showDataGrid();
                List<SAPBOScheduleBean> list = dataProvider.getList();
                list.clear();
                list.addAll(result);
//                if (result.size() >= 1000) {
//                    new SimpleInfoDialog().buildAndShowDialog(I18n.schedulePobranoDuzoRekordow.get(), null, null);
//                }
                exportToExcelPb.setVisible(!list.isEmpty());
                dataProvider.refresh();
                selectionModel.clear();
                refreshActionButtons();
                dataGrid.redrawHeaders();
                refreshBtn.setEnabled(true);
                clearFiltersBtn.setEnabled(true);
            }

            @Override
            public void onFailure(Throwable caught) {
                stopQueryBtn.setVisible(false);
                showDataGrid();
                refreshBtn.setEnabled(true);
                clearFiltersBtn.setEnabled(true);
//                new SimpleInfoDialog().buildAndShowDialog("Error: " + caught.getMessage(), null, null);
                super.onFailure(caught);
            }
        });
    }

    @Override
    protected void initTableColumns(ListHandler<SAPBOScheduleBean> sortHandler) {
        // Checkbox column. This table will uses a checkbox column for selection.
        // Alternatively, you can call dataGrid.setSelectionEnabled(true) to enable
        // mouse selection.
        Column<SAPBOScheduleBean, Boolean> checkColumn = new Column<SAPBOScheduleBean, Boolean>(new CheckboxCell(true, false)) {
            @Override
            public Boolean getValue(SAPBOScheduleBean object) {
                // Get the value from the selection model.
                return selectionModel.isSelected(object);
            }
        };

        checkColumn.setFieldUpdater(new FieldUpdater<SAPBOScheduleBean, Boolean>() {

            @Override
            public void update(int index, SAPBOScheduleBean object, Boolean value) {
                selectionModel.setSelected(object, true);
                dataProvider.refresh();
            }
        });

//        dataGrid.setSelectionModel(selectionModel);
        Header<Boolean> selectAllHeader = new Header<Boolean>(new HeaderCheckbox()) {

            @Override
            public Boolean getValue() {

                for (SAPBOScheduleBean item : dataGrid.getVisibleItems()) {
                    if (!selectionModel.isSelected(item)) {
                        return false;
                    }
                }
                return dataGrid.getVisibleItems().size() > 0;
            }
        };
        selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {

            @Override
            public void update(Boolean value) {
                for (SAPBOScheduleBean object : dataGrid.getVisibleItems()) {

                    selectionModel.setSelected(object, value);
                }
            }
        });

        dataGrid.addColumn(checkColumn, selectAllHeader/*, SafeHtmlUtils.fromSafeConstant("<br/>")*/);
        dataGrid.setColumnWidth(checkColumn, "40px"/*, Unit.PX*/);

        // ID
        Column<SAPBOScheduleBean, String> idColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return object.getId().toString();
            }
        };
        idColumn.setSortable(true);
        sortHandler.setComparator(idColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        dataGrid.addColumn(idColumn, I18n.scheduleID.get());
        dataGrid.setColumnWidth(idColumn, "100px"/*, Unit.PX*/);

        // Nazwa
        Column<SAPBOScheduleBean, String> nameColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return object.getName();
            }
        };
        nameColumn.setSortable(true);
        sortHandler.setComparator(nameColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        dataGrid.addColumn(nameColumn, I18n.scheduleNazwa.get());
        dataGrid.setColumnWidth(nameColumn, "300px"/*, Unit.PX*/);

        // Status
        Column<SAPBOScheduleBean, String> statusColumn
                = new Column<SAPBOScheduleBean, String>(new TextCell()) {
                    @Override
                    public String getValue(SAPBOScheduleBean object) {
                        return BIKCenterUtils.getStatus(object.getScheduleType());
                    }
                };
        statusColumn.setSortable(true);
        sortHandler.setComparator(statusColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return BIKCenterUtils.getStatus(o1.getScheduleType()).compareTo(BIKCenterUtils.getStatus(o2.getScheduleType()));
            }
        });
        dataGrid.addColumn(statusColumn, I18n.scheduleStatus.get());
        dataGrid.setColumnWidth(statusColumn, "180px"/*, Unit.PX*/);

        // Lokalizacja
        Column<SAPBOScheduleBean, String> pathColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return object.getLocation();
            }
        };
        pathColumn.setSortable(true);
        sortHandler.setComparator(pathColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getLocation().compareTo(o2.getLocation());
            }
        });
        dataGrid.addColumn(pathColumn, I18n.lokalizacja.get());
        dataGrid.setColumnWidth(pathColumn, "450px"/*, Unit.PX*/);

        // Typ
        Column<SAPBOScheduleBean, SafeHtml> typeColumn = new Column<SAPBOScheduleBean, SafeHtml>(new SafeHtmlCell()) {
            @Override
            public SafeHtml getValue(SAPBOScheduleBean object) {
                return SafeHtmlUtils.fromSafeConstant("<img src='images/" + KIND_TO_ICON_MAP.get(object.getKind()) + ".gif' style='vertical-align: middle'> " + object.getKind());
            }
        };
        typeColumn.setSortable(true);
        sortHandler.setComparator(typeColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getKind().compareTo(o2.getKind());
            }
        });
        dataGrid.addColumn(typeColumn, I18n.typObiektu.get());
        dataGrid.setColumnWidth(typeColumn, "150px"/*, Unit.PX*/);

        // Start
        Column<SAPBOScheduleBean, String> startColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return SimpleDateUtils.toCanonicalString(object.getStarted());
            }
        };
        startColumn.setSortable(true);
        sortHandler.setComparator(startColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                if (o1.getStarted() == null) {
                    if (o2.getStarted() == null) {
                        return 0; //equal
                    } else {
                        return -1; // null is before other strings
                    }
                } else // this.member != null
                if (o2.getStarted() == null) {
                    return 1;  // all other strings are after null
                } else {
                    return o1.getStarted().compareTo(o2.getStarted());
                }
            }
        });
        dataGrid.addColumn(startColumn, I18n.scheduleStart.get());
        dataGrid.setColumnWidth(startColumn, "200px"/*, Unit.PX*/);

        // End
        Column<SAPBOScheduleBean, String> endColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return SimpleDateUtils.toCanonicalString(object.getEnded());
            }
        };
        endColumn.setSortable(true);
        sortHandler.setComparator(endColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                if (o1.getEnded() == null) {
                    if (o2.getEnded() == null) {
                        return 0; //equal
                    } else {
                        return -1; // null is before other strings
                    }
                } else // this.member != null
                if (o2.getEnded() == null) {
                    return 1;  // all other strings are after null
                } else {
                    return o1.getEnded().compareTo(o2.getEnded());
                }
            }
        });
        dataGrid.addColumn(endColumn, I18n.scheduleKoniec.get());
        dataGrid.setColumnWidth(endColumn, "200px"/*, Unit.PX*/);

        // Czas trwania
        Column<SAPBOScheduleBean, String> durationColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return BIKCenterUtils.getDuration(object);
            }
        };
        durationColumn.setSortable(true);
        sortHandler.setComparator(durationColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return BIKCenterUtils.getDuration(o1).compareTo(BIKCenterUtils.getDuration(o2));
            }
        });
        dataGrid.addColumn(durationColumn, I18n.scheduleCzasTrwania.get());
        dataGrid.setColumnWidth(durationColumn, "150px"/*, Unit.PX*/);

        // Recurring
        Column<SAPBOScheduleBean, String> recurringColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return BIKCenterUtils.getRecurring(object.getRecurring());
            }
        };
        recurringColumn.setSortable(true);
        sortHandler.setComparator(recurringColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return BIKCenterUtils.getRecurring(o1.getRecurring()).compareTo(BIKCenterUtils.getRecurring(o2.getRecurring()));
            }
        });
        dataGrid.addColumn(recurringColumn, I18n.scheduleCykliczny.get());
        dataGrid.setColumnWidth(recurringColumn, "100px"/*, Unit.PX*/);

        // Owner
        Column<SAPBOScheduleBean, String> ownerColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return object.getOwner();
            }
        };
        ownerColumn.setSortable(true);
        sortHandler.setComparator(ownerColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getOwner().compareTo(o2.getOwner());
            }
        });
        dataGrid.addColumn(ownerColumn, I18n.scheduleWlasciciel.get());
        dataGrid.setColumnWidth(ownerColumn, "200px"/*, Unit.PX*/);

        // ID raportu
        Column<SAPBOScheduleBean, String> reportIDColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return object.getParentId().toString();
            }
        };
        reportIDColumn.setSortable(true);
        sortHandler.setComparator(reportIDColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getParentId().compareTo(o2.getParentId());
            }
        });
        dataGrid.addColumn(reportIDColumn, I18n.scheduleIDRaportu.get());
        dataGrid.setColumnWidth(reportIDColumn, "100px"/*, Unit.PX*/);

        // Utworzono
        Column<SAPBOScheduleBean, String> createdColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return SimpleDateUtils.toCanonicalString(object.getCreationTime());
            }
        };
        createdColumn.setSortable(true);
        sortHandler.setComparator(createdColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return SimpleDateUtils.toCanonicalString(o1.getCreationTime()).compareTo(SimpleDateUtils.toCanonicalString(o2.getCreationTime()));
            }
        });
        dataGrid.addColumn(createdColumn, I18n.scheduleUtworzono.get());
        dataGrid.setColumnWidth(createdColumn, "200px"/*, Unit.PX*/);

        // Zmodyfikowano
        Column<SAPBOScheduleBean, String> updatedColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return SimpleDateUtils.toCanonicalString(object.getModify());
            }
        };
        updatedColumn.setSortable(true);
        sortHandler.setComparator(updatedColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return SimpleDateUtils.toCanonicalString(o1.getModify()).compareTo(SimpleDateUtils.toCanonicalString(o2.getModify()));
            }
        });
        dataGrid.addColumn(updatedColumn, I18n.scheduleZmodyfikowano.get());
        dataGrid.setColumnWidth(updatedColumn, "200px"/*, Unit.PX*/);

        // CUID
        Column<SAPBOScheduleBean, String> cuidColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return object.getCuid();
            }
        };
        cuidColumn.setSortable(true);
        sortHandler.setComparator(cuidColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getCuid().compareTo(o2.getCuid());
            }
        });
        dataGrid.addColumn(cuidColumn, I18n.scheduleCUID.get());
        dataGrid.setColumnWidth(cuidColumn, "200px"/*, Unit.PX*/);

        // Miejsce docelowe
        Column<SAPBOScheduleBean, String> destinationColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return object.getDestination();
            }
        };
        destinationColumn.setSortable(true);
        sortHandler.setComparator(destinationColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getDestination().compareTo(o2.getDestination());
            }
        });
        dataGrid.addColumn(destinationColumn, I18n.scheduleMiejsceDocelowe.get());
        dataGrid.setColumnWidth(destinationColumn, "150px"/*, Unit.PX*/);

        // Miejsce docelowe - szczegóły
        Column<SAPBOScheduleBean, String> destinationDetailsColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return object.getDestinationPaths();
            }
        };
        destinationDetailsColumn.setSortable(true);
        sortHandler.setComparator(destinationDetailsColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getDestinationPaths().compareTo(o2.getDestinationPaths());
            }
        });
        dataGrid.addColumn(destinationDetailsColumn, I18n.scheduleMiejsceDoceloweSzczegoly.get());
        dataGrid.setColumnWidth(destinationDetailsColumn, "300px"/*, Unit.PX*/);

        // Formaty
//        Column<SAPBOScheduleBean, String> formatColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
//            @Override
//            public String getValue(SAPBOScheduleBean object) {
//                return object.getFormat();
//            }
//        };
//        formatColumn.setSortable(true);
//        sortHandler.setComparator(formatColumn, new Comparator<SAPBOScheduleBean>() {
//            @Override
//            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
//                return o1.getFormat().compareTo(o2.getFormat());
//            }
//        });
//        dataGrid.addColumn(formatColumn, I18n.formaty.get());
//        dataGrid.setColumnWidth(formatColumn, "150px"/*, Unit.PX*/);
        // Błąd
        Column<SAPBOScheduleBean, String> errorColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return object.getErrorMessage();
            }
        };
        errorColumn.setSortable(true);
        sortHandler.setComparator(errorColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getErrorMessage().compareTo(o2.getErrorMessage());
            }
        });
        dataGrid.addColumn(errorColumn, I18n.scheduleBlad.get());
        dataGrid.setColumnWidth(errorColumn, "500px"/*, Unit.PX*/);

        //Serwer
        Column<SAPBOScheduleBean, String> serverColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return object.getServer();
            }
        };
        serverColumn.setSortable(true);
        sortHandler.setComparator(serverColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getServer().compareTo(o2.getServer());
            }
        });
        dataGrid.addColumn(serverColumn, I18n.serwer.get());
        dataGrid.setColumnWidth(serverColumn, "300px"/*, Unit.PX*/);

        // Data modyfikacji raportu
        Column<SAPBOScheduleBean, String> modifyTimeReportColumn = new Column<SAPBOScheduleBean, String>(new TextCell()) {
            @Override
            public String getValue(SAPBOScheduleBean object) {
                return SimpleDateUtils.toCanonicalString(object.getModifyTimeReport());
            }
        };
        modifyTimeReportColumn.setSortable(true);
        sortHandler.setComparator(modifyTimeReportColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return SimpleDateUtils.toCanonicalString(o1.getModifyTimeReport()).compareTo(SimpleDateUtils.toCanonicalString(o2.getModifyTimeReport()));
            }
        });
        dataGrid.addColumn(modifyTimeReportColumn, I18n.scheduleZmodyfikowanoRaport.get());
        dataGrid.setColumnWidth(modifyTimeReportColumn, "170px"/*, Unit.PX*/);

        // Świat obiektów
        Column<SAPBOScheduleBean, SafeHtml> universeColumn = new Column<SAPBOScheduleBean, SafeHtml>(new SafeHtmlCell()) {
            @Override
            public SafeHtml getValue(SAPBOScheduleBean object) {
                return SafeHtmlUtils.fromSafeConstant(BaseUtils.safeToStringDef(object.getUniverses(), ""));
            }
        };
        universeColumn.setSortable(true);
        sortHandler.setComparator(universeColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getUniverses().compareTo(o2.getUniverses());
            }
        });

        dataGrid.addColumn(universeColumn, I18n.scheduleUniverses.get());
        dataGrid.setColumnWidth(universeColumn, "300px"/*, Unit.PX*/);

        // Data modyfikacji świata obiektów
        Column<SAPBOScheduleBean, SafeHtml> modifyTimeUniverseColumn = new Column<SAPBOScheduleBean, SafeHtml>(new SafeHtmlCell()) {
            @Override
            public SafeHtml getValue(SAPBOScheduleBean object) {
                return SafeHtmlUtils.fromSafeConstant(BaseUtils.safeToStringDef(object.getModifyTimeUniverse(), ""));
            }
        };
        modifyTimeUniverseColumn.setSortable(true);
        sortHandler.setComparator(modifyTimeUniverseColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getModifyTimeUniverse().compareTo(o2.getModifyTimeUniverse());
            }
        });
        dataGrid.addColumn(modifyTimeUniverseColumn, I18n.scheduleZmodyfikowanoSwiatobiektow.get());
        dataGrid.setColumnWidth(modifyTimeUniverseColumn, "400px"/*, Unit.PX*/);

        // Połączenie
        Column<SAPBOScheduleBean, SafeHtml> connectionsColumn = new Column<SAPBOScheduleBean, SafeHtml>(new SafeHtmlCell()) {
            @Override
            public SafeHtml getValue(SAPBOScheduleBean object) {
                return SafeHtmlUtils.fromSafeConstant(BaseUtils.safeToStringDef(object.getConnections(), ""));
            }
        };
        connectionsColumn.setSortable(true);
        sortHandler.setComparator(connectionsColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getConnections().compareTo(o2.getConnections());
            }
        });
        dataGrid.addColumn(connectionsColumn, I18n.scheduleConnection.get());
        dataGrid.setColumnWidth(connectionsColumn, "250px"/*, Unit.PX*/);

        // Silnik bazy danych
        Column<SAPBOScheduleBean, SafeHtml> databaseEngineColumn = new Column<SAPBOScheduleBean, SafeHtml>(new SafeHtmlCell()) {

            @Override
            public SafeHtml getValue(SAPBOScheduleBean object) {

                return SafeHtmlUtils.fromSafeConstant(BaseUtils.safeToStringDef(object.getDatabaseEngine(), ""));
            }

        };
        databaseEngineColumn.setSortable(true);
        sortHandler.setComparator(databaseEngineColumn, new Comparator<SAPBOScheduleBean>() {
            @Override
            public int compare(SAPBOScheduleBean o1, SAPBOScheduleBean o2) {
                return o1.getDatabaseEngine().compareTo(o2.getDatabaseEngine());
            }
        });
        dataGrid.addColumn(databaseEngineColumn, I18n.scheduleDatabaseEngine.get());
        dataGrid.setColumnWidth(databaseEngineColumn, "450px"/*, Unit.PX*/);

    }

    @Override
    public String getId() {
        return "admin:biadmin:instanceManager";
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        // NO OP
    }

    @Override
    public boolean isPageEnabled() {
        return super.isPageEnabled() && BIKClientSingletons.isBIAdminEnabled();
    }

    @Override
    protected int getGridHeight() {
        return 5650;
    }

    enum Status {

        // from ISchedulingInfo.ScheduleStatus.
        ALL("<" + I18n.wszystkie.get() + ">", -1), RUNNING(I18n.scheduleRunning.get(), 0), SUCCESS(I18n.scheduleComplete.get(), 1), FAILURE(I18n.scheduleFailure.get(), 3), PAUSED(I18n.schedulePaused.get(), 8), PENDING(I18n.schedulePending.get(), 9);

        Status(String name, int value) {
            this.name = name;
            this.value = value;
        }

        protected String name;
        protected int value;

        public String getName() {
            return name;
        }

        public int getValue() {
            return value;
        }
    }

    public static enum Type {

        Reports,
        Folders,
        Connections
    }

    protected PushButton createFillterByReportsOrFoldersButton(final Type type) {
        PushButton fillterBtn = new PushButton(new Image("images/edit_yellow.png" /* I18N: no */), new Image("images/edit_yellow.png" /* I18N: no */));
        fillterBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().getBIAInstanceTreeCode(type.name(), new StandardAsyncCallback<String>() {

                    @Override
                    public void onSuccess(String result) {
                        switch (type) {
                            case Reports:
                                buildTreeSelectorReportsDialog(result);
                                break;
                            case Folders:
                                buildTreeSelectorFoldersDialog(result);
                                break;
                            case Connections:
                                buildTreeSelectorConnectionsDialog(result);
                                break;

                        }
                    }
                });

            }
        });
        fillterBtn.setStyleName("admin-btn");
        fillterBtn.setWidth("16px");
        fillterBtn.setTitle(I18n.edytuj.get() /* I18N:  */);
        return fillterBtn;
    }

    protected void buildTreeSelectorReportsDialog(String code) {

        new TreeSelectorBIAInstanceReportsDialog().buildAndShowDialog(selectedReportNodes, code, new IParametrizedContinuation<Set<TreeNodeBranchBean>>() {

            @Override
            public void doIt(Set<TreeNodeBranchBean> param) {
                selectedReportNodes = param;
                selectedReportsLbl.setText(param.isEmpty() ? "" : I18n.scheduleSelected.get() + " " + param.size());
                reportsObjId = new ArrayList<String>();
                for (TreeNodeBranchBean tb : param) {
                    reportsObjId.add(tb.objId);

                }
            }
        }
        );

    }

    protected void buildTreeSelectorFoldersDialog(String code) {
        new TreeSelectorBIAInstanceFoldersDialog().buildAndShowDialog(selectedFolderNodes, code, new IParametrizedContinuation<Set<TreeNodeBranchBean>>() {

            @Override
            public void doIt(Set<TreeNodeBranchBean> param) {
                selectedFolderNodes = param;
                selectedFoldersLbl.setText(param.isEmpty() ? "" : I18n.scheduleSelected.get() + " " + param.size());
                ancestorsObjId = new ArrayList<String>();
                for (TreeNodeBranchBean tb : param) {
                    ancestorsObjId.add(tb.objId);

                }
            }
        }
        );
    }

    protected void buildTreeSelectorConnectionsDialog(String code) {
        new TreeSelectorBIAInstanceConnectionsDialog().buildAndShowDialog(selectedConnectionNodes, code, new IParametrizedContinuation<Set<TreeNodeBranchBean>>() {

            @Override
            public void doIt(final Set<TreeNodeBranchBean> param) {
                selectedConnectionNodes = param;
                selectedConnectionsLbl.setText(param.isEmpty() ? "" : I18n.scheduleSelected.get() + " " + param.size());
                connectionsNodeId = new ArrayList<Integer>();
                for (TreeNodeBranchBean tb : param) {
                    connectionsNodeId.add(tb.nodeId);
                }

            }
        }
        );
    }

    @SuppressWarnings("unchecked")
    protected void clearFilters() {
        clearTb(instanceId, reportId, ownerName, folderId);
        clearHTML(selectedReportsLbl, selectedFoldersLbl, selectedConnectionsLbl);
        clearListBox(statusLbx, recurringLbx, dateLbx);
        clearDateBox(startDate, endDate);
        clearList(reportsObjId, ancestorsObjId);
        clearList(connectionsNodeId);
        clearList(selectedReportNodes, selectedFolderNodes, selectedConnectionNodes);
    }
}
