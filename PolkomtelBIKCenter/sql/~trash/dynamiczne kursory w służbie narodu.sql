/*
select 
	table_name, column_name, 
	data_type + case data_type
		when 'sql_variant' then ''
		when 'text' then ''
		when 'ntext' then ''
		when 'xml' then ''
		when 'decimal' then '(' + cast(numeric_precision as varchar) + ', ' + cast(numeric_scale as varchar) + ')'
		when 'numeric' then '(' + cast(numeric_precision as varchar) + ', ' + cast(numeric_scale as varchar) + ')'
		else coalesce('('+case when character_maximum_length = -1 then 'max' else cast(character_maximum_length as varchar) end +')','')
  end as column_type
from information_schema.columns
where table_name like 'bik_%' and data_type = 'varchar' and character_maximum_length = -1
order by table_name, ordinal_position;
*/

declare @sql_txt nvarchar(max) = '';

select --top 10
  @sql_txt = @sql_txt + case when @sql_txt = '' then '' else ' union all ' end + 
  'select ' + quotename(column_name) + ' as txt, ' + quotename(table_name + '.' + column_name, '''') + ' as src from ' + quotename(table_name)
from information_schema.columns
where table_name like 'bik_%' and data_type = 'varchar' and character_maximum_length = -1
;

set @sql_txt = 'set @my_cur = cursor fast_forward for select txt, src from (' + @sql_txt + ') as x where txt like ''%file[_]%'' and txt like ''%fsb?get=%''; open @my_cur;';

--print @sql_txt;

declare @cur cursor;

execute sp_executesql @sql_txt, N'@my_cur cursor output', @my_cur = @cur output;

declare @res table (file_name varchar(max), src varchar(1000));

declare @txt varchar(max), @src varchar(1000);

--open @cur;

fetch next from @cur into @txt, @src;

while @@fetch_status = 0 begin

  declare @prev_pos bigint = 1;

  while 1 = 1 begin

    declare @pos bigint = charindex('fsb?get=', @txt, @prev_pos);

    if @pos <= 0 break;

    declare @file_name_start_pos bigint = @pos + 8;

    declare @end_pos bigint = (select min(pos) from (values (charindex('''', @txt, @file_name_start_pos)), (charindex('"', @txt, @file_name_start_pos))) as x(pos) where x.pos > 0);

    if @end_pos is not null insert into @res values (substring(@txt, @file_name_start_pos, @end_pos - @file_name_start_pos), @src);

    set @prev_pos = @end_pos + 1;

  end;

  fetch next from @cur into @txt, @src;
  
end;

deallocate @cur;

declare @biks_upload_dir varchar(max) = 'C:\Temp\bikcenter-upload\'; -- mo�e by� puste

if coalesce(@biks_upload_dir, '') = ''
  select distinct * from @res order by file_name;
else
begin
  if @biks_upload_dir not like '%\' set @biks_upload_dir = @biks_upload_dir + '\';
  
  select cmd from (
  select 'echo. > $$$missing_files$$$.txt' as cmd, '' as file_name, '' as src
  union all
  select 
    'if not exist "' + @biks_upload_dir + /*'x' +*/ file_name + '" echo ' + src + ' : ' + file_name + ' >> $$$missing_files$$$.txt', file_name, src    
  from (select file_name, src, row_number() over (order by src, file_name) as row_num from (select distinct file_name, src from @res) as x) as x
  ) as x order by src, file_name;   
end;


--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------

if  exists (select * from sys.objects where object_id = object_id(N'[dbo].[fn_str_contains_any]') and type in (N'fn', N'if', N'tf', N'fs', N'ft'))
drop function [dbo].[fn_str_contains_any]
go

create function [dbo].[fn_str_contains_any](@str varchar(max), @items varchar(max), @sep varchar(max), @trim_opt int)
returns int
as
begin  
  return coalesce((select min(idx) from dbo.fn_split_by_sep(@items, @sep, @trim_opt) where @str like '%' + str + '%'), 0);
end;
go

--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------

select * from bik_attribute_linked x inner join bik_node n on n.id = x.node_id where value like '%file_1979174692055046807.png%';

select * from bik_searchable_attr_val x inner join bik_node n on n.id = x.node_id where value like '%file_1979174692055046807.png%';

select * 
from bik_help h 
where dbo.fn_str_contains_any(text_help, 'file_3211446910564372475.png, file_4915013163361687958.png, file_5468664995068128348.png, file_7355470839962970541.png', ',', 7) <> 0;


select * from bik_metapedia x inner join bik_node n on n.id = x.node_id
where dbo.fn_str_contains_any(body, 
'file_38025561878870024.jpg ,
file_4998270903137453714.jpg, 
file_5953189999315815010.jpg ,
file_655132473331298885.jpg ,
file_6819257394332266540.jpg ,
file_7951992571059791491.jpg ,
file_808808855053520054.jpg ,
file_8346393430613155368.jpg ,
file_8763097965532486050.jpg ,
file_9099478658035552275.jpg '
, ',', 7) <> 0;


select * from bik_node where dbo.fn_str_contains_any(descr, 
',file_1191653276816154424.jpg 
,file_1232754509449428630.PNG 
,file_2126540887356114674.png 
,file_2300919433301949973.png 
,file_2652570802090550725.png 
,file_2738302221101358674.png 
,file_599568838226049989.png 
,file_6115788845652354372.jpg 
,file_6659711282769992310.jpg 
,file_7611120317926262419.jpg 
,file_8254750416035324298.png 
,file_8418357732298201964.png '
, ',', 7) <> 0;


