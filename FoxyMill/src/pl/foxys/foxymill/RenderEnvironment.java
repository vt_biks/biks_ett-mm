/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill;

import simplelib.IValueFormatter;
import java.util.Map;
import pl.foxys.foxymill.exprlang.IExpressionLanguage;
import simplelib.IPredicate;

/**
 *
 * @author wezyr
 */
public class RenderEnvironment {
    public Map<String, IExpressionLanguage> exprLangs;
    public CacheStorage cacheStorage;
    public IPredicate<String> enabledLoggerPredicate;
    private IValueFormatter vf;// = new ValueFormatterForHTML();

    public RenderEnvironment(IValueFormatter vf) {
        this.vf = vf;
    }

    public boolean isEnabledLoggerKind(String kind) {
        return enabledLoggerPredicate == null || enabledLoggerPredicate.project(kind);
    }

    public IValueFormatter getValueFormatter() {
        return vf;
    }

//    public ITag createCustomTag() {
//
//    }
}
