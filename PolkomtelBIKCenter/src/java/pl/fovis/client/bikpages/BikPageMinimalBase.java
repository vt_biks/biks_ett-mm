/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;

/**
 *
 * @author wezyr
 */
public abstract class BikPageMinimalBase implements IBikPage<Integer> {

    protected Widget pageWidget;

//    public IBikPage<Integer> getTab() {
//        return this;
//    }
    protected abstract Widget buildWidgets();

    //ww: to jest do usunięca stąd i z podklas - zakomentowane, nieużywane
//    public abstract String getCaptionDeprecated();
    final public Widget createMainWidget() {
        if (pageWidget == null) {
            pageWidget = buildWidgets();
        }
        return pageWidget;
    }

    public Integer getDisplayedNodeId() {
        return getSelectedNodeId();
    }

    @Override
    public boolean isPageEnabled() {
        return BIKClientSingletons.isBikPageEnabled(this);
    }

    public Integer getSelectedNodeId() {
        return null;
    }

    public ScrollPanel createMainScrollPanel(Widget w, Integer ifShorter) {
        ScrollPanel scMain = new ScrollPanel();
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(scMain, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT
                - (ifShorter != null ? ifShorter : 0));
//        scMain.setStyleName("EntityDetailsPane");
        scMain.add(w);
        return scMain;
    }
}
