package pl.bssg.metadatapump.ad;

import com4j.COM4J;
import com4j.Variant;
import com4j.typelibs.activeDirectory.IADs;
import com4j.typelibs.ado20.ClassFactory;
import com4j.typelibs.ado20.Field;
import com4j.typelibs.ado20.Fields;
import com4j.typelibs.ado20._Command;
import com4j.typelibs.ado20._Connection;
import com4j.typelibs.ado20._Recordset;
import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

public class ADPump extends MetadataPumpBase {

    private static ILameLogger logger = LameUtils.getMyLogger();
    protected static final String ATTRIBUTE_LIST_SEPARATOR = "%|%";
    protected String oneUserLoginName = null; // obecnie niewykorzystywane
//    protected _Connection con = null;
    protected Map<String, Boolean> userAttributesMap = PumpConstants.AD_USER_ATTRIBUTES;
    protected Map<String, Boolean> groupAttributesMap = PumpConstants.AD_GROUP_ATTRIBUTES;
    protected Map<String, Boolean> ouAttributesMap = PumpConstants.AD_OU_ATTRIBUTES;

    public ADPump() {
    }

    public ADPump(String loginName) {
        this.oneUserLoginName = loginName;
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_AD;
    }

    @Override
    public boolean isActionAfterNeeded() {
        return oneUserLoginName == null;
    }

//    public String getPumpGroup() {
//        return PumpConstants.PUMP_GROUP_AD;
//    }
    @Override
    protected Pair<Integer, String> startPump() {
        pumpLogger.pumpLog("Start: connecting to domain controller", 10);
        ADBean config = PumpUtils.readAdminBean("ad", ADBean.class, adhocDao);
        List<String> domains = BaseUtils.splitBySep(config.domains, ";", true);
        if (BaseUtils.isCollectionEmpty(domains)) { // domyślana domena
            IADs rootDSE = COM4J.getObject(IADs.class, "LDAP://rootDSE", null);
            String defaultNamingContext = (String) rootDSE.get("defaultNamingContext");
            domains.add(defaultNamingContext);
        }
        int step = 80 / domains.size();
        int i = 0;
        _Connection con = ClassFactory.createConnection();
        con.provider("ADsDSOObject");
        con.open("Active Directory Provider", ""/*default*/, ""/*default*/, -1/*default*/);
        _Command cmd = ClassFactory.createCommand();
        cmd.activeConnection(con);
        cmd.properties("Page Size").value(60);
        for (String domain : domains) {
            pumpLogger.pumpLog("Connect to domain: " + domain, 20 + i * step);
            i++;
            try {
                execNamedCommandWithCommit("truncateADtables");
                List<Map<String, String>> usersData = getUsers(domain, cmd, config.usersFilter);
                for (Map<String, String> uData : usersData) {
                    addParentDistinguishedNameForUser(uData);
                    execNamedCommandWithCommit("addOrModifyADUser", uData.get("s_a_m_account_name"), domain, uData);
                }
                List<Map<String, String>> groupsData = getGroups(domain, cmd, config.filter);
//                execNamedCommandWithCommit("setADGroupAsDeleted", domain);
                for (Map<String, String> gData : groupsData) {
                    addParentDistinguishedName(gData, false);
                    execNamedCommandWithCommit("addOrModifyADGroup", gData.get("s_a_m_account_name"), domain, gData);
                }
                List<Map<String, String>> ouData = getOrganizationalUnit(domain, cmd, config.organizationalUnitFilter);
//                execNamedCommandWithCommit("setADOUAsDeleted", domain);
                for (Map<String, String> oData : ouData) {
                    addParentDistinguishedName(oData, true);
                    execNamedCommandWithCommit("addOrModifyADOU", oData.get("distinguished_name"), domain, oData);
                }
                //wersja dla MultiMedia ma trochę inną structurę tej funkcji - rózni się dla Prod i dla Test
                execNamedCommandWithCommit("fixSystemUsersAndGroupsFromAD", domain);  
            } catch (Exception e) {
                System.out.println("Error for domain: " + domain + ", message: " + e.getMessage());
                if (logger.isErrorEnabled()) {
                    logger.error("Error with connection with domain controller " + domain + ": ", e);
                }
                con.close();
                return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Error for domain: " + domain + ", message: " + e.getMessage());
            }
        }
        con.close();
        pumpLogger.pumpLog("Recalculating users grants", 95);
        execNamedCommandWithCommit("recalculateGrants");
        return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
    }

    protected void addParentDistinguishedName(Map<String, String> oData, boolean insertNullAsParent) {
        String distinguishedName = oData.get("distinguished_name");
        String parentDistinguishedName = !distinguishedName.contains("OU=") ? (insertNullAsParent ? null : new String(distinguishedName.substring(distinguishedName.indexOf("DC=")))) : BaseUtils.splitString(distinguishedName.replace("\\,", "%%|%%"), ",").v2.replace("%%|%%", "\\,");
        oData.put("parent_distinguished_name", parentDistinguishedName);
    }

    protected void addParentDistinguishedNameForUser(Map<String, String> oData) {
        String distinguishedName = oData.get("distinguished_name");
        String parentDistinguishedName = BaseUtils.splitString(distinguishedName.replace("\\,", "%%|%%"), ",").v2.replace("%%|%%", "\\,");
        oData.put("parent_distinguished_name", parentDistinguishedName);
    }

    protected List<Map<String, String>> getUsers(String defaultNamingContext, _Command cmd, String filter) {
        String userFields = createKeyStringFromMap(userAttributesMap);
        String str = "<LDAP://" + defaultNamingContext + ">;" + filter
                //                + "(&(objectCategory=person)(objectclass=user)(!(objectclass=inetOrgPerson)));"
                + userFields + ";subTree";
        System.out.println(str);
        cmd.commandText(str);
        List<Map<String, String>> adUsers = getAttributeListFromAD(cmd, userAttributesMap, false);
        return adUsers;
    }

    protected List<Map<String, String>> getGroups(String defaultNamingContext, _Command cmd, String filter) {
        String groupFields = createKeyStringFromMap(groupAttributesMap);
        String str = "<LDAP://" + defaultNamingContext + ">;" + filter + groupFields + ";subTree";
        System.out.println(str);
        cmd.commandText(str);
//        cmd.commandText("<LDAP://" + defaultNamingContext + ">;" + filter + groupFields + ";subTree");
//        cmd.commandText("<LDAP://" + defaultNamingContext + ">;(&(objectCategory=group)(objectclass=group));" + groupFields + ";subTree");
        List<Map<String, String>> adGroups = getAttributeListFromAD(cmd, groupAttributesMap, false);
        return adGroups;
    }

    protected List<Map<String, String>> getOrganizationalUnit(String defaultNamingContext, _Command cmd, String filter) {
        String ouFields = createKeyStringFromMap(ouAttributesMap);
        String str = "<LDAP://" + defaultNamingContext + ">;" + filter
                //                + "(|(objectCategory=organizationalUnit)(objectclass=domain));"
                + ouFields + ";subTree";
        System.out.println(str);
        cmd.commandText(str);
//        cmd.commandText("<LDAP://" + defaultNamingContext + ">;(|(objectCategory=organizationalUnit)(objectclass=domain));" + ouFields + ";subTree");
        List<Map<String, String>> adOu = getAttributeListFromAD(cmd, ouAttributesMap, false);
        return adOu;
    }

    protected List<Map<String, String>> getAttributeListFromAD(_Command cmd, Map<String, Boolean> attrMap, boolean onlyFirstValForList) {
        List<Map<String, String>> adCollection = new ArrayList<Map<String, String>>();
        _Recordset rs = cmd.execute(null, Variant.getMissing(), -1/*default*/);
        if (rs.eof()) {
            System.out.println("no results...");
        } else {
            for (int i = 0; i < rs.recordCount(); i++) {
                final Fields fields = rs.fields();
                if (fields == null) {
                    continue;
                }
                Map<String, String> oneRowMap = new LinkedHashMap<String, String>();
                int fieldsCount = fields.count();
                for (int j = 0; j < fieldsCount; j++) {
                    try {
                        final Field field = fields.item(j);
                        final String attribute = field.name();
                        if (!BaseUtils.isStrEmptyOrWhiteSpace(attribute) && attrMap.containsKey(attribute)) {
                            Object value = field.value();
                            String textValue = (value == null) ? "" : value.toString();
                            if (value instanceof Object[] && ((Object[]) value).length > 0) {
                                Object[] vals = ((Object[]) value);
                                if (onlyFirstValForList) {
                                    textValue = vals[0].toString();
                                } else {
                                    StringBuilder sb = new StringBuilder();
                                    int u = 0;
                                    for (Object val : vals) {
                                        sb.append(u++ != 0 ? ATTRIBUTE_LIST_SEPARATOR : "").append(val.toString());
                                    }
                                    textValue = sb.toString();
                                }
                            }
                            oneRowMap.put(BaseUtils.beanFieldNameToTableColName(attribute), textValue);
                        }
                    } catch (Exception ecom) {
                        if (logger.isWarnEnabled()) {
                            logger.warn("Error in getting attribute's value: " + ecom.getMessage());
                        }
                    }
                }
                adCollection.add(oneRowMap);
                rs.moveNext();
            }
        }
        rs.close();
        return adCollection;
    }

    protected String createKeyStringFromMap(Map<String, Boolean> map) {
        StringBuilder fieldBuilder = new StringBuilder();
        int o = 0;
        for (String attribute : map.keySet()) {
            fieldBuilder.append(o++ != 0 ? "," : "").append(attribute);
        }
        return fieldBuilder.toString();
    }
}
