/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

/**
 *
 * @author lb
 */
public class TeradataConnectionConfig implements Cloneable {

    public String server;
    public String database;
    public String user; // null -> use integrated login
    public String tmode; // Allows the selection of the database transaction mode., There are two transaction modes:• American National Standards Institute (ANSI) • Teradata
    public String charset; // UTF8
    public String password;
    public int poolSize = 30;
    public int timeout = 20;
    public String port;

    public TeradataConnectionConfig() {
    }

    public TeradataConnectionConfig(String server, String database, String user, String password, String tmode, String charset, String port) {
        this.server = server;
        this.database = database;
        this.user = user;
        this.password = password;
        this.tmode = tmode;
        this.charset = charset;
        this.port = port;
    }

    @Override
    public TeradataConnectionConfig clone() {
        try {
            return (TeradataConnectionConfig) super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException("what a surprise! not cloneable?", ex);
        }
    }
}
