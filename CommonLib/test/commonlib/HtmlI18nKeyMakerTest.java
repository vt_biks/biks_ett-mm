/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import junit.framework.*;
import simplelib.HtmlUtils;
import static junit.framework.Assert.*;

/**
 *
 * @author wezyr
 */
public class HtmlI18nKeyMakerTest extends TestCase {

    public HtmlI18nKeyMakerTest() {
    }

    static {
        LameUtils.initFactories();
    }

    public static void setUpClass() throws Exception {
    }

    public static void tearDownClass() throws Exception {
    }

    public void testProcessFile() {
        String htmlTxt = "<a tiTle='x\n"
                + "xx' href='d>d<\n"
                + "d'>ula la</a><b\n"
                + ">xyxyyx</b>";
        HtmlI18nKeyMaker km = new HtmlI18nKeyMaker();

        System.out.println(km.processFile("abba", htmlTxt));

        htmlTxt = LameUtils.loadAsStringClassPathFile(HtmlI18nKeyMakerTest.class,
                "oneQuestion_box.html", "utf8");

        System.out.println(km.processFile("abba", htmlTxt));
    }

    public void testStripHtmlComments() {
        String s = "<a><!-- ala ma "
                + "kota -->dupayx</a><!-- dfgdfg  -->\n";
        assertEquals("<a>dupayx</a>\n", HtmlUtils.stripHtmlComments(s));
        assertEquals("<a>|dupayx</a>|\n", HtmlUtils.replaceHtmlComments(s, "|"));
    }

    public void testReplaceButNotInComments() {
        String res = HtmlI18nKeyMaker.replaceButNotInComments(
                "pies <!-- ala ma pieska --> kot i pies",
                "pies", "chomik");

        assertEquals("chomik <!-- ala ma pieska --> kot i chomik", res);
    }

    public void testReplaceButNotInFoxyElems() {
        String res = HtmlI18nKeyMaker.replaceButNotInFoxyElems(
                "pies <!-- ala ma pieska --> kot i pies oraz %${piesek}$%",
                "pies", "chomik");

        assertEquals("chomik <!-- ala ma pieska --> kot i chomik oraz %${piesek}$%", res);
    }
}
