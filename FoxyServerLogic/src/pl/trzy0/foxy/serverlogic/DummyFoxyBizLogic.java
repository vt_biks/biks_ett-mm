/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.serverlogic;

import java.util.Collection;
import simplelib.logging.ILameLogger.LameLogLevel;
import simplelib.logging.LameLoggerMsg;

/**
 *
 * @author wezyr
 */
public class DummyFoxyBizLogic implements IFoxyBizLogic {

    public void logRequest(String clientIP, int clientPort, String method, String requestURI, String requestHeaders, String requestParams, String sessionId, int durationMillis, String errorMsg, String referer) {
        // no-op
    }

    public void logMsgs(Collection<LameLoggerMsg> msgs, LameLogLevel minLevel) {
        // no-op
    }

    public void finishWorkUnit(boolean success) {
        // no-op
    }

    public void destroy() {
        // no-op
    }

}
