/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.user.client.ui.Label;
import java.util.Collections;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.BikTreeWidget;
import pl.fovis.client.bikpages.PageDataFetchBroker;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pku
 */
public class ChangeCategoryDialog extends BaseActionOrCancelDialog {

    protected Integer rootNodeId;
    protected TreeNodeBean optSelectedNode;
    protected BikTreeWidget bbTree;
    public int itemsToMoveCount;
    public String currentCategoryName;
    public IParametrizedContinuation<Integer> saveCont;

    public ChangeCategoryDialog(Integer rootNodeId) {
        this.rootNodeId = rootNodeId;
    }

    public ChangeCategoryDialog(Integer rootNodeId, TreeNodeBean optSelectedNode) {
        this(rootNodeId);
        this.optSelectedNode = optSelectedNode;
    }

    @Override
    protected String getDialogCaption() {
        return I18n.zmienKategorieWybranychWezlow.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zmienKategorie.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        main.add(new Label(I18n.pozycjiDoPrzeniesienia.get() /* I18N:  */ + ": " + itemsToMoveCount));
        main.add(new Label(I18n.zKategorii.get() /* I18N:  */ + " " + currentCategoryName + " " + I18n.doKategorii.get() /* I18N:  */ + ":"));
        bbTree = new BikTreeWidget(false,
                BIKClientSingletons.ORIGINAL_EDP_BODY, "wwNewTreePopup wwNewTree",
                new PageDataFetchBroker() {
                    {
                        isFullDataFetched = true;
                    }
                }, true,
                "name=" /* i18n: no */ + I18n.nazwa.get() /* I18N:  */);
        BIKClientSingletons.getService().getBikEntityDetailsData(rootNodeId, rootNodeId, false, new StandardAsyncCallback<EntityDetailsDataBean>() {
            @Override
            public void onSuccess(EntityDetailsDataBean result) {
                bbTree.initWithFullData(Collections.singletonList(result.node), true, null, null);
            }
        });
        bbTree.addSelectionChangedHandler(new IParametrizedContinuation<Map<String, Object>>() {

            @Override
            public void doIt(Map<String, Object> param) {
                Integer currentNodeId = bbTree.getCurrentNodeId();
                if (optSelectedNode != null) { // przenosimy kategorię
                    actionBtn.setEnabled(!BaseUtils.safeEquals(currentNodeId, optSelectedNode.id) && !BaseUtils.safeEquals(currentNodeId, optSelectedNode.parentNodeId));
                } else { // przenosimy elementy
                    actionBtn.setEnabled(!BaseUtils.safeEquals(currentNodeId, rootNodeId));
                }
            }
        });
        main.add(bbTree.getTreeGridWidget());
    }

    // TF: niepotrzebne - ograne disablowaniem przycisku
//    @Override
//    protected boolean doActionBeforeClose() {
//        TreeNodeBean selectedBean = bbTree.getCurrentNodeBean();
//        return selectedBean != null && selectedBean.id != parentNodeId;
//    }
    @Override
    protected void doAction() {
        saveCont.doIt(bbTree.getCurrentNodeBean().id);
    }
}
