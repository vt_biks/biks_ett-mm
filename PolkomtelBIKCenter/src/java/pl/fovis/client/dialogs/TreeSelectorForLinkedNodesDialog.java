/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author mgraczkowski
 */
public class TreeSelectorForLinkedNodesDialog extends TreeSelectorDialog/*TreeSelectorDialogBase*/ {

//    protected TreeNodeBean treeNode;
    protected Integer treeId;
    protected Integer nodeKindId;
   protected List<Integer> parentNodeIdsDirectOverride = new ArrayList<Integer>();

    @Override
    protected String getDialogCaption() {
        return I18n.dodajDowiazanie.get() /* I18N:  */;
    }

    protected String getHeadLblPrefixMsg() {
        return isInMultiSelectMode() ? I18n.dodajeszDowiazanieDla.get() : I18n.dodajeszDowiazanieDla.get();
    }

    @Override
    protected String getMessageForUnselectableNode(TreeNodeBean tnb) {
        return null;
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        if (useLazyLoading) {
            getService().getBikAllRootTreeNodesExcludingOneTree(treeId, getNodeKindId(), getRelationType(), getAttributeName(),parentNodeIdsDirectOverride, callback);
        } else {
            getService().getBikAllTreeNodesExcludingOneTree(treeId, callback);
        }
    }

    public void buildAndShowDialog(Integer treeId, Integer nodeId,
            IParametrizedContinuation<TreeNodeBean> saveCont,
            IJoinedObjSelectionManager<Integer> alreadyJoinedManager) {
        this.treeId = treeId;
        super.buildAndShowDialog(nodeId == null ? 0 : nodeId, saveCont,
                alreadyJoinedManager);
    }

    public void buildAndShowDialog(Integer treeId, Integer nodeId, Integer nodeKindId,
            IParametrizedContinuation<TreeNodeBean> saveCont,
            IJoinedObjSelectionManager<Integer> alreadyJoinedManager) {
        this.treeId = treeId;
        this.nodeKindId = nodeKindId;
        super.buildAndShowDialog(nodeId == null ? 0 : nodeId, saveCont,
                alreadyJoinedManager);
    }

    @Override
    protected boolean isInheritToDescendantsCheckBoxVisible() {
        return false;
    }

    @Override
    protected Collection<Integer> getTreeIdsForFiltering() {
        return BIKClientSingletons.getProperTreeIdsWithoutOneTree(-1);
        //return BIKClientSingletons.getProperTreeIdsWithoutOneTree(treeNode.treeId);
        //return " tree_id != " + treeNode.treeId;
    }

    @Override
    protected Integer getNodeKindId() {
        return nodeKindId;
//        return isNodeFromDynamicTree ? nodeKindId : null;
    }

    protected Integer getTreeId() {
        return treeId;
    }

//     BIKConstants.LINKING
    protected String getRelationType() {
        return BIKConstants.LINKING;
    }

    protected String getAttributeName() {
        return null;
    }

    @Override
    protected boolean doActionBeforeClose() {
        if (isInMultiSelectMode()) {
            return super.doActionBeforeClose();
        }

        if (getNodeKindId() != null && isNodeSelectable(selectedNode)) {
            return selectedNode.isFixed;
        }

        return super.doActionBeforeClose();
    }

    @Override
    protected boolean isActionButtonEnabled() {
        if (isInMultiSelectMode()) {
            return super.isActionButtonEnabled();
        }
        if (getNodeKindId() != null && isNodeSelectable(selectedNode)) {
            return selectedNode.isFixed;
        }
        return super.isActionButtonEnabled();
    }

    protected Pair<String, String> getInnerJoinRelationType(Integer nodeKindId) {
        return new Pair<String, String>(nodeKindId != -1 ? BIKConstants.CHILD : null, BIKConstants.LINKING);
    }

    @Override
    protected void addAdditionalButtons(HorizontalPanel hp) {
        if (BIKClientSingletons.isShowAddNodeButtonOnHyperlinkDialog()) {
            hp.add(addNodeBtn());
        }
    }
}
