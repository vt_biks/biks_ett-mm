/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public class NodeNamePathsAsynchFetcher {

    protected final Map<Integer, String> nodeNamesById;
    protected Map<String, IParametrizedContinuation<Pair<String, String>>> pendingSetters = new HashMap<String, IParametrizedContinuation<Pair<String, String>>>();
    protected Set<Integer> pendingIdsForNameFetch = new HashSet<Integer>();
    protected final Set<Integer> fetchInProgressIds = new HashSet<Integer>();

    public NodeNamePathsAsynchFetcher(Map<Integer, String> optInitialNodeNamesById) {
        this.nodeNamesById = optInitialNodeNamesById == null ? new HashMap<Integer, String>() : optInitialNodeNamesById;
    }

    public void addIdWithName(int id, String name) {
        nodeNamesById.put(id, name);
    }

    protected String tryBuildNamePath(String branchIds, boolean firstAttempt) {
        return BIKCenterUtils.buildAncestorPathByNodeNames(branchIds, nodeNamesById, firstAttempt ? pendingIdsForNameFetch : null);
    }

    protected void setOrCollectInner(String branchIds, IParametrizedContinuation<Pair<String, String>> setterCont, boolean firstAttempt) {
        String optNamePath = tryBuildNamePath(branchIds, firstAttempt);

        if (optNamePath == null) {
            if (firstAttempt) {
                pendingSetters.put(branchIds, setterCont);
            }
        } else {
            setterCont.doIt(new Pair<String, String>(branchIds, optNamePath));
        }
    }

    public void setOrCollect(String branchIds, IParametrizedContinuation<Pair<String, String>> setterCont) {
        setOrCollectInner(branchIds, setterCont, true);
    }

    protected void processFetchedNames(Set<Integer> requestedIds, Map<Integer, String> fetchedNames) {

        fetchInProgressIds.removeAll(requestedIds);

        nodeNamesById.putAll(fetchedNames);

        requestedIds.removeAll(fetchedNames.keySet());

        for (Integer id : requestedIds) {
            nodeNamesById.put(id, "[...]");
        }

        Map<String, IParametrizedContinuation<Pair<String, String>>> prevPendingSetters = pendingSetters;
        pendingSetters = new HashMap<String, IParametrizedContinuation<Pair<String, String>>>();

        for (Entry<String, IParametrizedContinuation<Pair<String, String>>> e : prevPendingSetters.entrySet()) {
            setOrCollect(e.getKey(), e.getValue());
        }
    }

    public void fetchAndSetCollected() {

        final Set<Integer> idsToFetch = pendingIdsForNameFetch;
        pendingIdsForNameFetch = new HashSet<Integer>();

        idsToFetch.removeAll(fetchInProgressIds);

        if (idsToFetch.isEmpty()) {
            return;
        }

        fetchInProgressIds.addAll(idsToFetch);

        BIKClientSingletons.getService().getNodeNamesByIds(idsToFetch, new StandardAsyncCallback<Map<Integer, String>>() {
            @Override
            public void onSuccess(Map<Integer, String> result) {
                processFetchedNames(idsToFetch, result);
            }

//            @Override
//            public void onFailure(Throwable caught) {
//                super.onFailure(caught);
//            }
        });

    }
}
