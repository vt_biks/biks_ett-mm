/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
@RemoteServiceRelativePath("boxiservice")
public interface VersionInfoService extends RemoteService {

    public Pair<String, String> getAppVersion();
}
