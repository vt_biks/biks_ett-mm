/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import java.util.Map;

/**
 *
 * @author pmielanczuk
 */
public interface ISlicer {

    public void init();

    public void addOrUpdateObject(int objId, Map<String, Object> attrVals);

    public void close();
}
