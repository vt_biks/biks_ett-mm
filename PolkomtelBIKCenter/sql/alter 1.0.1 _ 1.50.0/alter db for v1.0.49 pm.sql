﻿exec sp_check_version '1.0.49';
  
--  ALTER TABLE bik_authors ALTER COLUMN is_admin int set default 0;

update bik_authors set is_admin = 0 where is_admin is null;
  
ALTER TABLE bik_authors alter column is_admin int NOT NULL;
go
  
exec sp_update_version '1.0.49', '1.0.50';
go
