/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.common;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author tflorczak
 */
public class SapBoUniverseExtraDataBean implements Serializable {

    public String type;
    public String textOfSelect;
    public String textOfWhere;
    public String agregate;
    public List<String> tables;
    
}
