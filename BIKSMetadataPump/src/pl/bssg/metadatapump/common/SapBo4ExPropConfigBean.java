/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import java.io.Serializable;

/**
 *
 * @author ctran
 */
public class SapBo4ExPropConfigBean implements Serializable {

    public String path;
    public String reportPath;
    public String idtPath;
    public String clientToolsPath;
    public String jREx86Path;
}
