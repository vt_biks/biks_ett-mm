/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.common;

import simplelib.LameRuntimeException;

/**
 *
 * @author pmielanczuk
 */
public class BIKSAdmRuntimeException extends LameRuntimeException {

    public BIKSAdmRuntimeException() {
        //ww: do celów serializacji
    }

    public BIKSAdmRuntimeException(String msg) {
        super(msg);
    }
}
