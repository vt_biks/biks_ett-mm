﻿exec sp_check_version '1.1.5.3';
go

 declare @node_kind_id int;
    select @node_kind_id = id
    from bik_node_kind
    where code = 'ReportQuery';

select bn1.name,max(bn1.id)as good,min(bn1.id) as bad 
into #tmp
from bik_node bn1 join bik_node bn2 on
bn1.name=bn2.name and bn1.node_kind_id=@node_kind_id 
and bn2.node_kind_id=@node_kind_id 
and bn1.id<>bn2.id
and bn1.parent_node_id=bn2.parent_node_id 
and bn1.is_deleted=0 and bn2.is_deleted=0
group by bn1.name,bn1.parent_node_id

select t.bad,t.good, bjo.dst_node_id
into #tmp2
from #tmp t join bik_joined_objs bjo
on t.bad= bjo.src_node_id and type=0

--------
declare @good int;
declare @bad int;
declare @dst_node_id int;
declare contact_cursor cursor for
select bad, good, dst_node_id from #tmp2

open contact_cursor;

fetch next from contact_cursor
into @bad, @good, @dst_node_id

while @@fetch_status = 0
begin

    update bik_joined_objs
	set dst_node_id = @good 
    where dst_node_id=@bad and src_node_id = @dst_node_id and @dst_node_id not in(
    select src_node_id from bik_joined_objs where dst_node_id = @good);
    
    
	update bik_joined_objs
	set src_node_id = @good 
    where src_node_id=@bad and dst_node_id = @dst_node_id and @dst_node_id not in(
    select dst_node_id from bik_joined_objs where src_node_id = @good);
    

   fetch next from contact_cursor
   into @bad, @good, @dst_node_id;
end

close contact_cursor;
deallocate contact_cursor;
go
-----------------------------------------------------------------------------
update bik_note 
set node_id = x.good
from (select good,bad from #tmp)x  where bik_note.node_id=x.bad

update bik_attribute_linked 
set node_id = x.good
from (select good,bad from #tmp)x  where bik_attribute_linked.node_id=x.bad

update bik_user_in_node 
set node_id = x.good
from (select good,bad from #tmp)x  where bik_user_in_node.node_id=x.bad and bik_user_in_node.role_for_node_id not in (
select role_for_node_id from bik_user_in_node where node_id=x.good)

update bik_node set is_deleted=1 ,
obj_id='@#$!'+obj_id
where id in (select bad from #tmp)

drop table #tmp2
drop table #tmp
----------------------------------------------------------------------------
declare @treeId int
select @treeId=id from bik_tree where code='Reports'

declare @filterTxt varchar(max) = 'n.tree_id = ' + cast(@treeId as varchar(20))

exec sp_verticalize_node_attrs @filterTxt
go


exec sp_update_version '1.1.5.3', '1.1.5.3.1';
go

