/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.BeanWithIntIdAndNameBase;
import simplelib.IParametrizedContinuation;
import simplelib.LameRuntimeException;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class SystemConfigurationDialog extends SystemConfigurationDialogBase {

    protected static final int SHOW_ATTR_DICT = 3;
    protected static final int SHOW_ATTR_DEF = 2;
//    protected static final int SHOW_ATTR = 2;
    protected static final int SHOW_NODE_KIND = 1;
    protected static final int SHOW_TREE_KIND = 0;
    protected Integer nOverViewDialog = 3;
    protected Map<Integer, Integer> attrDictConfMap, attrDefConfMap, attrConfMap, nodeKindConfMap, treeKindConfMap;
    protected Set<Integer> attrDictIds2Delete, attrDefIds2Delete, attrIds2Delete, nodeKindIds2Delete, treeKindIds2Delete;

    static final Map<Integer, String> dialogCaptionMap = new HashMap<Integer, String>();

    static {
        dialogCaptionMap.put(SHOW_ATTR_DICT, I18n.podgladKategoriiAtrybutow.get());
        dialogCaptionMap.put(SHOW_ATTR_DEF, I18n.podgladDefinicjiAtrybutow.get());
//        dialogCaptionMap.put(SHOW_ATTR, I18n.podgladAtrybutow.get());
        dialogCaptionMap.put(SHOW_NODE_KIND, I18n.podgladNodeKind.get());
        dialogCaptionMap.put(SHOW_TREE_KIND, I18n.podgladTreeKind.get());
    }

    protected StandardAsyncCallback<Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>>> callback = new StandardAsyncCallback<Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>>>() {

        @Override
        public void onSuccess(Pair<List<BeanWithIntIdAndNameBase>, List<BeanWithIntIdAndNameBase>> result) {
            new SystemConfigurationImplementOverviewDialog().buildAndShowDialog(result.v1, result.v2, dialogCaptionMap.get(nOverViewDialog), nOverViewDialog > 0 ? "OK" : I18n.implementuj.get(), new IParametrizedContinuation<Pair<Map<Integer, Integer>, Set<Integer>>>() {

                @Override
                public void doIt(Pair<Map<Integer, Integer>, Set<Integer>> p) {
                    if (nOverViewDialog == SHOW_ATTR_DICT) {
                        attrDictConfMap = p.v1;
                        attrDictIds2Delete = p.v2;
                    } else if (nOverViewDialog == SHOW_ATTR_DEF) {
                        attrDefConfMap = p.v1;
                        attrDefIds2Delete = p.v2;
//                    } else if (nOverViewDialog == SHOW_ATTR) {
//                        attrConfMap = p.v1;
//                        attrIds2Delete = p.v2;
                    } else if (nOverViewDialog == SHOW_NODE_KIND) {
                        nodeKindConfMap = p.v1;
                        nodeKindIds2Delete = p.v2;
                    } else if (nOverViewDialog == SHOW_TREE_KIND) {
                        treeKindConfMap = p.v1;
                        treeKindIds2Delete = p.v2;
                    }
                    nOverViewDialog--;
                    showOverViewDialog();
                }
            });
        }
    };

    @Override
    public void doBuildConfigTree(boolean deleteOldTree) {
        final BIKSProgressInfoDialog dialog = new BIKSProgressInfoDialog();
        dialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaPostepowanie.get(), true);
        BIKClientSingletons.getService().buildConfigTree(deleteOldTree, new StandardAsyncCallback<Void>() {

            @Override
            public void onSuccess(Void result) {
                dialog.hideDialog();
                BIKClientSingletons.showInfo(I18n.proszeOdswiezycPrzegladarke.get());
            }

            @Override
            public void onFailure(Throwable caught) {
                dialog.hideDialog();
                LameRuntimeException e = (LameRuntimeException) caught;
                Window.alert(e.getMessage());
//                        super.onFailure(caught); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }

//    private Widget createChangeConfigTreeBtn() {
//        changeConfigTreeBtn = new PushButton(I18n.ustaw.get());
//        NewLookUtils.makeCustomPushButton(changeConfigTreeBtn);
//        changeConfigTreeBtn.addClickHandler(new ClickHandler() {
//
//            @Override
//            public void onClick(ClickEvent event) {
//
//            }
//        });
//        return changeConfigTreeBtn;
//    }
    private void showOverViewDialog() {
        if (nOverViewDialog == SHOW_ATTR_DICT) {
            BIKClientSingletons.getService().getAttributeDictOverView(callback);
        } else if (nOverViewDialog == SHOW_ATTR_DEF) {
            BIKClientSingletons.getService().getAttributeDefOverView(callback);
//        } else if (nOverViewDialog == SHOW_ATTR) {
//            BIKClientSingletons.getService().getAttribute4NodeKindOverView(callback);
        } else if (nOverViewDialog == SHOW_NODE_KIND) {
            BIKClientSingletons.getService().getNodeKindsOverView(callback);
        } else if (nOverViewDialog == SHOW_TREE_KIND) {
            BIKClientSingletons.getService().getTreeKindsOverView(callback);
        } else if (nOverViewDialog == -1) {
            final BIKSProgressInfoDialog dialog = new BIKSProgressInfoDialog();
            dialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaPostepowanie.get(), true);
            BIKClientSingletons.getService().implementConfigTree(attrDictConfMap, attrDictIds2Delete, attrDefConfMap, attrDefIds2Delete, /*attrConfMap, attrIds2Delete, */ nodeKindConfMap, nodeKindIds2Delete, treeKindConfMap, treeKindIds2Delete, new StandardAsyncCallback<Void>() {

                        @Override
                        public void onSuccess(Void result) {
                            dialog.hideDialog();
                            SystemConfigurationDialog.this.hideDialog();
                            BIKClientSingletons.showInfo(I18n.proszeOdswiezycPrzegladarke.get());
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                            dialog.hideDialog();
                            SystemConfigurationDialog.this.hideDialog();
                            Window.alert(caught.getMessage());
                            BIKClientSingletons.showWarning(caught.getMessage());
//                        super.onFailure(caught); //To change body of generated methods, choose Tools | Templates.
                        }
                    });
        }
    }

    @Override
    public void doImplement() {
//        showOverViewDialog();
        final BIKSProgressInfoDialog dialog = new BIKSProgressInfoDialog();
        dialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaPostepowanie.get(), true);
        BIKClientSingletons.getService().implementConfigTree(new StandardAsyncCallback<Void>() {

            @Override
            public void onSuccess(Void result) {
                dialog.hideDialog();
                BIKClientSingletons.showInfo(I18n.proszeOdswiezycPrzegladarke.get());
            }

            @Override
            public void onFailure(Throwable caught) {
                dialog.hideDialog();
                SystemConfigurationDialog.this.hideDialog();
                Window.alert(caught.getMessage());
                BIKClientSingletons.showWarning(caught.getMessage());
//                        super.onFailure(caught); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }

    @Override
    public String getConfigurationAppPropCode() {
        return BIKConstants.APP_PROP_SYS_CONF;
    }

    private class SystemConfigurationImplementOverviewDialog extends BaseActionOrCancelDialog {

        protected Grid grid;
        protected IParametrizedContinuation cont;
        protected String actionBtnCaption;
        protected String dialogCaption;
        protected List<BeanWithIntIdAndNameBase> confList;
        protected List<BeanWithIntIdAndNameBase> sysList;
        protected Map<ListBox, Integer> actionLb2Map = new HashMap<ListBox, Integer>();
        protected Map<Integer, Integer> confMap = new HashMap<Integer, Integer>();
        protected Set<Integer> ids2Delete = new HashSet<Integer>();
        protected VerticalPanel deleteObjsVp = new VerticalPanel();
        protected ScrollPanel scUp, scDown;

        @Override
        protected String getDialogCaption() {
            return dialogCaption;
        }

        @Override
        protected String getActionButtonCaption() {
            return actionBtnCaption;
        }

        @Override
        protected void buildMainWidgets() {
            grid = new Grid(confList.size(), 3);
            scUp = new ScrollPanel(grid);
            main.add(new Label(I18n.dodaj.get() + "/" + I18n.aktualizuj.get() + ":"));
            main.add(scUp);
            if (confList.size() > 20) {
                scUp.setHeight("250px");
            }
            int row = 0;
            for (BeanWithIntIdAndNameBase conf : confList) {
                final ListBox actionLb = new ListBox();
//                actionLb.setEnabled(false);
                actionLb.addItem(I18n.aktualizuj.get());
                actionLb.addItem(I18n.dodaj.get());
                actionLb2Map.put(actionLb, row);
                grid.setWidget(row, 0, actionLb);

                ListBox confLb = createListBox(confList, conf.name);
                confLb.setEnabled(false);
                grid.setWidget(row, 1, confLb);

                final ListBox sysLb = createListBox(sysList, conf.name);
                grid.setWidget(row++, 2, sysLb);

                if (ClientDiagMsgs.isShowDiagEnabled(ClientDiagMsgs.APP_MSG_KIND)) {
                    ClientDiagMsgs.showDiagMsg(ClientDiagMsgs.APP_MSG_KIND, (row - 1) + ":" + conf.name + ":" + confLb.getSelectedItemText() + ":" + sysLb.getSelectedItemText());
                }
                if (isSameCode(confLb.getSelectedItemText(), sysLb.getSelectedItemText())) {
                    actionLb.setSelectedIndex(0);
                } else {
                    actionLb.setSelectedIndex(1);
                }
                if (I18n.dodaj.get().equals(actionLb.getSelectedItemText())) {
                    sysLb.setVisible(false);
                }

                actionLb.addChangeHandler(new ChangeHandler() {

                    @Override
                    public void onChange(ChangeEvent event) {
                        sysLb.setVisible(I18n.aktualizuj.get().equals(actionLb.getSelectedItemText()));
                        updateIdMapAndRefreshInterface();
                    }
                });
            }
            main.add(new Label(I18n.usun.get() + ":"));
            scDown = new ScrollPanel(deleteObjsVp);
            main.add(scDown);
            updateIdMapAndRefreshInterface();
        }

        private void updateIdMapAndRefreshInterface() {
            confMap.clear();
            ids2Delete.clear();

            for (int i = 0; i < grid.getRowCount(); i++) {
                ListBox actionLb = (ListBox) grid.getWidget(i, 0);
                ListBox confLb = (ListBox) grid.getWidget(i, 1);
                ListBox sysLb = (ListBox) grid.getWidget(i, 2);
                boolean isAdding = I18n.dodaj.get().equals(actionLb.getSelectedItemText());
                confMap.put(BaseUtils.tryParseInteger(confLb.getSelectedValue()), isAdding ? null : BaseUtils.tryParseInteger(sysLb.getSelectedValue()));
            }

            deleteObjsVp.clear();
            for (BeanWithIntIdAndNameBase sysConf : sysList) {
                boolean isDeleted = true;
                for (int i = 0; i < grid.getRowCount(); i++) {
                    ListBox actionLb = (ListBox) grid.getWidget(i, 0);
                    ListBox confLb = (ListBox) grid.getWidget(i, 1);
                    ListBox sysLb = (ListBox) grid.getWidget(i, 2);
                    boolean isUpdating = I18n.aktualizuj.get().equals(actionLb.getSelectedItemText());
                    if (isUpdating && sysLb.getSelectedItemText().equals(sysConf.name)) {
                        isDeleted = false;
                    }
                }

                if (isDeleted) {
                    ids2Delete.add(sysConf.id);
                    deleteObjsVp.add(new Label(sysConf.name));
                }
            }
            if (ids2Delete.size() > 10) {
                scDown.setHeight("200px");
            }
        }

        private boolean isSameCode(String codeAndName1, String codeAndName2) {
            return (!BaseUtils.isStrEmptyOrWhiteSpace(codeAndName1) && !BaseUtils.isStrEmptyOrWhiteSpace(codeAndName2) && (codeAndName1.equalsIgnoreCase(codeAndName2) /*|| (BaseUtils.splitBySep(codeAndName1, ":").get(0).equals(BaseUtils.splitBySep(codeAndName2, ":").get(0))) && nOverViewDialog != SHOW_ATTR*/));
        }

        @Override
        protected void doAction() {

            cont.doIt(new Pair<Map<Integer, Integer>, Set<Integer>>(confMap, ids2Delete));
        }

        public void buildAndShowDialog(List<BeanWithIntIdAndNameBase> confList, List<BeanWithIntIdAndNameBase> sysList, String dialogCaption, String actionBtnCaption, IParametrizedContinuation cont) {
            this.confList = confList;
            this.sysList = sysList;
            this.dialogCaption = dialogCaption;
            this.actionBtnCaption = actionBtnCaption;
            this.cont = cont;
            super.buildAndShowDialog(); //To change body of generated methods, choose Tools | Templates.
        }

        private ListBox createListBox(List<BeanWithIntIdAndNameBase> confList, String selectedText) {
            ListBox lb = new ListBox();
            int i = 0;
            for (BeanWithIntIdAndNameBase bean : confList) {
                lb.addItem(bean.name, BaseUtils.safeToString(bean.id));
                if (isSameCode(bean.name, selectedText)) {
                    lb.setSelectedIndex(i);
                }
                i++;
            }
            lb.addChangeHandler(new ChangeHandler() {

                @Override
                public void onChange(ChangeEvent event) {
                    updateIdMapAndRefreshInterface();
                }
            });
            return lb;
        }
    }
}
