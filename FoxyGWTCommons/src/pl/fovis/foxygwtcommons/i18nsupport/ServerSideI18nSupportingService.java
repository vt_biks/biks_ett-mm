/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.i18nsupport;

import com.google.gwt.user.client.rpc.RemoteService;
import java.util.Set;

/**
 *
 * @author pmielanczuk
 */
//@RemoteServiceRelativePath("fsb")
public interface ServerSideI18nSupportingService extends RemoteService {

    public LocaleNegotiationResultBean setSessionLocaleName(String localeName, Set<String> clientSupportedLocaleNames);

    public Set<String> getServerSupportedLocaleNames();
}
