exec sp_check_version '1.0.36';
go

insert into bik_node_kind(code, caption) values('UsersGroup',  'Grupa użytkowników')
go

insert into bik_node_kind(code, caption) values('User', 'Użytkownik')
go

alter table bik_tree drop constraint CK_bik_tree_tree_kind
go

alter table bik_tree add constraint CK_bik_tree_tree_id check(tree_kind in (0, 1, 2, 3, 4))
go

insert into bik_tree(name, tree_kind, node_kind_id, code)
select 'Użytkownicy', 4, (select id from bik_node_kind where code = 'UsersGroup'), 'Users'
go

update bik_user_kind
set name = 'Ekspert'
where name = 'Expert'
go

declare @nodeKindId int
declare @treeId int
select @nodeKindId = id from bik_node_kind where code = 'UsersGroup'
select @treeId = id from bik_tree where code = 'Users'

insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id, branch_ids, obj_id, descr, is_deleted, indexVer)
values(NULL, @nodeKindId, 'Administratorzy', @treeId, NULL, NULL, NULL, NULL, 0, 0)

insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id, branch_ids, obj_id, descr, is_deleted, indexVer)
values(NULL, @nodeKindId, 'Eksperci', @treeId, NULL, NULL, NULL, NULL, 0, 0)

insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id, branch_ids, obj_id, descr, is_deleted, indexVer)
values(NULL, @nodeKindId, 'Sponsorzy', @treeId, NULL, NULL, NULL, NULL, 0, 0)

insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id, branch_ids, obj_id, descr, is_deleted, indexVer)
values(NULL, @nodeKindId, 'Użytkownicy', @treeId, NULL, NULL, NULL, NULL, 0, 0)
go

update bik_node set branch_ids=convert(varchar, id) + '|' where tree_id = (select id from bik_tree where code = 'Users')
go

alter table bik_user
add node_id int references bik_node(id) null default null
go

declare @parentNodeId int
declare @nodeKindId int
declare @treeId int
select @nodeKindId = id from bik_node_kind where code = 'User'
select @treeId = id from bik_tree where code = 'Users'

select @parentNodeId = id from bik_node where tree_id = @treeId and name = 'Administratorzy' 

insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id, branch_ids, obj_id, descr, is_deleted, indexVer)
select @parentNodeId, @nodeKindId, name, @treeId, NULL, NULL, NULL, NULL, 0, 0 from bik_user
where user_kind_id = (select id from bik_user_kind where name = 'Administrator')

select @parentNodeId = id from bik_node where tree_id = @treeId and name = 'Eksperci' 

insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id, branch_ids, obj_id, descr, is_deleted, indexVer)
select @parentNodeId, @nodeKindId, name, @treeId, NULL, NULL, NULL, NULL, 0, 0 from bik_user
where user_kind_id = (select id from bik_user_kind where name = 'Ekspert')

select @parentNodeId = id from bik_node where tree_id = @treeId and name = 'Sponsorzy' 

insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id, branch_ids, obj_id, descr, is_deleted, indexVer)
select @parentNodeId, @nodeKindId, name, @treeId, NULL, NULL, NULL, NULL, 0, 0 from bik_user
where user_kind_id = (select id from bik_user_kind where name = 'Sponsor')

select @parentNodeId = id from bik_node where tree_id = @treeId and name = 'Użytkownicy' 

insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id, branch_ids, obj_id, descr, is_deleted, indexVer)
select @parentNodeId, @nodeKindId, name, @treeId, NULL, NULL, NULL, NULL, 0, 0 from bik_user
where user_kind_id = (select id from bik_user_kind where name = 'Użytkownik')
go

declare @treeId int
select @treeId = id from bik_tree where code = 'Users'

update bik_user
set node_id = (select id from bik_node where tree_id = @treeId and name = bik_user.name)
go

update bik_node 
set branch_ids = (select branch_ids from bik_node parent where parent.id = bik_node.parent_node_id) + convert(varchar, id) + '|'
where tree_id = (select id from bik_tree where code = 'Users') and branch_ids is null and parent_node_id is not null
go

exec sp_update_version '1.0.36', '1.0.37';
go
