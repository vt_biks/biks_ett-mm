/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import pl.bssg.metadatapump.common.ConfluenceObjectBean;

/**
 *
 * @author tflorczak
 */
public class ConfluenceBean4TreeExtractor implements IBean4TreeExtractor<ConfluenceObjectBean, String> {

    @Override
    public String extractId(ConfluenceObjectBean b) {
        return b.id;
    }

    @Override
    public String extractParentId(ConfluenceObjectBean b) {
        return b.parentId;
    }

    @Override
    public boolean extractHasNoChildren(ConfluenceObjectBean b) {
        return b.hasNoChildren;
    }

    @Override
    public String extractName(ConfluenceObjectBean b) {
        return b.name;
    }

    @Override
    public int extractVisualOrder(ConfluenceObjectBean b) {
        return 0;
    }

    @Override
    public boolean extractIsFolder(ConfluenceObjectBean b) {
        return true; // ??
    }

    @Override
    public String extractBranchIds(ConfluenceObjectBean b) {
        return b.branchIds;
    }
}
