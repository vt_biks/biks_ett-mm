/*
 * JavaScriptExprBuilder.java
 *
 * Created on 30 październik 2007, 12:45
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package simplelib;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author wezyr
 */
public class JavaScriptExprBuilder {

    public static String escapeJavaScriptAndQuote(String str) {
        return StringEscapeUtils.escapeJavaScriptAndQuote(str);
    }

    public static String mapConstant(Map<String, Object> m) {
        if (m == null) {
            return "null";
        }

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        boolean firstItem = true;

        for (Entry<String, Object> e : m.entrySet()) {
            if (!firstItem) {
                sb.append(",");
            } else {
                firstItem = false;
            }

            sb.append(escapeJavaScriptAndQuote(e.getKey())).append(":")
                    .append(escapeJavaScriptAndQuote(BaseUtils.safeToString(e.getValue())));
        }

        return sb.append("}").toString();
    }

    public static String arrayConstant(List<String> arr) {
        return "[" + BaseUtils.mergeWithSepEx(arr, ",", new BaseUtils.Projector<String, String>() {
            public String project(String val) {
                return escapeJavaScriptAndQuote(val);
            }
        }) + "]";
    }

    public static String arrayConstant(Collection<Map<String, Object>> arr) {
        return "[" + BaseUtils.mergeWithSepEx(arr, ",", new BaseUtils.Projector<Map<String, Object>, String>() {
            public String project(Map<String, Object> val) {
                return mapConstant(val);
            }
        }) + "]";
    }
}
