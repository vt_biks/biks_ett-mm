/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author młydkowski
 */
public class BlogBean extends NameDescBean implements Serializable/*, IBeanWithDstNodeId*/,
        IHasSimilarityScore {

    public int id;
    public Date dateAdded;
    public int userId;
    public String userName;
    public int nodeId;
    public String nodeTreeCode;
    public String avatar;
    public String loginName;
    public Set<Integer> deleteNodeId;
    public String blogSubject;
    public String nodeKind;
    public String dstCode;
    public boolean dstHasChildren;
    public int nodeIdEntry;
    public int srcNodeId;
    public String branchIds;
    public boolean inheritToDescendants;
    public boolean isAuxiliary;
    public String roleBranchIds;
    public String descrPlain;
    public Double similarityScore;
    public String attributes;
    public int joinedObjsId;
    /*
     public Integer getDstNodeId() {
     return nodeId;
     }
     */

    @Override
    public Double getSimilarityScore() {
        return similarityScore;
    }
}
