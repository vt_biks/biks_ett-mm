USE [boxi]
GO

/****** Object:  Table [dbo].[aaa_blog_entry]    Script Date: 03/11/2011 13:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[aaa_blog_entry](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[content] [varchar](max) NOT NULL,
	[subject] [nvarchar](150) NOT NULL,
	[user_id] [bigint] NOT NULL,
	[date_added] [datetime] NOT NULL,
 CONSTRAINT [PK_aaa_blog_entry] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


