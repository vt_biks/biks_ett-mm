/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbw;

import commonlib.LameUtils;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.common.PumpConstants;
import simplelib.BaseUtils;
import simplelib.CSVReader;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class SapBWPump extends MetadataPumpBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected static final Map<String, Boolean> BW_TYPE_NAME_MAP = new HashMap<String, Boolean>() {

        {
            put("QUER", false);
            put("MPRO", false);
            put("CUBE", false);
            put("ODSO", false);
            put("ISET", false);
            put("AREA", false);
        }
    };
//    protected String path;

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_SAPBW;
    }

//    public String getPumpGroup() {
//        return PumpConstants.PUMP_GROUP_SAPBW;
//    }
    protected List<SapBWBean> createListOfBeansFromListOfMap(List<Map<String, String>> content) {
        List<SapBWBean> listOfBeans = new LinkedList<SapBWBean>();
        for (Map<String, String> row : content) {
            listOfBeans.add(new SapBWBean(row.get("INFOCUBE"), BaseUtils.tryParseInteger(row.get("IPLEVEL")), row.get("PARTCUBE"), row.get("IOBJNM"), row.get("FILETYPE"), row.get("TCTBWOTYPE"), row.get("INFOPROVUP"), row.get("INFOPROVTXT"), row.get("IOBJTXT"), row.get("INFOAREA"), row.get("IOBJTYPE"), row.get("QUERYUPDATE"), row.get("QUERYOWNER"), row.get("QUERYEDIT")));
        }
        return listOfBeans;
    }

    @Override
    protected Pair<Integer, String> startPump() {
        try {
            String path = execNamedQuerySingleVal("getConfigValue", false, "value", "sapbw.path");
            pumpLogger.pumpLog("Getting data from CSV file", 10);
//            String csvContent = LameUtils.loadAsString("C:\\Projekty\\Konektor Sap Bw\\Paczka 7\\ZTCT_META.CSV", "ISO-8859-2");
//            long startTime = System.currentTimeMillis();
            String csvContent = LameUtils.loadAsString(path, "ISO-8859-2");
            CSVReader reader = new CSVReader(csvContent, false, '|');
            List<Map<String, String>> content = reader.readRowsAsMaps(BaseUtils.splitBySep("INFOCUBE,IPLEVEL,PARTCUBE,IOBJNM,FILETYPE,TCTBWOTYPE,INFOPROVUP,INFOPROVTXT,IOBJTXT,ENDLEVEL,INFOAREA,IOBJTYPE,QUERYUPDATE,QUERYOWNER,QUERYEDIT", ","));
            List<SapBWBean> contentList = createListOfBeansFromListOfMap(content);
            int contentListSize = contentList.size();
            if (contentListSize == 0) {
                return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "File is empty!");
            }
            checkForAllFilesTypeInList(contentList);
//            fixBadQueryEncoding(contentList);
            Set<String> missingFileType = new HashSet<String>();
            for (String type : BW_TYPE_NAME_MAP.keySet()) {
                if (!BW_TYPE_NAME_MAP.get(type)) {
                    missingFileType.add(type);
                }
            }
            if (!missingFileType.isEmpty()) {
                return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Bad file! Missing types: " + missingFileType);
            }
            pumpLogger.pumpLog("Insert into temporary table", 20);
            execNamedCommandWithCommit("deleteTmpSapBWTable");
            insertListToDBInPackages(contentList, "insertSapBWMetadata");
//            long endTime = System.currentTimeMillis();
//            System.out.println("Czas trwania w sekundach: " + (endTime - startTime) / 1000);
            execNamedCommandWithCommit("insertAreas");
            execNamedCommandWithCommit("insertQueries");
            execNamedCommandWithCommit("insertProviders");
            execNamedCommandWithCommit("insertFlows");
            execNamedCommandWithCommit("insertObjects");
            pumpLogger.pumpLog("Update BIKS structure: Reports", 70);
            execNamedCommandWithCommit("insertBWMetadataToBikNode", "BWReports");
            pumpLogger.pumpLog("Update BIKS structure: Data providers", 80);
            execNamedCommandWithCommit("insertBWMetadataToBikNode", "BWProviders");
            pumpLogger.pumpLog("Adding connections between metadata", 90);
            execNamedCommandWithCommit("insertBWMetadataConnections");
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error: " + ex.getMessage(), ex);
            }
            throw new LameRuntimeException(ex.getMessage(), ex);
        }
    }

    protected void checkForAllFilesTypeInList(List<SapBWBean> list) {
        for (SapBWBean sapBWBean : list) {
            Boolean type = BW_TYPE_NAME_MAP.get(sapBWBean.fileType);
            if (BW_TYPE_NAME_MAP.containsKey(sapBWBean.fileType) && !type) {
                BW_TYPE_NAME_MAP.put(sapBWBean.fileType, true);
            }
        }
    }

    protected void fixBadQueryEncoding(List<SapBWBean> contentList) {
        for (SapBWBean sapBWBean : contentList) {
            if (sapBWBean.fileType.trim().equals("QUER")) {
                String queryName = sapBWBean.infoDescr;
                System.out.print("Query: " + queryName + ", bytes: " + Arrays.toString(queryName.getBytes()) + "\n");
            }
        }
    }
}
