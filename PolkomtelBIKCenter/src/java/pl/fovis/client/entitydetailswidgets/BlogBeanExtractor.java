/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.HTML;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.BlogBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author beata
 */
public class BlogBeanExtractor extends NodeAwareBeanWithAttributeExtractorBase<BlogBean> {

    protected int currentNodeId;

    public BlogBeanExtractor(boolean isAddJoinedButtonEnabled, int currentNodeId) {
        super(isAddJoinedButtonEnabled);
        this.currentNodeId = currentNodeId;
    }

    public Integer getNodeId(BlogBean b) {
        return b.nodeId;
    }

    public String getTreeCode(BlogBean b) {
        return b.nodeTreeCode;
    }

    protected String getName(BlogBean b) {
        return b.subject != null ? b.subject : b.blogSubject;
    }

    protected String getNodeKindCode(BlogBean b) {
        return b.dstCode;
    }

    @Override
    public boolean hasChildren(BlogBean b) {
        return b.dstHasChildren;
    }

    public String getBranchIds(BlogBean b) {
        return b.branchIds;//narazie brak
    }

    @Override
    public int getAddColCnt() {
        return BIKClientSingletons.isDescriptionInJoinedPanelVisible() ? 3 : 2;
    }

    @Override
    public Object getAddColVal(BlogBean b, int addColIdx) {
        boolean isDescriptionInJoinedPanelVisible = BIKClientSingletons.isDescriptionInJoinedPanelVisible();
        if (isDescriptionInJoinedPanelVisible && addColIdx == 0) {
            return BIKClientSingletons.isDifferentViewType() ? getDescription(b.descrPlain) : b.attributes;
        }

        String userName = b.userName;

        if (userName != null) {
            userName = //userName + getStarIcon(BIKClientSingletons.isStarredSystemUser(b.userId))/*BIKClientSingletons.isStarredSystemUser(b.userId) ? "*" : ""*/;
                    getNameWithOptStarAsHtml(userName, BIKClientSingletons.isStarredSystemUser(b.userId));
        }
        return addColIdx == (0 + (isDescriptionInJoinedPanelVisible ? 1 : 0)) ? (b.dstCode.equals(BIKGWTConstants.NODE_KIND_BLOG_ENTRY)
                //                ? BIKClientSingletons.createIconImg(BIKClientSingletons.fixAvatarUrl(b.avatar, getIconUrlByNodeKindCode(BIKGWTConstants.NODE_KIND_USERS_GROUP)), "16px", "16px", null) : "")
                ? BIKClientSingletons.createAvatarImage(b.avatar, 16, 16, getIconUrlByNodeKindCode(BIKGWTConstants.NODE_KIND_USERS_GROUP, null), null, null) : "")
                : new HTML(userName);
    }

    @Override
    protected void execActDelete(BlogBean b) {
        BIKClientSingletons.getService().deleteBikLinkedMeta(b.srcNodeId, getNodeId(b), getDoNothingCallback());
        deleteBeanFromList(b, I18n.usunietoWpis.get() /* I18N:  */);
        //refreshView("Usunięto wpis");
    }

    @Override
    public boolean isDeleteBtnVisible2Wtf(BlogBean b) {
        return b.srcNodeId == currentNodeId || b.similarityScore != null;
    }

    @Override
    public Double getOptSimilarityScore(BlogBean b) {
        return b.similarityScore;
    }

    @Override
    protected Integer getCurrentNodeId() {
        return currentNodeId;
    }

    @Override
    public Integer getJoinedObjId(BlogBean b) {
        return b.joinedObjsId;
    }

    protected void setAttributes(BlogBean b, String value) {
        b.attributes = value;
    }
}
