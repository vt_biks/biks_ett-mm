/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;
import simplelib.CSVReader.CSVParseMode;

/**
 *
 * @author wezyr
 */
public class CSVReaderTest {

    public CSVReaderTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     * Test of readValue method, of class CSVReader.
     */
    @Test
    public void testReadValue() {
        CSVReader csvr = new CSVReader("\"ala\";\"ma\nkota\";\"psa\"");
        assertEquals("ala", csvr.readValue());
        assertEquals("ma\nkota", csvr.readValue());
        assertEquals("psa", csvr.readValue());

        csvr = new CSVReader("\"ala\";;\"ma\nkota\";\"psa\"");
        assertEquals("ala", csvr.readValue());
        assertEquals("", csvr.readValue());
        assertEquals("ma\nkota", csvr.readValue());
        assertEquals("psa", csvr.readValue());

        csvr = new CSVReader("\"ala\"\n\"ma\nkota\"\r\"psa\"\r\n\"xxx\"");
        assertEquals("ala", csvr.readValue());
        assertEquals("ma\nkota", csvr.readValue());
        assertEquals("psa", csvr.readValue());
        assertEquals("xxx", csvr.readValue());

        csvr = new CSVReader("\"ala\"\n\"ma\n\\\"kota\"\r\"psa \\\\\"\r\n\"xxx\"");
        assertEquals("ala", csvr.readValue());
        assertEquals("ma\n\"kota", csvr.readValue());
        assertEquals("psa \\", csvr.readValue());
        assertEquals("xxx", csvr.readValue());
    }

    /**
     * Test of readRow method, of class CSVReader.
     */
    @Test
    public void testReadRow() {
        String txt4row = "\"ala\";\"ma\nkota\";\"psa\";\"chomika\"";
        CSVReader csvr = new CSVReader(txt4row + "\n" + txt4row + "\r"
                + txt4row + "\r\n" + txt4row + ";\n");

        assertEquals(4, csvr.readRow().size());
        assertEquals(4, csvr.readRow().size());
        assertEquals(4, csvr.readRow().size());
        assertEquals(5, csvr.readRow().size());
        assertEquals(null, csvr.readRow());
    }

    @Test
    public void testQuoteAtEof() {
        String txt = "ala,ma,\"kota\"";
//        String txt = "ala,ma,kota\"";
        CSVReader csvr = new CSVReader(txt, CSVParseMode.CONSIDER_QUOTES, ',', false);
        csvr.readRowsAsLists();
    }

    @Test
    public void testNotEscapingBackslash() {
        String txt = "ala,ma,\"kota\\\"";
//        String txt = "ala,ma,kota\"";
        CSVReader csvr = new CSVReader(txt, CSVParseMode.CONSIDER_QUOTES, ',', true);
        System.out.println("testNotEscapingBackslash:\n" + csvr.readRowsAsLists());
    }
}
