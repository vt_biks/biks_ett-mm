/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.util.ArrayList;
import java.util.List;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class GlobalThreadStopWatch {

    public static boolean IsGlobalThreadStopWatchEnabled = false;
    private static final ILameLogger logger = LameUtils.getMyLogger();

    private static class SubProcessInfo {

        public String procName;
        public String stageName;
        public long procStart;
        public long lastStageStart;
        public long currStageStart;
        public int stageNum;
        private boolean finished;

        public SubProcessInfo(String procName) {
            this.procName = procName;
            this.stageName = "starting";
            this.procStart = System.nanoTime();
            this.currStageStart = this.procStart;
        }

        public void nextStage(String stageName) {
            if (isFinished()) {
                throw new LameRuntimeException("SubProcessInfo: " + procName + " finished, but called with stage: " + stageName);
            }
            this.stageName = stageName;
            this.lastStageStart = currStageStart;
            this.currStageStart = System.nanoTime();
            this.stageNum++;
        }

        public void finished(String optInfo) {
            nextStage("finished" + (optInfo != null ? " (" + optInfo + ")" : ""));
            this.finished = true;
        }

        /**
         * @return the finished
         */
        public boolean isFinished() {
            return finished;
        }
    }

    private static class ThreadProcessInfo {

        public int currProcLvl;
        public int disabledAtProcLvl = Integer.MAX_VALUE;
        public List<SubProcessInfo> levels = new ArrayList<SubProcessInfo>();
//        public boolean isLoggingDisabled;
    }

    public static boolean isProcInfoLoggingEnabled() {
        return IsGlobalThreadStopWatchEnabled && logger.isInfoEnabled();
    }

    private static class ThreadLocalStopWatch extends ThreadLocal<ThreadProcessInfo> {

        @Override
        protected ThreadProcessInfo initialValue() {
            return new ThreadProcessInfo();
//            if (isProcInfoLoggingEnabled()) {
//                return new ThreadProcessInfo();
//            }
//            return null;
        }
    }
    private static ThreadLocalStopWatch processes = new ThreadLocalStopWatch();

    private static void logCurrentStage(ThreadProcessInfo proc) {
        SubProcessInfo p = BaseUtils.getLastItem(proc.levels);
        String prevStageTime = p.stageNum == 0 ? ""
                : " (prev stage time: "
                + BaseUtils.doubleToString((p.currStageStart - p.lastStageStart) / 1000000000.0, 3) + " s)";

        if (p.isFinished()) {
            prevStageTime = prevStageTime + " (total: "
                    + BaseUtils.doubleToString((p.currStageStart - p.procStart) / 1000000000.0, 3) + " s)";
        }

        logger.info(/*"[" + Thread.currentThread().getName() + "]: "
                 +*/BaseUtils.replicateStr(">> ", proc.levels.size() - 1)
                + p.procName + ": #" + p.stageNum + " " + p.stageName + prevStageTime);
//            if (!isProcInfoLoggingEnabled()) {
//                proc.isLoggingDisabled = true;
//            }
    }

    public static void startProcess(String processName) {
        startProcess(processName, false);
    }

    public static void startProcess(String processName, boolean disableLoggingForSubProcesses) {
//        if (!isProcInfoLoggingEnabled()) {
//            return;
//        }

        ThreadProcessInfo proc = processes.get();

        proc.currProcLvl++;
        if (!isProcInfoLoggingEnabled() || proc.disabledAtProcLvl < proc.currProcLvl) {
            return;
        }

        if (disableLoggingForSubProcesses) {
            proc.disabledAtProcLvl = proc.currProcLvl;
        }

        //        if (proc == null) {
        //            return;
        //        }
        proc.levels.add(new SubProcessInfo(processName));
        logCurrentStage(proc);
    }

    public static void startProcessStage(String stageName) {
        ThreadProcessInfo proc = processes.get();
        if (!isProcInfoLoggingEnabled() || proc.disabledAtProcLvl < proc.currProcLvl) {
            return;
        }
//        if (proc == null) {
//            return;
//        }
        BaseUtils.getLastItem(proc.levels).nextStage(stageName);
        logCurrentStage(proc);
    }

    public static void endProcess() {
        endProcess(null);
    }

    public static void endProcess(String optInfo) {
        ThreadProcessInfo proc = processes.get();
//        if (proc == null) {
//            return;
//        }

        if (proc.currProcLvl <= 0) {
            throw new LameRuntimeException("no process to end: currProcLvl=" + proc.currProcLvl);
        }

        proc.currProcLvl--;

        if (proc.currProcLvl < proc.levels.size()) {
            if (isProcInfoLoggingEnabled()) {
                BaseUtils.getLastItem(proc.levels).finished(optInfo);
                logCurrentStage(proc);
            }

            BaseUtils.removeLastItem(proc.levels);
            if (proc.levels.isEmpty()) {
                processes.remove();
            }
        }

        if (proc.currProcLvl < proc.disabledAtProcLvl) {
            proc.disabledAtProcLvl = Integer.MAX_VALUE;
        }
    }
}
