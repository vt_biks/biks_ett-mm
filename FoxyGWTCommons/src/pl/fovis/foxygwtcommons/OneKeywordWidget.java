/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;

/**
 *
 * @author tflorczak
 */
public class OneKeywordWidget extends HorizontalPanel {

    protected String name;
    protected Label lblName;
    protected PushButton btnDelete;
    protected KeywordsWidget tab;

    public OneKeywordWidget(KeywordsWidget tab, String tag) {
        this.name = tag;
        this.tab = tab;
        buildWidgets();
    }

    public String getName() {
        return name;
    }

    private void buildWidgets() {
        lblName = new Label(name);
        lblName.setStyleName("lblKeyword");
        btnDelete = new PushButton(new Image("images/delete.png" /* I18N: no */));
        btnDelete.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
                tab.deleteOneTag(name);
            }
        });
        Style style = btnDelete.getElement().getStyle();
        style.setMargin(0, Unit.PX);
        style.setPadding(0, Unit.PX);
        GWTUtils.setStyleAttribute(btnDelete.getElement(), "background" /* I18N: no */, "none" /* I18N: no */);
        style.setBorderWidth(0, Unit.PX);

        GWTUtils.setStyleAttribute(this, "float" /* I18N: no */, "left" /* I18N: no */);
        this.setStyleName("OneKeywordWidget");

        add(lblName);
        add(btnDelete);
    }
}
