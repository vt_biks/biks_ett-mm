/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
public class NodeActionCut extends NodeActionUpdatingBase {

    @Override
    public String getCaption() {
        return I18n.wytnij.get() /* I18N:  */;
    }

    @Override
    public void execute() {
        if (clipboard.getNode() != null && clipboard.isCut()) {
            ctx.getTreeGrid().markAsUncutNode(clipboard.getNode().id);
            clipboard.clear();
        }
        clipboard.cut(node);
        ctx.getTreeGrid().markAsCutNode(node.id);
    }

    @Override
    public boolean isEnabledByDesign() {
        if(!treeBean.treeKind.equals(BIKConstants.TREE_KIND_META_BIKS)){
            return false;
        }
        if (isNodeOneOfTheKind(false, BIKConstants.NODE_KIND_META_BUILT_IN_NODE_KIND)) {
            return false;
        }
        if (isConfluenceObject()) {
            return node.isBuiltIn && isAdminOrExpertOrAuthorOrEditor();
        }
        if (isInDQMAdminMode()) {
            return !isNodeBuiltIn();
        }
        if (BIKClientSingletons.isLoggedUserAuthorOfTree(node.treeId) || BIKClientSingletons.isLoggedUserCreatorOfTree(node.treeId)) {
            return true;
        }
        if (isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_BLOG, BIKGWTConstants.NODE_KIND_USER, BIKGWTConstants.NODE_KIND_GLOSSARY_VERSION, BIKGWTConstants.NODE_KIND_DQM_RATE, BIKGWTConstants.NODE_KIND_DQM_RATE_ACCEPTED, BIKGWTConstants.NODE_KIND_DQM_RATE_REJECTED, BIKGWTConstants.NODE_KIND_DQM_REPORT)) {
            return false;
        } else if (isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_BLOG_ENTRY)) {
            return isNodeNotLinkedAnyhowNorReadOnlyNodeAndBlogEntryNodeAndUserInProperAuthor();
        } else {
            return isNodeOneOfTheKind(false, BIKConstants.NODE_KIND_LISA_DICTIONARY, BIKConstants.NODE_KIND_LISA_FOLDER)
                    || (!isNodeReadOnlyAndBuiltIn() && !isNodeLinkedUnderRoot());//isNodeNotLinkedAnyhowNorBuiltinNorReadOnlyNode();
        }
    }
}
