/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog.AttributeType;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author bfechner
 */
public class ManagmentAttrubuteWidget {

    protected DataGrid<AttributeBean> dataGrid;
    protected MultiSelectionModel<AttributeBean> selectionModel;
    protected ListDataProvider<AttributeBean> dataProvider;
    VerticalPanel rightVp;
    protected SimplePager pager;
    VerticalPanel mainVp;

    public ManagmentAttrubuteWidget() {

    }

    public Widget buildWidgets() {
        rightVp = new VerticalPanel();
        mainVp = new VerticalPanel();
        mainVp.setWidth("100%");
        rightVp.setWidth("100%");
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(rightVp, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 150);
        initDataGrid();
        PushButton b = new PushButton(I18n.odswiez.get());
        NewLookUtils.makeCustomPushButton(b);
        b.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                refreshGrid();
            }
        });

        FlowPanel fp = new FlowPanel();
        fp.add(b);
        fp.add(new Label(I18n.sortowanieKolumn.get()));
        mainVp.add(fp);
        mainVp.add(rightVp);

        mainVp.add(pager);
        return mainVp;

    }

    protected void initDataGrid() {
        //////////////////////////////////////////
        // Initialize DataGrid
        ProvidesKey<AttributeBean> keyProvider = new ProvidesKey<AttributeBean>() {
            @Override
            public Object getKey(AttributeBean item) {
                return item == null ? null : item.id + " " + item.atrName + " " + item.kindCaption;
            }
        };
        dataGrid = new DataGrid<AttributeBean>(25, keyProvider);
        dataGrid.setMinimumTableWidth(1540, Style.Unit.PX);
        dataGrid.setAlwaysShowScrollBars(true);
        // Set the message to display when the table is empty.
        Label emptyTableWidget = new Label(I18n.brakWynikow.get());
        dataGrid.setEmptyTableWidget(emptyTableWidget);
        dataGrid.getEmptyTableWidget().getParent().setStyleName("BIAdmin-none");
        // Attach a column sort handler to the ListDataProvider to sort the list.
        dataProvider = new ListDataProvider<AttributeBean>();
        ColumnSortEvent.ListHandler<AttributeBean> sortHandler = new ColumnSortEvent.ListHandler<AttributeBean>(dataProvider.getList());
        dataGrid.addColumnSortHandler(sortHandler);
        // Create a Pager to control the table.
        pager = new SimplePager(SimplePager.TextLocation.CENTER, false, 0, true);
        pager.setDisplay(dataGrid);
        // Add a selection model so we can select cells.
        selectionModel = new MultiSelectionModel<AttributeBean>(/*keyProvider*/);
        dataGrid.setSelectionModel(selectionModel, DefaultSelectionEventManager.<AttributeBean>createCheckboxManager());
        selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {

            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
//                refreshActionButtons();
            }
        });
        // Initialize the columns.
        initTableColumns(sortHandler);
        // Add data display
        dataProvider.addDataDisplay(dataGrid);
//        dataGrid.setWidth("1500px");
//        dataGrid.setHeight("500px");
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(dataGrid, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 105);
        // Data Grid init END
        /////////////////////////////////////////////
    }

    protected void initTableColumns(ColumnSortEvent.ListHandler<AttributeBean> sortHandler) {

        // Nazwa
        Column<AttributeBean, String> attributeName = new Column<AttributeBean, String>(new TextCell()) {
            @Override
            public String getValue(AttributeBean object) {
                return object.getAtrName();
            }
        };
        attributeName.setSortable(true);
        sortHandler.setComparator(attributeName, new Comparator<AttributeBean>() {
            @Override
            public int compare(AttributeBean o1, AttributeBean o2) {
                return o1.getAtrName().compareTo(o2.getAtrName());
            }
        });
        dataGrid.addColumn(attributeName, BaseUtils.capitalize(I18n.atrybut.get()));
        dataGrid.setColumnWidth(attributeName, "100px"/*, Unit.PX*/);

        //        -------------
//        Column<AttributeBean, String> path = new Column<AttributeBean, String>(new TextCell()) {
//            @Override
//            public String getValue(AttributeBean object) {
//                return object.getPath();
//            }
//        };
//        path.setSortable(true);
//        sortHandler.setComparator(path, new Comparator<AttributeBean>() {
//            @Override
//            public int compare(AttributeBean o1, AttributeBean o2) {
//                return o1.getPath().compareTo(o2.getPath());
//            }
//        });
//        dataGrid.addColumn(path, " ");
//        dataGrid.setColumnWidth(path, "100px"/*, Unit.PX*/);
        // ICON
        Column<AttributeBean, SafeHtml> icon = new Column<AttributeBean, SafeHtml>(new SafeHtmlCell()) {
            @Override
            public SafeHtml getValue(AttributeBean object) {
                return SafeHtmlUtils.fromSafeConstant("<img src='images/" + object.getIconName() + ".gif' style='vertical-align: middle'> ");
            }
        };
        dataGrid.addColumn(icon, " ");
        dataGrid.setColumnWidth(icon, "15px"/*, Unit.PX*/);

//     ------------------
        Column<AttributeBean, String> nodeKindCaption = new Column<AttributeBean, String>(new TextCell()) {
            @Override
            public String getValue(AttributeBean object) {
                return object.getKindCaption();
            }
        };
        nodeKindCaption.setSortable(true);
        sortHandler.setComparator(nodeKindCaption, new Comparator<AttributeBean>() {
            @Override
            public int compare(AttributeBean o1, AttributeBean o2) {
                return o1.getKindCaption().compareTo(o2.getKindCaption());
            }
        });
        dataGrid.addColumn(nodeKindCaption, "Węzeł");
        dataGrid.setColumnWidth(nodeKindCaption, "100px"/*, Unit.PX*/);

        List<String> typeAttrs = new ArrayList<String>();
        for (AttributeType type : AttributeType.values()) {
            typeAttrs.add(type.caption);
        }
        Column<AttributeBean, String> typeAttr = new Column<AttributeBean, String>(new SelectionCell(typeAttrs)) {
            @Override
            public String getValue(AttributeBean object) {
                return AddOrEditAdminAttributeWithTypeDialog.AttributeType.valueOf(object.typeAttr).caption;
            }
        };
        typeAttr.setSortable(false);
        typeAttr.setFieldUpdater(new FieldUpdater<AttributeBean, String>() {

            @Override
            public void update(int index, final AttributeBean object, String value) {
                object.typeAttr = AddOrEditAdminAttributeWithTypeDialog.getSelectedAttrType(value);
                BIKClientSingletons.getService().updateAttributes(object, new StandardAsyncCallback<Void>() {

                    @Override
                    public void onSuccess(Void result) {

                        BIKClientSingletons.showInfo(I18n.atrybut.get() + " " + object.atrName + " dla " + object.kindCaption
                                + " " + I18n.zostalzmienony.get());
                    }
                });
            }
        });

        dataGrid.addColumn(typeAttr, I18n.typ.get());
        dataGrid.setColumnWidth(typeAttr, "100px"/*, Unit.PX*/);

        Column<AttributeBean, String> valueOpt = new Column<AttributeBean, String>(new EditTextCell()) {
            @Override
            public String getValue(AttributeBean object) {
                return object.valueOpt == null ? "" : object.valueOpt;
            }
        };
        valueOpt.setSortable(false);
        valueOpt.setFieldUpdater(new FieldUpdater<AttributeBean, String>() {

            @Override
            public void update(int index, final AttributeBean object, String value) {
                object.valueOpt = value;
                BIKClientSingletons.getService().updateAttributes(object, new StandardAsyncCallback<Void>() {

                    @Override
                    public void onSuccess(Void result) {

                        BIKClientSingletons.showInfo(I18n.atrybut.get() + " " + object.atrName + " dla " + object.kindCaption
                                + " " + I18n.zostalzmienony.get());
                    }
                });
            }
        });

        dataGrid.addColumn(valueOpt, I18n.opis.get());
        dataGrid.setColumnWidth(valueOpt, "100px"/*, Unit.PX*/);
        Column<AttributeBean, String> isFolder = new Column<AttributeBean, String>(new TextCell()) {
            @Override
            public String getValue(AttributeBean object) {
                return object.isFolder() ? " ✓" : "-";
            }
        };
        isFolder.setSortable(true);

        sortHandler.setComparator(isFolder, new Comparator<AttributeBean>() {
            @Override
            public int compare(AttributeBean o1, AttributeBean o2) {
                return (o1.isFolder() ? " ✓" : "-").compareTo((o2.isFolder() ? " ✓" : "-"));
            }
        });
        dataGrid.addColumn(isFolder, "Czy folder");
        dataGrid.setColumnWidth(isFolder, "35px"/*, Unit.PX*/);

        Column<AttributeBean, Boolean> checkColumnIsRequired = new Column<AttributeBean, Boolean>(new CheckboxCell(true, true)) {
            @Override
            public Boolean getValue(AttributeBean object) {
                return object.isRequired();
                // Get the value from the selection model.
//                return selectionModel.isSelected(object);
            }

        };
        checkColumnIsRequired.setFieldUpdater(new FieldUpdater<AttributeBean, Boolean>() {

            @Override
            public void update(int index, final AttributeBean object, final Boolean value) {
                object.isRequired = value;

                BIKClientSingletons.getService().updateAttributes(object, new StandardAsyncCallback<Void>() {

                    @Override
                    public void onSuccess(Void result) {

                        BIKClientSingletons.showInfo(I18n.atrybut.get() + " " + object.atrName + " dla " + object.kindCaption
                                + " " + I18n.zostalzmienony.get() + " " + (value ? I18n.wymagany.get() : I18n.niewymagany.get()));
                    }
                });

            }
        });

        checkColumnIsRequired.setSortable(true);

        sortHandler.setComparator(checkColumnIsRequired, new Comparator<AttributeBean>() {
            @Override
            public int compare(AttributeBean o1, AttributeBean o2) {
                return (o1.isRequired ? "T" : "N").compareTo((o2.isRequired ? "T" : "N"));
            }
        });

        dataGrid.addColumn(checkColumnIsRequired, "Wymagany");
        dataGrid.setColumnWidth(checkColumnIsRequired, "35px"/*, Unit.PX*/);

        Column<AttributeBean, Boolean> checkColumnIsVisible = new Column<AttributeBean, Boolean>(new CheckboxCell(true, true)) {
            @Override
            public Boolean getValue(AttributeBean object) {
                return object.isVisible();
                // Get the value from the selection model.
//                return selectionModel.isSelected(object);
            }

        };

        checkColumnIsVisible.setSortable(true);

        sortHandler.setComparator(checkColumnIsVisible, new Comparator<AttributeBean>() {
            @Override
            public int compare(AttributeBean o1, AttributeBean o2) {
                return (o1.isVisible() ? "T" : "N").compareTo((o2.isVisible() ? "T" : "N"));
            }
        });

        checkColumnIsVisible.setFieldUpdater(new FieldUpdater<AttributeBean, Boolean>() {

            @Override
            public void update(int index, final AttributeBean object, final Boolean value) {
                object.isVisible = value ? 1 : 0;
                BIKClientSingletons.getService().updateAttributes(object, new StandardAsyncCallback<Void>() {

                    @Override
                    public void onSuccess(Void result) {
                        BIKClientSingletons.showInfo(I18n.atrybut.get() + " " + object.atrName + " dla " + object.kindCaption
                                + " " + I18n.zostalzmienony.get() + " " + (value ? I18n.widoczny.get() : I18n.niewidoczny.get()));
                    }
                });
            }
        });

        dataGrid.addColumn(checkColumnIsVisible, "Widoczny");
        dataGrid.setColumnWidth(checkColumnIsVisible, "35px"/*, Unit.PX*/);

    }

    public void refreshGrid() {
//        showLoadingWidget();
//        refreshBtn.setEnabled(false);
        BIKClientSingletons.getService().getManagmentAttrubutes(new StandardAsyncCallback<List<AttributeBean>>() {

            @Override
            public void onSuccess(List<AttributeBean> result) {

                showDataGrid();

                List<AttributeBean> list = dataProvider.getList();
                list.clear();
                list.addAll(result);

                dataProvider.refresh();
                selectionModel.clear();
//                refreshActionButtons();
                dataGrid.redrawHeaders();
            }

            @Override
            public void onFailure(Throwable caught) {
//                showDataGrid();
//                refreshBtn.setEnabled(true);
                super.onFailure(caught);
            }

        });

    }

    public void showDataGrid() {
        rightVp.clear();
        rightVp.add(dataGrid);
    }

}
