/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.IJoinedObjSelectionManager;
import pl.fovis.client.dialogs.TreeSelectorForJoinedObjectsDialog;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.JoinedObjBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.GWTUtils;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.LameBag;

/**
 *
 * @author beata
 * @param <T>
 */
public abstract class JoinedObjWidgetFlatBase<T> extends EntityDetailsTabPaginationBase<JoinedObjBean> {

    private HorizontalPanel filters;
    private FlowPanel checkBoxContainer;
    protected List<CheckBox> checkboxes = new ArrayList<CheckBox>();
    protected Set<String> activeKinds;

    protected void recreateFilterCheckboxes() {

        LameBag<String> counts = new LameBag<String>();
        List<JoinedObjBean> joinedObjsForMetadata = items;
        if (joinedObjsForMetadata != null) {
            for (JoinedObjBean job : joinedObjsForMetadata) {
                counts.add(job.dstCode);
            }
        }
        clearCheckboxes();
        addCheckbox("*", "<b>" + I18n.wszystko.get() /* I18N:  */ + "</b> (" + BaseUtils.collectionSizeFix(joinedObjsForMetadata) + ")",
                true, new IContinuation() {

                    public void doIt() {
                        boolean check = isFilterOn(0);
                        for (int i = 1; i < getFilterCnt(); i++) {
                            setFilterActive(i, check);
                        }
//                showDescendentNodes();
                        displayData();
                    }
                });

        for (String kind : counts.uniqueSet()) {

            addCheckbox(kind,
                    BIKClientSingletons.getHtmlForNodeKindIconWithText(kind, null, BIKClientSingletons.getNodeKindCaptionByCode(kind) + " (" + counts.getCount(kind) + ")"), true,
                    new IContinuation() {

                        @Override
                        public void doIt() {
                            displayData();
//                            showDescendentNodes();
                        }
                    });
        }

        finalizeCheckboxes();
    }

    @Override
    protected BikCustomButton createAddJoinedButton(final Set<String> treeKinds) {
//        final IJoinedObjSelectionManager<Integer> alreadyJoinedManager = getAlreadyJoinedManager(); //null;//this;
//
//        BikCustomButton addJoinedObjBtn = new BikCustomButton(I18n.dodajPowiazanie.get() /* I18N:  */, "Glosariusz-addTezaurusEntry" /* i18n: no */,
//                new IContinuation() {
//                    @Override
//                    public void doIt() {
//
//                        final TreeSelectorForJoinedObjectsDialog tsfjod = new TreeSelectorForJoinedObjectsDialog();
//
//                        tsfjod.buildAndShowDialog(nodeId, treeKinds,
//                                new IParametrizedContinuation<TreeNodeBean>() {
//                                    @Override
//                                    public void doIt(TreeNodeBean param) {
//                                        BIKClientSingletons.getService().addBikJoinedObject(nodeId, param.id,
//                                                tsfjod.isInheritToDescendantsChecked(), getAntiNodeRefreshingCallback() //getNodeRefreshingCallback()
//                                        );
//                                        JoinedObjBean njob = new JoinedObjBean();
//                                        njob.srcId = nodeId;
//                                        njob.dstId = param.id;
//                                        njob.dstType = param.nodeKindCaption;
//                                        njob.dstName = param.name;
//                                        njob.tabId = param.treeCode;
//                                        njob.dstCode = param.nodeKindCode;
//                                        njob.treeKind = param.treeKind;
////                                        getData().joinedObjs.add(njob);
//                                        items.add(njob);
//                                        recreateFilterCheckboxes();
//                                        displayData();
//                                        refreshTabCaption();
//                                    }
//                                }, alreadyJoinedManager);
//                    }
//                });
//        addWidgetVisibledByState(addJoinedObjBtn);
//        return addJoinedObjBtn;
        return createAddJoinedButton(I18n.dodajPowiazanie.get(),treeKinds, null, null);
    }

    protected BikCustomButton createAddJoinedButton(String caption,final Set<String> treeKinds, final Integer nodeKindId, final String relationType) {
        final IJoinedObjSelectionManager<Integer> alreadyJoinedManager = getAlreadyJoinedManager(); //null;//this;

        BikCustomButton addJoinedObjBtn = new BikCustomButton(caption/*I18n.dodajPowiazanie.get() */, "Glosariusz-addTezaurusEntry" /* i18n: no */,
                new IContinuation() {
                    @Override
                    public void doIt() {

                        final TreeSelectorForJoinedObjectsDialog tsfjod = new TreeSelectorForJoinedObjectsDialog();

                        tsfjod.buildAndShowDialog(nodeId, treeKinds, nodeKindId,treeId, relationType, BIKClientSingletons.isTreeKindDynamic(getTreeKind()),
                                new IParametrizedContinuation<TreeNodeBean>() {
                                    @Override
                                    public void doIt(TreeNodeBean param) {
                                        BIKClientSingletons.getService().addBikJoinedObject(nodeId, param.id,
                                                tsfjod.isInheritToDescendantsChecked(), getAntiNodeRefreshingCallback() //getNodeRefreshingCallback()
                                        );
                                        JoinedObjBean njob = new JoinedObjBean();
                                        njob.srcId = nodeId;
                                        njob.dstId = param.id;
                                        njob.dstType = param.nodeKindCaption;
                                        njob.dstName = param.name;
                                        njob.tabId = param.treeCode;
                                        njob.dstCode = param.nodeKindCode;
                                        njob.treeKind = param.treeKind;
//                                        getData().joinedObjs.add(njob);
                                        items.add(njob);
                                        recreateFilterCheckboxes();
                                        displayData();
                                        refreshTabCaption();
                                    }
                                }, alreadyJoinedManager);
                    }
                });
        addWidgetVisibledByState(addJoinedObjBtn);
        return addJoinedObjBtn;
    }

    @Override
    protected BikCustomButton createAddJoinedButton2(final Set<String> treeKinds) {
//        final IJoinedObjSelectionManager<Integer> alreadyJoinedManager = getAlreadyJoinedManager(); //null;//this;
//
//        BikCustomButton addJoinedObjBtn = new BikCustomButton(I18n.dodajPowiazanieZFiltrowanie.get() /* I18N:  */, "Glosariusz-addTezaurusEntry" /* i18n: no */,
//                new IContinuation() {
//                    @Override
//                    public void doIt() {
//                        BIKClientSingletons.getService().getBikAllJoinableNodeListByLinkedKind(
//                                nodeKindId, treeBean.id, BIKConstants.ASSOCIATION, new StandardAsyncCallback<List<TreeNodeBean>>() {
//
//                                    @Override
//                                    public void onSuccess(List<TreeNodeBean> result) {
//
//                                        new AddJoinDialog().buildAndShowDialog(nodeKindId, result, nodeId, alreadyJoinedManager, new IParametrizedContinuation<TreeNodeBean>() {
//
//                                            @Override
//                                            public void doIt(TreeNodeBean param) {
//
//                                                BIKClientSingletons.getService().addBikJoinedObject(nodeId, param.id,
//                                                        true/*tsfjod.isInheritToDescendantsChecked()*/, getAntiNodeRefreshingCallback() //getNodeRefreshingCallback()
//                                                );
//                                                JoinedObjBean njob = new JoinedObjBean();
//                                                njob.srcId = nodeId;
//                                                njob.dstId = param.id;
//                                                njob.dstType = param.nodeKindCaption;
//                                                njob.dstName = param.name;
//                                                njob.tabId = param.treeCode;
//                                                njob.dstCode = param.nodeKindCode;
//                                                njob.treeKind = param.treeKind;
////                                        getData().joinedObjs.add(njob);
//                                                items.add(njob);
//                                                recreateFilterCheckboxes();
//                                                displayData();
//                                                refreshTabCaption();
//                                            }
//                                        });
//                                    }
//                                });
////
//                    }
//                });
//        addWidgetVisibledByState(addJoinedObjBtn);
        //return addJoinedObjBtn;

        return createAddJoinedButton(I18n.dodajPowiazanieZFiltrowanie.get(),treeKinds, nodeKindId, BIKConstants.ASSOCIATION);
    }

    @Override
    protected void addOptionalFilterWidgets() {
        if (BaseUtils.collectionSizeFix(items) > 0) {
            filters = new HorizontalPanel();
            filters.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
            HTML lblFilters = new HTML(I18n.pokazTypyObiektowPowiazanych.get() /* I18N:  */ + ":");
            filters.add(lblFilters);
            filters.setWidth("100%");
            GWTUtils.setStyleAttribute(filters, "minHeight", "25px");
            filters.setCellWidth(lblFilters, "180px");
            checkBoxContainer = new FlowPanel();
            filters.add(checkBoxContainer);

            FlowPanel fpHelp = new FlowPanel();
            fpHelp.add(filters);
            fpHelp.setStyleName("RelatedObj-filters");
            mainJoinedObjVl.add(fpHelp);
            mainJoinedObjVl.setCellHeight(filters, "20px");

            recreateFilterCheckboxes();
        }
    }

//    @Override
//    protected boolean shouldShowItem(JoinedObjBean e) {
//        return true;
//    }
    @Override
    protected void dataChanged() {
        recreateFilterCheckboxes();
        super.dataChanged();
    }

    @Override
    public boolean hasEditableControls() {
        return !isInUserTree(getNodeTreeCode());
    }

    @Override
    public boolean hasContent() {
        return !isInUserTree(getNodeTreeCode()) && super.hasContent();
    }

    protected NodeAwareBeanExtractorBase<JoinedObjBean> getBeanExtractor() {
        return new JoinedObjBeanExtractor(hasAddJoinedButton, nodeId);
    }

    @Override
    protected boolean shouldShowItem(JoinedObjBean e) {
        return activeKinds.contains(e.dstCode);
    }

    @Override
    protected void prepareItemFiltering() {
        boolean allActive = true;
        activeKinds = new HashSet<String>();
        for (int i = 1; i < getFilterCnt(); i++) {
            if (isFilterOn(i)) {
                activeKinds.add(getFilterName(i));
            } else {
                allActive = false;
            }
        }

        setFilterActive(0, allActive);
    }

    protected void addCheckbox(String name, String caption, boolean active, final IContinuation contWhenChanged) {
        final CheckBox cb = new CheckBox(caption, true);
        cb.setStyleName("Filters-Checkbox");
        cb.setValue(active);
        cb.setName(name);
        //tymczasowa szerokość wstawiona tutaj.
        //cb.setWidth("155px");
        cb.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent ce) {
                contWhenChanged.doIt();
            }
        });
        checkboxes.add(cb);
    }

    protected void finalizeCheckboxes() {
        checkBoxContainer.clear();
        for (CheckBox cb : checkboxes) {
            checkBoxContainer.add(cb);
        }
    }

    protected int getFilterCnt() {
        return checkboxes.size();
    }

    protected boolean isFilterOn(int idx) {
        return checkboxes.get(idx).getValue();
    }

    protected String getFilterName(int idx) {
        return checkboxes.get(idx).getName();
    }

    protected void setFilterActive(int idx, boolean active) {
        checkboxes.get(idx).setValue(active);
    }

    protected void clearCheckboxes() {
        checkboxes.clear();
    }

    @Override
    protected boolean isChangedViewDescriptionInJoinedPanelVisible() {
        return true;
    }

    @Override
    protected boolean isViewWithAttributeRbInJoinedPanelVisible() {
        return true;
    }

    /**
     * Pobieramy zawsze
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}
