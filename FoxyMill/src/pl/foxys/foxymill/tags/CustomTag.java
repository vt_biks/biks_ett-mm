/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.tags;

import java.util.Map;
import pl.foxys.foxymill.IDynamicCustomTagsModel;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class CustomTag extends InsertTag {

    protected boolean fixedUp;
    protected Object val;
    protected final String tagName;

    public CustomTag(String tagName) {
        this.tagName = tagName;
    }

    @Override
    protected Object evalExprAttrVal(IRenderCtx renderCtx) {
        if (!fixedUp) {
            throw new LameRuntimeException("this custom tag is not fixed up");
        }

        Object res;

        Object model = renderCtx.getRootModel();
        if (model instanceof IDynamicCustomTagsModel) {
            IDynamicCustomTagsModel dctm = (IDynamicCustomTagsModel) model;
            res = dctm.getCustomTagVal(tagName);
        } else {
            res = val;
        }

        return res;
    }

    @Override
    protected void fixUpSelf(Map<String, Object> globals) {
        if (!globals.containsKey(tagName)) {
            //throw new LameRuntimeException("no definition for custom tag named '" + tagName + "'");
            raiseError("no definition for custom tag named '" + tagName + "'");
        }
        val = globals.get(tagName);
        fixedUp = true;
    }

    @Override
    protected boolean isExprAttrRequired() {
        return false;
    }
}
