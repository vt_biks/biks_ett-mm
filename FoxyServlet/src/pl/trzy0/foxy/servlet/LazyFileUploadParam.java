/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.servlet;

import org.apache.commons.fileupload.FileItem;
import pl.trzy0.foxy.commons.IFileUploadParam;

/**
 *
 * @author wezyr
 */
public class LazyFileUploadParam implements IFileUploadParam {

    private FileItem item;

    public LazyFileUploadParam(FileItem item) {
        this.item = item;
    }

    public String getFileName() {
        return item.getName();
    }

    public byte[] getFileContent() {
        return item.get();
    }

    public long getFileSize() {
        return item.getSize();
    }    
}
