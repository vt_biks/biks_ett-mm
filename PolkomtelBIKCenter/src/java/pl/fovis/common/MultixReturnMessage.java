/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author LKI
 */
public class MultixReturnMessage implements Serializable {

    public String msg;
    public msgType type;

    public enum msgType {

        error, userGotAccount, ok, alreadyUsed;

        public boolean isError() {
            return error.equals(this) || alreadyUsed.equals(this);
        }
    }

    public MultixReturnMessage() {
    }

    public MultixReturnMessage(String msg, msgType type) {
        this.msg = msg;
        this.type = type;
    }
}
