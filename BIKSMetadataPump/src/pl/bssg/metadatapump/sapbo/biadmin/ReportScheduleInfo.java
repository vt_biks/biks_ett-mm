/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.biadmin;

import com.crystaldecisions.sdk.occa.infostore.CePropertyID;
import com.crystaldecisions.sdk.occa.infostore.IDestination;
import com.crystaldecisions.sdk.occa.infostore.IDestinationPlugin;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import com.crystaldecisions.sdk.occa.infostore.IProcessingInfo;
import com.crystaldecisions.sdk.occa.infostore.ISchedulingInfo;
import com.crystaldecisions.sdk.plugin.destination.diskunmanaged.IDiskUnmanagedOptions;
import com.crystaldecisions.sdk.plugin.destination.smtp.ISMTPOptions;
import com.crystaldecisions.sdk.properties.IProperties;
import com.crystaldecisions.sdk.properties.IProperty;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.common.SAPBOScheduleBean;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class ReportScheduleInfo {

    public static List<SAPBOScheduleBean> getSchedules(IInfoStore iStore) {
        List<SAPBOScheduleBean> scheduleList = new ArrayList<SAPBOScheduleBean>();
        Integer siID = null;
        try {
            while (true) {
                IInfoObjects results = iStore.query("select top 100 * from ci_infoobjects"
                        + (siID == null ? " where si_recurring = 1" : (" where (si_id > " + siID + ") AND ( si_recurring = 1 )")) + " order by si_id");

                for (int i = 0; i < results.size(); i++) {
                    IInfoObject obj = (IInfoObject) results.get(i);
                    scheduleList.add(getOneSchedule(obj, iStore));
                    siID = obj.getID();
                }
                if (results.size() < 100) {
                    break;
                }
            }
        } catch (Exception e) {
            return null;
        }
        return scheduleList;
    }

    public static SAPBOScheduleBean getOneSchedule(IInfoObject iInfoObject, IInfoStore iStore) {
        IProperties properties = iInfoObject.properties();
        ISchedulingInfo schedulingInfo = iInfoObject.getSchedulingInfo();
        IProcessingInfo processingInfo = iInfoObject.getProcessingInfo();
        IProperties processingProperties = processingInfo.properties();

        SAPBOScheduleBean oneSchedule = new SAPBOScheduleBean();
        oneSchedule.id = getId(properties);
        oneSchedule.parentId = getParentId(properties);
        oneSchedule.name = getName(iInfoObject, properties);
        oneSchedule.destination = getDestination(schedulingInfo);
        oneSchedule.emailAddresses = getEmailAddreses(schedulingInfo, iStore);
        oneSchedule.owner = getOwner(properties);
        oneSchedule.creationTime = getCreationTime(properties);
        oneSchedule.nextruntime = getNextRunTime(properties);
        oneSchedule.expire = getExpireTime(schedulingInfo);
        oneSchedule.type = getType(schedulingInfo);
        oneSchedule.scheduleType = getScheduleType(properties);
        oneSchedule.intervalMinutes = getIntervalMinutes(schedulingInfo);
        oneSchedule.intervalHours = getIntervalHours(schedulingInfo);
        oneSchedule.intervalDays = getIntervalDays(schedulingInfo);
        oneSchedule.intervalMonths = getIntervalMonths(schedulingInfo);
        oneSchedule.intervalNthDay = getIntervalNthDay(schedulingInfo);
        oneSchedule.format = getFormat(processingProperties);
        oneSchedule.parameters = getParameters(processingProperties);
        // exteded
        oneSchedule.started = getStartDate(properties, schedulingInfo);
        oneSchedule.ended = getEndDate(properties);
        oneSchedule.errorMessage = getErrorMessage(schedulingInfo);
        oneSchedule.recurring = getIsRecurring(properties);
        oneSchedule.modifyTime = getModifyTime(properties);
        oneSchedule.server = getSiMmachineUsed(schedulingInfo);
        oneSchedule.cuid = getCuid(properties);
        oneSchedule.destinationPaths = getDestinationPaths(schedulingInfo, iStore);
        oneSchedule.kind = getKind(properties);
        return oneSchedule;
    }

    private static Integer getId(IProperties properties) {
        return (Integer) properties.getProperty("SI_ID").getValue();
    }

    private static Integer getParentId(IProperties properties) {
        return (Integer) properties.getProperty("SI_PARENTID").getValue();
    }

    private static String getName(IInfoObject iInfoObject, IProperties properties) {
        String name = iInfoObject.getTitle();
//        int status = properties.getInt("SI_SCHEDULE_STATUS");
//        if (status == ISchedulingInfo.ScheduleStatus.PAUSED) {
//            name += " (wstrzymany)";
//        }
        return name;
    }

    private static String getDestination(ISchedulingInfo schedulingInfo) {
        if (schedulingInfo.getDestinations().size() > 0) {
            IDestination desc = (IDestination) schedulingInfo.getDestinations().get(0);
            String dest = desc.getName();
            return dest == null ? "Default" : dest.replace("CrystalEnterprise.", "");
        }
        return "Default";
    }

    private static String getEmailAddreses(ISchedulingInfo schedulingInfo, IInfoStore iStore) {
        if (schedulingInfo.getDestinations().size() > 0) {
            IDestination desc = (IDestination) schedulingInfo.getDestinations().get(0);
            String destinationName = desc.getName();
            if (destinationName.compareTo("CrystalEnterprise.Smtp") == 0) {
                try {
                    IDestinationPlugin boDestinationPlugin = (IDestinationPlugin) iStore.query("Select Top 1 * from CI_SYSTEMOBJECTS Where SI_NAME = 'CrystalEnterprise.SMTP'").get(0);
                    desc.copyToPlugin(boDestinationPlugin);
                    ISMTPOptions boSMTPOptions = (ISMTPOptions) boDestinationPlugin.getScheduleOptions();
                    @SuppressWarnings("unchecked")
                    List<String> listTo = (List<String>) boSMTPOptions.getToAddresses();
                    @SuppressWarnings("unchecked")
                    List<String> listCc = (List<String>) boSMTPOptions.getCCAddresses();
                    @SuppressWarnings("unchecked")
                    List<String> recipients = BaseUtils.addManyLists(listTo, listCc);
                    String addreses = BaseUtils.mergeWithSepEx(recipients, ", ");
                    return addreses;
                } catch (Exception e) {
                    return null;
                }
            }
        }
        return null;
    }

    private static String getDestinationPaths(ISchedulingInfo schedulingInfo, IInfoStore iStore) {
        if (schedulingInfo.getDestinations().size() > 0) {
            IDestination desc = (IDestination) schedulingInfo.getDestinations().get(0);
            String destinationName = desc.getName();
            if (destinationName.compareTo("CrystalEnterprise.DiskUnmanaged") == 0) {
                try {
                    IDestinationPlugin boDestinationPlugin = (IDestinationPlugin) iStore.query("SELECT top 1 * FROM CI_SYSTEMOBJECTS WHERE SI_NAME='CrystalEnterprise.DiskUnmanaged'").get(0);
                    desc.copyToPlugin(boDestinationPlugin);
                    IDiskUnmanagedOptions diskUnmanagedOptions = (IDiskUnmanagedOptions) boDestinationPlugin.getScheduleOptions();
                    @SuppressWarnings("unchecked")
                    List<String> destinationFiles = (List<String>) diskUnmanagedOptions.getDestinationFiles();
                    return BaseUtils.mergeWithSepEx(destinationFiles, ", ");
                } catch (Exception e) {
                    return null;
                }
            }
        }
        return null;
    }

    private static String getOwner(IProperties properties) {
        return BaseUtils.safeToString(properties.getProperty("SI_OWNER"));
    }

    private static Date getCreationTime(IProperties properties) {
        return properties.getDate("SI_CREATION_TIME");
    }

    private static Date getModifyTime(IProperties properties) {
        return properties.getDate("SI_UPDATE_TS");
    }

    private static Date getNextRunTime(IProperties properties) {
        return properties.getDate("SI_NEXTRUNTIME");
    }

    private static Date getExpireTime(ISchedulingInfo schedulingInfo) {
        return schedulingInfo.getEndDate();
    }

    private static int getType(ISchedulingInfo schedulingInfo) {
        return schedulingInfo.getType();
    }

    private static Date getStartDate(IProperties properties, ISchedulingInfo schedulingInfo) {
        Date startDate = properties.getDate("SI_STARTTIME");
        if (startDate == null) {
            startDate = getNextRunTime(properties);
        }
        return startDate;
    }

    private static Date getEndDate(IProperties properties) {
        return properties.getDate("SI_ENDTIME");
    }

    private static String getErrorMessage(ISchedulingInfo schedulingInfo) {
        return schedulingInfo.getErrorMessage();
    }

    private static String getCuid(IProperties properties) {
        return properties.getString("SI_CUID");
    }

    private static String getKind(IProperties properties) {
        return properties.getString("SI_KIND");
    }

    private static int getIsRecurring(IProperties properties) {
        return properties.getBoolean("SI_RECURRING") ? 1 : 0;
    }

    private static int getScheduleType(IProperties properties) {
        int type = properties.getInt("SI_SCHEDULE_STATUS");
//        ISchedulingInfo.ScheduleStatus.PAUSED
        return type;
    }

    private static String getSiMmachineUsed(ISchedulingInfo boSchedulingInfo) {
        IProperties properties = boSchedulingInfo.properties();
        if (properties == null) {
            return null;
        }
        return BaseUtils.safeToString(properties.getString("SI_MACHINE_USED"));
    }

    private static int getIntervalMinutes(ISchedulingInfo schedulingInfo) {
        return schedulingInfo.getIntervalMinutes();
    }

    private static int getIntervalHours(ISchedulingInfo schedulingInfo) {
        return schedulingInfo.getIntervalHours();
    }

    private static int getIntervalDays(ISchedulingInfo schedulingInfo) {
        return schedulingInfo.getIntervalDays();
    }

    private static int getIntervalMonths(ISchedulingInfo schedulingInfo) {
        return schedulingInfo.getIntervalMonths();
    }

    private static int getIntervalNthDay(ISchedulingInfo schedulingInfo) {
        return schedulingInfo.getIntervalNthDay();
    }

    private static String getFormat(IProperties processingProperties) {
        String format = null;
        try {
            format = processingProperties.getProperties("SI_FORMAT_INFO").getString("SI_FORMAT");
        } catch (Exception e) {
        }
        return format == null ? "Webi" : format;
    }

    private static String getParameters(IProperties processingProperties) {
        Set<String> parameters = new LinkedHashSet<String>();
        try {
            IProperties prompts = processingProperties.getProperties("SI_WEBI_PROMPTS");
            Iterator promptIterator = prompts.entrySet().iterator();
            while (promptIterator.hasNext()) {
                Map.Entry p = (Map.Entry) promptIterator.next();
                IProperty prop = (IProperty) p.getValue();
                if (!prop.isContainer()) {
                    continue;
                }
                IProperties vals = (IProperties) prop.getValue();//getProperties("SI_VALUES");
                Iterator valsIterator = vals.entrySet().iterator();
                while (valsIterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) valsIterator.next();
                    Integer id = (Integer) entry.getKey();
                    String propName = CePropertyID.idToName(id).toUpperCase();
                    if (!propName.equals("SI_VALUES")) {
                        continue;
                    }
                    IProperty val = (IProperty) entry.getValue();
                    IProperties values = (IProperties) val.getValue();
                    for (int j = 1; j < values.size(); j++) {
                        String value = values.getString(j);
                        parameters.add(value);

                    }
                }
            }
        } catch (Exception e) {
            return null;
        }
        return parameters.isEmpty() ? null : BaseUtils.mergeWithSepEx(parameters, ";");
    }

    // for tests
//    public static void main(String[] args) {
//        IEnterpriseSession enterpriseSession = null;
//        try {
//            ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
////            enterpriseSession = sessionMgr.logon("Administrator", "1qaz2wsx", "192.168.167.57", "secEnterprise");
//            enterpriseSession = sessionMgr.logon("BIKS_Admin", "xxx", "bosrv04", "secEnterprise");
//            IInfoStore iStore = (IInfoStore) enterpriseSession.getService("InfoStore");
//
//            Integer siID = null;
//            while (true) {
//                IInfoObjects results = iStore.query("select top 100 * from ci_infoobjects where si_kind = 'Publication'" + (siID != null ? " and si_id > " + siID : "") + " order by si_id");
//                for (int i = 0; i < results.size(); i++) {
//                    IInfoObject obj = (IInfoObject) results.get(i);
//                    SAPBOScheduleBean oneSchedule = getOneSchedule(obj, iStore);
//                    System.out.println("" + oneSchedule.toCSVString());
//                    siID = obj.getID();
//                }
//
//                if (results.size() < 100) {
//                    break;
//                }
//            }
////                SAPBOScheduleBean oneSchedule = getOneSchedule((IInfoObject) query.get(0), iStore);
////            System.out.println("oneSchedule.destinationPaths = " + oneSchedule.destinationPaths);
//        } catch (Exception e) {
//            System.out.println("Error: " + e.getMessage());
//        } finally {
//            if (enterpriseSession != null) {
//                enterpriseSession.logoff();
//            }
//        }
//    }
}
