/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.PushButton;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.grid.IBeanDisplay;
import simplelib.INamedTypedPropsBeanBroker;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tbzowka
 */
public class LisaBeanDisplay<T extends LisaDictColumnBean> implements IBeanDisplay<T> {

    public static final String ACT_EDIT = "act:edit";
    public static final String NAME_IN_DB = "nameInDb";
    public static final String TITLE_TO_DISPLAY = "titleToDisplay";
    public static final String DESCRIPTION = "description" /* i18n: no */;
    public static final String DATA_TYPE = "dataType";
    public static final String IS_MAIN_KEY = "isMainKey";
    public static final String ACCESS_LEVEL = "accessLevel";
    public static final String DICT_VALIDATION = "dictValidation";
    public static final String COL_VALIDATION = "colValidation";
    public static final String TEXT_DISPLAYED = "textDisplayed";

    protected final INamedTypedPropsBeanBroker<T> beanPropsBroker;
    protected IParametrizedContinuation<T> doEditCont;

    public LisaBeanDisplay(Class<T> beanClass,
            IParametrizedContinuation<T> doEditCont,
            ILisaBeansWrapperCreator beanCreator) {
        this.beanPropsBroker = beanCreator.getBroker(beanClass);
        this.doEditCont = doEditCont;
    }

    @Override
    public Object getHeader(String colName) {
        if (colName.equals(ACT_EDIT)) {
            return I18n.edycja.get() /* I18N:  */;
        } else if (colName.equals(NAME_IN_DB)) {
            return I18n.nazwaKolumny.get() /* I18N:  */;
        } else if (colName.equals(TITLE_TO_DISPLAY)) {
            return I18n.nazwaWyswietlana.get() /* I18N:  */;
        } else if (colName.equals(DESCRIPTION)) {
            return I18n.opisKolumny.get() /* I18N:  */;
        } else if (colName.equals(DATA_TYPE)) {
            return I18n.typDanych.get() /* I18N:  */;
        } else if (colName.equals(IS_MAIN_KEY)) {
            return I18n.kluczGlowny.get() /* I18N:  */;
        } else if (colName.equals(ACCESS_LEVEL)) {
            return I18n.poziomDostepu.get() /* i18n:  */;
        } else {
            return colName;
        }
    }

    @Override
    public Object getValue(final T bean, String colName) {
        if (colName.equals(ACT_EDIT)) {
            PushButton btn = new PushButton(I18n.zmien.get() /* I18N:  */, new ClickHandler() {
                        @Override
                        public void onClick(ClickEvent event) {
                            doEditCont.doIt(bean);
                        }
                    });
            NewLookUtils.makeCustomPushButton(btn);
            return btn;
        } else if (ACCESS_LEVEL.equals(colName)) {
            return ColumnAvailabilityText.getDescription(bean.accessLevel);
        } else if (colName.equals(IS_MAIN_KEY)) {
            Boolean isMainKey = bean.isMainKey;
            if (isMainKey != null && isMainKey == true) {
                return I18n.tak2.get() /* I18N:  */;
            } else {
                return I18n.nie2.get() /* I18N:  */;
            }
        }

        return beanPropsBroker.getPropValue(bean, colName);
    }
}
