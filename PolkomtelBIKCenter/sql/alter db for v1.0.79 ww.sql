exec sp_check_version '1.0.79';
go

------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------

IF OBJECT_ID (N'dbo.fn_bik_node_ancestorIds', N'TF') IS NOT NULL
    DROP FUNCTION dbo.fn_bik_node_ancestorIds;
GO

CREATE FUNCTION dbo.fn_bik_node_ancestorIds (@node_id int)
RETURNS @tab TABLE (node_id int not null, level int not null)
WITH EXECUTE AS CALLER
AS
BEGIN
  declare @level int = 0;

  while 1 = 1 begin
    declare @parent_node_id int
    select @parent_node_id = parent_node_id from bik_node where id = @node_id
    if @parent_node_id is null break
    else insert into @tab (node_id, level) values (@parent_node_id, @level)
    
    set @node_id = @parent_node_id;
    set @level = @level - 1;
  end

  update @tab set level = level - @level - 1  
     
  RETURN;
END;
GO

/*

select * from bik_node

-- 15|14|13|2|

select * from dbo.fn_bik_node_ancestorIds(2)
order by level

*/

create index idx_bik_node_is_deleted_parent_node_id on bik_node (parent_node_id, is_deleted)
go


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
exec sp_update_version '1.0.79', '1.0.80';
go
