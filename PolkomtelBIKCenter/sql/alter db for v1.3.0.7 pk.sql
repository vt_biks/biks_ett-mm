﻿exec sp_check_version '1.3.0.7';
go

create table bik_mona_dict_column(
	id int identity(1,1) not null primary key,
	dictionary_id varchar(6) not null,
	column_id int not null,
	sequence int not null,
	name_in_db varchar(30) not null,
	description varchar(255),
	title_to_display varchar(30) not null,
	is_active smallint
);
go

exec sp_update_version '1.3.0.7', '1.3.0.8';
go