/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.biadmin;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.AdminConfigBIKBaseWidget;
import pl.fovis.common.ConnectionParametersAuditBean;
import pl.fovis.common.i18npoc.I18n;
import static pl.fovis.foxygwtcommons.AdminConfigBaseWidget.createButton;
import static pl.fovis.foxygwtcommons.AdminConfigBaseWidget.createLabel;
import static pl.fovis.foxygwtcommons.AdminConfigBaseWidget.createLine;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class BIAuditConfigWidget extends AdminConfigBIKBaseWidget<ConnectionParametersAuditBean> {

//    protected TabPanel mainTb;
//    protected VerticalPanel mainVp = new VerticalPanel();
    protected CheckBox chbAuditIsActive;
    protected TextBox tbxAuditServer;
    protected TextBox tbxAuditInstance;
    protected TextBox tbxAuditUser;
    protected TextBox tbxAuditPassword;
//    protected CheckBox chbAuditPassword;
    protected TextBox tbxAuditDbName;
    protected PushButton btnSave;

    @Override
    protected void buildInnerWidget() {

        VerticalPanel auditMainVp = new VerticalPanel();
        auditMainVp.setSpacing(5);
        chbAuditIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbAuditIsActive.getValue());
            }
        }, false, auditMainVp);

        createLine(auditMainVp);
        createLabel(I18n.serwer.get(), auditMainVp);
        tbxAuditServer = createTextBox(auditMainVp);
        createLabel(I18n.instancja.get(), auditMainVp);
        tbxAuditInstance = createTextBox(auditMainVp);
        createLabel(I18n.uzytkownik.get(), auditMainVp);
        tbxAuditUser = createTextBox(auditMainVp);
        createLabel(I18n.haslo.get(), auditMainVp);
        tbxAuditPassword = createPasswordTextBox(auditMainVp);
//        chbAuditPassword = createCheckBox(I18n.ukryjHaslo.get(), new ValueChangeHandler<Boolean>() {
//            @Override
//            public void onValueChange(ValueChangeEvent<Boolean> event) {
//                tbxAuditPassword.getElement().setAttribute("type" /* I18N: no */, chbAuditPassword.getValue() ? "password" /* I18N: no */ : "text" /* I18N: no */);
//            }
//        }, true, auditMainVp);
        createLabel(I18n.bazaDanych.get(), auditMainVp);
        tbxAuditDbName = createTextBox(auditMainVp);

        createLine(auditMainVp);
        btnSave = createButton(I18n.zapisz.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ConnectionParametersAuditBean auditBean = new ConnectionParametersAuditBean();
                auditBean.isActive = chbAuditIsActive.getValue() ? 1 : 0;
                auditBean.server = tbxAuditServer.getText();
                auditBean.login = tbxAuditUser.getText();
                auditBean.password = tbxAuditPassword.getText();
                auditBean.instance = tbxAuditInstance.getText();
                auditBean.dbName = tbxAuditDbName.getText();
                BIKClientSingletons.getService().setBIAuditDatabaseConnectionParameters(auditBean, new StandardAsyncCallback<Void>("Error in setBIAdminConnectionParameters") {

                    @Override
                    public void onSuccess(Void result) {
                        BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get());
                    }
                });
            }
        }, auditMainVp);
        resultTest = new HTML();

        createTestConnectionWidget(I18n.testujPolaczenie.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().runBIAAuditTestConnection(new StandardAsyncCallback<Pair<Integer, String>>("Error in" /* I18N: no */ + " runProfileTestConnection") {
                            @Override
                            public void onSuccess(Pair<Integer, String> result) {
                                setResultTestConnection(result);
                            }
                        });
            }
        }, auditMainVp);
        main.add(auditMainVp);

    }

    @Override
    public void populateWidgetWithData(ConnectionParametersAuditBean auditBean) {
        tbxAuditServer.setText(auditBean.server);
        tbxAuditInstance.setText(auditBean.instance);
        tbxAuditUser.setText(auditBean.login);
        tbxAuditPassword.setText(auditBean.password);
        tbxAuditDbName.setText(auditBean.dbName);
        chbAuditIsActive.setValue(auditBean.isActive == 1);
        setWidgetsEnabled(chbAuditIsActive.getValue());
    }

}
