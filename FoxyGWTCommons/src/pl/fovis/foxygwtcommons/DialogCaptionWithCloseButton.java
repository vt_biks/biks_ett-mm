package pl.fovis.foxygwtcommons;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import simplelib.IContinuation;

public class DialogCaptionWithCloseButton extends DialogCaption {

    //private Button closeXBtn = new Button("[x]");
    private PushButton closeButton = new PushButton(new Image("images/close_icon.gif" /* I18N: no */), new Image("images/close_icon_hover.gif" /* I18N: no */));

    public DialogCaptionWithCloseButton(String caption, final IContinuation closeButtonCont) {
        this.captionHtml = new HTML(caption);
        fl.addStyleName("Caption" /* I18N: no */);
        fl.add(hp);
        hp.setWidth("100%");
        hp.add(this.captionHtml);
//        hp.setCellWidth(captionHtml, "100%");
        // closeXBtn
        //            closeButton.getUpHoveringFace().setImage(new Image("images/close_icon_hover.gif"));
        NewLookUtils.onHoverImagePushButton(closeButton, "images/close_icon_hover.gif" /* I18N: no */);
        closeButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                //Window.alert("aqq - co, zamknąć chciałeś, co? przyznaj się! ;-)");
                //cancelClicked();
                closeButtonCont.doIt();
            }
        });

        closeButton.setWidth("20px");
        HorizontalPanel hpCloseButton = new HorizontalPanel();
        hpCloseButton.setWidth("100%");
        hpCloseButton.add(closeButton);
        hpCloseButton.setCellHorizontalAlignment(closeButton, HasHorizontalAlignment.ALIGN_RIGHT);

        //            hp.add(closeXBtn);
        closeButton.addStyleName("zeroBtn");
        hp.add(hpCloseButton);
        hp.setCellWidth(this.captionHtml, "50%");
        hp.setCellWidth(hpCloseButton, "50%");
    }
}
