/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

import java.io.Serializable;

/**
 *
 * @author ctran
 */
public class ImportOverviewBean implements Serializable {

    public int added;
    public int pkDuplicated;
    public int deleted;
    public int updated;
    public int invalid;
    public int pkEmpty;
}
