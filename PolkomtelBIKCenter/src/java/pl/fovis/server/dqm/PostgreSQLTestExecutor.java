/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import commonlib.LameUtils;
import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.trzy0.foxy.commons.PostgresConnectionConfig;
import pl.trzy0.foxy.serverlogic.db.PostgresConnection;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class PostgreSQLTestExecutor extends AbstractDatabaseTestExecutor {

//    protected IBeanConnectionEx<Object> mc;
    private static final ILameLogger logger = LameUtils.getMyLogger();

    public PostgreSQLTestExecutor(IBIKSDQMSubservice service) {
        super(service);
    }

    @Override
    public boolean initConnection(DQMTestBean test) {
        this.test = test;
        return initConnectionInner(test.dbServerId, test.connectionId, useFunctionInsteadSQLQuery ? test.functionDatabase : test.queryDatabase);
    }

    protected boolean initConnectionInner(Integer dbServerId, Integer connectionId, String databaseName) {
        try {
            ConnectionParametersDBServerBean serverConfig = getDatabaseConfig(dbServerId, connectionId);
            this.mc = new PostgresConnection(new PostgresConnectionConfig(serverConfig.server, serverConfig.databaseName != null ? serverConfig.databaseName : databaseName, serverConfig.user, serverConfig.password, serverConfig.port));
            mc.setDbStatementExecutionWarnThresholdMillis(-1);
            mc.setAutoCommit(true);
            mc.execQrySingleValInteger("SELECT 1", false);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in PostgreSQL initConnection", e);
            }
            return false;
        }
        return true;
    }

//    @Override
//    public void closeConnection() {
//        mc.destroy();
//    }
    @Override
    protected String getCallTestFunctionQuery(String functionName) {
        functionName = functionName.endsWith(")") ? functionName : functionName + "()";
        return "select * from " + functionName;
    }

//    @Override
//    protected List<Map<String, Object>> runTestQuery(String sqlText) {
//        return mc.execQry(sqlText, IRawConnection.FieldNameConversion.ToLowerCase);
//    }
    @Override
    public boolean initConnection(DQMMultiSourceBean source) {
        this.source = source;
        return initConnectionInner(source.dbServerId, source.connectionId, source.databaseName);
    }

    @Override
    public String getSourceName() {
        return "PostgreSQL";
    }
}
