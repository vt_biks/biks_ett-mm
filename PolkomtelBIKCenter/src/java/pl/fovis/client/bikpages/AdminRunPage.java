/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import java.util.Set;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import pl.fovis.foxygwtcommons.dialogs.OneListBoxDialog;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.SingleValue;

/**
 *
 * @author tflorczak
 */
public class AdminRunPage extends BikPageMinimalBase {

    protected VerticalPanel main;
    protected HTML showAdv;
    protected PushButton runResetLoadThread;
    protected boolean isHiddenShowAdv = true;
    protected FlexTable grid;

    @Override
    protected Widget buildWidgets() {
        main = new VerticalPanel();

        main.setSpacing(20);
        showAdv = new HTML(I18n.pokazZaawansowane.get() /* I18N:  */);
        showAdv.setStyleName("runPageText");

        grid = new FlexTable();
        grid.setCellSpacing(10);

        BIKClientSingletons.getService().getAvailableConnectors(new StandardAsyncCallback<Set<String>>("Error in" /* I18N: no */ + " getAvailableConnectors") {
                    @Override
                    public void onSuccess(Set<String> result) {
                        createHeaders();
                        createRows(result);
                    }
                });
        main.setWidth("100%");
        main.add(grid);
        main.add(showAdv);
        showAdv.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (isHiddenShowAdv) {
                    showAdv.setHTML(I18n.ukryjZaawansowane.get() /* I18N:  */);
                } else {
                    showAdv.setHTML(I18n.pokazZaawansowane.get() /* I18N:  */);
                }
                runResetLoadThread.setVisible(isHiddenShowAdv);
                isHiddenShowAdv = !isHiddenShowAdv;
            }
        });
        runResetLoadThread = new PushButton(I18n.zresetujProcesZasilajacy.get() /* I18N:  */);
        NewLookUtils.makeCustomPushButton(runResetLoadThread);
        runResetLoadThread.setVisible(!isHiddenShowAdv);
        runResetLoadThread.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                new ConfirmDialog().buildAndShowDialog(I18n.czyNAPEWNOChceszZatrzymaProcesZa.get() /* I18N:  */ + "!", I18n.resetujProces.get() /* I18N:  */, new IContinuation() {
                            @Override
                            public void doIt() {
                                BIKClientSingletons.getService().resetPumpThread(new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " resetPumpThread") {
                                    @Override
                                    public void onSuccess(Void result) {
                                        BIKClientSingletons.showInfo(I18n.zresetowanoProcesZasilajacy.get() /* I18N:  */);
                                    }
                                });
                            }
                        });

            }
        });
        main.add(runResetLoadThread);
        return createMainScrollPanel(main, null);
    }

    @Override
    public String getId() {
        return "admin:load:runManual";
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        //no-op
    }

    protected void createHeaders() {
        grid.setWidget(0, 0, new HTML("<b>" + I18n.konektor.get() /* I18N:  */ + "</b>"));
        grid.setWidget(0, 1, new HTML("<b>" + I18n.uruchom.get() /* I18N:  */ + "</b>"));
    }

    protected void createRows(Set<String> result) {
        int row = 1;
        if (result.contains(PumpConstants.SOURCE_NAME_AD)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_AD));
            grid.setWidget(row++, 1, createRunButton("AD" /* I18N: no */, PumpConstants.SOURCE_NAME_AD, true, true, new IContinuation() {
                        @Override
                        public void doIt() {
                            runADPump();
                        }
                    }));
        }
        if (result.contains(PumpConstants.SOURCE_NAME_SAPBO)) {
            row = createBOPanel(row, PumpConstants.SOURCE_NAME_SAPBO);

        }
        if (result.contains(PumpConstants.SOURCE_NAME_SAPBO4)) {
            row = createBOPanel(row, PumpConstants.SOURCE_NAME_SAPBO4);
        }
        if (result.contains(PumpConstants.SOURCE_NAME_CONFLUENCE)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_CONFLUENCE));
            grid.setWidget(row++, 1, createRunButton("Confluence" /* i18n: no */, PumpConstants.SOURCE_NAME_CONFLUENCE, true, true, new IContinuation() {
                        @Override
                        public void doIt() {
                            runConfluencePump();
                        }
                    }));
        }
        if (result.contains(PumpConstants.SOURCE_NAME_DQC)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_DQC));
            grid.setWidget(row++, 1, createRunButton("DQC" /* I18N: no */, PumpConstants.SOURCE_NAME_DQC, true, true, new IContinuation() {
                        @Override
                        public void doIt() {
                            runDQCPump();
                        }
                    }));
        }
        if (result.contains(PumpConstants.SOURCE_NAME_MSSQL)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_MSSQL));
            grid.setWidget(row++, 1, createRunButton("MS SQL" /* I18N: no */, PumpConstants.SOURCE_NAME_MSSQL, true, false, new IContinuation() {
                        @Override
                        public void doIt() {
                            getAndSelectSystemInstance(PumpConstants.SOURCE_NAME_MSSQL, new IParametrizedContinuation<String>() {
                                @Override
                                public void doIt(String param) {
                                    runMSSQLPump(param);
                                }
                            });
                        }
                    }));
        }
        if (result.contains(PumpConstants.SOURCE_NAME_SAPBW)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_SAPBW));
            grid.setWidget(row++, 1, createRunButton("SAP BW" /* i18n: no */, PumpConstants.SOURCE_NAME_SAPBW, true, true, new IContinuation() {
                        @Override
                        public void doIt() {
                            runSAPBWPump();
                        }
                    }));
        }
        if (result.contains(PumpConstants.SOURCE_NAME_SAS)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_SAS));
            //todo
        }
        if (result.contains(PumpConstants.SOURCE_NAME_ORACLE)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_ORACLE));
            grid.setWidget(row++, 1, createRunButton("Oracle" /* I18N: no */, PumpConstants.SOURCE_NAME_ORACLE, true, false, new IContinuation() {
                        @Override
                        public void doIt() {
                            getAndSelectSystemInstance(PumpConstants.SOURCE_NAME_ORACLE, new IParametrizedContinuation<String>() {
                                @Override
                                public void doIt(String param) {
                                    runOraclePump(param);
                                }
                            });
                        }
                    }));
        }
        if (result.contains(PumpConstants.SOURCE_NAME_POSTGRESQL)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_POSTGRESQL));
            grid.setWidget(row++, 1, createRunButton("PostgreSQL" /* I18N: no */, PumpConstants.SOURCE_NAME_POSTGRESQL, true, false, new IContinuation() {
                        @Override
                        public void doIt() {
                            getAndSelectSystemInstance(PumpConstants.SOURCE_NAME_POSTGRESQL, new IParametrizedContinuation<String>() {
                                @Override
                                public void doIt(String param) {
                                    runPostgresPump(param);
                                }
                            });
                        }
                    }));
        }
        if (result.contains(PumpConstants.SOURCE_NAME_JDBC)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_JDBC));
            grid.setWidget(row++, 1, createRunButton("JDBC" /* I18N: no */, PumpConstants.SOURCE_NAME_JDBC, true, false, new IContinuation() {

                        @Override
                        public void doIt() {
                            getAndSelectSystemInstance(PumpConstants.SOURCE_NAME_JDBC, new IParametrizedContinuation<String>() {
                                @Override
                                public void doIt(String param) {
                                    runJdbcPump(param);
                                }
                            });
                        }
                    }));
        }
        if (result.contains(PumpConstants.SOURCE_NAME_FILE_SYSTEM)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_FILE_SYSTEM));
            grid.setWidget(row++, 1, createRunButton("File System" /* I18N: no */, PumpConstants.SOURCE_NAME_FILE_SYSTEM, true, false, new IContinuation() {

                        @Override
                        public void doIt() {
                            getAndSelectSystemInstance(PumpConstants.SOURCE_NAME_FILE_SYSTEM, new IParametrizedContinuation<String>() {
                                @Override
                                public void doIt(String param) {
                                    runFileSystemPump(param);
                                }
                            });
                        }
                    }));
        }
        if (result.contains(PumpConstants.SOURCE_NAME_PLAIN_FILE)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_PLAIN_FILE));
            grid.setWidget(row++, 1, createRunButton("Plain File" /* I18N: no */, PumpConstants.SOURCE_NAME_PLAIN_FILE, true, false, new IContinuation() {

                        @Override
                        public void doIt() {
                            getAndSelectSystemInstance(PumpConstants.SOURCE_NAME_PLAIN_FILE, new IParametrizedContinuation<String>() {
                                @Override
                                public void doIt(String param) {
                                    runPlainFilePump(param);
                                }
                            });
                        }
                    }));
        }
        if (result.contains(PumpConstants.SOURCE_NAME_DYNAMIC_AX)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_DYNAMIC_AX));
            grid.setWidget(row++, 1, createRunButton("Dynamic AX" /* I18N: no */, PumpConstants.SOURCE_NAME_DYNAMIC_AX, true, false, new IContinuation() {

                        @Override
                        public void doIt() {
                            getAndSelectSystemInstance(PumpConstants.SOURCE_NAME_DYNAMIC_AX, new IParametrizedContinuation<String>() {
                                @Override
                                public void doIt(String param) {
                                    runDynamicAxPump(param);
                                }

                            });
                        }
                    }));
        }

        if (result.contains(PumpConstants.SOURCE_NAME_PROFILE)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_PROFILE));
            grid.setWidget(row++, 1, createRunButton("Profile" /* I18N: no */, PumpConstants.SOURCE_NAME_PROFILE, true, true, new IContinuation() {
                        @Override
                        public void doIt() {
                            runProfilePump();
                        }
                    }));
        }
        if (result.contains(PumpConstants.SOURCE_NAME_TERADATA)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_TERADATA));
            grid.setWidget(row++, 1, createRunButton("Teradata" /* I18N: no */, PumpConstants.SOURCE_NAME_TERADATA, true, true, new IContinuation() {
                        @Override
                        public void doIt() {
                            runTeradataPump();
                        }
                    }));
        }
        if (result.contains(PumpConstants.SOURCE_NAME_TERADATA_DATAMODEL)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_TERADATA_DATAMODEL));
            grid.setWidget(row++, 1, createRunButton("Teradata Data Model" /* I18N: no */, PumpConstants.SOURCE_NAME_TERADATA_DATAMODEL, true, true, new IContinuation() {
                        @Override
                        public void doIt() {
                            runTeradataDataModelPump();
                        }
                    }));
        }
        if (result.contains(PumpConstants.SOURCE_NAME_ERWIN_DATAMODEL)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_ERWIN_DATAMODEL));
            grid.setWidget(row++, 1, createRunButton(PumpConstants.SOURCE_NAME_ERWIN_DATAMODEL, PumpConstants.SOURCE_NAME_ERWIN_DATAMODEL, true, true, new IContinuation() {
                @Override
                public void doIt() {
                    runErwinDataModelPump();
                }
            }));
        }
                
        if (result.contains(PumpConstants.SOURCE_NAME_BIKS_SQL)) {
            grid.setWidget(row, 0, new Label(PumpConstants.SOURCE_NAME_BIKS_SQL));
            grid.setWidget(row++, 1, createRunButton(PumpConstants.SOURCE_NAME_BIKS_SQL, PumpConstants.SOURCE_NAME_BIKS_SQL, true, false, new IContinuation() {
                @Override
                public void doIt() {
                    getAndSelectSystemInstance(PumpConstants.SOURCE_NAME_BIKS_SQL, new IParametrizedContinuation<String>() {
                        @Override
                        public void doIt(String param) {
                            runPermissionsPump(param);
                        }
                    });

                }
            }));
        }

        if (row > 1) { // przypadek gdy nie ma wcale konektorów
            grid.setWidget(row, 1, createRunButton(I18n.pelneZasilanie.get() /* I18N:  */, PumpConstants.SOURCE_NAME_SAPBO, true, true, new IContinuation() {
                        @Override
                        public void doIt() {
                            runAllPump();
                        }
                    }));
        }
    }

    public PushButton createRunButton(final String buttonName, final String sourceName, final boolean checkLastLog, final boolean askForSure, final IContinuation con) {
        PushButton btn = new PushButton(buttonName);
        NewLookUtils.makeCustomPushButton(btn);
        btn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (askForSure) {
                    new ConfirmDialog().buildAndShowDialog(I18n.czyNaPewnoRozpoczacZasilanieZ.get() /* I18N:  */ + ": " + buttonName + "?", I18n.uruchom.get() /* I18N:  */, new IContinuation() {
                                @Override
                                public void doIt() {
                                    checkLastSourceAndRun(checkLastLog, sourceName, con);
                                }
                            });
                } else {
                    checkLastSourceAndRun(checkLastLog, sourceName, con);
                }
            }
        });
        return btn;
    }

    protected void checkLastSourceAndRun(final boolean checkLastLog, final String sourceName, final IContinuation con) {
        if (checkLastLog) {
            BIKClientSingletons.getService().getLastSourceStatus(sourceName, new StandardAsyncCallback<Integer>("Error in" /* I18N: no */ + " getLastSourceStatus") {
                        @Override
                        public void onSuccess(Integer result) {
                            if (result != null && result == PumpConstants.LOG_LOAD_STATUS_STARTED) {
                                new ConfirmDialog().buildAndShowDialog(I18n.zasilaniJestJuzUruchomiLubNieWsz.get() /* I18N:  */ + ")", I18n.uruchomZasilanie.get() /* I18N:  */, new IContinuation() {
                                            @Override
                                            public void doIt() {
                                                con.doIt();
                                            }
                                        });
                            } else {
                                con.doIt();
                            }
                        }
                    });
        } else {
            con.doIt();
        }
    }

    public static void showStartNotification(String source) {
        BIKClientSingletons.showInfo(I18n.rozpoczetoZasilanie.get() /* I18N:  */ + ": " + source);
    }

    public void runTeradataPump() {
        BIKClientSingletons.getService().runTeradataPump(new PumpAsyncCallback(PumpConstants.SOURCE_NAME_TERADATA));
    }

    public void runTeradataDataModelPump() {
        BIKClientSingletons.getService().runTeradataDataModelPump(new PumpAsyncCallback(PumpConstants.SOURCE_NAME_TERADATA_DATAMODEL));
    }

    public void runErwinDataModelPump() {
        BIKClientSingletons.getService().runErwinDataModelPump(new PumpAsyncCallback(PumpConstants.SOURCE_NAME_ERWIN_DATAMODEL));
    }

    public void runOraclePump(String instanceName) {
        BIKClientSingletons.getService().runOraclePump(instanceName, new PumpAsyncCallback(PumpConstants.SOURCE_NAME_ORACLE));
    }

    public void runPostgresPump(String instanceName) {
        BIKClientSingletons.getService().runPostgresPump(instanceName, new PumpAsyncCallback(PumpConstants.SOURCE_NAME_POSTGRESQL));
    }

    public void runJdbcPump(String instanceName) {
        BIKClientSingletons.getService().runJdbcPump(instanceName, new PumpAsyncCallback(PumpConstants.SOURCE_NAME_JDBC));
    }

    public void runFileSystemPump(String instanceName) {
        BIKClientSingletons.getService().runFileSystemPump(instanceName, new PumpAsyncCallback(PumpConstants.SOURCE_NAME_FILE_SYSTEM));
    }

    public void runPlainFilePump(String instanceName) {
        BIKClientSingletons.getService().runPlainFilePump(instanceName, new PumpAsyncCallback(PumpConstants.SOURCE_NAME_PLAIN_FILE));
    }

    public void runDynamicAxPump(String instanceName) {
        BIKClientSingletons.getService().runDynamicAxPump(instanceName, new PumpAsyncCallback(PumpConstants.SOURCE_NAME_DYNAMIC_AX));
    }

    public void runProfilePump() {
        BIKClientSingletons.getService().runProfilePump(new PumpAsyncCallback(PumpConstants.SOURCE_NAME_PROFILE));
    }

    public void runDQCPump() {
        BIKClientSingletons.getService().runDQCPump(new PumpAsyncCallback(PumpConstants.SOURCE_NAME_DQC));
    }

    public void runMSSQLPump(String instanceName) {
        BIKClientSingletons.getService().runMSSQLPump(instanceName, new PumpAsyncCallback(PumpConstants.SOURCE_NAME_MSSQL));
    }

    public void runADPump() {
        BIKClientSingletons.getService().runADPump(new PumpAsyncCallback(PumpConstants.SOURCE_NAME_AD));
    }

    public void runSAPBWPump() {
        BIKClientSingletons.getService().runSAPBWPump(new PumpAsyncCallback(PumpConstants.SOURCE_NAME_SAPBW));
    }

    public void runConfluencePump() {
        BIKClientSingletons.getService().runConfluencePump(new PumpAsyncCallback(PumpConstants.SOURCE_NAME_CONFLUENCE));
    }

    public void runBOPump(String instanceName, String source) {
        BIKClientSingletons.getService().runSapBOPumpAll(instanceName, source, new PumpAsyncCallback(source));
    }

    public void runBOJavaPump(String instanceName, String source) {
        BIKClientSingletons.getService().runSapBOPump(instanceName, source, new PumpAsyncCallback(source));
    }

    public void runBODesignerPump(String instanceName, String source) {
        BIKClientSingletons.getService().runSapBODesignerPump(instanceName, source, new PumpAsyncCallback(source));
    }

    public void runSapBOIDTPump(String instanceName, String source) {
        BIKClientSingletons.getService().runSapBOIDTPump(instanceName, new PumpAsyncCallback(source));
    }

    public void runBOReportPump(String instanceName, String source) {
        BIKClientSingletons.getService().runSapBOReportPump(instanceName, source, new PumpAsyncCallback(source));
    }

    public void runPermissionsPump(String sqlName) {
        BIKClientSingletons.getService().runPermissionsPump(sqlName, new PumpAsyncCallback(PumpConstants.SOURCE_NAME_BIKS_SQL));
    }

    public void runAllPump() {
        BIKClientSingletons.getService().runAll(new PumpAsyncCallback(" 'All'"));
    }

    protected void checkResultsAndSelectInstance(String source, final List<String> result, final IParametrizedContinuation<String> con) {
        if (BaseUtils.isCollectionEmpty(result)) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.brakAktywnycInstancjNieMoznaUruc.get() /* I18N:  */ + "!", I18n.nieMoznaUruchomicZasilania.get() /* I18N:  */ + "!", "OK" /* I18N: no */);
            return;
        }
        if (result.size() == 1) {
            new ConfirmDialog().buildAndShowDialog(I18n.czyNaPewnoRozpoczacZasilanieZ.get() /* I18N:  */ + ": " + source + "?", I18n.uruchom.get() /* I18N:  */, new IContinuation() {

                        @Override
                        public void doIt() {
                            con.doIt(result.get(0));
                        }
                    });
        } else {
            new OneListBoxDialog().buildAndShowDialog(I18n.wybierzInstancje.get() /* I18N:  */, result, con);
        }
    }

    protected void getAndSelectSystemInstance(final String source, final IParametrizedContinuation<String> con) {
        BIKClientSingletons.getService().getActiveSystemInstance(source, new StandardAsyncCallback<List<String>>("error in" /* I18N: no:err-fail-in */ + " getActiveBO31Instance()") {
                    @Override
                    public void onSuccess(List<String> result) {
                        checkResultsAndSelectInstance(source, result, con);
                    }
                });
    }

    protected int createBOPanel(int row, final String source) {
        grid.setWidget(row, 0, new Label(source));
        VerticalPanel sapboPanel = new VerticalPanel();
        HorizontalPanel advPanel = new HorizontalPanel();
        final HorizontalPanel hiddenButtonsPanel = new HorizontalPanel();
        hiddenButtonsPanel.setVisible(false);
        final HTML showAdvBO = new HTML(I18n.pokazZaawansowane.get() /* I18N:  */);
        final SingleValue<Boolean> isHiddenShowAdvBO = new SingleValue<Boolean>(true);
        showAdvBO.setStyleName("runPageText");
        advPanel.add(createRunButton(source, source, true, false, new IContinuation() {
            @Override
            public void doIt() {
                getAndSelectSystemInstance(source, new IParametrizedContinuation<String>() {
                    @Override
                    public void doIt(String param) {
                        runBOPump(param, source);
                    }
                });
            }
        }));
        advPanel.add(hiddenButtonsPanel);
        Label pseudoMargin = new Label(" ");
        pseudoMargin.setWidth("10px");
        hiddenButtonsPanel.add(pseudoMargin);
        hiddenButtonsPanel.add(createRunButton("Java SDK" /* I18N: no */, source, true, false, new IContinuation() {
                    @Override
                    public void doIt() {
                        getAndSelectSystemInstance(source, new IParametrizedContinuation<String>() {
                            @Override
                            public void doIt(String param) {
                                runBOJavaPump(param, source);
                            }
                        });
                    }
                }));
        hiddenButtonsPanel.add(createRunButton("Designer SDK" /* I18N: no */, source, false, false, new IContinuation() {
                    @Override
                    public void doIt() {
                        getAndSelectSystemInstance(source, new IParametrizedContinuation<String>() {
                            @Override
                            public void doIt(String param) {
                                runBODesignerPump(param, source);
                            }
                        });
                    }
                }));
        if (source.equals(PumpConstants.SOURCE_NAME_SAPBO4)) {
            hiddenButtonsPanel.add(createRunButton("IDT SDK" /* I18N: no */, source, false, false, new IContinuation() {
                        @Override
                        public void doIt() {
                            getAndSelectSystemInstance(source, new IParametrizedContinuation<String>() {
                                @Override
                                public void doIt(String param) {
                                    runSapBOIDTPump(param, source);
                                }
                            });
                        }
                    }));
        }
        hiddenButtonsPanel.add(createRunButton("Report SDK" /* I18N: no */, source, false, false, new IContinuation() {
                    @Override
                    public void doIt() {
                        getAndSelectSystemInstance(source, new IParametrizedContinuation<String>() {
                            @Override
                            public void doIt(String param) {
                                runBOReportPump(param, source);
                            }
                        });
                    }
                }));
        sapboPanel.add(advPanel);
        sapboPanel.add(showAdvBO);
        showAdvBO.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (isHiddenShowAdvBO.v) {
                    showAdvBO.setHTML(I18n.ukryjZaawansowane.get() /* I18N:  */);
                } else {
                    showAdvBO.setHTML(I18n.pokazZaawansowane.get() /* I18N:  */);
                }
                hiddenButtonsPanel.setVisible(isHiddenShowAdvBO.v);
                isHiddenShowAdvBO.v = !isHiddenShowAdvBO.v;
            }
        });
        grid.setWidget(row++, 1, sapboPanel);
        return row;
    }

    static class PumpAsyncCallback extends StandardAsyncCallback<Void> {

        String source;

        @Override
        public void onSuccess(Void result) {
            showStartNotification(source);
        }

        PumpAsyncCallback(String source) {
            this.source = source;
        }
    }
}
