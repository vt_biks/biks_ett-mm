/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import edu.emory.mathcs.backport.java.util.Collections;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.SearchFullResult;
import pl.fovis.common.SearchFullResult.IndexCrawlStatus;
import pl.fovis.common.SearchResult;
import pl.fovis.common.SearchWizardHitResult;
import pl.fovis.common.SearchableAttrBean;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import pl.fovis.server.fts.ISimilarNodesWizard;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class BIKFullTextSearchViaMSSQL implements IBikFullTextSearch {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    protected IBOXIServiceForSearch service;
    protected INamedSqlsDAO<Object> adhocDao;

    public BIKFullTextSearchViaMSSQL(IBOXIServiceForSearch service, INamedSqlsDAO<Object> adhocDao) {
        this.service = service;
        this.adhocDao = adhocDao;
    }

//    @Override
//    public void setSearchWizard(ISimilarNodesWizard wizard) {
//        this.searchWizard = wizard;
//    }
//    public void addNode(int nodeId) {
//        updateNodeInternal(nodeId);
//    }
    protected Object updateAttrValLock = new Object();

    @Override
    public boolean updateNodeAttributes(int nodeId, Map<String, String> attrVals) {
        Map<String, Object> isNewNodeRow = adhocDao.execNamedQuerySingleRowCFN("isUnverticalizedNode", false, FieldNameConversion.ToLowerCase, nodeId);
        boolean res = (Integer) isNewNodeRow.get("is_new_node") != 0;

        for (Entry<String, String> e : attrVals.entrySet()) {
            String attrName = e.getKey();
            String attrVal = e.getValue();

            if (attrVal == null) {
                attrVal = "";
            }

            synchronized (updateAttrValLock) {
                adhocDao.execNamedCommand("insertOrUpdateAttrVal", nodeId, attrVal, attrName);
            }
        }

        return res;
    }

//    public void updateNode(int nodeId) {
//        updateNodeInternal(nodeId);
//    }
//    protected void updateNodeInternal(int nodeId) {
//        //...
//    }
    public void deleteTree(String treeCode) {
        // tak ma być
        throw new UnsupportedOperationException("UNDEFINED" /* I18N: no */);
    }

    public void updateTree(int treeId) {
        // no-op
    }

    public int unindexedPendingCount() {
        return 0;
    }

    protected Map<Integer, Integer> getSearchKindIdsAndCounts(String text,
            Collection<Integer> optTreeIds, Collection<Integer> optKindIdsToSearch,
            Collection<String> optLikePatts,
            Collection<ISearchByAttrValueCriteria> optSBAVCs,
            String opExpr, boolean useNodeIdsFromWizard) {
        Pair<List<ISearchByAttrValueCriteria>, String> tmp = duplicateAndAddAdditionalTxtPostfixToCriteria(optSBAVCs, new StringBuilder(opExpr));
        long start = System.currentTimeMillis();
        if (logger.isDebugEnabled()) {
            logger.debug("getSearchKindIdsAndCounts start");
        }
        List<Map<String, Object>> rows = adhocDao.execNamedQueryCFN("searchWithFullTextIndexKindsAndCounts",
                FieldNameConversion.ToLowerCase, text, optTreeIds,
                optKindIdsToSearch, optLikePatts, tmp.v1, tmp.v2, useNodeIdsFromWizard);
        if (logger.isDebugEnabled()) {
            logger.debug("Finish time:" + (System.currentTimeMillis() - start) / 1000.0);
        }
        Map<Integer, Integer> res = new LinkedHashMap<Integer, Integer>();
        for (Map<String, Object> row : rows) {
            res.put(adhocDao.sqlNumberToInt(row.get("node_kind_id")),
                    adhocDao.sqlNumberToInt(row.get("" + "cnt" /* I18N: no */)));
        }

        return res;
    }

    private Pair<List<ISearchByAttrValueCriteria>, String> duplicateAndAddAdditionalTxtPostfixToCriteria(Collection<ISearchByAttrValueCriteria> optSBAVCs, StringBuilder opExpr) {
        if (optSBAVCs == null || optSBAVCs.isEmpty()) {
            return new Pair<List<ISearchByAttrValueCriteria>, String>();
        }
        List<ISearchByAttrValueCriteria> ret = new ArrayList<ISearchByAttrValueCriteria>();
        Map<Integer, ISearchByAttrValueCriteria> map = new HashMap<Integer, ISearchByAttrValueCriteria>();
        for (ISearchByAttrValueCriteria criteria : optSBAVCs) {
            map.put(criteria.getIdInSelectedCriteriaList(), criteria);
        }
        boolean done = false;
        while (!done) {
            int id = getFirstIdWithPlaceHolder(optSBAVCs.size(), opExpr);
            if (id != -1) {
                String s = BaseUtils.createPlaceHolder(id, BIKConstants.OPEN_PLACEHOLDER, BIKConstants.CLOSE_PLACEHOLDER);
                int p = opExpr.indexOf(s);
                ISearchByAttrValueCriteria criteria = map.get(id);
                criteria.setTxtPrefix(BaseUtils.isStrEmptyOrWhiteSpace(opExpr.substring(0, p)) ? null : opExpr.substring(0, p));
                ret.add(criteria);
                opExpr.delete(0, p + s.length());
            } else {
                done = true;
            }
        };
        return new Pair<List<ISearchByAttrValueCriteria>, String>(ret, BaseUtils.isStrEmptyOrWhiteSpace(opExpr.toString()) ? null : opExpr.toString());
    }

    protected List<SearchResult> searchWithFullTextIndex(String text,
            int offset, int limit,
            Collection<Integer> optTreeIds,
            Collection<Integer> optKindIds,
            Collection<String> optLikePatts,
            Collection<ISearchByAttrValueCriteria> optSBAVCs,
            String opExpr, List<SearchWizardHitResult> searchWizardResultFull) {
        boolean useNodeIdsFromWizard = searchWizardResultFull != null;

        Pair<List<ISearchByAttrValueCriteria>, String> tmp = duplicateAndAddAdditionalTxtPostfixToCriteria(optSBAVCs, new StringBuilder(opExpr));
        long start = System.currentTimeMillis();
        if (logger.isDebugEnabled()) {
            logger.debug("searchWithFullTextIndex start");
        }
        List<SearchResult> res = adhocDao.createBeansFromNamedQry("searchWithFullTextIndex", SearchResult.class,
                text, offset + limit, optTreeIds, optKindIds,
                service.getAppPropValueEx("search_order_by_exprs", true,
                        "sr.max_search_weight desc, bn.search_rank + bnk.search_rank desc, bn.vote_sum desc, sum_combined_rank desc" /* I18N: no:sql */),
                optLikePatts, tmp.v1, tmp.v2, useNodeIdsFromWizard);

        if (logger.isDebugEnabled()) {
            logger.debug("Finish time: " + (System.currentTimeMillis() - start) / 1000.0);
        }
        int afterLastIdx = Math.min(offset + limit, res.size());
        if (offset > afterLastIdx) {
            offset = afterLastIdx;
        }

        res = new ArrayList<SearchResult>(res.subList(offset, afterLastIdx));

        Map<Integer, SearchResult> nodeIdsMap = new HashMap<Integer, SearchResult>();
        for (SearchResult sr : res) {
            nodeIdsMap.put(sr.nodeId, sr);
            sr.foundInAttrCaptions = new ArrayList<String>();
        }

        if (!nodeIdsMap.isEmpty()) {

            if (useNodeIdsFromWizard) {

                Map<Integer, SearchWizardHitResult> hitsMap = new HashMap<Integer, SearchWizardHitResult>();

                for (SearchWizardHitResult swhr : searchWizardResultFull) {
                    if (nodeIdsMap.containsKey(swhr.nodeId)) {
                        hitsMap.put(swhr.nodeId, swhr);
                    }
                }

                for (SearchResult sr : res) {
                    SearchWizardHitResult swhr = hitsMap.get(sr.nodeId);
                    if (swhr == null || swhr.occurenceFields == null) {
                        continue;
                    }
                    sr.foundInAttrCaptions.addAll(swhr.occurenceCaptions);
                }

            } else {
                List<Map<String, Object>> foundInAttrNamesRows = adhocDao.execNamedQueryCFN(
                        "getFoundInAttrCaptionsRowsFTS", FieldNameConversion.ToLowerCase,
                        text, nodeIdsMap.keySet(), optLikePatts);

                for (Map<String, Object> row : foundInAttrNamesRows) {
                    int nodeId = adhocDao.sqlNumberToInt(row.get("node_id"));
                    String attrCaption = (String) row.get("attr_caption");
                    SearchResult sr = nodeIdsMap.get(nodeId);
                    sr.foundInAttrCaptions.add(attrCaption);
                }
            }
        }

        return res;
    }

    @SuppressWarnings("unchecked")
    protected void saveWizardResult(List<SearchWizardHitResult> searchWizardResultFull) {

        if (logger.isDebugEnabled()) {
            logger.debug("saveWizardResult: results=" + searchWizardResultFull.size());
        }

        Set<String> attrNames = new HashSet<String>();

        Map<Integer, SearchWizardHitResult> hitsMap = new HashMap<Integer, SearchWizardHitResult>();

        for (SearchWizardHitResult swhr : searchWizardResultFull) {
            if (swhr.occurenceFields != null) {
                attrNames.addAll(swhr.occurenceFields);
            }
            hitsMap.put(swhr.nodeId, swhr);
        }

        List<SearchableAttrBean> searchableAttrs
                = attrNames.isEmpty() ? Collections.<SearchableAttrBean>emptyList()
                        : adhocDao.createBeansFromNamedQry("getSearchableAttrsWithNames", SearchableAttrBean.class, attrNames);

        final Map<String, SearchableAttrBean> searchableAttrsByName = new HashMap<String, SearchableAttrBean>();
        for (SearchableAttrBean sab : searchableAttrs) {
            searchableAttrsByName.put(sab.name, sab);
        }

        for (SearchWizardHitResult swhr : searchWizardResultFull) {
            if (swhr.occurenceFields == null) {
                continue;
            }
            List<String> names = new ArrayList<String>(swhr.occurenceFields);
            Collections.sort(names, new Comparator<String>() {

                @Override
                public int compare(String o1, String o2) {
                    SearchableAttrBean sab1 = searchableAttrsByName.get(o1);
                    SearchableAttrBean sab2 = searchableAttrsByName.get(o2);
                    return (sab2 == null ? 0 : sab2.searchWeight) - (sab1 == null ? 0 : sab1.searchWeight);
                }
            });

            if (!names.isEmpty()) {
                SearchableAttrBean sab1 = searchableAttrsByName.get(names.get(0));
                if (sab1 != null) {
                    swhr.bestAttrWeight = sab1.searchWeight;
                }
            }

            for (int i = 0; i < names.size(); i++) {
                String name = names.get(i);
                SearchableAttrBean sab = searchableAttrsByName.get(name);
                if (sab != null) {
                    names.set(i, sab.caption);
                }
            }

            swhr.occurenceCaptions = names;
        }

        int maxValsInSingleInsert = 1000;
        int maxSqlTextLength = 1000000;
        StringBuilder sb = new StringBuilder("if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#node_ids_from_wizard'))\n"
                + "  drop table #node_ids_from_wizard;\n"
                + "create table #node_ids_from_wizard (id int not null primary key, score float not null, best_attr_weight int not null, unique (id, score, best_attr_weight), unique (best_attr_weight, score, id));");

        int currValsInInsert = 0;
        for (SearchWizardHitResult swhr : searchWizardResultFull) {
            String valStr = "(" + swhr.nodeId + "," + swhr.score + "," + swhr.bestAttrWeight + ")";
            if (currValsInInsert != 0 && sb.length() + 1 + valStr.length() > maxSqlTextLength) {
                if (logger.isDebugEnabled()) {
                    logger.debug("saveWizardResult: command to run:\n" + sb.toString());
                }
                adhocDao.execNamedCommand("execAnySql", sb.toString());
                sb.setLength(0);
                currValsInInsert = 0;
            }
            if (currValsInInsert == 0 || currValsInInsert >= maxValsInSingleInsert) {
                sb.append("\ninsert into #node_ids_from_wizard (id, score, best_attr_weight) values ");
                currValsInInsert = 0;
            } else {
                sb.append(",");
            }
            sb.append(valStr);
            currValsInInsert++;
        }

        if (sb.length() > 0) {
            if (logger.isDebugEnabled()) {
                logger.debug("saveWizardResult: last command to run:\n" + sb.toString());
            }
            adhocDao.execNamedCommand("execAnySql", sb.toString());
        }
    }

    @Override
    public SearchFullResult search(String text, int offset,
            int limit, Collection<Integer> optTreeIds,
            Collection<Integer> optKindIdsToSearch, Collection<Integer> optKindIdsToExclude,
            boolean calcKindCounts, Collection<ISearchByAttrValueCriteria> optSBAVCs, String opExpr,
            boolean useLucene) {

        if (!service.hasFulltextCatalog()) {
            useLucene = true;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("search: starting for text=" + text);
        }

        IndexCrawlStatus ics = service.getFullTextCrawlCompleteStatus(useLucene);
        boolean canPerformSearch = ics != IndexCrawlStatus.NoIndex;

        SearchFullResult res = new SearchFullResult();
        res.indexCrawlStatus = ics;

        res.searchedText = text;

        Pair<List<String>, List<String>> p = BIKCenterUtils.normalizeTextForFTSAndOptLike(text);

        String txtForContains;

        if (BaseUtils.isCollectionEmpty(p.v1) || text.equals("*")) {
            txtForContains = null;
        } else {
            txtForContains = "\"" + BaseUtils.mergeWithSepEx(p.v1, "\" " + "AND" /* I18N: no */ + " \"") + "\"";
        }

        //if (logger.isDebugEnabled()) logger.debug("txtForContains=" + txtForContains);
        Set<Integer> optKindIds;

        boolean useNodeIdsFromWizard = false;

        List<SearchWizardHitResult> searchWizardResultFull = null;

        if (useLucene && getSearchWizard() != null && txtForContains != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("search: search via wizard");
            }

            searchWizardResultFull = getSearchWizard().findNodeIds(BaseUtils.mergeWithSepEx(p.v1, " "), 0f, -1, null, false);
//            Map<Integer, Float> wizardResult = LuceneLameUtils.convertSearchHitsToMap(searchWizardResultFull);
            if (searchWizardResultFull == null) {
                canPerformSearch = false;
                res.indexCrawlStatus = IndexCrawlStatus.NoIndex;
            } else {
                saveWizardResult(searchWizardResultFull);
            }
            useNodeIdsFromWizard = true;
            txtForContains = null;
        }

        if (/*
                 * true ||
                 */calcKindCounts && canPerformSearch) {
            if (logger.isDebugEnabled()) {
                logger.debug("search: before getSearchKindIdsAndCounts");
            }
            res.kindIdsAndCounts = getSearchKindIdsAndCounts(txtForContains, optTreeIds, optKindIdsToSearch,
                    p.v2, optSBAVCs, opExpr, useNodeIdsFromWizard);
//            res.kindIdsAndCounts = new HashMap<Integer, Integer>();
            if (logger.isDebugEnabled()) {
                logger.debug("search: after getSearchKindIdsAndCounts");
            }

            optKindIds = new LinkedHashSet<Integer>(res.kindIdsAndCounts.keySet());
        } else {
            optKindIds = optKindIdsToSearch == null ? null : new LinkedHashSet<Integer>(optKindIdsToSearch);
        }

        //ww: testy przerywania (cancel)
//        for (int sleepIdx = 0; sleepIdx < 10; sleepIdx++) {
//            if (service.isDBJobCanceled()) {
//                if (logger.isDebugEnabled()) logger.debug("JOB IS CANCELED!");
//                break;
//            }
//            LameUtils.threadSleep(2000);
//            if (service.isDBJobCanceled()) {
//                if (logger.isDebugEnabled()) logger.debug("JOB IS CANCELED 2!");
//            }
//        }
        if (optKindIdsToExclude != null) {
            optKindIds.removeAll(optKindIdsToExclude);
        }

        if (canPerformSearch) {
            if (logger.isDebugEnabled()) {
                logger.debug("search: before searchWithFullTextIndex");
            }
            res.foundItems = searchWithFullTextIndex(txtForContains, offset, limit, optTreeIds, optKindIds,
                    p.v2, optSBAVCs, opExpr, searchWizardResultFull);
//            res.foundItems = new ArrayList<SearchResult>();
            if (logger.isDebugEnabled()) {
                logger.debug("search: after searchWithFullTextIndex");
            }

            if (!res.foundItems.isEmpty()) {
                Set<Integer> allNodeIds = new HashSet<Integer>();

                for (SearchResult sr : res.foundItems) {
                    //if (logger.isDebugEnabled()) logger.debug("»»»»» node name=" + sr.name + ", branchIds=" + sr.branchIds);
                    BIKCenterUtils.splitBranchIdsIntoColl(sr.branchIds, true, allNodeIds);
                }

                if (allNodeIds.isEmpty()) {
                    res.ancestorNames = new HashMap<Integer, String>();
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("search: before service.getNodeNamesByIds");
                    }
                    res.ancestorNames = service.getNodeNamesByIds(allNodeIds);
                    if (logger.isDebugEnabled()) {
                        logger.debug("search: after service.getNodeNamesByIds");
                    }
                }

            }
        }

        //res.searchedText = text;
        res.searchedTreeIds = optTreeIds;
        res.searchedKindIds = optKindIdsToSearch;
        res.excludedKindIds = optKindIdsToExclude;
        res.calcKindCounts = calcKindCounts;
        res.sbavcs = optSBAVCs;

        if (logger.isDebugEnabled()) {
            logger.debug("search: done for text=" + text);
        }

        return res;
    }

    public void deleteNodes(Collection<Integer> nodeIds) {
//        service.addOrUpdateSpidHistoryForCurrentUser();
        adhocDao.execNamedCommand("updateSearchableAttrValsForNodes", nodeIds);
    }

    private int getFirstIdWithPlaceHolder(int maxId, StringBuilder opExpr) {
        int minP = opExpr.length();
        int id = -1;
        for (int i = 1; i <= maxId; i++) {
            String s = BaseUtils.createPlaceHolder(i, BIKConstants.OPEN_PLACEHOLDER, BIKConstants.CLOSE_PLACEHOLDER);
            int p = opExpr.indexOf(s);
            if (p >= 0 && p < minP) {
                minP = p;
                id = i;
            }
        }
        return id;
    }

    protected ISimilarNodesWizard getSearchWizard() {
        return service.getSearchWizard();
    }
}
