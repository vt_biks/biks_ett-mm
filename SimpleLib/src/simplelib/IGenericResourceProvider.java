/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author wezyr
 */
public interface IGenericResourceProvider<T> {

    public T gainResource(String resourceName);

    public long getLastModifiedOfResource(String resourceName);
}
