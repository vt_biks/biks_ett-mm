/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.IOutboundUrlRewriter;
import java.util.Map;
import pl.foxys.foxymill.IRenderable;
import pl.trzy0.foxy.commons.templates.INamedTemplateProvider;
import simplelib.MapHelper;

/**
 *
 * @author wezyr
 */
public interface IApplicationEnvironment {
    //public IFoxyMillResourcesModel getResourcesModel(String langCode);

    public MapHelper<String> getSysProps();

    public Map<String, String> getTextProps(String langCode);

    public IRenderable getPage(String name);

    public Object getGlobal(String name);

    public INamedTemplateProvider getTemplateProvider(String langCode);

    public String getCanonicalAppUrlBase();

    public String getCanonicalResourceUrlBase();

    public String getCanonicalSiteUrl();

    public IOutboundUrlRewriter getOptOutboundUrlRewriter();

    public boolean getUsesSubdomains();
}
