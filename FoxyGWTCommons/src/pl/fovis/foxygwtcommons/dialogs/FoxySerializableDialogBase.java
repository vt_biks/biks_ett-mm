/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import com.google.gwt.user.client.ui.ListBox;
import java.io.Serializable;

/**
 *
 * @author pmielanczuk
 */
public abstract class FoxySerializableDialogBase<INB extends Serializable, OUTB extends Serializable> extends BaseActionOrCancelDialog {

    static {
        USE_ON_DIALOG_NOTIFICATIONS = true;
    }
    protected FoxyDialogSerializer<Serializable, INB, OUTB> serializer;

    protected void setSerializer(FoxyDialogSerializer<Serializable, INB, OUTB> serializer) {
        this.serializer = serializer;
    }

    protected void fillControlsFromBean(INB inputBean) {
        serializer.fillControlsFromBean(inputBean);
    }

    protected void fillBeanFromControls(OUTB outputBean) {
        serializer.fillBeanFromControls(outputBean);
    }

    protected <B extends Serializable> void fillListBox(ListBox lb, Iterable<B> beans, String idProp, String nameProp, String selectedValue) {
        serializer.fillListBox(lb, beans, idProp, nameProp, selectedValue);
    }
}
