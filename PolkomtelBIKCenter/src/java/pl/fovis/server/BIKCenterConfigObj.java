/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import javax.servlet.ServletContext;
import pl.trzy0.foxy.commons.MailServerConfig;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import pl.trzy0.foxy.servlet.ServletConfigReader;

/**
 *
 * @author wezyr
 */
public class BIKCenterConfigObj /*extends ServletConfigObj*/ {

    public String dirForUpload;
    public String urlForPublicAccess;
    public MssqlConnectionConfig connConfig;
    // to nie jest aktualnie w praktyce używane, w pliku cfg nie musi to
    // występować, ale w przyszłości może będziemy używać wysyłania maili
    public MailServerConfig mailServerConfig;
    // to też nie jest (i raczej nie będzie już) używane
    public String dirForLucene;
    public String dirForIcons;
    public String indexTimeInterval;

    public static BIKCenterConfigObj readConfigObj(ServletContext servletContext) {
        return ServletConfigReader.readConfigObj(servletContext, BIKCenterConfigObj.class,
                "bikcenter-config");
    }

    @Override
    public String toString() {
        return "BIKCenterConfigObj{" + "dirForUpload=" + dirForUpload + ", urlForPublicAccess=" + urlForPublicAccess + ", connConfig=" + connConfig + ", mailServerConfig=" + mailServerConfig + ", dirForLucene=" + dirForLucene + '}';
    }
}
