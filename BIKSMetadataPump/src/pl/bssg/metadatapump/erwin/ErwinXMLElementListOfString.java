/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.erwin;

import java.util.ArrayList;

/**
 *
 * @author pgajda
 */
public class ErwinXMLElementListOfString extends ErwinXMLElement<ArrayList<String>> {

    public void setPropertieValue(String key, String value) {
        ArrayList<String> list = propertiesMap.get(key);
        if(list==null){
            list = new ArrayList<String>();
            propertiesMap.put(key, list);
        }
        list.add(value);
    }
        
}
