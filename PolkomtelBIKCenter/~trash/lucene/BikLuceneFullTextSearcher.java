/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.lucene;

import commonlib.LameUtils;
import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
//import org.apache.lucene.analysis.pl;
import pl.fovis.common.SearchResult;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.QueryParser.Operator;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import pl.fovis.server.lucene.NodeIndexingTaskData.TaskKind;
import pl.fovis.common.SearchFullResult;
import pl.fovis.server.IBOXIServiceForSearch;
import pl.fovis.server.IBikFullTextSearch;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.logging.ILameLogger;

/**
 *
 * @author AMamcarz
 */
public class BikLuceneFullTextSearcher extends Thread implements IBikFullTextSearch {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    @SuppressWarnings("deprecation")
    public static final Version LUCENE_VER = Version.LUCENE_30;
    public static final int NEXT_DOC_PACK_MILLIS = 10000;
    //private String dirForLucene;
//    private PolishAnalyzer analyzer = new PolishAnalyzer(Version.LUCENE_CURRENT);
    //private boolean doneIndexing;
    private IBOXIServiceForSearch service;
    private BikLuceneSearcherHelperThread sht;
    private StandardAnalyzer analyzer = new StandardAnalyzer(LUCENE_VER);
    private File luceneDirFile;

    public BikLuceneFullTextSearcher(IBOXIServiceForSearch service, String dirForLucene) {
        this.service = service;
        //this.dirForLucene = dirForLucene;
        this.luceneDirFile = new java.io.File(dirForLucene);
        //this.service = service;
        //this.indexInit = service.getIndexVer();

        this.sht = new BikLuceneSearcherHelperThread(service, analyzer, luceneDirFile, service.getIndexVer());
        sht.resetIndex();
        sht.start();

        reindexIfNeeded();
    }

//    public void indexStart() {
//        start();
//    }
//    protected boolean buildIndex() {
//        if (logger.isDebugEnabled()) logger.debug("in buildIndex");
//        List<NodeIndexingTaskData> newsDok = service.getBikDocumentResults(INDEX_VER);
//
//        boolean hasDocs = !newsDok.isEmpty();
//
//        if (hasDocs) {
//            try {
//                if (logger.isDebugEnabled()) logger.debug("buildIndex: hasDocs");
//                //addToIndex(newsDok, mustCreateNewIndex());
//                unindexedPending.addAll(newsDok);
//            } catch (Exception ex) {
//                if (logger.isDebugEnabled()) logger.debug("Failed in start indexing");
//            }
//        }
//
//        if (logger.isDebugEnabled()) logger.debug("finished buildIndex");
//        return hasDocs;
//    }
    protected void addOrUpdateNode(int nodeId, boolean update) {
        if (logger.isDebugEnabled()) {
            logger.debug("FTS: addOrUpdateNode: nodeId=" + nodeId + ", update=" + update);
        }

        NodeIndexingTaskData nodeDR = service.getBikDocumentResult(nodeId);

        if (nodeDR != null) {
            nodeDR.indexVer = update ? -1 : 0;
            addNodeForIndexing(nodeDR);
        } else {
            deleteNodeInternal(nodeId);
        }
    }

    public void addNode(final int nodeId) {

        addAfterCommitTaskForCurrentThread(new IContinuation() {

            public void doIt() {
                //fts.addNode(id);
                addOrUpdateNode(nodeId, false);

            }
        }, "fts addNode id=: " + nodeId);

//        try {
//            NodeIndexingTaskData nodeDR = service.getBikDocumentResult(nodeId);
//
//            if (nodeDR != null) {
//                addToIndex(Collections.singletonList(nodeDR), false);
//            }
//        } catch (Exception ex) {
//            if (logger.isDebugEnabled()) logger.debug("Failed in add new object to index");
//        }
    }

    public void updateNode(final int nodeId) {

        addAfterCommitTaskForCurrentThread(new IContinuation() {

            public void doIt() {
                addOrUpdateNode(nodeId, true);
            }
        }, "fts updateNode id=" + nodeId);

//        addOrUpdateNode(nodeId, true);

        //service.setIndexVerIntoBikNode(Collections.singleton(nodeId), -1);
//        try {
//            deleteTree(nodeId);
//            addNode(nodeId);
//        } catch (Exception ex) {
//            if (logger.isDebugEnabled()) logger.debug("Failed in update object to index");
//        }
    }

    protected List<SearchResult> searchInternal(String text, int limit) throws Exception {
        List<SearchResult> found = new LinkedList<SearchResult>();

        if (BaseUtils.isStrEmptyOrWhiteSpace(text)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Brak prawidłowego zapytania");
            }
            return found;
        }

        IndexReader reader = null;
        IndexSearcher searcher = null;
        TopScoreDocCollector collector = null;
        MultiFieldQueryParser parser = null;
        Query query = null;
        ScoreDoc[] hits = null;
        try {
            if (!luceneDirFile.canRead()) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Nie można czytać jeszcze z pliku");
                }
                return found;
            }
            reader = IndexReader.open(FSDirectory.open(luceneDirFile), true);
            searcher = new IndexSearcher(reader);
            //create topscore document collector
            collector = TopScoreDocCollector.create(limit, false);
            String[] fields = {"name", "description"};

            //tutaj cos próbuje

//            Map<String, Float> boost = new HashMap<String, Float>();
//            boost.put("name", new Float(2));
//            boost.put("description", new Float(1));
//            boost.put("keyWords", 0.4f);

            //koniec próby

            parser = new MultiFieldQueryParser(LUCENE_VER, fields, analyzer/*, boost*/);
            parser.setAllowLeadingWildcard(true);
            parser.setDefaultOperator(Operator.OR);
            query = parser.parse(text);
            //tutaj znowu dopisuje
            BooleanQuery booleanQuery = new BooleanQuery();
            BooleanQuery.setMaxClauseCount(limit);
            booleanQuery.add(query, BooleanClause.Occur.SHOULD);


            //koniec

//            searcher.search(query, collector);
            searcher.search(booleanQuery, collector);
            hits = collector.topDocs().scoreDocs;
            if (hits.length > 0) {

                for (int i = 0; i < hits.length; i++) {
                    int scoreId = hits[i].doc;
                    Document document = searcher.doc(scoreId);
//                    if (logger.isDebugEnabled()) {
//                        logger.debug("boost wyniósł: " + document.getField("name").getBoost()
//                                + " description: " + Similarity.encodeNorm(document.getField("description").getBoost());
//                                + " keyWords: " + document.getField("keyWords").getBoost());
//                    }
                    System.out.println("boost wyniósł: " + document.getBoost()//getField("name").getBoost()
                            //                                + " description: " + document.getField("description").getBoost()
                            + " ->" + searcher.doc(scoreId).getBoost());
                    SearchResult sr = new SearchResult(document.getField("nodeId").stringValue(), document.getField("name").stringValue(),
                            document.getField("nodeKindCode").stringValue(),
                            document.getField("nodeKindCaption").stringValue(), document.getField("treeName").stringValue(), document.getField("treeCode").stringValue());
                    found.add(sr);
                }
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Żadnych trafień");
                }
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return found;
    }

    public SearchFullResult search(String text, int offset,
            int limit, Collection<Integer> optTreeIds,
            Collection<Integer> optKindIdsToSearch, Collection<Integer> optKindIdsToExclude,
            boolean calcKindCounts) {
        try {
            //return searchInternal(text, limit);
            SearchFullResult sfr = new SearchFullResult();
            sfr.foundItems = searchInternal(text, limit);
            sfr.kindIdsAndCounts = new HashMap<Integer, Integer>();
            return sfr;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void updateTree(final int treeId) {
        addAfterCommitTaskForCurrentThread(new IContinuation() {

            public void doIt() {
                reindexIfNeeded();
            }
        }, "fts reindexInNeeded for updateBikTree: " + treeId);
    }

    protected void reindexIfNeeded() {
        sht.addToUnindexedPending(service.getBikDocumentResults(BikLuceneSearcherHelperThread.INDEX_VER));

//        List<NodeIndexingTaskData> newDocs = service.getBikDocumentResults(BikLuceneSearcherHelperThread.INDEX_VER);
//
//        boolean hasDocs = !newDocs.isEmpty();
//
//        if (hasDocs) {
//            try {
//                //if (logger.isDebugEnabled()) logger.debug("buildIndex: hasDocs");
//                //addToIndex(newsDok, mustCreateNewIndex());
//                //unindexedPending.addAll(newsDok);
//                sht.addToUnindexedPending(newDocs);
//            } catch (Exception ex) {
//                throw new RuntimeException(ex);
//            }
//        }
    }

    public int unindexedPendingCount() {
        return sht.unindexedPendingCount();
    }

    protected void addNodeForIndexing(NodeIndexingTaskData dr) {
        sht.addToUnindexedPending(Collections.singletonList(dr));
    }

    protected void deleteNodeInternal(int nodeId) {
        NodeIndexingTaskData nodeDR = new NodeIndexingTaskData();
        nodeDR.nodeId = nodeId;
        nodeDR.isDeleted = true;
        addNodeForIndexing(nodeDR);
    }

    public void deleteTree(final String treeCode) {
        addAfterCommitTaskForCurrentThread(new IContinuation() {

            public void doIt() {
                NodeIndexingTaskData nodeDR = new NodeIndexingTaskData();
                nodeDR.kind = TaskKind.DeleteTree;
                nodeDR.treeCode = treeCode;
                addNodeForIndexing(nodeDR);
            }
        }, "fts deleteTree code=" + treeCode);
    }

    public void addAfterCommitTaskForCurrentThread(IContinuation taskCont, String descr) {
        service.addAfterCommitTaskForCurrentThreadX(taskCont, descr);
    }

    public void deleteNodes(final Iterable<Integer> nodeIds) {
        addAfterCommitTaskForCurrentThread(new IContinuation() {

            public void doIt() {
                for (int nodeId : nodeIds) {
                    deleteNodeInternal(nodeId);
                }
            }
        }, "fts: deleteNodes");
    }

    public void updateNodeAttributes(int nodeId, Map<String, String> attrVals) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
