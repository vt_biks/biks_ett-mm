/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.TreeSelectorForLinkedNodesDialog;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class NodeActionLinkNode extends NodeActionUpdatingBase {

    @Override
    public String getCaption() {
        return I18n.dodajDowiazanie.get() /* I18N:  */;
    }

    @Override
    public void execute() {
        Integer treeId = null;
        Integer nodeId = null;
        if (node != null) {
            nodeId = node.id;
            treeId = node.treeId;
        }
        new TreeSelectorForLinkedNodesDialog().buildAndShowDialog(treeId, nodeId,node.nodeKindId, new IParametrizedContinuation<TreeNodeBean>() {

            public void doIt(TreeNodeBean selected) {
                if (selected != null) {
                    BIKClientSingletons.getService().createTreeNode(node.id, selected.nodeKindId, selected.name, selected.objId, node.treeId, selected.id/*, node.branchIds*/,
                            new StandardAsyncCallback<Pair<Integer, List<String>>>("Failure on" /* I18N: no */ + " createTreeNode") {

                                public void onSuccess(Pair<Integer, List<String>> result) {
                                    BIKClientSingletons.showInfo(I18n.dodanoDowiazanie.get() /* I18N:  */);
                                    refreshRedisplayDataAndSelectRecord(result.v1/*, false*/);
                                }
                            });
                }
            }
        }, null);
    }

    @Override
    public boolean isEnabledByDesign() {
        return isNodeAllowLinking() /*&& !isNodeFromDynamicTree()*/;
    }
}
