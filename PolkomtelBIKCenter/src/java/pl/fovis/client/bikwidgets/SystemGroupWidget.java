/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddUsersToGroupDialog;
import pl.fovis.client.dialogs.TreeSelectorForRoleRestrictionsDialog;
import pl.fovis.common.CustomRightRoleBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.TreeNodeBranchBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author Eugeniusz
 */
public class SystemGroupWidget {

    protected VerticalPanel main;
    protected FlexTable grid;
    protected Map<Integer, Set<TreeNodeBranchBean>> customRightRoleUserEntries = new HashMap<Integer, Set<TreeNodeBranchBean>>();
    protected SystemUserGroupBean userGroup;
    protected PushButton saveBtn;

    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }

    protected void buildMainWidgets() {
    }

    public Widget buildAndShowDialog(SystemUserGroupBean userGroup) {
        this.userGroup = userGroup;
        this.customRightRoleUserEntries = BaseUtils.safeNewHashMap(userGroup.customRightRoleUserGroup);
        buildMainWidgets();
        return main;
    }

    protected void grupInOrInGrupVp(String caption, List<SystemUserGroupBean> list) {
        VerticalPanel vp = new VerticalPanel();
        vp.add(createLabelWithStyle(caption));
        if (BaseUtils.isCollectionEmpty(list)) {
            vp.add(new Label(I18n.brak.get()));
        } else {
            for (final SystemUserGroupBean ub : list) {
                vp.add(new Label(ub.name));
            }
        }
        main.add(vp);
    }

    protected void usersInGroupVp(String caption, final List<String> list) {
        final VerticalPanel vp = new VerticalPanel();
        final HorizontalPanel fp = new HorizontalPanel();
        fp.add(createLabelWithStyle(caption));
        PushButton btn = createEditRoleRestrictionsButton(new IContinuation() {

            @Override
            public void doIt() {
                String ticket = BIKClientSingletons.getCancellableRequestTicket();
                BIKClientSingletons.getService().getAllBikSystemUserWithFilter(ticket,
                        "", false, null, null, null, null, true, new StandardAsyncCallback<List<SystemUserBean>>() {

                            @Override
                            public void onSuccess(List<SystemUserBean> result) {

                                new AddUsersToGroupDialog().buildAndShowDialog(userGroup.usersName, result, I18n.przypiszUzytkownika.get(), true, new IParametrizedContinuation<Pair<List<Integer>, List<String>>>() {

                                    @Override
                                    public void doIt(final Pair<List<Integer>, List<String>> param) {
//                                     
                                        BIKClientSingletons.getService().addSystemUserToGroup(userGroup.name, param.v1, new StandardAsyncCallback<Void>() {

                                            @Override
                                            public void onSuccess(Void result) {
                                                BIKClientSingletons.showInfo(I18n.uzytkownikPrzyspisany.get());
                                                vp.clear();
                                                vp.add(fp);
                                                vp.add(getLabel(param.v2));
                                                userGroup.usersName = param.v2;
                                            }
                                        });
                                    }
                                });
                            }
                        });

            }
        });
        if (!userGroup.isBuiltIn) {
            fp.add(btn);
        }
        vp.add(fp);
        vp.add(getLabel(list));
        main.add(vp);
    }

    private Label getLabel(List<String> list) {
        if (!BaseUtils.isCollectionEmpty(list)) {
            return new Label(BaseUtils.mergeWithSepEx(list, ", "));
        } else {
            return new Label(I18n.brak.get());
        }
    }

    protected CheckBox createCheckBoxWithStyle() {
        CheckBox tempCheckBox = new CheckBox();
        tempCheckBox.setStyleName("Filters-Checkbox");
        return tempCheckBox;
    }

    protected Label createLabelWithStyle(String title) {
        Label tempLabel = new Label(title);
        tempLabel.setStyleName("addAttrLbl");
        return tempLabel;
    }

    protected void addRowToGrid(String name, Widget w, int row) {
        addRowToGrid(name, w, row, null);
    }

    protected void addRowToGrid(String name, Widget w, int row, String optRowStyleName) {
        grid.setWidget(row, 0, createLabelWithStyle(name));
        if (w != null) {
            grid.setWidget(row, 1, w);
        }
        if (optRowStyleName != null) {
            grid.getRowFormatter().addStyleName(row, optRowStyleName);
        }
    }

    protected void showEditRoleRestrictionsSelectorDialog(final CheckBox hasObjectsInRoleCb, final CustomRightRoleBean crr) {
        final Set<TreeNodeBranchBean> prev = customRightRoleUserEntries.get(crr.id);
        hasObjectsInRoleCb.setValue(!BaseUtils.isCollectionEmpty(prev));

        new TreeSelectorForRoleRestrictionsDialog().buildAndShowDialog(crr.caption,
                BIKClientSingletons.treeSelectorToTreeCodes(crr.treeSelector), prev, new IParametrizedContinuation<Set<TreeNodeBranchBean>>() {

                    @Override
                    public void doIt(Set<TreeNodeBranchBean> param) {
                        customRightRoleUserEntries.put(crr.id, param);
                        hasObjectsInRoleCb.setValue(!param.isEmpty());
                    }
                });
    }

    protected PushButton createEditRoleRestrictionsButton(final IContinuation onClick) {
        PushButton editorEditBtn;
        Image img = new Image("images/edit_yellow.png" /* I18N: no */);
        editorEditBtn = new PushButton(img, img);
        editorEditBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                onClick.doIt();
            }
        });

        editorEditBtn.setStyleName("admin-btn");
        editorEditBtn.setWidth("16px");
        editorEditBtn.setTitle(I18n.edytuj.get() /* I18N:  */);
        return editorEditBtn;
    }
}
