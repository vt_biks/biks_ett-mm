/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import junit.framework.TestCase;

/**
 *
 * @author msmoktunowicz
 */
public class CryptoUtilsTest extends TestCase {

    public CryptoUtilsTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getDigest method, of class CryptoUtils.
     */
    public void testGetDigest() {
        System.out.println("getDigest");
        String pass = "smok";
        String key = "key";
        String algorithm = "MD5";
        String expResult = "f73f81fb9c1dfbcfa878dcfe22314f57";
        String result = CryptoUtils.getDigest(pass, key, algorithm);
        assertEquals(expResult, result);
    }
}
