﻿exec sp_check_version '1.0.95.1';
go
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 DECLARE @default_constraint_name sysname
  SELECT --@default_constraint_name =
   object_name(cdefault)
  FROM syscolumns
  WHERE id = object_id('bik_system_user')
  AND name = 'is_activ'
  if @default_constraint_name is not null
    EXEC ('ALTER TABLE bik_system_user  DROP CONSTRAINT ' + @default_constraint_name)
go
------------------------------------------------------------------------------
-------------------------------------------------------------------------------
declare @CK_Name varchar(100);
select @CK_Name = d.name
 from sys.tables t  join    sys.check_constraints d on d.parent_object_id = t.object_id  
  join    sys.columns c on c.object_id = t.object_id  and c.column_id = d.parent_column_id
 where t.name = 'bik_system_user' and c.name = 'is_activ'
if(not @CK_Name is null)
	exec ('ALTER TABLE bik_system_user DROP CONSTRAINT '+ @CK_Name);
go
------------------------------------------------------------------------------
-------------------------------------------------------------------------------
exec sp_rename 'bik_system_user.is_activ', 'is_disabled', 'column';
go 
------------------------------------------------------------------------------
-------------------------------------------------------------------------------
alter table bik_system_user
	add constraint CK_bik_system_user_is_disabled check (is_disabled in (0,1))
go

--alter table bik_system_user
--	add constraint DF_bik_system_is_disabled  default 0  for is_disabled
--go
------------------------------------------------------------------------------
-------------------------------------------------------------------------------

update bik_system_user
set is_disabled = 
case is_disabled
	when 0 then   1
	else  0 end
go

------------------------------------------------------------------------------
-------------------------------------------------------------------------------
exec sp_update_version '1.0.95.1', '1.0.95.2';
go