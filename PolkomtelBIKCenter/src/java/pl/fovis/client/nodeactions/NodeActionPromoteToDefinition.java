/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.ArticleBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class NodeActionPromoteToDefinition extends NodeActionAddOrPromoteDefBase {

    @Override
    protected void callService(ArticleBean art, int /*Set<String>*/ action, AsyncCallback<Pair<Integer, List<String>>> refreshAfterAction) {
        BIKClientSingletons.getService().promoteMetapediaArt/*updateMetapediaArt*/(node, art.id, art.subject,
                        art.body, art.official, art.nodeId, action, art.versions/*depsToConnect*//*, node.branchIds*/,
                        refreshAfterAction);
    }

    @Override
    public String getCaption() {
        return I18n.przemianujNaDefinicje.get() /* I18N:  */;
    }

    @Override
    protected void fillArtProps(ArticleBean artB) {
        artB.nodeId = node.id;
        artB.subject = node.name;
    }

    @Override
    protected String getActionSuccessMsg() {
        return I18n.przemianowanoNaDefinicje.get() /* I18N:  */;
    }

    @Override
    public boolean isEnabledByDesign() {
        return isNodeNotLinkedAnyhowNorReadOnlyTreeAndOneOfTheKind("Keyword" /* I18N: no */);
    }

    @Override
    protected String getSubject() {
        return node.name;
    }

    @Override
    protected ArticleBean getBody() {
        return null;
    }

    @Override
    protected boolean isNewDefinition() {
        return true;
    }
}
