/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.ListBox;
import java.util.HashMap;
import java.util.Map;
import static pl.fovis.client.dialogs.dqm.AddOrEditDQMTestDialogBase.addLabel;
import pl.fovis.common.dqm.DataQualityTestBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class AddOrEditDQMTestDialogForDatabases extends AddOrEditDQMTestDialogBase {

    protected Map<Integer, SourceType> indexToSourceTypeMap;

    @Override
    protected void addSourcePanel() {
        multiWidget = new DQMWidgetConfigMultiSourceForDatabases();

        multiWidget.getWidget(nodeId);
        indexToSourceTypeMap = new HashMap<Integer, SourceType>();
        sourcePanel.add(addLabel(I18n.konektor.get()));
        lbxOneSource = new ListBox();
        int index = 0;
        for (SourceType value : SourceType.values()) {
            lbxOneSource.addItem(value.getName());
            indexToSourceTypeMap.put(index++, value);
            value.getWidget().getWidget(nodeId);
        }
        lbxOneSource.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                setSelectedSourceWidget();
                cleanWidgets();
            }
        });
        sourcePanel.add(lbxOneSource);
    }

    @Override
    protected void addToSourceWidget() {
        sourceWidget.add(SourceType.Oracle.getWidget().getWidget(nodeId));
    }

    protected void setWidgetVisibility(boolean isMulti) {
        sourcePanel.setVisible(!isMulti);
        scopePanel.setVisible(!isMulti);
        multiWidget.getWidget(nodeId).setVisible(isMulti);

        if (!isMulti) {
            setSelectedSourceWidget();

        } else {
            setSelectedSourceWidget(multiWidget.getWidget(nodeId));
        }
    }

    @Override
    protected IDQMWidgetConfig getSelectedWidget() {
        if (oneSourceTest.getValue()) {
            return getSelectedSource().getWidget();
        } else {
            return multiWidget;
        }
    }

    @Override
    protected int getSelectedIdx() {
        String sourceName = bean.sourceName != null ? bean.sourceName.trim() : null;
        int index = 0;
        for (Map.Entry<Integer, SourceType> entrySet : indexToSourceTypeMap.entrySet()) {
            String sourceName1 = entrySet.getValue().getSourceName();
            if ((sourceName1 == null && sourceName == null) || sourceName1.equals(sourceName/*.trim()*/)) {
                return entrySet.getKey();
            }
        }
        return index;
    }

    protected void setOtherValueInBean(DataQualityTestBean bean) {
        bean.sourceName = oneSourceTest.getValue() ? getSelectedSource().getSourceName() : null;
    }

    protected SourceType getSelectedSource() {
        return indexToSourceTypeMap.get(lbxOneSource.getSelectedIndex());
    }

    protected void setSelectedSourceWidget() {
        setSelectedSourceWidget(getSelectedSource().getWidget().getWidget(nodeId));
    }

//    enum SourceType {
//
//        Oracle(PumpConstants.SOURCE_NAME_ORACLE, PumpConstants.SOURCE_NAME_ORACLE, new DQMWidgetConfigOracle(), BIKConstants.TREE_CODE_ORACLE), SQLServer(PumpConstants.SOURCE_NAME_MSSQL, PumpConstants.SOURCE_NAME_MSSQL, new DQMWidgetConfigMSSQL(), BIKConstants.TREE_CODE_MSSQL), PostgreSQL(PumpConstants.SOURCE_NAME_POSTGRESQL, PumpConstants.SOURCE_NAME_POSTGRESQL, new DQMWidgetConfigPostgreSQL(), BIKConstants.TREE_CODE_POSTGRES), Profile(PumpConstants.SOURCE_NAME_PROFILE, PumpConstants.SOURCE_NAME_PROFILE, new DQMWidgetConfigProfile(), BIKConstants.TREE_CODE_PROFILE)/*, Multi("Multi", null, new DQMWidgetConfigMultiSource(), null)*/;
//
//        private String sourceName;
//        private IDQMWidgetConfig widget;
//        private String name;
//        private String treeCode;
//
//        SourceType(String name, String sourceName, IDQMWidgetConfig widget, String treeCode) {
//            this.name = name;
//            this.sourceName = sourceName;
//            this.widget = widget;
//            this.treeCode = treeCode;
//        }
//
//        public String getSourceName() {
//            return sourceName;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public IDQMWidgetConfig getWidget() {
//            return widget;
//        }
//
//        public String getTreeCode() {
//            return treeCode;
//        }
//    }
}
