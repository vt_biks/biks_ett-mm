/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddOrEditAdminJoinedAttributeWithTypeDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class AdminJoinedAttributeWidget {

    protected VerticalPanel vp;
    protected VerticalPanel main;
    protected ScrollPanel attrJoinedScrollPnl = new ScrollPanel();
    protected Map<Integer, AttributeBean> attrs = new HashMap<Integer, AttributeBean>();

    public Widget buildWidgets() {
        vp = new VerticalPanel();

        main = new VerticalPanel();
        main.setWidth("100%");

        Label attrsLbl = new Label(I18n.atrybuty.get() /* I18N:  */ + ":");
        attrsLbl.setStyleName("AttrHeadLbl");
        attrsLbl.addStyleName("rightTreeLbl");
        main.add(attrsLbl);
        safeAddIntoContainer(main, createAddAttributeButton(), false);
        attrJoinedScrollPnl.add(vp);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(attrJoinedScrollPnl,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 77);
        main.add(attrJoinedScrollPnl);
        vp.addStyleName("selectRolesScroll");

        return main;
    }

    protected BikCustomButton createAddAttributeButton() {
        return new BikCustomButton(I18n.dodajAtrybut.get() /* I18N:  */, "CategorizedDict-add",
                new IContinuation() {
                    public void doIt() {
                        showAddAttributeDialog();
                    }
                });
    }

    protected void safeAddIntoContainer(Panel container, Widget w, boolean showOnlyOnHover) {
        if (w != null) {
            container.add(w);
            if (showOnlyOnHover) {
                BIKClientSingletons.setupDisplayOnHover(w);
            }
        }
    }

    protected void showAddAttributeDialog() { //dodaj atrybut powiązań

        BIKClientSingletons.getService().getJoinedObjAttribute(new StandardAsyncCallback<List<AttributeBean>>("Error in" /* I18N: no */ + " getAttributes()") {
                    @Override
                    public void onSuccess(List<AttributeBean> result) {
                        new AddOrEditAdminJoinedAttributeWithTypeDialog().buildAndShowDialog(I18n.dodajAtrybut.get(), null, result,
                                new IParametrizedContinuation<AttributeBean>() {
                                    @Override
                                    public void doIt(final AttributeBean param) {
                                        String attrName = param.atrName.trim();
                                        addAttribut(attrName, param.typeAttr, param.valueOpt, param.valueOptTo, param.usedInDrools);
//                                        addItemInCategory(categoryId, attrName, param.typeAttr, param.valueOpt, param.displayAsNumber);
                                        BIKClientSingletons.addNewAttrToHintsMap(attrName);
//                                        refreshData();
                                    }
                                });
                    }
                });
    }

    protected void addAttribut(final String itemName, final String typeAttr, final String valueOptList, String valueOptTo, boolean usedInDrools) {
        BIKClientSingletons.getService().addJoinedObjAttribute(itemName, typeAttr, valueOptList, valueOptTo, usedInDrools, new StandardAsyncCallback<Integer>("Failed in adding new attribute" /* I18N: no:err-fail-in */) {
            public void onSuccess(Integer result) {
                BIKClientSingletons.showInfo(I18n.dodanoNowyAtrybut.get() /* I18N:  */);
                refreshData();
                AttributeBean ab = new AttributeBean();
                ab.id = result;
                ab.atrName = itemName;
                ab.typeAttr = typeAttr;
                ab.valueOpt = valueOptList;
                attrs.put(result, ab);
            }
        });
    }

    protected void refreshData() {
        displayData();
    }

    public void displayData() {

        BIKClientSingletons.getService().getJoinedObjAttribute(new StandardAsyncCallback<List<AttributeBean>>() {
            public void onSuccess(List<AttributeBean> result) {
                displayFetchedData(result);
            }
        });
    }

    protected void displayFetchedData(List<AttributeBean> result) {
        vp.clear();
        attrs.clear();
        HorizontalPanel hp;
        for (AttributeBean it : result) {
            String itemName = it.atrName;
            final String itemTypeForItem = it.typeAttr;

            String itemnTypeOpt = itemTypeForItem != null ? " [" + itemTypeForItem + "]" : "";
            hp = new HorizontalPanel();
            attrs.put(it.id, it);
            createOneItemInCategoryWidgets(vp, it.id, itemName, itemnTypeOpt, it.usedInDrools);
            vp.add(hp);
        }
    }

    protected void createOneItemInCategoryWidgets(Panel container, final int itemId, final String attrName, String typeOpt, boolean usedInDrools) {
        HorizontalPanel wrapper = new HorizontalPanel();
        wrapper.setStyleName("AttrLbl");

//        Widget cb = createOptCheckboxForItemWidget(itemId);
//        if (cb != null) {
//            wrapper.add(cb);
//        }
        HTML lbl = usedInDrools ? new HTML("<b>" + attrName + typeOpt + "</b>") : new HTML(attrName + typeOpt);
//        Label lbl = new Label(attrName + typeOpt);
        wrapper.add(lbl);
        createEditAndDeleteButtons(wrapper, itemId, attrName);
//        safeAddIntoContainer(wrapper, createOptEditHintAttrButton(attrName), true);
        container.add(wrapper);
    }

    protected void createEditAndDeleteButtons(HorizontalPanel wrapper, final int itemId, final String attrName) {
        safeAddIntoContainer(wrapper, createEditButton(new IContinuation() {
            public void doIt() {
                showEditItemDialog(itemId, attrName);
            }
        }), true);
        safeAddIntoContainer(wrapper, createDeleteButton(itemId, attrName), true);
    }

    protected PushButton createEditButton(final IContinuation showEditDialogCont) {

        PushButton editButton = new PushButton(new Image("images/edit_gray.png" /* I18N: no */), new Image("images/edit_yellow.png" /* I18N: no */));
        editButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                showEditDialogCont.doIt();
            }
        });
        editButton.setStyleName("Attr" /* I18N: no */ + "-editBtn");
        editButton.setTitle(I18n.edytuj.get() /* I18N:  */);
        return editButton;
    }

    protected PushButton createDeleteButton(final int attrOrRoleId, final String name) {
        PushButton deleteBtn = new PushButton(new Image("images/trash_gray.gif" /* I18N: no */), new Image("images/trash_red.png" /* I18N: no */));
        deleteBtn.addStyleName("zeroBtn");
        deleteBtn.setWidth("16px");
        deleteBtn.setTitle(I18n.usun.get() /* I18N:  */);
        deleteBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                showDeleteItemConfirmDialog(attrOrRoleId);
            }
        });
        return deleteBtn;
    }

    protected void showEditItemDialog(final int itemId, String attrOrRoleName) {
        @SuppressWarnings("unchecked")
        final List<AttributeBean> actualJoinedAttributes = BaseUtils.addManyLists(attrs.values());
        new AddOrEditAdminJoinedAttributeWithTypeDialog().buildAndShowDialog(I18n.edytujAtrybut.get(), attrs.get(itemId), actualJoinedAttributes /*result*/,
                new IParametrizedContinuation<AttributeBean>() {
                    @Override
                    public void doIt(final AttributeBean param) {
                        String attrName = param.atrName.trim();

                        BIKClientSingletons.getService().updateJoinedObjAttribute(itemId, attrName, param.typeAttr, param.valueOpt, param.valueOptTo, param.usedInDrools, new StandardAsyncCallback<Void>() {

                            @Override
                            @SuppressWarnings("unchecked")
                            public void onSuccess(Void result) {
                                AttributeBean ab = param;
                                ab.id = itemId;
                                attrs.put(ab.id, ab);
//                                actualJoinedAttributes.add(ab);
                                displayFetchedData(BaseUtils.addManyLists(attrs.values()));
                            }
                        });
                    }
                });
    }

    protected void showDeleteItemConfirmDialog(final int itemId) {
        new ConfirmDialog().buildAndShowDialog(I18n.czyNaPewnoChceszUsunacAtrybut.get() /* I18N:  */, I18n.usun.get() /* I18N:  */, new IContinuation() {
                    public void doIt() {
                        BIKClientSingletons.getService().deleteJoinedObjAttribute(itemId,
                                new StandardAsyncCallback<Void>("Failed in update attribute" /* I18N: no:err-fail-in */ + ".") {
                                    public void onSuccess(Void result) {
                                        attrs.remove(itemId);
                                        BIKClientSingletons.showInfo(I18n.usunietoAtrybut.get() /* I18N:  */ + ".");
//                                        refreshData();
                                        @SuppressWarnings("unchecked")
                                        List<AttributeBean> actualJoinedAttributes = BaseUtils.addManyLists(attrs.values());
                                        displayFetchedData(actualJoinedAttributes);
                                    }
                                });
                    }
                });
    }
}
