/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class ConfluenceLoginDialog extends BaseActionOrCancelDialog {

    protected IParametrizedContinuation<Pair<String, String>> saveCont;
    protected Grid grid;
    protected TextBox tBLogin;
    protected TextBox tBPassword;
    //--
    protected static String confluenceLogin;
    protected static String confluencePassword; // for loginName

    @Override
    protected String getDialogCaption() {
        return I18n.zalogujJako.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.publikuj.get();
    }

    @Override
    protected void buildMainWidgets() {
        grid = new Grid(2, 2);
        grid.setCellSpacing(4);
        grid.setWidget(0, 0, new Label(I18n.login.get() + ":"));
        tBLogin = new TextBox();
        tBLogin.setWidth("200px");
        grid.setWidget(0, 1, tBLogin);
        grid.setWidget(1, 0, new Label(I18n.haslo.get() + ":"));
        tBPassword = new PasswordTextBox();
        tBPassword.setWidth("200px");
        grid.setWidget(1, 1, tBPassword);
        main.add(new Label(I18n.podajUzytkownikaIhasloDoCOnfluence.get()));
        main.add(grid);
    }

    @Override
    protected boolean doActionBeforeClose() {
        confluenceLogin = tBLogin.getText();
        confluencePassword = tBPassword.getText();
        saveCont.doIt(new Pair<String, String>(tBLogin.getText(), tBPassword.getText()));
        return false;
    }

    @Override
    protected void doAction() {
        throw new IllegalStateException(I18n.toSieNiePowinnoWywolac.get() /* I18N:  */);
    }

    public void buildAndShowDialog(String login, IParametrizedContinuation<Pair<String, String>> saveCont) {
        super.buildAndShowDialog();
        this.tBLogin.setText(BaseUtils.coalesce(confluenceLogin, login));
        this.tBPassword.setText(BaseUtils.coalesce(confluencePassword, ""));
        this.saveCont = saveCont;
    }

    @Override
    public void hideDialog() {
        super.hideDialog();
    }
}
