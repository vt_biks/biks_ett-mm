﻿exec sp_check_version '1.8.0.24';
go

alter table dbo.aaa_teradata_data_model_object alter column type varchar(max)
alter table dbo.aaa_teradata_data_model_object alter column name varchar(max)
alter table dbo.aaa_teradata_data_model_object alter column branch_names varchar(max)
alter table dbo.aaa_teradata_data_model_object alter column database_name varchar(max)
alter table dbo.aaa_teradata_data_model_object alter column table_name varchar(max)

alter table dbo.aaa_teradata_data_model_process_object_rel alter column symb_proc varchar(max)
alter table dbo.aaa_teradata_data_model_process_object_rel alter column area varchar(max)
alter table dbo.aaa_teradata_data_model_process_object_rel alter column database_name varchar(max)

alter table dbo.aaa_teradata_object alter column type varchar(max)
alter table dbo.aaa_teradata_object alter column name varchar(max)

go
exec sp_update_version '1.8.0.24', '1.8.0.25';
go
