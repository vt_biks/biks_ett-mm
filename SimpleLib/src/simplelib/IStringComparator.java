/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author tflorczak
 */
public interface IStringComparator {

    public int compare(String left, String right);
}
