/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
public class NodeActionCopy extends NodeActionUpdatingBase {

    public String getCaption() {
        return I18n.kopiuj.get() /* I18N:  */;
    }

    public void execute() {
        clipboard.copy(node);
    }

    @Override
    public boolean isEnabledByDesign() {
        return false;
    }
}
