/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.oracle;

/**
 *
 * @author tflorczak
 */
public class OracleColumnBean {

    public String name;
    public String owner;
    public String tableName;
    public String serverName;
    public int columnId;
    public String description;
    public boolean isNullable;
    public String type;
    public String defaultField;

    public OracleColumnBean(String name, String owner, String tableName, String serverName, int columnId, String description, boolean isNullable, String type, String defaultField) {
        this.name = name;
        this.owner = owner;
        this.tableName = tableName;
        this.serverName = serverName;
        this.columnId = columnId;
        this.description = description;
        this.isNullable = isNullable;
        this.type = type;
        this.defaultField = defaultField;
    }
}
