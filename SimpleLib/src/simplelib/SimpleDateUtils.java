/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.sql.Time;
import java.util.Date;

/**
 *
 * @author wezyr
 */
public class SimpleDateUtils {

    public static final long MINUTE_AS_MILLIS = 60L * 1000L;
    public static final long HOUR_AS_MILLIS = 60L * MINUTE_AS_MILLIS;
    public static final long DAY_AS_MILLIS = 24L * HOUR_AS_MILLIS;

    @SuppressWarnings("deprecation")

    public static int getYear(Date d) {
        return d.getYear() + 1900;
    }

    @SuppressWarnings("deprecation")
    public static int getMonth(Date d) {
        return d.getMonth() + 1;
    }

    @SuppressWarnings("deprecation")
    public static int getDay(Date d) {
        return d.getDate();
    }

    @SuppressWarnings("deprecation")
    public static int getHours(Date d) {
        return d instanceof java.sql.Date ? 0 : d.getHours();
    }

    @SuppressWarnings("deprecation")
    public static int getMinutes(Date d) {
        return d instanceof java.sql.Date ? 0 : d.getMinutes();
    }

    @SuppressWarnings("deprecation")
    public static int getSeconds(Date d) {
        return d instanceof java.sql.Date ? 0 : d.getSeconds();
    }

    public static int getMillis(Date d) {
//        Date d2 = newDate(getYear(d), getMonth(d), getDay(d),
//                getHours(d), getMinutes(d), getSeconds(d));
//        return (int) (d.getTime() - d2.getTime());
        return (int) (d.getTime() % 1000);
    }

    public static Date truncateDay(Date d) {
        return newDate(getYear(d), getMonth(d), getDay(d));
        // poniższe źle działa (czas letni / zimowy - godzinę się myli czasem)
        //return addMillis(d, -getTimePart(d));
    }

    // millis from midnight
    public static long getTimePart(Date d) {
        return d.getTime() - truncateDay(d).getTime();
        // dodajemy godzinę, bo czas 0 to 1970-01-01 01:00:00.000 - czwartek, pierwsza w nocy
        // ale to i tak źle działa - bo czas letni / zimowy
        //return ((d.getTime() + HOUR_AS_MILLIS) % DAY_AS_MILLIS);
    }

    public static Date addMillis(Date d, long millis) {
        return new Date(d.getTime() + millis);
    }

    public static Date addSeconds(Date d, long seconds) {
        return new Date(d.getTime() + seconds * 1000);
    }

    public static Date addMinutes(Date d, long minutes) {
        return new Date(d.getTime() + minutes * MINUTE_AS_MILLIS);
    }

    public static Date addHours(Date d, long hours) {
        return new Date(d.getTime() + hours * HOUR_AS_MILLIS);
    }

    @SuppressWarnings("deprecation")
    public static Date addDays(Date d, int days) {
        Date res = new Date(d.getTime());
        res.setDate(res.getDate() + days);
        return res;
        //return new Date(d.getTime() + days * DAY_AS_MILLIS);
    }

    @SuppressWarnings("deprecation")
    public static Date newDate(int year, int month, int day,
            int hours, int minutes, int seconds) {
        Date d = new Date(year - 1900, month - 1, day, hours, minutes, seconds);
        return d;
    }

    public static Date newDate(int year, int month, int day,
            int hours, int minutes) {
        Date d = newDate(year, month, day, hours, minutes, 0);
        return d;
    }

    public static Date newDate(int year, int month, int day) {
        Date d = newDate(year, month, day, 0, 0, 0);
        return d;
    }

    public static Date newDateWithMillis(int year, int month, int day,
            int hours, int minutes, int secods, int millis) {
        Date d = newDate(year, month, day, hours, minutes, secods);
        return addMillis(d, millis);
    }

    public static String toCanonicalStringWithMillis(Date d) {
        return dateTimeToString(d, " ", true);
    }

    private static String padInt2(int v) {
        return BaseUtils.padInt(v, 2);
    }

    public static String dateTimeToString(Date d, String dateFromTimeSep, boolean withMillis) {
        if (d == null) {
            return null;
        }

        return dateToString(d, "-") + dateFromTimeSep + timeToString(d, withMillis);
    }

    public static String toISOString(Date d) {
        return dateTimeToString(d, "T", false);
    }

    public static String toCanonicalString(Date d) {
        return dateTimeToString(d, " ", false);
    }

    public static String toCanonicalStringNoSec(Date d) {
        if (d == null) {
            return null;
        }

        return dateToString(d, "-") + " " + timeToStringNoSec(d);
    }

    public static String toCanonicalString(Date d, boolean withMillis) {
        return dateTimeToString(d, " ", withMillis);
    }

    public static String timeToStringNoSec(Date d) {
        String res = padInt2(getHours(d)) + ":"
                + padInt2(getMinutes(d));
        return res;
    }

    public static String timeToString(Date d, boolean withMillis) {
        String res = padInt2(getHours(d)) + ":"
                + padInt2(getMinutes(d)) + ":" + padInt2(getSeconds(d));
        if (withMillis) {
            res += "." + BaseUtils.padInt(getMillis(d), 3);
        }
        return res;
    }

    public static String toSingleQuotedCanonicalString(Date d) {
        return toSingleQuotedCanonicalString(d, false);
    }

    public static String toSingleQuotedCanonicalString(Date d, boolean withMillis) {
        return d != null ? "\'" + toCanonicalString(d, withMillis) + "\'" : null;
    }

    public static String toQuotedISOString(Date d) {
        return "\"" + toISOString(d) + "\"";
    }

    public static String dateToString(Date d) {
        return dateToString(d, "-");
    }

    public static String dateToString(Date d, String sep) {
        return d == null ? null : getYear(d) + sep + padInt2(getMonth(d)) + sep + padInt2(getDay(d));
    }

    public static String dateTimeToStringNoSep(Date d) {
        return dateToString(d, "") + padInt2(getHours(d)) + padInt2(getMinutes(d)) + padInt2(getSeconds(d));
    }

    public static String dateToSingleQuotedCanonicalString(Date d) {
        return d != null ? "\'" + dateToString(d, "-") + "\'" : null;
    }

    public static long getElapsedMillis(Date start, Date end) {
        return end.getTime() - start.getTime();
    }

    public static long getElapsedSeconds(Date start, Date end) {
        return getElapsedMillis(start, end) / 1000;
    }

    public static double getElapsedSecondsDouble(Date start, Date end) {
        return getElapsedMillis(start, end) / 1000.0;
    }

    public static String getElapsedSecondsWithMilliPrec(Date start, Date end) {
        return BaseUtils.doubleToString(getElapsedSecondsDouble(start, end), 3);
    }

    public static String dateTimeToJson(Date date) {
        return "new Date("
                + SimpleDateUtils.getYear(date) + ","
                + (SimpleDateUtils.getMonth(date) - 1) + ","
                + SimpleDateUtils.getDay(date) + ","
                + SimpleDateUtils.getHours(date) + ","
                + SimpleDateUtils.getMinutes(date) + ","
                + SimpleDateUtils.getSeconds(date) + ","
                + SimpleDateUtils.getMillis(date)
                + ")";
    }

    public static int getElapsedSecondsComponent(long elapsedSeconds, long shift, long range) {
        long res0 = elapsedSeconds / shift;
        int res = (int) (range > 0 ? res0 % range : res0);
        return res;
    }

    public static String getElapsedSecondsComponentStr(long elapsedSeconds, long shift, long range) {
        int res = getElapsedSecondsComponent(elapsedSeconds, shift, range);
        int rangeSize = Long.toString(range).length();
        return BaseUtils.padInt(res, rangeSize);
    }

    public static Pair<Integer, Integer> getShiftAndRangeForTimeComponent(int partNo) {
        int shift, range = 60;
        switch (partNo) {
            case 0: // sekundy
                shift = 1;
                break;
            case 1: // minuty
                shift = 60;
                break;
            case 2: // godziny
                shift = 60 * 60;
                range = 24;
                break;
            default: // dni
                shift = 24 * 60 * 60;
                range = -1;
                break;
        }
        return new Pair<Integer, Integer>(shift, range);
    }

    public static int getElapsedSecondsComponentByPartNo(long elapsedSeconds, int partNo) {
        Pair<Integer, Integer> sr = getShiftAndRangeForTimeComponent(partNo);
        return getElapsedSecondsComponent(elapsedSeconds, sr.v1, sr.v2);
    }

    // helper proc for getShortElapsedTimeStr
    // compactFactor <= 0 -> no compacting
    // compactFactor > 0 -> if preceding component >= compactFactor then
    //                         suppress subsequent components
    private static boolean appendToSETStr(String partName, int partNum,
            long elapsedSecs, StringBuilder sb, int compactFactor) {
        int val = SimpleDateUtils.getElapsedSecondsComponentByPartNo(elapsedSecs, partNum);
        boolean first = sb.length() == 0;
        boolean willAppend;

        if (compactFactor <= 0) {
            willAppend = val > 0 || !first || (val == 0 && partNum == 0);
        } else {
            if (elapsedSecs == 0 && partNum == 0) {
                willAppend = true;
            } else {
                Pair<Integer, Integer> sr = getShiftAndRangeForTimeComponent(partNum);

                int higherVal = sr.v2 <= 0 ? 0 : (int) (elapsedSecs / sr.v1 / sr.v2);

                willAppend = higherVal < compactFactor && val > 0;
            }
        }

        if (willAppend) {
            if (!first) {
                sb.append(" ");
            }
            sb.append(val).append(" ").append(partName);
        }
        return willAppend;
    }

    public static String getShortPeriodStr(long elapsedSecs,
            String daysStr, String hoursStr, String minsStr, String secsStr,
            int compactFactor) {
        StringBuilder sb = new StringBuilder();
        appendToSETStr(daysStr, 3, elapsedSecs, sb, compactFactor);
        appendToSETStr(hoursStr, 2, elapsedSecs, sb, compactFactor);
        appendToSETStr(minsStr, 1, elapsedSecs, sb, compactFactor);
        appendToSETStr(secsStr, 0, elapsedSecs, sb, compactFactor);
        return sb.toString();
    }

    public static String getShortElapsedTimeStr(Date startDate, Date endDate,
            String daysStr, String hoursStr, String minsStr, String secsStr,
            int compactFactor) {
        long elapsedSecs = SimpleDateUtils.getElapsedSeconds(startDate, endDate);
        return getShortPeriodStr(elapsedSecs,
                daysStr, hoursStr, minsStr, secsStr, compactFactor);
    }

    // YYYY-MM-DD[ HH:mm[:SS[.ZZZ]]]
    public static Date parseFromCanonicalString(String dateStr) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(dateStr)) {
            return null;
        }

        int y, m, d, h, mi, s, z;

        // wszystko co nie jest cyfrą, to jest separatorem
        char[] cs = dateStr.toCharArray();

        for (int i = 0; i < cs.length; i++) {
            char c = cs[i];
            if (c < '0' || c > '9') {
                cs[i] = ' ';
            }
        }

        String[] parts = new String(cs).trim().split("[ ]+");
        int partsLen = parts.length;
        if (partsLen == 1 && parts[0].isEmpty()) {
            partsLen--;
        }
        int idx = 0;

        y = idx < partsLen ? Integer.parseInt(parts[idx++]) : 1;
        m = idx < partsLen ? Integer.parseInt(parts[idx++]) : 1;
        d = idx < partsLen ? Integer.parseInt(parts[idx++]) : 1;
        h = idx < partsLen ? Integer.parseInt(parts[idx++]) : 0;
        mi = idx < partsLen ? Integer.parseInt(parts[idx++]) : 0;
        s = idx < partsLen ? Integer.parseInt(parts[idx++]) : 0;
        z = idx < partsLen ? Integer.parseInt(parts[idx++]) : 0;

        return newDateWithMillis(y, m, d, h, mi, s, z);
    }

//    public static int getDayOfWeekCal(Date d) {
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(d);
//        // 1 -> sunday
//        // 2 -> monday
//        // 3 -> tuesday
//        return cal.get(Calendar.DAY_OF_WEEK);
//    }
//
//    public static Date getDateOfWeekMondayCal(Date d) {
//        // 1 -> sunday (subtract 6 days from d)
//        // 2 -> monday (subtract 0 days from d)
//        // 3 -> tuesday (subtract 1 days from d)
//        // 4 -> wednesday
//        // 5 -> thursday
//        // 6 -> friday
//        // 7 -> saturday
//        int dayOfWeek = getDayOfWeekCal(d);
//        int deltaFix = (dayOfWeek + 5) % 7;
//        return addDays(d, deltaFix);
//    }
    // 1 -> sunday (subtract 6 days from d)
    // 2 -> monday (subtract 0 days from d)
    // 3 -> tuesday (subtract 1 days from d)
    // 4 -> wednesday
    // 5 -> thursday
    // 6 -> friday
    // 7 -> saturday
    @SuppressWarnings("deprecation")
    public static int getDayOfWeek(Date d) {
        return d.getDay() + 1;
//        // poniższe branie południa (12:00) rozwiązuje problem zmiany czasu
//        Date dFix = newDate(getYear(d), getMonth(d), getDay(d), 12, 00);
//        long timeFrom0 = truncateDay(dFix).getTime() + HOUR_AS_MILLIS;
//        long daysFrom0 = timeFrom0 / DAY_AS_MILLIS;
//
//        return (int) ((4 + daysFrom0) % 7) + 1;
    }

    public static Date getDateOfWeekMonday(Date d) {
        // 1 -> sunday (subtract 6 days from d)
        // 2 -> monday (subtract 0 days from d)
        // 3 -> tuesday (subtract 1 days from d)
        // 4 -> wednesday
        // 5 -> thursday
        // 6 -> friday
        // 7 -> saturday
        int dayOfWeek = getDayOfWeek(d);
        int deltaFix = (dayOfWeek + 5) % 7;
        return addDays(d, -deltaFix);
    }

    public static Date getDateOfWeekMondayMidnight(Date d) {
        return truncateDay(getDateOfWeekMonday(d));
    }

    public static String toSingleQuotedCanonicalString(Time d, boolean withMillis) {
        return d != null ? "\'" + timeToString(d, withMillis) + "\'" : null;
    }

    public static String elapsedNanosToMillis3DPrec(long startNanos, long endNanos) {
        return BaseUtils.doubleToString((endNanos - startNanos) / 1000000.0, 3);
    }

    public static String elapsedNanosToSeconds3DPrec(long startNanos, long endNanos) {
        return BaseUtils.doubleToString((endNanos - startNanos) / 1000000000.0, 3);
    }

    @SuppressWarnings("deprecation")
    public static String dateDiagInfo(Date d) {
        return "(toString: " + d + ", canonical: " + SimpleDateUtils.toCanonicalStringWithMillis(d) + ", getTime: " + d.getTime()
                + ", TZO: " + d.getTimezoneOffset() + ")";
    }

//    public static void testTruncateDay(Date d) {
//        Date truncated = truncateDay(d);
//        Date slowTruncated = newDate(getYear(d), getMonth(d), getDay(d));
//        System.out.println("Date=" + toCanonicalStringWithMillis(d) + ", truncated to day=" + toCanonicalStringWithMillis(truncated) +
//                ", slow trunc=" + toCanonicalStringWithMillis(slowTruncated) + (truncated.equals(slowTruncated) ? " OK" : " *** ERROR!!! *"));
//    }
//
//    public static void main(String[] args) {
//        testTruncateDay(new Date());
//        testTruncateDay(newDate(2000, 10, 10));
//        testTruncateDay(newDate(1970, 1, 1, 0, 0));
//        testTruncateDay(newDate(1970, 1, 1, 0, 59, 59));
//        testTruncateDay(newDate(1970, 1, 1, 1, 0));
//        testTruncateDay(newDate(1970, 1, 1, 1, 1));
//    }
//
//    public static Date getDateOfWeekMonday(Date d) {
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(d);
//        // 1 -> sunday (subtract 6 days from d)
//        // 2 -> monday (subtract 0 days from d)
//        // 3 -> tuesday (subtract 1 days from d)
//        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
//        int deltaFix = (dayOfWeek + 5) % 7;
//        return addDays(d, deltaFix);
//    }
//    public static void testDayOfWeek(Date d) {
//        int fromCal = getDayOfWeekCal(d);
//        int noCal = getDayOfWeek(d);
//        System.out.println("Date=" + toCanonicalStringWithMillis(d) + ": fromCal=" + fromCal + ", noCal=" + noCal
//                + (fromCal == noCal ? " OK!" : " *** ERROR!!! ***") + ", date truncated to day=" + toCanonicalStringWithMillis(truncateDay(d)));
//    }
//
//    public static void main(String[] args) {
//        testDayOfWeek(newDate(1970, 1, 1));
//        testDayOfWeek(newDate(1970, 1, 2));
//        testDayOfWeek(newDate(1970, 1, 1, 23, 59));
//        testDayOfWeek(newDate(1970, 1, 2, 23, 59));
//        testDayOfWeek(newDate(1970, 1, 2, 0, 1));
//        testDayOfWeek(newDate(1970, 1, 2, 1, 0));
//        testDayOfWeek(new Date());
//        testDayOfWeek(newDate(2000, 1, 1));
//        testDayOfWeek(newDate(2000, 2, 28));
//        testDayOfWeek(newDate(2000, 2, 29));
//        testDayOfWeek(newDate(2012, 4, 22, 1, 0));
//        testDayOfWeek(newDate(2012, 4, 22, 0, 0));
//        testDayOfWeek(newDate(2012, 4, 22, 0, 1));
//        testDayOfWeek(newDate(2012, 4, 21, 23, 30));
//        testDayOfWeek(newDate(2012, 1, 21, 23, 30));
//        testDayOfWeek(newDate(2012, 9, 20, 23, 30));
//        testDayOfWeek(newDate(2012, 9, 21, 23, 30));
//        testDayOfWeek(newDate(2012, 9, 22, 23, 30));
////        Date date0 = new Date(0); //1970-01-01 01:00:00.000 - czwartek
////        System.out.println("date0=" + toCanonicalStringWithMillis(date0));
////
////        Calendar cal = Calendar.getInstance();
////        cal.setTime(date0);
////        System.out.println("cal0=" + toCanonicalStringWithMillis(cal.getTime()));
////        System.out.println("cal0 day of week=" + cal.get(Calendar.DAY_OF_WEEK));
////        System.out.println("cal now day of week=" + Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
////        System.out.println("now day of week=" + getDayOfWeek(new Date()));
////        System.out.println("0+23:59 day of week=" + getDayOfWeek(newDate(1970, 1, 1, 23, 59)));
////        System.out.println("1+00:59 day of week=" + getDayOfWeek(newDate(1970, 1, 2, 0, 59)));
////        System.out.println("1+01:02 day of week=" + getDayOfWeek(newDate(1970, 1, 2, 1, 2)));
////        System.out.println("cal 0+23:59 day of week=" + getDayOfWeekCal(newDate(1970, 1, 1, 23, 59)));
////        System.out.println("cal 1+00:59 day of week=" + getDayOfWeekCal(newDate(1970, 1, 2, 0, 59)));
////        System.out.println("cal 1+01:02 day of week=" + getDayOfWeekCal(newDate(1970, 1, 2, 1, 2)));
////        System.out.println("2000 day of week=" + getDayOfWeekCal(newDate(2000, 1, 1, 23, 59)));
////        System.out.println("0+23:59 day of week=" + getDayOfWeekCal(newDate(2000, 1, 1, 23, 59)));
//
////        Date xxx = newDate(2012, 1, 10);
////        System.out.println("xxx=" + dateToString(xxx, "-"));
////        Date xxxMinus30days = addMillis(xxx, -30L * 24L * 60L * 60L * 1000L);
////        System.out.println("xxxMinus30days=" + dateToString(xxxMinus30days, "-"));
////        System.out.println("date=" + toCanonicalStringWithMillis(parseFromCanonicalString("2011")));
////        System.out.println("date=" + toCanonicalStringWithMillis(parseFromCanonicalString("2011:11-02")));
////        System.out.println("date=" + toCanonicalStringWithMillis(parseFromCanonicalString("2012:3-02 17")));
////        System.out.println("date=" + toCanonicalStringWithMillis(parseFromCanonicalString("2012:01-02 17:01")));
////        System.out.println("date=" + toCanonicalStringWithMillis(parseFromCanonicalString("2012:13-02 17:01:33")));
////        System.out.println("date=" + toCanonicalStringWithMillis(parseFromCanonicalString("2001:11-02 07:1.30.666")));
//    }
//
//    public static void main(String[] args) {
//        Date xxx = newDate(2012, 1, 10);
//        System.out.println("xxx=" + dateToString(xxx, "-"));
//        Date xxxMinus30days = addMillis(xxx, -30L * 24L * 60L * 60L * 1000L);
//        System.out.println("xxxMinus30days=" + dateToString(xxxMinus30days, "-"));
//        System.out.println("date0=" + toCanonicalStringWithMillis(parseFromCanonicalString("")));
//        System.out.println("date=" + toCanonicalStringWithMillis(parseFromCanonicalString("2011")));
//        System.out.println("date=" + toCanonicalStringWithMillis(parseFromCanonicalString("2011:11-02")));
//        System.out.println("date=" + toCanonicalStringWithMillis(parseFromCanonicalString("2012:3-02 17")));
//        System.out.println("date=" + toCanonicalStringWithMillis(parseFromCanonicalString("2012:01-02 17:01")));
//        System.out.println("date=" + toCanonicalStringWithMillis(parseFromCanonicalString("2012:13-02 17:01:33")));
//        System.out.println("date=" + toCanonicalStringWithMillis(parseFromCanonicalString("2001:11-02 07:1.30.666")));
//    }
}
