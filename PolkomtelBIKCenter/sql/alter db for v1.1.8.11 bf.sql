﻿exec sp_check_version '1.1.8.11';
go

alter table bik_tree
add is_in_ranking int default 0 not null
go

update bik_tree set is_in_ranking = 1 where 
code='Glossary' or code='Documents' or code='Blogs' 
or code='ShortcutsGlossary' or code='BusinessGlossary'

insert into bik_app_prop (name, val, is_editable) values ('availableConnectors', 'SAP BO,Teradata,DQC,Active Directory', 0)

insert into bik_app_prop (name, val, is_editable) values ('starredUserCount', '10', 1)

exec sp_update_version '1.1.8.11', '1.1.8.12';
go