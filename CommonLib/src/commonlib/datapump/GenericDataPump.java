/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.datapump;

import simplelib.LameRuntimeException;

/**
 *
 * @author pmielanczuk
 */
public class GenericDataPump {

    protected IDataPumpSource src;
    protected IDataPumpDestination dst;

    public GenericDataPump(IDataPumpSource src, IDataPumpDestination dst) {
        this.src = src;
        this.dst = dst;
    }

    protected String getOptPumpTaskDescr() {
        return null;
    }

    public int pumpIt() {

        long startNanos;
        long endNanos;
        long totalNanos = 0;

        String executionStage = "init";

        String optPumpTaskDescr = getOptPumpTaskDescr();

        long rowNum = 0;
        boolean success = false;

        try {

//            startNanos = System.currentTimeMillis();
            startNanos = System.nanoTime();

            executionStage = "openSource";
            ColumnMetadata[] sourceColsMetadata = src.openSource();

            endNanos = System.nanoTime();
            totalNanos += (endNanos - startNanos);

            try {
                executionStage = "openDestination";
                dst.openDestination(sourceColsMetadata);
                int cnt = 0;
                try {
                    startNanos = System.nanoTime();
                    executionStage = "hasSourceData";
                    while (src.hasSourceData()) {

                        executionStage = "readDataRow";
                        Object[] dataRow = src.readDataRow();

                        endNanos = System.nanoTime();
                        totalNanos += (endNanos - startNanos);

                        executionStage = "writeDataRow";
                        dst.writeDataRow(dataRow);

                        startNanos = System.nanoTime();
                        executionStage = "hasSourceData";

//                        System.out.println("Added " + (cnt++) + " records");
                    }

                    endNanos = System.nanoTime();
                    totalNanos += (endNanos - startNanos);
                    success = true;
                } finally {
                    boolean prevSuccess = success;
                    success = false;
                    executionStage = "closeDestination(success=" + prevSuccess + ")";
                    dst.closeDestination(prevSuccess);
                    success = prevSuccess;
                }

            } finally {
                boolean prevSuccess = success;
                success = false;
                executionStage = "closeSource(success=" + prevSuccess + ")";
                src.closeSource();
                success = prevSuccess;
            }
        } catch (Exception ex) {
            throw new LameRuntimeException("error in execution stage: " + executionStage + ", rowNum=" + rowNum + (optPumpTaskDescr == null ? "" : ", info: " + optPumpTaskDescr)
                    + "\n" + ex.getMessage(), ex);
        }

        return (int) (totalNanos / 1000 / 1000000);
    }
}
