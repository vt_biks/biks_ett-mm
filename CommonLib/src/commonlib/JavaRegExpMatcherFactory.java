/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import simplelib.IRegExpMatcher;
import simplelib.IRegExpMatcherFactory;
import simplelib.IRegExpPattern;

/**
 *
 * @author wezyr
 */
public class JavaRegExpMatcherFactory implements IRegExpMatcherFactory {

//    public IRegExpMatcher matcher(String pattern, String str) {
//        return new JavaRexExpMatcher(Pattern.compile(pattern), str);
//    }
    public IRegExpPattern compile(String pattern, boolean ignoreCase, boolean multiline) {
        Pattern p = Pattern.compile(pattern,
                (ignoreCase ? Pattern.CASE_INSENSITIVE + Pattern.UNICODE_CASE : 0)
                + (multiline ? Pattern.MULTILINE + Pattern.DOTALL : 0));
        return new JavaRexExpPattern(p);
    }

    public static class JavaRexExpPattern implements IRegExpPattern {

        private Pattern p;

        public JavaRexExpPattern(Pattern p) {
            this.p = p;
        }

        public IRegExpMatcher matcher(String str) {
            return new JavaRexExpMatcher(p, str);
        }
    }

    public static class JavaRexExpMatcher implements IRegExpMatcher {

        private Matcher m;

        public JavaRexExpMatcher(Pattern p, String s) {
            this.m = p.matcher(s);
        }

        public boolean find() {
            return m.find();
        }

        public int start() {
            return m.start();
        }

        public int end() {
            return m.end();
        }

        public int start(int grp) {
            return m.start(grp);
        }

        public int end(int grp) {
            return m.end(grp);
        }

        public String replaceAll(String replacement) {
            return m.replaceAll(replacement);
        }

        public String group(int grp) {
            return m.group(grp);
        }

        public boolean find(int startPos) {
            return m.find(startPos);
        }
    }
}
