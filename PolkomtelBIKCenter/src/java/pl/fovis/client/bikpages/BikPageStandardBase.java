/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.IPageDataFetchBroker.TreeDataRequestKind;
import simplelib.BaseUtils;

/**
 *
 * @author bfechner
 */
public abstract class BikPageStandardBase<DATA>
        extends BikPageMinimalBase /*implements IMenuItemMainWidget, IBikPage<Integer>*/ //implements IPageDataFetchBroker
{

    //protected Boolean isDataFetched = null;
    protected Integer optIdToSelect = null;
    // czy trwa właśnie ściąganie danych (!= null)
    // jeżeli trwa filtrowanych, to isFullDataRequested == false
    // w przeciwnym razie (isFullDataRequested == true) - pełne dane są
    // właśnie ściągane (poleciał request)
    //protected Boolean isFullDataRequested = null;
    protected IPageDataFetchBroker dataFetchBroker = new PageDataFetchBroker();

//    // unused
//    final protected boolean isInited() {
//        return pageWidget != null;
//    }
    //ww: ważne!!! callback musi wywoływać dataFetchFinished(success) - zarówno
    // w przypadku powodzenia (onSuccess) jak i porażki (onFailure)
    protected abstract Request callServiceToGetData(AsyncCallback<DATA> callback);

    protected abstract void populateWidgetsWithData(DATA data/*, boolean hasIdToSelect*/);

//    protected abstract void populateWidgetsWithData(DATA data);
    protected void innerSelectEntityById(Integer optId) {
        // no-op
    }

    protected final void selectEntityById(Integer optId) {
        if (dataFetchBroker.getRequestInProgressKind() != null) {
            optIdToSelect = optId;
            return;
        }

        optIdToSelect = null;
        innerSelectEntityById(optId);
    }

//    public void dataFetchCancel() {
//        if (fetchRequest != null && fetchRequest.isPending()) {
//            if (fetchDataRequestTicket != null) {
//                BIKClientSingletons.cancelServiceRequest(fetchDataRequestTicket);
//                fetchDataRequestTicket = null;
//            }
//            fetchRequest.cancel();
//            fetchRequest = null;
//        }
//        //isFullDataRequested = null;
//        treeDataRequestKind = null;
//    }
//
//    public void dataFetchStarting(TreeDataRequestKind treeDataRequestKind, Request fetchRequest, String optRequestTicket) {
//        // Additional można zawołać tylko gdy dane są już wczytane i nie jest właśnie
//        // wykonywany żaden request albo jest to inny Additional
//        if (treeDataRequestKind == TreeDataRequestKind.Additional
//                && (this.isFullDataFetched == null
//                || this.treeDataRequestKind != null
//                && this.treeDataRequestKind != TreeDataRequestKind.Additional)) {
//            BIKClientSingletons.showError("cannot call additional request");
//            throw new RuntimeException("cannot call additional request");
//        }
//
//        dataFetchCancel();
//        //this.isFullDataRequested = isFullData;
//        this.treeDataRequestKind = treeDataRequestKind;
//        this.fetchRequest = fetchRequest;
//        this.fetchDataRequestTicket = optRequestTicket;
//    }
//
////    public void dataFetchNextRequest(Request fetchRequest) {
////        this.fetchRequest = fetchRequest;
////    }
//    public void dataFetchFinished(boolean success) {
//        if (success) {
//            if (treeDataRequestKind != treeDataRequestKind.Additional)
//            isFullDataFetched = treeDataRequestKind == treeDataRequestKind.Full;
//        }
//        //this.isFullDataRequested = null;
//        treeDataRequestKind = null;
//        fetchRequest = null;
//        fetchDataRequestTicket = null;
//    }
    protected void clearFilterControls() {
        // no-op
        // w podklasach jeżeli są kontrolki do filtrowania, to tutaj trzeba
        // wstawić kod, który je wyczyści, ale uwaga! nie może on wołać
        // wczytywania danych
    }

    protected Set<Integer> getExpandedBeforeFetch() {
        return null;
    }

    protected void reexpandAfterFetch(Set<Integer> expandedBeforeFetch, Integer optIdToSelect) {
        // no-op
    }

    protected final void fetchData(boolean keepExpandedNodes) {

        final Set<Integer> expandedBeforeFetch = keepExpandedNodes ? getExpandedBeforeFetch() : null;

        clearFilterControls();
        //isDataFetched = false;
        dataFetchBroker.dataFetchStarting(TreeDataRequestKind.Full,
                callServiceToGetData(
                        //                        new TreeDataFetchAsyncCallback<DATA>("fetch data failed" /* I18N: no */, dataFetchBroker) {
                        new TreeDataFetchAsyncCallback<DATA>(null /* I18N: no */, dataFetchBroker) {

                                    @Override
                                    protected void doOnSuccess(DATA result) {
                                        //Window.alert("optIdToSelect = " + optIdToSelect);
                                        populateWidgetsWithData(result/*, (optIdToSelect != null)*/);
                                        refreshMainWidgetState();

                                        if (expandedBeforeFetch != null && !expandedBeforeFetch.isEmpty()) {
                                            reexpandAfterFetch(expandedBeforeFetch, optIdToSelect);
                                        } else {
                                            innerSelectEntityById(optIdToSelect);
                                        }
                                    }
                                } //                new StandardAsyncCallback<DATA>("fetch data failed") {
                //
                //            public void onSuccess(DATA result) {
                //                //isDataFetched = true;
                //                dataFetchFinished(true);
                //                populateWidgetsWithData(result);
                //                refreshMainWidgetState();
                //                innerSelectEntityById(optIdToSelect);
                //            }
                //
                //            @Override
                //            public void onFailure(Throwable caught) {
                //                dataFetchFinished(false);
                //                super.onFailure(caught);
                //            }
                //        }
                ), null);
    }

    protected boolean isFullDataFetchInProgress() {
        //return BaseUtils.safeEquals(isDataFetched, false);
        return //BaseUtils.safeEquals(isFullDataRequested, true);
                //treeDataRequestKind == treeDataRequestKind.Full;
                dataFetchBroker.getRequestInProgressKind() == TreeDataRequestKind.Full;
    }

    protected boolean isDataNeverFetched() {
        //return isDataFetched == null;
        return dataFetchBroker.isFullDataFetched() == null;
    }

//    protected boolean isDataAlreadyFetched() {
//        return BaseUtils.safeEquals(isDataFetched, true);
//    }
    protected void refreshMainWidgetState() {
        // no-op
    }

    protected void refreshDetailWidgetsState() {
        // no-op
    }

    protected void refreshDetailData() {
        // no-op
    }

    protected final void refreshWidgetsState(boolean refreshDetailData) {
        refreshMainWidgetState();

        if (refreshDetailData) {
            refreshDetailData();
        } else {
            refreshDetailWidgetsState();
        }
    }

    protected boolean needsRefreshByItself() {
//        return false;
        return BIKClientSingletons.getTreeTabAutoRefreshLevel() == 1;
    }

//    protected void clearFilterIfNeeded() {
//        // no-op
//    }
    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
//        this.optIdToSelect = optIdToSelect;
        this.optIdToSelect = BaseUtils.nullToDef(optIdToSelect, getSelectedNodeId());

        if (isFullDataFetchInProgress()) {
            // leci full fetch więc wystarczy poczekać aż się skończy
            // jeżeli trzeba się ustawić na konkretnym rekordzie, to
            // nastąpi to na końcu full fetch
            return;
        }

        boolean shouldFetch = false;

        if (refreshData || needsRefreshByItself() || isDataNeverFetched()) {
            // trzeba ściągnąć dane (full fetch),
            // bo taki parametr, albo jeszcze wcale nie ma danych
            shouldFetch = true;
        } else if (dataFetchBroker.getRequestInProgressKind() != null && optIdToSelect != null) {
            // filtered fetch leci (pełny fetch obsłużony powyżej),
            // więc trzeba go przerwać i wywołać full fetch,
            // bo musimy ustawić się na konkretnym rekordzie
            shouldFetch = true; // full fetch (bez filtrowania)
        } else if (BaseUtils.safeEquals(dataFetchBroker.isFullDataFetched(), false) && optIdToSelect != null) {
            // nie trzeba odświeżać, ale jeżeli wyświetlamy przefiltrowane dane
            // i trzeba się ustawić na rekordzie, to robimy full fetch
            shouldFetch = true;
        }

        if (shouldFetch) {
            fetchData(true);
        } else {
            if (optIdToSelect == null) {
                optIdToSelect = getSelectedNodeId();
            }
            if (optIdToSelect != null) {
                refreshMainWidgetState();
                selectEntityById(optIdToSelect);
            } else {
                refreshWidgetsState(refreshDetailData);
            }
        }
    }

    protected void refreshPage(Integer optIdToSelect) {
        activatePage(true, optIdToSelect, false);
    }
}
