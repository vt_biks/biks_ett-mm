﻿exec sp_check_version '1.0.99.7';
go

------------------------------------
------------------------------------

insert into bik_admin(param, value) values ('dqc.groupFilter','')
insert into bik_admin(param, value) values ('dqc.activeFilter','60')
insert into bik_admin(param, value) values ('dqc.withInactiveTests','1')

alter table bik_dqc_test
add test_type int not null default 0
go

update bik_node_kind
set code = 'DQCTestSuccess',
caption = 'Test pozytywny',
icon_name = 'dqctests'
where code = 'DQCTest'

insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank) values ('DQCTestFailed', 'Test negatywny', 'dqctestf', 'Metadata',1,25)
insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank) values ('DQCTestInactive', 'Test nieaktywny', 'dqctestn', 'Metadata',1,25)


-- poprzednia wersja procedury w alter db for v1.0.96.2 tf.sql
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_dqc]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_node_dqc
go

create procedure [dbo].[sp_insert_into_bik_node_dqc]
as
begin

declare @dqc_group int;
select @dqc_group=id from bik_node_kind where code='DQCGroup'
declare @dqc_test_s int;
select @dqc_test_s=id from bik_node_kind where code='DQCTestSuccess'
declare @dqc_test_f int;
select @dqc_test_f=id from bik_node_kind where code='DQCTestFailed'
declare @dqc_test_n int;
select @dqc_test_n=id from bik_node_kind where code='DQCTestInactive'
declare @dqc_alltests int;
select @dqc_alltests=id from bik_node_kind where code='DQCAllTestsFolder'
declare @dqc_tree_id int;
select @dqc_tree_id=id from bik_tree where code='DQC'
declare @alltestsid int;
select @alltestsid = id from bik_node where is_built_in=1 and is_deleted=0 and linked_node_id is null and node_kind_id=@dqc_alltests

-- update grup
update bik_node 
set parent_node_id = null, node_kind_id = @dqc_group, name = dqc.name,
	is_deleted = 0, descr = dqc.description, tree_id = @dqc_tree_id
from bik_dqc_group dqc
where bik_node.obj_id = dqc.__obj_id 
and bik_node.linked_node_id is null
and dqc.__deleted=0

-- dodanie nowych grup
insert into bik_node (parent_node_id, node_kind_id, name, descr, tree_id, obj_id)
	select null,@dqc_group, dqc.name, dqc.description, @dqc_tree_id, dqc.__obj_id
	from bik_dqc_group dqc 
	left join bik_node bn on bn.obj_id=dqc.__obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null 
	and bn.node_kind_id=@dqc_group 
	and bn.tree_id=@dqc_tree_id
	where bn.id is null 
	and dqc.__deleted=0

-- usuniecie niepotrzebnych grup
update bik_node
set is_deleted=1
where node_kind_id=@dqc_group
and obj_id not in (select __obj_id from bik_dqc_group where __deleted=0)
and linked_node_id is null
and is_deleted=0

-- update testów
update bik_node 
set parent_node_id = @alltestsid, node_kind_id = case when dqc.test_type = 0 then @dqc_test_s when dqc.test_type = 1 then @dqc_test_f else @dqc_test_n end, name = dqc.name,
	is_deleted = 0, descr = dqc.description, tree_id = @dqc_tree_id
from bik_dqc_test dqc
where bik_node.obj_id = dqc.__obj_id 
and bik_node.linked_node_id is null
and dqc.__deleted=0

-- dodanie nowych testów
insert into bik_node (parent_node_id, node_kind_id, name, descr, tree_id, obj_id)
	select @alltestsid,case when dqc.test_type = 0 then @dqc_test_s when dqc.test_type = 1 then @dqc_test_f else @dqc_test_n end, dqc.name, dqc.description, @dqc_tree_id, dqc.__obj_id
	from bik_dqc_test dqc 
	left join bik_node bn on bn.obj_id=dqc.__obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null 
	and bn.node_kind_id in (@dqc_test_s, @dqc_test_f, @dqc_test_n)
	and bn.tree_id=@dqc_tree_id
	where bn.id is null 
	and dqc.__deleted=0

-- usuniecie niepotrzebnych testów
update bik_node
set is_deleted=1
where node_kind_id in (@dqc_test_s, @dqc_test_f, @dqc_test_n)
and obj_id not in (select __obj_id from bik_dqc_test where __deleted=0)
and linked_node_id is null
and is_deleted=0

create table #tmp_test_group (test_node_id int,group_node_id int, test_name varchar(max), obj_id varchar(max), test_type int);

insert into #tmp_test_group(test_node_id,group_node_id,test_name,obj_id,test_type)
	select bn2.id, bn.id, bn2.name, bn2.obj_id, bn2.node_kind_id from bik_dqc_group_test dgt
	join bik_dqc_group dg on dg.id_group=dgt.id_group
	join bik_dqc_test dt on dt.id_test=dgt.id_test
	join bik_node bn on bn.obj_id=dg.__obj_id
	and bn.is_deleted = 0 and bn.linked_node_id is null
	and bn.node_kind_id = @dqc_group and bn.parent_node_id is null
	join bik_node bn2 on bn2.obj_id=dt.__obj_id
	and bn2.is_deleted = 0 and bn2.linked_node_id is null
	and bn2.node_kind_id in (@dqc_test_s, @dqc_test_f, @dqc_test_n) and bn2.parent_node_id=@alltestsid
	where dgt.__deleted = 0 and dt.__deleted = 0 and dg.__deleted = 0

-- update testów podlinkowanych
update bik_node 
set parent_node_id = dqc.group_node_id, node_kind_id = dqc.test_type, name = dqc.test_name,
	is_deleted = 0, tree_id = @dqc_tree_id, linked_node_id = dqc.test_node_id
from #tmp_test_group dqc
where bik_node.node_kind_id in (@dqc_test_s, @dqc_test_f, @dqc_test_n)
and bik_node.linked_node_id is not null
and bik_node.tree_id = @dqc_tree_id
and bik_node.linked_node_id=dqc.test_node_id
and bik_node.parent_node_id=dqc.group_node_id


-- dodanie nowych testów podlinkowanych
insert into bik_node (parent_node_id, node_kind_id, name, tree_id, obj_id, linked_node_id)
	select dqc.group_node_id,dqc.test_type, dqc.test_name, @dqc_tree_id, dqc.obj_id, dqc.test_node_id
	from #tmp_test_group dqc 
	left join bik_node bn on bn.obj_id=dqc.obj_id 
	and bn.parent_node_id=dqc.group_node_id
	and bn.linked_node_id = dqc.test_node_id
	and bn.is_deleted=0
	and bn.linked_node_id is not null 
	and bn.node_kind_id in (@dqc_test_s, @dqc_test_f, @dqc_test_n)
	and bn.tree_id=@dqc_tree_id
	where bn.id is null 

-- usuniecie niepotrzebnych testów podlinkowanych
update bik_node
set is_deleted=1
where node_kind_id in (@dqc_test_s, @dqc_test_f, @dqc_test_n)
and obj_id not in (select obj_id from #tmp_test_group)
and linked_node_id is not null
and is_deleted=0

drop table #tmp_test_group;

exec sp_node_init_branch_id @dqc_tree_id,null

end;
go
------------------------------------
------------------------------------

exec sp_update_version '1.0.99.7', '1.0.99.8';
go