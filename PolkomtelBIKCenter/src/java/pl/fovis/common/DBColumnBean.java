/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class DBColumnBean implements Serializable {

    public String name;
    public String type;
    public String description;
    public boolean isNullable;
    public String defaultField;
    public Long seedValue;
    public Long incrementValue;
    // technical
    public String icon;
    public int nodeId;
    public String treeCode;
}
