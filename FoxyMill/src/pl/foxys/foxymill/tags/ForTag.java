/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.tags;

import commonlib.LameUtils;
import java.util.Iterator;
import pl.foxys.foxymill.FoxyMillUtils;
import pl.foxys.foxymill.renderctx.IRenderCtx;

/**
 *
 * @author wezyr
 */
public class ForTag extends TagWithExpr {

    public static class ForTagStatus {

        public boolean isLast;
        public Object val;
        public int idx;

        public ForTagStatus(Object val, int idx, boolean isLast) {
            this.val = val;
            this.idx = idx;
            this.isLast = isLast;
        }
    }
    private String var;
    private String sep;
    private String statusVar;
//    private IParsedExpression expr;

    @Override
    protected void initSelf() {
        var = attrs.get("var");
        statusVar = attrs.get("statusVar");
        sep = attrs.get("sep");
    }

//    @Override
//    protected void prepareSelf() {
//        expr = attrToExpr("expr"); //exprLang.parseExpression(attrs.get("expr"));
//    }

    @Override
    protected void renderWithExprVal(//IRenderContext
            IRenderCtx renderCtx, Object val) {
        //Object val = expr.eval(renderCtx);
        if (val == null) {
            return;
        }

        Iterator iter = LameUtils.makeIterable(val, false).iterator();
        
//        if (val instanceof Iterable) {
//            iter = ((Iterable) val).iterator();
//        } else if (val.getClass().isArray()) {
//            iter = //Arrays.asList((Object[])val).iterator();
//                    new PrimitiveArrayIterator(val);
//        } else {
//            throw new LameRuntimeException("for expr is of class " + BaseUtils.safeGetClassName(val)
//                    + " which cannot be iterated");
//        }

        boolean hasNext = iter.hasNext();
        int idx = 0;
        while (hasNext) {
            Object oneItem = iter.next();
            hasNext = iter.hasNext();
            if (var != null) {
                renderCtx.getVars().put(var, oneItem);
            }
            if (statusVar != null) {
                renderCtx.getVars().put(statusVar, new ForTagStatus(oneItem, idx, !hasNext));
            }
            FoxyMillUtils.render(subTags, renderCtx);
            if (hasNext && sep != null) {
                renderCtx.getOutputDevice().append(sep);
            }
            idx++;
        }
    }
}
