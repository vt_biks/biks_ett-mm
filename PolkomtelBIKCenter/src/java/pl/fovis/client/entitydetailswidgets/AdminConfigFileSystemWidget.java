/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.List;
import pl.bssg.metadatapump.common.ConnectionParametersFileSystemBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.trzy0.foxy.commons.FoxyRuntimeException;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuationWithReturn;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class AdminConfigFileSystemWidget extends AdminConfigManyServersBase<List<ConnectionParametersFileSystemBean>, ConnectionParametersFileSystemBean> {

    protected CheckBox chbIsActive;
//    protected CheckBox chbDownloadFiles;
    protected TextBox tbxName;
    protected TextBox tbxPath;
    protected TextBox tbxUser;
    protected TextBox tbxPw;
    protected TextBox tbxDomain;
    protected CheckBox chbDownloadFiles;
    protected CheckBox chbIsRemote;
    protected TextBox tbxMaxFileSize;
    protected TextBox tbxFileNamePatterns;
//    protected CheckBox chbPw;
    protected DateBox dbFileSince;
    protected ConnectionParametersFileSystemBean activatedBean;

    @Override
    protected void activateForServer(String id) {
        activatedBean = serversMap.get(id);
        chbIsActive.setValue(activatedBean.isActive);
        chbIsRemote.setValue(activatedBean.isRemote);
        chbDownloadFiles.setValue(activatedBean.downloadFiles);
        tbxName.setText(activatedBean.name);
        tbxPath.setText(activatedBean.path);
        tbxUser.setText(activatedBean.user);
        tbxPw.setText(activatedBean.password);
        tbxDomain.setText(activatedBean.domain);
        tbxMaxFileSize.setText(BaseUtils.safeToStringDef(activatedBean.maxFileSize, "10.0"));
        tbxFileNamePatterns.setText(activatedBean.fileNamePatterns == null ? "*.*" : activatedBean.fileNamePatterns);
        dbFileSince.getTextBox().setText(activatedBean.fileSince == null ? "" : activatedBean.fileSince);
        setWidgetsEnabled(chbIsActive.getValue());
        setAccountWidgetEnabled(chbIsRemote.getValue());
        tbxMaxFileSize.setEnabled(activatedBean.downloadFiles && chbIsActive.getValue());
    }

    private void setAccountWidgetEnabled(Boolean isRemote) {
        if (chbIsActive.getValue()) {
            tbxDomain.setEnabled(isRemote);
            tbxUser.setEnabled(isRemote);
            tbxPw.setEnabled(isRemote);
        }
    }

    @Override
    protected void buildRightWidget(CellPanel panelRight) {
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
                setAccountWidgetEnabled(chbIsRemote.getValue());
                tbxMaxFileSize.setEnabled(chbDownloadFiles.getValue() && chbIsActive.getValue());
            }
        }, false, panelRight);
        createLabel(I18n.nazwa.get() + ":", panelRight);
        tbxName = createTextBox(panelRight, false);
        tbxName.setEnabled(false);
        createLabel(I18n.sciezkaDoKatalogu.get() + ":", panelRight);
        tbxPath = createTextBox(panelRight);
        chbIsRemote = createCheckBox(I18n.zdalnie.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setAccountWidgetEnabled(chbIsRemote.getValue());
            }
        }, true, panelRight);
        createLabel(I18n.uzytkownik.get() + ":", panelRight);
        tbxUser = createTextBox(panelRight);
        createLabel(I18n.haslo.get() + ":", panelRight);
        tbxPw = createPasswordTextBox(panelRight);
//        chbPw = createAndAddCheckBox(I18n.ukryjHaslo.get(), new ValueChangeHandler<Boolean>() {
//            @Override
//            public void onValueChange(ValueChangeEvent<Boolean> event) {
//                tbxPw.getElement().setAttribute("type" /* I18N: no */, chbPw.getValue() ? "password" /* I18N: no */ : "text" /* I18N: no */);
//            }
//        }, panelRight);
        createLabel(I18n.domena.get() + ":", panelRight);
        tbxDomain = createTextBox(panelRight);
        chbDownloadFiles = createAndAddCheckBox(I18n.pobieraniePlikow.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                tbxMaxFileSize.setEnabled(chbDownloadFiles.getValue());
            }
        }, panelRight);
        createLabel(I18n.granicaRozmiaruPlikow.get() + ":", panelRight);
        tbxMaxFileSize = createTextBox(panelRight);
        tbxMaxFileSize.setEnabled(false);
        createLabel(I18n.maskaPlikow.get() + ":", panelRight);
        tbxFileNamePatterns = createTextBox(panelRight);
        createLabel(I18n.wyswietlajPlikiNowszeNiz.get() + ":", panelRight);
        dbFileSince = createDateBox(panelRight);

//        chbDownloadFiles = createAndAddCheckBox(I18n.pobierajPliki.get(), null, panelRight);
        createButton(I18n.zapiszZmiany.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                updateBean();
                activatedBean.fileNamePatterns = tbxFileNamePatterns.getText();
                activatedBean.fileSince = dbFileSince.getTextBox().getText();
                callServiceToSaveData(activatedBean, new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " runTestConnection: " + getSourceName()) {

                            @Override
                            public void onSuccess(Void result) {
                                resultTest.setText("");
                                listbox.setItemText(getSelectedIndex(), activatedBean.name);
                                BIKClientSingletons.showInfo(I18n.zmienionoParametryDoPolaczenia.get() /* I18N:  */ + ": " + getSourceName());
                            }

                            @Override
                            public void onFailure(Throwable caught) {
                                super.onFailure(caught); //To change body of generated methods, choose Tools | Templates.
                                if (caught instanceof FoxyRuntimeException) {
                                    BIKClientSingletons.showWarning(caught.getMessage());
                                }
                            }
                        });
            }

        }, panelRight);
        createTestConnectionWidget(panelRight, new IParametrizedContinuationWithReturn<Void, Integer>() {

            @Override
            public Integer doIt(Void param) {
                return activatedBean.id;
            }
        });
        panelRight.setWidth("100%");
    }

    private void updateBean() {
        activatedBean.name = tbxName.getText();
        activatedBean.isActive = chbIsActive.getValue();
        activatedBean.isRemote = chbIsRemote.getValue();
        activatedBean.downloadFiles = chbDownloadFiles.getValue();
        activatedBean.path = tbxPath.getText().replace("\\", "/");
        if (!activatedBean.path.endsWith("/")) {
            activatedBean.path = activatedBean.path + "/";
        }
        activatedBean.user = tbxUser.getText();
        activatedBean.password = tbxPw.getText();
        activatedBean.domain = tbxDomain.getText();
        activatedBean.maxFileSize = BaseUtils.tryParseDouble(tbxMaxFileSize.getText());
        if (activatedBean.maxFileSize == null) {
            BIKClientSingletons.showWarning(I18n.niePrawidlowaWartosc.get() + ": " + I18n.granicaRozmiaruPlikow.get());
            return;
        }
    }

    @Override
    protected void callServiceToAddNewServer(String source, String serverName, boolean needSeparateSchedule, AsyncCallback<ConnectionParametersFileSystemBean> asyncCallback) {
        BIKClientSingletons.getService().addFileSystemConfig(serverName, asyncCallback);
    }

    @Override
    protected void callServiceToDeleteServer(int id, String serverName, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().deleteFileSystemConfig(getSourceName(), id, serverName, asyncCallback);
    }

    @Override
    protected void callServiceToTestConnection(int id, AsyncCallback<Pair<Integer, String>> asyncCallback) {
        BIKClientSingletons.getService().runFileSystemTestConnection(id, asyncCallback);
    }

    protected void callServiceToSaveData(ConnectionParametersFileSystemBean bean, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().setFileSystemConnectionParameters(bean, asyncCallback);
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_FILE_SYSTEM;
    }
}
