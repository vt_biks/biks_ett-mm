/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.erwin;

import java.util.HashMap;

/**
 *
 * @author pgajda
 */
public class ErwinKeyGroupMember extends ErwinXMLElementString{
    public ErwinKeyGroupMember() {
        propertiesMap = new HashMap<String, String>() {
            {
                put("Long_Id", null);
                put("Attribute_Ref", null);
                put("KeyGroupLongId", null);
            }
        };
    }      
    
}
