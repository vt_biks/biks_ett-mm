﻿exec sp_check_version '1.1.7.9';
go


-- poprzednia wersja w pliku alter db for v1.0.98.2 pm.sql
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_metadata]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_metadata
go

create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_extradata', 'node_id', 'author, owner, cuid, guid, ruid', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_engine, database_source, connetion_networklayer_name, user_name, server', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @optNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table', null, @optNodeFilter
  
	declare @tree_id int = (select id from bik_tree where code = 'DQC')
	declare @inact_nk_id int = (select id from bik_node_kind where code = 'DQCTestInactive'),
    @succ_nk_id int = (select id from bik_node_kind where code = 'DQCTestSuccess'),
    @fail_nk_id int = (select id from bik_node_kind where code = 'DQCTestFailed')

	declare @dqc_src_sql varchar(max) = '(select cast(dt.long_name as varchar(max)) as long_name, cast(dt.sampling_method as varchar(max)) as sampling_method, cast(dt.verified_attributes as varchar(max)) as verified_attributes,cast(dt.expected_result as varchar(max)) as expected_result,cast(dt.logging_details as varchar(max)) as logging_details, cast(dt.benchmark_definition as varchar(max)) as benchmark_definition, cast(dt.results_object as varchar(max)) as results_object, cast(dt.additional_information as varchar(max)) as additional_information, n.id as node_id
	from bik_dqc_test dt inner join bik_node n on dt.__obj_id = n.obj_id and n.node_kind_id in (' +
	cast(@inact_nk_id as varchar(20)) + ',' + cast(@succ_nk_id as varchar(20)) + ',' + cast(@fail_nk_id as varchar(20)) + ') and tree_id = ' + cast(@tree_id as varchar(20)) + '
	where n.linked_node_id is null)'
	exec sp_verticalize_node_attrs_one_metadata_table @dqc_src_sql, 'node_id', 'long_name, sampling_method, verified_attributes,expected_result,logging_details, benchmark_definition, results_object, additional_information', null, @optNodeFilter
	
	declare @ad_src_sql varchar(max) = '(select coalesce(bu.email,bu2.email) as email, coalesce(bu.phone_num,bu2.phone_num)as phone_num, ad.physical_delivery_office_name,ad.postal_code, ad.manager, ad.telephone_number, ad.description, ad.department, ad.title, ad.mobile, ad.display_name, coalesce(bu.node_id,bu2.node_id) as node_id
	from bik_active_directory ad left join bik_user bu on ad.s_a_m_account_name=bu.login_name_for_ad
	left join bik_system_user bsu on ad.s_a_m_account_name=bsu.login_name left join bik_user bu2 on bu2.id = bsu.user_id)'
	exec sp_verticalize_node_attrs_one_metadata_table @ad_src_sql, 'node_id', 'email, phone_num, physical_delivery_office_name,postal_code,manager, telephone_number, description, department, title, mobile, display_name', null, @optNodeFilter
	
end
go

delete from bik_help where help_key='56'


declare @tree_id int = (select id from bik_tree where code = 'DQC')
declare @nodeFilterSql varchar(max) = 'n.tree_id = ' + cast(@tree_id as varchar(20))
exec sp_verticalize_node_attrs_metadata @nodeFilterSql

declare @users_tree_id int = (select id from bik_tree where code = 'Users')
declare @userNodeFilterSql varchar(max) = 'n.tree_id = ' + cast(@users_tree_id as varchar(20))
exec sp_verticalize_node_attrs_metadata @userNodeFilterSql

update bik_searchable_attr set caption = 'Long name' where name = 'long_name'
update bik_searchable_attr set caption = 'Additional Information' where name = 'additional_information'
update bik_searchable_attr set caption = 'Benchmark definition' where name = 'benchmark_definition'
update bik_searchable_attr set caption = 'Expected Result' where name = 'expected_result'
update bik_searchable_attr set caption = 'Logging Details' where name = 'logging_details'
update bik_searchable_attr set caption = 'Results Object' where name = 'results_object'
update bik_searchable_attr set caption = 'Sampling Method' where name = 'sampling_method'
update bik_searchable_attr set caption = 'Verified Attributes' where name = 'verified_attributes'
--
update bik_searchable_attr set caption = 'Stanowisko' where name = 'title'
update bik_searchable_attr set caption = 'Numer telefonu' where name = 'telephone_number'
update bik_searchable_attr set caption = 'Kod pocztowy' where name = 'postal_code'
update bik_searchable_attr set caption = 'Firma' where name = 'physical_delivery_office_name'
update bik_searchable_attr set caption = 'Telefon' where name = 'phone_num'
update bik_searchable_attr set caption = 'Telefon komórkowy' where name = 'mobile'
update bik_searchable_attr set caption = 'Manager' where name = 'manager'
update bik_searchable_attr set caption = 'E-mail' where name = 'email'
update bik_searchable_attr set caption = 'Wyświetlana nazwa' where name = 'display_name'
update bik_searchable_attr set caption = 'Opis' where name = 'description'
update bik_searchable_attr set caption = 'Pion' where name = 'department'

exec sp_update_version '1.1.7.9', '1.1.8';
go
