/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.bikwidgets.JoinTypeRolesWidget;
import pl.fovis.common.JoinTypeRoles;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.UseInNodeSelectionBean;
import pl.fovis.common.UserInNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pgajda
 */
public class TreeSelectorForJoinedObjectsDialogNew extends TreeSelectorDialog/*TreeSelectorDialogBase*/ {

    protected int roleId;
    protected Set<String> treeKinds;
    protected JoinTypeRolesWidget joinTypeRoleWidget;
    protected JoinTypeRoles roleType;
    protected boolean isAlreadyJoinedReplace;
    protected String userName;
    protected String roleName;
    private static final String EDITING_JOINED_OBJS_FOR_MSG = I18n.edytujesPowiazanDlaUzytkown.get() /* I18N:  */ + ": ";
    private static final String EDITING_IN_ROLE = I18n.wRoli.get() /* I18N:  */ + ": ";

    @Override
    protected boolean isInheritToDescendantsCheckBoxVisible() {
        return true;
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        if (useLazyLoading) {
            getService().getBikAllJoinableRootTreeNodes(treeKinds, mustHideLinkedNodes(), null, null,null, callback);
        } else {
            getService().getBikAllJoinableTreeNodes(callback);
        }

    }

    @Override
    protected Collection<Integer> getTreeIdsForFiltering() {
        //ww->dynTrees: nie wiem czy tutaj nie powinno być czasem TREE_KIND_USER_ROLES, a może obie
        // te listy powinny być razem złączone???
        return BIKClientSingletons.getProperTreeIdsWithTreeKinds(treeKinds/*BIKGWTConstants.TREE_KIND_USERS*/);
    }

    @Override
    protected Widget buildOptionalRightSide() {
        VerticalPanel rightVp = new VerticalPanel();
        rightVp.setWidth("350px");
        rightVp.setHeight("100%");
        rightVp.setStyleName("EntityDetailsPane selectRoleInDialogPane");

        main.add(rightVp);

        joinTypeRoleWidget = new JoinTypeRolesWidget(nodeId, treeKinds/*null, null, null*/);
        rightVp.add(joinTypeRoleWidget.buildJointTypeRoleWidget(0, true));
        main.setCellHeight(rightVp, "100%");

        return rightVp;
    }

    public void buildAndShowDialog(int thisNodeId,
            String userName,
            Set<String> treeKinds,
            List<UserInNodeBean> usersInNodeBean,
            Map<Integer, Integer> descendantsCntInRole,
            IParametrizedContinuation<TreeNodeBean> saveCont,
            IJoinedObjSelectionManager<Integer> alreadyJoinedManager,
            UseInNodeSelectionBean param) {
        this.nodeId = thisNodeId;
        this.userName = userName;
        this.treeKinds = treeKinds;
        this.roleId = param.roleForNodeId;
        this.roleName = param.roleForNodeName;

        super.buildAndShowDialog(nodeId, saveCont, alreadyJoinedManager);
        joinTypeRoleWidget.buildRolePanelWithData(param.roleForNodeId, true);

    }

    @Override
    protected boolean doActionBeforeClose() {
        if (joinTypeRoleWidget.getJoinTypeRoleSelectedRb() == null) {
            Window.alert(I18n.wybierzTypPowiazania.get() /* I18N:  */);
            return false;
        }

        return super.doActionBeforeClose();
    }

    @Override
    protected void doAction() {

        JoinTypeRoles typeSelectedRb = joinTypeRoleWidget.getJoinTypeRoleSelectedRb();

        roleType = (JoinTypeRoles.PrimaryOldNotReplace == typeSelectedRb && joinTypeRoleWidget.primaryReplace.getValue())
                ? JoinTypeRoles.SelectedPrimaryOldReplace : typeSelectedRb;
        isAlreadyJoinedReplace = joinTypeRoleWidget.isUserInRoleSelected();

        if (isInMultiSelectMode()) {
            BIKClientSingletons.showInfo(I18n.zapisywaniePowiazanProszeCzekac.get() /* I18N:  */ + "...");
            Collection<Integer> alreadyJoinedObjIdsTmp = alreadyJoinedManager.getJoinedObjIds();
            for (int dO : directOverride.keySet()) {
                if (alreadyJoinedObjIdsTmp.contains(dO)) {
                    alreadyJoinedObjIdsTmp.remove(dO);
                }
            }
            if (!BaseUtils.isMapEmpty(directOverride)) {
                getService().updateUserRolesForNode(nodeId, mustHideLinkedNodes(),
                        directOverride, subNodesOverride, roleId, treeKinds, roleType == JoinTypeRoles.Auxiliary,
                        roleType, isAlreadyJoinedReplace ? alreadyJoinedObjIdsTmp : null, new StandardAsyncCallback<Boolean>() {
                            public void onSuccess(Boolean result) {
                                BIKClientSingletons.showInfo(I18n.powiazaniaZapisaneOdswiezanie.get() /* I18N:  */);
                                if (result) {
                                    refreshAndRedisplayData();
                                } else {
                                    alreadyJoinedManager.refreshJoinedNodes(nodeId);
                                }
                            }
                        });
            }
        } else {
            saveCont.doIt(selectedNode);
        }

    }

    protected void refreshAndRedisplayData() {
        BIKClientSingletons.invalidateTabData(BIKGWTConstants.TREE_CODE_USER_ROLES);
    }

    @Override
    protected void setHeadLabel() {
        if (headLbl != null) {
            headLbl.setText(EDITING_JOINED_OBJS_FOR_MSG + userName + " " + EDITING_IN_ROLE + roleName);
        }
    }

    @Override
    protected boolean isGrouppingByMenuEnabled() {
        return true;
    }
}
