/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.biadmin;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashSet;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.BikPageAdminConfigBase;
import pl.fovis.common.ConnectionParametersAuditBean;
import pl.fovis.common.ConnectionParametersBIAdminBean;
import pl.fovis.common.ConnectionParametersSapBoBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class BIAConfigPage extends BikPageAdminConfigBase<ConnectionParametersBIAdminBean> {

    //
    protected CheckBox chbIsActive;
    protected PushButton btnSave;
    protected PushButton btnTestConnection;
    protected TextBox tbxHost;
    protected TextBox tbxUser;
    protected TextBox tbxPassword;
//    protected CheckBox chbPassword;
    protected ListBox lbxServers;
    protected TabPanel mainTb;
    protected VerticalPanel mainVp = new VerticalPanel();

    protected ConnectionParametersBIAdminBean bean;
    protected BIAuditConfigWidget ba = new BIAuditConfigWidget();

    @Override
    protected void buildInnerWidget() {
        mainVp.setSpacing(5);
        //
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
            }
        }, false, mainVp);
        createLine(mainVp);
        createLabel(I18n.serwer.get(), mainVp);
        tbxHost = createTextBox(mainVp);
        createLabel(I18n.uzytkownik.get(), mainVp);
        tbxUser = createTextBox(mainVp);
        createLabel(I18n.haslo.get(), mainVp);
        tbxPassword = createPasswordTextBox(mainVp);
//        chbPassword = createCheckBox(I18n.ukryjHaslo.get(), new ValueChangeHandler<Boolean>() {
//
//            public void onValueChange(ValueChangeEvent<Boolean> event) {
//                tbxPassword.getElement().setAttribute("type" /* I18N: no */, chbPassword.getValue() ? "password" /* I18N: no */ : "text" /* I18N: no */);
//            }
//        }, true, mainVp);
        createLabel(I18n.konfiguracjaSerwera.get(), mainVp);
        lbxServers = createListBoxEx(new HashSet<String>(), mainVp);
        createLine(mainVp);
        btnSave = createButton(I18n.zapisz.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                bean.isActive = chbIsActive.getValue() ? 1 : 0;
                bean.host = tbxHost.getText();
                bean.user = tbxUser.getText();
                bean.password = tbxPassword.getText();
                bean.serverBoId = lbxServers.getSelectedValue();
                BIKClientSingletons.getService().setBIAdminConnectionParameters(bean, new StandardAsyncCallback<Void>("Error in setBIAdminConnectionParameters") {

                    @Override
                    public void onSuccess(Void result) {
                        BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get());
                    }
                });
            }
        }, mainVp);
    }

    @Override
    public String getId() {
        return "admin:biadmin:cfg";
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        BIKClientSingletons.getService().getBIAdminConnectionParameters(new StandardAsyncCallback<Pair<List<ConnectionParametersSapBoBean>, ConnectionParametersBIAdminBean>>("Error in getBIAdminConnectionParameters") {

            @Override
            public void onSuccess(Pair<List<ConnectionParametersSapBoBean>, ConnectionParametersBIAdminBean> result) {
                bean = result.v2;
                tbxHost.setText(bean.host);
                tbxUser.setText(bean.user);
                tbxPassword.setText(bean.password);
                chbIsActive.setValue(bean.isActive == 1);
                setWidgetsEnabled(chbIsActive.getValue());
                lbxServers.clear();
                int index = 0;
                for (ConnectionParametersSapBoBean server : result.v1) {
                    lbxServers.addItem(server.name, String.valueOf(server.id));
                    if (!BaseUtils.isStrEmptyOrWhiteSpace(result.v2.serverBoId) && server.id == BaseUtils.tryParseInt(result.v2.serverBoId)) {
                        lbxServers.setSelectedIndex(index);
                    }
                    index++;
                }
            }
        });

        BIKClientSingletons.getService().getBIAuditDatabaseConnectionParameters(new StandardAsyncCallback<ConnectionParametersAuditBean>("Error in getBIAuditDatabaseConnectionParameters") {

            @Override
            public void onSuccess(ConnectionParametersAuditBean result) {
                ba.populateWidgetWithData(result);
            }
        });
    }

    @Override
    public boolean isPageEnabled() {
        return super.isPageEnabled() && BIKClientSingletons.isBIAdminEnabled();
    }

    @Override
    public void populateWidgetWithData(ConnectionParametersBIAdminBean data) {
        //NO-OP
//        activatePage(false, null, false); //nie jest uzywany
    }

    @Override
    public Widget createMainWidget() {
        return buildWidget();
    }

    @Override
    public Widget buildWidget() {
        mainTb = new TabPanel();
        mainTb.setAnimationEnabled(true);
        mainTb.setHeight("100%");
        mainTb.setWidth("100%");
        mainTb.addStyleName("mainConnConf");
        buildInnerWidget();
        mainTb.add(mainVp, I18n.serwerBO.get());
        mainTb.add(ba.buildWidget(), I18n.bazaAudytu.get());
        mainTb.selectTab(0);
        main.add(mainTb);
        return main;
    }
}
