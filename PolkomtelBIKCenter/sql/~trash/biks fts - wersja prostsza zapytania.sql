set nocount on


print cast(sysdatetime() as varchar(23)) + ': start'

select top 20 bn.id as node_id,
        bn.name,
        bnk.code as node_kind_code,
        bnk.caption as node_kind_caption,
        bt.name as tree_name,
        bt.code as tree_code,
        bn.vote_sum,
        bn.vote_cnt
from
(
select distinct top 20 av.search_weight, av.node_id
from 
containstable(bik_searchable_attr_val, *, '"id_konto"') sr inner loop 
join
bik_searchable_attr_val av with (index(PK_bik_searchable_attr_val)) 
on av.id = sr.[key]
where    --and
        av.tree_id in --        (1,2) --        
        (1,2,3,4,8,12,15,14,16,6,7,13,18)
         and 
        av.node_kind_id in --        (56,39) --        
        (14,42,43,10,62,61,40,54,55,9,56,53,41,39)
        --and case when av.value like '%id[_]konto%' then 1 else 0 end = 1
        and av.value like '%id[_]konto%'
order by av.search_weight, av.node_id --av.tree_id, av.node_kind_id, av.node_id
) sr inner join bik_node bn on sr.node_id = bn.id
inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
        inner join bik_tree bt on bn.tree_id = bt.id
order by sr.search_weight--, bn.name

print cast(sysdatetime() as varchar(23)) + ': koniec'


---------------------------------
---------------------------------
---------------------------------
-- wersja bez wyszukiwania (full dla wybranych drzew / kind�w)


print cast(sysdatetime() as varchar(23)) + ': start'

select top 20 bn.id as node_id,
        bn.name,
        bnk.code as node_kind_code,
        bnk.caption as node_kind_caption,
        bt.name as tree_name,
        bt.code as tree_code,
        bn.vote_sum,
        bn.vote_cnt
from
bik_node bn 
inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
        inner join bik_tree bt on bn.tree_id = bt.id
where bn.is_deleted = 0 and (bn.linked_node_id is null or bn.disable_linked_subtree <> 0)
        and bn.tree_id in --        (1,2) --        
        (1,2,3,4,8,12,15,14,16,6,7,13,18)
         and 
        bn.node_kind_id in --        (56,39) --        
        (14,42,43,10,62,61,40,54,55,9,56,53,41,39)
order by bn.name --bn.vote_cnt, bn.name


print cast(sysdatetime() as varchar(23)) + ': koniec'


------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
-- starsza wersja poni�ej
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------


set nocount on

declare @rc int

print cast(sysdatetime() as varchar(23)) + ': start'

        select top 20 bn.id as node_id,
        bn.name,
        --bnk.code as node_kind_code,
        --bnk.caption as node_kind_caption,
        --bt.name as tree_name,
        --bt.code as tree_code,
        bn.vote_sum,
        bn.vote_cnt
        from
        bik_node bn
            inner join
            (select node_id,
            --sum(sr.[rank] /* * coalesce(ap.search_weight, 1) */) 
            1 as sum_combined_rank,
            --max(sr.[rank] /* * coalesce(ap.search_weight, 1) */) 
            1 as max_combined_rank,
            /*sum(coalesce(ap.search_weight, 1))*/ 1 as sum_search_weight,
            /*max(coalesce(ap.search_weight, 1))*/ 1 as max_search_weight
            --
            --,max(case when av.value like '%id[_]konto%' then 1 else 0 end) as has_match
            from bik_searchable_attr_val av 
            --inner join          containstable(bik_searchable_attr_val, *, '"id konto"') sr on av.id = sr.[key]
            --inner join bik_searchable_attr a on av.attr_id = a.id
            --left join bik_searchable_attr_props ap on a.name = ap.name
where   contains(*, '"id konto"') and
        av.tree_id in (1,2,3,4,8,12,15,14,16,6,7,13,18)
         and 
        av.node_kind_id in (14,42,43,10,62,61,40,54,55,9,56,53,41,39)
        --and av.value like '%id[_]konto%'
            group by node_id
            ) sr on sr.node_id = bn.id
        inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
        inner join bik_tree bt on bn.tree_id = bt.id
--where --bn.is_deleted = 0 and (bn.linked_node_id is null or bn.disable_linked_subtree <> 0)
        --and 
        --
        where --sr.has_match = 1
        --and bn.tree_id in (1,2)
        --and bn.node_kind_id in (56,39)
--and 
bn.tree_id in (1,2,3,4,8,12,15,14,16,6,7,13,18)
         and 
        bn.node_kind_id in (14,42,43,10,62,61,40,54,55,9,56,53,41,39)          
order by max_search_weight desc, bn.search_rank + bnk.search_rank desc, bn.vote_sum desc, sum_combined_rank desc



set @rc = @@rowcount
print cast(sysdatetime() as varchar(23)) + ': koniec, wierszy: ' + cast(@rc as varchar(20))
