/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.morfologik.MorfologikAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;

/**
 *
 * @author pmielanczuk
 */
public class FtsByLucene implements ISlicer {

    public static final String INDEX_PATH = "c:/temp/lucene-idx";
    public static final String COLLECTIVE_FIELD_NAME = "_all_";
    public static final String NODE_ID_FIELD_NAME = "_objId_";
    public static final String IS_DELETED_FIELD_NAME = "is_deleted";

    protected IndexWriter writer;

    public static Analyzer createAnalyzer() {
        return new MorfologikAnalyzer();
    }

    public void init() {
        try {
            initInner();
        } catch (IOException ex) {
            throw new LameRuntimeException("exception in init", ex);
        }
    }

    protected void initInner() throws IOException {

        boolean create = true;

        Directory dir = FSDirectory.open(Paths.get(INDEX_PATH));
        Analyzer analyzer = createAnalyzer();
        IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

        if (create) {
            // Create a new index in the directory, removing any
            // previously indexed documents:
            iwc.setOpenMode(OpenMode.CREATE);
        } else {
            // Add new documents to an existing index:
            iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
        }

        // Optional: for better indexing performance, if you
        // are indexing many documents, increase the RAM
        // buffer.  But if you do this, increase the max heap
        // size to the JVM (eg add -Xmx512m or -Xmx1g):
        //
        iwc.setRAMBufferSizeMB(128.0);

        writer = new IndexWriter(dir, iwc);
    }

    public void close() {
        // NOTE: if you want to maximize search performance,
        // you can optionally call forceMerge here.  This can be
        // a terribly costly operation, so generally it's only
        // worth it when your index is relatively static (ie
        // you're done adding documents to it):
        //
        // writer.forceMerge(1);

        try {
            writer.close();
        } catch (IOException ex) {
            throw new LameRuntimeException("exception in finalize", ex);
        }
    }

    protected void indexDoc(int objId, Map<String, Object> attrVals) {
        Document doc = new Document();

//        Field fldObjId = new IntField(NODE_ID_FIELD_NAME, objId, Field.Store.YES);
        Field fldObjId = new TextField(NODE_ID_FIELD_NAME, Integer.toString(objId), Field.Store.YES);
        doc.add(fldObjId);

//        boolean hasVals = false;
        StringBuilder sb = new StringBuilder();

        for (Entry<String, Object> e : attrVals.entrySet()) {
            String attrName = e.getKey();
            Object attrVal = e.getValue();

            if (attrVal == null) {
                continue;
            }

            String attrValStr = attrVal.toString();

            if (BaseUtils.isStrEmptyOrWhiteSpace(attrValStr)) {
                continue;
            }

//            hasVals = true;
            if (sb.length() != 0) {
                sb.append(" ");
            }
            sb.append(attrValStr);

            Field attrField = new TextField(attrName, attrValStr, Field.Store.NO);
            doc.add(attrField);
        }

//        if (hasVals) {
        if (sb.length() > 0) {
            try {
                doc.add(new TextField(COLLECTIVE_FIELD_NAME, sb.toString(), Field.Store.YES));
                writer.addDocument(doc);
            } catch (IOException ex) {
                throw new LameRuntimeException("error in indexDoc with id=" + objId, ex);
            }
        }
    }

    @Override
    public void addOrUpdateObject(int objId, Map<String, Object> attrVals) {
        indexDoc(objId, attrVals);
    }
}
