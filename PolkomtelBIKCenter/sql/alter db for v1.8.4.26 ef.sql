﻿exec sp_check_version '1.8.4.26';
GO

ALTER TABLE bik_system_user ADD can_save_pict INT DEFAULT 0
GO

UPDATE bik_system_user
SET can_save_pict = '0'
GO

exec sp_update_version '1.8.4.26'
	,'1.8.4.27';
GO