/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.logging;

import java.util.HashSet;
import java.util.Set;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class BaseLameLogger implements ILameLogger {

    public static final boolean IsLameLoggerEnabled = true;
    private String fullName;
    private String simpleName;
    private LameLogLevel level;
    private Set<LameLogLevel> abusedLevels;
    private long lastLevelCheck = 0;

    public BaseLameLogger(//LameLogLevel level,
            String fullName) {
        //this.level = level;
        this.fullName = fullName;
        this.simpleName = LameLoggerFactory.getLoggingClassName(fullName);
    }

    protected void recheckLevel() {
        long cfgLastMod = LameLoggerFactory.getConfigLastModified();
        //System.out.println("NAME=" + simpleName + ": " + "rechecking, cfgLastMod=" + cfgLastMod + ", lastLevelCheck=" + lastLevelCheck);
        if (level == null || cfgLastMod != lastLevelCheck) {
            lastLevelCheck = cfgLastMod;
            this.level = LameLoggerFactory.getConfiguredLoggerLevel(this.fullName);
            //System.out.println("NAME=" + simpleName + ": " + "new level after rechecking:" + this.level);
        } else {
            //System.out.println("NAME=" + simpleName + ": " + "level without rechecking:" + this.level);
        }
    }

    public LameLogLevel getLoggerLevel() {
        recheckLevel();
        return level;
    }

    public String getSimpleLoggerName() {
        return simpleName;
    }

    public void logAtLevel(LameLogLevel level, String msg) {
        if (!IsLameLoggerEnabled) {
            return;
        }
        if (!isEnabledAtLevel(level)) {
            logAbuse(level, msg);
        } else {
            innerLog(level, msg);
        }
    }

    public void logAtLevel(LameLogLevel level, String msg, Throwable cause) {
        if (!IsLameLoggerEnabled) {
            return;
        }
        if (!isEnabledAtLevel(level)) {
            logAbuse(level, msg, cause);
        } else {
            innerLog(level, msg, cause);
        }
    }

    public void debug(String msg) {
        if (!IsLameLoggerEnabled) {
            return;
        }
        logAtLevel(LameLogLevel.Debug, msg);
    }

    public void info(String msg) {
        if (!IsLameLoggerEnabled) {
            return;
        }
        logAtLevel(LameLogLevel.Info, msg);
    }

    public void stage(String msg) {
        if (!IsLameLoggerEnabled) {
            return;
        }
        logAtLevel(LameLogLevel.Stage, msg);
    }

    public void warn(String msg) {
        if (!IsLameLoggerEnabled) {
            return;
        }
        logAtLevel(LameLogLevel.Warn, msg);
    }

    public void warn(String msg, Throwable cause) {
        if (!IsLameLoggerEnabled) {
            return;
        }
        logAtLevel(LameLogLevel.Warn, msg, cause);
    }

    public void error(String msg) {
        if (!IsLameLoggerEnabled) {
            return;
        }
        logAtLevel(LameLogLevel.Error, msg);
    }

    public void error(String msg, Throwable cause) {
        if (!IsLameLoggerEnabled) {
            return;
        }
        logAtLevel(LameLogLevel.Error, msg, cause);
    }

    public void fatal(String msg) {
        if (!IsLameLoggerEnabled) {
            return;
        }
        logAtLevel(LameLogLevel.Fatal, msg);
    }

    public void fatal(String msg, Throwable cause) {
        if (!IsLameLoggerEnabled) {
            return;
        }
        logAtLevel(LameLogLevel.Fatal, msg, cause);
    }

    public boolean isEnabledAtLevel(LameLogLevel level) {
        if (!IsLameLoggerEnabled) {
            return false;
        }
        recheckLevel();
        boolean res = (this.level == null || this.level.ordinal() <= level.ordinal())
                && LameLoggerFactory.currentMinLogLevel().ordinal() <= level.ordinal();
//        System.out.println("logger: " + fullName + ", for level: " + level + " is "
//                + (res ? "enabled" : "disabled"));
        return res;
    }

    public boolean isDebugEnabled() {
        if (!IsLameLoggerEnabled) {
            return false;
        }
        return isEnabledAtLevel(LameLogLevel.Debug);
    }

    public boolean isInfoEnabled() {
        if (!IsLameLoggerEnabled) {
            return false;
        }
        return isEnabledAtLevel(LameLogLevel.Info);
    }

    public boolean isStageEnabled() {
        if (!IsLameLoggerEnabled) {
            return false;
        }
        return isEnabledAtLevel(LameLogLevel.Stage);
    }

    public boolean isWarnEnabled() {
        if (!IsLameLoggerEnabled) {
            return false;
        }
        return isEnabledAtLevel(LameLogLevel.Warn);
    }

    public boolean isErrorEnabled() {
        if (!IsLameLoggerEnabled) {
            return false;
        }
        return isEnabledAtLevel(LameLogLevel.Error);
    }

    public <T extends Throwable> void errorAndThrow(String msg, T ex) throws T {
        if (IsLameLoggerEnabled) {
            if (isErrorEnabled()) {
                error(msg, ex);
            }
        }
        throw ex;
    }

    public <T extends Throwable> void errorAndThrowNew(String exceptionMsg, T cause) {
        errorAndThrow(exceptionMsg + ", cause:\n" + cause, new LameRuntimeException(exceptionMsg, cause));
    }

    public <T extends Throwable> void errorAndThrowNew(String exceptionMsg) {
        errorAndThrow(exceptionMsg, new LameRuntimeException(exceptionMsg));
    }

    protected String getAbuseMsg(LameLogLevel level, String msg) {
        return "logger." + level.name() + " disabled, but called with msg: " + msg
                + ", check logger owner class because subsequent abuses at this level will be suppressed";
    }

    protected void logAbuse(LameLogLevel level, String msg) {
        if (isErrorEnabled() && addAbusedLevel(level)) {
            error(getAbuseMsg(level, msg));
        }
    }

    // returns true for newly abused level (i.e. false if it was already abused earlier)
    protected boolean addAbusedLevel(LameLogLevel level) {
        if (abusedLevels == null) {
            abusedLevels = new HashSet<LameLogLevel>();
        } else if (abusedLevels.contains(level)) {
            return false;
        }
        return abusedLevels.add(level);
    }

    protected void logAbuse(LameLogLevel level, String msg, Throwable cause) {
        if (isErrorEnabled() && addAbusedLevel(level)) {
            error(getAbuseMsg(level, msg), cause);
        }
    }

    protected void innerLog(LameLogLevel level, String msg) {
        innerLog(level, msg, null);
    }

    protected void innerLog(LameLogLevel level, String msg, Throwable cause) {
        //ww: zbędne, teraz mamy StackTraceUtil.getCustomStackTrace
        // które jest użyte w LameLoggerMsg() - inicjalizacja causeTxt
//        if (cause != null) {
//            cause.printStackTrace();
//        }
        LameLoggerFactory.sendMsgToOutput(new LameLoggerMsg(level, this, msg, cause));
    }

    public String getFullLoggerName() {
        return fullName;
    }

    @Deprecated
    public ILameLogger setLevelForDebugOnly(LameLogLevel level) {
        LameLoggerFactory.loggerSystemWarning("logger[" + fullName
                + "].setLevelForDebugOnly(" + level
                + ") - this call should be removed right after debugging!");
        LameLoggerFactory.setOneConfigEntry(this.fullName, level);
        return this;
    }
}
