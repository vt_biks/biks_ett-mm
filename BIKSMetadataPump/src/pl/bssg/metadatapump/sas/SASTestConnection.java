/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sas;

import pl.bssg.metadatapump.MetadataTestConnectionBase;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class SASTestConnection extends MetadataTestConnectionBase {
    
    public SASTestConnection(MssqlConnectionConfig mcfg) {
        super(mcfg);
    }

    @Override
    public Pair<Integer, String> testConnection() {
        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Error: Connector for SAS is in the process of implementation");
//        return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, "OK");
    }
    
}
