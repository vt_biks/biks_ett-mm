/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

/**
 *
 * @author wezyr
 */
public class JoinedObjBean extends JoinedObjBasicInfoBean implements IBeanWithDstNodeId, IHasSimilarityScore {

    public int id;
//    public int srcId;
//    public int dstId;
//    public boolean inheritToDescendants;
//    //ww: type == false -> ręczne, można usuwać, type == true -> fixed (z metadanych)
//    public boolean type;
    public String dstCode;
    public String dstType;
    public String dstName;
    public boolean dstHasChildren;
    public String tabId;
    public String treeKind;
    public String descrPlain;
    public Double similarityScore;
    public String attributes;
    public String nodeMenuPath;
    public String dstTreeName;
    public boolean isFromHyperlink;

    @Override
    public Integer getDstNodeId() {
        return dstId;
    }

    public Integer getSrcNodeId() {
        return srcId;
    }

    @Override
    public Double getSimilarityScore() {
        return similarityScore;
    }
}
