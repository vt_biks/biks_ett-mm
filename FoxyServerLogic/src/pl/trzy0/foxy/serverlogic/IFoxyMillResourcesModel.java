/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.serverlogic;

import pl.trzy0.foxy.glass.Glass;

/**
 *
 * @author wezyr
 */
public interface IFoxyMillResourcesModel {
    public String getText(String key);
    public String glassAsText(Glass g);
}
