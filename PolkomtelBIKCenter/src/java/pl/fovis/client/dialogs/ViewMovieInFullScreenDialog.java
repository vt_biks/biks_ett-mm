/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.client.BIKClientSingletons;

/**
 *
 * @author bfechner
 */
public abstract class ViewMovieInFullScreenDialog extends ViewSomethingInBIKSDialog {

    @Override
    public void hideDialog() {
        super.hideDialog();
        BIKClientSingletons.setBrowserTitle();
    }

    @Override
    public void buildAndShowDialog() {
        super.buildAndShowDialog();
        BIKClientSingletons.setBrowserTitle();
    }
}
