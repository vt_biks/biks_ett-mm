/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.dqc;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class DQCRequestBean implements Serializable {

    public int idRequest; // INTEGER NOT NULL,
    public int idAliasSql; // INTEGER NOT NULL,
    public int idAliasMdx; // INTEGER NOT NULL,
    public int idTest; // INTEGER NOT NULL,
    public int idPerson; // INTEGER NOT NULL,
    public int idRequestState; // INTEGER NOT NULL,
    public Long caseCount; // BIGINT NULL,
    public Long errorCount; // BIGINT NULL,
    public Double errorPercentage; // DECIMAL(18,8) NULL,
    public Boolean errorLevelExceeded; // BIT NULL,
    public String comments; // TEXT NULL,
    public Boolean passed; // BIT NULL,
    public String log; // TEXT NULL,
    public Date startTimestamp; // DATETIME NOT NULL,
    public Date endTimestamp; // DATETIME NULL,
    public Date lastTrialTimestamp; // DATETIME NULL,
    public Integer lastTrialCount; // INTEGER NULL,
    public boolean isManual; // BIT NOT NULL,
    public Integer finishedSteps; // INTEGER NULL DEFAULT 0,
    public Integer numberOfSteps; // INTEGER NULL,
    public boolean deleted; // BIT NOT NULL DEFAULT 0,
    public String testObjId;// varchar(max) not null,
	//
    public boolean isSuccess;
}
