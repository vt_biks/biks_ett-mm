/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.db.setdbperformance;

import commonlib.GlobalThreadStopWatch;

/**
 *
 * @author pmielanczuk
 */
public class ExecCmdStep extends Step {

    public static long totalRowCnt = 0;
    String cmdTxt;

    public ExecCmdStep(String cmdTxt) {
        this.cmdTxt = cmdTxt;
    }

    public void run() {
        if (true) {
            return;
        }

        GlobalThreadStopWatch.startProcess("step execCmd run", true);
        try {
            DaoSingleton.execCmd(cmdTxt);
        } finally {
            GlobalThreadStopWatch.endProcess();
        }
    }
}
