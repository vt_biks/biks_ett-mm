/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.common;

/**
 *
 * @author pmielanczuk
 */
public enum DbAutoUpgradeStatus {

    Clear, Unrecognized, InProgress, Success, Failure
}
