/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.mltx;

/**
 *
 * @author lbiegniewski
 */
public enum MultiXRegistrationStatus {

    NEW(0, "New registration."),
    MAIL_SENT(1, "Mail sent successfully."),
    MAIL_ERROR(-1, "Error during mail processing."),
    LINK_CLICKED(2, "Link has been clicked."),
    TOKEN_EXPIRED_ERROR(-2, "Token has expired."),
    FINISHED(3, "Registration successfully finished.");
    public Integer id;
    public String description;

    private MultiXRegistrationStatus(Integer id, String description) {
        this.id = id;
        this.description = description;
    }
}
