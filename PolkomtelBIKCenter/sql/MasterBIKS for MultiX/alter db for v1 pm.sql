------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- 1. poprawka do sp_mltx_check_version
--    - zerwie po��czenie lub rzuci szybciej b��dem
-- 2. powi�zanie bazy z rejestracj�
--    - nowe pole registration_id w mltx_database
-- 3. nowe pola w mltx_registration
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_mltx_check_version '1'
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_mltx_check_version]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_mltx_check_version]
GO

create procedure [dbo].[sp_mltx_check_version] (@ver varchar(255)) as
begin	
    set nocount on
    
	declare @version varchar(255)
	select @version = val from mltx_app_prop where name = 'multiXDbVer';
	if(coalesce(@version,'skljsdlfjs') != coalesce(@ver, 'asdsadsa'))
	begin
	    declare @txt nvarchar(4000) = N'!!! Wrong version number: ' + @ver + 
	      '. Current version is ' + @version + '. !!!'
	    
	    while 1 = 1 begin
	    print @txt
	    
		raiserror (@txt,
				20,
				-1) with LOG;

		raiserror (@txt,
				15,
				-1) with NOWAIT;
				
		print 'you must manually stop execution of this script!!!'
		print 'prosz� przerwa� dalsze wykonanie tego skryptu r�cznie!!!'
		
		select @txt
		select 'you must manually stop execution of this script!!!'
		select 'prosz� przerwa� dalsze wykonanie tego skryptu r�cznie!!!'
		
		WAITFOR DELAY '00:00:10';
	    end
	end	
end
GO


------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

alter table mltx_database add registration_id int null
go

update mltx_database set registration_id = 
(select r.id from mltx_registration r inner join mltx_login l on r.email = l.login
  where l.database_id = mltx_database.id and r.status = 3)
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

alter table mltx_registration add
  first_name varchar(200) null,
  last_name varchar(200) null,
  phone_num varchar(30) null,
  company varchar(200) null,
  job_title varchar(200) null,
  country varchar(200) null,
  city varchar(200) null
go 

update mltx_registration set company = substring(email, charindex('@', email) + 1, 8000)
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_mltx_update_version '1', '1.1'
go
