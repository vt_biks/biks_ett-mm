/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.profile;

import commonlib.LameUtils;
import commonlib.ftp.FTP4JFoxyFTPClient;
import commonlib.ftp.IFoxyFTPClient;
import commonlib.ftp.IFoxyFTPFile;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import pl.bssg.metadatapump.AdhocPartialBeanCollector;
import pl.bssg.metadatapump.GenericNodeBean;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.PumpUtils;
import pl.bssg.metadatapump.common.ProfileBean;
import pl.bssg.metadatapump.common.ProfileFileBean;
import pl.bssg.metadatapump.common.PumpConstants;
import simplelib.BaseUtils;
import simplelib.CSVReader;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class ProfilePump extends MetadataPumpBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    public final static String TYPE_LIBRARY = "ProfileLibrary";
    public final static String TYPE_FILE = "ProfileFile";
    public final static String TYPE_ITEM = "ProfileDataItem";
    public final static String FILE_PREFIX = "DBTBL1-";
    public final static String ITEM_PREFIX = "DBTBL1D-";
//    public final static String LOCAL_FOLDER = BaseUtils.ensureStrHasSuffix(System.getProperty("java.io.tmpdir"), "\\", true) + "Profile";
    protected AdhocPartialBeanCollector<GenericNodeBean> beanCollector;
    protected AdhocPartialBeanCollector<ProfileBean> beanExtradataCollector;
    protected AdhocPartialBeanCollector<ProfileFileBean> beanFileExtradataCollector;
    protected String localFolder;
    protected Map<String, List<String>> alreadyAddedObjs = new HashMap<String, List<String>>();

    public static List<ProfileBean> createListFromItemMap(List<Map<String, String>> content) {
        List<ProfileBean> list = new ArrayList<ProfileBean>();
        // %LIBS, FID, DI, NOD, LEN, DFT, DOM, TBL, PTN, XPO, XPR, TYP, DES, ITP, MIN, MAX, DEC, REQ, CMP, SFD, SFD1, SFD2, SFP, SFT, SIZ, DEL, POS, RHD, SRL, CNV, LTD, USER, MDD, VAL4EXT, DEPREP, DEPOSTP, NULLIND, MDDFID, VALIDCMP
        for (Map<String, String> row : content) {
            Date ltd;
            try {
                ltd = new SimpleDateFormat("MM/dd/yy", Locale.ENGLISH).parse(row.get("LTD"));
            } catch (ParseException ex) {
                ltd = null;
            }
            list.add(new ProfileBean(row.get("%LIBS"), row.get("FID"), row.get("DI"), row.get("NOD"), BaseUtils.tryParseInteger(row.get("LEN")), row.get("DFT"), row.get("DOM"), row.get("TBL"), row.get("PTN"), row.get("XPO"), row.get("XPR"), row.get("TYP"), row.get("DES"), row.get("ITP"), row.get("MIN"), row.get("MAX"), BaseUtils.tryParseInteger(row.get("DEC")), row.get("REQ"), row.get("CMP"), row.get("SFD"), BaseUtils.tryParseInteger(row.get("SFD1")), BaseUtils.tryParseInteger(row.get("SFD2")), BaseUtils.tryParseInteger(row.get("SFP")), row.get("SFT"), BaseUtils.tryParseInteger(row.get("SIZ")), BaseUtils.tryParseInteger(row.get("DEL")), BaseUtils.tryParseInteger(row.get("POS")), row.get("RHD"), row.get("SRL"), BaseUtils.tryParseInteger(row.get("CNV")), ltd, row.get("USER"), row.get("MDD"), row.get("VAL4EXT"), row.get("DEPREP"), row.get("DEPOSTP"), row.get("NULLIND"), row.get("MDDFID"), row.get("VALIDCMP")));
        }
        return list;
    }

    protected List<ProfileFileBean> createListFromFileMap(List<Map<String, String>> content) {
        List<ProfileFileBean> list = new ArrayList<ProfileFileBean>();
        // %LIBS FID GLOBAL AKEY1 AKEY2 AKEY3 AKEY4 AKEY5 AKEY6 AKEY7 DEL SYSSN NETLOC PARFID MPLCTDD DFTDES DFTORD DFTHDR DFTDES1 LTD USER FILETYP EXIST EXTENDLENGTH FSN FDOC QID1 ACCKEYS VAL4EXT PREDAEN SCREEN RFLAG DFLAG UDACC UDFILE FPN UDPRE UDPOST PUBLISH ZRSFILE GLREF RECTYP PTRUSER PTRTLD LOG ARCHFILES ARCHKEY PTRTIM PTRUSERU PTRTLDU PTRTIMU LISTDFT LISTREQ DES
        for (Map<String, String> row : content) {
            Date ltd;
            try {
                ltd = new SimpleDateFormat("MM/dd/yy", Locale.ENGLISH).parse(row.get("LTD"));
            } catch (ParseException ex) {
                ltd = null;
            }
            list.add(new ProfileFileBean(row.get("%LIBS"), row.get("FID"), row.get("GLOBAL"), row.get("AKEY1"), row.get("AKEY2"), row.get("AKEY3"), row.get("AKEY4"), row.get("AKEY5"), row.get("AKEY6"), row.get("AKEY7"), BaseUtils.tryParseInteger(row.get("DEL")), row.get("SYSSN"), BaseUtils.tryParseInteger(row.get("NETLOC")), row.get("PARFID"), row.get("MPLCTDD"), row.get("DFTDES"), row.get("DFTORD"), row.get("DFTHDR"), row.get("DFTDES1"), ltd, row.get("USER"), BaseUtils.tryParseInteger(row.get("FILETYP")), BaseUtils.tryParseInteger(row.get("EXIST")), row.get("EXTENDLENGTH"), row.get("FSN"), row.get("FDOC"), row.get("QID1"), row.get("ACCKEYS"), row.get("VAL4EXT"), row.get("PREDAEN"), row.get("SCREEN"), row.get("RFLAG"), row.get("DFLAG"), row.get("UDACC"), row.get("UDFILE"), row.get("FPN"), row.get("UDPRE"), row.get("UDPOST"), row.get("PUBLISH"), row.get("ZRSFILE"), row.get("GLREF"), BaseUtils.tryParseInteger(row.get("RECTYP")), row.get("PTRUSER"), row.get("PTRTLD"), row.get("LOG"), row.get("ARCHFILES"), row.get("ARCHKEY"), row.get("PTRTIM"), row.get("PTRUSERU"), row.get("PTRTLDU"), row.get("PTRTIMU"), row.get("LISTDFT"), row.get("LISTREQ"), row.get("DES")));
        }
        return list;
    }

    @Override
    protected Pair<Integer, String> startPump() {
        try {
            localFolder = BaseUtils.ensureStrHasSuffix(dirForUpload.replace("/", "\\"), "\\", true) + "Profile";
//            System.out.println("localFolder = " + localFolder);
            createCollectors();
            execNamedCommandWithCommit("deleteProfileTempTables");
            pumpLogger.pumpLog("Downloading files from FTP", 10);
            Pair<List<String>, List<String>> localFilesNames = downloadFilesFromFTP();
            pumpLogger.pumpLog("Insert data to BIKS database", 60);
            for (String localItemFileName : localFilesNames.v1) {
                getItemMetadata(localItemFileName);
            }
            for (String localFileFileName : localFilesNames.v2) {
                getFileMetadata(localFileFileName);
            }
//            System.out.println("delete localFolder = " + localFolder);
            deleteLocalTempFiles(localFolder);
            flushCollectors();
            if (!beanCollector.wasSomethingAdded()) {
                return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "File is empty!");
            }
            execNamedCommandWithCommit("initBranchInAmgNode"); // init branch names

            if (logger.isInfoEnabled()) {
                logger.info("Update BIKS structure");
            }
            pumpLogger.pumpLog("Update BIKS structure", 80);
            execNamedCommandWithCommit("insertProfileIntoBikNode");
            execNamedCommandWithCommit("insertProfileExtradata");
            if (logger.isInfoEnabled()) {
                logger.info("All right. Success!");
            }
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in Profile pump", ex);
            }
            throw new LameRuntimeException(ex.getMessage(), ex);
        }
    }

    protected void getItemMetadata(String localFileName) {
        String csvContent = LameUtils.loadAsString(localFileName, "ISO-8859-2");
//        String csvContent = LameUtils.loadAsString("C:\\temp\\profile\\DBTBL1D-CIF.txt", "ISO-8859-2");
        CSVReader reader = new CSVReader(csvContent, CSVReader.CSVParseMode.CONSIDER_QUOTES, '\t', true);
        // %LIBS, FID, DI, NOD, LEN, DFT, DOM, TBL, PTN, XPO, XPR, TYP, DES, ITP, MIN, MAX, DEC, REQ, CMP, SFD, SFD1, SFD2, SFP, SFT, SIZ, DEL, POS, RHD, SRL, CNV, LTD, USER, MDD, VAL4EXT, DEPREP, DEPOSTP, NULLIND, MDDFID, VALIDCMP
        List<Map<String, String>> content = reader.readFullTableAsMaps();
        List<ProfileBean> createListFromMap = createListFromItemMap(content);

        for (ProfileBean oneBean : createListFromMap) {
            addLibraryIfNotExists(alreadyAddedObjs, oneBean.libs);
            addFileIfNotExists(alreadyAddedObjs, oneBean.libs, oneBean.fid);
            beanCollector.add(new GenericNodeBean(oneBean.libs + "|" + oneBean.fid + "|" + oneBean.di + "|", oneBean.libs + "|" + oneBean.fid + "|", TYPE_ITEM, oneBean.di, null));
            beanExtradataCollector.add(oneBean);
        }
    }

    protected void getFileMetadata(String localFileName) {
        String csvContent = LameUtils.loadAsString(localFileName, "ISO-8859-2");
        CSVReader reader = new CSVReader(csvContent, CSVReader.CSVParseMode.CONSIDER_QUOTES, '\t', true);
        // %LIBS FID GLOBAL AKEY1 AKEY2 AKEY3 AKEY4 AKEY5 AKEY6 AKEY7 DEL SYSSN NETLOC PARFID MPLCTDD DFTDES DFTORD DFTHDR DFTDES1 LTD USER FILETYP EXIST EXTENDLENGTH FSN FDOC QID1 ACCKEYS VAL4EXT PREDAEN SCREEN RFLAG DFLAG UDACC UDFILE FPN UDPRE UDPOST PUBLISH ZRSFILE GLREF RECTYP PTRUSER PTRTLD LOG ARCHFILES ARCHKEY PTRTIM PTRUSERU PTRTLDU PTRTIMU LISTDFT LISTREQ DES
        List<Map<String, String>> content = reader.readFullTableAsMaps();
        List<ProfileFileBean> createListFromMap = createListFromFileMap(content);
        for (ProfileFileBean oneBean : createListFromMap) {
            beanFileExtradataCollector.add(oneBean);
        }
    }

    protected void createCollectors() {
        beanCollector = new AdhocPartialBeanCollector<GenericNodeBean>(PACKAGE_SIZE, adhocDao, "insertIntoGenericTable");
        beanExtradataCollector = new AdhocPartialBeanCollector<ProfileBean>(PACKAGE_SIZE, adhocDao, "insertProfileItemExtradata");
        beanFileExtradataCollector = new AdhocPartialBeanCollector<ProfileFileBean>(PACKAGE_SIZE, adhocDao, "insertProfileFileExtradata");
    }

    protected void flushCollectors() {
        beanCollector.flush();
        beanExtradataCollector.flush();
        beanFileExtradataCollector.flush();
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_PROFILE;
    }

    protected void addLibraryIfNotExists(Map<String, List<String>> alreadyAddedObjs, String libs) {
        if (alreadyAddedObjs.get(libs) == null) {
            alreadyAddedObjs.put(libs, new ArrayList<String>());
            beanCollector.add(new GenericNodeBean(libs + "|", null, TYPE_LIBRARY, libs, null));
        }
    }

    protected void addFileIfNotExists(Map<String, List<String>> alreadyAddedObjs, String libs, String fid) {
        List<String> lst = alreadyAddedObjs.get(libs);
        if (lst != null && !lst.contains(fid)) {
            lst.add(fid);
            beanCollector.add(new GenericNodeBean(libs + "|" + fid + "|", libs + "|", TYPE_FILE, fid, null));
        }
    }

    protected Pair<List<String>, List<String>> downloadFilesFromFTP() {
        Pair<List<String>, List<String>> localFilesName = new Pair<List<String>, List<String>>(new ArrayList<String>(), new ArrayList<String>());
        ProfileConfigBean config = PumpUtils.readAdminBean("profile", ProfileConfigBean.class, adhocDao);
        IFoxyFTPClient client = new FTP4JFoxyFTPClient();//SFTP4JFoxyFTPClient();
        try {
            client.connectAndLogin(config.host, BaseUtils.tryParseInteger(config.port, 21/*22*/), config.user, config.password);
            client.changeDirectory(config.path);
            List<IFoxyFTPFile> files = client.getFiles();
            for (IFoxyFTPFile file : files) {
                String fileName = file.getName();
                String remotePathToFile = BaseUtils.ensureDirSepPostfix(config.path) + fileName;
                String localPathToFile = localFolder + "\\" + fileName;
//                System.out.println("localFolder = " + localFolder + ", localPathToFile = " + localPathToFile);
                createFolderIfNotExists(localFolder);
                boolean isFolder = file.isDirectory();
//                System.out.println("remotePathToFile = " + remotePathToFile + ", localPathToFile" + localPathToFile);
                String fileNameAfterUpperCase = fileName.toUpperCase();
                if (!isFolder && fileNameAfterUpperCase.startsWith(ITEM_PREFIX)) {
                    client.download(remotePathToFile, localPathToFile);
                    localFilesName.v1.add(localPathToFile);
                }
                if (!isFolder && fileNameAfterUpperCase.startsWith(FILE_PREFIX)) {
                    client.download(remotePathToFile, localPathToFile);
                    localFilesName.v2.add(localPathToFile);
                }
            }
        } finally {
            client.disconnect();
        }
        return localFilesName;
    }

    public static void deleteLocalTempFiles(String localFolder) {
        String directory = /*BaseUtils.extractFilePath(*/ localFolder.replace("\\", "/")/*)*/;
        File file = new File(directory);
        if (file.exists() && file.isDirectory()) {
            boolean delete = LameUtils.killDir(file);
//                        System.out.println("Czy usunieto? " + delete);
        }
    }

    public static void createFolderIfNotExists(String localFolder) {
        File file = new File(localFolder);
        if (!file.exists()) {
            file.mkdir();
        }
    }

//    public static void main(String[] args) {
//        createFolderIfNotExists();
//        System.out.println("folder: " + LOCAL_FOLDER);
//    }
}
