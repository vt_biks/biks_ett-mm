/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import static pl.fovis.client.bikpages.SystemUserPage.createScrollPanelEx;
import pl.fovis.client.widgets.LoadingWidget;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.BeanWithIntegerIdBase;

/**
 *
 * @author tflorczak
 * @param <BEAN> - Bean odpowiadający jednemu wierszowi w gridzie
 */
public abstract class FilterGridPageBase<BEAN extends BeanWithIntegerIdBase> extends BikPageMinimalBase {

    public static final String LEFT_PANE_WIDTH = "255px";
    protected VerticalPanel rightVp;
    protected VerticalPanel leftVp;
    protected HorizontalPanel mainHp;
    protected DataGrid<BEAN> dataGrid;
    protected MultiSelectionModel<BEAN> selectionModel;
    protected ListDataProvider<BEAN> dataProvider;
    protected SimplePager pager;
    protected HorizontalPanel actionPanel;
    protected HorizontalPanel actionHorizontalPanel;

    @Override
    protected Widget buildWidgets() {
        mainHp = new HorizontalPanel();
        mainHp.setWidth("100%");
        mainHp.setHeight("100%");
        leftVp = new VerticalPanel();
        leftVp.setWidth("100%");
        leftVp.setHeight("100%");
        leftVp.setStyleName("SystemUserPageColor");
        rightVp = new VerticalPanel();
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(leftVp, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT /*- 18*/);
        rightVp.setWidth("100%");
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(rightVp, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT);
        ///////// other widgets //////////
        initDataGrid();
        createFilters(leftVp);
        createActionButtons();
        addGridWIthFooterToRightPanel();
        optAddActionButtonsToPanel(actionPanel, actionHorizontalPanel);
        refreshActionButtons();
        //////////////////////////////////
        ScrollPanel sc = createScrollPanelEx(leftVp, LEFT_PANE_WIDTH);
        mainHp.add(sc);
        mainHp.setCellWidth(sc, LEFT_PANE_WIDTH);
        mainHp.add(rightVp);
        return mainHp;
    }

    protected void initDataGrid() {
        //////////////////////////////////////////
        // Initialize DataGrid
        ProvidesKey<BEAN> keyProvider = new ProvidesKey<BEAN>() {
            @Override
            public Object getKey(BEAN item) {
                return item == null ? null : item.id;
            }
        };
        dataGrid = new DataGrid<BEAN>(25, keyProvider);
        dataGrid.setMinimumTableWidth(getGridHeight(), Style.Unit.PX);
        dataGrid.setAlwaysShowScrollBars(true);
        // Set the message to display when the table is empty.
        Label emptyTableWidget = new Label(I18n.brakWynikow.get());
        dataGrid.setEmptyTableWidget(emptyTableWidget);
        dataGrid.getEmptyTableWidget().getParent().setStyleName("BIAdmin-none");
        // Attach a column sort handler to the ListDataProvider to sort the list.
        dataProvider = new ListDataProvider<BEAN>();
        ColumnSortEvent.ListHandler<BEAN> sortHandler = new ColumnSortEvent.ListHandler<BEAN>(dataProvider.getList());
        dataGrid.addColumnSortHandler(sortHandler);
        // Create a Pager to control the table.
        pager = new SimplePager(SimplePager.TextLocation.CENTER, false, 0, true);
        pager.setDisplay(dataGrid);
        // Add a selection model so we can select cells.
        selectionModel = new MultiSelectionModel<BEAN>(/*keyProvider*/);
        dataGrid.setSelectionModel(selectionModel, DefaultSelectionEventManager.<BEAN>createCheckboxManager());
        selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {

            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                refreshActionButtons();
            }
        });
        // Initialize the columns.
        initTableColumns(sortHandler);
        // Add data display
        dataProvider.addDataDisplay(dataGrid);
//        dataGrid.setWidth("1500px");
//        dataGrid.setHeight("500px");
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(dataGrid, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 27);
        // Data Grid init END
        /////////////////////////////////////////////
    }

    protected void addGridWIthFooterToRightPanel() {
        actionPanel = new HorizontalPanel();
        actionPanel.setSpacing(2);
        actionPanel.setBorderWidth(0);
        actionPanel.add(pager);
        actionHorizontalPanel = new HorizontalPanel();
        actionHorizontalPanel.setStyleName("BIAdmin");
        actionHorizontalPanel.setWidth("100%");
        actionHorizontalPanel.add(actionPanel);
//        rightVp.add(dataGrid);
//        rightVp.add(actionHorizontalPanel);
        showDataGrid();
    }

    protected void showLoadingWidget() {
        rightVp.clear();
        rightVp.add(new LoadingWidget(I18n.pleaseWait.get()).buildWidget());
    }

    protected void showDataGrid() {
        rightVp.clear();
        rightVp.add(dataGrid);
        rightVp.add(actionHorizontalPanel);
    }

    protected void clearTb(TextBox... tbs) {
        for (TextBox tb : tbs) {
            tb.setText("*");
        }
    }

    protected void clearHTML(HTML... htmls) {
        for (HTML h : htmls) {
            h.setText("");
        }
    }

    protected void clearListBox(ListBox... lbx) {
        for (ListBox lb : lbx) {
            lb.setSelectedIndex(0);
        }
    }

    protected void clearDateBox(DateBox... dbx) {
        for (DateBox db : dbx) {
            db.setEnabled(false);
            db.setValue(null);
        }
    }

    public <T> void clearList(Collection<T>... lists) {
        for (Collection<T> l : lists) {
            if (!BaseUtils.isCollectionEmpty(l)) {
                l.clear();
            }
        }
    }

    protected Set<BEAN> getSelectedObjects() {
        return selectionModel.getSelectedSet();
    }

    protected List<Integer> getSelectedObjectsIds() {
        Set<BEAN> selectedSet = selectionModel.getSelectedSet();
        List<Integer> list = new ArrayList<Integer>();
        if (selectedSet == null) {
            return list;
        }
        for (BEAN oneInstance : selectedSet) {
            list.add(oneInstance.id);
        }
        return list;
    }

    /**
     *
     * @return int value in PX
     */
    protected abstract int getGridHeight();

    protected abstract void initTableColumns(ColumnSortEvent.ListHandler<BEAN> sortHandler);

    protected abstract void refreshActionButtons();

    protected abstract void createFilters(VerticalPanel leftVp);

    protected abstract void createActionButtons();

    protected abstract void optAddActionButtonsToPanel(HorizontalPanel actionPanel, HorizontalPanel actionHorizontalPanel);

}
