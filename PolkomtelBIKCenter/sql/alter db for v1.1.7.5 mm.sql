﻿exec sp_check_version '1.1.7.5';
go

create table bik_statistic_dict(
	counter_name varchar(255) not null unique,
	description varchar(255) not null unique	
);

insert into bik_statistic_dict (counter_name, description) values ('newSearch', 'Wyszukiwarka')
insert into bik_statistic_dict (counter_name, description) values ('userLogin', 'Logowanie do systemu')

insert into bik_statistic_dict
select bt.code as counter_name, bt.name as description 
from bik_tree bt
where  bt.id <>(select id from bik_tree where code = 'TreeOfTrees')

exec sp_update_version '1.1.7.5', '1.1.7.6';
go