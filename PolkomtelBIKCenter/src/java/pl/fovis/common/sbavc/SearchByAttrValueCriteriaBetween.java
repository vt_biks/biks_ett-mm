/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.sbavc;

import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author pmielanczuk
 */
public class SearchByAttrValueCriteriaBetween extends SearchByAttrValueCriteriaBase {

    public String lowerValue;
    public String higherValue;

    public SearchByAttrValueCriteriaBetween() {
    }

    public SearchByAttrValueCriteriaBetween(int attrId, String attrName, String lowerValue, String higherValue, boolean isNegation) {
        super(attrId, attrName, isNegation);
        this.lowerValue = lowerValue;
        this.higherValue = higherValue;
    }

    @Override
    public String getCriteriaMessage() {
        return "<b>" + attrName + "</b> " + (isNegation ? I18n.nie3.get() + " " : "") + I18n.fromTo.get() + ": <b>" + lowerValue + "</b> -> <b>" + higherValue + "</b>";
    }
}
