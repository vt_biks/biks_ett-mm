/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.ThreadedJobExecutor;
import java.util.Timer;

/**
 *
 * @author ctran
 */
public class BIKSThreadedWorker implements Runnable {

    protected ThreadedJobExecutor jobExecutor = new ThreadedJobExecutor();
    protected Timer timer = new Timer();

    @Override
    public void run() {
//        initExecutor();
        jobExecutor.run();
    }

    public void addJob(Runnable task) {
        jobExecutor.addJob(task);
    }

    public void destroy() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = new Timer();
        }
    }
}
