/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import java.util.Collection;
import pl.fovis.common.JoinedObjBasicInfoBean;

/**
 *
 * @author wezyr
 */
public interface IJoinedObjSelectionManager<IDT> {

    //ww: to się będzie wywoływać tylko dla tych id, które znajdują się w
    // kolekcji zwracanej przez getJoinedObjIds()
    public JoinedObjBasicInfoBean getInfoForJoinedNodeById(IDT dstNodeId);

    //ww: to się będzie wywoływać tylko raz - gdy TreeSelectorDialogBase startuje
    public Collection<IDT> getJoinedObjIds();

    public void refreshJoinedNodes(IDT forNodeId);
}
