/*

--- robimy kopi� bik_joined_objs - na czas test�w

select * 
into tmp_bik_joined_objs
from bik_joined_objs

--- odtwarzamy z kopii

delete from bik_joined_objs

INSERT INTO [biks_polkomtel_v1_1_6].[dbo].[bik_joined_objs]
           ([src_node_id]
           ,[dst_node_id]
           ,[type]
           ,[inherit_to_descendants])
     select [src_node_id]
           ,[dst_node_id]
           ,[type]
           ,[inherit_to_descendants]
           from tmp_bik_joined_objs

*/

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_delete_bik_joined_objs_by_kinds]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_delete_bik_joined_objs_by_kinds
go

create procedure sp_delete_bik_joined_objs_by_kinds @kinds_one_str varchar(max), @kinds_two_str varchar(max) as
begin
  set nocount on

  declare @timestamp_start_total datetime = current_timestamp
  declare @diags_level int = 1 -- warto�� 0 oznacza brak logowania, 1 i wi�cej - jest logowanie
  declare @rc int
  declare @timestamp_start datetime 
  declare @timestamp_end datetime
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds: start' +
      ', @kinds_one_str = ' + @kinds_one_str + 
      ', @kinds_two_str = ' + @kinds_two_str
  end

  -- declare @kinds_one varchar(max) = 'Universe', @kinds_two varchar(max) = 'TeradataSchema'
  declare @codes_one table (code varchar(255) not null primary key)
  declare @codes_two table (code varchar(255) not null primary key)

  insert into @codes_one (code)
  select distinct str
  from dbo.fn_split_by_sep(@kinds_one_str, ',', 7)

  insert into @codes_two (code)
  select distinct str
  from dbo.fn_split_by_sep(@kinds_two_str, ',', 7)

  declare @kind_one_id table (id int not null primary key)
  declare @kind_two_id table (id int not null primary key)

  insert into @kind_one_id (id)
  select distinct k.id
  from @codes_one x inner join bik_node_kind k on x.code = k.code
  where substring(x.code, 1, 1) <> '@'
  
  insert into @kind_two_id (id)
  select distinct k.id
  from @codes_two x inner join bik_node_kind k on x.code = k.code
  where substring(x.code, 1, 1) <> '@'

  declare @joined_ids_to_del table (id int not null primary key)

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds: before insert into @joined_ids_to_del'
  end

  set @timestamp_start = current_timestamp

  insert into @joined_ids_to_del (id)
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.src_node_id = n_one.id
  inner join bik_node n_two on jo.dst_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1
  union -- bez all, bo mog� by� duplikaty, a chcemy je w�a�nie wyeliminowa�
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.dst_node_id = n_one.id
  inner join bik_node n_two on jo.src_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds: after insert into @joined_ids_to_del, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end
  
  --select count(*) from @joined_ids_to_del

  declare @tree_one_id table (id int not null primary key)
  declare @tree_two_id table (id int not null primary key)

  delete from @codes_one where substring(code, 1, 1) <> '@'
  delete from @codes_two where substring(code, 1, 1) <> '@'

  update @codes_one set code = substring(code, 2, len(code))
  update @codes_two set code = substring(code, 2, len(code))

  insert into @tree_one_id (id)
  select distinct t.id
  from @codes_one x inner join bik_tree t on x.code = t.code

  insert into @tree_two_id (id)
  select distinct t.id
  from @codes_two x inner join bik_tree t on x.code = t.code

  declare @joined_ids_to_del_by_tree table (id int not null primary key)

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds: before insert into @joined_ids_to_del_by_tree'
  end

  set @timestamp_start = current_timestamp

  insert into @joined_ids_to_del_by_tree (id)
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.src_node_id = n_one.id
  inner join bik_node n_two on jo.dst_node_id = n_two.id
  inner join @tree_one_id t1 on n_one.tree_id = t1.id
  inner join @tree_two_id t2 on n_two.tree_id = t2.id
  where jo.type = 1
  union
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.dst_node_id = n_one.id
  inner join bik_node n_two on jo.src_node_id = n_two.id
  inner join @tree_one_id t1 on n_one.tree_id = t1.id
  inner join @tree_two_id t2 on n_two.tree_id = t2.id
  where jo.type = 1  

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds: after insert into @joined_ids_to_del_by_tree, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end
  
  --select count(*) from @joined_ids_to_del_by_tree

  declare @joined_ids_to_del_by_tree_kind table (id int not null primary key)

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds: before insert into @joined_ids_to_del_by_tree_kind'
  end

  set @timestamp_start = current_timestamp

  insert into @joined_ids_to_del_by_tree_kind (id)
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.src_node_id = n_one.id
  inner join bik_node n_two on jo.dst_node_id = n_two.id
  inner join @tree_one_id t1 on n_one.tree_id = t1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1
  union
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.dst_node_id = n_one.id
  inner join bik_node n_two on jo.src_node_id = n_two.id
  inner join @tree_one_id t1 on n_one.tree_id = t1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1  

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds: after insert into @joined_ids_to_del_by_tree_kind, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end

  --select count(*) from @joined_ids_to_del_by_tree_kind

  declare @joined_ids_to_del_by_kind_tree table (id int not null primary key)

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds: before insert into @joined_ids_to_del_by_kind_tree'
  end

  set @timestamp_start = current_timestamp

  insert into @joined_ids_to_del_by_kind_tree (id)
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.src_node_id = n_one.id
  inner join bik_node n_two on jo.dst_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @tree_two_id t2 on n_two.tree_id = t2.id
  where jo.type = 1
  union
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.dst_node_id = n_one.id
  inner join bik_node n_two on jo.src_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @tree_two_id t2 on n_two.tree_id = t2.id
  where jo.type = 1  

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds: after insert into @joined_ids_to_del_by_kind_tree, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end

  --select count(*) from @joined_ids_to_del_by_kind_tree
  
  insert into @joined_ids_to_del (id)
  select bn.id from @joined_ids_to_del_by_tree bn left join @joined_ids_to_del bb on bn.id = bb.id
  where bb.id is null
  
  insert into @joined_ids_to_del (id)
  select bn.id from @joined_ids_to_del_by_tree_kind bn left join @joined_ids_to_del bb on bn.id = bb.id
  where bb.id is null

  insert into @joined_ids_to_del (id)
  select bn.id from @joined_ids_to_del_by_kind_tree bn left join @joined_ids_to_del bb on bn.id = bb.id
  where bb.id is null

  --select count(*) from @joined_ids_to_del

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds: before final delete'
  end

  set @timestamp_start = current_timestamp
  
  delete from bik_joined_objs where id in (select id from @joined_ids_to_del)
  
  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  declare @timestamp_end_total datetime = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds: after final delete, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
      + ', TOTAL TIME TAKEN: ' + cast(datediff(ms, @timestamp_start_total, @timestamp_end_total) / 1000.0 as varchar(20)) + ' s'
  end
end
go




/*

select * from bik_node_kind

select * from bik_tree

exec sp_delete_bik_joined_objs_by_kinds 'Universe,Measure,Dimension,Detail,Filter', 'TeradataSchema,TeradataSchema,TeradataTable,TeradataColumn,TeradataView'

exec sp_delete_bik_joined_objs_by_kinds 'Universe,Measure,Dimension,Detail,Filter', 'Universe,Measure,Dimension,Detail,Filter'

exec sp_delete_bik_joined_objs_by_kinds 'Universe, DataConnection', 'Webi'

exec sp_delete_bik_joined_objs_by_kinds 'Universe, @Connections', 'Webi, @DQC'

exec sp_delete_bik_joined_objs_by_kinds '@Connections', 'Webi'

exec sp_delete_bik_joined_objs_by_kinds 'Universe, @Connections', 'Webi, @DQC'

exec sp_delete_bik_joined_objs_by_kinds 'Universe', 'Webi'

exec sp_delete_bik_joined_objs_by_kinds '@Connections', '@DQC'

exec sp_delete_bik_joined_objs_by_kinds '@Connections', 'Webi'
exec sp_delete_bik_joined_objs_by_kinds 'Universe', '@DQC'

exec sp_delete_bik_joined_objs_by_kinds 'Webi, @DQC, DQCGroup,DQCTestSuccess,DQCAllTestsFolder,DQCTestFailed,DQCTestInactive', 'Universe, @Connections'


select distinct node_kind_id from bik_node where tree_id = 2
select distinct node_kind_id from bik_node where tree_id = 1
select distinct node_kind_id from bik_node where tree_id = 3
select distinct node_kind_id, tree_id from bik_node

exec sp_delete_bik_joined_objs_by_kinds 'TeradataView,TeradataTable,TeradataSchema,Webi,ReportQuery', 'Universe,DataConnection,Measure,Dimension,Detail,Filter,Webi,ReportQuery,UniverseAliasTable,UniverseDerivedTable,UniverseTable'

2012-08-24 16:56:15.950: sp_delete_bik_joined_objs_by_kinds: start, @kinds_one_str = TeradataView,TeradataTable,TeradataSchema,Webi,ReportQuery, @kinds_two_str = Universe,DataConnection,Measure,Dimension,Detail,Filter,Webi,ReportQuery,UniverseAliasTable,UniverseDerivedTable,UniverseTable
2012-08-24 16:56:15.953: sp_delete_bik_joined_objs_by_kinds: before insert into @joined_ids_to_del
2012-08-24 16:56:22.236: sp_delete_bik_joined_objs_by_kinds: after insert into @joined_ids_to_del, @rc=212483, time taken: 6.283000 s
2012-08-24 16:56:22.236: sp_delete_bik_joined_objs_by_kinds: before insert into @joined_ids_to_del_by_tree
2012-08-24 16:56:22.351: sp_delete_bik_joined_objs_by_kinds: after insert into @joined_ids_to_del_by_tree, @rc=0, time taken: 0.113000 s
2012-08-24 16:56:22.352: sp_delete_bik_joined_objs_by_kinds: before insert into @joined_ids_to_del_by_tree_kind
2012-08-24 16:56:22.442: sp_delete_bik_joined_objs_by_kinds: after insert into @joined_ids_to_del_by_tree_kind, @rc=0, time taken: 0.090000 s
2012-08-24 16:56:22.442: sp_delete_bik_joined_objs_by_kinds: before insert into @joined_ids_to_del_by_kind_tree
2012-08-24 16:56:22.539: sp_delete_bik_joined_objs_by_kinds: after insert into @joined_ids_to_del_by_kind_tree, @rc=0, time taken: 0.096000 s
2012-08-24 16:56:22.539: sp_delete_bik_joined_objs_by_kinds: before final delete
2012-08-24 16:56:25.519: sp_delete_bik_joined_objs_by_kinds: after final delete, @rc=212483, time taken: 2.980000 s, TOTAL TIME TAKEN: 9.570000 s


*/
