/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author bfechner
 */
public class AttrAnsiText extends AttrWigetBase {

    String atrLinValue;

    public AttrAnsiText(String atrLinValue) {
        this.atrLinValue = atrLinValue;
    }

    protected TextArea ansiiTextArea = new TextArea();

    @Override
    public Widget getWidget() {
        VerticalPanel vp = new VerticalPanel();
        vp.add(ansiiTextArea);
        vp.setWidth("100%");
        ansiiTextArea.setStyleName("richTextArea");
        ansiiTextArea.setWidth(Window.getClientWidth() - 387 + "px");
        ansiiTextArea.setHeight(Window.getClientHeight() - 307 + "px");
        ansiiTextArea.setText(atrLinValue);

        return vp;
    }

    @Override
    public String getValue() {
        return ansiiTextArea.getValue();
    }

    @Override
    public int getWidgetHeight() {
        return 160;
    }

}
