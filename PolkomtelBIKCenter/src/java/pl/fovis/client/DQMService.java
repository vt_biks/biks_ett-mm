/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.ConnectionParametersDQMBean;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.fovis.common.NoteBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.dqm.DQMDataErrorIssueBean;
import pl.fovis.common.dqm.DQMEvaluationBean;
import pl.fovis.common.dqm.DQMLogBean;
import pl.fovis.common.dqm.DQMScheduleBean;
import pl.fovis.common.dqm.DataQualityTestBean;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
@RemoteServiceRelativePath("boxiservice")
public interface DQMService extends RemoteService {

    public int addDQMTest(DataQualityTestBean bean, int parentNodeId/*, boolean isAddedToAllTestFolder*/);

    public int assignDQMTestToGroup(int testId, int groupId, String name);

    public void editDQMTest(DataQualityTestBean bean);

    public List<TreeNodeBean> getDQMTestsTreeNodes(Integer optUpSelectedNodeId);

    public List<String> deleteDQMTest(TreeNodeBean node);

    public void changeDQMTestActivity(int nodeId, boolean deactivateTest);

    public Set<Integer> getAlreadyAssignedDQMTest(int nodeId);

    public Pair<Set<String>,Map<Integer, ConnectionParametersDQMConnectionsBean>> getDQMTestsNameAndOptConnections();

    public List<TreeNodeBean> getBikAllRootTreeNodesForTreeCodes(Set<String> treeCodes);

    public void runDQMTest(int testId);

    public List<DataQualityTestBean> getDQMTests();

    public Pair<Integer, List<DQMLogBean>> getDQMLogs(int pageNum, int pageSize);

    public void setDQMConnectionParameters(ConnectionParametersDQMBean bean);

    public ConnectionParametersDQMBean getDQMConnectionParameters();

    public Pair<Integer, List<DQMScheduleBean>> getDQMSchedules(int pageNum, int pageSize);

    public void setDQMSchedules(List<DQMScheduleBean> schedules);

    public DQMEvaluationBean getEvaluationPreInfo(int nodeId);

    public Integer saveEvaluation(DQMEvaluationBean bean);

    public void setDQMEvaluationStatus(int nodeId, NoteBean comment);

    public Integer generateDQMReport(int nodeId, Date fromDate, Date toDate);

    public void refreshDQMReport(int nodeId);

    public List<DQMEvaluationBean> getEvaluationPeriods(int nodeId);

    public Integer saveDataErrorIssue(DQMDataErrorIssueBean bean);

    public DQMDataErrorIssueBean getDQMDataErrorIssuePreInfo(int nodeId);

    public DQMDataErrorIssueBean getDQMDataErrorIssueByNodeId(int nodeId);

    public Pair<String, String> prepareFileToDownload(String exportType, Long requestId);
}
