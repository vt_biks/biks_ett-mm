/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.lisa;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import pl.fovis.common.lisa.LisaDictColumnBean;

/**
 *
 * @author ctran
 */
public class TeradataBatchUpdateWorker extends BaseTeradataBatchWorker {

    public TeradataBatchUpdateWorker(PreparedStatement ps, List<LisaDictColumnBean> columnsList) {
        super(ps, columnsList);
    }

    @Override
    public void parse2PreparedStatement(Map<String, String> record) throws SQLException {
        int colId = 1;

        for (LisaDictColumnBean column : nonPkColumnsList) {
            String value = record.get(column.nameInDb);
            setParametr4PreparedStatement(ps, colId++, column, value);
        }
        for (LisaDictColumnBean column : pkColumnsList) {
            String value = record.get(column.nameInDb);
            setParametr4PreparedStatement(ps, colId++, column, value);
            setParametr4PreparedStatement(ps, colId++, column, value);
        }
    }
}
