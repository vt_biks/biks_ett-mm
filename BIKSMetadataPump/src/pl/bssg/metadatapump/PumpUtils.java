/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import commonlib.MuppetMaker;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.common.SchedulePumpBean;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;
import simplelib.SimpleDateUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class PumpUtils {

    public static Connection getConnection(String driverClass, String url, String user, String password) {
        return getConnection(driverClass, url, user, password, null);
    }

    public static Connection getConnection(String driverClass, String url, String user, String password, ILameLogger logger) {
        try {
            Class.forName(driverClass);
            Connection connection = DriverManager.getConnection(url, user, password);
            return connection;
        } catch (ClassNotFoundException ex) {
            String errorMsg = "Error in loading driver: ";
            if (logger != null && logger.isErrorEnabled()) {
                logger.error(errorMsg, ex);
            } else {
                System.out.println(errorMsg + ex);
            }
            return null;
        } catch (SQLException ex) {
            String errorMsg = "Error in connecting: ";
            if (logger != null && logger.isErrorEnabled()) {
                logger.error(errorMsg, ex);
            } else {
                System.out.println(errorMsg + ex);
            }
            return null;
        }
    }

    public static void closeConnection(Connection connection) {
        closeConnection(connection, null);
    }

    public static void closeConnection(Connection connection, ILameLogger logger) {
        try {
            connection.close();
        } catch (SQLException ex) {
            String errorMsg = "Error in close connection: ";
            if (logger != null && logger.isErrorEnabled()) {
                logger.error(errorMsg, ex);
            } else {
                System.out.println(errorMsg + ex);
            }
        }
    }

    public static <B> B readAdminBean(String prefix, Class<B> c, INamedSqlsDAO<Object> adhocDao) {
        List<Map<String, Object>> list = adhocDao.execNamedQueryCFN("getAdminValue", FieldNameConversion.ToLowerCase, prefix);
        Map<String, Object> vals = new HashMap<String, Object>();
        for (Map<String, Object> map : list) {
            String key = ((String) map.get("param" /* I18N: no */)).replaceFirst(prefix + ".", "");
            Object val = map.get("value" /* I18N: no */);
            vals.put(key, val);
        }
        return //BeanMaker.createOneBean(vals, c);
                MuppetMaker.newMuppet(c, vals);
    }

    // string postaci: <property_name>=<display_name>|<property_name>=<display_name>|, ew. tłumaczenia w różnych językach
    // z tłumaczeniami: <property_name>=<[pl|en|...]>:<display_name>|<property_name>=<[pl|en|...]>:<display_name>
    public static Map<String, String> getExPropList(String param, String lang) {
        return Translator.getExPropList(param, lang);
    }

    public static String getServerNameWithoutSuffix(String server) {
        if (BaseUtils.isStrEmptyOrWhiteSpace(server)) {
            return "";
        }
        int indexOfHash = server.indexOf("#");
        if (indexOfHash != -1) {
            server = server.substring(0, indexOfHash);
        }
        return server;
    }

    public static Date getStartedDateForSchedule(SchedulePumpBean scheduleBean) {
        Date dateNow = new Date();
        if (scheduleBean.interval == 0 && scheduleBean.dateStarted != null && scheduleBean.dateStarted.before(SimpleDateUtils.truncateDay(dateNow))) {
            return null;
        }
        int actualHour = SimpleDateUtils.getHours(dateNow);
        int actualMinute = SimpleDateUtils.getMinutes(dateNow);
        Date dateSchedule;
        if (scheduleBean.dateStarted != null) {
            dateSchedule = SimpleDateUtils.newDate(SimpleDateUtils.getYear(scheduleBean.dateStarted), SimpleDateUtils.getMonth(scheduleBean.dateStarted), SimpleDateUtils.getDay(scheduleBean.dateStarted), scheduleBean.hour, scheduleBean.minute);
            if (dateSchedule.before(dateNow)) {
                dateSchedule = SimpleDateUtils.newDate(SimpleDateUtils.getYear(dateNow), SimpleDateUtils.getMonth(dateNow), SimpleDateUtils.getDay(dateNow), scheduleBean.hour, scheduleBean.minute);
                if (dateSchedule.before(dateNow)) {
                    dateSchedule = SimpleDateUtils.addDays(dateSchedule, 1); // next day
                }
            }
        } else {
            if (actualHour > scheduleBean.hour || (actualHour == scheduleBean.hour && actualMinute > scheduleBean.minute)) {
                dateNow = SimpleDateUtils.addDays(dateNow, 1);
            }
            dateSchedule = SimpleDateUtils.newDate(SimpleDateUtils.getYear(dateNow), SimpleDateUtils.getMonth(dateNow), SimpleDateUtils.getDay(dateNow), scheduleBean.hour, scheduleBean.minute);
        }
        return dateSchedule;
    }
}
