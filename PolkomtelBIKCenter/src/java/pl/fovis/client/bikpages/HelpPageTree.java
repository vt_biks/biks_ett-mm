/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.TreeAndNodeKindTreeBase;
import pl.fovis.client.dialogs.EditHelpDescriptionDialog;
import pl.fovis.common.HelpBean;
import pl.fovis.common.MenuExtender.MenuBottomLevel;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.i18nsupport.GWTCurrentLocaleNameProvider;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author pgajda
 */
public class HelpPageTree extends BikPageMinimalBase implements IBikPageForAppAdmin {

    protected ScrollPanel treeScrollPnl = new ScrollPanel();
    protected HTML titleLbl = new HTML();
    protected Label lblTitle = new Label();
    protected Label lbLanguage = new Label();
    protected ListBox languageListBox = new ListBox();
    protected Label lblArea = new Label();
    protected HTML helpTxtTitle = new HTML();
    protected HTML helpTxtContent = new HTML();
//    protected HelpHintExtractor helpHintExtractor = null;
    protected HelpBean currentHelpBean = null;
    protected Map<String, HelpBean> keyHelpHelpBean = null;
    protected Set<Integer> idHelpOfParentNodes = null;
    protected Integer firstRowIdToShow = null;
    protected String helpCode = null;
    protected String helpName = null;
    protected TreeAndNodeKindTreeBase treeForHelp = new TreeAndNodeKindTreeBase() {
        @Override
        protected MenuBottomLevel whatToKeepAsLeavesInMenuTree() {
            return MenuBottomLevel.BikPages;
        }

        @Override
        protected void selectionChangedWholeLine(Map<String, Object> param) {
            Integer idNode = safeAsInteger(param.get("id" /* I18N: no */));
            if (idNode != null) {
                if (idHelpOfParentNodes.contains(idNode)) {
                    return;
                }
            }

            helpCode = getCorrectHelpCode(BaseUtils.safeToStringDef(param.get("helpCode"), ""));
            helpName = BaseUtils.safeToStringDef(param.get("helpName"), "");
            helpTxtTitle.setHTML(BaseUtils.safeToString(helpName));

            String lang = languageListBox.getValue(languageListBox.getSelectedIndex());

            loadHelpMessage(helpCode, helpName, lang);
        }

        @Override
        public List<MenuNodeBean> extendMenuNodes() {
            List<MenuNodeBean> res = super.extendMenuNodes();

//            MenuExtender.prettyPrintMenuNodes("extendMenuNodes, after super", res);
            if (idHelpOfParentNodes == null) {
                idHelpOfParentNodes = new HashSet<Integer>();
            } else {
                idHelpOfParentNodes.clear();
            }

            int otherMenuHelpId = -1;
            for (MenuNodeBean mnb : res) {
                if (firstRowIdToShow == null && mnb.parentNodeId != null) {
                    firstRowIdToShow = mnb.id;
                }
                if (otherMenuHelpId < mnb.id) {
                    otherMenuHelpId = mnb.id;
                }
                keyHelpHelpBean.remove(getCorrectHelpCode(mnb.code));
                if (mnb.parentNodeId != null) {
                    idHelpOfParentNodes.add(mnb.parentNodeId);
                }
            }

            res.add(new MenuNodeBean(++otherMenuHelpId, I18n.inne.get() /* I18N:  */, null, null));
            idHelpOfParentNodes.add(otherMenuHelpId);
            int tmpVal = otherMenuHelpId + 1;
            for (HelpBean hb : keyHelpHelpBean.values()) {
                res.add(new MenuNodeBean(tmpVal++, hb.caption, otherMenuHelpId, hb.helpKey));
            }

//            MenuExtender.prettyPrintMenuNodes("extendMenuNodes, final after fixing", res);
            return res;
        }

        @Override
        protected Map<String, Object> convertBeanToRow(final MenuNodeBean mnb) {
            InlineHTML iht = new InlineHTML();
            iht.setStyleName("cellInner");
            if (!idHelpOfParentNodes.contains(mnb.id)) {
                iht.setHTML("<b>" + BaseUtils.encodeForHTMLTag(mnb.name) + "</b>");
            } else {
                iht.setHTML(BaseUtils.encodeForHTMLTag(mnb.name));
            }

            //iht.setHTML("<b>" + mnb.name + "</b>");
            HashMap<String, Object> hm = new HashMap<String, Object>();
            hm.put("id" /* I18N: no */, mnb.id);
            hm.put("parentId", mnb.parentNodeId);
            hm.put("name" /* I18N: no */, iht);
            hm.put("icon" /* I18N: no */, null);
            hm.put("helpCode", mnb.code);
            hm.put("helpName", mnb.name);

            return hm;
        }

        protected boolean isExpandAllNodes() {
            return false;
        }
    };

    @Override
    protected Widget buildWidgets() {

        HorizontalPanel main = new HorizontalPanel();

        FlowPanel left = new FlowPanel();
        left.setStyleName("EntityDetailsPane");
        GWTUtils.setStyleAttribute(left, "padding" /* I18N: no */, "4px");

        final FlowPanel right = new FlowPanel();
        right.setStyleName("EntityDetailsPane");
        GWTUtils.setStyleAttribute(right, "padding" /* I18N: no */, "4px");

        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(left,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 8);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(right,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 8);

        main.add(left);
        main.add(right);

        main.setCellWidth(right, "100%");
        main.setCellHeight(left, "100%");
        main.setCellHeight(right, "100%");

        left.add(treeScrollPnl);
        treeScrollPnl.setWidth("350px");

        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(treeScrollPnl,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 4);

        HorizontalPanel titlePnl = new HorizontalPanel();
        titlePnl.setWidth("100%");

        titleLbl.setHTML("<b>" + I18n.trescPomocy.get() /* I18N:  */ + "</b>");
        titleLbl.setStyleName("BizDef-titleLbl");
        titleLbl.setWidth("90%");
        PushButton pB = createEditButton(new IContinuation() {
            public void doIt() {
                if (currentHelpBean == null) {
                    return;
                }

                final String language = languageListBox.getValue(languageListBox.getSelectedIndex());
                BIKClientSingletons.getService().getMultipleHelpDescr(currentHelpBean.helpKey, currentHelpBean.caption, language, new StandardAsyncCallback<HelpBean>() {
                    public void onSuccess(final HelpBean result) {
                        HelpBean resultVal = null;
                        if (result == null) {
                            resultVal = currentHelpBean;
                        } else {
                            resultVal = result;
                        }
                        new EditHelpDescriptionDialog().buildAndShowDialog(resultVal, new IParametrizedContinuation<HelpBean>() {
                            public void doIt(final HelpBean param) {

                                param.lang = language;
                                BIKClientSingletons.getService().addOrUpdateBikHelp(param, new StandardAsyncCallback<Void>() {
                                    public void onSuccess(Void result) {                                        //refreshView("Zmieniono opis");
                                        BIKClientSingletons.showInfo(I18n.zmienionoOpis.get() /* I18N:  */);
                                        helpTxtContent.setHTML(BaseUtils.safeToStringDef(param.textHelp, I18n.brakOpisu.get() /* I18N:  */));
                                    }
                                });
                            }
                        });

                    }
                });
            }
        });

        languageListBox.addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                int index = languageListBox.getSelectedIndex();
                String value = languageListBox.getValue(index);
                loadHelpMessage(helpCode, helpName, value);
            }
        });

        titlePnl.add(titleLbl);
        titlePnl.add(pB);
        titlePnl.setCellHorizontalAlignment(pB, HasHorizontalAlignment.ALIGN_RIGHT);

        right.add(titlePnl);

        List<String> languageList = BIKClientSingletons.getLanguages();

        String language = GWTCurrentLocaleNameProvider.getCurrentLocaleNameStatic();
        int idx = 0;
        for (String lang : languageList) {
            languageListBox.addItem(lang);
            if (lang.equals(language)) {
                idx = languageList.indexOf(language);
            }
        }
        languageListBox.setSelectedIndex(idx);
        lbLanguage.setStyleName("addBlogLbl");
        lblTitle.setStyleName("addBlogLbl");
        lblArea.setStyleName("addBlogLbl");

        helpTxtTitle.setStyleName("addBlogTitle");
        helpTxtTitle.setWidth("470px");

        Grid mainGrid = new Grid(3, 2);
        mainGrid.setWidth("100%");
        mainGrid.setCellSpacing(4);

        ScrollPanel helpMsgScrollPnl = new ScrollPanel() {
            @Override
            protected void onAttach() {
                super.onAttach();
                int width = right.getOffsetWidth() - 80;
                if (width > 0) {
                    setWidth(width + "px");
                    setVisible(true);
                }
            }
        };
        helpMsgScrollPnl.setVisible(false);
        helpMsgScrollPnl.setWidget(helpTxtContent);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(helpMsgScrollPnl,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 87);

        mainGrid.setWidget(0, 0, lblTitle);
        mainGrid.setWidget(0, 1, helpTxtTitle);
        mainGrid.setWidget(1, 0, lbLanguage);
        mainGrid.setWidget(1, 1, languageListBox);
        mainGrid.setWidget(2, 0, lblArea);
        mainGrid.setWidget(2, 1, helpMsgScrollPnl);

        //tylko raz stosuje formater, dlatego nie wyciagam do oddzielnej zmiennej
        mainGrid.getCellFormatter().setVerticalAlignment(1, 0, HasVerticalAlignment.ALIGN_TOP);

        right.add(mainGrid);

        helpTxtContent.setWordWrap(true);
        setLabeTitle();

        return main;
    }

    protected void setLabeTitle() {
        lblTitle.setText(I18n.tytul.get() /* I18N:  */ + ":");
        lblArea.setText(I18n.tresc.get() /* I18N:  */ + ":");
        lbLanguage.setText(I18n.jezyk.get() /* I18N:  */ + ":");
    }

    protected void loadHelpMessage(final String helpKey, final String helpCaption, final String lang) {

        BIKClientSingletons.getService().getMultipleHelpDescr(helpKey, null, lang, new StandardAsyncCallback<HelpBean>() {
            public void onSuccess(HelpBean t) {
                currentHelpBean = t;
                if (t != null) {
                    helpTxtContent.setHTML(BaseUtils.safeToStringDef(t.textHelp, I18n.brakOpisu.get() /* I18N:  */));
                } else {
                    helpTxtContent.setHTML(I18n.brakOpisu.get() /* I18N:  */);
                    currentHelpBean = new HelpBean();
                    currentHelpBean.caption = helpCaption;
                    currentHelpBean.helpKey = helpKey;
                    currentHelpBean.lang = lang;
                }
            }
        });

    }

    protected void show() {

        getAllHelpMessages(new StandardAsyncCallback<List<HelpBean>>() {
            public void onSuccess(List<HelpBean> t) {

                if (keyHelpHelpBean == null) {
                    keyHelpHelpBean = new LinkedHashMap<String, HelpBean>();
                } else {
                    keyHelpHelpBean.clear();
                }

                for (HelpBean nb : t) {
                    keyHelpHelpBean.put(nb.helpKey, nb);
                }
                treeScrollPnl.clear();
                Widget treeWidget = treeForHelp.buildWidgets();
                treeScrollPnl.add(treeWidget);
                treeForHelp.selectRecord(firstRowIdToShow);

                //pg: z jakiś powodów do FoxyWezyrTreeGrid przy zmianie zaznaczonego węzła występuje wymuszenie zmiany szerokości drzewa na 400px
                //zapytać się Wezyra dlaczego tak jest, bo jeśli to nie jest potrzebne, to można to ręczne ustawianie na 330px skasować
                treeWidget.setWidth("330px");
            }
        });
    }

    protected Request getAllHelpMessages(AsyncCallback<List<HelpBean>> callback) {

        return BIKClientSingletons.getService().getHelpHints(callback);
    }

    protected PushButton createEditButton(final IContinuation showEditDialogCont) {

        PushButton editButton = new PushButton(new Image("images/edit_gray.png" /* I18N: no */), new Image("images/edit_yellow.png" /* I18N: no */));
        editButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                showEditDialogCont.doIt();
            }
        });
        editButton.setStyleName("Attr" /* I18N: no */ + "-editBtn");
        editButton.setTitle(I18n.edytuj.get() /* I18N:  */);
        return editButton;
    }

//    public String getCaptionDeprecated() {
//        return I18n.trescPomocy.get() /* I18N:  */;
//    }
    @Override
    public String getId() {
        return "admin:dict:attrHints";
    }

    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        show();
    }

    protected String getCorrectHelpCode(String helpCode) {

        String retHelpCode = helpCode;
        if (helpCode != null && helpCode.length() >= 1 && !Character.isLetterOrDigit(helpCode.charAt(0))) {
            retHelpCode = helpCode.substring(1);
        }

        return retHelpCode;
    }

    protected static Integer safeAsInteger(Object o) {
        if (o != null && o instanceof Integer) {
            return (Integer) o;
        }
        return null;
    }
}
