/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.TextArea;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class TextAreaDialog extends BaseActionOrCancelDialog {

    protected TextArea area;
    protected String body;
    protected IParametrizedContinuation<String> cont;

    @Override
    protected String getDialogCaption() {
        return I18n.zapytanieSQL.get() /* I18N:  */;
    }

    public void buildAndShowDialog(String body, IParametrizedContinuation<String> cont) {
        this.body = body;
        this.cont = cont;
        super.buildAndShowDialog();
    }

    @Override
    protected void doAction() {
        cont.doIt(area.getValue());
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get();
    }

    @Override
    protected void onEnterPressed() {
        // NO ACTION
    }

    @Override
    protected void buildMainWidgets() {
        area = new TextArea();
        area.setText(body == null ? "" : body);
        area.setSize("600px", "600px");
        main.add(area);
    }
}
