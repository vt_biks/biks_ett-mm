/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.mltx;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author pmielanczuk
 */
public class MltxDatabaseBean implements Serializable {
    /*
     id	int
     created_at	datetime
     database_name	varchar
     registration_id	int
     */

    public int id;
    public Date createdAt;
    public String databaseName;
    public Integer registrationId;
}
