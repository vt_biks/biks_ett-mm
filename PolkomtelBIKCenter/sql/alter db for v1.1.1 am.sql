﻿exec sp_check_version '1.1.1';
go
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
--nullowanie powtarzających się rekordów w bik_system_user do bik_user
update bik_system_user
set user_id = null
from bik_system_user su inner join (select min(id) as best_id, user_id from bik_system_user group by user_id) 
ahm on su.user_id = ahm.user_id and su.id <> ahm.best_id
go

alter table bik_system_user
	add constraint PK_bik_system_user_id primary key(id)
go

--zakładam unikalność na user_id o ile nie jest nullem
create unique index idx_unique_bik_system_user_user_id on bik_system_user(user_id)
where user_id is not null
go

alter table bik_user
	add constraint UQ_bik_user_node_id unique (node_id)
go

--dodanie do bik_system_user nowej kolumny name
alter table bik_system_user
	add name varchar(255) ;
go

update bik_system_user
set name = login_name
go

alter table bik_system_user alter column name varchar(255) not null;
go

--dodanie sztucznego użytkownika
insert into bik_system_user(login_name, name, password, is_disabled, is_admin, is_expert, user_id)
values('_Anonim_', '_Anonim_', '', 1, 0, 0, null)
go

------------------------------------------------------------------------------------------------------------------
--usunięcie z tabeli bik_user columny activ wraz z jego defaultem i checkiem
------------------------------------------------------------------------------------------------------------------
declare @default_constraint_name sysname
select @default_constraint_name = object_name(cdefault)
from syscolumns
where id = object_id('bik_user') and name = 'activ'

if @default_constraint_name is not null
 exec ('alter table bik_user drop constraint ' + @default_constraint_name)

declare @sqltxt varchar(8000) = 
(select 'alter table bik_user drop constraint ' + cc.name
from sys.check_constraints cc inner join syscolumns sc on cc.parent_object_id = sc.id and cc.parent_column_id = sc.colid
where cc.parent_object_id = object_id('bik_user')  and sc.name = 'activ'
)
execute(@sqltxt)

if exists(select 1 from syscolumns sc where id = object_id('bik_user') and name = 'activ')
 exec ('alter table bik_user drop column  activ')
go

------------------------------------------------------------------------------------------------------------------
--usunięcie z tabeli bik_user columny old_login_name wraz z jego defaultem
------------------------------------------------------------------------------------------------------------------
declare @default_constraint_name sysname
select @default_constraint_name = object_name(cdefault)
from syscolumns
where id = object_id('bik_user') and name =  'old_login_name'

if @default_constraint_name is not null
 exec ('alter table bik_user drop constraint ' + @default_constraint_name)

if exists(select 1 from syscolumns sc where id = object_id('bik_user') and name =  'old_login_name')
 exec ('alter table bik_user drop column old_login_name')
go

------------------------------------------------------------------------------------------------------------------
--usunięcie z tabeli bik_user columny user_kind_id wraz z jego kluczem oncym
------------------------------------------------------------------------------------------------------------------
declare @nameFK varchar(255);

select @nameFK = fk.ForeignKey_Name
	from sys.objects so inner join sys.columns sc on so.object_id = sc.object_id
		left join vw_ww_foreignkeys fk on fk.table_name = so.name and fk.name = sc.name
	where  so.name like 'bik_user' and sc.name = 'user_kind_id'
		
	if @nameFK is not null	
		exec ('alter table bik_user drop constraint ' + @nameFK)

if exists(select 1 from syscolumns sc where id = object_id('bik_user') and name = 'user_kind_id')
 exec ('alter table bik_user drop column user_kind_id')
go

--usunięcie tabeli bik_user_kind
drop table bik_user_kind
go

alter table bik_node_vote drop constraint uk_bik_node_vote
go

alter table bik_authors drop constraint uk_bik_authors
go

--przepinanie powiązań na bik_system_user id
declare @no_exec int = 0;
declare @table_name varchar(255);
declare @column_name varchar(255);
declare @nameFK varchar(100);
declare @sql varchar(max) = '';
declare @anonim_id int;
 
select @anonim_id = id from bik_system_user where login_name = '_Anonim_';

declare db_cursor cursor for
	select table_name, name
	from vw_ww_foreignkeys
	where ReferencedTableName = 'bik_user' and ReferencedColumn = 'id'
	and Table_Name <>'bik_system_user' and Table_Name <>'bik_user_in_node'
	
open db_cursor  
fetch next from db_cursor into @table_name, @column_name  

while @@fetch_status = 0  
begin  
print 'TABELKA : ' + @table_name + ' KOLUMNA: ' + @column_name;
	--  dodanie do tabeli nowej kolumny
	set @sql = 'alter table ' + @table_name + ' add  ' + @column_name+'_old int ';
	print @sql;
	if @no_exec = 0 
	exec (@sql);
	set @sql = '';
	
	--  zapisanie starych wartości z user_id do user_id_old
	set @sql = 'update ' + @table_name + ' set ' + @column_name+'_old = ' + @column_name;
	print @sql;
	if @no_exec = 0 
	exec (@sql);
	set @sql = '';
	
	--  usunięcie klucza obcego do bik_user
	select @nameFK = fk.ForeignKey_Name
	from sys.objects so inner join sys.columns sc on so.object_id = sc.object_id
		left join vw_ww_foreignkeys fk on fk.table_name = so.name and fk.name = sc.name
	where  so.name like @table_name and sc.name = @column_name
		
	if @nameFK is not null begin
	  set @sql = 'alter table ' + @table_name + ' drop constraint ' + @nameFK
	  print @sql
	  if @no_exec = 0 
		exec (@sql)
	end
	
	set @sql = ' update ' + @table_name + ' set ' + @column_name + ' = ';
		if not exists(	select 1
						from sys.tables st join sys.columns sc on st.object_id = sc.object_id
						where st.name = @table_name and sc.name = @column_name and sc.is_nullable = 0)
			set @sql = @sql + ' null';
		else
			set @sql = @sql + cast(@anonim_id as varchar(50));
	print @sql;
    if @no_exec = 0 
	exec (@sql)
	set @sql = '';
			
	--dodanie klucza obcego do bik_system_user do kolumny id
	set @sql = 'alter table ' + @table_name + ' add constraint FK_' + @table_name + '_' + @column_name + ' foreign key (' + @column_name + ') references bik_system_user (id)'
	print @sql;
    if @no_exec = 0 
	exec (@sql);
	set @sql = '';
	
	--  zamiana z id z bik_user na id z bik_system_user
	-- rozgałęzienie na kolumny nulowe i nie nulowe
	
	--najpierw te nullowe
	set @sql =	 'update ' + @table_name + ' set ' + @column_name + ' = ' +
								' bik_system_user.id 
								  from ' +  @table_name + ' join bik_system_user on ' + @table_name + '.' + @column_name + '_old ' + ' = bik_system_user.user_id '
	print @sql; 
    if @no_exec = 0 
	exec (@sql);
	set @sql = '';
	
       fetch next from db_cursor into @table_name, @column_name  
end  

close db_cursor  
deallocate db_cursor 
go
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
alter table bik_node_vote 
add constraint uk_bik_node_vote unique (node_id, user_id)
go

alter table bik_authors 
add constraint uk_bik_authors unique (node_id, user_id)
go

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
exec sp_update_version '1.1.1', '1.1.1.1';
go