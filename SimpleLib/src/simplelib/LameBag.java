package simplelib;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author wezyr
 */
public class LameBag<T> extends AbstractCollection<T> {

    private Map<T, Integer> map;
    private int size;

    public LameBag() {
        this.map = new LinkedHashMap<T, Integer>();
    }

    public LameBag(LameBag<? extends T> src) {
        this.map = new LinkedHashMap<T, Integer>(src.map);
        this.size = src.size;
    }

    public LameBag(T[] arr) {
        this();
        addAll(Arrays.asList(arr));
    }

    public int getUniqueCount() {
        return map.size();
    }

    public int getCount(T o) {
        Integer cnt = map.get(o);
        return cnt == null ? 0 : cnt;
    }

    public Set<T> uniqueSet() {
        return Collections.unmodifiableSet(map.keySet());
    }

    protected class LameBagIterator implements Iterator<T> {

        private Entry<T, Integer> e;
        private Iterator<Entry<T, Integer>> entryIter = map.entrySet().iterator();
        private int toIterInEntry;
        private boolean canRemove;

        public boolean hasNext() {
            return toIterInEntry > 0 || entryIter.hasNext();
        }

        public T next() {
            if (toIterInEntry == 0) {
                e = entryIter.next();
                toIterInEntry = e.getValue();
            }
            toIterInEntry--;
            canRemove = true;
            return e.getKey();
        }

        public void remove() {
            if (!canRemove) {
                throw new IllegalStateException("cannot remove item");
            }
            int itemsInEntry = e.getValue() - 1;
            if (itemsInEntry > 0) {
                e.setValue(itemsInEntry);
            } else {
                entryIter.remove();
            }
            size--;
            canRemove = false;
        }
    }

    protected boolean removeFromMap(T o) {
        Integer cnt = map.get(o);
        if (cnt == null || cnt == 0) {
            return false;
        }

        cnt--;
        if (cnt > 0) {
            map.put(o, cnt);
        } else {
            map.remove(o);
        }

        return true;
    }

    //-------------------------------
    // AbstractCollection methods
    public Iterator<T> iterator() {
        return new LameBagIterator();
    }

    public int size() {
        return size;
    }

    @Override
    public boolean add(T o) {
        addEx(o);
        return true;
    }

    public int addEx(T o) {
        Integer cnt = map.get(o);
        if (cnt == null) {
            cnt = 0;
        }
        cnt++;
        map.put(o, cnt);
        //lst.add(o);
        size++;
        return cnt;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean contains(Object o) {
        return map.containsKey((T) o);
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean remove(Object o) {
        Integer cnt = map.get((T) o);
        if (cnt == null) {
            return false;
        }
        cnt--;
        if (cnt > 0) {
            map.put((T) o, cnt);
        } else {
            map.remove((T) o);
        }
        size--;
        return true;
    }

    public String asString(String keyValSep, String entrySep) {
        return asString(keyValSep, entrySep, false);
    }

    public String asString(String keyValSep, String entrySep, boolean encodeForHtml) {
        StringBuilder sb = new StringBuilder();

        boolean first = true;
        for (Entry<T, Integer> e : map.entrySet()) {
            if (first) {
                first = false;
            } else {
                sb.append(entrySep);
            }
            String k = BaseUtils.safeToString(e.getKey());
            String v = BaseUtils.safeToString(e.getValue());
            if (encodeForHtml) {
                k = BaseUtils.encodeForHTMLTag(k);
                v = BaseUtils.encodeForHTMLTag(v);
            }
            sb.append(k).append(keyValSep).append(v);
        }

        return sb.toString();
    }
    
    public void sort(Comparator<T> comparator) {
        List<T> keys = new ArrayList<T>(map.keySet());
        
        Collections.sort(keys, comparator);
        Map<T, Integer> newMap = new LinkedHashMap<T, Integer>();
        for (T key : keys) {
            newMap.put(key, getCount(key));
        }
        
        map = newMap;
    }
    
//    public static void main(String[] args) {
//        System.out.println("zażółć gęślą jaźń");
//    }
}
