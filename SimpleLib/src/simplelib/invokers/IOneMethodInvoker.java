/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.invokers;

/**
 *
 * @author pmielanczuk
 */
public interface IOneMethodInvoker<T> {

    public Object invoke(T obj, Object... args);

    public boolean isParamAssignableFrom(int paramIdx, Object argVal);

    public int getArity();
    
}
