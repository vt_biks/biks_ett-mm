/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.lisa;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.lisa.LisaExportDictionaryDialog;
import pl.fovis.client.nodeactions.NodeActionBase;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryBean;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author ctran
 */
public class NodeActionLisaExport extends NodeActionBase {

    @Override
    public boolean isEnabledByDesign() {
        return isNodeOneOfTheKind(false,
                BIKConstants.NODE_KIND_LISA_DICTIONARY/*,
         BIKConstants.NODE_KIND_LISA_CATEGORY,
         BIKConstants.NODE_KIND_LISA_FOLDER,
         BIKConstants.NODE_KIND_LISA_CATEGORIZATION*/);
    }

    @Override
    public String getCaption() {
        return I18n.eksportuj.get();
    }

    @Override
    public void execute() {
//        Window.alert("nodeId=" + node.id + " objId=" + node.objId);
        final LisaDictionaryBean selectedDict = new LisaDictionaryBean();
        selectedDict.nodeId = node.id;
        selectedDict.dbName = node.objId;
        selectedDict.displayName = node.name;

        BIKClientSingletons.getLisaService().getLisaInstanceByNodeId(selectedDict.nodeId, new StandardAsyncCallback<LisaInstanceInfoBean>() {

            @Override
            public void onSuccess(LisaInstanceInfoBean result) {
                selectedDict.lisaId = result.id;
                selectedDict.lisaName = result.name;
                BIKClientSingletons.getLisaService().getDictionaryColumns(selectedDict == null ? "" : selectedDict.dbName, new StandardAsyncCallback<List<LisaDictColumnBean>>() {
                    @Override
                    public void onSuccess(List<LisaDictColumnBean> allColumnsOfSelectedDict) {
                        new LisaExportDictionaryDialog().buildAndShowDialog(selectedDict, allColumnsOfSelectedDict);
                    }
                });
            }
        });

    }
}
