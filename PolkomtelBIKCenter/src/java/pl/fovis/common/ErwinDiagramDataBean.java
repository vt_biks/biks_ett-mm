/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author pgajda
 */
public class ErwinDiagramDataBean implements Serializable {

    public List<ErwinTableBean> erwinListTables;
    public List<ErwinTableColumnBean> erwinTableColumns;
    public List<ErwinRelationshipBean> erwinRelationships;
    public List<ErwinShapeBean> erwinShapes;
}
