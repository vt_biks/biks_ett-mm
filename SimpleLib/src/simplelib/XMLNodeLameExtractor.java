/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author wezyr
 */
public class XMLNodeLameExtractor {

    public static final String NODE_NAME_KEY = "<name>";

    public static int findNodeNameEnd(String xmlTxt, int startPos) {
        int xmlLen = xmlTxt.length();

        while (startPos < xmlLen) {
            char c = xmlTxt.charAt(startPos);

            if (c == '=' || c == '>' || c == '/' || c == '\'' || c == '\"' || BaseUtils.isHTMLWhiteSpaceChar(c)) {
                break;
            }

            startPos++;
        }

        return startPos;
    }

    public static int findWhiteSpaceEnd(String xmlTxt, int startPos) {
        int xmlLen = xmlTxt.length();

        while (startPos < xmlLen && BaseUtils.isHTMLWhiteSpaceChar(xmlTxt.charAt(startPos))) {
            startPos++;
        }

        return startPos;
    }

    public static int findAssignmentEnd(String xmlTxt, int startPos) {
        //System.out.println("findAssignmentEnd: #1: startPos==" + startPos);
        startPos = findWhiteSpaceEnd(xmlTxt, startPos);
        //System.out.println("findAssignmentEnd: #2: startPos==" + startPos);
        if (startPos >= xmlTxt.length() || xmlTxt.charAt(startPos) != '=') {
            //System.out.println("findAssignmentEnd: startPos==" + startPos);
            return -1;
        }
        return findWhiteSpaceEnd(xmlTxt, startPos + 1);
    }

    public static int findValueEnd(String xmlTxt, int startPos) {
        int xmlLen = xmlTxt.length();

        if (startPos >= xmlLen) {
            return -1;
        }

        char qC = xmlTxt.charAt(startPos++);

        while (startPos < xmlLen && xmlTxt.charAt(startPos) != qC) {
            startPos++;
        }

        if (xmlTxt.charAt(startPos) != qC) {
            return -1;
        }

        return startPos + 1;
    }

    public static List<Map<String, String>> extractXmlNodes(String xmlTxt, Set<String> nodeNames,
            boolean lowerCaseNodeNames, boolean lowerCaseAttrNames) {
        List<Map<String, String>> res = new ArrayList<Map<String, String>>();

        int pos = 0;
        int xmlLen = xmlTxt.length();

        while (pos >= 0 && pos < xmlLen) {
            pos = xmlTxt.indexOf("<", pos);

            if (pos < 0) {
                break;
            }

            pos++;

            int pos2 = findNodeNameEnd(xmlTxt, pos);

            String nodeName = xmlTxt.substring(pos, pos2);

            if (lowerCaseNodeNames) {
                nodeName = nodeName.toLowerCase();
            }

            if (nodeNames.contains(nodeName)) {
                Map<String, String> m = new LinkedHashMap<String, String>();

                m.put(NODE_NAME_KEY, xmlTxt.substring(pos, pos2));

                while (pos2 >= 0 && pos2 < xmlLen) {

                    //System.out.println("#1, pos2=" + pos2);

                    pos2 = findWhiteSpaceEnd(xmlTxt, pos2);

                    if (pos2 >= xmlLen) {
                        break;
                    }

                    //System.out.println("#2, pos2=" + pos2);

                    char cc = xmlTxt.charAt(pos2);
                    if (cc == '/' || cc == '>') {
                        break;
                    }

                    //System.out.println("#3, pos2=" + pos2);
                    int pos3 = findNodeNameEnd(xmlTxt, pos2);

                    if (pos3 < 0 || pos3 == pos2) {
                        break;
                    }

                    //System.out.println("#4, pos3=" + pos3);
                    int pos4 = findAssignmentEnd(xmlTxt, pos3);

                    if (pos4 < 0) {
                        break;
                    }

                    //System.out.println("#5, pos4=" + pos4);
                    int pos5 = findValueEnd(xmlTxt, pos4);

                    if (pos5 < 0) {
                        break;
                    }

                    //System.out.println("#6, pos5=" + pos5);
                    String attrName = xmlTxt.substring(pos2, pos3);
                    String attrVal = xmlTxt.substring(pos4 + 1, pos5 - 1);

                    if (lowerCaseAttrNames) {
                        attrName = attrName.toLowerCase();
                    }

                    m.put(attrName, attrVal);

                    pos2 = pos5;
                }

                res.add(m);
            }
        }

        return res;
    }

    public static void main(String[] args) {
        //            01234567
        String xml = "<node x=\"a\" y=\'b\'/>";

        xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><WEBI_DOC_PROPS><WEBI_PROP NAME=\"VERSION\" VALUE=\"1.0\"/><WEBI_PROP NAME=\"is_refresh_on_open\" VALUE=\"false\"/> <WEBI_DP DPNAME=\"Query 1\" DPID=\"DP0\" DSNAME=\"PGE_Repo\" DSID=\"UnivCUID=AZgTKRHeThxChNBidDrShCI;UnivID=7362;ShortName=PGE_Repo;UnivName=PGE_Repo\"/> </WEBI_DOC_PROPS>";

        System.out.println(extractXmlNodes(xml, BaseUtils.paramsAsSet("webi_prop"), true, true));

        xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><WEBI_DOC_PROPS><WEBI_PROP NAME=\"VERSION\" VALUE=\"1.0\"/><WEBI_PROP NAME=\"is_refresh_on_open\" VALUE=\"false\"/> <WEBI_DP DPNAME=\"Query 1\" DPID=\"DP0\" DSNAME=\"PGE_Repo\" DSID=\"UnivCUID=AZgTKRHeThxChNBidDrShCI;UnivID=7362;ShortName=PGE_Repo;UnivName=PGE_Repo\"/> <WEBI_PROMPT QUESTION=\"Enter Mies:\" PID=\"Enter Mies:\" LOCALE=\"pl_PL\" OPTIONAL=\"no\" PERSISTENT=\"yes\" INDEXAWARE=\"no\" ORDER=\"0\" RESPONSE=\"unique\" ORIGIN=\"user\" TYPE=\"number\"><DPID>DP0</DPID> <DPID>DP0</DPID> </WEBI_PROMPT> <WEBI_PROMPT QUESTION=\"Enter Rok:\" PID=\"Enter Rok:\" LOCALE=\"pl_PL\" OPTIONAL=\"no\" PERSISTENT=\"yes\" INDEXAWARE=\"no\" ORDER=\"0\" RESPONSE=\"unique\" ORIGIN=\"user\" TYPE=\"number\"><DPID>DP0</DPID> <DPID>DP0</DPID> </WEBI_PROMPT> </WEBI_DOC_PROPS>";
        System.out.println(extractXmlNodes(xml, BaseUtils.paramsAsSet("webi_prop", "webi_prompt"), true, true));
    }
}
