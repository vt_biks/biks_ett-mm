/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.simpledblib;

import commonlib.DateUtils;
import simplelib.LameRuntimeException;
import commonlib.LameUtils;
import commonlib.LameUtils.SqlFlavor;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class ConnectionViaJdbc3 extends ConnectionViaJdbc {

    private Statement batchStmt;

    public ConnectionViaJdbc3(Map<String, Object> props) {
        super(props);
    }
    private List<String> batchTxtSplit;
    private List<StringBuilder> batchParts;

    @Override
    public void startBatch(String sqlTxt, int[] paramTypes) {
        if (batchStmt != null) {
            throw new LameRuntimeException("cannot start new batch (\"" +
                    sqlTxt + "\") while other (\"" + batchTxt + "\") is started");
        }
        try {
            batchStmt = conn.createStatement();
        //conn.setAutoCommit(false);
        } catch (Exception ex) {
            throw new LameRuntimeException("error preparing statement for sql: \"" + sqlTxt + "\"", ex);
        }
        batchTxt = sqlTxt;
        this.paramTypes = paramTypes;
        batchTxtSplit = LameUtils.splitBySep(sqlTxt, "?");
        batchParts = new ArrayList<StringBuilder>();
        batchParts.add(new StringBuilder());
    }

    protected String getParValAsSqlTxt(Object parVal, int jdbcType) {
        if (parVal == null) {
            return "NULL";
        }

        String res;

        switch (jdbcType) {
            case Types.INTEGER:
            case Types.DOUBLE:
                res = parVal.toString();
                break;
            case Types.DATE:
                res = "convert(datetime, '" +
                        DateUtils.getDateAsISO8601DateTimeString((Date) parVal) +
                        "', 126)";
                break;
            case Types.VARCHAR:
                res = LameUtils.toSqlString(parVal, SqlFlavor.Other);
                break;
            default:
                throw new LameRuntimeException("jdbcType=" + jdbcType + " is not supported, val=\"" + parVal + "\"");
        }

        return res;
    }

    protected String getExpliciteSqlForBatchParams(Object[] parVals) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < parVals.length; i++) {
            String txtPart = batchTxtSplit.get(i);
            sb.append(txtPart).append(getParValAsSqlTxt(parVals[i], paramTypes[i]));
        }

        sb.append(batchTxtSplit.get(parVals.length));

        return sb.toString();
    }

    protected StringBuilder getCurrentBatchPart() {
        return batchParts.get(batchParts.size() - 1);
    }
    public static final int MAX_BATCH_PART_SIZE = 32000;
    public static final String CMD_IN_BATCH_SEP = "\r\n";

    @Override
    public void addToBatch(Object[] parVals) {
        if (batchStmt == null) {
            throw new LameRuntimeException("no batch is started");
        }
        try {
            String explSql = getExpliciteSqlForBatchParams(parVals);
            //batchStmt.addBatch(explSql);
            if (explSql.length() > MAX_BATCH_PART_SIZE) {
                throw new LameRuntimeException(
                        "explicite sql text length (" + explSql.length() +
                        ") exceeds MAX_BATCH_PART_SIZE (" + MAX_BATCH_PART_SIZE +
                        "), txt=\"" + explSql + "\"");
            }
            StringBuilder bp = getCurrentBatchPart();
            if (bp.length() + explSql.length() + CMD_IN_BATCH_SEP.length() > MAX_BATCH_PART_SIZE) {
                bp = new StringBuilder();
                batchParts.add(bp);
            } else {
                bp.append(CMD_IN_BATCH_SEP);
            }
            bp.append(explSql);

        } catch (Exception ex) {
            throw new LameRuntimeException("error adding to batch for sql: \"" +
                    batchTxt + "\" and param values: " +
                    Arrays.toString(parVals), ex);
        }
    }

    @Override
    public void execBatch() {
        try {
            for (StringBuilder sb : batchParts) {
                batchStmt.execute(sb.toString());
            }
            conn.commit();
        } catch (Exception ex) {
            throw new LameRuntimeException("error executing batch for sql: \"" +
                    batchTxt + "\"", ex);
        }
        clearBatch();
    }

    @Override
    public void clearBatch() {
        try {
            if (batchStmt != null) {
                batchStmt.clearBatch();
                batchStmt.close();
                conn.rollback();
            }
        //conn.setAutoCommit(true);
        } catch (Exception ex) {
            throw new LameRuntimeException("error clearing batch for sql: \"" +
                    batchTxt + "\"", ex);
        }
        batchStmt = null;
        batchTxt = null;
        batchParts = null;
    }
}
