/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import pl.fovis.client.entitydetailswidgets.lisa.WidgetWithValue;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryValue;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tbzowka
 */
public class LisaDictDialog extends BaseActionOrCancelDialog {

    private List<WidgetWithValue> fields = new ArrayList<WidgetWithValue>();
    private List<LisaDictColumnBean> columns;
    private Map<String, LisaDictionaryValue> records;
    private IParametrizedContinuation<Map<String, LisaDictionaryValue>> saveCont;

    @Override
    protected String getDialogCaption() {
        return I18n.edycjaSlownika.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {

        int gridSize = columns.size();
        Grid grid = new Grid(gridSize, 2);

        for (int i = 0; i < gridSize; i++) {
            LisaDictColumnBean bean = columns.get(i);

            String labelText = bean.titleToDisplay;
//            if (bean.isActive) {
//                labelText += "*";
//            }

            LisaDataType dictionaryEnum = LisaDataType.getValueFor(bean.dataType.trim());
            WidgetWithValue editedField = dictionaryEnum.createWidget();

            LisaDictionaryValue colValue = records.get(bean.nameInDb);
            editedField.setValue(colValue.getValue() != null ? colValue.getValue() : null);
            if (bean.isReadOnlyAccess()) {
                editedField.setEditable(false);
            }
            fields.add(editedField);

            grid.setWidget(i, 0, new Label(labelText));
            grid.setWidget(i, 1, editedField.getWidget());

        }

        ScrollPanel sp = new ScrollPanel(grid);
        sp.setStyleName("scrollPanelMaxHight");
        //int height = (int)(Window.getClientHeight()*0.7)
        //sp.setHeight(height+"px");

        main.add(sp);
    }

    @Override
    protected void doAction() {

        Map<String, LisaDictionaryValue> afterEditMap = new HashMap<String, LisaDictionaryValue>();

        int gridSize = columns.size();
        for (int i = 0; i < gridSize; i++) {
            LisaDictColumnBean bean = columns.get(i);
            afterEditMap.put(bean.nameInDb, fields.get(i).getValue());
        }

        saveCont.doIt(afterEditMap);

    }

    public void buildAndShowDialog(List<LisaDictColumnBean> columns, Map<String, LisaDictionaryValue> records, IParametrizedContinuation<Map<String, LisaDictionaryValue>> saveCont) {
        this.columns = columns;
        this.records = records;
        this.saveCont = saveCont;

        super.buildAndShowDialog();
    }
}
