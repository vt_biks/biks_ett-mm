/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author wezyr
 */
public interface INamedPropGetter<Bean> {

    public Object getValue(Bean o);
}

