/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public interface IBIKPumpTaskNew {

    public Integer setConnectionAndLog(String urlForPublicAccess, String optDirForUpload, IBeanConnectionEx<Object> mc, INamedSqlsDAO<Object> adhoDAO, IPumpLogger log);

    public void setOptPumpInstanceName(String instanceName);

//    public void setConnectionWithoutLog(IBeanConnectionEx<Object> mc);
    public Pair<Integer, String> pump();

    public String getPumpGroup();

    public String getSourceName();

    public boolean isActionAfterNeeded();
}
