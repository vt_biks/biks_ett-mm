﻿SELECT (physical_memory_in_use_kb/1024) AS Memory_usedby_Sqlserver_MB, 
(locked_page_allocations_kb/1024) AS Locked_pages_used_Sqlserver_MB, 
(total_virtual_address_space_kb/1024) AS Total_VAS_in_MB, process_physical_memory_low, 
process_virtual_memory_low 
FROM sys.dm_os_process_memory;

select * from sys.dm_os_sys_memory

sp_configure 'show advanced options', 1;
GO
RECONFIGURE;
GO

exec sp_configure




SELECT TEXT, query_plan, requested_memory_kb,
granted_memory_kb,used_memory_kb, wait_order
FROM sys.dm_exec_query_memory_grants MG
CROSS APPLY sys.dm_exec_sql_text(sql_handle)
CROSS APPLY sys.dm_exec_query_plan(MG.plan_handle)



select *
from sys.dm_exec_query_memory_grants
