/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.i18nsupport;

// i18n-default: no
import java.util.Collection;
import java.util.Set;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public class I18nMessagePlEn extends I18nMessage {

    public static final Set<String> localeNames = BaseUtils.splitUniqueBySep("pl,en" /* i18n: no */, ",", true);

    public I18nMessagePlEn(String... msgsInLangs) {
        super(msgsInLangs);
    }

    @Override
    protected Collection<String> getLocaleNames() {
        return localeNames;
    }
}
