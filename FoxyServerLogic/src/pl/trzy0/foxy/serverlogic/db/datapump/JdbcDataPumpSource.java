/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db.datapump;

import commonlib.LameUtils;
import commonlib.datapump.ColumnMetadata;
import commonlib.datapump.IDataPumpSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import simplelib.BaseUtils;
import simplelib.StackTraceUtil;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public class JdbcDataPumpSource implements IDataPumpSource {

    public static class JdbcColumnMetadata extends ColumnMetadata {

        public ResultSetMetaData metaData;

        public JdbcColumnMetadata(String colName, String typeName, int displaySize, int prec, int scale, Boolean isNullable, int colIdx, ResultSetMetaData metaData) {
            super(colName, typeName, displaySize, prec, scale, isNullable, colIdx);
            this.metaData = metaData;
        }

        @Override
        public String toString() {

            String javaClassName;
            try {
                javaClassName = metaData.getColumnClassName(colIdx + 1);
            } catch (SQLException ex) {
                javaClassName = "<unknown>";
            }

            return '{' + "colName=" + colName + ", typeName=" + typeName + ", displaySize=" + displaySize + ", prec=" + prec
                    + ", scale=" + scale + ", isNullable=" + isNullable + ", colIdx=" + colIdx + ", java class: " + javaClassName + '}';
        }
    }
    private static final ILameLogger logger = LameUtils.getMyLogger();

    protected Connection srcConn;
    protected String srcSqlTxt;
    protected Statement stmt;
    protected ResultSet rset;
    protected ColumnMetadata[] sourceColsMetadata;
    protected String optObjsForGrantsTesting;
    protected IBeanConnectionEx<Object> conn;

    public JdbcDataPumpSource(Connection srcConn, String srcSqlTxt, IBeanConnectionEx<Object> conn, String optObjsForGrantsTesting) {
        this.srcConn = srcConn;
        this.srcSqlTxt = srcSqlTxt;
        this.conn = conn;
        this.optObjsForGrantsTesting = optObjsForGrantsTesting;
    }

    @Override
    public ColumnMetadata[] openSource() throws Exception {
        try {
//            System.out.println("openSource - inner");
            return openSourceInner();
        } catch (Exception ex) {
            System.out.println("openSource - exception");
            throw new RuntimeException("exception in openSource"
                    + (BaseUtils.isStrEmptyOrWhiteSpace(optObjsForGrantsTesting) ? ""
                            : "\n" + BeanConnectionUtils.optTestGrantsPerObjects(conn, optObjsForGrantsTesting))
                    + "\n" + StackTraceUtil.getCustomStackTrace(ex),
                    ex
            );
        }
    }

    protected ColumnMetadata[] openSourceInner() throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("openSource for sql: " + srcSqlTxt);
        }

        stmt = srcConn.createStatement();
        rset = stmt.executeQuery(srcSqlTxt);
        ResultSetMetaData md = rset.getMetaData();
        final int columnCount = md.getColumnCount();

        sourceColsMetadata = new ColumnMetadata[columnCount];

        for (int i = 1; i <= columnCount; i++) {
            String colName = md.getColumnName(i);
            String typeName = md.getColumnTypeName(i);
            int displaySize = md.getColumnDisplaySize(i);
            int prec = md.getPrecision(i);
            int scale = md.getScale(i);
            int nullable = md.isNullable(i);
//            Boolean isNullable = nullable == 0 ? false : (nullable == 2 ? null : true);

            Boolean isNullable;
            if (nullable == 0) {
                isNullable = false;
            } else if (nullable == 2) {
                isNullable = null;
            } else {
                isNullable = true;
            }
//            System.out.println("nullable=" + nullable + ", isNullable=" + isNullable);
            JdbcColumnMetadata col = new JdbcColumnMetadata(colName, typeName, displaySize, prec, scale, isNullable, i - 1, md);

            if (logger.isDebugEnabled()) {
                logger.debug("col=" + col);
            }

            sourceColsMetadata[i - 1] = col;
        }

        return sourceColsMetadata;
    }

    @Override
    public void closeSource() throws Exception {
        try {
            rset.close();
        } finally {
            stmt.close();
        }
    }

    @Override
    public boolean hasSourceData() throws Exception {
        return rset.next();
    }

    @Override
    public Object[] readDataRow() throws Exception {

        Object[] res = new Object[sourceColsMetadata.length];

        for (int i = 1; i <= sourceColsMetadata.length; i++) {
            final int im1 = i - 1;

            if ("TIMESTAMP".equals(sourceColsMetadata[im1].typeName)) {
                res[im1] = rset.getTimestamp(i);
            } else {
                res[im1] = rset.getObject(i);
            }
        }

        return res;
    }
}
