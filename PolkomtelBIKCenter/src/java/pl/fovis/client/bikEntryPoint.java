/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;
import pl.fovis.client.menu.LoginLogoutHeader;
import pl.fovis.common.StartupDataBean;
import pl.fovis.common.mltx.MultixConstants;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.InitFactories4GWT;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.i18nsupport.EntryPointWithI18nSupport;

/**
 *
 * @author pku
 */
public class bikEntryPoint extends EntryPointWithI18nSupport {

    public static HandlerRegistration historyHandlerRegistration = null;

    static {
        InitFactories4GWT.initFactories();
        BIKClientSingletons.setupRpcRequestBuilderParams();
    }

    private HTML setupInfoErrorLabel(String labelId) {
        HTML label = HTML.wrap(GWTUtils.getElementById(labelId));
        label.setVisible(false);
        return label;
    }

    @Override
    protected void onModuleLoadProper() {
//        System.out.println("We always start from EntryPoint");
//        List<String> jsLibs = BaseUtils.splitBySep(BIKClientSingletons.getJsLibs(), ";", true);
//        for (String jsScript : jsLibs) {
//            ScriptInjector.fromUrl(jsScript).setRemoveTag(false).setWindow(ScriptInjector.TOP_WINDOW).inject();
//        ScriptInjector.fromUrl("https://www.gstatic.com/charts/loader.js").setRemoveTag(false).setWindow(ScriptInjector.TOP_WINDOW).inject();
//        }
//        Resources.INSTANCE.visCss().ensureInjected();
        BIKClientSingletons.showAppAndDBVersions(MultixConstants.MLTX_APP_VERSION, "???");
//        Window.alert("Window.Location.getHref()=" + Window.Location.getHref()
//                + "\nWindow.Location.getQueryString()=" + Window.Location.getQueryString());

        BIKClientSingletons.markToIgnoreDifferentUserInNextCall();

        BIKClientSingletons.getMultiXService().getStartupData(new StandardAsyncCallback<StartupDataBean>() {
            @Override
            public void onSuccess(final StartupDataBean sdb) {
                boolean isMultiModeNotLogged = (sdb.isMultiMode && sdb.loggedUser == null) || (sdb.disableGuestMode && sdb.loggedUser == null);

                BIKClientSingletons.setNotificationLabels(setupInfoErrorLabel("infoPanel"), setupInfoErrorLabel("errorPanel"));
                BIKClientSingletons.setBrowserTitle();

                if (sdb.isMultiMode) {
                    BIKClientSingletons.setupAppProps(sdb.appProps, sdb.isMultiMode);
                    BIKClientSingletons.showAppAndDBVersions(sdb.appVersion, BIKClientSingletons.getDBVersion());
                    if (sdb.loggedUser == null) {
//                        BIKClientSingletons.performOnLoginLogoutCont = new IContinuation() {
//                            public void doIt() {
//                                reloadWithDb();
////                                reloadWithDefaultDb();
//                            }
//                        };
                        //                    new EDPForms(/*this*/).showLoginDialog();

                        if (sdb.defaultSingleBiksDb != null) {
                            BIKClientSingletons.reloadWithSetDb(sdb.defaultSingleBiksDb);
                            return;
                        }

                        new bikMltxEntryPoint().init();
                    } else {
//                        String db = UrlParser.getParameterValue(Window.Location.getHref(), MultixConstants.DB_NAME_PARAM);
                        String db = Window.Location.getParameter(MultixConstants.DB_NAME_PARAM);
                        if (db == null) {
                            new bikMltxEntryPoint().init();
                        } else {
                            new bikcenterEntryPoint().init(sdb);
                        }
                    }
                } else {
                    new bikcenterEntryPoint().init(sdb);
                }
                hideUnnecessaryWidgets(isMultiModeNotLogged);
                LoginLogoutHeader.reRender(sdb.isMultiMode);
//                if (!BIKClientSingletons.isVisInjected()) {
//                    StyleInjector.inject(BIKResources.INSTANCE.visCss().getText(), true);
//                    ScriptInjector.fromString(BIKResources.INSTANCE.visJs().getText()).setRemoveTag(false).setWindow(ScriptInjector.TOP_WINDOW).inject();
//                }
            }

            @Override
            public void onFailure(Throwable caught) {
                bikcenterEntryPoint.onInitializationFailure(caught, "getStartupData" /* i18n: no */);
                //super.onFailure(caught); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }

//    private void reloadWithDb() {
//        String dbToReload = BIKClientSingletons.getCurrentDbNameFromUrl();
//        if (BaseUtils.isStrEmptyOrWhiteSpace(dbToReload)) {
//            dbToReload = BIKClientSingletons.getLoggedUserBean().defaultDbName;
//        }
//        BIKClientSingletons.reloadWithSetDb(dbToReload);
//    }
//    private void reloadWithDefaultDb() {
//        String initialDb = BIKClientSingletons.getLoggedUserBean().defaultDbName;
//        BIKClientSingletons.reloadWithSetDb(initialDb);
//    }
    public static void registerHistoryHandler(HandlerRegistration h) {
        if (historyHandlerRegistration != null) {
            historyHandlerRegistration.removeHandler();
        }
        historyHandlerRegistration = h;
//        History.newItem("", false);
    }

    public static void hideUnnecessaryWidgets(boolean isMultiModeNotLogged) {
        RootPanel.get("szuk4NaqNaq").setVisible(!isMultiModeNotLogged); // pole wyszukiwania zaszyte w html
    }
}
