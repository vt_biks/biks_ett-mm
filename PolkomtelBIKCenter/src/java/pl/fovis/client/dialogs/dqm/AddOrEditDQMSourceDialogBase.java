/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public abstract class AddOrEditDQMSourceDialogBase extends BaseActionOrCancelDialog {

    protected final static String TEST_FILE_SUFFIX = "_yyyyMMddhhmmss.csv";
    protected IParametrizedContinuation<DQMMultiSourceBean> saveCont;
//    protected Map<Integer, SourceType> indexToSourceTypeMap;
    protected Integer nodeId;
    protected DQMMultiSourceBean bean;
    protected boolean isNewSource;
//    protected Integer databaseNodeId;
//    protected String serverName;
    protected String sqlTestText;
    protected TextBox codeNameTbx;
//    protected TextBox fileNameTbx;
    protected ListBox sourceLbx;
    protected TextArea scopeArea;
//    protected Label fileNameLbl;

    protected TextArea queryArea;
    protected VerticalPanel databaseVP;
    protected VerticalPanel profileVP;

    @Override
    protected String getDialogCaption() {
        return isNewSource ? I18n.dodajZrodlo.get() : I18n.edytujZrodlo.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return isNewSource ? I18n.dodaj.get() : I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {
//        indexToSourceTypeMap = new HashMap<Integer, SourceType>();
        FlexTable table = new FlexTable();
        databaseVP = new VerticalPanel();
        profileVP = new VerticalPanel();
        table.setHTML(0, 0, I18n.nazwaKodowaZrodla.get());
        table.setHTML(1, 0, I18n.zakresDanych.get());
        table.setHTML(2, 0, getName());
        table.setWidget(0, 1, codeNameTbx = new TextBox());
        table.setWidget(1, 1, scopeArea = new TextArea());
        table.setWidget(2, 1, sourceLbx = new ListBox());
        table.getColumnFormatter().setWidth(0, "180px");
        codeNameTbx.setWidth("200px");
        scopeArea.setWidth("200px");
        scopeArea.setHeight("70px");
        fillLbx();
//        int index = 0;
//        for (SourceType value : SourceType.values()) {
////            if (value.equals(SourceType.Multi)) {
////                continue;
////            }
//            sourceLbx.addItem(value.getName());
//            indexToSourceTypeMap.put(index++, value);
//        }
//        sourceLbx.addChangeHandler(new ChangeHandler() {
//
//            @Override
//            public void onChange(ChangeEvent event) {
//                setSelectedSourceWidget();
//                cleanWidgets();
//            }
//        });
//        FlexTable databaseFT = new FlexTable();
//        FlexTable profileFT = new FlexTable();
//        HorizontalPanel databaseHP = new HorizontalPanel();
//        databaseHP.add(databaseNameLbl = new Label(I18n.brak.get()));
//        PushButton selectDatabaseBtn = new PushButton(I18n.edytuj.get());
//        NewLookUtils.makeCustomPushButton(selectDatabaseBtn);
//        selectDatabaseBtn.addClickHandler(new ClickHandler() {
//
//            @Override
//            public void onClick(ClickEvent event) {
//                new TreeSelectorForDQMDatabase().buildAndShowDialog(nodeId, indexToSourceTypeMap.get(sourceLbx.getSelectedIndex()).getTreeCode(), new IParametrizedContinuation<TreeNodeBean>() {
//
//                    @Override
//                    public void doIt(TreeNodeBean param) {
//                        databaseNameLbl.setText(param.name);
//                        databaseNodeId = param.id;
//                        serverName = BaseUtils.getFirstItems(param.objId, "|", 1);
//                    }
//                });
//            }
//        });
//        databaseHP.add(selectDatabaseBtn);
//        databaseFT.setHTML(0, 0, I18n.bazaDanych.get());
//        databaseFT.setWidget(0, 1, databaseHP);
//        databaseFT.getColumnFormatter().setWidth(0, "180px");
//        databaseVP.add(databaseFT);
//        databaseVP.add(new Label(I18n.zapytaniePobierajaceDane.get()));
//        databaseVP.add(queryArea = new TextArea());
//        queryArea.setWidth("380px");
//        queryArea.setHeight("200px");
//        profileFT.setHTML(0, 0, I18n.plikZDanymi.get());
//        profileFT.setHTML(1, 0, I18n.nazwaPlikuZDanymiNaFTP.get());
//        profileFT.setWidget(0, 1, fileNameTbx = new TextBox());
//        fileNameTbx.addKeyUpHandler(new KeyUpHandler() {
//
//            @Override
//            public void onKeyUp(KeyUpEvent event) {
//                updateLabelTexts();
//            }
//        });
//        fileNameTbx.setWidth("200px");
//        profileFT.setWidget(1, 1, fileNameLbl = new Label(""));
//        profileFT.getColumnFormatter().setWidth(0, "180px");
//        profileVP.add(profileFT);
        main.add(table);
        innerWidget();
//        main.add(databaseVP);
//        main.add(profileVP);
        fillWidgets();
        setSelectedSourceWidget();
    }

    protected abstract String getName();

    protected abstract void fillLbx();

    protected abstract void innerWidget();

//    protected void updateLabelTexts() {
//        String text = fileNameTbx.getText().trim();
//        boolean isStrEmpty = BaseUtils.isStrEmptyOrWhiteSpace(text);
//        fileNameLbl.setText(isStrEmpty ? "" : text + TEST_FILE_SUFFIX);
//    }
//    protected SourceType getSelectedSource() {
//        return indexToSourceTypeMap.get(sourceLbx.getSelectedIndex());
//    }
    @Override
    protected void doAction() {
        if (bean == null) {
            bean = new DQMMultiSourceBean();
        }
//        bean.databaseName = databaseNameLbl.getText();
//        bean.databaseNodeId = databaseNodeId;
//        bean.serverName = serverName;
//        bean.resultFileName = fileNameTbx.getText();
//        bean.sourceName = sourceLbx.getValue(sourceLbx.getSelectedIndex());
        bean.sqlTest = queryArea.getText();
        bean.codeName = codeNameTbx.getText();
        bean.dataScope = scopeArea.getText();
        doActionOpt(bean);
        saveCont.doIt(bean);
    }

    protected abstract void doActionOpt(DQMMultiSourceBean bean);

//    @Override
//    protected boolean doActionBeforeClose() {
//        boolean isProfileSelected = getSelectedSource().equals(AddOrEditDQMTestDialogForDatabases.SourceType.Profile);
//        if (BaseUtils.isStrEmptyOrWhiteSpace(codeNameTbx.getText())) {
//            new SimpleInfoDialog().buildAndShowDialog(I18n.nazwaKodowaJestPusta.get(), I18n.blad.get(), null);
//            return false;
//        }
////        if (!isProfileSelected && databaseNodeId == null) {
////            new SimpleInfoDialog().buildAndShowDialog(I18n.wybierzBazeDanych.get(), I18n.blad.get(), null);
////            return false;
////        }
//        if (!isProfileSelected && BaseUtils.isStrEmptyOrWhiteSpace(queryArea.getText())) {
//            new SimpleInfoDialog().buildAndShowDialog(I18n.zapytaniePobierajaceDaneNieMozeBycPuste.get(), I18n.blad.get(), null);
//            return false;
//        }
//        if (isProfileSelected && BaseUtils.isStrEmptyOrWhiteSpace(fileNameTbx.getText())) {
//            new SimpleInfoDialog().buildAndShowDialog(I18n.nazwaPlikuNaSFTPNieMozeBycPusta.get(), I18n.blad.get(), null);
//            return false;
//        }
//        return true;
//    }
    public void buildAndShowDialog(int nodeId, DQMMultiSourceBean bean, IParametrizedContinuation<DQMMultiSourceBean> saveCont) {
        this.nodeId = nodeId;
        this.bean = bean;
        this.isNewSource = bean == null;
        this.saveCont = saveCont;
        super.buildAndShowDialog();
    }

    protected abstract void cleanWidgetsOpt();

    protected void cleanWidgets() {
//        databaseNodeId = null;
//        databaseNameLbl.setText("");
//        fileNameTbx.setText("");
        sqlTestText = "";
        queryArea.setText("");
        cleanWidgetsOpt();
    }

    protected abstract void setSelectedSourceWidget();
//    protected void setSelectedSourceWidget() {
//        SourceType type = indexToSourceTypeMap.get(sourceLbx.getSelectedIndex());
//        boolean isProfileSelected = type.equals(SourceType.Profile);
//        databaseVP.setVisible(!isProfileSelected);
//        profileVP.setVisible(isProfileSelected);
//    }

    protected void fillWidgets() {
        if (bean != null) {
            codeNameTbx.setText(bean.codeName);
            scopeArea.setText(bean.dataScope);
//            databaseNameLbl.setText(!BaseUtils.isStrEmptyOrWhiteSpace(bean.databaseName) ? bean.databaseName : "");
//            databaseNodeId = bean.databaseNodeId;
            queryArea.setText(!BaseUtils.isStrEmptyOrWhiteSpace(bean.sqlTest) ? bean.sqlTest : "");
            sqlTestText = bean.sqlTest;

//            int index = 0;
//            for (Map.Entry<Integer, SourceType> entrySet : indexToSourceTypeMap.entrySet()) {
//                String sourceName = entrySet.getValue().getSourceName();
//                if (sourceName.equals(bean.sourceName.trim())) {
//                    index = entrySet.getKey();
//                    break;
//                }
//            }
            sourceLbx.setSelectedIndex(getSelectedIdx());
//            lbxSetSelected();
            updateLabelTexts();
        }
    }

    protected void updateLabelTexts() {

    }

    protected abstract int getSelectedIdx();
}
