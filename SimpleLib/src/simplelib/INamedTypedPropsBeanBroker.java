/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author pmielanczuk
 * @param <B>
 */
public interface INamedTypedPropsBeanBroker<B> extends INamedPropsClassInfo {

    public Object getPropValue(B bean, String propName);

    public void setPropValue(B bean, String propName, Object value);
}
