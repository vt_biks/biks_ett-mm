/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tbzowka
 */
public class LisaLogBean implements Serializable {

    public Integer id;
    public Integer systemUserId;
    public Date timeAdded;
    public String teradataLogin;
    public String dictionaryName;
    public String info;
    public String systemUserName;
}
