exec sp_check_version '1.5.x.8';
go

if not exists (select * from sysobjects where name='bik_lisa_extradata' and xtype='U')
begin
	create table bik_lisa_extradata (
		node_id integer not null primary key,
		ref_table_name varchar(256) not null,
		ref_column_name varchar(256) not null,
		foreign key (node_id) references bik_node(id)
	)

	exec sp_add_menu_node null, 'Sandbox', 'Sandbox'
end
go

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees');
declare @menuNodeId int = (select id from bik_node where tree_id = @tree_id and obj_id = 'Sandbox');   


        
update bik_node set parent_node_id = @menuNodeId where tree_id = @tree_id and 
obj_id = (select obj_id from bik_node where tree_id = @tree_id and obj_id = '$DictionaryDWH')


exec sp_node_init_branch_id @tree_id, @menuNodeId
go

exec sp_update_version '1.5.x.8', '1.5.x.9';
go