﻿exec sp_check_version '1.6.z3.4';
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

if not exists(SELECT 1 FROM sys.indexes WHERE name = 'idx_bik_confluence_extradata_node_id_short_link' AND object_id = OBJECT_ID('bik_confluence_extradata'))
  create index idx_bik_confluence_extradata_node_id_short_link ON bik_confluence_extradata (node_id, short_link)
go

if not exists(SELECT 1 FROM sys.indexes WHERE name = 'idx_bik_confluence_extradata_short_link_node_id' AND object_id = OBJECT_ID('bik_confluence_extradata'))
  create index idx_bik_confluence_extradata_short_link_node_id ON bik_confluence_extradata (short_link, node_id)
go

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

exec sp_update_version '1.6.z3.4', '1.6.z3.5';
go
