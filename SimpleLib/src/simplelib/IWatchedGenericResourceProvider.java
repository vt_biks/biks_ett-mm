/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author pmielanczuk
 */
public interface IWatchedGenericResourceProvider<T> extends IGenericResourceProvider<T>, IWatchedResources {

    public boolean hasBeenModifiedRecently(String resourceName);
}
