/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.List;

/**
 *
 * @author wezyr
 */
public class ListCollector<T> extends ListCollectorEx<T, T> {

    public ListCollector(List<T> list) {
        super(null, list);
    }

    public ListCollector() {
        super(null, null);
    }

    @Override
    protected T convertItem(T item) {
        return item;
    }
}
