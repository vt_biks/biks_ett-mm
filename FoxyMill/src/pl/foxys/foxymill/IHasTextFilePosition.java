/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill;

import simplelib.TextFilePosition;

/**
 *
 * @author wezyr
 */
public interface IHasTextFilePosition {
    public TextFilePosition getTextFilePosition();
}
