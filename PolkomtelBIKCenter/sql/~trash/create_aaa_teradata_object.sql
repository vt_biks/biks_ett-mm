

/****** Object:  Table [dbo].[aaa_teradata_object]    Script Date: 18/08/2011 09:56:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[aaa_teradata_object](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[parent_id] [int] NULL,
        [data_load_log_id] [int] NOT NULL,
        [type] [varchar](100) NOT NULL,
	[name] [varchar](100) NOT NULL,
        [extra_info] [varchar](5000) NULL
PRIMARY KEY CLUSTERED
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [aaa_teradata_object_uniq] UNIQUE NONCLUSTERED
(
        [data_load_log_id] ASC,
        [parent_id] ASC,
	[type] ASC,
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[aaa_teradata_object] WITH CHECK ADD FOREIGN KEY([data_load_log_id])
REFERENCES [dbo].[aaa_data_load_log] ([id])
GO

SET ANSI_PADDING OFF
GO
