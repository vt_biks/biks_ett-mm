/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.SimpleProgressInfoDialog;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public class WaitingForLoginAgainDialog extends SimpleProgressInfoDialog {

//    protected IContinuation whenOkCont;
    protected Timer t;

//    public void buildAndShowDialog(boolean isUnloggedOnServer, final IContinuation whenOkCont) {
    public void buildAndShowDialog(boolean isUnloggedOnServer) {

//        this.whenOkCont = whenOkCont;
        BIKClientSingletons.showingWaitingForLoginAgainDialog = true;

        super.buildAndShowDialog(I18n.oczekiwanieNaZalogowanie.get(),
                getMainWidgetText(isUnloggedOnServer), true);

        t = new Timer() {

            @Override
            public void run() {
                BIKClientSingletons.getService().getLoggedUserName(new StandardAsyncCallback<String>() {

                    @Override
                    public void onSuccess(String result) {
                        checkUserLoggedOnServer(result);
                    }
                });
            }
        };

        t.scheduleRepeating(1000);
    }

    protected static String getMainWidgetText(boolean isUnloggedOnServer) {
        return I18n.aktualnieJestes.format(isUnloggedOnServer ? I18n.niezalogowany.get() : I18n.zalogowanyJakoInny.get(), BIKClientSingletons.getLoggedUserLoginName());
    }

    protected void checkUserLoggedOnServer(String result) {
        if (BaseUtils.safeEquals(BIKClientSingletons.getLoggedUserLoginName(), result)) {
            t.cancel();
//            Window.alert(I18n.okMozeszKontynuowacPrace.get());
            hideDialog();
            loginChangedToGood();
        } else {
            main.clear();
            main.add(new Label(getMainWidgetText(result == null)));
        }
    }

    public static void loginChangedToGood() {
        BIKClientSingletons.showingWaitingForLoginAgainDialog = false;
        BIKClientSingletons.invalidateAllTabs();
    }
}
