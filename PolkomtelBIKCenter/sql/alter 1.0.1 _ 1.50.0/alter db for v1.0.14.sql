exec sp_check_version '1.0.14';
go

----------------------------------------------------------------------
----------------------------------------------------------------------

select * from bik_node_kind
insert into bik_node_kind (code, caption)
values ('TezaurusCategory', 'Katalog poj��')
go

update bik_node 
set node_kind_id = (select id from bik_node_kind where code = 'TezaurusCategory')
--select * 
from bik_node n inner join bik_tree t on n.tree_id = t.id
where t.name = 'Tezaurus'
and exists (select * from bik_node cn where cn.tree_id = n.tree_id and cn.branch_ids like n.branch_ids + '%' and cn.id <> n.id)
go

----------------------------------------------------------------------
----------------------------------------------------------------------

declare @node_id int;

select @node_id = bik_node.id
from bik_node join bik_tree on bik_node.tree_id = bik_tree.id
where bik_tree.name = 'Teradata' and bik_node.name like 'VD_US_DM_SPRZEDAZ'

--select @node_id

insert into bik_monitor (name, caption, node_id)
values('data zasilenia', '28.03.2011', @node_id)

insert into bik_monitor (name, caption, node_id)
values('aktualno�� danych', '27.03.2011', @node_id)

insert into bik_monitor (name, caption, node_id)
values('proces zasilaj�cy', 'pusty', @node_id)

insert into bik_monitor (name, caption, node_id)
values('liczba zapyta� /dzie�', '43', @node_id)

insert into bik_monitor (name, caption, node_id)
values('liczba raport�w powi�zanych', '15', @node_id)

insert into bik_monitor (name, caption, node_id)
values('liczba u�ytkownik�w', '24', @node_id)
go

----------------------------------------------------------------------
----------------------------------------------------------------------

update bik_metapedia set subject = rtrim(ltrim(subject))
go


update
--select * from 
bik_metapedia 
set body = 'Odej�cie klienta (klient�w), zaprzestanie korzystania z us�ugi danej firmy. Churn mo�e by� dzia�aniem formalnym � np. z�o�enie przez klienta wniosku o rezygnacj� z us�ug lub dzia�aniem nie formalnym � np. zaprzestanie p�atno�ci faktur bez wypowiedzenia umowy. Definiujemy ten wska�nik jako liczb� os�b, kt�ra przestala korzysta� z danego produktu czy us�ugi w okre�lonym czasie, podzielona przez �redni� liczb� og�lnych u�ytkownik�w wspomnianego produktu lub us�ugi. Zjawisko churn pokazuje przyrost lub spadek liczby og�lnej konsument�w danego produktu/us�ugi, jak r�wnie� �redni czas u�ytkowania (�ycia) danego produktu/us�ugi.'
where subject = 'Churn'
go

alter table bik_metapedia add constraint uk_bik_metapedia_subject unique (subject)
go

/*
select * from 
bik_metapedia m inner join bik_node n on m.subject = n.name and n.tree_id = 8
select * from bik_tree
*/

update bik_metapedia set subject = 'Reaktywacja' where subject = 'Rekatywacja'
go

update 
--select * from 
bik_node 
set name = 'reaktywacja'
where name = 'Rekatywacja'
go


----------------------------------------------------------------------
----------------------------------------------------------------------

exec sp_update_version '1.0.14', '1.0.15';
go
