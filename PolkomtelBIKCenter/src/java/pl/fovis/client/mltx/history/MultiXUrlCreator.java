/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.mltx.history;

import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.Window;
import pl.fovis.common.mltx.MultixConstants;

/**
 *
 * @author LKI
 */
public class MultiXUrlCreator {

    public static String createUrl() {
        return createUrl(true);
    }

    public static String createUrl(boolean removeHash) {
        UrlBuilder ub = Window.Location.createUrlBuilder();
        if (removeHash) {
            ub.setHash(null);
        }
        return ub.removeParameter(LocaleInfo.getLocaleQueryParam()).removeParameter(MultixConstants.DB_NAME_PARAM).buildString();
    }
}
