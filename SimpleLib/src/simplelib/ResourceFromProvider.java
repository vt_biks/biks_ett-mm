/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class ResourceFromProvider<T> implements IResourceFromProvider<T> {

    private static final ILameLogger logger = LameLoggerFactory.getLogger(ResourceFromProvider.class);
    private IGenericResourceProvider<T> resProvider;
    private String name;

    public ResourceFromProvider(IGenericResourceProvider<T> resProvider, String name) {
        this.resProvider = resProvider;
        this.name = name;
    }

    public T gainResource() {
        if (logger.isStageEnabled()) {
            logger.stage("gainResource: name=\"" + name + "\", resProvider class="
                    + BaseUtils.safeGetClassName(resProvider));
        }

        return resProvider.gainResource(name);
    }

    public long getLastModifiedOfResource() {
        return resProvider.getLastModifiedOfResource(name);
    }
}
