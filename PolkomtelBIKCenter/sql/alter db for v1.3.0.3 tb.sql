exec sp_check_version '1.3.0.3';
go

insert into bik_app_prop (name,val,is_editable) values ('languages','pl,en',0);

exec sp_update_version '1.3.0.3', '1.3.0.4';
go