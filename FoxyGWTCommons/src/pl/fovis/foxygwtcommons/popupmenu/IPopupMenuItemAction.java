/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.popupmenu;

/**
 *
 * @author tflorczak
 */
public interface IPopupMenuItemAction<CTX> {

    public void setup(CTX contextObject);

    public boolean isEnabled();

    public String getCaption();

    public String getButtonCss();

    public void execute();
}
