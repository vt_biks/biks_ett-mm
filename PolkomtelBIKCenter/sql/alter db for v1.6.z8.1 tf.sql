﻿exec sp_check_version '1.6.z8.1';
go


if not exists (select * from bik_app_prop where name = 'createDisabledUsersInSSO')
begin
	insert into bik_app_prop(name, val, is_editable)
	values ('createDisabledUsersInSSO', 'false', 1)
end;
go

exec sp_update_version '1.6.z8.1', '1.6.z8.2';
go