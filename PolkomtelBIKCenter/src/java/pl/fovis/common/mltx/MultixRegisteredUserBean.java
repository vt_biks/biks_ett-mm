/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.mltx;

import java.io.Serializable;

/**
 *
 * @author LKI
 */
public class MultixRegisteredUserBean implements Serializable {

    public Integer id;
    public String firstName;
    public String lastName;
    public String email;
    public String phoneNum;
    public String company;
    public String jobTitle;
    public String country;
    public String city;
    public String additionalAttentions;
}
