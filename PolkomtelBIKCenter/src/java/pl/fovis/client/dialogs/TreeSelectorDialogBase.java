///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package pl.fovis.client.dialogs;
//
//import com.google.gwt.event.dom.client.BlurEvent;
//import com.google.gwt.event.dom.client.BlurHandler;
//import com.google.gwt.event.dom.client.ClickEvent;
//import com.google.gwt.event.dom.client.ClickHandler;
//import com.google.gwt.event.dom.client.FocusEvent;
//import com.google.gwt.event.dom.client.FocusHandler;
//import com.google.gwt.user.client.Window;
//import com.google.gwt.user.client.rpc.AsyncCallback;
//import com.google.gwt.user.client.ui.*;
//import java.util.Collection;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import pl.fovis.client.BIKClientSingletons;
//import pl.fovis.client.BOXIService.BikNodeTreeOptMode;
//import pl.fovis.client.BOXIServiceAsync;
//import pl.fovis.client.bikpages.BikTreeWidget;
//import pl.fovis.client.bikpages.IPageDataFetchBroker;
//import pl.fovis.client.bikpages.ITreeGridCellWidgetBuilder;
//import pl.fovis.client.bikpages.PageDataFetchBroker;
//import pl.fovis.client.bikpages.TreeDataFetchAsyncCallback;
//import pl.fovis.client.bikwidgets.BikItemButtonBig;
//import pl.fovis.client.treeandlist.TreeNodeBeanAsMapStorage;
//import pl.fovis.common.*;
//import pl.fovis.common.i18npoc.I18n;
//import pl.fovis.foxygwtcommons.StandardAsyncCallback;
//import pl.fovis.foxygwtcommons.GWTUtils;
//import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
//import pl.fovis.foxygwtcommons.popupmenu.PopupMenuDisplayer;
//import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeOrListGridRightClickHandler;
//import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;
//import pl.fovis.foxygwtcommons.wezyrtreegrid.InlineFlowPanel;
//import simplelib.BaseUtils;
//import simplelib.IContinuation;
//import simplelib.IParametrizedContinuation;
//import simplelib.Pair;
//
///**
// *
// * @author wezyr
// */
// Nie jest uzywane
//public abstract class TreeSelectorDialogBase extends BaseActionOrCancelDialog {
//
//    private static final String ADDING_ONE_JOINED_OBJ_FOR_MSG = I18n.dodajeszPowiazanieDla.get() /* I18N:  */ + ": ";
//    private static final String EDITING_MANY_JOINED_OBJS_FOR_MSG = I18n.edytujeszPowiazaniaDla.get() /* I18N:  */ + ": ";
//    private static final String EDITING_MANY_JOINED_OBJS_HINT_NAME = "EditnigJoinedObj";
//    protected boolean useLazyLoading = true;//false; //
//    protected IParametrizedContinuation<TreeNodeBean> saveCont;
//    protected BikTreeWidget bikTree;
//    protected TreeNodeBean selectedNode = null;
//    //ww:mltx 2013.09.26 - kasuję to kuriozum, WTF, po co to było?
//    //    protected TreeNodeBean lastSelectedNode = null;
//    protected TextBox filterBox;
//    protected CheckBox inheritToDescendantsCb;
//    protected int nodeId;
//    //protected String optNodeNamePathStr;
//    protected Label headLbl;// = new Label(ADDING_ONE_JOINED_OBJ_FOR_MSG + "...");
////    protected Set<Integer> alreadyJoinedObjIds;
////    protected Set<Integer> fixedJoinedObjIds;
//    protected IJoinedObjSelectionManager alreadyJoinedManager;
//    protected Map<Integer, JoinedObjMode> directOverride = new HashMap<Integer, JoinedObjMode>();
//    // Boolean == true -> all descendants, false -> children only
//    protected Map<Integer, Pair<JoinedObjMode, Boolean>> subNodesOverride = new HashMap<Integer, Pair<JoinedObjMode, Boolean>>();
//    protected Map<Integer, InlineHTML> htmlForJOMImg = new HashMap<Integer, InlineHTML>();
//    protected Map<Integer, InlineLabel> lblForJOM = new HashMap<Integer, InlineLabel>();
//    protected Set<Integer> ancestorIdsOfJoinedObjs = new HashSet<Integer>();
//    protected boolean isInFilterBox;
//    protected Set<Integer> alreadyJoinedObjIds;
//    protected IPageDataFetchBroker dataFetchBroker = new PageDataFetchBroker();
//
//    protected abstract boolean isInheritToDescendantsCheckBoxVisible();
//
//    protected abstract void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback);
//
//    protected abstract Collection<Integer> getTreeIdsForFiltering();
//
//    //protected abstract String getMessageForUnselectableNode(TreeNodeBean tnb);
//    //ww: powinna być klasa bazowa dialogów BIKSowych i tam to powinno siedzieć
//    // zauważmy, że BaseActionOrCancelDialog jest ogólną klasą bazową dialogów
//    // nie ograniczoną tylko do BIKSa, więc siedzi w innym pakiecie
//    // a wszystkie dialogi biksowe mają lub moga mieć coś wspólnego - bo są
//    // w BIKSie używane - np. właśnie ten service...
//    protected BOXIServiceAsync getService() {
//        return BIKClientSingletons.getService();
//    }
//
//    protected void fetchData() {
//        fetchData(null);
//    }
//
//    protected void processAndShowNodes(List<TreeNodeBean> result, Integer optNodeIdToSelect) {
//        //   resultRepo = result;
//        //bikTree.isGetDataInProgress = false;
//        //bikTree.buildUniTree(result);
//        List<TreeNodeBean> rootDataNodes = //bikTree.utb.getRootDataNodes();
//                BIKCenterUtils.getRootNodes(result);
//
//        Set<Pair<Integer, TreeNodeBean>> newRootNodeTreeIds = new HashSet<Pair<Integer, TreeNodeBean>>();
//        for (TreeNodeBean tnb : rootDataNodes) {
//            newRootNodeTreeIds.add(new Pair<Integer, TreeNodeBean>(tnb.treeId, tnb));
//        }
//        Map<Integer, TreeNodeBean> newRootNodes = new HashMap<Integer, TreeNodeBean>();
//        Map<Integer, TreeNodeBean> parentNewRootNodes = new HashMap<Integer, TreeNodeBean>();
//        for (Pair<Integer, TreeNodeBean> p : newRootNodeTreeIds) {
//            if (newRootNodes.containsKey(p.v1)) {
//                continue;
//            }
//            // Parent Tree category
//            if (p.v2.parentTreeId != null && !parentNewRootNodes.containsKey(p.v2.parentNodeId)) {
//                TreeNodeBean parentNewRoot = new TreeNodeBean();
//                parentNewRoot.id = -p.v2.parentTreeId;
//                parentNewRoot.name = p.v2.parentTreeName;
//                parentNewRoot.nodeKindCode = I18n.kategoria.get() /* I18N:  *//*"Category"*/;
//                parentNewRoot.nodeKindCaption = I18n.kategoria.get() /* I18N:  */;
//                parentNewRoot.nodeKindId = -1;
//                parentNewRoot.parentNodeId = null;
//                parentNewRoot.branchIds = -p.v2.parentTreeId + "|";
//                parentNewRoot.treeId = -1;
//                parentNewRoot.treeCode = I18n.kategoria.get();
//                parentNewRoot.treeName = I18n.kategoria.get();
//                parentNewRootNodes.put(p.v2.parentTreeId, parentNewRoot);
//
//                if (alreadyJoinedManager != null && ancestorIdsOfJoinedObjs.contains(p.v2.id)) {
//                    ancestorIdsOfJoinedObjs.add(parentNewRoot.id);
//                }
//            }
//            // Tree category
//            TreeNodeBean newRoot = new TreeNodeBean();
//            newRoot.id = -p.v2.id; //newRootId--;
//            newRoot.name = p.v2.treeName;
//            //mm-> poprawka na szybko, żeby było po polsku - do poprawienia, jak będzie koncepcja
//            newRoot.nodeKindCode = I18n.kategoria.get() /* I18N:  *//*"Category"*/;
//            newRoot.nodeKindCaption = I18n.kategoria.get() /* I18N:  */;
//            newRoot.nodeKindId = -1;
//            newRoot.parentNodeId = p.v2.parentTreeId == null ? null : -p.v2.parentTreeId;
//            newRoot.branchIds = p.v2.parentTreeId == null ? (-p.v2.treeId) + "|" : ((-p.v2.parentTreeId) + "|" + (-p.v2.id) + "|");
//            newRoot.treeId = -1;
//            newRoot.treeCode = p.v2.treeCode;
//            newRoot.treeName = p.v2.treeName;
//            newRootNodes.put(p.v1, newRoot);
////            result.add(newRoot);
//
//            if (alreadyJoinedManager != null && ancestorIdsOfJoinedObjs.contains(p.v2.id)) {
//                ancestorIdsOfJoinedObjs.add(newRoot.id);
//            }
//        }
//        if (parentNewRootNodes.size() > 1) {
//            for (Map.Entry e : parentNewRootNodes.entrySet()) {
//                result.add((TreeNodeBean) e.getValue());
//            }
//        }
//        if (newRootNodes.size() > 1) {
//            for (TreeNodeBean tnb : rootDataNodes) {
//                tnb.parentNodeId = newRootNodes.get(tnb.treeId).id;
//            }
//            for (Map.Entry e : newRootNodes.entrySet()) {
//                result.add((TreeNodeBean) e.getValue());
//            }
//            for (TreeNodeBean tnb : result) {
//                if (tnb.treeId > 0) { // normalne obiekty, bez sztucznych
//                    tnb.branchIds = (tnb.parentTreeId == null ? (-tnb.treeId) + "|" : (-tnb.parentTreeId) + "|" + ((-tnb.treeId) + "|")) + tnb.branchIds;
//                }
////                tnb.branchIds = -tnb.treeId + "|" + (tnb.branchIds == null ? "" : tnb.branchIds);
//            }
//        }
//        bikTree.initWithFullData(result, useLazyLoading, getTreeOptExtraMode());
//        bikTree.selectEntityById(optNodeIdToSelect);
//    }
//
//    protected BikNodeTreeOptMode getTreeOptExtraMode() {
//        return null;
//    }
//
//    protected boolean calcHasNoChildren() {
//        return true;
//    }
//
//    protected void fetchData(final Integer optNodeIdToSelect) {
//        final AsyncCallback<List<TreeNodeBean>> callback = new TreeDataFetchAsyncCallback<List<TreeNodeBean>>("Communication failed" /* I18N: no */,
//                dataFetchBroker) {
//            protected void doOnSuccess(List<TreeNodeBean> result) {
//                processAndShowNodes(result, optNodeIdToSelect);
//            }
//        };
//        //bikTree.isGetDataInProgress = true;
//        callServiceToGetData(callback);
//    }
//
//    @Override
//    protected String getDialogCaption() {
//        return I18n.dodajPowiazanie.get() /* I18N:  */;
//    }
//
//    @Override
//    protected String getActionButtonCaption() {
//        return isInMultiSelectMode() ? I18n.zapisz.get() /* I18N:  */ : I18n.wybierz.get() /* I18N:  */;
//    }
//
//    protected boolean mustHideLinkedNodes() {
//        return true;
//    }
//
//    protected String getHeadLblPrefixMsg() {
//        return isInMultiSelectMode() ? EDITING_MANY_JOINED_OBJS_FOR_MSG : ADDING_ONE_JOINED_OBJ_FOR_MSG;
//    }
//
//    protected Widget setOptionalWigetAfterHeadLabel() {
//        return null;
//    }
//
//    @Override
//    protected void buildMainWidgets() {
//        //if (optNodeNamePathStr != null) {
//        headLbl = new Label(getHeadLblPrefixMsg() + "...");
//        Widget headLblWidget;
//
//        if (isInMultiSelectMode() && BIKClientSingletons.helpHasHint(EDITING_MANY_JOINED_OBJS_HINT_NAME)) {
//            HorizontalPanel headHp = new HorizontalPanel();
//            HorizontalPanel headHpLblOpt = new HorizontalPanel();
//            headHp.setWidth("100%");
//            headHpLblOpt.add(headLbl);
//            headHp.add(headHpLblOpt);
//            Widget optionalWidget = setOptionalWigetAfterHeadLabel();
//            if (optionalWidget != null) {
//                headHpLblOpt.add(optionalWidget);
//                GWTUtils.setStyleAttribute(headLbl, "padding" /* I18N: no */, "0px 10px 0px 0");
//            }
//
//            //headHp.setCellWidth(hintBtn, ZINDEX_OVER_GLASS)
//            Widget hintBtn = BIKClientSingletons.createHelpHintButton(EDITING_MANY_JOINED_OBJS_HINT_NAME, null);
//            headHp.add(hintBtn);
//            headHp.setCellHorizontalAlignment(hintBtn, HasHorizontalAlignment.ALIGN_RIGHT);
//            //headHp.setCellWidth(hintBtn, ZINDEX_OVER_GLASS)
//            headLblWidget = headHp;
//        } else {
//            headLblWidget = headLbl;
//        }
//
//        main.add(headLblWidget); //main.add(headLbl);
//        headLblWidget.setStyleName("AttrHeadLbl treeSelectorHeadLbl");
//        //}
//
//        Widget optRightSideWidget = buildOptionalRightSide();
//
//        Panel mainBodyContainer;
//
//        if (optRightSideWidget != null) {
//            VerticalPanel leftVp = new VerticalPanel();
//            leftVp.setStyleName("EntityDetailsPane");
//
//            //leftVp.setWidth("600px");//main.setWidth("600px");
//            //leftVp.setHeight("300px");//main.setHeight("300px");
//
//            HorizontalPanel hp = new HorizontalPanel();
//            hp.add(leftVp);
//            hp.add(optRightSideWidget);
//            main.add(hp);
//            mainBodyContainer = leftVp;
//        } else {
//            //main.setStyleName("EntityDetailsPane");
//            mainBodyContainer = main;
//        }
//
//        mainBodyContainer.setWidth("600px");
//        setHeadLabel();
//
//        buildTreeMainWidgets(mainBodyContainer);
//
//        fetchData();
//    }
//
//    protected void setHeadLabel() {
//        if (headLbl == null) {
//            return;
//        }
//
//        getService().getAncestorNodePathByNodeId(nodeId, new StandardAsyncCallback<List<String>>() {
//            public void onSuccess(List<String> result) {
//                //headLbl.setText(getHeadLblPrefixMsg() + BaseUtils.mergeWithSepEx(result, " » "));
//                headLbl.setText(getHeadLblPrefixMsg() + BaseUtils.mergeWithSepEx(result, BIKConstants.RAQUO_STR_SPACED));
//                if (dialog.isVisible()) {
//                    dialog.center();
//                }
//            }
//        });
//    }
//
//    protected void doFilter() {
//        //jezeli nic nie zostalo wpisane to wroc do pierwotnego wygladu
//        if (filterBox.getText().equals("")) {
//            fetchData();
//        } else {
//            bikTree.filterTreeEx(filterBox.getText(), useLazyLoading,
//                    getTreeIdsForFiltering(), getTreeOptExtraMode());
//        }
//    }
//
//    protected void doAfterDoubleClickOnTree(Map<String, Object> param) {
//        setSelectedNodeFromBikTree(param);
//        actionClicked();
//    }
//
//    protected Widget buildOptionalRightSide() {
//        return null;
//    }
//
//    protected void resetJoinedObjModeForSubTree(int nodeId) {
//        Map<String, Object> row = bikTree.getTreeStorage().getRowById(nodeId);
//
//        Collection<Map<String, Object>> childRows = bikTree.getTreeStorage().getChildRows(row);
//
//        if (childRows == null) {
//            return;
//        }
//
//        for (Map<String, Object> childRow : childRows) {
//            int childId = (Integer) childRow.get(TreeNodeBeanAsMapStorage.ID_FIELD_NAME);
//            subNodesOverride.remove(childId);
//            directOverride.remove(childId);
//
//            TreeNodeBean childNode = bikTree.getTreeStorage().getBeanById(childId);
//
//            InlineHTML imgHtml = htmlForJOMImg.get(childId);
//            if (imgHtml != null) {
//                JoinedObjMode selectedVal = getNodeSelectedValue(childNode);
//                final boolean isFixed = isNodeSelectionFixed(childNode);
//                imgHtml.setHTML(generateHtmlForJoinedObjImg(selectedVal, isFixed));
//            }
//
////            InlineLabel lbl = lblForJOM.get(childId);
////            setJOMLabelStyles(childNode, lbl);
//
//            updateLabelStylesOfNode(childNode);
//
//            resetJoinedObjModeForSubTree(childId);
//        }
//    }
//
//    protected void updateLabelStylesOfNode(TreeNodeBean node) {
//        InlineLabel lbl = lblForJOM.get(node.id);
//        setJOMLabelStyles(node, lbl);
//    }
//
//    protected void updateLabelStylesOfNodeById(int id) {
//        updateLabelStylesOfNode(bikTree.getTreeStorage().getBeanById(id));
//    }
//
//    protected void setJoinedObjModeForDescendants(TreeNodeBean node, JoinedObjMode mode, boolean allDescendants) {
//        if (node.id < 0) {
//            Map<String, Object> row = bikTree.getTreeStorage().getRowById(node.id);
//            Collection<Map<String, Object>> childRows = bikTree.getTreeStorage().getChildRows(row);
//
//            for (Map<String, Object> childRow : childRows) {
//                int childId = (Integer) childRow.get(TreeNodeBeanAsMapStorage.ID_FIELD_NAME);
//                if (allDescendants) {
//                    subNodesOverride.put(childId, new Pair<JoinedObjMode, Boolean>(mode, allDescendants));
//                }
//                directOverride.put(childId, mode);
//                resetJoinedObjModeForSubTree(childId);
//            }
//        } else {
//            subNodesOverride.put(node.id, new Pair<JoinedObjMode, Boolean>(mode, allDescendants));
//            resetJoinedObjModeForSubTree(node.id);
//        }
//    }
//
//    protected void showPopupMenu(Map<String, Object> dataRow, int clientX, int clientY) {
//        ITreeRowAndBeanStorage<Integer, Map<String, Object>, TreeNodeBean> storage = bikTree.getTreeStorage();
//        final TreeNodeBean node = storage.getBeanById(storage.getTreeBroker().getNodeId(dataRow));
//        //(Integer) dataRow.get(TreeNodeBeanAsMapStorage.ID_FIELD_NAME));
//
//        JoinedObjMode directJOM = directOverride.get(node.id);
//
//        if (node.hasNoChildren && directJOM == null) {
//            return;
//        }
//
//        PopupMenuDisplayer popupMenu = new PopupMenuDisplayer();
//
//        if (!node.hasNoChildren) {
//            popupMenu.addMenuItem(I18n.dodajDziedzicPowiazanDlaObiektow.get() /* I18N:  */, new IContinuation() {
//                public void doIt() {
//                    setJoinedObjModeForDescendants(node, JoinedObjMode.WithInheritance, false);
//                }
//            });
//            popupMenu.addMenuItem(I18n.dodajNiedziedPowiazanDlaObiektow.get() /* I18N:  */, new IContinuation() {
//                public void doIt() {
//                    setJoinedObjModeForDescendants(node, JoinedObjMode.Single, false);
//                }
//            });
////            popupMenu.addMenuSeparator();
////            popupMenu.addMenuItem("Dodaj dziedziczone powiązanie dla wszystkich obiektów podrzędnych", new IContinuation() {
////
////                public void doIt() {
////                    setJoinedObjModeForDescendants(node, JoinedObjMode.WithInheritance, true);
////                }
////            });
////            popupMenu.addMenuItem("Dodaj niedziedziczone powiązanie dla wszystkich obiektów podrzędnych", new IContinuation() {
////
////                public void doIt() {
////                    setJoinedObjModeForDescendants(node, JoinedObjMode.Single, true);
////                }
////            });
//            popupMenu.addMenuSeparator();
//            popupMenu.addMenuItem(I18n.usunPowiazanDlaObiektowBezposreP.get() /* I18N:  */, new IContinuation() {
//                public void doIt() {
//                    setJoinedObjModeForDescendants(node, JoinedObjMode.NotJoined, false);
//                }
//            });
//            popupMenu.addMenuItem(I18n.usunPowiazanDlaWszystkiObiektowP.get() /* I18N:  */, new IContinuation() {
//                public void doIt() {
//                    setJoinedObjModeForDescendants(node, JoinedObjMode.NotJoined, true);
//                }
//            });
//            popupMenu.addMenuSeparator();
//            popupMenu.addMenuItem(I18n.przywrocOryginalStanPowiazanWPod.get() /* I18N:  */, new IContinuation() {
//                public void doIt() {
//                    setJoinedObjModeForDescendants(node, null, true);
//                }
//            });
//        }
//        JoinedObjMode currVal = getNodeSelectedValue(node);
//        JoinedObjMode origVal = getNodeSelectedValueEx(node, true);
//        if (currVal != origVal) {
//            boolean isFixed = isNodeSelectionFixed(node);
//            if (!isFixed) {
//                popupMenu.addMenuItem(I18n.przywrocOryginalStanPowiazanWTym.get() /* I18N:  */, new IContinuation() {
//                    public void doIt() {
//                        directOverride.remove(node.id);
//
//                        JoinedObjMode currVal = getNodeSelectedValue(node);
//                        JoinedObjMode origVal = getNodeSelectedValueEx(node, true);
//
//                        if (currVal != origVal) {
//                            directOverride.put(node.id, origVal);
//                        }
//                        InlineHTML imgHtml = htmlForJOMImg.get(node.id);
//                        imgHtml.setHTML(generateHtmlForJoinedObjImg(origVal, false));
//                        updateLabelStylesOfNode(node);
//                    }
//                });
//            }
//        }
//
//        popupMenu.showAtPosition(clientX, clientY);
//    }
//
//    public boolean isInheritToDescendantsChecked() {
//        return inheritToDescendantsCb == null ? false : inheritToDescendantsCb.getValue();
//    }
//
//    protected void setSelectedNodeFromBikTree(Map<String, Object> param) {
//        selectedNode = //(TreeNodeBean) param.get(BaseBIKTreeWidget.TREE_NODE_BEAN_KEY_IN_ROW);
//                bikTree.getNodeBeanByDataNode(param);
////        if (lastSelectedNode == null) {
////            lastSelectedNode = selectedNode;
////        }
//        configureButtons();
////        actionBtn.setEnabled(selectedNode.id >= 0 && selectedNode.id != nodeId);
//    }
//
//    protected void configureButtons() {
//        actionBtn.setEnabled(isActionButtonEnabled());
//    }
//
//    protected boolean isActionButtonEnabled() {
//        //ww:mltx dodałem spr. selectedNode != null
//        return selectedNode != null && selectedNode.id >= 0 && selectedNode.id != nodeId;
//    }
//
//    protected void bikTreeNodeChanged(Map<String, Object> param) {
//        setSelectedNodeFromBikTree(param);
//    }
//
//    @Override
//    protected boolean doActionBeforeClose() {
//        if (isInMultiSelectMode()) {
//            return true;
//        }
//
//        if (selectedNode.id >= 0) {
//            if (selectedNode.id == nodeId) {
//                Window.alert(I18n.nieMoznaWybracSamegoSiebie.get() /* I18N:  */ + ".");
//                return false;
//            }
//            String messageForUnselectable = getMessageForUnselectableNode(selectedNode);
//
//            boolean canSelect = messageForUnselectable == null;
//            if (canSelect) {
//                return true;
//            } else {
//                Window.alert(messageForUnselectable);
//                return false;
//            }
//        } else {
//            Window.alert(I18n.wybierzZKategoriElementPodrzedn.get() /* I18N:  */ + ".");
//            return false;
//        }
//    }
//
//    @Override
//    protected void doAction() {
//        if (isInMultiSelectMode()) {
//            BIKClientSingletons.showInfo(I18n.zapisywaniePowiazanProszeCzekac.get() /* I18N:  */ + "...");
//            getService().updateJoinedObjsForNode(nodeId, mustHideLinkedNodes(),
//                    directOverride, subNodesOverride, new StandardAsyncCallback<Void>() {
//                public void onSuccess(Void result) {
//                    BIKClientSingletons.showInfo(I18n.powiazaniaZapisaneOdswiezanie.get() /* I18N:  */);
//                    alreadyJoinedManager.refreshJoinedNodes(nodeId);
//                }
//            });
//        } else {
//            saveCont.doIt(selectedNode);
//        }
//    }
//
//    protected void buildAndShowDialog(int nodeId, IParametrizedContinuation<TreeNodeBean> saveCont,
//            IJoinedObjSelectionManager alreadyJoinedManager) {
////        this.optNodeNamePathStr = optNodeNamePath == null ? null
////                : BaseUtils.mergeWithSepEx(optNodeNamePath, " » ");
//        this.saveCont = saveCont;
//        this.nodeId = nodeId;
////        this.alreadyJoinedObjIds = alreadyJoinedObjIds;
////        this.fixedJoinedObjIds = fixedJoinedObjIds;
//        this.alreadyJoinedManager = alreadyJoinedManager;
//
//        buildAlreadyJoinedObjIds();
////        if (alreadyJoinedManager != null) {
////            alreadyJoinedObjIds = new HashSet<Integer>(alreadyJoinedManager.getJoinedObjIds());
////
////            for (Integer id : alreadyJoinedObjIds) {
////                JoinedObjBasicInfoBean jobi = alreadyJoinedManager.getInfoForJoinedNodeById(id);
////                BIKCenterUtils.splitBranchIdsIntoColl(jobi.branchIds, true, ancestorIdsOfJoinedObjs);
////            }
////        }
//
//        super.buildAndShowDialog();
//    }
//
//    protected void buildAndShowDialog(int nodeId, IParametrizedContinuation<TreeNodeBean> saveCont,
//            IJoinedObjSelectionManager alreadyJoinedManager, boolean useLazyLoading) {
//        this.useLazyLoading = useLazyLoading;
//        buildAndShowDialog(nodeId, saveCont, alreadyJoinedManager);
//    }
//
//    protected void buildAlreadyJoinedObjIds() {
//        if (alreadyJoinedManager != null) {
//            alreadyJoinedObjIds = new HashSet<Integer>(alreadyJoinedManager.getJoinedObjIds());
//
//            for (Integer id : alreadyJoinedObjIds) {
//                JoinedObjBasicInfoBean jobi = alreadyJoinedManager.getInfoForJoinedNodeById(id);
//                BIKCenterUtils.splitBranchIdsIntoColl(jobi.branchIds, true, ancestorIdsOfJoinedObjs);
//            }
//        }
//    }
//
//    protected boolean isInMultiSelectMode() {
//        return alreadyJoinedManager != null && isInheritToDescendantsCheckBoxVisible();
//    }
//
//    protected boolean isNodeSelectable(TreeNodeBean node) {
//        return node.id >= 0 /*
//                 * && node.id != nodeId
//                 */;
//    }
//
//    // może zwrócić NULL
//    protected JoinedObjBasicInfoBean getJoinedObjBasicInfo(int dstNodeId) {
//        if (alreadyJoinedManager == null || !alreadyJoinedObjIds.contains(dstNodeId)) {
//            return null;
//        }
//        return alreadyJoinedManager.getInfoForJoinedNodeById(dstNodeId);
//    }
//
//    protected boolean isJobiFixed(JoinedObjBasicInfoBean jobi) {
//        return jobi != null && (jobi.type || jobi.srcId != nodeId);
//    }
//
//    protected boolean isNodeSelectionFixed(TreeNodeBean node) {
//        if (node.id == nodeId) {
//            return true;
//        }
//
//        JoinedObjBasicInfoBean jobi = getJoinedObjBasicInfo(node.id);
//        return isJobiFixed(jobi);
//    }
//
//    protected JoinedObjMode getNodeSelectedValueEx(TreeNodeBean node, boolean noOverride) {
//        JoinedObjBasicInfoBean jobi = getJoinedObjBasicInfo(node.id);
//
//        if (!isJobiFixed(jobi) && !noOverride) {
//            JoinedObjMode jobiOverride = directOverride.get(node.id);
//            if (jobiOverride != null) {
//                return jobiOverride;
//            }
//
//            List<Integer> ancestorNodeIds = BIKCenterUtils.splitBranchIdsEx(node.branchIds, true);
//            JoinedObjMode fromAncestor = null;
//            boolean isParent = true;
//            for (int i = ancestorNodeIds.size() - 1; i >= 0; i--) {
//                int ancestorNodeId = ancestorNodeIds.get(i);
//                Pair<JoinedObjMode, Boolean> ancestorOverride = subNodesOverride.get(ancestorNodeId);
//                if (ancestorOverride != null && (ancestorOverride.v2 || isParent)) {
//                    fromAncestor = ancestorOverride.v1;
//                    break;
//                }
//                isParent = false;
//            }
//            if (fromAncestor != null) {
//                return fromAncestor;
//            }
//        }
//
//        if (jobi == null) {
//            return JoinedObjMode.NotJoined;
//        }
//
//        if (jobi.inheritToDescendants || jobi.srcId != nodeId) {
//            return JoinedObjMode.WithInheritance;
//        }
//
//        return JoinedObjMode.Single;
//    }
//
//    protected JoinedObjMode getNodeSelectedValue(TreeNodeBean node) {
//        return getNodeSelectedValueEx(node, false);
//    }
//
//    protected String generateHtmlForJoinedObjImg(JoinedObjMode selectedVal, boolean isFixed) {
//        return "<" + "img src='images" /* I18N: no */ + "/joined_"
//                + selectedVal.imgNamePart + (isFixed ? "_fixed" : "")
//                + "." + "png' style=\"width: 16px; height: 16px;\" border=\"0\" class" /* I18N: no */ + "=\"treeIcon\"/>";
//    }
//
//    protected boolean hasChangeInDescendants(TreeNodeBean node) {
//        return false;
//    }
//
//    protected boolean hasChangeInNodeById(TreeNodeBean node) {
//        JoinedObjMode currVal = getNodeSelectedValue(node);
//        JoinedObjMode origVal = getNodeSelectedValueEx(node, true);
//        return currVal != origVal;
//    }
//
//    protected boolean isNodeOriginallyJoined(TreeNodeBean node) {
//        return getJoinedObjBasicInfo(node.id) != null;
//        //alreadyJoinedManager != null && alreadyJoinedManager.getInfoForJoinedNodeById(node.id) != null;
//    }
//
//    protected boolean hasJoinedNodesInDescendantsById(TreeNodeBean node) {
//        return alreadyJoinedManager != null && ancestorIdsOfJoinedObjs.contains(node.id);
//    }
//
//    protected void setJOMLabelStyles(TreeNodeBean node, InlineLabel lbl) {
//        lbl.setStyleName("");
//        if (hasChangeInNodeById(node)) {
//            lbl.addStyleName("tree-selector-hasChangeInNode");
//        }
//        if (isNodeOriginallyJoined(node)) {
//            lbl.addStyleName("tree-selector-isNodeOriginallyJoined");
//        }
//        if (hasJoinedNodesInDescendantsById(node)) {
//            lbl.addStyleName("tree-selector-hasJoinedNodesInDescendants");
//        }
//        if (hasChangeInDescendants(node)) {
//            lbl.addStyleName("tree-selector-hasChangeInDescendants");
//        }
//    }
//
//    protected Widget createWidgetForNameColumn(final TreeNodeBean node) {
//        InlineLabel lbl = new InlineLabel(node.name);
//        setJOMLabelStyles(node, lbl);
//
//        lblForJOM.put(node.id, lbl);
//
//        if (!isNodeSelectable(node)) {
//            return lbl;
//        }
//
//        JoinedObjMode selectedVal = getNodeSelectedValue(node);
//        final boolean isFixed = isNodeSelectionFixed(node);
//        InlineFlowPanel ifp = new InlineFlowPanel();
//        final InlineHTML imgHtml = new InlineHTML(generateHtmlForJoinedObjImg(selectedVal, isFixed));
//
//        if (!isFixed) {
//            htmlForJOMImg.put(node.id, imgHtml);
//
//            imgHtml.addClickHandler(new ClickHandler() {
//                public void onClick(ClickEvent event) {
//                    JoinedObjMode nextVal = getNodeSelectedValue(node).getNextVal();
//                    //JoinedObjMode origVal = getNodeSelectedValueEx(node, true);
//
////                    if (nextVal == origVal) {
////                        directOverride.remove(node.id);
////                        //subNodesOverride.remove(node.id);
////                    } else {
//                    directOverride.put(node.id, nextVal);
//                    //subNodesOverride.put(node.id, new Pair<JoinedObjMode, Boolean>(nextVal, false));
////                    }
//                    imgHtml.setHTML(generateHtmlForJoinedObjImg(nextVal, isFixed));
//                    updateLabelStylesOfNode(node);
//                }
//            });
//        }
//
//        ifp.add(imgHtml);
//        ifp.add(lbl);
//        return ifp;
//    }
//
//    protected String getMessageForUnselectableNode(TreeNodeBean tnb) {
//        if (tnb.id == nodeId) {
//            return I18n.nieMoznaPowiazacWezlaZNimSamym.get() /* I18N:  */;
//        }
//        return null;
//    }
//
//    //ww: nie rób focusa wcale
//    @Override
//    protected void tryFocusFirstWidget() {
//        // no-op
//    }
//
//    @Override
//    protected void onEnterPressed() {
//        if (isInFilterBox) {
//            doFilter();
//        } else {
//            super.onEnterPressed();
//        }
//    }
//
//    protected void treeHasNoData() {
//        setSelectedNodeFromBikTree(null);
//    }
//
//    protected void buildTreeMainWidgets(Panel mainBodyContainer) {
//
//        bikTree = getTreeWidget();
//
//        if (isInMultiSelectMode()) {
//            bikTree.setCellWidgetBuilder(new ITreeGridCellWidgetBuilder<TreeNodeBean>() {
//                public Widget getOptWidget(TreeNodeBean row, String colName) {
//                    if (!BaseUtils.safeEquals(colName, TreeNodeBeanAsMapStorage.NAME_FIELD_NAME)) {
//                        return null;
//                    }
//                    return createWidgetForNameColumn(row);
//                }
//            });
//
//            bikTree.addRightClickHandler(new IFoxyTreeOrListGridRightClickHandler<Map<String, Object>>() {
//                public void doIt(Map<String, Object> dataRow, int clientX, int clientY, boolean isRowSelected) {
//                    showPopupMenu(dataRow, clientX, clientY);
//                }
//            });
//        }
//
//        Widget treeWidget = bikTree.getTreeGridWidget();
//        //treeWidget.setSize("100%", "100%");
//        treeWidget.setSize("600px", "100%");
//        mainBodyContainer.add(treeWidget);
//
//        HorizontalPanel hPanel = new HorizontalPanel();
//        hPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
//        filterBox = new TextBox();
//        filterBox.setStyleName("filterBox");
//        filterBox.setWidth("164px");
//
//        filterBox.addFocusHandler(new FocusHandler() {
//            public void onFocus(FocusEvent event) {
//                isInFilterBox = true;
//            }
//        });
//
//        filterBox.addBlurHandler(new BlurHandler() {
//            public void onBlur(BlurEvent event) {
//                isInFilterBox = false;
//            }
//        });
//
//        hPanel.add(filterBox);
//        hPanel.setCellWidth(filterBox, "180px");
//        hPanel.setStyleName("treeBikFilterBox");
//        hPanel.setWidth("100%");
//
//        IContinuation filterCont = new IContinuation() {
//            public void doIt() {
//                doFilter();
//            }
//        };
//        BikItemButtonBig filterButton = new BikItemButtonBig(I18n.fILTRUJ.get() /* I18N:  */, filterCont);
//
//        hPanel.add(filterButton);
//        hPanel.setCellHorizontalAlignment(filterButton, HasHorizontalAlignment.ALIGN_LEFT);
//        mainBodyContainer.add(hPanel);
//
//        if (!isInMultiSelectMode()) {
//            bikTree.addSelectionChangedHandler(new IParametrizedContinuation<Map<String, Object>>() {
//                public void doIt(Map<String, Object> param) {
//                    bikTreeNodeChanged(param);
//                }
//            });
//
//            bikTree.addDoubleClickHandler(new IParametrizedContinuation<Map<String, Object>>() {
//                public void doIt(Map<String, Object> param) {
//                    doAfterDoubleClickOnTree(param);
//                }
//            });
//        }
//        fetchData();
//    }
//
//    protected void optFixBeanPropsAfterLoad(TreeNodeBean tnb) {
//        //no-op
//    }
//
//    protected BikTreeWidget getTreeWidget() {
//        return new BikTreeWidget(mustHideLinkedNodes(),
//                dataFetchBroker,
//                calcHasNoChildren(),
//                TreeNodeBeanAsMapStorage.NAME_FIELD_NAME + "=300:Nazwa",
//                TreeNodeBeanAsMapStorage.PRETTY_KIND_FIELD_NAME + "=100:Typ") {
//            @Override
//            protected void optFixBeanPropsAfterLoad(TreeNodeBean tnb) {
//                TreeSelectorDialogBase.this.optFixBeanPropsAfterLoad(tnb);
//            }
//
//            @Override
//            protected void afterInitWithNoData() {
//                super.afterInitWithNoData();
//                treeHasNoData();
//            }
//        };
//    }
//}

