/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import pl.fovis.client.BIKClientSingletons;
import static pl.fovis.client.BIKClientSingletons.createDeleteButton;
import static pl.fovis.client.BIKClientSingletons.createEditChildAttributeButton;
import pl.fovis.client.bikpages.IBikTreePage;
import pl.fovis.client.bikwidgets.BikIcon;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog.AttributeType;
import pl.fovis.client.dialogs.EditEventAttributeRow;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.ChildrenBean;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.SimpleDateUtils;

/**
 *
 * @author tflorczak
 */
public class ChildrenWithAttributesWidget {

    protected List<ChildrenBean> children;
    protected Collection<String> attrDicList;
    protected Map<String, Boolean> attrDicListIsOrNotStatus = new HashMap<String, Boolean>();
    protected boolean showDescription;
    protected NodeKindBean leafNodeKind;
    protected IBikTreePage bikPage;
    protected Integer nodeId;
    protected boolean widgetActive;
    protected TreeNodeBean node;
    protected Integer nodeKindId;

    public ChildrenWithAttributesWidget(IBikTreePage bikPage, List<ChildrenBean> children, Collection<String> attrDicList) {
        this(bikPage, children, attrDicList, false, null);
    }

    public ChildrenWithAttributesWidget(IBikTreePage bikPage, List<ChildrenBean> children, Collection<String> attrDicList, boolean showDescription, NodeKindBean leafNodeKind) {
        this(bikPage, children, attrDicList, showDescription, leafNodeKind, null);
    }

    public ChildrenWithAttributesWidget(IBikTreePage bikPage, List<ChildrenBean> children, Collection<String> attrDicList, boolean showDescription, NodeKindBean leafNodeKind, TreeNodeBean node) {
        this.bikPage = bikPage;
        this.children = BaseUtils.isCollectionEmpty(children) ? new ArrayList<ChildrenBean>() : children;
        this.attrDicList = BaseUtils.isCollectionEmpty(attrDicList) ? new HashSet<String>() : attrDicList;
        this.showDescription = showDescription;
        this.leafNodeKind = leafNodeKind;
        this.node = node;
    }

    public Widget buildWidget() {
        VerticalPanel vp = new VerticalPanel();
        vp.add(EntityDetailsWidgetFlatBase.createPostSpacing());
        vp.setStyleName("childrenTable");
        FlexTable main = new FlexTable();
        main.setStyleName("gridJoinedObj");
        //
        checkIfDescriptionIsNeeded();
        checkShouldShowSomeHeader();
        //
        int numberOfAttributes = attrDicList.size();
//        if (!BaseUtils.isCollectionEmpty(children)) {
//            numberOfAttributes++;
//        }
        List<String> columns = new ArrayList<String>();
        int tmpIdx = 0;
        if (!BaseUtils.isCollectionEmpty(children)) {
            columns.add(I18n.nazwaObiektu.get());
        }
        List<Integer> widths = new ArrayList<Integer>();
        int colIdx = 0;
        boolean hasNoAttributes = BaseUtils.isCollectionEmpty(attrDicList);
        int attributesWidths = BaseUtils.isCollectionEmpty(attrDicList) ? 0 : 60;
//        widths[colIdx++] = 1; // ikona
        widths.add(1);
        if (showDescription) {
            columns.add(I18n.opis.get());
            if (hasNoAttributes) {
                widths.add(47); // nazwa
                widths.add(45); // description
            } else {
                widths.add(24); // nazwa
                widths.add(20); // description
                attributesWidths = 48;
            }
        } else {
            widths.add(32 + (hasNoAttributes ? 60 : 0)); // nazwa
        }

        int cntStatus = 0;
        for (Entry<String, Boolean> attrD : attrDicListIsOrNotStatus.entrySet()) {
            if (attrD.getValue()) {
                cntStatus++;
            }
        }
        for (Entry<String, Boolean> attrD : attrDicListIsOrNotStatus.entrySet()) {
            final String attr = attrD.getKey();
//        for (String attr : attrDicList) {
            columns.add(attr.indexOf('.') < 0 ? attr : attr.split("\\.")[1]);
//            columns.add("Data zmiany?");
            widths.add(attributesWidths / (attrDicList.size() + cntStatus));

            if (attrD.getValue()) {
                columns.add(I18n.dataStatusu.get());
                widths.add(attributesWidths / (attrDicList.size() + cntStatus));
            }
        }
        if (!BaseUtils.isCollectionEmpty(children) && node != null) {
            columns.add(" ");
            columns.add(" ");
            columns.add(" ");
        }
        widths.add(2); // strzlki do przejscia
        widths.add(2); // strzlki do przejscia
        widths.add(2); // strzlki do przejscia
        EntityDetailsWidgetFlatBase.createHeaders(main, columns.toArray(new String[columns.size()]));
        EntityDetailsWidgetFlatBase.setColumnWidth(main, widths.toArray(new Integer[widths.size()]));
        int indexCount = 1;
        final Map<Integer, Map<String, String>> nodeKidIdnameAndTypeAttr = new HashMap<Integer, Map<String, String>>();
        final Map<Integer, List<String>> nodeKidIdnameAttr = new HashMap<Integer, List<String>>();
        for (final ChildrenBean child : children) {
            Map<String, String> nameAndTypeAttr = nodeKidIdnameAndTypeAttr.get(child.nodeKindId);
            List<String> attrNames = nodeKidIdnameAttr.get(child.nodeKindId);
            if (nameAndTypeAttr == null) {
                nameAndTypeAttr = new HashMap<String, String>();
            }
            if (attrNames == null) {
                attrNames = new ArrayList<String>();
            }
            List<AttributeBean> attrs = child.attributes;
            if (attrs != null && !attrs.isEmpty()) {
                for (AttributeBean ab : attrs) {
                    nameAndTypeAttr.put(ab.atrName, ab.typeAttr);
                    attrNames.add(ab.atrName);
                }
                nodeKidIdnameAttr.put(child.nodeKindId, attrNames);
                nodeKidIdnameAndTypeAttr.put(child.nodeKindId, nameAndTypeAttr);
            }
        }

        for (final ChildrenBean child : children) {
            final List<AttributeBean> attrs = child.attributes;

            final Integer childrenNodeId = child.nodeId;
            final Integer treeId = child.treeId;
            final String branchIds = child.branchIds;
            final List<Widget> widgets = new ArrayList<Widget>();
            widgets.add(new HTML());
            int tabCnt = BaseUtils.splitBySep(child.nodePath, "»").size();
            HTML blank = new HTML();
            if (tabCnt > 1) {
                blank.setWidth(20 * (tabCnt - 1) + "px");
            }

//            int widgetIndex = 0;
            HorizontalPanel hp = new HorizontalPanel();
//            widgets.add(BikIcon.createIcon("images/" + child.icon + ".gif" /* I18N: no */, "RelatedObj-ObjIcon"));
            hp.add(blank);
            if (node != null && node.isRegistry) {
                //jeśli to rejestr - zamiast linku na atrybut wyświetlamy nazwę zdarzenia
                Label eventNameLabel = new Label(child.name);
                eventNameLabel.setStyleName("linkPointerAndColor");
                eventNameLabel.addClickHandler(new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        showAttributeFullList(child);
                    }
                });
                hp.add(eventNameLabel);
            } else {
                hp.add(BikIcon.createIcon("images/" + child.icon + ".gif" /* I18N: no */, "RelatedObj-ObjIcon"));
                hp.add(BIKClientSingletons.createLinkAsAnchor(child.name, child.treeCode, child.nodeId));//name of an event as a link
            }
            widgets.add(hp);
            if (showDescription) {
                widgets.add(new Label(
                        BaseUtils.getTrimStrToLength(child.description, 80)));
            }
            Map<String, AttributeBean> attrsMap = new HashMap<String, AttributeBean>();
            if (attrs != null) {
                for (AttributeBean attr : attrs) {
//                    Window.alert(attr.atrName + "  :  " + attr.typeAttr);
                    attrsMap.put(attr.atrName, attr);
                }
            }
            boolean isVisibleAttrEditBtns = false;
            if (BIKClientSingletons.isEffectiveExpertLoggedIn()
                    || BIKClientSingletons.isBranchAuthorLoggedIn(child.branchIds) || BIKClientSingletons.isLoggedUserAuthorOfTree(child.treeId)
                    || BIKClientSingletons.isBranchCreatorLoggedIn(child.branchIds) || BIKClientSingletons.isLoggedUserCreatorOfTree(child.treeId)
                    || BIKClientSingletons.isSysAdminLoggedIn()) {
                isVisibleAttrEditBtns = true;
            }

//            for (final String attr : attrDicList) {
            for (Entry<String, Boolean> attrD : attrDicListIsOrNotStatus.entrySet()) {
                final String attr = attrD.getKey();
                final AttributeBean attrBean = attrsMap.get(attr);

                final ClickHandler handler = new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        AttributeBean emptyAttribute = new AttributeBean();
                        if (attrBean == null) {
                            /*Gdy nie ma wartości atrbutu nie jest pobierana nazwa i typ */
                            emptyAttribute.atrName = attr;
                            emptyAttribute.typeAttr = nodeKidIdnameAndTypeAttr.get(child.nodeKindId).get(attr);
                            emptyAttribute.isAddNewInChildrenNodes = true;
                        }
                        Map<String, List<Integer>> usedLink = new HashMap<String, List<Integer>>();
                        String typeAttr = attrBean == null ? emptyAttribute.typeAttr : attrBean.typeAttr;
                        if ((AttributeType.hyperlinkInMulti.name().equals(typeAttr)
                                || AttributeType.hyperlinkInMono.name().equals(typeAttr))
                                || AttributeType.permissions.name().equals(typeAttr)) {
                            if (!BaseUtils.isStrEmptyOrWhiteSpace(attrBean.atrLinValue)) {
                                List<Integer> links = new ArrayList<Integer>();
                                List<String> linkValues = BaseUtils.splitBySep(attrBean.atrLinValue, "|");
                                for (String link : linkValues) {

                                    if (!BaseUtils.isStrEmptyOrWhiteSpace(link)) {
                                        links.add(BaseUtils.tryParseInteger(BaseUtils.splitBySep(link, "_").get(0)));
                                    }
                                }

                                usedLink.put(attrBean.atrName, links);
//                                BIKClientSingletons.getEDPForms().showAttributeForm(childrenNodeId, node, nodeId, attrBean, treeId, attrs, node.nodeKindId, true, usedLink);
                            }
                        }
                        BIKClientSingletons.getEDPForms().showAttributeForm(childrenNodeId, branchIds, node, attrBean == null ? emptyAttribute : attrBean, treeId, attrs, child.nodeKindId, true, usedLink, false);
                    }
                };

                if (attrBean == null /*&& !BaseUtils.isCollectionEmpty(child.attributes)*/) {
                    List<String> a = nodeKidIdnameAttr.get(child.nodeKindId);
                    if (a != null && a.contains(attr)) {
                        widgets.add(createEditChildAttributeButton(childrenNodeId, isVisibleAttrEditBtns, handler));
//                    widgets.add(new Label("A"));
                    } else {
                        widgets.add(new Label(" "));
                    }
                    if (attrD.getValue()) {
                        widgets.add(new Label(" "));
                    }
                } else {
                    boolean displayAsNumber = attrBean != null && attrBean.displayAsNumber;
//                    if (attrBean != null && attrBean.typeAttr.equals(AddOrEditAdminAttributeWithTypeDialog.AttributeType.comboBooleanBox.name())) {
//                        widgets.add(BikIcon.createIcon(attrBean.valueOpt.startsWith(attrBean.atrLinValue) ? "images/check2.png" : "images/delete.png", "RelatedObj-ObjIcon"));
//                    } else {

                    Widget w = BIKClientSingletons.createAttributeView(attrBean.typeAttr, attrBean.atrLinValue, displayAsNumber, true, child.nodeId, null);
                    w.addStyleName("BikLabelClickable");

                    FlowPanel fp = new FlowPanel();
                    fp.setStyleName("ChildrenFp");
                    fp.add(createEditChildAttributeButton(childrenNodeId, isVisibleAttrEditBtns, handler));
                    fp.add(w);
                    widgets.add(fp);

//                    String trimmedVal = attrBean == null || attrBean.atrLinValue == null ? "" : attrBean.atrLinValue.trim();
//                    if (attrBean != null && AttributeType.hyperlinkInMulti.name().equals(attrBean.typeAttr)) {
//                        int idx = attrBean.atrLinValue.indexOf(",");
//                        final String nodeIdLink = attrBean.atrLinValue.substring(0, idx);
//                        String caption = attrBean.atrLinValue.substring(idx + 1, attrBean.atrLinValue.length());
//                        final List<String> link = BaseUtils.splitBySep(nodeIdLink, "_");
//                        HTML btn = new HTML(caption);
//                        btn.getElement().getStyle().setCursor(Style.Cursor.POINTER);
//                        btn.addClickHandler(new ClickHandler() {
//
//                            @Override
//                            public void onClick(ClickEvent event) {
//                                EntityDetailsPaneDialog edpd = new EntityDetailsPaneDialog();
//                                edpd.buildAndShowDialog(BaseUtils.tryParseInteger(link.get(0)), null, link.get(1));
//                            }
//                        });
//                        widgets.add(btn);
//                    } else if (BIKClientSingletons.isRenderAttributesAsHTMLMode() && BaseUtils.strHasPrefix(trimmedVal, RENDER_AS_HTML_VAL_PREFIX, false) && BaseUtils.strHasSuffix(trimmedVal, RENDER_AS_HTML_VAL_SUFFIX, false)) {
//                        if (displayAsNumber) {
//                            trimmedVal = BaseUtils.chunkHTMLWithSepSpace(trimmedVal);
//                        }
//                        widgets.add(new HTML(getHTMLInnerText(trimmedVal)));
//                    } else {
//                        if (displayAsNumber) {
//                            trimmedVal = BaseUtils.chunkStringWithSepSpace(trimmedVal);
//                        }
//                        widgets.add(new Label(trimmedVal));
//                    }
//                    }
//                    widgets.add(new Label("Test" + attr));
                    if (BIKClientSingletons.showAddStatus()) {
                        if (attrBean.isStatus != null && attrBean.isStatus && attrBean.statusDate != null) {

//                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//                        Date convertedCurrentDate = sdf.parse(attrBean.statusDate);
                            widgets.add(new Label(SimpleDateUtils.dateToString(attrBean.statusDate)
                            ));
                        } else {
                            if (attrD.getValue()) {
                                widgets.add(new Label(" "));
                            }
                        }
                    }
                }

            }
            if (bikPage != null && !node.isRegistry && BIKClientSingletons.showAddChildNodeBtn()) {
                Widget addChildBtn = leafNodeKind == null ? null : BIKClientSingletons.createAddChildButton(bikPage, I18n.dodaj.get() + " ...", child.nodeId);
                if (addChildBtn != null) {
                    widgets.add(addChildBtn);
                }
            }
            if (node != null && node.isRegistry && BIKClientSingletons.isSysAdminLoggedIn()) {
                widgets.add(createDeleteButton(child, child.name));
            } else if (node != null && !node.isRegistry) {
                widgets.add(BIKClientSingletons.createLookupButton(child.treeCode, child.nodeId, null, true));
                widgets.add(BIKClientSingletons.createActionButton("link_arrows", I18n.przejdz.get() /* I18N:  */, EntityDetailsWidgetFlatBase.createFollowCH(child.treeCode, child.nodeId)));
            }
            EntityDetailsWidgetFlatBase.createRow(main, indexCount++, widgets.toArray(new Widget[widgets.size()]));
        }
        main.setWidth("100%");
        main.setHeight("100%");
        vp.add(main);
        return vp;
    }

    protected void checkShouldShowSomeHeader() {
        Map<String, Boolean> attrMap = new HashMap<String, Boolean>();
        if (!BaseUtils.isCollectionEmpty(attrDicList)) {
            for (String attr : attrDicList) {
                attrMap.put(attr, Boolean.FALSE);
                attrDicListIsOrNotStatus.put(attr, Boolean.FALSE);
            }
        }
        if (!BaseUtils.isCollectionEmpty(children)) {
            for (ChildrenBean row : children) {
                List<AttributeBean> attributes = row.attributes;
                if (attributes == null) {
                    continue;
                }
                for (AttributeBean attribute : attributes) {
                    if (attribute.isPublic == 0
                            && (BIKClientSingletons.checkNonPublicUserRights(node.treeId, node.branchIds) || BIKClientSingletons.checkAuthorRights(node.treeId, node.branchIds)
                            || BIKClientSingletons.checkCreatorRights(node.treeId, node.branchIds) || BIKClientSingletons.isEffectiveExpertLoggedIn())) {
                        if (attrDicList.contains(attribute.atrName) && !BaseUtils.isStrEmptyOrWhiteSpace(attribute.atrLinValue)) {
                            attrMap.put(attribute.atrName, Boolean.TRUE);
                        }
                    } else if (attribute.isPublic == 1) {
                        if (attrDicList.contains(attribute.atrName) && !BaseUtils.isStrEmptyOrWhiteSpace(attribute.atrLinValue)) {
                            attrMap.put(attribute.atrName, Boolean.TRUE);
                            attrDicListIsOrNotStatus.put(attribute.atrName, BIKClientSingletons.showAddStatus() ? attribute.isStatus : false);
                        }
                    }

                }
            }
        }
        for (Map.Entry<String, Boolean> entrySet : attrMap.entrySet()) {
            if (!entrySet.getValue()) {
                attrDicList.remove(entrySet.getKey());

                attrDicListIsOrNotStatus.remove(entrySet.getKey());//usuwa nagłówki gdy brak wartości dla dzieci/ może usunąć date statusu?
            }
        }
    }

    protected void checkIfDescriptionIsNeeded() {
        if (showDescription) {
            showDescription = false;
            if (!BaseUtils.isCollectionEmpty(children)) {
                for (ChildrenBean child : children) {
                    if (child != null && !BaseUtils.isHTMLTextEmpty(child.description)) {
                        showDescription = true;
                        return;
                    }
                }
            }
        }
    }

    protected List<String> attrPopUpInfo = new ArrayList<String>();

    protected void showAttributeFullList(final ChildrenBean child) {

        BIKClientSingletons.getService().getRequiredAttrsWithVisOrder(child.nodeKindId, new StandardAsyncCallback<List<AttributeBean>>() {

            @Override
            public void onSuccess(List<AttributeBean> result) {

                if (!BaseUtils.isCollectionEmpty(child.attributes) && !BaseUtils.isCollectionEmpty(result)) {
                    for (AttributeBean listAb : result) {
                        for (AttributeBean ab : child.attributes) {
                            if (listAb.atrName.equals(ab.atrName)) {
                                listAb.atrLinValue = ab.atrLinValue;
                            }
                        }
                    }
                }

                new EditEventAttributeRow().buildAndShowDialog(child.nodeKindId, node.treeId, false, null, node,
                        result, child.name, new IParametrizedContinuation<List<AttributeBean>>() {

                    @Override
                    public void doIt(List<AttributeBean> param) {

                        for (final AttributeBean ab : param) {
                            BIKClientSingletons.getService().editBikEntityAttribute(ab.id, child.nodeId, ab.atrLinValue,
                                    new StandardAsyncCallback<Void>() {

                                @Override
                                public void onSuccess(Void result) {
                                }

                                @Override
                                public void onFailure(Throwable caught) {
                                    BIKClientSingletons.showInfo("Wartość atrybutu " + ab.atrName + " nie została zmieniona");
                                }
                            });
                        }
                        BIKClientSingletons.getTabSelector().invalidateNodeData(node.id);
                        BIKClientSingletons.showInfo(I18n.attrRowEdited.get());
                    }
                });
            }
        });
//               new EditEventAttributeRow().buildAndShowDialog(child.nodeKindId, node.treeId, false, null, node,
//                        result, child.name, new IParametrizedContinuation<List<AttributeBean>>() {
//
//                            @Override
//                            public void doIt(List<AttributeBean> param) {
//
//                                for (final AttributeBean ab : param) {
//                                    BIKClientSingletons.getService().editBikEntityAttribute(ab.id, child.nodeId, ab.atrLinValue,
//                                            new DoNothingAsyngCallback<Void>());
//                                }
//                                BIKClientSingletons.getTabSelector().invalidateNodeData(node.id);
//                                BIKClientSingletons.showInfo(I18n.attrRowEdited.get());
//                            }
//                        });
    }
}
