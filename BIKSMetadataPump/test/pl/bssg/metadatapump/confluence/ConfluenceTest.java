/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.confluence;

import commonlib.LameUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.swift.common.soap.confluence.ConfluenceSoapService;
import org.swift.common.soap.confluence.ConfluenceSoapServiceServiceLocator;
import org.swift.common.soap.confluence.RemoteBlogEntry;
import org.swift.common.soap.confluence.RemoteBlogEntrySummary;
import org.swift.common.soap.confluence.RemotePageSummary;
import org.swift.common.soap.confluence.RemoteServerInfo;
import org.swift.common.soap.confluence.RemoteSpaceSummary;
import pl.bssg.metadatapump.common.ConfluenceObjectBean;

/**
 *
 * @author tflorczak
 */
public class ConfluenceTest {

//    private static ConfluenceSoapServiceServiceLocator fConfluenceSoapServiceGetter = new ConfluenceSoapServiceServiceLocator();
    private static ConfluenceSoapService fConfluenceSoapService = null;
    private static String fToken = null;

    public static String getToken() {
        return fToken;
    }

    public static ConfluenceSoapService getConfluenceSOAPService() {
        return fConfluenceSoapService;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        Date da = new Date();
        System.out.println(da.getTime());
        readConfluence();

        createDummyContent();
//        tryKyo();
    }

    private static void readConfluence() {
        try {
            String server = "http://localhost:8090"; // CC
//            String server = "http://192.168.167.54:8090"; // 4Monia
//            String server = "http://192.168.167.57:8090"; // winbor3
//            String server = "http://192.168.167.71:8081/confluence"; // AD
//            String server = "http://cyfropedia/confluence";
            String user = "admin";
//            String user = "mtararuj";
            String pass = "1qazxsw23edc";
//            String pass = "87654321";
            try {
//                String endPoint = "/rpc/soap-axis/confluenceservice-v1";
                String endPoint = "/rpc/soap-axis/confluenceservice-v2";
//                String endPoint = "/wiki/rpc/soap-axis/confluenceservice-v1";
//                String endPoint = "/rpc/xmlrpc";
//                String endPoint = "/wiki/rpc/xmlrpc/";
                ConfluenceSoapServiceServiceLocator fConfluenceSoapServiceGetter = new ConfluenceSoapServiceServiceLocator();
//                fConfluenceSoapServiceGetter.setConfluenceserviceV1EndpointAddress(server + endPoint);
                fConfluenceSoapServiceGetter.setConfluenceserviceV2EndpointAddress(server + endPoint);
                fConfluenceSoapServiceGetter.setMaintainSession(true);
//                fConfluenceSoapService = fConfluenceSoapServiceGetter.getConfluenceserviceV1();
                fConfluenceSoapService = fConfluenceSoapServiceGetter.getConfluenceserviceV2();
                System.out.println("before login");
                fToken = fConfluenceSoapService.login(user, pass);
                System.out.println("after login");
            } catch (Exception e) {
                System.out.println("Err: " + e.getMessage());
            }
            System.out.println("Connected ok.");
            ConfluenceSoapService service = getConfluenceSOAPService();
            String token = getToken();

            RemoteServerInfo info = null;
            try {
                info = service.getServerInfo(token);
            } catch (Exception ex) {
                System.out.println("Error: " + ex.getMessage());
            }
            System.out.println("Confluence version: " + info.getMajorVersion() + "." + info.getMinorVersion());
            RemoteSpaceSummary[] spaces = service.getSpaces(token);
            for (int i = 0; i < spaces.length; i++) {
                RemoteSpaceSummary oneSpace = spaces[i];
                System.out.println("Space: " + oneSpace.getName() + ", type: " + oneSpace.getType() + ", url: " + oneSpace.getUrl() + ", key: " + oneSpace.getKey());
                System.out.println("===> Pages: ");
                RemotePageSummary[] pages = service.getTopLevelPages(token, oneSpace.getKey());
                for (int j = 0; j < pages.length; j++) {
                    RemotePageSummary onePage = pages[j];
                    System.out.println("---------> Id: " + onePage.getId() + ", parentID: " + onePage.getParentId() + ", title: " + onePage.getTitle() + ", space: " + onePage.getSpace() + ", version:" + onePage.getVersion() + ", url: " + onePage.getUrl());
//                    RemotePage page = service.getPage(token, onePage.getId());
//                    System.out.println("++++++++++++++++> Content: " + page.getContent());
//                    try {
//                        System.out.println("################> Content as XHTML: " + service.convertWikiToStorageFormat(token, page.getContent()));
//                    } catch (Exception e) {
//                        System.out.println("################> ERROR IN CONVERT TO XHTML FOR PAGE: " + onePage.getTitle());
//                    }
                }
                RemoteBlogEntrySummary[] blogEntries = service.getBlogEntries(token, oneSpace.getKey());
                System.out.println("$$$$$$> Blogs: ");
                for (int j = 0; j < blogEntries.length; j++) {
                    RemoteBlogEntrySummary blog = blogEntries[j];
                    System.out.println("Blog: " + blog.getTitle() + ", space: " + blog.getSpace() + ", author: " + blog.getAuthor() + ", date: " + blog.getPublishDate() + ", id: " + blog.getId());
                    RemoteBlogEntry blogEntry = service.getBlogEntry(token, blog.getId());
//                    blogEntry.getContent();
                }
//                RemotePage page = service.getPage(token, 557135);
//                System.out.println("Status: " + page.getContentStatus());
//                System.out.println("Children size: " + children.length);
            }
            System.out.println("Completed.");
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }

//    public static void tryKyo() throws FileNotFoundException {
//        Kryo kryo = new Kryo();
//        // ...
//        Output output = new Output(new FileOutputStream("C:/Chung/file.bin"));
//        List<List<ConfluenceObjectBean>> someObject = new ArrayList<List<ConfluenceObjectBean>>();
//        for (int i = 0; i < 100; i++) {
//            List<ConfluenceObjectBean> l = new ArrayList<ConfluenceObjectBean>();
//            for (int j = 0; j < 100; j++) {
//                ConfluenceObjectBean obj = new ConfluenceObjectBean();
//                obj.addAsRoot = false;
//                obj.author = LameUtils.generateRandomToken(20);
//                obj.authorFullName = LameUtils.generateRandomToken(20);
//                obj.branchIds = LameUtils.generateRandomToken(40);
//                obj.content = LameUtils.generateRandomToken(1000);
//                obj.groupId = 10029;
//                obj.hasNoChildren = true;
//                obj.id = LameUtils.generateRandomToken(5);
//                obj.kind = LameUtils.generateRandomToken(1000);
//                obj.name = LameUtils.generateRandomToken(100);
//                obj.parentId = LameUtils.generateRandomToken(100);
//                obj.space = LameUtils.generateRandomToken(50);
//                obj.treeId = 29;
//                obj.url = LameUtils.generateRandomToken(100);
//                l.add(obj);
//            }
//            someObject.add(l);
//        }
//        kryo.writeObject(output, someObject);
//        output.close();
//        // ...
//        Input input = new Input(new FileInputStream("C:/Chung/file.bin"));
//        ArrayList<List<ConfluenceObjectBean>> readObject = kryo.readObject(input, ArrayList.class);
//        for (int i = 0; i < readObject.size(); i++) {
//            List<ConfluenceObjectBean> l = readObject.get(i);
//            for (ConfluenceObjectBean obj : l) {
//                System.out.println(obj.groupId);
//                System.out.println(obj.url);
//                System.out.println(obj.treeId);
//            }
//        }
//        input.close();
//    }
    private static void createDummyContent() throws InterruptedException, Exception {
        String login = "admin";
        String password = "1qazxsw23edc";
        IConfluenceConnector conn = new ConfluenceConnector(new ConfluenceConfigBean(1, "http://localhost:8090", login, password, 1));
        conn.initializeService();
        conn.login();

        List<String> ids = new ArrayList<String>();
        ids.add("65576");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");

        Calendar cal1 = Calendar.getInstance();
        Random rand = new Random();
        StringBuilder articleBody = new StringBuilder();
        for (int j = 0; j < 1048; j++) {
            articleBody.append("\n").append(LameUtils.generateRandomToken(1 << 10));
        }
        for (int i = 0; i < 10000; i++) {
            long timeInMillis = System.currentTimeMillis();
            Integer idx = (int) (rand.nextDouble() * ids.size());
            String parentId = ids.get(idx);

            parentId = "1867811";
            System.out.println(i + ":" + parentId);
            cal1.setTimeInMillis(timeInMillis);
            String title = "Dummy title " + sdf.format(cal1.getTime()) + LameUtils.generateRandomToken(10);
            ConfluenceObjectBean bean = conn.publishArticleToConfluence(title, articleBody.toString(), login, password, parentId, "BSSG");
            ids.add(bean.id);
//            Thread.sleep(3000);
        }
//        tryKyo();
    }
}
