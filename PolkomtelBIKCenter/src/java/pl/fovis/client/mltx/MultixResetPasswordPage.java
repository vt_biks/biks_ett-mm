/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.mltx;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.mltx.history.HistoryAction;
import pl.fovis.client.mltx.history.MultiXHistoryToken;
import pl.fovis.common.MultixReturnMessage;
import pl.fovis.common.TokenType;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.GWTUtils;
import simplelib.IContinuation;

/**
 *
 * @author LKI
 */
public class MultixResetPasswordPage extends SimplePanel {

    protected VerticalPanel vp;
    protected FlexTable ft;
    private PushButton confirmBtn;
    private Label info;
    protected PasswordTextBox passwordTb, repeatPasswordTb;
    protected Label passwordLbl, repeatPasswordLbl;
    private String token;

    public static HistoryAction getHistoryAction() {
        return new HistoryAction() {
            public Widget performAction(String id) {
                return new MultixResetPasswordPage(id);
            }
        };
    }

    public MultixResetPasswordPage(String token) {
        this.token = token;
        buildWidgets();
    }

    private void buildWidgets() {
        vp = new VerticalPanel();
        info = new Label();
        info.setStyleName("NewLoginLbl");

        vp.add(info);
        BIKClientSingletons.getMultiXService().verifyToken(token, TokenType.RESET_PASSWORD, new StandardAsyncCallback<MultixReturnMessage>() {
            public void onSuccess(MultixReturnMessage result) {
                if (result.type == MultixReturnMessage.msgType.error) {
                    createNextPage(result.msg);
                } else {
                    vp.add(buildRessetFt());
                }
            }
        });

        FlexTable MltxBackground = NewLookUtils.mltxBackground(vp);
        this.add(MltxBackground);
        displayInfo(I18n.trwaWeryfikacja.get());
    }

    protected FlexTable buildRessetFt() {
        displayInfo(I18n.resetingPassword.get());

        ft = new FlexTable();
        ft.setCellSpacing(2);
        HorizontalPanel emailPanel = new HorizontalPanel();
        emailPanel.setWidth("100%");
        ft.setWidget(0, 0, passwordLbl = new Label(I18n.wprowadzHaslo.get()));
        ft.setWidget(0, 1, passwordTb = new PasswordTextBox());
        ft.setWidget(1, 0, repeatPasswordLbl = new Label(I18n.powtorzHaslo2.get()));
        ft.setWidget(1, 1, repeatPasswordTb = new PasswordTextBox());

        passwordTb.setStyleName("userDialogInput");
        passwordTb.setWidth("258px");
        passwordLbl.setStyleName("userDialogLbl");
        repeatPasswordTb.setStyleName("userDialogInput");
        repeatPasswordTb.setWidth("258px");
        repeatPasswordLbl.setStyleName("userDialogLbl");

        addEnterKeyHandler(passwordTb);

        confirmBtn = new PushButton(I18n.reset.get());
        NewLookUtils.makeCustomPushButton(confirmBtn);
        confirmBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                doAction();
            }
        });

        ft.setWidget(2, 0, confirmBtn);

        confirmBtn.setStyleName("gwt-PushButton");

        return ft;
    }

    protected void addEnterKeyHandler(TextBox tb) {
        GWTUtils.addEnterKeyHandler(tb, new IContinuation() {
            @Override
            public void doIt() {
                doAction();
            }
        });
    }

    private void createNextPage(String info) {
        displayInfo(info);
        AnchorAsHyperlink login = new AnchorAsHyperlink(I18n.zaloguj.get(), MultiXHistoryToken.LOGIN.hname);
        vp.add(login);
    }

    private void doAction() {
        if (!isPasswordCorrect()) {
            BIKClientSingletons.showError(I18n.bledneHaslo.get());
            return;
        }
        ft.clear();
        BIKClientSingletons.getMultiXService().resetPassword(token, passwordTb.getText(), new StandardAsyncCallback<String>() {
            public void onSuccess(String result) {
                if (result != null) {
                    createNextPage(result);
                } else {
                    createNextPage(I18n.resetowanieHaslaZakonczonePomyslnie.get());
                }
            }
        });

    }

    private void displayInfo(String txt) {
        info.setText(txt);
    }

    private boolean isPasswordCorrect() {
        return passwordTb.getText().length() != 0 && passwordTb.getText().equals(repeatPasswordTb.getText());
    }
}
