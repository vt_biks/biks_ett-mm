/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import pl.fovis.client.nodeactions.INodeAction;
import pl.fovis.client.nodeactions.NodeActionAddObjectFromConfluence;
import pl.fovis.client.nodeactions.NodeActionAddObjectToConfluence;
import pl.fovis.client.nodeactions.NodeActionContext;
import pl.fovis.common.TreeBean;
import pl.trzy0.foxy.commons.annotations.DisableDynamicClassCreation;

/**
 *
 * @author tflorczak
 */
@DisableDynamicClassCreation
public class ConfluenceTreeBikPage extends TreeBikPage {

    protected INodeAction importFromConfluence;
    protected INodeAction exportToConfluence;
    protected PushButton importButton;
    protected PushButton exportButton;

    public ConfluenceTreeBikPage(TreeBean tree) {
        super(tree, TreeBikPage.TREE_BIK_PAGE_ONE_COL_FOR_TREE);
    }

    @Override
    protected void addOptionalExtraWidgetsToTreeFooter(HorizontalPanel hPanel) {
        super.addOptionalExtraWidgetsToTreeFooter(hPanel);
        importFromConfluence = new NodeActionAddObjectFromConfluence();
        exportToConfluence = new NodeActionAddObjectToConfluence();

        importFromConfluence.setup(new NodeActionContext(null, this, null));
        exportToConfluence.setup(new NodeActionContext(null, this, null));

//        if (importFromConfluence.canBeEnabledByStateForThisNodeAndUserLoggedIn()) {
        if (importFromConfluence.isEnabled()) {
            importButton = createButton(hPanel, importFromConfluence, "images/import_confluence.png" /* I18N: no */);
        }
//        if (exportToConfluence.canBeEnabledByStateForThisNodeAndUserLoggedIn()) {
        if (exportToConfluence.isEnabled()) {
            exportButton = createButton(hPanel, exportToConfluence, "images/export_confluence.png" /* I18N: no */);
        }
    }

    protected PushButton createButton(HorizontalPanel hPanel, final INodeAction nodeaction, String icon) {
        PushButton button = new PushButton(new Image(icon), new Image(icon));
        button.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                nodeaction.execute();
            }
        });
        button.setTitle(nodeaction.getCaption());
        button.addStyleName("plusLarge");
        hPanel.add(button);
        return button;
    }

    @Override
    protected void optRefreshOtherButtons() {
        super.optRefreshOtherButtons();

        if (importButton != null) {
            importButton.setVisible(importFromConfluence.isEnabled());
        }
        if (exportButton != null) {
            exportButton.setVisible(exportToConfluence.isEnabled());
        }
    }
}
