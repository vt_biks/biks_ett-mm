/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

//import pl.trzy0.foxy.gwt.asyncdata.ServerCall;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import java.util.List;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author wezyr
 */
public class FoxyFileUpload extends Composite {

    //ww: poniższe można nadpisać z pliku językowego
    public static String NO_FILE_TO_SEND_MSG = I18n.brakPodanegoPlikuNieMaCoWysylac.get() /* I18N:  */;
    public static String SEND_BTN_CAPTION = I18n.wyslij.get() /* I18N:  */;
    public static String SENDING_IN_PROGRESS_STATUS = I18n.wysylaniePlikuWToku.get() /* I18N:  */ + "...";
    public static String FILE_SENT_STATUS = I18n.plikWyslany.get() /* I18N:  */;
    public static List allowedExst;
    public static boolean uploadIsAllowed;

    public static enum FileUploadStatus {

        NoFileToUpload, FileSelected, UploadInProgress, UploadDone
    };
    protected FileUpload upload;
    protected String serverFileName;
    //    private IParametrizedContinuation<FoxyFileUpload> doOnComplete;
    protected IContinuation doOnComplete;
    protected FormPanel form = new FormPanel();

    public FoxyFileUpload() {
        //initWidget(createUploadForm(servletName));
        this(FoxyClientSingletons.FILE_HANDLING_SERVLET_NAME, null);
    }

    public FoxyFileUpload(String servletName) {
        //initWidget(createUploadForm(servletName));
        this(servletName, null);
    }

    public FoxyFileUpload(IContinuation/*<FoxyFileUpload>*/ doOnComplete) {
        this(FoxyClientSingletons.FILE_HANDLING_SERVLET_NAME, doOnComplete);
    }

    public FoxyFileUpload(String servletName, IContinuation/*<FoxyFileUpload>*/ doOnComplete) {
        this(servletName, doOnComplete, true);
    }

    public FoxyFileUpload(IContinuation/*<FoxyFileUpload>*/ doOnComplete,
            boolean includeButton) {
        this(FoxyClientSingletons.FILE_HANDLING_SERVLET_NAME, doOnComplete, includeButton);
    }

    public FoxyFileUpload(IContinuation doOnComplete,
            boolean includeButton, List<String> allowedExst) {
        this(FoxyClientSingletons.FILE_HANDLING_SERVLET_NAME, doOnComplete, includeButton);
        this.allowedExst = allowedExst;
    }

    public FoxyFileUpload(String servletName, IContinuation/*<FoxyFileUpload>*/ doOnComplete,
            boolean includeButton) {
        this.doOnComplete = doOnComplete;
        initWidget(createUploadFormPrivate(servletName, includeButton));
    }

    public void performUpload() {
        if (!isFileSelected()) {
            Window.alert(NO_FILE_TO_SEND_MSG);
        } else {
            form.submit();
        }
    }

    private FormPanel createUploadFormPrivate(String servletName, boolean includeButton) {
        return createUploadForm(servletName, includeButton);
    }

    protected FormPanel createUploadForm(String servletName, boolean includeButton) {
        // Create a FormPanel and point it at a service.
        form.setAction(FoxyClientSingletons.getWebAppUrlPrefixForOtherServlet(servletName));

        // Because we're going to add a FileUpload widget, we'll need to set the
        // form to use the POST method, and multipart MIME encoding.
        form.setEncoding(FormPanel.ENCODING_MULTIPART);
        form.setMethod(FormPanel.METHOD_POST);

        // Create a panel to hold all of the form widgets.
        final HorizontalPanel panel = new HorizontalPanel();
        form.setWidget(panel);

        // Create a FileUpload widget.
        upload = new FileUpload();
        upload.setName("file" /* I18N: no */);
        panel.add(upload);

        if (includeButton) {

            final Button submit = new Button(SEND_BTN_CAPTION);
            // final PushButton submit = new PushButton("Wyślij");
            // NewLookUtils.makeCustomPushButton(submit);

            submit.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    submit.setVisible(false);
                    performUpload();
                }
            });

            // Add a 'submit' button.
            panel.add(submit);
        }

        final Label uploadStatus = new Label();
        uploadStatus.setVisible(false);
        panel.add(uploadStatus);

        // Add an event handler to the form.
        form.addSubmitHandler(new FormPanel.SubmitHandler() {
            public void onSubmit(SubmitEvent event) {
                if (upload.getFilename() == null) {
                    Window.alert(NO_FILE_TO_SEND_MSG);
                    event.cancel();
                } else {
                    serverFileName = "?";
                    upload.setVisible(false);
                    uploadStatus.setText(SENDING_IN_PROGRESS_STATUS);
                    uploadStatus.setVisible(true);
                }
            }
        });

        form.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
            public void onSubmitComplete(SubmitCompleteEvent event) {
                String res = event.getResults();
                serverFileName = res;
                uploadIsAllowed = false;
                if (allowedExst.contains(serverFileName.substring(serverFileName.lastIndexOf('.')).toLowerCase())) {
                    uploadIsAllowed = true;
                }
                uploadStatus.setText(FILE_SENT_STATUS + ":\""
                        + /*upload.getFilename()*/ getClientFileName() + "\"");
                if (doOnComplete != null) {
                    doOnComplete.doIt(/*FoxyFileUpload.this*/);
                }
            }
        });

//        form.addFormHandler(new FormHandler() {
//
//            public void onSubmit(FormSubmitEvent event) {
//                if (upload.getFilename() == null) {
//                    Window.alert("Brak podanego pliku, nie ma co wysyłać");
//
//                    event.setCancelled(true);
//                } else {
//                    serverFileName = "?";
//                    upload.setVisible(false);
//                    uploadStatus.setText("Wysyłanie pliku w toku...");
//                    uploadStatus.setVisible(true);
//                }
//            }
//
//            public void onSubmitComplete(FormSubmitCompleteEvent event) {
//                String res = event.getResults();
//                serverFileName = res;
//                uploadStatus.setText("Plik \""
//                        + /*upload.getFilename()*/ getClientFileName() + "\" wysłany");
//                if (doOnComplete != null) {
//                    doOnComplete.doIt(FoxyFileUpload.this);
//                }
//            }
//        });
        return form;
    }

    public FileUploadStatus getStatus() {
        if (serverFileName == null) {
            if (BaseUtils.isStrEmptyOrWhiteSpace(upload.getFilename())) {
                return FileUploadStatus.NoFileToUpload;
            } else {
                return FileUploadStatus.FileSelected;
            }
        } else {
            if ("?".equals(serverFileName)) {
                return FileUploadStatus.UploadInProgress;
            } else {
                return FileUploadStatus.UploadDone;
            }
        }
    }

    public String getServerFileName() {
        return getStatus() == FileUploadStatus.UploadDone ? serverFileName : null;
    }

    public String getClientFileName() {
        String fileName = upload.getFilename();
        if (BaseUtils.isStrEmptyOrWhiteSpace(fileName)) {
            return null;
        }

        int pos = BaseUtils.lastPos(fileName.lastIndexOf("/"), fileName.lastIndexOf("\\"), false);

        return fileName.substring(pos + 1);
    }

    public boolean isFileSelected() {
        return !BaseUtils.isStrEmptyOrWhiteSpace(upload.getFilename());
    }

    public HandlerRegistration addChangeHandler(ChangeHandler handler) {
        return upload.addChangeHandler(handler);
    }

    public static boolean isUploadIsAllowed() {
        return uploadIsAllowed;
    }
}
