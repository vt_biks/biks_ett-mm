/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.logging;

/**
 *
 * @author wezyr
 */
public interface ILameLogger {

    public static enum LameLogLevel {

        Debug, Info, Stage, Warn, Error, Fatal, None;

        public static LameLogLevel getHigherLevel(LameLogLevel l1, LameLogLevel l2) {
            if (l1 == null && l2 == null) {
                return Debug;
            }
            if (l1 == null) {
                return l2;
            }
            if (l2 == null) {
                return l1;
            }
            return l1.ordinal() < l2.ordinal() ? l2 : l1;
        }

        public static LameLogLevel getLowerLevel(LameLogLevel l1, LameLogLevel l2) {
            if (l1 == null && l2 == null) {
                return None;
            }
            if (l1 == null) {
                return l2;
            }
            if (l2 == null) {
                return l1;
            }
            return l1.ordinal() > l2.ordinal() ? l2 : l1;
        }

        public boolean isLowerLevelThan(LameLogLevel l) {
            return ordinal() < l.ordinal();
        }
    };

    public String getSimpleLoggerName();

    public String getFullLoggerName();

    public LameLogLevel getLoggerLevel();

    public void debug(String msg);

    public void info(String msg);

    public void stage(String msg);

    public void warn(String msg);

    public void warn(String msg, Throwable cause);

    public void error(String msg);

    public void error(String msg, Throwable cause);

    public void fatal(String msg);

    public void fatal(String msg, Throwable cause);

    public boolean isDebugEnabled();

    public boolean isInfoEnabled();

    public boolean isStageEnabled();

    public boolean isWarnEnabled();

    public boolean isErrorEnabled();

    // log if error is enabled and always throw
    public <T extends Throwable> void errorAndThrow(String msg, T ex) throws T;

    public <T extends Throwable> void errorAndThrowNew(String exceptionMsg, T cause);

    public <T extends Throwable> void errorAndThrowNew(String exceptionMsg);

    public void logAtLevel(LameLogLevel level, String msg);

    public void logAtLevel(LameLogLevel level, String msg, Throwable cause);

    public boolean isEnabledAtLevel(LameLogLevel level);

    @Deprecated
    public ILameLogger setLevelForDebugOnly(LameLogLevel level);
}
