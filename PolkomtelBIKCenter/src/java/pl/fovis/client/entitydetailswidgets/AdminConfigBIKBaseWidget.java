/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.AdminConfigBaseWithTestWidget;

/**
 *
 * @author tflorczak
 */
public abstract class AdminConfigBIKBaseWidget<T> extends AdminConfigBaseWithTestWidget<T> {

    @Override
    protected void showSuccessTestInfo() {
        BIKClientSingletons.showInfo(I18n.testPolaczeniaZakonczonySukcesem.get() /* I18N:  */);
    }

    @Override
    protected void showErrorTestInfo() {
        BIKClientSingletons.showWarning(I18n.testPolaczenZakonczoNiepowod.get() /* I18N:  */);
    }
}
