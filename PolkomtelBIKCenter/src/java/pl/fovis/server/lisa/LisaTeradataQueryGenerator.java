/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.lisa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.entitydetailswidgets.lisa.LisaFiltrColumnValueBase;
import pl.fovis.client.entitydetailswidgets.lisa.LisaFiltrColumnValueBetween;
import pl.fovis.client.entitydetailswidgets.lisa.LisaFiltrColumnValueSingle;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaConstants;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryBean;
import pl.fovis.common.lisa.LisaDictionaryValue;
import pl.fovis.common.lisa.LisaGridParamBean;
import pl.fovis.common.lisa.LisaUtils;
import pl.fovis.common.lisa.TeradataException;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author lbiegniewski
 */
// i18n-default: no
public class LisaTeradataQueryGenerator {

    private String SCHEMA_NAME;
    protected static final String AND = " and ";
    protected static final String WHERE = " where ";
    protected static final String WITH_NO_LOCK = " LOCKING ROW FOR ACCESS ";

    private final AuthorizedTeradataConnection tConnection;
    //Dodatkowa walidacja w procedurach - tymczasowe
    //zmienne do czasu jak nie wprowadzą walidacji w Polkomtelu
    public static final Boolean VALIDATION_PROCEDURES = true;
    public static final String VALIDATION_SUFFIX = "";

    private int deliveredTableDumpId = 0;

    public LisaTeradataQueryGenerator(String schemaName, AuthorizedTeradataConnection tConnection) {
        this.SCHEMA_NAME = schemaName;
        this.tConnection = tConnection;
    }

    public String getTeradataDictionaryQuery(String dictionaryName) {
        return "select " + LisaConstants.DICTIONARY_ID_COL + ", id_slownik_grupa, nazwa_obszar_zr_slownik, nazwa_tabela_slownik, nazwa_slownik, nazwa_obszar_zr_proc_spr, nazwa_proc_spr, opis_slownik, id_slownik_typ, akt FROM " + SCHEMA_NAME + " SLOWNIK where id_slownik_typ = 'K' and nazwa_slownik = " + tConnection.valToString(dictionaryName) + " ORDER BY " + LisaConstants.DICTIONARY_ID_COL + ";";
    }

    public String getTeradataCategoryTypeDefQuery(String dictionaryCode) {
        return "select " + LisaConstants.CATEGORY_TYPE_DEF_ID_COL + ", " + LisaConstants.DICTIONARY_ID_COL + ", nazwa_typ_kateg_def, nazwa_obszar_zr, nazwa_tablica_rel, nazwa_kolumna_rel FROM " + SCHEMA_NAME + "kateg_typ_def where " + LisaConstants.DICTIONARY_ID_COL + " = " + tConnection.valToString(dictionaryCode) + " ORDER BY " + LisaConstants.CATEGORY_TYPE_DEF_ID_COL + ";";
    }

    public String getTeradataDictionaryColumnsMetadataQuery(String dictionaryTable) {
        return "SELECT distinct(dc.columnName), dc.columnId, dc.columnType, dc.columnLength, dc.nullable, dc.decimalTotalDigits, dc.decimalFractionalDigits FROM DBC.Columns dc WHERE dc.DatabaseName='" + LisaUtils.getDbWithoutNameTable(dictionaryTable, SCHEMA_NAME).replace(".", "") + "' AND dc.TableName='" + LisaUtils.getTableNameWithoutDB(dictionaryTable) + "' order by dc.columnId;";
    }

    public String getTeradataDictionaryPrimaryKeyColumnsQuery(String dictionaryTable) {
        /*
         * P (Nonpartitioned Primary)
         * Q (Partitioned Primary)
         * K (primary key)
         */
        return "SELECT distinct(columnName) from DBC.indices where tableName='" + LisaUtils.getTableNameWithoutDB(dictionaryTable) + "' and indexType in ('P','K','Q')";
    }

    public String getTeradataCategoryTypeQuery(String dictionaryRelTable, List<String> existingCategoriesTypes) {
        String csTypes = BaseUtils.mergeWithSepEx(existingCategoriesTypes, ",");
        StringBuilder sb = new StringBuilder();
        //sb.append("select " + CATEGORIZATION_TYPE_ID + ", " + CATEGORY_TYPE_DEF_ID_COL + ", " + CATEGORIZATION_TYPE_NAME + ", " + CATEGORIZATION_TYPE_DESCR + ", data_utworzenia, data_zmiany, wersjonowana FROM ").append(SCHEMA_NAME).append("kateg_typ where ");
        sb.append("select " + LisaConstants.CATEGORIZATION_TYPE_ID + ", " + LisaConstants.CATEGORY_TYPE_DEF_ID_COL + ", " + LisaConstants.CATEGORIZATION_TYPE_NAME + ", " + LisaConstants.CATEGORIZATION_TYPE_DESCR + ", data_utworzenia, data_zmiany, wersjonowana FROM ").append("kateg_typ where ");
        //pg - tak nie zadziała jeśli nie ma categoryTypeDefId!!! dlatego wywalamy to
        //+ CATEGORY_TYPE_DEF_ID_COL + " = ").append(tConnection.valToString(categoryTypeDefId));
        sb.append(LisaConstants.CATEGORIZATION_TYPE_ID + " in ( select distinct " + LisaConstants.CATEGORIZATION_TYPE_ID + " from ").append(LisaUtils.getDbWithoutNameTable(dictionaryRelTable, SCHEMA_NAME)).append(LisaUtils.getTableNameWithoutDB(dictionaryRelTable)).append(" )");
        if (!csTypes.isEmpty()) {
            sb.append(" and " + LisaConstants.CATEGORIZATION_TYPE_ID + " not in (").append(csTypes).append(")");
        }
        sb.append(" ORDER BY " + LisaConstants.CATEGORIZATION_TYPE_ID + ";");
        return sb.toString();
    }

    public String getTeradataCategoriesQuery(Set<String> categoryTypeIds, List<String> existingCategories) {
        String csTypes = BaseUtils.mergeWithSepEx(categoryTypeIds, ",", new BaseUtils.Projector<String, String>() {
            @Override
            public String project(String val) {
                return tConnection.valToString(val);
            }
        });
        String csCategories = BaseUtils.mergeWithSepEx(existingCategories, ",");
        StringBuilder sb = new StringBuilder();
        //sb.append("select kat." + CATEGORIZATION_TYPE_ID + ", kat." + CATEGORY_ID + ", kat." + CATEGORY_NAME + ", kat.opis_kateg, kat.data_utworzenia, kat.data_zmiany, kat.nazwa_kateg_en, kat.opis_kateg_en, kkr.id_kateg as parent_kateg_id " + "FROM ").append(SCHEMA_NAME).append("kateg kat ");
        sb.append("select kat." + LisaConstants.CATEGORIZATION_TYPE_ID + ", kat." + LisaConstants.CATEGORY_ID + ", kat." + LisaConstants.CATEGORY_NAME + ", kat.opis_kateg, kat.data_utworzenia, kat.data_zmiany, kat.nazwa_kateg_en, kat.opis_kateg_en, kkr.id_kateg as parent_kateg_id " + "FROM ").append("kateg kat ");

        //sb.append("left join " + SCHEMA_NAME + "kateg_kateg_rel kkr on kkr." + CATEGORIZATION_TYPE_ID + " = kat." + CATEGORIZATION_TYPE_ID + " and kkr.kateg_podrz = kat." + CATEGORY_ID + " ");
        sb.append("left join " + "kateg_kateg_rel kkr on kkr." + LisaConstants.CATEGORIZATION_TYPE_ID + " = kat." + LisaConstants.CATEGORIZATION_TYPE_ID + " and kkr.kateg_podrz = kat." + LisaConstants.CATEGORY_ID + " ");
        sb.append(" where 1=1 ");
        if (!csTypes.isEmpty()) {
            sb.append(" AND kat." + LisaConstants.CATEGORIZATION_TYPE_ID + " IN (").append(csTypes).append(") ");
        } else {
            sb.append(" AND 1=0 ");
        }
        if (!csCategories.isEmpty()) {
            sb.append(" AND kat." + LisaConstants.CATEGORY_ID + " NOT IN (").append(csCategories).append(")");
        }
        sb.append(" ORDER BY kat." + LisaConstants.CATEGORIZATION_TYPE_ID + ",kat." + LisaConstants.CATEGORY_ID + ";");
        return sb.toString();
    }

    public String getLatestDateForDictionaryValueQuery(LisaDictionaryBean dictData, String primaryKeyValues, Object categorizationObjId, Object categoryOldObjId) {
        return WITH_NO_LOCK + "select " + dictData.columnRelName + " as " + dictData.columnRelName + ", max(dr.data_od) as data_od from " + LisaUtils.getDbWithoutNameTable(dictData.dbRelTable, SCHEMA_NAME) + LisaUtils.getTableNameWithoutDB(dictData.dbRelTable) + " dr "
                + "where dr." + dictData.columnRelName + " in(" + primaryKeyValues + ")"
                + " and dr.id_typ_kateg = " + tConnection.valToString(categorizationObjId)
                + " and dr.id_kateg = " + tConnection.valToString(categoryOldObjId)
                + " and data_do >= current_timestamp"
                + " group by " + dictData.columnRelName;
    }

    public Pair<String, String> deleteDictionaryQuery(final Set<Map<String, LisaDictionaryValue>> records, String dictionaryName, List<LisaDictColumnBean> columns) {
        final String DELETE_PREFIX = "delete from " + LisaUtils.getDbWithoutNameTable(dictionaryName, SCHEMA_NAME) + LisaUtils.getTableNameWithoutDB(dictionaryName) + " where ";
        final Map<String, String> recordPkColumns = new HashMap<String, String>();
        final Set<String> pkColumns = removePkColumns(columns);

        for (final String pk : pkColumns) {
            String pkAsString = BaseUtils.mergeWithSepEx(records, ", ", new BaseUtils.Projector<Map<String, LisaDictionaryValue>, String>() {
                @Override
                public String project(Map<String, LisaDictionaryValue> val) {
                    return tConnection.valToString(val.get(pk).getValue());
                }
            });
            recordPkColumns.put(pk, pkAsString);
        }

        if (recordPkColumns.isEmpty()) {
            TeradataException e = new TeradataException(I18n.nieZdefinioKluczaGlownegoWTymSlo.get() /* i18n:  */);
            e.setIsBlocking(true);
            throw e;
        }

        StringBuilder sb = new StringBuilder(DELETE_PREFIX);
        // append where conditions, depending on is_main_key=1
        String logId = BaseUtils.mergeWithSepEx(recordPkColumns.entrySet(), AND, new BaseUtils.ProjectorToString<Map.Entry<String, String>>() {
            @Override
            public String project(Map.Entry<String, String> val) {
                return val.getKey() + " in ( " + val.getValue() + ")";
            }
        });

        sb.append(logId);
        return new Pair<String, String>(LisaServerUtils.transcodeToLatin1(sb.toString()), logId);
    }

    public Pair<String, String> insertDictionaryQuery(final Map<String, LisaDictionaryValue> record, String dictionaryName, List<LisaDictColumnBean> columns) {
        final String INSERT_PREFIX = "insert into " + LisaUtils.getDbWithoutNameTable(dictionaryName, SCHEMA_NAME) + LisaUtils.getTableNameWithoutDB(dictionaryName) + " ( ";
        StringBuilder sb = new StringBuilder(INSERT_PREFIX);
        sb.append(BaseUtils.mergeWithSepEx(columns, ",", new BaseUtils.ProjectorToString<LisaDictColumnBean>() {
            @Override
            public String project(LisaDictColumnBean column) {
                return "\"" + column.nameInDb + "\"";// + EQUALS + tConnection.valToString(record.get(column.nameInDb).getValue());
            }
        }));

        sb.append(") values (");
        String logId = BaseUtils.mergeWithSepEx(columns, ",", new BaseUtils.ProjectorToString<LisaDictColumnBean>() {
            @Override
            public String project(LisaDictColumnBean column) {
                Object value = record.get(column.nameInDb).getValue();
                return value == null ? "null" : tConnection.valToString(value);
            }
        });

        sb.append(logId).append(")");
        return new Pair<String, String>(LisaServerUtils.transcodeToLatin1(sb.toString()), logId);
    }

//    public String insertDictionaryQuery(List<Map<String, String>> lines, String dictionaryName, final List<LisaDictColumnBean> columnsInfo) {
//        final String INSERT_PREFIX = "insert into " + LisaUtils.getDbWithoutNameTable(dictionaryName, SCHEMA_NAME) + LisaUtils.getTableNameWithoutDB(dictionaryName) + " ( ";
//        StringBuilder sb = new StringBuilder(INSERT_PREFIX);
//        StringBuilder sb = new StringBuilder();
//        sb.append(BaseUtils.mergeWithSepEx(lines, ";", new BaseUtils.Projector<Map<String, String>, String>() {
//
//            @Override
//            public String project(final Map<String, String> record) {
//                return INSERT_PREFIX + BaseUtils.mergeWithSepEx(columnsInfo, ",", new BaseUtils.ProjectorToString<LisaDictColumnBean>() {
//                    @Override
//                    public String project(LisaDictColumnBean column) {
//                        return "\"" + column.nameInDb + "\"";// + EQUALS + tConnection.valToString(record.get(column.nameInDb).getValue());
//                    }
//                }) + ")  values(" + BaseUtils.mergeWithSepEx(columnsInfo, ",", new BaseUtils.ProjectorToString<LisaDictColumnBean>() {
//                    @Override
//                    public String project(LisaDictColumnBean column) {
//                        String value = record.get(column.nameInDb);
//                        if (value == null || (LisaConstants.IS_NULLABLE.equals(column.nullable.trim()) && BaseUtils.isStrEmptyOrWhiteSpace(value))) {
//                            value = "null";
//                        } else {
//                            value = "'" + value + "'";
//                        }
//
//                        switch (LisaDataType.getValueFor(column.dataType)) {
//                            case AT:
//                                value = "cast(" + value + " as time)";
//                            case BF:
//                            case BV:
//                                return "TO_BYTE(cast(" + value + " as integer))";
//                            case D:
//                                return "cast(" + value + " as decimal(" + column.decimalTotalDigits + "," + column.decimalFractionalDigits + "))";
//                            case DA:
//                                return "cast(" + value + " as date)";
//                            case F:
//                                return "cast(" + value + " as float)";
//                            case I1:
//                                return "cast(" + value + " as byteint)";
//                            case I2:
//                                return "cast(" + value + " as smallint)";
//                            case I8:
//                                return "cast(" + value + " as bigint)";
//                            case I:
//                                return "cast(" + value + " as integer)";
//                            case TS:
//                                return "cast(cast(" + value + " as date) as timestamp)";
//                            default:
//                                return "cast(" + value + " as char(" + column.columnLength + "))";
//                        }
//                    }
//                }) + ")";
//            }
//        }));
//        sb.append(BaseUtils.mergeWithSepEx(columnsInfo, ",", new BaseUtils.ProjectorToString<LisaDictColumnBean>() {
//            @Override
//            public String project(LisaDictColumnBean column) {
//                return "\"" + column.nameInDb + "\"";// + EQUALS + tConnection.valToString(record.get(column.nameInDb).getValue());
//            }
//        }));
//
//        sb.append(") select * from (");
//        String values = BaseUtils.mergeWithSepEx(lines, " union all ", new BaseUtils.ProjectorToString<Map<String, String>>() {
//
//            @Override
//            public String project(final Map<String, String> record) {
//                return "select * from (select " + BaseUtils.mergeWithSepEx(columnsInfo, ",", new BaseUtils.ProjectorToString<LisaDictColumnBean>() {
//                    @Override
//                    public String project(LisaDictColumnBean column) {
////                        Class<?> type = columnType.get(column.nameInDb);
//                        String value = record.get(column.nameInDb).trim();
//                        value = getCastPhrase(value, column);
//                        return value + " as " + "\"" + column.nameInDb + "\"";
//                    }
//
//                    private String getCastPhrase(String value, LisaDictColumnBean column) {
//                        if (LisaConstants.IS_NULLABLE.equals(column.nullable.trim()) && BaseUtils.isStrEmptyOrWhiteSpace(value)) {
//                            value = "null";
//                        } else {
//                            value = "'" + value + "'";
//                        }
//
//                        switch (LisaDataType.getValueFor(column.dataType)) {
//                            case AT:
//                                value = "cast(" + value + " as time)";
//                            case BF:
//                            case BV:
//                                return "TO_BYTE(cast(" + value + " as integer))";
//                            case D:
//                                return "cast(" + value + " as decimal(" + column.decimalTotalDigits + "," + column.decimalFractionalDigits + "))";
//                            case DA:
//                                return "cast(" + value + " as date)";
//                            case F:
//                                return "cast(" + value + " as float)";
//                            case I1:
//                                return "cast(" + value + " as byteint)";
//                            case I2:
//                                return "cast(" + value + " as smallint)";
//                            case I8:
//                                return "cast(" + value + " as bigint)";
//                            case I:
//                                return "cast(" + value + " as integer)";
//                            case TS:
//                                return "cast(cast(" + value + " as date) as timestamp)";
//                            default:
//                                return "cast(" + value + " as char(" + column.columnLength + "))";
//                        }
//                    }
//                }) + ") tmp" + (deliveredTableDumpId++);
//            }
//        });
//        sb.append(values).append(") tmp");
//        return LisaServerUtils.transcodeToLatin1(sb.toString());
//        return sb.toString();
//    }
    public Pair<String, String> updateDictionaryQuery(final Map<String, LisaDictionaryValue> record, String dictionaryName, List<LisaDictColumnBean> columns) {
        final String UPDATE_PREFIX = "update " + LisaUtils.getDbWithoutNameTable(dictionaryName, SCHEMA_NAME) + LisaUtils.getTableNameWithoutDB(dictionaryName) + " set ";
        final Map<String, LisaDictionaryValue> recordPkColumns = new HashMap<String, LisaDictionaryValue>();
        final Set<String> pkColumns = removePkColumns(columns);

        for (String pk : pkColumns) {
            recordPkColumns.put(pk, record.get(pk));
        }

        if (recordPkColumns.isEmpty()) {
            TeradataException e = new TeradataException(I18n.nieZdefinioKluczaGlownegoWTymSlo.get() /* i18n:  */);
            e.setIsBlocking(true);
            throw e;
        }

        StringBuilder sb = new StringBuilder(UPDATE_PREFIX);
        sb.append(BaseUtils.mergeWithSepEx(columns, ",", new BaseUtils.ProjectorToString<LisaDictColumnBean>() {
            @Override
            public String project(LisaDictColumnBean column) {
                Object value = record.get(LisaUtils.getTableNameWithoutDB(column.nameInDb)).getValue();
                return LisaUtils.getTableNameWithoutDB("\"" + column.nameInDb + "\"=") + (value == null ? "null" : tConnection.valToString(value));
            }
        }));

        sb.append(WHERE);
        // append where conditions, depending on is_main_key=1
        String logId = BaseUtils.mergeWithSepEx(recordPkColumns.entrySet(), AND, new BaseUtils.ProjectorToString<Map.Entry<String, LisaDictionaryValue>>() {
            @Override
            public String project(Map.Entry<String, LisaDictionaryValue> val) {
                return BaseUtils.safeEqualsConditionInSqlWhere("\"" + val.getKey() + "\"", tConnection.valToString(val.getValue().getValue()));
            }
        });

        sb.append(logId);
        return new Pair<String, String>(LisaServerUtils.transcodeToLatin1(sb.toString()), logId);
    }

    private Set<String> removePkColumns(final List<LisaDictColumnBean> columns) {
        final List<LisaDictColumnBean> toRemove = new ArrayList<LisaDictColumnBean>();
        Set<String> pkList = BaseUtils.projectToSet(columns, new BaseUtils.Projector<LisaDictColumnBean, String>() {
            @Override
            public String project(LisaDictColumnBean column) {
                if (column.isMainKey) {
                    toRemove.add(column);
                    return column.nameInDb;
                }
                return null;
            }
        }, true);
        columns.removeAll(toRemove);
        return pkList;
    }

    public String selectDictionaryQuery(Integer instanceId, LisaGridParamBean params, List<LisaDictColumnBean> columnList, LisaDictionaryBean dictData, Integer categoryObjId, String categorizationObjId) {

//        if (categoryObjId == null) {
//            throw new FoxyRuntimeException(I18n.bledyIdentyfikatorKategoryzacji.get());
//        }
        //final String DICT_ALIAS = "d.";
        Integer cnt = 0;
        final String DICT_ALIAS = " d0.";
        Map<String, Integer> stt = new HashMap<String, Integer>();
        stt.put(getFullQualifiedName(dictData.dbName), cnt++);
        StringBuilder columns = new StringBuilder();
        boolean isFirstCol = true;

        for (LisaDictColumnBean column : columnList) {
            StringBuilder s = new StringBuilder();

            if (column.dictValidation == null) {
                s = s.append(DICT_ALIAS).append('"').append(column.nameInDb).append('"');
            } else {
                stt.put(getFullQualifiedName(addInstanceId2DictValidation(instanceId, column.dictValidation)).toLowerCase(), cnt);
                String dictVal = "d" + cnt;
                cnt++;
                s = s.append(dictVal).append(".").append('"').append(column.textDisplayed).append('"');
                s.append(" as ").append('"').append(column.nameInDb).append('"');
            }

            if (isFirstCol) {
                columns.append(s);
                isFirstCol = false;
            } else {
                columns.append(", ").append(s);
            }
        }

        String sortColumn = columnList.get(0).nameInDb;
        if (params.sortColumnName != null) {
            sortColumn = params.sortColumnName;
        }

        StringBuilder innerQry = new StringBuilder();
        if (params.limit != 0) {
            innerQry.append(WITH_NO_LOCK + "select ").append(columns);//.append(", ROW_NUMBER() " + "over (order by " + DICT_ALIAS).append(sortColumn).append(") " + "as RowNum");
        } else {
            //PG musi być rzutowanie na bigint ponieważ w trybie ASCII zwraca bigint-a, a w tera int-a
            innerQry.append(WITH_NO_LOCK + "select cast(count(*) as bigint) ");
        }
        innerQry.append(" from ");
        innerQry.append(getFullQualifiedName(dictData.dbName)).append(" d0 ");
        for (LisaDictColumnBean colBean : columnList) {
            if (colBean.dictValidation != null) {
//                String dictVal = getFullQualifiedName(colBean.dictValidation).toLowerCase();
                String dictVal = getFullQualifiedName(addInstanceId2DictValidation(instanceId, colBean.dictValidation)).toLowerCase();
                innerQry.append(" left join ").append(dictVal).append(" d").append(stt.get(dictVal));
                innerQry.append(" on ").append(DICT_ALIAS).append('"').append(colBean.nameInDb).append('"').append("=");
                innerQry.append(" d").append(stt.get(dictVal)).append(".").append('"').append(colBean.colValidation).append('"');
            }
        }
        //if (params.limit != 0) {
        //innerQry.append(" d ");
        //}
        if (categoryObjId != null) {
            innerQry.append(" inner join ").append(LisaUtils.getDbWithoutNameTable(dictData.dbRelTable, SCHEMA_NAME))
                    .append(LisaUtils.getTableNameWithoutDB(dictData.dbRelTable))
                    .append(" " + "dr on dr.")
                    .append(dictData.columnRelName)
                    .append(" = " + DICT_ALIAS)
                    .append(dictData.columnRelName);
            innerQry.append(" where dr.id_kateg = ").append(BaseUtils.safeToString(categoryObjId));
            innerQry.append(" and dr.id_typ_kateg = ").append(BaseUtils.safeToString(categorizationObjId));
//            if (LisaUtils.hasDateColumns(columnList)) {
            // TF: ŹLE!! Sprawdzamy kolumny w tabeli z kategoriami a nie w tabeli słownikowej
            if (dictData.isVersioned == 1) {
                innerQry.append(" and dr.data_od <= current_date and dr.data_do >= current_date");
            }
//            }
        }
        if (!BaseUtils.isCollectionEmpty(params.infoToFiltr)) {
            innerQry.append(categoryObjId == null ? " where (" : " and (");
            innerQry.append(addCriteria("d0", params.infoToFiltr.get(0)));
            for (int i = 1; i < params.infoToFiltr.size(); i++) {
                innerQry.append(" and ");
                innerQry.append(addCriteria("d0", params.infoToFiltr.get(i)));
            }
            innerQry.append(")");
        }
        if (params.limit != 0) {
            innerQry.append(" qualify ROW_NUMBER() over (order by d0.").append(sortColumn);
            if (params.sortColumnName != null) {
                if (params.isAcsending) {
                    innerQry.append(" ASC");
                } else {
                    innerQry.append(" DESC");
                }
            }
            innerQry.append(") ");
            innerQry.append("between ").append(params.offset).append("+1 and ").append(params.offset + params.limit);
        }
        /*      StringBuilder outerQry = new StringBuilder();
         outerQry.append("select * from (");
         outerQry.append(innerQry);
         outerQry.append(") " + "as outer_table where outer_table.RowNum BETWEEN ").append(params.offset).append(" AND ").append(params.offset + params.limit);
         */
        return innerQry.toString();
    }

    public String getTeradataTableCheckQuery(String teradataTableName) {
        return "select top 1 1 from "
                + LisaUtils.getDbWithoutNameTable(teradataTableName, SCHEMA_NAME)
                + LisaUtils.getTableNameWithoutDB(teradataTableName);
    }

    public String getTeradataConnectionCheckQuery() {
        return "select current_date;";
    }

    public String getTeradataRefColCheckQuery(String dbRelTable, String columnRelName) {
        return "select top 1 " + columnRelName + " from "
                + LisaUtils.getDbWithoutNameTable(dbRelTable, SCHEMA_NAME)
                + LisaUtils.getTableNameWithoutDB(dbRelTable);
    }

    public String checkLisaUnassignedCategoryQuery(String systemUserLogin) {
        String lisaUser = systemUserLogin;
        // TF - wywalam poniższe, już niepotrzebne :)
//        if (systemUserLogin.toLowerCase().contains("pawel.gajda")) {
//            lisaUser = "jerzy.paryska"; //w celach Developerskich namierzenia problemu
//        }
//        if (systemUserLogin.toLowerCase().contains("pgajda")) {
//            lisaUser = "pawel.gajda"; //w celach Developerskich namierzenia problemu
//        }
        return "select "
                + "rapd.Nazwa_baza dbName,"
                + "rapd.Nazwa_obiekt dictionary,"
                + "rapd.warunek_where condition,"
                + "u.Imie userName,"
                + "u.Nazwisko userSurname,"
                + "u.Email email,"
                + "rap.wynik as counter "
                + "from "
                + "VD00_Meta.V0050_Raport rap "
                + "join VD00_Meta.V0051_Raport_def rapd on rap.id_raport_def = rapd.id_raport_def "
                + "join VD00_Meta.V0054_Uzytkownik_raport_def  urd on urd.id_raport_def = rapd.id_raport_def "
                + "join VD00_Meta.V0053_Uzytkownik u on u.id_uzytkownik = urd.id_uzytkownik "
                + "where rap.data_generacji >=(current_date-1) "
                + "and urd.data_do ='3000-12-31' "
                + "and  u.Email like " + tConnection.valToString(lisaUser + "%") + ";";
    }

    public String getProcessStatusQuery(Collection<String> processNameList) {
        StringBuilder processNames = new StringBuilder();

        if (processNameList.isEmpty()) {
            processNames.append(" ''");
        } else {
            for (String procName : processNameList) {
                processNames.append(",").append(tConnection.valToString(procName));
            }
        }

        String query = "select all_tab.symb_proc as symbProc, all_tab.stan_proc as stanProc , all_tab.czas_zlec as czasZlec from "
                + "("
                + "select symb_proc, max(id_proc) as max_id_proc "
                + "from VD_US_META_PROC.Proces "
                //+ "from DB00_META.T0036_proces "
                + "where symb_proc in ("
                + processNames.substring(1)
                + ") "
                + "group by symb_proc "
                + ") as max_tab "
                + "left join "
                + "( "
                //+ "select * from DB00_META.T0036_proces "
                + "select * from VD_US_META_PROC.Proces "
                + ") as all_tab "
                + "on all_tab.symb_proc  = max_tab.symb_proc "
                + "and all_tab.id_proc  = max_tab.max_id_proc ";

        return query;
    }

    private String addCriteria(String dbName, LisaFiltrColumnValueBase columnValue) {
        StringBuilder res = new StringBuilder();
        res.append("(").append(dbName).append(".").append(columnValue.nameInDb);
        if (columnValue instanceof LisaFiltrColumnValueSingle) {
            LisaDictionaryValue value = ((LisaFiltrColumnValueSingle) columnValue).value;
            switch (((LisaFiltrColumnValueSingle) columnValue).kind) {
                case LIKE:
                    res.append(" like '%").append(value.getValue()).append("%'");
                    break;
                case EQUAL:
                    res.append(" = ").append(tConnection.valToString(value.getValue()));
                    break;
                case GREATER:
                    res.append(" >= ").append(tConnection.valToString(value.getValue()));
                    break;
                case SMALLER:
                    res.append(" <= ").append(tConnection.valToString(value.getValue()));
                    break;
            }
        } else {
            res.append(" between ").append(
                    tConnection.valToString(((LisaFiltrColumnValueBetween) columnValue).fromValue.getValue())
            ).append(" and ").append(
                    tConnection.valToString(((LisaFiltrColumnValueBetween) columnValue).toValue.getValue())
            );
        }
        res.append(")");
        return res.toString();
    }

    public String getDistinctColumsValueQuery(Integer instanceId, LisaDictColumnBean bean, String value, int numberFetchItem, boolean isStrictlyEquals) {
        //maxLength = dlugosc stringu w Teradata
        int maxLength = 10000;
//        if (bean.dataType.equals(LisaDataType.CF.toString()) || bean.dataType.equals(LisaDataType.CV.toString())) {
//            maxLength = bean.columnLength;
//        }
        StringBuilder sb = new StringBuilder();
        String orgDict = getFullQualifiedName(bean.dictionaryName);
        String refDict = getFullQualifiedName(addInstanceId2DictValidation(instanceId, bean.dictValidation));

        sb.append(WITH_NO_LOCK + "select" + (numberFetchItem > 0 ? " top " + numberFetchItem : "")).append(" cast(r.").append(bean.textDisplayed).append(" as varchar(").append(maxLength).append(")) as ").append("obj_id");
        sb.append(", cast(r.").append(bean.textDisplayed).append(" as varchar(").append(maxLength).append(")) as ").append(bean.textDisplayed);
        sb.append(" from ").append(refDict).append(" r ");
//        sb.append(" from ").append(orgDict).append(" o join ").append(refDict).append(" r ");
//        sb.append(" on ").append(" o.").append(bean.nameInDb).append(" = ").append(" r.").append(bean.colValidation);

        if (!BaseUtils.isStrEmpty(value)/* value == null*/) {
            sb.append(" where ").append("obj_id ");
//            sb.append("is null");
//        } else
            if (!isStrictlyEquals) {
                sb.append("like '").append(value).append("%'");
            } else {
                sb.append(" = '").append(value).append("'");
            }
        }
//        sb.append(" group by obj_id");
//        sb.append(", r.").append(bean.textDisplayed);
//        sb.append(" order by obj_id");
        //System.out.println(sb.toString());
        return sb.toString();
    }

    public String checkUniqueColumnQuery(String dictName, String colName) {
        String s = "select " + colName + ", count(*) from " + LisaUtils.getDbWithoutNameTable(dictName, SCHEMA_NAME) + LisaUtils.getTableNameWithoutDB(dictName) + " group by " + colName + " having count(*) > 1";
        //System.out.println(s);
        return s;
    }

    private String getFullQualifiedName(String dbName) {
        return (LisaUtils.getDbWithoutNameTable(dbName, SCHEMA_NAME) + LisaUtils.getTableNameWithoutDB(dbName)).toLowerCase();
    }

    public String originalValueFromReferenceQuery(Integer instanceId, LisaDictColumnBean columnBean, String value) {
        StringBuilder sb = new StringBuilder();
        String orgDict = getFullQualifiedName(columnBean.dictionaryName);
        String refDict = getFullQualifiedName(addInstanceId2DictValidation(instanceId, columnBean.dictValidation));
//        sb.append("select top 1 o.").append(columnBean.nameInDb).append(" from ").append(orgDict);
        sb.append("select top 1 r.").append(columnBean.colValidation).append(" from ").append(refDict).append(" r");
//        sb.append(" o join ").append(refDict).append(" r");
//        sb.append(" on o.").append(columnBean.nameInDb).append(" = ").append("r.").append(columnBean.colValidation);
        sb.append(" where ").append("r.").append(columnBean.textDisplayed).append(" = '").append(value).append("'");
        return sb.toString();
    }

    public String countNumberOfDictionaryRecordsQuery(String dbName) {
        return WITH_NO_LOCK + "select cast(count(1) as bigint) from " + getFullQualifiedName(dbName);
    }

    public String getDictionaryRecordsQuery(String tableName, List<LisaDictColumnBean> columns, int offset, Integer limit) {
        StringBuilder sb = new StringBuilder(WITH_NO_LOCK + "select ");
        sb.append(BaseUtils.mergeWithSepEx(columns, ",", new BaseUtils.ProjectorToString<LisaDictColumnBean>() {
            @Override
            public String project(LisaDictColumnBean column) {
                return "\"" + column.nameInDb + "\"";
            }
        }));
        sb.append(" from ").append(getFullQualifiedName(tableName))
                .append(" qualify ROW_NUMBER() over (order by ").append(columns.get(0).nameInDb)
                .append(" ) between ").append(offset).append("+1 and ").append(offset + limit);
        return sb.toString();
    }

    public String insertRecordsPreparedStatment(String tableName, List<LisaDictColumnBean> columnsList) {
        return "insert into " + getFullQualifiedName(tableName)
                + " ( " + BaseUtils.mergeWithSepEx(columnsList, ",", new BaseUtils.ProjectorToString<LisaDictColumnBean>() {
                    @Override
                    public String project(LisaDictColumnBean column) {
                        return "\"" + column.nameInDb + "\"";// + EQUALS + tConnection.valToString(record.get(column.nameInDb).getValue());
                    }
                }) + ") values ("
                + BaseUtils.mergeWithSepEx(columnsList, ",", new BaseUtils.ProjectorToString<LisaDictColumnBean>() {
                    @Override
                    public String project(LisaDictColumnBean column) {
//                        switch (LisaDataType.getValueFor(column.dataType)) {
//                            case AT:
//                                return "cast(? as time)";
//                            case BF:
//                            case BV:
//                                return "TO_BYTE(cast(? as integer))";
//                            case D:
//                                return "cast(? as decimal(" + column.decimalTotalDigits + "," + column.decimalFractionalDigits + "))";
//                            case DA:
//                                return "cast(? as date)";
//                            case F:
//                                return "cast(? as float)";
//                            case I1:
//                                return "cast(? as byteint)";
//                            case I2:
//                                return "cast(? as smallint)";
//                            case I8:
//                                return "cast(? as bigint)";
//                            case I:
//                                return "cast(? as integer)";
//                            case TS:
//                                return "cast(cast(? as date) as timestamp)";
//                            default:
//                                return "cast(? as char(" + column.columnLength + "))";
//                        }
                        return "?";
                    }
                }) + ")";
    }

    public String getLockTableQuery(String tableName) {
        return "LOCK TABLE " + getFullQualifiedName(tableName) + " FOR WRITE";
    }

    public String getSelectPKQuery(String tableName, List<LisaDictColumnBean> indexColumnsNames) {
        return WITH_NO_LOCK + "select 1 from " + getFullQualifiedName(tableName)
                + " where (" + BaseUtils.mergeWithSepEx(indexColumnsNames, " AND ", new BaseUtils.Projector<LisaDictColumnBean, String>() {

                    @Override
                    public String project(LisaDictColumnBean column) {
                        return "\"" + column.nameInDb + "\"=?";
                    }
                }) + ")";
    }

    public String getDictionaryIndexiesQuery(String tableName, List<LisaDictColumnBean> indexColumns, int offset, Integer limit) {
        StringBuilder sb = new StringBuilder(WITH_NO_LOCK + "select ");
        sb.append(BaseUtils.mergeWithSepEx(indexColumns, ",", new BaseUtils.ProjectorToString<LisaDictColumnBean>() {
            @Override
            public String project(LisaDictColumnBean column) {
                return "\"" + column.nameInDb + "\"";
            }
        }));
        sb.append(" from ").append(getFullQualifiedName(tableName))
                .append(" qualify ROW_NUMBER() over (order by ").append(indexColumns.get(0).nameInDb)
                .append(" ) between ").append(offset).append("+1 and ").append(offset + limit);
        return sb.toString();
    }

    public String updateRecordPreparedStatement(String tableName, List<LisaDictColumnBean> columnsList, List<LisaDictColumnBean> pkColumns) {
        List<LisaDictColumnBean> updateColumns = LisaUtils.extractNonPkColumns(columnsList);
        return "update " + getFullQualifiedName(tableName) + " set "
                + BaseUtils.mergeWithSepEx(updateColumns, ",", new BaseUtils.Projector<LisaDictColumnBean, String>() {

                    @Override
                    public String project(LisaDictColumnBean column) {
                        return "\"" + column.nameInDb + "\"=?";
                    }
                }) + " where ("
                + BaseUtils.mergeWithSepEx(pkColumns, " AND ", new BaseUtils.Projector<LisaDictColumnBean, String>() {

                    @Override
                    public String project(LisaDictColumnBean column) {
                        return "(coalesce(\"" + column.nameInDb + "\", null) = coalesce(?, null) or (\"" + column.nameInDb + "\" is null and ? is null))";
                    }
                }) + ")";
    }

    public String getDictionaryPkValuesPreparedStatement(String tableName, List<LisaDictColumnBean> pkColumns) {
        return "select "
                + BaseUtils.mergeWithSepEx(pkColumns, ",", new BaseUtils.Projector<LisaDictColumnBean, String>() {

                    @Override
                    public String project(LisaDictColumnBean column) {
                        return "\"" + column.nameInDb + "\"";

                    }
                }) + " from " + getFullQualifiedName(tableName)
                + " qualify ROW_NUMBER() over (order by " + pkColumns.get(0).nameInDb + " ) between ? and ?";
    }

    public String deleteRecordPreparedStatement(String tableName, List<LisaDictColumnBean> pkColumnsList) {
        return "delete " + getFullQualifiedName(tableName) + " where "
                + BaseUtils.mergeWithSepEx(pkColumnsList, " AND ", new BaseUtils.Projector<LisaDictColumnBean, String>() {

                    @Override
                    public String project(LisaDictColumnBean column) {
                        return "(\"" + column.nameInDb + "\"=?)";
                    }
                });
    }

    public String mergeRecordPreparedStatement(String tableName, List<LisaDictColumnBean> columnsList, List<LisaDictColumnBean> pkColumnsList) {
        List<LisaDictColumnBean> notPkColumnsList = new ArrayList<LisaDictColumnBean>();
        for (LisaDictColumnBean column : columnsList) {
            if (!column.isMainKey) {
                notPkColumnsList.add(column);
            }
        }
        String preparedSql = "merge " + getFullQualifiedName(tableName) + " as tar using (select "
                + BaseUtils.mergeWithSepEx(columnsList, ",", new BaseUtils.Projector<LisaDictColumnBean, String>() {

                    @Override
                    public String project(LisaDictColumnBean column) {
                        return "? as \"" + column.nameInDb + "\"";
                    }
                }) + ") as src on ("
                + BaseUtils.mergeWithSepEx(pkColumnsList, " AND ", new BaseUtils.Projector<LisaDictColumnBean, String>() {

                    @Override
                    public String project(LisaDictColumnBean column) {
                        return "(tar.\"" + column.nameInDb + "\" = src.\"" + column.nameInDb + "\")";
                    }
                }) + ") when not matched then insert ("
                + BaseUtils.mergeWithSepEx(columnsList, ",", new BaseUtils.Projector<LisaDictColumnBean, String>() {

                    @Override
                    public String project(LisaDictColumnBean column) {
                        return "\"" + column.nameInDb + "\"";
                    }
                }) + ") values ("
                + BaseUtils.mergeWithSepEx(columnsList, ",", new BaseUtils.Projector<LisaDictColumnBean, String>() {

                    @Override
                    public String project(LisaDictColumnBean column) {
                        return "src.\"" + column.nameInDb + "\"";
                    }
                }) + ") when matched then update set "
                + BaseUtils.mergeWithSepEx(notPkColumnsList, ",", new BaseUtils.Projector<LisaDictColumnBean, String>() {

                    @Override
                    public String project(LisaDictColumnBean column) {
                        return "\"" + column.nameInDb + "\" = src.\"" + column.nameInDb + "\"";
                    }
                });
        return preparedSql;
    }

    public String checkTableHasColumn(String tableName, String columnName) {
        return "select 1 from dbc.columns dc where dc.DatabaseName='" + LisaUtils.getDbWithoutNameTable(tableName, SCHEMA_NAME).replace(".", "") + "' and dc.TableName='" + LisaUtils.getTableNameWithoutDB(tableName) + "' and lower(dc.columnName) = '" + columnName + "';";
    }

    private String addInstanceId2DictValidation(Integer instanceId, String dictValidation) {
        return BaseUtils.isStrEmptyOrWhiteSpace(dictValidation) ? dictValidation : "" + instanceId + "." + dictValidation;
    }
}
