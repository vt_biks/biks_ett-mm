/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import commonlib.IRequestContext;
import commonlib.LameUtils;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.trzy0.foxy.serverlogic.FoxyUrlRewriter;
import simplelib.MapHelper;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class FoxyHttpModelRequestContext implements IRequestContext, IHttpResponseAware {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    private Map<String, Object> paramsAsMap;
    private String subLocal;
    private String subRemote;
    private String rubLocal;
    private String rubRemote;
    private String method;
    private HttpServletResponse response;
    private FoxyUrlRewriter urlRewriter;
    //private MapHelper<String> fullParams;
    private boolean usesSubdomains;

    public FoxyHttpModelRequestContext(HttpServletRequest request,
            //HttpServletResponse response,
            MapHelper<String> fullParams,
            FoxyUrlRewriter urlRewriter,
            boolean usesSubdomains) {
        this.paramsAsMap = fullParams.helperAsMap();
        this.subLocal = FoxyServletUtils.getLocalServletUrl(request);
        this.subRemote = FoxyServletUtils.getRemoteServletUrl(request);
        this.rubLocal = FoxyServletUtils.getLocalWebAppUrl(request);
        this.rubRemote = FoxyServletUtils.getRemoteWebAppUrl(request);
        this.method = request.getMethod();
        this.urlRewriter = urlRewriter;
        this.usesSubdomains = usesSubdomains;

        //this.response = response;
        // this.fullParams = fullParams;
    }

    public String getRequestMethod() {
        return //request.getMethod();
                method;
    }

    public String encodeUrl(String url) {
        String rewritten = urlRewriter == null ? url : urlRewriter.rewriteOutgoingUrl(url);

        String res = rewritten; //response.encodeURL(rewritten);

        if (logger.isDebugEnabled()) {
            logger.debug("$$$$$$$$$$$$$$$$ encode url: old=" + url + ", new=" + res
                    + ", " + (urlRewriter == null ? "no urlRewriter" : "rewritten was: " + rewritten));
        }

        return res;
    }

    public String getServletUrlBase(boolean forRemoteURL) {
        return forRemoteURL ? //FoxyServletUtils.getRemoteWebAppUrl(request)
                //request.getRequestURL().toString()
                //FoxyServletUtils.getRemoteServletUrl(request)
                subRemote
                : //request.getRequestURI();
                //FoxyServletUtils.getLocalServletUrl(request)
                subLocal;
    }

    public Map<String, Object> getParams() {
        return paramsAsMap;
    }

    public String getResourceUrlBase(boolean forRemoteURL) {
        return forRemoteURL
                ? //FoxyServletUtils.getRemoteWebAppUrl(request)
                rubRemote
                : //FoxyServletUtils.getLocalWebAppUrl(request);
                rubLocal;
    }

    public boolean getUsesSubdomains() {
        return usesSubdomains;
    }

    public void setHttpResponse(HttpServletResponse response) {
        this.response = response;
    }
}
