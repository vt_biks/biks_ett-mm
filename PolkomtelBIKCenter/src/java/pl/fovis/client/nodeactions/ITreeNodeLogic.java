/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.common.TreeNodeBean;

/**
 *
 * @author wezyr
 */
public interface ITreeNodeLogic {

    public boolean canPasteHere(TreeNodeBean target, TreeNodeBean source, boolean isCut);

    public boolean canCopyOrCut(TreeNodeBean source, boolean isCut);
}
