exec sp_check_version '1.1.9.16';
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

--ww: poprzednia wersja w v1.0.6
-- zmiana - print tylko gdy @diags_level > 0, doda�em tak� zmienn�,
-- bo printy by� wcze�niej bezwarunkowo (bez sensu)
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_node_init_branch_id]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_node_init_branch_id]
GO

create procedure sp_node_init_branch_id (@opt_tree_id int, @opt_node_id int)
as
begin  
  set nocount on

  declare @diags_level int = 0;


	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #1'

  create table #ids (id int not null primary key);
  
  insert into #ids (id) 
  select ID from bik_node 
  where 
    (@opt_tree_id is null or tree_id = @opt_tree_id)
    and 
    (  @opt_node_id is null and parent_node_id is null 
    or 
       @opt_node_id is not null and id = @opt_node_id
    )
   ;

	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #2'
    
  create table #child_ids (id int not null primary key);
    
  while exists (select 1 from #ids)
  begin
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #3'
    
    update bik_node
    set branch_ids = coalesce(parent.branch_ids, '') + CAST(bik_node.id as varchar(10)) + '|'
    from bik_node inner join #ids on bik_node.id = #ids.id
      left join bik_node as parent on parent.id = bik_node.parent_node_id;    

	/*
    update bik_node
    set branch_ids = coalesce((select parent.branch_ids from bik_node as parent where parent.id = bik_node.parent_node_id), '')
    + CAST(id as varchar(10)) + '|'
    where bik_node.id in (select id from #ids);
    */
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #4'

    truncate table #child_ids;
    
    insert into #child_ids (id)
    select ID from bik_node where parent_node_id in (select id from #ids);
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #5'

    truncate table #ids;
    
    insert into #ids (id) select ID from #child_ids;

	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #6'    
  end
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #7'

  drop table #ids;
  drop table #child_ids;
end
go


------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------
--ww: poprzednia wersja w alter db for v1.0.87.2 ww.sql
-- dodajemy visual_order - dorzucamy na ko�cu
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_add_menu_node]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_add_menu_node]
GO

create procedure [dbo].[sp_add_menu_node](@parent_obj_id varchar(255), @name varchar(255), @obj_id varchar(255))
as
begin
  declare @parent_node_id int = dbo.fn_node_id_by_tree_code_and_obj_id('TreeOfTrees', @parent_obj_id)
  declare @tree_of_trees_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
  declare @visual_order int = coalesce((select max(visual_order) from bik_node where parent_node_id = @parent_node_id and
    is_deleted = 0 and tree_id = @tree_of_trees_id), 0) + 1

  insert into bik_node (parent_node_id, node_kind_id, tree_id, name, obj_id, visual_order)
  values (@parent_node_id, dbo.fn_node_kind_id_by_code('MenuItem'), @tree_of_trees_id, @name, @obj_id, @visual_order)
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_fix_nodes_visual_order_ex]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_fix_nodes_visual_order_ex
go

create procedure sp_fix_nodes_visual_order_ex(@parent_id int, @tree_id int, @ignore_nodekindcodes_in_larch int) as
begin
  set nocount on

  declare @vofix table (id int not null primary key, visual_order_fix int not null)

  insert into @vofix (id, visual_order_fix)
  select id, (ROW_NUMBER() OVER (ORDER BY visual_order, name)) - 1 AS visual_order_fix --, * 
  from 
  bik_node
  where tree_id = @tree_id and is_deleted = 0 and (@parent_id is null and parent_node_id is null or @parent_id is not null and parent_node_id = @parent_id)
  and (@ignore_nodekindcodes_in_larch = 0 or obj_id not like '&%')
  
  update 
  bik_node
  set visual_order = visual_order_fix
  from @vofix v
  where v.id = bik_node.id
  and bik_node.tree_id = @tree_id and bik_node.is_deleted = 0 and (@parent_id is null and bik_node.parent_node_id is null or @parent_id is not null and bik_node.parent_node_id = @parent_id)
  and (@ignore_nodekindcodes_in_larch = 0 or obj_id not like '&%')
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

--ww: poprzednia wersja w alter db for v1.1.4.1 ww.sql
-- dodajemy opcj� EX - mo�liwo�� ignorowania w�z��w nodeKindowych dla menuItems�w (Modrzew, TreeOfTrees)
-- patrz wy�ej (sp_fix_nodes_visual_order_ex)
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_fix_nodes_visual_order]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_fix_nodes_visual_order
go

create procedure sp_fix_nodes_visual_order(@parent_id int, @tree_id int) as
begin
  set nocount on
  exec sp_fix_nodes_visual_order_ex @parent_id, @tree_id, 0
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_set_node_visual_order_ex]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_set_node_visual_order_ex
go

create procedure sp_set_node_visual_order_ex(@node_id int, @visual_order int, @ignore_nodekindcodes_in_larch int) as
begin
  set nocount on

  declare @parent_id int, @tree_id int

  select @parent_id = parent_node_id, @tree_id = tree_id from bik_node where id = @node_id

  exec sp_fix_nodes_visual_order_ex @parent_id, @tree_id, @ignore_nodekindcodes_in_larch
  
  update bik_node
  set visual_order = visual_order + 1
  where tree_id = @tree_id and is_deleted = 0 and (@parent_id is null and parent_node_id is null or @parent_id is not null and parent_node_id = @parent_id)
    and visual_order >= @visual_order
  and (@ignore_nodekindcodes_in_larch = 0 or obj_id not like '&%')
    
  update bik_node set visual_order = @visual_order where id = @node_id  
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

--ww: poprzednia wersja w alter db for v1.1.4.1 ww.sql
-- dodajemy opcj� EX - mo�liwo�� ignorowania w�z��w nodeKindowych dla menuItems�w (Modrzew, TreeOfTrees)
-- patrz wy�ej (sp_set_node_visual_order_ex)
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_set_node_visual_order]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_set_node_visual_order
go

create procedure sp_set_node_visual_order(@node_id int, @visual_order int) as
begin
  set nocount on
  exec sp_set_node_visual_order_ex @node_id, @visual_order, 0
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

update bik_tree set code = 'UserRoles', tree_kind = 'UserRoles' 
where code = 'UsersRoles'
go

declare @treeOfTreesId int = (select id from bik_tree where code = 'TreeOfTrees')

update bik_node set obj_id = '$UserRoles'
where tree_id = @treeOfTreesId and obj_id = '$UsersRoles'

update bik_node set obj_id = '@Users'
where tree_id = @treeOfTreesId and obj_id = '$Users'
go

update bik_tree set name = 'Grupy u�ytkownik�w' where code = 'Users'
go

declare @treeOfTreesId int = (select id from bik_tree where code = 'TreeOfTrees')
update bik_node set name = 'Drzewa dynamiczne' where tree_id = @treeOfTreesId and obj_id = '#admin:dict:trees'
go

exec sp_add_menu_node 'community', '@', '$Users'

declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
declare @menuNodeId int = (select id from bik_node where tree_id = @tree_id and obj_id = '$Users')        

exec sp_node_init_branch_id @tree_id, @menuNodeId
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_update_version '1.1.9.16', '1.1.9.17';
go
