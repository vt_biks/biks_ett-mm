/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.JoinedObjBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author beata
 */
public class JoinedObjBeanExtractor extends NodeAwareBeanWithAttributeExtractorBase<JoinedObjBean> {

    protected int currentNodeId;

    public JoinedObjBeanExtractor(boolean isAddJoinedButtonEnabled, int currentNodeId) {
        super(isAddJoinedButtonEnabled);
        this.currentNodeId = currentNodeId;
    }

    public Integer getNodeId(JoinedObjBean b) {
        return b.dstId;
    }

    public String getTreeCode(JoinedObjBean b) {
        return b.tabId;
    }

    protected String getName(JoinedObjBean b) {
        return b.dstName;
    }

    protected String getNodeKindCode(JoinedObjBean b) {
        return b.dstCode;
    }

    @Override
    public boolean hasChildren(JoinedObjBean b) {
        return b.dstHasChildren;
    }

    public String getBranchIds(JoinedObjBean b) {
        return b.branchIds;
    }

    @Override
    protected void execActDelete(JoinedObjBean b) {
        int nodeId = b.srcId;
        BIKClientSingletons.getService().deleteBikLinkedMeta(nodeId, getNodeId(b), getRefreshingCallback(nodeId));
        deleteBeanFromList(b, I18n.usunietoPowiazanie.get() /* I18N:  */);
        //refreshView("Usunięto powiązanie");
    }

    @Override
    public boolean isDeleteBtnVisible2Wtf(JoinedObjBean b) {
        return !b.type && b.srcId == currentNodeId || b.similarityScore != null;
    }

    @Override
    protected boolean hasInheritedMarkByName(JoinedObjBean b) {
        return b.inheritToDescendants;
    }

    @Override
    public Object getAddColVal(JoinedObjBean b, int addColIdx) {
        return BIKClientSingletons.isDifferentViewType() ? getDescription(b.descrPlain)
                : b.isFromHyperlink ? BaseUtils.capitalize(I18n.atrybut.get()) + ": " + b.attributes : b.attributes;
    }

    @Override
    public int getAddColCnt() {
        return BIKClientSingletons.isDescriptionInJoinedPanelVisible() ? 1 : 0;
    }

    @Override
    public Double getOptSimilarityScore(JoinedObjBean b) {
        return b.similarityScore;
    }

    @Override
    protected Integer getCurrentNodeId() {
        return currentNodeId;
    }

    @Override
    public Integer getJoinedObjId(JoinedObjBean b) {
        return b.id;
    }

    @Override
    protected void setAttributes(JoinedObjBean b, String value) {
        b.attributes = value;
    }

    @Override
    public boolean isActDeleteBtnEnabled(JoinedObjBean b) {
        return (isEffectiveExpertLoggedInEx(b) || BIKClientSingletons.isBranchAuthorLoggedIn(getBranchIds(b)) || BIKClientSingletons.isBranchCreatorLoggedIn(getBranchIds(b))
                || BIKClientSingletons.isLoggedUserCreatorOfTree(BIKClientSingletons.getTreeIdOfCurrentTab())
                || BIKClientSingletons.isLoggedUserAuthorOfTree(BIKClientSingletons.getTreeIdOfCurrentTab()))
                && isDeleteBtnEnabled(b);

        // return (isEffectiveExpertLoggedInEx(b) || BIKClientSingletons.isBranchAuthorLoggedin(getBranchIds(b))) && isDeleteBtnEnabled(b);
    }

    protected boolean isFromHyperlink(JoinedObjBean b) {
        return b.isFromHyperlink;
    }
}
