/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CellPanel;
import java.util.List;
import pl.bssg.metadatapump.common.ConnectionParametersDynamicAxBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public class AdminConfigDynamicAxWidget extends AdminConfigManyServersBase<List<ConnectionParametersDynamicAxBean>, ConnectionParametersDynamicAxBean> {

    @Override
    protected void activateForServer(String id) {
    }

    @Override
    protected void buildRightWidget(CellPanel panelRight) {

    }

    @Override
    protected void callServiceToAddNewServer(String source, String serverName, boolean needSeparateSchedule, AsyncCallback<ConnectionParametersDynamicAxBean> asyncCallback) {
        BIKClientSingletons.getService().addDynamicAxConfig(serverName, asyncCallback);
    }

    @Override
    protected void callServiceToDeleteServer(int id, String serverName, AsyncCallback<Void> asyncCallback) {
    }

    @Override
    protected void callServiceToTestConnection(int id, AsyncCallback<Pair<Integer, String>> asyncCallback) {
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_DYNAMIC_AX;
    }
}
