/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.MenuExtender.MenuBottomLevel;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;
import pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase;
import pl.fovis.foxygwtcommons.wezyrtreegrid.FoxyWezyrTreeGrid;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author beata
 */
public abstract class TreeAndNodeKindTreeBase {

    protected ITreeRowAndBeanStorage<Integer, Map<String, Object>, MenuNodeBean> storage = new MapTreeRowAndBeanStorageBase<Integer, MenuNodeBean>(
            "id" /* I18N: no */, "parentId", null) {
                @Override
                protected Map<String, Object> convertBeanToRow(final MenuNodeBean mnb) {
                    return TreeAndNodeKindTreeBase.this.convertBeanToRow(mnb);
                }
            };

    //public int nodeKindId = 0;
    protected FoxyWezyrTreeGrid<Integer> tree;

    protected abstract boolean isExpandAllNodes();

    //ww: metoda użyta w wywołaniu MenuExtender.prepareMenuNodes, wrzucona do
    // metody, aby było możliwe nadpisanie w podklasie
    protected boolean expandDynamicTreesInMenu() {
        return true;
    }

    //ww: metoda użyta w wywołaniu MenuExtender.prepareMenuNodes, wrzucona do
    // metody, aby było możliwe nadpisanie w podklasie
    protected MenuBottomLevel whatToKeepAsLeavesInMenuTree() {
        return MenuBottomLevel.NodeKinds;
    }

    protected boolean mustAddUltimateRootToMenuTree() {
        return true;
    }

    protected boolean mustHideReduntantGroups() {
        return false;
    }

    protected List<MenuNodeBean> getMenuNodes() {
        return BIKClientSingletons.getMenuNodes();
    }

    protected Collection<TreeBean> getTrees() {
        return BIKClientSingletons.getTrees();
    }

    protected Map<String, NodeKindBean> getNodeKindsByCodeMap() {
        return BIKClientSingletons.getNodeKindsByCodeMap();
    }

    protected List<MenuNodeBean> extendMenuNodes() {
        return BIKClientSingletons.getExtendedMenuNodes(
                getMenuNodes(), getTrees(), getNodeKindsByCodeMap(),
                expandDynamicTreesInMenu(), whatToKeepAsLeavesInMenuTree(),
                mustAddUltimateRootToMenuTree(), mustHideReduntantGroups());
    }

    protected List<String> getFieldsToDisplay() {
        return BaseUtils.arrayToArrayList("name=" /* i18n: no */ + I18n.nazwa.get() /* I18N:  */);
    }

    public Widget buildWidgets() {
        tree = new FoxyWezyrTreeGrid<Integer>();
        tree.setFirstColStyleName("wwNewTreeSearch wwNewTree");

        tree.defineTreeFields("id" /* I18N: no */, "parentId", "name" /* I18N: no */, "prettyKind", "icon" /* I18N: no */, null);

        List<String> ftd = getFieldsToDisplay();
        final int ftdCnt = ftd.size();
        String[] ftda = new String[ftdCnt];
        for (int i = 0; i < ftdCnt; i++) {
            ftda[i] = ftd.get(i);
        }
        tree.setFields(ftda);

        List<Integer> ids = new ArrayList<Integer>();

        List<MenuNodeBean> menuItems = extendMenuNodes();

        for (MenuNodeBean mnb : menuItems) {
            //if (!BaseUtils.safeStartsWith(mnb.code, "#")) {
            storage.registerAndConvertBeanToRow(mnb);
            //System.out.println(mnb.code);
            if ((mnb.treeId == null && mnb.nodeKindId == null) || isExpandAllNodes()) {
                ids.add(mnb.id);
            }
            //}
        }

        tree.setData(storage, null);

        tree.expandNodesByIds(ids, null);

        tree.addSelectionChangedHandler(new IParametrizedContinuation<Map<String, Object>>() {
            public void doIt(Map<String, Object> param) {
                selectionChangedWholeLine(param);
            }
        });

        Widget treeWidget = tree.buildWidget();

        return treeWidget;
//        treeScrollPnl.add(treeWidget);
    }

    protected void selectionChanged() {
        // no-op
    }

    protected abstract Map<String, Object> convertBeanToRow(final MenuNodeBean mnb);

    public void selectRecord(Integer id) {
        tree.selectRecord(id);
    }

    public Integer getIdOfSelectedRecord() {
        return tree.getIdOfSelectedRecord();
    }

    public MenuNodeBean getMenuNodeBeanById(int id) {
        return storage.getBeanById(id);
    }

    protected void selectionChangedWholeLine(Map<String, Object> param) {
        // no-op
    }

    //ww: przydatne w niektórych podklasach lub użyciach
    public Integer getSelectedRecordIndexInSiblings() {
        Integer selectedId = getIdOfSelectedRecord();
        return selectedId == null ? null : tree.getNodeIndexInSiblings(selectedId);
    }

    public Integer getParentIdOfNode(int nodeId) {
        return tree.getParentIdOfNode(nodeId);
    }
}
