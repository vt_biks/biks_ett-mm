/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import pl.fovis.common.TreeNodeBean;

/**
 *
 * @author tflorczak
 */
public class TreeNodeBean4TreeExtractor implements IBean4TreeExtractor<TreeNodeBean, Integer> {

    public Integer extractId(TreeNodeBean b) {
        return b.id;
    }

    public Integer extractParentId(TreeNodeBean b) {
        return b.parentNodeId;
    }

    public String extractName(TreeNodeBean b) {
        return b.name;
    }

    public boolean extractHasNoChildren(TreeNodeBean b) {
        return b.hasNoChildren;
    }

    public int extractVisualOrder(TreeNodeBean b) {
        return b.visualOrder;
    }

    public boolean extractIsFolder(TreeNodeBean b) {
        return b.isFolder;
    }

    public String extractBranchIds(TreeNodeBean b) {
        return b.branchIds;
    }
}
