/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.lisa;

import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import pl.fovis.common.lisa.LisaConstants;
import simplelib.BaseUtils;

/**
 *
 * @author pgajda
 */
public class LisaServerUtils {

    public static SimpleDateFormat LISA_DATE_FORMAT = new SimpleDateFormat(LisaConstants.LISA_DATE_STING_FORMAT);

    public static String getDateStringInDefaultDateFormat(Date date) {
        return (date != null ? LISA_DATE_FORMAT.format(date) : null);
    }

    public static Date parseDate(String value) throws ParseException {
        return (BaseUtils.isStrEmptyOrWhiteSpace(value) ? null : LISA_DATE_FORMAT.parse(value));
    }

    public static String transcodeToLatin1(String s) {
//        Charset charset = Charset.forName("ISO-8859-1");
//        Charset charset = Charset.forName("cp1252");
//        String q = new String(s.getBytes(), charset);

        return new String(s.getBytes(), Charset.forName("ISO-8859-1"));
//        return new String(s.getBytes(), StandardCharsets.ISO_8859_1);
    }

    public static String transcodeFromLatin1(String s) {
//        Charset charset = Charset.forName("Cp1250");
//        Charset charset = Charset.forName("ISO-8859-1");
//        Charset charset = Charset.forName("ISO-8859-1");
//        Charset charset = Charset.forName("ISO8859_15_FDIS");

//        String q = new String(s.getBytes(charset));
//        return new String(s.getBytes(Charset.forName("ISO-8859-1")));
        return new String(s.getBytes(Charset.forName("ISO-8859-1")));
//        return new String(s.getBytes(StandardCharsets.ISO_8859_1));
//        return q;
//        return Junicode.unicode(s);
    }
}
