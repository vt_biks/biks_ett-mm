/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.SimplePanel;
import pl.fovis.client.dialogs.LoginForm;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class LoginPageBase extends SimplePanel {

    protected PushButton loginBt;
    protected final LoginForm form;

    public LoginPageBase() {
        //IContinuation afterProperLogin = BIKClientSingletons.performOnLoginLogoutCont;
        IParametrizedContinuation<Boolean> loggingResultCont = new IParametrizedContinuation<Boolean>() {
            @Override
            public void doIt(Boolean param) {
                //do nothing, actually
            }
        };

        this.form = new LoginForm(/*afterProperLogin,*/loggingResultCont);
        this.form.addEnterPressedInTextBoxesHandler();
        buildWidgets();
    }

    private void buildWidgets() {
        final SimplePanel main = this;
        form.buildMainWidgets();
        loginBt = createLoginBt();

        int row = 0;
        FlexTable ft = new FlexTable();
        final FlexTable.FlexCellFormatter flexCellFormatter = ft.getFlexCellFormatter();
        flexCellFormatter.setColSpan(row, 0, 2);
        ft.setWidget(row++, 0, form);
        ft.setWidget(row, 0, loginBt);
        addOptButtons(ft, flexCellFormatter, row);
        flexCellFormatter.setHorizontalAlignment(row, 0, HasAlignment.ALIGN_LEFT);
        FlexTable MltxBackground = NewLookUtils.mltxBackground(ft);
        main.add(MltxBackground);
    }

    protected void addOptButtons(FlexTable ft, FlexTable.FlexCellFormatter flexCellFormatter, int row) {
        // no op
    }

    private PushButton createLoginBt() {
        PushButton b = new PushButton(I18n.zaloguj.get());
        NewLookUtils.makeCustomPushButton(b);
        b.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                form.onLoginClick();
            }
        });
        return b;
    }
}
