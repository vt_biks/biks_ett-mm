/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import commonlib.LameUtils;
import html2pdf.Html2pdf;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.ILameCollector;
import simplelib.IParametrizedContinuation;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author bfechner
 */
public class TreeExportManager extends ExportManagerBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected BOXIServiceImpl service;

    public TreeExportManager(BOXIServiceImpl service) {
        this.service = service;
    }

    boolean useLowMem = true;

    @Override
    protected Pair<String, IParametrizedContinuation<ILameCollector<Map<String, Object>>>> optGetFileNamePrefixAndContentProducer(HttpServletRequest req) {
        if (!useLowMem || !BaseUtils.isStrEmptyOrWhiteSpace(req.getParameter(BIKConstants.TREE_EXPORT_ACTION_PARAM_NAME))) {
            return null;
        }

        String fileNamePrefix = req.getParameter(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME);
        final int treeId = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME));

        return new Pair<String, IParametrizedContinuation<ILameCollector<Map<String, Object>>>>(fileNamePrefix, new IParametrizedContinuation<ILameCollector<Map<String, Object>>>() {

            @Override
            public void doIt(ILameCollector<Map<String, Object>> param) {
                service.getTreeToExportViaCollector(treeId, param);
            }
        });
    }

//    // OBJ_ID	PARENT_OBJ_ID	NAME	NODE_KIND_CODE	DESCR	VISUAL_ORDER
//    List<String> colNames = BaseUtils.splitBySep("OBJ_ID, PARENT_OBJ_ID, NAME, NODE_KIND_CODE, DESCR, VISUAL_ORDER", ",", true);
//
//    @Override
//    public void handleExportRequest(final HttpServletRequest req, final HttpServletResponse resp) {
//        //dla csv i xlsx
//        boolean isXlsxExportReq = BIKConstants.EXPORT_FORMAT_XLSX.equals(req.getParameter(BIKConstants.TREE_EXPORT_PARAM_NAME));
//        boolean isCsvExportReq = BIKConstants.EXPORT_FORMAT_CSV.equals(req.getParameter(BIKConstants.TREE_EXPORT_PARAM_NAME));
//        ServletOutputStream output = null;
//        try {
//            output = resp.getOutputStream();
//            req.setCharacterEncoding("utf8");
//            String treeId = req.getParameter(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME);
//
//            final int intTreeId = BaseUtils.tryParseInt(treeId);
//
//            if (isMemUsageLoggingEnabled()) {
//                logMemUsageWithMessage("before getTreeToExport");
//            }
//
//            List<Map<String, Object>> tree = null;
//            if (!useLowMem) {
//                tree = service.getTreeToExport(intTreeId);
//                if (isMemUsageLoggingEnabled()) {
//                    logMemUsageWithMessage("after getTreeToExport, row count=" + tree.size());
//                }
//            }
//
//            String fileName = req.getParameter(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME);
//            if (isXlsxExportReq) {
////                handleXlsxExportRequest(req, resp, tree, fileName);
//                writeXlsxFormat(req, resp, fileName, tree);
//            } else if (isCsvExportReq) {
//
//                if (useLowMem) {
//                    writeCsvFormatWithProducer(req, resp, new IParametrizedContinuation<ILameCollector<Map<String, Object>>>() {
//
//                        @Override
//                        public void doIt(ILameCollector<Map<String, Object>> param) {
//                            service.getTreeToExportViaCollector(intTreeId, param);
//                        }
//                    }, fileName, colNames);
//                } else {
////                    handleCsvExportRequest(req, resp, tree, fileName);
//                    writeCsvFormat(req, resp, fileName, tree);
//                }
//            }
//        } catch (Exception e) {
//            if (logger.isErrorEnabled()) {
//                logger.error("TreeExportManager: something went wrong while generating export");
//            }
//            e.printStackTrace();
//        } finally {
//            if (output != null) {
//                try {
//                    output.close();
//                } catch (IOException e) {
//                    if (logger.isErrorEnabled()) {
//                        logger.error("TreeExportManager: something went wrong while closing output");
//                    }
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
    @Override
    protected String getFileFormat(HttpServletRequest req) {
        return req.getParameter(BIKConstants.TREE_EXPORT_PARAM_NAME);
    }

    @Override
    protected Pair<String, List<Map<String, Object>>> getFileNamePrefixAndContent(HttpServletRequest req) {
        String fileNamePrefix = req.getParameter(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME);
        String act = req.getParameter(BIKConstants.TREE_EXPORT_ACTION_PARAM_NAME);
        int treeId = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME));
        List<Map<String, Object>> content = new ArrayList<Map<String, Object>>();
        List<String> branchIds = new ArrayList<String>();
        branchIds.add("");
        if (BaseUtils.isStrEmptyOrWhiteSpace(act)) {
            content = service.getTreeToExport(treeId, branchIds);
        } else {
            Integer bindedTreeId = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_BINDING_PARAM_NAME));
            String bindedTreeName = req.getParameter(BIKConstants.TREE_EXPORT_BINDED_TREE_NAME_PARAM_NAME);
            fileNamePrefix = fileNamePrefix + "_" + bindedTreeName;
            if (act.equalsIgnoreCase(BIKConstants.TREE_EXPORT_ACTION_LINKED_ASSOCIATIONS)) {
                content = service.getRelatedNodeToExport(treeId, branchIds, bindedTreeId);
            }
        }
        return new Pair<String, List<Map<String, Object>>>(fileNamePrefix, content);
    }

    protected void optAsk4Data(HttpServletRequest req, IParametrizedContinuation cont) {
        int treeId = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME));
        int depth = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_DEPTH_PARAM_NAME));
        int numbering = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_NUMBERING_PARAM_NAME));
        List<String> nodeIdsAsStr = BaseUtils.splitBySep(req.getParameter(BIKConstants.TREE_EXPORT_SELECTED_NODE_PARAM_NAME), ",");
        List<Integer> nodeIds2Export = new ArrayList<Integer>();
        for (String id : nodeIdsAsStr) {
            Integer nodeId = BaseUtils.tryParseInteger(id);
            if (nodeId != null) {
                nodeIds2Export.add(nodeId);
            }
        }
        try {
            //rekurencyjnie
            service.createDocument4TreeProducer(treeId, nodeIds2Export, depth, numbering, cont);
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
    }

    protected void optAsk4DataPrint(HttpServletRequest req, IParametrizedContinuation cont) {
        int treeId = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME));
        int depth = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_DEPTH_PARAM_NAME));
        int numbering = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_NUMBERING_PARAM_NAME));
        List<String> nodeIdsAsStr = BaseUtils.splitBySep(req.getParameter(BIKConstants.TREE_EXPORT_SELECTED_NODE_PARAM_NAME), ",");
//        int nodeKindId = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_NODE_KIND_ID));
        String nodeKindCode = req.getParameter(BIKConstants.TREE_EXPORT_NODE_KIND_CODE);
//        String template = req.getParameter(BIKConstants.TREE_EXPORT_TEMPLATE);

        Integer templateNodeId = BaseUtils.tryParseInteger(req.getParameter(BIKConstants.TREE_EXPORT_TEMPLATE_NODE_ID));
        Integer showNewLine = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_SHOW_NEW_LINE));
        Integer showAuthor = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_SHOW_AUTHOR));
        Integer maxPrintLevel = BaseUtils.tryParseInteger(req.getParameter(BIKConstants.TREE_EXPORT_LEVEL));
        List<Integer> nodeIds2Export = new ArrayList<Integer>();
        for (String id : nodeIdsAsStr) {
            Integer nodeId = BaseUtils.tryParseInteger(id);
            if (nodeId != null) {
                nodeIds2Export.add(nodeId);
            }
        }
        boolean isPDFFormat = BIKConstants.EXPORT_FORMAT_PDF.equals(getFileFormat(req));
        try {
            //rekurencyjnie
            service.createDocument4TreeProducerPrint(treeId, nodeIds2Export, depth, numbering, nodeKindCode, templateNodeId, maxPrintLevel, showNewLine == 1, showAuthor == 1, isPDFFormat, cont);
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }
    }

    @Override
    protected List<String> getFiles2Zip(HttpServletRequest req) {
        int treeId = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME));
        List<String> nodeIdsAsStr = BaseUtils.splitBySep(req.getParameter(BIKConstants.TREE_EXPORT_SELECTED_NODE_PARAM_NAME), ",");
        List<Integer> nodeIds2Export = new ArrayList<Integer>();
        for (String id : nodeIdsAsStr) {
            Integer nodeId = BaseUtils.tryParseInteger(id);
            if (nodeId != null) {
                nodeIds2Export.add(nodeId);
            }
        }
        return service.saveTree2Disk(treeId, nodeIds2Export);
    }

    private String generateHtml(HttpServletRequest req, boolean withBom) throws IOException {

        //tylko eksport drzewa
        String exportFolderPath = service.getTreeDocumentExportFolderPath();
        File exportFolder = new File(exportFolderPath);
        if (!exportFolder.exists()) {
            exportFolder.mkdirs();
        }
        String fileName = req.getParameter(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME) + ".html";
        String filePath = exportFolderPath + fileName;
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
        final OutputStream outputStream = new FileOutputStream(file);
        final StringBuilder sb = new StringBuilder();
        if (withBom) {
            sb.append("\uFEFF");
        }
//        sb.append("<body>").append("\n");
        sb.append("<head><style type=\"text/css\"> h2 {"
                + "    background: none repeat scroll 0 0 transparent;"
                + "    border-bottom: 1px solid #CCDAEC;"
                + "    font-weight: normal;"
                + "    margin: 0;"
                + "    overflow: hidden;"
                + "    padding-bottom: 0.17em;"
                + "    padding-top: 0.5em;"
                + "    width: auto;"
                + "    font-family: sans-serif;"
                + "}"
                + "table.def {"
                + "    background: none repeat scroll 0 0 #F9F9F9;"
                + "    border: 1px solid #AAAAAA;"
                + "    border-collapse: collapse;}"
                + ".def th, .def td {"
                + "    border: 1px solid #AAAAAA;"
                + "    height: 2em;"
                + "}"
                + "</style></head>");
        sb.append("<body style=\"font-family: Tahoma, Arial, sans-serif;\">").append("\n");
//         sb.append("<body style=\"font-family: Tahoma, Arial, sans-serif, Helvetica Neue, Helvetica, Verdana;\">").append("\n");
        IParametrizedContinuation cont = new IParametrizedContinuation<String>() {

            @Override
            public void doIt(String param) {
                Set<Integer> links = BIKCenterUtils.extractNodeIdsFromHtml(param);
                if (!links.isEmpty()) {
                    param = BIKCenterUtils.removeHrefsFromHtml(param, links);
                }
                sb.append(param);
                if (sb.length() > 20 * 1024) {
                    try {
                        //
                        outputStream.write(sb.toString().getBytes(StandardCharsets.UTF_8));
                        outputStream.flush();
                        sb.setLength(0);
                    } catch (IOException ex) {
                        throw new LameRuntimeException(ex);
                    }
                }
            }
        };

        optAsk4Data(req, cont);
        sb.append("</body>");
        if (sb.length() > 0) {
            outputStream.write(sb.toString().getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
        }
        outputStream.close();

        return filePath;
    }

    protected String getFileName(HttpServletRequest req) {

        String print = req.getParameter(BIKConstants.TREE_EXPORT_PRINT);
        boolean isPrint = !BaseUtils.isStrEmptyOrWhiteSpace(print);
        String fileName = "";
        if (isPrint) {
            List<Integer> nodeIds2Export = new ArrayList<Integer>();
            List<String> nodeIdsAsStr = BaseUtils.splitBySep(req.getParameter(BIKConstants.TREE_EXPORT_SELECTED_NODE_PARAM_NAME), ",");
            for (String id : nodeIdsAsStr) {
                Integer nodeId = BaseUtils.tryParseInteger(id);
                if (nodeId != null) {
                    nodeIds2Export.add(nodeId);
                }
            }
            Integer nodeId = nodeIds2Export.get(0);
            fileName = service.nodeNamesById(nodeId);// + ".html";
        } else {
            fileName = req.getParameter(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME);
        }
        if (fileName.length() > 200) {
            fileName = fileName.substring(0, 200);
        }
//        fileName += ".html";
//        String fileName = req.getParameter(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME);
        fileName = fileName.replaceAll("[\\\\/:*?\"<>|@#$%^&*{}()]", "_");
        return fileName;
    }

    private String generatePrintHtml(HttpServletRequest req, boolean withBom) throws IOException {

        //tylko eksport drzewa
        String exportFolderPath = service.getTreeDocumentExportFolderPath();
        File exportFolder = new File(exportFolderPath);
        if (!exportFolder.exists()) {
            exportFolder.mkdirs();
        }

        String fileName = getFileName(req) + ".html";// service.nodeNamesById(nodeId) + ".html";
//        String fileName2 = req.getParameter(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME) + ".html";
//        String objName = req.getParameter(BIKConstants.TREE_EXPORT_SELECTED_NODE_NAME);

        Integer showPage = BaseUtils.tryParseInt(req.getParameter(BIKConstants.TREE_EXPORT_SHOW_PAGE));
        String filePath = exportFolderPath + fileName;
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }

        boolean isPdf = BIKConstants.EXPORT_FORMAT_PDF.equals(getFileFormat(req));
        String font = isPdf ? "Calibri" : "Arial";
        final OutputStream outputStream = new FileOutputStream(file);
        final StringBuilder sb = new StringBuilder();
        if (withBom) {
            sb.append("\uFEFF");
        }
        //        sb.append("<body>").append("\n");
        sb.append("<head><style type=\"text/css\"> "
                + "h2 {"
                + "    background: none repeat scroll 0 0 transparent;"
                + "    border-bottom: 1px solid #CCDAEC;"
                + "    font-weight: normal;"
                + "    margin: 0;"
                + "    overflow: hidden;"
                + "    padding-bottom: 0.17em;"
                + "    padding-top: 0.5em;"
                + "    width: auto;"
                + "    font-family: ").append(font).append(";"
                + "    -fs-pdf-font-embed: embed;"
                + "    -fs-pdf-font-encoding: Identity-H;"
        );

        //                + "    page-break-before: always;"
        sb.append("}"
                + "table.def {"
                + "    background: none repeat scroll 0 0 #F9F9F9;"
                + "    border: 1px solid #AAAAAA;"
                + "    border-collapse: collapse;}"
                + ".def th, .def td {"
                + "    border: 1px solid #AAAAAA;"
                + "    height: 2em;"
                + "}");
        if (showPage == 1) {
            sb.append(
                    "    @page {"
                    + "        @bottom-right {"
                    + "            content: \"")
                    //                .append(objName).append(" \" \n \" ")
                    .append(I18n.printStrona.get()).append(" \" counter(page) \" ")
                    .append(I18n.printZ.get()).append(" \" counter(pages);"
                    + "        }"
                    + "    }");
        }

        if (isPdf) {
            sb.append("@font-face {"
                    + "    font-family: " + font + ";"
                    + "    src: url(\"" + font + ".ttf\");"
                    + "    -fs-pdf-font-embed: embed;"
                    + "    -fs-pdf-font-encoding: Identity-H;"
                    + "}"
                    + ""
                    + "* {"
                    + "        font-family: " + font + ";"
                    + "}");
        }
//        sb.append(
//                "@media print {"
//                + "  .new-page {"
//                + "    page-break-before: always;"
//                + "  }"
//                + "}");
        sb.append(
                "</style>"
                + ""
                + ""
                + "</head>"
        );
//        sb.append("<body style=\"font-family:DejaVu Sans;\">").append("\n");
        if (isPdf) {
            sb.append("<body style=\"font-family: ").append(font).append(";\">").append("\n");
        } else {
            sb.append("<body style=\"font-family: Tahoma, Arial, sans-serif, Helvetica Neue, Helvetica, Verdana;\">").append("\n");
        }
        IParametrizedContinuation cont = new IParametrizedContinuation<String>() {

            @Override
            public void doIt(String param) {
                Set<Integer> links = BIKCenterUtils.extractNodeIdsFromHtml(param);
                if (!links.isEmpty()) {
                    param = BIKCenterUtils.removeHrefsFromHtml(param, links);
                }
                sb.append(param);
                if (sb.length() > 20 * 1024) {
                    try {
                        //
                        outputStream.write(sb.toString().getBytes(StandardCharsets.UTF_8));
                        outputStream.flush();
                        sb.setLength(0);
                    } catch (IOException ex) {
                        throw new LameRuntimeException(ex);
                    }
                }
            }
        };

        optAsk4DataPrint(req, cont);
        sb.append("</body>");
        if (sb.length() > 0) {
            outputStream.write(sb.toString().getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
        }
        outputStream.close();

        return filePath;
    }

    @Override
    protected void writeHtmlFormat(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String print = req.getParameter(BIKConstants.TREE_EXPORT_PRINT);

        String htmlFilePath = BaseUtils.isStrEmptyOrWhiteSpace(print) ? generateHtml(req, true) : generatePrintHtml(req, true);
        String fileNamePrefix = getFileName(req);
//        String fileNamePrefix = req.getParameter(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME);
        writeFile2ResponseStream(resp, htmlFilePath, getContentDispositionValNew(fileNamePrefix, req, ".html", true));
    }

    /* przenieść do oddzielnej klasy */
    protected void writeJasperFormat(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String filePath = generateJasperReport(req, true);
        String extension = req.getParameter(BIKConstants.JASPER_EXPORT_EXTENSION);
        File file = new File(filePath);
        resp.setContentType(URLConnection.guessContentTypeFromName(file.getName()));
        resp.setHeader("Content-disposition", getContentDispositionVal(req.getParameter(BIKConstants.JASPER_EXPORT_NAME_PARAM_NAME), req, "." + extension));

        resp.setContentLength(Long.valueOf(file.length()).intValue());
        InputStream is = new FileInputStream(file);
        byte[] buffer = new byte[is.available()];

        final ServletOutputStream outputStream = resp.getOutputStream();
        is.read(buffer);
        outputStream.write(buffer);
        outputStream.flush();
        outputStream.close();

    }

    private String generateJasperReport(HttpServletRequest req, boolean withBom) throws IOException {

        String exportFolderPath = service.getJaspeReportsExportFolderPath();
        File exportFolder = new File(exportFolderPath);
        if (!exportFolder.exists()) {
            exportFolder.mkdirs();
        }

        String reportURL = req.getParameter(BIKConstants.JASPER_EXPORT_PARAM_NAME);
        Integer nodeId = BaseUtils.tryParseInt(req.getParameter(BIKConstants.JASPER_EXPORT_NODE_ID));
        String extension = req.getParameter(BIKConstants.JASPER_EXPORT_EXTENSION);

        String name = req.getParameter(BIKConstants.JASPER_EXPORT_NAME_PARAM_NAME);
//        String fileName = "file_" + name + nodeId + "." + extension;
        String fileName = nodeId + "_" + name + "." + extension;
        String filePath = exportFolderPath + fileName;
        byte[] response = service.getJasperReports(nodeId, reportURL, extension);
        FileOutputStream fos = new FileOutputStream(exportFolderPath + fileName);
        fos.write(response);
        fos.close();

        return filePath;
    }

    private void writeFile2ResponseStream(HttpServletResponse resp, String filePath, String contentDispositionVal) throws IOException {

//        resp.setContentType(contentType);
        File file = new File(filePath);
        resp.setContentType(URLConnection.guessContentTypeFromName(file.getName()));
        resp.setHeader("Content-disposition", contentDispositionVal);
//        Response.AddHeader("content-disposition", "attachment; filename=\"ąęź.csv\"");
//        resp.setCharacterEncoding("Windows-1252");
        resp.setContentLength(Long.valueOf(file.length()).intValue());
        InputStream is = new FileInputStream(file);
        byte[] buffer = new byte[is.available()];

        final ServletOutputStream outputStream = resp.getOutputStream();
        is.read(buffer);
        outputStream.write(buffer);
        outputStream.flush();
        outputStream.close();

    }

    @Override
    protected void writePdfFormat(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String print = req.getParameter(BIKConstants.TREE_EXPORT_PRINT);

        String htmlFilePath = BaseUtils.isStrEmptyOrWhiteSpace(print) ? generateHtml(req, true) : generatePrintHtml(req, true);
//        String htmlFilePath = generateHtml(req, true);
        String pdfFilePath = htmlFilePath.substring(0, htmlFilePath.lastIndexOf(".html")) + ".pdf";
        File pdfFile = new File(pdfFilePath);
        if (pdfFile.exists()) {
            pdfFile.delete();
        }
        try {
            Html2pdf.convert(htmlFilePath, pdfFilePath);

        } catch (Throwable ex) {
            throw new LameRuntimeException(ex);
        }
//        byte[] encoded = Files.readAllBytes(Paths.get(htmlFilePath));
//        String html = new String(encoded, StandardCharsets.UTF_8);

//
//        OutputStream outputStream = new FileOutputStream(pdfFile);
//        ITextRenderer renderer = new ITextRenderer();
//
//        renderer.setDocumentFromString(html);
//        renderer.layout();
//        try {
//            renderer.createPDF(outputStream);
//        } catch (DocumentException ex) {
//            throw new LameRuntimeException(ex.getMessage());
//        } finally {
//            outputStream.close();
//        }
//        String fileNamePrefix = req.getParameter(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME);
        String fileNamePrefix = getFileName(req);
        writeFile2ResponseStream(resp, pdfFilePath, getContentDispositionValNew(fileNamePrefix, req, ".pdf", true));
    }
}
