/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import java.io.InputStream;
import java.util.Properties;
import pl.trzy0.foxy.commons.ITextProvider;
import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.IGenericResourceProvider;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class TextProviderFromResProvider implements ITextProvider, IGenericResourceProvider<String> {

    private static final long CHECK_LASTMOD_INTERVAL_MILLIS = 1000;
    private String realResourceName;
    private IGenericResourceProvider<Properties> resProvider;
    private Properties props;
    private Long propsLastMod;
    private Long lastCheck;

    public TextProviderFromResProvider(IGenericResourceProvider<Properties> resProvider, String resourceName) {
        this.realResourceName = resourceName;
        this.resProvider = resProvider;
    }

    public static TextProviderFromResProvider fromLameResourceProvider(
            IGenericResourceProvider<InputStream> resProviderIs, String resourceName) {
        return new TextProviderFromResProvider(BaseUtils.compoundProvider(resProviderIs, new Projector<InputStream, Properties>() {

            public Properties project(InputStream val) {
                Properties props = new Properties();
                try {
                    props.load(val);
                } catch (Exception ex) {
                    throw new LameRuntimeException("error loading props", ex);
                }
                return props;
            }
        }), resourceName);
    }

    protected void ensureFreshProps() {
        long currMillis = System.currentTimeMillis();

        if (lastCheck == null || lastCheck + CHECK_LASTMOD_INTERVAL_MILLIS < currMillis) {
            long currLastMod = resProvider.getLastModifiedOfResource(realResourceName);
            if (propsLastMod == null || currLastMod != propsLastMod) {
                props = resProvider.gainResource(realResourceName);
                propsLastMod = currLastMod;
            }
            lastCheck = currMillis;
        }
    }

    public String getText(String key) {
        ensureFreshProps();
        return props.getProperty(key, key);
    }

    public String gainResource(String resourceName) {
        return getText(resourceName);
    }

    public long getLastModifiedOfResource(String resourceName) {
        return resProvider.getLastModifiedOfResource(realResourceName);
    }
}
