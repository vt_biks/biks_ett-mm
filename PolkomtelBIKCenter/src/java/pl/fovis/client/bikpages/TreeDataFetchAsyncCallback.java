/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author wezyr
 */
public abstract class TreeDataFetchAsyncCallback<DATA> extends StandardAsyncCallback<DATA> {

    protected IPageDataFetchBroker optBroker;

    public TreeDataFetchAsyncCallback(IPageDataFetchBroker optBroker) {
        this(null, optBroker);
    }

    public TreeDataFetchAsyncCallback(String errorMsg, IPageDataFetchBroker optBroker) {
        super(errorMsg);
        this.optBroker = optBroker;
    }

    protected void dataFetchFinished(boolean success) {
        if (optBroker != null) {
            optBroker.dataFetchFinished(success);
        }
    }

    protected abstract void doOnSuccess(DATA result);

    protected void doOnFailure(Throwable caught) {
        // opcjonalnie można pokryć (override) w podklasach,
        // ale raczej zazwyczaj nie trzeba

        // to wywołanie powoduje pokazanie komunikatu o błędzie
        super.onFailure(caught);
    }

    public void onSuccess(DATA result) {
        dataFetchFinished(true);
        doOnSuccess(result);
    }

    @Override
    public void onFailure(Throwable caught) {
        dataFetchFinished(false);
        doOnFailure(caught);
    }
}
