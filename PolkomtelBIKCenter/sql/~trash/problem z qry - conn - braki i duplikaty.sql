declare @rapTreeId int = (select id from bik_tree where code = 'Reports')
--select @rapTreeId 
declare @rapWebiKindId int = (select dbo.fn_node_kind_id_by_code('Webi'))
--select @rapWebiKindId 
declare @qryKindId int = (select dbo.fn_node_kind_id_by_code('ReportQuery'))
--select @qryKindId
declare @objMeasureKindId int = (select dbo.fn_node_kind_id_by_code('Measure')) --  ('Measure', 'Dimension', 'Detail')
declare @objDimensionKindId int = (select dbo.fn_node_kind_id_by_code('Dimension')) --  ('Measure', 'Dimension', 'Detail')
declare @objDetailKindId int = (select dbo.fn_node_kind_id_by_code('Detail')) --  ('Measure', 'Dimension', 'Detail')
--select @objMeasureKindId, @objDimensionKindId, @objDetailKindId
declare @univTreeId int = (select id from bik_tree where code = 'ObjectUniverses')
--select @univTreeId
declare @univKindId int = (select dbo.fn_node_kind_id_by_code('Universe'))
declare @connKindId int = (select dbo.fn_node_kind_id_by_code('DataConnection'))
declare @connTreeId int = (select id from bik_tree where code = 'Connections')

declare @derivedTableKindId int = (select dbo.fn_node_kind_id_by_code('UniverseDerivedTable')) --  ('UniverseDerivedTable', 'UniverseAliasTable', 'UniverseTable')
declare @aliasTableKindId int = (select dbo.fn_node_kind_id_by_code('UniverseAliasTable')) --  ('UniverseDerivedTable', 'UniverseAliasTable', 'UniverseTable')
declare @tableKindId int = (select dbo.fn_node_kind_id_by_code('UniverseTable')) --  ('UniverseDerivedTable', 'UniverseAliasTable', 'UniverseTable')
-- select @derivedTableKindId, @aliasTableKindId, @tableKindId


-- samych qry jest: 6819
-- po zjoinowaniu z conn: 6820
select qry.id, qry.name as qry_name, count(*), min(conn.id) as min_conn_id, min(conn.name) as min_conn_name, max(conn.id) as max_conn_id, 
  max(conn.name) as max_conn_name
from bik_node qry 
  left join (bik_joined_objs connjoins
  inner join bik_node conn on conn.id = connjoins.dst_node_id)  on qry.id = connjoins.src_node_id  and conn.is_deleted = 0 and conn.linked_node_id is null and conn.tree_id = @connTreeId and conn.node_kind_id = @connKindId
where
  qry.is_deleted = 0 and qry.tree_id = @rapTreeId and qry.node_kind_id = @qryKindId
group by qry.id, qry.name
having count(*) > 1


select qry.*
from bik_node qry 
  left join (bik_joined_objs connjoins
  inner join bik_node conn on conn.id = connjoins.dst_node_id)  on qry.id = connjoins.src_node_id  and conn.is_deleted = 0 and conn.linked_node_id is null and conn.tree_id = @connTreeId and conn.node_kind_id = @connKindId
where
  qry.is_deleted = 0 and qry.tree_id = @rapTreeId and qry.node_kind_id = @qryKindId
  and conn.id is null
  