/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.IPageDataFetchBroker;
import pl.fovis.client.bikpages.PageDataFetchBroker;
import pl.fovis.client.bikpages.TreeDataFetchAsyncCallback;
import pl.fovis.client.bikpages.UserGroupTree;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class SystemGroupDialog extends BaseActionOrCancelDialog {

    protected ScrollPanel treeScrollPanel = new ScrollPanel();
    protected UserGroupTree treeWidget;
    protected IPageDataFetchBroker dataFetchBroker = new PageDataFetchBroker();
    protected List<Integer> userGroups;
    protected IParametrizedContinuation<List<Integer>> saveCont;

    public void buildAndShowDialog(List<Integer> userGroups, IParametrizedContinuation<List<Integer>> saveCont) {
        this.userGroups = userGroups;
        this.saveCont = saveCont;
        super.buildAndShowDialog();

    }

    @Override
    protected String getDialogCaption() {
        return I18n.nalezyDoGrupy.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(main,
                BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 8);
        main.add(treeScrollPanel);
        treeScrollPanel.setWidth("330px");

        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(treeScrollPanel,
                BIKClientSingletons.ORIGINAL_TREE_GRID_PANEL_HEIGHT);
        treeWidget = new UserGroupTree(null, dataFetchBroker, true, userGroups);
        Widget treeGridWidget = treeWidget.getTreeGridWidget();

        treeGridWidget.setWidth("330px");

        treeScrollPanel.add(treeGridWidget);

        show();
    }

    @Override
    protected void doAction() {
        saveCont.doIt(treeWidget.getListSelected());
    }

    protected void show() {
        dataFetchBroker.dataFetchStarting(IPageDataFetchBroker.TreeDataRequestKind.Full,
                BIKClientSingletons.getService().getGroupRootObjects(
                        new TreeDataFetchAsyncCallback<List<SystemUserGroupBean>>(null, dataFetchBroker) {
                            @Override
                            public void doOnSuccess(List<SystemUserGroupBean> t) {
                                treeWidget.initWithFullData(t, true, null, null);
                                treeWidget.getTreeGrid().selectFirstRecord();

                            }
                        }), null);
    }

}
