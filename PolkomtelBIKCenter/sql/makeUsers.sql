-- u�ytkownik aplikacyjny (login/has�o do serwera)
create login biks_app with password = 'app' -- to nale�y wpisa� w pliku konfiguracyjnym BIKSa: bikcenter-config.cfg
go
-- u�ytkownik do zasile� (login/has�o do serwera)
create login biks_load with password = 'load' -- to nale�y wpisa� poni�ej - aby si� zapisa�o w konfiguracji w bazie danych
go

exec sp_set_app_prop 'load_user_login', 'biks_load' -- login u�ytkownika do zasilania
go
exec sp_set_app_prop 'load_user_pwd', 'load' -- has�a u�ytkownika do zasilania
go

-- u�ytkownik do raportowania (login/has�o do serwera)
create login biks_rep with password = 'rep'
go

create user biks_app for login biks_app
go

CREATE ROLE biks_executor
go

grant execute to biks_executor
go


EXEC sp_addrolemember 'db_ddladmin', 'biks_app'
go
EXEC sp_addrolemember 'db_datawriter', 'biks_app'
go
EXEC sp_addrolemember 'db_datareader', 'biks_app'
go
EXEC sp_addrolemember 'biks_executor', 'biks_app'
go


create user biks_load for login biks_load
go

EXEC sp_addrolemember 'db_ddladmin', 'biks_load'
go
EXEC sp_addrolemember 'db_datawriter', 'biks_load'
go
EXEC sp_addrolemember 'db_datareader', 'biks_load'
go
EXEC sp_addrolemember 'biks_executor', 'biks_app'
go


create user biks_rep for login biks_rep
go

EXEC sp_addrolemember 'db_datareader', 'biks_load'
go
