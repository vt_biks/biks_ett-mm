/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

/**
 *
 * @author wezyr
 */
public interface IDstNodeIdExtractor<T> {

    public Integer getDstNodeId(T bean);

    public String getTreeKind(T bean);
}
