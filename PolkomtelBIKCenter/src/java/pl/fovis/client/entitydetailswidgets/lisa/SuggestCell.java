package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.cell.client.AbstractEditableCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import static com.google.gwt.dom.client.BrowserEvents.BLUR;
import static com.google.gwt.dom.client.BrowserEvents.CLICK;
import static com.google.gwt.dom.client.BrowserEvents.KEYDOWN;
import static com.google.gwt.dom.client.BrowserEvents.KEYUP;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.text.shared.SafeHtmlRenderer;
import com.google.gwt.text.shared.SimpleSafeHtmlRenderer;
import com.google.gwt.user.client.ui.ListBox;
import java.util.logging.Logger;
import pl.fovis.client.entitydetailswidgets.lisa.SuggestCell.ViewData;
import pl.fovis.common.lisa.LisaConstants;
import pl.fovis.common.lisa.LisaDictColumnBean;
import simplelib.BaseUtils;

public class SuggestCell extends AbstractEditableCell<String, ViewData> implements DoubleClickHandler {

    protected static final Logger logger = Logger.getLogger("SuggestCell");
    protected static Template template = GWT.create(Template.class);
    protected final SafeHtmlRenderer<String> renderer = SimpleSafeHtmlRenderer.getInstance();
    protected LisaDataGrid dataGrid;
    protected ListBoxInPopup suggestPopup;
    protected boolean isEditable;
    protected LisaDictColumnBean columnBean;
    protected Context context = null;
    protected ValueUpdater<String> valueUpdater = null;
    protected Element parent = null;
    protected Element lastParent = null;

    interface Template extends SafeHtmlTemplates {

        @SafeHtmlTemplates.Template("<input type=\"text\" value=\"{0}\" tabindex=\"-1\"></input>")
        SafeHtml input(String value);
    }

    public SuggestCell(LisaDataGrid dataGrid, ListBoxInPopup suggestPopup, boolean isEditable, LisaDictColumnBean columnBean) {
        super(CLICK, KEYUP, KEYDOWN, BLUR);
        this.dataGrid = dataGrid;
        this.suggestPopup = suggestPopup;
        this.isEditable = isEditable;
        this.columnBean = columnBean;
        if (mustValidate()) {
            this.suggestPopup.getLb().addDoubleClickHandler(this);
        }
    }

    @Override
    public void onDoubleClick(DoubleClickEvent event) {
        ListBox lb = suggestPopup.getLb();
        String value = lb.getItemText(lb.getSelectedIndex());
        ViewData viewData = getViewData(context.getKey());
        viewData.setText(value);
        startOrFinishEditing(value);
        commit(viewData);
    }

    protected boolean mustValidate() {
        return columnBean.validateLevel != 0;
    }

    @Override
    public boolean isEditing(Context context, Element parent, String value) {
        ViewData viewData = getViewData(context.getKey());
        return viewData == null ? false : viewData.isEditing();
    }

    protected void resetListBox() {
        if (!mustValidate()) {
            return;
        }
        String value = getInputElement(parent).getValue();
        suggestPopup.setPopupPosition(parent.getParentElement().getAbsoluteLeft(), parent.getAbsoluteBottom());
        suggestPopup.getLb().setWidth(dataGrid.getColumnWidth(context.getColumn()) + "px");
        suggestPopup.setColumnBean(columnBean);
        suggestPopup.resetListBox(value);
        suggestPopup.show();
    }

    @Override
    public void onBrowserEvent(Context context, Element parent, String value, NativeEvent event, ValueUpdater<String> valueUpdater) {
        if (dataGrid.isClickedAdd()) {
            if (context.getIndex() == 0) {
                updateCellContentOnBrowserEvent(context, parent, value, event, valueUpdater);
            }
        } else if (isEditable && dataGrid.isInEditMode()) {
            updateCellContentOnBrowserEvent(context, parent, value, event, valueUpdater);
        }
    }

    protected void updateCellContentOnBrowserEvent(Context context, Element parent, String value, NativeEvent event, ValueUpdater<String> valueUpdater) {
        if (this.parent != parent && this.context != null) {
            ViewData oldViewData = getViewData(this.context.getKey());
            if (mustValidate()) {
                cancel(oldViewData);
            }
        }
        this.parent = parent;
        this.context = context;
        this.valueUpdater = valueUpdater;

        Object key = context.getKey();
        ViewData viewData = getViewData(key);
        if (viewData != null && viewData.isEditing()) {
            editEvent(value, viewData, event);
        } else {
            String type = event.getType();
            if (viewData == null) {
                viewData = new ViewData(value);
                setViewData(key, viewData);
            } else {
                viewData.setEditing(true);
            }
            if (CLICK.equals(type)) {
                onClickEvent(value);
            }
        }
    }

    protected void onClickEvent(String value) {
        startOrFinishEditing(value);
        resetListBox();
    }

    @Override
    public void render(Context context, String value, SafeHtmlBuilder sb) {
        // Get the view data.
        Object key = context.getKey();
        ViewData viewData = getViewData(key);
        if (viewData != null
                && !viewData.isEditing()
                && BaseUtils.safeEqualsStr(value, LisaConstants.NULL_VALUE, false) /*&& value.equals(viewData.getText())*/) {
            clearViewData(key);
            viewData = null;
        }

        String toRender = value;
        if (viewData != null) {
            String text = viewData.getText();
            if (viewData.isEditing()) {
                /*
                 * Do not use the renderer in edit mode because the value of a text
                 * input element is always treated as text. SafeHtml isn't valid in the
                 * context of the value attribute.
                 */
                sb.append(text == null ? template.input("") : template.input(text));
                return;
            } else {
                // The user pressed enter, but view data still exists.
                toRender = text;
            }
        }

        if (toRender != null && toRender.trim().length() > 0) {
            sb.append(renderer.render(toRender));
        } else {
            /*
             * Render a blank space to force the rendered element to have a height.
             * Otherwise it is not clickable.
             */
//            sb.appendHtmlConstant("\u00A0");
            sb.append(renderer.render(LisaConstants.NULL_VALUE));
        }
    }

    @Override
    public boolean resetFocus(Context context, Element parent, String value) {
        if (isEditing(context, parent, value)) {
            getInputElement(parent).focus();
            return true;
        }
        return false;
    }

    protected void startOrFinishEditing(String value) {
        setValue(context, parent, value);
        InputElement input = getInputElement(parent);
        input.focus();
        input.select();
    }

    private native void clearInput(Element input) /*-{
     if (input.selectionEnd)
     input.selectionEnd = input.selectionStart;
     else if ($doc.selection)
     $doc.selection.clear();
     }-*/;

    protected void commit(ViewData viewData) {
        String value = updateViewData(viewData, false);
        clearInput(getInputElement(parent));
//        logger.log(Level.SEVERE, "commit: " + value);
        cleanupWork(value);
    }

    protected void cancel(ViewData viewData) {
        String originalText = viewData.getOriginal();
        clearInput(getInputElement(parent));
//        logger.log(Level.SEVERE, "cancel: " + originalText);
        cleanupWork(originalText);
    }

    protected void cleanupWork(String value) {
        setValue(context, parent, value);
        if (valueUpdater != null) {
            valueUpdater.update(value);
        }
        suggestPopup.hide();
        clearViewData(context.getKey());
        this.parent = null;
        this.context = null;
    }

    protected void editEvent(String value, ViewData viewData, NativeEvent event) {
        String type = event.getType();
        boolean keyUp = KEYUP.equals(type);
        boolean keyDown = KEYDOWN.equals(type);
        if (keyUp || keyDown) {
            int keyCode = event.getKeyCode();
            if (keyUp && keyCode == KeyCodes.KEY_ENTER) {
                // Commit the change.
                if (mustValidate() && !suggestPopup.isSelected()) {
                    cancel(viewData);
                } else {
                    commit(viewData);
                }
            } else if (keyUp && keyCode == KeyCodes.KEY_ESCAPE) {
                // Cancel edit mode.
                cancel(viewData);
            } else {
                // Update the text in the view data on each key.
                updateViewData(viewData, true);
                resetListBox();
            }
        } else if (BLUR.equals(type)) {
            if (!mustValidate()) {
                commit(viewData);
            }
        } else if (CLICK.equals(type)) {
            onClickEvent(value);
        }
    }

    /**
     * Get the input element in edit mode.
     */
    private InputElement getInputElement(Element parent) {
        return parent.getFirstChild().<InputElement>cast();
    }

    /**
     * Update the view data based on the current value.
     *
     * @param parent the parent element
     * @param viewData the {@link ViewData} object to update
     * @param isEditing true if in edit mode
     * @return the new value
     */
    private String updateViewData(ViewData viewData, boolean isEditing) {
        String value = getInputElement(parent).getValue();
        viewData.setText(value);
        viewData.setEditing(isEditing);
        return value;
    }

    static class ViewData {

        private boolean isEditing;
        private boolean isEditingAgain;
        private String original;
        private String text;

        public ViewData(String text) {
            this.original = BaseUtils.safeEquals(text, LisaConstants.NULL_VALUE) ? null : text;
            this.text = BaseUtils.safeEquals(text, LisaConstants.NULL_VALUE) ? null : text;;
            this.isEditing = true;
            this.isEditingAgain = false;
        }

        public String getOriginal() {
            return original;
        }

        public String getText() {
            return text;
        }

        public boolean isEditing() {
            return isEditing;
        }

        public boolean isEditingAgain() {
            return isEditingAgain;
        }

        public void setEditing(boolean isEditing) {
            boolean wasEditing = this.isEditing;
            this.isEditing = isEditing;

            // This is a subsequent edit, so start from where we left off.
            if (!wasEditing && isEditing) {
                isEditingAgain = true;
                original = text;
            }
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
