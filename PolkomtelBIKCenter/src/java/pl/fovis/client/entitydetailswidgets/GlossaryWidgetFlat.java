/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import java.util.List;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.JoinedObjsTabKind;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.JoinedObjBean;

/**
 *
 * @author młydkowski
 */
public class GlossaryWidgetFlat extends JoinedObjWidgetFlatBase<JoinedObjBean> {

    @Override
    protected List<JoinedObjBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
//        return data.arts;
        return cddb.jobs.get(JoinedObjsTabKind.Glossary);
    }

    @Override
    public String getCaption() {
        return BIKGWTConstants.MENU_NAME_GLOSSARY;
    }

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_GLOSSARY;
    }
}
