/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import pl.fovis.client.LisaService;
import pl.fovis.client.dialogs.lisa.AuthenticationModeDialog.AuthenticationMode;
import pl.fovis.client.dialogs.lisa.LisaDataType;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.ErwinDataModelProcessObjectsRel;
import pl.fovis.common.NewsBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.DateLDV;
import pl.fovis.common.lisa.ImportOverviewBean;
import pl.fovis.common.lisa.IntegerLDV;
import pl.fovis.common.lisa.LisaAuthenticationBean;
import pl.fovis.common.lisa.LisaConstants;
import pl.fovis.common.lisa.LisaConstants.ImportMode;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryBean;
import pl.fovis.common.lisa.LisaDictionaryValue;
import pl.fovis.common.lisa.LisaGridParamBean;
import pl.fovis.common.lisa.LisaImportActionBean;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import pl.fovis.common.lisa.LisaLogBean;
import pl.fovis.common.lisa.LisaUtils;
import pl.fovis.common.lisa.StringLDV;
import pl.fovis.common.lisa.TeradataException;
import pl.fovis.server.lisa.AuthorizedTeradataConnection;
import pl.fovis.server.lisa.BaseTeradataBatchWorker;
import pl.fovis.server.lisa.DictionaryGenerator;
import pl.fovis.server.lisa.LisaNodeCreator;
import pl.fovis.server.lisa.LisaServerUtils;
import pl.fovis.server.lisa.LisaTeradataQueryGenerator;
import pl.fovis.server.lisa.LisaUnassignedCategory;
import pl.fovis.server.lisa.TeradataBatchDeleteWorker;
import pl.fovis.server.lisa.TeradataBatchInsertWorker;
import pl.fovis.server.lisa.TeradataBatchUpdateWorker;
import pl.fovis.server.lisa.TeradataLogBean;
import pl.trzy0.foxy.commons.FoxyRuntimeException;
import pl.trzy0.foxy.commons.IBeanConnectionThreadHandle;
import pl.trzy0.foxy.commons.IFoxyTransactionalPerformer;
import pl.trzy0.foxy.commons.IHasFoxyTransactionalPerformers;
import pl.trzy0.foxy.serverlogic.db.TeradataConnection;
import pl.trzy0.foxy.serverlogic.db.TeradataConnectionConfig;
import simplelib.BaseUtils;
import simplelib.FieldNameConversion;
import simplelib.IContinuationWithReturn;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author pku
 */
// i18n-default: no
public class LisaServiceImpl extends BIKSServiceBaseForSingleBIKS implements LisaService,
        IHasFoxyTransactionalPerformers {

    protected Map<String, Pair<String, TeradataConnection>> teradataConnectionPools = new ConcurrentHashMap<String, Pair<String, TeradataConnection>>();
    protected Map<Integer, Integer> lisaInstanceIdByNodeId = new HashMap<Integer, Integer>();
    private final BOXIServiceImpl boxiService;
//    private final ConnectionRemover remover = new ConnectionRemover();
    private CancallableRunner cancellableRunner = new CancallableRunner();

    LisaServiceImpl(BOXIServiceImpl boxiService) {
        this.boxiService = boxiService;
    }

    private TeradataConnectionConfig generateTeradataConnConfig(Integer instanceId, String username, String password) {
        LisaInstanceInfoBean connectionBean = getLisaInstanceInfoById(instanceId);
        TeradataConnectionConfig tdCfg = new TeradataConnectionConfig();
//        tdCfg.database = connectionBean.dbName;
        tdCfg.tmode = connectionBean.tmode;
        tdCfg.charset = connectionBean.charset;
        tdCfg.user = username;
        tdCfg.password = password;
        tdCfg.server = connectionBean.url;
        tdCfg.port = connectionBean.port;
        return tdCfg;
    }

    public List<LisaDictColumnBean> getDictionaryColumns(Integer categoryNodeId) {
//        Integer instanceId = getInstanceIdByNodeId(categoryNodeId);
//        return addInstanceId2DictValidation(createBeansFromNamedQry("prepareLisaColumnsForSelect", LisaDictColumnBean.class, categoryNodeId, null, 1), instanceId);
        return createBeansFromNamedQry("prepareLisaColumnsForSelect", LisaDictColumnBean.class, categoryNodeId, null, 1);
    }

    public List<LisaDictColumnBean> getDictionaryColumnsForDic(Integer dicNodeId) {
//        Integer instanceId = getInstanceIdByNodeId(dicNodeId);
//        return addInstanceId2DictValidation(createBeansFromNamedQry("prepareLisaColumnsForSelectDic", LisaDictColumnBean.class, dicNodeId, 1), instanceId);
        return createBeansFromNamedQry("prepareLisaColumnsForSelectDic", LisaDictColumnBean.class, dicNodeId, 1);
    }

    public List<LisaDictColumnBean> getDictionaryColumns(String dictionaryName) {
//        if (BaseUtils.isStrEmpty(dictionaryName)) {
//            return null;
//        }
//        Integer instanceId = BaseUtils.tryParseInt(dictionaryName.substring(0, dictionaryName.indexOf(".")));
//        return addInstanceId2DictValidation(createBeansFromNamedQry("prepareLisaColumnsForSelect", LisaDictColumnBean.class, null, dictionaryName, 0), instanceId);
        return createBeansFromNamedQry("prepareLisaColumnsForSelect", LisaDictColumnBean.class, null, dictionaryName, 0);
    }

    public List<LisaDictColumnBean> getEditableDictionaryColumns(String dictionaryName) {
//        if (BaseUtils.isStrEmpty(dictionaryName)) {
//            return null;
//        }
//        Integer instanceId = BaseUtils.tryParseInt(dictionaryName.substring(0, dictionaryName.indexOf(".")));
//        return addInstanceId2DictValidation(createBeansFromNamedQry("prepareLisaColumnsForSelect", LisaDictColumnBean.class, null, dictionaryName, 2), instanceId);
        return createBeansFromNamedQry("prepareLisaColumnsForSelect", LisaDictColumnBean.class, null, dictionaryName, 2);
    }

    private String getSchemaName(Integer instanceId) {
//        String schemaName = execNamedQuerySingleVal("getAdminSingleValue", false, null, LisaConstants.PARAM_TERADATA_SCHEMA_NAME);
        LisaInstanceInfoBean bean = createBeanFromNamedQry("getLisaInstanceInfoById", LisaInstanceInfoBean.class, false, instanceId);
        return bean.schemaName + ".";
    }

    @Override
    protected Map<String, IBeanConnectionThreadHandle> getCancellableExecutionsInSession() {
        return getSessionData().cancellableExecutions;
    }

    public List<LisaDictColumnBean> getDictionaryColumnsByNodeId(Integer nodeId) {
//        Integer instanceId = getInstanceIdByNodeId(nodeId);
//        return addInstanceId2DictValidation(createBeansFromNamedQry("prepareLisaColumnsForSelectByNodeId", LisaDictColumnBean.class, nodeId), instanceId);
        return createBeansFromNamedQry("prepareLisaColumnsForSelectByNodeId", LisaDictColumnBean.class, nodeId);
    }

    public Pair<List<LisaDictColumnBean>, String> initQueryGetDictionaryRecords(Integer instanceId, final LisaGridParamBean params, AuthorizedTeradataConnection tConnection) throws TeradataException {
        List<LisaDictColumnBean> columns;
        LisaDictionaryBean dictData;
        Integer categoryObjId = null;
        String categorizationObjId = null;

        LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(getSchemaName(instanceId), tConnection);
        if (params.nodeKindCode.equals(BIKConstants.NODE_KIND_LISA_DICTIONARY)) {
            columns = getDictionaryColumnsForDic(params.categoryNodeId);
            dictData = getDictionaryDataForDic(params.categoryNodeId);
        } else {
            columns = getDictionaryColumns(params.categoryNodeId);
            categoryObjId = fetchCategoryObjId(params.categoryNodeId);
            categorizationObjId = execNamedQuerySingleVal("getLisaCategorizationObjIdForCategoryNode", false, null, params.categoryNodeId);
            categorizationObjId = LisaUtils.fetchCategoryTypeObjIdAsString(categorizationObjId);
            dictData = getDictionaryData(params.categoryNodeId);
            checkCategoryVersioningAndUpdateBikDb(instanceId, tConnection, queryGenerator, dictData);
        }
        return new Pair<List<LisaDictColumnBean>, String>(columns, queryGenerator.selectDictionaryQuery(instanceId, params, columns, dictData, categoryObjId, categorizationObjId));
    }

    @Override
    public List<Map<String, LisaDictionaryValue>> getDictionaryRecords(Integer instanceId, final LisaGridParamBean params, String ticket) throws TeradataException {
        AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        Pair<List<LisaDictColumnBean>, String> colInfoAndQuery = initQueryGetDictionaryRecords(instanceId, params, tConnection);
        //System.out.println("Zapytanie: " + query);
        List<Map<String, Object>> objs = tConnection.execQry(colInfoAndQuery.v2);//tConnection.runCancellableQry(query,ticket);
        return convertToLDVMap(colInfoAndQuery.v1, objs);
    }

    @Override
    public Integer getCountDictionaryRecords(Integer instanceId, final LisaGridParamBean params, String ticket) throws TeradataException {
        AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        Pair<List<LisaDictColumnBean>, String> colInfoAndQuery = initQueryGetDictionaryRecords(instanceId, params, tConnection);
        //System.out.println("Zapytanie: " + query);
        //tConnection.runCancellableQry(query,ticket);
        return ((Long) tConnection.execQrySingleVal(colInfoAndQuery.v2)).intValue();
    }

    @Override
    public void updateDictionaryColumns(LisaDictColumnBean bean) {
        execNamedCommand("updateLisaDictionaryColumns", bean);
    }

    @Override
    public List<LisaDictionaryBean> getDictionaries(Integer instanceId) {
        return createBeansFromNamedQry("getLisaDictionaries", LisaDictionaryBean.class, instanceId);
    }

    @Override
    public TreeNodeBean saveNewCategory(Integer instanceId, final String categoryName, Integer categoryTypeNodeId) throws TeradataException {
        AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
//
        TreeNodeBean categoryTypeNode = createBeanFromNamedQry("getBikNode", TreeNodeBean.class, false, categoryTypeNodeId);
        Integer categoryKindId = execNamedQuerySingleValAsInteger("getBikNodeKindIdByCode", false, "id" /* i18n: no */, BIKConstants.NODE_KIND_LISA_CATEGORY);

        LisaNodeCreator creator = new LisaNodeCreator(tConnection, boxiService, getSchemaName(instanceId));
        TreeNodeBean bean = creator.createCategory(categoryName, categoryTypeNode, categoryKindId);
        saveTeradataLog(instanceId, getDictionaryNameForCategoryType(categoryTypeNodeId), "created new category" /* i18n: no */ + ": " + categoryName);
        return bean;
    }

    @Override
    public List<String> deleteCategory(Integer instanceId, TreeNodeBean nodeToDelete) throws TeradataException {
        final AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        final String schema = getSchemaName(instanceId);

        final LisaDictionaryBean dictData = getDictionaryData(nodeToDelete.id);
        final Integer categorizationObjId = LisaUtils.fetchCategoryTypeObjIdAsInt(nodeToDelete.objId);
        final Integer categoryObjId = LisaUtils.fetchCategoryObjId(nodeToDelete.objId);

        tConnection.execWithQueryBand(getQueryBand(), new IParametrizedContinuation<Connection>() {

            @Override
            public void doIt(Connection conn) {
                try {
                    String errorMessage = null;
                    String prepareQuery = " " + "call" /* i18n: no */ + " " + schema + "del_kateg"
                            + (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES ? LisaTeradataQueryGenerator.VALIDATION_SUFFIX : "")
                            + "(?,?,?,?"
                            + "); ";

                    CallableStatement cs = conn.prepareCall(prepareQuery);

                    cs.setString(1, LisaUtils.getTableNameWithoutDB(dictData.dbRelTable)); //p_kateg_rel_table
                    cs.setInt(2, categorizationObjId); //p_id_typ_kateg
                    cs.setInt(3, categoryObjId);//p_id_kateg
                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
                        cs.registerOutParameter(4, java.sql.Types.VARCHAR);
                    }
                    System.out.println("NAS: deleteCategory query: " + prepareQuery + ", params: " + LisaUtils.getTableNameWithoutDB(dictData.dbRelTable) + ", " + categorizationObjId + ", " + categoryObjId);
                    cs.executeQuery();

                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
                        errorMessage = cs.getString(4);
                        if (errorMessage != null && errorMessage.length() > 0) {
                            throw new FoxyRuntimeException(errorMessage);
                        }
                    }
                    cs.clearParameters();
                } catch (SQLException ex) {
                    throw new FoxyRuntimeException(ex.getMessage());
                }
            }
        });
//        tConnection.execWithJdbcConnection(new IParametrizedContinuation<Connection>() {
//            public void doIt(Connection conn) {
//
//                try {
//                    //Dodaj QUERY_BAND
//                    String errorMessage = null;
//                    String prepareQuery = " " + "call" /* i18n: no */ + " " + schema + "del_kateg"
//                            + (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES ? LisaTeradataQueryGenerator.VALIDATION_SUFFIX : "")
//                            + "(?,?,?,?"
//                            + "); ";
//
//                    conn.commit();
//                    CallableStatement cs = conn.prepareCall(prepareQuery);
//
//                    cs.setString(1, LisaUtils.getTableNameWithoutDB(dictData.dbRelTable)); //p_kateg_rel_table
//                    cs.setInt(2, categorizationObjId); //p_id_typ_kateg
//                    cs.setInt(3, categoryObjId);//p_id_kateg
//                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
//                        cs.registerOutParameter(4, java.sql.Types.VARCHAR);
//                    }
//                    System.out.println("NAS: deleteCategory query: " + prepareQuery + ", params: " + LisaUtils.getTableNameWithoutDB(dictData.dbRelTable) + ", " + categorizationObjId + ", " + categoryObjId);
//                    cs.executeQuery();
//
//                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
//                        errorMessage = cs.getString(4);
//                        if (errorMessage != null && errorMessage.length() > 0) {
//                            throw new FoxyRuntimeException(errorMessage);
//                        }
//                    }
//                    cs.clearParameters();
//
//                } catch (SQLException ex) {
//                    try {
//                        conn.rollback();
//                    } catch (SQLException exIn) {
//                        //Trzeba to przerobic, aby CallableStatement byĹ‚ w pl.trzy0.foxy.serverlogic.db, szybka poprawka dla Jerzego
//                        //inaczej sie nie wygrzebÄ™ z tego NAS-a, przy NAS etap2 to zmieniÄ‡!!!!
//                    }
//                    throw new FoxyRuntimeException(ex.getMessage());
//                }
//            }
//        });

        saveTeradataLog(instanceId, dictData.dbName, "delete category:" + nodeToDelete.name + " categ_id:" + categoryObjId);
        return boxiService.deleteTreeNode(nodeToDelete);
    }

    public List<String> moveCategory(Integer instanceId, TreeNodeBean nodeToMove, final Integer newCategNodeId) throws TeradataException {

        final AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        final String schema = getSchemaName(instanceId);

        final LisaDictionaryBean dictData = getDictionaryData(nodeToMove.id);
        final Integer categorizationObjId = LisaUtils.fetchCategoryTypeObjIdAsInt(nodeToMove.objId);
        final Integer categoryObjId = LisaUtils.fetchCategoryObjId(nodeToMove.objId);
        final Integer newCategId = fetchCategoryObjId(newCategNodeId);

        tConnection.execWithQueryBand(getQueryBand(), new IParametrizedContinuation<Connection>() {
            public void doIt(Connection conn) {

                try {
                    String errorMessage = null;
                    String prepareQuery = " " + "call" /* i18n: no */ + " " + schema + "move_kateg"
                            + (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES ? LisaTeradataQueryGenerator.VALIDATION_SUFFIX : "")
                            + "(?,?,?,?"
                            + "); ";

                    CallableStatement cs = conn.prepareCall(prepareQuery);

                    cs.setInt(1, categorizationObjId); //p_id_typ_kateg
                    cs.setInt(2, categoryObjId);//p_id_kateg
                    cs.setInt(3, newCategId);//p_id_kateg
                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
                        cs.registerOutParameter(4, java.sql.Types.VARCHAR);
                    }
                    cs.executeQuery();

                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
                        errorMessage = cs.getString(4);
                        if (errorMessage != null && errorMessage.length() > 0) {
                            throw new FoxyRuntimeException(errorMessage);
                        }
                    }
                    cs.clearParameters();

                } catch (SQLException ex) {
                    throw new FoxyRuntimeException(ex.getMessage());
                }
            }
        });
//        tConnection.execWithJdbcConnection(new IParametrizedContinuation<Connection>() {
//            public void doIt(Connection param) {
//
//                try {
//                    //Dodaj QUERY_BAND
//                    String errorMessage = null;
//                    String prepareQuery = " " + "call" /* i18n: no */ + " " + schema + "move_kateg"
//                            + (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES ? LisaTeradataQueryGenerator.VALIDATION_SUFFIX : "")
//                            + "(?,?,?,?"
//                            + "); ";
//
//                    param.commit();
//                    CallableStatement cs = param.prepareCall(prepareQuery);
//
//                    cs.setInt(1, categorizationObjId); //p_id_typ_kateg
//                    cs.setInt(2, categoryObjId);//p_id_kateg
//                    cs.setInt(3, newCategId);//p_id_kateg
//                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
//                        cs.registerOutParameter(4, java.sql.Types.VARCHAR);
//                    }
//                    cs.executeQuery();
//
//                    if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
//                        errorMessage = cs.getString(4);
//                        if (errorMessage != null && errorMessage.length() > 0) {
//                            throw new FoxyRuntimeException(errorMessage);
//                        }
//                    }
//                    cs.clearParameters();
//
//                } catch (SQLException ex) {
//                    try {
//                        param.rollback();
//                    } catch (SQLException exIn) {
//                        //Trzeba to przerobic, aby CallableStatement byĹ‚ w pl.trzy0.foxy.serverlogic.db, szybka poprawka dla Jerzego
//                        //inaczej sie nie wygrzebÄ™ z tego NAS-a, przy NAS etap2 to zmieniÄ‡!!!!
//                    }
//                    throw new FoxyRuntimeException(ex.getMessage());
//                }
//            }
//        });

        nodeToMove.parentNodeId = newCategNodeId;
        saveTeradataLog(instanceId, dictData.dbName, "move category:" + nodeToMove.name + " categ_id:" + categoryObjId);
        return boxiService.updateTreeNodeAndChangeFvs(nodeToMove);
    }

    public Pair<TreeNodeBean, TreeNodeBean> saveNewCategorization(Integer instanceId, final String categorizationName, Integer dictionaryParentNodeId) throws TeradataException {
        AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(getSchemaName(instanceId), tConnection);

        TreeNodeBean dictionaryParentNode = createBeanFromNamedQry("getBikNode", TreeNodeBean.class, false, dictionaryParentNodeId);
        Integer categorizationKindId = execNamedQuerySingleValAsInteger("getBikNodeKindIdByCode", false, "id", BIKConstants.NODE_KIND_LISA_CATEGORIZATION);
        Integer categoryKindId = execNamedQuerySingleValAsInteger("getBikNodeKindIdByCode", false, "id", BIKConstants.NODE_KIND_LISA_CATEGORY);
        LisaNodeCreator creator = new LisaNodeCreator(tConnection, boxiService, getSchemaName(instanceId));
        String dictionaryName = BaseUtils.safeToString(execNamedQuerySingleVal("getLisaObjIdForCategoryNode", false, null, dictionaryParentNodeId));
        String unassignedCategName = execNamedQuerySingleVal("getAdminSingleValue", false, null, LisaConstants.PARAM_UNASSIGNED_CATEGORY_NAME);
        LisaDictionaryBean dictData = getDictionaryData(dictionaryParentNodeId);
        checkCategoryVersioningAndUpdateBikDb(instanceId, tConnection, queryGenerator, dictData);
        Pair<TreeNodeBean, TreeNodeBean> result = creator.createCategorization(categorizationName, dictionaryParentNode, categorizationKindId, categoryKindId, unassignedCategName, dictData.isVersioned, getAdhocDao());
        saveTeradataLog(instanceId, dictionaryName, "created new categorization: " + categorizationName);
        return result;
    }
//    @Override
//    public void updateDictionaryContent(Map<String, LisaDictionaryValue> record, Integer nodeId, boolean editFromDictonary) throws TeradataException {
//        AuthorizedTeradataConnection tConnection = authorizeConnection();
//        String dictionaryName = getDictionaryName(editFromDictonary, nodeId);
//        LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(getSchemaName(), tConnection);
//        generateAndUpdateContentForRecord(queryGenerator, record, dictionaryName, tConnection);
//    }

    @Override
    public void updateDictionaryContents(Integer instanceId, Set<Map<String, LisaDictionaryValue>> records, Integer nodeId, boolean editFromDictonary) throws TeradataException {
        updateDictionaryContents(instanceId, null, records, nodeId, editFromDictonary);
    }

    @Override
    public void updateDictionaryContents(Integer instanceId, List<LisaDictColumnBean> columnInfo, Set<Map<String, LisaDictionaryValue>> records, Integer nodeId, boolean editFromDictonary) throws TeradataException {
        AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        String dictionaryName = getDictionaryName(editFromDictonary, nodeId);
        LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(getSchemaName(instanceId), tConnection);
        for (Map<String, LisaDictionaryValue> record : records) {
            convertToStringLDVType(instanceId, record, columnInfo);
            generateAndUpdateContentForRecord(instanceId, nodeId, queryGenerator, record, dictionaryName, tConnection);
        }
    }

    @Override
    public Boolean validateRecords(Integer instanceId, List<LisaDictColumnBean> columnInfo, Set<Map<String, LisaDictionaryValue>> records, Integer nodeId, boolean editFromDictonary) throws TeradataException {
//        AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
//        String dictionaryName = getDictionaryName(editFromDictonary, nodeId);
//        LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(getSchemaName(instanceId), tConnection);

        for (LisaDictColumnBean column : columnInfo) {
            if (column.validateLevel == LisaConstants.VALIDATE_FROM_LIST) {
                List<String> validationList = getValidationList(column.id);
                for (Map<String, LisaDictionaryValue> aRecord : records) {
                    String value = LisaConstants.NULL_VALUE;
                    if (aRecord.get(column.nameInDb) != null && aRecord.get(column.nameInDb).getValue() != null) {
                        value = (String) aRecord.get(column.nameInDb).getValue();
                    }
                    boolean isOk = false;
                    for (String s : validationList) {
                        if (s.equals(value)) {
                            isOk = true;
                            break;
                        }
                    }
                    if (!isOk) {
                        return false;
                    }
                }
            } else if (column.validateLevel == LisaConstants.VALIDATE_FROM_DICT) {
                for (Map<String, LisaDictionaryValue> aRecord : records) {
                    String value = null;
                    if (aRecord.get(column.nameInDb) != null && aRecord.get(column.nameInDb).getValue() != null) {
                        value = (String) aRecord.get(column.nameInDb).getValue();
                    }
                    List<String> distinctColumnValues = getDistinctColumnValues(instanceId, column, value, 1, true);
                    if (BaseUtils.isCollectionEmpty(distinctColumnValues) || !BaseUtils.safeEqualsStr(value, distinctColumnValues.get(0), false)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public void deleteDictionaryContent(Integer instanceId, Set<Map<String, LisaDictionaryValue>> records, Integer nodeId, boolean editFromDictonary) throws TeradataException {
        AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        String dictionaryName = getDictionaryName(editFromDictonary, nodeId);
        LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(getSchemaName(instanceId), tConnection);
        Pair<String, String> r = queryGenerator.deleteDictionaryQuery(records, dictionaryName, getEditableDictionaryColumns(dictionaryName));
        final String query = r.v1;
        String logId = r.v2;

        //Dodaj QUERY_BAND
        tConnection.execQryWithoutParamsWithQueryBand(query, getQueryBand());
//        tConnection.execQry(query);
        saveTeradataLog(instanceId, dictionaryName, "deleted entry" /* i18n: no */ + " " + logId);
    }

    @Override
    public void insertDictionaryContent(Integer instanceId, Map<String, LisaDictionaryValue> record, Integer nodeId, boolean editFromDictonary) throws TeradataException {
        insertDictionaryContent(instanceId, null, record, nodeId, editFromDictonary);
    }

    @Override
    public void insertDictionaryContent(Integer instanceId, List<LisaDictColumnBean> columnInfo, Map<String, LisaDictionaryValue> record, Integer nodeId, boolean editFromDictonary) throws TeradataException {
        // działa dla słowników bez kategorii
        // założenie, że nie ma sztucznych kluczy (fizycznie, albo wśród kolumn)
        String dictionaryName = getDictionaryName(editFromDictonary, nodeId);
        insertDictionaryContentInternal(instanceId, columnInfo, record, nodeId, dictionaryName, editFromDictonary);
    }

    private void insertDictionaryContentInternal(Integer instanceId, List<LisaDictColumnBean> columnInfo, Map<String, LisaDictionaryValue> record, Integer nodeId, String dictionaryName, boolean editFromDictonary) {
        // założenie, że rekord jest poprawny, czyli juz sprawdzona była walidacja, zobacz klas ListBoxInPopup, która uzywana była w nowe LisaDictionaryContentDisplayedByDataGrid
        convertToStringLDVType(instanceId, record, columnInfo);
        AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(getSchemaName(instanceId), tConnection);
        Pair<String, String> r = queryGenerator.insertDictionaryQuery(record, dictionaryName, getEditableDictionaryColumns(dictionaryName));
        final String query = r.v1;
        String logId = r.v2;
        //Dodaj QUERY_BAND

        tConnection.execQryWithoutParamsWithQueryBand(query, getQueryBand());
//        tConnection.execQry(query);
        saveTeradataLog(instanceId, dictionaryName, "inserted entry" /* i18n: no */ + " " + logId);
    }

    public void authorizeTeradataConnection(Integer instanceId, String username, String password) throws TeradataException {
        authorizeTeradataConnection(instanceId, username, password, null);
    }

    public void authorizeTeradataConnection(Integer instanceId, String username, String password, TeradataConnectionConfig tdCfg) throws TeradataException {
        Pair<String, TeradataConnection> conn = teradataConnectionPools.get(combineLisaInstanceIdAndUsername(instanceId, username));
        if (conn == null) {
            registerNewConnection(instanceId, username, password, tdCfg);
        } else {
//            remover.removeConnection(instanceId);
            new ConnectionRemover(instanceId).removeConnection();
            registerNewConnection(instanceId, username, password, tdCfg);
//            if(conn.v2.isConnectionOpenAndValid()){
//                confirmPassword(conn, password);
//            }else{
//                remover.removeConnection();
//                registerNewConnection(username, password, tdCfg);
//            }
        }

        BOXISessionData sd = getSessionData();
        if (sd.lisaTeradataLogins == null) {
            sd.lisaTeradataLogins = new HashMap<String, String>();
        }
        //sd.lisaTeradataLogins.put(getCurrentDatabase().get(), username);
        sd.lisaTeradataLogins.put(combineSingleBiksDbForCurrentRequestAndLisaInstanceId(instanceId), combineLisaInstanceIdAndUsername(instanceId, username));
        //getSessionData().lisaTeradataLogins.put(getCurrentDatabase().get(), username);
    }

    private Pair<String, TeradataConnection> registerNewConnection(Integer instanceId, String username, String password, TeradataConnectionConfig tdCfg) throws TeradataException {
        if (tdCfg == null) {
            tdCfg = generateTeradataConnConfig(instanceId, username, password);
        }
        TeradataConnection conn = new TeradataConnection(tdCfg);
        //conn.setAutoCommit(true);
        Pair<String, TeradataConnection> ret = new Pair<String, TeradataConnection>(password, conn);
        teradataConnectionPools.put(combineLisaInstanceIdAndUsername(instanceId, username), ret);
        return ret;
    }

    private void confirmPassword(Pair<String, TeradataConnection> conn, String password) throws TeradataException {
        if (!conn.v1.equals(password)) {
            throw new TeradataException(I18n.proszePodacPoprawnyLoginIHasloDo.get() /* i18n:  */ + " ");
        }
    }

//    public LisaTeradataConnectionBean getLisaConnectionData() {
//        return createBeanFromNamedQry("getLisaConnectionData", LisaTeradataConnectionBean.class, true);
//    }
    public void moveValuesToCategory(Integer instanceId, final Set<Map<String, LisaDictionaryValue>> values, Integer oldCategoryNodeId, Integer newCategoryNodeId) throws TeradataException {
        moveValuesToCategorySynch(instanceId, values, oldCategoryNodeId, newCategoryNodeId);
    }

    protected synchronized void moveValuesToCategorySynch(Integer instanceId, final Set<Map<String, LisaDictionaryValue>> values, Integer oldCategoryNodeId, Integer newCategoryNodeId) throws TeradataException {
        final AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        final LisaDictionaryBean dictData = getDictionaryData(newCategoryNodeId);
        final String schema = getSchemaName(instanceId);

        String objId = BaseUtils.safeToString(execNamedQuerySingleVal("getLisaCategorizationObjIdForCategoryNode", false, null, newCategoryNodeId));
        final Integer categorizationObjId = LisaUtils.fetchCategoryTypeObjIdAsInt(objId);
//        final Integer categoryOldObjId = fetchCategoryObjId(oldCategoryNodeId);
        final Integer categoryObjId = fetchCategoryObjId(newCategoryNodeId);
        final java.sql.Date today = new java.sql.Date(new java.util.Date().getTime());

        tConnection.execWithQueryBand(getQueryBand(), new IParametrizedContinuation<Connection>() {

            @Override
            public void doIt(Connection conn) {
                CallableStatement cs = null;
                try {
                    String errorMessage = null;
                    String prepareQuery = " call" /* i18n: no */ + " " + schema + "ch_kateg_list"
                            + (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES ? LisaTeradataQueryGenerator.VALIDATION_SUFFIX : "")
                            + "(?,?,?,?,?,?"
                            //+ ",?" //wyrzucić to gdy wejdzie szybka procedura zmian kategorii
                            + (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES ? ",?" : "")
                            + "); ";
                    try {
                        cs = conn.prepareCall(prepareQuery);
                        StringBuilder idList = new StringBuilder();
                        for (Map<String, LisaDictionaryValue> row : values) {
                            idList.append((idList.length() > 0 ? "," : ""));
                            idList.append(tConnection.valToString(row.get(dictData.columnRelName).getValue().toString()));
                        }

                        //zmiana pod nową szybką procedurę zmiany kategorii
                        cs.setString(1, LisaUtils.getTableNameWithoutDB(dictData.dbRelTable)); //p_kateg_rel_table
                        cs.setInt(2, categorizationObjId); //p_id_typ_kateg
                        cs.setString(3, dictData.columnRelName);//p_id_ref
                        cs.setString(4, (String) idList.toString());//p_id_list
                        cs.setInt(5, categoryObjId);//p_id_kateg
                        cs.setDate(6, today);//p_data_zmiany
                        if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
                            cs.registerOutParameter(7, java.sql.Types.VARCHAR);
                        }
                        cs.executeQuery();

                        if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
                            errorMessage = cs.getString(7);
                            if (errorMessage != null && errorMessage.length() > 0) {
                                throw new FoxyRuntimeException(errorMessage);
                            }
                        }
                    } finally {
                        if (cs != null) {
                            cs.close();
                        }
                    }
                } catch (SQLException ex) {
                    throw new FoxyRuntimeException(ex.getMessage());
                }
            }
        });

//        tConnection.execWithJdbcConnection(new IParametrizedContinuation<Connection>() {
//            @Override
//            public void doIt(Connection conn) {
//                CallableStatement cs = null;
//                Statement stmt = null;
//                String tmode = tConnection.getTeradataMode();
//
//                try {
//                    String errorMessage = null;
//                    String prepareQuery = " call" /* i18n: no */ + " " + schema + "ch_kateg_list"
//                            + (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES ? LisaTeradataQueryGenerator.VALIDATION_SUFFIX : "")
//                            + "(?,?,?,?,?,?"
//                            //+ ",?" //wyrzucić to gdy wejdzie szybka procedura zmian kategorii
//                            + (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES ? ",?" : "")
//                            + "); ";
//                    conn.commit();
//
//                    stmt = conn.createStatement();
//                    try {
//                        if (tmode.equals("TERA")) {
//                            stmt.execute("BT");
//                        }
//
//                        //Dodaj QUERY_BAND
////                        stmt.execute("SET QUERY_BAND='app=BIKS;' FOR TRANSACTION");
//                        conn.setAutoCommit(false);
//                        cs = conn.prepareCall(prepareQuery);
//
//                        StringBuilder idList = new StringBuilder();
//                        for (Map<String, LisaDictionaryValue> row : values) {
//                            idList.append((idList.length() > 0 ? "," : ""));
//                            idList.append(tConnection.valToString(row.get(dictData.columnRelName).getValue().toString()));
//                        }
//
//                        //zmiana pod nową szybką procedurę zmiany kategorii
//                        cs.setString(1, LisaUtils.getTableNameWithoutDB(dictData.dbRelTable)); //p_kateg_rel_table
//                        cs.setInt(2, categorizationObjId); //p_id_typ_kateg
//                        cs.setString(3, dictData.columnRelName);//p_id_ref
//                        cs.setString(4, (String) idList.toString());//p_id_list
//                        cs.setInt(5, categoryObjId);//p_id_kateg
//                        cs.setDate(6, today);//p_data_zmiany
//                        if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
//                            cs.registerOutParameter(7, java.sql.Types.VARCHAR);
//                        }
//                        cs.executeQuery();
//
//                        if (LisaTeradataQueryGenerator.VALIDATION_PROCEDURES) {
//                            errorMessage = cs.getString(7);
//                            if (errorMessage != null && errorMessage.length() > 0) {
//                                throw new FoxyRuntimeException(errorMessage);
//                            }
//                        }
//                    } finally {
//                        if (stmt != null) {
//                            stmt.execute("SET QUERY_BAND = NONE for TRANSACTION");
//                            if (tmode.equals("TERA")) {
//                                stmt.execute("ET");
//                            }
//                            stmt.close();
//                        }
//                        if (cs != null) {
//                            cs.close();
//                        }
//                    }
//                    conn.commit();
//                } catch (SQLException ex) {
//                    try {
//                        conn.rollback();
//                    } catch (SQLException exIn) {
//                        //Trzeba to przerobic, aby CallableStatement był w pl.trzy0.foxy.serverlogic.db, szybka poprawka dla Jerzego
//                        //inaczej sie nie wygrzebę z tego NAS-a, przy NAS etap2 to zmienić!!!!
//                    }
//                    throw new FoxyRuntimeException(ex.getMessage());
//                }
//            }
//        });
        saveTeradataLog(instanceId, dictData.dbName, "moved" /* i18n: no */ + " " + values.size() + " " + "entries to category with id" /* i18n: no */ + " =: " + categoryObjId);
    }
//    public void updateLisaConnectionData(LisaTeradataConnectionBean bean) {
//        execNamedCommand("updateLisaConnectionData", bean);
//    }

    private List<Map<String, LisaDictionaryValue>> convertToLDVMap(List<LisaDictColumnBean> columns, List<Map<String, Object>> objs) {
        List<Map<String, LisaDictionaryValue>> obis = new ArrayList<Map<String, LisaDictionaryValue>>();
        for (Map<String, Object> row : objs) {
            HashMap<String, LisaDictionaryValue> rowRev = new HashMap<String, LisaDictionaryValue>();
            for (Entry<String, Object> entry : row.entrySet()) {

                LisaDictionaryValue ldv;

                Object value = entry.getValue();
                if (value instanceof java.sql.Date) {
                    ldv = new DateLDV();
                    ldv.setValue(value);
                } else if (value instanceof java.sql.Timestamp) {
                    LisaDictColumnBean column = null;
                    for (LisaDictColumnBean col : columns) {
                        if (entry.getKey().equalsIgnoreCase(col.nameInDb)) {
                            column = col;
                            break;
                        }
                    }
                    String valueStr = BaseUtils.safeToStringDef(value, "");
                    ldv = new StringLDV();
                    if (column.decimalFractionalDigits == null || column.decimalFractionalDigits == 0) {
//                        ldv.setValue(LisaServerUtils.transcodeFromLatin1(valueStr.substring(0, valueStr.indexOf('.'))));
                        ldv.setValue(valueStr.substring(0, Math.min(valueStr.indexOf('.'), valueStr.length())));
                    } else {
//                        ldv.setValue(LisaServerUtils.transcodeFromLatin1(valueStr.substring(0, valueStr.indexOf('.') + 1 + column.decimalFractionalDigits)));

                        ldv.setValue(valueStr.substring(0, Math.min(valueStr.indexOf('.') + 1 + column.decimalFractionalDigits, valueStr.length())));
                    }
                } else if (value instanceof java.lang.Integer) {
                    ldv = new IntegerLDV();
                    ldv.setValue(value);
                } else {
                    ldv = new StringLDV();
                    if (value == null) {
                        ldv.setValue(null);
                    } else {
                        ldv.setValue(LisaServerUtils.transcodeFromLatin1(BaseUtils.safeToString(value)));
//                        ldv.setValue(BaseUtils.safeToString(value));
                    }
                }

                rowRev.put(entry.getKey(), ldv);
            }
            obis.add(rowRev);
        }
        return obis;
    }

    private AuthorizedTeradataConnection authorizeConnection(Integer instanceId) throws TeradataException {
        String teradataLogin = getLisaTeradataLogin(instanceId);

        if (teradataLogin == null || !teradataConnectionPools.containsKey(teradataLogin)) {
            //prompt client for proper db credentials
            throw new TeradataException(false);
        }
        Pair<String, TeradataConnection> passConn = teradataConnectionPools.get(teradataLogin);
        return new AuthorizedTeradataConnection(passConn.v2, new ConnectionRemover(instanceId), cancellableRunner);
    }

    //ww: to dopuszcza nulla, albo listę na której jest null - działa bezpiecznie
    // kod który tego używa
    @Override
    public Collection<? extends IFoxyTransactionalPerformer> getTransactionalPerformers() {
        return BaseUtils.projectToList(teradataConnectionPools.values(),
                BaseUtils.<String, TeradataConnection>getPairV2Projector());
    }

    private String getDictionaryName(Integer categoryNodeId) {
        String dictionaryName = execNamedQuerySingleVal("getLisaDictionaryNameForCategoryNode", false, null, categoryNodeId);
        return dictionaryName.toLowerCase();
    }

    private String getDictionaryNameForDicNode(Integer dicNodeId) {
        String dictionaryName = execNamedQuerySingleVal("getLisaDictionaryNameForDicNode", false, null, dicNodeId);
        return dictionaryName.toLowerCase();
    }

    private LisaDictionaryBean getDictionaryData(Integer categoryNodeId) {
        LisaDictionaryBean bean = createBeanFromNamedQry("getLisaDictionaryDataForCategoryNode", LisaDictionaryBean.class, false, categoryNodeId);
        bean.dbName = bean.dbName.toLowerCase();
        return bean;
    }

    private LisaDictionaryBean getDictionaryDataForDic(Integer dictonaryNodeId) {
        LisaDictionaryBean bean = createBeanFromNamedQry("getLisaDictionaryDataForDictonaryNode", LisaDictionaryBean.class, false, dictonaryNodeId);
        bean.dbName = bean.dbName.toLowerCase();
        return bean;
    }

    private String getDictionaryNameForCategoryType(Integer categoryTypeNodeId) {
        String dictionaryName = execNamedQuerySingleVal("getLisaDictionaryNameForCategoryNodeType", false, null, categoryTypeNodeId);
        return dictionaryName.toLowerCase();
    }

    private List<Map<String, Object>> getLatestDateForDictionaryValueMap(String schema, String primaryKeys, LisaDictionaryBean dictData, Integer categoryOldObjId, Integer categorizationObjId, AuthorizedTeradataConnection tConnection) throws TeradataException {
        LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(schema, tConnection);
        //List<Map<String, java.sql.Date>> listOfRightRolesOfUsersByIds = execNamedQueryCFN("getRightRolesOfUsersByIds", FieldNameConversion.ToJavaPropName);
        return tConnection.execQry(queryGenerator.getLatestDateForDictionaryValueQuery(dictData, primaryKeys, categorizationObjId, categoryOldObjId));
    }

    private void saveTeradataLog(Integer instanceId, String dictionaryName, String info) {
        TeradataLogBean bean = new TeradataLogBean();
        bean.dictionaryName = dictionaryName;
        bean.info = info;
        bean.teradataLogin = getLisaTeradataLogin(instanceId);
        SystemUserBean loggedUserBean = getLoggedUserBean();
        if (loggedUserBean != null) { // tf: dla niezalowowanego nie zapisujemy logów, bo tabela z logami wymaga podania id usera
            bean.systemUserId = loggedUserBean.id;
            execNamedCommand("saveTeradataLog", bean);
        }
    }

    @Override
    public String addNewDictionary(Integer instanceId, LisaDictionaryBean b) {
        final AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        final LisaTeradataQueryGenerator queryGen = new LisaTeradataQueryGenerator(getSchemaName(instanceId), tConnection);
        try {
            //run test query, to validate conn credentials
            tConnection.execQry(queryGen.getTeradataConnectionCheckQuery());
        } catch (RuntimeException ex) {
            throw new TeradataException(I18n.proszePodacPoprawnyLoginIHasloDo.get() /* i18n:  */ + " ");
        }

        checkCategoryVersioning(tConnection, queryGen, b);
        DictionaryGenerator generator = new DictionaryGenerator(tConnection, queryGen, getAdhocDao(), boxiService);
        return generator.generate(b);
    }

    private Integer fetchCategoryObjId(Integer categNodeId) {
        String s = BaseUtils.safeToString(execNamedQuerySingleVal("getLisaObjIdForCategoryNode", false, null, categNodeId));
        return LisaUtils.fetchCategoryObjId(s);
    }

    protected String getDictionaryName(boolean editFromDictonary, Integer nodeId) {
        String dictionaryName;
        if (editFromDictonary) {
            dictionaryName = getDictionaryNameForDicNode(nodeId);
        } else {
            dictionaryName = getDictionaryName(nodeId);
        }
        return dictionaryName;
    }

    protected void generateAndUpdateContentForRecord(Integer instanceId, Integer nodeId, LisaTeradataQueryGenerator queryGenerator, Map<String, LisaDictionaryValue> record, String dictionaryName, AuthorizedTeradataConnection tConnection) throws TeradataException {
        Pair<String, String> r = queryGenerator.updateDictionaryQuery(record, dictionaryName, getEditableDictionaryColumns(dictionaryName));
        final String query = r.v1;
        String logId = r.v2;
        //System.out.println(query);
        //Dodaj QUERY_BAND
        tConnection.execQryWithoutParamsWithQueryBand(query, getQueryBand());
        saveTeradataLog(getLisaInstanceIdByDictNodeId(nodeId), dictionaryName, "edited entry" /* i18n: no */ + " " + logId);
    }

    @Override
    public List<String> getDistinctColumnValues(Integer instanceId, LisaDictColumnBean column, String value, int numberFetchItem) {
        return getDistinctColumnValues(instanceId, column, value, numberFetchItem, false);
    }

    public List<String> getDistinctColumnValues(Integer instanceId, LisaDictColumnBean column, String value, int numberFetchItem, boolean isStrictlyEquals) {
        AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(getSchemaName(instanceId), tConnection);
        String query = queryGenerator.getDistinctColumsValueQuery(instanceId, column, value, numberFetchItem, isStrictlyEquals);
        List<Map<String, Object>> result = tConnection.execQry(query);
        return BaseUtils.projectToList(result,
                new BaseUtils.Projector<Map<String, Object>, String>() {
                    public String project(Map<String, Object> val) {
                        return BaseUtils.safeToString(val.get("obj_id"));
                    }
                });
    }

//    @Override
//    public Integer checkIsUniqueColumn(String dictName, String colName) {
//        AuthorizedTeradataConnection tConnection = authorizeConnection();
//        LisaTeradataQueryGenerator queryGen = new LisaTeradataQueryGenerator(getSchemaName(), tConnection);
//        List<Map<String, Object>> ans = tConnection.execQry(queryGen.checkUniqueColumnQuery(dictName, colName));
//        return ans == null ? 0 : ans.size();
//    }
    @Override
    public String getOriginalValueFromReference(Integer instanceId, LisaDictColumnBean columnBean, String value) {
        AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        LisaTeradataQueryGenerator queryGen = new LisaTeradataQueryGenerator(getSchemaName(instanceId), tConnection);
        return tConnection.execQrySingleVal(queryGen.originalValueFromReferenceQuery(instanceId, columnBean, value)).toString();
    }

    private void convertToStringLDVType(Integer instanceId, Map<String, LisaDictionaryValue> record, List<LisaDictColumnBean> columnInfo) {
        if (columnInfo == null) {
            return;
        }

        for (LisaDictColumnBean columnBean : columnInfo) {
            String value = null;
            Object obj = record.get(columnBean.nameInDb).getValue();
            if (obj instanceof java.util.Date) {
                value = LisaServerUtils.getDateStringInDefaultDateFormat((java.util.Date) obj);
            } else {
                if (obj != null) {
                    value = BaseUtils.safeToString(obj);
                }
            }

            if (columnBean.dictValidation != null) {
                value = getOriginalValueFromReference(instanceId, columnBean, value);
            }

            if (columnBean.isLatin != null && columnBean.isLatin && !BaseUtils.isStrEmptyOrWhiteSpace(value)) {
                value = BaseUtils.changePolishChar2Latin(value);
            }
            record.put(columnBean.nameInDb, new StringLDV(value));
        }
    }

    @Override
    public List<String> getValidationList(int columnIdInBIK) {
        return execNamedQuerySingleColAsList("getValidationList", "value", columnIdInBIK);
    }

    @Override
    public void addValidationItem(int columnIdInBIK, String value) {
        execNamedCommand("addValidationItem", columnIdInBIK, value);
    }

    @Override
    public void deleteValidationItem(int columnIdInBIK, String value) {
        execNamedCommand("deleteValidationItem", columnIdInBIK, value);
    }

    private Map<String, Class<?>> createHeaderTypeHint(List<LisaDictColumnBean> columns) {
        HashMap<String, Class<?>> columnType = new HashMap<String, Class<?>>();
        for (LisaDictColumnBean column : columns) {
            switch (LisaDataType.getValueFor(column.dataType)) {
                case DA:
                    columnType.put(column.nameInDb, java.sql.Date.class);
                    break;
                case I1:
                case I2:
                case I8:
                case I:
                    columnType.put(column.nameInDb, java.lang.Integer.class);
                    break;
                default:
                    columnType.put(column.nameInDb, String.class);
            }
        }
        return columnType;
    }

    private boolean isValidFromList(Integer instanceId, LisaDictColumnBean column, String value) {
        List<String> validationList = getValidationList(column.id); //zalozone, ze tylko walidacja dziala z wartosciami tekstowymi
        return validationList.isEmpty() || (value != null && validationList.indexOf(value) >= 0);
    }

    private boolean isValidFromDict(Integer instanceId, Integer nodeId, LisaDictColumnBean column, String value) {
        //TO-DO: zalozmy, ze walidacja rzadko jest wykorzystana, dlatego kazdym razem wywolana Teradata, jesli nie to trzeba poprawic
        return !BaseUtils.isCollectionEmpty(getDistinctColumnValues(instanceId, column, value, 1));
    }

//    private boolean checkExistInTeradata(String tableName, List<Map<String, String>> lines, List<LisaDictColumnBean> indexColumns, Comparator<List<String>> comparator) {
//        long start = System.currentTimeMillis();
//        List<List<String>> list = loadIndexiesInTeradata(tableName, indexColumns);
//        System.out.println("loadIndexiesInTeradata: " + (System.currentTimeMillis() - start) / 1000.0 + "s");
//        Collections.sort(list, comparator);
//        for (Map<String, String> line : lines) {
//            List<String> indexValue = new ArrayList<String>();
//            for (LisaDictColumnBean column : indexColumns) {
//                indexValue.add(line.get(column.nameInDb));
//            }
//            if (Collections.binarySearch(list, indexValue, comparator) >= 0) {
//                return false;
//            }
//        }
//        return true;
//    }
    private Map<String, Integer> loadAllPkValueInTeradata(Integer instanceId, Integer nodeId, final String tableName, final List<LisaDictColumnBean> pkColumnsList) throws TeradataException {
        final Map<String, Integer> allPkValues = new HashMap<String, Integer>();
        AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        final LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(getSchemaName(instanceId), tConnection);
        final Long nRecords = (Long) tConnection.execQrySingleVal(queryGenerator.countNumberOfDictionaryRecordsQuery(tableName));

        tConnection.execWithJdbcConnection(new IParametrizedContinuation<Connection>() {

            @Override
            public void doIt(Connection conn) {
                PreparedStatement ps = null;
                try {
                    //Dodaj QUERY_BAND ? -> nie
                    ps = conn.prepareStatement(queryGenerator.getDictionaryPkValuesPreparedStatement(tableName, pkColumnsList));
                    for (int pageNr = 0; pageNr < (nRecords - 1) / LisaConstants.EXPORT_PACK_SIZE + 1; pageNr++) {
                        int offset = pageNr * LisaConstants.EXPORT_PACK_SIZE;

                        ps.setInt(1, offset + 1);
                        ps.setInt(2, offset + LisaConstants.EXPORT_PACK_SIZE);
                        ResultSet rs = ps.executeQuery();
                        while (rs.next()) {
                            List<String> pkValue = new ArrayList<String>();
                            for (LisaDictColumnBean column : pkColumnsList) {
                                pkValue.add(rs.getString(column.nameInDb));
                            }
                            String keyValueStr = LisaUtils.mergeToString(pkValue);
                            Integer cnt = allPkValues.containsKey(keyValueStr) ? allPkValues.get(keyValueStr) : 0;
                            allPkValues.put(keyValueStr, cnt + 1);//lepiej uzywac hashcode?
                        }
                    }
                } catch (SQLException ex) {
                    throw getRootMessageAndThowTeradataException(ex);
                } finally {
                    if (ps != null) {
                        try {
                            ps.close();
                        } catch (SQLException ex) {
                            throw getRootMessageAndThowTeradataException(ex);
                        }
                    }
                }
            }
        });
        return allPkValues;
    }
    //    private boolean checkExistInTeradata(final String tableName, final List<Map<String, String>> lines, final List<LisaDictColumnBean> indexColumns) {
    //        AuthorizedTeradataConnection tConnection = authorizeConnection();
    //        final LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(getSchemaName(), tConnection);
    //
    //        return tConnection.execWithJdbcConnection(new IParametrizedContinuationWithReturn<Connection, Boolean>() {
    //
    //            @Override
    //            public Boolean doIt(Connection connection) {
    //                try {
    //                    PreparedStatement ps = connection.prepareStatement(queryGenerator.getSelectPKQuery(tableName, indexColumns));
    //                    for (Map<String, String> record : lines) {
    //                        for (int colId = 0; colId < indexColumns.size(); colId++) {
    //                            LisaDictColumnBean column = indexColumns.get(colId);
    //                            String value = record.get(column.nameInDb);
    //
    //                            setParametr4PreparedStatement(ps, colId + 1, column, value);
    //                        }
    //                        ResultSet rs = ps.executeQuery();
    //                        if (rs.next()) {
    //                            return false;
    //                        }
    //                    }
    //                    return true;
    //                } catch (SQLException ex) {
    //                    String msg = null;
    //                    for (SQLException exIt = ex; exIt != null; exIt = exIt.getNextException()) {
    //                        exIt.printStackTrace();
    //                        msg = exIt.getMessage();
    //                    }
    //                    throw new FoxyRuntimeException(msg);
    //                }
    //            }
    //        });
    //
    //    }

    private TeradataException getRootMessageAndThowTeradataException(Exception ex) {
        String msg = ex.getMessage();
        if (ex instanceof SQLException) {
            for (SQLException exIt = (SQLException) ex; exIt != null; exIt = exIt.getNextException()) {
                msg = exIt.getMessage();
            }
        }
        return new TeradataException(msg);
    }

    private String extractPkValue(Map<String, String> record, List<LisaDictColumnBean> pkColumnsList) {
        List<String> pkValue = new ArrayList<String>();
        boolean allBlank = true;
        for (LisaDictColumnBean column : pkColumnsList) {
            String value = record.get(column.nameInDb);
            if (!BaseUtils.isStrEmptyOrWhiteSpace(value)) {
                allBlank = false;
            }
            pkValue.add(value);
        }
        return (allBlank ? "" : LisaUtils.mergeToString(pkValue));
    }

    private boolean validate(Integer instanceId, Integer nodeId, Map<String, String> record, List<LisaDictColumnBean> columnsList) {
        for (LisaDictColumnBean column : columnsList) {
            if ((column.validateLevel == LisaConstants.VALIDATE_FROM_DICT && !isValidFromDict(instanceId, nodeId, column, record.get(column.nameInDb)))
                    || (column.validateLevel == LisaConstants.VALIDATE_FROM_LIST && !isValidFromList(instanceId, column, record.get(column.nameInDb)))) {
                return false;
            }
        }
        return true;
    }

//    private void deleteOldRecords(String tableName, List<LisaDictColumnBean> columnsList) {
//    }
    private void checkCategoryVersioning(AuthorizedTeradataConnection tConnection, LisaTeradataQueryGenerator queryGenerator, LisaDictionaryBean dictData) {
        dictData.isVersioned = ((tConnection.execQrySingleValEx(queryGenerator.checkTableHasColumn(dictData.dbNameReal, "data_od"), true) != null)
                && (tConnection.execQrySingleValEx(queryGenerator.checkTableHasColumn(dictData.dbNameReal, "data_do"), true) != null) ? 1 : 0);
    }

    private void checkCategoryVersioningAndUpdateBikDb(Integer instanceId, AuthorizedTeradataConnection tConnection, LisaTeradataQueryGenerator queryGenerator, LisaDictionaryBean dictData) {
        if (dictData.isVersioned == null) {
            checkCategoryVersioning(tConnection, queryGenerator, dictData);
            Integer treeId = execNamedQuerySingleValAsInteger("getTreeId", false, null, BIKConstants.TREE_CODE_DICTIONARIES_DWH);
            Integer dictNodeId = execNamedQuerySingleValAsInteger("getLisaNodeIdForObjId", true, null, treeId, dictData.dbName, instanceId);

            execNamedCommand("setDictionaryCategoryIsVersioned", dictNodeId, dictData.isVersioned);
        }
    }

    private LisaInstanceInfoBean getLisaInstanceInfoById(Integer instanceId) {
        return createBeanFromNamedQry("getLisaInstanceInfoById", LisaInstanceInfoBean.class, false, instanceId);
    }

    private Integer getLisaInstanceIdByDictNodeId(Integer nodeId) {
        return execNamedQuerySingleValAsInteger("getLisaInstanceIdByDictNodeId", true, null, nodeId);
    }

//    private List<LisaDictColumnBean> addInstanceId2DictValidation(List<LisaDictColumnBean> columnInfo, Integer instanceId) {
//        if (!BaseUtils.isCollectionEmpty(columnInfo)) {
//            for (LisaDictColumnBean col : columnInfo) {
//                if (!BaseUtils.isStrEmptyOrWhiteSpace(col.dictValidation)) {
//                    col.dictValidation = String.valueOf(instanceId) + "." + col.dictValidation;
//                }
//            }
//        }
//        return columnInfo;
//    }
//    private Integer getInstanceIdByNodeId(Integer nodeId) {
//        Integer parentId = nodeId;
//        LisaInstanceInfoBean bean = null;
//        while (parentId != null && bean == null) {
//            bean = createBeanFromNamedQry("getLisaInstanceInfoByNodeId", LisaInstanceInfoBean.class, true, parentId);
//            parentId = execNamedQuerySingleVal("getParentNodeId", false, null, parentId);
//        }
//        if (bean != null) {
//            execNamedCommand("addDict2Instance", nodeId, bean.id);
//        }
//        return bean.id;
//    }
    public class ConnectionRemover {

        protected Integer instanceId;

        public ConnectionRemover(Integer instanceId) {
            this.instanceId = instanceId;
        }

        public void removeConnection() {
            String login = getLisaTeradataLogin(instanceId);
            if (login != null) {
                // TF -> podwójne sprawdzenie, tak dla absolutnej pewności? ;)
//                if (login != null) {
                Pair<String, TeradataConnection> connPair = teradataConnectionPools.remove(login);
                if (connPair != null) {
                    TeradataConnection c = connPair.v2;
                    c.destroy();
                }
//                }
            }
        }

        public void logError(String info) {
            saveTeradataLog(instanceId, "ERROR", info);
        }
    }

    public class CancallableRunner {

        public <T> T runCancellableJob(String ticket, IBeanConnectionThreadHandle handle, IContinuationWithReturn<T> cont) {
            return runCancellableDBJob(ticket, handle, cont);
        }
    }

    @Override
    public List<LisaLogBean> getLisaLogs(LisaGridParamBean params) {
        return createBeansFromNamedQry("getLisaLogs", LisaLogBean.class, params);
    }

    @Override
    public void deleteDictionary(Integer instanceId, String dictionaryName) throws TeradataException {
        Integer dictNodeId = execNamedQuerySingleValAsInteger("getLisaDictionaryNodeID", true, "id", instanceId, dictionaryName);
        execNamedCommand("deleteLisaOldDictionaryMetadata", dictionaryName, dictNodeId);

        if (dictNodeId != null) {
            TreeNodeBean nodeToDelete = boxiService.getNodeBeanById(dictNodeId, false);
            boxiService.deleteTreeNode(nodeToDelete);
        }
    }

//    @Override
//    public Boolean checkLisaUnassignedCategory() throws TeradataException {
//        try {
//
//            SystemUserBean sub = getLoggedUserBean();
//            if (!initTeradataConnectionOnDefaultUser() || sub == null) {
//                return false;
//            }
//
//            AuthorizedTeradataConnection tConnection = authorizeConnection();
//            LisaTeradataQueryGenerator queryGen = new LisaTeradataQueryGenerator(getSchemaName(), tConnection);
//
//            String query = queryGen.checkLisaUnassignedCategoryQuery(/*sub.loginName*/sub.ssoLogin);
//
//            List<LisaUnassignedCategory> unassignedCategoryList = tConnection.createBeansFromQry(query, LisaUnassignedCategory.class);
//
//            return insertUnassignedCategoryNews(unassignedCategoryList, sub.id);
//        } finally {
//            remover.removeConnection();
//        }
//    }
    private boolean insertUnassignedCategoryNews(List<LisaUnassignedCategory> unassignedCategoryList, int loggedUserId) {

        if (unassignedCategoryList == null || unassignedCategoryList.isEmpty()) {
            return false;
        }

        List<Map<String, Object>> toCategorizeList = execNamedQueryCFN("getLisaToCategorize", FieldNameConversion.ToJavaPropName, BIKConstants.TREE_CODE_DICTIONARIES_DWH, unassignedCategoryList);
        if (toCategorizeList == null || toCategorizeList.isEmpty()) {
            return false;
        }

        StringBuilder sb = new StringBuilder();
        sb.append(I18n.elementyDoSkategoryzowania.get() + ":<br/>");
        for (Map<String, Object> cat : toCategorizeList) {
            sb.append("<a href=\"#" + BIKConstants.TREE_CODE_DICTIONARIES_DWH + "/" + cat.get("id") + "\">" + cat.get("dbname") + "-->" + cat.get("kategname") + " " + I18n.doSkategoryzawania.get() + " " + cat.get("counter") + " " + I18n.elementow.get() + " </a><br/>");
        }

        NewsBean news = new NewsBean();
        news.title = I18n.elementyDoSkategoryzowaniaWSlowniku.get();
        news.text = sb.toString();
        news.authorId = loggedUserId;

        boxiService.addNewsForUser(news, loggedUserId);

        return true;
    }

    @Override
    public Boolean deleteAllTeredataSessions() throws TeradataException {

        for (Iterator<Pair<String, TeradataConnection>> iter = teradataConnectionPools.values().iterator(); iter.hasNext();) {
            TeradataConnection c = iter.next().v2;
            if (c != null) {
                c.destroy();
            }
        }
        teradataConnectionPools.clear();
        return true;
    }

    @Override
    public List<ErwinDataModelProcessObjectsRel> getDataModelProcessObjectsRel(int nodeId, String areaName, boolean dashboardObjects) {
        Integer instanceId = getLisaInstanceIdByDictNodeId(nodeId);
        List<ErwinDataModelProcessObjectsRel> listErwinProcess = createBeansFromNamedQry("getDataModelProcessObjectsRel", ErwinDataModelProcessObjectsRel.class, nodeId, areaName, dashboardObjects);

        if (!initTeradataConnectionOnDefaultUser(nodeId)) {
            return listErwinProcess;
        }

        try {
            AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
            LisaTeradataQueryGenerator queryGen = new LisaTeradataQueryGenerator(getSchemaName(instanceId), tConnection);
            List<String> processNameList = new ArrayList<String>();
            for (ErwinDataModelProcessObjectsRel edmoor : listErwinProcess) {
                processNameList.add(edmoor.symbProc);
            }

            List<ErwinProcessStatus> erwinProcessStatusList = new ArrayList<ErwinProcessStatus>();;
            if (processNameList.size() > 0) {
                String query = queryGen.getProcessStatusQuery(processNameList);
                erwinProcessStatusList = tConnection.createBeansFromQry(query, ErwinProcessStatus.class);
            }
            HashMap<String, ErwinProcessStatus> mapStatus = new HashMap<String, ErwinProcessStatus>();
            for (ErwinProcessStatus erProc : erwinProcessStatusList) {
                mapStatus.put(erProc.symbProc, erProc);
            }

            for (ErwinDataModelProcessObjectsRel edmpor : listErwinProcess) {
                String symbProc = edmpor.symbProc;
                if (mapStatus.containsKey(symbProc)) {
                    ErwinProcessStatus eps = mapStatus.get(symbProc);
                    edmpor.procStatus = eps.stanProc.trim();
                    edmpor.procDate = eps.czasZlec;
                }
            }
        } catch (Exception ex) {
            throw getRootMessageAndThowTeradataException(ex);
        } finally {
            new ConnectionRemover(instanceId).removeConnection();
        }
        //losowy status - do testowania tylko
//        String statusStr = "ZDBRCEIJAWXF";
//        Random random = new Random(System.currentTimeMillis());
//        for (ErwinDataModelProcessObjectsRel process : listErwinProcess) {
//            process.procStatus = "" + statusStr.charAt(random.nextInt(statusStr.length()));
//        }
        return listErwinProcess;
    }

    protected boolean initTeradataConnectionOnDefaultUser(Integer nodeId) throws TeradataException {
        String teradataUser = execNamedQuerySingleVal("getAdminSingleValue", false, "value", "teradata.user");
        String teradataPassword = execNamedQuerySingleVal("getAdminSingleValue", false, "value", "teradata.password");

        if (teradataUser.isEmpty() || teradataPassword.isEmpty()) {
            return false;
        }

        Integer instanceId = getLisaInstanceIdByDictNodeId(nodeId);
        TeradataConnectionConfig tdCfg = generateTeradataConnConfig(instanceId, teradataUser, teradataPassword);
        if (tdCfg.server == null || tdCfg.server.isEmpty()) {
            return false;
        }

        authorizeTeradataConnection(instanceId, teradataUser, teradataPassword, tdCfg);

        return true;
    }

    @Override
    public LisaAuthenticationBean getLisaAuthentication(int nodeId) {
        LisaAuthenticationBean bean = createBeanFromNamedQry("getLisaAuthentication", LisaAuthenticationBean.class, true, nodeId);
        if (bean == null) {
            bean = new LisaAuthenticationBean();
            bean.nodeId = nodeId;
            bean.loginMode = AuthenticationMode.ASK_EVERY_TIME.mode;
            bean.teradataLogin = "";
            bean.teradataPwd = "";
        }
        return bean;
    }

    @Override
    public void saveOrAddLisaAuthentication(LisaAuthenticationBean bean) {
        execNamedCommand("saveOrAddLisaAuthentication", bean);
    }

    @Override
    public Boolean tryLoginToTeradata(Integer instanceId, Integer nodeId) throws TeradataException {
        do {
            LisaAuthenticationBean bean = getLisaAuthentication(nodeId);
            if (instanceId != null && AuthenticationMode.USE_SAVED_LOGIN.mode == bean.loginMode) {
                Pair<String, TeradataConnection> conn = teradataConnectionPools.get(combineLisaInstanceIdAndUsername(instanceId, bean.teradataLogin));
                if (conn == null) {
                    conn = registerNewConnection(instanceId, bean.teradataLogin, bean.teradataPwd, null);
                }
                try {
                    if (conn.v2.isConnectionOpenAndValid()) {
                        return true;
                    }
                } catch (RuntimeException e) {
                    nodeId = execNamedQuerySingleVal("getParentNodeId", false, null, nodeId);
                }
            } else {
                nodeId = execNamedQuerySingleVal("getParentNodeId", false, null, nodeId);
            }
        } while (nodeId != null);
        throw new TeradataException();
    }

    @Override
    public void addLisaFolder(int parentNodeId, String folderName) {
        execNamedCommand("addLisaFolder", parentNodeId, folderName);
    }

    @Override
    public String exportDictionary(Integer instanceId, Integer nodeId, List<String> mappedNames) {
        final String tableName = getDictionaryName(nodeId);
        final List<LisaDictColumnBean> columnsList = getDictionaryColumnsByNodeId(nodeId);

        AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(getSchemaName(instanceId), tConnection);

        Long nRecords = (Long) tConnection.execQrySingleVal(queryGenerator.countNumberOfDictionaryRecordsQuery(tableName));
        BufferedOutputStream fileOut = null;
        String systemFileName = null;
        //        Long start = System.currentTimeMillis();
        try {
            File dir = new File(dirForUpload);
            File file = File.createTempFile(BIKConstants.NAS_FILE_PREFIX, ".xlsx", dir);

            systemFileName = file.getName();
            fileOut = new BufferedOutputStream(new FileOutputStream(file));
            // write to file
            XlsxWriter writer = new XlsxWriter(mappedNames);
            for (int pageNr = 0; pageNr < (nRecords - 1) / LisaConstants.EXPORT_PACK_SIZE + 1; pageNr++) {
                int offset = pageNr * LisaConstants.EXPORT_PACK_SIZE;
                List<Map<String, Object>> content = tConnection.execQry(queryGenerator.getDictionaryRecordsQuery(tableName, columnsList, offset, LisaConstants.EXPORT_PACK_SIZE));
                List<Map<String, LisaDictionaryValue>> convertedContent = convertToLDVMap(columnsList, content);
                content.clear();
                for (Map<String, LisaDictionaryValue> val : convertedContent) {
                    Map<String, Object> o = new HashMap<String, Object>();
                    for (String key : val.keySet()) {
                        o.put(key, val.get(key).getValue());
                    }
                    content.add(o);
                }
                writer.writeContent(content);
//                System.out.println("Page " + pageNr + ": Done!");
            }
            writer.closeWorkbook(fileOut);
            fileOut.close();
            return systemFileName;
        } catch (Exception ex) {
            throw getRootMessageAndThowTeradataException(ex);
        } finally {
            if (fileOut != null) {
                try {
                    fileOut.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
//        System.out.println("Time = " + (System.currentTimeMillis() - start) / (1000.0 * 60));
    }

    @Override
    public List<String> extractColumnNamesFromFile(String serverFileName) {
        try {
            File file = new File(dirForUpload, serverFileName);
            XlsxReader reader = new XlsxReader(file, null);
            reader.close();
            return reader.getHeaderNames();
        } catch (Exception ex) {
            throw getRootMessageAndThowTeradataException(ex);
        }
    }

    @Override
    public void importDictionary(Integer instanceId, int nodeId, final Map<String, String> mappedHeaders, final String serverFileName, final ImportMode importMode) throws TeradataException {
        AuthorizedTeradataConnection tConnection = authorizeConnection(instanceId);
        final LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(getSchemaName(instanceId), tConnection);
        final String tableName = getDictionaryName(nodeId);
        final List<LisaDictColumnBean> columnsList = getDictionaryColumnsByNodeId(nodeId);
        final List<LisaDictColumnBean> pkColumnsList = LisaUtils.extractPkColumns(columnsList);
        final XlsxReader reader = new XlsxReader(new File(dirForUpload, serverFileName), mappedHeaders);
        final List<Map<String, String>> content = reader.getContent();

//        Map<String, Class<?>> columnsType = createHeaderTypeHint(columnsList);
        tConnection.execWithJdbcConnection(new IParametrizedContinuation<Connection>() {

            @Override
            public void doIt(Connection conn) {
                PreparedStatement ips = null;
                PreparedStatement dps = null;
                PreparedStatement ups = null;
                try {
                    conn.setAutoCommit(false);
                    blockTable(conn, queryGenerator, tableName);
                    //Dodaj QUERY_BAND ? -> nie
                    ips = conn.prepareStatement(queryGenerator.insertRecordsPreparedStatment(tableName, columnsList));
                    for (int colId = 0; colId < columnsList.size(); colId++) {
                        LisaDictColumnBean column = columnsList.get(colId);
                        column.sqlDataType = ips.getParameterMetaData().getParameterType(colId + 1);
                    }

                    List<String> allPkList = null;
                    BaseTeradataBatchWorker worker = null;

                    if (importMode == ImportMode.DELETE) {
                        allPkList = execNamedQuerySingleColAsList("getImportedPkValues", null, serverFileName, LisaConstants.IMPORT_ACTION_DELETE);
                        dps = conn.prepareStatement(queryGenerator.deleteRecordPreparedStatement(tableName, pkColumnsList));
                        worker = new TeradataBatchDeleteWorker(dps, columnsList);
                    } else if (importMode == ImportMode.ADD) {
                        allPkList = execNamedQuerySingleColAsList("getImportedPkValues", null, serverFileName, LisaConstants.IMPORT_ACTION_ADD);
                        worker = new TeradataBatchInsertWorker(ips, columnsList);
                    } else if (importMode == ImportMode.OVERWRITE) {
                        allPkList = execNamedQuerySingleColAsList("getImportedPkValues", null, serverFileName, LisaConstants.IMPORT_ACTION_UPDATE);
                        ups = conn.prepareStatement(queryGenerator.updateRecordPreparedStatement(tableName, columnsList, pkColumnsList));
                        worker = new TeradataBatchUpdateWorker(ups, columnsList);
                    }
                    Set<String> allPkSet = new HashSet<String>(allPkList);
                    for (Map<String, String> record : content) {
                        String pkValue = extractPkValue(record, pkColumnsList);
                        if (allPkSet.contains(pkValue)) {
                            worker.addBatch(record);
                        }
                    }
                    worker.finish();
//                    for (String pkValueStr : allPkList) {
//                        Map<String, String> pkMap = new HashMap<String, String>();
//                        List<String> pkValueList = BaseUtils.splitBySep(pkValueStr, LisaConstants.PK_VALUE_SEPERATOR);
//                        for (int i = 0; i < pkColumnsList.size(); i++) {
//                            LisaDictColumnBean column = pkColumnsList.get(i);
//                            String pkValue = pkValueList.get(i);
//                            pkMap.put(column.nameInDb, pkValue);
//                        }
//                        worker.addBatch(pkMap);
//                    }
//                    TeradataBatchMergeWorker mergeWorker = new TeradataBatchMergeWorker(mps = conn.prepareStatement(queryGenerator.mergeRecordPreparedStatement(tableName, columnsList, pkColumnsList)), columnsList, pkColumnsList);

//                    mergeWorker.finish();
                    conn.commit();
                } catch (SQLException ex) {
                    try {
                        conn.rollback();
                    } catch (SQLException exIt) {
                        throw getRootMessageAndThowTeradataException(exIt);
                    }
                    throw getRootMessageAndThowTeradataException(ex);
                } finally {
                    try {
                        if (ips != null) {
                            ips.close();
                        }
                        if (dps != null) {
                            dps.close();
                        }
                        if (ups != null) {
                            ups.close();
                        }
                    } catch (SQLException exIt) {
                        throw getRootMessageAndThowTeradataException(exIt);
                    }
                }
            }
        });
        reader.close();
    }

    private void blockTable(Connection conn, LisaTeradataQueryGenerator queryGenerator, String tableName) throws SQLException {
        Statement statement = conn.createStatement();
        statement.execute(queryGenerator.getLockTableQuery(tableName));
    }

//    private Pair<Integer, String> validate(String tableName, XlsxReader reader, Map<String, String> mappedHeaders, List<LisaDictColumnBean> columnsList, Boolean isAdding) throws Exception {
//        List<Map<String, String>> lines = reader.getContent();
//        for (LisaDictColumnBean column : columnsList) {
//            //validate
//            switch (column.validateLevel) {
//                case LisaConstants.VALIDATE_FROM_LIST:
//                    if (!isValidFromList(column, lines)) {
//                        return new Pair<Integer, String>(LisaConstants.IMPORT_FAILURE, I18n.walidacjaNieUdalaSie.get());
//                    }
//                    break;
//                case LisaConstants.VALIDATE_FROM_DICT:
//                    if (!isValidFromDict(column, lines)) {
//                        return new Pair<Integer, String>(LisaConstants.IMPORT_FAILURE, I18n.walidacjaNieUdalaSie.get());
//                    }
//                    break;
//            }
//        }
//        if (isAdding) {
//            List<LisaDictColumnBean> indexColumnsNames = extractIndexColumns(columnsList);
//            Comparator<List<String>> comparator = new Comparator<List<String>>() {
//
//                @Override
//                public int compare(List<String> list1, List<String> list2) {
//                    for (int i = 0; i < list1.size(); i++) {
//                        String s1 = list1.get(i);
//                        String s2 = list2.get(i);
//                        if (s1 == null && s2 == null) {
//                            continue;
//                        }
//                        if (s1 == null) {
//                            return -1;
//                        }
//                        if (s2 == null) {
//                            return 1;
//                        }
//                        int ret = list1.get(i).compareTo(list2.get(i));
//                        if (ret != 0) {
//                            return ret;
//                        }
//                    }
//                    return 0;
//                }
//            };
//            long start = System.currentTimeMillis();
//            if (!checkDuplicationInFile(lines, indexColumnsNames, comparator)) {
//                return new Pair<Integer, String>(LisaConstants.IMPORT_FAILURE, I18n.powtorzajaSieRekordyWPliku.get());
//            }
//            System.out.println("checkDuplicationInFile: " + (System.currentTimeMillis() - start) / 1000.0 + "s");
//            start = System.currentTimeMillis();
//            if (!checkExistInTeradata(tableName, lines, indexColumnsNames, comparator)) {
//                return new Pair<Integer, String>(LisaConstants.IMPORT_FAILURE, I18n.juzIstniejaWBazieNiektoreRekordy.get());
//            }
//            System.out.println("checkExistInTeradata: " + (System.currentTimeMillis() - start) / 1000.0 + "s");
//        }
//        return new Pair<Integer, String>(LisaConstants.VALIDATION_SUCCESS, "");
//    }
//    private Pair<Integer, String> doImport(final XlsxReader reader, final List<LisaDictColumnBean> columnsList, final String tableName, final Boolean isAdding) throws Exception {
//        final Pair<Integer, String> ret = new Pair<Integer, String>(LisaConstants.IMPORT_SUCCESS, "");
//        final List<LisaDictColumnBean> indexColumns = extractIndexColumns(columnsList);
//        AuthorizedTeradataConnection tConnection = authorizeConnection();
//        final LisaTeradataQueryGenerator queryGenerator = new LisaTeradataQueryGenerator(getSchemaName(), tConnection);
//        List<List<String>> indexValuesList = loadAllPkValueInTeradata(tableName, indexColumns);
//        final Set<String> dbIndexValuesSet = new HashSet<String>();
//        for (List<String> indexValue : indexValuesList) {
//            dbIndexValuesSet.add(mergeToString(indexValue));
//        }
//        tConnection.execWithJdbcConnection(new IParametrizedContinuation<Connection>() {
////            PreparedStatement statement;
//
//            private Connection conn;
//            private Statement statement = null;
//            private PreparedStatement ps = null;
//            private Map<String, Integer> sqlDataType;
//            private Integer colId;
//
//            @Override
//            public void doIt(Connection conn) {
//                this.conn = conn;
//                int cnt = 0;
//                int lineNr = 0;
//                try {
//                    conn.setAutoCommit(false);
//                    blockTable();
//                    String sql = isAdding ? "" : queryGenerator.updateDictionaryQueryPreparedStatement(tableName, columnsList, indexColumns) + " ELSE ";
//                    sql += queryGenerator.insertDictionaryQueryPreparedStatment(tableName, columnsList);
//                    List<Map<String, String>> content = reader.getContent();
////                    Statement statment = conn.createStatement();
////                    for (int fromId = 0; fromId < content.size(); fromId += LisaConstants.IMPORT_PACK_SIZE) {
////                        long start = System.currentTimeMillis();
////
////                        int toId = fromId + LisaConstants.IMPORT_PACK_SIZE;
////                        if (toId > content.size()) {
////                            toId = content.size();
////                        }
////                        statment.executeUpdate(queryGenerator.insertDictionaryQuery(content.subList(fromId, toId), dbName, columnsList));
////                        System.out.println("Done pack " + fromId / LisaConstants.IMPORT_PACK_SIZE + " in " + (System.currentTimeMillis() - start) / 1000.0);
////                    }
//                    long start = System.currentTimeMillis();
//                    int addedBatchSize = 0;
//                    loadColumnDataTypes();
//                    ps = conn.prepareStatement(sql);
//                    Set<String> fileIndexValuesSet = new HashSet<String>();
//                    for (Map<String, String> record : content) {
//                        lineNr++;
//                        List<String> indexValue = new ArrayList<String>();
//                        for (LisaDictColumnBean column : indexColumns) {
//                            indexValue.add(record.get(column.nameInDb));
//                        }
//                        String indexValueStr = mergeToString(indexValue);
//                        if (dbIndexValuesSet.contains(indexValueStr) && isAdding) {
//                            continue;
//                        }
//                        if (fileIndexValuesSet.contains(indexValueStr)) {
//                            continue;
//                        }
//                        cnt++;
//                        if (!fileIndexValuesSet.contains(indexValueStr)) {
//                            fileIndexValuesSet.add(indexValueStr);
//                        }
//                        colId = 0;
//                        if (!isAdding) {
//                            setParameters4UpdateStatement(record);
//                        }
//                        setParameters4InsertStatement(record);
//                        ps.addBatch();
//                        addedBatchSize++;
////            System.out.println(query);
//                        if (addedBatchSize == LisaConstants.IMPORT_PACK_SIZE) {
//                            ps.executeBatch();
//                            ps.clearBatch();
////                            conn.commit();
//                            addedBatchSize = 0;
//                            System.out.println("Done pack " + cnt / LisaConstants.IMPORT_PACK_SIZE + " in " + (System.currentTimeMillis() - start) / 1000.0);
//                            start = System.currentTimeMillis();
//                        }
//                    }
//                    if (addedBatchSize > 0) {
//                        ps.executeBatch();
//                    }
//                    conn.commit();
//                    ret.v2 = I18n.dodanoZaktualizowano.get() + ":" + cnt + " " + I18n.rekordy.get();
//                } catch (SQLException ex) {
//                    System.out.println("lineNr = " + lineNr);
//                    try {
//                        conn.rollback();
//                    } catch (SQLException exIn) {
//                        //Trzeba to przerobic, aby CallableStatement był w pl.trzy0.foxy.serverlogic.db, szybka poprawka dla Jerzego
//                        //inaczej sie nie wygrzebę z tego NAS-a, przy NAS etap2 to zmienić!!!!
//                    }
//                    String msg = null;
//                    for (SQLException exIt = ex; exIt != null; exIt = exIt.getNextException()) {
//                        exIt.printStackTrace();
//                        msg = exIt.getMessage();
//                    }
//                    throw new FoxyRuntimeException(msg);
//                } finally {
//                    try {
//                        if (statement != null) {
//                            statement.close();
//                        }
//                        if (ps != null) {
//                            ps.close();
//                        }
//                    } catch (SQLException ex) {
//                    }
//                }
//            }
//
//            private void blockTable() throws SQLException {
//                statement = conn.createStatement();
//                statement.execute(queryGenerator.getLockTableQuery(tableName));
//            }
//
//            private void loadColumnDataTypes() throws SQLException {
//                ps = conn.prepareStatement(queryGenerator.insertDictionaryQueryPreparedStatment(tableName, columnsList));
//                sqlDataType = new HashMap<String, Integer>();
//                for (int colId = 0; colId < columnsList.size(); colId++) {
//                    LisaDictColumnBean column = columnsList.get(colId);
//                    sqlDataType.put(column.nameInDb, ps.getParameterMetaData().getParameterType(colId + 1));
//                }
//                ps.close();
//            }
//
//            private void setParameters4UpdateStatement(Map<String, String> record) throws SQLException {
//                for (LisaDictColumnBean column : columnsList) {
//                    if (!indexColumns.contains(column)) {
//                        String value = record.get(column.nameInDb);
//                        int dataType = sqlDataType.get(column.nameInDb);
//                        setParametr4PreparedStatement(ps, ++colId, column, value, dataType);
//                    }
//                }
//
//                for (LisaDictColumnBean column : indexColumns) {
//                    String value = record.get(column.nameInDb);
//                    int dataType = sqlDataType.get(column.nameInDb);
//                    setParametr4PreparedStatement(ps, ++colId, column, value, dataType);
//                    setParametr4PreparedStatement(ps, ++colId, column, value, dataType);
//                    setParametr4PreparedStatement(ps, ++colId, column, value, dataType);
//                }
//            }
//
//            private void setParameters4InsertStatement(Map<String, String> record) throws SQLException {
//                for (LisaDictColumnBean column : columnsList) {
//                    String value = record.get(column.nameInDb);
//                    int dataType = sqlDataType.get(column.nameInDb);
//                    setParametr4PreparedStatement(ps, ++colId, column, value, dataType);
//                }
//            }
//        }
//        );
//        return ret;
//    }
//
//    private void setParametr4PreparedStatement(PreparedStatement ps, int paramId, LisaDictColumnBean column, String value, int dataType) throws SQLException {
//        boolean isNull = (LisaConstants.IS_NULLABLE.equals(column.nullable.trim()) && (value == null || BaseUtils.isStrEmptyOrWhiteSpace(value)));
//        if (isNull) {
//            ps.setNull(paramId, dataType);
//        } else {
//            ps.setObject(paramId, value, dataType);
//        }
//    }
    @Override
    public ImportOverviewBean analyseDictWithImportedFile(Integer instanceId, int nodeId, Map<String, String> mappedNames, String serverFileName, ImportMode importMode) throws TeradataException {
        ImportOverviewBean ret = new ImportOverviewBean();
        final String tableName = getDictionaryName(nodeId);
        final List<LisaDictColumnBean> columnsList = getDictionaryColumnsByNodeId(nodeId);
        final List<LisaDictColumnBean> pkColumnsList = LisaUtils.extractPkColumns(columnsList);
        final Map<String, Integer> pkValuesInDb = loadAllPkValueInTeradata(instanceId, nodeId, tableName, pkColumnsList);
        final Set<String> pkValuesInFile = new HashSet<String>();
        File file = new File(dirForUpload, serverFileName);
        XlsxReader reader = new XlsxReader(file, mappedNames);
        List<Map<String, String>> content = reader.getContent();

        int recordNr = 0;
        for (Map<String, String> record : content) {
            String pkValue = extractPkValue(record, pkColumnsList);
            if (BaseUtils.isStrEmptyOrWhiteSpace(pkValue)) {
                ret.pkEmpty++;
                //brak klucza glownego
                execNamedCommand("addRecordActionToCache", serverFileName, recordNr, pkValue, LisaConstants.IMPORT_ACTION_PK_EMPTY);
            } else {
                if (pkValuesInFile.contains(pkValue)) {
                    ret.pkDuplicated++;
                    execNamedCommand("addRecordActionToCache", serverFileName, recordNr, pkValue, LisaConstants.IMPORT_ACTION_PK_DUPLICATE);
                }
                pkValuesInFile.add(pkValue);

                if (importMode == ImportMode.DELETE) {
                    if (pkValuesInDb.containsKey(pkValue)) {
                        ret.deleted += pkValuesInDb.get(pkValue);
                        execNamedCommand("addRecordActionToCache", serverFileName, recordNr, pkValue, LisaConstants.IMPORT_ACTION_DELETE);
                    }
                } else if (!validate(instanceId, nodeId, record, columnsList)) {
                    ret.invalid++;
                    //nie przeszlo walidacji
                    execNamedCommand("addRecordActionToCache", serverFileName, recordNr, pkValue, LisaConstants.IMPORT_ACTION_INVALID);
                } else if (importMode == ImportMode.ADD && !pkValuesInDb.containsKey(pkValue)) {
                    ret.added++;
                    execNamedCommand("addRecordActionToCache", serverFileName, recordNr, pkValue, LisaConstants.IMPORT_ACTION_ADD);
                } else if (importMode == ImportMode.OVERWRITE && pkValuesInDb.containsKey(pkValue)) {
                    ret.updated += pkValuesInDb.get(pkValue);
                    execNamedCommand("addRecordActionToCache", serverFileName, recordNr, pkValue, LisaConstants.IMPORT_ACTION_UPDATE);
                }
            }
            recordNr++;
        }
        return ret;
    }

    @Override
    public void clearLisaImportCache(String serverFileName) {
        execNamedCommand("clearLisaImportCache", serverFileName);
        deleteTmpFile(serverFileName);
    }

    @Override
    public List<LisaImportActionBean> getActionList(String serverFileName, String actionCode, int offset, int limit) {
        return createBeansFromNamedQry("getActionList", LisaImportActionBean.class, serverFileName, actionCode, offset, limit);
    }

    @Override
    public Integer getCategoryzationNodeId(Integer categoryId) {
        TreeNodeBean nodeBeanById = boxiService.getNodeBeanById(categoryId, false);
        if (nodeBeanById.nodeKindCode.equals(BIKConstants.NODE_KIND_LISA_CATEGORIZATION) || nodeBeanById.parentNodeId == null) {
            return nodeBeanById.id;
        } else {
            return getCategoryzationNodeId(nodeBeanById.parentNodeId);
        }
    }

    private String getQueryBand() {
        SystemUserBean loggedUserBean = getLoggedUserBean();
        return "SET QUERY_BAND='app=BIKS; user=" + (loggedUserBean == null || loggedUserBean.ssoLogin == null ? "" : loggedUserBean.ssoLogin) + ";' FOR SESSION";
    }

    @Override
    public List<LisaInstanceInfoBean> getLisaInstanceList() {
        return createBeansFromNamedQry("getLisaInstanceList", LisaInstanceInfoBean.class);
    }

    @Override
    public void updateLisaInstanceInfo(LisaInstanceInfoBean bean) {
        execNamedCommand("updateLisaInstanceInfo", bean);
    }

    @Override
    public void addLisaInstanceInfo(LisaInstanceInfoBean bean) {
        execNamedCommand("addLisaInstanceInfo", bean);
    }

    @Override
    public void deleteLisaInstance(Integer instanceId) throws TeradataException {
        //TO-DO: check dict list is empty
        if (execNamedQuerySingleValAsInteger("countDictNumberBelong2Instance", false, null, instanceId) == 0) {
            execNamedCommand("deleteLisaInstanceInfo", instanceId);
        } else {
            throw new TeradataException(I18n.listaSlownikowNieJestPusta.get());
        }
    }

    @Override
    public LisaInstanceInfoBean getLisaInstanceByNodeId(int nodeId) {
        return createBeanFromNamedQry("getLisaInstanceInfoByNodeId", LisaInstanceInfoBean.class, true, nodeId);
    }
}
