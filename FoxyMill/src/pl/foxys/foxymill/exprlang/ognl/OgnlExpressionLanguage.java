/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.exprlang.ognl;

import pl.foxys.foxymill.exprlang.ExpressionLanguageBase;
import pl.foxys.foxymill.exprlang.ParsedExpressionBase;

/**
 *
 * @author wezyr
 */
public class OgnlExpressionLanguage extends ExpressionLanguageBase {

    @Override
    protected Class<? extends ParsedExpressionBase> getExprClass() {
        return OgnlParsedExpression.class;
    }
}
