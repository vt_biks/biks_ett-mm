--Tabelka z nazwami wszystkich drzewek
create table bik_tree(
	id int not null identity(1,1) primary key,
	name varchar(255) not null, 
	is_metadata int check(is_metadata in(0,1))
);
--uzupe�nienie danymi
insert into bik_tree (name, is_metadata) values('Raporty', 1);
insert into bik_tree (name, is_metadata) values('Światy obiektów', 1);
insert into bik_tree (name, is_metadata) values('Połączenia', 1);
insert into bik_tree (name, is_metadata) values('Teradata', 1);
insert into bik_tree (name, is_metadata) values('Obszary biznesowe', 1);

--Tabelka ze wszystkimi Kind'ami node'�w
create table bik_node_kind (
  id int not null identity primary key,
  code varchar(255) not null unique,
  caption varchar(255) not null
);
--Uzupe�nienie tabelki z kind'ami nod�w
insert into bik_node_kind values('Folder','Folder');
----Po��czenia
insert into bik_node_kind values('MetaData.DataConnection','Połączenie');
----Raporty
insert into bik_node_kind (code, caption) values('Webi', 'Raport Webi');
insert into bik_node_kind (code, caption) values('Flash','Raport Flash');
insert into bik_node_kind (code, caption) values('CristalReports', 'CristalReports');
insert into bik_node_kind (code, caption) values('Universe', 'Świat obiektów');
insert into bik_node_kind (code, caption) values('_BIZAREA_', 'Obszar biznesowy');
insert into bik_node_kind (code, caption) values('SCHEMA', 'Schemat');
insert into bik_node_kind (code, caption) values('TABLE', 'Tabela');
insert into bik_node_kind (code, caption) values('COLUMN', 'Kolumna');
insert into bik_node_kind (code, caption) values('VIEW', 'Widok');
insert into bik_node_kind (code, caption) values('PROCEDURE', 'Procedura');

--Tabelka do przechowywania wszystkich node'�w
create table bik_node (
  id int not null identity(1,1) primary key,
  parent_node_id int,
  node_kind_id int not null references bik_node_kind (id),
  name varchar(4000) not null, 
  obj_id int,
  tree_id int not null references bik_tree (id)
);

--1. Tabelka do przechowywania wersji bazy danych
create table bik_app_prop (
	name varchar(255) not null,
	val varchar(255) not null,
	unique(name,val)
	);
	
--2. Wrzucenie wersji pocz�tkowej
insert into bik_app_prop
values('bik_ver','1.0.0');

-- dodatkowa kolumna w bik_node: linked_node_id
alter table bik_node
  add linked_node_id int references bik_node(id);