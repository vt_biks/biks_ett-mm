/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author AMamcarz
 */
public class AttributeAndTheirKindsBean implements Serializable {

    public int id;
    public String atrName;
    public int kindId;
    public String kindCode;
    public String kindCaption;
}
