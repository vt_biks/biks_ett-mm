﻿exec sp_check_version '1.6.z6.8';
go

if not exists (select * from bik_app_prop where name = 'logoOverlay')
begin
	insert into bik_app_prop(name, val, is_editable)
	values ('logoOverlay', 'images/santa2.png|12.06|01.06|-120|8;', 1)
end;
go

declare @treeOfTrees int = (select id from bik_tree where code = 'TreeOfTrees')

update bik_node set name = 'Społeczność' where tree_id = @treeOfTrees and obj_id = 'community'

update bik_translation 
set txt = 'Community'
where kind = 'node' and lang = 'en' and code = 'community'

exec sp_update_version '1.6.z6.8', '1.6.z7';
go