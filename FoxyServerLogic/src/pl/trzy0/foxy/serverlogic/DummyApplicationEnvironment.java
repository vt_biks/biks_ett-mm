/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.IOutboundUrlRewriter;
import java.util.Collections;
import java.util.Map;
import pl.foxys.foxymill.IRenderable;
import pl.trzy0.foxy.commons.templates.INamedTemplateProvider;
import simplelib.BaseUtils;
import simplelib.MapHelper;

/**
 *
 * @author wezyr
 */
public class DummyApplicationEnvironment implements IApplicationEnvironment {

    protected MapHelper<String> sysProps;
    protected Map<String, Map<String, String>> textsProps;
    protected Map<String, IRenderable> pages;
    protected Map<String, Object> globals;
    protected Map<String, INamedTemplateProvider> tmplProviders;
    protected String canonicalAppUrlBase;
    protected String canonicalResourceUrlBase;
    protected String canonicalSiteUrl;

    public MapHelper<String> getSysProps() {
        if (sysProps == null) {
            sysProps = new MapHelper<String>(Collections.<String, Object>emptyMap());
        }
        return sysProps;
    }

    public Map<String, String> getTextProps(String langCode) {
        return BaseUtils.safeMapGet(textsProps, langCode);
    }

    public IRenderable getPage(String name) {
        return BaseUtils.safeMapGet(pages, name);
    }

    public Object getGlobal(String name) {
        return BaseUtils.safeMapGet(globals, name);
    }

    public INamedTemplateProvider getTemplateProvider(String langCode) {
        return BaseUtils.safeMapGet(tmplProviders, langCode);
    }

    public String getCanonicalAppUrlBase() {
        return canonicalAppUrlBase;
    }

    public String getCanonicalResourceUrlBase() {
        return canonicalResourceUrlBase;
    }

    public String getCanonicalSiteUrl() {
        return canonicalSiteUrl;
    }

    public IOutboundUrlRewriter getOptOutboundUrlRewriter() {
        return null;
    }

    public boolean getUsesSubdomains() {
        return false;
    }
}
