/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class JoinedObjBasicGenericInfoBean<IDT> implements Serializable {

    public IDT srcId;
    public IDT dstId;
    //ww: type == false -> ręczne, można usuwać, type == true -> fixed (z metadanych)
    public boolean type;
    public boolean inheritToDescendants;
    //---
    public String branchIds;

    @Override
    public String toString() {
        return "JoinedObjBasicGenericInfoBean{" + "srcId=" + srcId + ", dstId=" + dstId + ", type=" + type + ", inheritToDescendants=" + inheritToDescendants + ", branchIds=" + branchIds + '}';
    }
}
