/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.client;

import java.util.LinkedHashMap;
import java.util.Map;
import pl.fovis.common.TreeNodeBean;

/**
 *
 * @author mgraczkowski
 */
public class UniTreeNode {

    public TreeNodeBean tnb;
    public Map<Integer, UniTreeNode> children;

    public UniTreeNode(TreeNodeBean tnb) {
        this.tnb = tnb;
        this.children = null;
    }

    public void addChild(UniTreeNode child) {
        if (children == null) {
            children = new LinkedHashMap<Integer, UniTreeNode>();
        }
        children.put(child.tnb.id, child);
    }
}
