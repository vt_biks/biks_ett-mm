/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import commonlib.LameUtils;
import java.util.HashMap;
import java.util.Map;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.junit.Test;
import simplelib.ILameCollector;
import simplelib.Pair;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author pmielanczuk
 */
public class LuceneSimilarNodesWizardTest {

    static {
        Map<String, String> loggersCfg = new HashMap<String, String>();
        loggersCfg.put("/level", "Info");
        LameLoggerFactory.applyLameLoggersConfig(loggersCfg);
    }
    private static final ILameLogger logger = LameUtils.getMyLogger();
    LuceneSimilarNodesWizard wizard = new LuceneSimilarNodesWizard("c:/temp/LuceneWizard/001");

    INodesDataFetcher fetcher = new INodesDataFetcher() {

        @Override
        public void fetchFullData(ILameCollector<Map<String, Pair<String, Float>>> collector) {
            collector.add(prepareNodeData(1, "ala ma kota, a kot ma alę"));
            collector.add(prepareNodeData(2, "ala ma psa, a kot ma alę"));
        }

        @Override
        public void fetchChangedData(ILameCollector<Map<String, Pair<String, Float>>> collector) {
            collector.add(prepareNodeData(2, true));
            collector.add(prepareNodeData(3, "hela jest głupia", "ale hela ma jeża, a kot ma szczura, a ala ma kota"));
            collector.add(prepareNodeData(4, "barbara ma chomika, a kot ma helę"));
            collector.add(prepareNodeData(5, "ale hela ma jeża", "hela jest głupia"));
            collector.add(prepareNodeData(6, "ala ma psa, a kot ma alę"));
            collector.add(prepareNodeData(7, "hela jest głupia", "ala ma kota, a kot ma alę"));
            collector.add(prepareNodeData(8, "annapurna to nie hela", "to taki szczyt"));
            collector.add(prepareNodeData(9, "szczyt Annapurna", "ala nie wie co to jest Annapurna, hehe"));
        }
    };

    protected Map<String, Pair<String, Float>> prepareNodeData(int nodeId, boolean isDeleted) {
        return prepareNodeData(nodeId, null, null, true);
    }

    protected Map<String, Pair<String, Float>> prepareNodeData(int nodeId, String name, String descr) {
        return prepareNodeData(nodeId, name, descr, false);
    }

    protected Map<String, Pair<String, Float>> prepareNodeData(int nodeId, String name, String descr, boolean isDeleted) {
        Map<String, Pair<String, Float>> nodeData = new HashMap<String, Pair<String, Float>>();
        nodeData.put(FtsByLucene.NODE_ID_FIELD_NAME, new Pair<String, Float>(Integer.toString(nodeId), 1.0f));
        nodeData.put("name", new Pair<String, Float>(name, 1000000.0f));
        nodeData.put("descr", new Pair<String, Float>(descr, 10000.0f));
        if (isDeleted) {
            nodeData.put(FtsByLucene.IS_DELETED_FIELD_NAME, new Pair<String, Float>("1", 100.0f));
        }
        return nodeData;
    }

    protected Map<String, Pair<String, Float>> prepareNodeData(int nodeId, String name) {
        return prepareNodeData(nodeId, name, null);
    }

    public LuceneSimilarNodesWizardTest() {
        wizard.setUp(fetcher, true);
        wizard.waitForFinalizeIndexingIfInProgress();
    }

    //@Test
    public void testSomeMethod() {
        int resultNum = 0;
        System.out.println((++resultNum) + ": " + wizard.findSimilarNodeIds(1, 0, 100, null));
        wizard.nodeDataChanged();
        //LameUtils.threadSleep(1000);
        System.out.println((++resultNum) + ": " + wizard.findSimilarNodeIds(1, 0, 100, null));
        LameUtils.threadSleep(1000);
        System.out.println((++resultNum) + ": " + wizard.findSimilarNodeIds(1, 0, 100, null));
    }

    protected Integer findDocByObjId(int objId) {
        try {
            Query q0 = new TermQuery(new Term(FtsByLucene.NODE_ID_FIELD_NAME, Integer.toString(objId)));

            TopDocs td = wizard.searcher.search(q0, 1);

            ScoreDoc[] hits = td.scoreDocs;

            if (hits.length == 0) {
                if (logger.isInfoEnabled()) {
                    logger.info("prepareMoreLikeThisQuery: no results for nodeId=" + objId);
                }
                return null;
            }

            return hits[0].doc;
        } catch (Exception ex) {
            throw new RuntimeException("error in findDocByObjId for objId=" + objId, ex);
        }
    }

    @Test
    public void testSearch() throws Exception {
        wizard.nodeDataChanged();
        wizard.waitForFinalizeIndexingIfInProgress();

        Query q = new QueryParser("name", wizard.analyzer).parse("descr:Annapurna");
        System.out.println("q=" + q);
        TopDocs td = wizard.searcher.search(q, 100);
        System.out.println("total hits: " + td.totalHits);
//        wizard.reader.getTermVectors(docID)
        int doc = findDocByObjId(8);
        System.out.println("doc id for objId=8: " + doc);
//        Fields flds = wizard.reader.getTermVectors(doc);
//        System.out.println("field size=" + flds.size());
//        for (String field : flds) {
//            System.out.println("field=" + field);
//            Terms t = flds.terms(field);
//            TermsEnum te = t.iterator();
//            BytesRef br = te.next();
//            while (br != null) {
//                System.out.println("  term=" + br.utf8ToString());
//                br = te.next();
//            }
//        }
    }

    // bez boost
    // 3: {6=1.0, 7=0.88378894, 3=0.7158784, 4=0.46268004, 5=0.1355416}
    // z boost
    // 3: {6=1.0, 7=0.9097872, 3=0.7307203, 4=0.50741214, 5=0.12527102}
    // boost 2
    // 3: {6=1.0, 7=0.9097872, 3=0.7307203, 4=0.50741214, 5=0.12527102}
    // boost 1000
    // 3: {6=1.0, 7=0.90978724, 3=0.7307204, 4=0.5074122, 5=0.12527104}
    // boost 1000000
    // 3: {6=1.0, 7=0.9097872, 3=0.7307203, 4=0.50741214, 5=0.12527102}
}
