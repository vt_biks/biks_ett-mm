/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package commonlib;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Set;
import simplelib.LameRuntimeException;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author wezyr
 */
public class LameJarResourceProvider implements ILameResourceProvider {

    private static final ILameLogger logger = LameLoggerFactory.getLogger(LameJarResourceProvider.class);    
    
//    public LameJarResourceProvider() {
//
//    }

    public Set<String> getResourceNames(String base, Set<String> fileExts, boolean recurseSubDirs) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    protected String noUrlExplanation(String resourceName) {
        return this.getClass() +
                ".getCheckedUrlForResource: url not found for resource: " +
                resourceName;
    }

    protected URL getUrlForResource(String resourceName) {
        //resourceName = LameUtils.cutPrefix(resourceName, "/");
        //InputStream is = classLoader.getResourceAsStream(resourceName);
        URL res = getClass().getResource(resourceName);
        //System.out.println("getUrlForResource: resourceName=" + resourceName + ", url=" + res);
        return res;
    }

    protected URL getCheckedUrlForResource(String resourceName) {
        URL res = getUrlForResource(resourceName);
        if (res == null) {
            throw new LameRuntimeException(noUrlExplanation(resourceName));
        }
        return res;
    }

    public long getLastModifiedOfResource(String resourceName) {
        URL url = getCheckedUrlForResource(resourceName);
        try {
            long res = url.openConnection().getLastModified();
            
            if (logger.isDebugEnabled()) {
                logger.debug("getLastModifiedOfResource: resourceName=\"" + resourceName + "\", res=" + res);
            }
            
            return res;
        } catch (IOException ex) {
            throw new LameRuntimeException("getLastModifiedOfResource: " + resourceName, ex);
        }
    }

    public InputStream gainResource(String resourceName) {
        URL url = getCheckedUrlForResource(resourceName);
        try {
            return url.openStream();
        } catch (IOException ex) {
            throw new LameRuntimeException("getInputStreamForResource: " + resourceName, ex);
        }
    }
}
