/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.RadioButton;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.JoinedObjMode;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class TreeSelector2ExportDialog extends TreeSelectorDialog {

    protected Integer treeId;
    protected String branchId;
    protected Set<Integer> selectedNodes = new HashSet<Integer>();
    protected RadioButton exportTree, generateHtml, generatePdf;
    protected CheckBox numbering = new CheckBox(I18n.czyChceszDodacNumeracje.get());

    @Override
    protected String getDialogCaption() {
        return I18n.wybierzObiektyDoEksportu.get();
    }

    @Override
    protected boolean isHeaderVisible() {
        return false;
    }

    public TreeSelector2ExportDialog(Integer treeId) {
        this.treeId = treeId;
    }

    public TreeSelector2ExportDialog(Integer treeId, String branchId) {
        this.treeId = treeId;
        this.branchId = branchId;
    }

    @Override
    protected boolean isInMultiSelectMode() {
        return true;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.eksport.get();
    }

    @Override
    protected void addPopupMenuInMultiSelectMode() {
        //no-op
    }

    @Override
    protected boolean isStandardJoinedObjMode() {
        return false;
    }

    @Override
    protected boolean isInheritToDescendantsCheckBoxVisible() {
        return true;
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        BIKClientSingletons.getService().getAllTreeNodesToExport(treeId, branchId, callback);
    }

    @Override
    protected Collection<Integer> getTreeIdsForFiltering() {
        List<Integer> list = new ArrayList<Integer>();
        list.add(treeId);
        return list;
    }

    @Override
    protected void optHandlerWhenNodeSelected(TreeNodeBean node) {
        JoinedObjMode status = getNodeSelectedValue(node);
        if (status == JoinedObjMode.Single) {
            selectedNodes.add(node.id);
        } else {
            selectedNodes.remove(node.id);
        }
    }

    @Override
    protected void buildMainWidgets() {
        main.add(createActionPanel());
        main.add(numbering);
        super.buildMainWidgets(); //To change body of generated methods, choose Tools | Templates.
    }

//    private void updateAnsectors(TreeNodeBean node) {
//        List<String> ids = BaseUtils.splitBySep(node.branchIds, "|");
//        for (int i = ids.size() - 1; i >= 0; i--) {
//            String id = ids.get(i);
//            Integer ansectorNodeId = BaseUtils.tryParseInteger(id, node.id);
//            if (ansectorNodeId != node.id) {
//                TreeNodeBean parentNode = bikTree.getTreeStorage().getBeanById(ansectorNodeId);
//                boolean selectedOneDescendant = false;
//                for (Entry<Integer, JoinedObjMode> e : directOverride.entrySet()) {
//                    if (e.getValue().equals(JoinedObjMode.Single)) {
//                        TreeNodeBean descendant = bikTree.getTreeStorage().getBeanById(e.getKey());
//                        if (descendant.parentNodeId == parentNode.id) {
//                            selectedOneDescendant = true;
//                            break;
//                        }
//                    }
//                }
//                InlineHTML imgHtml = htmlForJOMImg.get(ansectorNodeId);
//                JoinedObjMode val = selectedOneDescendant ? JoinedObjMode.Single : JoinedObjMode.NotJoined;
//                imgHtml.setHTML(generateHtmlForJoinedObjImg(val, false));
//                directOverride.put(parentNode.id, val);
//                updateLabelStylesOfNode(parentNode);
//            }
//        }
//    }
    @Override
    protected void doAction() {

        String exportedNodeIdsAsStr = BaseUtils.mergeWithSepEx(selectedNodes, ",");
        String treeName = BIKClientSingletons.getTreeBeanById(treeId).name;
        StringBuilder sb = new StringBuilder(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet());
        if (exportTree.getValue()) {
            sb.append("?").append(BIKConstants.TREE_EXPORT_PARAM_NAME).append("=").append(BIKConstants.EXPORT_FORMAT_ZIP).append("&")
                    .append(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME).append("=").append(treeId).append("&")
                    .append(BIKConstants.TREE_EXPORT_SELECTED_NODE_PARAM_NAME).append("=").append(exportedNodeIdsAsStr).append("&")
                    .append(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME).append("=").append(BaseUtils.deAccentPolishChars(treeName).replace(" ", "_"));
        } else if (generateHtml.getValue()) {
            sb.append("?").append(BIKConstants.TREE_EXPORT_PARAM_NAME).append("=").append(BIKConstants.EXPORT_FORMAT_HTML).append("&")
                    .append(BIKConstants.TREE_EXPORT_ACTION_PARAM_NAME).append("=").append(BIKConstants.TREE_EXPORT_ACTION_CREATE_DOCUMENT).append("&")
                    .append(BIKConstants.TREE_EXPORT_SELECTED_NODE_PARAM_NAME).append("=").append(exportedNodeIdsAsStr).append("&")
                    .append(BIKConstants.TREE_EXPORT_DEPTH_PARAM_NAME).append("=").append("999").append("&")
                    .append(BIKConstants.TREE_EXPORT_NUMBERING_PARAM_NAME).append("=").append(numbering.getValue() ? "1" : "0").append("&")
                    .append(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME).append("=").append(treeId).append("&")
                    .append(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME).append("=").append(BaseUtils.deAccentPolishChars(treeName).replace(" ", "_"));
        } else if (generatePdf.getValue()) {
            sb.append("?").append(BIKConstants.TREE_EXPORT_PARAM_NAME).append("=").append(BIKConstants.EXPORT_FORMAT_PDF).append("&")
                    .append(BIKConstants.TREE_EXPORT_ACTION_PARAM_NAME).append("=").append(BIKConstants.TREE_EXPORT_ACTION_CREATE_DOCUMENT).append("&")
                    .append(BIKConstants.TREE_EXPORT_SELECTED_NODE_PARAM_NAME).append("=").append(exportedNodeIdsAsStr).append("&")
                    .append(BIKConstants.TREE_EXPORT_DEPTH_PARAM_NAME).append("=").append("999").append("&")
                    .append(BIKConstants.TREE_EXPORT_NUMBERING_PARAM_NAME).append("=").append(numbering.getValue() ? "1" : "0").append("&")
                    .append(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME).append("=").append(treeId).append("&")
                    .append(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME).append("=").append(BaseUtils.deAccentPolishChars(treeName).replace(" ", "_"));
        }
        Window.open(sb.toString(), "_blank", "");
    }

    boolean isGenerateHTML() {
        return BIKClientSingletons.isAppAdminLoggedIn(BIKConstants.ACTION_EXPORT_TO_HTML)
                || BIKClientSingletons.isEffectiveExpertLoggedInEx(BIKConstants.ACTION_EXPORT_TO_HTML)
                || BIKClientSingletons.isSysAdminLoggedInEx(BIKConstants.ACTION_EXPORT_TO_HTML);

//        return true;
    }

    protected IsWidget createActionPanel() {
        HorizontalPanel hp = new HorizontalPanel();
//        hp.add(new Label(I18n.akcja.get() + ": "));

        hp.add(exportTree = new RadioButton("action", I18n.eksportDrzewa.get()));
        exportTree.setVisible(BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_IMPORT_TREE, null));
        hp.add(generateHtml = new RadioButton("action", I18n.generujHtml.get()));
        generateHtml.setVisible(BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_TO_HTML, null));
        hp.add(generatePdf = new RadioButton("action", I18n.generujPdf.get()));
        generatePdf.setVisible(BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_TO_PDF, null));
        exportTree.setValue(true);
        numbering.setEnabled(false);
        exportTree.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                numbering.setEnabled(!exportTree.getValue());
            }
        });
        generateHtml.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                numbering.setEnabled(generateHtml.getValue());
            }
        });
        generatePdf.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                numbering.setEnabled(generatePdf.getValue());
            }
        });
        return hp;
    }
}
