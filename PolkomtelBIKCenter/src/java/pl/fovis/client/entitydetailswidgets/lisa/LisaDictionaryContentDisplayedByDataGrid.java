package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.ColumnSortList.ColumnSortInfo;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionModel;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.lisa.ChangeCategoryDialog;
import pl.fovis.client.dialogs.lisa.LisaDataType;
import pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.DateLDV;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryValue;
import pl.fovis.common.lisa.LisaGridParamBean;
import pl.fovis.common.lisa.LisaUtils;
import pl.fovis.common.lisa.StringLDV;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author ctran
 */
public class LisaDictionaryContentDisplayedByDataGrid extends EntityDetailsWidgetFlatBase implements IDataGrid {

    private static int PAGE_SIZE = 50;
    private static final int MAX_PAGE_SIZE = 1000;
    private static final int columnSize = 120;
    private VerticalPanel panel;
    private HorizontalPanel headerPanel;
    private final ListDataProvider<Map<String, LisaDictionaryValue>> dataProvider = new ListDataProvider<Map<String, LisaDictionaryValue>>();
    private final CustomListHandler sortHandler = new CustomListHandler(null, this);
    private final SelectionModel<Map<String, LisaDictionaryValue>> selectionModel = new MultiSelectionModel<Map<String, LisaDictionaryValue>>(null);

    private LisaDataGrid dataGrid;
    private int seperatorCnt = 0;
    private List<LisaDictColumnBean> columnInfo;
    private Map<String, LisaDictionaryValue> blankRecord;
    private Set<Map<String, LisaDictionaryValue>> changes = new HashSet<Map<String, LisaDictionaryValue>>();
    private int categoryNodeId;
    private String nodeKindCode;
    private PushButton refreshBtn, cancelBtn, addBtn, deleteBtn, editBtn, confirmBtn;
    private ListBoxInPopup suggestPopup;
    private TextBox pageSizeTb;
    private int pageNum = -1;
    private boolean isInChangeCategoryMode;
    private String orderByColumn;
    private boolean isAscending;
    private Integer beanCnt;
    private LisaFiltrDialog filtrDlg;
    private List<LisaFiltrColumnValueBase> infoToFiltr;
    private int tableHeight = 1000, tableWidth;
    private DataGridPager pager;
    private boolean hasMoreData = false;
    private String dictionaryName;
    private Header<Boolean> selectPageHeader;
    private CheckboxHeaderCell checkboxHeaderCell;
    protected Integer lisaInstanceId;
    protected String lisaInstanceName;

    private Widget createPager() {
        pager = new DataGridPager(this);
        return pager.asWidget();
    }

    private void createDatePickerColumn(final LisaDictColumnBean columnBean, boolean isEditable) {
        //DatePicker
        //DateTimeFormat dateFormat = DateTimeFormat.getFormat(PredefinedFormat.DATE_MEDIUM);
        final DateTimeFormat dateFormat = DateTimeFormat.getFormat("yyyy-MM-dd");
        addColumn(new CustomDatePickerCell(dataGrid, isEditable, dateFormat), columnBean.titleToDisplay, columnBean.textDisplayed, new GetValue<Date>() {
            @Override
            public Date getValue(Map<String, LisaDictionaryValue> row) {
                LisaDictionaryValue cellValue = row.get(columnBean.nameInDb);
//                String s = BaseUtils.trimAndCompactSpaces(value.getValue().toString());
                return (cellValue.getValue() == null ? null : (Date) cellValue.getValue());
//                return s.isEmpty() ? LisaConstants.NULL_VALUE : (Date) value.getValue();
            }
        }, new FieldUpdater<Map<String, LisaDictionaryValue>, Date>() {
            @Override
            public void update(int index, Map<String, LisaDictionaryValue> record, Date value) {
                //confirmBtn.setEnabled(true);
                record.put(columnBean.nameInDb, new DateLDV(value));
                if (index == 0) {
                    blankRecord = record;
                }
                changes.add(record);
            }
        });
    }

    private void createEditTextColumn(final LisaDictColumnBean columnBean, boolean isEditable) {
        //EditTextCell
//        addColumn(new CustomEditTextCell(isEditable), columnBean.nameInDb, new GetValue<String>() {
//            @Override
//            public String getValue(Map<String, LisaDictionaryValue> row) {
//                LisaDictionaryValue value = row.get(columnBean.nameInDb);
//                return value == null ? null : String.valueOf(value.getValue());
//            }
//        }, new FieldUpdater<Map<String, LisaDictionaryValue>, String>() {
//            @Override
//            public void update(int index, Map<String, LisaDictionaryValue> record, String value) {
//                if (index == 0) {
//                    addBtn.setEnabled(true);
//                    createOrUpdateValueForRecord(blankRecord, columnBean, value);
//                } else {
//                    confirmBtn.setEnabled(true);
//                    record.put(columnBean.nameInDb, new StringLDV(value));
//                    changes.add(record);
//                }
//            }
//        });

        //Dodaj kolume
        addColumn(new SuggestCell(dataGrid, new ListBoxInPopup(getLisaInstanceId()), isEditable, columnBean), columnBean.titleToDisplay, columnBean.textDisplayed, new GetValue<String>() {
            @Override
            public String getValue(Map<String, LisaDictionaryValue> row) {
                LisaDictionaryValue cellValue = row.get(columnBean.nameInDb);
//                return BaseUtils.safeToString(value.getValue());
                return BaseUtils.safeToString(cellValue.getValue());
            }
        }, new FieldUpdater<Map<String, LisaDictionaryValue>, String>() {
            @Override
            public void update(int index, final Map<String, LisaDictionaryValue> record, String value) {
                //confirmBtn.setEnabled(true);
                //System.out.println(value);
//                if (record.get(columnBean.nameInDb) instanceof StringLDV) {
//                    record.put(columnBean.nameInDb, new StringLDV(value));
//                } else if (record.get(columnBean.nameInDb) instanceof IntegerLDV) {
//                    Integer number = 0;
//                    try {
//                        number = Integer.parseInt(value);
//                    } catch (NumberFormatException e) {
//                    }
//                    record.put(columnBean.nameInDb, new IntegerLDV(number));
//                }
//                System.out.println("Change: " + columnBean.nameInDb + " -> " + value);
//                Window.alert("createEditTextColumn" + BaseUtils.safeToStringTxt(value));
                record.put(columnBean.nameInDb, new StringLDV(value));
                if (index == 0) {
                    blankRecord = record;
                }
//                for (Map.Entry< String, LisaDictionaryValue> e : record.entrySet()) {
//                    System.out.println(e.getKey() + "->" + e.getValue().toString());
//                }
                changes.add(record);
            }
        });
    }

    private PushButton createChangeCategoryBtn() {
        PushButton changeCategoryButton = new PushButton(I18n.zmienKategorie.get() /* I18N:  */);
        NewLookUtils.makeCustomPushButton(changeCategoryButton);
        changeCategoryButton.setVisible(hasEditRoleInDictionary());
        changeCategoryButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                final Set<Map<String, LisaDictionaryValue>> checkedItems = getCheckedItems();
                final int itemsCount = checkedItems.size();
                if (itemsCount < 1) {
                    new SimpleInfoDialog().buildAndShowDialog(I18n.zaznaczPozycjeDoPrzeniesienia.get() /* I18N:  */ + ".", null, null);
                } else {
                    final TreeNodeBean node = getData().node;
                    BIKClientSingletons.getLisaService().getCategoryzationNodeId(node.parentNodeId, new StandardAsyncCallback<Integer>("Error in getCategoryzationNodeId") {

                        @Override
                        public void onSuccess(Integer rootNodeId) {
                            ChangeCategoryDialog dialog = new ChangeCategoryDialog(rootNodeId);
                            dialog.itemsToMoveCount = itemsCount;
                            dialog.currentCategoryName = node.name;
                            dialog.saveCont = new IParametrizedContinuation<Integer>() {
                                @Override
                                public void doIt(final Integer newCategoryNodeId) {
                                    new LisaAuthorizedRequest<Void>(lisaInstanceId, lisaInstanceName) {
                                        @Override
                                        protected void performServerAction(AsyncCallback<Void> callback) {
                                            BIKClientSingletons.getLisaService().moveValuesToCategory(lisaInstanceId, checkedItems, node.id/*nodeId*/, newCategoryNodeId, callback);
                                        }

                                        @Override
                                        public void onActionSuccess(Void result) {
                                            BIKClientSingletons.showInfo(I18n.pozycjeZostalyPrzeniesDoNowejKat.get() /* I18N:  */);
                                            pager.navigate(0);
                                        }
                                    }.setNodeId(getNodeId()).go();
                                }
                            };
                            dialog.buildAndShowDialog();
                        }
                    });
                }
            }
        });
        return changeCategoryButton;
    }

    @SuppressWarnings(value = "unchecked")
    @Override
    public String getSortColumnName() {
        ColumnSortList sortList = dataGrid.getColumnSortList();
        int columnIndex;
        if (sortList == null || sortList.size() == 0) {
            columnIndex = -1;
        } else {
            ColumnSortInfo columnSortInfo = sortList.get(0);
            columnIndex = dataGrid.getColumnIndex((Column<Map<String, LisaDictionaryValue>, Cell>) columnSortInfo.getColumn()) / 2 - 1;
        }

        return (columnIndex == -1 ? null : columnInfo.get(columnIndex).nameInDb);
    }

    @Override
    public int getPageNum() {
        return pageNum;
    }

    @Override
    public int getPageSize() {
        return PAGE_SIZE;
    }

    @Override
    public Integer getBeanCnt() {
        return beanCnt;
    }

    @Override
    public void changeDirectionOrder() {
        this.isAscending = !this.isAscending;
    }

    @Override
    public List<LisaDictColumnBean> getColumnInfo() {
        return columnInfo;
    }

    @Override
    public void performLisaFiltr(List<LisaFiltrColumnValueBase> infoToFiltr) {
        this.infoToFiltr = infoToFiltr;
        beanCnt = null;
        pager.navigate(0);
    }

    @Override
    public void updateDataGrid(List<Map<String, LisaDictionaryValue>> dataList) {
        if (dataList.size() > PAGE_SIZE) {
            setHasMoreData(true);
            pager.getNextPageBtn().setEnabled(true);
            dataList.remove(PAGE_SIZE);
        } else {
            pager.getNextPageBtn().setEnabled(false);
            setHasMoreData(false);
        }
        dataProvider.getList().clear();
        dataProvider.getList().addAll(dataList);
        sortHandler.setList(dataProvider.getList());
        dataProvider.flush();
        dataGrid.redraw();
        dataProvider.refresh();
        setStateForAllRecord(checkboxHeaderCell.getValue());
    }

    public void resizeDataGrid(int height, int width) {
        tableHeight = height;
        tableWidth = width;
        if (dataGrid != null) {
            dataGrid.setHeight(height + "px");
            if (dataGrid.getOffsetWidth() < width) {
                dataGrid.adjustGrid(width);
            }
            dataProvider.refresh();
        }
    }

    private void createSeperator() {
        Column<Map<String, LisaDictionaryValue>, String> column = new Column<Map<String, LisaDictionaryValue>, String>(new TextCell()) {
            @Override
            public String getValue(Map<String, LisaDictionaryValue> object) {
                return null;
            }
        };
        Header<String> header = new Header<String>(new TextCell()) {

            @Override
            public String getValue() {
                return "";
            }
        };
        header.setHeaderStyleNames("seperator-column");

        dataGrid.addColumn(column, header);
        dataGrid.setColumnWidthPx(dataGrid.getColumnCount() - 1, LisaDataGrid.SEP_COLUMN_SIZE);
    }

    private PushButton createEditBtn() {
        editBtn = new PushButton(I18n.edytuj.get());
        editBtn.setVisible(hasEditRoleInDictionary());
        NewLookUtils.makeCustomPushButton(editBtn);
        editBtn.setEnabled(BIKClientSingletons.isUserLoggedIn());
        editBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (addBtn != null) {
                    addBtn.setEnabled(false);
                }
                editBtn.setEnabled(false);
                deleteBtn.setEnabled(false);
                confirmBtn.setEnabled(true);
                dataGrid.setInEditMode(true);
            }
        });
        return editBtn;
    }

//    private PushButton createCancelBtn() {
//        cancelBtn = new PushButton(I18n.anuluj.get());
//        NewLookUtils.makeCustomPushButton(cancelBtn);
//        cancelBtn.addClickHandler(new ClickHandler() {
//
//            @Override
//            public void onClick(ClickEvent event) {
//                resetGrid();
//            }
//        });
//        return cancelBtn;
//    }
    private void createCheckBoxHeader() {
        checkboxHeaderCell = new CheckboxHeaderCell();
        selectPageHeader = new Header<Boolean>(checkboxHeaderCell) {

            @Override
            public Boolean getValue() {
                for (Map<String, LisaDictionaryValue> row : dataProvider.getList()) {
                    if (!selectionModel.isSelected(row)) {
                        return false;
                    }
                }
                return (dataGrid.getRowCount() > 0);
            }
        };
        selectPageHeader.setUpdater(new ValueUpdater<Boolean>() {

            @Override
            public void update(Boolean value) {
                setStateForAllRecord(value);
            }
        });
        //firstColumn
        Column<Map<String, LisaDictionaryValue>, Boolean> checkColumn = new Column<Map<String, LisaDictionaryValue>, Boolean>(new CheckboxCell(true, false)) {
            @Override
            public Boolean getValue(Map<String, LisaDictionaryValue> object) {
                // Get the value from the selection model.
                return selectionModel.isSelected(object);
            }
        };

        dataGrid.addColumn(checkColumn, selectPageHeader);
        dataGrid.setColumnWidthPx(dataGrid.getColumnCount() - 1, LisaDataGrid.FIRST_COLUMN_SIZE);
    }

    public void setStateForAllRecord(Boolean value) {
        for (Map<String, LisaDictionaryValue> row : dataProvider.getList()) {
            selectionModel.setSelected(row, value);
        }
    }

    private static interface GetValue<C> {

        C getValue(Map<String, LisaDictionaryValue> row);
    }

    private void resetGrid() {
        int newPageSize = BaseUtils.tryParseInt(pageSizeTb.getValue());

        changes.clear();
        blankRecord = null;
        suggestPopup.hide();
        //System.out.println(newPageSize);
        if (0 < newPageSize && newPageSize < MAX_PAGE_SIZE) {
            PAGE_SIZE = newPageSize;
            pager.navigate(pageNum);
        } else {
            BIKClientSingletons.showWarning(I18n.niePrawidlowaWartosc.get());
        }
        resetButtons();
    }

    private PushButton createRefreshBtn() {
        refreshBtn = new PushButton(I18n.odswiez.get() + "/" + I18n.anuluj.get());
        NewLookUtils.makeCustomPushButton(refreshBtn);
        refreshBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                resetGrid();
            }
        });
        return refreshBtn;
    }

    @Override
    public void changePage(int pageNum) {
        if (pageNum == -1) {
            return;
        }
        this.pageNum = pageNum;

        getData(pageNum, PAGE_SIZE);
    }

    private void doAddOrUpdateRecords() {
        if (dataGrid.isClickedAdd()) {
            new LisaAuthorizedRequest<Void>(getLisaInstanceId(), getLisaInstanceName()) {
                @Override
                protected void performServerAction(AsyncCallback<Void> callback) {
                    BIKClientSingletons.getLisaService().insertDictionaryContent(lisaInstanceId, columnInfo, blankRecord, categoryNodeId, BIKGWTConstants.NODE_KIND_LISA_DICTIONARY.equals(getNodeKindCode()), callback);
                }

                @Override
                public void onActionSuccess(Void result) {
                    blankRecord = createBlankRecord();
                    beanCnt++;
                    if (addBtn != null) {
                        addBtn.setEnabled(false);
                    }
                    BIKClientSingletons.showInfo(I18n.dodanoNowyElement.get() /* i18n:  */);
                    dataGrid.setClickedAdd(false);
                    resetGrid();
                }

                @Override
                public void onActionFailure(Throwable caught) {
                    BIKClientSingletons.showWarning(I18n.bladPrzyDodawaniuWiersza.get() /* i18n:  */);
                    super.onActionFailure(caught); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public boolean doBeforeAction() {
                    return doBeforeInsertUpdate(columnInfo, blankRecord);
                }

            }.setNodeId(nodeId).go();
        } else {
            new LisaAuthorizedRequest<Void>(lisaInstanceId, lisaInstanceName) {
                @Override
                protected void performServerAction(AsyncCallback<Void> callback) {
                    System.out.println("changes.size() = " + changes.size());
                    BIKClientSingletons.getLisaService().updateDictionaryContents(lisaInstanceId, columnInfo, changes, categoryNodeId, BIKGWTConstants.NODE_KIND_LISA_DICTIONARY.equals(getNodeKindCode()), callback);
                }

                @Override
                public void onActionSuccess(Void result) {
                    BIKClientSingletons.showInfo(I18n.zaktualizowane.get() /* I18N:  */);
                    resetGrid();
                }
            }.setNodeId(getNodeId()).go();
        }
    }

    private PushButton createConfirmBtn() {
        confirmBtn = new PushButton(I18n.zatwierdzModyfikacje.get());
        confirmBtn.setVisible(hasEditRoleInDictionary());
        NewLookUtils.makeCustomPushButton(confirmBtn);
        confirmBtn.setEnabled(false);
        confirmBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new LisaAuthorizedRequest<Boolean>(lisaInstanceId, lisaInstanceName) {
                    @Override
                    protected void performServerAction(AsyncCallback<Boolean> callback) {
                        BIKClientSingletons.getLisaService().validateRecords(lisaInstanceId, columnInfo, changes, categoryNodeId, BIKGWTConstants.NODE_KIND_LISA_DICTIONARY.equals(getNodeKindCode()), callback);
                    }

                    @Override
                    public void onActionSuccess(Boolean validated) {
                        if (validated) {
                            doAddOrUpdateRecords();
                        } else {
                            BIKClientSingletons.showWarning(I18n.wpiszPoprawneWartosci.get());
//                            resetGrid();
                        }
                    }
                }.setNodeId(getNodeId()).go();
            }
        });
        return confirmBtn;
    }

    private PushButton createDeleteBtn() {
        deleteBtn = new PushButton(I18n.usun.get());
        deleteBtn.setVisible(hasEditRoleInDictionary());
        NewLookUtils.makeCustomPushButton(deleteBtn);
        deleteBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final Set<Map<String, LisaDictionaryValue>> checkedItems = getCheckedItems();

                final int numberItemToDelete = checkedItems.size();
                if (numberItemToDelete < 1) {
                    new SimpleInfoDialog().buildAndShowDialog(I18n.zaznaczPozycjeDoSkasowania.get() /* I18N:  */ + ".", null, null);
                } else {
                    StringBuilder sb = new StringBuilder(I18n.czyNaPewnoUsunac.get());
                    sb.append(" ").append(numberItemToDelete).append(" ").append(I18n.pozycje.get()).append("?");
                    new ConfirmDialog().buildAndShowDialog(sb.toString(), new IContinuation() {
                        public void doIt() {
                            new LisaAuthorizedRequest<Void>(lisaInstanceId, lisaInstanceName) {
                                @Override
                                protected void performServerAction(AsyncCallback<Void> callback) {
                                    BIKClientSingletons.getLisaService().deleteDictionaryContent(lisaInstanceId, checkedItems, categoryNodeId, BIKGWTConstants.NODE_KIND_LISA_DICTIONARY.equals(getNodeKindCode()), callback);
                                }

                                @Override
                                public void onActionSuccess(Void result) {
                                    BIKClientSingletons.showInfo(I18n.pozycjeZostalyUsuniete.get() /* I18N:  */);
                                    checkedItems.clear();
                                    beanCnt -= numberItemToDelete;
                                    resetGrid();
                                }
                            }.setNodeId(getNodeId()).go();
                        }
                    });
                }
            }

        });
        return deleteBtn;
    }

    private Set<Map<String, LisaDictionaryValue>> getCheckedItems() {
        Set<Map<String, LisaDictionaryValue>> checkedItems = new HashSet<Map<String, LisaDictionaryValue>>();
        for (int i = (dataGrid.isClickedAdd() ? 1 : 0); i < dataProvider.getList().size(); i++) {
            Map<String, LisaDictionaryValue> record = dataProvider.getList().get(i);
            if (selectionModel.isSelected(record)) {
                checkedItems.add(record);
            }
        }
        return checkedItems;
    }

    private boolean hasEditRoleInDictionary() {
        return isEditonEnabled();
    }

    @Override
    protected String getOptNodeActionCode() {
        return "EditLisaDict";
    }

    private PushButton createAddBtn() {
        addBtn = new PushButton(I18n.dodaj.get());
        addBtn.setVisible(hasEditRoleInDictionary());
        NewLookUtils.makeCustomPushButton(addBtn);
        addBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                blankRecord = createBlankRecord();
                dataProvider.getList().add(0, blankRecord);
                dataProvider.flush();
                dataGrid.setClickedAdd(true);
                addBtn.setEnabled(false);
                editBtn.setEnabled(false);
                deleteBtn.setEnabled(false);
                confirmBtn.setEnabled(true);
                if (!validate(blankRecord)) {
                    return;
                }
            }

        });
        return addBtn;
    }

    private void resetButtons() {
        if (addBtn != null) {
            addBtn.setEnabled(BIKClientSingletons.isUserLoggedIn());
        }
        editBtn.setEnabled(BIKClientSingletons.isUserLoggedIn());
        deleteBtn.setEnabled(BIKClientSingletons.isUserLoggedIn());
        confirmBtn.setEnabled(false);
        dataGrid.setInEditMode(false);
        dataGrid.setClickedAdd(false);
        changes.clear();
    }

    private boolean validate(Map<String, LisaDictionaryValue> record) {
        for (LisaDictColumnBean columnBean : columnInfo) {
            if (!columnBean.isNotVisibleAccess()) {
                if (LisaDataType.getValueFor(columnBean.dataType) != LisaDataType.DA) {
                    if (!validateOneColumn(record.get(columnBean.nameInDb), columnBean.dataType)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    private boolean validateOneColumn(LisaDictionaryValue data, String dataType) {
        return true;
    }

    private boolean doBeforeInsertUpdate(List<LisaDictColumnBean> columnInformation, Map<String, LisaDictionaryValue> record) {
        for (LisaDictColumnBean columnInfo : columnInformation) {
            if (columnInfo.isMainKey) {
                if (record.containsKey(columnInfo.nameInDb)) {
                    LisaDictionaryValue cell = record.get(columnInfo.nameInDb);
                    Object value = cell.getValue();
                    if (value == null) {
                        new SimpleInfoDialog().buildAndShowDialog(I18n.proszeUzupelnickolumnyZKluczem.get() + " (" + I18n.column.get().toLowerCase() + ":" + columnInfo.titleToDisplay + ")", null, null);
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public LisaDictionaryContentDisplayedByDataGrid() {
    }

    @Override
    public void displayData() {
        if (hasContent()) {
            generateGrid();
        }
    }

    private void generateGrid() {
        //(new ArrayList<Map<String, LisaDictionaryValue>>());
        dataProvider.getList().clear();
        StandardAsyncCallback<List<LisaDictColumnBean>> callback = new StandardAsyncCallback<List<LisaDictColumnBean>>() {
            @Override
            public void onSuccess(List<LisaDictColumnBean> result) {
                String dicName = LisaUtils.getTableNameWithDBFromObjId(getData().node.objId);
                if (dictionaryName == null || dicName == null || !dicName.equals(dictionaryName)) {
                    infoToFiltr = new ArrayList<LisaFiltrColumnValueBase>();
                    if (filtrDlg != null) {
                        filtrDlg.clearFilter();
                    }
                }
                lisaInstanceId = getLisaInstanceId();
                lisaInstanceName = getLisaInstanceName();
                dictionaryName = dicName;
                columnInfo = result;
                blankRecord = createBlankRecord();
                categoryNodeId = getNodeId();
                nodeKindCode = getNodeKindCode();
                isAscending = true; //ASC
                beanCnt = null;
                suggestPopup = new ListBoxInPopup(lisaInstanceId);
                createHeaderPanel();
                createFiltr();
                createDataGridDisplay();
//                loadColumnTypes();
                dataGrid.setClickedAdd(false);
//                resetButtons();
//                resetGrid();
            }
        };
        panel.clear();
        if (BIKGWTConstants.NODE_KIND_LISA_DICTIONARY.equals(getNodeKindCode())) {
            BIKClientSingletons.getLisaService().getDictionaryColumnsForDic(getNodeId(), callback);
        } else {
            BIKClientSingletons.getLisaService().getDictionaryColumns(getNodeId(), callback);
        }
    }

//    private void loadColumnTypes() {
//        StandardAsyncCallback<List<LisaDictColumnBean>> callback = new StandardAsyncCallback<List<LisaDictColumnBean>>() {
//            @Override
//            public void onSuccess(List<LisaDictColumnBean> result) {
//            }
//        };
//    }
    private void createFiltr() {
        HTML filtrLb = new HTML("<b>" + I18n.wprowadzFrazeDoFiltru.get() + "</b>");
        filtrLb.setStyleName("labelLink");
        filtrLb.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (filtrDlg == null) {
                    filtrDlg = new LisaFiltrDialog(getInstance());
                    filtrDlg.buildAndShowDialog();
                } else {
                    filtrDlg.showDialog();
                }

            }
        });
        panel.add(filtrLb);
    }

    private LisaDictionaryContentDisplayedByDataGrid getInstance() {
        return this;
    }

    private Map<String, LisaDictionaryValue> createBlankRecord() {
        Map<String, LisaDictionaryValue> record = new HashMap<String, LisaDictionaryValue>();
        for (LisaDictColumnBean columnBean : columnInfo) {
            if (!columnBean.isNotVisibleAccess()) {
                LisaUtils.createOrUpdateValueForRecord(record, columnBean, BaseUtils.isStrEmptyOrWhiteSpace(columnBean.defaultValue) ? null : columnBean.defaultValue);
            }
        }
        return record;
    }

    private void createHeaderPanel() {
//        HeaderPanel headerPanel = new HeaderPanel();
        headerPanel = new HorizontalPanel();
        headerPanel.setSpacing(6);
        headerPanel.add(createPager());
        headerPanel.add(new Label(I18n.liczbaWyswietlonychPozycjiNaStronie.get() + ": "));
        pageSizeTb = new TextBox();
        pageSizeTb.setText(String.valueOf(PAGE_SIZE));
        pageSizeTb.setWidth("30px");
        headerPanel.add(pageSizeTb);
        headerPanel.add(createRefreshBtn());
//        headerPanel.add(createCancelBtn());
        isInChangeCategoryMode = false;
        if (BIKGWTConstants.NODE_KIND_LISA_CATEGORY.equals(getNodeKindCode())) {
            headerPanel.add(createChangeCategoryBtn());
            isInChangeCategoryMode = true;
        } else {
            headerPanel.add(createAddBtn());
        }
        headerPanel.add(createEditBtn());
        headerPanel.add(createDeleteBtn());
        headerPanel.add(createConfirmBtn());
        sortHandler.setIsInChangeCategoryMode(isInChangeCategoryMode);
        panel.add(headerPanel);
    }

    private void createDataGridDisplay() {
        for (HasData<Map<String, LisaDictionaryValue>> displayer : dataProvider.getDataDisplays()) {
            dataProvider.removeDataDisplay(displayer);
        }

        dataGrid = new LisaDataGrid(MAX_PAGE_SIZE, columnInfo);
        dataGrid.setAutoHeaderRefreshDisabled(false);
//        dataGrid.setAlwaysShowScrollBars(false);
        //dataGrid.setVerticalScrollBarAlwaysVisible(true);
        seperatorCnt = 0;
        dataGrid.addDomHandler(new MouseDownHandler() {

            @Override
            public void onMouseDown(MouseDownEvent event) {
                dataGrid.setStartX(event.getX());
                int seperatorId = dataGrid.getChoosenColumn(event.getX());
//                System.out.println("MouseDownEvent " + event.getX() + " seperatorId = " + seperatorId);
//                Window.alert("MouseDownEvent " + event.getX() + " seperatorId = " + seperatorId);
                if (seperatorId % 2 == 0) { //nie byl klikniety na pustej kolumnie
                    seperatorId = 0; //znacznik
                }
                dataGrid.setSeperatorId(seperatorId);
            }
        }, MouseDownEvent.getType());
        dataGrid.addDomHandler(new MouseUpHandler() {

            @Override
            public void onMouseUp(MouseUpEvent event) {
//                Window.alert("MouseUpEvent " + event.getX());
//                System.out.println("MouseUpEvent " + event.getX());
                if (dataGrid.getSeperatorId() != 0) { //byla naciskana odpowiednio pusta kolumna
                    dataGrid.updateColumnSize(event.getX());
                }
            }
        }, MouseUpEvent.getType());

        dataProvider.addDataDisplay(dataGrid);
        dataGrid.addColumnSortHandler(sortHandler);
        dataGrid.setSelectionModel(selectionModel, DefaultSelectionEventManager.<Map<String, LisaDictionaryValue>>createCheckboxManager());

//        dataGrid.setHeight(tableHeight + "px");
//        System.out.println("tableHeight = " + tableHeight + " width = " + width);
        dataGrid.setStyleName("dataGrid-cc");
        initTableColumns();

        int width = LisaDataGrid.FIRST_COLUMN_SIZE + LisaDataGrid.SEP_COLUMN_SIZE + columnInfo.size() * (LisaDataGrid.SEP_COLUMN_SIZE + columnSize) + LisaDataGrid.LAST_COLUMN_SIZE;
//        dataGrid.setWidth(width + "px");
        if (tableWidth < width) {
            tableWidth = width;
        }
        //dataGrid.setMinimumTableWidth(width, Unit.PX);
        resizeDataGrid(tableHeight, tableWidth);
        panel.add(dataGrid);
        pager.navigate(0);
    }

    private void initTableColumns() {
        createCheckBoxHeader();
        createSeperator();
        //realColumn
        for (LisaDictColumnBean columnBean : columnInfo) {
            if (!columnBean.isNotVisibleAccess()) {
                if (columnBean.isReadOnlyAccess()) {
                    if (LisaDataType.getValueFor(columnBean.dataType) == LisaDataType.DA) {
                        createDatePickerColumn(columnBean, false);
                    } else {
                        createEditTextColumn(columnBean, false);
                    }
                } else if (LisaDataType.getValueFor(columnBean.dataType) == LisaDataType.DA) {
                    createDatePickerColumn(columnBean, true);
                } else {
                    createEditTextColumn(columnBean, true);
                }

                createSeperator();
            }
        }
        //lastColumn
        Column<Map<String, LisaDictionaryValue>, String> blankColumn = new Column<Map<String, LisaDictionaryValue>, String>(new TextCell()) {
            @Override
            public String getValue(Map<String, LisaDictionaryValue> object) {
                return null;
            }
        };
        Header<String> header = new Header<String>(new TextCell()) {

            @Override
            public String getValue() {
                return "";
            }
        };

        dataGrid.addColumn(blankColumn, header);
        dataGrid.setColumnWidthPx(dataGrid.getColumnCount() - 1, LisaDataGrid.LAST_COLUMN_SIZE);
    }

    @Override
    public void getData(int pageNum, int pageSize) {
        //Window.alert("" + pageNum);
        String sortColumnName = getSortColumnName();
        if (sortColumnName == null) {
            sortColumnName = orderByColumn;
        }
        orderByColumn = sortColumnName;
        final LisaGridParamBean params = new LisaGridParamBean(pageNum * PAGE_SIZE, pageSize + 1, categoryNodeId, nodeKindCode, infoToFiltr, isAscending, sortColumnName);
        new LisaAuthorizedRequest<List<Map<String, LisaDictionaryValue>>>(lisaInstanceId, lisaInstanceName) {
            private boolean showInfo = false;

            @Override
            protected void performServerAction(AsyncCallback<List<Map<String, LisaDictionaryValue>>> callback) {
                BIKClientSingletons.getLisaService().getDictionaryRecords(lisaInstanceId, params, null, callback);
            }

            @Override
            public void onActionSuccess(final List<Map<String, LisaDictionaryValue>> result) {
                //setIfShowSuccessInfo(false);
                doUpdateDataGrid(result);
            }

            @Override
            public void onActionFailure(Throwable caught) {
                //BIKClientSingletons.showError("Something wrong when query");
                BIKClientSingletons.showWarning(caught.getMessage());
            }
        }.setNodeId(getNodeId()).go();
    }

    private void doUpdateDataGrid(final List<Map<String, LisaDictionaryValue>> result) {
        if (beanCnt == null) {
            new LisaAuthorizedRequest<Integer>(lisaInstanceId, lisaInstanceName) {
                @Override
                protected void performServerAction(AsyncCallback<Integer> callback) {
                    BIKClientSingletons.getLisaService().getCountDictionaryRecords(lisaInstanceId, new LisaGridParamBean(0, 0, categoryNodeId, nodeKindCode, infoToFiltr), null, callback);
                }

                @Override
                public void onActionSuccess(Integer cnt) {
                    //setIfShowSuccessInfo(false);
                    beanCnt = cnt;
                    updateDataGrid(result);
                }

                protected void showSuccessInfo() {
                    BIKClientSingletons.showInfo(I18n.pobieranieDanychSlownikaGotowe.get());
                }
            }.setNodeId(getNodeId()).go();
        } else {
            updateDataGrid(result);
        }
    }

    private <C> Column<Map<String, LisaDictionaryValue>, C> addColumn(Cell<C> cell, final String columnName, final String columnTitle, final GetValue<C> getter, FieldUpdater<Map<String, LisaDictionaryValue>, C> fieldUpdater) {
        Column<Map<String, LisaDictionaryValue>, C> column = new Column<Map<String, LisaDictionaryValue>, C>(cell) {
            @Override
            public C getValue(Map<String, LisaDictionaryValue> object) {
                return getter.getValue(object);
            }

        };
        column.setFieldUpdater(fieldUpdater);
        column.setSortable(true);
        //BIKResizableHeader<Map<String, LisaDictionaryValue>> header = new BIKResizableHeader<Map<String, LisaDictionaryValue>>(dataProvider, columnName, dataGrid, column, Window.getClientHeight());

        sortHandler.setComparator(column, new Comparator<Map<String, LisaDictionaryValue>>() {

            @Override
            public int compare(Map<String, LisaDictionaryValue> row1, Map<String, LisaDictionaryValue> row2) {
                //int res = doCompare(getter.getValue(row1), getter.getValue(row2));
                return doCompare(getter.getValue(row1), getter.getValue(row2));
            }

            private int doCompare(Object v1, Object v2) {
                if (v1 == null && v2 == null) {
                    return 0;
                }
                if (v1 == null) {
                    return -1;
                }
                if (v2 == null) {
                    return 1;
                }
                if (v1 instanceof Date) {
                    return ((Date) v1).compareTo((Date) v2);
                } else {
                    return ((String) v1).compareTo((String) v2);
                }
            }
        });
        Header<String> header = new Header<String>(new TextCell()) {

            @Override
            public String getValue() {
                return columnName;
            }
        };

//        if (dataGrid.getColumnCount() % 2 == 0) {
//            header.setHeaderStyleNames("odd-column");
//        } else {
//            header.setHeaderStyleNames("even-column");
//        }
        //header.setHeaderStyleNames("datagrid-header");
        dataGrid.addColumn(column, header);
        dataGrid.setColumnWidthPx(dataGrid.getColumnCount() - 1, columnSize);

        return column;
    }

    @Override
    public Widget buildWidgets() {
        panel = new VerticalPanel();
        return panel;
    }

    @Override
    public boolean hasContent() {
        return BIKGWTConstants.NODE_KIND_LISA_CATEGORY.equals(getNodeKindCode()) || (BIKGWTConstants.NODE_KIND_LISA_DICTIONARY.equals(getNodeKindCode()) && !hasChildren());
    }

    @Override
    public boolean isHasMoreData() {
        return hasMoreData;
    }

    public void setHasMoreData(boolean hasMoreData) {
        this.hasMoreData = hasMoreData;
    }
}
