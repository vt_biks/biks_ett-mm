/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.teradata;

import java.io.Serializable;

/**
 *
 * @author pgajda
 */
public class TeradataBeanProcesRel implements Serializable {

    public String symbProc;
    public String idObj;
    public String idTypProcess;
    public String objType;
    public String bikNodeObjId;
    public String area;
    public String dbName;
    
    public TeradataBeanProcesRel(String symbProc, String idObj, String idTypProcess, String objType, String bikNodeObjId, String area, String dbName) {
        this.symbProc = symbProc;
        this.idObj = idObj;
        this.idTypProcess = idTypProcess;
        this.objType = objType;
        this.bikNodeObjId = bikNodeObjId;
        this.area = area;
        this.dbName = dbName;
    }
}
