/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.logging;

import simplelib.IContinuationWithReturn;

/**
 *
 * @author wezyr
 */
public class GlobalLameLoggingEnvironment implements ILameLoggingEnvironment {

    private boolean noLogging = false;

    synchronized public boolean getRunWithoutLoggingMode() {
        return noLogging;
    }

    synchronized public <T> T runWithoutLogging(boolean noLogging, IContinuationWithReturn<T> cont) {

        boolean oldVal = this.noLogging;
        try {
            this.noLogging = noLogging;
            return cont.doIt();
        } finally {
            this.noLogging = oldVal;
        }
    }

    public String getCurrentRuningContextInfo() {
        return null;
    }
}
