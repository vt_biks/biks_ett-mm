/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import pl.fovis.client.bikpages.IBikPage;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public interface ITabSelector<IDT> {

    // wrzuć namiary na nową stronę i opcjonalny obiekt (val) na stronie
    // do historii przeglądarki
    // to może spowodować reakcję na zmianę adresu (historyChange) o ile
    // fireOnChange == true i wtedy nastąpi wywołanie
    // bikcenterEntryPoint.onValueChange (a to w końcu wywoła selectValueOnTab)
    // natomiast w przypadku fireOnChange == false nie nastąpi przejście na
    // wskazaną zakładkę/obiekt a jedynie odłożenie tego w historii przeglądarki
    public void submitNewTabSelection(String tabId, IDT val, boolean fireOnChange);

    // powoduje przejście na wybraną zakładkę i ew. obiekt na zakładce 
    // (opcjonalne val)
    public void selectValueOnTab(String tabId, IDT val);

    public boolean hasTab(String tabId);

//    public String siKindToTabId(String siKind);
//    public String getCurrentTabId();
    // true if current tab == tabId
    public boolean invalidateTabData(String tabId);

    // true if current node == nodeId
    public boolean invalidateNodeData(IDT nodeId);

    public void refreshAfterLoginLogout();

    public Pair<String, IDT> getCurrentTabIdAndSelectedVal();

    public String getCurrentTabCaption();

    public IBikPage<IDT> getCurrentTab();

    public IBikPage<IDT> getTabById(String tabId);
}
