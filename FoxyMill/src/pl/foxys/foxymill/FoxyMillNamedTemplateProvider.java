/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill;

import pl.foxys.foxymill.templates.FoxyMillParser;
import pl.trzy0.foxy.commons.ITextProvider;
import pl.trzy0.foxy.commons.templates.PlainNamedTemplateProvider;

/**
 *
 * @author wezyr
 */
public class FoxyMillNamedTemplateProvider extends PlainNamedTemplateProvider {
    public FoxyMillNamedTemplateProvider(ITextProvider textProvider) {
        super(textProvider, new FoxyMillParser());
    }
}
