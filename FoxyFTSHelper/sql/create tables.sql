create sequence qna_wwfts_word_seq;

create table qna_wwfts_word (
  id bigint not null primary key default nextval('qna_wwfts_word_seq'),
  word varchar(410) not null unique
);

create sequence qna_wwfts_obj_q_seq;

create table qna_wwfts_obj_q (
  id int not null primary key default nextval('qna_wwfts_obj_q_seq'),
  obj_id int not null,
  field_name varchar(255) not null,
  word_id bigint not null references qna_wwfts_word (id),
  weight double precision not null,
  kind varchar(50) not null,
  constraint fk_wwfts_q_ak_objid_fn_wordid unique (obj_id, field_name, word_id)
);


create sequence qna_wwfts_session_seq;

create sequence qna_wwfts_word4obj_seq;

-- drop table qna_wwfts_word4obj;
create table qna_wwfts_word4obj (
  id int not null primary key default nextval('qna_wwfts_word4obj_seq'),
  session_id bigint not null,
  obj_id int not null,
  field_name varchar(255) not null,
  word varchar(1000) not null,
  weight double precision not null,
  kind varchar(50) not null,
  word_id int
);

create index idx_qna_wwfts_word4obj on qna_wwfts_word4obj (session_id);


/*

update qna_wwfts_word4obj set word_id = w.id from qna_wwfts_word w where w.word = qna_wwfts_word4obj.word and qna_wwfts_word4obj.session_id = -1

insert into qna_wwfts_word (word) select word from qna_wwfts_word4obj where word_id is null and session_id = -1

update qna_wwfts_word4obj set word_id = w.id from qna_wwfts_word w where w.word = qna_wwfts_word4obj.word and word_id is null and qna_wwfts_word4obj.session_id = -1

insert into qna_wwfts_obj_q (obj_id, field_name, word_id, weight, kind) select obj_id, field_name, word_id, weight, kind from qna_wwfts_word4obj where session_id = -1

delete from qna_wwfts_word4obj where session_id = -1



select * from qna_wwfts_word

select * from qna_wwfts_obj_q

select * from qna_wwfts_word4obj

delete from qna_wwfts_obj_q where obj_id = 1

truncate table qna_wwfts_word4obj, qna_wwfts_obj_q, qna_wwfts_word;


*/