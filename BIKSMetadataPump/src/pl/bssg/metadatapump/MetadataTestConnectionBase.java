/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import commonlib.LameUtils;
import java.util.Set;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public abstract class MetadataTestConnectionBase {

    protected MssqlConnection mc;
    protected INamedSqlsDAO<Object> adhocDao;
    protected static String ADHOC_SQLS_PATH = LameUtils.getPackagePathOfClass(MetadataTestConnectionBase.class) + "metadataAdhocSqls.html";

    public MetadataTestConnectionBase(MssqlConnectionConfig mcfg) {
        if (mcfg.instance != null && mcfg.instance.trim().equals("")) {
            mcfg.instance = null;
        }
        this.mc = new MssqlConnection(mcfg);
        this.mc.setDbStatementExecutionWarnThresholdMillis(-1);
        this.adhocDao = createAdhocDao(mc);
    }

    public MetadataTestConnectionBase() {
    }

    private static INamedSqlsDAO<Object> createAdhocDao(IBeanConnectionEx<Object> mc) {
        return FoxyAppUtils.createNamedSqlsDAOFromJar(mc, ADHOC_SQLS_PATH);
    }

    protected <V> V execNamedQuerySingleVal(String qryName, boolean allowNoResult,
            String optColumnName, Object... args) {
        return BeanConnectionUtils.<V>execNamedQuerySingleVal(adhocDao, qryName, allowNoResult,
                optColumnName, args);
    }

    protected <V> Set<V> execNamedQuerySingleColAsSet(String qryName, String optColumnName, Object... args) {
        return BeanConnectionUtils.execNamedQuerySingleColAsSet(adhocDao, qryName, optColumnName, args);
    }

    public abstract Pair<Integer, String> testConnection();
}
