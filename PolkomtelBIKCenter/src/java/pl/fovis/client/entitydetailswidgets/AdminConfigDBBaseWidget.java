/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import java.util.List;
import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuationWithReturn;

/**
 *
 * @author tflorczak
 */
public abstract class AdminConfigDBBaseWidget extends AdminConfigManyServersBase<List<ConnectionParametersDBServerBean>, ConnectionParametersDBServerBean> {

    protected TextBox tbxName;
    protected TextBox tbxServer;
    protected TextBox tbxInstance;
    protected TextBox tbxUser;
    protected TextBox tbxPw;
    protected TextBox tbxPort;
//    protected CheckBox chbPw;
    protected Label lblInstance;
    protected Label lblPort;
    protected Label lblDatabaseFilter;
    protected Label lblObjectFilter;
    protected Label lblObjectFilter2;
    protected TextBox tbxDatabaseFilter;
    protected TextBox tbxObjectFilter;
    protected CheckBox chbIsActive;
    protected int serverId;

    @Override
    protected void buildRightWidget(CellPanel panelRight) {
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
            }
        }, false, panelRight);
        createLine(panelRight);
        createLabel(I18n.nazwa.get() + ":", panelRight);
        tbxName = createTextBox(panelRight, false);
        tbxName.setEnabled(isNameEditable());
        createLabel(I18n.adresSerwera.get() + ":", panelRight);
        tbxServer = createTextBox(panelRight);
        lblInstance = createLabel(getInstanceText(), panelRight);
        tbxInstance = createTextBox(panelRight);
        createLabel(I18n.uzytkownik.get() + ":", panelRight);
        tbxUser = createTextBox(panelRight);
        createLabel(I18n.haslo.get() + ":", panelRight);
        tbxPw = createPasswordTextBox(panelRight);
//        chbPw = createAndAddCheckBox(I18n.ukryjHaslo.get(), new ValueChangeHandler<Boolean>() {
//            @Override
//            public void onValueChange(ValueChangeEvent<Boolean> event) {
//                tbxPw.getElement().setAttribute("type" /* I18N: no */, chbPw.getValue() ? "password" /* I18N: no */ : "text" /* I18N: no */);
//            }
//        }, panelRight);
        lblPort = createLabel(I18n.port.get() + ":", panelRight);
        tbxPort = createTextBox(panelRight);
        lblDatabaseFilter = createLabel(I18n.databaseFilter.get() + ":", panelRight);
        tbxDatabaseFilter = createTextBox(panelRight);
        lblObjectFilter = createLabel(I18n.optionalObjectFilterUseTAsAliasT.get() /* i18n:  */ + "):", panelRight);
        lblObjectFilter2 = createLabel("(<databasename>=<wherestring>;<databasename2>=<wherestring2>;)", panelRight);
        tbxObjectFilter = createTextBox(panelRight);
        createLine(panelRight);
        createButton(I18n.zapisz.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (serverNames.contains(tbxName.getText()) && !tbxName.getText().equals(listbox.getItemText(getSelectedIndex()))) {
                    Window.alert(I18n.istniejeJuzTakaNazwaSerwera.get() /* I18N:  */);
                } else {
                    final ConnectionParametersDBServerBean bean = new ConnectionParametersDBServerBean();
                    bean.id = serverId;
                    bean.name = tbxName.getText();
                    bean.server = tbxServer.getText();
                    bean.instance = tbxInstance.getText();
                    bean.user = tbxUser.getText();
                    bean.port = BaseUtils.tryParseInteger(tbxPort.getText());
                    bean.databaseFilter = tbxDatabaseFilter.getText();
                    bean.objectFilter = tbxObjectFilter.getText();
                    bean.password = tbxPw.getText();
                    bean.isActive = chbIsActive.getValue();
                    openWaitDialogAndCallService(bean, new IContinuation() {

                        @Override
                        public void doIt() {
                            serversMap.put(String.valueOf(bean.id), bean);
                            serverNames.remove(listbox.getItemText(getSelectedIndex()));
                            serverNames.add(bean.name);
                            listbox.setItemText(getSelectedIndex(), bean.name);
                            BIKClientSingletons.showInfo(I18n.zmienionoParametryDoPolaczenia.get() /* I18N:  */ + ": " + getSourceName());
                        }
                    });
                }
            }
        }, panelRight);
        createTestConnectionWidget(panelRight, new IParametrizedContinuationWithReturn<Void, Integer>() {

            @Override
            public Integer doIt(Void param) {
                return serverId;
            }
        });

        lblPort.setVisible(isPortAvailable());
        tbxPort.setVisible(isPortAvailable());
        lblInstance.setVisible(isInstanceAvailable());
        tbxInstance.setVisible(isInstanceAvailable());
        lblDatabaseFilter.setVisible(isAdditionalDatabaseFilterAvailable());
        tbxDatabaseFilter.setVisible(isAdditionalDatabaseFilterAvailable());
        lblObjectFilter.setVisible(isAdditionalObjectFiltersAvailable());
        lblObjectFilter2.setVisible(isAdditionalObjectFiltersAvailable());
        tbxObjectFilter.setVisible(isAdditionalObjectFiltersAvailable());
    }

    @Override
    protected void activateForServer(String id) {
        ConnectionParametersDBServerBean bean = serversMap.get(id);
        tbxName.setText(bean.name);
        tbxServer.setText(bean.server);
        tbxInstance.setText(bean.instance);
        tbxUser.setText(bean.user);
        tbxPw.setText(bean.password);
        tbxPort.setText(BaseUtils.safeToStringNotNull(bean.port));
        tbxDatabaseFilter.setText(bean.databaseFilter);
        tbxObjectFilter.setText(bean.objectFilter);
        chbIsActive.setValue(bean.isActive);
        setWidgetsEnabled(chbIsActive.getValue());
        serverId = Integer.parseInt(id);
//        resultTest.setText("");
    }

    protected String getInstanceText() {
        return I18n.instancja.get() + ":";
    }

    protected boolean isPortAvailable() {
        return true;
    }

    protected boolean isInstanceAvailable() {
        return true;
    }

    protected boolean isAdditionalDatabaseFilterAvailable() {
        return false;
    }

    protected boolean isAdditionalObjectFiltersAvailable() {
        return false;
    }

    protected boolean setAlsoSchedule() {
        return false;
    }

    protected boolean isNameEditable() {
        return true;
    }

    protected void openWaitDialogAndCallService(ConnectionParametersDBServerBean bean, final IContinuation con) {
        final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
        infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.zapisywanieParametrowPolaczenProszeCzekac.get(), true);
        callServiceToSaveData(bean, new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " runTestConnection: " + getSourceName()) {

                    @Override
                    public void onSuccess(Void result) {
                        infoDialog.hideDialog();
                        con.doIt();
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        infoDialog.hideDialog();
                        super.onFailure(caught);
                    }
                });
    }

    protected abstract void callServiceToSaveData(ConnectionParametersDBServerBean bean, AsyncCallback<Void> asyncCallback);

    protected abstract String getTreeCodeForServer();

    @Override
    protected void callServiceToAddNewServer(String source, String serverName, boolean needSeparateSchedule, AsyncCallback<ConnectionParametersDBServerBean> asyncCallback) {
        BIKClientSingletons.getService().addDBConfig(source, serverName, needSeparateSchedule, getTreeCodeForServer(), asyncCallback);
    }

    @Override
    protected void callServiceToDeleteServer(int id, String serverName, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().deleteDBConfig(id, getSourceName(), serverName, asyncCallback);
    }
}
