/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.mltx;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.io.Serializable;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.mltx.history.MultiXUrlCreator;
import pl.fovis.common.BIKRightRoles.RightRole;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.FoxyDialogSerializer;
import pl.fovis.foxygwtcommons.dialogs.FoxyValidatingDialogBase;
import pl.fovis.foxygwtcommons.dialogs.IAsyncDialogAction;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author uwa
 */
public class InviteUsersDialog extends FoxyValidatingDialogBase<Serializable> {

    private final String RADIO_GROUP = "radioGroup";
    private VerticalPanel vp;
    private TextArea mails;
    private RadioButton regular;
    private RadioButton expert;
    private RadioButton admin;

    @Override
    protected String getDialogCaption() {
        return I18n.zaprosUzytkownikow.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.dodaj.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        setSerializer(new FoxyDialogSerializer<Serializable, Serializable, Serializable>(null, null, this));
        vp = new VerticalPanel();
        vp.setStyleName("inviteUsersMain");
        addUser();
        main.add(vp);
        regular = new RadioButton(RADIO_GROUP, I18n.zwyklyUzytkownik.get());
        expert = new RadioButton(RADIO_GROUP, I18n.rightRoleExpert.get());
        admin = new RadioButton(RADIO_GROUP, I18n.rightRoleAppAdmin.get());
        regular.setValue(true);
        VerticalPanel userType = new VerticalPanel();
        userType.add(regular);
        
        /* zmienić na nowe role  kiedy jakie mozna dodawać*/
        if (BIKClientSingletons.isEffectiveExpertLoggedIn()) {
            /*Ekspert -> Do jakich drzew?,Redaktor */
            userType.add(expert);
        }
        if (BIKClientSingletons.isAppAdminLoggedIn()) {
            userType.add(admin);
        }
        main.add(userType);

    }

    private String getRole() {
        if (regular.getValue()) {
            return RightRole.RegularUser.getCode();
        } else if (expert.getValue()) {
            return RightRole.Expert.getCode();
        } else {
            return RightRole.AppAdmin.getCode();
        }
    }

    ;

    private void addUser() {
        vp.add(new Label(I18n.podajAdresyUzytkownikow.get()));
        mails = new TextArea();
        vp.add(mails);
    }

    @Override
    protected Serializable createBean() {
        return null;
    }

    @Override
    protected void afterBuildAllWidgetsBeforeShow() {
        //no op
    }

    @Override
    protected boolean checkForInvalidValues() {
        return false;
    }

    @Override
    protected void fillControlsFromBean(Serializable inputBean) {
        //no op
    }

    @Override
    protected void fillBeanFromControls(Serializable outputBean) {
        //no op
    }

    @Override
    protected <B extends Serializable> void fillListBox(ListBox lb, Iterable<B> beans, String idProp, String nameProp, String selectedValue) {
        //no op
    }

    private String getMails() {
        return mails.getText();
    }

    public IAsyncDialogAction<Serializable> getContinuation() {
        return new IAsyncDialogAction<Serializable>() {
            public void doIt(Serializable bean, final IParametrizedContinuation<String> onCompleteCont) {
                String propUrl = MultiXUrlCreator.createUrl();
                BIKClientSingletons.getMultiXService().sendInvitationEmail(getMails(), getRole(), propUrl, new StandardAsyncCallback<String>() {
                    public void onSuccess(String result) {
                        onCompleteCont.doIt(result);
                        if (result == null) {
                            BIKClientSingletons.showInfo(I18n.naPodanyAdresWyslanoWiadomosc.get());
                        }
                    }
                });
            }
        };
    }
}
