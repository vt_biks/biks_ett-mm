/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.TreeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
//import simplelib.IContinuation;
import simplelib.IContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class SearchTreeFilterWidget extends SearchFilterBase {

    protected CheckBox allTreesCb;
    protected Map<Integer, CheckBox> allTreesCheckBoxes = new HashMap<Integer, CheckBox>();
    protected Map<Integer, List<Integer>> nodeKindIdsInTree = new HashMap<Integer, List<Integer>>();
    protected int totalCnt = 0;
    protected IContinuation ic;

    public SearchTreeFilterWidget(IContinuation ic) {
        this.ic = ic;
    }

    @Override
    protected VerticalPanel buildWidget() {
        VerticalPanel vp = new VerticalPanel();
        vp.add(new Label(I18n.drzewa.get()));
        vp.addStyleName("lblPadding Search");

        ScrollPanel treesSP = new ScrollPanel();
        treesSP.setHeight("170px");
        treesSP.setWidth("300px");
        VerticalPanel checkboxesVp = new VerticalPanel();
        allTreesCb = new CheckBox(I18n.wszystkieDrzewa.get());
        allTreesCb.setValue(true);
        allTreesCb.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                for (CheckBox cb : allTreesCheckBoxes.values()) {
                    if (cb.isVisible()) {
                        cb.setValue(allTreesCb.getValue());
                    }
                }
                ic.doIt();
            }
        });

        for (TreeBean treeBean : BIKClientSingletons.getTrees()) {
            if (!BIKConstants.TREE_CODE_TREEOFTREES.equals(treeBean.code) && !treeBean.treeKind.startsWith(BIKConstants.METABIKS_PREFIX)) {

                nodeKindIdsInTree.put(treeBean.id, treeBean.nodeKindIds);

                CheckBox cb = new CheckBox(treeBean.name);
                cb.setTitle(BIKClientSingletons.getMenuPathForTreeId(treeBean.id));
                cb.setValue(true);
                cb.setFormValue(BaseUtils.safeToString(treeBean.id));
                allTreesCheckBoxes.put(treeBean.id, cb);
                cb.addClickHandler(new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        boolean all = true;
                        for (CheckBox x : allTreesCheckBoxes.values()) {
                            if (!x.getValue() && x.isVisible()) {
                                all = false;
                                break;
                            }
                        }
                        allTreesCb.setValue(all);
                        ic.doIt();
                    }
                });
                checkboxesVp.add(cb);
            }
        }
        treesSP.add(checkboxesVp);
        vp.add(filterPanel(filterTb = new TextBox()));
        vp.add(getAllCheckboxFpWithStyle(allTreesCb));
        vp.add(treesSP);
        return vp;
    }

    protected Set<Integer> getSelectedTreeIds() {
        Set<Integer> selectedTreeIds = new HashSet<Integer>();

        for (CheckBox cb : allTreesCheckBoxes.values()) {
            if (cb.getValue()) {
                selectedTreeIds.add(BaseUtils.tryParseInt(cb.getFormValue()));
            }
        }
        return selectedTreeIds;
    }

    protected int getTotalCnt() {
        return totalCnt;
    }

    protected void updateCheckBoxes(Map<Integer, Integer> treeCnt) {
        totalCnt = 0;
        for (Integer cnt : treeCnt.values()) {
            totalCnt += cnt;
        }

        allTreesCb.setText(I18n.wszystkieDrzewa.get() + "(" + totalCnt + ")");
        for (TreeBean treeBean : BIKClientSingletons.getTrees()) {
            if (!BIKConstants.TREE_CODE_TREEOFTREES.equals(treeBean.code) && !treeBean.treeKind.startsWith(BIKConstants.METABIKS_PREFIX)) {
                CheckBox cb = allTreesCheckBoxes.get(treeBean.id);

                Integer cnt = treeCnt.get(treeBean.id);
                if (cnt == null) {
                    cnt = 0;
                }
                String txt = treeBean.name + "(" + cnt + ")";
                cb.setHTML(txt);
                if (cnt > 0) {
                    cb.setStyleName("boldText");
                } else {
                    cb.removeStyleName("boldText");
                    cb.setStyleName("gwt-CheckBox");
                }
            }
        }
    }

    protected void filteredTree(String filtr) {

        for (CheckBox cb : allTreesCheckBoxes.values()) {
            if (BaseUtils.isStrEmptyOrWhiteSpace(filtr) || cb.getHTML().toLowerCase().contains(filtr.toLowerCase())) {
                cb.setVisible(true);
                cb.setValue(true);
            } else {
                cb.setVisible(false);
                cb.setValue(false);
            }
        }
    }
    
      public void selectOrUnselectAllFilter(boolean check) {
        allTreesCb.setValue(check);
        for (CheckBox cb : allTreesCheckBoxes.values()) {
            cb.setValue(check);
            cb.setVisible(true);
        }
        filterTb.setText("");
    }
    

    protected Pair<Set<Integer>, Boolean> getNodeKindIdsInTrees() {
        boolean isAnythingTreeSelected = false;
        Set<Integer> nodeKindIds = new HashSet<Integer>();
        for (CheckBox cb : allTreesCheckBoxes.values()) {
            if (/*cb.getFormValue() != null &&*/cb.isVisible() && cb.getValue()) {
                isAnythingTreeSelected = true;
                List<Integer> nkids = nodeKindIdsInTree.get(BaseUtils.tryParseInteger(cb.getFormValue()));
                if (!BaseUtils.isCollectionEmpty(nkids)) {
                    for (Integer i : nkids) {
                        nodeKindIds.add(i);
                    }
                }
            }
        }
        return new Pair<Set<Integer>, Boolean>(nodeKindIds, isAnythingTreeSelected);
    }

    protected void filterAction() {
        filteredTree(filterTb.getText());
        ic.doIt();
    }
}
