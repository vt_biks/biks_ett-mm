/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.widgets.sbavc;

import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import pl.fovis.common.sbavc.SearchByAttrValueCriteriaNotEmpty;

/**
 *
 * @author tflorczak
 */
public class SearchByAttrValueCriteriaNotEmptyWidget extends SearchByAttrValueCriteriaWidgetBase {

    @Override
    protected void createOptWidget() {
        // no op
    }

    @Override
    public ISearchByAttrValueCriteria provideBean() {
        return (isValidInput() ? new SearchByAttrValueCriteriaNotEmpty(getSelectedAttrId(), getSelectedAttrCaption(), negationChbx.getValue()) : null);
    }

}
