/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.IInitByJson;
import simplelib.INamedPropsBean;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.logging.LameLoggerMsg;

/**
 *
 * @author wezyr
 */
public class JsonCodeGenerator {
    private static final String VERSION_STRING = "v0.5.2";

    private static String makeHelperVarName(String propName) {
        return "_" + propName + "_helper_";
    }
    private Map<Class, String> generatedClasses = new LinkedHashMap<Class, String>();

    private static Pair<Class, String> resolveCollectionType(Type t) {
        if (t instanceof Class) {
            Class c = (Class)t;
            return new Pair<Class, String>(c, c.getSimpleName());
        } else if (t instanceof TypeVariable) {
            TypeVariable tv = (TypeVariable)t;
            Type[] bounds = tv.getBounds();
            if (bounds.length != 1) {
                throw new LameRuntimeException(
                        "unsupported number (" + bounds.length + ") of bounds for type: " + t +
                        " of class: " + t.getClass().getCanonicalName() +
                        ", bounds: " + Arrays.deepToString(bounds));
            }
            if (bounds[0] instanceof Class) {
                return new Pair<Class, String>((Class)bounds[0], tv.getName());
            } else {
                throw new LameRuntimeException(
                        "unsupported type of single bound (bound: " + bounds[0] +
                        " of class: " + bounds[0].getClass().getCanonicalName() +
                        ") of bounds for type: " + t +
                        " of class: " + t.getClass().getCanonicalName() +
                        ", bounds: " + Arrays.deepToString(bounds));
            }
        } else {
            throw new LameRuntimeException("unsupported type: " + t + " of class: " + t.getClass().getCanonicalName());
        }
    }

    @SuppressWarnings("unchecked")
    private void initByJsonCollectionProp(Class c, String propName, StringBuilder sb, Class propClass) throws LameRuntimeException {
        Type t = MuppetMerger.getPropertyGenericClass(c, propName, true);

        if (!(t instanceof ParameterizedType)) {
            throw new LameRuntimeException("property " + propName + " of class " + c.getCanonicalName() + " has unsupported type " + propClass.getCanonicalName() + " - unknown generic type " + LameUtils.safeGetClassName(t));
        }

        String collClassName;
        if (Set.class.isAssignableFrom(propClass)) {
            collClassName = "LinkedHashSet";
        } else if (propClass.isAssignableFrom(List.class)) {
            collClassName = "ArrayList";
        } else {
            collClassName = propClass.getSimpleName();
        }

        ParameterizedType type = (ParameterizedType) t;
        Type[] typeArguments = type.getActualTypeArguments();
        Pair<Class, String> resolvedType = resolveCollectionType(typeArguments[0]);
        //Class innerClass = (Class) typeArguments[0];
        //String innerClassName = innerClass.extractSimpleClassName();
        Class innerClass = resolvedType.v1;
        String innerClassName = resolvedType.v2;
        
        String helperCollType;
        boolean isInitByJson = false;
        boolean isEnum = false;
        if (innerClass.isAssignableFrom(String.class) ||
                innerClass.isAssignableFrom(Integer.class) ||
                innerClass.isAssignableFrom(Boolean.class) ||
                innerClass.isAssignableFrom(Date.class)) {
            helperCollType = innerClassName;
        } else if (Enum.class.isAssignableFrom(innerClass)) {
            helperCollType = "String";
            isEnum = true;
        } else if (IInitByJson.class.isAssignableFrom(innerClass)) {
            helperCollType = "Map<String, Object>";
            isInitByJson = true;
        } else {
            if (!generatedClasses.containsKey(innerClass)) {
                generatedClasses.put(innerClass, null);
                generatedClasses.put(innerClass, makeInitByJsonMapWorkerWithDeclaration(innerClass));
            }
            helperCollType = "Map<String, Object>";
            isInitByJson = true;
        //throw new LameRuntimeException("class " + innerClassName + " is not assignable from IInitByJson");
        }

        String helperVarName = makeHelperVarName(propName);
        sb.append("  Collection<" + helperCollType + "> " + helperVarName + " = (Collection<" + helperCollType + ">)JsonValueHelper.getCollection(o);\n");
        sb.append("  if (" + helperVarName + " != null) {\n");
        sb.append("    " + propName + " = new " + collClassName + "();\n");
        sb.append("    for (" + helperCollType + " item : " + helperVarName + ") {\n");
        if (isInitByJson) {
            sb.append("      " + innerClassName + " v = new " + innerClassName + "();\n");
            sb.append("      v.initByJsonMap(new MapHelper<String>(item));\n");
            sb.append("      " + propName + ".add(v);\n");
        } else if (isEnum) {
            sb.append("      " + propName + ".add(BaseUtils.safeStringToEnum(" + innerClassName + ".class, item));\n");
        } else {
            sb.append("      " + propName + ".add(item);\n");
        }
        sb.append("    }\n");
        sb.append("  } else { " + propName + " = null; }\n");
    }

    private static String getInitMethodHeader() {
        return "public void initByJsonMap(MapHelper<String> mh) {\n";
    }

    private static String getInitMethodFooter() {
        return "}\n";
    }

    private String makeInitByJsonMapWorkerWithDeclaration(Class c) {
        return getInitMethodHeader() + makeInitByJsonMapWorker(c) + getInitMethodFooter();
    }

    @SuppressWarnings("unchecked")
    private void makeSetter(String propName, StringBuilder sb, Class c) {
        Class propClass = MuppetMerger.getPropertyClass(c, propName, true);
            String accessMethod = null;
            String accessPrefix = "";
            if (propClass.isAssignableFrom(int.class)) {
                accessMethod = "Int";
            } else if (propClass.isAssignableFrom(Integer.class)) {
                accessMethod = "Integer";
            } else if (propClass.isAssignableFrom(String.class)) {
                accessMethod = "String";
            } else if (propClass.isAssignableFrom(boolean.class)) {
                accessMethod = "Bool";
            } else if (propClass.isAssignableFrom(double.class)) {
                accessMethod = "Double";
            } else if (propClass.isAssignableFrom(Double.class)) {
                accessMethod = "Double";
            } else if (propClass.isAssignableFrom(Boolean.class)) {
                accessMethod = "Boolean";
            } else if (propClass.isAssignableFrom(Date.class)) {
                accessMethod = "Date";
            } else if (Map.class.isAssignableFrom(propClass)) {
                accessMethod = "Map";
                accessPrefix = "(Map)";
            }

            if (accessMethod != null) {
                sb.append("  " + propName + " = " + accessPrefix + "JsonValueHelper.get" + accessMethod + "(o);\n");
            } else {
                if (Collection.class.isAssignableFrom(propClass)) {
                    initByJsonCollectionProp(c, propName, sb, propClass);
                } else if (IInitByJson.class.isAssignableFrom(propClass)) {
                    sb.append("  if (o != null) {\n");
                    sb.append("  " + propName + " = new " + propClass.getSimpleName() + "();\n");
                    sb.append("  " + propName + ".initByJsonMap(JsonValueHelper.getMapHelper(o));\n");
                    sb.append("  } else { " + propName + " = null; }\n");
                } else if (Enum.class.isAssignableFrom(propClass)) {
                    sb.append("  " + propName + " = BaseUtils.safeStringToEnum(" + propClass.getSimpleName() + ".class, JsonValueHelper.getString(o));\n");
//                    String helperVarName = makeHelperVarName(propName);
//                    sb.append("  String " + helperVarName + " = mh.getString(\"" + propName + "\");\n");
//                    sb.append("  " + propName + " = " + helperVarName +
//                            " == null ? null : Enum.valueOf(" + propClass.extractSimpleClassName() + ".class, " + helperVarName + ");\n");
                } else {
                    throw new LameRuntimeException("property " + propName + " of class " + c.getCanonicalName() +
                            " has unsupported type " + propClass.getCanonicalName());
                }
            }
    }

    private String makeSetters(Class c) {
        Set<String> props = MuppetMerger.getMuppetProps(c, 3);
        StringBuilder sb = new StringBuilder();

        for (String propName : props) {
            sb.append("  private void set" + BaseUtils.capitalize(propName) + "(Object o) {\n");
            makeSetter(propName, sb, c);
            sb.append("  }\n");
        }

        return sb.toString();
    }

    private String makeInitByJsonMapWorker(Class c) {
        Set<String> props = MuppetMerger.getMuppetProps(c, 3);
        StringBuilder sb = new StringBuilder();

        for (String propName : props) {
            sb.append("  set" + BaseUtils.capitalize(propName) + "(mh.get(\"" + propName.replace("Dot",".") + "\"));\n");
        }

        return sb.toString();
    }

    public static String makeInitByJsonMap(Class c) {
        JsonCodeGenerator jcg = new JsonCodeGenerator();

        String res = jcg.makeInitByJsonMapWorker(c);
        StringBuilder sb = new StringBuilder();

        sb.append("  public static List<" + c.getSimpleName() + "> create" + c.getSimpleName() + "List(String jsonString) {\n");
        sb.append("    List<" + c.getSimpleName() + "> res = new ArrayList<" + c.getSimpleName() + ">();\n");
        sb.append("    Collection<Object> data = JsonReader.readList(jsonString);\n");
        sb.append("    for (Object d : data) {\n");
        sb.append("      @SuppressWarnings(\"unchecked\")\n");
        sb.append("      MapHelper<String> mh = new MapHelper<String>((Map<String, Object>)d);\n");
        sb.append("      " + c.getSimpleName() + " b = new " + c.getSimpleName() + "(mh);\n");
        sb.append("      res.add(b);\n");
        sb.append("    }\n");
        sb.append("  return res;\n");
        sb.append("  }\n\n");
        sb.append("  public " + c.getSimpleName() + "() {\n");
        sb.append("  }\n\n");
        sb.append("  public " + c.getSimpleName() + "(String jsonString) {\n");
        sb.append("    this(JsonReader.readMapHelper(jsonString));\n");
        sb.append("  }\n\n");
        sb.append("  public " + c.getSimpleName() + "(MapHelper<String> mh) {\n");
        sb.append("    initByJsonMap(mh);\n");
        sb.append("  }\n\n");
        sb.append(jcg.makeSetters(c) + "\n");
        
        sb.append(getInitMethodHeader());

        for (Entry<Class, String> e : jcg.generatedClasses.entrySet()) {
            sb.append("/*****************************************\n");
            sb.append("// --- aux generated method for " + e.getKey().getCanonicalName() + " starts\n");
            sb.append(e.getValue());
            sb.append("// --- aux generated method for " + e.getKey().getCanonicalName() + " ends\n");
            sb.append("*****************************************/\n\n");
        }

        sb.append(res);
        sb.append(getInitMethodFooter());

        return sb.toString();
    }

    public static String makeNamedPropsBean(Class c) {
        String className = c.getSimpleName();
        Set<String> props = MuppetMerger.getMuppetProps(c, 3);
        StringBuilder sb = new StringBuilder();

        sb.append("private static Map<String, INamedPropGetterSetter<" + className +
                ">> namedProps = new LinkedHashMap<String, INamedPropGetterSetter<" +
                className + ">>();\n");
        sb.append("static {\n");
        for (String prop : props) {
            sb.append("  namedProps.put(\"" + prop.replace("Dot", ".") + "\", new INamedPropGetterSetter<" + className + ">(){\n");
            sb.append("  public Object getValue(" + className + " o) { return o." + prop + "; }\n");
            sb.append("  public void setValue(" + className + " o, Object value) { o.set" + BaseUtils.capitalize(prop) + "(value); } });\n");
        }
        sb.append("}\n");

        sb.append("  public Set<String> getPropNames() { return namedProps.keySet(); }");
        sb.append("  public Object getPropValue(String propName) {\n");
        sb.append("    INamedPropGetter<" + className + "> getter = namedProps.get(propName);\n");
        sb.append("    if (getter == null) throw new LameRuntimeException(\"no prop with name: \" + propName + \" in class: \" + getClass().getName());\n");
        sb.append("    return getter.getValue(this);\n  }");
        sb.append("  public void setPropValue(String propName, Object value) {\n");
        sb.append("    INamedPropSetter<" + className + "> setter = namedProps.get(propName);\n");
        sb.append("    if (setter == null) throw new LameRuntimeException(\"no prop with name: \" + propName + \" in class: \" + getClass().getName());\n");
        sb.append("    setter.setValue(this, value);\n  }");
        return sb.toString();
    }

    public static String makeMethods(Class c) {
        String code = makeInitByJsonMap(c);
        if (INamedPropsBean.class.isAssignableFrom(c)) {
            code = code + makeNamedPropsBean(c);
        }
        
        return "\n\n//////////////////////////////////////////\n\n\n" +
                "// <editor-fold defaultstate=\"collapsed\" desc=\"Autogenerated by JsonCodeGenerator " + VERSION_STRING + "\">\n\n" +
                code +
                "\n\n// </editor-fold>\n" +
                "\n\n\n//////////////////////////////////////////\n\n";
    }

    public static void makeAndPrintMethods(Class c) {
        System.out.println(makeMethods(c));
    }

    public static void makeAndPrintMethodsForCurrentClass() {
        makeAndPrintMethods(LameUtils.getCurrentClass(1));
    }

    public static void main(String[] args) {
        makeAndPrintMethods(LameLoggerMsg.class);
    }
}
