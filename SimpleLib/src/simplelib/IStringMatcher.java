/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author pmielanczuk
 */
public interface IStringMatcher {

    public boolean find();

    public boolean find(int startPos);

    public int start();

    public int end();
}
