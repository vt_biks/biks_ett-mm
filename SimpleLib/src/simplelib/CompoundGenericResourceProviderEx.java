/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.Set;
import simplelib.BaseUtils.Projector;

/**
 *
 * @author wezyr
 */
public class CompoundGenericResourceProviderEx<T1, T2> extends CompoundGenericResourceProvider<T1, T2>
        implements IGenericResourceProviderEx<T2> {

    private IGenericResourceProviderEx<T1> providerEx;

    public CompoundGenericResourceProviderEx(IGenericResourceProviderEx<T1> providerEx,
            Projector<T1, T2> proj) {
        super(providerEx, proj);
        this.providerEx = providerEx;
    }

    public Set<String> getResourceNames(String base, Set<String> fileExts, boolean recurseSubDirs) {
        return providerEx.getResourceNames(base, fileExts, recurseSubDirs);
    }
}
