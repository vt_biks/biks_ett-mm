/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import java.util.HashMap;
import java.util.Map;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.DataSourceStatusBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.SimpleDateUtils;

/**
 *
 * @author beata
 */
public class DataSourceStatusBeanExtractor extends NodeAwareBeanExtractorBase<DataSourceStatusBean> {

    public DataSourceStatusBeanExtractor() {
        super(false);
    }

    @Override
    public boolean hasIcon() {
        return false;
    }
    //dla tych bez buttonow(przejdz,usun itp) i ikonek trzeba też klasę bazową

    @Override
    protected String getNodeKindCode(DataSourceStatusBean b) {
        return null;
    }

    protected String getName(DataSourceStatusBean b) {
        return null;
    }

    public Integer getNodeId(DataSourceStatusBean b) {
        return null;
    }

    public String getTreeCode(DataSourceStatusBean b) {
        return null;
    }

    public String getBranchIds(DataSourceStatusBean b) {
        return null;
    }

    @Override
    public int getAddColCnt() {
        return 5;
    }

    @Override
    public Object getAddColVal(DataSourceStatusBean b, int addColIdx) {
        Map<Integer, Object> m = new HashMap<Integer, Object>();
        HTML systemName = new HTML(b.system);
        systemName.setStyleName("Notes" /* i18n: no:css-class */ + "-labBody");
        m.put(0, systemName);
        m.put(1, new Label(b.server));
        m.put(2, new Label(SimpleDateUtils.toCanonicalString(b.startTime)));

//        int minutes = (int) SimpleDateUtils.getElapsedSeconds(b.startTime, b.finishTime) / 60;
//        int excessiveSec = (int) SimpleDateUtils.getElapsedSeconds(b.startTime, b.finishTime) % 60;
        String elapsedTimeStr = "";
//        if (minutes != 0) {
//            elapsedTimeStr = minutes + " m ";
        if (b.finishTime != null) {
            int minutes = (int) SimpleDateUtils.getElapsedSeconds(b.startTime, b.finishTime) / 60;
            int excessiveSec = (int) SimpleDateUtils.getElapsedSeconds(b.startTime, b.finishTime) % 60;

            if (minutes != 0) {
                elapsedTimeStr = minutes + " m ";
            }
            elapsedTimeStr = elapsedTimeStr + excessiveSec + " s";
        }
//        elapsedTimeStr = elapsedTimeStr + excessiveSec + " s";

        Label lblTimeElapsed = new Label((b.finishTime != null)
                ? elapsedTimeStr : I18n.zasilanieWTrakcie.get() /* I18N:  */);

        lblTimeElapsed.setTitle(I18n.lacznyCzasPrzetwarzaniaDanychPrzezKonektor.get());
        m.put(3, lblTimeElapsed);

        m.put(4, b.status);
        return m.get(addColIdx);
    }

    @Override
    public boolean createGridColumnNames(FlexTable gp) {
        gp.setStyleName("gridJoinedObj");
        gp.setWidget(0, 0, new HTML(I18n.konektor.get()));
        gp.setWidget(0, 1, new HTML(I18n.serwer.get()));
        gp.setWidget(0, 2, new HTML(I18n.dataUruchomienia.get() /* I18N:  */));
        gp.setWidget(0, 3, new HTML(I18n.czasTrwaniaS.get() /* I18N:  */ + "(min)"));
        gp.setWidget(0, 4, new HTML(I18n.status.get()));
        gp.getColumnFormatter().setWidth(0, "15%");
        gp.getColumnFormatter().setWidth(1, "15%");
        gp.getColumnFormatter().setWidth(2, "15%");
        gp.getColumnFormatter().setWidth(3, "15%");
        gp.getColumnFormatter().setWidth(4, "40%");
        gp.getRowFormatter().setStyleName(0, "RelatedObj-oneObj-dqc");
        return true;
    }

    @Override
    public int getActionCnt() {
        return 0;
    }
}
