/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.lisa;

import com.google.gwt.http.client.Request;
import java.util.Map;
import pl.fovis.common.lisa.LisaDictionaryValue;
import pl.fovis.common.lisa.LisaGridParamBean;
import pl.fovis.foxygwtcommons.grid.IBeanProvider;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author pku
 */
public class ChangableBeanProvider implements IBeanProvider<Map<String, LisaDictionaryValue>> {

    private Integer categoryId;
    private String nodeKindCode;
    private Request requestInProgress = null;
    private String requestTicket = null;

    ChangableBeanProvider(Integer nodeId, String nodeKindCode) {
        this.categoryId = nodeId;
        this.nodeKindCode = nodeKindCode;
    }

    @Override
    public void getBeans(int offset, int limit, Iterable<String> sortCols, Iterable<String> descendingCols,
            final IParametrizedContinuation<Pair<? extends Iterable<Map<String, LisaDictionaryValue>>, Integer>> showBeansCont) {

        final LisaGridParamBean params = new LisaGridParamBean(offset, limit, categoryId, nodeKindCode);

//        new LisaAuthorizedRequest<List<Map<String, LisaDictionaryValue>>>() {
//            @Override
//            protected void performServerAction(AsyncCallback<List<Map<String, LisaDictionaryValue>>> callback) {
//                //BIKClientSingletons.cancelRequestIfRunning(requestInProgress, requestTicket);
//                // requestTicket=BIKClientSingletons.getCancellableRequestTicket();
//                requestInProgress = BIKClientSingletons.getLisaService().getDictionaryRecords(params, requestTicket, callback);
//            }
//
//            @Override
//            public void onActionSuccess(List<Map<String, LisaDictionaryValue>> result) {
//                Pair<List<Map<String, LisaDictionaryValue>>, Integer> p = new Pair<List<Map<String, LisaDictionaryValue>>, Integer>();
//                p.v1 = result;
//                p.v2 = 100000;
//                showBeansCont.doIt(p);
//            }
//        }.go();
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public void setNodeKindCode(String nodeKindCode) {
        this.nodeKindCode = nodeKindCode;
    }
}
