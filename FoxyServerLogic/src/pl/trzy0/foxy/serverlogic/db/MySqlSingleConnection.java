/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import java.sql.Connection;
import java.sql.DriverManager;
import pl.trzy0.foxy.serverlogic.IConnectionDialectHelper;

/**
 *
 * @author wezyr
 */
public class MySqlSingleConnection extends JdbcSingleConnectionBase {

//    protected SqlValueFormatterCurlyTimeStamp formatter = new SqlValueFormatterCurlyTimeStamp();
//    protected Connection conn;

    public static Connection createConnectionForMySql(String userName, String password, String serverName, String databaseName) {
        String url = "jdbc:mysql://" + serverName + "/" + databaseName;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = DriverManager.getConnection(url, userName, password);
            return conn;
        } catch (Exception ex) {
            throw new RuntimeException("error creating mysql jdbc connection objects", ex);
        }
    }

    public MySqlSingleConnection(String userName, String password, String serverName, String databaseName) {
        super(createConnectionForMySql(userName, password, serverName, databaseName));
//
//        String url = "jdbc:mysql://" + serverName + "/" + databaseName;
//        try {
//            Class.forName("com.mysql.jdbc.Driver").newInstance();
//            conn = DriverManager.getConnection(url, userName, password);
//        } catch (Exception ex) {
//            throw new RuntimeException("error creating mysql jdbc connection objects", ex);
//        }
    }

    @Override
    protected IConnectionDialectHelper createDialectHelper() {
        return new SqlValueFormatterCurlyTimeStamp();
    }
//    @Override
//    public String valToString(Object val) {
//        return formatter.valToString(val);
//    }
//
//    @Override
//    public int readSequenceNextVal(String seqName) {
//        throw new UnsupportedOperationException("No support for sequences");
//    }
}
