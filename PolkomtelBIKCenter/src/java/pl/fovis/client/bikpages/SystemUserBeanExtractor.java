/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import static pl.fovis.client.BIKClientSingletons.isNodeActionEnabledByCustomRightRoles;
import pl.fovis.client.ICheckEnabledByDesign;
import pl.fovis.client.dialogs.SystemUserDialog;
import pl.fovis.client.entitydetailswidgets.GenericBeanExtractorBase;
import pl.fovis.client.entitydetailswidgets.IGridBeanAction;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BIKRightRoles;
import pl.fovis.common.CustomRightRoleBean;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.TreeNodeBranchBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author mmastalerz
 */
public class SystemUserBeanExtractor extends GenericBeanExtractorBase<SystemUserBean> {

    protected Integer sortColNr = null;
    protected Boolean isDownSort = false;
    protected Map<Integer, Map<Boolean, Pair<Image, Image>>> mapOfDownAndUpArrowsForColumnsWithSort = new LinkedHashMap<Integer, Map<Boolean, Pair<Image, Image>>>();
    protected Map<Integer, SystemUserSortType> mapOfSortTypeForColumn = new LinkedHashMap<Integer, SystemUserSortType>();
//    protected Map<String, RightRoleBean> mapOfAllRightRoleByCode;
    Set<Label> usersRolesFromGroup = new HashSet<Label>();

    public SystemUserBeanExtractor() {
        super(false);
    }

    protected boolean areActionsEnabledOnSysUser(final SystemUserBean sub) {
        if (BIKClientSingletons.disableBuiltInRoles()) {
//            isNodeActionEnabledByCustomRightRoles(BIKConstants.ACTION_OTHER_ACTIONS_APP_ADMIN, new ICheckEnabledByDesign() {
//
//                @Override
//                public boolean isEnabledByDesign() {
//                    return false;
//                }
//            });
            boolean isSys = isNodeActionEnabledByCustomRightRoles(BIKConstants.ACTION_OTHER_ACTIONS_SYS_ADMIN, new ICheckEnabledByDesign() {

                @Override
                public boolean isEnabledByDesign() {

                    return true;
                }
            });

            final Map<Integer, Set<TreeNodeBranchBean>> customRightRoleUserEntries = sub.customRightRoleUserEntries;
            List<String> rightRoleCode = new ArrayList<String>();
            if (customRightRoleUserEntries != null) {
                for (Entry<Integer, Set<TreeNodeBranchBean>> e : customRightRoleUserEntries.entrySet()) {
                    if (!BaseUtils.isCollectionEmpty(e.getValue())) {
                        final CustomRightRoleBean crrb = BIKClientSingletons.getCustomRightRoleBeanById(e.getKey());
                        if (crrb != null) {
                            rightRoleCode.add(crrb.code);
                        }
                    }
                }
            }

            return isSys || !rightRoleCode.contains("Administrator");
        } else {
            return BIKRightRoles.canSysUserAddOrEditSysUsers(BIKClientSingletons.getLoggedUserBean(),
                    sub);
        }
    }

    @Override
    protected boolean isEditBtnEnabled(SystemUserBean b) {
        return areActionsEnabledOnSysUser(b);
    }

    //ww->authors: zbędne, bo brak akcji delete!
//    @Override
//    public boolean isDeleteBtnEnabled(SystemUserBean b) {
//        return areActionsEnabledOnSysUser(b);
//    }
    @Override
    public boolean hasIcon() {
        return false;
    }

    @Override
    protected String getName(SystemUserBean b) {
        return null;
    }

    @Override
    public int getAddColCnt() {
        return !BIKClientSingletons.hideSocialUserInfoOnAdminUsersTab() ? 4 : 3;
    }

    protected String createStringLikeUserRightRoleCaptionsList(SystemUserBean b) {

        Set<String> userRightRoleCodes = b.userRightRoleCodes;

        Set<String> rightRoleCaptions = new LinkedHashSet<String>();
        if (userRightRoleCodes != null) {
            for (String code : userRightRoleCodes) {
//                rightRoleCaptions.add(mapOfAllRightRoleByCode.get(code).caption);
                String caption = BIKRightRoles.getRightRoleCaptionByCode(code);
                if (caption.equals(I18n.rightRoleAppAdmin.get()) || caption.equals(I18n.rightRoleAdministrator.get())) {
                }
                rightRoleCaptions.add(caption);
            }
        }
        final Map<Integer, Set<TreeNodeBranchBean>> customRightRoleUserEntries = b.customRightRoleUserEntries;

        if (customRightRoleUserEntries != null) {
            for (Entry<Integer, Set<TreeNodeBranchBean>> e : customRightRoleUserEntries.entrySet()) {
                if (!BaseUtils.isCollectionEmpty(e.getValue())) {
                    final CustomRightRoleBean crrb = BIKClientSingletons.getCustomRightRoleBeanById(e.getKey());
                    if (crrb != null) {
                        String caption = crrb.caption;
                        rightRoleCaptions.add(caption);
                        if (caption.equals(I18n.rightRoleAppAdmin.get()) || caption.equals(I18n.rightRoleAdministrator.get())) {
                        }
                    }
                }
            }
        }

        if (rightRoleCaptions.isEmpty()) {
//            return "(" + I18n.brak.get() /* I18N:  */ + ")";
            return I18n.zwyklyUzytkownik.get();
        } else {
            if (rightRoleCaptions.size() > 1) {
                rightRoleCaptions.remove(I18n.zwyklyUzytkownik.get());
            }
        }

        return BaseUtils.mergeWithSepEx(rightRoleCaptions, ", ");
    }

    protected String createStringLikeUserRightRoleFromGroupCaptionsList(SystemUserBean b) {
        Set<String> rightRoleCaptions = new LinkedHashSet<String>();
        boolean isSysAdminOrAppAdmin = false;
        if (b.customRightRoleUserEntriesFromGroup != null) {
            for (Integer s : b.customRightRoleUserEntriesFromGroup) {
                final CustomRightRoleBean crrb = BIKClientSingletons.getCustomRightRoleBeanById(s);
                if (crrb != null) {
                    String caption = crrb.caption;
                    rightRoleCaptions.add(caption + "*");
                    if (caption.equals(I18n.rightRoleAppAdmin.get()) || caption.equals(I18n.rightRoleAdministrator.get())) {
                        isSysAdminOrAppAdmin = true;
                    }
                }
            }
            if (b.customRightRoleUserEntriesFromGroup.size() > 1 && isSysAdminOrAppAdmin) {
                rightRoleCaptions.remove(I18n.zwyklyUzytkownik.get() + "*");
            }
        }
        return (!BaseUtils.isCollectionEmpty(rightRoleCaptions) ? ", " : "") + BaseUtils.mergeWithSepEx(rightRoleCaptions, ", ");
    }

    @Override
    public Object getAddColVal(SystemUserBean b, int addColIdx) {
        int ind = 0;
        Map<Integer, Object> m = new HashMap<Integer, Object>();
        HTML loginName = new HTML(b.loginName);
        loginName.setStyleName("Notes" /* i18n: no:css-class */ + "-labBody");
        m.put(ind++, loginName);
        if (!BIKClientSingletons.hideSocialUserInfoOnAdminUsersTab()) {
            m.put(ind++, new Label(b.userId == null ? "(" + I18n.brak.get() /* I18N:  */ + ")" : b.userName + ""));
        }
        HorizontalPanel fp = new HorizontalPanel();
        fp.add(new Label(createStringLikeUserRightRoleCaptionsList(b)));
        Label userRolesFromGroup = new Label(createStringLikeUserRightRoleFromGroupCaptionsList(b));
        usersRolesFromGroup.add(userRolesFromGroup);
        fp.add(userRolesFromGroup);
        m.put(ind++, fp/* new Label(createStringLikeUserRightRoleCaptionsList(b)*/);
        m.put(ind++, new Label(b.isDisabled ? I18n.tak.get() /* I18N:  */ : I18n.nie.get() /* I18N:  */));
        return m.get(addColIdx);
    }

    protected SystemUserSortType addColumnNameToGrid(FlexTable gp, String columnName, int col, SystemUserSortType sust) {
        gp.setWidget(0, col, getWidget(columnName, col, sust));
        if (sust != null) {
            mapOfSortTypeForColumn.put(col, sust);
        }
        return sust;
    }

    @Override
    public boolean createGridColumnNames(FlexTable gp) {
//        mapOfAllRightRoleByCode = BIKClientSingletons.getMapOfAllRightRolesByCode();
        gp.setStyleName("gridJoinedObj");
        int col = 0;
        addColumnNameToGrid(gp, I18n.login.get() /* I18N:  */, col++, SystemUserSortType.LoginName);
        if (!BIKClientSingletons.hideSocialUserInfoOnAdminUsersTab()) {
            addColumnNameToGrid(gp, I18n.uzytkownikSpol2.get() /* I18N:  */ + ".", col++, SystemUserSortType.UserName);
        }
        addColumnNameToGrid(gp, I18n.uprawnienia.get() /* I18N:  */, col++, null);
        addColumnNameToGrid(gp, I18n.wylaczony.get() /* I18N:  */, col++, SystemUserSortType.IsDisabled);
        addColumnNameToGrid(gp, I18n.edycja.get() /* I18N:  */, col++, null);
        if (sortColNr == null) {
            sortUserListByType(allBeans, mapOfSortTypeForColumn.get(0), isDownSort);
            sortColNr = 0;
        } else {
            sortUserListByType(allBeans, mapOfSortTypeForColumn.get(sortColNr), isDownSort);
        }
        Pair<Image, Image> p = mapOfDownAndUpArrowsForColumnsWithSort.get(sortColNr).get(isDownSort);
        p.v1.setVisible(true);
        p.v2.setVisible(false);
        addGridColumnStyle(gp);
        return true;
    }

    protected void addGridColumnStyle(FlexTable gp) {
        for (int i = 0; i < gp.getCellCount(0); i++) {
            gp.getCellFormatter().addStyleName(0, i,
                    mapOfSortTypeForColumn.get(i) != null ? "SystemUserColumnTypeWithSort" : "SystemUserColumnTypeWithoutSort");
        }
    }

    protected Image createArrowImage(String url, String title, boolean isVisible) {
        Image img = BIKClientSingletons.createIconImg(url, "13px", "10px", title);
        img.removeStyleName("RelatedObj-ObjIcon");
//        img.setStyleName(title);
        img.setVisible(isVisible);
        return img;
    }

    protected HorizontalPanel createIconsInOneDirection(final int sortCol, final boolean isDown, String title,
            String urlSelectedImg, String urlNoSelectedImg, Map<Boolean, Pair<Image, Image>> mapOfArrowsToSortWithDirection) {
        HorizontalPanel hp = new HorizontalPanel();
        Image selectedArrow = createArrowImage(urlSelectedImg, title, false);
        selectedArrow.setStyleName("SystemUserArrows");
        Image noSelectedArrow = createArrowImage(urlNoSelectedImg, title, true);
        noSelectedArrow.setStyleName("SystemUserArrows");
        noSelectedArrow.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (sortColNr != sortCol) {
                    sortColNr = sortCol;
                }
                isDownSort = isDown;
                refreshView();
            }
        });
        Pair<Image, Image> direction = new Pair<Image, Image>();
        direction.v1 = selectedArrow;
        direction.v2 = noSelectedArrow;
        mapOfArrowsToSortWithDirection.put(isDown, direction);
        hp.add(selectedArrow);
        hp.add(noSelectedArrow);
        return hp;
    }

    protected FlowPanel getWidget(String name, final int sortCol, final SystemUserSortType s) {
        HorizontalPanel hp = new HorizontalPanel();
        hp.setHeight("100%");
        final HTML html = new HTML(name);
        html.setStyleName("SystemUserColumnName");
        hp.add(html);
        hp.setCellVerticalAlignment(html, HasVerticalAlignment.ALIGN_MIDDLE);

        if (s != null) {
            VerticalPanel vp = new VerticalPanel();
            html.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    if (sortColNr == sortCol) {
                        isDownSort = !isDownSort;
                    } else {
                        sortColNr = sortCol;
                        isDownSort = false;
                    }
                    refreshView();
                }
            });

            Map<Boolean, Pair<Image, Image>> mapOfArrowsToSortWithDirection = new LinkedHashMap<Boolean, Pair<Image, Image>>();
            HorizontalPanel arrowsUp = createIconsInOneDirection(sortCol, false, "[A-Z]",
                    "images/sortArrowUp.png" /* I18N: no */, "images/noSortArrowUp.png" /* I18N: no */, mapOfArrowsToSortWithDirection);
            HorizontalPanel arrowsDown = createIconsInOneDirection(sortCol, true, "[Z-A]",
                    "images/sortArrowDown.png" /* I18N: no */, "images/noSortArrowDown.png" /* I18N: no */, mapOfArrowsToSortWithDirection);
            mapOfDownAndUpArrowsForColumnsWithSort.put(sortCol, mapOfArrowsToSortWithDirection);
            vp.add(arrowsUp);
            vp.add(arrowsDown);
            hp.add(vp);
        }
        FlowPanel fp = new FlowPanel();
        fp.setStyleName("RelatedObj-oneObj-User");
        fp.add(hp);

        return fp;
    }

    @Override
    protected List<IGridBeanAction<SystemUserBean>> createActionList() {
        List<IGridBeanAction<SystemUserBean>> res = new ArrayList<IGridBeanAction<SystemUserBean>>();
        res.add(actEdit);
        return res;
    }

    @Override
    protected void execActEdit(SystemUserBean b) {
        updateSystemUser(b);
    }

    public void refreshView() {
        if (dataChangedHandler != null) {
            dataChangedHandler.doIt();
        }
    }

    protected void updateSystemUser(final SystemUserBean sUser) {
//        BIKClientSingletons.getService().getAllBikSystemUser(new StandardAsyncCallback<List<SystemUserBean>>() {
//            public void onSuccess(List<SystemUserBean> result) {
//                List<Integer> allJoinabledUsers = new ArrayList<Integer>();
//                for (SystemUserBean su : result) {
//                    if (su.userId != null) {
//                        allJoinabledUsers.add(su.userId);
//                    }
//                }
        BIKClientSingletons.getService().getAlreadyJoinedUserIds(new StandardAsyncCallback<Set<Integer>>() {
            @Override
            public void onSuccess(Set<Integer> result) {
                result.remove(sUser.userId);
                new SystemUserDialog().buildAndShowDialog(sUser, result, new IParametrizedContinuation<SystemUserBean>() {
                    @Override
                    public void doIt(SystemUserBean param) {
                        sUser.loginName = param.loginName;
                        sUser.systemUserName = param.systemUserName;
                        sUser.isDisabled = param.isDisabled;
                        sUser.userName = param.userName;
                        sUser.userId = param.userId;
                        sUser.userRightRoleCodes = param.userRightRoleCodes;
                        sUser.userAuthorTreeIDs = param.userAuthorTreeIDs;
                        sUser.userCreatorTreeIDs = param.userCreatorTreeIDs;
                        sUser.nonPublicUserTreeIDs = param.nonPublicUserTreeIDs;
                        sUser.customRightRoleUserEntries = param.customRightRoleUserEntries;
                        sUser.isSendMails = param.isSendMails;
                        sUser.doNotFilterHtml = param.doNotFilterHtml;
                        refreshView(I18n.zmodyfikowanoUzytkownika.get() /* I18N:  */);
                    }
                }, false);
            }
        });
    }

    public void sortUserListByType(List<SystemUserBean> systemUsersBeanList, final SystemUserSortType type, final boolean down) {
        Collections.sort(systemUsersBeanList, new Comparator<SystemUserBean>() {
            @Override
            public int compare(SystemUserBean o1, SystemUserBean o2) {

                if (down) {
                    SystemUserBean t;
                    t = o1;
                    o1 = o2;
                    o2 = t;
                }
                if (type == SystemUserSortType.LoginName) {
                    return String.CASE_INSENSITIVE_ORDER.compare(o1.loginName, o2.loginName);
                } else if (type == SystemUserSortType.UserName) {
                    return String.CASE_INSENSITIVE_ORDER.compare(o1.userName != null ? o1.userName : "żżżż" /* I18N: no:wtf? */, o2.userName != null ? o2.userName : "żżżż" /* I18N: no:wtf? */);
                } else {
                    return String.CASE_INSENSITIVE_ORDER.compare(Boolean.toString(o1.isDisabled), Boolean.toString(o2.isDisabled));
                }
            }
        });
        this.allBeans = systemUsersBeanList;
    }

    @Override
    public String getTreeCode(SystemUserBean b) {
        /* nie uzywane return BIKGWTConstants.TREE_CODE_USERS;*/
        return null;
    }

    public enum SystemUserSortType {

        UserName, LoginName, IsDisabled;
    }

    public void setVisible(boolean visible) {
        for (Label lbl : usersRolesFromGroup) {
            lbl.setVisible(visible);
        }
    }
}
