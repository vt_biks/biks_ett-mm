/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.IBikTreePage;
import pl.fovis.client.nodeactions.NodeActionAddAnyNodeForKind;
import pl.fovis.client.nodeactions.NodeActionAddCustomTreeNode;
import pl.fovis.client.nodeactions.NodeActionContext;
import pl.fovis.common.ChildrenBean;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.TreeKindBean;
import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author tflorczak
 */
public class ChildrenExtradataWidget extends EntityDetailsWidgetFlatBase {

    protected static final String ATTRS_SEPARATOR = "|";
    protected VerticalPanel mainVp;
//    protected TreeKindBean treeKind;
//    protected NodeKindBean leafNodeKind;
//    protected NodeActionAddLeaf addChildNodeAction;
//    protected NodeActionAddCustomTreeNode addCustomTreeNodeAction;
    protected NodeActionAddAnyNodeForKind ddAnyNodeForKind;
    protected List<NodeActionAddCustomTreeNode> addCustomTreeNodeActionList;
    protected IBikTreePage bikPage;
    protected boolean widgetActive;
    protected List<ChildrenBean> sortedChildList;

    public ChildrenExtradataWidget(IBikTreePage bikPage, boolean widgetActive) {
        super();
        this.bikPage = bikPage;
        this.widgetActive = widgetActive;
    }

    @Override
    public void displayData() {
        mainVp.clear();
        List<ChildrenBean> children = getData().childrenExtradata;
        //sortowanie można przenieść do klasy EntityDetailsDataBeanPropsReader - metoda getChildrenExtradata()

        //sortowanie tablicy zdarzeń, żeby wyświetlać najnowsze zdarzenie na górze
        if (!BaseUtils.isCollectionEmpty(children) && getData().node != null && getData().node.isRegistry) {
            // if (!BaseUtils.isCollectionEmpty(children)) {
            Collections.sort(children, new Comparator<ChildrenBean>() {
                @Override
                public int compare(ChildrenBean childB1, ChildrenBean childB2) {
                    return Integer.compare(childB2.nodeId, childB1.nodeId);
                }
            });
        }

        String attrsDic = getData().node.nodeKindChildrenAttrs;
        Set<String> attrDicList = BaseUtils.splitUniqueBySep(attrsDic != null ? attrsDic : "", ATTRS_SEPARATOR, true);

        // sprawdzenie uprawnien do atrybutów
//            Set<String> attrToRemove = new HashSet<String>();
//            for (String attrName : attrDicList) {
//                if (!BIKClientSingletons.getRightsManager().hasRightToNodeAttrAction(getData().node, attrName, IBiksRightsManager.RightsNodeAttrAction.View)) {
//                    attrToRemove.add(attrName);
//                }
//            }
//            attrDicList.removeAll(attrToRemove);
//        mainVp.add(createHeader(I18n.obiektyPodrzedne.get()));
        TreeKindBean treeKind = BIKClientSingletons.getTreeKindBeanByCode(getData().node.treeKind);
        NodeKindBean leafNodeKind = null;
        if (treeKind != null) {
            leafNodeKind = BIKClientSingletons.getNodeKindById(treeKind.leafNodeKindId);
        }

        mainVp.add(createPostSpacing());
        mainVp.add(new ChildrenWithAttributesWidget(bikPage, children, attrDicList, true, leafNodeKind, getData().node).buildWidget());
//        if (getData().node.isFolder) {
        if (!BIKClientSingletons.isCustomTreeModeEnable(getData().node.treeKind)) {
//            addChildNodeAction = new NodeActionAddLeaf();
//            if (ctx != null) {
//                addChildNodeAction.setup(new NodeActionContext(getData().node, bikPage, getData()) {
//
//                    @Override
//                    public Integer getSelectedNodeId() {
//                        return getData().node.id;
//                    }
//                });
//                if (addChildNodeAction.isEnabled()) {
//                    mainVp.add(createPostSpacing());
//                    mainVp.add(createAddChildBtn());
//                }
//            }
        } else {
            int i = 0;
            List<NodeKindBean> nodeKindsForTreeKind = BIKClientSingletons.getNodeKindsForTreeKind(getData().node);
            if (!BaseUtils.isCollectionEmpty(nodeKindsForTreeKind)) {
//                ddAnyNodeForKind = new NodeActionAddAnyNodeForKind();
//
//                if (ctx != null) {
//                    ddAnyNodeForKind.setup(new NodeActionContext(getData().node, bikPage, getData()) {
//                        @Override
//                        public Integer getSelectedNodeId() {
//                            return getData().node.id;
//                        }
//                    });
//                    if (ddAnyNodeForKind.isEnabled() && !getData().node.isRegistry) {
//                        mainVp.add(createAddCustomTreeNodeBtn(i));
//                    }
//                }

//                }
                addCustomTreeNodeActionList = new ArrayList<NodeActionAddCustomTreeNode>();
                mainVp.add(createPostSpacing());
                for (NodeKindBean nkb : nodeKindsForTreeKind) {
                    if (nkb.isLeaf) {
//                        Window.alert(nkb.code);
                        addCustomTreeNodeActionList.add(new NodeActionAddCustomTreeNode(nkb));
                        if (ctx != null) {
                            addCustomTreeNodeActionList.get(i).setup(new NodeActionContext(getData().node, bikPage, getData()) {
                                @Override
                                public Integer getSelectedNodeId() {
                                    return getData().node.id;
                                }
                            });
                            if (addCustomTreeNodeActionList.get(i).isEnabled() && !getData().node.isRegistry && !BIKClientSingletons.hideAddNodeKindBtnsOnRight()) {
//                            if (addCustomTreeNodeActionList.get(i).isEnabled() && !getData().node.isRegistry && !BIKClientSingletons.hideAttrAndNodeKindBtnsOnRight()) {
                                mainVp.add(createAddCustomTreeNodeBtn(i));
                            }
                        }
                        i = i + 1;
                    }
                }
                mainVp.add(createPostSpacing());
            }
        }
    }

    @Override
    public Widget buildWidgets() {
        mainVp = new VerticalPanel();
        mainVp.setWidth("100%");
        return mainVp;
    }

    @Override
    public boolean hasContent() {
//        return !BaseUtils.isCollectionEmpty(getData().childrenExtradata);
        return true;
    }

//    private IsWidget createAddChildBtn() {
//
////        IsWidget btn = new BikCustomButton(I18n.dodaj.get() + " " + leafNodeKind.caption /* I18N:  */, "Attr" /* I18N: no */ + "-newAddAtrr",
//        IsWidget btn = new BikCustomButton(addChildNodeAction.getCaption(), "Attr" /* I18N: no */ + "-newAddAtrr",
//                new IContinuation() {
//                    @Override
//                    public void doIt() {
//                        addChildNodeAction.execute();
//                    }
//                });
//        return btn;
//    }
    private IsWidget createAddCustomTreeNodeBtn(final int i) {
        IsWidget btn = new BikCustomButton(addCustomTreeNodeActionList.get(i).getCaption() /*addCustomTreeNodeActionList.get(i).getCaption()*/, "Attr" /* I18N: no */ + "-newAddAtrr",
                new IContinuation() {
            @Override
            public void doIt() {
//                        ddAnyNodeForKind.execute();
                addCustomTreeNodeActionList.get(i).execute();
            }
        });
        return btn;
    }
}
