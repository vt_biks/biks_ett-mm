/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

/**
 *
 * @author pmielanczuk
 */
public abstract class InlineFoxyNotificationHandlerExBase extends InlineFoxyNotificationHandlerBase implements IFoxyNotificationHandlerEx {

    protected String throwableToMsgPart(Throwable ex) {
        if (ex == null) {
            return "";
        }

        //ww: stack trace zawiera samego exceptiona na początku
        //ww: jednak stact trace jest kiepski, bo nie pokazuje info z serwera,
        // tylko z klienta, więc lepiej pokazać exception
        return "\nException: " + ex;
//                "\nStack trace: " + StackTraceUtil.getCustomStackTrace(ex);
    }

    public void showWarning(String err, Throwable ex) {
        showWarning(err + throwableToMsgPart(ex));
    }
}
