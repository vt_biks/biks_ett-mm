--procedura
alter procedure sp_check_version (@ver varchar(255))
as
	declare @version varchar(255)
begin
	
	select @version = val from bik_app_prop;
	if(@version != @ver)
	begin
		raiserror(N'Wrong number of version.Current version is %s',--Message text
				20,
				-1,
				@version) with log;
				print N'Wrong number of version.Current version is %s';
	end
	
end
