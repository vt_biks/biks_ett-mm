/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.NodeAuthorBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author bfechner
 */
public class UserRankPageGrid extends MyBIKSGridSingleRowBase<NodeAuthorBean> implements IPaginatedItemsDisplayer<NodeAuthorBean> {

    @Override
    protected String getIconUrl(NodeAuthorBean n) {
//        return BIKClientSingletons.fixFileResourceUrl(n.avatar);
        return BIKClientSingletons.fixAvatarUrl(n.avatar, NodeAwareBeanExtractorBase.getIconUrlByNodeKindCode(BIKGWTConstants.NODE_KIND_USER, null));
//
//        if (n != null && n.avatar != null && n.avatar.startsWith("file_")) {
//            n.avatar = FoxyFileUpload.getWebAppUrlPrefixForUploadedFile("fsb?get=") + n.avatar;
//        }
//        return n.avatar;
    }

    @Override
    protected boolean isHeaderRowVisible() {
        return true;
    }

    @Override
    protected void addHeaderRow() {
        int row = 0;
        columnsCnt = 0;
        gp.getFlexCellFormatter().setColSpan(0, 1, 2);
        gp.setWidget(row, columnsCnt++, new Label(""));
        Label user = new Label(I18n.uzytkownicy.get() /* I18N:  */);
        user.setStyleName("Rank-Users-Gp-Label");
        gp.setWidget(row, columnsCnt++, user);
        gp.setWidget(row, columnsCnt++, new Label(I18n.liczbaDodanychWpisow.get() /* I18N:  */));
        gp.setWidget(row, columnsCnt++, new Label(I18n.oceny.get() /* I18N:  */));
        gp.setWidget(row, columnsCnt++, new Label(""));
        gp.setWidget(row, columnsCnt++, new Label(""));
        gp.setWidth("100%");
        gp.getRowFormatter().setStyleName(0, "Rank-Users-Gp");
    }

    protected HTML getCnt(NodeAuthorBean n) {
        int allVoteCnt = n.voteUsefulSum + n.voteUselessSum;
        final HTML title = new HTML(n.voteUsefulSum + " " + I18n.z_noSingleLetter.get() /* I18N:  */ + " " + allVoteCnt);
        title.setTitle(/*n.cnt + " dodanych wpisów / " + */n.voteUsefulSum + " " + I18n.z_noSingleLetter.get() /* I18N:  */ + " " + allVoteCnt + " " + I18n.uzytkownUznaloWpisyZaUzyteczn.get() /* I18N:  */);
        return title;
    }

    @Override
    protected void setUpGridColumnsWidths(FlexTable gp) {
        gp.setWidth("100%");
        int idx = 0;
//        gp.getColumnFormatter().setWidth(idx++, "100px");
        gp.getColumnFormatter().setWidth(idx++, "5px");
        gp.getColumnFormatter().setWidth(idx++, "26px");
        gp.getColumnFormatter().setWidth(idx++, "40%");
        gp.getColumnFormatter().setWidth(idx++, "30%");
        gp.getColumnFormatter().setWidth(idx++, "30%");
        gp.getColumnFormatter().setWidth(idx++, "26px");
    }

    @Override
    protected void addOrRefreshTitles(int row, NodeAuthorBean n) {
    }

    @Override
    protected void createRowFromBean(int row, NodeAuthorBean n) {
        columnsCnt = 0;
        HTML idx = new HTML(getCurrentRowGlobalIdx() + 1 + ".");
        idx.setStyleName("Rank-Users");
        String iconUrl = getIconUrl(n);
        gp.setWidget(row, columnsCnt++, idx);
//        gp.setWidget(row, columnsCnt++,
//                BIKClientSingletons.createIconImg(iconUrl == null
//                ? "images/" + BIKClientSingletons.getNodeKindIconNameByCode(BIKGWTConstants.NODE_KIND_USERS_GROUP) + ".gif"
//                : iconUrl, "16px", "16px", null));
        gp.setWidget(row, columnsCnt++, BIKClientSingletons.createAvatarImage(iconUrl, 16, 16,
                "images" /* I18N: no */ + "/" + BIKClientSingletons.getNodeKindIconNameByCode(BIKGWTConstants.NODE_KIND_USER, null) + "." + "gif" /* I18N: no */, null, null));
        HorizontalPanel hp = new HorizontalPanel();
        String treeCode = BIKGWTConstants.TREE_CODE_USER_ROLES;
        int nodeId = n.nodeId;
        hp.add(BIKClientSingletons.createLabelLookupButton(n.userName, treeCode, nodeId, null, true));
        if (getCurrentRowGlobalIdx() < BIKClientSingletons.getStarredUserCount()) {
            hp.add(BIKClientSingletons.createIconImg("images/star.png" /* I18N: no */, "12px", "12px", null));
        }
        gp.setWidget(row, columnsCnt++, hp);
        gp.setWidget(row, columnsCnt++, new Label(n.cnt + ""));
        gp.setWidget(row, columnsCnt++, getCnt(n));
        gp.setWidget(row, columnsCnt++, BIKClientSingletons.createLookupButton(treeCode, nodeId, null, true));
        gp.setWidget(row, columnsCnt++, BIKClientSingletons.createFollowButton(treeCode, nodeId, true));
        gp.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-grey" : "RelatedObj-oneObj-white");
    }
}
