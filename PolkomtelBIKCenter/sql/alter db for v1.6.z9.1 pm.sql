exec sp_check_version '1.6.z9.1';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
--
-- poprawka do atrybut�w wyliczanych - usuni�to konieczno�� stosowania
-- frazy into ${dst_vals}$ dla wylicze� warto�ci opartych o pe�ne 
-- zapytanie select
--
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------


if OBJECT_ID('sp_calc_formula_on_attr_vals_of_nodes_in_one_node_kind') is not null
  drop proc sp_calc_formula_on_attr_vals_of_nodes_in_one_node_kind;
go

create procedure sp_calc_formula_on_attr_vals_of_nodes_in_one_node_kind(@sql_to_select_node_ids varchar(max), @opt_dependent_node_ids_before_change varchar(max)) as
begin
  set nocount on;

  declare @diags_level int = 0; -- 0 oznacza brak logowania, 1 - standardowe, 2 oznacza wi�ksze - z selectami

  if @diags_level > 0 begin
	  print cast(sysdatetime() as varchar(23)) + ': @sql_to_select_node_ids=' + coalesce(@sql_to_select_node_ids, '<null>');
  end;  

  declare @nksql nvarchar(max) = 'set @node_kind_id_ret = (select top 1 node_kind_id from bik_node where id in (' + @sql_to_select_node_ids + '))';

  --declare @node_kind_id int = (select node_kind_id from bik_node where id = @node_id);
  declare @node_kind_id int;
  
  exec sp_executesql @nksql, N'@node_kind_id_ret int output', @node_kind_id_ret=@node_kind_id output;

  if @diags_level > 0 begin
	  print cast(sysdatetime() as varchar(23)) + ': @node_kind_id=' + coalesce(cast(@node_kind_id as varchar(20)), '<null>');
  end;  

  if @node_kind_id is null return;  -- nie ma nic do roboty, ten select nic nie zwr�ci� (�adnych node'�w)

  --declare @sep_pos int = charindex(':::', @formula);
  
  -- declare @cols_def varchar(max) = rtrim(ltrim(substring(@formula, 1, @sep_pos - 1)));
  declare @cols_def varchar(max), @formula_expr varchar(max), @attr_dependent_nodes_sql varchar(max);
  
  select @cols_def = rtrim(ltrim(attr_calculating_types)), @formula_expr = rtrim(ltrim(attr_calculating_expressions)), @attr_dependent_nodes_sql = rtrim(ltrim(attr_dependent_nodes_sql))
  from bik_node_kind where id = @node_kind_id;

  if (@formula_expr is null or @formula_expr = '') and (@attr_dependent_nodes_sql is null or @attr_dependent_nodes_sql = '')
    return;
  
  declare @temp_tab_name_base sysname = '##biks_calc_formula_on_attr_vals__' + replace(convert(varchar(36),NEWID()), '-', '_');
  --declare @temp_tab_name_src sysname = case when @cols_def is null or @cols_def = '' then null else @temp_tab_name_base + '_src' end;
  if @cols_def = '' set @cols_def = null;
  
  declare @temp_tab_name_src sysname = @temp_tab_name_base + '_src';

  declare @create_temp_tab_sql varchar(max) = 'create table ' + @temp_tab_name_src + ' (node_id int' + case when @cols_def is null then '' else ', ' + @cols_def end + ')';
  
  --print @create_temp_tab_sql;
  exec(@create_temp_tab_sql);

  --declare @insert_node_id_sql varchar(max) = 'insert into ' + @temp_tab_name_src + ' (node_id) values (' + cast(@node_id as varchar(20)) + ')';
  declare @insert_node_id_sql varchar(max) = 'insert into ' + @temp_tab_name_src + ' (node_id) ' + @sql_to_select_node_ids;
  exec(@insert_node_id_sql)
	   
  if @cols_def is not null
  begin	  
	  -- declare @formula_expr varchar(max) = ltrim(rtrim(substring(@formula, @sep_pos + 3, len(@formula))));
	  -- declare @formula_expr varchar(max) = ltrim(rtrim((select attr_calculating_expressions from bik_node_kind where id = @node_kind_id)));
	   
	  declare @temp_tab_id int = object_id('tempdb..' + @temp_tab_name_src);
	 
	  declare @tempdb_schema_name sysname = (select ss.name from tempdb.sys.objects so inner join tempdb.sys.schemas ss on so.schema_id = ss.schema_id
		where so.object_id = @temp_tab_id);

	  --declare @cnt int = 0;
	 
	  declare @col_name sysname, @type_name varchar(max);  
	 
	  declare cur cursor for 
	  SELECT 
			column_name, 
			data_type + case data_type
				when 'sql_variant' then ''
				when 'text' then ''
				when 'ntext' then ''
				when 'xml' then ''
				when 'decimal' then '(' + cast(numeric_precision as varchar) + ', ' + cast(numeric_scale as varchar) + ')'
				when 'numeric' then '(' + cast(numeric_precision as varchar) + ', ' + cast(numeric_scale as varchar) + ')'
				else coalesce('('+case when character_maximum_length = -1 then 'max' else cast(character_maximum_length as varchar) end +')','') end        
		   from tempdb.information_schema.columns where table_name = @temp_tab_name_src and table_schema = @tempdb_schema_name
		   order by ordinal_position;

	  open cur;

	  fetch next from cur into @col_name, @type_name;

	  while @@fetch_status = 0
	  begin    
		declare @col_name_quoted varchar(max) = quotename(@col_name);
		declare @val_expr varchar(max) = 'cast(value as ' + @type_name + ')';
	    
		-- tu mo�na zmieni� @val_expr na inn� konwersj� w zale�no�ci od @type_name
    if charindex('numeric', @type_name) <> 0 -- poprawka dla typu numeric
	  begin
			set @val_expr = 'cast(replace(value,'','', ''.'') as ' + @type_name + ')';
	  end;				
		
		--print @col_name_quoted + ' ' + @type_name;

		declare @conv_one_val_sql varchar(max);
	    
		declare @attribute_id int = (select a.id from bik_attr_def ad inner join bik_attribute a on ad.id = a.attr_def_id where ad.name = @col_name and a.node_kind_id = @node_kind_id);

		--declare @select_val_conv_sql varchar(max) = 'select ' + @val_expr + ' from bik_attribute_linked where node_id = ' + cast(@node_id as varchar(10)) + ' and attribute_id = ' + cast(@attribute_id as varchar(10));	    
		--set @conv_one_val_sql = 'update ' + @temp_tab_name_src + ' set ' + @col_name_quoted + ' = (' + @select_val_conv_sql + ')';

		declare @select_val_conv_sql varchar(max) = 'select ' + @val_expr + ' from bik_attribute_linked where node_id = ' + @temp_tab_name_src + '.node_id and attribute_id = ' + cast(@attribute_id as varchar(10));	    
		set @conv_one_val_sql = 'update ' + @temp_tab_name_src + ' set ' + @col_name_quoted + ' = (' + @select_val_conv_sql + ')';
	    
		--print @conv_one_val_sql;
	    
		exec(@conv_one_val_sql);
	      
		--set @cnt = @cnt + 1;
		fetch next from cur into @col_name, @type_name;
	  end;
	 
	  close cur;
	  deallocate cur;
  end;

  declare @print_sql varchar(max);
  if @diags_level > 1 begin
    set @print_sql = 'select * from ' + @temp_tab_name_src;
    exec(@print_sql);
  end;
  
  if @formula_expr is not null and @formula_expr <> ''
  begin	 
    declare @temp_tab_name_dst sysname = @temp_tab_name_base + '_dst';
    
    --declare @select_from_temp_sql varchar(max) = 'select node_id, ' + @formula_expr + ' into ' + @temp_tab_name_dst + case when @temp_tab_name_src is null then '' else ' from ' + @temp_tab_name_src end;
    declare @select_from_temp_sql varchar(max);
    
    if (substring(ltrim(@formula_expr), 1, 6) = 'select')
      --set @select_from_temp_sql = replace(replace(@formula_expr, '${src_vals}$', @temp_tab_name_src), '${dst_vals}$', @temp_tab_name_dst)
      set @select_from_temp_sql = 'select * into ' + @temp_tab_name_dst + ' from (' + replace(@formula_expr, '${src_vals}$', @temp_tab_name_src) + ') x'
    else
      set @select_from_temp_sql = 'select node_id, ' + @formula_expr + ' into ' + @temp_tab_name_dst + ' from ' + @temp_tab_name_src + ' as x';
    
    if @diags_level > 0 begin
	    print cast(sysdatetime() as varchar(23)) + ': @select_from_temp_sql=' + @select_from_temp_sql;
    end;  
      
    exec(@select_from_temp_sql);
    
    if @diags_level > 0 begin
	    print cast(sysdatetime() as varchar(23)) + ': after exec @select_from_temp_sql';
    end;  

    if @diags_level > 1 begin
      set @print_sql = 'select * from ' + @temp_tab_name_dst;
      exec(@print_sql);
    end;

    declare @temp_tab_id_dst int = object_id('tempdb..' + @temp_tab_name_dst);
   
    declare @tempdb_schema_name_dst sysname = (select ss.name from tempdb.sys.objects so inner join tempdb.sys.schemas ss on so.schema_id = ss.schema_id
	  where so.object_id = @temp_tab_id_dst);

    --declare @cnt int = 0;
   
    declare @col_name_dst sysname, @type_name_dst varchar(max);  
   
    declare cur_dst cursor for 
    SELECT 
		  column_name, 
		  data_type
	     from tempdb.information_schema.columns where table_name = @temp_tab_name_dst and table_schema = @tempdb_schema_name_dst
	     order by ordinal_position;

    open cur_dst;

    fetch next from cur_dst into @col_name_dst, @type_name_dst;

    while @@fetch_status = 0
    begin
      declare @attr_def_id_dst int = (select id from bik_attr_def where name = @col_name_dst);
  	
      declare @attribute_id_dst int = (select id from bik_attribute where node_kind_id = @node_kind_id and attr_def_id = @attr_def_id_dst);     

	  declare @convert_dst_expr nvarchar(max) = 'cast(x.' + quotename(@col_name_dst) + ' as varchar(max))';
	  -- tu opcjonalnie inne konwersje zale�nie od typu @type_name_dst
      
      declare @merge_sql varchar(max) = 'MERGE INTO bik_attribute_linked as av USING ' + @temp_tab_name_dst + ' as x' +
        ' ON x.node_id = av.node_id and av.attribute_id = ' + cast(@attribute_id_dst as varchar(20)) +
        ' WHEN MATCHED and x.' + quotename(@col_name_dst) + ' is null THEN delete' +
        ' WHEN MATCHED THEN update set av.value = ' + @convert_dst_expr +
        ' WHEN NOT MATCHED BY TARGET and x.' + quotename(@col_name_dst) + ' is not null
          THEN insert (attribute_id, node_id, value, is_deleted) values (' + cast(@attribute_id_dst as varchar(20)) + ', x.node_id, ' + @convert_dst_expr + ', 0);';

    if @diags_level > 0 begin
	    print cast(sysdatetime() as varchar(23)) + '@merge_sql: ' + @merge_sql;
	  end;
    exec(@merge_sql);

	  /*
	  declare @ssql nvarchar(max) = 'select @retvalout=' + @convert_dst_expr + ' from ' + @temp_tab_name_dst;
  		
	  declare @retval varchar(max);   

	  exec sp_executesql @ssql, N'@retvalout varchar(max) output', @retvalout=@retval output;

      declare @attr_def_id_dst int = (select id from bik_attr_def where name = @col_name_dst);

      if @attr_def_id_dst is not null
      begin
	    declare @attribute_id_dst int = (select id from bik_attribute where node_kind_id = @node_kind_id and attr_def_id = @attr_def_id_dst);     
      
        if @retval is null or @retval = ''
        begin
          delete from bik_attribute_linked 
		  where attribute_id = @attribute_id_dst and node_id = @node_id;
        end
        else
        begin
          update bik_attribute_linked 
		  set value = @retval, is_deleted = 0
		  where attribute_id = @attribute_id_dst and node_id = @node_id;
  	      
		  if @@rowcount = 0
		  begin
		    insert into bik_attribute_linked (attribute_id, node_id, value, is_deleted)
		    values (@attribute_id_dst, @node_id, @retval, 0);
		  end;
        end;
              
        --select top 10 * from bik_attribute_linked
      end;
      */
      
      fetch next from cur_dst into @col_name_dst, @type_name_dst;
    end;
    
    close cur_dst;
    deallocate cur_dst;
    
    declare @drop_dyn_tab_sql_dst varchar(max) = 'drop table ' + @temp_tab_name_dst;
    exec(@drop_dyn_tab_sql_dst);
  end;
    
  declare @dependent_nodes_sql varchar(max) = replace(@attr_dependent_nodes_sql, '${node_ids}$', 
    '(select node_id from ' + @temp_tab_name_src + ')');  

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': @dependent_nodes_sql=' + coalesce(@dependent_nodes_sql, '<null>');
  end;    

  if @dependent_nodes_sql is not null and @dependent_nodes_sql <> '' 
  begin
    declare @nodes_to_recalc table (node_id int not null, node_kind_id int);

    if @opt_dependent_node_ids_before_change is not null
    begin
      insert into @nodes_to_recalc (node_id)
      select cast(str as int) from dbo.fn_split_by_sep(@opt_dependent_node_ids_before_change, ',', 7);      
    end;
    
    --declare @dep_nodes_with_kinds_sql varchar(max) = 'select x.node_id, n.node_kind_id from (' + @dependent_nodes_sql + ') x inner join bik_node n on x.node_id = n.id';
    --insert into @nodes_to_recalc (node_id, node_kind_id) exec(@dep_nodes_with_kinds_sql);

    insert into @nodes_to_recalc (node_id) exec(@dependent_nodes_sql);
    update ntc set node_kind_id = n.node_kind_id from @nodes_to_recalc as ntc inner join bik_node n on n.id = ntc.node_id;

    declare dn_cur cursor local for select distinct node_id, node_kind_id from @nodes_to_recalc where node_kind_id is not null order by node_kind_id;  

    open dn_cur;
    
    declare @dn_node_id int, @dn_node_kind_id int;
    
    declare @dn_node_ids_as_str varchar(max) = '';
    declare @dn_node_ids_for_in varchar(max) = '';
    
    fetch next from dn_cur into @dn_node_id, @dn_node_kind_id;
    declare @prev_dn_node_kind_id int = @dn_node_kind_id;
    
    while @@fetch_status = 0
    begin
      if @prev_dn_node_kind_id <> @dn_node_kind_id
      begin
        declare @select_dn_ids_sql varchar(max) = 'select node_id from (values ' + @dn_node_ids_as_str + ') as xxx(node_id)';
        --select node_id from (values (10), (20), (30)) as x(node_id) 
        
        exec sp_calc_formula_on_attr_vals_of_nodes_in_one_node_kind @select_dn_ids_sql, null;      
        
        set @prev_dn_node_kind_id = @dn_node_kind_id;
        set @dn_node_ids_as_str = '';      
      end;
    
      set @dn_node_ids_as_str = @dn_node_ids_as_str + case when @dn_node_ids_as_str <> '' then ',' else '' end + '(' + cast(@dn_node_id as varchar(20)) + ')';
      set @dn_node_ids_for_in = @dn_node_ids_for_in + case when @dn_node_ids_for_in <> '' then ',' else '' end + cast(@dn_node_id as varchar(20));
    
      fetch next from dn_cur into @dn_node_id, @dn_node_kind_id;
    end;

    close dn_cur;
    deallocate dn_cur;  

    if @dn_node_ids_as_str <> ''
    begin
      set @select_dn_ids_sql = 'select node_id from (values ' + @dn_node_ids_as_str + ') as xxx(node_id)';
      exec sp_calc_formula_on_attr_vals_of_nodes_in_one_node_kind @select_dn_ids_sql, null;      
    end;    
    
    if @dn_node_ids_for_in <> ''
    begin
      declare @node_filter varchar(max) = 'n.id in (' + @dn_node_ids_for_in + ')';
      
      if @diags_level > 0 begin
        print cast(sysdatetime() as varchar(23)) + ': @node_filter=' + coalesce(@node_filter, '<null>');
      end;    
            
      exec sp_verticalize_node_attrs_inner @node_filter;
    end;    
  end;
    
  declare @drop_dyn_tab_sql varchar(max) = 'drop table ' + @temp_tab_name_src;
  exec(@drop_dyn_tab_sql);
end;
go


-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------


exec sp_update_version '1.6.z9.1', '1.6.z9.2';
go
