/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.List;
import pl.bssg.metadatapump.common.SchedulePumpBean;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.ScheduleWidgetBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class AdminSchedulePage extends SchedulePageBase<SchedulePumpBean, ScheduleWidgetBean> {

    @Override
    protected void createAddtionalWidgets(VerticalPanel main) {
        main.add(new Label(I18n.tlumaczenieCzestotliwosci.get()));
    }

    @Override
    public String getId() {
        return "admin:load:schedule";
    }

    @Override
    protected void createHeaders() {
        grid.setWidget(0, 0, new HTML("<b>" + I18n.konektor.get() /* I18N:  */ + "</b>"));
        grid.setWidget(0, 1, new HTML("<b>" + I18n.serwer.get() /* I18N:  */ + "</b>"));
        grid.setWidget(0, 2, new HTML("<b>" + I18n.godzina.get() /* I18N:  */ + " (hh:mm)</b>"));
        grid.setWidget(0, 3, new HTML("<b>" + I18n.czestotliwosc.get() /* I18N:  */ + "</b>"));
        grid.setWidget(0, 4, new HTML("<b>" + I18n.dataRozpoczecia.get() /* I18N:  */ + "</b>"));
        grid.setWidget(0, 5, new HTML("<b>" + I18n.aktywny.get() /* I18N:  */ + "</b>"));
    }

    @Override
    protected void createRow(int row, SchedulePumpBean bean) {
        final ScheduleWidgetBean widgetBean = new ScheduleWidgetBean(grid, row, 4);
        setWidgetsFromBean(widgetBean, bean);
        grid.setWidget(row, 0, new Label(widgetBean.name));
        grid.setWidget(row, 1, new Label(widgetBean.instance != null ? widgetBean.instance : ""));
        grid.setWidget(row, 2, widgetBean.time);
        grid.setWidget(row, 3, widgetBean.interval);
//        grid.setWidget(row, 4, widgetBean.startTime);
        grid.setWidget(row, 4, widgetBean.startTimeFake);
        grid.setWidget(row, 5, widgetBean.isSchedule);
        widgets.put(widgetBean.id, widgetBean);
        if (row % 2 != 0) {
            grid.getRowFormatter().addStyleName(row, "evenTestRow");
        }
    }

    @Override
    protected void getSchedules(StandardAsyncCallback<Pair<Integer, List<SchedulePumpBean>>> async) {
        BIKClientSingletons.getService().getSchedule(pwh.pageNum, pwh.pageSize, async);
    }

    @Override
    protected void populateRow(SchedulePumpBean bean) {
        ScheduleWidgetBean widgetBean = widgets.get(bean.id);
        populateWidgets(widgetBean, bean);
    }

    @Override
    protected void setSchedules(List<SchedulePumpBean> schedules, StandardAsyncCallback<Void> async) {
        BIKClientSingletons.getService().setSchedule(schedules, async);
    }

    @Override
    protected SchedulePumpBean getScheduleBean() {
        return new SchedulePumpBean();
    }

    @Override
    protected int getColumnCount() {
        return 6;
    }
}
