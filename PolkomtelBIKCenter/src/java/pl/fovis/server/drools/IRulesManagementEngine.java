/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.drools;

import java.util.Collection;
import simplelib.Pair;

/**
 *
 * @author ctran
 */
public interface IRulesManagementEngine {

    public void addAll(Object... res);

    public void fireAllRules();

    public Collection<? extends Object> getObjects();

    public Pair<EngineStatus, String> reset(String drlFolder);

    public Pair<EngineStatus, String> reset(String drlFolder, boolean doReset);
}
