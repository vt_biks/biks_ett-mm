/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.ResizeSensitiveWidgetHelper;
import pl.fovis.client.treeandlist.IBean4TreeExtractor;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GenericTreeHelper;
import pl.fovis.foxygwtcommons.ITreeBroker;
import pl.fovis.foxygwtcommons.TreeAlgorithms;
import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeGrid;
import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeOrListGridRightClickHandler;
import pl.fovis.foxygwtcommons.treeandlist.ILazyRowLoaderEx;
import pl.fovis.foxygwtcommons.treeandlist.ITreeGridCellWidgetBuilder;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;
import pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase;
import pl.fovis.foxygwtcommons.wezyrtreegrid.FoxyWezyrTreeGrid;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.IPredicate;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public abstract class AbstractBIKTreeWidget<BEAN, IDT> /*implements IBean4TreeExtractor<BEAN, IDT>*/ {

    protected IFoxyTreeGrid<IDT> treeGrid;
    private Widget treeGridWidget;
    public GenericTreeHelper<BEAN, IDT> treeHelper;
    protected ILazyRowLoaderEx<Map<String, Object>, IDT> lazyRowLoader;
    protected ITreeRowAndBeanStorage<IDT, Map<String, Object>, BEAN> storage;
    protected int treeGridHeight = BIKClientSingletons.ORIGINAL_TREE_GRID_WIDGET_HEIGHT;
    protected boolean useDynamicHeight = true;
    protected boolean noHeightMode;
    protected String firstColStyleName;
    protected ITreeGridCellWidgetBuilder<BEAN> cellWidgetBuilder;
    protected IPageDataFetchBroker dataFetchBroker;
    protected boolean isInitialized;
    protected boolean isWidgetRunFromDialog;
    protected IBean4TreeExtractor<BEAN, IDT> beanExtractor = null;

    public AbstractBIKTreeWidget(String firstColStyleName,
            IPageDataFetchBroker dataFetchBroker, boolean isWidgetRunFromDialog, String... fields) {
        this.noHeightMode = true;
        this.dataFetchBroker = dataFetchBroker;
        this.firstColStyleName = firstColStyleName;
        this.isWidgetRunFromDialog = isWidgetRunFromDialog;
        initTreeGrid(fields);
    }

    public AbstractBIKTreeWidget(Integer treeGridHeight,
            String firstColStyleName, IPageDataFetchBroker dataFetchBroker,
            String... fields) {
        if (treeGridHeight != null) {
            this.treeGridHeight = treeGridHeight;
            this.useDynamicHeight = false;
        }
        this.dataFetchBroker = dataFetchBroker;
        this.firstColStyleName = firstColStyleName;
        initTreeGrid(fields);
    }

    public AbstractBIKTreeWidget(IPageDataFetchBroker dataFetchBroker, String... fields) {
        this(null, null, dataFetchBroker, fields);
    }

    public abstract boolean isSubTreeFilteringCapable();

    protected void afterInitWithNoData() {
        // no-op
    }

    public void addNode(BEAN node) {
        if (storage.getAllBeans().size() > 0 && !getTreeGrid().hasUnloadedChildren(getExtractor().extractParentId(node))) {
            Map<String, Object> row = storage.registerAndConvertBeanToRow(node);

            Integer siblingsIdx = null;
            IDT parentNodeId = getExtractor().extractParentId(node);

            if (getExtractor().extractName(node) != null && !BIKClientSingletons.areChildrenCustomSorted(storage, parentNodeId, getExtractor())) {
                int siblingCnt = getTreeGrid().getNodeChildCount(parentNodeId);
                for (int i = 0; i < siblingCnt; i++) {
                    IDT siblingId = getTreeGrid().getNodeIdAtIndexInSiblings(parentNodeId, i);
                    BEAN tnb = storage.getBeanById(siblingId);
                    if (getExtractor().extractName(tnb) != null && getExtractor().extractName(node).compareToIgnoreCase(getExtractor().extractName(tnb)) < 0) {
                        siblingsIdx = i;
                        break;
                    }
                    //pg: jeśli są posortowane domyślnie, to najpierw są foldery a następnie inne elementy, wszystko alfabetycznie
                    //pg: jeśli wstawiam folder to sprawdzam, czy nie natknąłem się na zwykły element
                    if (getExtractor().extractIsFolder(node) && !getExtractor().extractIsFolder(tnb)) {
                        siblingsIdx = i;
                        break;
                    }
                }
            }

            getTreeGrid().addNode(row, siblingsIdx);
        } else {
            Map<String, Object> row = storage.registerAndConvertBeanToRow(node);
            getTreeGrid().addNode(row, null);
        }

        getTreeGrid().selectRecord(getExtractor().extractId(node), false);
    }

    private void initTreeGrid(String... fields) {
        treeGrid = new FoxyWezyrTreeGrid<IDT>();//TreeAndListCreator.createTreeGrid();
        if (firstColStyleName != null) {
            treeGrid.setFirstColStyleName(firstColStyleName);
        }
        treeGrid.defineTreeFields(MapTreeRowAndBeanStorageBase.ID_FIELD_NAME,
                MapTreeRowAndBeanStorageBase.PARENT_ID_FIELD_NAME,
                MapTreeRowAndBeanStorageBase.NAME_FIELD_NAME,
                MapTreeRowAndBeanStorageBase.PRETTY_KIND_FIELD_NAME,
                MapTreeRowAndBeanStorageBase.ICON_FIELD_NAME,
                MapTreeRowAndBeanStorageBase.ADDITIONAL_ICON_FIELD_NAME);
        treeGrid.setFields(fields);
        treeGrid.setEmptyMessage(I18n.wczytujeDaneProszeCzekac.get() /* I18N:  */ + "...");
        treeGridWidget = treeGrid.buildWidget();

        if (!noHeightMode) {
            if (useDynamicHeight) {
                BIKClientSingletons.registerResizeSensitiveWidget(new ResizeSensitiveWidgetHelper(treeGridWidget) {
                    @Override
                    public void setFixedHeight(int height) {
                        treeGrid.setHeight(height);
                    }
                }, treeGridHeight);
            } else if (treeGridHeight != 0) {
                treeGrid.setHeight(treeGridHeight);
            }
        }

    }

    public BEAN getNodeBeanByDataNode(Map<String, Object> dataNode) {
        if (dataNode == null) {
            return null;
        }
        IDT id = storage.getTreeBroker().getNodeId(dataNode);
        return storage.getBeanById(id);
    }

    protected void populateTreeGridWithData(Collection<BEAN> items,
            boolean useLazyLoading, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode) {

        for (BEAN tnb : items) {
            storage.registerAndConvertBeanToRow(tnb);
        }

        if (useLazyLoading) {
            lazyRowLoader = createLazyRowLoader(optExtraMode, optFilteringNodeActionCode);
        } else {
            lazyRowLoader = null;
        }

        treeGrid.setData(//rows,
                storage, lazyRowLoader);
        treeGrid.refreshWidgetsState();
        //isGetDataInProgress = false;

    }

    public BEAN getCurrentNodeBean() {
        return getNodeBeanByDataNode(getCurrentNodeData());
    }

    public Map<String, Object> getCurrentNodeData() {
        Map<String, Object> currRow = treeGrid.getSelectedRecord();
        return currRow;
    }

    public IDT getCurrentNodeId() {
        Map<String, Object> currRow = getCurrentNodeData();
        return currRow == null ? null : //(Integer) currRow.get(ID_FIELD_NAME);
                storage.getTreeBroker().getNodeId(currRow);
    }

    public void selectEntityById(IDT id) {

        if (id == null) {
            treeGrid.selectFirstRecord();
        } else {
            treeGrid.selectRecord(id, false);
        }
    }

    public IFoxyTreeGrid<IDT> getTreeGrid() {
        return treeGrid;
    }

    public Widget getTreeGridWidget() {
        return treeGridWidget;
    }

    public void refreshWidgetsState() {
        treeGrid.refreshWidgetsState();
    }

    public void addSelectionChangedHandler(IParametrizedContinuation<Map<String, Object>> handler) {
        treeGrid.addSelectionChangedHandler(handler);
    }

    public void addDoubleClickHandler(IParametrizedContinuation<Map<String, Object>> handler) {
        treeGrid.addDoubleClickHandler(handler);
    }

    public void addRightClickHandler(IFoxyTreeOrListGridRightClickHandler<Map<String, Object>> handler) {
        treeGrid.addRightClickHandler(handler);
    }

    protected Set<IDT> extractParentIds(Collection<BEAN> nodes) {
        Set<IDT> res = new HashSet<IDT>();
        for (BEAN t : nodes) {
            BEAN p = treeHelper.getParentNode(t);
            if (p != null) {
                res.add(getExtractor().extractId(p));
            }
        }
        return res;
    }

    public void filterTree(final String val, final Integer optSubTreeRootNodeId,
            final Boolean useLazyLoading, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode) {
        this.filterTreeEx(val, optSubTreeRootNodeId, useLazyLoading, null, optExtraMode, optFilteringNodeActionCode);
    }

    protected IParametrizedContinuation<Integer> getFilterNotification(final int size) {
        return new IParametrizedContinuation<Integer>() {
            public void doIt(Integer param) {
                if (param == size) {
                    BIKClientSingletons.showInfo(I18n.filtrowanieZakonczone.get() /* I18N:  */);
                } else {
                    BIKClientSingletons.showInfo(I18n.wyswietlamZnalezionePozycje.get() /* I18N:  */ + ": " + (param * 100L / size) + "%");
                }
            }
        };
    }

    protected void reinitWithFilteredData(List<BEAN> result, boolean useLazyLoading,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode) {

        BIKClientSingletons.showInfo(I18n.znalezionychPozycji.get() /* I18N:  */ + ": " + result.size() + ", " + I18n.wyswietlam.get() /* I18N:  */);

        BEAN item = result.isEmpty() ? null : result.get(0);

        IDT itemId = item == null ? null : getExtractor().extractId(item);

        initWithFullData(result, useLazyLoading, optExtraMode, optFilteringNodeActionCode, itemId);

        Set<IDT> parentNodeIds = extractParentIds(result);

        treeGrid.expandNodesByIds(parentNodeIds, getFilterNotification(parentNodeIds.size()));
        treeGrid.selectFirstRecord(); // dodane by TF, aby zaznaczał węzeł po filtrowaniu
    }

    public void filterTreeEx(final String val, final Integer optSubTreeRootNodeId, final Boolean useLazyLoading,
            final Collection<Integer> treeIds, final BikNodeTreeOptMode optExtraMode, final String optFilteringNodeActionCode) {
        dataFetchBroker.dataFetchCancel();

        if (useLazyLoading) {

            BIKClientSingletons.showInfo(I18n.wyszukuje.get() /* I18N:  */ + ": " + val);

            AsyncCallback<List<BEAN>> filterCallback
                    = new TreeDataFetchAsyncCallback<List<BEAN>>("Failure on getting filtered nodes" /* I18N: no:err-fail-in */, dataFetchBroker) {
                                @Override
                                protected void doOnSuccess(List<BEAN> result) {
                                    reinitWithFilteredData(result, useLazyLoading, optExtraMode, optFilteringNodeActionCode);
                                }
                            };

                    String filterRequestTicket = BIKClientSingletons.getCancellableRequestTicket();
                    Request filterRequest;
                    filterRequest = makeFilterRequest(treeIds, val, optSubTreeRootNodeId, optExtraMode, optFilteringNodeActionCode, filterRequestTicket, filterCallback);

                    dataFetchBroker.dataFetchStarting(IPageDataFetchBroker.TreeDataRequestKind.Filtered,
                            filterRequest, filterRequestTicket);
        } else {
            innerFilterTree(val, useLazyLoading);
        }
    }

    // używane w trybie eagerLoad (!lazyLoad) a więc już nie jest to używane wcale
    @Deprecated
    protected void innerFilterTree(String val, boolean useLazyLoading) {
        final String nameForFilter = val.toLowerCase();
        Pair<List<BEAN>, List<BEAN>> filtered
                = TreeAlgorithms.filter(treeHelper, storage.getAllBeans(),
                        new IPredicate<BEAN>() {
                            public Boolean project(BEAN val) {
                                return getExtractor().extractName(val).toLowerCase().contains(nameForFilter);
                            }
                        }, true);

        populateTreeGridWithData(filtered.v2, useLazyLoading, null, null);

        treeGrid.selectFirstRecord();

        Set<BEAN> parents = new HashSet<BEAN>();
        if (!BaseUtils.isStrEmpty(val)) {
            for (BEAN t : filtered.v1) {
                BEAN p = treeHelper.getParentNode(t);
                if (p != null) {
                    parents.add(p);
                }
            }
        }
        Collection<BEAN> nodesToExpand = parents;
        Collection<IDT> ids = new ArrayList<IDT>();
        for (BEAN propResNode : nodesToExpand) {
            ids.add(getExtractor().extractId(propResNode));
        }
        treeGrid.expandNodesByIds(ids, getFilterNotification(ids.size()));
    }

    public void initWithFullData(List<BEAN> items, boolean useLazyLoading,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode) {
        initWithFullData(items, useLazyLoading, optExtraMode, optFilteringNodeActionCode, null);
    }

    public void initWithFullData(List<BEAN> items, boolean useLazyLoading,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode //            , boolean hasIdToSelect
            , IDT optIdToSelect
    ) {
        this.storage = createStorage();//new TreeNodeBeanAsMapStorage(optCellWidgetBuilder, isWidgetRunFromDialog);
//        items = filtrByRole(items);
        this.treeHelper = new GenericTreeHelper<BEAN, IDT>(createTreeBroker(),//new TreeNodeBroker(),
                items);

        populateTreeGridWithData(items, useLazyLoading, optExtraMode, optFilteringNodeActionCode);

        if (items.size() == 1) {
            BEAN item = items.get(0);
            IDT itemId = getExtractor().extractId(item);

            if (!getExtractor().extractHasNoChildren(item) && (optIdToSelect == null || BaseUtils.safeEquals(optIdToSelect, itemId))) {
                //ww:todo ta linia wydaje się zbędna i wydaje się mieszać, bo w kolejnej jest expand
                //ww:???
                //lazyRowLoader.loadSubRowsOnExpand(storage.getRowById(itemId));
                // TF: rezygnacja z selectRecord, poniewaz powodowało to ponowne data fetch (zbędne)
//                treeGrid.selectRecord(itemId, true);
                List<IDT> coll = new ArrayList<IDT>();
                coll.add(itemId);
                treeGrid.expandNodesByIds(coll, null);
            }
        } else if (items.isEmpty()) {
            treeGrid.selectRecord(null, true);
            afterInitWithNoData();
        }
        isInitialized = true;
    }

    public boolean isTreeInitialized() {
        return isInitialized;
    }

//    public int getTreeId() {
//        return treeId;
//    }
    public void setCellWidgetBuilder(ITreeGridCellWidgetBuilder<BEAN> cellWidgetBuilder) {
        this.cellWidgetBuilder = cellWidgetBuilder;
    }

    //ww: storage jest dostępny dopiero po wywołaniu initWithFullData!
    public ITreeRowAndBeanStorage<IDT, Map<String, Object>, BEAN> getTreeStorage() {
        return storage;
    }

    protected Request makeFilterRequest(final Collection<Integer> treeIds, final String val,
            final Integer optSubTreeRootNodeId,
            final BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String filterRequestTicket, AsyncCallback<List<BEAN>> filterCallback) {
        Request filterRequest;
        if (treeIds == null) {
            filterRequest = getBikFilteredNodes(val, optSubTreeRootNodeId, optExtraMode,
                    optFilteringNodeActionCode, filterRequestTicket, filterCallback);/*BIKClientSingletons.getService().getBikFilteredNodes(treeId, val,
             optExtraMode, filterRequestTicket,
             filterCallback);*/

        } else {
            filterRequest = getBikFilteredNodesWithCondidtion(val, optSubTreeRootNodeId,
                    treeIds, optExtraMode, optFilteringNodeActionCode, filterRequestTicket, filterCallback);//BIKClientSingletons.getService().getBikFilteredNodesWithCondidtion(val, treeIds, noLinkedNode, optExtraMode, filterRequestTicket, filterCallback);
        }
        return filterRequest;
    }

    protected abstract Request getBikFilteredNodes(String val, final Integer optSubTreeRootNodeId, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode,
            String filterRequestTicket, AsyncCallback<List<BEAN>> asyncCallback);

    protected abstract Request getBikFilteredNodesWithCondidtion(String val, final Integer optSubTreeRootNodeId, Collection<Integer> treeIds, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode,
            String filterRequestTicket, AsyncCallback<List<BEAN>> asyncCallback);

    protected abstract MapTreeRowAndBeanStorageBase<IDT, BEAN> createStorage();

    protected abstract ITreeBroker<BEAN, IDT> createTreeBroker();

    protected abstract ILazyRowLoaderEx<Map<String, Object>, IDT> createLazyRowLoader(BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode);// {

    protected IBean4TreeExtractor<BEAN, IDT> getExtractor() {
        if (beanExtractor == null) {
            beanExtractor = createBeanExtractor();
        }
        return beanExtractor;
    }

    public abstract IBean4TreeExtractor<BEAN, IDT> createBeanExtractor();

    public void expandNodesByIds(Collection<IDT> ids) {
        treeGrid.expandNodesByIds(ids, null);
    }

//    private List<BEAN> filtrByRole(List<BEAN> items) {
//        List<BEAN> res = new ArrayList<BEAN>();
//        for (BEAN bean : items) {
//            if (((bean instanceof TreeNodeBean) && BIKClientSingletons.isBranchAuthorLoggedin(((TreeNodeBean) bean).branchIds))
//                    || ((bean instanceof TreeNodeBranchBean) && BIKClientSingletons.isBranchAuthorLoggedin(((TreeNodeBranchBean) bean).branchIds))) {
//                res.add(bean);
//            }
//        }
//        return res;
////        return items;
//    }
}
