exec sp_check_version '1.0.76';
go

-- developerMode - tryb developerski dzia�ania BIK Center
insert into bik_app_prop (name, val) values ('developerMode', '0')

-- logowanie do konsoli w przegl�darce (z poziomu GWT) - dzia�a tylko
-- w trybie developerskim i dla wskazanych typ�w (kinds) logowania
-- typy oddzielamy przecinkami, specjalny typ * oznacza dopuszczenie 
-- wszystkich kind�w
-- dzia�a� te� b�d� subkindy (podtypy) czyli przy wymienionym tutaj ww
-- dzia�a� b�dzie te� ww:abc, ww:xxx - bo znak : w nazwie podtypu u�ytego
-- na kliencie to separator podtyp�w ;-)
insert into bik_app_prop (name, val) values ('allowedShowDiagKinds', 'ww')

-- prosty skrypt do w��czania / prze��czania developerMode i allowedShowDiagKinds
-- jest dost�pny pod nazw� "konfiguruj developerMode i allowedShowDiagKinds.sql"

exec sp_update_version '1.0.76', '1.0.77';
go
