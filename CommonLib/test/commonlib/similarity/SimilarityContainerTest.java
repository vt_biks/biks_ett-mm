/*
 * SimilarityContainerTest.java
 * JUnit based test
 *
 * Created on 31 sierpie� 2007, 20:59
 */
package commonlib.similarity;

import commonlib.JdbcUtil;
import commonlib.LameUtils;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;
import junit.framework.*;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class SimilarityContainerTest extends TestCase {

    public SimilarityContainerTest(String testName) {
        super(testName);
    }

    public void testFindSimilar1() {
        SimilarityContainer2 sc = new SimilarityContainer2(80);
        sc.add("ala");
        sc.add("alaX");
        sc.add("alaY");
        sc.add("alaZ");
        sc.add("hela");
        //sc.add("alZa");
        System.out.println(sc.findSimilar("alaB"));
    }

    public void testFindSimilar2() {
        SimilarityContainer2 sc = new SimilarityContainer2(10);
        sc.add("karolina");

        assertEquals(1, sc.findSimilar("karollina").size());
        assertEquals(1, sc.findSimilar("karrolina").size());
//        assertEquals(0, sc.findSimilar("karrollina").size());
//        assertEquals(1, sc.findSimilar("karoilna").size());
//        assertEquals(1, sc.findSimilar("karola").size());
//        assertEquals(1, sc.findSimilar("karolcia").size());
//        assertEquals(1, sc.findSimilar("karo").size());
//        assertEquals(1, sc.findSimilar("karolla").size());
//        assertEquals(1, sc.findSimilar("karrola").size());
//        assertEquals(1, sc.findSimilar("karolinka").size());
    }

    public void testKotaKoty() {
        SimilarityContainer2 sc = new SimilarityContainer2(70);
        sc.add("kota");
        assertEquals(1, sc.findSimilar("koty").size());
    }

    public void testBPR_OSOBY() {
        //if (true) return;

        SimilarityContainer2 sc = new SimilarityContainer2(70);

        Connection conn = JdbcUtil.makeConn("org.postgresql.Driver", //"jdbc:postgresql://localhost/postgres", "postgres", "kolo"
                "jdbc:postgresql://212.244.29.28/empikWW", "smok", "euro2012byflexi");
        try {
            //conn.setCatalog("bpr");
            Statement st = conn.createStatement();

            System.out.println("before execute query");
            ResultSet rs = st.executeQuery("select OSO_ID, IMIE, NAZWISKO, NAZWA_ZESPOLU from bpr_osoba");
            System.out.println("before read data loop");

            int rowCnt = 0;
            while (rs.next()) {
                Map<String, Object> dataRow = JdbcUtil.resultSetCurrRecToMap(rs);
                String[] words = LameUtils.buildWordArrayNoSort(LameUtils.nullToDef(dataRow.get("IMIE"), "") + " "
                        + LameUtils.nullToDef(dataRow.get("NAZWISKO"), "") + " "
                        + LameUtils.nullToDef(dataRow.get("NAZWA_ZESPOLU"), ""));
                if (words != null) {
                    for (String w : words) {
                        sc.add(w);
                    }
                }
                rowCnt++;
                if (rowCnt % 1000 == 0) {
                    System.out.println("in loop, rowCnt: " + rowCnt);
                }
            }
        } catch (Exception ex) {
            throw new LameRuntimeException(ex);
        }

        sc.add("Nitszche");
        sc.add("NitszcheX");
        sc.add("NitszcheZ");

        System.out.println("before findSimilar, sc.wordCount=" + sc.getWordCount());
        sc.findSimilar("Marry");
        sc.findSimilar("Jackson");
        sc.findSimilar("Micheal");
        sc.findSimilar("Michael");
        sc.findSimilar("Nitzche");
        sc.findSimilar("Nitszche");
        sc.findSimilar("Nitzshe");
        sc.findSimilar("Roman");

        //System.out.println(sc.calcSimilarity("Micheal", "Michael"));
        //System.out.println(sc.calcSimilarity("Nitszche", "Nitzshe"));
    }
}
