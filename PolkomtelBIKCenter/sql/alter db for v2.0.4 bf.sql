exec sp_check_version '2.0.4';
GO

declare @metaCategory int = (select id from bik_attr_category where name='metaBiks')
exec sp_add_attr_def 'metaBiks.Domy�lny szablon', @metaCategory, 'shortText', null, 0, 0
exec sp_add_attr_2_node_kind 'metaBiksNodeKind', 'metaBIKS.Domy�lny szablon', 'Podstawowy', 1, null, null


if not exists (select 1 from sys.all_columns where object_id=object_id('bik_node_kind') and name='default_template')
alter table bik_node_kind add default_template varchar(max) default 'Podstawowy'
go
update bik_node_kind set default_template='Podstawowy'


if not exists (select 1 from sys.all_columns where object_id=object_id('aaa_node_kind') and name='default_template')
alter table aaa_node_kind add default_template varchar(max)
go

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------


if object_id('dbo.sp_build_metamodel') is not null
drop procedure dbo.sp_build_metamodel
go

create procedure dbo.sp_build_metamodel(@deleteOldTree int)
as
begin
    truncate table amg_node
    truncate table amg_attribute

    declare @confTreeCode varchar(max)=(select val from bik_app_prop where name='metadataTree')
    declare @treeId int = (select id from bik_tree where code=@confTreeCode)
    declare @root varchar(max)

    set @root = 'Definicje atrybut�w'
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    values(@root, 'metaBiksDefinitionGroup', null, @root+'|', null, 0)

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select name, 'metaBiksAttributesCatalog', null, @root+'|'+name+'|', @root+'|', 0
    from bik_attr_category where is_deleted=0 and is_built_in=0 and name not like 'metaBiks%'

	--Meta atrybuty do definicji katalogi
    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Nazwa katalogu', bac.name from bik_attr_category bac join amg_node a on a.name=bac.name
    where a.node_kind_code='metaBiksAttributesCatalog' and bac.is_built_in=0 and bac.is_deleted=0 and bac.name not like 'metaBiks%'

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select adef.name, 'metaBiksAttributeDef', null, @root+'|'+bac.name+'|'+adef.name+'|', @root+'|'+bac.name+'|', 0
    from bik_attr_def adef join bik_attr_category bac on adef.attr_category_id=bac.id
    where adef.is_deleted=0 and adef.is_built_in=0 and bac.is_deleted=0 and bac.is_built_in=0 and bac.name not like 'metaBiks%'

    --Meta atrybuty do definicji atrybutow
    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Nazwa atrybutu', adef.name from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0 and adef.name not like 'metaBiks%'

	insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Lista warto�ci', adef.value_opt from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Tre�� pomocy', adef.hint from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Typ atrybutu', adef.type_attr from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.U�ywaj w systemie zarz�dzania regu�ami', case when adef.in_drools=1 then 'Tak' else 'Nie' end from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wy�wietlaj jako liczb�', case when adef.display_as_number=1 then 'Tak' else 'Nie' end from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    --Typ obiektu
    set @root = 'Definicje obiekt�w'
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    values(@root, 'metaBiksDefinitionGroup', null, @root+'|', null, 0)

    declare @ids table(id int)
    insert into @ids
    select distinct branch_node_kind_id from bik_tree_kind where is_deleted=0 union
    select distinct leaf_node_kind_id from bik_tree_kind where is_deleted=0 union
    select distinct dst_node_kind_id from bik_node_kind_4_tree_kind where (src_node_kind_id is null or relation_type = 'Dziecko') and (is_deleted=0) union
    select distinct src_node_kind_id from bik_node_kind_4_tree_kind where (dst_node_kind_id is null or relation_type = 'Dziecko') and (is_deleted=0) union
    select distinct id from bik_node_kind where tree_kind is null or ltrim(tree_kind)=''
    delete from @ids where id in (select nk.id from bik_node_kind nk join @ids ids on ids.id=nk.id and nk.code like 'metaBiks%')

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select code, 'metaBiksNodeKind', null, @root+'|'+code+'|', @root+'|', 0 from bik_node_kind where id in (select id from @ids) and is_deleted=0 and code not like 'metaBiks%'

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select code, 'metaBiksBuiltInNodeKind', null, @root+'|'+code+'|', @root+'|', 0 from bik_node_kind where id not in (select id from @ids) and is_deleted=0 and code not like 'metaBiks%'
    ---------
	insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.attr_calculating_types', nk.attr_calculating_types from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

	insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.SQL', nk.sql from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

	insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Domy�lny szablon', nk.default_template from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

	insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Raporty', nk.jasper_reports from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'



	insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.attr_calculating_expressions', nk.attr_calculating_expressions from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

	insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.attr_dependent_nodes_sql', nk.attr_dependent_nodes_sql from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

	insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.uploadable_children_kinds', nk.uploadable_children_kinds from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

	insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.generic_node_select', nk.generic_node_select from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

	insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Domy�lny opis', nk.default_desc from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'
	
	insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Automatycznie wpisa� nazw� obiektu z warto�ci atrybutu', nk.attr_control_node_name  from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Atrybuty dzieci w tabeli', nk.children_attr_names from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Dzieci w tabeli', nk.children_kinds from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Ikona', nk.icon_name from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Nazwa typu obiektu', nk.caption from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Rejestr', case when nk.is_registry=1 then 'Tak' else 'Nie' end from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Tre�� pomocy dla autora', coalesce(h.text_help, '') from bik_node_kind nk join amg_node a on a.name=nk.code left join bik_help h on help_key=nk.code+':author' and lang='pl'
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Tre�� pomocy dla innych', coalesce(h.text_help, '') from bik_node_kind nk join amg_node a on a.name=nk.code left join bik_help h on help_key=nk.code+':user' and lang='pl'
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'
    ------------
    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Nazwa typu obiektu', nk.caption from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksBuiltInNodeKind' and nk.is_deleted=0 and nk.id not in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Tre�� pomocy dla autora', coalesce(h.text_help, '') from bik_node_kind nk join amg_node a on a.name=nk.code left join bik_help h on help_key=nk.code+':author' and lang='pl'
    where a.node_kind_code='metaBiksBuiltInNodeKind' and nk.is_deleted=0 and nk.id not in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Tre�� pomocy dla innych', coalesce(h.text_help, '') from bik_node_kind nk join amg_node a on a.name=nk.code left join bik_help h on help_key=nk.code+':user' and lang='pl'
    where a.node_kind_code='metaBiksBuiltInNodeKind' and nk.is_deleted=0 and nk.id not in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    declare @attr table(node_kind_code varchar(max), attr_name varchar(max), category varchar(max), default_value varchar(max), is_required int, type_attr varchar(max), value_opt varchar(max), is_empty int, is_public int)
    insert into @attr(node_kind_code, attr_name, category, default_value, is_required, type_attr, value_opt, is_empty, is_public)
    select nk.code, adef.name, c.name, attr.default_value, attr.is_required, attr.type_attr, attr.value_opt, attr.is_empty, attr.is_public
    from bik_attribute attr join bik_node_kind nk on nk.id=attr.node_kind_id
    join bik_attr_def adef on adef.id=attr.attr_def_id
    join bik_attr_category c on adef.attr_category_id=c.id
    where attr.is_deleted=0 and adef.is_built_in=0 and nk.id in (select id from @ids) and nk.code not like 'metaBiks%'

    declare @attr_builtin table(node_kind_code varchar(max), attr_name varchar(max), category varchar(max), default_value varchar(max), is_required int, type_attr varchar(max), value_opt varchar(max), is_empty int, is_public int)
    insert into @attr(node_kind_code, attr_name, category, default_value, is_required, type_attr, value_opt, is_empty, is_public)
    select nk.code, adef.name, c.name, attr.default_value, attr.is_required, attr.type_attr, attr.value_opt, attr.is_empty, attr.is_public
    from bik_attribute attr join bik_node_kind nk on nk.id=attr.node_kind_id
    join bik_attr_def adef on adef.id=attr.attr_def_id
    join bik_attr_category c on adef.attr_category_id=c.id
    where attr.is_deleted=0 and adef.is_built_in=0 and nk.id not in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%' and adef.name not like 'metaBiks%'

    --Atrybut dla typu obiektu dynamicznego
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select attr.attr_name, 'metaBiksAttribute4NodeKind', null, @root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|', @root+'|'+attr.node_kind_code+'|', 0
    from @attr attr

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Lista warto�ci', attr.value_opt from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Typ atrybutu', attr.type_attr from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Mo�e by� pusty', case when attr.is_empty=1 then 'Tak' else 'Nie' end from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Poka� na formatce nowego obiektu', case when attr.is_required=1 then 'Tak' else 'Nie' end from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Publiczny', case when attr.is_public=1 then 'Tak' else 'Nie' end from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Warto�� domy�lna', attr.default_value from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    --Atrybut dla typu obiektu wbudowanego
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select attr.attr_name, 'metaBiksAttribute4NodeKind', null, @root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|', @root+'|'+attr.node_kind_code+'|', 0
    from @attr_builtin attr

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Lista warto�ci', attr.value_opt from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Typ atrybutu', attr.type_attr from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Mo�e by� pusty', case when attr.is_empty=1 then 'Tak' else 'Nie' end from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Poka� na formatce nowego obiektu', case when attr.is_required=1 then 'Tak' else 'Nie' end from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Publiczny', case when attr.is_public=1 then 'Tak' else 'Nie' end from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Warto�� domy�lna', attr.default_value from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    --Typ drzew
    set @root='Definicje drzew'
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    values(@root, 'metaBiksDefinitionGroup', null, @root+'|', null, 0)

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select tk.code, 'metaBiksTreeKind', null, @root+'|'+tk.code+'|', @root+'|', 0
    from bik_tree_kind tk where tk.code not like 'metaBiks%' and tk.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Nazwa typu drzewa', tk.caption from amg_node a join bik_tree_kind tk on a.obj_id=@root+'|'+tk.code+'|'
    where a.node_kind_code='metaBiksTreeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Pozwoli� dowi�za�', case when tk.allow_linking=1 then 'Tak' else 'Nie' end from amg_node a join bik_tree_kind tk on a.obj_id=@root+'|'+tk.code+'|'
    where a.node_kind_code='metaBiksTreeKind'

	 insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wy�wietlaj "Dodaj ga��� g��wn�"', case when tk.show_add_main_branch_btn=1 then 'Tak' else 'Nie' end from amg_node a join bik_tree_kind tk on a.obj_id=@root+'|'+tk.code+'|'
    where a.node_kind_code='metaBiksTreeKind'


    declare @nkInTk table(tk_code varchar(max), src_code varchar(max), dst_code varchar(max), relation_type varchar(max))
    insert into @nkInTk(tk_code, src_code, dst_code, relation_type)
    select tk.code, snk.code, dnk.code, relation_type from bik_node_kind_4_tree_kind nk4tk
    join bik_tree_kind tk on tk.id=nk4tk.tree_kind_id
    left join bik_node_kind snk on snk.id=nk4tk.src_node_kind_id
    left join bik_node_kind dnk on dnk.id=nk4tk.dst_node_kind_id
    where nk4tk.is_deleted=0 and tk.code not like 'metaBiks%' and tk.is_deleted=0 and (snk.is_deleted is null or snk.is_deleted=0) and (dnk.is_deleted is null or dnk.is_deleted=0)
 	--select * from @nkInTk where tk_code='MM_Organizacja'

    declare @nk4tk table(tk_code varchar(max), nk_code varchar(max), type_in_tree varchar(max) default '')
    insert into @nk4tk(tk_code, nk_code)
    select tk_code, src_code as nk_code from @nkInTk where (src_code is not null and relation_type is null) or relation_type='Dziecko' union
    select tk_code, dst_code as nk_code from @nkInTk where (dst_code is not null and relation_type is null) or relation_type='Dziecko' union
    select tk_code, src_code as nk_code from @nkInTk where relation_type is not null and relation_type <> 'Dziecko'

	--select * from @nk4tk where tk_code='MM_Organizacja'

    update t1 set t1.type_in_tree='G��wna ga���+' from @nk4tk t1 where t1.nk_code in (select t2.dst_code from @nkInTk t2 where t2.tk_code=t1.tk_code and t2.src_code is null and t2.dst_code is not null and t2.relation_type is null)
    update t1 set t1.type_in_tree=t1.type_in_tree+'Ga���+' from @nk4tk t1 where t1.nk_code in (select t2.src_code from @nkInTk t2 where t2.tk_code=t1.tk_code and t2.src_code is not null and t2.dst_code is not null and t2.relation_type='Dziecko')
    update t1 set t1.type_in_tree=t1.type_in_tree+'Li��+' from @nk4tk t1 where t1.nk_code in (select t2.src_code from @nkInTk t2 where t2.src_code is not null and t2.dst_code is null and t2.relation_type is null)
    update @nk4tk set type_in_tree=substring(type_in_tree, 0, len(type_in_tree)) where type_in_tree like '%+'

	--select * from @nk4tk where tk_code='MM_Organizacja'

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select nk4tk.nk_code, 'metaBiksNodeKind4TreeKind', null, @root+'|'+nk4tk.tk_code+'|'+nk4tk.nk_code+'|', @root+'|'+nk4tk.tk_code+'|', 0
    from @nk4tk as nk4tk

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Typ w drzewie', nk4tk.type_in_tree from @nk4tk nk4tk join amg_node a on a.obj_id= @root + '|'+nk4tk.tk_code+'|'+nk4tk.nk_code+'|'
    where a.node_kind_code='metaBiksNodeKind4TreeKind'

    declare @relation table(obj_id varchar(max), value varchar(max))
    insert into @relation(obj_id, value)
    select @root + '|'+nkInTk.tk_code+'|'+nkInTk.src_code+'|'+nkInTk.dst_code+'|', nkInTk.relation_type from @nkInTk nkInTk
    where nkInTk.relation_type is not null

    insert into amg_attribute(obj_id, name, value)
    select obj_id, 'metaBiks.Typ relacji', stuff((select '+' + r2.value from @relation r2 where r1.obj_id = r2.obj_id for xml path ('')),1,1,'') from @relation r1 group by obj_id

	insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
	select x.str, 'metaBiksNodeKindInRelation', null, a.obj_id, substring(a.obj_id, 1, len(a.obj_id)-charindex('|',reverse(a.obj_id),2)+1), 0
    from amg_attribute as a cross apply fn_split_by_sep(a.obj_id, '|', 7) as x
	where a.name='metaBiks.Typ relacji' and x.idx=4

	/* szablony */
	delete from amg_templates
	insert into amg_templates (name, node_kind_code, descr, obj_id, parent_obj_id, visual_order,main_node_kind_name)
	select distinct group_template,'metaBiksPrintsTemplates',null,an.obj_id+group_template+'|',an.obj_id,0,main_node_kind_name
	from bik_group_template pbt join amg_node an on pbt.main_node_kind_name=an.name and an.node_kind_code='metaBiksNodeKind'
	select * from amg_templates

	insert into amg_templates (name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
	select distinct template,  'metaBiksPrint',null,at.obj_id+template+'|',at.obj_id,0
	from bik_group_template bt join amg_templates at on bt.group_template=at.name --and an.node_kind_code='metaBiksNodeKind'
	and bt.main_node_kind_name=at.main_node_kind_name

	insert into amg_templates (name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
	select name,node_kind_code,null, dbo.fn_get_obj_id_by_node_name_path(node_id),dbo.fn_get_obj_id_by_node_name_path(parent_node_id) ,0
	from bik_print_template

	insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
	select name,node_kind_code,null,obj_id,parent_obj_id,0 from amg_templates

	exec sp_generic_insert_into_bik_node_ex @confTreeCode, null, @deleteOldTree;

    --Atrybuty Wzorzec
    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wzorzec', cast(bn.id as varchar(100))+'_'+@confTreeCode+','+attr.attr_name from amg_node a join @attr attr on a.obj_id='Definicje obiekt�w|'+attr.node_kind_code+'|'+attr.attr_name+'|' join bik_node bn on bn.obj_id='Definicje atrybut�w|'+attr.category+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind' and bn.tree_id=@treeId and bn.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wzorzec', cast(bn.id as varchar(100))+'_'+@confTreeCode+','+attr.attr_name from amg_node a join @attr_builtin attr on a.obj_id='Definicje obiekt�w|'+attr.node_kind_code+'|'+attr.attr_name+'|' join bik_node bn on bn.obj_id='Definicje atrybut�w|'+attr.category+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind' and bn.tree_id=@treeId and bn.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wzorzec', cast(bn.id as varchar(100))+'_'+@confTreeCode+','+nk_code from amg_node a join @nk4tk nk4tk on a.obj_id='Definicje drzew|'+nk4tk.tk_code+'|'+nk4tk.nk_code+'|' join bik_node bn on bn.obj_id='Definicje obiekt�w|'+nk4tk.nk_code+'|'
    where a.node_kind_code='metaBiksNodeKind4TreeKind' and bn.tree_id=@treeId and bn.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wzorzec', cast(bn.id as varchar(100))+'_'+@confTreeCode+','+x.str
	from amg_attribute a cross apply dbo.fn_split_by_sep(a.obj_id, '|', 7) as x join bik_node bn on bn.obj_id='Definicje obiekt�w|'+x.str+'|'
    where a.name='metaBiks.Typ relacji' and bn.tree_id=@treeId and bn.is_deleted=0 and x.idx=4
    --
    merge bik_attribute_linked al
    using (
    select bn.id as node_id, attr.id as attr_id, a.value from bik_node bn join amg_attribute a on bn.obj_id=a.obj_id
    join bik_tree bt on bn.tree_id=bt.id
    join bik_attr_def adef on adef.name=a.name
    join bik_attribute attr on attr.node_kind_id=bn.node_kind_id and attr.attr_def_id=adef.id
    where bt.code=@confTreeCode
    ) x on x.node_id=al.node_id and x.attr_id=al.attribute_id
    when matched then
    update set al.value=x.value, al.is_deleted=0
    when not matched by target then
    insert (attribute_id, value, node_id, is_deleted) values (x.attr_id, x.value, x.node_id, 0)
    when not matched by source and al.node_id in (select bn.id from bik_node as bn inner join bik_tree as bt on bt.id = bn.tree_id and bt.code = @confTreeCode) then
    update set al.is_deleted=@deleteOldTree
    ;
end
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------



if object_id('dbo.sp_implement_metamodel') is not null
drop procedure dbo.sp_implement_metamodel
go

create procedure dbo.sp_implement_metamodel
as
begin
	declare @metaTree int = (select id from bik_tree where code in (select val from bik_app_prop where name='metadataTree'))
	update bik_attr_category set is_deleted=1 where is_built_in=0 and name not like 'metaBiks%'

	truncate table aaa_attr_category
	insert into aaa_attr_category(id, code)
	select bn.id, bn.name
	from bik_node bn join bik_node_kind nk on nk.id=bn.node_kind_id join bik_tree bt on bt.id=bn.tree_id
	where nk.code='metaBiksAttributesCatalog' and bn.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_attr_category', 'metaBiks.Nazwa katalogu', 'name', null

	update bik_attr_category set is_deleted=1 where is_built_in=0 and name not like 'metaBiks%'

	--Kategorie atrybutow
	merge bik_attr_category cat
	using aaa_attr_category x on x.code=cat.name
	when matched then update set is_deleted=0, name=x.name
	when not matched by target then insert (name) values(x.name);

	update bn set bn.name=x.name
	from bik_node bn join aaa_attr_category x on x.code=bn.name

	update bik_attr_def set is_deleted=1 where is_built_in=0 and name not like 'metaBiks%'

	truncate table aaa_attr_def
	insert into aaa_attr_def(id, code, attr_category_id)
	select bn1.id, bn1.name, cat.id
	from bik_node bn1 join bik_node bn2 on bn1.parent_node_id=bn2.id
	join bik_tree bt on bt.id=bn1.tree_id
	join bik_node_kind nk on bn1.node_kind_id=nk.id
	join bik_attr_category cat on cat.name=bn2.name and cat.is_built_in=0
	where bn1.is_deleted=0 and nk.code='metaBiksAttributeDef' and bt.id=@metaTree

	declare @binaryConverter varchar(max)='case x.value when ''Tak'' then 1 else 0 end'
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Nazwa atrybutu', 'name', null
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Lista warto�ci', 'value_opt', null
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Tre�� pomocy', 'hint', null
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Typ atrybutu', 'type_attr', null
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.U�ywaj w systemie zarz�dzania regu�ami', 'in_drools', @binaryConverter
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Wy�wietlaj jako liczb�', 'display_as_number', @binaryConverter

	--Atrybuty
	merge bik_attr_def adef
	using aaa_attr_def x on x.code=adef.name and adef.is_built_in=0
	when matched then update set is_deleted=0, name=x.name, hint=x.hint, type_attr=x.type_attr, value_opt=x.value_opt, display_as_number=x.display_as_number, in_drools=x.in_drools
	when not matched by target then
	insert(name, attr_category_id, hint, type_attr, value_opt, display_as_number, in_drools)
	values(x.name, x.attr_category_id, x.hint, x.type_attr, x.value_opt, x.display_as_number, x.in_drools);

	declare attr_def_cursor cursor local
		for select id, name from aaa_attr_def
	declare @node_id int
	declare @attr_name varchar(max)
	open attr_def_cursor
	fetch next from attr_def_cursor into @node_id, @attr_name
	while @@fetch_status=0 begin
		exec sp_meta_biks_change_node_name @node_id, @attr_name
		fetch next from attr_def_cursor into @node_id, @attr_name
	end
	close attr_def_cursor

	--Aktualizacja nazw w obiektach szablon�w, mo�na doda� sprawdzenie czy s� szablony, zeby niepotrzebnie nie przelicza�?
		 	declare attr_def_cursor2 cursor local
		for select id, name from bik_node where node_kind_id=(select id from bik_node_kind where code='metaBiksAttribute4NodeKind')
--	declare @node_id int
--	declare @attr_name varchar(max)
	open attr_def_cursor2
	fetch next from attr_def_cursor2 into @node_id, @attr_name
	while @@fetch_status=0 begin
		exec sp_meta_biks_change_node_name @node_id, @attr_name
		fetch next from attr_def_cursor2 into @node_id, @attr_name
	end
	close attr_def_cursor2




	update bik_attr_def set hint=null where ltrim(hint)=''
	update bik_attr_def set value_opt=null where ltrim(value_opt)=''

	update bik_node_kind set is_deleted=1 where code not like 'metaBiks%'

	--node_kind
	truncate table aaa_node_kind
	insert into aaa_node_kind(id, code)
	select bn.id, bn.name from bik_node bn join bik_tree bt on bn.tree_id=bt.id
	join bik_node_kind nk on nk.id=bn.node_kind_id
	where nk.code='metaBiksNodeKind' and bn.is_deleted=0 and bt.id=@metaTree

	truncate table aaa_help
	insert into aaa_help(id, code)
	select bn.id, bn.name from bik_node bn join bik_tree bt on bn.tree_id=bt.id
	join bik_node_kind nk on nk.id=bn.node_kind_id
	where nk.code='metaBiksNodeKind' and bn.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Raporty', 'jasper_reports', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Domy�lny szablon', 'default_template', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.SQL', 'sql', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.attr_calculating_types', 'attr_calculating_types', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.attr_calculating_expressions', 'attr_calculating_expressions', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.attr_dependent_nodes_sql', 'attr_dependent_nodes_sql', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.uploadable_children_kinds', 'uploadable_children_kinds', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.generic_node_select', 'generic_node_select', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Domy�lny opis', 'default_desc', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Automatycznie wpisa� nazw� obiektu z warto�ci atrybutu', 'attr_control_node_name', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Atrybuty dzieci w tabeli', 'children_attr_names', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Dzieci w tabeli', 'children_kinds', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Ikona', 'icon_name', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Nazwa typu obiektu', 'caption', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Rejestr', 'is_registry', @binaryConverter
	exec sp_update_table_column 'aaa_help', 'metaBiks.Nazwa typu obiektu', 'caption', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Tre�� pomocy dla autora', 'help_author', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Tre�� pomocy dla innych', 'help_user', null

	update bik_node_kind set is_deleted=1 where code not like 'metaBiks%' 
	merge bik_node_kind nk
	using aaa_node_kind x on x.code=nk.code
	when matched then update set 
	is_deleted=0, 
	children_attr_names=x.children_attr_names, 
	children_kinds=x.children_kinds, 
	icon_name=x.icon_name, 
	caption=x.caption, 
	is_registry=x.is_registry,
	attr_calculating_types=x.attr_calculating_types,
	attr_calculating_expressions=x.attr_calculating_expressions,
	attr_dependent_nodes_sql=x.attr_dependent_nodes_sql,
	uploadable_children_kinds=x.uploadable_children_kinds,
	generic_node_select=x.generic_node_select,
	default_desc=x.default_desc,
	attr_control_node_name=x.attr_control_node_name,
	jasper_reports=x.jasper_reports,
	default_template=x.default_template,
	sql=x.sql
	when not matched by target then
	insert(code, children_attr_names, children_kinds, icon_name, caption, is_registry)
	values(x.code, x.children_attr_names, x.children_kinds, x.icon_name, x.caption, x.is_registry);

	--built_in_node_kind
	truncate table aaa_node_kind
	insert into aaa_node_kind(id, code)
	select bn.id, bn.name from bik_node bn join bik_tree bt on bn.tree_id=bt.id
	join bik_node_kind nk on nk.id=bn.node_kind_id
	where nk.code='metaBiksBuiltInNodeKind' and bn.is_deleted=0 and bt.id=@metaTree

	insert into aaa_help(id, code)
	select bn.id, bn.name from bik_node bn join bik_tree bt on bn.tree_id=bt.id
	join bik_node_kind nk on nk.id=bn.node_kind_id
	where nk.code='metaBiksBuiltInNodeKind' and bn.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Nazwa typu obiektu', 'caption', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Nazwa typu obiektu', 'caption', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Tre�� pomocy dla autora', 'help_author', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Tre�� pomocy dla innych', 'help_user', null

	
	merge bik_node_kind nk
	using aaa_node_kind x on x.code=nk.code
	when matched then update set is_deleted=0, caption=x.caption;

	--help
	merge bik_help bh
	using aaa_help x on x.code+':author'=bh.help_key and bh.lang='pl'
	when matched then update set is_hidden=0, caption=x.caption, text_help=x.help_author
	when not matched by target then
	insert(help_key, caption, text_help, lang)
	values(x.code+':author', x.caption, x.help_author, 'pl');

	merge bik_help bh
	using aaa_help x on x.code+':user'=bh.help_key and bh.lang='pl'
	when matched then update set is_hidden=0, caption=x.caption, text_help=x.help_author
	when not matched by target then
	insert(help_key, caption, text_help, lang)
	values(x.code+':user', x.caption, x.help_user, 'pl');

	update attr set is_deleted=1
	from bik_attribute attr join bik_attr_def adef on adef.id=attr.attr_def_id
	join bik_node_kind nk on nk.id=attr.node_kind_id
	where nk.code not like 'metaBiks%' and adef.name not like 'metaBiks%' and adef.is_built_in=0

	--atrybuty dla node_kind
	truncate table aaa_attribute
	insert into aaa_attribute(id, node_kind_id, attr_def_id)
	select bn1.id, nk2.id, adef.id
	from bik_node bn1 join bik_node bn2 on bn1.parent_node_id=bn2.id
	join bik_tree bt on bt.id=bn1.tree_id
	join bik_node_kind nk1 on nk1.id=bn1.node_kind_id
	join bik_node_kind nk2 on nk2.code=bn2.name
	join bik_attr_def adef on adef.name=bn1.name
	where nk1.code='metaBiksAttribute4NodeKind' and bn1.is_deleted=0 and bt.id=@metaTree and adef.is_built_in=0

	--select * from aaa_attribute where id=136376

	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Lista warto�ci', 'value_opt', null
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Mo�e by� pusty', 'is_empty', @binaryConverter
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Poka� na formatce nowego obiektu', 'is_required', @binaryConverter
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Publiczny', 'is_public', @binaryConverter
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Typ atrybutu', 'type_attr', null
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Warto�� domy�lna', 'default_value', null

	merge bik_attribute attr
	using aaa_attribute x on attr.node_kind_id=x.node_kind_id and attr.attr_def_id=x.attr_def_id
	when matched then update set is_deleted=0, default_value=x.default_value, is_required=x.is_required, type_attr=x.type_attr, value_opt=x.value_opt, is_empty=x.is_empty, is_public=x.is_public
	when not matched by target then
	insert(node_kind_id, attr_def_id, default_value, is_required, type_attr, value_opt, is_empty, is_public)
	values(x.node_kind_id, x.attr_def_id, x.default_value, x.is_required, x.type_attr, x.value_opt, x.is_empty, x.is_public);

	update bik_attribute set default_value=null where ltrim(default_value)=''
	update bik_attribute set value_opt=null where ltrim(value_opt)=''
	update bik_attribute set type_attr=null where ltrim(type_attr)=''

	--tree_kind
	update bik_tree_kind set is_deleted=1 where code not like 'metaBiks%'

	truncate table aaa_tree_kind
	insert into aaa_tree_kind(id, code)
	select bn.id, bn.name
	from bik_node bn join bik_node_kind nk on nk.id=bn.node_kind_id
	join bik_tree bt on bt.id=bn.tree_id
	where nk.code='metaBiksTreeKind' and bn.is_deleted=0 and bt.id=@metaTree
	--select * from aaa_tree_kind

	exec sp_update_table_column 'aaa_tree_kind', 'metaBiks.Nazwa typu drzewa', 'caption', null
	exec sp_update_table_column 'aaa_tree_kind', 'metaBiks.Pozwoli� dowi�za�', 'allow_linking', @binaryConverter
	exec sp_update_table_column 'aaa_tree_kind', 'metaBiks.Wy�wietlaj "Dodaj ga��� g��wn�"', 'show_add_main_branch_btn', @binaryConverter



	merge bik_tree_kind tk
	using aaa_tree_kind x on tk.code=x.code
	when matched then update set is_deleted=0, caption=x.caption, allow_linking=x.allow_linking,show_add_main_branch_btn=x.show_add_main_branch_btn
	when not matched by target then
	insert(code, caption, allow_linking,show_add_main_branch_btn)
	values(x.code, x.caption, x.allow_linking,x.show_add_main_branch_btn);





	truncate table aaa_node_kind_in_tree_kind
	insert into aaa_node_kind_in_tree_kind(id, tree_kind_id, node_kind_id)
	select bn1.id, tk.id, nk2.id
	from bik_node bn1 join bik_node bn2 on bn1.parent_node_id=bn2.id
	join bik_node_kind nk1 on nk1.id=bn1.node_kind_id
	join bik_tree bt on bt.id=bn1.tree_id
	join bik_node_kind nk2 on nk2.code=bn1.name
	join bik_tree_kind tk on tk.code=bn2.name
	where nk1.code='metaBiksNodeKind4TreeKind' and bn1.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_node_kind_in_tree_kind', 'metaBiks.Typ w drzewie', 'type', null

	update tk set branch_node_kind_id=x.node_kind_id
	from bik_tree_kind tk join aaa_node_kind_in_tree_kind x on x.tree_kind_id=tk.id
	where charindex('Ga���', x.type, 0)>0 or charindex('G��wna ga���', x.type, 0)>0

	update tk set leaf_node_kind_id=x.node_kind_id
	from bik_tree_kind tk join aaa_node_kind_in_tree_kind x on x.tree_kind_id=tk.id
	where charindex('Li��', x.type, 0)>0

	update nk4tk set is_deleted=1
	from bik_node_kind_4_tree_kind nk4tk join bik_tree_kind tk on tk.id=nk4tk.tree_kind_id
	left join bik_node_kind snk on snk.id=nk4tk.src_node_kind_id
	left join bik_node_kind dnk on dnk.id=nk4tk.dst_node_kind_id
	where tk.code not like 'metaBiks%' and (snk.code is null or snk.code not like 'metaBiks%') and (dnk.code is null or dnk.code not like 'metaBiks%')

	truncate table aaa_node_kind_4_tree_kind

	insert into aaa_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type)
	select tree_kind_id, null, node_kind_id, null
	from aaa_node_kind_in_tree_kind x
	where charindex('G��wna ga���', x.type, 0)>0

	insert into aaa_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type)
	select tree_kind_id, node_kind_id, null, null
	from aaa_node_kind_in_tree_kind x
	where charindex('Li��', x.type, 0)>0

	truncate table aaa_node_kind_relation_in_tree
	insert into aaa_node_kind_relation_in_tree(id, tree_kind_id, src_node_kind_id, dst_node_kind_id)
	select bn1.id, tk.id, snk.id, dnk.id
	from bik_node bn1 join bik_node bn2 on bn1.parent_node_id=bn2.id
	join bik_node bn3 on bn2.parent_node_id=bn3.id
	join bik_tree bt on bt.id=bn1.tree_id
	join bik_node_kind nk on bn1.node_kind_id=nk.id
	join bik_node_kind snk on bn2.name=snk.code
	join bik_node_kind dnk on bn1.name=dnk.code
	join bik_tree_kind tk on bn3.name=tk.code
	where nk.code='metaBiksNodeKindInRelation' and bn1.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_node_kind_relation_in_tree', 'metaBiks.Typ relacji', 'relation_type', null

	insert into aaa_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type)
	select tree_kind_id, src_node_kind_id, dst_node_kind_id, 'Powi�zanie' from aaa_node_kind_relation_in_tree where charindex('Powi�zanie', relation_type, 0) > 0 union
	select tree_kind_id, src_node_kind_id, dst_node_kind_id, 'Dowi�zanie' from aaa_node_kind_relation_in_tree where charindex('Dowi�zanie', relation_type, 0) > 0 union
	select tree_kind_id, src_node_kind_id, dst_node_kind_id, 'Hyperlink' from aaa_node_kind_relation_in_tree where charindex('Hyperlink', relation_type, 0) > 0 union
	select tree_kind_id, src_node_kind_id, dst_node_kind_id, 'Dziecko' from aaa_node_kind_relation_in_tree where charindex('Dziecko', relation_type, 0) > 0

	update nk4tk set nk4tk.is_deleted=1
	from bik_node_kind_4_tree_kind nk4tk join bik_tree_kind tk on tk.id=nk4tk.tree_kind_id
	where tk.code not like 'metaBiks%'

	merge bik_node_kind_4_tree_kind nk4tk
	using aaa_node_kind_4_tree_kind x on x.tree_kind_id=nk4tk.tree_kind_id and x.src_node_kind_id=nk4tk.src_node_kind_id and x.dst_node_kind_id=nk4tk.dst_node_kind_id and x.relation_type=nk4tk.relation_type
	when matched then update set is_deleted=0
	when not matched by target then
	insert(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type)
	values(x.tree_kind_id, x.src_node_kind_id, x.dst_node_kind_id, x.relation_type);

	delete from bik_node_kind_4_tree_kind where is_deleted=1
	
	declare nk4tk_cursor cursor local
		for select tk.code as tree_code, tk.caption as tree_name, snk.code as src_code, dnk.code as dst_code 
		from bik_tree_kind tk  
		join bik_node_kind snk on snk.id=tk.branch_node_kind_id
		join bik_node_kind dnk on dnk.id=tk.leaf_node_kind_id
		where tk.code not like 'metaBiks%' and tk.is_deleted=0
	declare @tree_code varchar(max)
	declare @tree_name varchar(max)
	declare @src_code varchar(max)
	declare @dst_code varchar(max)
	open nk4tk_cursor
	fetch next from nk4tk_cursor into @tree_code, @tree_name, @src_code, @dst_code
	while @@fetch_status=0 begin
		set @tree_code='@'+@tree_code
		if @src_code is not null set @src_code='&'+@src_code
		set @dst_code='&'+@dst_code

		exec sp_add_menu_node 'knowledge', @tree_name, @tree_code
		if @src_code is null 
			exec sp_add_menu_node @tree_code, '@', @dst_code
		else begin
			exec sp_add_menu_node @tree_code, '@', @src_code
			if @src_code <> @dst_code exec sp_add_menu_node @src_code, '@', @dst_code
		end

		fetch next from nk4tk_cursor into @tree_code, @tree_name, @src_code, @dst_code
	end
	close nk4tk_cursor

	declare @treeOfTrees int = dbo.fn_tree_id_by_code('TreeOfTrees')
	exec sp_node_init_branch_id @treeOfTrees, null

	update bt set bt.node_kind_id=null from bik_tree bt join bik_tree_kind tk on bt.tree_kind=tk.code
	left join bik_node_kind nk on nk.id=bt.node_kind_id
	where tk.code not like 'metaBiks%' and bt.is_built_in=0 and bt.is_hidden=0 and (tk.is_deleted=1 or nk.is_deleted=1)

	update bt set node_kind_id=dst_node_kind_id  
	from bik_tree bt join bik_tree_kind tk on bt.tree_kind=tk.code
	join bik_node_kind_4_tree_kind nk4tk on nk4tk.tree_kind_id=tk.id
	where bt.is_hidden=0 and  bt.is_built_in=0 and tk.is_deleted=0 and tk.code not like 'metaBiks%' and nk4tk.is_deleted=0 and nk4tk.relation_type is null and nk4tk.src_node_kind_id is null

	/* Szablony */
	delete from aaa_template
	insert into aaa_template(group_template,node_id,parent_node_id)
	select distinct name,id,parent_node_id from bik_node where node_kind_id in (select id from bik_node_kind where code='metaBiksPrintsTemplates')
	and is_deleted=0
	update aaa_template set main_node_kind_name= (select name from bik_node where id=aaa_template.parent_node_id and is_deleted=0)

	delete from bik_group_template
	insert into bik_group_template
	select group_template,bn.name,at.main_node_kind_name,bn.id,branch_ids
	from aaa_template at join bik_node bn on at.node_id=bn.parent_node_id
	where bn.is_deleted=0


	delete from bik_print_template
	insert into bik_print_template
	(node_id
	,name
	,node_kind_code
	,parent_node_id
	,branch_ids
	,poziom
	)	
	select
	bn.id
	,name
	,code
	, parent_node_id
	, branch_ids
	,LEN(branch_ids) - LEN(REPLACE(branch_ids,'|',''))-4
	from bik_node bn
	join bik_node_kind bnk on bn.node_kind_id=bnk.id
	where code in ('metaBiksPrintAttribut','metaBiksPrintKind','metaBiksPrintBuiltInKind')	
	and bn.is_Deleted=0


	UPDATE
	bik_print_template
	SET
	bik_print_template.template =bn.template,
	bik_print_template.main_node_kind_name =	bn.main_node_kind_name
	FROM
	bik_print_template a
	INNER JOIN bik_group_template bn ON a.branch_ids like bn.branch_ids+'%'

	update bik_print_template set parent_name = (select name from bik_node where id = bik_print_template.parent_node_id and is_deleted=0)


end
go

exec sp_update_version '2.0.4','2.0.5';
GO