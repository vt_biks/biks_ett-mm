exec sp_check_version '1.0.13';
go

----------------------------------------------------------------------
----------------------------------------------------------------------
create table bik_monitor(
	id int identity(1,1) not null primary key,
	name varchar(255) not null,
	caption varchar(max) not null,
	node_id int not null references bik_node (id),
	constraint uk_bik_monior_id_node_id unique(name, node_id)
)
go
----------------------------------------------------------------------
/*do wrzucania danych*/
----------------------------------------------------------------------
declare @node_id int;

select @node_id = bik_node.id
from bik_node join bik_tree on bik_node.tree_id = bik_tree.id
where bik_tree.name = 'Teradata' and bik_node.name like 'VD_US_AIMS'


insert into bik_monitor (name, caption, node_id)
values('data zasilenia', '28.03.2011', @node_id)

insert into bik_monitor (name, caption, node_id)
values('aktualno�� danych', '27.03.2011', @node_id)

insert into bik_monitor (name, caption, node_id)
values('proces zasilaj�cy', 'pusty', @node_id)

insert into bik_monitor (name, caption, node_id)
values('liczba zapyta� /dzie�', '43', @node_id)

insert into bik_monitor (name, caption, node_id)
values('liczba raport�w powi�zanych', '15', @node_id)

insert into bik_monitor (name, caption, node_id)
values('liczba u�ytkownik�w', '24', @node_id)
go

----------------------------------------------------------------------
----------------------------------------------------------------------

exec sp_update_version '1.0.13', '1.0.14';
go