﻿exec sp_check_version '1.0.88.6';
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_connections_for_universe_objects]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_connections_for_universe_objects
go

create procedure [dbo].[sp_insert_connections_for_universe_objects]
as
begin
	
	declare @uniKind int;
    select @uniKind = id from bik_node_kind where code='Universe'

    declare @dcKind int;
    select @dcKind = id from bik_node_kind where code='DataConnection'

    create table #tmpBranch (branch_id varchar(max), node_id int);

    insert into #tmpBranch(branch_id,node_id)
    select bn.branch_ids, bn2.id from bik_node bn
    join bik_joined_objs jo on jo.src_node_id=bn.id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.node_kind_id=@dcKind
    and bn2.linked_node_id is null
    and bn2.is_deleted = 0
    where bn.node_kind_id=@uniKind
    and bn.linked_node_id is null
    and bn.is_deleted = 0

    exec sp_prepare_bik_joined_objs_tmp

    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id, a.node_id, 1 from #tmpBranch a join bik_node bn on bn.branch_ids like a.branch_id+'%'
    and bn.is_deleted=0
    and bn.linked_node_id is null
    where node_kind_id !=@uniKind
	
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn2.id, bn.id, 1 from bik_node bn join bik_node bn2 on bn2.branch_ids like bn.branch_ids+'%'
    and bn2.is_deleted=0
    and bn2.linked_node_id is null
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.node_kind_id=@uniKind
    and bn2.node_kind_id!=@uniKind

    delete from #tmpBranch
    drop table #tmpBranch

    exec sp_move_bik_joined_objs_tmp
	
end
go

exec sp_update_version '1.0.88.6', '1.0.88.7';
go