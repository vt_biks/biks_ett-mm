exec sp_check_version '1.0.32';
go

/*if  exists (select * from sys.foreign_keys where object_id = object_id(n'[dbo].[fk__bik_user__user_k__0c0d1618]') and parent_object_id = object_id(n'[dbo].[bik_user]'))
alter table [dbo].[bik_user] drop constraint [fk__bik_user__user_k__0c0d1618]

go*/

declare @nameFK varchar(100);

select @nameFK = 
fk.ForeignKey_Name
from sysobjects so inner join syscolumns sc on so.id = sc.id
 left join vw_ww_foreignkeys fk on fk.table_name = so.name and fk.name = sc.name
where  so.name like 'bik_user'
and sc.name = 'user_kind_id'
print @nameFK

exec ('alter table bik_user drop constraint ' + @nameFK)
go
alter table [dbo].[bik_user]  with check add foreign key([user_kind_id])
references [dbo].[bik_user_kind] ([id])
go

exec sp_update_version '1.0.32', '1.0.33';
go