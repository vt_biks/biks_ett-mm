/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog.AttributeType;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.JoinedObjAttributeLinkedBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author bfechner
 */
public class AddOrEditJoinedAttributeLinkedDialog extends BaseActionOrCancelDialog {

    protected List<JoinedObjAttributeLinkedBean> joinedObjAttributesLinked;
    protected Map<String, AttributeBean> joinedObjAttributes;
    protected IParametrizedContinuation<Pair<List<JoinedObjAttributeLinkedBean>, Integer>> saveCont;
    protected VerticalPanel mainWithFt;
    protected List<FlexTable> ftw = new ArrayList<FlexTable>();
    protected boolean withNodeId;
    protected TextBox tbNodeId = new TextBox();
    protected Label lblodeId = new Label("Node Id");

    public void buildAndShowDialog(Pair<List<JoinedObjAttributeLinkedBean>, Map<String, AttributeBean>> joinedObjAttributesAndLinked, boolean withNodeId, IParametrizedContinuation<Pair<List<JoinedObjAttributeLinkedBean>, Integer>> saveCont) {
        this.joinedObjAttributesLinked = joinedObjAttributesAndLinked.v1;
        this.joinedObjAttributes = joinedObjAttributesAndLinked.v2;
        this.saveCont = saveCont;
        this.withNodeId = withNodeId;
        super.buildAndShowDialog(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getDialogCaption() {
        return withNodeId ? I18n.dodajAtrybut.get() : I18n.edytujAtrybuty.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.save.get();
    }

    @Override
    protected void buildMainWidgets() {

        mainWithFt = new VerticalPanel();
        PushButton addAttributeBtn = new PushButton(I18n.dodajAtrybut.get(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (ftw.isEmpty()) {
                    addFlexTableHeader();
                }

                addFlexTables(null);
            }
        });
        boolean isJojnedAttrEmpty = joinedObjAttributes.isEmpty();
        addAttributeBtn.setEnabled(!isJojnedAttrEmpty);
        NewLookUtils.makeCustomPushButton(addAttributeBtn);
        if (withNodeId) {
            Grid grid = new Grid(1, 2);
            grid.setCellSpacing(4);
            grid.setWidget(0, 0, lblodeId);
            grid.setWidget(0, 1, tbNodeId);
            lblodeId.setStyleName("addNoteLbl");
            tbNodeId.setStyleName("addNote");
            tbNodeId.setWidth("100px");
            main.add(grid);
        }

        main.add(addAttributeBtn);
        if (isJojnedAttrEmpty) {
            main.add(new Label(I18n.brakZdefiniowanychAtrybutów.get()));
        }
        buildInnerWidget();
        main.add(mainWithFt);
    }

    protected void buildInnerWidget() {
        if (joinedObjAttributesLinked.size() > 0) {
            addFlexTableHeader();
        }
        for (JoinedObjAttributeLinkedBean jo : joinedObjAttributesLinked) {
            addFlexTables(jo);
        }
    }

    protected void addFlexTableHeader() {

        FlexTable ft = new FlexTable();
        ft.setWidget(0, 0, getHTMLWidgetWithStyle(I18n.atrybut.get()));
        ft.setWidget(0, 1, getHTMLWidgetWithStyle(I18n.wartoscDlaWezlaZrodlowego.get()));
        ft.setWidget(0, 2, new HTML());
        ft.setWidget(0, 3, getHTMLWidgetWithStyle(I18n.wartoscDlaWezlaDocelowego.get()));
        ft.setWidget(0, 4, new HTML());
        ft.setWidget(0, 5, new HTML());
        ft.getColumnFormatter().setWidth(2, "26px");
        ft.getColumnFormatter().setWidth(4, "20px");
        ft.getColumnFormatter().setWidth(5, "20px");
        ft.setStyleName("gridJoinedObj");
        ft.getRowFormatter().setStyleName(0, "RelatedObj-oneObj-dqc");
        mainWithFt.add(ft);

    }

    protected void addFlexTables(final JoinedObjAttributeLinkedBean attributeLinkedBean) {
        boolean isNewBean = attributeLinkedBean == null;
        final FlexTable ft = new FlexTable();
        RadioButton rb = new RadioButton("isMainRb");
        AttributeBean attributeBean = !isNewBean ? joinedObjAttributes.get(attributeLinkedBean.name) : joinedObjAttributes.values().iterator().next();;
        String linkedValueFrom = !isNewBean ? attributeLinkedBean.value : "";
        String linkedValueTo = !isNewBean ? attributeLinkedBean.valueTo : "";

        ft.setWidget(0, 0, setAttributesListBox(attributeLinkedBean != null ? attributeLinkedBean.name : null, ft));
        String typeAttr = attributeBean.typeAttr;
        ft.setWidget(0, 1, addWidget(typeAttr, linkedValueFrom, attributeBean.valueOpt));
        ft.setWidget(0, 2, addChangeBtn(ft));
        ft.setWidget(0, 3, addWidget(typeAttr, linkedValueTo, isAttrTypeDefaultValue(typeAttr) ? attributeBean.valueOptTo : attributeBean.valueOpt));
        ft.setWidget(0, 4, addRemoveBtn(ft));
        ft.setWidget(0, 5, rb);
        rb.setValue(!isNewBean ? attributeLinkedBean.isMain : ftw.isEmpty());
        ftw.add(ft);
        mainWithFt.add(ft);
    }

    protected Image addRemoveBtn(final FlexTable ft) {
        Image removeBtn = new Image("images/delete.png");
        removeBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                mainWithFt.remove(ft);
                ftw.remove(ft);
                if (ftw.isEmpty()) {
                    mainWithFt.clear();
                }
            }
        });
        return removeBtn;
    }

    protected Image addChangeBtn(final FlexTable ft) {
        Image removeBtn = new Image("images/link_arrows.png");
        removeBtn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                Widget w1 = ft.getWidget(0, 1);
                Widget w2 = ft.getWidget(0, 3);
                ft.setWidget(0, 1, w2);
                ft.setWidget(0, 3, w1);
            }
        });
        return removeBtn;
    }

    protected ListBox setAttributesListBox(String valueAttributeLinked, final FlexTable ft) {
        final ListBox lbx = new ListBox();
        int idx = 0;
        for (final String s : joinedObjAttributes.keySet()) {
            lbx.addItem(s);
            if (valueAttributeLinked != null && s.equals(valueAttributeLinked)) {
                lbx.setSelectedIndex(idx);
            }
            idx++;
        }
        lbx.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                AttributeBean ab = joinedObjAttributes.get(lbx.getValue(lbx.getSelectedIndex()));
                String typeAttr = ab.typeAttr;
                ft.setWidget(0, 1, addWidget(typeAttr, "", ab.valueOpt));
                ft.setWidget(0, 3, addWidget(typeAttr, "", isAttrTypeDefaultValue(typeAttr) ? ab.valueOptTo : ab.valueOpt));
            }
        });
        lbx.setWidth("200px");
        return lbx;
    }

    protected boolean isAttrTypeDefaultValue(String typeAttr) {
        return typeAttr.equals(AddOrEditAdminJoinedAttributeWithTypeDialog.AttributeType.definedValue.name());
    }

    protected Widget addWidget(String typeAttr, String valueAttributeLinked, String valueAttributeOpt) {

        if (typeAttr.equals(AttributeType.combobox.name())) {
            List<String> valuesToListBox = BaseUtils.splitBySep(valueAttributeOpt, ",");
            ListBox lb = new ListBox();
            int idx = 0;
            for (String v : valuesToListBox) {
                lb.addItem(v);
                if (v.equals(valueAttributeLinked)) {
                    lb.setSelectedIndex(idx);
                }
                idx++;
            }
            lb.setWidth("204px");
            return lb;
        } else if (typeAttr.equals(AttributeType.data.name())) {
            return getDateBox(valueAttributeLinked);
        } else if (typeAttr.equals(AttributeType.checkbox.name())) {
            VerticalPanel vp = new VerticalPanel();
            vp.setWidth("204px");
            List<String> valuesToCheckBoxes = BaseUtils.splitBySep(valueAttributeOpt, ",");
            List<String> valuesAttributeLinked = BaseUtils.splitBySep(valueAttributeLinked, ",");
            for (String v : valuesToCheckBoxes) {
                CheckBox checkBox = new CheckBox(v);
                vp.add(checkBox);
                if (valuesAttributeLinked.contains(v)) {
                    checkBox.setValue(true);
                }
            }

            return vp;
        } else if (typeAttr.equals(AddOrEditAdminJoinedAttributeWithTypeDialog.AttributeType.definedValue.name())) {
            Label lbl = new Label(BaseUtils.isStrEmpty(valueAttributeLinked) ? valueAttributeOpt : valueAttributeLinked);
            lbl.setWidth("204px");
            return lbl;
        }
        TextArea tb = new TextArea();
        tb.setValue(valueAttributeLinked);
        tb.setWidth("198px");
        return tb;

    }

    @Override
    protected boolean doActionBeforeClose() {

        if (withNodeId) {
            if (BaseUtils.isStrEmptyOrWhiteSpace(tbNodeId.getText())) {

                BIKClientSingletons.showWarning(I18n.nodeIdNiePuste.get() /* I18N:  */);
                return false;
            }
            if (BaseUtils.isCollectionEmpty(ftw)) {
                BIKClientSingletons.showWarning(I18n.dodajAtrybut.get() /* I18N:  */);
                return false;
            }
        }

        List<String> attrUsage = new ArrayList<String>();
        boolean isMain = false;
        for (FlexTable ft : ftw) {

            String attrName = getaAttrName(ft.getWidget(0, 0));
            if (attrUsage.contains(attrName)) {
                BIKClientSingletons.showWarning(BaseUtils.capitalize(I18n.atrybut.get()) + " " + attrName + " " + I18n.atrybutDodanyWiecejNizJedenRaz.get());
                return false;
            }
            attrUsage.add(attrName);
            if (BaseUtils.isStrEmpty(getAttrValue(ft.getWidget(0, 1)))) {
                BIKClientSingletons.showWarning(I18n.wartoscAtrybutuNieMozeBycPusta.get() /* I18N:  */ + getaAttrName(ft.getWidget(0, 0)));
                return false;
            }
            isMain = !isMain ? isMain(ft.getWidget(0, 5)) : true;
        }
        if (!ftw.isEmpty() && !isMain) {
            BIKClientSingletons.showWarning(I18n.zaznaczAtrybutGlowny.get() /* I18N:  */);
            return false;
        }

        return true;
    }

    @Override
    protected void doAction() {
        List<JoinedObjAttributeLinkedBean> saveBean = new ArrayList<JoinedObjAttributeLinkedBean>();
        for (FlexTable ft : ftw) {
            JoinedObjAttributeLinkedBean joab = new JoinedObjAttributeLinkedBean();
            joab.name = getaAttrName(ft.getWidget(0, 0));
            joab.value = getAttrValue(ft.getWidget(0, 1));
            joab.valueTo = getAttrValue(ft.getWidget(0, 3));
            joab.attributeId = joinedObjAttributes.get(joab.name).id;
            joab.isMain = isMain(ft.getWidget(0, 5));
            saveBean.add(joab);
        }
//        Integer nodeId=
        Pair<List<JoinedObjAttributeLinkedBean>, Integer> savePair = new Pair<List<JoinedObjAttributeLinkedBean>, Integer>();
        savePair.v1 = saveBean;
        if (withNodeId) {
            savePair.v2 = Integer.parseInt(tbNodeId.getValue());
        }
        saveCont.doIt(savePair);
    }

    protected String getaAttrName(Widget widget) {
        return ((ListBox) widget).getSelectedValue();
    }

    protected String getAttrValue(Widget widget) {
        if (widget instanceof TextArea) {
            return ((TextArea) widget).getValue();
        } else if (widget instanceof ListBox) {
            return ((ListBox) widget).getSelectedValue();
        } else if (widget instanceof DateBox) {
            return SimpleDateUtils.dateToString(((DateBox) widget).getValue());
        } else if (widget instanceof Label) {
            return ((Label) widget).getText();
        }
        List<String> cbs = new ArrayList<String>();
        for (int id = 0; id < ((VerticalPanel) widget).getWidgetCount(); id++) {
            CheckBox c = ((CheckBox) ((VerticalPanel) widget).getWidget(id));
            if (c.getValue()) {
                cbs.add(c.getText());
            }
        }
        return BaseUtils.mergeWithSepEx(cbs, ",");
    }

    protected boolean isMain(Widget widget) {
        return ((RadioButton) widget).getValue();
    }

    protected DateBox getDateBox(String value) {
        DateBox date = new DateBox();
        DateTimeFormat dateFormat = DateTimeFormat.getFormat("yyyy-MM-dd");
        date.setWidth("200px");
        date.setFormat(new DateBox.DefaultFormat(dateFormat));
        date.setValue(SimpleDateUtils.parseFromCanonicalString(value));
        return date;
    }

    protected HTML getHTMLWidgetWithStyle(String value) {
        HTML lbl = new HTML(value);
        lbl.setWidth("204px");
        return lbl;
    }

}
