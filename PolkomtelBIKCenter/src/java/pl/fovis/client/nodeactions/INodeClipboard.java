/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.common.TreeNodeBean;

/**
 *
 * @author wezyr
 */
public interface INodeClipboard {

    public void clear();

    public void copy(TreeNodeBean node);

    public void cut(TreeNodeBean node);

    public TreeNodeBean getNode();

    // meaningful only when getNode() != null
    public boolean isCut();
}
