exec sp_check_version '1.0.33';
go

alter table bik_joined_objs
add type int not null default 0 check (type in(0, 1));
go


exec sp_update_version '1.0.33', '1.0.34';
go