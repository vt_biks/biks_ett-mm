/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

/**
 *
 * @author pmielanczuk
 */
public enum LoggedUserOnClientVsServerStatus {
    SameUser, DifferentUser, UnloggedUser, DisabledUser

}
//    public static LoggedUserOnClientVsServerStatus decodeStatusEncodedInVersionAfterLastDotOnKeepSessionAlive(String verStr) {
//        if (verStr == null) {
//            return null;
//        }
//
//        int lastDotPos = verStr.lastIndexOf(".");
//
//        if (lastDotPos < 0) {
//            return null;
//        }
//
//        String afterLastDot = verStr.substring(lastDotPos + 1);
//        if (!BaseUtils.isStrEmptyOrWhiteSpace(afterLastDot)) {
//            return null;
//        }
//        int len = afterLastDot.length();
//
//        // number of chars (spaces) determines:
//        // 0 -> SameUser, no changes in nodes
//        // 1 -> DifferentUser, no changes in nodes
//        // 2 -> UnloggedUser, no changes in nodes
//        int loggedUserStatusOrdinal = len % 3;
//        return LoggedUserOnClientVsServerStatus.values()[loggedUserStatusOrdinal];
//    }
//    public static String encodeStatusInVersionAfterLastDotForKeepSessionAlive(String verStr,
//            LoggedUserOnClientVsServerStatus luocvss) {
//        int luocvssOrdinal = luocvss.ordinal();
//        return verStr + "." + BaseUtils.replicateChar(' ', luocvssOrdinal);
//    }
