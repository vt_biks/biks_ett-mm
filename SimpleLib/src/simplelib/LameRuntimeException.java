package simplelib;

import java.io.Serializable;

/**
 *
 * @author wezyr
 */
public class LameRuntimeException extends RuntimeException implements Serializable {

    public LameRuntimeException() {
        //ww: do celów serializacji
    }

    public LameRuntimeException(Throwable cause) {
        super(cause);
    }

    public LameRuntimeException(String errMsg) {
        super(errMsg);
    }

    public LameRuntimeException(String errMsg, Throwable cause) {
        super(errMsg, cause);

//        super(cause instanceof LameRuntimeException ? errMsg + "\nCause msg:" + ((LameRuntimeException)cause).getMessage() : errMsg,
//                //cause instanceof LameRuntimeException ? cause.getCause() :
//                cause  );
        //System.out.println("*** LameRuntimeException: " + errMsg + ", cause: " + cause);
        //System.out.println("***   getCause(): " + getCause());
        //System.out.flush();
    }
}
