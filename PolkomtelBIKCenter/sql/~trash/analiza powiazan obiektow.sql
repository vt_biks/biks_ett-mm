select chld.table_code, chld.si_kind, COUNT(distinct prnt.table_code, prnt.si_kind), MIN(prnt.si_kind), MAX(prnt.si_kind)
from aaa_global_props chld left join aaa_global_props prnt on chld.SI_PARENTID = prnt.si_id
group by chld.table_code, chld.si_kind
order by COUNT(distinct prnt.si_kind) desc



select chld.table_code, chld.si_kind, prnt.table_code, prnt.si_kind, COUNT(*)
from aaa_global_props chld left join aaa_global_props prnt on chld.SI_PARENTID = prnt.si_id
where 
  chld.si_kind in ('CrystalReport', 'Folder', 'MM.IntegratorSource', 'Webi', 'MM.Configuration')
  and prnt.si_id is not null
group by chld.table_code, chld.si_kind, prnt.table_code, prnt.si_kind
order by chld.table_code, chld.SI_KIND




select table_code, si_kind, 
  (select count(*) from (
    select distinct prnt.table_code, prnt.si_kind 
    from aaa_global_props chld inner join aaa_global_props prnt on chld.SI_PARENTID = prnt.si_id 
    where chld.SI_kind = kinds.si_kind and chld.table_code = kinds.table_code) x) as cnt
from
  (select distinct chld.table_code, chld.si_kind
  from aaa_global_props chld) as kinds
order by cnt desc

/*

info:CrystalReport
info:Folder
app:MM.IntegratorSource
info:Webi
app:MM.Configuration

*/