/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump;

import commonlib.LameUtils;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.IContinuation;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class CommandLinePumpExecutor extends BasePumpExecutor {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected IBIKPumpTaskNew pump;

    public CommandLinePumpExecutor(String urlForPublicAccess,
            MssqlConnectionConfig config, IBIKPumpTaskNew pump) {
        super(urlForPublicAccess);
        this.connConfg = config;
        this.pump = pump;
    }

    public void initAndExecute() {
        // init executor
        initExecutor();
        // let's pump!
        BeanConnectionUtils.execInAutoCommitMode(mc, new IContinuation() {
            public void doIt() {
                String source = "Undefined";
                boolean withAction = true;
                try {
                    actualLogId = pump.setConnectionAndLog(urlForPublicAccess, null, mc, adhocDao, pLogger);
                    source = pump.getSourceName();
                    withAction = pump.isActionAfterNeeded();
                    insertMemoryDiagsToLog(true, source, withAction);
                    Pair<Integer, String> pumpResult = pump.pump();
                    insertLogSizeToDB();
                    if (pumpResult.v1 == PumpConstants.LOG_LOAD_STATUS_ERROR) {
                        updateLogsWithStatus(pumpResult.v2, PumpConstants.LOG_LOAD_STATUS_ERROR);
                    } else {
                        insertLogToDB(pumpResult.v2);
                        updateMainLog(pumpResult.v2, pumpResult.v1);
                    }
                } catch (Exception e) {
                    if (!Thread.currentThread().isInterrupted() && !isCancelled) {
                        updateLogsWithStatus(e.getMessage(), PumpConstants.LOG_LOAD_STATUS_ERROR);
                    }
                    if (logger.isErrorEnabled()) {
                        logger.error("Error in data load", e);
                    }
                } finally {
                    if (!Thread.currentThread().isInterrupted() && !isCancelled) {
                        insertMemoryDiagsToLog(false, source, withAction);
                    }
                    mc.finishWorkUnit(!Thread.currentThread().isInterrupted() && !isCancelled); // zwolnienie połączenia
                }
            }
        });
    }
}
