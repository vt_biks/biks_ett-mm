exec sp_check_version '1.0.31';
go

update bik_node set node_kind_id=(select id from bik_node_kind where code='Keyword') where id in(
select bn.id from bik_node bn  join bik_tree b on bn.tree_id=b.id 
join bik_node_kind bnk on bn.node_kind_id=bnk.id
where bn.id not in(
select a.id from bik_node a join bik_tree b on a.tree_id=b.id 
join bik_metapedia c on a.id=c.node_id 
) and b.name='Tezaurus' and bnk.code='Tezaurus')

go

exec sp_update_version '1.0.31', '1.0.32';
go
