/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons;

/**
 *
 * @author wezyr
 */
public class SimpleFileUploadParam implements IFileUploadParam {

    private String fileName;
    private byte[] fileContent;
    private long fileSize;

    public SimpleFileUploadParam(String fileName, byte[] fileContent, long fileSize) {
        this.fileName = fileName;
        this.fileContent = fileContent;
        this.fileSize = fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public byte[] getFileContent() {
        return fileContent;
    }

    public long getFileSize() {
        return fileSize;
    }
}
