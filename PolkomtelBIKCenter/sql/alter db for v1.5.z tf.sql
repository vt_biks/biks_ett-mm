﻿exec sp_check_version '1.5.z';
go

-- pod fix dla mssql
if not exists(select * from sys.objects where object_id = object_id(N'aaa_mssql') and type in (N'U'))
begin
	create table aaa_mssql (
		name varchar(512) not null,
		type varchar(100) not null,
		extra_info varchar(max) null,
		definition_text varchar(max) null,
		branch_names varchar(900) not null,
		parent_branch_names varchar(max) null,
		visual_order int not null default (0)
	);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[aaa_mssql]') and name = N'idx_aaa_mssql_branch_names')
begin
	create index idx_aaa_mssql_branch_names on aaa_mssql (branch_names);
end;
go

if exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_mssql') and name = 'branch_names')
begin
	alter table bik_mssql alter column branch_names varchar(900) not null;
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_mssql]') and name = N'idx_bik_mssql_branch_names')
begin
	create index idx_bik_mssql_branch_names on bik_mssql (branch_names);
end;
go

exec sp_update_version '1.5.z', '1.5.z1';
go
