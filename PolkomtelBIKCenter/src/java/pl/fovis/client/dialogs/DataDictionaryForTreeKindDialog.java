/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.bikwidgets.TreeAndNodeKindTreeBase;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKCenterUtils.MenuNodeBeanType;
import pl.fovis.common.MenuExtender;
import pl.fovis.common.MenuExtender.MenuBottomLevel;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeBrokerForMenuNodeBean;
import pl.fovis.common.TreeKindBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.FoxyFileUpload;
import pl.fovis.foxygwtcommons.GenericTreeHelper;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author młydkowski
 */
public class DataDictionaryForTreeKindDialog extends BaseActionOrCancelDialog {

    public static class TreeUpdateBean {

        public Map<String, String> name;
        public String treeKind;
        public Integer visualOrder;
        public String parentMenuCode;
//        public String optMenuSubGroup;
        public Map<String, String> optMenuSubGroup;
        public Integer parentId;
        public String serverTreeFileName;
//        public String serverLinkFileName;
//        public int bindedTreeId;
        public Integer isAutoObjId;
        public Set<String> tmpFileName2Delete = new HashSet<String>();
    };
    final protected TreeUpdateBean res = new TreeUpdateBean();
    protected List<MenuNodeBean> menuNodes;
    protected Collection<TreeBean> trees;
    protected TreeBean treeBean;
//    protected TextBox nameText;
    protected ListBox treeKindCombo;
//    protected TextBox subMenuNameTb;
    protected HTML menuTreePosDescr;
    protected CheckBox autoObjIdChb;
    protected IParametrizedContinuation<TreeUpdateBean> saveCont;
    protected boolean isEdit;
    protected HTML errorMssg;
    protected Integer menuNodeBeanIdForTreeBean;
//    protected CheckBox hideTreeInMenuCb = new CheckBox(I18n.niePokazujWMenu.get() /* I18N:  */);
    protected CheckBox hideTreeInMenuCb = null;
    protected FlexTable grid = new FlexTable();
    protected PushButton editTreeButton;
    protected Map<String, String> nameInLang;
    protected Map<String, TextBox> langToNameTb = new HashMap<String, TextBox>();
    protected Map<String, TextBox> langToSubMenuNameTb = new HashMap<String, TextBox>();
    protected CustomTreeAndNodeKindTree tree = new CustomTreeAndNodeKindTree();
    protected Label treeFileNameLbl/*, linkFileNameLbl*/;

    @Override
    protected String getDialogCaption() {
        if (isEdit) {
            return I18n.edytuj.get() /* I18N:  */;
        } else {
            return I18n.utworzNowy.get() /* I18N:  */;
        }
    }

    @Override
    protected String getActionButtonCaption() {
        if (isEdit) {
            return I18n.zapisz.get() /* I18N:  */;
        } else {
            return I18n.utworz.get() /* I18N:  */;
        }
    }

    protected Widget makeLabelWidget(String caption) {
        Label res = new Label(caption + ":");
        res.setStyleName("dictionaryLbl");
        return res;
    }

    @Override
    protected void addAdditionalButtons(HorizontalPanel hp) {
        super.addAdditionalButtons(hp);
        if (hideTreeInMenuCb != null) {
            hp.add(hideTreeInMenuCb);
            hideTreeInMenuCb.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    updateWidgetsAfterSomeChanges();
                }
            });
        }
//        hideTreeInMenuCb.setWidth("400px");
//        hp.setWidth("300px");
    }

    private Widget createImportTreePanel() {
        HorizontalPanel hp = new HorizontalPanel();
        hp.setSpacing(6);
        hp.add(new Label(I18n.importDrzewa.get() + ": "));
        hp.add(createImportTreeButton());
        hp.add(treeFileNameLbl = new Label());
        return hp;
    }

    @Override
    protected void afterBuildAllWidgetsBeforeShow() {
        super.afterBuildAllWidgetsBeforeShow();

        //ww: extendMenuNodes wołane jest podczas buildWidgets, więc tu już
        // będzie wyliczone menuNodeBeanIdForTreeBean
        if (menuNodeBeanIdForTreeBean != null) {
            //ww: onSelectionChange na tym wywoła nam
            // updateWidgetsAfterSomeChanges()
            tree.selectRecord(menuNodeBeanIdForTreeBean);
        } else {
            updateWidgetsAfterSomeChanges();
        }
    }

    @Override
    protected void buildMainWidgets() {
        grid.setCellSpacing(4);
        int row = 0;

        KeyUpHandler textBoxKeyUpHandler = new KeyUpHandler() {
            public void onKeyUp(KeyUpEvent event) {
                updateWidgetsAfterSomeChanges();
            }
        };
        for (String lang : BIKClientSingletons.getLanguages()) {
            grid.setWidget(row, 0, makeLabelWidget(I18n.nazwa.get() /* I18N:  */ + " " + lang));
            TextBox nameTb = new TextBox();
            grid.setWidget(row, 1, nameTb);
            grid.getFlexCellFormatter().setWidth(row++, 0, "280px");
            nameTb.setStyleName("dictionaryTb");
            String name = nameInLang != null ? nameInLang.get(lang) : null;
            nameTb.setText(name != null ? name : treeBean.name);
            langToNameTb.put(lang, nameTb);
            nameTb.addKeyUpHandler(textBoxKeyUpHandler);
        }

        HorizontalPanel treeTypePanel = new HorizontalPanel();
        grid.setWidget(row, 0, makeLabelWidget(I18n.typDrzewa.get() /* I18N:  */));
        grid.setWidget(row, 1, treeTypePanel);
        grid.getFlexCellFormatter().setWidth(row++, 0, "280px");
        if (isEdit) {
            if (BIKClientSingletons.isTreeKindDynamic(treeBean.treeKind)) {
                treeTypePanel.add(treeKindCombo = createListBox());
                populateKindListbox(true, treeBean.treeKind);
            } else {
                treeTypePanel.add(new Label(BIKClientSingletons.getTreeKindCaptionsMap().get(treeBean.treeKind)));
            }
        } else {
            treeTypePanel.add(treeKindCombo = createListBox());
            populateKindListbox();
            editTreeButton = new PushButton(new Image("images/edit_gray.gif" /* I18N: no */), new ClickHandler() {
                public void onClick(ClickEvent event) {
                    final int id = BIKClientSingletons.getTreeKindBeanByCode(getListBoxSelectedTreeKind()).id;
                    BIKClientSingletons.getService().getTranslatedTreeKinds(id, new StandardAsyncCallback<Map<String, TreeKindBean>>() {
                        public void onSuccess(Map<String, TreeKindBean> result) {

                            new TreeKindDialog().buildAndShowDialog(result, id, new IParametrizedContinuation<TreeKindBean>() {
                                public void doIt(TreeKindBean result) {
                                    updateKindMaps(result);
                                    populateKindListbox();
                                    if (editTreeButton != null) {
                                        editTreeButton.setVisible(BIKClientSingletons.isTreeKindDynamic(getListBoxSelectedTreeKind()));
                                    }
                                }
                            });
                        }
                    });
                }
            });
            PushButton addTreeButton = new PushButton(new Image("images/edit_2add.png" /* I18N: no */), new ClickHandler() {
                public void onClick(ClickEvent event) {
                    new TreeKindDialog().buildAndShowDialog(null, null, new IParametrizedContinuation<TreeKindBean>() {
                        public void doIt(TreeKindBean result) {
                            updateKindMaps(result);
                            populateKindListbox();
                            editTreeButton.setVisible(BIKClientSingletons.isTreeKindDynamic(getListBoxSelectedTreeKind()));
                        }
                    });
                }
            });
            editTreeButton.setStyleName("followBtn");
            editTreeButton.setTitle(I18n.edytujZaznaczonySzablonDrzew.get() /* I18N:  */);
            addTreeButton.setStyleName("followBtn");
            addTreeButton.setTitle(I18n.dodajSzablonDrzew.get() /* I18N:  */);

            treeKindCombo.addChangeHandler(new ChangeHandler() {
                public void onChange(ChangeEvent event) {
                    editTreeButton.setVisible(BIKClientSingletons.isTreeKindDynamic(getListBoxSelectedTreeKind()));
                }
            });
            editTreeButton.setVisible(BIKClientSingletons.isTreeKindDynamic(getListBoxSelectedTreeKind()));
            treeTypePanel.add(addTreeButton);
            treeTypePanel.add(editTreeButton);
        }

        Widget treeWidget = tree.buildWidgets();

        treeWidget.setSize("100%", "100%");
        ScrollPanel scrollPnl = new ScrollPanel(treeWidget);
        scrollPnl.setSize("535px", "300px");

        grid.setWidget(row, 0, makeLabelWidget(I18n.umiejscowenieWMenu.get() /* I18N:  */));
        grid.getFlexCellFormatter().setColSpan(row++, 0, 2);
        grid.setWidget(row, 0, scrollPnl);
        grid.getFlexCellFormatter().setColSpan(row++, 0, 2);
        for (String lang : BIKClientSingletons.getLanguages()) {
            grid.setWidget(row, 0, makeLabelWidget(I18n.opcjonalnaPodgrupaMenu.get() /* I18N:  */ + " " + lang));
            TextBox subMenuName = new TextBox();
            grid.setWidget(row++, 1, subMenuName);
            langToSubMenuNameTb.put(lang, subMenuName);
            subMenuName.setStyleName("dictionaryTb");
            subMenuName.addKeyUpHandler(textBoxKeyUpHandler);
//            subMenuNameTb = new TextBox();
//            grid.setWidget(row++, 1, subMenuNameTb = new TextBox());
        }
//        subMenuNameTb.setStyleName("dictionaryTb");
//        subMenuNameTb.addKeyUpHandler(textBoxKeyUpHandler);

        grid.setWidget(row, 0, menuTreePosDescr = new HTML());
        grid.getFlexCellFormatter().setColSpan(row++, 0, 2);
        grid.setWidget(row, 0, autoObjIdChb = new CheckBox(I18n.automatyczieWyliczycIdObietowPrzyImporcie.get()));
        autoObjIdChb.setValue(treeBean.isAutoObjId > 0);
        grid.getFlexCellFormatter().setColSpan(row++, 0, 2);
//        grid.setWidget(row, 0, createImportTreePanel());
//        grid.getFlexCellFormatter().setColSpan(row++, 0, 2);
//        if (BIKClientSingletons.isEnableFullImportExportTree()) {
//            grid.setWidget(row, 0, createImportLinkedNodesPanel());
//            grid.getFlexCellFormatter().setColSpan(row++, 0, 2);
//        }
        main.add(errorMssg = new HTML());
        errorMssg.setStyleName("status" /* I18N: no */);
        main.add(grid);
    }

    private PushButton createImportTreeButton() {
        PushButton btn = new PushButton(I18n.importuj.get());
        NewLookUtils.makeCustomPushButton(btn);
        btn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new FileUploadDialogSimple().buildAndShowDialog(new IParametrizedContinuation<FoxyFileUpload>() {

                    @Override
                    public void doIt(final FoxyFileUpload ffu) {
                        res.serverTreeFileName = ffu.getServerFileName();
                        treeFileNameLbl.setText(ffu.getClientFileName());
                        res.tmpFileName2Delete.add(res.serverTreeFileName);
                    }
                }, Arrays.asList(BIKClientSingletons.allowedFileExtensionsList()));
            }
        });

        return btn;
    }

    protected void populateKindListbox(boolean onlyDynamic, String selectRecord) {
        treeKindCombo.clear();
        for (Entry<String, String> e : BIKClientSingletons.getTreeKindCaptionsMap().entrySet()) {
            if (!onlyDynamic || BIKClientSingletons.isTreeKindDynamic(e.getKey())) {
                treeKindCombo.addItem(e.getValue(), e.getKey());
            }
            if (selectRecord != null && e.getKey().equals(selectRecord)) {
                treeKindCombo.setSelectedIndex(treeKindCombo.getItemCount() - 1);
            }
        }
    }

    protected void populateKindListbox() {
        populateKindListbox(false, null);
    }

    protected ListBox createListBox() {
        ListBox lb = new ListBox();
        lb.setVisibleItemCount(1);
        lb.setSelectedIndex(0);
//        lb.setStyleName("dictionaryTb");
        lb.setWidth("186px");
        if (isEdit) {
            lb.setEnabled(false);
        }
        return lb;
    }

    protected boolean getHideTreeInMenu() {
        return hideTreeInMenuCb != null && hideTreeInMenuCb.getValue();
    }

    @Override
    protected boolean doActionBeforeClose() {

        String errMsg = getOptErrorMsg();
        if (errMsg != null) {
            errorMssg.setHTML(errMsg);
            return false;
        }

//        String newCaption = nameText.getText();
//        int treeKindIdx = isEdit ? 0 : treeKindCombo.getSelectedIndex();
        res.isAutoObjId = autoObjIdChb.getValue() ? 1 : 0;
        res.name = new HashMap<String, String>();
        for (Entry<String, TextBox> tkb : langToNameTb.entrySet()) {
            res.name.put(tkb.getKey(), tkb.getValue().getText());
        }
        if (!isEdit) {
            res.treeKind = getListBoxSelectedTreeKind();
        } else {
            boolean treeKindDynamic = BIKClientSingletons.isTreeKindDynamic(treeBean.treeKind);
            if (treeKindDynamic) {
                res.treeKind = getListBoxSelectedTreeKind();
            } else {
                res.treeKind = treeBean.treeKind;
            }
        }

        if (!getHideTreeInMenu()) {
            Integer selectedId = tree.getIdOfSelectedRecord();
            if (selectedId != null) {
                MenuNodeBean mnb = tree.getMenuNodeBeanById(selectedId);
                Integer parentId = mnb.parentNodeId;
                res.parentId = parentId;

                if (parentId != null) {
                    mnb = tree.getMenuNodeBeanById(parentId);
                } else {
                    mnb = null;
                }

                res.parentMenuCode = mnb == null ? "" : mnb.code;
                res.visualOrder = getDemandedVisualOrder(selectedId);
                res.optMenuSubGroup = null;
                if (getReversedMenuPathAndOptSubGroupFlag().v2) {
                    res.optMenuSubGroup = new HashMap<String, String>();
                    for (Entry<String, TextBox> t : langToSubMenuNameTb.entrySet()) {
                        String txt = t.getValue().getText();
                        if (!BaseUtils.isStrEmptyOrWhiteSpace(txt)) {
                            res.optMenuSubGroup.put(t.getKey(), txt);
                        }
                    }
                }
//                res.optMenuSubGroup = getReversedMenuPathAndOptSubGroupFlag().v2 ? subMenuNameTb.getText() : null;
            }
        }
//            Window.alert("isEdit: " + isEdit + ", parentMenu:" + treeBean.parentMenuCode + ", visualOrder: "
//                    + treeBean.visualOrder);

        BIKClientSingletons.getService().checkTreeAttributesCompatibilitiy(res.treeKind, res.serverTreeFileName, new StandardAsyncCallback<Boolean>() {

            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    saveCont.doIt(res);
                } else {
                    showImportTreeHelp();
                }
            }
        });
        return true;
    }

    protected void showImportTreeHelp() {
    }

    @Override
    protected boolean doCancelBeforeClose() {
        callService2DeleteTmpFiles();
        return true;
    }

    @Override
    protected void doAction() {
        //no-op
    }

    public void buildAndShowDialog(TreeBean treeBean,
            List<MenuNodeBean> menuNodes,
            Collection<TreeBean> trees, Map<String, String> nameInLang,
            IParametrizedContinuation<TreeUpdateBean> saveCont) {

        this.menuNodes = menuNodes;
        this.trees = trees;
        this.saveCont = saveCont;
        isEdit = treeBean != null;
        this.treeBean = isEdit ? treeBean : new TreeBean();
        this.nameInLang = nameInLang;
        super.buildAndShowDialog();
    }

    protected Pair<List<String>, Boolean> getReversedMenuPathAndOptSubGroupFlag() {

        if (getHideTreeInMenu()) {
            return new Pair<List<String>, Boolean>(new ArrayList<String>(), false);
        }

        Integer selectedMenuId = tree.getIdOfSelectedRecord();

        Integer menuId = tree.getParentIdOfNode(selectedMenuId);
        List<String> names = new ArrayList<String>();

        while (menuId != null) {
            names.add(tree.getMenuNodeBeanById(menuId).name);
            menuId = tree.getParentIdOfNode(menuId);
        }

        int lvl = names.size();

        boolean canAddSubMenu = lvl < 2;

        return new Pair<List<String>, Boolean>(names, canAddSubMenu);
    }

    //ww: zwraca opcjonalny tekst błędu, czyli jak wszystko OK, to zwróci NULL
    protected String updateMenuTreePosDescr() {
        String optErrMsg = getOptErrorMsg();

        String descr;

        if (optErrMsg != null || getHideTreeInMenu()) {
            descr = optErrMsg;
        } else {
//            Integer selectedMenuId = tree.getIdOfSelectedRecord();
//
//            Integer menuId = tree.getParentIdOfNode(selectedMenuId);
//            List<String> names = new ArrayList<String>();
//
//            while (menuId != null) {
//                names.add(tree.getMenuNodeBeanById(menuId).name);
//                menuId = tree.getParentIdOfNode(menuId);
//            }

            Pair<List<String>, Boolean> p = getReversedMenuPathAndOptSubGroupFlag();
            List<String> names = p.v1;
            boolean canAddSubMenu = p.v2;

            Collections.reverse(names);

            int lvl = names.size();

            int posInMenu = tree.getSelectedRecordIndexInSiblings() + 1;

//            boolean canAddSubMenu = lvl < 2;
            for (TextBox tb : langToSubMenuNameTb.values()) {
                tb.setEnabled(canAddSubMenu);
            }
//            subMenuNameTb.setEnabled(canAddSubMenu);
            if (canAddSubMenu) {
//                String subGroupOptName = subMenuNameTb.getText();
                String subGroupOptName = langToSubMenuNameTb.get(BIKGWTConstants.LANG_PL).getText();
                if (lvl < 3 && !BaseUtils.isStrEmptyOrWhiteSpace(subGroupOptName)) {
                    names.add(subGroupOptName);
                    lvl++;
                    posInMenu = 1;
                }
            }

            String posDescr = names.isEmpty() ? "(" + I18n.glowneMenu.get() /* I18N:  */ + ")"
                    : BaseUtils.mergeWithSepEx(names, BIKGWTConstants.RAQUO_STR_SPACED);

            descr = I18n.drzewo.get() /* I18N:  */ + " <b>" + BaseUtils.encodeForHTMLTag(langToNameTb.get(BIKGWTConstants.LANG_PL).getText())
                    + "</b> " + I18n.pojawiSieW.get() /* I18N:  */ + ": <b>" + BaseUtils.encodeForHTMLTag(posDescr) + "</b> (" + I18n.poziom.get() /* I18N:  */ + " "
                    + (lvl + 1) + "), " + I18n.naPozycji.get() /* I18N:  */ + ": " + posInMenu;
        }

        if (menuTreePosDescr != null) {
            menuTreePosDescr.setHTML(descr);
        }

        return optErrMsg;
    }

    protected String getOptErrorMsg() {
        if (!isEdit && treeKindCombo.getSelectedIndex() < 0) {
            return I18n.wybierzTypDrzewa.get() /* I18N:  */;
        }
        if (!getHideTreeInMenu() && tree.getIdOfSelectedRecord() == null) {
            return I18n.wybierzPozycjeWMenuWJakiejUmiesc.get() /* I18N:  */;
        }
        for (TextBox nameText : langToNameTb.values()) {
            if (BaseUtils.isStrEmptyOrWhiteSpace(nameText.getText())) {
                return I18n.nazwaDrzewaNieMozeBycPusta.get() /* I18N:  */;
            }
        }
        return null;
    }

    protected void updateKindMaps(TreeKindBean result) {
        BIKClientSingletons.updateTreeKindMaps(result);
    }

    protected String getListBoxSelectedTreeKind() {
        return treeKindCombo.getValue(treeKindCombo.getSelectedIndex());
    }

    protected void updateWidgetsAfterSomeChanges() {
        String optErrMsg = updateMenuTreePosDescr();

        boolean menuPosRowsVisible = !getHideTreeInMenu();

        //ww: wiersze o indeksach 2..5 dotyczą pozycji w menu
        for (int row = 2; row <= 5; row++) {
            grid.getRowFormatter().setVisible(row, menuPosRowsVisible);
        }

        actionBtn.setEnabled(optErrMsg == null);
    }

    private Integer getDemandedVisualOrder(Integer nodeId) {
        Map<String, Object> row = tree.getStorage().getRowById(nodeId);
        if (row == null) {
            return null;
        }
        return tree.getVisualOrderOfRow(row);
    }

    private class CustomTreeAndNodeKindTree extends TreeAndNodeKindTreeBase {

        @Override
        protected List<MenuNodeBean> getMenuNodes() {
            return menuNodes;
        }

        @Override
        protected Collection<TreeBean> getTrees() {
            return trees;
        }

        protected int extendForParent(GenericTreeHelper<MenuNodeBean, Integer> helper, MenuNodeBean parent, int lvl,
                List<MenuNodeBean> res, int nextNodeId) {

            MenuNodeBeanType type = BIKCenterUtils.getMenuNodeBeanType(parent);

            if (type == MenuNodeBeanType.Group || type == MenuNodeBeanType.TreeTemplate) {

                List<MenuNodeBean> children = helper.getChildNodes(parent);

                int cnt = 1;

                boolean isLastInChildren = false;

                if (children != null) {
                    for (MenuNodeBean mnb : children) {
                        isLastInChildren = isThisMenuItemForTreeBean(mnb);

                        int thisChildCnt = extendForParent(helper, mnb, lvl + 1, res, nextNodeId);
                        cnt += thisChildCnt;
                        nextNodeId += thisChildCnt;
                    }
                }

                if (!isLastInChildren) {
                    res.add(new MenuNodeBean(nextNodeId, "<" /* i18n: no */ + I18n.dodajTutaj.get() /* I18N:  */ + ">",
                            parent == null ? null : parent.id, "placeHolder" + nextNodeId));
                    nextNodeId++;
                }

                return cnt;
            }

            return 0;
        }

        protected boolean isThisMenuItemForTreeBean(MenuNodeBean mnb) {
            return isEdit && BaseUtils.safeEquals(mnb.code, "$" + treeBean.code);
        }

        @Override
        protected List<MenuNodeBean> extendMenuNodes() {
            List<MenuNodeBean> res = super.extendMenuNodes();

            int nextNewId = MenuExtender.getNextNewId(res);

            GenericTreeHelper<MenuNodeBean, Integer> helper = new GenericTreeHelper<MenuNodeBean, Integer>(new TreeBrokerForMenuNodeBean(), res);
            extendForParent(helper, null, 0, res, nextNewId);

            menuNodeBeanIdForTreeBean = null;

            for (MenuNodeBean mnb : res) {
                if (isThisMenuItemForTreeBean(mnb)) {
                    menuNodeBeanIdForTreeBean = mnb.id;
                    break;
                }
            }

            return res;
        }

        @Override
        protected boolean mustAddUltimateRootToMenuTree() {
            return false;
        }

        @Override
        protected MenuBottomLevel whatToKeepAsLeavesInMenuTree() {
            return MenuBottomLevel.BiksPagesOrGroups;
        }

        @Override
        protected boolean isExpandAllNodes() {
            return true;
        }

        @Override
        protected Map<String, Object> convertBeanToRow(MenuNodeBean mnb) {
            HashMap<String, Object> hm = new HashMap<String, Object>();
            hm.put("id" /* I18N: no */, mnb.id);
            hm.put("parentId", mnb.parentNodeId);
            hm.put("name" /* I18N: no */, mnb.name);
            hm.put("icon" /* I18N: no */, null);
            hm.put("visualOrder"/* I18N: no */, mnb.visualOrder);

            return hm;
        }

        @SuppressWarnings("unchecked")
        public Integer getVisualOrderOfRow(Map<String, Object> row) {
            return (Integer) row.get("visualOrder");
        }

        @Override
        protected void selectionChangedWholeLine(Map<String, Object> param) {
            updateWidgetsAfterSomeChanges();
        }

        public ITreeRowAndBeanStorage<Integer, Map<String, Object>, MenuNodeBean> getStorage() {
            return storage;
        }
    };

    private void callService2DeleteTmpFiles() {
        BIKClientSingletons.callService2DeleteTmpFiles(res.tmpFileName2Delete);
    }
}
