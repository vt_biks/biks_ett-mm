/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BOXIServiceAsync;
import pl.fovis.client.bikpages.AbstractBIKTreeWidget;
import pl.fovis.client.bikpages.BikTreeWidget;
import pl.fovis.client.bikpages.IPageDataFetchBroker;
import pl.fovis.client.bikpages.PageDataFetchBroker;
import pl.fovis.client.bikpages.TreeDataFetchAsyncCallback;
import pl.fovis.client.dialogs.AddEmptyAttributeDialog;
import pl.fovis.client.dialogs.AddOrEditAttributeDialog;
import pl.fovis.client.dialogs.AddOrEditJoinedAttributeLinkedDialog;
import pl.fovis.client.dialogs.AutomaticUpdateJoinedObjsForNodeDialog;
import pl.fovis.client.dialogs.IJoinedObjSelectionManager;
import pl.fovis.client.treeandlist.IBean4TreeExtractor;
import pl.fovis.client.treeandlist.TreeBean4TreeExtractor;
import pl.fovis.client.treeandlist.TreeNodeBeanAsMapStorage;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.JoinedObjAttributeLinkedBean;
import pl.fovis.common.JoinedObjBasicGenericInfoBean;
import pl.fovis.common.JoinedObjMode;
import pl.fovis.common.MenuExtender;
import pl.fovis.common.MenuNodeBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.TreeNodeBranchBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.popupmenu.PopupMenuDisplayer;
import pl.fovis.foxygwtcommons.treeandlist.ITreeGridCellWidgetBuilder;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.LameBag;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class TreeSelectorForAutomaticFillAttributeWidget {

    protected FlexTable attributeFp;
    protected Set<String> treeCodes;
    protected VerticalPanel main = new VerticalPanel();
//    protected int row;
    protected List<AttributeBean> abs = new ArrayList<AttributeBean>();
    protected IParametrizedContinuation<Pair<Set<TreeNodeBranchBean>, List<AttributeBean>>> saveRestrictionsCont;
//    protected IParametrizedContinuation<Set<TreeNodeBranchBean>> saveRestrictionsCont;
    protected String roleCaption;
    protected Set<TreeNodeBranchBean> selectedNodes = new HashSet<TreeNodeBranchBean>();
    protected Map<TreeNodeBranchBean, CheckBox> nodeToCheckBoxMap = new HashMap<TreeNodeBranchBean, CheckBox>();
    protected LameBag<TreeNodeBranchBean> selectedDescendantCounts = new LameBag<TreeNodeBranchBean>();
    protected Map<TreeNodeBranchBean, List<TreeNodeBranchBean>> childNodeMap = new HashMap<TreeNodeBranchBean, List<TreeNodeBranchBean>>();
    protected PushButton actionBtn;

    private static final String EDITING_MANY_JOINED_OBJS_HINT_NAME = "EditnigJoinedObj";
    protected boolean useLazyLoading = true;//false; //
    protected IParametrizedContinuation<TreeNodeBean> saveCont;
    protected AbstractBIKTreeWidget<TreeNodeBean, Integer> bikTree;
    protected TreeNodeBean selectedNode = null;
    //ww:mltx 2013.09.26 - kasuję to kuriozum, WTF, po co to było?
    //    protected TreeNodeBean lastSelectedNode = null;
    protected TextBox filterBox;
    protected BikItemButtonBig filterButton;
    private final CheckBox cbFilterInSubTree = new CheckBox();
    protected Integer currentFilterInSubTreeNodeId = null;
//    protected String currentFilterInSubTreeNamePath;
    protected String currentFilterVal = "";
    protected CheckBox inheritToDescendantsCb;
    protected Integer nodeId;
    //protected String optNodeNamePathStr;
    protected Label headLbl;// = new Label(ADDING_ONE_JOINED_OBJ_FOR_MSG + "...");

    protected IJoinedObjSelectionManager<Integer> alreadyJoinedManager;
    protected Map<Integer, JoinedObjMode> directOverride = new HashMap<Integer, JoinedObjMode>();
    // Boolean == true -> all descendants, false -> children only
    protected Map<Integer, Pair<JoinedObjMode, Boolean>> subNodesOverride = new HashMap<Integer, Pair<JoinedObjMode, Boolean>>();
    protected Map<Integer, InlineHTML> htmlForJOMImg = new HashMap<Integer, InlineHTML>();
    protected Map<Integer, InlineLabel> lblForJOM = new HashMap<Integer, InlineLabel>();
    protected Set<Integer> ancestorIdsOfJoinedObjs = new HashSet<Integer>();
    protected boolean isInFilterBox;
    protected Set<Integer> alreadyJoinedObjIds;

    protected IBean4TreeExtractor<TreeNodeBean, Integer> beanExtractor = null;

    protected TreeNodeBranchBean getParentNode(TreeNodeBranchBean node) {
        if (node.nodeId == null) {
            return null;
        }

        List<String> s = BaseUtils.splitBySep(node.branchIds, "|", true);

        final int nodeIdIdx = s.size() - 2;

        // zabiezpieczenie przed dziwna akcją typu pusty string, typu "123", "123|"
        // dopiero "123|456|" będzie OK, bo daje 3 elementy
        if (nodeIdIdx < 0) {
            return null;
        }

        if (nodeIdIdx == 0) {
            // czyli było "123|" -> parentem jest samo drzewo
            return new TreeNodeBranchBean(node.treeId, null, null);
        }

        // tutaj mamy przynajmniej "123|456|" - 3 elementy
        Integer parentId = BaseUtils.tryParseInteger(s.get(nodeIdIdx - 1), -1);
        s.remove(nodeIdIdx);

        return new TreeNodeBranchBean(node.treeId, parentId, BaseUtils.mergeWithSepEx(s, "|"));
    }

    protected void addOrRemoveNodeToCnts(TreeNodeBranchBean node, boolean doAdd) {
        while (node != null) {
            if (doAdd) {
                selectedDescendantCounts.add(node);
            } else {
                selectedDescendantCounts.remove(node);
            }

            CheckBox cb = nodeToCheckBoxMap.get(node);
            setStyleForCheckBox(cb, node);

            node = getParentNode(node);
        }
    }
//
//    protected final void setupSelectedDescendantCounts() {
//        for (TreeNodeBranchBean n : selectedNodes) {
//            addOrRemoveNodeToCnts(n, true);
//        }
//    }

    // zawsze na końcu niepustego branchIds jest "|"
    // np. 1234|
    //     34561|1111|
    protected String getProperBranchIds(String branchIds) {
        List<String> s = BaseUtils.splitBySep(branchIds, "|", true);

        // s.size() - 1, bo zawsze jest pałka na końcu
        int i = 0;
        final int properItemCnt = s.size() - 1;
        for (; i < properItemCnt; i++) {
            if (!BaseUtils.strHasPrefix(s.get(i), "-", false)) {
                break;
            }
        }

        if (i >= properItemCnt) {
            return null;
        }

        // to nam dorzuci pałkę na końcu
        return BaseUtils.mergeWithSepEx(s.subList(i, s.size()), "|");
    }

    protected boolean isInMultiSelectMode() {
        return true;
    }

    protected void addPopupMenuInMultiSelectMode() {
        // no-op
    }

    protected boolean generateNodesForTreeCategories() {
        return true;
    }

    protected boolean isHeaderVisible() {
        return false;
    }

    protected Set<String> getOptAllTreeCodesToShow() {
        return treeCodes;
    }

    protected boolean isGrouppingByMenuEnabled() {
        return true;
    }

    protected void setStyleForCheckBox(CheckBox cb, TreeNodeBranchBean node) {
//        if (cb != null) {
//            GWTUtils.addOrRemoveStyleName(cb, "node-has-selected-descendants", cb.isEnabled() && selectedDescendantCounts.getCount(node) > 0);
//        }
    }

    protected void setEnabledForDescendants(TreeNodeBranchBean node, boolean isEnabled) {
        List<TreeNodeBranchBean> childNodes = childNodeMap.get(node);
        if (childNodes == null) {
            return;
        }

        for (TreeNodeBranchBean childNode : childNodes) {
            CheckBox childCb = nodeToCheckBoxMap.get(childNode);
            if (childCb != null) {
//                childCb.setEnabled(isEnabled);
                setStyleForCheckBox(childCb, childNode);
            }

            setEnabledForDescendants(childNode, isEnabled);
        }
    }

    protected Widget createWidgetForNameColumn(final TreeNodeBean node) {
        Integer nId = node.id;
        int treeId = node.treeId;

        //showCheckboxForTreeCategories
        if (treeId == -1) {
            return new InlineLabel(node.name);
        }

        if (treeId < 0) {
            treeId = -treeId;
            nId = null;
            return new InlineLabel(node.name);
        }

        final TreeNodeBranchBean currNode = new TreeNodeBranchBean(treeId, nId, getProperBranchIds(node.branchIds), node.nodeKindId);
//        final TreeNodeBranchBean selectedNodePair = new TreeNodeBranchBean(treeId, nId, node.branchIds);

        final CheckBox cb = new CheckBox(node.name);
        nodeToCheckBoxMap.put(currNode, cb);
        TreeNodeBranchBean parentNode = getParentNode(currNode);
        BaseUtils.addToCollectingMapWithList(childNodeMap, parentNode, currNode);

//        cb.setEnabled(!isAncestorNodeSelected(currNode));
        cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                final boolean doAdd = event.getValue();
                // działaj tylko, gdy faktycznie nastąpiła zmiana wartości checkboxa
                // w stosunku do zapamiętanego stanu
                if (selectedNodes.contains(currNode) != doAdd) {
                    BaseUtils.setAddOrRemoveItem(selectedNodes, currNode, doAdd);
                    addOrRemoveNodeToCnts(currNode, doAdd);
                    setEnabledForDescendants(currNode, !doAdd);
                }
            }
        });

        cb.setValue(selectedNodes.contains(currNode), false);
        setStyleForCheckBox(cb, currNode);

        return cb;
    }

    protected Widget buildOptionalRightSide() {

        VerticalPanel rightVp = new VerticalPanel();
        rightVp.setWidth("100%");
        rightVp.setHeight("100%");
//        rightVp.setStyleName("EntityDetailsPane selectRoleInDialogPane");

        Label objectAttrs = new Label(I18n.atrybutyObiektow.get() /* I18N:  */ + ":");
        objectAttrs.setStyleName("AttrHeadLbl");
        objectAttrs.addStyleName("rightTreeLbl");
        rightVp.add(objectAttrs);
        rightVp.add(addOrEditAttributeBtn(true, I18n.dodajAtrybut.get()));
        rightVp.add(addOrEditAttributeBtn(false, I18n.edytujAtrybut.get()));
        rightVp.add(addOrEditAttributeBtn(true, I18n.dodajPustyAtrybut.get(), true));
        rightVp.add(addOrEditAttributeBtn(false, I18n.edytujNaPustyAtrybut.get(), true));
        rightVp.add(deleteAttributeBtn());
        Label joinedObjs = new Label(I18n.powiazania.get() /* I18N:  */ + ":");
        joinedObjs.setStyleName("AttrHeadLbl");
        joinedObjs.addStyleName("rightTreeLbl");
        rightVp.add(joinedObjs);
        rightVp.add(addJoinedObjsBtn());
        Label joinedObjsAttr = new Label(I18n.atrybutyPowiazan.get() /* I18N:  */ + ":");
        joinedObjsAttr.setStyleName("AttrHeadLbl");
        joinedObjsAttr.addStyleName("rightTreeLbl");
        rightVp.add(joinedObjsAttr);
        rightVp.add(addAttributToJoinedObjsBtn());
        attributeFp = new FlexTable();
        attributeFp.setWidth("100%");
        attributeFp.setHeight("800%");
        rightVp.add(attributeFp);

        return rightVp;
    }

    protected PushButton addOrEditAttributeBtn(final boolean addAttribute, final String caption) {
        return addOrEditAttributeBtn(addAttribute, caption, false);
    }

    protected PushButton addOrEditAttributeBtn(final boolean addAttribute, final String caption, final boolean addEmptyAttribute) {

        PushButton b = new PushButton(caption);
        NewLookUtils.makeCustomPushButton(b);

        b.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (selectedNodes.isEmpty()) {
                    Window.alert(I18n.zaznaczWezel.get());
                } else {

                    BIKClientSingletons.getService().getBikAttributesNameAndType(null, null, BIKConstants.HYPERLINK, null, null, new StandardAsyncCallback<Pair<Map<String, AttributeBean>, Map<String, List<TreeNodeBean>>>>(I18n.brakZdefinioAtrybutoDlaDanegoTyp.get() /* I18N:  */) {

                        public void onSuccess(Pair<Map<String, AttributeBean>, Map<String, List<TreeNodeBean>>> result) {

                            if (!addEmptyAttribute) {
                                new AddOrEditAttributeDialog(null, false).buildAndShowDialog(result.v1, new HashMap<String, String>(), null, null,
                                        caption, new IParametrizedContinuation<AttributeBean>() {

                                            public void doIt(final AttributeBean param) {
//                                                BIKClientSingletons.getService().automaticFillAttributesForNode(selectedNodes, param, addAttribute, new StandardAsyncCallback<Void>() {
////
////
//                                                    public void onSuccess(Void result) {
//                                                        /* Dodawanie atrybutów */
//                                                        BIKClientSingletons.showInfo(addAttribute ? I18n.dodanoAtrybut.get() : I18n.edytowanoAtrybut.get() /* I18N:  */ + ".");
//                                                    }
//                                                });
                                                automaticFillAttributesForNode(addAttribute, param);
                                            }
                                        });
                            } else {
                                new AddEmptyAttributeDialog().buildAndShowDialog(caption, result.v1, new IParametrizedContinuation<AttributeBean>() {

                                    @Override
                                    public void doIt(AttributeBean param) {
                                        automaticFillAttributesForNode(addAttribute, param);
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
        return b;
    }

    protected void automaticFillAttributesForNode(final boolean addAttribute, AttributeBean param) {
        BIKClientSingletons.getService().automaticFillAttributesForNode(selectedNodes, param, addAttribute, new StandardAsyncCallback<Void>() {
//
//
            public void onSuccess(Void result) {
                /* Dodawanie atrybutów */
                BIKClientSingletons.showInfo(addAttribute ? I18n.dodanoAtrybut.get() : I18n.edytowanoAtrybut.get() /* I18N:  */ + ".");
            }
        });
    }

    protected PushButton deleteAttributeBtn() {

        PushButton b = new PushButton(I18n.usunAtrybut.get());
        NewLookUtils.makeCustomPushButton(b);
        b.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {

                if (selectedNodes.isEmpty()) {
                    Window.alert(I18n.zaznaczWezel.get());
                } else {
                    BIKClientSingletons.getService().getBikAttributesNameAndType(null, null, BIKConstants.HYPERLINK, null, null, new StandardAsyncCallback<Pair<Map<String, AttributeBean>, Map<String, List<TreeNodeBean>>>>() {

                        @Override
                        public void onSuccess(Pair<Map<String, AttributeBean>, Map<String, List<TreeNodeBean>>> result) {
                            new AddOrEditAttributeDialog().buildAndShowDialog(result.v1, new HashMap<String, String>(), null, null, I18n.usunAtrybut.get(), new IParametrizedContinuation<AttributeBean>() {

                                public void doIt(final AttributeBean param) {
                                    BIKClientSingletons.getService().automaticDeleteAttributesForNode(selectedNodes, param, new StandardAsyncCallback<Void>() {
//
//
                                        public void onSuccess(Void result) {
                                            /* Dodawanie atrybutów */
                                            BIKClientSingletons.showInfo(I18n.usunietoAtrybut.get() /* I18N:  */ + ".");
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
        return b;
    }

    protected PushButton addJoinedObjsBtn() {

        PushButton b = new PushButton(I18n.dodajPowiazanie.get() /* I18N:  */);
        NewLookUtils.makeCustomPushButton(b);
        b.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (selectedNodes.isEmpty()) {
                    Window.alert(I18n.zaznaczWezel.get());
                } else {
                    new AutomaticUpdateJoinedObjsForNodeDialog().buildAndShowDialog(new IParametrizedContinuation<Integer>() {

                        public void doIt(Integer param) {
                            BIKClientSingletons.getService().automaticUpdateJoinedObjsForNodeInner(param, selectedNodes, new StandardAsyncCallback<Void>() {

                                public void onSuccess(Void result) {

                                    BIKClientSingletons.showInfo(I18n.dodanoPowiazanie.get() /* I18N:  */ + ".");
                                }
                            });
                        }
                    });
                }
            }
        });
        return b;
    }

    protected PushButton addAttributToJoinedObjsBtn() {

        PushButton b = new PushButton(I18n.dodajAtrybutyDoPowiazan.get() /* I18N:  */);
        NewLookUtils.makeCustomPushButton(b);
        b.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (selectedNodes.isEmpty()) {
                    Window.alert(I18n.zaznaczWezel.get());
                } else {
                    BIKClientSingletons.getService().getJoinedAttributesLinkedAndoinedObjAttributes(1, new StandardAsyncCallback<Pair<List<JoinedObjAttributeLinkedBean>, Map<String, AttributeBean>>>() {

                        public void onSuccess(Pair<List<JoinedObjAttributeLinkedBean>, Map<String, AttributeBean>> result) {
                            new AddOrEditJoinedAttributeLinkedDialog().buildAndShowDialog(result, true, new IParametrizedContinuation<Pair<List<JoinedObjAttributeLinkedBean>, Integer>>() {

                                public void doIt(final Pair<List<JoinedObjAttributeLinkedBean>, Integer> param) {
                                    BIKClientSingletons.getService().automatiAddJoinedAttributeLinked(param.v2, selectedNodes, param.v1,
                                            new StandardAsyncCallback<Void>() {

                                                public void onSuccess(Void result) {
                                                    BIKClientSingletons.showInfo(I18n.dodanoAtrybutDoPowiazan.get() /* I18N:  */ + ".");
                                                }
                                            });

                                }
                            });
                        }
                    });
                }
            }
        });
        return b;
    }

    protected VerticalPanel buildMainWidgets() {
        //if (optNodeNamePathStr != null) {
        String headLblCaption = getHeadLabelInitialText();
        if (headLblCaption != null) {
            headLbl = new Label(headLblCaption);
        } else {
            headLbl = new Label();
        }
        Widget headLblWidget;

        if (!isNodesIdFromExternalData() && isInMultiSelectMode() && BIKClientSingletons.helpHasHint(EDITING_MANY_JOINED_OBJS_HINT_NAME)) {
            HorizontalPanel headHp = new HorizontalPanel();
            HorizontalPanel headHpLblOpt = new HorizontalPanel();
            headHp.setWidth("100%");
            headHpLblOpt.add(headLbl);
            headHp.add(headHpLblOpt);
            Widget optionalWidget = setOptionalWigetAfterHeadLabel();
            if (optionalWidget != null) {
                headHpLblOpt.add(optionalWidget);
                GWTUtils.setStyleAttribute(headLbl, "padding" /* I18N: no */, "0px 10px 0px 0");
            }

            //headHp.setCellWidth(hintBtn, ZINDEX_OVER_GLASS)
            Widget hintBtn = BIKClientSingletons.createHelpHintButton(EDITING_MANY_JOINED_OBJS_HINT_NAME, I18n.legendaEdycjiPowiazan.get());
            headHp.add(hintBtn);
            headHp.setCellHorizontalAlignment(hintBtn, HasHorizontalAlignment.ALIGN_RIGHT);
            //headHp.setCellWidth(hintBtn, ZINDEX_OVER_GLASS)
            headLblWidget = headHp;
        } else {
            headLblWidget = headLbl;
        }

        if (isHeaderVisible()) {
            main.add(headLblWidget); //main.add(headLbl);
        }
        headLblWidget.setStyleName("AttrHeadLbl treeSelectorHeadLbl");
        //}

        Widget optRightSideWidget = buildOptionalRightSide();

        Panel mainBodyContainer;

        if (optRightSideWidget != null) {
            VerticalPanel leftVp = new VerticalPanel();
            leftVp.setStyleName("EntityDetailsPane");

            //leftVp.setWidth("600px");//main.setWidth("600px");
            //leftVp.setHeight("300px");//main.setHeight("300px");
            HorizontalPanel hp = new HorizontalPanel();
            hp.add(leftVp);
            hp.add(optRightSideWidget);
            main.add(hp);
            mainBodyContainer = leftVp;
        } else {
            //main.setStyleName("EntityDetailsPane");
            mainBodyContainer = main;
        }

        mainBodyContainer.setWidth("600px");
//        setHeadLabel();
//
        buildTreeMainWidgets(mainBodyContainer);
//
        fetchData();
//        .setHeight("100%");
        main.setHeight("100%");
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(main, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 10);
        return main;
    }

    protected String getHeadLblPrefixMsg() {
        return "OPIs";
    }

    protected IPageDataFetchBroker dataFetchBroker = new PageDataFetchBroker();

    protected void fetchData() {
        final AsyncCallback<List<TreeNodeBean>> callback = new TreeDataFetchAsyncCallback<List<TreeNodeBean>>("Communication failed" /* I18N: no */,
                dataFetchBroker) {
                    protected void doOnSuccess(List<TreeNodeBean> result) {
                        processAndShowNodes(result, null);
                    }
                };
        //bikTree.isGetDataInProgress = true;
        callServiceToGetData(callback);
    }

    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        treeCodes = BIKClientSingletons.treeSelectorToTreeCodes(BIKClientSingletons.getTreeSelectorForRegularUserCrr());
        BIKClientSingletons.getDQMService().getBikAllRootTreeNodesForTreeCodes(treeCodes, callback);
    }

    //protected abstract String getMessageForUnselectableNode(TreeNodeBean tnb);
    //ww: powinna być klasa bazowa dialogów BIKSowych i tam to powinno siedzieć
    // zauważmy, że BaseActionOrCancelDialog jest ogólną klasą bazową dialogów
    // nie ograniczoną tylko do BIKSa, więc siedzi w innym pakiecie
    // a wszystkie dialogi biksowe mają lub moga mieć coś wspólnego - bo są
    // w BIKSie używane - np. właśnie ten service...
    protected BOXIServiceAsync getService() {
        return BIKClientSingletons.getService();
    }

    protected void processAndShowNodes(List<TreeNodeBean> result, Integer optNodeIdToSelect) {
        if (isGrouppingByMenuEnabled()) {
            groupResultsByMenu(result);
        } else {
            optFixResults(result);
        }
        bikTree.initWithFullData(result, useLazyLoading, getTreeOptExtraMode(), getOptFilteringNodeActionCode());
        bikTree.selectEntityById(optNodeIdToSelect);
    }

    protected void optFixResults(List<TreeNodeBean> result) {
        // no op
    }

    protected BikNodeTreeOptMode getTreeOptExtraMode() {
        return null;
    }

    protected String getOptFilteringNodeActionCode() {
        return "AddJoinedObjs";
    }

    protected boolean calcHasNoChildren() {
        return true;
    }

    protected void fetchData(final Integer optNodeIdToSelect) {
        final AsyncCallback<List<TreeNodeBean>> callback = new TreeDataFetchAsyncCallback<List<TreeNodeBean>>("Communication failed" /* I18N: no */,
                dataFetchBroker) {
                    protected void doOnSuccess(List<TreeNodeBean> result) {
                        processAndShowNodes(result, optNodeIdToSelect);
                    }
                };
        //bikTree.isGetDataInProgress = true;
        callServiceToGetData(callback);
    }

    protected boolean mustHideLinkedNodes() {
        return true;
    }

    protected Widget setOptionalWigetAfterHeadLabel() {
        return null;
    }

    protected String getHeadLabelInitialText() {
        return getHeadLblPrefixMsg() + "...";
    }

    protected void optGetNodePathAndCenter(IContinuation con) {
        con.doIt();
    }

    protected void fetchCurrentFilterInSubTreeNamePathAndUpdateFilterButtonTitle() {
//        currentFilterInSubTreeNamePath = currentFilterInSubTreeNodeId == null ? null : BIKCenterUtils.buildAncestorPathByStorage(getBikTree().getCurrentNodeBean().branchIds, getBikTree().getTreeStorage());
    }

    protected Integer getCurrentNodeIdAsInteger() {
        return null;
        //return bikTree.getCurrentNodeId();
    }

    protected void doFilter() {
        //jezeli nic nie zostalo wpisane to wroc do pierwotnego wygladu

        currentFilterVal = filterBox.getText();

        if (currentFilterVal.equals("")) {
            currentFilterInSubTreeNodeId = null;
//            currentFilterInSubTreeNamePath = null;
            fetchData();
        } else {

            currentFilterInSubTreeNodeId = cbFilterInSubTree.isEnabled() && cbFilterInSubTree.getValue() ? getCurrentNodeIdAsInteger() : null;

            if (currentFilterInSubTreeNodeId != null) {
                fetchCurrentFilterInSubTreeNamePathAndUpdateFilterButtonTitle();
            }

            bikTree.filterTreeEx(currentFilterVal, currentFilterInSubTreeNodeId, useLazyLoading,
                    getTreeIdsForFiltering(), getTreeOptExtraMode(), getOptFilteringNodeActionCode());
        }

        configureButtons();
    }

    protected void doAfterDoubleClickOnTree(Map<String, Object> param) {
        setSelectedNodeFromBikTree(param);
//        actionClicked();
    }

    protected Collection<Integer> getTreeIdsForFiltering() {
        List<Integer> list = new ArrayList<Integer>();
        for (String treeCode : treeCodes) {
            TreeBean tb = BIKClientSingletons.getTreeBeanByCode(treeCode);
            if (tb != null) {
                list.add(tb.id);
            }
        }
        return list;
    }

    protected void resetJoinedObjModeForSubTree(Integer nodeId) {
        Map<String, Object> row = bikTree.getTreeStorage().getRowById(nodeId);

        Collection<Map<String, Object>> childRows = bikTree.getTreeStorage().getChildRows(row);

        if (childRows == null) {
            return;
        }

        for (Map<String, Object> childRow : childRows) {
            @SuppressWarnings("unchecked")
            Integer childId = (Integer) childRow.get(TreeNodeBeanAsMapStorage.ID_FIELD_NAME);
            subNodesOverride.remove(childId);
            directOverride.remove(childId);

            TreeNodeBean childNode = bikTree.getTreeStorage().getBeanById(childId);

            InlineHTML imgHtml = htmlForJOMImg.get(childId);
            if (imgHtml != null) {
                JoinedObjMode selectedVal = getNodeSelectedValue(childNode);
                final boolean isFixed = isNodeSelectionFixed(childNode);
                imgHtml.setHTML(generateHtmlForJoinedObjImg(selectedVal, isFixed));
            }

//            InlineLabel lbl = lblForJOM.get(childId);
//            setJOMLabelStyles(childNode, lbl);
            updateLabelStylesOfNode(childNode);

            resetJoinedObjModeForSubTree(childId);
        }
    }

    protected void updateLabelStylesOfNode(TreeNodeBean node) {
        InlineLabel lbl = lblForJOM.get(getExtractor().extractId(node));
        setJOMLabelStyles(node, lbl);
    }

    protected void updateLabelStylesOfNodeById(Integer id) {
        updateLabelStylesOfNode(bikTree.getTreeStorage().getBeanById(id));
    }

    protected void setJoinedObjModeForDescendants(TreeNodeBean node, JoinedObjMode mode, boolean allDescendants) {
        if (!isNodeSelectable(node)) {
            Map<String, Object> row = bikTree.getTreeStorage().getRowById(getExtractor().extractId(node));
            Collection<Map<String, Object>> childRows = bikTree.getTreeStorage().getChildRows(row);

            for (Map<String, Object> childRow : childRows) {
                @SuppressWarnings("unchecked")
                Integer childId = (Integer) childRow.get(TreeNodeBeanAsMapStorage.ID_FIELD_NAME);
                if (allDescendants) {
                    subNodesOverride.put(childId, new Pair<JoinedObjMode, Boolean>(mode, allDescendants));
                }
                directOverride.put(childId, mode);
                resetJoinedObjModeForSubTree(childId);
            }
        } else {
            subNodesOverride.put(getExtractor().extractId(node), new Pair<JoinedObjMode, Boolean>(mode, allDescendants));
            resetJoinedObjModeForSubTree(getExtractor().extractId(node));
        }
    }

    protected void showPopupMenu(Map<String, Object> dataRow, int clientX, int clientY) {
        ITreeRowAndBeanStorage<Integer, Map<String, Object>, TreeNodeBean> storage = bikTree.getTreeStorage();
        final TreeNodeBean node = storage.getBeanById(storage.getTreeBroker().getNodeId(dataRow));
        //(Integer) dataRow.get(TreeNodeBeanAsMapStorage.ID_FIELD_NAME));

        JoinedObjMode directJOM = directOverride.get(getExtractor().extractId(node));

        if (getExtractor().extractHasNoChildren(node) && directJOM == null) {
            return;
        }

        PopupMenuDisplayer popupMenu = new PopupMenuDisplayer();

        if (!getExtractor().extractHasNoChildren(node)) {
            popupMenu.addMenuItem(I18n.dodajDziedzicPowiazanDlaObiektow.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            setJoinedObjModeForDescendants(node, JoinedObjMode.WithInheritance, false);
                        }
                    });
            popupMenu.addMenuItem(I18n.dodajNiedziedPowiazanDlaObiektow.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            setJoinedObjModeForDescendants(node, JoinedObjMode.Single, false);
                        }
                    });
            popupMenu.addMenuSeparator();
            popupMenu.addMenuItem(I18n.usunPowiazanDlaObiektowBezposreP.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            setJoinedObjModeForDescendants(node, JoinedObjMode.NotJoined, false);
                        }
                    });
            popupMenu.addMenuItem(I18n.usunPowiazanDlaWszystkiObiektowP.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            setJoinedObjModeForDescendants(node, JoinedObjMode.NotJoined, true);
                        }
                    });
            popupMenu.addMenuSeparator();
            popupMenu.addMenuItem(I18n.przywrocOryginalStanPowiazanWPod.get() /* I18N:  */, new IContinuation() {
                        public void doIt() {
                            setJoinedObjModeForDescendants(node, null, true);
                        }
                    });
        }
        JoinedObjMode currVal = getNodeSelectedValue(node);
        JoinedObjMode origVal = getNodeSelectedValueEx(node, true);
        if (currVal != origVal) {
            boolean isFixed = isNodeSelectionFixed(node);
            if (!isFixed) {
                popupMenu.addMenuItem(I18n.przywrocOryginalStanPowiazanWTym.get() /* I18N:  */, new IContinuation() {
                            public void doIt() {
                                directOverride.remove(getExtractor().extractId(node));

                                JoinedObjMode currVal = getNodeSelectedValue(node);
                                JoinedObjMode origVal = getNodeSelectedValueEx(node, true);

                                if (currVal != origVal) {
                                    directOverride.put(getExtractor().extractId(node), origVal);
                                }
                                InlineHTML imgHtml = htmlForJOMImg.get(getExtractor().extractId(node));
                                imgHtml.setHTML(generateHtmlForJoinedObjImg(origVal, false));
                                updateLabelStylesOfNode(node);
                            }
                        });
            }
        }

        popupMenu.showAtPosition(clientX, clientY);
    }

    public boolean isInheritToDescendantsChecked() {
        return inheritToDescendantsCb == null ? false : inheritToDescendantsCb.getValue();
    }

    protected void setSelectedNodeFromBikTree(Map<String, Object> param) {
        selectedNode = //(TreeNodeBean) param.get(BaseBIKTreeWidget.TREE_NODE_TreeNodeBean_KEY_IN_ROW);
                bikTree.getNodeBeanByDataNode(param);
        configureButtons();
    }

    protected void configureButtons() {
        actionBtn.setEnabled(isActionButtonEnabled());
        updateFilterButtonEnabled();
    }

    protected boolean isActionButtonEnabled() {
        if (isInMultiSelectMode()) {
            if (false) {
                for (JoinedObjMode value : directOverride.values()) {
                    if (!value.equals(JoinedObjMode.NotJoined)) {
                        return true;
                    }
                }
                return false;
            }

            for (Map.Entry<Integer, JoinedObjMode> e : directOverride.entrySet()) {
                TreeNodeBean node = bikTree.getTreeStorage().getBeanById(e.getKey());
                if (hasChangeInNodeById(node) || hasChangeInDescendants(node)) {
                    return true;
                }
            }

            for (Map.Entry<Integer, Pair<JoinedObjMode, Boolean>> e : subNodesOverride.entrySet()) {
                if (e.getValue().v1 != null) {
                    return true;
                }
            }

            return false;

        } else {
            //ww:mltx dodałem spr. selectedNode != null
            return selectedNode != null && isNodeSelectable(selectedNode) && (isNodesIdFromExternalData()
                    || !BaseUtils.safeEquals(getExtractor().extractId(selectedNode), nodeId));
        }
    }

    protected void bikTreeNodeChanged(Map<String, Object> param) {
        setSelectedNodeFromBikTree(param);
    }

    protected Collection<Integer> splitBranchIdsIntoColl(String branchIds, boolean withOutSelf, Collection<Integer> coll) {
        return BIKCenterUtils.splitBranchIdsIntoColl(branchIds, withOutSelf, coll);
    }

    // defaultowo na wszystkich węzłach da się kliknąć
    protected boolean isNodeSelectable(TreeNodeBean node) {
        return true;
    }

    // może zwrócić NULL
    protected JoinedObjBasicGenericInfoBean getJoinedObjBasicInfo(Integer dstNodeId) {
        if (alreadyJoinedManager == null || !alreadyJoinedObjIds.contains(dstNodeId)) {
            return null;
        }

        JoinedObjBasicGenericInfoBean jobi = alreadyJoinedManager.getInfoForJoinedNodeById(dstNodeId);

        if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
            ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "jobi for id=" + dstNodeId + ": " + jobi);
        }

        return jobi;
    }

    protected boolean isJobiFixed(JoinedObjBasicGenericInfoBean jobi) {
        boolean res = jobi != null && (jobi.type || isNodesIdFromExternalData() || !BaseUtils.safeEquals(jobi.srcId, nodeId));

        if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
            ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "isJobiFixed jobi=" + jobi + ", nodeId=" + nodeId + ", result: " + res);
        }

        return res;
    }

    protected boolean isNodeSelectionFixed(TreeNodeBean node) {
        if (!isNodesIdFromExternalData() && BaseUtils.safeEquals(getExtractor().extractId(node), nodeId)) {

            if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
                ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "isNodeSelectionFixed (true) for id=" + getExtractor().extractId(node) + ", nodeId=" + nodeId);
            }

            return true;
        }

        JoinedObjBasicGenericInfoBean jobi = getJoinedObjBasicInfo(getExtractor().extractId(node));
        return isJobiFixed(jobi);
    }

    protected JoinedObjMode getNodeSelectedValueEx(TreeNodeBean node, boolean noOverride) {
        JoinedObjBasicGenericInfoBean jobi = getJoinedObjBasicInfo(getExtractor().extractId(node));

        if (!isJobiFixed(jobi) && !noOverride) {
            JoinedObjMode jobiOverride = directOverride.get(getExtractor().extractId(node));
            if (jobiOverride != null) {
                if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
                    ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "getNodeSelectedValueEx (true) for id=" + getExtractor().extractId(node) + ", nodeId=" + nodeId + ", from override: " + jobiOverride);
                }
                return jobiOverride;
            }

            List<Integer> ancestorNodeIds = (List<Integer>) splitBranchIdsIntoColl(getExtractor().extractBranchIds(node), true, new ArrayList<Integer>());
            JoinedObjMode fromAncestor = null;
            boolean isParent = true;
            for (int i = ancestorNodeIds.size() - 1; i >= 0; i--) {
                Integer ancestorNodeId = ancestorNodeIds.get(i);
                Pair<JoinedObjMode, Boolean> ancestorOverride = subNodesOverride.get(ancestorNodeId);
                if (ancestorOverride != null && (ancestorOverride.v2 || isParent)) {
                    fromAncestor = ancestorOverride.v1;
                    break;
                }
                isParent = false;
            }
            if (fromAncestor != null) {
                return fromAncestor;
            }
        }

        if (jobi == null) {
            if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
                ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "getNodeSelectedValueEx (true) for id=" + getExtractor().extractId(node) + ", nodeId=" + nodeId + ", no jobi");
            }
            return JoinedObjMode.NotJoined;
        }

        if (jobi.inheritToDescendants || isNodesIdFromExternalData() || !BaseUtils.safeEquals(jobi.srcId, nodeId)) {
            if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
                ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "getNodeSelectedValueEx (true) for id=" + getExtractor().extractId(node) + ", nodeId=" + nodeId + ", jobi=" + jobi + ", with inheritance");
            }
            return JoinedObjMode.WithInheritance;
        }

        if (ClientDiagMsgs.isShowDiagEnabled("getJoinedObjBasicInfo")) {
            ClientDiagMsgs.showDiagMsg("getJoinedObjBasicInfo", "getNodeSelectedValueEx (true) for id=" + getExtractor().extractId(node) + ", nodeId=" + nodeId + ", jobi=" + jobi + ", single");
        }
        return JoinedObjMode.Single;
    }

    protected JoinedObjMode getNodeSelectedValue(TreeNodeBean node) {
        return getNodeSelectedValueEx(node, false);
    }

    protected String generateHtmlForJoinedObjImg(JoinedObjMode selectedVal, boolean isFixed) {
        return "<" + "img src='images" /* I18N: no */ + "/joined_"
                + selectedVal.imgNamePart + (isFixed ? "_fixed" : "")
                + "." + "png' style=\"width: 16px; height: 16px;\" border=\"0\" class" /* I18N: no */ + "=\"treeIcon\"/>";
    }

    protected boolean hasChangeInDescendants(TreeNodeBean node) {
        return false;
    }

    protected boolean hasChangeInNodeById(TreeNodeBean node) {
        JoinedObjMode currVal = getNodeSelectedValue(node);
        JoinedObjMode origVal = getNodeSelectedValueEx(node, true);
        return currVal != origVal;
    }

    protected boolean isNodeOriginallyJoined(TreeNodeBean node) {
        return getJoinedObjBasicInfo(getExtractor().extractId(node)) != null;
        //alreadyJoinedManager != null && alreadyJoinedManager.getInfoForJoinedNodeById(node.id) != null;
    }

    protected boolean hasJoinedNodesInDescendantsById(TreeNodeBean node) {
        return alreadyJoinedManager != null && ancestorIdsOfJoinedObjs.contains(getExtractor().extractId(node));
    }

    protected void setJOMLabelStyles(TreeNodeBean node, InlineLabel lbl) {
        lbl.setStyleName("");
        if (hasChangeInNodeById(node)) {
            lbl.addStyleName("tree-selector-hasChangeInNode");
        }
        if (isNodeOriginallyJoined(node)) {
            lbl.addStyleName("tree-selector-isNodeOriginallyJoined");
        }
        if (hasJoinedNodesInDescendantsById(node)) {
            lbl.addStyleName("tree-selector-hasJoinedNodesInDescendants");
        }
        if (hasChangeInDescendants(node)) {
            lbl.addStyleName("tree-selector-hasChangeInDescendants");
        }
    }

    protected String getMessageForUnselectableNode(TreeNodeBean tnb) {
        if (!isNodesIdFromExternalData() && BaseUtils.safeEquals(getExtractor().extractId(tnb), nodeId)) {
            return I18n.nieMoznaPowiazacWezlaZNimSamym.get() /* I18N:  */;
        }
        return null;
    }

    //ww: nie rób focusa wcale
    protected void tryFocusFirstWidget() {
        // no-op
    }

    protected void treeHasNoData() {
        setSelectedNodeFromBikTree(null);
    }

    protected AbstractBIKTreeWidget<TreeNodeBean, Integer> getTreeWidget() {
        return new BikTreeWidget(mustHideLinkedNodes(), BIKClientSingletons.ORIGINAL_EDP_BODY, "wwNewTreePopup wwNewTree",
                dataFetchBroker,
                calcHasNoChildren(),
                TreeNodeBeanAsMapStorage.NAME_FIELD_NAME + "=300:Nazwa",
                TreeNodeBeanAsMapStorage.PRETTY_KIND_FIELD_NAME + "=100:Typ") {
                    @Override
                    protected void optFixBeanPropsAfterLoad(TreeNodeBean tnb) {
//                        TreeSelectorDialog.this.optFixBeanPropsAfterLoad(tnb);
                    }

                    @Override
                    protected void afterInitWithNoData() {
                        super.afterInitWithNoData();
                        treeHasNoData();
                    }
                };
    }

    protected void buildTreeMainWidgets(Panel mainBodyContainer) {

        bikTree = getTreeWidget();

        Widget treeWidget = bikTree.getTreeGridWidget();
        //treeWidget.setSize("100%", "100%");
        treeWidget.setSize("600px", "100%");
        mainBodyContainer.add(treeWidget);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(treeWidget, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - 60);
//
//
        HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);

        if (bikTree.isSubTreeFilteringCapable()) {
            cbFilterInSubTree.setTitle(I18n.filtrujWPoddrzewie.get());
            cbFilterInSubTree.addClickHandler(new ClickHandler() {

                public void onClick(ClickEvent event) {
                    updateFilterButtonEnabled();
                }
            });

            hPanel.add(cbFilterInSubTree);
            hPanel.setCellWidth(cbFilterInSubTree, "22px");
        }
//
        filterBox = new TextBox();
        filterBox.setStyleName("filterBox");
        filterBox.setWidth("164px");

        filterBox.addFocusHandler(new FocusHandler() {
            public void onFocus(FocusEvent event) {
                isInFilterBox = true;
            }
        });
//
        filterBox.addBlurHandler(new BlurHandler() {
            public void onBlur(BlurEvent event) {
                isInFilterBox = false;
            }
        });
//
        hPanel.add(filterBox);
        hPanel.setCellWidth(filterBox, "180px");
        hPanel.setStyleName("treeBikFilterBox");
        hPanel.setWidth("100%");
//
        filterBox.addKeyUpHandler(new KeyUpHandler() {
            public void onKeyUp(KeyUpEvent event) {
                updateFilterButtonEnabled();
            }
        });

        IContinuation filterCont = new IContinuation() {
            public void doIt() {
                doFilter();
            }
        };
        filterButton = new BikItemButtonBig(I18n.fILTRUJ.get() /* I18N:  */, filterCont);

        hPanel.add(filterButton);
        hPanel.setCellHorizontalAlignment(filterButton, HasHorizontalAlignment.ALIGN_LEFT);
        mainBodyContainer.add(hPanel);

        if (isInMultiSelectMode()) { // multi mode
            bikTree.setCellWidgetBuilder(new ITreeGridCellWidgetBuilder<TreeNodeBean>() {
                public Widget getOptWidget(TreeNodeBean row, String colName) {
                    if (!BaseUtils.safeEquals(colName, TreeNodeBeanAsMapStorage.NAME_FIELD_NAME)) {
                        return null;
                    }
                    return createWidgetForNameColumn(row);
                }
            });
//            addPopupMenuInMultiSelectMode();
        } else { // single mode
            bikTree.addSelectionChangedHandler(new IParametrizedContinuation<Map<String, Object>>() {
                public void doIt(Map<String, Object> param) {
                    bikTreeNodeChanged(param);
                }
            });
            bikTree.addDoubleClickHandler(new IParametrizedContinuation<Map<String, Object>>() {
                public void doIt(Map<String, Object> param) {
                    doAfterDoubleClickOnTree(param);
                }
            });
        }

        bikTree.addSelectionChangedHandler(new IParametrizedContinuation<Map<String, Object>>() {

            public void doIt(Map<String, Object> param) {
                updateFilterButtonEnabled();
            }
        });

//        actionBtn.setEnabled(false);
////        tf: zbędny, podwójny fetch
////        fetchData();
    }

    protected void updateFilterButtonEnabled() {

        Integer currentNodeIdAsInteger = getCurrentNodeIdAsInteger();
        cbFilterInSubTree.setEnabled(currentNodeIdAsInteger != null);

        boolean subTreeFilteringChanged = (currentFilterInSubTreeNodeId != null) != (cbFilterInSubTree.isEnabled() && cbFilterInSubTree.getValue())
                || !BaseUtils.safeEquals(currentFilterInSubTreeNodeId, currentNodeIdAsInteger);
        boolean textHasChanged = !BaseUtils.safeEquals(filterBox.getText(), currentFilterVal);
        filterButton.setEnabled(subTreeFilteringChanged || textHasChanged);

        boolean filterIsOn = !BaseUtils.isStrEmpty(currentFilterVal);

        GWTUtils.addOrRemoveStyleName(filterButton, "filterIsOn", filterIsOn);

        if (filterIsOn) {
            if (currentFilterInSubTreeNodeId == null) {
                filterButton.setTitle("Filtrowanie [" + currentFilterVal + "]");
            }
//            filterButton.setTitle("Filtrowanie [" + currentFilterVal + "]"
//                    + (currentFilterInSubTreeNodeId != null ? " w poddrzewie węzła: ..." : ""));
        } else {
            filterButton.setTitle("Brak filtrowania");
        }
    }

    protected IBean4TreeExtractor<TreeNodeBean, Integer> getExtractor() {
        if (beanExtractor == null) {
            beanExtractor = createBeanExtractor();
        }
        return beanExtractor;
    }

    public IBean4TreeExtractor<TreeNodeBean, Integer> createBeanExtractor() {
        return new TreeBean4TreeExtractor();
    }

    // tf: flaga mówiąca, czy id są z zewnętrznego systemu i nie powinny być porównywalne z nodeId z bik_node
    // (np aby nie móc dodać powiązanie do samego siebie)
    // defaultowo false
    protected boolean isNodesIdFromExternalData() {
        return false;
    }

    protected void groupResultsByMenu(List<TreeNodeBean> result) {
        List<MenuNodeBean> menuItems = extendMenuNodes();

        Map<Integer, TreeNodeBean> menuItemsWithoutTrees = new HashMap<Integer, TreeNodeBean>();

        List<TreeNodeBean> newRootNodesAsList = new ArrayList<TreeNodeBean>();
        Map<Integer, TreeNodeBean> newTreeRootNodes = new HashMap<Integer, TreeNodeBean>();
        Collection<Integer> treeIdsForFiltering = getTreeIdsForFiltering();
        for (MenuNodeBean mnb : menuItems) {

            if (mnb.nodeKindId == null && (mnb.treeId == null || treeIdsForFiltering.contains(mnb.treeId))) {

                TreeNodeBean newRoot = new TreeNodeBean();
                newRoot.id = -mnb.id; //newRootId--;
                newRoot.name = mnb.name;
                //mm-> poprawka na szybko, żeby było po polsku - do poprawienia, jak będzie koncepcja
                newRoot.nodeKindCode = I18n.kategoria.get() /* I18N:  *//*"Category"*/;
                newRoot.nodeKindCaption = I18n.kategoria.get() /* I18N:  */;
                newRoot.nodeKindId = -1;
                newRoot.parentNodeId = mnb.parentNodeId == null ? null : -mnb.parentNodeId;
                newRoot.branchIds = mnb.parentNodeId == null ? (-mnb.id) + "|" : ((-mnb.parentNodeId) + "|" + (-mnb.id) + "|");
                newRoot.treeId = mnb.treeId == null ? -1 : -mnb.treeId; //-1;
                newRoot.treeCode = mnb.code;
                newRoot.treeName = mnb.name;
                newRoot.visualOrder = menuItems.indexOf(mnb); // do sortowania
                if (mnb.treeId != null) {
                    newTreeRootNodes.put(mnb.treeId, newRoot);
                    newRootNodesAsList.add(newRoot);
                } else {
                    menuItemsWithoutTrees.put(newRoot.id, newRoot);
                }
            }
        }

        List<TreeNodeBean> rootDataNodes
                = BIKCenterUtils.getRootNodes(result);
        if (newTreeRootNodes.size() > 1) {

            for (Map.Entry<Integer, TreeNodeBean> tnb : newTreeRootNodes.entrySet()) {
                getAndAddAncestors(tnb.getValue(), newRootNodesAsList, menuItemsWithoutTrees);
            }

            Collections.sort(newRootNodesAsList, new Comparator<TreeNodeBean>() {

                @Override
                public int compare(TreeNodeBean o1, TreeNodeBean o2) {
                    if (o1.visualOrder > o2.visualOrder) {
                        return 1;
                    }
                    if (o1.visualOrder < o2.visualOrder) {
                        return -1;
                    }
                    return 0;
                }
            });
            result.addAll(newRootNodesAsList);

            for (TreeNodeBean tnb : rootDataNodes) {
                tnb.parentNodeId = newTreeRootNodes.get(tnb.treeId) != null ? newTreeRootNodes.get(tnb.treeId).id : null;
            }

            for (TreeNodeBean tnb : result) {
                if (tnb.treeId > 0) { // normalne obiekty, bez sztucznych
                    tnb.branchIds = (tnb.parentTreeId == null ? (-tnb.treeId) + "|" : (-tnb.parentTreeId) + "|" + ((-tnb.treeId) + "|")) + tnb.branchIds;
                }
            }
        }
    }

    protected List<MenuNodeBean> extendMenuNodes() {
        return BIKClientSingletons.getExtendedMenuNodes(
                BIKClientSingletons.getMenuNodes(), BIKClientSingletons.getTrees(), null/* BIKClientSingletons.getNodeKindsByCodeMap()*/,
                true, MenuExtender.MenuBottomLevel.NodeTrees,
                false, false);
    }

    protected void getAndAddAncestors(TreeNodeBean tnb, List<TreeNodeBean> newRootNodesAsList, Map<Integer, TreeNodeBean> menuItemsWithoutTrees) {
        TreeNodeBean newTnb = menuItemsWithoutTrees.get(tnb.parentNodeId);
        if (newTnb != null && !newRootNodesAsList.contains(newTnb)) {
            newRootNodesAsList.add(newTnb);
            getAndAddAncestors(newTnb, newRootNodesAsList, menuItemsWithoutTrees);
        }
    }

}
