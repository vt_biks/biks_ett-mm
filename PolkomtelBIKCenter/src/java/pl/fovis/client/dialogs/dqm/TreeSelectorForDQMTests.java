/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.TreeSelectorDialog;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class TreeSelectorForDQMTests extends TreeSelectorDialog {

    protected Set<Integer> alreadyAssigned;

    public void buildAndShowDialog(int thisNodeId, Set<Integer> alreadyAssigned, IParametrizedContinuation<TreeNodeBean> saveCont) {
        super.buildAndShowDialog(thisNodeId, saveCont, null);
        this.alreadyAssigned = alreadyAssigned;
    }

    @Override
    protected boolean isInheritToDescendantsCheckBoxVisible() {
        return false;
    }

    @Override
    protected void callServiceToGetData(AsyncCallback<List<TreeNodeBean>> callback) {
        BIKClientSingletons.getDQMService().getDQMTestsTreeNodes(nodeId, callback);
    }

    @Override
    protected Collection<Integer> getTreeIdsForFiltering() {
        return BIKClientSingletons.getProperTreeIdsWithTreeKind(BIKGWTConstants.TREE_KIND_QUALITY_METADATA);
    }

    @Override
    protected boolean isActionButtonEnabled() {
        return selectedNode != null && (selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_DQM_TEST_SUCCESS)
                || selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_DQM_TEST_NOT_STARTED)
                || selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_DQM_TEST_INACTIVE)
                || selectedNode.nodeKindCode.equals(BIKGWTConstants.NODE_KIND_DQM_TEST_FAILED));
    }

    @Override
    protected boolean mustHideLinkedNodes() {
        return false;
    }

    @Override
    protected boolean doActionBeforeClose() {
        if (alreadyAssigned != null && (alreadyAssigned.contains(selectedNode.id) || alreadyAssigned.contains(selectedNode.linkedNodeId))) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.wybranyTestJestJuzPrzypisany.get(), I18n.blad.get(), null);
            return false;
        }
        return true;
    }

    @Override
    protected String getDialogCaption() {
        return I18n.przypiszTest.get();
    }

    @Override
    protected String getHeadLblPrefixMsg() {
        return I18n.przypisujeszTestDoGrupy.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.przypisz.get();
    }
}
