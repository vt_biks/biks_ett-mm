/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class DQMLogBean implements Serializable {

    public int id;
    public int testId;
    public String testName;
    public String sourceName;
    public String serverName;
    public String serverInstance;
    public String fileName;
    public String databaseName;
    public Date startTime;
    public Date finishTime;
    public String description;
    public Integer multiSourceId;
    public Integer dataReadSecs;
    public String connectionName;
}
