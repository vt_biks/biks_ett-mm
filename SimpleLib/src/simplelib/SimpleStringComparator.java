/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author tflorczak
 */
public class SimpleStringComparator implements IStringComparator {

    private static final SimpleStringComparator instance = new SimpleStringComparator();
    private static final Map<Character, Long> map = new HashMap<Character, Long>();
    private static final int FIX_RESOLUTION = 2;
    
    private static void addMapping(char plChar, char noPlChar) {
        addMapping(plChar, noPlChar, 1);
    }

    private static void addMapping(char plChar, char noPlChar, int fix) {
        map.put(plChar, ((long)noPlChar << FIX_RESOLUTION) + fix);
    }

    static { // sztuczne kody unicode dla polskich znaków
        addMapping('ą', 'a');
        addMapping('ć', 'c');
        addMapping('ę', 'e');
        addMapping('ł', 'l');
        addMapping('ń', 'n');
        addMapping('ó', 'o');
        addMapping('ś', 's');
        addMapping('ź', 'z');
        addMapping('ż', 'z', 2);
    }

//    public native int compare(String source, String target) /*-{
//    var sour = source.toLowerCase();
//    var dest = target.toLowerCase();
//    var r = sour.localeCompare( dest );
//    return r < 0 ? -1 : (r > 0 ? 1 : 0);
//    }-*/;
    public int compare(String source, String target) {
        return myCompare(source.toLowerCase(), target.toLowerCase());
    }

    public static SimpleStringComparator getInstance() {
        return instance;
    }

    private int myCompare(String s, String d) {
        int min = s.length() > d.length() ? d.length() : s.length();
        for (int i = 0; i < min; i++) {
            long s1 = 0;
            long d1 = 0;
            if (map.containsKey(s.charAt(i))) {
                s1 = map.get(s.charAt(i));
            } else {
                s1 = s.charAt(i) << FIX_RESOLUTION;
            }

            if (map.containsKey(d.charAt(i))) {
                d1 = map.get(d.charAt(i));
            } else {
                d1 = d.charAt(i) << FIX_RESOLUTION;
            }

            if (s1 > d1) {
                return 1;
            }
            if (s1 < d1) {
                return -1;
            }
        }

        if (s.length() > d.length()) {
            return 1;
        }
        if (s.length() < d.length()) {
            return -1;
        }
        
        return 0;
    }
}
