/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import simplelib.BeanWithIntIdAndNameBase;

/**
 *
 * @author ctran
 */
public class ConfigPlainFilePumpBean extends BeanWithIntIdAndNameBase {

    public boolean isActive;
    public String sourceFolderPath;
    public boolean isRemote;
    public String domain;
    public String username;
    public String password;
    public Integer isIncremental;
}
