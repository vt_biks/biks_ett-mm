/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author wezyr
 * @param <ID>
 */
public interface IBikPage<ID> /*extends IMenuItemMainWidget*/ {

    public String getId(); /* + */

    //public String getCaption();

    public Widget createMainWidget();

    //public boolean isInited();
    //public String getDescription();
//    public String getCaption();
    public void activatePage(boolean refreshData, ID optIdToSelect, boolean refreshDetailData);

    public Integer getSelectedNodeId();

    public Integer getDisplayedNodeId();

    public boolean isPageEnabled();
}
