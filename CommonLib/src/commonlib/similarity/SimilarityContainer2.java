/*
 * SimilarityContainer2.java
 *
 * Created on 31 sierpie� 2007, 19:31
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib.similarity;

import commonlib.LameUtils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author wezyr
 */
public class SimilarityContainer2 implements SimilarityContainerIntf {
    
    private static Locale PolishLocale = new Locale("pl");
    
    private Set<String> exactWords = new HashSet<String>();
    private Map<String, List<String>> partialWords;
    private double minSimilarity;
    
    public SimilarityContainer2(double minSimilarity) {
        this.minSimilarity = minSimilarity;
        this.partialWords = new HashMap<String, List<String>>();//[partCount]; //HashMap<String, List<String>>
    }
    
    protected String normalizeWord(String word) {
        return word.toLowerCase(PolishLocale);
    }
    
    protected String makePartial(String word, int partNo, int partNo2) {
        char buff[] = new char[word.length()];
        int outPos = 0;
        
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            if (i != partNo && i != partNo2)
                buff[outPos++] = c;
        }
        
        String res = new String(buff, 0, outPos);
        //System.out.println("makePartial: word=" + word + ", partNo=" + partNo + ", res=" + res);
        return res;
    }
    
    public void add(String word) {
        String normalizedWord = normalizeWord(word);
        
        if (exactWords.contains(word))
            return;
        
        exactWords.add(normalizedWord);
        
//        Map<String, List<String>>[] partialWords = partialWordsMap.get(normalizedWord.length());
//        if (partialWords == null) {
//            partialWords = newPartialWords();
//            partialWordsMap.put(normalizedWord.length(), partialWords);
//        }
        
        LameUtils.addToCollectingMapWithList(partialWords, normalizedWord, normalizedWord);
        if (normalizedWord.length() > 1) {
            for (int i = 0; i < normalizedWord.length(); i++) {
                String partialWord = makePartial(normalizedWord, i, -1);
                //System.out.println("normalized: " + normalizedWord + ", partial: " + partialWord);
                LameUtils.addToCollectingMapWithList(partialWords, partialWord, normalizedWord);
//            List<String> fullWords = partialWords[i].get(partialWord);
//            if (fullWords == null) {
//                fullWords = new ArrayList<String>();
//                partialWords[i].put(partialWord, fullWords);
//            }
//            fullWords.add(normalizedWord);
            }
        }
    }
    
    public int getWordCount() {
        return exactWords.size();
    }
    
    public double calcSimilarity(String w1, String w2) {
        int maxL = Math.max(w1.length(), w2.length());
        int dist = StringSimilarityUtils.calcDamerauLevenshteinDistance(w1, w2);
        return 100 - dist * 5;
        
        //return (maxL - dist) * 100.0 / maxL;
        
//        return //100.0;
//                20.0 * LetterTupleSimilarity.calcRankForStrings(w1, w2, 2, false) +
//                30.0 * LetterTupleSimilarity.calcRankForStrings(w1, w2, 2, true) +
//                50.0 * LetterTupleSimilarity.calcRankForStrings(w1, w2, 1, false);
    }
    
    public Map<String, Double> findSimilar(String word) {
        String normalizedWord = normalizeWord(word);
        
        Map<String, Double> candidates = new HashMap<String, Double>();
        
        int calcCnt = 0;
        
        if (normalizedWord.length() > 1) {
            for (int i = 0; i < normalizedWord.length(); i++) {
                String partialWord = makePartial(normalizedWord, i, -1);
                List<String> fullWords = partialWords.get(partialWord);
                if (fullWords != null) {
                    for (String w : fullWords) {
                        calcCnt++;
                        double similarity = calcSimilarity(normalizedWord, w);
                        if (similarity >= minSimilarity)
                            candidates.put(w, similarity);
                    }
                }
            }
        }
        
        List<String> fullWords = partialWords.get(normalizedWord);
        if (fullWords != null) {
            for (String w : fullWords) {
                calcCnt++;
                double similarity = calcSimilarity(normalizedWord, w);
                if (similarity >= minSimilarity)
                    candidates.put(w, similarity);
            }
        }
        
        //System.out.println("findSimilar: " + word + ", calcCnt=" + calcCnt + ", res=" + candidates);
        return candidates;
    }
}
