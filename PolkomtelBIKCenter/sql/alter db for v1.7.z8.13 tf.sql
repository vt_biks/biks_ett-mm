﻿exec sp_check_version '1.7.z8.13';
go


if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_user_in_node]') and name = N'idx_bik_user_in_node_user_id_node_id_role_for_node_id')
begin
	create index idx_bik_user_in_node_user_id_node_id_role_for_node_id on bik_user_in_node (user_id, node_id, role_for_node_id) include (is_auxiliary, inherit_to_descendants, is_built_in);
end;
go

exec sp_update_version '1.7.z8.13', '1.7.z8.14';
go