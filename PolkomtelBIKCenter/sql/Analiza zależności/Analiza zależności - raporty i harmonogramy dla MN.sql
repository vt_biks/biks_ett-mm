﻿IF OBJECT_ID('tempdb..#xxx') IS NOT NULL 
drop table #xxx
go

declare @rapTreeId int = (select id from bik_tree where code = 'Reports2')
declare @rapWebiKindId int = (select dbo.fn_node_kind_id_by_code('Webi'))
declare @rapDeskiKindId int = (select dbo.fn_node_kind_id_by_code('FullClient'))
declare @rapCrystalKindId int = (select dbo.fn_node_kind_id_by_code('CrystalReport'))
declare @rapExcelKindId int = (select dbo.fn_node_kind_id_by_code('Excel'))
declare @scheduleKindId int = (select dbo.fn_node_kind_id_by_code('ReportSchedule'))


select
		rap.name as report_name,
		cast('' as varchar(max)) as rap_parent_path, 
		rap.parent_node_id as rap_ancestor_id,
		bnk.caption,
		case sch.schedule_type when 8 then 'Wstrzymany' 
							   when 9 then 'Cykliczny'
							   when 0 then 'Uruchomiony'
							   when 1 then 'Zakończony sukcesem'
							   when 3 then 'Zakończony błędem' end as schedule_status,
		case sch.type when 0 then 'Jednorazowo'
					  when 1 then 'Obiekt uruchamiany co ' + cast(sch.interval_hours as varchar(50)) + ' godzin i ' + cast(sch.interval_minutes as varchar(50)) + ' minut'
					  when 2 then 'Obiekt uruchamiany co ' + cast(sch.interval_days as varchar(50)) + ' dni'
					  when 3 then 'Obiekt uruchamiany co tydzień'
					  when 4 then 'Obiekt uruchamiany co ' + cast(sch.interval_months as varchar(50)) + ' miesięcy'
					  when 5 then 'Obiekt jest uruchamiany ' + cast(sch.interval_nth_day as varchar(50)) + ' dnia każdego miesiąca'
					  when 6 then 'Obiekt jest uruchamiany w pierwszy poniedziałek każdego miesiąca'
					  when 7 then 'Obiekt jest uruchamiany ostatniego dnia miesiąca'
					  when 8 then 'Obiekt jest uruchamiany zgodnie z kalendarzem'
					  when 9 then 'Obiekt jest uruchamiany zgodnie z kalendarzem' end as running_type,
		sch.nextruntime, ex.last_run_time
		--,sch.*
into #xxx
from
		bik_node rap
		inner join bik_node_kind bnk on bnk.id = rap.node_kind_id
		inner join bik_node har on har.parent_node_id = rap.id
		inner join bik_sapbo_schedule sch on sch.node_id = har.id
		left join bik_sapbo_extradata ex on rap.id = ex.node_id
where
		rap.is_deleted = 0 and rap.linked_node_id is null and rap.tree_id = @rapTreeId and rap.node_kind_id in (@rapWebiKindId,@rapDeskiKindId,@rapCrystalKindId,@rapExcelKindId)
		and har.is_deleted = 0 and har.linked_node_id is null and har.node_kind_id = @scheduleKindId
go


while 1 = 1 begin
  declare @rc int = null

  update #xxx
  set rap_parent_path = bn.name + '\' + rap_parent_path, rap_ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #xxx.rap_ancestor_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go



select rap_parent_path as Raport_sciezka, report_name as Raport, caption as Typ_raportu, schedule_status as Status_harmonogramu, running_type as Uruchamiany_co,/* last_run_time as Poprzednie_Uruchomienie,*/ nextruntime as Nastepne_Uruchomienie from #xxx
order by Raport_sciezka, Raport

