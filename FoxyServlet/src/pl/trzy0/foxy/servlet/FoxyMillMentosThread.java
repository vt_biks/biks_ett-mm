/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import commonlib.LameUtils;
import commonlib.ILameResourceProvider;
import pl.trzy0.foxy.serverlogic.FoxyUrlRewriter;
import pl.trzy0.foxy.serverlogic.IApplicationEnvironment;

/**
 * "mentos" stands here for "fresh maker" ;-)
 * @author wezyr
 */
public class FoxyMillMentosThread extends Thread implements IFoxyMillBundleBroker {

    public static final int MENTOS_SLEEP_MILLIS = 3000;
    private FoxyMillBundle bundle;
    private ILameResourceProvider resProvider;
    private String propsFilePath;
    private String pageFilesDir;
    private String globalFilesDir;
    private IBundleDirector bundleDirector;
    private FoxyUrlRewriter urlRewriter;
    private IApplicationEnvironment appEnv;

    public FoxyMillMentosThread(IBundleDirector bundleDirector,
            String propsFilePath, String pageFilesDir,
            String globalFilesDir, ILameResourceProvider resProvider,
            FoxyUrlRewriter urlRewriter) {
        this.bundleDirector = bundleDirector;
        this.resProvider = resProvider;
        this.propsFilePath = propsFilePath;
        this.pageFilesDir = pageFilesDir;
        this.globalFilesDir = globalFilesDir;
        this.urlRewriter = urlRewriter;
        //this.appEnv = appEnv;
        //this.bundle = makeInitialBundle();
    }

    @Override
    public void run() {
        while (!interrupted()) {
            LameUtils.threadSleep(MENTOS_SLEEP_MILLIS);
            if (!isInterrupted()) {
                FoxyMillBundle newBoundle = FoxyMillBundle.calcNewBoundle(getBundle());
                if (newBoundle != null) {
                    setBoundle(newBoundle);
                }
            }
        }
    }

    private synchronized void setBoundle(FoxyMillBundle newBoundle) {
        this.bundle = newBoundle;
    }

    public synchronized FoxyMillBundle getBundle() {
        return bundle;
    }

    private FoxyMillBundle makeInitialBundle() {
        FoxyMillBundle res = new FoxyMillBundle();
        res.generateInitial(bundleDirector, propsFilePath, pageFilesDir,
                globalFilesDir, resProvider, urlRewriter, appEnv);
        return res;
    }

    public void setApplicationEnvironment(IApplicationEnvironment appEnv) {
        this.appEnv = appEnv;
        this.bundle = makeInitialBundle();
    }
}
