/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.util.List;
import simplelib.BeanWithIntIdBase;

/**
 *
 * @author tflorczak
 */
public class TreeKindBean extends BeanWithIntIdBase {

    public String code;
    public String caption;
    public String treeKindCaption;
    public int branchNodeKindId;
    public int leafNodeKindId;
    public int isDeleted;
    public int allowLinking;
    public String branchCaption;
    public String branchCode;
    public String leafCaption;
    public String leafCode;
    public String branchIcon;
    public String leafIcon;
    public String lang;
    // dodatkowe node_kindy - działają w trybie niedzewiastym
    public List<NodeKind4TreeKind> additionalNodeKindsId;

    public TreeKindBean() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TreeKindBean other = (TreeKindBean) obj;
        if ((this.code == null) ? (other.code != null) : !this.code.equals(other.code)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.code != null ? this.code.hashCode() : 0);
        return hash;
    }

}
