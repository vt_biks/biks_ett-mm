/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author pmielanczuk
 */
public class TreeNodeBranchBean implements Serializable {

    public Integer treeId;
    public Integer nodeId;
    public String branchIds;
    public String objId;
    public int nodeKindId;

    public TreeNodeBranchBean() {
    }

    public TreeNodeBranchBean(Integer treeId, Integer nodeId, String branchIds) {
        this.treeId = treeId;
        this.nodeId = nodeId;
        this.branchIds = branchIds;
    }

    public TreeNodeBranchBean(Integer treeId, Integer nodeId, String branchIds, String objId) {
        this.treeId = treeId;
        this.nodeId = nodeId;
        this.branchIds = branchIds;
        this.objId = objId;
    }

    public TreeNodeBranchBean(Integer treeId, Integer nodeId, String branchIds, int nodeKindId) {
        this.treeId = treeId;
        this.nodeId = nodeId;
        this.branchIds = branchIds;
        this.nodeKindId = nodeKindId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.treeId != null ? this.treeId.hashCode() : 0);
        hash = 47 * hash + (this.nodeId != null ? this.nodeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TreeNodeBranchBean other = (TreeNodeBranchBean) obj;
        if (this.treeId != other.treeId && (this.treeId == null || !this.treeId.equals(other.treeId))) {
            return false;
        }
        if (this.nodeId != other.nodeId && (this.nodeId == null || !this.nodeId.equals(other.nodeId))) {
            return false;
        }
        return true;
    }
}
