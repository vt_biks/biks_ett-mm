/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class GenericTreeHelper<DN, ID> implements ITreeChildBroker<DN> {

    protected ITreeBroker<DN, ID> treeBroker;
    protected Map<ID, DN> nodesMap;
    protected Map<ID, List<DN>> childNodesMap;

    public GenericTreeHelper(ITreeBroker<DN, ID> treeBroker, Collection<DN> nodes) {
        this.treeBroker = treeBroker;
        processTreeNodes(nodes);
    }

    protected void processTreeNodes(Collection<DN> nodes) {
        nodesMap = new HashMap<ID, DN>();
        childNodesMap = new HashMap<ID, List<DN>>();

        for (DN node : nodes) {
            ID nodeId = treeBroker.getNodeId(node);
            nodesMap.put(nodeId, node);
        }

        for (DN node : nodes) {
            ID parentNodeId = treeBroker.getParentId(node);

            // parentNodeId fixing
            DN parentNode = nodesMap.get(parentNodeId);
            if (parentNode == null) {
                parentNodeId = null;
            }

            List<DN> childNodes = childNodesMap.get(parentNodeId);
            if (childNodes == null) {
                childNodes = new ArrayList<DN>();
                childNodesMap.put(parentNodeId, childNodes);
            }
            childNodes.add(node);
        }
    }

    public DN getNodeById(ID id) {
        return nodesMap.get(id);
    }

    public DN getParentNode(DN node) {
        return getNodeById(treeBroker.getParentId(node));
    }

    // node == null --> get root nodes
    public List<DN> getChildNodes(DN node) {
        return childNodesMap.get(node == null ? null : treeBroker.getNodeId(node));
    }

    public boolean isDescendantNode(DN node, DN descendantCandidate) {
        while (descendantCandidate != null && descendantCandidate != node) {
            descendantCandidate = getParentNode(descendantCandidate);
        }
        return descendantCandidate == node;
    }

    public ITreeBroker<DN, ID> getTreeBroker() {
        return treeBroker;
    }

    public int populateWithDescendants(Collection<? super DN> coll, DN node,
            Projector<DN, Pair<Boolean, Boolean>> suitableTest) {
        List<DN> childNodes = getChildNodes(node);

        if (ClientDiagMsgsStub.isShowDiagEnabled("ww:filterTree")) {
            ClientDiagMsgsStub.showDiagMsg("ww:filterTree",
                    "populateWithDescendants: for node" /* I18N: no */ + ": " + node + ", " + ""
                    + "childNodes.size" /* I18N: no */ + ": " + BaseUtils.collectionSizeFix(childNodes));
        }

        if (childNodes == null) {
            return 0;
        }

        Pair<Boolean, Boolean> st = null;
        int res = 0;

        for (DN childNode : childNodes) {
            if (suitableTest != null) {
                st = suitableTest.project(childNode);
            }

            if (ClientDiagMsgsStub.isShowDiagEnabled("ww:filterTree")) {
                ClientDiagMsgsStub.showDiagMsg("ww:filterTree",
                        "populateWithDescendants: test for node" /* I18N: no */ + ": " + childNode + ", " + ""
                        + "st" /* I18N: no */ + "=" + st);
            }

            if (st == null || st.v1) {
                res++;
                coll.add(childNode);
            }

            if (st == null || st.v2) {
                res += populateWithDescendants(coll, childNode);
            }
        }

        return res;
    }

    public int populateWithDescendants(Collection<? super DN> coll, DN node) {
        return populateWithDescendants(coll, node, null);
    }

    public List<DN> eliminateAscendantNodes(Collection<DN> nodes) {
        Set<DN> ascendantNodes = new HashSet<DN>();

        for (DN node : nodes) {
            DN parentNode = node;
            while (parentNode != null && !ascendantNodes.contains(parentNode)) {
                parentNode = getParentNode(parentNode);
                if (parentNode != null) {
                    ascendantNodes.add(parentNode);
                }
            }
        }

        List<DN> res = new ArrayList<DN>();

        for (DN node : nodes) {
            if (!ascendantNodes.contains(node)) {
                res.add(node);
            }
        }

        return res;
    }

    public void populateWithAscendants(Collection<? super DN> coll, DN node) {
        DN parentNode = node;

        while (parentNode != null) {
            parentNode = getParentNode(parentNode);
            if (parentNode != null) {
                coll.add(parentNode);
            }
        }
    }
}
