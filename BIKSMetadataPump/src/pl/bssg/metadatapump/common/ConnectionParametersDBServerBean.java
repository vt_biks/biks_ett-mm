/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import simplelib.BeanWithIntIdAndNameBase;

/**
 *
 * @author tflorczak
 */
public class ConnectionParametersDBServerBean extends BeanWithIntIdAndNameBase {

    public String server;
    public String instance;
    public String user;
    public String password;
    public boolean isActive;
    public Integer port;
    public String databaseFilter;
    public String objectFilter;
    public int treeId;
    public String databaseList;
    // dla DQM connections
    public String databaseName;
}
