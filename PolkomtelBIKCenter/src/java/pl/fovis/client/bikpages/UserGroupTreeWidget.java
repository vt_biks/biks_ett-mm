/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.treeandlist.GroupBeanExtractor;
import pl.fovis.client.treeandlist.IBean4TreeExtractor;
import pl.fovis.client.treeandlist.UserGroupLazyRowLoader;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.foxygwtcommons.ITreeBroker;
import pl.fovis.foxygwtcommons.treeandlist.ILazyRowLoaderEx;
import pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase;
import static pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase.ICON_FIELD_NAME;
import static pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase.ID_FIELD_NAME;
import static pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase.NAME_FIELD_NAME;
import static pl.fovis.foxygwtcommons.treeandlist.MapTreeRowAndBeanStorageBase.PARENT_ID_FIELD_NAME;

/**
 *
 * @author tflorczak
 */
public class UserGroupTreeWidget extends AbstractBIKTreeWidget<SystemUserGroupBean, String> {

    public UserGroupTreeWidget(String firstColStyleName, IPageDataFetchBroker dataFetchBroker, boolean isWidgetRunFromDialog) {
        super(firstColStyleName, dataFetchBroker, isWidgetRunFromDialog, MapTreeRowAndBeanStorageBase.NAME_FIELD_NAME + "=300:Nazwa");
    }

    @Override
    protected Request getBikFilteredNodes(String val, final Integer optSubTreeRootNodeId,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String filterRequestTicket, AsyncCallback<List<SystemUserGroupBean>> asyncCallback) {
        return BIKClientSingletons.getService().getUserGroupFilteredNodes(val, filterRequestTicket, asyncCallback);
    }

    @Override
    protected Request getBikFilteredNodesWithCondidtion(String val, final Integer optSubTreeRootNodeId,
            Collection<Integer> treeIds, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, String filterRequestTicket, AsyncCallback<List<SystemUserGroupBean>> asyncCallback) {
        // niewykorzystane
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isSubTreeFilteringCapable() {
        return false;
    }

    @Override
    protected MapTreeRowAndBeanStorageBase<String, SystemUserGroupBean> createStorage() {
        return new MapTreeRowAndBeanStorageBase<String, SystemUserGroupBean>(MapTreeRowAndBeanStorageBase.ID_FIELD_NAME, MapTreeRowAndBeanStorageBase.PARENT_ID_FIELD_NAME) {

            @Override
            protected Map<String, Object> convertBeanToRow(SystemUserGroupBean bean) {
                Map<String, Object> row = new HashMap<String, Object>();
                row.put(ID_FIELD_NAME, bean.objId);
                row.put(PARENT_ID_FIELD_NAME, bean.parentId);
                row.put(NAME_FIELD_NAME, bean.name);
//                putColValueWithConvertion(row, bean, NAME_FIELD_NAME, bean.name);
                
                row.put(ICON_FIELD_NAME, bean.isBuiltIn ? "images/userRoleAD.gif":"images/usersObjects.gif");
                return row;
            }
        };
    }

    @Override
    protected ITreeBroker<SystemUserGroupBean, String> createTreeBroker() {
        return new ITreeBroker<SystemUserGroupBean, String>() {
            @Override
            public String getNodeId(SystemUserGroupBean n) {
                return n.objId;
            }

            @Override
            public String getParentId(SystemUserGroupBean n) {
                return n.parentId;
            }
        };
    }

    @Override
    protected ILazyRowLoaderEx<Map<String, Object>, String> createLazyRowLoader(BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode) {
        return new UserGroupLazyRowLoader(treeGrid, storage, optExtraMode, dataFetchBroker, true);
    }

    @Override
    public IBean4TreeExtractor<SystemUserGroupBean, String> createBeanExtractor() {
        return new GroupBeanExtractor();
    }
}
