/*
 * LetterTupleSimilarity.java
 *
 * Created on 28 lipiec 2007, 15:40
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib.similarity;

import commonlib.LameUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author wezyr
 */
public class LetterTupleSimilarity {
    
    private LetterTupleSimilarity() {
    }
    
    public static List<String> makeTuples0(String[] words, int tupleSize, boolean sortedTuples) {
        List<String> res = new ArrayList<String>();
        
        char[] chars = new char[tupleSize];
        
        for (int wIdx = 0; wIdx < words.length; wIdx++) {
            for (int i = 0; i <= words[wIdx].length() - tupleSize; i++) {
                String s;
                if (sortedTuples) {
                    words[wIdx].getChars(i, i + tupleSize, chars, 0);
                    Arrays.sort(chars);
                    s = new String(chars);
                } else
                    s = words[wIdx].substring(i, i + tupleSize);
                res.add(s);
            }
        }
        return res;
    }
    
    public static List<String> makeSortedTuples(String[] words, int tupleSize) {
        List<String> res = new ArrayList<String>();
        
        String oneStr = LameUtils.mergeWithSep(words, " ");
        
        char[] chars = new char[tupleSize];
        
        for (int i = 0; i <= oneStr.length() - tupleSize; i++) {
            oneStr.getChars(i, i + tupleSize, chars, 0);
            Arrays.sort(chars);
            res.add(//oneStr.substring(i, i + tupleSize)
                    new String(chars));
        }
        return res;
    }
    
    public static double calcRank(String[] words1, String[] words2, int tupleSize, boolean sortedTuples) {
        List<String> tuples1 = makeTuples0(words1, tupleSize, sortedTuples);
        List<String> tuples2 = makeTuples0(words2, tupleSize, sortedTuples);
        Collections.sort(tuples1);
        Collections.sort(tuples2);
        
        return LameUtils.countCommonItemsOfSortedLists(tuples1, tuples2) * 2.0 / (tuples1.size() + tuples2.size());
    }
    
    public static double calcRankForStrings(String str1, String str2, int tupleSize, boolean sortedTuples) {
        String[] words1 = LameUtils.buildWordArrayNoSort(str1);
        String[] words2 = LameUtils.buildWordArrayNoSort(str2);
        return calcRank(words1, words2, tupleSize, sortedTuples);
    }
    
    protected static void testStrings(String str1, String str2, int tupleSize) {
        System.out.println(str1 + " =?(" + tupleSize + ")?= " + str2 + " :=-> " + calcRankForStrings(str1, str2, tupleSize, false));
    }
    
    public static void main(String[] args) {
        testStrings("Mieczysław Szcześniak", "Szcześniak Mieczysław", 2);
        testStrings("Mieczysław Szcześniak", "Mieczysła Szcześniak", 2);
        testStrings("Mieczysław Szcześniak", "Mieczysła Szcześniak", 3);
        testStrings("Mieczysław Szcześniak", "Mieciu Szcześniak", 2);
        testStrings("Mieczysław Szcześniak", "Szcześniak Mieczysław", 3);
        
        testStrings("Mary Jane Blige", "Mary J. Blige", 2);
        testStrings("Mary J. Blige", "Mary Blige", 2);
        testStrings("Mary Jane Blige", "Mary Blige", 2);
        
        testStrings("Mary Jane Blige", "Mary J. Blige", 3);
        testStrings("Mary J. Blige", "Mary Blige", 3);
        testStrings("Mary Jane Blige", "Mary Blige", 3);
    }
}
