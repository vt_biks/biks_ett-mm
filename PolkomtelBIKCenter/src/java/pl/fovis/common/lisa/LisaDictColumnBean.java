/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

import java.io.Serializable;
import java.util.LinkedList;

/**
 *
 * @author tbzowka
 */
public class LisaDictColumnBean implements Serializable {

    public Integer id;
    public Integer columnId;
    public Integer sequence;
    public String nameInDb;
    public String description;
    public String titleToDisplay;
    public Integer accessLevel;
    public int validateLevel;
    public String dataType;
    public Boolean isMainKey;
    public Boolean isLatin;
    public Integer columnLength;
    public String nullable;
    public Integer decimalTotalDigits;
    public Integer decimalFractionalDigits;
    public String dictionaryName;
    public String dictValidation;
    public String colValidation; // join dictValidation on dictValidation.colValidation = ?.nameInDb... np. joinujemy po id
    public String textDisplayed; // nazwa, ktora wyswietlamy
    public LinkedList<String> choices;
    public String mappedName;
    public int sqlDataType;
    public String defaultValue;

    public boolean isReadOnlyAccess() {
        return LisaConstants.READ_ONLY.equals(accessLevel);
    }

    public boolean isNotVisibleAccess() {
        return LisaConstants.NOT_VISIBLE.equals(accessLevel);
    }
}
