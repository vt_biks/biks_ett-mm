package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author AMamcarz
 */
public class FindBean implements Serializable {

    public String text;
    public String type;
    public String id;
    public String kind;

    public FindBean() {
    }

    public FindBean(String text, String type, String id, String kind) {
        this.text = text;
        this.type = type;
        this.id = id;
        this.kind = kind;
    }
}
