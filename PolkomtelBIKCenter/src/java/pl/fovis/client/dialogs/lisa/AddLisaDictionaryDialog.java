/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaDictionaryBean;
import simplelib.BaseUtils;

/**
 *
 * @author pku
 */
public class AddLisaDictionaryDialog extends SerializableLisaDialogBase<LisaDictionaryBean> {

    public TextBox displayNameTb = new TextBox();
    public TextBox dbNameTb = new TextBox();
    public TextBox dbNameRealTb = new TextBox();
    public TextBox dbRelTableTb = new TextBox();
    public TextBox columnRelNameTb = new TextBox();

    @Override
    protected String getDialogCaption() {
        return I18n.daneSlownika.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.importuj.get();
    }

    @Override
    protected void buildMainWidgets() {
        Label displayNameLbl = new Label(I18n.wyswietlanaNazwaSlownika.get());
        Label dbNameLbl = new Label(I18n.nazwaSlownikaWBazieDanych.get());
        Label dbNameRealLbl = new Label(I18n.nazwaSlownikaWBazieDanychFaktyczna.get());
        Label dbRelTableLb1 = new Label(I18n.nazwaTabeliReferencjiWBazieDanych.get());
        Label columnRelNameLb1 = new Label(I18n.nazwaKolumnyReferencji.get());

        Grid grid = new Grid(5, 2);
        int row = 0;
        grid.setWidget(row, 0, displayNameLbl);
        grid.setWidget(row++, 1, displayNameTb);
        grid.setWidget(row, 0, dbNameLbl);
        grid.setWidget(row++, 1, dbNameTb);
        grid.setWidget(row, 0, dbNameRealLbl);
        grid.setWidget(row++, 1, dbNameRealTb);
        grid.setWidget(row, 0, dbRelTableLb1);
        grid.setWidget(row++, 1, dbRelTableTb);
        grid.setWidget(row, 0, columnRelNameLb1);
        grid.setWidget(row++, 1, columnRelNameTb);

        main.add(grid);
    }

    @Override
    protected LisaDictionaryBean createBean() {
        return new LisaDictionaryBean();
    }

    @Override
    protected void customFillControlsBeforeShow() {
        if (!BaseUtils.isStrEmptyOrWhiteSpace(bean.dbName)) {
            dbNameTb.setText(bean.dbName.substring(bean.dbName.indexOf(".") + 1));
        }
    }
}
