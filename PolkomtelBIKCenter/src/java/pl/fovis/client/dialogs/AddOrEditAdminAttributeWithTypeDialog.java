/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.AttrHintBean;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.SimpleDateUtils;

/**
 *
 * @author beata
 */
public class AddOrEditAdminAttributeWithTypeDialog extends AddOrUpdateItemNameDialog {

    protected IParametrizedContinuation<AttributeBean> saveCont;
    protected ListBox attrTypeListBox = new ListBox();
    protected static Map<String, String> nameToValAT = new LinkedHashMap<String, String>();
    protected TextBox optValueTb = new TextBox();
    protected ListBox categoryListBox;
    protected Map<Integer, Integer> listboxCategoryId = new HashMap<Integer, Integer>();
    protected Map<Integer, String> category;
    protected AttributeBean ab;
    protected List<AttrHintBean> actualAttributes;
    protected CheckBox displayAsNumberCb;
    protected CheckBox useInDroolsCb;
    protected VerticalPanel optVpToDefinedValue = new VerticalPanel();

    public void buildAndShowDialog(Map<Integer, String> category, String dialogCaption, AttributeBean ab, List<AttrHintBean> actualAttributes, IParametrizedContinuation<AttributeBean> saveCont) {
        this.actualAttributes = actualAttributes;
        this.category = category;
        this.saveCont = saveCont;
        this.ab = ab;
        this.name = dialogCaption;
        super.buildAndShowDialog();
    }

    public enum AttributeType {

        shortText(I18n.krotkiTekst.get() /* I18N:  */),
        longText(I18n.HTMLText.get() /* I18N:  */),
        ansiiText(I18n.dlugiTekst.get()),
        hyperlinkInMulti(I18n.linkWewnetrzny.get() /* I18N:  */),
        hyperlinkInMono(I18n.linkWewnetrznyPojedynczy.get() /* I18N:  */),
        hyperlinkOut(I18n.linkZewnetrzny.get() /* I18N:  */),
        data(I18n.data.get() /* I18N:  */),
        combobox(I18n.poleJednokrotnegoWyboru.get() /* I18N:  */),
        checkbox(I18n.poleWielokrotnegoWyboru.get() /* I18N:  */),
        comboBooleanBox(I18n.wartoscLogiczna.get()),
        calculation(I18n.wartoscWyliczana.get()),
        javascript(I18n.javascript.get()),
        selectOneJoinedObject(I18n.selectOneJoinedObject.get() /* I18N:  */),
        sql(I18n.sql.get() /* I18N:  */),
        permissions(I18n.uprawnienia.get()),
        datetime(I18n.dataIgodzina.get());
        public String caption;

        AttributeType(String c) {
            this.caption = c;
        }
    }

    static {
        for (AttributeType e : AttributeType.values()) {
            nameToValAT.put(e.caption, e.name());
        }
    }

    @Override
    protected void doAction() {
        if (!tbValue.getValue().equals("")) {
            AttributeBean atrb = new AttributeBean();
            if (!listboxCategoryId.isEmpty()) {
                atrb.categoryId = listboxCategoryId.get(categoryListBox.getSelectedIndex());
            }
            atrb.atrName = tbValue.getValue();
            atrb.typeAttr = getSelectedAttrType(); //nameToValAT.get(attrTypeListBox.getItemText(attrTypeListBox.getSelectedIndex()));
            atrb.valueOpt = (//atrb.typeAttr.equals(AttributeType.checkbox.name()) || atrb.typeAttr.equals(AttributeType.combobox.name())
                    isCheckboxOrComboBoxType(atrb.typeAttr) || isBooleanType(atrb.typeAttr)) || isDateType(atrb.typeAttr) ? BIKClientSingletons.mergeWithSepExAndTrim(optValueTb.getText()) : "";

            atrb.displayAsNumber = isShortTextType(atrb.typeAttr) ? displayAsNumberCb.getValue() : false;
            atrb.usedInDrools = BIKClientSingletons.isUseDrools() ? useInDroolsCb.getValue() : false;
            optDoAction(atrb);
            saveCont.doIt(atrb);
        }
    }

    protected void optDoAction(AttributeBean atrb) {

    }

    @Override
    protected boolean doActionBeforeCloseInner() {

        //ww: pachnie to c&p z AttrOrRoleEditDialog
        //ww: pachnie to c&p z AddOrUpdateItemNameDialog
        //ww: pachnie to c&p z AddOrEditAdminAttributeWithTypeDialog
//        String newCaption = tbValue.getValue();
        //String type = nameToValAT.get(attrTypeListBox.getItemText(attrTypeListBox.getSelectedIndex()));
        if (isCheckOrComboOrBooleanBoxOrDateType() && BaseUtils.isStrEmptyOrWhiteSpace(optValueTb.getText())) {
            errorMssg.setHTML(I18n.podajWartosciAtrybutu.get() /* I18N:  */ + ".");
            return false;
        }

        if (isDateType()) {
            List<String> l = BaseUtils.splitBySep(BIKClientSingletons.mergeWithSepExAndTrim(optValueTb.getText()), ",");
            if (l.size() != 2) {
                return false;
            }
            try {
                if (SimpleDateUtils.parseFromCanonicalString(l.get(0)) == null || (SimpleDateUtils.parseFromCanonicalString(l.get(1)) == null)) {
                    return false;
                }
            } catch (Exception ex) {
                return false;
            }
        }

        if (isBooleanType() && (BaseUtils.isStrEmptyOrWhiteSpace(optValueTb.getText()) || BaseUtils.splitBySep(optValueTb.getText(), ",", true).size() != 2)) {
            errorMssg.setHTML(I18n.podajWartosciAtrybutuBoolean.get() /* I18N:  */ + ".");
            return false;
        }
        return isAttributeAlreadyExists();
    }

    protected boolean isAttributeAlreadyExists() {
        String newCaption = tbValue.getValue();
        if (ab == null || !ab.atrName.equals(newCaption)) {
            for (AttrHintBean attrHintBean : actualAttributes) {
                String newCaptionLowerCased = newCaption.toLowerCase();
                String attrName = attrHintBean.name;
                //ww: tłumaczenie tu nie może być, bo nie tłumaczymy atr. dodatkowych!
//                String attrNameTranslated = BIKClientSingletons.getTranslatedAttributeName(attrName);
                if ((attrName.toLowerCase().equals(newCaptionLowerCased) /*|| attrNameTranslated.toLowerCase().equals(newCaptionLowerCased)*/)
                        && attrHintBean.isBuiltIn == false
                        && !attrHintBean.isDeleted) {
                    errorMssg.setHTML(I18n.atrybutOPodanejNazwieJuzIstnieje.get() /* I18N:  */ + ".");
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void buildMainWidgets() {

        main.add(errorMssg = new HTML());
        errorMssg.setStyleName("status" /* i18n: no:css-class */);
        boolean isEditAttr = (ab != null);

        tbValue = addTextBoxAndsetStyle(isEditAttr ? ab.atrName : "");

        if (isAttributeInCategory()) {
            fillCategory();
        }

        main.add(addLabelAndsetLabelStyle(I18n.nazwa.get() /* I18N:  */ + ":"));
        main.add(tbValue);
        main.add(addLabelAndsetLabelStyle(I18n.typ.get() /* I18N:  */ + ":"));

        attrTypeListBox.setStyleName("addOUpItemTb");
        attrTypeListBox.setWidth("260px");

        fillAttrLbx(isEditAttr);

        //opcjonalne dla list
        //String nameSelected = nameToValAT.get(attrTypeListBox.getItemText(attrTypeListBox.getSelectedIndex()));
        final HTML valueOptlb = addHTMLAndsetStyle(isBooleanType() ? I18n.podajWartosciAtrybutuDlaWartosciLogicznej.get() : I18n.podajWartosciAtrybutuPoPrzecink.get());
//        boolean isOptVisible = isCheckboxOrComboBoxType(nameSelected);

//        valueOptlb.setVisible(isOptVisible);
        optValueTb = addTextBoxAndsetStyle(isEditAttr ? ab.valueOpt : "");
//        valueOptTb.setVisible(isOptVisible);

        final VerticalPanel checkOrComboOrBooleanBoxPanel = new VerticalPanel();
        checkOrComboOrBooleanBoxPanel.setHeight("50px");
        checkOrComboOrBooleanBoxPanel.add(valueOptlb);
        checkOrComboOrBooleanBoxPanel.add(optValueTb);
        checkOrComboOrBooleanBoxPanel.setVisible(isCheckOrComboOrBooleanBoxOrDateType());

        displayAsNumberCb = new CheckBox(I18n.wyswietlajJakoLiczbe.get());
        displayAsNumberCb.setVisible(isShortTextType(getSelectedAttrType()));
        displayAsNumberCb.setValue(isEditAttr ? ab.displayAsNumber : false);

        useInDroolsCb = new CheckBox(I18n.uzywanyWDrools.get());
        useInDroolsCb.setVisible(BIKClientSingletons.isUseDrools());
        useInDroolsCb.setValue(isEditAttr ? ab.usedInDrools : false);

        attrTypeListBox.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
//                String nameSelected = nameToValAT.get(attrTypeListBox.getItemText(attrTypeListBox.getSelectedIndex()));
//                boolean isOptVisible = isCheckboxOrComboBoxType(nameSelected);
                checkOrComboOrBooleanBoxPanel.setVisible(isCheckOrComboOrBooleanBoxOrDateType());
                String hint = "";
                if (isBooleanType()) {
                    hint = I18n.podajWartosciAtrybutuDlaWartosciLogicznej.get();
                } else if (isDateType()) {
                    hint = I18n.podajZakresDat.get();
                } else {
                    hint = I18n.podajWartosciAtrybutuPoPrzecink.get();
                }
                valueOptlb.setHTML(hint);
//                valueOptlb.setVisible(isOptVisible);
//                valueOptTb.setVisible(isOptVisible);
                displayAsNumberCb.setVisible(isShortTextType(getSelectedAttrType()));
                setValueOptVisible();
            }
        });

        attrTypeListBox.setVisibleItemCount(8);
//        attrTypeListBox.setVisibleItemCount(AttributeType.values().length);
        main.add(attrTypeListBox);
        main.add(checkOrComboOrBooleanBoxPanel);

        optTxbToDefinedValue(isEditAttr ? ab.valueOpt : "", isEditAttr ? ab.valueOptTo : "");
        main.add(displayAsNumberCb);
        main.add(useInDroolsCb);

        // if(attrTypeListBox.g)
    }

    protected boolean isAttributeInCategory() {
        return true;
    }

    protected void fillCategory() {
        boolean isEditAttr = (ab != null);
        Label categoryLb;
        main.add(categoryLb = addLabelAndsetLabelStyle(I18n.wybierzKategorie.get() /* I18N:  */ + ":"));
        categoryLb.setVisible(isEditAttr);

        main.add(categoryListBox = new ListBox());
        categoryListBox.setStyleName("addOUpItemTb");
        categoryListBox.setWidth("260px");
        categoryListBox.setVisible(isEditAttr);
        if (isEditAttr) {
            int i = 0;
            for (Map.Entry<Integer, String> c : category.entrySet()) {
                categoryListBox.addItem(c.getValue());
                if (ab.categoryId == c.getKey()) {
                    categoryListBox.setSelectedIndex(i);
                }
                listboxCategoryId.put(i, c.getKey());
                i++;
            }
        }
    }

    protected void fillAttrLbx(boolean isEditAttr) {
        int i = 0;
        boolean selected = false;
        for (AttributeType e : AttributeType.values()) {
            attrTypeListBox.addItem(e.caption);
            if (isEditAttr && ab != null && (e.caption).equals(ab.typeAttr.trim())) {
                attrTypeListBox.setSelectedIndex(i);
                selected = true;
            }
            i++;
        }
        if (!selected) {
            attrTypeListBox.setSelectedIndex(0);
        }
    }

    public static String getSelectedAttrType(String name) {
        return nameToValAT.get(name);
    }

    protected String getSelectedAttrType() {
        return getSelectedAttrType(attrTypeListBox.getItemText(attrTypeListBox.getSelectedIndex()));
    }

    protected boolean isCheckOrComboOrBooleanBoxOrDateType() {
        return isCheckboxOrComboBoxType() || isBooleanType() || isDateType();
    }

    protected boolean isCheckboxOrComboBoxType() {
        String attrType = getSelectedAttrType();
        return isCheckboxOrComboBoxType(attrType);
    }

    protected boolean isCheckboxOrComboBoxType(String name) {
        return BIKCenterUtils.isCheckboxOrComboBoxType(name);
    }

    protected boolean isShortTextType(String name) {
        return BIKCenterUtils.isShortTextType(name);
    }

    protected boolean isBooleanType() {
        String attrType = getSelectedAttrType();
        return isBooleanType(attrType);
    }

    protected boolean isDateType() {
        String attrType = getSelectedAttrType();
        return isDateType(attrType);
    }

    protected boolean isDateType(String attrType) {
        return BIKCenterUtils.isDateType(attrType);
    }

    protected boolean isBooleanType(String attrType) {
        return BIKCenterUtils.isBooleanType(attrType);
    }

    protected Label addLabelAndsetLabelStyle(String name) {
        Label label = new Label(name);
        label.setStyleName("addAttrLbl");
        return label;
    }

    protected HTML addHTMLAndsetStyle(String name) {
        HTML html = new HTML(name);
        html.setStyleName("addAttrLbl");
        return html;
    }

    protected TextBox addTextBoxAndsetStyle(String value) {
        TextBox tb = new TextBox();
        tb.setStyleName("addOUpItemTb");
        tb.setValue(value);
        tb.setWidth("250px");
        return tb;
    }

    protected void optTxbToDefinedValue(String valueFrom, String valueTo) {

    }

    protected void setValueOptVisible() {
        optVpToDefinedValue.setVisible(false);
    }
}
