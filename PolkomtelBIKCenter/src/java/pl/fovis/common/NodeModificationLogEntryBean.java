/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import simplelib.BeanWithIntIdBase;

/**
 *
 * @author pmielanczuk
 */
public class NodeModificationLogEntryBean extends BeanWithIntIdBase {

    public String name;
    public boolean isDeleted;
    public String branchIds;

    public NodeModificationLogEntryBean() {
    }

    public NodeModificationLogEntryBean(int id, String name, boolean isDeleted, String branchIds) {
        this.id = id;
        this.name = name;
        this.isDeleted = isDeleted;
        this.branchIds = branchIds;
    }
}
