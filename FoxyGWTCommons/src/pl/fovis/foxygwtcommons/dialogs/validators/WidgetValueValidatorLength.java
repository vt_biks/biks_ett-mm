/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs.validators;

import com.google.gwt.user.client.ui.Widget;
import pl.fovis.foxygwtcommons.dialogs.FoxyDialogSerializer;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author pmielanczuk
 */
public class WidgetValueValidatorLength<BB, INB extends BB, OUTB extends BB> implements IWidgetValueValidator {

    protected final Widget inputControl;
    protected Integer minLength;
    protected Integer maxLength;
    protected final FoxyDialogSerializer<BB, INB, OUTB> serializer;

    public WidgetValueValidatorLength(final FoxyDialogSerializer<BB, INB, OUTB> serializer, Widget inputControl, Integer minLength, Integer maxLength) {
        this.serializer = serializer;
        this.inputControl = inputControl;
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    protected String getValueTooShort(Object value, String valStr, int len) {
        return I18n.wymaganePrzynajmniej.get() /* I18N:  */+ " " + minLength + " " +I18n.znakowWprowadzono.get() /* I18N:  */+ ": " + len;
    }

    protected String getValueTooLong(Object value, String valStr, int len) {
        return I18n.dopuszczalneNajwyzej.get() /* I18N:  */+ " " + maxLength + " " +I18n.znakowWprowadzono.get() /* I18N:  */+ ": " + len;
    }

    public String validate() throws FoxyGWTValidationException {
        Object value = serializer.getInputControlValue(inputControl);
        String valStr = BaseUtils.safeToString(value);
        int len = BaseUtils.strLengthFix(valStr);
        if (minLength != null && minLength > len) {
            return getValueTooShort(value, valStr, len);
        }
        if (maxLength != null && maxLength < len) {
            return getValueTooLong(value, valStr, len);
        }
        return null;
    }
}
