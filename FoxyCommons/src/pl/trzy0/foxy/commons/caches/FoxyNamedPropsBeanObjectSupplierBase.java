/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.commons.caches;

import java.util.Set;
import simplelib.BaseUtils;
import simplelib.INamedPropsBean;

/**
 *
 * @author wezyr
 */
public abstract class FoxyNamedPropsBeanObjectSupplierBase<K, V extends INamedPropsBean>
        extends FoxyObjectSupplierBase<K, V> implements IFoxyObjectSupplier<K, V> {

    protected String primaryKeyName;
    protected Set<String> altKeyNames;
    protected Set<String> keysUniqueOnNull;

    public FoxyNamedPropsBeanObjectSupplierBase(String primaryKeyName,
            Set<String> altKeyNames, Set<String> keysUniqueOnNull) {
        this.primaryKeyName = primaryKeyName;
        this.altKeyNames = altKeyNames;
        this.keysUniqueOnNull = keysUniqueOnNull;
    }

    public FoxyNamedPropsBeanObjectSupplierBase(String primaryKeyName,
            String altKeyNames, String keysUniqueOnNull) {
        this(primaryKeyName, BaseUtils.splitUniqueBySep(altKeyNames, ","),
                BaseUtils.splitUniqueBySep(keysUniqueOnNull, ","));
    }

    @Override
    @SuppressWarnings("unchecked")
    public K getKeyValue(V obj) {
        return (K)obj.getPropValue(primaryKeyName);
    }

    public abstract V getByKey(K key);

    @Override
    public boolean isNullUniqueForKey(String keyName) {
        return keysUniqueOnNull != null && keysUniqueOnNull.contains(keyName);
    }

    @Override
    public Set<String> getAlternativeKeyNames() {
        return altKeyNames;
    }

    @Override
    public Object getObjectKeyValue(V obj, String keyName) {
        return obj.getPropValue(keyName);
    }
}
