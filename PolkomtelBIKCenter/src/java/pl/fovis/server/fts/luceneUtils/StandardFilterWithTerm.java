/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts.luceneUtils;

import commonlib.LameUtils;
import java.io.IOException;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.ArrayUtil;
import org.apache.lucene.util.RamUsageEstimator;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public class StandardFilterWithTerm extends TokenFilter {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    public char[] tokenChars;
    public int tokenLength;
    protected CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);

    public StandardFilterWithTerm(TokenStream in) {
        super(in);
    }

    @Override
    final public boolean incrementToken() throws IOException {
        boolean hasNext = input.incrementToken();

        if (hasNext) {
            if (logger.isDebugEnabled()) {
                logger.debug("incrementToken():");
            }
            lowerCaseCurrentToken();
        } else {
            tokenLength = -1;
        }

        return hasNext;
    }

    protected void lowerCaseCurrentToken() {
        tokenLength = termAtt.length();

        if (logger.isDebugEnabled()) {
            logger.debug("lowerCaseCurrentToken: termAtt=" + termAtt.toString() + ", tokenLength=" + tokenLength);
        }

        tokenChars = adjustToNewLength(tokenChars, tokenLength);
        toLowerCase(termAtt.buffer(), tokenLength, tokenChars);

        if (logger.isDebugEnabled()) {
            String s = new String(tokenChars, 0, tokenLength);
            logger.debug("lowerCaseCurrentToken: tokenChars=" + s + ", length=" + s.length());
        }
    }

    @Override
    public void reset() throws IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("reset()");
        }
        tokenLength = -1;
        super.reset();
    }

    public static char[] adjustToNewLength(char[] chars, int newLength) {
        if (chars == null || newLength > chars.length) {
            chars = new char[ArrayUtil.oversize(newLength, RamUsageEstimator.NUM_BYTES_CHAR)];
        }
        return chars;
    }

    // dstChars musi mieć przynajmniej length znaków, dstChars może być tą samą
    // tablicą znaków co srcChars - wtedy jest podmianka w miejscu
    public static void toLowerCase(char[] srcChars, int length, char[] dstChars) {
        for (int i = 0; i < length;) {
            i += Character.toChars(Character.toLowerCase(
                    Character.codePointAt(srcChars, i, length)),
                    dstChars, i);
        }
    }
}
