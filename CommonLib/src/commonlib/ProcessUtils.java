/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import simplelib.LameRuntimeException;

/**
 *
 * Testowane tylko pod Windowsem
 *
 * @author tflorczak
 */
public class ProcessUtils {

    private static final String TASKLIST = "tasklist";
    private static final String KILL = "taskkill /F /IM ";

    public static Process executeProcess(String[] callAndArgs, String[] envs) {
        try {
            return Runtime.getRuntime().exec(callAndArgs, envs);
        } catch (IOException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    public static Process executeProcess(String[] callAndArgs) {
        try {
            return Runtime.getRuntime().exec(callAndArgs);
        } catch (IOException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    public static boolean isProcessRunning(String serviceName) {
        try {
            Process p = Runtime.getRuntime().exec(TASKLIST);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(serviceName)) {
                    return true;
                }
            }
            return false;
        } catch (IOException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    public static void killProcessByName(String serviceName) {
        try {
            Runtime.getRuntime().exec(KILL + serviceName);
        } catch (IOException ex) {
            throw new LameRuntimeException(ex);
        }
    }

//    public static void main(String[] args) {
//        killProcessByName("chrome.exe");
//    }
}
