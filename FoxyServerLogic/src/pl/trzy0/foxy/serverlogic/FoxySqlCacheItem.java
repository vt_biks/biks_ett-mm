/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author wezyr
 */
public class FoxySqlCacheItem {

    public String sqlTxt;
    public Set<String> keys;
    public List<Map<String, Object>> result;
    public int duration;
}
