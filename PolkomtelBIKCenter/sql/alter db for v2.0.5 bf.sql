exec sp_check_version '2.0.5';
GO

if not exists (select * from bik_attr_def where name in ('metaBiks.Typ atrybutu','metaBiks.Nadpisany typ atrybutu') and value_opt like '%datetime%') 
update bik_attr_def set value_opt=value_opt+',datetime' where name in ('metaBiks.Typ atrybutu','metaBiks.Nadpisany typ atrybutu')

if not exists (select * from bik_attribute where attr_def_id in (select id from bik_attr_def where name in
 ('metaBiks.Typ atrybutu','metaBiks.Nadpisany typ atrybutu'))
 and value_opt like '%datetime%'  and value_opt like '%hyperlinkInMulti%') 
 update bik_attribute set value_opt=value_opt+'datetime,' where attr_def_id in (select id from bik_attr_def where name in
 ('metaBiks.Typ atrybutu','metaBiks.Nadpisany typ atrybutu')) and value_opt like '%hyperlinkInMulti%'


exec sp_update_version '2.0.5','2.0.6';
GO