/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import java.sql.Connection;
import java.sql.Statement;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public abstract class JdbcSingleConnectionBase extends JdbcConnectionBase<Object> {

    private Connection connection;
    private Statement statement;

    public JdbcSingleConnectionBase(Connection conn) {
        this.connection = conn;
    }

    protected Connection getConnection() {
        return connection;
    }

    protected Statement getStatement() {
        if (statement == null) {
            try {
                statement = getConnection().createStatement();
            } catch (Exception ex) {
                throw new LameRuntimeException("cannot create statement", ex);
            }
        }
        return statement;
    }

    public void destroy() {
        try {
            if (statement != null) {
                statement.close();
                statement = null;
            }
            connection.close();
            connection = null;
        } catch (Exception ex) {
            throw new LameRuntimeException("error in connection close", ex);
        }
    }

    @Override
    protected void releaseStatement(Statement st) {
        // no-op
    }
}
