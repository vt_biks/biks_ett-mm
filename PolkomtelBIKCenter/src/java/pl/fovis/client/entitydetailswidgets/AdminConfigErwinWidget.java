/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.ConnectionParametersErwinBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author pgajda
 */
public class AdminConfigErwinWidget extends AdminConfigBIKBaseWidget<ConnectionParametersErwinBean> {

    protected CheckBox chbIsActive;
    protected PushButton btnSave;
    protected TextBox tbxPath;
    protected Label lblPath;

    @Override
    protected void buildInnerWidget() {
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
            }
        }, false);
        createLine();
        lblPath = createLabel(I18n.BUILD_allTests.get() + ":");
        tbxPath = createTextBox();
        createLine();
        btnSave = createButton(I18n.zapisz.get() /* I18N:  */, new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        ConnectionParametersErwinBean bean = new ConnectionParametersErwinBean();
                        bean.isActive = chbIsActive.getValue() ? 1 : 0;
                        bean.path = tbxPath.getText();
                        BIKClientSingletons.getService().setConnectionParametersErwinBean(bean, new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " setConnectionParametersErwinBean") {
                            @Override
                            public void onSuccess(Void result) {
                                BIKClientSingletons.showInfo(I18n.zmienionParametrDoPolaczenErwinDM.get() /* I18N:  */);
                            }
                        });
                    }
                });

    }

    @Override
    public void populateWidgetWithData(ConnectionParametersErwinBean data) {
        tbxPath.setText(data.path);
        chbIsActive.setValue(data.isActive == 1);
        setWidgetsEnabled(chbIsActive.getValue());
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_ERWIN_DATAMODEL;
    }
}
