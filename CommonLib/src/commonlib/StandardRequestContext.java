/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.util.Map;

/**
 *
 * @author wezyr
 */
public class StandardRequestContext implements IRequestContext {

    public static final String EMPTY_METHOD_NAME = "";
    protected String method;
    protected String remoteAppUrlBase;
    protected String localAppUrlBase;
    protected String remoteResourceUrlBase;
    protected String localResourceUrlBase;
    protected Map<String, Object> params;
    protected IOutboundUrlRewriter optUrlRewriter;

    public StandardRequestContext(String canonicalAppUrlBase,
            String canonicalResourceUrlBase) {
        this(EMPTY_METHOD_NAME, canonicalAppUrlBase, canonicalResourceUrlBase);
    }

    public StandardRequestContext(String canonicalAppUrlBase,
            String canonicalResourceUrlBase, Map<String, Object> params,
            IOutboundUrlRewriter optUrlRewriter) {
        this(EMPTY_METHOD_NAME, canonicalAppUrlBase, canonicalResourceUrlBase, params);
        setOptUrlRewriter(optUrlRewriter);
    }

    public StandardRequestContext(String method, String canonicalAppUrlBase,
            String canonicalResourceUrlBase, Map<String, Object> params) {
        this(method, canonicalAppUrlBase, canonicalAppUrlBase,
                canonicalResourceUrlBase, canonicalResourceUrlBase, params);
    }

    public StandardRequestContext(String method, String canonicalAppUrlBase,
            String canonicalResourceUrlBase) {
        this(method, canonicalAppUrlBase, canonicalResourceUrlBase, null);
    }

    public StandardRequestContext(String method,
            String localAppUrlBase, String remoteAppUrlBase,
            String localResourceUrlBase, String remoteResourceUrlBase,
            Map<String, Object> params) {
        this.method = method;
        this.localAppUrlBase = localAppUrlBase;
        this.remoteAppUrlBase = remoteAppUrlBase;
        this.localResourceUrlBase = localResourceUrlBase;
        this.remoteResourceUrlBase = remoteResourceUrlBase;
        this.params = params;
        //dump("cons");
    }

//    private void dump(String msg) {
//        System.out.println("%%%%% " + msg + " %%%%%\n"
//                + "localAppUrlBase=" + localAppUrlBase
//                + ", remoteAppUrlBase=" + remoteAppUrlBase
//                + ", localResourceUrlBase=" + localResourceUrlBase
//                + ", remoteResourceUrlBase" + remoteResourceUrlBase);
//    }
    public String getRequestMethod() {
        return method;
    }

    public String encodeUrl(String url) {
        String res = optUrlRewriter == null ? url : optUrlRewriter.rewriteOutgoingUrl(url);
//        System.out.println("@@@@@@@@@@@@ encodeUrl: old=" + url
//                + ", new=" + res + ", has urlrewriter: " + (optUrlRewriter != null));
        return res;
    }

    public String getServletUrlBase(boolean forRemoteURL) {
        //dump("getServletUrlBase(" + forRemoteURL + ")");
        return forRemoteURL ? remoteAppUrlBase : localAppUrlBase;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public String getResourceUrlBase(boolean forRemoteURL) {
        //dump("getResourceUrlBase(" + forRemoteURL + ")");
        return forRemoteURL ? remoteResourceUrlBase : localResourceUrlBase;
    }

    public void setOptUrlRewriter(IOutboundUrlRewriter optUrlRewriter) {
        this.optUrlRewriter = optUrlRewriter;
    }

    public boolean getUsesSubdomains() {
        return false;
    }
}
