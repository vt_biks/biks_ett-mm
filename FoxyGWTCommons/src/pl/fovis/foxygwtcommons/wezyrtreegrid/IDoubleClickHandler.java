/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.foxygwtcommons.wezyrtreegrid;

/**
 *
 * @author mgraczkowski
 */
public interface IDoubleClickHandler<ROW> {
    public void rowDoubleClicked(ROW row);
}
