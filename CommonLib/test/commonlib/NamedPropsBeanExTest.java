/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package commonlib;

import junit.framework.TestCase;
import simplelib.BaseUtils;
import simplelib.INamedPropsBeanEx;
import simplelib.NamedPropsRenameStrategy;

/**
 *
 * @author wezyr
 */
public class NamedPropsBeanExTest extends TestCase {
    
    public NamedPropsBeanExTest(String testName) {
        super(testName);
    }

    public static class T {
        public int x;
        public String y;
    }

    public static class T2 {
        public long x;
        public String y;
    }

    public void testOne() {
        T t1 = new T();
        t1.x = 10;
        t1.y = "aaa";
        INamedPropsBeanEx be1 = new NamedPropsBeanEx(t1);
        System.out.println("t1=" + BaseUtils.namedPropsBeanToMap(be1));

        T2 t2 = new T2();
        NamedPropsRenameStrategy renameStrategy = new NamedPropsRenameStrategy();
        NamedPropsRenameStrategy.copyProps(be1, new NamedPropsBeanEx(t2), renameStrategy);
        System.out.println("t2=" + BaseUtils.namedPropsBeanToMap(new NamedPropsBeanEx(t2)));
    }
}
