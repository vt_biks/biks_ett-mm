USE [boxi]
GO

-- select * from aaa_entity_in_tree_node

drop table [aaa_entity_in_tree_node]
go

drop table aaa_tree_node
go


/****** Object:  Table [dbo].[aaa_tree_node]    Script Date: 03/27/2011 11:42:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[aaa_tree_node](
	[id] [int] IDENTITY(-1,-1) NOT NULL,
	[item_si_id] [int] NULL,
	[parent_id] [int] NULL,
	[id_path] [varchar](800) NULL,
	[caption] [varchar](max) NOT NULL,
	[tree_num] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[aaa_tree_node]  WITH CHECK ADD FOREIGN KEY([parent_id])
REFERENCES [dbo].[aaa_tree_node] ([id])
GO




USE [boxi]
GO

/****** Object:  Table [dbo].[aaa_entity_in_tree_node]    Script Date: 03/27/2011 11:43:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[aaa_entity_in_tree_node](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[si_id] [int] NULL,
	[tree_node_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uk_aaa_entity_in_tree_node] UNIQUE NONCLUSTERED 
(
	[si_id] ASC,
	[tree_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[aaa_entity_in_tree_node]  WITH CHECK ADD FOREIGN KEY([tree_node_id])
REFERENCES [dbo].[aaa_tree_node] ([id])
GO



CREATE TABLE [dbo].[aaa_aux_tree_node](
	[id] [int] NOT NULL primary key,
	[item_si_id] [int] NULL,
	[parent_id] [int] NULL references aaa_aux_tree_node(id),
	[id_path] [varchar](800) NULL,
	[caption] [varchar](max) NOT NULL,
	[tree_num] [int] NOT NULL
	)

GO
