/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author tflorczak
 */
public class PumpConstants {

    public static final String PUMP_GROUP_BO = "sapbo";
    //---
    public static final String PUMP_DATA_LOAD_SUCCESS = "Success";
    public static final String PUMP_TEST_CONNECTION_OK = "OK";
    //---
    public static final int LOG_LOAD_STATUS_DONE = 1;
    public static final int LOG_LOAD_STATUS_ERROR = 2;
    public static final int LOG_LOAD_STATUS_STARTED = 0;
    //---
    public static final int TEST_CONNECTION_SUCCESS = 0;
    public static final int TEST_CONNECTION_FAIL = 1;
    //---
    public static final String SOURCE_NAME_SAPBO = "Business Objects 3.1";
    public static final String SOURCE_NAME_SAPBO4 = "Business Objects 4.x";
    public static final String SOURCE_NAME_TERADATA = "Teradata DBMS";
    public static final String SOURCE_NAME_TERADATA_DATAMODEL = "Teradata Data Model";
    public static final String SOURCE_NAME_DQC = "DQC";
    public static final String SOURCE_NAME_MSSQL = "MS SQL";
    public static final String SOURCE_NAME_JDBC = "JDBC";
    public static final String SOURCE_NAME_AD = "Active Directory";
    public static final String SOURCE_NAME_SAS = "SAS";
    public static final String SOURCE_NAME_ORACLE = "Oracle";
    public static final String SOURCE_NAME_SAPBW = "SAP BW";
    public static final String SOURCE_NAME_ERWIN_DATAMODEL = "Erwin Data Model";
    public static final String SOURCE_NAME_CONFLUENCE = "Confluence";
    public static final String SOURCE_NAME_POSTGRESQL = "PostgreSQL";
    public static final String SOURCE_NAME_PROFILE = "Profile";
    public static final String SOURCE_NAME_FILE_SYSTEM = "File System";
    public static final String SOURCE_NAME_DYNAMIC_AX = "Dynamic AX";
    public static final String SOURCE_NAME_PLAIN_FILE = "Plain File";
    public static final String SOURCE_NAME_BIKS_SQL = "BIKS SQL";
//    public static final String SOURCE_NAME_ATTRIBUTE_USAGE = "Attribute usage";
    // inne moduły
    public static final String SOURCE_NAME_DQM = "DQM";
    public static final String SOURCE_NAME_BIAdmin = "BIAdmin";
    //---
    public static final String GENERIC_NODE_OBJID = "OBJ_ID";
    public static final String GENERIC_NODE_LINKED_OBJID = "LINKED_OBJ_ID";
    public static final String GENERIC_NODE_JOINED_OBJID = "JOINED_OBJ_ID";
    public static final String GENERIC_NODE_PARENT_OBJID = "PARENT_OBJ_ID";
    public static final String GENERIC_NODE_NAME = "NAME";
    public static final String GENERIC_NODE_NODE_KIND_CODE = "NODE_KIND_CODE";
    public static final String GENERIC_NODE_DESCR = "DESCR";
    public static final String GENERIC_NODE_VISUAL_ORDER = "VISUAL_ORDER";
    public static final String GENERIC_NODE_SRC_ID = "SRC_ID";
    public static final String GENERIC_NODE_DST_ID = "DST_ID";
    public static final String GENERIC_IS_INHERIT = "IS_INHERIT";
    public static final String GENERIC_MAIN_ATTRIBUTE = "MAIN_ATTRIBUTE";
    public static final String GENERIC_ATTRIBUTE_SRC_SUFFIX = "_SRC_VALUE";
    public static final String GENERIC_ATTRIBUTE_DST_SUFFIX = "_DST_VALUE";
    public static final String GENERIC_SRC_PREFIX = "SRC_";
    public static final String GENERIC_DST_PREFIX = "DST_";
    public static final String GENERIC_SRC_NAME = GENERIC_SRC_PREFIX + GENERIC_NODE_NAME;
    public static final String GENERIC_DST_NAME = GENERIC_DST_PREFIX + GENERIC_NODE_NAME;
    public static final String GENERIC_SRC_PARENT_OBJID = GENERIC_SRC_PREFIX + GENERIC_NODE_PARENT_OBJID;
    public static final String GENERIC_DST_PARENT_OBJID = GENERIC_DST_PREFIX + GENERIC_NODE_PARENT_OBJID;
    public static final String GENERIC_TYPE = "TYPE";
    public static final String GENERIC_HYPER_LINK_IN_ATTR_NAME = "HYPER_LINK_IN_ATTR_NAME";
    public static final String GENERIC_SRC_OBJID = GENERIC_SRC_PREFIX + GENERIC_NODE_OBJID;
    public static final String GENERIC_DST_OBJID = GENERIC_DST_PREFIX + GENERIC_NODE_OBJID;
    public static final String GENERIC_IS_DELETED = "IS_DELETED";

    public static final String METABIKS_META_ATTR_CATEGORY = "metaBiks";
    public static final String METABIKS_PREFIX = "metaBiks";
    public static final String METABIKS_META_ATTR_PREFIX = "metaBiks.";
    public static final String METABIKS_OBJ_ID_ATTRIBUTE_DEF = "Definicje atrybutów";
    public static final String METABIKS_OBJ_ID_NODE_KIND_DEF = "Definicje obiektów";
    public static final String METABIKS_OBJ_ID_TREE_KIND_DEF = "Definicje drzew";
    public static final String METABIKS_ATTR_TYPE_ATTR = METABIKS_META_ATTR_PREFIX + "Typ atrybutu";
    public static final String METABIKS_ATTR_ATTR_NAME = METABIKS_META_ATTR_PREFIX + "Nazwa atrybutu";
    public static final String METABIKS_ATTR_ATTR_CAT_NAME = METABIKS_META_ATTR_PREFIX + "Nazwa katalogu";
    public static final String METABIKS_ATTR_ATTR_DESC = METABIKS_META_ATTR_PREFIX + "Treść pomocy";
    public static final String METABIKS_ATTR_OVERWRITE_TYPE_ATTR = METABIKS_META_ATTR_PREFIX + "Nadpisany typ atrybutu";
    public static final String METABIKS_ATTR_OVERWRITE_VALUE_OPT = METABIKS_META_ATTR_PREFIX + "Nadpisana lista wartości";
    public static final String METABIKS_ATTR_NODE_KIND_CAPTION_PL = METABIKS_META_ATTR_PREFIX + "Nazwa typu obiektu";
    public static final String METABIKS_ATTR_TREE_KIND_CAPTION_PL = METABIKS_META_ATTR_PREFIX + "Nazwa typu drzewa";
    public static final String METABIKS_ATTR_REGISTRY = METABIKS_META_ATTR_PREFIX + "Rejestr";
    public static final String METABIKS_ATTR_VALUE_OPT = METABIKS_META_ATTR_PREFIX + "Lista wartości";
//    public static final String METABIKS_ATTR_VISUAL_ORDER = METABIKS_META_ATTR_PREFIX + "Kolejność wyświetlenia";
    public static final String METABIKS_ATTR_DISPLAY_AS_NUMBER = METABIKS_META_ATTR_PREFIX + "Wyświetlaj jako liczbę";
    public static final String METABIKS_ATTR_USED_IN_DROOLS = METABIKS_META_ATTR_PREFIX + "Używaj w systemie zarządzania regułami";
    public static final String METABIKS_ATTR_ICON_NAME = METABIKS_META_ATTR_PREFIX + "Ikona";
    public static final String METABIKS_ATTR_NODE_KIND_CODE = METABIKS_META_ATTR_PREFIX + "Kod";
//    public static final String METABIKS_ATTR_IS_FOLDER = METABIKS_META_ATTR_PREFIX + "Folder";
    public static final String METABIKS_ATTR_CHILDREN_KINDS = METABIKS_META_ATTR_PREFIX + "Dzieci w tabeli";
    public static final String METABIKS_ATTR_REPORTS = METABIKS_META_ATTR_PREFIX + "Raporty";
    public static final String METABIKS_ATTR_ATTR_CHILDREN_KINDS = METABIKS_META_ATTR_PREFIX + "Atrybuty dzieci w tabeli";
    public static final String METABIKS_ATTR_ORIGINAL = METABIKS_META_ATTR_PREFIX + "Wzorzec";
    public static final String METABIKS_ATTR_DEFAULT_VALUE = METABIKS_META_ATTR_PREFIX + "Wartość domyślna";
    public static final String METABIKS_ATTR_IS_REQUIRED = METABIKS_META_ATTR_PREFIX + "Pokaż na formatce nowego obiektu";
    public static final String METABIKS_ATTR_IS_PUBLIC = METABIKS_META_ATTR_PREFIX + "Publiczny";
    public static final String METABIKS_ATTR_IS_EMPTY = METABIKS_META_ATTR_PREFIX + "Może być pusty";
    public static final String METABIKS_ATTR_HELP_AUTHOR = METABIKS_META_ATTR_PREFIX + "Treść pomocy dla autora";
    public static final String METABIKS_ATTR_HELP_USER = METABIKS_META_ATTR_PREFIX + "Treść pomocy dla innych";
    public static final String METABIKS_ATTR_ALLOW_LINKING = METABIKS_META_ATTR_PREFIX + "Pozwolić dowiązać";
    public static final String METABIKS_ATTR_BRANCH_TYPE_IN_TREE = METABIKS_META_ATTR_PREFIX + "Typ w drzewie";
    public static final String METABIKS_ATTR_RELATION_TYPE = METABIKS_META_ATTR_PREFIX + "Typ relacji";
    public static final String METABIKS_ATTR_TREE_TYPE = METABIKS_META_ATTR_PREFIX + "Typ drzewa";
    public static final String METAMENU_OBJ_ID = METABIKS_META_ATTR_PREFIX + "Kod w menu";
    public static final String METABIKS_ATTR_STATUS_NAME = METABIKS_META_ATTR_PREFIX + "Status";
    public static final String YES = "Tak";
    public static final String NO = "Nie";
    public static final String BRANCH = "Gałąź";
    public static final String MAIN_BRANCH = "Główna gałąź";
    public static final String LEAF = "Liść";
    public static final String ASSOCIATION = "Powiązanie";
    public static final String LINKING = "Dowiązanie";
    public static final String CHILD = "Dziecko";
    public static final String HYPERLINK = "Hyperlink";

    //---
    public static final int DQC_TEST_SUCCESS = 0;
    public static final int DQC_TEST_FAILED = 1;
    public static final int DQC_TEST_INACTIVE = 2;
    public static final Map<String, Boolean> AD_USER_ATTRIBUTES = new HashMap<String, Boolean>() {
        {
            //(AtributeName, isList)
            put("sAMAccountName", false);
            put("distinguishedName", false);
            put("userPrincipalName", false);
            put("wWWHomePage", false);
            put("url", true);
            put("title", false);
            put("telephoneNumber", false);
            put("streetAddress", false);
            put("st", false);
            put("sn", false);
            put("postOfficeBox", true);
            put("postalCode", false);
            put("physicalDeliveryOfficeName", false);
            put("pager", false);
            put("otherTelephone", true);
            put("otherPager", true);
            put("otherMobile", true);
            put("otherIpPhone", true);
            put("otherFacsimileTelephoneNumber", true);
            put("mobile", false);
            put("manager", false);
//            put("managedObjects", true);
            put("l", false);
            put("ipPhone", false);
            put("initials", false);
            put("homePhone", false);
            put("givenName", false);
            put("facsimileTelephoneNumber", false);
            put("directReports", true);
            put("department", false);
            put("displayName", false);
            put("description", true);
            put("notes", false);
            put("co", false);
            put("c", false);
            // dodane dodatkowo dla PB
            put("mail", false);
            put("company", false);
            //dodane podczsa rozbudowy konektora
            put("logonCount", false);
            put("memberof", true);
            //dodane podczsa kolejnej rozbudowy konektora
            put("extensionAttribute1", false);
            put("extensionAttribute2", false);
            put("extensionAttribute3", false);
            put("extensionAttribute4", false);
            put("extensionAttribute5", false);
        }
    };
    public static final Map<String, Boolean> AD_GROUP_ATTRIBUTES = new HashMap<String, Boolean>() {
        {
            //(AtributeName, isList)
            put("sAMAccountName", false);
            put("sAMAccountType", false);
            put("distinguishedName", false);
            put("mail", false);
            put("description", false);
            put("member", true);
            put("memberof", true);
            put("groupType", false);
            put("objectclass", false);
            put("objectcategory", false);
        }
    };
    public static final Map<String, Boolean> AD_OU_ATTRIBUTES = new HashMap<String, Boolean>() {
        {
            //(AtributeName, isList)
            put("name", false);
            put("distinguishedName", false);
            put("description", false);
            put("objectclass", false);
            put("objectcategory", false);
        }
    };
    public static final Set<String> BO_REPORT_KINDS = new HashSet<String>();
    public static final Set<String> BO_UNIV_KINDS = new HashSet<String>();
    public static final Set<String> BO_CONN_KINDS = new HashSet<String>();

    static {
        // 'Webi', 'CrystalReport', 'Pdf', 'FullClient', 'Rtf', 'Txt', 'Flash', 'Hyperlink', 'Excel', 'Powerpoint', 'Word'
        BO_REPORT_KINDS.add("Webi");
        BO_REPORT_KINDS.add("CrystalReport");
        BO_REPORT_KINDS.add("Pdf");
        BO_REPORT_KINDS.add("FullClient");
        BO_REPORT_KINDS.add("Rtf");
        BO_REPORT_KINDS.add("Txt");
        BO_REPORT_KINDS.add("Flash");
        BO_REPORT_KINDS.add("Hyperlink");
        BO_REPORT_KINDS.add("Excel");
        BO_REPORT_KINDS.add("Powerpoint");
        BO_REPORT_KINDS.add("Word");
        // 'Universe', 'DSL.MetaDataFile'
        BO_UNIV_KINDS.add("Universe");
        BO_UNIV_KINDS.add("DSL.MetaDataFile");
        // 'MetaData.DataConnection', 'CCIS.DataConnection', 'CommonConnection', 'Connection'
        BO_CONN_KINDS.add("MetaData.DataConnection");
        BO_CONN_KINDS.add("CCIS.DataConnection");
        BO_CONN_KINDS.add("CommonConnection");
        BO_CONN_KINDS.add("Connection");
    }
}
