/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.postgres;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import simplelib.FieldNameConversion;
import static pl.bssg.metadatapump.PumpUtils.closeConnection;
import static pl.bssg.metadatapump.PumpUtils.getConnection;
import static pl.bssg.metadatapump.postgresql.PostgreSQLPump.DRIVER_CLASS;
import static pl.bssg.metadatapump.postgresql.PostgreSQLPump.POSTGRES_DEFAULT_DATABASE_NAME;
import pl.trzy0.foxy.commons.IRawConnection;
import pl.trzy0.foxy.commons.PostgresConnectionConfig;
import pl.trzy0.foxy.serverlogic.db.JdbcConnectionBase;
import pl.trzy0.foxy.serverlogic.db.PostgresConnection;

/**
 *
 * @author tflorczak
 */
public class PostgresTest {

    public static void main(String[] args) {
//        testViaJDBC();
        testViaDS();
    }

    protected static void testViaJDBC() {
        Connection conn = null;
        try {
            System.out.println("start");
            StringBuilder url = new StringBuilder();
            url.append("jdbc:postgresql://").append("localhost").append("/").append(POSTGRES_DEFAULT_DATABASE_NAME);

            conn = getConnection(DRIVER_CLASS, url.toString(), "postgres", "root");
            if (conn == null) {
                System.out.println("Error in connect!");
            }
            Statement statment = conn.createStatement();
            ResultSet databases = statment.executeQuery("select pg_get_viewdef(cl.oid) as datname from pg_class cl\n"
                    + "inner join pg_namespace nm on nm.oid = cl.relnamespace\n"
                    + "where relname = 'columns'");
//            ResultSet databases = statment.executeQuery("select datname from pg_database where datistemplate = false");
//            ResultSet databases = statment.executeQuery("select schema_name as datname from information_schema.schemata where schema_name not in ('information_schema', 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'pg_catalog')");
//            ResultSet databases = statment.executeQuery(INDEX_QUERY);
            while (databases.next()) {
                String db = databases.getString("datname");
                System.out.println("Database = " + db);
            }
            System.out.println("koniec");
        } catch (Exception e) {
            System.out.println("Exception = " + e.getMessage());
        } finally {
            if (conn != null) {
                closeConnection(conn);
            }
        }
    }

    public static void testViaDS() {
        JdbcConnectionBase<Object> mc = new PostgresConnection(new PostgresConnectionConfig("localhost", "pmks", "postgres", "root", 5432));
        mc.setDbStatementExecutionWarnThresholdMillis(-1);
        mc.setAutoCommit(true);
        mc.execQrySingleValInteger("SELECT 1", false);
        List<Map<String, Object>> execQry = mc.execQry("select * from pmks_cost", FieldNameConversion.ToLowerCase);
        System.out.println("OK, rows = " + execQry.size());
    }
}
