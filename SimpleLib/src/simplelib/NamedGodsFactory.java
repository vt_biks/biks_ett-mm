/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.HashMap;
import java.util.Map;

/**
 * Named Generic On-Demand Singleton Factory
 *
 * @author wezyr
 */
public class NamedGodsFactory<K, V> {

    private Map<K, IObjectCreator<V>> creators = new HashMap<K, IObjectCreator<V>>();
    private Map<K, V> singletons = new HashMap<K, V>();
    private K defKey;
    private V defVal;
    private boolean allowNullFromCreator;

    public NamedGodsFactory() {
        this(null);
    }

    public NamedGodsFactory(K defKey) {
        this(defKey, null, false);
    }

    public NamedGodsFactory(K defKey, V defVal, boolean allowNullFromCreator) {
        this.defKey = defKey;
        this.defVal = defVal;
        this.allowNullFromCreator = allowNullFromCreator;
    }

    public synchronized void addCreator(K key, IObjectCreator<V> creator) {
        creators.put(key, creator);
    }

    protected synchronized V innerGet(K key, boolean allowNoCreator) {
        V res = singletons.get(key);
        if (res == null) {
            IObjectCreator<V> creator = creators.get(key);

            if (creator != null) {
                res = creator.create();
                if (res == null) {
                    if (allowNullFromCreator) {
                        res = defVal;
                    } else {
                        throw new LameRuntimeException("creator returned null for key: " + key);
                    }
                }
                singletons.put(key, res);
            } else if (!allowNoCreator) {
                throw new LameRuntimeException("no creator for key: " + key);
            }
        }
        return res;
    }

    public synchronized V get(K key) {
        V res = innerGet(key, true);
        if (res == null) {
            if (defKey != null) {
                res = innerGet(defKey, false);
            }
        }
        return res;
    }
}
