/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

/**
 *
 * @author wezyr
 */
public interface ITreeBroker<DN, ID> {

    public ID getNodeId(DN n);

    public ID getParentId(DN n);
}
