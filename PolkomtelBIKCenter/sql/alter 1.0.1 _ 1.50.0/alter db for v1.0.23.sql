exec sp_check_version '1.0.23';
go


alter table bik_node
add indexVer int not null default 0;

exec sp_update_version '1.0.23', '1.0.24';
go
