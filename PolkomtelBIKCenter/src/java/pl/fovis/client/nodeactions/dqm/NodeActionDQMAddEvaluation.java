/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.dqm;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.dqm.AddOrEditDQMEvaluationDialog;
import pl.fovis.common.dqm.DQMEvaluationBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author tflorczak
 */
public class NodeActionDQMAddEvaluation extends NodeActionDQMBase {

    @Override
    public boolean isEnabledByDesign() {
        return node != null && isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_DQM_FOLDER_RATE);
    }

    @Override
    public String getCaption() {
        return I18n.dodajOcene.get();
    }

    @Override
    public void execute() {
        BIKClientSingletons.getDQMService().getEvaluationPreInfo(node.id, new StandardAsyncCallback<DQMEvaluationBean>("Error in getEvaluationPreInfo") {

            @Override
            public void onSuccess(final DQMEvaluationBean result) {
                BIKClientSingletons.getDQMService().getEvaluationPeriods(node.id, new StandardAsyncCallback<List<DQMEvaluationBean>>("Error in getEvaluationPeriods") {

                    @Override
                    public void onSuccess(List<DQMEvaluationBean> periods) {
                        if (BaseUtils.isCollectionEmpty(periods)) {
                            new SimpleInfoDialog().buildAndShowDialog(I18n.brakOkresowDoDodania.get(), null, null);
                        } else {
                            new AddOrEditDQMEvaluationDialog().buildAndShowDialog(result, periods, new IParametrizedContinuation<DQMEvaluationBean>() {

                                @Override
                                public void doIt(DQMEvaluationBean param) {
                                    BIKClientSingletons.getDQMService().saveEvaluation(param, new StandardAsyncCallback<Integer>("Error in saveEvaluation") {

                                        @Override
                                        public void onSuccess(Integer result) {
                                            BIKClientSingletons.showInfo(I18n.dodanoOcene.get());
                                            refreshRedisplayDataAndSelectRecord(result);
                                        }
                                    });
                                }
                            }, false);
                        }
                    }
                });
            }
        });
    }
}
