/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import com.rometools.rome.feed.synd.SyndContent;
import com.rometools.rome.feed.synd.SyndContentImpl;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndEntryImpl;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.feed.synd.SyndFeedImpl;
import com.rometools.rome.io.SyndFeedOutput;
import commonlib.LameUtils;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.fovis.common.NewsBean;
import pl.fovis.common.ObjectInFvsChangeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.logging.ILameLogger;

/**
 *
 * @author ctran
 */
public class RssExportManager implements IExportManager {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected BOXIServiceImpl service;

    public RssExportManager(BOXIServiceImpl boxiService) {
        this.service = boxiService;
    }

    @Override
    public void handleExportRequest(HttpServletRequest req, HttpServletResponse resp) {
        try {
            resp.setCharacterEncoding("utf8");
            resp.setContentType("application/rss+xml");
            String servletPath = req.getServletPath();
            String requestUrl = req.getRequestURL().toString();
            String requestUrlBase = requestUrl.substring(0, requestUrl.indexOf(servletPath));

            SyndFeed feed = new SyndFeedImpl();
            feed.setFeedType("rss_2.0");
//            feed.setTitle("BIKS Ulubione i Aktualności");
            feed.setTitle(I18n.biksaktualnosci.get());
//            feed.setDescription("Twoje ulubione obiekty i najnowsze aktualności od BIKSa");
            feed.setDescription(I18n.biksaktualnosciTytul.get());
            feed.setLink(requestUrlBase);
            feed.setPublishedDate(new Date());
//            System.out.println("getAuthType = " + req.getAuthType());
//            System.out.println("getRemoteUser = " + req.getRemoteUser());
//
//            System.out.println("Authen " + req.authenticate(resp));
//            System.out.println("HeaderNames = ");
//            Enumeration<String> headerNames = req.getHeaderNames();
//            while (headerNames.hasMoreElements()) {
//                System.out.println(headerNames.nextElement() + ",");
//            }
//            System.out.println("Cookie header = " + req.getHeader("Cookie"));
//            System.out.println("Host header = " + req.getHeader("Host"));
//            System.out.println("Authorization header = " + req.getHeader("Authorization"));
//            String authorization = req.getHeader("Authorization");
//            if (authorization != null && authorization.startsWith("Basic")) {
//                String base64Credentials = authorization.substring("Basic".length()).trim();
//                String credentials = new String(Base64.decode(base64Credentials),
//                        Charset.forName("UTF-8"));
//                // credentials = username:password
//                final String[] values = credentials.split(":", 2);
//                System.out.println(values[0] + ":" + values[1]);
//            }
//            System.out.println("Username=" + req.getRemoteUser());
//            System.out.println("Accept-Language header = " + req.getHeader("Accept-Language"));
//            System.out.println("Connection header = " + req.getHeader("Connection"));
//            System.out.println("User-Agent header = " + req.getHeader("User-Agent"));

            List<SyndEntry> entries = new ArrayList<SyndEntry>();
//            Integer uid = service.getUserIdByLogin(req.getRemoteUser());
//            entries.addAll(addFavourite(requestUrlBase, uid));
            entries.addAll(addNews());
            feed.setEntries(entries);

            Writer writer = resp.getWriter();
            SyndFeedOutput output = new SyndFeedOutput();
            output.output(feed, writer);
            writer.close();
        } catch (Exception ex) {
            logger.error("Error when handling request: " + ex.getMessage());
        }
    }

    private List<SyndEntry> addFavourite(String requestUrlBase, Integer uid) {
        List<SyndEntry> ret = new ArrayList<SyndEntry>();
        List<ObjectInFvsChangeBean> favouritesForUser = service.getFavouritesForUser(uid);
        int i = 0;
        for (ObjectInFvsChangeBean bean : favouritesForUser) {
            SyndEntry entry = new SyndEntryImpl();
//            SyndContent description = new SyndContentImpl();

            entry.setTitle(bean.caption);
//            entry.setPublishedDate(bean.dateAdded);
            entry.setLink(requestUrlBase + "/#" + bean.treeCode + "/" + bean.nodeId);
//            entry.setAuthor(bean.caption);
//            description.setType("text/plain");
//            description.setValue(bean.name);
//            entry.setDescription(description);
            ret.add(entry);
        }
        return ret;
    }

    private List<SyndEntry> addNews() {
        List<SyndEntry> ret = new ArrayList<SyndEntry>();
        List<NewsBean> news = service.getPublicNews();
        for (NewsBean bean : news) {
            SyndEntry entry = new SyndEntryImpl();

            entry.setTitle(bean.title);
            //            entry.setPublishedDate(bean.dateAdded);
//            entry.setUri(requestUrlBase + "/#MyBIKS");
            SyndContent description = new SyndContentImpl();
            description.setType("text/html");
            description.setValue(bean.text);
            entry.setDescription(description);
            ret.add(entry);
        }
        return ret;
    }
}
