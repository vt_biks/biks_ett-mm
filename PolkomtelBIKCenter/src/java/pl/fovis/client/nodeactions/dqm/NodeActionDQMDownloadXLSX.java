/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.dqm;

import com.google.gwt.user.client.Window;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.dqm.DQMConstanst;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;

/**
 *
 * @author tflorczak
 */
public class NodeActionDQMDownloadXLSX extends NodeActionDQMBase {

    protected boolean isReportNode;
    protected boolean isDataErrorIssueNode;

    @Override
    public boolean isEnabledByDesign() {
        isReportNode = isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_DQM_REPORT, BIKGWTConstants.NODE_KIND_DQM_RATE, BIKGWTConstants.NODE_KIND_DQM_RATE_ACCEPTED, BIKGWTConstants.NODE_KIND_DQM_RATE_REJECTED);
        isDataErrorIssueNode = isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_DQM_ERROR_REGISTER, BIKGWTConstants.NODE_KIND_DQM_ERROR_CLOSED, BIKGWTConstants.NODE_KIND_DQM_ERROR_OPENED);
        return (node != null) && (isReportNode || isDataErrorIssueNode);
    }

    @Override
    public String getCaption() {
        return I18n.pobierzXLS.get();
    }

    @Override
    public void execute() {
        Window.open(generateFileLink(data.node.id), "_blank", "");
    }

    protected String generateFileLink(int nodeId) {
        StringBuilder sb = new StringBuilder(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet());
        sb.append("?").append(DQMConstanst.DQM_EXPORT_REPORT_PARAM_NAME).append("=").append(nodeId);
        return sb.toString();
    }
}
