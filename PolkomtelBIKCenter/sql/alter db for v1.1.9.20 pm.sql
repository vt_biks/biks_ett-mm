﻿exec sp_check_version '1.1.9.20';
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

--ww: poprzednia wersja w alter db for v1.1.9.16 pm.sql
-- zmiana: uwzględnianie is_folder z bik_node_kind - foldery lecą 
-- wyżej niż zwykłe węzły
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_fix_nodes_visual_order_ex]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_fix_nodes_visual_order_ex
go

create procedure sp_fix_nodes_visual_order_ex(@parent_id int, @tree_id int, @ignore_nodekindcodes_in_larch int) as
begin
  set nocount on

  declare @vofix table (id int not null primary key, visual_order_fix int not null)

  insert into @vofix (id, visual_order_fix)
  select n.id, (ROW_NUMBER() OVER (ORDER BY n.visual_order, k.is_folder desc, n.name)) - 1 AS visual_order_fix --, * 
  from 
  bik_node n inner join bik_node_kind k on n.node_kind_id = k.id
  where n.tree_id = @tree_id and n.is_deleted = 0 and (@parent_id is null and n.parent_node_id is null or @parent_id is not null and n.parent_node_id = @parent_id)
  and (@ignore_nodekindcodes_in_larch = 0 or n.obj_id not like '&%')
  
  update 
  bik_node
  set visual_order = visual_order_fix
  from @vofix v
  where v.id = bik_node.id
  and bik_node.tree_id = @tree_id and bik_node.is_deleted = 0 and (@parent_id is null and bik_node.parent_node_id is null or @parent_id is not null and bik_node.parent_node_id = @parent_id)
  and (@ignore_nodekindcodes_in_larch = 0 or obj_id not like '&%')
end
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

--ww: poprzednia wersja w pliku: "alter db for v1.1.0.4 am.sql"
-- zmiana: dodanie filtrowania po bik_node.visual_order, bik_node_kind.is_folder (desc),
-- jako ostatnie w kolejności jest (tak jak było) po bik_node.name
if exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_filter_bik_nodes]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_filter_bik_nodes]
go

create procedure [dbo].[sp_filter_bik_nodes] (@txt varchar(max), @tree_ids_filter varchar(8000) /*@optTreeId int*/, @opt_bik_node_filter varchar(max))
as
begin
  set nocount on
  
  declare @diag_level int = 0
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': start!'
  
declare @sql varchar(8000);

set @sql = 'select id from bik_node where id in (' + dbo.fn_bik_node_name_chunk_search_sql(@txt, @tree_ids_filter)--@optTreeId) 
  + ') and name like ''%' + replace(@txt, '''', '''''') + '%'' and is_deleted = 0' +
  case when @opt_bik_node_filter is null then '' else ' and (' + @opt_bik_node_filter + ')' end +
  case when @tree_ids_filter is null then '' else ' and (' + @tree_ids_filter + ')' end
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': have sql: sql=' + @sql
  print @sql
-- ostatnio wrzucone
declare @t table (id int not null primary key, parent_node_id int null);
-- parent_node_ids ostatnio wrzuconych - te będziemy wrzucać teraz
declare @p table (id int not null primary key);
-- wszystkie
declare @a table (id int not null primary key);

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': before exec sql'

  insert into @p (id)
  exec(@sql)

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': sql executed'
  
  while exists (select 1 from @p) begin
    insert into @a select id from @p;

    delete from @t;

    insert into @t (id, parent_node_id) select id, parent_node_id from bik_node where id in (select id from @p);

    delete from @p;

    insert into @p 
    select distinct parent_node_id 
    from @t 
    where parent_node_id is not null and parent_node_id not in (select id from @a);
  end;

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': after loop'

select bn.*,
    bnk.caption as node_kind_caption,
    bnk.code as node_kind_code,
    bnk.is_folder as is_folder,
    bt.name as tree_name, bt.tree_kind, bt.code as tree_code
    , case when disable_linked_subtree = 0 and exists 
        (select 1 from bik_node where is_deleted = 0 and parent_node_id = coalesce(bn.linked_node_id, bn.id)) then 0 else 1 end as has_no_children
    from bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    inner join bik_tree bt on bn.tree_id = bt.id
    where bn.id in (select id from @a)
    order by bn.visual_order, bnk.is_folder desc, bn.name

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': after final select'
end;
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

-- usunięcie pozycji z menu Admin\Role Użytkowników (kod #admin:dict:role)
declare @treeOfTreesId int = (select id from bik_tree where code = 'TreeOfTrees')

delete 
--select *
from bik_node
where tree_id = @treeOfTreesId and obj_id = '#admin:dict:role'
go

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

exec sp_update_version '1.1.9.20', '1.1.9.21';
go
