﻿exec sp_check_version '1.0.97.0.1';
go

----------------------------------
----------------------------------
----------------------------------

--drop index idx_bik_sapbo_universe_table_node_id on bik_sapbo_universe_table
--go

CREATE unique INDEX idx_bik_searchable_attr_val_node_attr_ids
ON [dbo].[bik_searchable_attr_val] ([node_id], attr_id)
go

CREATE INDEX idx_bik_node_del_kind_tree_ln_dis_lin_st
ON [dbo].[bik_node] ([is_deleted],[node_kind_id],[tree_id])
INCLUDE ([linked_node_id],[disable_linked_subtree])
go

----------------------------------
----------------------------------
----------------------------------

exec sp_update_version '1.0.97.0.1', '1.0.97.0.2';
go
