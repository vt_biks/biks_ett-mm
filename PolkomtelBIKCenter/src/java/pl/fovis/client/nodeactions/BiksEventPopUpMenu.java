package pl.fovis.client.nodeactions;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.TreeBikPage;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.MoveNodeInTreeMode;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.popupmenu.FoxyPopUpMenu;

/**
 *
 * @author efedchenko
 */
public class BiksEventPopUpMenu extends FoxyPopUpMenu<INodeActionContext> {

    public BiksEventPopUpMenu(TreeBikPage bikPage, TreeNodeBean node, EntityDetailsDataBean data, int clientX, int clientY) {
        super(new NodeActionContext(node, bikPage, data), clientX, clientY);
    }

    @Override
    protected void addActions() {
        if (BIKClientSingletons.isSysAdminLoggedIn()) {
            addAction(new NodeActionRename());
            addAction(new NodeActionEditDescription());
            addSeparator();
            //        addAction(new NodeActionCut());
            addAction(new NodeActionDelete());
        }
        // addAction(new NodeActionCopy()); w każdym razie nie działa (isEnabledByDesign() returns false)
        addAction(new NodeActionPaste());
        addSeparator();
        addAction(new NodeActionMoveInTree(I18n.przeniesNaGore.get() /* I18N:  */, MoveNodeInTreeMode.MakeFirst));
        addAction(new NodeActionMoveInTree(I18n.przeniesWyzej.get() /* I18N:  */, MoveNodeInTreeMode.MoveUp));
        addAction(new NodeActionMoveInTree(I18n.przeniesNizej.get() /* I18N:  */, MoveNodeInTreeMode.MoveDown));
        addAction(new NodeActionMoveInTree(I18n.przeniesNaDol.get() /* I18N:  */, MoveNodeInTreeMode.MakeLast));
        addSeparator();
        addAction(new NodeActionResetVisualOrder());
        addSeparator();
        List<NodeKindBean> nodeKindsForTreeKind = BIKClientSingletons.getNodeKindsForTreeKind(contextObject.getNode());
        if (nodeKindsForTreeKind.get(0) == null) {
            BIKClientSingletons.showInfo(I18n.brakTypowWezlow.get());
        } else {
            addAction(new NodeActionAddEvent(nodeKindsForTreeKind.get(0)));
//        addAction(new NodeActionAddEvent());
        }
    }
}
