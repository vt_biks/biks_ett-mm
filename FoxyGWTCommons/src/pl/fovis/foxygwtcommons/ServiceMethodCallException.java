/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;

/**
 *
 * @author pmielanczuk
 */
public class ServiceMethodCallException extends Exception implements Serializable, IsSerializable {

    public String message;

    public ServiceMethodCallException() {
        // no-op
    }

    public ServiceMethodCallException(String message) {
        this.message = message;
    }
}
