/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import pl.bssg.gwtservlet.IFoxySessionDataRemoteUserAware;
import pl.fovis.common.SystemUserBean;
import pl.trzy0.foxy.commons.IBeanConnectionThreadHandle;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class BOXISessionData implements IFoxySessionDataRemoteUserAware, Serializable {
//    public UserBean loggedUser;

    // baza->sysUserBean
    private Map<String, Pair<SystemUserBean, Long>> loggedUsers;
    public String loginName;
    //public SystemUserBean loggedUser;
    public String remoteUser; // teraz z domeną, np: POLSATC\jkowalski
    public boolean ignoreRemoteUser;
    public boolean userExistButDisabled;
    public Map<String, IBeanConnectionThreadHandle> cancellableExecutions = new HashMap<String, IBeanConnectionThreadHandle>();
    //public String lisaTeradataLogin;
    public Map<String, String> lisaTeradataLogins; //<SingleBiksDbForCurrentRequest, Pair<LisaId, username>>
    //ww: dla multiXa, czyli Multi Knowledge Systemu, czyli wielobiksowości
    //public String xInstanceDbName;
    //---
    //ww: czy warunek getSessionData().xInstanceDbNames != null <==> isLoggedIn
    // jest dobry?
    // kiedy to jest uzupełniane?
    // xInstanceDbNames bywa nazywane accessibleDatabases w BIKSServiceBase.requestStarting
    private Set<String> accessibleDbNames;
    //ww: a po co jest to?
    //tf: po analizie wywalam to pole
//    public String defaultDbName;
    final public Map<String, Map<Integer, Long>> accessedTreesOnDb = new HashMap<String, Map<Integer, Long>>();
    public Long lastActivityInServerTimeMillis;

    @Override
    public void setRemoteUserName(String remoteUserName) {
        this.remoteUser = remoteUserName;
    }

    //---
    //ww: to nie było używane (od pewnego czasu, pewnie efekt refaktoringu)
//    public boolean isLoggedIn() {
//        return accessibleDbNames != null;
//    }
    public boolean isAccessibleDbName(String dbName) {
        return BaseUtils.collectionContainsFix(accessibleDbNames, dbName);
    }

    public void addAccessibleDbName(String dbName) {
        if (accessibleDbNames == null) {
            accessibleDbNames = new LinkedHashSet<String>();
        }
        accessibleDbNames.add(dbName);
    }

    public Set<String> getAccessibleDbNames() {
        return accessibleDbNames;
    }

    public void setAccessibleDbNames(Set<String> accessibleDbNames) {
        this.accessibleDbNames = accessibleDbNames;
    }

    public void clearForLogout() {
        loggedUsers = null;
        remoteUser = null;
        loginName = null;
        //loggedUser = null;
        ignoreRemoteUser = true;
        userExistButDisabled = false;
        lisaTeradataLogins = null;
        // xInstanceDbName = null;
        accessibleDbNames = null;
//        defaultDbName = null;
        accessedTreesOnDb.clear();
    }

    public Pair<SystemUserBean, Long> getSystemUserBeanAndTimeStampIfLoggedOnDb(String singleBiksDb) {
        if (loggedUsers == null) {
            return null;
        }

        return loggedUsers.get(singleBiksDb);
    }

    public boolean isUserLoggedToSingleBiksDb(String singleBiksDb) {
        return loggedUsers != null && loggedUsers.containsKey(singleBiksDb);
    }

    public void setUserLoggedOnDb(String singleBiksDb, Pair<SystemUserBean, Long> systemUserBeanAndTimeStamp) {
        if (loggedUsers == null) {
            loggedUsers = new HashMap<String, Pair<SystemUserBean, Long>>();
        }

        loggedUsers.put(singleBiksDb, systemUserBeanAndTimeStamp);
    }
}
