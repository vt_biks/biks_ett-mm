/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.mssql;

/**
 *
 * @author tflorczak
 */
public class MsSqlBean {

    public String name;
    public String type;
    public String extraInfo;
    public String definitionText;
    public String branchNames;
    public String parentBranchNames;
    public Integer visualOrder;

    public MsSqlBean() {
    }

    public MsSqlBean(String name, String type, String extraInfo, String definitionText, String branchNames, String parentBranchNames) {
        this.name = name;
        this.type = type;
        this.extraInfo = extraInfo;
        this.definitionText = definitionText;
        this.branchNames = branchNames;
        this.parentBranchNames = parentBranchNames;
        this.visualOrder = 0;
    }

    public MsSqlBean(String name, String type, String extraInfo, String definitionText, String branchNames, String parentBranchNames, int visualOrder) {
        this(name, type, extraInfo, definitionText, branchNames, parentBranchNames);
        this.visualOrder = visualOrder;
    }
}
