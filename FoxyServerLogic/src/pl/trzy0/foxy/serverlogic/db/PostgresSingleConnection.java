/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import commonlib.JdbcUtil;
import java.sql.Connection;
import pl.trzy0.foxy.commons.PostgresConnectionConfig;
import pl.trzy0.foxy.serverlogic.IConnectionDialectHelper;

/**
 *
 * @author wezyr
 */
public class PostgresSingleConnection extends JdbcSingleConnectionBase {

    public PostgresSingleConnection(Connection conn) {
        super(conn);
    }

    public PostgresSingleConnection(PostgresConnectionConfig config) {
        this(JdbcUtil.getConnection("org.postgresql.Driver",
                "jdbc:postgresql://" + config.server + "/" + config.database,
                config.user, config.password, null, true));
    }

    @Override
    protected IConnectionDialectHelper createDialectHelper() {
        return new PostgresConnectionDialectHelper(this);
    }
}
