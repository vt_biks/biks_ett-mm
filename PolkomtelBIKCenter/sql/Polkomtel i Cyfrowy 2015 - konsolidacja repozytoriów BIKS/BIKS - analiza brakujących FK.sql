
select 
  o.name as table_name, c.name as column_name, t.name
, * 
from sys.columns c inner join sys.objects o on c.object_id = o.object_id inner join sys.types t on c.system_type_id = t.system_type_id
where 
  o.type = 'U' and o.is_ms_shipped = 0
  and not exists (select 1 from vw_ww_foreignkeys fk where fk.table_name = o.name and fk.name = c.name)
  and c.name like '%[_]id'
  and o.name like 'bik[_]%'
  --and o.name not like 'bik[_]%' 
  and c.name not like 'si[_]%id'
  and t.name = 'int'
order by o.name, c.name


/*

select name_prefix, count(*) as table_cnt
from
(
select substring(name, 1, case when charindex('_', name) = 0 then len(name) else charindex('_', name) end) as name_prefix, * 
from sys.tables
where type = 'u' and is_ms_shipped = 0
) x
group by name_prefix
order by name_prefix




select * from vw_ww_foreignkeys

select * from sys.types

select t1.name, t2.name
from (select * from sys.objects t1 where t1.type = 'u') t1 full join (select * from biks_cyfrowy_2015.sys.objects t2 where t2.type = 'u') t2 on t1.name = t2.name
where t1.name is null or t2.name is null


*/
