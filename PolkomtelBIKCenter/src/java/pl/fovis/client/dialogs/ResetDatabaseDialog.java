/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;

/**
 *
 * @author uwa
 */
public class ResetDatabaseDialog extends BaseActionOrCancelDialog {

    private VerticalPanel vp;
    private boolean preserveInvited = true;

    @Override
    protected String getDialogCaption() {
        return I18n.resetujBaze.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.tak.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        vp = new VerticalPanel();
        vp.setStyleName("resetDatabase");
        vp.add(new Label(I18n.zachowacZaproszonych.get()));
        main.add(vp);
    }

    @Override
    protected void doAction() {
        executeReset();
    }

    private void executeReset() {
//        BIKClientSingletons.getService().getLoggedUserBean(new StandardAsyncCallback<SystemUserBean>() {
//            public void onSuccess(SystemUserBean result) {
//                resetBaseForUser();
//            }
//        });
        resetBaseForUser();
    }

    private void resetBaseForUser() {
//        FoxyClientSingletons.showInfo(I18n.resettingDbInProgressPleaseWait.get());

        final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
        infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.resettingDbInProgressPleaseWait.get(), true);

        BIKClientSingletons.getMultiXService().resetDB(preserveInvited,
                new StandardAsyncCallback<String>() {
                    @Override
                    public void onSuccess(String result) {
                        infoDialog.hideDialog();
                        if (result != null) {
                            BIKClientSingletons.performUserLogout(/*BIKClientSingletons.performOnLoginLogoutCont*/);
                        } else {
                            Window.Location.reload();
                        }
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        infoDialog.hideDialog();
                        super.onFailure(caught);
                    }
                });
    }

    @Override
    protected void addAdditionalButtons(HorizontalPanel hp) {
        PushButton pb = new PushButton(I18n.nie.get());
        NewLookUtils.makeCustomPushButton(pb);
        pb.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                preserveInvited = false;
                actionClicked();
            }
        });
        hp.add(pb);
    }
    /*    public void buildAndShowDialog(NewsBean n, IParametrizedContinuation<NewsBean> saveCont) {
     this.saveCont = saveCont;
     if(n == null){
     news = new NewsBean();
     news.text = "";
     news.title = "";
     }else{
     news = n;}
     super.buildAndShowDialog();
     if (tbTytul.getText().equals("") && tbDescription.getText().equals("")) {
     actionBtn.setEnabled(false);
     }
     }*/
}
