/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.menu;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.HistoryManager;
import pl.fovis.client.IAutoScrollTopMenu;
import pl.fovis.client.ITabSelector;
import pl.fovis.client.bikpages.IBikPage;
import pl.fovis.client.mltx.history.MultiXUrlCreator;
import pl.fovis.foxygwtcommons.ITreeChildBroker;
import pl.fovis.foxygwtcommons.OrderedList;
import pl.fovis.foxygwtcommons.TreeAlgorithms;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author wezyr
 */
public class MenuBuilder implements ITabSelector<Integer> {

    //ww: uwaga - drobny niuans: ten broker nie działa dla nulla (powinien dawać wtedy rooty)
    // ale tak go używamy poniżej, że nie ma takiej potrzeby
    public static final ITreeChildBroker<IMenuItem> childBrokerForMenuItems = new ITreeChildBroker<IMenuItem>() {
        public List<IMenuItem> getChildNodes(IMenuItem node) {
            if (node instanceof IMenuItemSubMenu) {
                IMenuItemSubMenu subMenu = (IMenuItemSubMenu) node;
                return subMenu.getSubItems();
            }
            return null;
        }
    };
    protected Element topLevelItemsContainerElement;
    protected Element secondaryLevelItemsContainerElement;
    protected HasWidgets mainBodyContainer;
    protected List<IMenuItem> topItems;
    protected OrderedList topLevelItemsContainer;
    protected OrderedList secondaryLevelItemsContainer;
    protected Map<String, MenuItemWidget> topLevelMenuWidgetMap = new HashMap<String, MenuItemWidget>();
    protected Map<String, IMenuItem> topLevelMenuItemMap = new HashMap<String, IMenuItem>();
    protected Map<String, Widget> secondaryLevelMenuWidgetMap = new HashMap<String, Widget>();
    protected Map<String, IMenuItem> secondaryLevelMenuItemMap = new HashMap<String, IMenuItem>();
    protected Map<String, LinkTabPanel> tabPanelMap = new HashMap<String, LinkTabPanel>();
    protected Map<String, Integer> tabIndexMap = new HashMap<String, Integer>();
    protected MenuItemWidget currentTopLevelMenuWidget = null;
    protected Widget currentSecondaryLevelMenuWidget = null;
    protected Map<IMenuItem, Set<String>> tabIdsOnSecondLevel = new HashMap<IMenuItem, Set<String>>();
    protected Set<String> initedTabIds = new HashSet<String>();
    protected Map<String, IMenuItemMainWidget> tabIdToMainWidgetMap = new HashMap<String, IMenuItemMainWidget>();
//    protected Panel loginLogoutPanel;
    //private IBikPage<Integer> currentActiveTab;
    protected IMenuItemMainWidget currentActiveMenuItem;
    //protected EDPForms forms = new EDPForms(/*this*/);
    private Set<String> invalidatedTabIds = new HashSet<String>();
    private IAutoScrollTopMenu bikCenter;

    public MenuBuilder(Element topLevelItemsContainerElement,
            Element secondaryLevelItemsContainerElement, HasWidgets mainBodyContainer) {
        this.topLevelItemsContainerElement = topLevelItemsContainerElement;
        this.secondaryLevelItemsContainerElement = secondaryLevelItemsContainerElement;
        this.mainBodyContainer = mainBodyContainer;

//        this.loginLogoutPanel = RootPanel.get("loginButton");
    }

    public MenuBuilder(Element topLevelItemsContainerElement, Element secondaryLevelItemsContainerElement, HasWidgets mainBodyContainer, IAutoScrollTopMenu bikCenter) {
        this(topLevelItemsContainerElement, secondaryLevelItemsContainerElement, mainBodyContainer);
        this.bikCenter = bikCenter;
    }

    public void buildMenu(IMenuItem... topItems) {
        buildMenu(BaseUtils.arrayToArrayList(topItems));
    }

    protected IBikPage<Integer> getFirstTabInMenu(IMenuItem item) {
        if (item instanceof IMenuItemMainWidget) {
            IMenuItemMainWidget mimw = (IMenuItemMainWidget) item;
            return mimw.getTab();
        }

        IMenuItemSubMenu subMenu = (IMenuItemSubMenu) item;
        List<IMenuItem> subItems = subMenu.getSubItems();

        if (subItems != null) {
            for (IMenuItem subItem : subItems) {
                IBikPage<Integer> first = getFirstTabInMenu(subItem);
                if (first != null) {
                    return first;
                }
            }
        }
        return null;
    }

    public void submitNewTabSelection(String tabId, Integer val, boolean fireOnChange) {
//        if (tabId.equals("Pojęcie biznesowe")) {
//            tabId = "mpTab";
//        }
//        if (tabId.equals("Wpis blogowy")) {
//            tabId = "blogTab";
//        }

        // wrzuć do historii aktualnie wybraną zakładkę i węzeł na niej
        if (getCurrentActiveTab() != null) {
            HistoryManager.addHistoryToken(getCurrentActiveTab().getId(),
                    BaseUtils.safeToString(getCurrentActiveTab().getSelectedNodeId()),
                    false);
        }
        if (!HistoryManager.addHistoryToken(tabId, BaseUtils.safeToString(val), fireOnChange)
                && fireOnChange) {
            selectValueOnTab(tabId, val);
        }
    }

    protected void submitNewTabSelection(String tabId, boolean fireOnChange) {
        submitNewTabSelection(tabId, null, fireOnChange);
    }

    protected void submitNewTabSelection(IBikPage<Integer> w, boolean fireOnChange) {
        if (w != null) {
            submitNewTabSelection(w.getId(), fireOnChange);
        }
    }

    protected MenuItemWidget addItemToContainer(final IMenuItem item, OrderedList ol) {
        final String itemCaption = item.getCaption();

        MenuItemWidget li = new MenuItemWidget(itemCaption,
                new ClickHandler() {
                    public void onClick(ClickEvent event) {
                        IBikPage<Integer> w = getFirstTabInMenu(item);
                        submitNewTabSelection(w, true);
                    }
                });

        ol.add(li);

        return li;
    }

    // if all items are disabled -> returns empty list
    public static List<IMenuItem> filterOutDisabledMenuItems(List<IMenuItem> items) {
        List<IMenuItem> res = new ArrayList<IMenuItem>();
        if (!BaseUtils.isCollectionEmpty(items)) {
            for (IMenuItem item : items) {
                boolean willAddItem;

                if (item != null) {
                    if (item instanceof IMenuItemMainWidget) {
                        IMenuItemMainWidget mimw = (IMenuItemMainWidget) item;
                        willAddItem = mimw.getTab().isPageEnabled();
//                if (!willAddItem) {
//                    Window.alert("page: " + item.getCaption() + ", tabId=" + mimw.getTab().getId() + " is disabled");
//                }
                    } else {
                        IMenuItemSubMenu mism = (IMenuItemSubMenu) item;
                        List<IMenuItem> subItems = mism.getSubItems();
                        List<IMenuItem> filteredSubItems = filterOutDisabledMenuItems(subItems);
                        willAddItem = !filteredSubItems.isEmpty();
                        if (willAddItem && subItems.size() != filteredSubItems.size()) {
                        // some submenu items have been just filtered out
                            // so, create fixed IMenuItemSubMenu object
                            item = new MenuItemSubMenu(mism.getCaption(), filteredSubItems);
                        }
                    }

                    if (willAddItem) {
                        res.add(item);
                    }
                }
            }
        }

        return res;
    }

    //ww: pomocnicza metoda do debugowania
    protected void printMenu(List<IMenuItem> items, int lvl) {
        TreeAlgorithms.prettyPrintTreeLevelsDown(items, childBrokerForMenuItems, lvl, new BaseUtils.Projector<IMenuItem, String>() {
            public String project(IMenuItem val) {
                return val.getCaption();
            }
        });
    }

    public void buildMenu(List<IMenuItem> topItems0) {
//        System.out.println("originalMenu:");
//        printMenu(topItems0, 1);
//        this.topItems = new ArrayList<IMenuItem>();
//        for (int i = start; i < topItems0.size(); i++) {
//            this.topItems.add(topItems0.get(i));
//        }
        this.topItems = filterOutDisabledMenuItems(topItems0);

//        System.out.println("menu after filtering:");
//        printMenu(this.topItems, 1);
        int topItemCnt = this.topItems.size();
        //ww: trzeba naprawiać menu gdy część ma poziom 2 a część 3 w jakimś topItem
        for (int topItemIdx = 0; topItemIdx < topItemCnt; topItemIdx++) {
            IMenuItem topItem = this.topItems.get(topItemIdx);

            Pair<Integer, Integer> vals = TreeAlgorithms.calcMinAndMaxSubtreeLevels(childBrokerForMenuItems, topItem);
            if (vals.v1 != vals.v2) {
//                System.out.println("Menu Item: " + topItem.getCaption() + " has minLvl=" + vals.v1 + ", maxLvl=" + vals.v2);

                List<IMenuItem> subItems = new ArrayList<IMenuItem>();

                IMenuItemSubMenu subMenuTopItem = (IMenuItemSubMenu) topItem;

                for (IMenuItem subItem : subMenuTopItem.getSubItems()) {
                    if (subItem instanceof IMenuItemMainWidget) {
                        subItem = new MenuItemSubMenu(subItem.getCaption(), subItem);
                    }
                    subItems.add(subItem);
                }

                this.topItems.set(topItemIdx, new MenuItemSubMenu(topItem.getCaption(), subItems));
            }
        }
        //ww: sprawdzenie czy menu jest dobrze naprawione
//        for (int topItemIdx = 0; topItemIdx < topItemCnt; topItemIdx++) {
//            IMenuItem topItem = this.topItems.get(topItemIdx);
//
//            Pair<Integer, Integer> vals = TreeAlgorithms.calcMinAndMaxSubtreeLevels(childBrokerForMenuItems, topItem);
//            if (vals.v1 != vals.v2) {
//                System.out.println("%%% Menu Item: " + topItem.getCaption() + " has minLvl=" + vals.v1 + ", maxLvl=" + vals.v2);
//            }
//        }

        topLevelItemsContainer = OrderedList.wrap(topLevelItemsContainerElement);
        secondaryLevelItemsContainer = OrderedList.wrap(secondaryLevelItemsContainerElement);

        for (IMenuItem item : this.topItems) {
            MenuItemWidget topLevelMenuItem = addItemToContainer(item, topLevelItemsContainer);

            if (item instanceof IMenuItemSubMenu) {
                IMenuItemSubMenu mism = (IMenuItemSubMenu) item;
                for (IMenuItem subItem : mism.getSubItems()) {
                    if (subItem instanceof IMenuItemSubMenu) {
                        Collection<String> tabIdsInSubMenu = getTabIdsInMenu(subItem);
                        for (String tabIdInSubMenu : tabIdsInSubMenu) {
                            secondaryLevelMenuItemMap.put(tabIdInSubMenu, subItem);
                        }

                        tabIdsOnSecondLevel.put(subItem, new LinkedHashSet<String>(tabIdsInSubMenu));
                    }
                }
            }

            Collection<String> tabIdsInMenu = getTabIdsInMenu(item);

            for (String tabIdInMenu : tabIdsInMenu) {
                topLevelMenuWidgetMap.put(tabIdInMenu, topLevelMenuItem);
                topLevelMenuItemMap.put(tabIdInMenu, item);
            }
        }

        MenuItemWidget topLevelMenuWidget = topLevelMenuWidgetMap.get(BIKGWTConstants.TAB_ID_MYBIKS);
        if (BIKClientSingletons.isIconHomeInMenu() && topLevelMenuWidget != null) {
            //topLevelMenuWidget.setStyleName("homeMenuItem");
            Anchor anc = topLevelMenuWidget.getAnchor();
            anc.setHTML("<img src=\"images/home_transparency.gif\" border=\"0\" style=\"margin: 0px 3px 0px 0px;\" />" + anc.getHTML());
        }
    }

    protected void refreshWidgetsStateOfCurrentTab(
            Integer optIdToSelect /*IBikPage tab/*, boolean refreshDetails*/) {
        String tabId = getCurrentActiveTab().getId();
        boolean needsRefresh = invalidatedTabIds.contains(tabId);
        invalidatedTabIds.remove(tabId);

        getCurrentActiveTab().activatePage(needsRefresh, optIdToSelect, true);
//        if (tab instanceof IRefreshWidgetsState) {
//            IRefreshWidgetsState refreshable = (IRefreshWidgetsState) tab;
//            refreshable.refreshAfterLoginLogout();
//        }
//
//        if ((refreshDetails || !(tab instanceof ISelectableEntities)) && tab instanceof IRefreshDetailProps) {
//            IRefreshDetailProps rdp = (IRefreshDetailProps) tab;
//            rdp.refreshDetailProps();
//        }
    }

    public void selectValueOnTab(String tabId, Integer val) {
        IBikPage<Integer> tab = selectTab(tabId, val);
//
//        Integer intVal = val;
//
//        if (intVal != null) {
//            if (tab instanceof ISelectableEntities) {
//                ISelectableEntities<Integer> seTab = (ISelectableEntities<Integer>) tab;
//
//                seTab.selectEntityById(intVal, false);
//
//            } else {
//                throw new RuntimeException("tab with id " + tabId + " is not ISelectableEntities");
//            }
//        }
    }

    protected IBikPage<Integer> selectTab(String tabId, //boolean refreshDetails
            Integer optValToSelectOnTab) {
        MenuItemWidget topLevelMenuWidget = topLevelMenuWidgetMap.get(tabId);

        IMenuItem topLevelMenuItem = topLevelMenuItemMap.get(tabId);

        IMenuItem secondaryLevelMenuItem = secondaryLevelMenuItemMap.get(tabId);

        Widget secondaryLevelMenuWidget = null;

        if (topLevelMenuWidget != currentTopLevelMenuWidget) {

            if (currentTopLevelMenuWidget != null) {
                currentTopLevelMenuWidget.setActive(false);//removeStyleName("active");
            }

            //topLevelMenuWidget.addStyleName("active");
            topLevelMenuWidget.setActive(true);

            //topLevelItemsContainer.getWidget(1).addStyleName("aqq qpa");
            secondaryLevelItemsContainer.clear();
            secondaryLevelMenuWidgetMap.clear();

            if (topLevelMenuItem instanceof IMenuItemSubMenu) {
                IMenuItemSubMenu sm = (IMenuItemSubMenu) topLevelMenuItem;

                if (secondaryLevelMenuItem != null) {
                    for (IMenuItem subItem : sm.getSubItems()) {
                        Widget slmiw = addItemToContainer(subItem, secondaryLevelItemsContainer);
                        if (subItem == secondaryLevelMenuItem) {
                            slmiw.addStyleName("active" /* I18N: no */);

                            secondaryLevelMenuWidget = slmiw;
                        }

                        Set<String> tabIds = tabIdsOnSecondLevel.get(subItem);
                        for (String tt : tabIds) {
                            secondaryLevelMenuWidgetMap.put(tt, slmiw);
                        }
                    }
                }
            }
        } else {
            if (currentSecondaryLevelMenuWidget != null) {
                currentSecondaryLevelMenuWidget.removeStyleName("active" /* I18N: no */);
            }

            secondaryLevelMenuWidget = secondaryLevelMenuWidgetMap.get(tabId);
            if (secondaryLevelMenuWidget != null) {
                secondaryLevelMenuWidget.addStyleName("active" /* I18N: no */);
            }
        }

        LinkTabPanel tlp = tabPanelMap.get(tabId);

        if (tlp == null) {
            tlp = new LinkTabPanel(tabId/*25, Unit.PX*/);

//            tlp.setStyleName("linkTabPanel");
//            DOM.setElementAttribute(tlp.getElement(), "id", "mainTabLookId");
//            DOM.getChild(tlp.getElement(), 1).setClassName("mainTabLook");
            tlp.setSize("100%", "100%");

            IMenuItem parentMenuItem = BaseUtils.nullToDef(secondaryLevelMenuItem, topLevelMenuItem);

            final List<IMenuItem> bottomLevelItems;

            if (parentMenuItem instanceof IMenuItemSubMenu) {
                IMenuItemSubMenu mism = (IMenuItemSubMenu) parentMenuItem;
                bottomLevelItems = mism.getSubItems();
            } else {
                bottomLevelItems = Collections.singletonList(parentMenuItem);
            }

            int tabIdx = 0;
            for (IMenuItem bottomLevelItem : bottomLevelItems) {
                if (bottomLevelItem != null) {  //bf:-->[zakladki] ukrywanie zakładek menu górne
                    IBikPage<Integer> mw = ((IMenuItemMainWidget) bottomLevelItem).getTab();
                    FlowPanel fp = new FlowPanel();
                    fp.setSize("100%", "100%");
                    // fp.addStyleName("vatican");
                    tlp.add(fp, bottomLevelItem.getCaption());
                    String mwTabId = mw.getId();
                    tabPanelMap.put(mwTabId, tlp);
                    tabIndexMap.put(mwTabId, tabIdx++);
                }
            }

            //NewLookUtils.setupClassOnTabs(tlp);
            tlp.addSelectionHandler(new SelectionHandler<Integer>() {
                public void onSelection(SelectionEvent<Integer> event) {
                    Integer idx = event.getSelectedItem();
                    IBikPage<Integer> mw = ((IMenuItemMainWidget) bottomLevelItems.get(idx)).getTab();
                    submitNewTabSelection(mw, true);
                }
            });
        }
        tlp.setExportAndImportBtnVsible(tabId);
        int tabIdx = tabIndexMap.get(tabId);

        currentActiveMenuItem = tabIdToMainWidgetMap.get(tabId);

        if (initedTabIds.contains(tabId)) {
            //refreshWidgetsStateOfCurrentTab(currentActiveTab, refreshDetails);
            // no-op here now...
        } else {
            initedTabIds.add(tabId);
            FlowPanel fp = (FlowPanel) tlp.getWidget(tabIdx);
            Widget tabBodyWidget = getCurrentActiveTab().createMainWidget();

            fp.add(tabBodyWidget);
            // tabBodyWidget.setStyleName("crazy4beer");
        }

        //currentActiveTab.activatePage(false, optValToSelectOnTab);
        refreshWidgetsStateOfCurrentTab(optValToSelectOnTab);

        tlp.selectTab(tabIdx, false);

        mainBodyContainer.clear();
        mainBodyContainer.add(tlp);

        currentTopLevelMenuWidget = topLevelMenuWidget;
        currentSecondaryLevelMenuWidget = secondaryLevelMenuWidget;

        bikCenter.scrollTopMenu();
        return getCurrentActiveTab();
    }

    protected void getTabIdsInMenuInner(IMenuItem item, Collection<String> res) {
        if (item instanceof IMenuItemMainWidget) {
            IMenuItemMainWidget mimw = (IMenuItemMainWidget) item;

            IBikPage<Integer> mw = mimw.getTab();
            String mwId = mw.getId();

            res.add(mwId);

            tabIdToMainWidgetMap.put(mwId, mimw);
        } else {
            IMenuItemSubMenu misb = (IMenuItemSubMenu) item;

            if (misb != null && misb.getSubItems() != null) { //bf:-->[zakladki] ukrywanie zakładek menu górne (dodanie misb != null )
                for (IMenuItem subItem : misb.getSubItems()) {
                    getTabIdsInMenuInner(subItem, res);
                }
            }
        }
    }

    protected Collection<String> getTabIdsInMenu(IMenuItem item) {
        List<String> res = new ArrayList<String>();

        getTabIdsInMenuInner(item, res);

        return res;
    }
//    private final static Map<String, String> siKindToTabIdMap = new HashMap<String, String>();
//
//    static {
//        siKindToTabIdMap.put("Webi", "repTab");
//        siKindToTabIdMap.put("Flash", "repTab");
//        siKindToTabIdMap.put("Universe", "univTab");
//        siKindToTabIdMap.put("MetaData.DataConnection", "connTab");
//        siKindToTabIdMap.put("Wpis blogowy", "blogTab");
//        siKindToTabIdMap.put("Metapedia", "mpTab");
//        siKindToTabIdMap.put("Glosariusz", "mpTab2");
//        siKindToTabIdMap.put("Teradata", "teraTab");
//        siKindToTabIdMap.put("Schema", "teradataTab2");
//    }
//
//    public String siKindToTabId(String siKind) {
//        String tabId = siKindToTabIdMap.get(siKind);
//
//        if (tabId == null) {
//            throw new RuntimeException("siKind = " + siKind + " is not supported");
//        }
//
//        return tabId;
//    }

    //ww: dziwne, ciekawe kto i po co to tutaj dodał...
//    private Boolean loginInfoForLoggedIn = null;
//    public void setLoginInfo() {
//        boolean isLoggedIn = BIKClientSingletons.isUserLoggedIn();
////        if (loginInfoForLoggedIn != null && isLoggedIn == loginInfoForLoggedIn) {
////            return;
////        }
//
//        loginLogoutPanel.clear();
//
//        if (SupportedLocaleNameEnumerator.isMultiLanguage()) {
//            SupportedLocaleNameEnumerator.enumerate(SupportedLocaleNameEnumerator.EnumerationMode.CurrentAsLast, new IParametrizedContinuation<SupportedLocaleNameEnumerator>() {
//                @Override
//                public void doIt(SupportedLocaleNameEnumerator param) {
//                    boolean isCurrent = param.isCurrent();
//                    String localeName = param.getLocaleName();
//                    Widget w = isCurrent ? new InlineLabel(localeName)
//                            : new Anchor(localeName, param.getUrlForLocale());
//                    w.setStyleName("localeLangSelector");
//                    w.setStyleName((isCurrent ? "" : "in" /* I18N: no */) + "active" /*i18n: no*/, true);
//                    w.setStyleName("localeInfo_" + localeName, true);
//                    w.setTitle(isCurrent ? I18n.aktualnieWybranyJezyk.get() /* I18N:  */ : I18n.zmienJezyk.get() /* I18N:  */);
//                    loginLogoutPanel.add(w);
//                }
//            });
//        }
//
//        if (isLoggedIn) {
//            loginLogoutPanel.add(new InlineHTML(I18n.witaj.get() /* I18N:  */ + ", "));//<a href=\"#myBIK\"><b>" + BIKClientSingletons.getLoggedUserName() + "</b></a>"));
//            Anchor userNameAnc = new Anchor(BIKClientSingletons.getLoggedUserName());
//            userNameAnc.setStyleName("welcomeUserLogin");
//            userNameAnc.addClickHandler(BIKClientSingletons.createClickHandlerForShowMyBIKSTab(ReachableMyBIKSTabCode.EditUser));
//            loginLogoutPanel.add(userNameAnc);
//            BikCustomButton logoutBtn = new BikCustomButton(I18n.wyloguj.get() /* I18N:  */, "labelLink",
//                    new IContinuation() {
//                        public void doIt() {
//                            BIKClientSingletons.performUserLogout(BIKClientSingletons.performOnLoginLogoutCont);
//                        }
//                    });
//            //logoutBtn.addStyleName("labelLink");
//            loginLogoutPanel.add(logoutBtn);
//        } else {
//            loginLogoutPanel.add(new InlineHTML(I18n.witajNieznajomy.get() /* I18N:  */));
//            BikCustomButton loginBtn = new BikCustomButton(I18n.zaloguj.get() /* I18N:  */, "labelLink",
//                    new IContinuation() {
//                        public void doIt() {
//                            forms.showLoginDialog();
//                        }
//                    });
//            //loginBtn.addStyleName("labelLink");
//            loginLogoutPanel.add(loginBtn);
//        }
//        loginInfoForLoggedIn = isLoggedIn;
//    }
    public void refreshAfterLoginLogout() {

        Window.Location.replace(MultiXUrlCreator.createUrl());
//        Window.Location.reload();
//        refreshWidgetsStateOfCurrentTab(//currentActiveTab/*, true*/
//                null);
//        setLoginInfo();
    }

//    public void refreshAndRedisplayData() {
//        if (currentActiveTab instanceof IRefreshAndRedisplayData) {
//            IRefreshAndRedisplayData refreshable = (IRefreshAndRedisplayData) currentActiveTab;
//            refreshable.refreshAndRedisplayData();
//        }
//    }
//    public String getCurrentTabId() {
//        if (currentActiveTab == null) {
//            return null;
//        }
//        return currentActiveTab.getId();
//    }
    public boolean invalidateTabData(String tabId) {
        invalidatedTabIds.add(tabId);

        boolean isCurrTab = getCurrentActiveTab() != null && BaseUtils.safeEquals(getCurrentActiveTab().getId(), tabId);

        if (isCurrTab) {
            refreshWidgetsStateOfCurrentTab(getCurrentActiveTab().getSelectedNodeId());
        }

        return isCurrTab;
    }

    public boolean invalidateNodeData(Integer nodeId) {
        Integer currentNodeId = getCurrentActiveTab().getDisplayedNodeId();

        boolean isCurrNode = BaseUtils.safeEquals(currentNodeId, nodeId);

        if (isCurrNode) {
            getCurrentActiveTab().activatePage(false, null, true);
        }

        return isCurrNode;
    }

    public Pair<String, Integer> getCurrentTabIdAndSelectedVal() {
        return new Pair<String, Integer>(getCurrentActiveTab() != null ? getCurrentActiveTab().getId() : null,
                getCurrentActiveTab() != null ? getCurrentActiveTab().getSelectedNodeId() : null);
    }

    public boolean hasTab(String tabId) {
        return topLevelMenuWidgetMap.get(tabId) != null;
    }

    public String getCurrentTabCaption() {
        return currentActiveMenuItem != null ? currentActiveMenuItem.getCaption() : null;
    }

    public IBikPage<Integer> getCurrentTab() {
        return getCurrentActiveTab();
    }

    public IBikPage<Integer> getTabById(String tabId) {
        IMenuItemMainWidget mimw = tabIdToMainWidgetMap.get(tabId);
        return mimw == null ? null : mimw.getTab();
    }

    public IBikPage<Integer> getCurrentActiveTab() {
        return currentActiveMenuItem != null ? currentActiveMenuItem.getTab() : null;
    }
}
