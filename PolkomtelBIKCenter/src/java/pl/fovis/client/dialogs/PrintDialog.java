/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.PrintTemplateBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;

/**
 *
 * @author bfechner
 */
public class PrintDialog extends BaseActionOrCancelDialog {

    protected RadioButton generateHtml, generatePdf;
    protected Integer nodeId;
    protected Integer treeId;
    protected Integer nodeKindId;
    protected String nodeKindCode;
    protected Map<String, PrintTemplateBean> templates;
    protected ListBox lbs;
    String name;

    @Override
    protected String getDialogCaption() {
        return I18n.wydruk.get();

    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.drukuj.get();
    }

    public PrintDialog(Integer nodeId, String name, Integer treeId, Integer nodeKindId, String nodeKindCode, Map<String, PrintTemplateBean> templates) {

        this.nodeId = nodeId;
        this.nodeKindId = nodeKindId;
        this.treeId = treeId;
        this.nodeKindCode = nodeKindCode;
        this.templates = templates;
        this.name = name;
    }

    @Override
    protected void buildMainWidgets() {
        VerticalPanel vp = new VerticalPanel();

        HorizontalPanel hp = new HorizontalPanel();
        hp.add(generateHtml = new RadioButton("action", I18n.generujHtml.get()));
        generateHtml.setVisible(BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_TO_HTML, null));
        hp.add(generatePdf = new RadioButton("action", I18n.generujPdf.get()));
        generatePdf.setVisible(BIKClientSingletons.isAdminOrExpertOrAuthorOrCreator(BIKConstants.ACTION_EXPORT_TO_PDF, null));
        generateHtml.setValue(true);
        vp.add(hp);

        Label lbTyp = new Label(I18n.formatWydruku.get() + ":  ");
        lbTyp.setStyleName("addAttrLbl");
        Label lbl = new Label(I18n.szablonWydruku.get() + ":  ");
        lbl.setStyleName("addAttrLbl");

        lbs = new ListBox();
        lbs.setWidth("330px");
        lbs.setStyleName("addAttr");
        for (String tmp : templates.keySet()) {
            lbs.addItem(tmp);
        }
        FlexTable table = new FlexTable();
        table.setCellSpacing(5);
        table.setWidget(0, 0, lbTyp);
        table.setWidget(0, 1, hp);
        table.setWidget(1, 0, lbl);
        table.setWidget(1, 1, lbs);
        main.add(table);
    }

    @Override
    protected void doAction() {
        String exportedNodeIdsAsStr = nodeId.toString();
        String treeName = BIKClientSingletons.getTreeBeanById(treeId).name;
        StringBuilder sb = new StringBuilder(FoxyClientSingletons.getWebAppUrlPrefixForFileHandlingServlet());
        PrintTemplateBean pb = templates.get(lbs.getSelectedValue());
        if (generateHtml.getValue()) {
            sb.append("?").append(BIKConstants.TREE_EXPORT_PARAM_NAME).append("=").append(BIKConstants.EXPORT_FORMAT_HTML).append("&")
                    .append(BIKConstants.TREE_EXPORT_ACTION_PARAM_NAME).append("=").append(BIKConstants.TREE_EXPORT_ACTION_CREATE_DOCUMENT).append("&")
                    .append(BIKConstants.TREE_EXPORT_SELECTED_NODE_PARAM_NAME).append("=").append(exportedNodeIdsAsStr).append("&")
                    .append(BIKConstants.TREE_EXPORT_PRINT).append("=").append("print").append("&")
                    .append(BIKConstants.TREE_EXPORT_DEPTH_PARAM_NAME).append("=").append("999").append("&")
                    .append(BIKConstants.TREE_EXPORT_NUMBERING_PARAM_NAME).append("=").append(/*numbering.getValue() ? "1" : "0"*/"0").append("&")
                    .append(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME).append("=").append(treeId).append("&")
                    .append(BIKConstants.TREE_EXPORT_NODE_KIND_ID).append("=").append(nodeKindId).append("&")
                    .append(BIKConstants.TREE_EXPORT_NODE_KIND_CODE).append("=").append(nodeKindCode).append("&")
                    //                    .append(BIKConstants.TREE_EXPORT_SELECTED_NODE_NAME).append("=").append(name).append("&")
                    //                    .append(BIKConstants.TREE_EXPORT_TEMPLATE).append("=").append(BaseUtils.deAccentPolishChars(lbs.getSelectedValue()).replace(" ", "_")).append("&")
                    .append(BIKConstants.TREE_EXPORT_LEVEL).append("=").append(pb.maxLevel).append("&")
                    .append(BIKConstants.TREE_EXPORT_SHOW_PAGE).append("=").append(pb.showPageNumber ? "1" : "0").append("&")
                    .append(BIKConstants.TREE_EXPORT_SHOW_NEW_LINE).append("=").append(pb.showNewLine ? "1" : "0").append("&")
                    .append(BIKConstants.TREE_EXPORT_SHOW_AUTHOR).append("=").append(pb.showAuthor ? "1" : "0").append("&")
                    .append(BIKConstants.TREE_EXPORT_TEMPLATE_NODE_ID).append("=").append(pb.templateNodeId);//.append("&");
//                    .append(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME).append("=").append(BaseUtils.deAccentPolishChars(name).replace(" ", "_"));
        } else if (generatePdf.getValue()) {

            sb.append("?").append(BIKConstants.TREE_EXPORT_PARAM_NAME).append("=").append(BIKConstants.EXPORT_FORMAT_PDF).append("&")
                    .append(BIKConstants.TREE_EXPORT_ACTION_PARAM_NAME).append("=").append(BIKConstants.TREE_EXPORT_ACTION_CREATE_DOCUMENT).append("&")
                    .append(BIKConstants.TREE_EXPORT_SELECTED_NODE_PARAM_NAME).append("=").append(exportedNodeIdsAsStr).append("&")
                    .append(BIKConstants.TREE_EXPORT_DEPTH_PARAM_NAME).append("=").append("999").append("&")
                    .append(BIKConstants.TREE_EXPORT_NUMBERING_PARAM_NAME).append("=").append(/*numbering.getValue() ? "1" : "0"*/"0").append("&")
                    .append(BIKConstants.TREE_EXPORT_TREE_ID_PARAM_NAME).append("=").append(treeId).append("&")
                    .append(BIKConstants.TREE_EXPORT_PRINT).append("=").append("print").append("&")
                    .append(BIKConstants.TREE_EXPORT_NODE_KIND_ID).append("=").append(nodeKindId).append("&")
                    .append(BIKConstants.TREE_EXPORT_NODE_KIND_CODE).append("=").append(nodeKindCode).append("&")
                    //                    .append(BIKConstants.TREE_EXPORT_SELECTED_NODE_NAME).append("=").append(name).append("&")
                    //                    .append(BIKConstants.TREE_EXPORT_TEMPLATE).append("=").append(BaseUtils.deAccentPolishChars(lbs.getSelectedValue()).replace(" ", "_")).append("&")
                    .append(BIKConstants.TREE_EXPORT_LEVEL).append("=").append(pb.maxLevel).append("&")
                    .append(BIKConstants.TREE_EXPORT_SHOW_PAGE).append("=").append(pb.showPageNumber ? "1" : "0").append("&")
                    .append(BIKConstants.TREE_EXPORT_SHOW_NEW_LINE).append("=").append(pb.showNewLine ? "1" : "0").append("&")
                    .append(BIKConstants.TREE_EXPORT_SHOW_AUTHOR).append("=").append(pb.showAuthor ? "1" : "0").append("&")
                    .append(BIKConstants.TREE_EXPORT_TEMPLATE_NODE_ID).append("=").append(pb.templateNodeId);//.append("&");
            //                    .append(BIKConstants.TREE_EXPORT_TREE_NAME_PARAM_NAME).append("=").append(BaseUtils.deAccentPolishChars(name).replace(" ", "_"));
        }
        Window.open(sb.toString(), "_blank", "");
    }

}
