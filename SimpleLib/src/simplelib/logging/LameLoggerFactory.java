/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.logging;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.IContinuationWithReturn;
import simplelib.Pair;
import simplelib.logging.ILameLogger.LameLogLevel;

/**
 *
 * @author wezyr
 */
public class LameLoggerFactory {

    public static final String subLoggerSep = "#";
    public static final String configOptionSep = "/";
    public static final String MIN_LEVEL_KEY_PART = "minLevel";
    public static final String MIN_LEVEL_KEY_NAME = configOptionSep + MIN_LEVEL_KEY_PART;
    public static final LameLogLevel globalMinLogLevel = LameLogLevel.Debug;
    public static final LameLogLevel globalDefaultLogLevel = LameLogLevel.Warn;
    // {simpleName : {fullName : number}}
    private static Map<String, LinkedHashMap<String, Integer>> loggingClassNameCounts
            = new LinkedHashMap<String, LinkedHashMap<String, Integer>>();
    public static Set<ILameLoggerAppender> appenders = new LinkedHashSet<ILameLoggerAppender>();
    private static int msgsSize = 0;
    private static int autoFlushSize = 32000;
    private static List<LameLoggerMsg> msgs = new ArrayList<LameLoggerMsg>();
    private static LameLogLevel minLevelInMsgs = LameLogLevel.None;
    private static LameLogLevel maxLevelInMsgs = LameLogLevel.Debug;
    private static LameLogLevel appendersMinLevel = LameLogLevel.None;
    private static boolean uninitializedWarned = false;
    private static boolean msgLevelErrorWarned = false;
    private static final ILameLogger innerLogger = LameLoggerFactory.getLogger(LameLoggerFactory.class);

    public static synchronized void addAppender(ILameLoggerAppender appender) {
        boolean alreadyAdded = !appenders.add(appender);

        LameLogLevel appenderMinLevel = appender.getMinLogLevel();
        if (!alreadyAdded) {
            if (appenderMinLevel.ordinal() < globalMinLogLevel.ordinal()) {
                loggerConfigWarning("minLogLevel of appender (of class: "
                        + safeGetLoggingClassName(appender) + ") : " + appenderMinLevel
                        + " is lower than global minLogLevel: " + globalMinLogLevel);
            } else {
                if (appendersMinLevel.ordinal() > appenderMinLevel.ordinal()) {
                    appendersMinLevel = appenderMinLevel;
                }
            }
        }

        if (innerLogger.isInfoEnabled()) {
            innerLogger.info("addAppender of class: " + safeGetLoggingClassName(appender)
                    + " " + (alreadyAdded ? "(already added) " : "")
                    + "with minLogLevel: " + appenderMinLevel + ", all appenders ("
                    + appenders.size() + ") minLogLevel: " + appenderMinLevel
                    + ", globalMinLogLevel: " + globalMinLogLevel);
        }
    }

    public static LameLogLevel currentMinLogLevel() {
        return appendersMinLevel;
    }
    private static final Map<String, LameLogLevel> minLevelConfig = new HashMap<String, LameLogLevel>();

    private static LameLogLevel getMinConfiguredLevelInner(String loggerName//, boolean checkLastPart
    ) {
        if (minLevelConfig == null) {
            return globalDefaultLogLevel;
        }

        synchronized (minLevelConfig) {

            LameLogLevel res = minLevelConfig.get(loggerName);
            if (res != null) {
                return res;
            }
            if (BaseUtils.isStrEmpty(loggerName)) {
                return globalDefaultLogLevel; // ? było minlvl
            }

            Pair<String, String> p = BaseUtils.splitByLastNthSep(loggerName, ".", 1);
            //if (checkLastPart) {
            res = minLevelConfig.get(p.v2);
            if (res != null) {
                return res;
            }
            //}
            return getMinConfiguredLevelInner(BaseUtils.nullToDef(p.v1, "")//, false
            );
        }
    }

    private static LameLogLevel getMinConfiguredLevel(String loggerName) {
        String fixedLoggerName = loggerName.replace(subLoggerSep, ".");
        LameLogLevel res = getMinConfiguredLevelInner(fixedLoggerName//, true
        );
        //System.out.println("getMinConfiguredLevel: loggerName=" + loggerName + ", level=" + res);
        return res;
    }

    public static LameLogLevel getConfiguredLoggerLevel(String name) {
        LameLogLevel minLvlFromCfg = minLevelConfig.get(MIN_LEVEL_KEY_NAME);

        if (minLvlFromCfg == null) {
            minLvlFromCfg = globalMinLogLevel;
        }

        //System.out.println("minLvlFromCfg: " + minLvlFromCfg);
        return LameLogLevel.getHigherLevel(minLvlFromCfg, getMinConfiguredLevel(name));
    }

    public static ILameLogger getLogger(String name) {
        return new BaseLameLogger(//getConfiguredLoggerLevel(name, level),
                name);
    }

    public static boolean isInsideLogging() {
        return inSendMsgToOutputCount > 0 || inSendOneMsgToOutputLevel > 0;
    }

    private static void loggerConfigWarning(String msg) {
        System.err.println("loggerConfigWarning: " + msg);
    }

    public static void loggerSystemWarning(String msg) {
        if (innerLogger != null) {
            if (innerLogger.isErrorEnabled()) {
                innerLogger.error("loggerSystemWarning: " + msg);
            }
        } else {
            loggerConfigWarning("loggerSystemWarning: " + msg);
        }
    }
    private static int inSendOneMsgToOutputLevel = 0;
    private static int inSendMsgToOutputCount = 0;

    private static void sendMsgsToOutput(Collection<LameLoggerMsg> msgs) {
        if (appenders.isEmpty()) {
            if (!uninitializedWarned) {
                loggerConfigWarning("no appenders (yet)");
                uninitializedWarned = true;
            }
            return;
        }

        if (inSendOneMsgToOutputLevel >= 1) {
            return;
        }

        inSendOneMsgToOutputLevel++;
        try {
            for (ILameLoggerAppender appender : appenders) {
                appender.sendMsgsToOutput(msgs, minLevelInMsgs, maxLevelInMsgs);
            }
        } finally {
            inSendOneMsgToOutputLevel--;
        }
    }

    public static synchronized void sendMsgToOutput(LameLoggerMsg msg) {
        if (inSendOneMsgToOutputLevel >= 1 || inSendMsgToOutputCount > 0 || getRunWithoutLoggingMode()) {
            return;
        }

        inSendMsgToOutputCount++;
        try {
            if (globalMinLogLevel.ordinal() > msg.level.ordinal()) {
                if (!msgLevelErrorWarned) {
                    loggerConfigWarning("message at level: " + msg.level
                            + ", but globalMinLogLevel is: " + globalMinLogLevel);
                    msgLevelErrorWarned = true;
                }
                return;
            }

            if (appendersMinLevel.ordinal() > msg.level.ordinal()) {
                return;
            }

            msgs.add(msg);
            minLevelInMsgs = LameLogLevel.getLowerLevel(msg.level, minLevelInMsgs);
            maxLevelInMsgs = LameLogLevel.getHigherLevel(msg.level, maxLevelInMsgs);

            msgsSize += msg.msg.length();
            if (inSendMsgToOutputCount == 1 && (bufferingCount == 0 || msgsSize >= autoFlushSize)) {
                flushLogs();
                if (bufferingCount > 0 && innerLogger.isDebugEnabled()) {
                    innerLogger.debug("auto-flushed, msgsSize=" + msgsSize
                            + ", autoFlushSize=" + autoFlushSize);
                }
            }
        } finally {
            inSendMsgToOutputCount--;
        }
    }
    private static int bufferingCount = 0;

    public static synchronized void startBuffering() {
        bufferingCount++;
    }

    public static synchronized void endBuffering() {
        if (bufferingCount == 0) {
            if (innerLogger.isErrorEnabled()) {
                innerLogger.error("endBuffering called, but buffering count == 0");
            }
        } else {
            bufferingCount--;
        }
        if (bufferingCount == 0) {
            flushLogs();
        }
    }

    private static synchronized void flushLogs() {
        sendMsgsToOutput(msgs);
        msgs.clear();
        msgsSize = 0;
        minLevelInMsgs = LameLogLevel.None;
        maxLevelInMsgs = LameLogLevel.Debug;
    }

    public static ILameLogger getLogger(Class c) {
        return getLogger(//safeGetLoggingClassNameForClass(c)
                c == null ? "<null>" : c.getName());
    }

    public static String safeGetLoggingClassName(Object o) {
        if (o == null) {
            return "<null>";
        }

        return getLoggingClassName(o.getClass().getName());
    }

    public static synchronized String getLoggingClassName(String className) {
        String simpleClassName = BaseUtils.extractSimpleClassName(className);
        LinkedHashMap<String, Integer> fullNames = loggingClassNameCounts.get(simpleClassName);
        if (fullNames == null) {
            fullNames = new LinkedHashMap<String, Integer>();
            loggingClassNameCounts.put(simpleClassName, fullNames);
        }
        Integer num = fullNames.get(className);
        boolean newNum = num == null;
        if (newNum) {
            num = fullNames.size();
            fullNames.put(className, num);
        }

        String simpleClassNameWithNum;
        if (num != 0) {
            simpleClassNameWithNum = simpleClassName + "#" + num;
        } else {
            simpleClassNameWithNum = simpleClassName;
        }

        if (innerLogger != null && newNum && num >= 1) {
            if (innerLogger.isInfoEnabled()) {
                innerLogger.info("from now on, class " + className + " will be logged as " + simpleClassNameWithNum);
            }
            if (num == 1 && innerLogger.isInfoEnabled()) {
                innerLogger.info("already logged class name " + simpleClassName + " refers to " + fullNames.keySet().iterator().next());
            }
        }
        return simpleClassName;
    }

    public static synchronized String safeGetLoggingClassNameForClass(Class c) {
        return c == null ? "<null>" : getLoggingClassName(c.getName());
    }

    public static ILameLogger getClassLogger() {
        return getLogger(LameLoggerFactory.class);
    }

    public static void applyLameLoggersConfig(Map<String, String> cfg) {
        applyLameLoggersConfig(cfg, true);
    }

    public static void innerSetOneConfigEntry(String key, String val) {
        //BaseUtils.nullToDef(p.v1, p.v2);
        String properLvlName = BaseUtils.capitalize(val.toLowerCase());
        LameLogLevel lvl = BaseUtils.safeTryStringToEnum(
                LameLogLevel.class, properLvlName, null);

        if (lvl != null) {
            innerSetOneConfigEntry(key, lvl);
        } else {
            if (innerLogger.isStageEnabled()) {
                innerLogger.stage("applyLameLoggersConfig: key=\"" + key + "\" "
                        + "has unproper logger level \"" + val + "\" (fixed: "
                        + properLvlName + ")");
            }
        }
    }

    public static void innerSetOneConfigEntry(String key, LameLogLevel lvl) {
        Pair<String, String> p = BaseUtils.splitByLastNthSep(key, configOptionSep, 1);

        String loggerName = p.v1;

        if (BaseUtils.safeEquals(p.v1, "") && BaseUtils.safeEquals(MIN_LEVEL_KEY_PART, p.v2)) {
            loggerName = key; // == "/minLevel";
        } else if (!BaseUtils.safeEquals("level", p.v2) && p.v1 != null) {
            if (innerLogger.isStageEnabled()) {
                innerLogger.stage("applyLameLoggersConfig: key=\"" + key + "\" "
                        + "has unproper option to configure (" + p.v2 + "), line ignored");
            }
            return;
        }

        if (loggerName == null) {
            loggerName = p.v2;
        }

        minLevelConfig.put(loggerName, lvl);
    }

    public static void setOneConfigEntry(String key, String val) {
        synchronized (minLevelConfig) {
            updateConfigLastModified();
            innerSetOneConfigEntry(key, val);
        }
    }

    public static void setOneConfigEntry(ILameLogger logger, LameLogLevel lvl) {
        setOneConfigEntry(logger.getFullLoggerName(), lvl);
    }

    public static void setOneConfigEntry(String key, LameLogLevel lvl) {
        synchronized (minLevelConfig) {
            innerSetOneConfigEntry(key, lvl);
            updateConfigLastModified();
        }
    }

    public static void setOneConfigEntry(Class c, LameLogLevel lvl) {
        setOneConfigEntry(c.getName(), lvl);
    }

    // keys/vals cannot be null
    public static void applyLameLoggersConfig(Map<String, String> cfg, boolean appendCfg) {
        synchronized (minLevelConfig) {
            if (!appendCfg) {
                minLevelConfig.clear();
            }

            //System.out.println("applying lame loggers config:" + cfg);
            for (Entry<String, String> e : cfg.entrySet()) {
                String key = e.getKey().trim();
                String val = e.getValue().trim();
                //System.out.println("key=" + key + ":val=" + val);
                innerSetOneConfigEntry(key, val);
            }

            updateConfigLastModified();
        }

        //System.out.println("***** minLevelConfig after config applying: " + minLevelConfig);
        if (innerLogger.isDebugEnabled()) {
            innerLogger.debug("minLevelConfig after config applying: " + minLevelConfig);
        }
//        else {
//            System.out.println("my inner logger has level: " + innerLogger.getLoggerLevel() +
//                    ", name=" + innerLogger.getFullLoggerName());
//        }
    }

    public static ILameLogger getSubLogger(ILameLogger parentLogger, String suffix) {
        return getLogger(parentLogger.getFullLoggerName() + subLoggerSep + suffix);
    }
    private static long configLastModified = 1;

    public static long getConfigLastModified() {
        return configLastModified;
    }

    private static void updateConfigLastModified() {
        configLastModified++;// = //System.nanoTime();
        //System.currentTimeMillis();

        if (innerLogger.isDebugEnabled()) {
            innerLogger.debug("minLevelConfig=" + minLevelConfig);
        }
    }
    private static ILameLoggingEnvironment logEnv;

    synchronized private static ILameLoggingEnvironment getLoggingEnvironment() {
        if (logEnv == null) {
            logEnv = new GlobalLameLoggingEnvironment();
        }
        return logEnv;
    }

    public static <T> T runWithoutLogging(boolean noLogging, IContinuationWithReturn<T> cont) {
        return getLoggingEnvironment().runWithoutLogging(noLogging, cont);
    }

    synchronized public static void setLoggingEnvironment(ILameLoggingEnvironment logEnv) {
        LameLoggerFactory.logEnv = logEnv;
    }

    public static boolean getRunWithoutLoggingMode() {
        if (!BaseLameLogger.IsLameLoggerEnabled) {
            return true;
        }
        return getLoggingEnvironment().getRunWithoutLoggingMode();
    }

    public static String getCurrentRuningContextInfo() {
        return getLoggingEnvironment().getCurrentRuningContextInfo();
    }
}
