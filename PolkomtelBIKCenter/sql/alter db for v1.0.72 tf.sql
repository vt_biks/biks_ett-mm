﻿exec sp_check_version '1.0.72';
go


delete from bik_data_load_log where data_source_type_id=(select id from bik_data_source_def where description = 'BO Universe Designer')
delete from bik_data_source_def where description = 'BO Universe Designer'


exec sp_update_version '1.0.72', '1.0.73';
go