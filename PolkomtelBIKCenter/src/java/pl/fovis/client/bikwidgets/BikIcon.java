/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.Image;

/**
 *
 * @author AMamcarz
 */
public class BikIcon {

    public static Image createIcon(String iconName, String iconStyle) {
        Image objIcon = new Image(iconName);
        objIcon.setStyleName(iconStyle);
        return objIcon;
    }
}
