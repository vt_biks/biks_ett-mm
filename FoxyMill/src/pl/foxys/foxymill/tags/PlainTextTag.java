/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.foxymill.tags;

import java.util.Map;
import pl.foxys.foxymill.RenderEnvironment;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import simplelib.TextFilePosition;

/**
 *
 * @author wezyr
 */
public class PlainTextTag implements ITag {
    private String txt;
    
    public PlainTextTag(String txt) {
        this.txt = txt;
    }

    public void render(//IRenderContext
            IRenderCtx renderCtx) {
        renderCtx.getOutputDevice().append(txt);
    }

    public void prepare(RenderEnvironment renderEnv) {
        //no-op
    }

//    public void init(Map<String, String> attrs, List<ITag> subTags, ITag prevTag,
//            TextFilePosition tfp) {
//        // not used
//    }

    public void fixUp(Map<String, Object> globals) {
        // no-op
    }

    public TextFilePosition getTextFilePosition() {
        return null;
    }
}
