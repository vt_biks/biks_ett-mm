/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.util.Map;
import java.util.Set;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.UserBean;

/**
 *
 * @author tflorczak
 */
public interface IBOXIServiceForTabWidgetDP {

    public Set<String> getDynamicTreeKindCodes();

    public UserBean bikUserByNodeId(int nodeId);

    public String getAvatarFieldForAd();

    public TreeNodeBean getNodeBeanById(int nodeId, boolean allowNull);

    public Map<Integer, Float> getOptSimilarNodes(int nodeId);

    public int getSuggestSimilarMaxResults();
}
