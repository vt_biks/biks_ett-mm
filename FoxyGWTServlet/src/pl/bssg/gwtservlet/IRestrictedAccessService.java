/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.gwtservlet;

import java.lang.reflect.Method;

/**
 *
 * @author pku
 */
public interface IRestrictedAccessService {

    public boolean canAccess(Method mtc);

    public boolean userNotLoggedInError(Method implMethod);

    public Exception getExceptionForNotLoggedUser();
}
