﻿IF OBJECT_ID('tempdb..#xxx') IS NOT NULL 
drop table #xxx
go

declare @rapTreeId int = (select id from bik_tree where code = 'Reports')
declare @rapWebiKindId int = (select dbo.fn_node_kind_id_by_code('Webi'))
declare @qryKindId int = (select dbo.fn_node_kind_id_by_code('ReportQuery'))
declare @objMeasureKindId int = (select dbo.fn_node_kind_id_by_code('Measure')) 
declare @objDimensionKindId int = (select dbo.fn_node_kind_id_by_code('Dimension')) 
declare @objDetailKindId int = (select dbo.fn_node_kind_id_by_code('Detail'))
declare @objFilterKindId int = (select dbo.fn_node_kind_id_by_code('Filter'))
declare @univTreeId int = (select id from bik_tree where code = 'ObjectUniverses')
declare @univKindId int = (select dbo.fn_node_kind_id_by_code('Universe'))
declare @connKindId int = (select dbo.fn_node_kind_id_by_code('DataConnection'))
declare @connTreeId int = (select id from bik_tree where code = 'Connections')
declare @derivedTableKindId int = (select dbo.fn_node_kind_id_by_code('UniverseDerivedTable')) 
declare @aliasTableKindId int = (select dbo.fn_node_kind_id_by_code('UniverseAliasTable')) 
declare @tableKindId int = (select dbo.fn_node_kind_id_by_code('UniverseTable')) 

select
		cast('' as varchar(max)) as uni_parent_path,
		uni.parent_node_id as uni_ancestor_id,
		uni.name as uni_name,
		uni.id as uni_id,
		extUni.cuid as uni_cuid,
		extUni.created as uni_created,
		extUni.modified as uni_modified,
		--extUni.author as uni_author,
		--extUni.owner as uni_owner,
		extUni.file_path + extUni.file_name as uni_file_path,
		extUni.statistic as uni_statistic,
		conn.name as conn_name,
		cast('' as varchar(max)) as class_path,
		obj.parent_node_id as class_ancestor_id,
		obj.name as obj_name,
		extObj.text_of_select as obj_text_of_select,
		extObj.text_of_where as obj_text_of_where,
		extObj.aggregate_function as obj_aggregate_function,
		extObj.type as obj_type,
		tab.name as table_name,
		rap.name as report_name

into #xxx
from
		bik_node uni
		inner join bik_node obj on obj.branch_ids like uni.branch_ids + '%'
		inner join bik_joined_objs objs on objs.src_node_id = uni.id and type = 1
		inner join bik_node conn on conn.id=objs.dst_node_id
		inner join bik_sapbo_universe_object extObj on extObj.node_id=obj.id
		inner join bik_sapbo_extradata extUni on extUni.node_id=uni.id
		left join (bik_joined_objs tabjoins
		inner join bik_node tab on tab.id = tabjoins.dst_node_id) on tabjoins.src_node_id = obj.id and tab.is_deleted = 0 and tab.linked_node_id is null and tab.tree_id = @univTreeId and tab.node_kind_id in (@derivedTableKindId, @aliasTableKindId, @tableKindId)
		left join (bik_joined_objs repObj 
		inner join bik_node rap on rap.id = repObj.dst_node_id) on repObj.src_node_id = obj.id and repObj.type = 1 and rap.is_deleted = 0 and rap.linked_node_id is null and rap.tree_id = @rapTreeId and rap.node_kind_id = @rapWebiKindId
where 
		uni.is_deleted = 0 and uni.linked_node_id is null and uni.tree_id = @univTreeId and uni.node_kind_id = @univKindId
		and obj.is_deleted = 0 and obj.linked_node_id is null and obj.tree_id = @univTreeId and obj.node_kind_id in (@objMeasureKindId, @objDimensionKindId, @objDetailKindId,@objFilterKindId)
		and conn.is_deleted = 0 and conn.linked_node_id is null and conn.tree_id = @connTreeId and conn.node_kind_id = @connKindId
go


while 1 = 1 begin
  declare @rc int = null

  update #xxx
  set uni_parent_path = bn.name + '\' + uni_parent_path, uni_ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #xxx.uni_ancestor_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go

while 1 = 1 begin
  declare @rc int = null

  update #xxx
  set class_path = bn.name + '\' + class_path, class_ancestor_id = bn.parent_node_id
  from bik_node bn
  where bn.id = #xxx.class_ancestor_id
  and bn.id <> #xxx.uni_id
  
  set @rc = @@rowcount
  
  if @rc = 0 break
end
go


--select * from #xxx
select uni_parent_path,uni_name,uni_cuid,uni_created,uni_modified,uni_file_path,uni_statistic,conn_name,class_path,obj_name,obj_text_of_select,obj_text_of_where,obj_aggregate_function,obj_type,table_name,report_name
from #xxx
order by uni_parent_path,uni_name,class_path,obj_name