/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.dialogs.TreeNodeDialog;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class NodeActionRenameRole extends NodeActionUpdatingBase {

    public String getCaption() {
        return I18n.zmienNazweRoli.get() /* I18N:  */;
    }

    public void execute() {
        new TreeNodeDialog().buildAndShowDialog(node.nodeKindId, node.nodeKindCode, node, null, node.treeId, null, new IParametrizedContinuation<Pair<TreeNodeBean, Map<String, AttributeBean>>>() {
            public void doIt(Pair<TreeNodeBean, Map<String, AttributeBean>> param2) {
                TreeNodeBean param=param2.v1;
                String name = node.name;
                int nodeId = node.id;
                BIKClientSingletons.getService().updateTreeNodeWithTheUserRole(nodeId, name, new StandardAsyncCallback<List<String>>("Failure on" /* I18N: no */ + " updateTreeNode") {
                    public void onSuccess(List<String> result) {
                        BIKClientSingletons.showInfo(I18n.zmienionoNazwe.get() /* I18N:  */);
                        refreshAndRedisplayDataByTreeCode(result);
                        if (result == null || result.isEmpty()) {
                            redisplayData();
                        }
                    }
                });
                ctx.getTreeGrid().setNodeName(nodeId, name);
                maybeSortSiblingsWithDefaultOrder();

            }
        });
    }

    @Override
    public boolean isEnabledByDesign() {
        if (isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_USERS_ROLE)
                //                && isAppAdmin()
                //ww->authors: dopuszczam też eksperta
                && isEffectiveExpert()
                && (!isNodeBuiltIn() || isInUltimateEditMode())) {
            return true;
        }
        return false;
    }
}
