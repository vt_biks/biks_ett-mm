﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- Nowa wersja MultiX
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------


---UWAGA: Tylko raz wykonujemy ten skrypt-------------
exec sp_mltx_check_version '1.16.y'
go

if not exists (select 1 from mltx_app_prop where name='useHashedPassword') 
insert into mltx_app_prop(name, val) values ('useHashedPassword', '0')
go

--update mltx_app_prop set val='1' where name='useHashedPassword';

if exists (select 1 from mltx_app_prop where name='useHashedPassword' and val='1')
update mltx_registered_user set password=hashbytes('SHA1', password) where password is not null

exec sp_mltx_update_version '1.16.y', '1.16.z'
go