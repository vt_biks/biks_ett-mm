/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill;

import commonlib.LameJarResourceProvider;
import java.util.HashMap;
import pl.foxys.foxymill.tags.ITag;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.foxys.foxymill.renderctx.RenderCtx;
import static org.junit.Assert.*;
import simplelib.RawString;

/**
 *
 * @author wezyr
 */
public class ParserTaskTest {

    public ParserTaskTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testRead1() {
        ParserTask pt = new ParserTask("ala %{dupa}% ma kota");
        List<ITag> res = pt.read();
        assertEquals(3, res.size());

        pt = new ParserTask("ala <f:insert expr='dupa'/> ma kota");
        res = pt.read();
        assertEquals(3, res.size());

        pt = new ParserTask("ala <f:insert expr='dupa'/>%{xxx}% ma kota");
        res = pt.read();
        assertEquals(4, res.size());

        pt = new ParserTask("ala <f:insert expr='dupa'>%{xxx}%</f:insert>%{uu}% ma kota");
        res = pt.read();
        assertEquals(4, res.size());
    }

//    public static Map<String, IExpressionLanguage> getExprLangs(boolean defaultMvel) {
//        return FoxyMillUtils.getExprLangs(defaultMvel);
//    }
//
//    public static Map<String, IExpressionLanguage> getExprLangs() {
//        return getExprLangs(true);
//    }
//
    public static RenderEnvironment makeRenderEnv(boolean defaultMvel) {
        return FoxyMillUtils.makeRenderEnvForHTML(defaultMvel, false);
    }

    public static RenderEnvironment makeRenderEnv() {
        return makeRenderEnv(true);
    }

    @Test
    public void testEval1() {
        ParserTask pt = new ParserTask("shit-%{(int)dupa/2}%");
        List<ITag> res = pt.read();
        assertEquals(2, res.size());
        RenderCtx renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", 666);
        //renderCtx.getVars().put("kupa", 2);

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("shit-333", renderCtx.getOutputStr());

        pt = new ParserTask("<f:if cond='dupa==666'>dupa-OK</f:if>");
        res = pt.read();
        assertEquals(1, res.size());
        renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", 666);

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("dupa-OK", renderCtx.getOutputStr());

        pt = new ParserTask("<f:if cond='dupa==666'>dupa-OK</f:if><f:else>dupa-NOT-ok:%{dupa}%</f:else>");
        res = pt.read();
        assertEquals(2, res.size());
        renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", 777);

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);

        String resStr = renderCtx.getOutputStr();
        //System.out.println("dupa-NOT-ok:777 <--> " + resStr);
        assertEquals("dupa-NOT-ok:777", resStr);

        renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", 666);

        FoxyMillUtils.render(res, renderCtx);

        assertEquals("dupa-OK", renderCtx.getOutputStr());
    }

    @Test
    public void testEvalElseIf() {
        ParserTask pt = new ParserTask("<f:if cond='dupa==666'>dupa-OK</f:if><f:elseif cond='dupa==777'>dupa-ok2:%{dupa}%</f:elseif>*aa");
        List<ITag> res = pt.read();
        RenderCtx renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", 777);

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("dupa-ok2:777*aa", renderCtx.getOutputStr());

        pt = new ParserTask("<f:if cond='dupa==666'>dupa-OK</f:if><f:elseif cond='dupa==777'>dupa-ok2:%{dupa}%</f:elseif><f:else>dupa-źle!</f:else>*aa");
        res = pt.read();
        renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", 7777);

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("dupa-źle!*aa", renderCtx.getOutputStr());
    }

    @Test
    public void testEvalFor() {
        ParserTask pt = new ParserTask("<f:for var='ddd' expr='dupa'>iter:%{ddd}%</f:for>");
        List<ITag> res = pt.read();
        RenderCtx renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", new int[]{10, 20, 33});

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("iter:10iter:20iter:33", renderCtx.getOutputStr());

        pt = new ParserTask("<f:for var='ddd' expr='dupa' sep='|'>iter:%{ddd}%</f:for>");
        res = pt.read();
        renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", new int[]{10, 20, 33});

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("iter:10|iter:20|iter:33", renderCtx.getOutputStr());

        pt = new ParserTask("<f:for statusVar='ddd' expr='dupa' sep='|'>%{ddd.idx+1}%:%{ddd.val}%</f:for>");
        res = pt.read();
        renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", new Object[]{10, "aaa", 33});

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("1:10|2:aaa|3:33", renderCtx.getOutputStr());
    }

    @Test
    public void testEvalSet() {
        ParserTask pt = new ParserTask("dupa*3=<f:set var='ddd' expr='dupa*2'/>%{ddd+dupa}%");
        List<ITag> res = pt.read();
        RenderCtx renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", 666);

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("dupa*3=" + (666 * 3), renderCtx.getOutputStr());
    }

    @Test
    public void testEvalWhile() {
        ParserTask pt = new ParserTask("<f:set var='i' expr='1'/>"
                + "begin<f:while cond='i < 5'>(%{i*2}%)<f:set var='i' expr='i+1'/></f:while>end");
        List<ITag> res = pt.read();
        RenderCtx renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", 666);

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("begin(2)(4)(6)(8)end", renderCtx.getOutputStr());
    }

    @Test
    public void testEvalInsertRenderable() {
        ParserTask pt = new ParserTask("<f:set var='pp' params='x'>x*2=%{x*2}%</f:set>"
                + "test:<f:insert expr='pp' x='101'/>");
        List<ITag> res = pt.read();
        RenderCtx renderCtx = RenderCtx.makeRootRenderCtx();
        //renderCtx.getVars().put("dupa", 666);

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("test:x*2=202", renderCtx.getOutputStr());
    }

    @Test
    public void testEvalInsertInOgnl() {
        ParserTask pt = new ParserTask("<f:insert expr='#dupa*2' exprLang='ognl'/>");
        List<ITag> res = pt.read();
        RenderCtx renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", 666);

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("1332", renderCtx.getOutputStr());

        pt = new ParserTask("<f:insert expr='#dupa*2'/>");
        res = pt.read();
        renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", 666);
        FoxyMillUtils.prepareTags(res, makeRenderEnv(false));
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("1332", renderCtx.getOutputStr());
    }

    @Test
    public void testEvalError() {
        ParserTask pt = new ParserTask("<f:insert expr='#dupa*2' exprLang='ognl'/>");
        List<ITag> res = pt.read();
        RenderCtx renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", 666);

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("1332", renderCtx.getOutputStr());

        pt = new ParserTask("<f:insert expr='#dupa*2'/>");
        res = pt.read();
        renderCtx = RenderCtx.makeRootRenderCtx();
        renderCtx.getVars().put("dupa", 666);
        FoxyMillUtils.prepareTags(res, makeRenderEnv(false));
        FoxyMillUtils.render(res, renderCtx);

        assertEquals("1332", renderCtx.getOutputStr());
    }

    @Test
    public void testCache1() {
        ParserTask pt = new ParserTask("<f:cache key='\"x\"'>ala ma kota</f:cache>");
        List<ITag> res = pt.read();
        RenderCtx renderCtx = RenderCtx.makeRootRenderCtx();

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);
        assertEquals("ala ma kota", renderCtx.getOutputStr());

        FoxyMillUtils.render(res, renderCtx);
        assertEquals("ala ma kota", renderCtx.getOutputStr());
    }

    @Test
    public void testSetToVar() {
        ParserTask pt = new ParserTask("<f:set var='x' expr='10'/>"
                + "<f:set var='a' render='true'>%{x+10}%</f:set>res=%{a}%");
        List<ITag> res = pt.read();
        RenderCtx renderCtx = RenderCtx.makeRootRenderCtx();

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);
        assertEquals("res=20", renderCtx.getOutputStr());
    }

    @Test
    public void testNamedTemplateByResProvider() {
        IFoxyMillNamedTagsProvider ntp = new NamedTemplateByResProvider2(
                new LameJarResourceProvider(), "/pl/foxys/foxymill");
        List<ITag> res = ntp.getParsedTemplate("xxx");
        System.out.println("res.size=" + res.size());
        res = ntp.getParsedTemplate("xxx");
        System.out.println("#2:res.size=" + res.size());
        res = ntp.getParsedTemplate("yyy");
        System.out.println("#3:res.size=" + res.size());
    }

    @Test
    public void testNamedTemplateRenderer() {
        IFoxyMillNamedTagsProvider ntp = new NamedTemplateByResProvider2(
                new LameJarResourceProvider(), "/pl/foxys/foxymill");
        INamedTemplateRenderer renderer = new NamedTemplateRenderer(ntp);

        Map<String, Object> vars = new HashMap<String, Object>();
        vars.put("dupa", 103);
        String res = renderer.render("yyy", vars, null);
        System.out.println("res=" + res);

        vars.put("dupa", -666);
        res = renderer.render("yyy", vars, null);
        System.out.println("res=" + res);
    }

    @Test
    public void testSetToVarForTF() {
        ParserTask pt = new ParserTask("<f:set var='dd' params='u'>%{new simplelib.RawString(u)}%:%{u}%</f:set>\n"
                + "<f:set var='x'>\n"+
                "<f:set var='a'>ala ma kota\n"
                + "shit xxx!</f:set><f:insert var='q' expr='a' asStr='true'/>res=<f:dd u='q'/></f:set>");
        List<ITag> res = pt.read();
        //RawString s;
        RenderCtx renderCtx = RenderCtx.makeRootRenderCtx();

        FoxyMillUtils.prepareTags(res, makeRenderEnv());
        FoxyMillUtils.render(res, renderCtx);
        FoxyMillUtils.fixupTags(res, renderCtx.getVars());
        System.out.println("--- testSetToVarForTF: ---");
        //System.out.println("x=" + renderCtx.getVars().get("x")); 
        IRenderable xR = (IRenderable)renderCtx.getVars().get("x");
        xR.render(renderCtx);
        System.out.println(renderCtx.getOutputStr());
    }
}
