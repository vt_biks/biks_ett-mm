/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.user.client.Random;
import simplelib.BaseUtils;
import simplelib.ISimpleRandomGenerator;

/**
 *
 * @author pmielanczuk
 */
public class JavaScriptSimpleRandomGenerator implements ISimpleRandomGenerator {

    private static boolean isFactorySet = false;
    
    public static synchronized void setFactory() {
        if (!isFactorySet) {
            isFactorySet = true;
            BaseUtils.setSimpleRandomGenerator(new JavaScriptSimpleRandomGenerator());
        }
    }
    
    public int nextInt(int upperBound) {
        return Random.nextInt(upperBound);
    }
}
