/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author bfechner
 */
public class AttrLongText extends AttrWigetBase {

    protected BikRichTextArea area = new BikRichTextArea();
    protected String atrLinValue;

    public AttrLongText(String atrLinValue) {
        this.atrLinValue = atrLinValue;
    }

    @Override
    public Widget getWidget() {
        MyRichTextAreaFoxyToolbar toolbar = new MyRichTextAreaFoxyToolbar(area);
        toolbar.setStyleName("richTextAreaToolbar");
        HorizontalPanel hp = new HorizontalPanel();
        hp.setStyleName("richTextAreaP");
        hp.add(toolbar);

        VerticalPanel vp = new VerticalPanel();
        vp.add(hp);
        vp.add(area);
        vp.setStyleName("richTextArea");
        vp.setWidth("100%");
        area.setWidth(Window.getClientWidth() - 307 + "px");
        area.setHeight(Window.getClientHeight() - 307 + "px");
//            area.setSize(Window.getClientWidth() - 130 + "px", Window.getClientHeight() - 190 + "px");
        area.setHTML(atrLinValue);
        return vp;
    }

    @Override
    public String getValue() {
        return area.getHTML();
    }

    @Override
    public int getWidgetHeight() {
        return 160;
    }

}
