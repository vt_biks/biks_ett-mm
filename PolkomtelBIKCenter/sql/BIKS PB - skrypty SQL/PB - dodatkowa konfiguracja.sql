﻿-- nazwy zakładek i ich kolejność
declare @treeOfTrees int = dbo.fn_tree_id_by_code('TreeOfTrees');

update bik_node set name = 'Zasilania - metadane', visual_order = 0 where tree_id = @treeOfTrees and obj_id = 'admin:load'
update bik_node set name = 'Zasilania - testy jakości', visual_order = 1 where tree_id = @treeOfTrees and obj_id = 'admin:dqm'
update bik_node set visual_order = 2 where tree_id = @treeOfTrees and obj_id = 'admin:dict'
update bik_node set visual_order = 3 where tree_id = @treeOfTrees and obj_id = 'admin:stat'

-- tłumaczenia
insert into bik_translation(code, txt, lang, kind)
values ('brakAktywnycInstancjNieMoznaUruc', 'Brak aktywnych systemów. Nie można uruchomić zasilania. Przejdź do konfiguracji zasileń i skonfiguruj system, zanim go uruchomisz', 'pl', 'i18n')

insert into bik_translation(code, txt, lang, kind)
values ('wybierzInstancje', 'Wybierz system', 'pl', 'i18n')

insert into bik_translation(code, txt, lang, kind)
values ('serwer', 'System', 'pl', 'i18n')

insert into bik_translation(code, txt, lang, kind)
values ('wybierzServerDoSkonfigurowania', 'Wybierz system do skonfigurowania', 'pl', 'i18n')

insert into bik_translation(code, txt, lang, kind)
values ('dodajNowySerWer', 'Dodaj nowy system', 'pl', 'i18n')

insert into bik_translation(code, txt, lang, kind)
values ('istniejeTakiSerwer', 'Istnieje system o takiej nazwie! Wybierz inną.', 'pl', 'i18n')

insert into bik_translation(code, txt, lang, kind)
values ('serwerZostalDodany', 'System został dodany', 'pl', 'i18n')

insert into bik_translation(code, txt, lang, kind)
values ('usunZaznaczonySerwer', 'Usuń zaznaczony system', 'pl', 'i18n')

insert into bik_translation(code, txt, lang, kind)
values ('usunietoSerwer', 'Usunięto system', 'pl', 'i18n')

insert into bik_translation(code, txt, lang, kind)
values ('serwery', 'Systemy', 'pl', 'i18n')

insert into bik_translation(code, txt, lang, kind)
values ('nazwaSerwera', 'Nazwa systemu', 'pl', 'i18n')

insert into bik_translation(code, txt, lang, kind)
values ('czyNaPewnoChceszUsunacSerwer', 'Czy na pewno chcesz usunąć wybrany system?', 'pl', 'i18n')

insert into bik_translation(code, txt, lang, kind)
values ('trwaUsuwanieSerwera', 'Trwa usuwanie systemu.', 'pl', 'i18n')


-- wniosek o dostęp

update bik_app_prop set val = 'Wniosek o dostęp do systemu BIKS dla %{userName}%%{ssoUserP}%' where name = 'dumMailTitle';
update bik_app_prop set val = 'Użytkownik <b>%{userName}%%{ssoUserP}%</b> wnioskuje o dostęp do następujących zakładek systemu BIKS:

%{optionsN}%' where name = 'dumMailBody';
update bik_app_prop set val = '<br/><a href="mailto:Admin@Of@BIKS" class="gwt-PushButton gwt-PushButton-up" id="myCustomPushButton"><div class="html-face">Wyślij wniosek</div></a><br/>'
where name = 'dumFooterMsg';

-- uproszczony dialog

merge into bik_app_prop as t
using (values 
  ('useSimpleMsgForUnloggedUser', '1')
) as x (name, val) on t.name = x.name
when not matched by target then
  insert (name, val, is_editable)
  values (x.name, x.val, 0)
when matched then
  update set t.val = x.val;


-- test14
declare @sourceId int = (select id from bik_data_source_def where description = 'Oracle');

if not exists(select * from bik_node where parent_node_id = 116886 and name = 'Poprawna_liczba_aktywowanych_kanalow_SBE - Ryzyko Operacyjne')
begin
    insert into bik_node(parent_node_id, node_kind_id, name, obj_id, tree_id)
    values(116886,(select id from bik_node_kind where code = 'DQMTestInactive'), 'Poprawna_liczba_aktywowanych_kanalow_SBE - Ryzyko Operacyjne', null, (select id from bik_tree where code = 'DQM'))
    
    declare @databaseNodeId int = (select id from bik_node where tree_id = (select id from bik_tree where code = 'Oracle') and is_deleted = 0 and name = 'SBEDSP' and node_kind_id = (select id from bik_node_kind where code = 'OracleOwner'));

    insert into bik_dqm_test(test_node_id, name, description, long_name, benchmark_definition, sampling_method, additional_information, error_threshold, create_date, db_server_id, error_db_server_id, procedure_node_id, error_procedure_node_id, source_id, type, database_node_id, sql_text, error_sql_text, result_file_name, data_scope)
    values(convert(int, scope_identity()), 'Poprawna_liczba_aktywowanych_kanalow_SBE - Ryzyko Operacyjne', 'Test powinien sprawdzać, czy dla każdego rachunku powiązanego z danym użytkownikiem zostały uruchomione wszystkie kanały do aktywowanych produktów, tzn.:
		• Dla klienta detalicznego zostały aktywowane 3 kanały (telefon, internet i sms) dla danego rachunku.
		• Dla klienta instytucjonalnego zostały aktywowane 2 kanały (telefon i internet) dla każdego rachunku.
		Należy sprawdzać rachunki o statusie różnym niż 4. 
		W wyniku powinny zostać podane pola:
		CIF użytkownika [USER_PROFILE.USR_CUS_CIF], 
		numer rachunku [USER_CHANNEL_PRODUCT. UCP_PRODUCT_ID], 
		typ produktu [USER_CHANNEL_PRODUCT. UCP_PRODUCT_CODE], 
		typ klienta[USER_PROFILE_CUSTOMER. UPC_BUSINESS_LINE] wartości „CORPO” lub „DETAL”, 
		BRCD użytkownika [USER_PROFILE. USR_BRCD], 
		kanały [USER_CHANNEL.USC_CHN_CODE]
		i data aktywacji dla każdego kanału  [USER_CHANNEL_PRODUCT. UCP_ACTIVATION_DATE]

		Zakres produktów dla klientów detalicznych: 3000|3005|3006|3031|3032|3501|3502|3503

		Zakres produktów dla klientów instytucjonalnych: 3007|3008|3009|3010|3020|3021|3022|3023|3024|3504|3505|3506
		', '', '', 'Raz na pół roku', 'Test uruchamiany za pomocą harmonogramu', 0.0, getdate(), 9, 9, NULL, NULL, @sourceId, 'DQMTestNotStarted', @databaseNodeId, 'select (select COUNT (*) 
   from 
     (select ucp.ucp_product_id, ucp.ucp_product_code,ucp.ucp_cus_cif,upc.UPC_BUSINESS_LINE,up.usr_brcd as BRCD, 
     count(*) as ilosc_kanalow
   from user_channel_product ucp 
    join user_profile up on up.usr_cus_cif = ucp.ucp_cus_cif
    join user_profile_customer upc on upc.UPC_USR_ID = up.usr_id
   where ucp.ucp_product_status not like ''4''  and 
     ((upc.UPC_BUSINESS_LINE like ''CORPO'' and ucp.ucp_product_id in (''3007'',''3008'',''3009'',
       ''3010'',''3020'',''3021'',''3022'',''3023'',''3024'',''3504'',''3505'',''3506''))
     or (upc.UPC_BUSINESS_LINE like ''DETAL'' and ucp.ucp_product_id in (''3000'',''3005'',''3006'',
       ''3031'',''3032'',''3501'',''3502'',''3503'')))
   group by ucp.ucp_product_id,ucp.ucp_product_code, ucp.ucp_cus_cif, upc.UPC_BUSINESS_LINE, up.usr_brcd
   order by ucp.ucp_product_id) x 
        join user_channel_product ucp2 on (x.ucp_product_id = ucp2.ucp_product_id and x.ucp_cus_cif = ucp2.ucp_cus_cif)
        join user_channel usc on ucp2.ucp_usc_id = usc.usc_id
        left join event_log el on trunc(el.eve_date)=trunc(ucp2.ucp_resignation_date) 
   and el.eve_emp_login is not null and ucp2.ucp_cus_cif = ''CIF_UK'') as case_count,
        (select COUNT(*)
    from 
    (select ucp.ucp_product_id, ucp.ucp_product_code,ucp.ucp_cus_cif,upc.UPC_BUSINESS_LINE,
      up.usr_brcd as BRCD, count(*) as ilosc_kanalow
    from user_channel_product ucp 
     join user_profile up on up.usr_cus_cif = ucp.ucp_cus_cif
     join user_profile_customer upc on upc.UPC_USR_ID = up.usr_id
    where ucp.ucp_product_status not like ''4''  and 
     ((upc.UPC_BUSINESS_LINE like ''CORPO'' and ucp.ucp_product_id in (''3007'',''3008'',''3009'',''3010'',
      ''3020'',''3021'',''3022'',''3023'',''3024'',''3504'',''3505'',''3506'') )
     or (upc.UPC_BUSINESS_LINE like ''DETAL'' and ucp.ucp_product_id in (''3000'',''3005'',''3006'',''3031'',
      ''3032'',''3501'',''3502'',''3503'')))
    group by ucp.ucp_product_id,ucp.ucp_product_code, ucp.ucp_cus_cif, upc.UPC_BUSINESS_LINE, up.usr_brcd
    order by ucp.ucp_product_id) x 
   join user_channel_product ucp2 on (x.ucp_product_id = ucp2.ucp_product_id and x.ucp_cus_cif = ucp2.ucp_cus_cif)
   join user_channel usc on ucp2.ucp_usc_id = usc.usc_id
   left join event_log el on trunc(el.eve_date)=trunc(ucp2.ucp_resignation_date) 
      and el.eve_emp_login is not null and ucp2.ucp_cus_cif = ''CIF_UK''
   where x.ilosc_kanalow <> (case when x.UPC_BUSINESS_LINE like ''CORPO'' then 2 else 3 end))as error_count
 from dual', 'select ucp2.ucp_cus_cif as CIF_uzytkownika, ucp2.ucp_product_id as numer_rachunku, ucp2.ucp_product_code as typ_produktu,
        x.UPC_BUSINESS_LINE as typ_klienta, x.BRCD as BRCD_uzytkownika, usc.USC_CHN_CODE as kanal, 
        ucp2.UCP_ACTIVATION_DATE as data_aktywacji_kanalu, ucp2.UCP_RESIGNATION_DATE as data_rezygnacji_z_kanalu,
        el.EVE_EMP_LOGIN as login_pracownika
  from 
    (select ucp.ucp_product_id, ucp.ucp_product_code,ucp.ucp_cus_cif,upc.UPC_BUSINESS_LINE,up.usr_brcd as BRCD, count(*) as ilosc_kanalow
        from user_channel_product ucp 
          join user_profile up on up.usr_cus_cif = ucp.ucp_cus_cif
          join user_profile_customer upc on upc.UPC_USR_ID = up.usr_id
        where ucp.ucp_product_status not like ''4''  and 
          ((upc.UPC_BUSINESS_LINE like ''CORPO'' and ucp.ucp_product_id in (''3007'',''3008'',''3009'',''3010'',''3020'',''3021'',''3022'',''3023'',''3024'',''3504'',''3505'',''3506'') )
            or (upc.UPC_BUSINESS_LINE like ''DETAL'' and ucp.ucp_product_id in (''3000'',''3005'',''3006'',''3031'',''3032'',''3501'',''3502'',''3503'')))
        group by ucp.ucp_product_id,ucp.ucp_product_code, ucp.ucp_cus_cif, upc.UPC_BUSINESS_LINE, up.usr_brcd
        order by ucp.ucp_product_id) x 
        join user_channel_product ucp2 on (x.ucp_product_id = ucp2.ucp_product_id and x.ucp_cus_cif = ucp2.ucp_cus_cif)
        join user_channel usc on ucp2.ucp_usc_id = usc.usc_id
        left join event_log el on trunc(el.eve_date)=trunc(ucp2.ucp_resignation_date) and el.eve_emp_login is not null and ucp2.ucp_cus_cif = ''CIF_UK''
      where x.ilosc_kanalow <> (case when x.UPC_BUSINESS_LINE like ''CORPO'' then 2 else 3 end)', null, 'Tabela 
USER_PROFILE  up, USER_CHANNEL_PRODUCT  ucp, 
USER_PROFILE_CUSTOMER upc, USER_CHANNEL uc, EVENT_LOG el
Kolumna 
ucp.ucp_product_id, ucp.ucp_product_code, ucp.ucp_cus_cif, 
upc.UPC_BUSINESS_LINE, up.usr_brcd, user_channel_product, up.usr_cus_cif, 
upc.UPC_USR_ID, up.usr_id, ucp.ucp_product_status, 
el.eve_date, ucp.ucp_resignation_date, el.eve_emp_login, 
ucp2.UCP_ACTIVATION_DATE, usc.USC_CHN_CODE, el.EVE_EMP_LOGIN')
      
    insert into bik_dqm_schedule(test_id, hour, minute, interval, is_schedule, is_deleted, priority)
    values (convert(int, scope_identity()), 0, 0, 1, 0, 0, 10)

	declare @dqmtree int = (select id from bik_tree where code = 'DQM')
	exec sp_node_init_branch_id  @dqmtree, null

end;
go

-- wbudowana dqm dokumentacja
update bik_tree set is_built_in = 1 where code = 'DQMDokumentacja'
update bik_node set is_deleted = 1 where id in (116735, 116736)

if not exists(select 1 from bik_node_kind where code = 'DQMFolderRate')
begin
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('DQMFolderRate', 'Folder ocen', 'postgresFolder', 'QualityMetadata', 0, 5, 0)
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('DQMFolderCoordination', 'Folder koordynatora', 'dqmfoldercoordination', 'QualityMetadata', 0, 5, 0)
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('DQMReport', 'Raport', 'oracleBIAnalysisAndReports', 'QualityMetadata', 0, 15, 0)
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('DQMRate', 'Nowa ocena', 'dqmrate', 'QualityMetadata', 0, 10, 0)
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('DQMRateAccepted', 'Ocena zaakceptowana', 'dqmrateaccepted', 'QualityMetadata', 0, 10, 0)
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('DQMRateRejected', 'Ocena odrzucona', 'dqmraterejected', 'QualityMetadata', 0, 10, 0)
	
	insert into bik_icon(name)
	values('dqmrate'), ('dqmrateaccepted'), ('dqmraterejected'), ('dqmfoldercoordination')
end
go

declare @dqmDocumentsTreeId int = (select id from bik_tree where code = 'DQMDokumentacja')

insert into bik_node(parent_node_id, tree_id, node_kind_id, name, is_built_in, visual_order)
select id, @dqmDocumentsTreeId, (select id from bik_node_kind where code = 'DQMFolderRate'), 'Oceny jakości danych', 1, 0
from bik_node 
where tree_id = @dqmDocumentsTreeId and is_deleted = 0 and parent_node_id is null

insert into bik_node(parent_node_id, tree_id, node_kind_id, name, is_built_in, visual_order)
select id, @dqmDocumentsTreeId, (select id from bik_node_kind where code = 'DocumentsFolder'), 'Ankiety użytkowników', 0, 1
from bik_node 
where tree_id = @dqmDocumentsTreeId and is_deleted = 0 and parent_node_id is null

insert into bik_node(parent_node_id, tree_id, node_kind_id, name, is_built_in, visual_order)
select id, @dqmDocumentsTreeId, (select id from bik_node_kind where code = 'DocumentsFolder'), 'Rejestr błędów', 0, 1
from bik_node 
where tree_id = @dqmDocumentsTreeId and is_deleted = 0 and parent_node_id is null

insert into bik_node(parent_node_id, tree_id, node_kind_id, name, is_built_in, visual_order, obj_id)
values (null, @dqmDocumentsTreeId, (select id from bik_node_kind where code = 'DQMFolderCoordination'), 'Koordynacja jakością danych', 1, 10000000, '#dqmCoordinationFolder')

insert into bik_node(parent_node_id, tree_id, node_kind_id, name, is_built_in, visual_order, obj_id)
values (convert(int, scope_identity()), @dqmDocumentsTreeId, (select id from bik_node_kind where code = 'DQMFolderCoordination'), 'Niezaakceptowane oceny jakości danych', 1, -1, '#dqmNotAcceptedFolder')

exec sp_node_init_branch_id @dqmDocumentsTreeId, null;
