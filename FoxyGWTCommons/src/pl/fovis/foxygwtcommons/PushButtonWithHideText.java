/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;

/**
 *
 * @author tflorczak
 */
public class PushButtonWithHideText extends PushButton {

    protected String hideText;
    protected String hideTitle;

    public PushButtonWithHideText(Image img) {
        super(img);
    }

    public String getHideText() {
        return hideText;
    }

    public void setHideText(String hideText) {
        this.hideText = hideText;
    }
    
    public String getHideTitle() {
        return hideTitle;
    }

    public void setHideTitle(String hideTitle) {
        this.hideTitle = hideTitle;
    }
}
