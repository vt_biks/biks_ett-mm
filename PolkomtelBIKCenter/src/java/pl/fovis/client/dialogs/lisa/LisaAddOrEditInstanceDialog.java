/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IContinuation;

/**
 *
 * @author ctran
 */
public class LisaAddOrEditInstanceDialog extends BaseActionOrCancelDialog {

    private final static String RADIO_GROUP = "LCPRadioGroup" /* i18n: no */;
    protected LisaInstanceInfoBean bean = null;
    protected boolean isEditing = true;
    protected TextBox nameTb, urlTb, portTb, dbNameTb, charsetTb, schemaTb, ducnTb;
    protected RadioButton ansiButton, teradataButton;
    protected IContinuation actionAfterSuccess;

    @Override
    protected String getDialogCaption() {
        return (isEditing ? I18n.edycjaInstancji.get() : I18n.nowaInstancja.get());
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get();
    }

    @Override
    protected void buildMainWidgets() {
        ansiButton = new RadioButton(RADIO_GROUP, "ANSI" /* i18n: no */);
        teradataButton = new RadioButton(RADIO_GROUP, "TERA" /* i18n: no */);
        VerticalPanel transactionTypePanel = new VerticalPanel();
        transactionTypePanel.add(ansiButton);
        transactionTypePanel.add(teradataButton);

        int row = 0;
        Grid grid = new Grid(8, 2);
        grid.setWidget(row, 0, new Label(I18n.nazwaInstancji.get() /* i18n:  */));
        grid.setWidget(row++, 1, nameTb = new TextBox());
        grid.setWidget(row, 0, new Label(I18n.nazwaSerwera.get()));
        grid.setWidget(row++, 1, urlTb = new TextBox());
        grid.setWidget(row, 0, new Label("Port" /* i18n: no */));
        grid.setWidget(row++, 1, portTb = new TextBox());
        grid.setWidget(row, 0, new Label(I18n.nazwaBazyDanych.get()));
        grid.setWidget(row++, 1, dbNameTb = new TextBox());
        grid.setWidget(row, 0, new Label(I18n.schemat.get() /* i18n:  */));
        grid.setWidget(row++, 1, schemaTb = new TextBox());
        grid.setWidget(row, 0, new Label("Charset" /* i18n: no */));
        grid.setWidget(row++, 1, charsetTb = new TextBox());
        grid.setWidget(row, 0, new Label(I18n.domyslnaNazwaKategDlaNieprzypisanych.get()));
        grid.setWidget(row++, 1, ducnTb = new TextBox());
        grid.setWidget(row, 0, new Label(I18n.typTransakcji.get() /* i18n:  */));
        grid.setWidget(row++, 1, transactionTypePanel);

        if (isEditing) {
            populateWidgetWithData();
        }
        main.add(grid);
    }

    private void populateWidgetWithData() {
        nameTb.setText(bean.name);
        urlTb.setText(bean.url);
        portTb.setText(bean.port);
        schemaTb.setText(bean.schemaName);
        dbNameTb.setText(bean.dbName);
        charsetTb.setText(bean.charset);
        ducnTb.setText(bean.defaultUnassignedCategoryName);
        if (bean.tmode.equals("ANSI" /* i18n: no */)) {
            ansiButton.setValue(true);
        } else {
            teradataButton.setValue(true);
        }
    }

    @Override
    protected void doAction() {
        bean.name = nameTb.getText();
        bean.url = urlTb.getText();
        bean.port = portTb.getText();
        bean.schemaName = schemaTb.getText();
        bean.dbName = dbNameTb.getText();
        bean.charset = charsetTb.getText();
        bean.defaultUnassignedCategoryName = ducnTb.getText();
        if (ansiButton.getValue()) {
            bean.tmode = "ANSI";
        } else {
            bean.tmode = "TERA";
        }
        AsyncCallback callback = new StandardAsyncCallback<Void>() {

            @Override
            public void onSuccess(Void result) {
                actionAfterSuccess.doIt();
            }
        };
        if (isEditing) {
            BIKClientSingletons.getLisaService().updateLisaInstanceInfo(bean, callback);
        } else {
            BIKClientSingletons.getLisaService().addLisaInstanceInfo(bean, callback);
        }
    }

    public void buildAndShowDialog(LisaInstanceInfoBean bean, boolean isEditing, IContinuation actionAfterSuccess) {
        this.bean = bean;
        this.isEditing = isEditing;
        this.actionAfterSuccess = actionAfterSuccess;
        super.buildAndShowDialog();
    }
}
