/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.List;
import pl.fovis.foxygwtcommons.i18n.I18n;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author tflorczak
 */
public class SimpleInfoDialog extends BaseActionOrCancelDialog {
    // a la window alert ale ładniejszy i bardziej sexy

    protected String text;
    protected String dialogCaption;
    protected String cancelButtonCaption;
    protected IContinuation optCloseCont;
    protected boolean usePlainText;

    @Override
    protected String getDialogCaption() {
        return dialogCaption;
    }

    @Override
    protected String getActionButtonCaption() {
        return null;
    }

    @Override
    protected String getCancelButtonCaption() {
        return cancelButtonCaption;
    }

    @Override
    protected void buildMainWidgets() {
        VerticalPanel textPanel = new VerticalPanel();

        if (usePlainText) {
            List<String> txts = BaseUtils.splitBySep(text, "\n");

            for (String txt : txts) {
                textPanel.add(new Label(txt));
            }
        } else {
            textPanel.add(new HTML(text));
        }

        textPanel.setStyleName("HomePage-bizDef");
        main.add(textPanel);
    }

    @Override
    protected void doAction() {
        // no-op
    }

    @Override
    protected void doCancel() {
        if (optCloseCont != null) {
            optCloseCont.doIt();
        }
    }

    protected String getDefCloseBtnCaption() {
        return "OK" /* I18N: no */;
    }

    protected String getDefDialogCaption() {
        return I18n.informacja.get() /* I18N:  */;
    }

    public void buildAndShowDialog(String text, String optDialogCaption, String optCloseButtonCaption) {
        buildAndShowDialog(text, optDialogCaption, optCloseButtonCaption, null);
    }

    public void buildAndShowDialog(String text, boolean usePlainText, String optDialogCaption, String optCloseButtonCaption,
            IContinuation optCloseCont) {
        this.text = text;
        this.dialogCaption = BaseUtils.nullToDef(optDialogCaption, getDefDialogCaption());
        this.cancelButtonCaption = BaseUtils.nullToDef(optCloseButtonCaption, getDefCloseBtnCaption());
        this.optCloseCont = optCloseCont;
        this.usePlainText = usePlainText;

        super.buildAndShowDialog();
    }

    public void buildAndShowDialog(String text, String optDialogCaption, String optCloseButtonCaption,
            IContinuation optCloseCont) {
        buildAndShowDialog(text, false, optDialogCaption, optCloseButtonCaption, optCloseCont);
    }
}
