﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- poprawki tekstów DEMO
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_check_version '1.5.z2.1';
go


------------------------------------------------------
------------------------------------------------------
------------------------------------------------------


update bik_translation set txt =
'<h3>Welcome to BI Knowledge System - demo version</h3>
BI Knowledge System is a solution that allows practical management of knowledge in different areas of an organization. The Demo which you have received access to simulates knowledge management in the field of Business Intelligence. Some of application’s functionalities have been limited in this demonstration version. All information contained in this demo is examplary. Descriptions of objects may not be true and the similarity of names of users is coincidental. BSSG company can not be held responsible for misuse of the demo.'
where code = 'myBIKSDemoMsg' and 
  lang	= 'en' and kind = 'aprop'
go

update bik_app_prop
set val = '<h3>Witaj w systemie BI Knowledge System – wersja demonstracyjna</h3>
BI Knowledge System jest rozwiązaniem, które pozwala na praktyczne zarządzanie wiedzą, w różnych obszarach działalności organizacji. Demo, do którego otrzymali Państwo dostęp, pozwala zasymulować zarządzanie wiedzą w obszarze Business Intelligence. W wersji demonstracyjnej pewne funkcjonalności aplikacji zostały ograniczone. Wszystkie informacje, zawarte w demo są przykładowe. Treści opisów obiektów mogą nie być zgodne z prawdą, a zbieżność nazwisk użytkowników jest przypadkowa. Firma BSSG nie ponosi odpowiedzialności za niewłaściwe użycie wersji demonstracyjnej.'
where name = 'myBIKSDemoMsg'
go


------------------------------------------------------
------------------------------------------------------
------------------------------------------------------


exec sp_update_version '1.5.z2.1', '1.5.z2.2';
go
