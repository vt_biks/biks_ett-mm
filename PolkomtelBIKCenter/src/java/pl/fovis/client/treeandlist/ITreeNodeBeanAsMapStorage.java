/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import java.util.Map;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;

/**
 *
 * @author wezyr
 */
public interface ITreeNodeBeanAsMapStorage<IDT> extends ITreeRowAndBeanStorage<IDT, Map<String, Object>, TreeNodeBean> {
    //public int getTreeId();
//    public TreeNodeBean getBeanById(IDT nodeId);
//
//    public Map<String, Object> registerAndConvertBeanToRow(TreeNodeBean node);
//
//    public Collection<TreeNodeBean> getAllBeans();
}
