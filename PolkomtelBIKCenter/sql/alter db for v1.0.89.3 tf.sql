﻿exec sp_check_version '1.0.89.3';
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_metadata_connections]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
GO


create procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp

	declare @reportKind int;
    select @reportKind = id from bik_tree where name='Raporty'
    declare @teradataKind int;
    select @teradataKind=id from bik_tree where name='Teradata'
    declare @schemaKind int;
    select @schemaKind=id from bik_node_kind where code='TeradataSchema'
    declare @tabKind int;
    select @tabKind=id from bik_node_kind where code='TeradataTable'
    declare @viewKind int;
    select @viewKind=id from bik_node_kind where code='TeradataView'
    declare @connection_kind int;
    select @connection_kind=id from bik_node_kind where code='DataConnection'
    declare @webi_kind int;
    select @webi_kind=id from bik_node_kind where code='Webi'
    declare @fullclient int;
    select @fullclient=id from bik_node_kind where code='FullClient'
    declare @query_kind int;
    select @query_kind=id from bik_node_kind where code='ReportQuery'

    ----------   universe  -->  zapytania   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
    select bn.id as src_node_id, bnu.id as dst_node_id, 1 as type, 1 as inherit_to_descendants
    from bik_sapbo_query sbq
    join bik_node bn on sbq.node_id=bn.id
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
	where bn.is_deleted=0 

    ----------   connection  -->  zapytania   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id as src_node_id, bnc.id as dst_node_id, 1 as type
    from bik_sapbo_query sbq
    join bik_node bn on sbq.node_id=bn.id
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
    join bik_node bnc on bnc.obj_id=convert(varchar(30),uni.SI_DATACONNECTION__1) and
    bnc.is_deleted=0 and bnc.node_kind_id=(select id from bik_node_kind where code='DataConnection')
	where bn.is_deleted=0 

    ----------   universe  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnu.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')

    ----------   raport  -->  universe   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnu.id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')

    ----------   raport  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct sbq.report_node_id as src_node_id, bnc.id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
    join bik_node bnc on bnc.obj_id=convert(varchar(30),uni.SI_DATACONNECTION__1) and
    bnc.is_deleted=0 and bnc.node_kind_id=(select id from bik_node_kind where code='DataConnection')

	----------   connection  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnc.id as src_node_id, sbq.report_node_id as dst_node_id, 1 as type
    from aaa_query sbq
    join APP_UNIVERSE uni on sbq.universe_cuid=uni.SI_CUID
    join bik_node bnu on bnu.obj_id=convert(varchar(30),uni.si_id) and
    bnu.is_deleted=0 and bnu.node_kind_id=(select id from bik_node_kind where code='Universe')
    join bik_node bnc on bnc.obj_id=convert(varchar(30),uni.SI_DATACONNECTION__1) and
    bnc.is_deleted=0 and bnc.node_kind_id=(select id from bik_node_kind where code='DataConnection')

    ----------   teradata Table/View  -->  obiekt   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)

    ----------   teradata Schema  -->  obiekt   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind

    ----------   universum  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type='Teradata' and b.is_derived=0

    ----------   teradata Table/View  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type='Teradata' and b.is_derived=0

    ----------   universum  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    where b.type='Teradata' and b.is_derived=0

    ----------   teradata Schema  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 from bik_sapbo_universe_table b
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    where b.type='Teradata' and b.is_derived=0

    ----------   connection  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null

    ----------   teradata Schema  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null

    ----------   teradata Table/View  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null

    ----------   connection  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 from bik_sapbo_universe_table b
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    join bik_node bnt on b.node_id=bnt.id
    join bik_node bntf on bnt.parent_node_id=bntf.id
    join bik_joined_objs jo on jo.src_node_id=bntf.parent_node_id
    join bik_node bn2 on jo.dst_node_id=bn2.id
    and bn2.is_deleted=0
    and bn2.node_kind_id=@connection_kind
    where b.type='Teradata' and b.is_derived=0 and bn2.id is not null 

    ----------   teradata Table/View  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   raport  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   teradata Schema  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id = @schemaKind
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   raport  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id = @schemaKind
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   zapytanie  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.schema_name
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id=@schemaKind
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

    ----------   zapytanie  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 from bik_sapbo_object_table a
    join bik_sapbo_universe_table b on a.table_node_id=b.node_id
    and b.type='Teradata'
    and b.is_derived=0
    join bik_node bn on bn.obj_id=b.name_for_teradata
    and bn.is_deleted=0
    and bn.linked_node_id is null
    and bn.tree_id=@teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    join bik_node bn3 on bn3.linked_node_id=a.object_node_id
    and bn3.is_deleted=0
    join bik_node bn4 on bn3.parent_node_id=bn4.id
    and bn4.node_kind_id=@query_kind
    and bn4.is_deleted=0

	----------   obiekt  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct ids.object_node_id, bn.parent_node_id, 1 from aaa_ids_for_report ids join bik_node bn on ids.query_node_id=bn.id
	
	----------   UniverseTable  -->  teradata Table/View   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 from bik_sapbo_universe_table bsut 
	join bik_node bn on bsut.name_for_teradata=bn.obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null

	----------   teradata Table/View  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bsut.node_id, 1 from bik_sapbo_universe_table bsut 
	join bik_node bn on bsut.name_for_teradata=bn.obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null

	----------   UniverseTable  -->  teradata Schema   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 from bik_sapbo_universe_table bsut 
	join bik_node bn on bsut.schema_name=bn.obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null
	
	----------   UniverseAliasTable  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bsut1.node_id, 1 from bik_sapbo_universe_table bsut 
	join bik_sapbo_universe_table bsut1 on bsut.original_table = bsut1.their_id
	join bik_node bn on bn.id=bsut.node_id
	join bik_node bn2 on bn2.id=bsut1.node_id
	where bsut.is_alias=1 and bn2.parent_node_id=bn.parent_node_id

    exec sp_move_bik_joined_objs_tmp
	
end
go



-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

exec sp_update_version '1.0.89.3', '1.0.89.4';
go