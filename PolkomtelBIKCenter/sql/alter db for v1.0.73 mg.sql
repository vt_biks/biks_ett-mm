﻿exec sp_check_version '1.0.73';
go

insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values('Metadata', 'Metadane', null, 'Metadata', 0)
go

update bik_tree
set node_kind_id = (select id from bik_node_kind where code = 'Metadata')
where tree_kind = 'Metadata'
go

exec sp_update_version '1.0.73', '1.0.74';
go
