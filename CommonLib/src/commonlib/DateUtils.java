package commonlib;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import simplelib.LameRuntimeException;

/**
 *
 * @author msmoktunowicz
 */
public class DateUtils {

    public static SimpleDateFormat getFlexiZDateTimeFormat() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    }

    public static SimpleDateFormat getFlexiNDateTimeFormat() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm");
    }

    public static SimpleDateFormat getFlexiSDateTimeFormat() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    public static SimpleDateFormat getISO8601DateTimeFormat() {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    }

    public static SimpleDateFormat getISO8601DateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd");
    }

    public static String getDateAsISO8601DateTimeString(Date date) {
        String result = getISO8601DateTimeFormat().format(date);
        return result;
    }

    public static String getDateAsISO8601DateString(Date date) {
        String result = getISO8601DateFormat().format(date);
        return result;
    }

    public static String getDateAsFlexiSDateTimeString(Date date) {
        String result = getFlexiSDateTimeFormat().format(date);
        return result;
    }

    public static boolean isTimePartEmpty(Date date) {
        Long truncTime = truncDate(date).getTime();
        return truncTime == date.getTime();
    }

    public static String getDateAsFlexiDateTimeString(Date date, String datePrec) {
        if (date == null) {
            return null;
        }

        if (datePrec == null) {
            datePrec = isTimePartEmpty(date) ? "D" : "S";
        }

        String dateFormat = null;
        if ("Y".equals(datePrec)) {
            dateFormat = "yyyy";
        } else if ("M".equals(datePrec)) {
            dateFormat = "yyyy-MM";
        } else if ("D".equals(datePrec)) {
            dateFormat = "yyyy-MM-dd";
        } else if ("N".equals(datePrec)) {
            dateFormat = "yyyy-MM-dd HH:mm";
        } else if ("S".equals(datePrec)) {
            dateFormat = "yyyy-MM-dd HH:mm:ss";
        } else if ("Z".equals(datePrec)) {
            dateFormat = "yyyy-MM-dd HH:mm:ss.SSS";
        } else if ("IZ".equals(datePrec)) {
            dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS";
        } else {
            throw new LameRuntimeException("getDateAsFlexiDateTimeString: unrecognized datePrec: \""
                    + datePrec + "\" for date:" + date);
        }

        return new SimpleDateFormat(dateFormat).format(date);
    }

    public static Date parse(DateFormat dateFormat, String str) {
        Date result = null;
        try {
            dateFormat.setLenient(false);
            ParsePosition pp = new ParsePosition(0);
            result = dateFormat.parse(str, pp);
            if (pp.getIndex() != str.length()) {
                result = null;
            }
        } catch (Exception ex) {
            // do nothing
        }
        return result;
    }

    public static Date getDateFromFlexiFormattedString(String str) throws LameRuntimeException {
        // D
        Date result = parse(getISO8601DateFormat(), str);
        // IZ
        if (result == null) {
            result = parse(getISO8601DateTimeFormat(), str);
        }
        // Z
        if (result == null) {
            result = parse(getFlexiZDateTimeFormat(), str);
        }
        // N
        if (result == null) {
            result = parse(getFlexiNDateTimeFormat(), str);
        }
        // S
        if (result == null) {
            result = parse(getFlexiSDateTimeFormat(), str);
        }
        if (result == null) {
            throw new LameRuntimeException("Could not parse date: " + str);
        }
        return result;
    }

// obsolete! deprecated!
    public static Date getDateFromISO8601String(String str) throws LameRuntimeException {
        Date result = parse(getISO8601DateFormat(), str);
        if (result == null) {
            result = parse(getISO8601DateTimeFormat(), str);
        }
        if (result == null) {
            throw new LameRuntimeException("Could not parse date: " + str);
        }
        return result;
    }

    public static Date truncDate(Date date) {
        SimpleDateFormat timeFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
        String str = timeFormat.format(date);
        try {
            return timeFormat.parse(str);
        } catch (ParseException ex) {
            throw new LameRuntimeException(ex);
        }
    }

    public static Date addInterval(Date d, long interval) {
        Date res = new Date();
        res.setTime(d.getTime() + interval);
        return res;
    }

    public static int getPartOfDate(Date d, int field) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        return cal.get(field);
    }

    public static int getYearOfDate(Date d) {
        return getPartOfDate(d, Calendar.YEAR);
    }

    public static int getMonthOfDate(Date d) {
        return getPartOfDate(d, Calendar.MONTH) + 1; // trzeba dodawa� 1!!! Aggghhhh
    }

    public static int getDayOfDate(Date d) {
        return getPartOfDate(d, Calendar.DAY_OF_MONTH);
    }

    public static String dateTimeFormat(Date date, String format) {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }
}
