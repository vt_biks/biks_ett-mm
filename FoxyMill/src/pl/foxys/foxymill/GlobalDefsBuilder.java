/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill;

import commonlib.StringFromInputStreamResourceProvider;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.foxys.foxymill.renderctx.IRenderCtx;
import pl.foxys.foxymill.renderctx.RenderCtx;
import pl.foxys.foxymill.tags.ITag;
import simplelib.IGenericResourceProvider;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class GlobalDefsBuilder {

    public static List<ITag> readAndParseFile(IGenericResourceProvider<InputStream> resProvider,
            RenderEnvironment renderEnv, String filePath) {
        return readAndParseFileSimple(new StringFromInputStreamResourceProvider(resProvider, "UTF-8"),
                renderEnv, filePath);
//        try {
//            String pageContent = LameUtils.gainStringFromProvider(resProvider, filePath, "UTF-8");
//
//            ParserTask pt = new ParserTask(filePath, pageContent);
//            final List<ITag> tags = pt.read();
//            FoxyMillUtils.prepareTags(tags, renderEnv);
//
//            return tags;//FoxyMillUtils.wrapAsRenderable(tags, null);
//        } catch (Exception ex) {
//            throw new LameRuntimeException("error reading foxymill file '" + filePath + "'", ex);
//        }
    }

    public static List<ITag> readAndParseFileSimple(IGenericResourceProvider<String> resProvider,
            RenderEnvironment renderEnv, String filePath) {
        String pageContent = null;
        try {
            pageContent = resProvider.gainResource(filePath);

            ParserTask pt = new ParserTask(filePath, pageContent);
            final List<ITag> tags = pt.read();
            FoxyMillUtils.prepareTags(tags, renderEnv);

            return tags;//FoxyMillUtils.wrapAsRenderable(tags, null);
        } catch (Exception ex) {
            throw new LameRuntimeException("error reading foxymill file '" + filePath + "'"
                    /*+ "'\n"
                    + "full file content:\n" + pageContent*/,
                    ex);
        }
    }

    public static Map<String, Object> buildGlobals(IGenericResourceProvider<InputStream> resProvider,
            RenderEnvironment renderEnv, Iterable<String> fileNames, Object model) {
        return buildGlobalsSimple(new StringFromInputStreamResourceProvider(resProvider, "UTF-8"),
                renderEnv, fileNames, model);
//        IRenderCtx renderCtx = RenderCtx.makeRootRenderCtx(model);
//
//        List<ITag> allTags = new ArrayList<ITag>();
//
//        for (String fileName : fileNames) {
//            //IRenderable renderable
//            List<ITag> tags = readAndParseFile(resProvider, renderEnv, fileName);
//            //renderable.render(renderCtx);
//            FoxyMillUtils.render(tags, renderCtx);
//            allTags.addAll(tags);
//        }
//
//        Map<String, Object> globals = renderCtx.getVars();
//
//        FoxyMillUtils.fixupTags(allTags, globals);
//        return globals;
    }

    public static Map<String, Object> buildGlobalsSimple(IGenericResourceProvider<String> resProvider,
            RenderEnvironment renderEnv, Iterable<String> fileNames, Object model) {
        IRenderCtx renderCtx = RenderCtx.makeRootRenderCtx(model);

        List<ITag> allTags = new ArrayList<ITag>();

        for (String fileName : fileNames) {
            //IRenderable renderable
            List<ITag> tags = readAndParseFileSimple(resProvider, renderEnv, fileName);
            //renderable.render(renderCtx);
            FoxyMillUtils.render(tags, renderCtx);
            allTags.addAll(tags);
        }

        Map<String, Object> globals = renderCtx.getVars();

        FoxyMillUtils.fixupTags(allTags, globals);
        return globals;
    }
}
