/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill;

/**
 *
 * @author wezyr
 */
public class RerenderWithNewModelException extends FoxyMillRenderingFallThroughException {

    public Object proceedModel;

    public RerenderWithNewModelException(Object proceedModel) {
        this.proceedModel = proceedModel;
    }
}
