/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.ChildrenWithAttributesWidget;
import pl.fovis.client.entitydetailswidgets.EntityDetailsWidgetFlatBase;
import pl.fovis.common.MyObjectsBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class MyBIKSMyObjects extends MyBIKSTabBase {

    @Override
    public String getCaption() {
        return I18n.mojeObiekty.get();
    }

    @Override
    public String getButtonStyle() {
        return "labelLink-myobjects";
    }

    @Override
    public void show(final VerticalPanel vp) {
        vp.clear();
        callServiceToGetData(new StandardAsyncCallback<List<MyObjectsBean>>("Error in getMyObjects()") {

            @Override
            public void onSuccess(List<MyObjectsBean> result) {
                for (MyObjectsBean child : result) {
                    if (BaseUtils.isCollectionEmpty(child.objects)) {
                        continue;
                    }
                    vp.add(new HTML("<b>" + child.role + "</b>"));
                    vp.add(new ChildrenWithAttributesWidget(null, child.objects, BIKClientSingletons.getMyObjectsAttributeNames()).buildWidget());
                    vp.add(EntityDetailsWidgetFlatBase.createPostSpacing());
                }
                if (vp.getWidgetCount() == 0) {
                    Label emptyLabel = new Label("Brak obiektów");
                    emptyLabel.setStyleName("MyBIK");
                    vp.add(emptyLabel);
                }
            }
        });
    }

    @Override
    public boolean isVisible() {
        return BIKClientSingletons.isMyObjectsTabVisible();
    }

    protected void callServiceToGetData(AsyncCallback<List<MyObjectsBean>> callback) {
        BIKClientSingletons.getService().getMyObjects(callback);
    }
}
