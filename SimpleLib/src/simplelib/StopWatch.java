/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import simplelib.logging.ILameLogger;
import simplelib.logging.ILameLogger.LameLogLevel;

/**
 *
 * @author wezyr
 */
public class StopWatch {

    public static interface IDurationMeter {

        public long currentTimeMillis();
    }
//
//    private static IDurationMeter durationMeter;
//
//    public static void setDurationMeter(IDurationMeter durationMeter) {
//        StopWatch.durationMeter = durationMeter;
//    }

    //private IDurationMeter durationMeter;
    private StopWatch activeChild = null;
    private long startMillis;
    private String startedLabel = null;
    private List<Pair<String, Long>> executedLabels;
    private boolean isDisabled = false;
    private boolean activeMode;
    private ILameLogger stopWatchLogger;
    private String loggerLabel;
    private LameLogLevel logAtLevel;

    public StopWatch(/*IDurationMeter durationMeter*/) {
        this(/*durationMeter,*/false);
    }

    public StopWatch(ILameLogger stopWatchLogger, String loggerLabel, LameLogLevel logAtLevel) {
        this(stopWatchLogger.isEnabledAtLevel(logAtLevel));
        this.stopWatchLogger = stopWatchLogger;
        this.loggerLabel = loggerLabel;
        this.logAtLevel = logAtLevel;
    }

    public StopWatch(ILameLogger stopWatchLogger, String loggerLabel) {
        this(stopWatchLogger, loggerLabel, LameLogLevel.Debug);
    }

    public StopWatch(/*IDurationMeter durationMeter,*/boolean activeMode) {
        this.startMillis = getCurrentTimeMillis(); //durationMeter.currentTimeMillis();
        //this.durationMeter = durationMeter;
        this.activeMode = activeMode;
        if (activeMode) {
            this.executedLabels = new ArrayList<Pair<String, Long>>();
        }
    }

    public StopWatch(StopWatch parent) {
        this(/*parent.durationMeter,*/parent.activeMode);
        parent.addChild(this);
    }

    protected long getCurrentTimeMillis() {
        return new Date().getTime();
    }

    public void reset() {
        if (!activeMode) {
            return;
        }
        stop();
        executedLabels.clear();
    }

    public void start(String label) {
        if (!activeMode) {
            return;
        }
        stop();
        startedLabel = label;
        startMillis = getCurrentTimeMillis(); //durationMeter.currentTimeMillis();
    }

    public void stop() {
        if (!activeMode) {
            return;
        }
        if (isDisabled) {
            throw new LameRuntimeException("this stopwatch is disabled!");
        }

        long endMillis = getCurrentTimeMillis(); //durationMeter.currentTimeMillis();
        if (startedLabel != null) {
            executedLabels.add(new Pair<String, Long>(startedLabel, endMillis - startMillis));
        }

        stopActiveChildIfNeeded();
        startedLabel = null;
    }

    public void stopAndLog() {
        if (activeMode) {
            stop();
            stopWatchLogger.logAtLevel(logAtLevel,
                    loggerLabel + " execution times:\n" +
                    getExecutedLabelsPretty(2));
        }
    }

    protected void stopActiveChildIfNeeded() {
        if (activeChild != null) {
            activeChild.stop();
            for (Pair<String, Long> p : activeChild.executedLabels) {
                executedLabels.add(new Pair<String, Long>(startedLabel + "." + p.v1, p.v2));
            }
            activeChild.isDisabled = true;
        }
        activeChild = null;
    }

    protected void addChild(StopWatch child) {
        activeChild = child;
    }

    public Collection<Pair<String, Long>> getExecutedLabels() {
        return executedLabels;
    }

    public String getExecutedLabelsPretty() {
        return getExecutedLabelsPretty(0);
    }

    private void appendPrettyTime(StringBuilder sb, long timeMillis, int maxDigits) {
        long secs = timeMillis / 1000;
        int millis = (int) (timeMillis % 1000);
        String str = BaseUtils.leftPad(secs + "." + BaseUtils.padInt(millis, 3),
                ' ', maxDigits);
        sb.append(str).append(" s.");
    }

    private void appendPrettyLine(StringBuilder sb, String label, int maxLabelLen,
            long timeMillis, int maxDigits) {

        sb.append(label).
                append(BaseUtils.replicateChar(' ', maxLabelLen - label.length())).
                append(": ");

        appendPrettyTime(sb, timeMillis, maxDigits);
    }

    public boolean isInActiveMode() {
        return activeMode;
    }

    public String getExecutedLabelsPretty(int indent) {
        if (!activeMode) {
            return "<stop watch in inactive mode>";
        }

        String totalLabelStr = "total time taken";
        int maxLabelLen = totalLabelStr.length();
        int totalMillis = 0;
        for (Pair<String, Long> l : executedLabels) {
            totalMillis += l.v2;
            if (l.v1.length() > maxLabelLen) {
                maxLabelLen = l.v1.length();
            }
        }

        int maxDigits = Integer.toString(totalMillis).length() + 1; // one for dot ('.')
        if (maxDigits < 5) {
            maxDigits = 5;
        }

        String indentStr = BaseUtils.replicateChar(' ', indent);
        StringBuilder sb = new StringBuilder();
//        sb.append(indentStr).append("[total time: ");
//        appendPrettyTime(sb, totalMillis, 0);
//        sb.append("]");

        //sb.append("\n");
        sb.append(indentStr);
        appendPrettyLine(sb, totalLabelStr, maxLabelLen, totalMillis, maxDigits);
        boolean first = false;
        for (Pair<String, Long> l : executedLabels) {
            if (first) {
                first = false;
            } else {
                sb.append("\n");
            }
            sb.append(indentStr);

            appendPrettyLine(sb, l.v1, maxLabelLen, l.v2, maxDigits);
        }

        return sb.toString();
    }
}
