/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author pmielanczuk
 */
public interface INamedPropsClassInfo extends INamedPropsBeanBase {

    public Class getPropClass(String propName);
}
