/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.logging;

import commonlib.LameUtils;
import java.util.LinkedHashMap;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import org.junit.*;
import simplelib.logging.ILameLogger.LameLogLevel;

/**
 *
 * @author wezyr
 */
public class LameLoggerFactoryTest {

    public LameLoggerFactoryTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetConfiguredLoggerLevel() {
        LameUtils.initFactories();

        Map<String, String> cfg = new LinkedHashMap<String, String>();

        //------------------------------------
        cfg.clear();
        cfg.put("/level", "Warn");
        cfg.put("/minLevel", "Info");
        cfg.put("JdbcConnectionBase", "debug");

        LameLoggerFactory.applyLameLoggersConfig(cfg, false);

        assertEquals(LameLogLevel.Info, LameLoggerFactory.getConfiguredLoggerLevel("pl.trzy0.foxy.serverlogic.db.JdbcConnectionBase"));
        assertEquals(LameLogLevel.Warn, LameLoggerFactory.getConfiguredLoggerLevel("pl.trzy0.foxy.serverlogic.db.JdbcConnectionBaseXXX"));

        //------------------------------------
        cfg.clear();
        cfg.put("/level", "Warn");
        cfg.put("JdbcConnectionBase", "debug");
        cfg.put("LameLoggerFactory", "debug");

        LameLoggerFactory.applyLameLoggersConfig(cfg, false);

        assertEquals(LameLogLevel.Debug, LameLoggerFactory.getConfiguredLoggerLevel(LameLoggerFactory.class.getName()));
        assertEquals(LameLogLevel.Debug, LameLoggerFactory.getConfiguredLoggerLevel("pl.trzy0.foxy.serverlogic.db.JdbcConnectionBase"));
        assertEquals(LameLogLevel.Warn, LameLoggerFactory.getConfiguredLoggerLevel("pl.trzy0.foxy.serverlogic.db.JdbcConnectionBaseXXX"));

        //------------------------------------
        cfg.clear();
        cfg.put("/level", "Info");
        cfg.put("/minLevel", "Warn");
        cfg.put("JdbcConnectionBase", "debug");

        LameLoggerFactory.applyLameLoggersConfig(cfg, false);

        assertEquals(LameLogLevel.Warn, LameLoggerFactory.getConfiguredLoggerLevel("pl.trzy0.foxy.serverlogic.db.JdbcConnectionBase"));
        assertEquals(LameLogLevel.Warn, LameLoggerFactory.getConfiguredLoggerLevel("pl.trzy0.foxy.serverlogic.db.JdbcConnectionBaseXXX"));
    }
}
