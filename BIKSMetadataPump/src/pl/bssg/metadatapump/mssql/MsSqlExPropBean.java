/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.mssql;

/**
 *
 * @author tflorczak
 */
public class MsSqlExPropBean {

    public String serverName;
    public String databaseName;
    public String type;
    public String owner;
    public String tableName;
    public String columnName;
    public String name;
    public String value;
    public String branchNames;

    public MsSqlExPropBean() {
    }

    public MsSqlExPropBean(String server, String databaseName, String type, String owner, String tableName, String columnName, String name, String value, String branchNames) {
        this.serverName = server;
        this.databaseName = databaseName;
        this.type = type;
        this.owner = owner;
        this.tableName = tableName;
        this.columnName = columnName;
        this.name = name;
        this.value = value;
        this.branchNames = branchNames;
    }
}
