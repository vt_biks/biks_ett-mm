/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.filesystem;

import commonlib.DateUtils;
import commonlib.LameUtils;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import pl.bssg.metadatapump.AdhocPartialBeanCollector;
import pl.bssg.metadatapump.GenericNodeBean;
import pl.bssg.metadatapump.MetadataPumpBase;
import pl.bssg.metadatapump.common.ConnectionParametersFileSystemBean;
import pl.bssg.metadatapump.common.PumpConstants;
import simplelib.BaseUtils;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author tflorczak
 */
public class FileSystemPump extends MetadataPumpBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    public static final String NODE_KIND_DOCUMENTS_FOLDER = "DocumentsFolder";
    public static final String NODE_KIND_RAW_FILE = "RawFile";
    public static final Integer MB = 1024 * 1024;

    protected AdhocPartialBeanCollector<GenericNodeBean> nodeCollector;
    protected AdhocPartialBeanCollector<FileSystemInfoBean> infoCollector;

    protected ConnectionParametersFileSystemBean config;
    protected NtlmPasswordAuthentication auth;
    protected List<String> wildCards;
    protected String path;
    protected long fileSince = 0;
    //    protected String fsPath;

    @Override
    protected Pair<Integer, String> startPump() {
        try {
            pumpLogger.pumpLog("Connect to server", 0);
            nodeCollector = new AdhocPartialBeanCollector<GenericNodeBean>(PACKAGE_SIZE, adhocDao, "insertIntoGenericTable");
            infoCollector = new AdhocPartialBeanCollector<FileSystemInfoBean>(PACKAGE_SIZE, adhocDao, "insertIntoAaaFileSystemInfoTable");

            config = adhocDao.createBeanFromNamedQry("getFileSystemConnectionParametersForInstanceName", ConnectionParametersFileSystemBean.class, false, instanceName);
            if (config.isRemote) {
                path = PumpFileUtils.fixPathIfNessecary(config.path);
                auth = new NtlmPasswordAuthentication(config.domain, config.user, config.password);
            } else {
                path = config.path;
            }
            wildCards = BaseUtils.splitBySep(config.fileNamePatterns, ",");
            if (BaseUtils.isCollectionEmpty(wildCards)) {
                wildCards.add("*.*");
            }
//            fsPath = "/fs/" + instanceName + "/";
            if (config.fileSince != null && !BaseUtils.isStrEmptyOrWhiteSpace(config.fileSince)) {
                fileSince = DateUtils.parse(new SimpleDateFormat("yyyy-MM-dd"), config.fileSince).getTime();
            }
            pumpLogger.pumpLog("Reading structures", 10);
            doPump();
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in File System", ex);
            }
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "FileSystemPump Pump error: " + ex.getMessage());
        }
        return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_FILE_SYSTEM;
    }

    private void doPump() throws MalformedURLException, SmbException, IOException {
        adhocDao.execNamedCommand("deleteFromAmgNodeBase");
        adhocDao.execNamedCommand("deleteFromAaaFileSystemInfoTable");
        List<String> queue = new ArrayList<String>();
        queue.add(path);

        for (int i = 0; i < queue.size(); i++) {
            String parentPath = queue.get(i).replace("\\", "/");
            IFileDelegator parentFolder = config.isRemote ? new SmbFileDelegator(parentPath, auth) : new LocalFileDelegator(parentPath);
            String parentObjId = parentPath.substring(path.length()).replace("/", "|");
            parentObjId = BaseUtils.ensureObjIdEndWith(parentObjId, "|");

            IFileDelegator[] listFilesAndFolders = parentFolder.listFiles();
            if (listFilesAndFolders != null) {
                for (IFileDelegator file : listFilesAndFolders) {
                    if (file.isDirectory()) {
                        queue.add(file.getPath());
                        String objId = BaseUtils.ensureObjIdEndWith(parentObjId + file.getFolderName(), "|");
                        nodeCollector.add(new GenericNodeBean(objId, parentObjId, NODE_KIND_DOCUMENTS_FOLDER, file.getFolderName(), null));
//                    File dir = new File(dirForUpload + fsPath + file.getPath().substring(remoteRootPath.length()));
//                    if (!dir.exists()) {
//                        dir.mkdirs();
//                    }
                    }
                }
            }
            for (String wildcard : wildCards) {
                try {
                    listFilesAndFolders = parentFolder.listFiles(wildcard);
                    if (listFilesAndFolders != null) {
                        for (IFileDelegator file : listFilesAndFolders) {
                            if (!file.isDirectory() && file.lastModified() > fileSince) {
                                String objId = BaseUtils.ensureObjIdEndWith(parentObjId + file.getName(), "|");
                                try {
                                    FileSystemInfoBean info = getFileSystemInfoAndDownloadFileIfNecessary(objId, file);
//                                String fileName = Normalizer.normalize(file.getName(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
                                    String fileName = file.getName();
                                    nodeCollector.add(new GenericNodeBean(objId, parentObjId, NODE_KIND_RAW_FILE, fileName, null));
                                    infoCollector.add(info);
                                } catch (Exception ex) {
                                    if (logger.isErrorEnabled()) {
                                        logger.error("Error in File System.doPump(). File name: " + file.getPath(), ex);
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                    // TF -> autor - cicho, cicho? źle!
                    if (logger.isErrorEnabled()) {
                        logger.error("Error in File System.doPump(): ", ex);
                    }
                }
            }
        }
        nodeCollector.flush();
        infoCollector.flush();
        pumpLogger.pumpLog("Delete old files", 70);
        deleteOldFiles();
        pumpLogger.pumpLog("Update BIKS structure", 90);
        adhocDao.execNamedCommand("insertGenericNodesIntoBikNode", config.treeCode);
        adhocDao.execNamedCommand("insertIntoFileSystemInfoTable", config.treeCode);
        adhocDao.execNamedCommand("updateNodeKindsForTree", config.treeCode);
    }

    private FileSystemInfoBean getFileSystemInfoAndDownloadFileIfNecessary(String objId, IFileDelegator file2Download) throws IOException {
        FileSystemInfoBean info = adhocDao.createBeanFromNamedQry("getFileSystemInfo", FileSystemInfoBean.class, true, config.treeCode, file2Download.getPath());
        if (info == null) {
            info = new FileSystemInfoBean();
        }
        info.targetFilePath = file2Download.getPath();
        info.objId = objId;
        info.fileName = file2Download.getName();
        if (config.downloadFiles && (file2Download.length() <= config.maxFileSize * MB || Math.abs(config.maxFileSize) < 1e-6)
                && (info.downloadLink == null //nie byl pobrany
                || info.fileSize.compareTo(file2Download.length()) != 0 //zmieniony rozmiar
                || info.lastModified.compareTo(file2Download.lastModified()) != 0)) {
            //download to BIKS
//            info.downloadLink = fsPath + file2Download.getPath().substring(remoteRootPath.length());
//            File file = new File(dirForUpload + info.downloadLink);
            String fileExt = info.fileName.substring(info.fileName.lastIndexOf("."));
//            if (!file.exists()) {
//                file.createNewFile();
//            }
            info.downloadLink = PumpFileUtils.write2TempFile(dirForUpload, fileExt, file2Download);
        } else {
            if (!config.downloadFiles || (file2Download.length() > config.maxFileSize * MB && Math.abs(config.maxFileSize) > 1e-6)) {
                info.downloadLink = null;
            }
        }

        info.fileSize = file2Download.length();
        info.lastModified = file2Download.lastModified();
        return info;
    }

    private void deleteOldFiles() {
        Set<String> deletedFilePaths = execNamedQuerySingleColAsSet("getDeletedFiles", null, config.treeCode);
        for (String filePath : deletedFilePaths) {
            if (filePath != null) {
                LameUtils.deleteFile(dirForUpload + "/" + filePath);
            }
        }
    }
}
