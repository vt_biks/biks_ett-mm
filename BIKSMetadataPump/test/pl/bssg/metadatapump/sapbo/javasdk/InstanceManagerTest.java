/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.javasdk;

import com.crystaldecisions.sdk.framework.CrystalEnterprise;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.framework.ISessionMgr;
import com.crystaldecisions.sdk.occa.infostore.IInfoObject;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import java.util.ArrayList;
import java.util.List;
import pl.bssg.metadatapump.common.SAPBOScheduleBean;
import static pl.bssg.metadatapump.sapbo.biadmin.ReportScheduleInfo.getOneSchedule;

/**
 *
 * @author tflorczak
 */
public class InstanceManagerTest {

    public static void main(String[] args) {
        IEnterpriseSession enterpriseSession = null;
        try {
            ISessionMgr sessionMgr = CrystalEnterprise.getSessionMgr();
            enterpriseSession = sessionMgr.logon("Administrator", "xxx", "192.168.168.102", "secEnterprise");
            IInfoStore iStore = (IInfoStore) enterpriseSession.getService("InfoStore");

            String whereQuery = "SI_INSTANCE = 1 and SI_UNDER_FAVORITES is null and si_kind = 'Webi'";

            List<SAPBOScheduleBean> schedules = new ArrayList<SAPBOScheduleBean>();
            Integer siID = null;
            while (true) {
                IInfoObjects results = iStore.query("select top 100 * from ci_infoobjects"
                        + (siID == null ? " where " + whereQuery : (" where (si_id > " + siID + ") AND " + whereQuery)) + " order by si_id");

                for (int i = 0; i < results.size(); i++) {
                    IInfoObject obj = (IInfoObject) results.get(i);
                    schedules.add(getOneSchedule(obj, iStore));
                    siID = obj.getID();
                }
                if (results.size() < 100) {
                    break;
                }
            }
            // print
            int i = 1;
            for (SAPBOScheduleBean schedule : schedules) {
                System.out.println(i++ + " - " + schedule.toString());
            }
        } catch (Exception ex) {
            System.out.println("Błąd: " + ex.getMessage());
        } finally {
            if (enterpriseSession != null) {
                enterpriseSession.logoff();
            }
        }
    }
}
