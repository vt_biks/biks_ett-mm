/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db.datapump;

import commonlib.LameUtils;
import commonlib.datapump.ColumnMetadata;
import commonlib.datapump.IDataPumpDestination;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.templates.INamedTemplateProvider;
import pl.trzy0.foxy.commons.templates.TemplateUtils;
import simplelib.BaseUtils;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public abstract class JdbcDataPumpDestination implements IDataPumpDestination {

    private static final ILameLogger logger = LameUtils.getMyLogger();

    public static final int ROWS_IN_PACK = 1000;

    protected IBeanConnectionEx<Object> dstConn;
    protected INamedTemplateProvider templateProvider;
    protected String dstTableName;
    protected ColumnMetadata[] sourceColsMetadata;
    protected Set<String> unmappedTypeInCols = new LinkedHashSet<String>();
    protected StringBuilder insertRowsSqlSb = new StringBuilder();
    protected long bufferedRows = 0;
    protected String insertIntoValuesSqlPart;
    protected String[] dstValStrBuff;
    protected int sourceColCnt;

    public JdbcDataPumpDestination(IBeanConnectionEx<Object> dstConn, INamedTemplateProvider templateProvider, String dstTableName) {
        this.dstConn = dstConn;
        this.templateProvider = templateProvider;
        this.dstTableName = dstTableName;
    }

    protected abstract void dropDstTableIfExists();

    protected String optAddAutoIncColumn() {
        return "";
    }

    @Override
    public void openDestination(ColumnMetadata[] sourceColsMetadata) throws Exception {

        if (logger.isDebugEnabled()) {
            logger.debug("openDestination: dstTableName=" + dstTableName);
        }

        this.sourceColsMetadata = sourceColsMetadata;
        sourceColCnt = sourceColsMetadata.length;

        if (!supportsMultiRowInserts()) {
            this.dstValStrBuff = new String[sourceColCnt];
        }

        StringBuilder sb = new StringBuilder();

        sb.append("create table [").append(dstTableName).append("] (\n");

        for (int i = 0; i < sourceColCnt; i++) {
            final ColumnMetadata srcColMd = sourceColsMetadata[i];

            String dstType = getDstType(srcColMd);

            if (i > 0) {
                sb.append(",\n");
            }
            sb.append("  [").append(srcColMd.colName).append("] ").append(dstType)/*.append(isNullable == 0 ? " not" : "")*/.append(" null");
        }
        sb.append(optAddAutoIncColumn());

        sb.append("\n)\n");

        String createTableSql = sb.toString();

        dropDstTableIfExists();

//        dstConn.finalizeTran(true);
        if (logger.isDebugEnabled()) {
            logger.debug("openDestination: create table sql: " + createTableSql);
        }

        dstConn.execCommand(createTableSql);
//        dstConn.finalizeTran(true);
    }

    @Override
    public void closeDestination(boolean success) throws Exception {
        if (success) {
            writeRowPackToDb();
        }
        dstValStrBuff = null;
    }

    public void writeRowPackToDb() {
        if (bufferedRows == 0) {
            return;
        }

        dstConn.execCommand(insertRowsSqlSb.toString());

        bufferedRows = 0;
        insertRowsSqlSb.setLength(0);
    }

    protected boolean supportsMultiRowInserts() {
        return true;
    }

    @Override
    public void writeDataRow(Object[] dataRow) {

        if (supportsMultiRowInserts()) {
            appendMultiRowInsertPart(dataRow);
        } else {
            appendSingleRowInsertPart(dataRow);
        }

        bufferedRows++;

        if (bufferedRows >= ROWS_IN_PACK) {
            writeRowPackToDb();
        }
    }

    protected void appendMultiRowInsertPart(Object[] dataRow) {
        boolean isFirstRow = insertRowsSqlSb.length() == 0;

        if (isFirstRow) {

            if (insertIntoValuesSqlPart == null) {

                insertRowsSqlSb.append("insert into [").append(dstTableName).append("] (");

                boolean isFirstCol = true;
                for (ColumnMetadata cMd : sourceColsMetadata) {
                    if (isFirstCol) {
                        isFirstCol = false;
                    } else {
                        insertRowsSqlSb.append(",");
                    }
                    insertRowsSqlSb.append("[").append(cMd.colName).append("]");
                }

                insertRowsSqlSb.append(") values ");

                insertIntoValuesSqlPart = insertRowsSqlSb.toString();
            } else {
                insertRowsSqlSb.append(insertIntoValuesSqlPart);
            }
        } else {
            insertRowsSqlSb.append(",");
        }

//        insertRowsSqlSb.append("(").append(BeanConnectionUtils.mergeAsSqlStrings(dstConn, Arrays.asList(dataRow))).append(")");
        insertRowsSqlSb.append("(");
        for (int i = 0; i < sourceColCnt; i++) {
            if (i > 0) {
                insertRowsSqlSb.append(",");
            }
            insertRowsSqlSb.append(formatValueForDst(sourceColsMetadata[i], dataRow[i]));
        }

        insertRowsSqlSb.append(")");
    }

    protected void appendSingleRowInsertPart(Object[] dataRow) {
        insertRowsSqlSb.append("insert into [").append(dstTableName).append("] (");

        for (int i = 0; i < sourceColCnt; i++) {
            dstValStrBuff[i] = formatValueForDst(sourceColsMetadata[i], dataRow[i]);
        }

        boolean isFirstVal = true;
        for (int i = 0; i < sourceColCnt; i++) {
            if (dstValStrBuff[i] == null) {
                continue;
            }

            if (isFirstVal) {
                isFirstVal = false;
            } else {
                insertRowsSqlSb.append(",");
            }

            ColumnMetadata cMd = sourceColsMetadata[i];
            insertRowsSqlSb.append("[").append(cMd.colName).append("]");
        }

        insertRowsSqlSb.append(") values ");

        isFirstVal = true;
        for (int i = 0; i < sourceColCnt; i++) {
            if (dstValStrBuff[i] == null) {
                continue;
            }

            if (isFirstVal) {
                isFirstVal = false;
            } else {
                insertRowsSqlSb.append(",");
            }

            insertRowsSqlSb.append(dstValStrBuff[i]);
        }
    }

    protected String getDstTypeForUnmappedSrcType(ColumnMetadata colMd) {
        unmappedTypeInCols.add(colMd.colName);

        return "nvarchar(max)";
    }

    protected String getDstType(ColumnMetadata colMd) {
        final HashMap<String, Object> noVars = new HashMap<String, Object>();
        String typeName = colMd.typeName;

        String res = TemplateUtils.render(templateProvider, typeName.toUpperCase(), noVars, colMd, true);

        if (res == null) {
            res = TemplateUtils.render(templateProvider, "*", noVars, colMd, true);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getDstType(" + colMd + "): " + res);
        }

        if (res == null) {
            res = getDstTypeForUnmappedSrcType(colMd);
        }

        return res;
    }

    protected String formatValueForDst(ColumnMetadata colMd, Object val) {

        String colName = colMd.colName;
        if (unmappedTypeInCols.contains(colName)) {
            return formatValueForUnmappedTypeInCol(colMd, val);
        }

        return dstConn.toSqlString(val);
    }

    protected String formatValueForUnmappedTypeInCol(ColumnMetadata colMd, Object val) {
        return dstConn.toSqlString(BaseUtils.safeToString(val));
    }
}
