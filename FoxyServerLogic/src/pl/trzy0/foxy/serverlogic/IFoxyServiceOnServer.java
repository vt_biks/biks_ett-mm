/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import pl.trzy0.foxy.commons.INamedSqlsDAO;

/**
 *
 * @author pmielanczuk
 * @param <CFG> - klasa z konfiguracją aplikacji
 * @param <SD> - klasa danych sesji (session data)
 * @param <SO> - klasa obiektu współdzielonego pomiędzy serwisami
 */
public interface IFoxyServiceOnServer<CFG, SD, SO> {

    public void init(CFG configBean, IFoxySessionDataProvider<SD> sessionDataProvider,
            INamedSqlsDAO<Object> adhocDao, SO sharedServiceObj);

    public void destroy();
}
