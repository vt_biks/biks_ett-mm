/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.mssql;

/**
 *
 * @author tflorczak
 */
public interface IMsSqlQueryNameProvider {

    public String getDatabases();

    public String getExtendedProperties();

    public String getObjects();

    public String getObjectEXProp();

    public String getColumns();

    public String getIndexes();

    public String getForeignKeys();

    public String getDependencies();

    public String getGrants();

}
