/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.dqm;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import java.util.HashMap;
import java.util.Map;
import pl.fovis.common.ConnectionParametersDQMConnectionsBean;
import pl.fovis.common.dqm.DQMMultiSourceBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public class AddOrEditDQMSourceDialogForConnections extends AddOrEditDQMSourceDialogBase {

    protected Map<Integer, ConnectionParametersDQMConnectionsBean> con = new HashMap<Integer, ConnectionParametersDQMConnectionsBean>();
    protected Map<Integer, Integer> indexToConnectionIdMap;

    public void buildAndShowDialog(int nodeId, DQMMultiSourceBean bean, Map<Integer, ConnectionParametersDQMConnectionsBean> connection, IParametrizedContinuation<DQMMultiSourceBean> saveCont) {
//        super.buildAndShowDialog(nodeId, bean, saveCont);
        this.nodeId = nodeId;
        this.bean = bean;
        this.saveCont = saveCont;
        this.con = connection;
        super.buildAndShowDialog();
    }

    @Override
    protected boolean doActionBeforeClose() {

        if (BaseUtils.isStrEmptyOrWhiteSpace(codeNameTbx.getText())) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.nazwaKodowaJestPusta.get(), I18n.blad.get(), null);
            return false;
        }

        if (BaseUtils.isStrEmptyOrWhiteSpace(queryArea.getText())) {
            new SimpleInfoDialog().buildAndShowDialog(I18n.zapytaniePobierajaceDaneNieMozeBycPuste.get(), I18n.blad.get(), null);
            return false;
        }
        return true;
    }

    @Override
    protected void fillLbx() {
        indexToConnectionIdMap = new HashMap<Integer, Integer>();
        int index = 0;
        for (ConnectionParametersDQMConnectionsBean cp : con.values()) {
            sourceLbx.addItem(cp.name);
            indexToConnectionIdMap.put(index++, cp.id);
        }
        sourceLbx.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
//                setSelectedSourceWidget();
                cleanWidgets();
            }
        });
    }

    @Override
    protected void innerWidget() {
        databaseVP.add(new Label(I18n.zapytaniePobierajaceDane.get()));
        databaseVP.add(queryArea = new TextArea());
        queryArea.setWidth("380px");
        queryArea.setHeight("200px");
        main.add(databaseVP);
    }

    @Override
    protected int getSelectedIdx() {
        int index = 0;
        for (Map.Entry<Integer, Integer> id : indexToConnectionIdMap.entrySet()) {
            if (bean.connectionId != null && id.getValue().equals(bean.connectionId)) {
                return id.getKey();

            }
        }
        return index;
    }

    @Override
    protected void doActionOpt(DQMMultiSourceBean bean) {
        bean.connectionId = indexToConnectionIdMap.get(sourceLbx.getSelectedIndex());
//        bean.sourceId = indexToConnectionIdMap.get(sourceLbx.getSelectedIndex());
        bean.sourceName = con.get(bean.connectionId).sourceType;
        bean.connectionName=sourceLbx.getSelectedItemText();
    }

    protected void cleanWidgetsOpt() {
        //no-op
    }

    protected void setSelectedSourceWidget() {
        //no-op
    }

    @Override
    protected String getName() {
        return I18n.dqmPolaczenie.get();
    }
}
