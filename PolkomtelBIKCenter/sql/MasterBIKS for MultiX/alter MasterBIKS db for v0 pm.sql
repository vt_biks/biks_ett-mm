
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- inicjalny wydzielony skrypt do zak�adania bazy
-- MasterBIKS dla MultiXa
-- poni�sze obiekty mo�na te� za�o�y� na bazie
-- BIKSa - maj� oddzielne prefiksy nazw
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

CREATE TABLE [mltx_app_prop](
	[name] [varchar](255) NOT NULL primary key,
	[val] [varchar](max) NULL
)
go

/*
select 'insert into mltx_app_prop (name, val) values (' + 
  dbo.fn_escape_and_quote_str(name) + ', ' + dbo.fn_escape_and_quote_str(val) + ')'
from mltx_app_prop
*/

insert into mltx_app_prop (name, val) values ('is_multi', 'true')
insert into mltx_app_prop (name, val) values ('multixEmailFrom', 'BIKS team <testerbps@gmail.com>')
insert into mltx_app_prop (name, val) values ('multixEmailPassword', 'maselnica')
insert into mltx_app_prop (name, val) values ('multixEmailSmtpHostName', 'smtp.gmail.com')
insert into mltx_app_prop (name, val) values ('multixEmailSmtpPort', '465')
insert into mltx_app_prop (name, val) values ('multixEmailSsl', 'true')
insert into mltx_app_prop (name, val) values ('multixEmailUser', 'testerbps')
insert into mltx_app_prop (name, val) values ('multiXDbVer', '0')
insert into mltx_app_prop (name, val) values ('languages', 'pl,en')

-- SELECT DB_NAME() AS DataBaseName

/*
declare @regular varchar(max) = (select top 1 physical_name from sys.database_files where type = 0);
declare @reversed varchar(max) = REVERSE(@regular);
declare @path varchar(max) = LEFT(@regular, len(@regular) - CHARINDEX('\', @reversed)+1) ;
go
*/
-- tak robimy, bo w adhocSqlsMultiX.xml, w createNewDBInstance jest logika (powy�ej zakomentowana)
-- okre�lania �cie�ki (�cie�ka musi ko�czy� si� na '\')
-- w razie potrzeby mo�na tutaj ustawi� to na sta��
insert into mltx_app_prop (name, val) values ('database_files_destination_path', null)
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

CREATE TABLE [mltx_database_template](
	[id] [int] IDENTITY(1,1) NOT NULL primary key,
	[caption] [varchar](256) NOT NULL,
	[restore_prefix] [varchar](32) NOT NULL,
	[file_path] [varchar](512) NOT NULL,
	[lang] [varchar](3) NOT NULL
)
GO

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

CREATE TABLE [mltx_database](
	[id] [int] IDENTITY(1,1) NOT NULL primary key,
	[created_at] [datetime] NOT NULL default getdate(),
	[database_name] [varchar](255) NOT NULL,
	UNIQUE (database_name)
)
GO

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

CREATE TABLE [mltx_registration](
	[id] [int] IDENTITY(1,1) NOT NULL primary key,
	[token] [varchar](128) NOT NULL,
	[email] [varchar](128) NOT NULL,
	[password] [varchar](128) NOT NULL,
	[is_valid] [int] NOT NULL DEFAULT ((1)),
	[created_at] [datetime] NOT NULL DEFAULT (getdate()),
	[expires_at] [datetime] NOT NULL,
	[status] [int] NOT NULL,
	[template_id] [int] NULL,
    CONSTRAINT uq_mltx_email_token UNIQUE ([email], [token]),
    CONSTRAINT fk_mltx_registration_database_template FOREIGN KEY (template_id) REFERENCES mltx_database_template (id)
)
GO

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

CREATE TABLE [mltx_login](
	[login] [varchar](255) NOT NULL primary key,
	[database_id] [int] NOT NULL,
	CONSTRAINT mltx_login_FK_to_mltx_database FOREIGN KEY(database_id) REFERENCES mltx_database (id)
)
GO


------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_mltx_check_version]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_mltx_check_version]
GO

create procedure [dbo].[sp_mltx_check_version] (@ver varchar(255)) as
begin	
	declare @version varchar(255)
	select @version = val from mltx_app_prop where name = 'multiXDbVer';
	if(coalesce(@version,'skljsdlfjs') != coalesce(@ver, 'asdsadsa'))
	begin
	    declare @txt nvarchar(4000) = N'!!! Wrong version number: ' + @ver + 
	      '. Current version is ' + @version + '. !!!'
	    
	    while 1 = 1 begin
	    print @txt
	    
		raiserror(@txt,
				15,
				-1);
				
		print 'you must manually stop execution of this script!!!'
		print 'prosz� przerwa� dalsze wykonanie tego skryptu r�cznie!!!'
		
		select @txt
		select 'you must manually stop execution of this script!!!'
		select 'prosz� przerwa� dalsze wykonanie tego skryptu r�cznie!!!'
		
		WAITFOR DELAY '00:00:10';
	    end
	end	
end
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_mltx_update_version]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_mltx_update_version]
GO

create procedure [dbo].[sp_mltx_update_version](@ver_current varchar(255), @ver_upper varchar(255))
as
	declare @version varchar(255)
begin
	exec sp_mltx_check_version @ver_current;
	
	update mltx_app_prop set val=@ver_upper where name='multiXDbVer';	
end
GO

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

alter table mltx_database_template add is_hidden int not null default 0
go

update mltx_database_template set is_hidden = 1
go

insert into mltx_database_template (caption, file_path, restore_prefix, lang)
     values ('Baza BIKS z przyk�adowymi danymi w j�zyku polskim', 'C:\projekty\mssql-backups\BIKS_GT_PL_v1.5.z.bak', 'biks_pl_', 'pl');
insert into mltx_database_template (caption, file_path, restore_prefix, lang)
     values ('BIKS database with sample english data', 'C:\projekty\mssql-backups\BIKS_GT_EN_v1.5.z.bak', 'biks_en_', 'en');
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_mltx_update_version '0', '1'
go
