/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author tflorczak
 */
public class TreeIconBean implements Serializable{
    
    public int id;
    public int treeId;
    public int nodeKindId;
    public String treeCode;
    public String nodeKindCode;
    public String icon;
}
