/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.lang.reflect.Array;
import java.util.Iterator;

/**
 *
 * @author wezyr
 */
public class GenericArrayIterator<T> implements Iterator<T> {

    private Object array;
    private int length;
    private int idx = 0;

    public GenericArrayIterator(Object array) {
        this.array = array;
        this.length = Array.getLength(array);
    }

    public boolean hasNext() {
        return idx < length;
    }

    @SuppressWarnings("unchecked")
    public T next() {
        if (hasNext()) {
            return (T)Array.get(array, idx++);
        }
        throw new IllegalStateException("no more elements");
    }

    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
