/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.commons.tags.htmltags;

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public interface IRenderableTag {
    public String getTagName();
    public Map<String, String> getTagAttrs();
    public boolean isTextTag();
    public boolean isClosedInPlace();
    public Collection<IRenderableTag> getRenderableSubTags();
}
