/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class AddUsersToGroupDialog extends BaseActionOrCancelDialog {

    protected IParametrizedContinuation<Pair<List<Integer>, List<String>>> saveCont;
    protected List<String> usersInGroup;
    protected List<SystemUserBean> allUsers;
    protected List<Integer> usersIdsInGroup = new ArrayList<Integer>();
    protected Map<Integer, CheckBox> checkBoxMaps;
    protected Map<Integer, RadioButton> radioButtonMaps;
    protected HashMap<Integer, String> idToNamesMap;
    protected boolean isWithCheckbox;
    protected String caption;

    @Override
    protected String getDialogCaption() {
        return caption;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.save.get();
    }

    @Override
    protected void buildMainWidgets() {

        FlexTable grid = new FlexTable();
        grid.setCellSpacing(4);
        int i = 0;
        int row = 0;
        checkBoxMaps = new HashMap<Integer, CheckBox>();
        radioButtonMaps = new HashMap<Integer, RadioButton>();
        idToNamesMap = new HashMap<Integer, String>();
        for (SystemUserBean s : allUsers) {
            String name = s.loginName + " (" + s.systemUserName + ")";
//
//            CheckBox cb = new CheckBox(name);
//            if (usersInGroup != null && usersInGroup.contains(name)) {
//                cb.setValue(true);
//            }
//            checkBoxMaps.put(s.id, cb);
            if (isWithCheckbox) {

                grid.setWidget(row, i++, checkBoxPanel(name, s.id));
            } else {
                grid.setWidget(row, i++, radioButtonPanel(name, s.id));

            }

            idToNamesMap.put(s.id, name);
//            grid.setWidget(row, i++, cb);

            if (i == 6) {
                i = 0;
                row++;
            }
        }
        ScrollPanel sc = new ScrollPanel(grid);

        setScrollPanelSize(sc, 100);
        sc.setWidth("100%");
        main.add(sc);

    }

    protected CheckBox checkBoxPanel(String name, int id) {
        CheckBox cb = new CheckBox(name);
        if (usersInGroup != null && usersInGroup.contains(name)) {
            cb.setValue(true);
        }
        checkBoxMaps.put(id, cb);
        return cb;
    }

    protected RadioButton radioButtonPanel(String name, int id) {
        RadioButton rb = new RadioButton("rb");
        rb.setText(name);
        if (usersInGroup != null && usersInGroup.contains(name)) {
            rb.setValue(true);
        }
        radioButtonMaps.put(id, rb);
        return rb;
    }

    @Override
    protected void doAction() {
        usersInGroup = new ArrayList<String>();
        if (isWithCheckbox) {
            for (Entry<Integer, CheckBox> a : checkBoxMaps.entrySet()) {
                if (a.getValue().getValue()) {
                    int id = a.getKey();
                    usersIdsInGroup.add(id);
                    usersInGroup.add(idToNamesMap.get(id));
                }
            }
        } else {

            for (Entry<Integer, RadioButton> a : radioButtonMaps.entrySet()) {
                if (a.getValue().getValue()) {
                    int id = a.getKey();
                    usersIdsInGroup.add(id);
                    usersInGroup.add(idToNamesMap.get(id));
                }
            }

        }
        saveCont.doIt(new Pair<List<Integer>, List<String>>(usersIdsInGroup, usersInGroup));
    }

    public void buildAndShowDialog(List<String> usersInGroup, List<SystemUserBean> allUsers,
            String caption, boolean isWithCheckbox,
            IParametrizedContinuation<Pair<List<Integer>, List<String>>> saveCont) {
        this.saveCont = saveCont;
        this.usersInGroup = usersInGroup;
        this.allUsers = allUsers;
        this.caption = caption;
        this.isWithCheckbox = isWithCheckbox;
        super.buildAndShowDialog();
    }

}
