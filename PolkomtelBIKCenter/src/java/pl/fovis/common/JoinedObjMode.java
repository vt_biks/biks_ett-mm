/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

/**
 *
 * @author wezyr
 */
public enum JoinedObjMode {
    //ww: świetny przykład siły enumów! polecam przestudiowanie co się tu
    // właściwie dzieje i jak to potem jest używane.

    NotJoined("none" /* I18N: no */), WithInheritance("many" /* I18N: no */), Single("single" /* I18N: no */);
    public String imgNamePart;

//    JoinedObjMode() {
//
//    }
//
    JoinedObjMode(String imgNamePart) {
        this.imgNamePart = imgNamePart;
    }

    public JoinedObjMode getNextVal(boolean isStandardJoinedObjMode) {
        int nextOrdinal;
        if (isStandardJoinedObjMode) {
             nextOrdinal = (ordinal() + 1) % JoinedObjMode.values().length;
        }else{// dla linkow zeby byl tylko wybór zaznaczone/niezaznaczone
            nextOrdinal = ordinal() == 0 ? 2 : 0;
        }

        return JoinedObjMode.values()[nextOrdinal];
    }
}
