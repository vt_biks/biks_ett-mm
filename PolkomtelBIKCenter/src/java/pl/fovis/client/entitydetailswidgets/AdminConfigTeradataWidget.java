/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.ConnectionParametersTeradataBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class AdminConfigTeradataWidget extends AdminConfigBIKBaseWidget<ConnectionParametersTeradataBean> {

//    protected TextBox tbxDriverClass;
    protected TextBox tbxURL;
    protected TextBox tbxUser;
    protected TextBox tbxPw;
    protected TextBox tbxSelect;
    protected TextBox tbxMaxErrorCount;
//    protected CheckBox chbPw;
//    protected Label lblDriverClass;
    protected Label lblURL;
    protected Label lblInstance;
    protected Label lblUser;
    protected Label lblPw;
    protected Label lblSelect;
    protected Label lblMaxErrorCount;
    protected PushButton btnSave;
    protected CheckBox chbIsActive;
    protected ListBox lbInstance;
    protected Map<String, LisaInstanceInfoBean> id2InstanceMap = new HashMap<String, LisaInstanceInfoBean>();
    protected ConnectionParametersTeradataBean data;

    @Override
    public void buildInnerWidget() {
        chbIsActive = createCheckBox(I18n.aktywny.get(), new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetsEnabled(chbIsActive.getValue());
            }
        }, false);
        createLine();
        lblInstance = createLabel(I18n.instancja.get());
        lbInstance = createListBox(new HashSet<String>());
        lbInstance.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                String instanceId = lbInstance.getSelectedValue();
                LisaInstanceInfoBean instance = id2InstanceMap.get(instanceId);
                tbxURL.setText("jdbc:teradata://" + instance.url);
            }
        });
        lblURL = createLabel(I18n.serwer.get());
        tbxURL = createTextBox();
        lblUser = createLabel(I18n.uzytkownik.get());
        tbxUser = createTextBox();
        lblPw = createLabel(I18n.haslo.get());
        tbxPw = createPasswordTextBox();
//        chbPw = createAndAddCheckBox(I18n.ukryjHaslo.get(), new ValueChangeHandler<Boolean>() {
//            @Override
//            public void onValueChange(ValueChangeEvent<Boolean> event) {
//                tbxPw.getElement().setAttribute("type" /* I18N: no */, chbPw.getValue() ? "password" /* I18N: no */ : "text" /* I18N: no */);
//            }
//        });

        populateInstanceList();
        lblSelect = createLabel(I18n.zapytaniePobierajaceTabele.get());
        tbxSelect = createTextBox();
        lblMaxErrorCount = createLabel(I18n.maksymalnaIloscBledowDlaShowTables.get());
        tbxMaxErrorCount = createTextBox();
        createLine();
        btnSave = createButton(I18n.zapisz.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ConnectionParametersTeradataBean bean = new ConnectionParametersTeradataBean();
//                bean.driverClass = tbxDriverClass.getText();
                bean.url = tbxURL.getText();
                bean.user = tbxUser.getText();
                bean.password = tbxPw.getText();
                bean.instanceId = lbInstance.getSelectedValue();
                bean.select = tbxSelect.getText();
                bean.maxErrorCount = tbxMaxErrorCount.getText();
                bean.isActive = chbIsActive.getValue() == true ? 1 : 0;
                BIKClientSingletons.getService().setTeradataConnectionParameters(bean, new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " setTeradataConnectionParameters") {
                            @Override
                            public void onSuccess(Void result) {
                                BIKClientSingletons.showInfo(I18n.zmienionParametrDoPolaczenZTerad.get() /* I18N:  */);
                            }
                        });
            }
        });
        createTestConnectionWidget(I18n.testujPolaczenie.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().runTeradataTestConnection(new StandardAsyncCallback<Pair<Integer, String>>("Error in" /* I18N: no */ + " runTeradataTestConnection") {
                            @Override
                            public void onSuccess(Pair<Integer, String> result) {
                                setResultTestConnection(result);
                            }
                        });
            }
        });
    }

    @Override
    public void populateWidgetWithData(ConnectionParametersTeradataBean data) {
//        tbxDriverClass.setText(data.driverClass);
        this.data = data;
        tbxURL.setText(data.url);
        tbxUser.setText(data.user);
        tbxPw.setText(data.password);
        tbxSelect.setText(data.select);
        tbxMaxErrorCount.setText(data.maxErrorCount);
        chbIsActive.setValue(data.isActive == 1);
        setWidgetsEnabled(chbIsActive.getValue());
        if (lbInstance != null) {
            for (int i = 0; i < lbInstance.getItemCount(); i++) {
                if (lbInstance.getValue(i).equals(data.instanceId)) {
                    lbInstance.setSelectedIndex(i);
                }
            }
        }
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_TERADATA;
    }

    private void populateInstanceList() {
        BIKClientSingletons.getLisaService().getLisaInstanceList(new StandardAsyncCallback<List<LisaInstanceInfoBean>>() {

            @Override
            public void onSuccess(List<LisaInstanceInfoBean> instanceList) {
                id2InstanceMap.clear();
                lbInstance.clear();
                for (LisaInstanceInfoBean instance : instanceList) {
                    id2InstanceMap.put(String.valueOf(instance.id), instance);
                    lbInstance.addItem(instance.name);
                    lbInstance.setValue(lbInstance.getItemCount() - 1, String.valueOf(instance.id));
                }
                if (data != null) {
                    for (int i = 0; i < lbInstance.getItemCount(); i++) {
                        if (lbInstance.getValue(i).equals(data.instanceId)) {
                            lbInstance.setSelectedIndex(i);
                        }
                    }
                }
            }
        });
    }
}
