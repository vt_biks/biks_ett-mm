/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.dev.gwt;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.TreeLogger.Type;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JField;
import com.google.gwt.core.ext.typeinfo.JGenericType;
import com.google.gwt.core.ext.typeinfo.JPackage;
import com.google.gwt.core.ext.typeinfo.JParameterizedType;
import com.google.gwt.core.ext.typeinfo.TypeOracle;
import com.google.gwt.dev.resource.ResourceOracle;
import com.google.gwt.dev.resource.impl.PathPrefixSet;
import com.google.gwt.dev.resource.impl.ResourceOracleImpl;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;
import commonlib.LameUtils;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;
import simplelib.StringEscapeUtils;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public abstract class FoxyGeneratorBase extends Generator {

    private static final TreeLogger.Type DEFAULT_LOGGER_LEVEL = TreeLogger.DEBUG;
    private TreeLogger logger;
    private GeneratorContext context;
    protected String genClassName;
    protected TypeOracle typeOracle;
    protected JClassType genClassType;
    private JPackage pkg;
    private String pkgName;
    private String outClassName;
    private PrintWriter printWriter;
    private PrintWriter contextPrintWriter;
    private StringWriter strWriter;
    private String qualifiedOutClassName;
    private ClassSourceFileComposerFactory composer;
    private SourceWriter sourceWriter;

    private boolean isAlreadyGenerated() {
        return contextPrintWriter == null;
    }

    protected void initEnvironment() {
        sourceWriter = null;
        lineNum = 0;

        typeOracle = context.getTypeOracle();
        genClassType = typeOracle.findType(genClassName);
        pkg = genClassType.getPackage();
        pkgName = pkg.getName();
        outClassName = genClassType.getName() + "Wrapper" /* I18N: no */;
        qualifiedOutClassName = pkgName + "." + outClassName;

        contextPrintWriter = context.tryCreate(logger, pkgName, outClassName);
        if (contextPrintWriter != null) {
            strWriter = new StringWriter();
            printWriter = new PrintWriter(strWriter);
            composer = new ClassSourceFileComposerFactory(pkgName, outClassName);
            addImports();
            setSuperclass();
            addImplementedInterfaces();
        }
    }

    protected void addImport(String qualifiedClassName) {
        composer.addImport(qualifiedClassName);
    }

    protected void addImport(Class c) {
        addImport(typeOracle.findType(c.getCanonicalName()).getQualifiedSourceName());
    }

    protected void addImports() {
    }

    protected JClassType getClassType(Class c) {
        JClassType classType = typeOracle.findType(c.getName());
        if (classType == null) {
            throw new LameRuntimeException("unable to find class" /* I18N: no */ + ": \"" + c.getCanonicalName() + "\"");
        }
        return classType;
    }

    @Override
    public String generate(TreeLogger logger, GeneratorContext context, String typeName) throws UnableToCompleteException {
        this.logger = logger;
        log("generate starts, typeName" /* I18N: no */ + "=\"" + typeName + "\", " + "initial line number" /* I18N: no */ + ": " + lineNum);
        //logger.log(LOGGER_LEVEL, "starting wwgenerator: genClassName=" + genClassName);
        this.context = context;
        this.genClassName = typeName;

        initEnvironment();

        if (!isAlreadyGenerated()) {
            log("before generateClassBody" /* I18N: no */);
            generateClassBody();
            log("done generateClassBody, before commit" /* I18N: no */);
            //println("}");
            //context.commit(logger, printWriter);
            closeClassAndCommit();
        } else {
            log("this class is already created" /* I18N: no */);
        }

        log("done generate, outClassName" /* I18N: no */ + "=\"" + qualifiedOutClassName + "\"");
        return qualifiedOutClassName;
    }

    protected abstract void generateClassBody() throws UnableToCompleteException;

    protected void setSuperclass() {
    }

    private void addImplementedInterfaces() {
        composer.addImplementedInterface(genClassName);
    }

    protected String loadFileContent(String fullResourcePath, String encoding) {
        InputStream inStream = null;
        inStream = context.getClass().getResourceAsStream(fullResourcePath);
        if (inStream == null) {
            throw new LameRuntimeException("input stream is null for file" /* I18N: no */ + "=\"" + fullResourcePath + "\"");
        }
        return LameUtils.loadAsString(inStream, BaseUtils.isStrEmpty(encoding) ? LameUtils.getDefaultSystemEncoding() : encoding);
    }

    private void ensureSourceWriter() {
        if (sourceWriter == null) {
            sourceWriter = composer.createSourceWriter(printWriter);
        }
    }
    private int lineNum = 1;

    protected void println(String line) {
        ensureSourceWriter();
        //int prevStringWriterSize = strWriter.getBuffer().length();
        //log("/*" + genClassType.getName() + "#" + lineNum + "*/ " + line);
        sourceWriter.println(line);
        //log("// prev string writer size: " + prevStringWriterSize + ", current: " + strWriter.getBuffer().length());
        //strWriter.write("//xxx#"+ lineNum + "\n");
        //log("// prev string writer size: " + prevStringWriterSize + ", current: " + strWriter.getBuffer().length());

        lineNum++;
    }

    private void closeClassAndCommit() {
        println("}");
        printWriter.close();
        String generatedClassCode = strWriter.toString();

//        logger.branch(getDefaultLogLevel(), "/*--- generated " + genClassName + " code ---*/")
//                .log(getDefaultLogLevel(), generatedClassCode);
        contextPrintWriter.print(generatedClassCode);
        contextPrintWriter.close();

        context.commit(logger, contextPrintWriter);

//        ensureSourceWriter();
//        sourceWriter.commit(logger);
    }

    protected void logWarn(String msg) {
        logger.log(TreeLogger.WARN, msg);
    }

    protected void log(String msg) {
        logger.log(getDefaultLogLevel(), msg);
    }

    protected void throwUnableToComplete(String errorMsg, Throwable ex) throws UnableToCompleteException {
        logger.log(TreeLogger.ERROR, errorMsg, ex);
        throw new UnableToCompleteException();
    }

    protected void throwUnableToComplete(String errorMsg) throws UnableToCompleteException {
        throwUnableToComplete(errorMsg, null);
    }

    protected Type getDefaultLogLevel() {
        return DEFAULT_LOGGER_LEVEL;
    }

    protected ResourceOracle createResourceOracle(PathPrefixSet pathPrefixSet) {
        ResourceOracleImpl ro = new ResourceOracleImpl(logger);
        ro.setPathPrefixes(pathPrefixSet);
        //ro.refresh(logger);
        // TF: przy użyciu GWT 2.7.0 poniższej metody nie ma. Wymaga analizy. Tymczasowo zostajemy przy bibliotece z 2.6.0
        ResourceOracleImpl.refresh(logger, ro);
        return ro;
    }

    protected Set<String> getResourcePathNames(PathPrefixSet pathPrefixSet) {
        ResourceOracle ro = createResourceOracle(pathPrefixSet);
        return ro.getPathNames();
    }

    protected String getPackagePath(String relativePath) {
        boolean isRPEmpty = BaseUtils.isStrEmpty(relativePath);
        if (!isRPEmpty && relativePath.startsWith("/")) {
            return relativePath;
        }
        String basePath = "/" + BaseUtils.dropLastItems(genClassName.replace(".", "/"), "/", 1);
        if (!isRPEmpty) {
            basePath = BaseUtils.mergePaths(basePath, relativePath, "/", "..");
        }
        return basePath;
    }

    protected String getThrowStmt(String... msgParts) {
        StringBuilder sb = new StringBuilder();

        int i = 0;
        for (String msgPart : msgParts) {
            if (i != 0) {
                sb.append(" + ");
            }
            if (i % 2 == 0) {
                msgPart = "\"" + StringEscapeUtils.escapeJavaScript(msgPart) + "\"";
            }
            sb.append(msgPart);
            i++;
        }

        return "throw new LameRuntimeException" /* I18N: no */ + "(" + sb.toString() + ");";
    }

    public static <C extends Collection<? super JField>> C addAllFields(JClassType jC, C coll) {
        while (jC != null) {
            coll.addAll(Arrays.asList(jC.getFields()));
            jC = jC.getSuperclass();
        }

//        System.out.println("addAllFields: " + coll);
        return coll;
    }

    public static Set<JField> getAllFields(JClassType c) {
        return addAllFields(c, new HashSet<JField>());
    }

    protected JClassType getGenClassParametrizationOf(Class c) throws UnableToCompleteException {
        JGenericType intfType = getClassType(c).isGenericType();
        if (intfType == null) {
            return null;
        }

        JParameterizedType parametrization = genClassType.asParameterizationOf(intfType);
        if (parametrization == null) {
            return null;
        }

        JClassType[] typeArgs = parametrization.getTypeArgs();

        if (typeArgs.length != 1) {
            throwUnableToComplete("parametrization for class" /* I18N: no */ + " \""
                    + genClassName
                    + "\" " + "has" /* I18N: no */ + " " + typeArgs.length + " " + "typeArgs, but should have just" /* I18N: no */ + " 1");
        }

        JClassType oneTypeArg = typeArgs[0];
        JGenericType oneArgGenericType = oneTypeArg.isGenericType();
        if (oneArgGenericType != null) {
            throwUnableToComplete("type arg of dynamic class creator for class" /* I18N: no */ + " \""
                    + genClassName
                    + "\" " + "is generic type" /* I18N: no */ + " \"" + oneTypeArg.getName() + "\"");
        }

        return oneTypeArg;
    }
}
