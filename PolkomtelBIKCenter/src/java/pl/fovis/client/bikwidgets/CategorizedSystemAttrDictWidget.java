/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author tflorczak
 */
public class CategorizedSystemAttrDictWidget extends CategorisedAttrWidgetBase {

    // czy zakładka dotyczy atrybutów wbudowanych/systemowych: 0-false, 1-true
    protected final static int IS_BUILT_IN = 1;
    protected Map<Integer, Integer> atrIdsCntTmp = new HashMap<Integer, Integer>();
    protected PushButton saveButton;
    protected HTML infoLabel;

    @Override
    protected String getItemNameForItem(AttributeBean item) {
        return BIKClientSingletons.getTranslatedAttributeName(item.atrName);
    }

    @Override
    protected String getCategoryNameForItem(AttributeBean item) {
        return BIKClientSingletons.getTranslatedAttrCategoryName(item.atrCategory);
    }

//    @Override
//    protected String getTranslatedItemCaption(String itemCaption) {
//        return BIKClientSingletons.getTranslatedAttributeName(itemCaption);
//    }
    @Override
    protected String getAddItemButtonCaption() {
        return I18n.dodajAtrybut.get() /* I18N:  */;
    }

    @Override
    protected void updateItemInCategory(final int itemId, String name, int categoryId) {
        // NO OP
    }

    @Override
    protected Widget createOptCheckboxForCategoryWidget(final int catId) {
        final String check = categoryCheckboxImage.get(catId) != null ? categoryCheckboxImage.get(catId) : "0";
        final PushButton categoryBtn = new PushButton(new Image("images/checkbox" /* I18N: no */ + check + "." + "gif" /* I18N: no */));
        categoryCheckboxBtns.put(catId, categoryBtn);
        categoryBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                int cnt = atrIdCntInCategory.get(catId) != null && atrIdCntInCategory.get(catId) == itemsInCategory.get(catId).size() ? 0 : 1;
                for (Integer ic : itemsInCategory.get(catId)) {
                    atrIdsCnt.put(ic, cnt);
                }
                refreshCheckbox(atrIdsCnt);
            }
        });
        categoryBtn.addStyleName("checkboxAttr");
        return categoryCheckboxBtns.get(catId);
    }

    @Override
    protected Widget createOptCheckboxForItemWidget(final int attrId) {
        String check = atrIdsCheckboxImage.get(attrId) != null ? atrIdsCheckboxImage.get(attrId) : "0";

        PushButton attrCheckboxBtn = new PushButton(new Image("images/checkbox" /* I18N: no */ + check + "." + "gif" /* I18N: no */));
        atrIdsCheckboxBtns.put(attrId, attrCheckboxBtn);

        attrCheckboxBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                int cnt = atrIdsCheckboxImage.get(attrId) != null && atrIdsCheckboxImage.get(attrId).equals("1") ? 0 : 1;
                atrIdsCnt.put(attrId, cnt);
                refreshCheckbox(atrIdsCnt);
            }
        });
        attrCheckboxBtn.addStyleName("checkboxAttr");
        return atrIdsCheckboxBtns.get(attrId);
    }

    @Override
    protected void refreshData() {
        // NO OP
    }

    @Override
    protected PushButton saveBtn() {
        PushButton saveTmpButton = new PushButton(I18n.zapiszZmiany.get() /* I18N:  */);
        NewLookUtils.makeCustomPushButton(saveTmpButton);
        saveTmpButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getService().modifySystemAttrVisibleForKind(atrIdsCnt, selectedNodeKindIds, new StandardAsyncCallback<Void>("Failed in save changes" /* I18N: no:err-fail-in */) {
                    public void onSuccess(Void result) {
                        for (Integer nodeKindId : selectedNodeKindIds) {
                            List<AttributeBean> attrList = BIKClientSingletons.getAttributesForKind(nodeKindId);
                            for (AttributeBean aB : attrList) {
                                if (atrIdsCnt.get(aB.id) != 2) { // 2 = checkbox przerywany. Dla niego nie zmieniamy nic
                                    aB.isVisible = atrIdsCnt.get(aB.id);
                                }
                            }
                        }
                        BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get() /* I18N:  */);
                    }
                });
            }
        });
        return saveTmpButton;
    }

    public void recalcCheckBoxesByNodeKindIds(Set<Integer> selectedNodeKindIds) {
        atrIdsCnt.clear();
        int count = 0;
        Set<AttributeBean> atrSet = new HashSet<AttributeBean>();
        for (Integer snk : selectedNodeKindIds) {
            List<AttributeBean> attributesForKind = BIKClientSingletons.getAttributesForKind(snk);
            if (attributesForKind != null) {
                atrSet.addAll(attributesForKind);
                count++;
                //
                for (AttributeBean ab : attributesForKind) {
                    if (atrIdsCnt.containsKey(ab.id)) {
                        if (atrIdsCnt.get(ab.id) != ab.isVisible) {
                            atrIdsCnt.put(ab.id, 2);
                        }
                    } else {
                        atrIdsCnt.put(ab.id, ab.isVisible);
                    }
                }
            }
        }
        List<AttributeBean> atrList = new ArrayList<AttributeBean>(atrSet);
        Collections.sort(atrList, new Comparator<AttributeBean>() {
            public int compare(AttributeBean o1, AttributeBean o2) {
                int tmp;
                if ((tmp = o1.atrCategory.compareToIgnoreCase(o2.atrCategory)) == 0) {
                    tmp = o1.atrName.compareToIgnoreCase(o2.atrName);
                }
                return tmp;
            }
        });
        displayFetchedData(atrList);
        if (count > 0) {
            saveButton.setVisible(true);
            infoLabel.setVisible(false);
        } else {
            saveButton.setVisible(false);
            infoLabel.setVisible(true);
        }
        this.selectedNodeKindIds = selectedNodeKindIds;
        selectedIdCnt = selectedNodeKindIds.size();
        refreshCheckbox(atrIdsCnt);
    }

    public void refreshCheckbox(Map<Integer, Integer> atrIdAndCnt) {
        String checkImg = "0";
        atrIdsCheckboxImage.clear();
        atrIdCntInCategory.clear();
        Map<Integer, Boolean> atrIdInCategoryCheck2 = new HashMap<Integer, Boolean>();
        for (Map.Entry<Integer, PushButton> btn : atrIdsCheckboxBtns.entrySet()) {
            if (!atrIdAndCnt.containsKey(btn.getKey())) {
                btn.getValue().getUpFace().setImage(new Image("images/checkbox" /* I18N: no */ + checkImg + "." + "gif" /* I18N: no */));
            }
        }
        for (Map.Entry<Integer, Integer> m : atrIdAndCnt.entrySet()) {
            checkImg = m.getValue().toString();
            if (m.getValue() == 0) {
//                checkImg = "0";
            } else {
//                checkImg = m.getValue() == selectedIdCnt ? "1" : "2";
                int categoryId = getCategoryIdForItemId(m.getKey());
                int cnt = atrIdCntInCategory.get(categoryId) != null && checkImg.equals("1") ? atrIdCntInCategory.get(categoryId) + 1 : 1;
                atrIdCntInCategory.put(categoryId, cnt);
                if (checkImg.equals("2")/*
                         * m.getValue() == 2
                         */) {
                    atrIdInCategoryCheck2.put(categoryId, true);
                }
            }
            atrIdsCheckboxImage.put(m.getKey(), checkImg);
            atrIdsCheckboxBtns.get(m.getKey()).getUpFace().setImage(new Image("images/checkbox" /* I18N: no */ + checkImg + "." + "gif" /* I18N: no */));
        }
        setCategoryCheckboxes(atrIdInCategoryCheck2);
    }

    @Override
    protected void showDeleteItemConfirmDialog(int itemId) {
        // NO OP
    }

    @Override
    protected void createButtonsForCategory(HorizontalPanel wrapper, final int categoryId, final String categoryName) {
        // NO OP
    }

    @Override
    protected void createEditAndDeleteButtons(HorizontalPanel wrapper, final int itemId, final String attrName) {
        // NO OP
    }

    @Override
    public Widget buildWidgets() {
        vp = new VerticalPanel();

        main = new VerticalPanel();
        main.setWidth("100%");

        infoLabel = new HTML("<B><I>" + I18n.brakAtrybutowZaznaczWezel.get() /* I18N:  */ + "</I></B>");
        main.add(infoLabel);
        addButton();
        saveButton.setVisible(false);
        main.add(vp);
        return main;
    }

    protected void addButton() {
        saveButton = saveBtn();
        if (saveButton != null) {
            main.add(saveButton);
        }
    }

    @Override
    protected int getAttributeType() {
        return IS_BUILT_IN;
    }

    @Override
    public void displayData() {
        // NO OP
    }

    protected void showAddItemInCategoryDialog(final int categoryId) {
        //no op
    }
}
