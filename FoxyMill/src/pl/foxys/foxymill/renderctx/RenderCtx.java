/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.renderctx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import simplelib.BaseUtils;
import simplelib.LameRuntimeException;

/**
 *
 * @author wezyr
 */
public class RenderCtx implements IRenderCtx {

    private Object rootModel;
    private IOutputDevice outputDevice;
    private Map<String, Object> vars;
    protected List<Boolean> ifCondVals;

    public IOutputDevice getOutputDevice() {
        return outputDevice;
    }

    public Object getRootModel() {
        return rootModel;
    }

    public Map<String, Object> getVars() {
        return vars;
    }

    public void pushIfCondVal(boolean bVal) {
        if (ifCondVals == null) {
            ifCondVals = new ArrayList<Boolean>();
        }
        ifCondVals.add(bVal);
    }

    public boolean popIfCondVal() {
        return BaseUtils.popLastItem(ifCondVals);
    }

    @Deprecated
    public String getOutputStr() {
        if (outputDevice instanceof StringOutputDevice) {
            return ((StringOutputDevice) outputDevice).doneRendering();
        }
        throw new LameRuntimeException("outputDevice is not instance of class StringOutputDevice");
    }

    public static RenderCtx makeRootRenderCtx() {
        return makeRootRenderCtx(null, new HashMap<String, Object>());
    }

    public static RenderCtx makeRootRenderCtx(Object rootModel) {
        return makeRootRenderCtx(rootModel, new HashMap<String, Object>());
    }

    public static RenderCtx makeRootRenderCtx(Object rootModel, Map<String, Object> vars) {
        return makeRenderCtx(rootModel, vars, new StringOutputDevice());
//        RenderCtx res = new RenderCtx();
//        res.rootModel = rootModel;
//        res.outputDevice = new StringOutputDevice();
//        res.vars = vars;
//        return res;
    }

    public static RenderCtx makeRenderCtx(Object rootModel, Map<String, Object> vars,
            IOutputDevice outputDevice) {
        RenderCtx res = new RenderCtx();
        res.rootModel = rootModel;
        res.outputDevice = outputDevice;
        res.vars = vars;
        return res;
    }
}
