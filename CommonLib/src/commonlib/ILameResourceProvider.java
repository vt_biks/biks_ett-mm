/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import simplelib.IGenericResourceProviderEx;
import java.io.InputStream;

/**
 *
 * @author wezyr
 */
public interface ILameResourceProvider extends IGenericResourceProviderEx<InputStream> {
    // root path is "/", returned resource names are always full paths (i.e.
    // they always start with "/" and full base param as prefix)
    // fileExts == null -> all file names but no dirs
    // "/" in fileExts is for listing dirs
    // dirs always end with "/"
    // ---
    // resourceName must always start with "/"
//    public Set<String> getResourceNames(String base, Set<String> fileExts, boolean recurseSubDirs);
//
//    public InputStream gainResource(String resourceName);
//
//    public long getLastModifiedOfResource(String resourceName);
}
