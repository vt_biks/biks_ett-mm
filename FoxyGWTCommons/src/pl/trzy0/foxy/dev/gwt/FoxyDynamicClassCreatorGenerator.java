/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.dev.gwt;

import pl.trzy0.foxy.commons.annotations.CreatorPackage;
import pl.trzy0.foxy.commons.annotations.AlternativeSimpleName;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JGenericType;
import com.google.gwt.core.ext.typeinfo.JPackage;
import com.google.gwt.core.ext.typeinfo.JParameterizedType;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.trzy0.foxy.commons.annotations.DisableDynamicClassCreation;
import simplelib.BaseUtils;
import simplelib.IDynamicClassCreator;
import simplelib.IObjectCreator;
import simplelib.LameRuntimeException;
import pl.fovis.foxygwtcommons.i18n.I18n;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public class FoxyDynamicClassCreatorGenerator extends FoxyGeneratorBase {

    @Override
    protected void addImports() {
        super.addImports();
        addImport(LameRuntimeException.class);
        addImport(Map.class);
        addImport(Set.class);
        addImport(LinkedHashMap.class);
        addImport(IObjectCreator.class);
        addImport(GWT.class);
    }

    @Override
    protected void generateClassBody() throws UnableToCompleteException {
        CreatorPackage cp = genClassType.getAnnotation(CreatorPackage.class);
        String relativePath;
        boolean includeSubPackages;
        if (cp == null) {
            //throwUnableToComplete("class \"" + genClassName + "\" must be annotated with CreatorPackage");
            relativePath = null;
            includeSubPackages = true;
        } else {
            relativePath = cp.value();
            includeSubPackages = cp.includeSubPackages();
        }

        String packagePath = getPackagePath(relativePath);
        log("package path for class" /* I18N: no */+ " \"" + genClassName + "\" " +"is" /* I18N: no */+ " \"" + packagePath + "\"");

        JGenericType intfType = getClassType(IDynamicClassCreator.class).isGenericType();
        if (intfType == null) {
            throwUnableToComplete("cannot get dynamic class creator as generic type" /* I18N: no */);
        }

        JParameterizedType parametrization = genClassType.asParameterizationOf(intfType);
        if (parametrization == null) {
            throwUnableToComplete("cannot get parametrization of dynamic class creator for class" /* I18N: no */+ ": \""
                    + genClassName + "\"");
        }

        JClassType[] typeArgs = parametrization.getTypeArgs();

        if (typeArgs.length != 1) {
            throwUnableToComplete("parametrization for class" /* I18N: no */+ " \""
                    + genClassName
                    + "\" " +"has" /* I18N: no */+ " " + typeArgs.length + " " +"typeArgs, but should have just" /* I18N: no */+ " 1");
        }

        JClassType oneTypeArg = typeArgs[0];
        JGenericType oneArgGenericType = oneTypeArg.isGenericType();
        if (oneArgGenericType != null) {
            throwUnableToComplete("type arg of dynamic class creator for class" /* I18N: no */+ " \""
                    + genClassName
                    + "\" " +"is generic type" /* I18N: no */+ " \"" + oneTypeArg.getName() + "\"");
        }

        String classOfCreator = oneTypeArg.getParameterizedQualifiedSourceName();
        log("dynamic class creator" /* I18N: no */+ " \"" + genClassName
                + "\" " +"creates subclasses of" /* I18N: no */+ " \"" + classOfCreator + "\"");
        String mapClassParams = "<" +"String, IObjectCreator" /* I18N: no */+ "<" + classOfCreator + ">>";

        String implsPackageName = BaseUtils.dropOptionalPrefix(packagePath.replace("/", "."), ".");
        //TypeOracle typeOracle = genClassType.getOracle();
        JPackage implsPackage = typeOracle.findPackage(implsPackageName);
        if (implsPackage == null) {
            throwUnableToComplete("cannot find package for implementations" /* I18N: no */+ ": \""
                    + implsPackageName + "\"");
        }

        Map<String, String> typeNames = new LinkedHashMap<String, String>();
        addClassesFromPackage(implsPackage, oneTypeArg, typeNames);

        if (includeSubPackages) {
            JPackage[] pkgs = typeOracle.getPackages();
            String implsSubPackagePrefix = implsPackageName + ".";
            for (JPackage aPkg : pkgs) {
                if (aPkg.getName().startsWith(implsSubPackagePrefix)) {
                    addClassesFromPackage(aPkg, oneTypeArg, typeNames);
                }
            }
        }

        log("found classes in package" /* I18N: no */+ " \"" + implsPackageName + "\": " + typeNames);

        for (String typeName : typeNames.keySet()) {
            addImport(typeName);
        }

//        String genSimpleClassName = BaseUtils.extractSimpleClassName(genClassName);
//        println("private static final " + genSimpleClassName + " impl = GWT.create(" + genSimpleClassName + ".class)");
//        println("public " + genSimpleClassName + " impl() { return impl; }");

        println("private static final Map" /* I18N: no */ + mapClassParams + " " +"creators = new LinkedHashMap" /* I18N: no */
                + mapClassParams + "();");

        println("static" /* I18N: no */+ " {");
        println("IObjectCreator<" + classOfCreator + "> " +"creator" /* I18N: no */+ ";");
        for (Entry<String, String> e : typeNames.entrySet()) {
            String typeName = e.getKey();
            String simpleNamesStr = e.getValue();
            Collection<String> simpleNames = BaseUtils.splitBySep(simpleNamesStr, ",", true);
            println("creator = new IObjectCreator" /* I18N: no */+ "<" + classOfCreator + ">() {" +""
                    + "public" /* I18N: no */+ " " + classOfCreator + " " +"create() { return new" /* I18N: no */+ " " + typeName
                    + "(); } };");
            for (String simpleName : simpleNames) {
                //String simpleName = BaseUtils.getLastItems(typeName, ".", 1);
                println("creators.put" /* I18N: no */+ "(\"" + simpleName + "\", " +"creator" /* I18N: no */+ ");");
            }
        }
        println("}");

        println(""
                    + "public" /* I18N: no */+ " " + classOfCreator + " " +"create(String name) { "
                + "IObjectCreator" /* I18N: no */+ "<" + classOfCreator + "> " +"creator = creators.get(name);"
                + "if (creator == null" /* I18N: no */+ ") " + getThrowStmt("no creator for name" /* I18N: no */+ " \"", "name" /* I18N: no */, "\"")
                + "return creator.create" /* I18N: no */+ "(); }");
        println("public Set<String> getNames() { return creators.keySet" /* I18N: no */+ "(); }");
    }

    private void addClassesFromPackage(JPackage pkg, JClassType superClassType,
            Map<String, String> classNames) {
        JClassType[] typesInPkg = pkg.getTypes();
        int cnt = 0;
        for (JClassType typeInPkg : typesInPkg) {
            DisableDynamicClassCreation ddcc = typeInPkg.getAnnotation(DisableDynamicClassCreation.class);
            if (ddcc == null && !typeInPkg.isAbstract() && typeInPkg.isAssignableTo(superClassType)) {
                if (typeInPkg.isDefaultInstantiable()) {

                    String simpleName = typeInPkg.getSimpleSourceName();

                    AlternativeSimpleName asn = typeInPkg.getAnnotation(AlternativeSimpleName.class);
                    if (asn != null) {
                        String altSimpleName = asn.value();
                        if (asn.replacesDefaultName()) {
                            simpleName = altSimpleName;
                        } else {
                            simpleName += "," + altSimpleName;
                        }
                    }

                    classNames.put(typeInPkg.getQualifiedSourceName(), simpleName);
                    cnt++;
                } else {
                    logWarn("class" /* I18N: no */+ " " + typeInPkg.getSimpleSourceName() + " " +"is assignable to" /* I18N: no */+ " "
                            + superClassType.getSimpleSourceName() + ", " +"but has no default constructor, cannot be instantiated dynamically. to suppress this warning annotate class with @DisableDynamicClassCreation" /* I18N: no */);
                }
            }
        }
        log("addClassesFromPackage(\"" + pkg.getName() + "\"): " +"added" /* I18N: no */+ " " + cnt
                + " " +"class(es) of total" /* I18N: no */+ " " + typesInPkg.length + " " +"class(es) in this package" /* I18N: no */);
    }
}