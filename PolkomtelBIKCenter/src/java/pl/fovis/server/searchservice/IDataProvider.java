/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.searchservice;

import java.util.Date;
import java.util.List;
import org.apache.lucene.document.Document;

/**
 *
 * @author ctran
 */
public interface IDataProvider {

    public List<Document> next();

    public void restart(Date indexFromDate, Date indexToDate);
}
