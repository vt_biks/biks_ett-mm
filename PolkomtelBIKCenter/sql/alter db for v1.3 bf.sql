﻿exec sp_check_version '1.3';
go

create table bik_translation(
id int identity(1,1) NOT NULL primary key,
code varchar(890)  not null,
txt varchar(max) not null,
lang varchar(3),
kind varchar(5),
constraint UQ_bik_code_language_kind unique (code,lang,kind)
);

exec sp_update_version '1.3', '1.3.0.1';
go

