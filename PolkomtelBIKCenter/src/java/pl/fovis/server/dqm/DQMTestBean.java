/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.dqm;

import java.util.Date;

/**
 *
 * @author tflorczak
 */
public class DQMTestBean implements Cloneable {

    public int id;
    public int testNodeId;
    public Integer procedureNodeId;
    public Integer errorProcedureNodeid;
    public String procedureName;
    public String errorProcedureName;
    public String testSql;
    public String errorTestSql;
    public String resultFileName;
    public Integer dbServerId;
    public Integer errorDbServerId;
    public String sourceName;
    public String functionDatabase;
    public String queryDatabase;
    public String errorFunctionDatabase;
    public Double errorThreshold;

    public Date optStartedDate;
//    public String optObjsForGrantsTesting;
    public String optTestGrantsPerObjects;

    // pola dla wersji z połączeniami
    public Integer connectionId;
    public String connectionName;

    public DQMTestBean() {
    }

    @Override
    public DQMTestBean clone() {
        try {
            return (DQMTestBean) super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException("what a surprise! not cloneable?", ex);
        }
    }

    @Override
    public String toString() {
        return "DQMTestBean{" + "id=" + id + ", testNodeId=" + testNodeId + ", procedureNodeId=" + procedureNodeId + ", errorProcedureNodeid=" + errorProcedureNodeid + ", procedureName=" + procedureName + ", errorProcedureName=" + errorProcedureName + ", testSql=" + testSql + ", errorTestSql=" + errorTestSql + ", resultFileName=" + resultFileName + ", dbServerId=" + dbServerId + ", errorDbServerId=" + errorDbServerId + ", sourceName=" + sourceName + ", functionDatabase=" + functionDatabase + ", queryDatabase=" + queryDatabase + ", errorFunctionDatabase=" + errorFunctionDatabase + ", errorThreshold=" + errorThreshold + ", optStartedDate=" + optStartedDate + ", optTestGrantsPerObjects=" + optTestGrantsPerObjects + '}';
    }

}
