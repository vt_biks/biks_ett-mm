﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- MSSQL - pobieranie dodatkowych opisów a la RedGate
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_check_version '1.6.y.6';
go

-- poprzednia wersja w pliku alter db for v1.2.4 tf.sql
-- poprawienie / optymalizacja procedury
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_mssql]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_mssql]
go

create procedure [dbo].[sp_insert_into_bik_node_mssql]
as
begin

	declare @mssql_tree_id int;
	select @mssql_tree_id = id from bik_tree where code='MSSQL'

	-- update
	update bik_node 
	set name = mssql.name, node_kind_id = bnk.id, is_deleted = 0, descr = mssql.extra_info, visual_order = mssql.visual_order
	from bik_mssql mssql
	inner join bik_node_kind bnk on mssql.type = bnk.code
	where bik_node.tree_id = @mssql_tree_id
	and bik_node.obj_id = mssql.branch_names
	and bik_node.linked_node_id is null

	-- insert nowych
	insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id, visual_order)
	select null, bnk.id, ms.name, ms.extra_info, @mssql_tree_id, ms.branch_names, ms.visual_order
	from bik_mssql ms
	inner join bik_node_kind bnk on ms.type = bnk.code
	left join (bik_node bn inner join bik_tree bt on bn.tree_id = bt.id and bn.tree_id = @mssql_tree_id) on bn.obj_id = ms.branch_names
	where bn.id is null
	order by ms.branch_names

	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join bik_mssql as ms on bik_node.obj_id = ms.branch_names
	inner join bik_node bk on bk.obj_id = ms.parent_branch_names 
	where bik_node.tree_id = @mssql_tree_id
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.tree_id = @mssql_tree_id
	and bk.is_deleted = 0
	and bk.linked_node_id is null

	-- usunięcie zbędnych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join bik_mssql on bik_node.obj_id = bik_mssql.branch_names
	where bik_mssql.name is null
	and tree_id = @mssql_tree_id
	and is_deleted = 0
	and linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @mssql_tree_id 
	and bn2.is_deleted = 1

	exec sp_node_init_branch_id @mssql_tree_id,null

end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_mssql_index') and type in (N'U'))
begin
	create table aaa_mssql_index (
		id int not null identity(1,1) primary key,
		name sysname null,
		server varchar(500) null,
		owner sysname not null,
		database_name sysname not null,
		table_name sysname not null,
		column_name sysname not null,
		column_position int null,
		is_unique int null,
		type varchar(50) null,
		is_primary_key int not null default(0),
		partition_ordinal int not null default(0),
		partition_schema varchar(100) null,
		partition_function varchar(100) null,
		partition_function_type varchar(100) null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_mssql_index') and type in (N'U'))
begin
	create table bik_mssql_index (
		id int not null identity(1,1) primary key,
		table_node_id int not null references bik_node(id),
		column_node_id int not null references bik_node(id),
		server varchar(500) null,
		column_name sysname not null,
		name sysname null,
		column_position int not null,
		is_unique int null,
		type varchar(50) null,
		is_primary_key int not null default(0),
		partition_ordinal int not null default(0),
		partition_schema varchar(100) null,
		partition_function varchar(100) null,
		partition_function_type varchar(100) null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_mssql_foreign_key') and type in (N'U'))
begin
	create table aaa_mssql_foreign_key (
		id int not null identity(1,1) primary key,
		name sysname not null,
		description varchar(500) null,
		server varchar(500) null,
		database_name sysname not null,
		fk_table sysname not null,
		fk_schema sysname not null,
		fk_column sysname not null,
		pk_table sysname not null,
		pk_schema sysname not null,
		pk_column sysname not null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_mssql_foreign_key') and type in (N'U'))
begin
	create table bik_mssql_foreign_key (
		id int not null identity(1,1) primary key,
		name sysname not null,
		description varchar(500) null,
		server varchar(500) not null,
		fk_table sysname not null,
		fk_table_node_id int not null references bik_node(id),
		fk_column sysname not null,
		fk_column_node_id int not null references bik_node(id),
		pk_table sysname not null,
		pk_table_node_id int not null references bik_node(id),
		pk_column sysname not null,
		pk_column_node_id int not null references bik_node(id)
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_mssql_column_extradata') and type in (N'U'))
begin
	create table aaa_mssql_column_extradata (
		id int not null identity(1,1) primary key,
		name sysname not null,
		owner sysname not null,
		table_name sysname not null,
		database_name sysname not null,
		server_name sysname not null,
		column_id int not null,
		description varchar(500) null,
		is_nullable int not null,
		type varchar(100) not null,
		default_field varchar(512) null,
		seed_value int null,
		increment_value int null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_mssql_column_extradata') and type in (N'U'))
begin
	create table bik_mssql_column_extradata (
		id int not null identity(1,1) primary key,
		name sysname not null,
		server varchar(500) null,
		table_node_id int not null references bik_node(id),
		column_node_id int not null references bik_node(id),
		column_id int not null,
		description varchar(500) null,
		is_nullable int not null,
		type varchar(100) not null,
		default_field varchar(512) null,
		seed_value int null,
		increment_value int null
	);
end;
go

if not exists (select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[bik_mssql_foreign_key]') and name = N'idx_bik_mssql_foreign_key_fk_table_node_id')
begin
	create index idx_bik_mssql_foreign_key_fk_table_node_id on [dbo].[bik_mssql_foreign_key](fk_table_node_id);
end;
go

if not exists (select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[bik_mssql_foreign_key]') and name = N'idx_bik_mssql_foreign_key_fk_column_node_id')
begin
	create index idx_bik_mssql_foreign_key_fk_column_node_id on [dbo].[bik_mssql_foreign_key](fk_column_node_id);
end;
go

if not exists (select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[bik_mssql_column_extradata]') and name = N'idx_bik_mssql_column_extradata_table_node_id')
begin
	create index idx_bik_mssql_column_extradata_table_node_id on [dbo].[bik_mssql_column_extradata](table_node_id);
end;
go

if not exists (select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[bik_mssql_column_extradata]') and name = N'idx_bik_mssql_column_extradata_column_node_id')
begin
	create index idx_bik_mssql_column_extradata_column_node_id on [dbo].[bik_mssql_column_extradata](column_node_id);
end;
go

if not exists (select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[bik_mssql_index]') and name = N'idx_bik_mssql_index_table_node_id')
begin
	create index idx_bik_mssql_index_table_node_id on [dbo].[bik_mssql_index](table_node_id);
end;
go

if not exists (select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[bik_mssql_index]') and name = N'idx_bik_mssql_index_column_node_id')
begin
	create index idx_bik_mssql_index_column_node_id on [dbo].[bik_mssql_index](column_node_id);
end;
go

if not exists (select * from bik_node_kind where code = 'MSSQLColumnPK')
begin
	insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank,is_leaf_for_statistic)
	values ('MSSQLColumnPK', 'Kolumna MSSQL PK', 'mssqlColumnPK', 'Metadata', 0, 7, 0)
end;

if not exists (select * from bik_node_kind where code = 'MSSQLColumnFK')
begin
	insert into bik_node_kind(code,caption,icon_name,tree_kind,is_folder,search_rank,is_leaf_for_statistic)
	values ('MSSQLColumnFK', 'Kolumna MSSQL FK', 'mssqlColumnFK', 'Metadata', 0, 7, 0)
end;

-- tranlate for node kinds
if not exists (select * from bik_translation where code = 'MSSQLColumnPK')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('MSSQLColumnPK', 'MSSQL column primary key', 'en', 'kind')
end;

if not exists (select * from bik_translation where code = 'MSSQLColumnFK')
begin
	insert into bik_translation(code, txt, lang, kind)
	values('MSSQLColumnFK', 'MSSQL column foreign key', 'en', 'kind')
end;

-- dodanie do modrzewia
declare @treeOfTreesId int = dbo.fn_tree_id_by_code('TreeOfTrees');
if not exists (select * from bik_node where tree_id = @treeOfTreesId and obj_id = '&MSSQLColumnPK')
begin
	--declare @treeOfTreesId int = dbo.fn_tree_id_by_code('TreeOfTrees');

	exec sp_add_menu_node '&MSSQLTable', '@', '&MSSQLColumnPK'
	exec sp_add_menu_node '&MSSQLTable', '@', '&MSSQLColumnFK'

	declare @mssqlDBNode int;
	declare @mssqlOwnerNode int;
	select @mssqlDBNode = id from bik_node where tree_id = @treeOfTreesId and obj_id = '&MSSQLDatabase' and is_deleted = 0
	select @mssqlOwnerNode = id from bik_node where tree_id = @treeOfTreesId and obj_id = '&MSSQLOwner' and is_deleted = 0

	update bik_node
	set parent_node_id = @mssqlDBNode
	where parent_node_id = @mssqlOwnerNode

	update bik_node
	set is_deleted = 1
	where id = @mssqlOwnerNode

	exec sp_node_init_branch_id @treeOfTreesId, null
end;


exec sp_update_version '1.6.y.6', '1.6.y.7';
go
