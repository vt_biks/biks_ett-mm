/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.FileUploadDialogBase;

/**
 *
 * @author tflorczak
 */
public class FileUploadDialogSimple extends FileUploadDialogBase {

    @Override
    protected String getDialogCaption() {
        return I18n.importuj.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.importuj.get();
    }

//    @Override
//    protected void buildMainWidgets() {
//        ffu = new FoxyFileUpload(new IContinuation() {
//            @Override
//            public void doIt() {
//                hideDialog();
//                saveCont.doIt(ffu);
//            }
//        }, false);
//        main.add(ffu);
//    }
//
//    public void buildAndShowDialog(IParametrizedContinuation<FoxyFileUpload> saveCont) {
//        this.saveCont = saveCont;
//        super.buildAndShowDialog();
//    }
//
//    @Override
//    protected void doAction() {
//        //no-op
//    }
//
//    @Override
//    protected boolean doActionBeforeClose() {
//        ffu.performUpload();
//        return false;
//    }
}
