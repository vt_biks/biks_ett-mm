/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import java.util.List;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.common.JoinedObjsTabKind;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.JoinedObjBean;

/**
 *
 * @author tflorczak
 */
public class ConfluenceWidgetFlat extends JoinedObjWidgetFlatBase<JoinedObjBean> {

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_CONFLUENCE;
    }

    @Override
    protected List<JoinedObjBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
        return cddb.jobs.get(JoinedObjsTabKind.Confluence);
    }

    public String getCaption() {
        return BIKGWTConstants.MENU_NAME_CONFLUENCE;
    }
}
