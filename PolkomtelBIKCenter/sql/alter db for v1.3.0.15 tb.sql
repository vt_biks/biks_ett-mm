﻿exec sp_check_version '1.3.0.15';
go

insert into bik_admin (param, value) values('lisateradata.url', '192.168.167.75');
go

insert into bik_admin (param, value) values('lisateradata.port', '');
go

insert into bik_admin (param, value) values('lisateradata.database', 'DBC');
go

insert into bik_admin (param, value) values('lisateradata.charset', 'UTF8');
go

insert into bik_admin (param, value) values('lisateradata.tmode', 'ANSI');
go

exec sp_update_version '1.3.0.15', '1.3.0.16';
go