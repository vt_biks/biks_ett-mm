/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.rpc.AsyncCallback;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.bssg.metadatapump.common.ConnectionParametersDBServerBean;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class AdminConfigOracleWidget extends AdminConfigDBBaseWidget {

    @Override
    protected boolean isGrantsTestPerObjectAvailable() {
        return BIKClientSingletons.showGrantsInTestConnection();
    }

    @Override
    protected void callServiceToSaveData(ConnectionParametersDBServerBean bean, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().setOracleConnectionParameters(bean, asyncCallback);
    }

    @Override
    protected void callServiceToTestConnection(int id, AsyncCallback<Pair<Integer, String>> asyncCallback) {
        BIKClientSingletons.getService().runOracleTestConnection(id, getOptObjsForGrantsTesting(), asyncCallback);
    }

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_ORACLE;
    }

    @Override
    protected String getInstanceText() {
        return "SID / Service" /* i18n: no */ + ":";
    }

    @Override
    protected String getTreeCodeForServer() {
        return BIKConstants.TREE_CODE_ORACLE;
    }

    @Override
    protected boolean isAdditionalDatabaseFilterAvailable() {
        return true;
    }

    @Override
    protected boolean isNameEditable() {
        return false;
    }

    @Override
    protected boolean needSeparateSchedule() {
        return true;
    }
}
