/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.entitydetailswidgets.IGridBeanAction;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.NoteBean;
import simplelib.BaseUtils;

/**
 *
 * @author beata
 */
public class NoteBeanExtractor extends NodeAwareBeanExtractorBase<NoteBean> {

    public NoteBeanExtractor() {
        super(false);
    }

    @Override
    public boolean hasIcon() {
        return false;
    }

    @Override
    protected String getNodeKindCode(NoteBean b) {
        return null;
    }

    protected String getName(NoteBean b) {
        return null;
    }

    public Integer getNodeId(NoteBean b) {
        return b.nodeId;
    }

    public String getTreeCode(NoteBean b) {
        return b.nodeTreeCode;
    }

    public String getBranchIds(NoteBean b) {
        return null;
    }

    @Override
    public int getAddColCnt() {
        return 3;
    }

    @Override
    public Object getAddColVal(NoteBean b, int addColIdx) {

        Label nodeNameAtNote = new Label(b.nodeName);
        nodeNameAtNote.setStyleName("gp-label");
        Map<Integer, Object> m = new HashMap<Integer, Object>();
        m.put(0, createSubjectAndBody(b.title, b.body));
        m.put(1, nodeNameAtNote);
        m.put(2, createDateAdded(b.dateAdded, "dqcMyBik-dateAdded"));

        return m.get(addColIdx);
    }

    @Override
    protected List<IGridBeanAction<NoteBean>> createActionList() {
        List<IGridBeanAction<NoteBean>> res = new ArrayList<IGridBeanAction<NoteBean>>();
        res.add(actFollow);
        return res;
    }

    protected FlowPanel createSubjectAndBody(String title, String body) {
        FlowPanel innerOneNoteEntryFp = new FlowPanel();
        HTML lab = new HTML("<b>" + BaseUtils.encodeForHTMLTag(title) + "</b>");
        lab.setStyleName("Notes" /* i18n: no:css-class */ + "-noteTitle");
        innerOneNoteEntryFp.add(lab);
        if (!BaseUtils.isStrEmptyOrWhiteSpace(body)) {
            HTML labBody = new HTML();
            labBody.setStyleName("Notes" /* i18n: no:css-class */ + "-labBody");
            labBody.setHTML(BaseUtils.encodeForHTMLTag(body));
            innerOneNoteEntryFp.add(labBody);
        }
        return innerOneNoteEntryFp;
    }

    @Override
    public int howManyTimesNameColumnWiderThanOther() {
        return 4;
    }
}
