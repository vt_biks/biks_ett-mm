/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.BIKSProgressInfoDialog;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import pl.fovis.foxygwtcommons.dialogs.OneTextBoxDialog;
import pl.fovis.foxygwtcommons.dialogs.SimpleInfoDialog;
import simplelib.BeanWithIntIdAndNameBase;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.IParametrizedContinuationWithReturn;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 * @param <T> list of ConnectionParametersBean (N)
 * @param <N> ConnectionParametersBean with id
 */
public abstract class AdminConfigManyServersBase<T extends Collection<N>, N extends BeanWithIntIdAndNameBase> extends AdminConfigBIKBaseWidget<T> {

    protected ListBox listbox;
    protected Map<String, N> serversMap;
    protected Set<String> serverNames;
    protected VerticalPanel vpLeft;
    protected VerticalPanel vpRight;
    protected HorizontalPanel hpDown;
//    protected HorizontalPanel hpTop;
//    protected HorizontalPanel hpOptTop;

    protected void selectFirstServer() {
        if (listbox.getItemCount() > 0) {
            listbox.setItemSelected(0, true);
            vpRight.setVisible(true);
            activateForServer(listbox.getValue(0));
            resultTest.setText("");
        } else {
            vpRight.setVisible(false);
        }
    }

    protected int getSelectedIndex() {
        return listbox.getSelectedIndex();
    }

    protected String getValueForSelectedIndex() {
        return listbox.getValue(getSelectedIndex());
    }

    @Override
    public void buildInnerWidget() {
        serversMap = new HashMap<String, N>();
        listbox = new ListBox();
        listbox.setVisibleItemCount(10);
        listbox.setWidth("300px");
        listbox.setStyleName("rightMargin");
        vpLeft = new VerticalPanel();
        hpDown = new HorizontalPanel();
        vpRight = new VerticalPanel();
        vpRight.setSpacing(2);
//        hpTop = new HorizontalPanel();
//        hpTop.setSpacing(10);
//        hpOptTop = new HorizontalPanel();
//        hpOptTop.setSpacing(10);
        serverNames = new HashSet<String>();

        // left
        vpLeft.add(new Label(getListBoxName()));
        createLine(vpLeft, "300");
        vpLeft.add(listbox);
        createLine(vpLeft, "300");
        createButton(I18n.dodaj.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                new OneTextBoxDialog().buildAndShowDialog(getTextToShowOnDialogAfterClickAddButton(), "", new IParametrizedContinuation<String>() {
                    @Override
                    public void doIt(final String param) {
                        if (serverNames.contains(param)) {
                            new SimpleInfoDialog().buildAndShowDialog(I18n.istniejeTakiSerwer.get(), null, null);
                        } else {
                            callServiceToAddNewServer(getSourceName(), param, needSeparateSchedule(), new StandardAsyncCallback<N>("Error in" /* I18N: no */ + " addConfig") {
                                        @Override
                                        public void onSuccess(N result) {
                                            String idAsString = String.valueOf(result.id);
                                            listbox.addItem(param, idAsString);
                                            serversMap.put(idAsString, result);
                                            serverNames.add(param);
                                            doOptActionAfterAddServer(param);
                                            selectFirstServer();
                                            BIKClientSingletons.showInfo(I18n.serwerZostalDodany.get());
                                        }
                                    });
                        }
                    }
                });
            }
        }, vpLeft);
        createLine(vpLeft, "300");
        createButton(I18n.usun.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
//                if (listbox.getItemCount() == 1 || listbox.getItemCount() == 0) {
//                    new SimpleInfoDialog().buildAndShowDialog("You can not remove the last server" /* I18N: no */ + "!", null, null);
//                } else {
                ConfirmDialog confirm = new ConfirmDialog();
                confirm.buildAndShowDialog(I18n.czyNaPewnoChceszUsunacSerwer.get(), I18n.usun.get(), new IContinuation() {

                    @Override
                    public void doIt() {
                        final BIKSProgressInfoDialog infoDialog = new BIKSProgressInfoDialog();
                        infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaUsuwanieSerwera.get(), true);
                        callServiceToDeleteServer(Integer.parseInt(getValueForSelectedIndex()), listbox.getItemText(getSelectedIndex()), new AsyncCallback<Void>() {
                            @Override
                            public void onSuccess(Void result) {
                                infoDialog.hideDialog();
                                serverNames.remove(listbox.getItemText(getSelectedIndex()));
                                serversMap.remove(getValueForSelectedIndex());
                                listbox.removeItem(getSelectedIndex());
                                selectFirstServer();
                                BIKClientSingletons.showInfo(I18n.usunietoSerwer.get());
                            }

                            @Override
                            public void onFailure(Throwable caught) {
                                infoDialog.hideDialog();
                                BIKClientSingletons.showError("Error in delete server" /* I18N: no */);
                            }
                        });
                    }
                });
//                }
            }
        }, vpLeft);
        listbox.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                activateForServer(getValueForSelectedIndex());
                resultTest.setText("");
            }
        });
//        buildTop(hpTop);
//        buildOptTop(hpOptTop);
        buildRightWidget(vpRight);
        hpDown.add(vpLeft);
        hpDown.add(vpRight);

//        main.add(hpTop);
//        main.add(hpOptTop);
//        createLine(main, "828px");
        main.add(hpDown);
    }

    protected abstract void activateForServer(String id);

    protected abstract void buildRightWidget(CellPanel panelRight);

    protected void buildTop(CellPanel panelTop) {
        //no op
    }

    protected void buildOptTop(CellPanel panelTop) {
        //no op
    }

    protected void doOptActionAfterAddServer(String serverName) {
        //no op
    }

    protected boolean needSeparateSchedule() {
        return false;
    }

    protected abstract void callServiceToAddNewServer(String source, String serverName, boolean needSeparateSchedule, AsyncCallback<N> asyncCallback);

    protected abstract void callServiceToDeleteServer(int id, String serverName, AsyncCallback<Void> asyncCallback);

    protected abstract void callServiceToTestConnection(int id, AsyncCallback<Pair<Integer, String>> asyncCallback);

    protected String getListBoxName() {
        return I18n.wybierzServerDoSkonfigurowania.get();
    }

    protected void createTestConnectionWidget(CellPanel panel, final IParametrizedContinuationWithReturn<Void, Integer> param) {
        createTestConnectionWidget(I18n.testujPolaczenie.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                callServiceToTestConnection(param.doIt(null), new StandardAsyncCallback<Pair<Integer, String>>("Error in" /* I18N: no */ + " runTestConnection: " + getSourceName()) {
                            @Override
                            public void onSuccess(Pair<Integer, String> result) {
                                setResultTestConnection(result);
                            }
                        });
            }
        }, panel);
    }

    @Override
    public void populateWidgetWithData(T data) {
        serversMap.clear();
        listbox.clear();
        serverNames.clear();
        for (N bean : data) {
            serversMap.put(String.valueOf(bean.id), bean);
            listbox.addItem(bean.name, String.valueOf(bean.id));
            serverNames.add(bean.name);
        }
        selectFirstServer();
    }
    protected String getTextToShowOnDialogAfterClickAddButton(){
        return I18n.nazwaSerwera.get();
    }
}
