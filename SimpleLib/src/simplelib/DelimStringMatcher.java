/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author pmielanczuk
 */
public class DelimStringMatcher implements IStringMatcher {

    protected String s;
    protected int sLen;
    protected String leftDelim;
    protected int leftDelimLen;
    protected String rightDelim;
    protected int rightDelimLen;
    protected int startPos = -1;
    protected int endPos = -1;

    public DelimStringMatcher(String s, String leftDelim, String rightDelim) {
        this.s = s;
        this.sLen = s.length();
        this.leftDelim = leftDelim;
        this.leftDelimLen = leftDelim.length();
        this.rightDelim = rightDelim;
        this.rightDelimLen = rightDelim.length();
    }

    protected boolean innerFind(int fromPos) {
        startPos = s.indexOf(leftDelim, fromPos);

        if (startPos < 0) {
            startPos = sLen;
            return false;
        }

        endPos = s.indexOf(rightDelim, startPos + leftDelimLen);
        if (endPos < 0) {
            startPos = sLen;
            return false;
        } else {
            endPos += rightDelimLen;
        }

        return true;
    }

    public boolean find() {
        if (startPos >= sLen) {
            return false;
        }

        return innerFind(startPos == -1 ? 0 : endPos + rightDelimLen);
    }

    public int start() {
        checkProperState();
        return startPos;
    }

    protected void checkProperState() {
        if (startPos < 0 || endPos < 0 || startPos >= sLen) {
            throw new IllegalStateException("no call of find() or it returned false");
        }
    }

    public int end() {
        checkProperState();
        return endPos;
    }

    public String group(int idx) {
        checkProperState();
        if (idx == 0) {
            return s.substring(startPos, endPos);
        }
        if (idx == 1) {
            return s.substring(startPos + leftDelimLen, endPos - rightDelimLen);
        }
        throw new IllegalStateException("incorrect group number: " + idx);
    }

    public int getStringLength() {
        return sLen;
    }

    public int getLeftDelimLength() {
        return leftDelimLen;
    }

    public int getRightDelimLength() {
        return rightDelimLen;
    }

    public boolean find(int startPos) {
        return innerFind(endPos);
    }
}
