exec sp_check_version '1.5.x.1';
go
-----[LISA] naprawa bledu - jesli zaden slownik nie byl importowany, to wezel glowny slownikow
----- nie ma zainicjowanego branchIds

declare @treeId int = dbo.fn_tree_id_by_code('DictionaryDWH')

exec sp_node_init_branch_id @treeId, null

exec sp_update_version '1.5.x.1', '1.5.x.2';
go
