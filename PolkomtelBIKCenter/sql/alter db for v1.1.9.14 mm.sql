﻿exec sp_check_version '1.1.9.14';
go

delete from bik_user_right

declare @admin_id int = (select id from bik_right_role where code = 'Administrator')
declare @expert_id int = (select id from bik_right_role where code = 'Expert')

insert into bik_user_right  
select bsu.id as user_id, @admin_id as right_id
from bik_system_user bsu
where bsu.is_admin = 1


insert into bik_user_right  
select bsu.id as user_id, @expert_id as right_id
from bik_system_user bsu
where bsu.is_expert = 1

exec sp_update_version '1.1.9.14', '1.1.9.15';
go