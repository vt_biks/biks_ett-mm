/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.fovis.foxygwtcommons;

import java.util.Set;

/**
 *
 * @author wezyr
 */
public interface IFoxyResourceBoundle {
    public String get(String name);
    public Set<String> getNames();
}
