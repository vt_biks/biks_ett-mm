/*
 * PropertyProjector.java
 *
 * Created on 9 luty 2007, 11:54
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib;

import simplelib.BaseUtils.Projector;

/**
 *
 * @author wezyr
 */
public class PropertyProjector<V, T> implements Projector<V, T> {
    
    private String propName;
    
    public PropertyProjector(String propName) {
        this.propName = propName;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public T project(V val) {
        return (T)MuppetMerger.getProperty(val, propName);
    }
}
