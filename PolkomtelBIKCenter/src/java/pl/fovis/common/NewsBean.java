/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author mmastalerz
 */
public class NewsBean implements Serializable, IsSerializable {

    public int id;
    public String title;
    public String text;
    public String authorName;
    public int authorId;
    public Date dateAdded;
    public boolean isRead;
}
