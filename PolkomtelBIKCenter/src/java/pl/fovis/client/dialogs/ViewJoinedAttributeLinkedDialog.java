/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.List;
import pl.fovis.common.JoinedObjAttributeLinkedBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;

/**
 *
 * @author bfechner
 */
public class ViewJoinedAttributeLinkedDialog extends BaseActionOrCancelDialog {

    protected String nameFrom;
    protected String nameTo;
    protected List<JoinedObjAttributeLinkedBean> joinedObjAttributeLinked;

    public void buildAndShowDialog(String nameFrom, String nameTo, List<JoinedObjAttributeLinkedBean> joinedObjAttributeLinked) {
        this.nameFrom = nameFrom;
        this.nameTo = nameTo;
        this.joinedObjAttributeLinked = joinedObjAttributeLinked;
        super.buildAndShowDialog(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getDialogCaption() {
        return I18n.atrybuty.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return null;
    }

    @Override
    protected void buildMainWidgets() {

        main.add(getCaption());
        main.setWidth("100%");
        FlexTable ft = new FlexTable();
        ft.setWidth("100%");
        ft.getColumnFormatter().setWidth(0, "250px");
        ft.setStyleName("gridJoinedObj");
        int row = 0;

//        ft.setWidget(row, 0, new HTML(BaseUtils.capitalize(I18n.atrybut.get())));
//        ft.setWidget(row, 1, new HTML(I18n.wartoscDlaWezlaZrodlowego.get()));
//        ft.setWidget(row, 2, new HTML(""));
//        ft.getRowFormatter().setStyleName(0, "RelatedObj-oneObj-dqc");
//        row++;

        for (JoinedObjAttributeLinkedBean jo : joinedObjAttributeLinked) {
            ft.setWidget(row, 0, new HTML("<b>" + jo.name + ": </b>"));
            Label valueLbl = new Label(jo.value);
            valueLbl.setStyleName("gp-html");
            ft.setWidget(row, 1, valueLbl);
            ft.setWidget(row, 2, new Label(jo.isMain ? "(" + I18n.atrybutGlowny.get() + ")" : ""));
            ft.getRowFormatter().setStyleName(row, row % 2 == 0 ? "RelatedObj-oneObj-white" : "RelatedObj-oneObj-grey");
            ft.getCellFormatter().setStyleName(row, 0, row % 2 == 0 ? "RelatedObj-oneObj-white-top" : "RelatedObj-oneObj-grey-top");

            row++;
        }
        main.add(ft);
    }

    protected VerticalPanel getCaption() {
        VerticalPanel captionVp = new VerticalPanel();
        captionVp.setStyleName("attributeJoinedObjs");
        HTML title = new HTML(I18n.atrybutyDoPowiazania.get());
        HTML joinHtml = new HTML("<br/><b>" + nameFrom + "</b>" + " i " + "<b>" + nameTo + "</b>");
        captionVp.add(title);
        captionVp.add(joinHtml);
        return captionVp;
    }

    @Override
    protected void doAction() {
        //no-op
    }

}
