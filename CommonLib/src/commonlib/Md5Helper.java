/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author witzar
 */
public class Md5Helper {

    //private static final Log log = LogFactory.getLog(Md5Helper.class);
    public static String hexMd5Digest(String input, int maxLength) {
        String stringOut = hexMd5Digest(input);
        return (stringOut.length() <= maxLength) ? stringOut : stringOut.substring(0, maxLength);
    }

    public static String hexMd5Digest(String... args) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            if (args[i] != null) {
                sb.append(args[i]);
            }
        }
        return hexMd5Digest(sb.toString());
    }

    public static String hexMd5Digest(String input) {
        byte[] bytesOut = md5Digest(input.getBytes(Charset.forName("UTF-8")));
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < bytesOut.length; i++) {
            String hex = Integer.toHexString(0xFF & bytesOut[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    private static byte[] md5Digest(byte[] input) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            //log.fatal(ex);
            throw new Error("no md5", ex);
        }
        md.reset();
        md.update(input);
        byte[] bytesOut = md.digest();
        return bytesOut;
    }
}
