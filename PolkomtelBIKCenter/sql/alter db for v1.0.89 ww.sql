exec sp_check_version '1.0.89';
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

exec sp_set_app_prop 'search_order_by_exprs', 'bn.max_search_weight desc, bn.search_rank + bnk.search_rank desc, bn.vote_sum desc, sum_combined_rank desc'
go

-----------

update bik_node_kind set search_rank = 10 where code = 'Glossary'

update bik_node_kind set search_rank = 9 where code = 'GlossaryNotCorpo'

update bik_node_kind set search_rank = 8 where code = 'GlossaryVersion'
go


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

insert into bik_node_kind (code,caption,icon_name,tree_kind,is_folder,search_rank)
values ('Question','Pytanie','question','FAQ',0,0)

update bik_node_kind set icon_name='questionfolder' where code='CategoriesOfQuestions'

update bik_node set descr=bm.body
from bik_node bn
join bik_metapedia bm on bn.id=bm.node_id
where parent_node_id=(select bn.id from bik_node bn 
join bik_tree bt on bn.tree_id=bt.id 
where bn.name='Artyku�y' and bt.code='Glossary')
and is_deleted=0

delete from bik_metapedia where node_id in (select id from bik_node where parent_node_id=(select bn.id from bik_node bn 
join bik_tree bt on bn.tree_id=bt.id 
where bn.name='Artyku�y' and bt.code='Glossary' )and is_deleted=0)

update bik_node set tree_id=(select id from bik_tree where code='FAQ'),
parent_node_id=(select bn.id from bik_node bn join bik_tree bt on bn.tree_id=bt.id  where bn.name='Glosariusz' and bt.code='FAQ'),
node_kind_id=(select id from bik_node_kind where code='Question')
where parent_node_id=(select bn.id from bik_node bn 
join bik_tree bt on bn.tree_id=bt.id 
where bn.name='Artyku�y' and bt.code='Glossary')
and is_deleted=0

update bik_node set is_deleted=0 where id=(select bn.id from bik_node bn 
join bik_tree bt on bn.tree_id=bt.id 
where bn.name='Artyku�y' and bt.code='Glossary')


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

exec sp_update_version '1.0.89', '1.0.89.1';
go
