/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib.connectionpool;

import commonlib.LameUtils;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public abstract class ConnectionPoolManagerBase implements IConnectionPoolManager {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    public static final String CONNECTIONPOOLMANAGERPLAIN_INVALID_KEY = "_ConnectionPoolManagerPlain_invalid_";
    public static final String CONNECTIONPOOLMANAGERPLAIN_CONNECTION_KEY = "_ConnectionPoolManagerPlain_connection_";
    protected Semaphore sem;
    protected int secsTimeout;
    protected final ArrayList<Connection> connectionPool = new ArrayList<Connection>();
    protected final Set<Connection> connsInUsePlain = new HashSet<Connection>();
    protected boolean isDisposed;
    protected ThreadLocal<Pair<Connection, Boolean>> connectionsInUse = new ThreadLocal<Pair<Connection, Boolean>>();
    protected IParametrizedContinuation<Connection> prepareNewConnCont;
    protected boolean supportsIsValid = true;

    public ConnectionPoolManagerBase(int poolSize, int secsTimeout, IParametrizedContinuation<Connection> prepareNewConnCont) {
        this.sem = new Semaphore(poolSize, true);
        this.secsTimeout = secsTimeout;
        this.prepareNewConnCont = prepareNewConnCont;
    }

    public ConnectionPoolManagerBase(int poolSize, int secsTimeout) {
        this(poolSize, secsTimeout, null);
    }

    public void disableConnectionIsValidCheck() {
        supportsIsValid = false;
    }

    protected Connection openNewConnection() {
        try {
            long oncStart = System.currentTimeMillis();
            if (logger.isDebugEnabled()) {
                logger.debug("openNewConnection: before ds.getConnection()");
            }
            Connection c = openNewConnectionInternal();
            if (logger.isDebugEnabled()) {
                logger.debug("openNewConnection: before setAutoCommit(false)");
            }
            c.setAutoCommit(false);
            long oncEnd = System.currentTimeMillis();

            if (logger.isInfoEnabled()) {
                logger.info("openNewConnection took " + (oncEnd - oncStart) + " millis");
            }

//            if (prepareNewConnCont != null) {
//                prepareNewConnCont.doIt(c);
//            }
            return c;
        } catch (Exception ex) {
            throw new RuntimeException("openNewConnection", ex);
        }
    }

    protected boolean isConnectionValid(Connection c) throws SQLException {
        return !supportsIsValid || c.isValid(secsTimeout);
    }

    public void markConnectionAsInvalidInternal(Connection c) {
        if (c == null) {
            if (logger.isWarnEnabled()) {
                logger.warn("cannot make connection as invalid while it is null");
            }
            return;
        }

        Boolean isClosed = null;
        Boolean isInvalid = null;

        Exception exx = null;

        try {
            isClosed = c.isClosed();
            isInvalid = !isConnectionValid(c);
        } catch (Exception ex) {
            exx = ex;
        }

        if (logger.isWarnEnabled()) {
            logger.warn("Marking connection as invalid for thread: " + Thread.currentThread()
                    + ", isClosed=" + isClosed + ", "
                    + "isInvalid=" + isInvalid + (exx == null ? "" : ", exception when checking=" + exx));
            if (exx != null) {
                exx.printStackTrace();
            }
        }

        try {
            c.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void setThreadValues(Connection c, boolean isInvalid) {
        //ThreadCustomVars.setVar(CONNECTIONPOOLMANAGERPLAIN_CONNECTION_KEY, c);
        //ThreadCustomVars.setVar(CONNECTIONPOOLMANAGERPLAIN_INVALID_KEY, isInvalid);
        if (logger.isDebugEnabled()) {
            logger.debug("setThreadValues: start (c=" + c + ", isInvalid=" + isInvalid + ")");
        }
        if (c == null && !isInvalid) {
            if (logger.isDebugEnabled()) {
                logger.debug("setThreadValues: connectionsInUse.remove()");
            }
            connectionsInUse.remove();
        } else {
            final Pair<Connection, Boolean> p = new Pair<Connection, Boolean>(c, isInvalid);
            if (logger.isDebugEnabled()) {
                logger.debug("setThreadValues: connectionsInUse.set(" + p + ")");
            }
            connectionsInUse.set(p);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("setThreadValues: end");
        }
    }

    // mo�e zwr�ci� NULL!
    protected Pair<Connection, Boolean> getThreadValues() {
//        Connection c = (Connection) ThreadCustomVars.getVars().get(CONNECTIONPOOLMANAGERPLAIN_CONNECTION_KEY);
//        boolean isInvalid = ThreadCustomVars.getVars().getBool(CONNECTIONPOOLMANAGERPLAIN_INVALID_KEY);
//        return new Pair<Connection, Boolean>(c, isInvalid);
        if (logger.isDebugEnabled()) {
            logger.debug("getThreadValues: start");
        }

        Pair<Connection, Boolean> res = connectionsInUse.get();
        if (logger.isDebugEnabled()) {
            logger.debug("getThreadValues: before return (end)");
        }
        return res;
    }

    public void markConnectionAsInvalid() {
        Connection c = getConnectionInternal();

        markConnectionAsInvalidInternal(c);
        setThreadValues(null, true);

        sem.release();
    }

    protected boolean isConnectionMarkedAsInvalid() {
        //return ThreadCustomVars.getVars().getBool(CONNECTIONPOOLMANAGERPLAIN_INVALID_KEY);
        Pair<Connection, Boolean> p = getThreadValues();
        return p != null ? p.v2 : false;
    }

    protected void checkConnectionIsValid() {
        boolean invalid = isConnectionMarkedAsInvalid();
        if (invalid) {
            throw new RuntimeException("connection for this thread: " + Thread.currentThread()
                    + " is marked as invalid, "
                    + "recycleThreadConnection must be called before getConnection");
        }
    }

    protected Connection getConnectionInternal() {
        if (logger.isDebugEnabled()) {
            logger.debug("getConnectionInternal: start");
        }
        //return (Connection) ThreadCustomVars.getVars().get(CONNECTIONPOOLMANAGERPLAIN_CONNECTION_KEY);
        Pair<Connection, Boolean> p = getThreadValues();
        if (logger.isDebugEnabled()) {
            logger.debug("getConnectionInternal: before return (end)");
        }
        return p != null ? p.v1 : null;
    }

    // res.v2 == true --> new conn created!
    protected Pair<Connection, Boolean> getFromPoolOrNewConnection() {
        if (logger.isDebugEnabled()) {
            logger.debug("getFromPoolOrNewConnection: start, connectionPool.size=" + connectionPool.size());
        }

        try {
            if (!sem.tryAcquire(secsTimeout, TimeUnit.SECONDS)) {
                throw new RuntimeException("timed out while waiting for connection in pool");
            }
            if (logger.isDebugEnabled()) {
                logger.debug("getFromPoolOrNewConnection: after sem.tryAcquire");
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("interrupted while waiting for connection", ex);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getFromPoolOrNewConnection: before synchronized (connectionPool)");
        }
        synchronized (connectionPool) {
            if (connectionPool.isEmpty()) {
                if (logger.isDebugEnabled()) {
                    logger.debug("getFromPoolOrNewConnection: before return with openNewConnection");
                }

                return new Pair<Connection, Boolean>(openNewConnection(), true);
            }

            if (logger.isDebugEnabled()) {
                logger.debug("getFromPoolOrNewConnection: before remove last item from connectionPool, connectionPool.size=" + connectionPool.size());
            }
            Connection c = BaseUtils.removeLastItem(connectionPool);

            boolean isOK;

            if (logger.isDebugEnabled()) {
                logger.debug("getFromPoolOrNewConnection: before checking not closed and valid");
            }

            try {
                boolean isClosed = c.isClosed();

                if (logger.isDebugEnabled()) {
                    logger.debug("getFromPoolOrNewConnection: after check isClosed, before checking isValid");
                }

                isOK = !isClosed && isConnectionValid(c);
            } catch (Exception ex) {
                isOK = false;
            }

            if (!isOK) {
                if (logger.isWarnEnabled()) {
                    logger.warn("getFromPoolOrNewConnection: connection from pool is closed or invalid, will remove from pool and create new");
                }

                markConnectionAsInvalidInternal(c);

                return new Pair<Connection, Boolean>(openNewConnection(), true);
            }

            if (logger.isDebugEnabled()) {
                logger.debug("getFromPoolOrNewConnection: connection is ok, before return");
            }

            return new Pair<Connection, Boolean>(c, false);
        }
    }

    public Connection getConnection() {
        if (logger.isDebugEnabled()) {
            logger.debug("getConnection: start");
        }

        if (isDisposed) {
            throw new RuntimeException("cannot getConnection when pool is disposed");
        }

        checkConnectionIsValid();
        if (logger.isDebugEnabled()) {
            logger.debug("getConnection: after checkConnectionIsValid, before getConnectionInternal");
        }
        Connection c = getConnectionInternal();

        if (c == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("getConnection: connection is null, before getFromPoolOrNewConnection");
            }
            Pair<Connection, Boolean> p = getFromPoolOrNewConnection();
            c = p.v1;
            connsInUsePlain.add(c);

            if (logger.isDebugEnabled()) {
                logger.debug("getConnection: before setThreadValues");
            }
            setThreadValues(c, false);
            if (p.v2 && prepareNewConnCont != null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("getConnection: before prepareNewConnCont");
                }
                prepareNewConnCont.doIt(c);
            }

            //ThreadCustomVars.setVar(CONNECTIONPOOLMANAGERPLAIN_CONNECTION_KEY, c);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getConnection: before return (end)");
        }
        return c;
    }

    public void recycleThreadConnection(boolean doCommit) {
        if (logger.isDebugEnabled()) {
            logger.debug("recycleThreadConnection: start");
        }

//        boolean isInvalid = isConnectionMarkedAsInvalid();
//        Connection c = getConnectionInternal();
        Pair<Connection, Boolean> p = getThreadValues();

        if (p == null) {
            // nic nie robimy - szybka �cie�ka, wychodzimy
            return;
        }

        boolean isInvalid = p.v2;
        Connection c = p.v1;
        Exception exx = null;

        if (!isInvalid && c != null) {
            try {
                if (doCommit) {
                    c.commit();
                } else {
                    c.rollback();
                }
            } catch (Exception ex) {
                exx = ex;
                markConnectionAsInvalid();
            }
        }

        //ThreadCustomVars.setVar(CONNECTIONPOOLMANAGERPLAIN_CONNECTION_KEY, null);
        //ThreadCustomVars.setVar(CONNECTIONPOOLMANAGERPLAIN_INVALID_KEY, false);
        setThreadValues(null, false);

        if (exx != null) {
            throw new RuntimeException("error in recycleThreadConnection(" + doCommit + ") for thread: "
                    + Thread.currentThread(), exx);
        } else if (c != null) {
            if (isDisposed) {
                if (connsInUsePlain.contains(c)) {
                    closeInDisposing(c);
                }
            } else {
                saveConnectionToPool(c);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("recycleThreadConnection: end");
        }
    }

//    protected void clearThreadVars() {
//
//    }
    protected void closeInDisposing(Connection c) {
        if (logger.isDebugEnabled()) {
            logger.debug("closeInDisposing: start");
        }
        try {
            c.close();
        } catch (Exception ex) {
            if (logger.isWarnEnabled()) {
                logger.warn("error closing connection in dispose", ex);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("closeInDisposing: end");
        }
    }

    @Override
    public void dispose() {
        if (logger.isInfoEnabled()) {
            logger.info("dispose starts, connectionPool size: " + connectionPool.size() + ", connsInUse: " + connsInUsePlain.size());
        }
        if (isDisposed) {
            if (logger.isWarnEnabled()) {
                logger.warn("dispose: already disposed");
            }
            return;
        }
        isDisposed = true;
        synchronized (connectionPool) {
            for (Connection c : connectionPool) {
                closeInDisposing(c);
            }
            connectionPool.clear();

            for (Connection c : connsInUsePlain) {
                closeInDisposing(c);
            }

            connsInUsePlain.clear();
        }
        if (logger.isInfoEnabled()) {
            logger.info("dispose ends");
        }
    }

    protected void saveConnectionToPool(Connection c) {
        if (logger.isDebugEnabled()) {
            logger.debug("saveConnectionToPool: start");
        }
        synchronized (connectionPool) {
            connectionPool.add(c);
            connsInUsePlain.remove(c);
        }
        sem.release();
        if (logger.isDebugEnabled()) {
            logger.debug("saveConnectionToPool: end, connectionPool.size=" + connectionPool.size());
        }
    }
    //ww: zb�dne
//    public boolean hasConnectionInUse() {
//        return getConnectionInternal() != null;
//    }

    protected abstract Connection openNewConnectionInternal() throws SQLException;
}
