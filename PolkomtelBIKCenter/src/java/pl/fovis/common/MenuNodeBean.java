/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import simplelib.BeanWithIntIdBase;

/**
 *
 * @author wezyr
 */
public class MenuNodeBean extends BeanWithIntIdBase {

    public String name;
    public Integer parentNodeId;
    //ww: każda pozycja menu może być określonego typu, rozpoznawanego po
    // pierwszym znaku pola code, nazwa typu zaczyna się wtedy od drugiego
    // znaku:
    // $ - drzewo (nazwa typu to tree.code - np. $Reports - oznacza
    //     drzewo raportów SAP BO)
    // @ - szablon dla drzew dynamicznych (trzeba dodać menu dla każdego
    //     drzewa dynamicznego pod tą pozycją menu, nazwa typu oznacza
    //     treeKind, np. @Taxonomy - pod tą pozycję w menu podłączamy
    //     wszystkie drzewa taksonomii - ich jest dynamiczna ilość)
    // & - menu z typem węzła, np. &UniversesFolder - folder świata
    //     obiektów)
    // # - zakładka stała BIKSa, np. #MyBIKS - dla wyszukiwarki eliminujemy
    //     takie zakładki, generalnie eliminujemy w wyszukiwarce wszystko
    //     co nie ma pod sobą pozycji menu typu &
    // INNE - inne przypadki w menu budują jego strukturę - zbierają
    //        zakładki w grupy itp. - np. SAP BO jest w Bibliotece i dopiero
    //        pod spodem, pod SAP BO są konkretne drzewa
    public String code;
    //---
    public Integer treeId;
    public String nodeKindCode;
    public Integer nodeKindId;
    public Integer visualOrder;

    public MenuNodeBean() {
    }

    public MenuNodeBean(int id, String name, Integer parentNodeId, String code) {
        this(id, name, parentNodeId, code, null);
    }

    public MenuNodeBean(int id, String name, Integer parentNodeId, String code, Integer visualOrder) {
        this.id = id;
        this.name = name;
        this.parentNodeId = parentNodeId;
        this.code = code;
        this.visualOrder = visualOrder;
    }
}
