/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.dialogs.validators;

import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author pmielanczuk
 */
public interface IValidationStateChangeHandler {

    public void stateChanged(Widget inputControl, boolean isNowInvalid);

    public void setErrorMessage(Widget inputControl, String errMsg);
    
    public void updateFinished(boolean isAnythingNowInvalid);
}
