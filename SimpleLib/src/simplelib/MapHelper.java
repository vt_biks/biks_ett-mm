/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author wezyr
 */
public class MapHelper<T> //implements IMapHelperWrapper<T>
{

    private Map<T, Object> map;
    private IMapHelperWrapper<T> wrapper;

    @SuppressWarnings("unchecked")
    public MapHelper(Map<T, ?> map) {
        this.map = (Map)map;
    }

    public MapHelper(IMapHelperWrapper<T> wrapper) {
        this.wrapper = wrapper;
    }

    public boolean getBool(T key, boolean defVal) {
        Boolean b = getBoolean(key);
        return b == null ? defVal : b;
    }

    public boolean getBool(T key) {
        Boolean b = getBoolean(key);
        return b == null ? false : b;
    }

    public Boolean getBoolean(T key) {
        Object o = get(key);

        if (o == null) {
            return null;
        }

        if (o instanceof Boolean) {
            return (Boolean) o;
        } else {
            return Boolean.parseBoolean(BaseUtils.safeToString(o));
        }
    }

    public Object get(T key) {
        Object res;
        if (wrapper != null) {
            res = wrapper.getPropValue(key);
        } else {
            res = map != null ? map.get(key) : null;
        }
        return res;
    }

    public long getLong(T key) {
        Object o = get(key);

        if (o == null) {
            return 0;
        }
        if (o instanceof Long) {
            return (Long) o;
        }

        return getInt(key);
    }

    @SuppressWarnings("unchecked")
    public Collection<Map> getMapCollection(T key) {
        return (Collection<Map>) get(key);
    }

    public Collection getCollection(T key) {
        return (Collection) get(key);
    }

    public Date getDate(T key) {
        return (Date) get(key);
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> getMap(T key) {
        return (Map<String, Object>) get(key);
    }

    @SuppressWarnings("unchecked")
    public MapHelper<String> getMapHelper(T key) {
        Map<String, Object> m2 = (Map<String, Object>) get(key);
        return new MapHelper<String>(m2);
    }

    public String getString(T key) {
        return getString(key, null);
    }

    public String getString(T key, String defVal) {
        Object o = get(key);
        return o == null ? defVal : BaseUtils.safeToString(o);
    }

    public Integer getInteger(T key) {
        Object o = get(key);

        if (o == null) {
            return null;
        }

        if (!(o instanceof Integer || o instanceof Double)) {
            o = BaseUtils.tryParseNumber(BaseUtils.safeToString(o));
        }

        if (o instanceof Integer) {
            return (Integer) o;
        }
        if (o instanceof Double) {
            return ((Double) o).intValue();
        }

        throw new LameRuntimeException("cannot parse number (for int): for key=\"" + key
                + "\", val=\"" + o + "\"");
    }

    public int getInt(T key) {
        return getInt(key, 0);
    }

    public int getInt(T key, int defVal) {
        Integer i = getInteger(key);
        return i == null ? defVal : i;
    }

    public Double getDouble(T key) {
        Object o = get(key);

        if (o == null) {
            return null;
        }

        if (!(o instanceof Integer || o instanceof Double)) {
            o = BaseUtils.tryParseNumber(BaseUtils.safeToString(o));
        }

        if (o instanceof Integer) {
            return ((Integer) o).doubleValue();
        }
        if (o instanceof Double) {
            return (Double) o;
        }

        throw new LameRuntimeException("cannot parse number (for double): for key=\"" + key
                + "\", val=\"" + o + "\"");
    }

    public Set<T> getPropNames() {
        if (wrapper != null) {
            return wrapper.getPropNames();
        }
        return map.keySet();
    }

    public Map<T, Object> helperAsMap() {
        if (wrapper == null) {
            return map;
        }
        Map<T, Object> m = new LinkedHashMap<T, Object>();
        for (T propName : wrapper.getPropNames()) {
            m.put(propName, wrapper.getPropValue(propName));
        }

        return m;
    }

    public MapHelper<T> put(T key, Object val) {
        if (map == null) {
            throw new LameRuntimeException("this MapHelper is immuteable");
        }
        map.put(key, val);
        return this;
    }

    // freshly created mutable instance
    public static MapHelper<String> makeEmpty() {
        return new MapHelper<String>(new LinkedHashMap<String, Object>());
    }
    private static final MapHelper<String> emptyImmutable =
            new MapHelper<String>(Collections.<String, Object>emptyMap());

    public static MapHelper<String> emptyImmutable() {
        return emptyImmutable;
    }

    public boolean getNonEmptyStrValAsTrue(T key) {
        String s = getString(key);
        return !BaseUtils.isStrEmptyOrWhiteSpace(s);
    }
}
