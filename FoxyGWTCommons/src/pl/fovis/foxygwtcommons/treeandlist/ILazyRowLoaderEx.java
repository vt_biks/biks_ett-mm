/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.treeandlist;

import simplelib.IParametrizedContinuation;

/**
 *
 * @author wezyr
 */
public interface ILazyRowLoaderEx<ROW, IDT> extends ILazyRowLoader<ROW> {

    public void loadAncestorsForSelection(IDT id, boolean expand, IParametrizedContinuation<IDT> optContInsteadOfSelectRecord);
}
