CREATE DATABASE "DB_SBX_NAS" FROM "DBC" AS
 PERMANENT = 999999965,
 SPOOL = 0,
 TEMPORARY = 0,
 ACCOUNT = 'DBC',
 NO FALLBACK,
 NO BEFORE JOURNAL,
 NO AFTER JOURNAL;

CREATE  TABLE "DB_SBX_NAS"."prod_wer"  (
  "id_prod" CHARACTER(11) CHARACTER SET LATIN,
  "nazwa_prod" VARCHAR(255) CHARACTER SET LATIN,
  "data_od" DATE,
  "data_do" DATE,
  "oplata_stala" DECIMAL(18 , 2),
  "id_load" INTEGER,
  "id_update" INTEGER
 )
 PRIMARY INDEX ("id_prod","data_od");

 CREATE  TABLE "DB_SBX_NAS"."kateg_prod_rel"  (
  "ID_TYP_KATEG" SMALLINT,
  "ID_KATEG" SMALLINT,
  "ID_PROD" CHARACTER(11) CHARACTER SET LATIN,
  "DATA_UTWORZENIA" DATE,
  "DATA_ZMIANY" DATE,
  "DATA_OD" DATE NOT NULL FORMAT 'YYYY-MM-DD' DEFAULT DATE '1990-01-01',
  "DATA_DO" DATE NOT NULL FORMAT 'YYYY-MM-DD' DEFAULT DATE '3000-12-31'
 )
 PRIMARY INDEX ("ID_TYP_KATEG","ID_PROD","DATA_OD");

CREATE  TABLE "DB_SBX_NAS"."kateg"  (
  "ID_TYP_KATEG" SMALLINT,
  "ID_KATEG" SMALLINT,
  "NAZWA_KATEG" VARCHAR(80) CHARACTER SET LATIN,
  "OPIS_KATEG" VARCHAR(255) CHARACTER SET LATIN,
  "DATA_UTWORZENIA" DATE,
  "DATA_ZMIANY" DATE,
  "NAZWA_KATEG_EN" VARCHAR(80) CHARACTER SET LATIN,
  "OPIS_KATEG_EN" VARCHAR(255) CHARACTER SET LATIN
 )
 UNIQUE PRIMARY INDEX ("ID_TYP_KATEG","ID_KATEG");

CREATE  TABLE "DB_SBX_NAS"."Log_sql"  (
  "tekst_sql" VARCHAR(2048) CHARACTER SET LATIN
 )
 PRIMARY INDEX ("tekst_sql");

CREATE  TABLE "DB_SBX_NAS"."prod"  (
  "id_prod" CHARACTER(11) CHARACTER SET LATIN,
  "id_prod_nadrz" CHARACTER(11) CHARACTER SET LATIN,
  "id_typ_prod" VARCHAR(10) CHARACTER SET LATIN,
  "nazwa_prod" VARCHAR(255) CHARACTER SET LATIN,
  "id_prod_wersja" VARCHAR(30) CHARACTER SET LATIN,
  "data_poczatku" DATE,
  "data_konca" DATE,
  "id_prod_zr" VARCHAR(100) CHARACTER SET LATIN,
  "id_system_zr" BYTEINT,
  "id_typ_usluga" CHARACTER(2) CHARACTER SET LATIN,
  "id_podtyp_usluga" CHARACTER(2) CHARACTER SET LATIN,
  "il_cykli" SMALLINT,
  "id_typ_jednostki_miary" CHARACTER(5) CHARACTER SET LATIN,
  "wartosc" DECIMAL(18 , 2),
  "id_typ_wartosc" BYTEINT,
  "il_wystapien" INTEGER,
  "id_cp_tier" BYTEINT,
  "id_typ_oplata" CHARACTER(1) CHARACTER SET LATIN,
  "id_typ_plan_taryfowy" BYTEINT,
  "il_miesiecy_plan_taryfowy" BYTEINT,
  "id_plan_taryfowy_nast" CHARACTER(11) CHARACTER SET LATIN,
  "id_typ_jednostki_miary_cena" CHARACTER(5) CHARACTER SET LATIN,
  "cena_domyslna" DECIMAL(18 , 2),
  "oplata_stala" DECIMAL(18 , 2),
  "id_sprzet_marka" SMALLINT,
  "id_typ_kupon" CHARACTER(2) CHARACTER SET LATIN,
  "data_stat" DATE,
  "id_podtyp_prod" SMALLINT,
  "blokada_wsk" CHARACTER(1) CHARACTER SET LATIN,
  "priorytet_wsk" SMALLINT,
  "wartosc_proc_upustu" DECIMAL(10 , 6),
  "kwota_odnowienia_od" SMALLINT,
  "suma_odnowienia" DECIMAL(18 , 2),
  "anu_wsk" BYTEINT,
  "wsk_in" BYTEINT,
  "strefa_czasowa_typ" VARCHAR(50) CHARACTER SET LATIN,
  "strefa_czasowa_dnia" VARCHAR(50) CHARACTER SET LATIN,
  "okres_powt" VARCHAR(50) CHARACTER SET LATIN,
  "id_typ_zuzycia" INTEGER,
  "alokacja_wsk" CHARACTER(1) CHARACTER SET LATIN,
  "id_kod_gl" VARCHAR(10) CHARACTER SET LATIN,
  "efaktura_wsk" BYTEINT,
  "id_load" INTEGER,
  "id_update" INTEGER
 )
 PRIMARY INDEX ("id_prod");

CREATE  TABLE "DB_SBX_NAS"."kateg_typ"  (
  "id_typ_kateg" SMALLINT NOT NULL,
  "id_typ_kateg_def" BYTEINT,
  "nazwa_typ_kateg" VARCHAR(80) CHARACTER SET LATIN,
  "opis_typ_kateg" VARCHAR(255) CHARACTER SET LATIN,
  "data_utworzenia" DATE NOT NULL,
  "data_zmiany" DATE NOT NULL,
  "wersjonowana" BYTEINT DEFAULT 0
 )
 UNIQUE PRIMARY INDEX "PK_T5001_KATEG_TYP" ("id_typ_kateg");

REPLACE VIEW "DB_SBX_NAS"."Kateg_prod_77_sel" AS
select p.id_prod, p.nazwa_prod, kpr.id_typ_kateg, kpr.id_kateg, k.nazwa_kateg, kpr.data_od, kpr.data_do
from DB_SBX_NAS.kateg_prod_rel kpr 
join  DB_SBX_NAS.prod p on kpr.id_prod = p.id_prod
join DB_SBX_NAS.kateg k on k.id_kateg = kpr.id_kateg and  k.id_typ_kateg = kpr.id_typ_kateg
where  kpr.id_typ_kateg = 77;

CREATE INDEX ( "ID_TYP_KATEG", "ID_PROD", "DATA_OD") ON "DB_SBX_NAS"."kateg_prod_rel";

CREATE INDEX ( "id_prod") ON "DB_SBX_NAS"."prod";

CREATE UNIQUE INDEX ( "ID_TYP_KATEG", "ID_KATEG") ON "DB_SBX_NAS"."kateg";

CREATE INDEX ( "tekst_sql") ON "DB_SBX_NAS"."Log_sql";

CREATE UNIQUE INDEX "PK_T5001_KATEG_TYP" ( "id_typ_kateg") ON "DB_SBX_NAS"."kateg_typ";

CREATE INDEX ( "id_prod", "data_od") ON "DB_SBX_NAS"."prod_wer";