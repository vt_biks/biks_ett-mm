/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.plainfile;

import commonlib.LameUtils;
import jcifs.smb.NtlmPasswordAuthentication;
import pl.bssg.metadatapump.MetadataTestConnectionBase;
import pl.bssg.metadatapump.common.ConnectionParametersPlainFileBean;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.bssg.metadatapump.filesystem.IFileDelegator;
import pl.bssg.metadatapump.filesystem.LocalFileDelegator;
import pl.bssg.metadatapump.filesystem.PumpFileUtils;
import pl.bssg.metadatapump.filesystem.SmbFileDelegator;
import pl.trzy0.foxy.serverlogic.db.MssqlConnectionConfig;
import simplelib.Pair;
import simplelib.logging.ILameLogger;

/**
 *
 * @author ctran
 */
public class PlainFileTestConnection extends MetadataTestConnectionBase {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected Integer configId;

    public PlainFileTestConnection(MssqlConnectionConfig mssqlConnCfg, int id) {
        super(mssqlConnCfg);
        this.configId = id;
    }

    @Override
    public Pair<Integer, String> testConnection() {
        try {
            ConnectionParametersPlainFileBean config = adhocDao.createBeanFromNamedQry("getPlainFileConfigForInstanceId", ConnectionParametersPlainFileBean.class, true, configId);
            IFileDelegator file;
            if (config.isRemote) {
                NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(config.domain, config.username, config.password);
                file = new SmbFileDelegator(PumpFileUtils.fixPathIfNessecary(config.sourceFolderPath), auth);
            } else {
                file = new LocalFileDelegator(config.sourceFolderPath);
            }
            if (!file.isDirectory()) {
                return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Failed: target is not a folder.");
            }
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_SUCCESS, "Success. There are " + file.getFileCount() + " objects.");
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("Error in File System test connection", ex);
            }
            return new Pair<Integer, String>(PumpConstants.TEST_CONNECTION_FAIL, "Failed: " + ex.getMessage());
        }
    }
}
