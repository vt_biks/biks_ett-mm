declare @dropScript varchar(max) = ''

select @dropScript = @dropScript + 'drop table ' + name + '
--go
' from sysobjects where type = 'U'
and not (name like 'aa%' or name like 'bik%' or name like 'sys%' or name like 'app%'
or name like 'info%' or name like 'a00%')
order by name

--select @dropScript
execute(@dropScript)
go
