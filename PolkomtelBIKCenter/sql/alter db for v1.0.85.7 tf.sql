exec sp_check_version '1.0.85.7';
go

--dla polka bez tych 3 alter�w, bo tam juz sa wykonane!!
alter table aaa_universe_obj
ADD obj_branch varchar(1000)
go

alter table aaa_universe_filtr
ADD filtr_branch varchar(1000)
go

alter table aaa_universe_table
ADD universe_branch varchar(1000)
go
-------------------

update bik_node_kind set icon_name='class' where code='UniverseClass'

ALTER TABLE aaa_report_object
ADD universe_name varchar(max)
go

ALTER TABLE aaa_query
ADD universe_name varchar(max)
go

ALTER TABLE aaa_report_object
ADD wrong_universe_cuid varchar(100)
go

ALTER TABLE aaa_query
ADD wrong_universe_cuid varchar(100)
go

create table bik_sapbo_object_table(
	id int identity(1,1) not null primary key,
	object_node_id int not null,
	table_id int not null
)
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_object_table]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_sapbo_object_table]
GO

create procedure sp_insert_into_bik_node_sapbo_object_table
as
begin

declare @tree_id int;
select @tree_id = id from bik_tree where code='ObjectUniverses'
declare @universe_kind int;
select @universe_kind = id from bik_node_kind where code='Universe'
declare @filtr_kind int;
select @filtr_kind = id from bik_node_kind where code='Filter'
declare @detail_nki int;
select @detail_nki = id	from bik_node_kind where code = 'Detail';
declare @dimension_nki int;
select @dimension_nki = id from bik_node_kind where code = 'Dimension';
declare @measure_nki int;
select @measure_nki = id from bik_node_kind	where code = 'Measure';

insert into bik_sapbo_object_table
select *
from (
select case when uot.obj_type=1 then bn.id else bn3.id end as obj_node_ide, sut.id from aaa_universe_obj_tables uot
left join aaa_universe_obj uo on uot.universe_obj_id=uo.id
left join bik_node bn on bn.obj_id = uo.obj_branch
and bn.is_deleted=0
and bn.linked_node_id is null
and bn.tree_id=@tree_id
and bn.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)
join aaa_universe_table ut on uot.universe_table_id=ut.id
join bik_node bn2 on bn2.obj_id = ut.universe_branch
and bn2.is_deleted=0
and bn2.linked_node_id is null
and bn2.node_kind_id=@universe_kind
join bik_sapbo_universe_table sut on ut.name=sut.name
and sut.universe_node_id=bn2.id
left join aaa_universe_filtr uf on uot.universe_obj_id=uf.id
left join bik_node bn3 on bn3.obj_id = uf.filtr_branch
and bn3.is_deleted=0
and bn3.linked_node_id is null
and bn3.tree_id=@tree_id
and bn3.node_kind_id=@filtr_kind
left join bik_sapbo_object_table bsot on bsot.object_node_id=case when uot.obj_type=1 then bn.id else bn3.id end and bsot.table_id=sut.id
where bsot.id is null
) x
where obj_node_ide is not null
end
go

alter table aaa_universe_connetion
	add type varchar(255)
go

alter table aaa_universe_connetion
	add database_enigme varchar(255)
go

alter table aaa_universe_connetion
	add client_number int
go

alter table aaa_universe_connetion
	add language varchar(10)
go

alter table aaa_universe_connetion
	add system_number varchar(255)
go

alter table aaa_universe_connetion
	add system_id varchar(255)
go

-----------------------------------

alter table bik_sapbo_universe_connection
	add type varchar(255)
go

alter table bik_sapbo_universe_connection
	add database_enigme varchar(255)
go

alter table bik_sapbo_universe_connection
	add client_number int
go

alter table bik_sapbo_universe_connection
	add language varchar(10)
go

alter table bik_sapbo_universe_connection
	add system_number varchar(255)
go

alter table bik_sapbo_universe_connection
	add system_id varchar(255)
go
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_objects_from_Designer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_objects_from_Designer]
GO

create procedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]
as
	declare @tree_id int;
	declare @kind_id int;
	declare @dimension_nki int, @measure_nki int, @detail_nki int, @filtr_nki int;
begin--begin procedure

	----wybranie id odpowiedniego drzewa
	select @tree_id = id
	from bik_tree
	where code = 'ObjectUniverses';	
	
	----wybranie id odpowiedniego kind'a
	select @kind_id = id
	from bik_node_kind
	where code = 'UniverseClass';
		
	create table #tmpNodes (si_id varchar(1000) , si_parentid varchar(1000), si_kind_id int, si_name varchar(max), si_description varchar(max));

	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Class
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), 
		case
			when uc.parent_id is null then convert(varchar, u.global_props_si_id)
			else convert(varchar, uc.parent_id) + '|' + convert(varchar, u.global_props_si_id) end as parent_id, 
		@kind_id, uc.name, uc.description
	from aaa_universe_class uc join aaa_universe u on u.id = uc.universe_id 
	
	----wybranie id odpowiedniego kind'a
	--detail
	select @detail_nki = id
	from bik_node_kind
	where code = 'Detail';
	--demension
	select @dimension_nki = id
	from bik_node_kind
	where code = 'Dimension';
	--measure
	select @measure_nki = id
	from bik_node_kind
	where code = 'Measure';
	--filtr
	select @filtr_nki = id
	from bik_node_kind
	where code = 'Filter';
	
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Obj
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), 
			case
				when uo.parent_id is null then convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id)
				else convert(varchar, uo.parent_id)+ '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id)
			end as parent_id, 
			case
				when uo.qualification = 'dsDetailObject' then @detail_nki
				when uo.qualification = 'dsMeasureObject' then @measure_nki
				when uo.qualification = 'dsDimensionObject' then @dimension_nki
				end as node_kind_id,
		 uo.name, uo.description
	from aaa_universe_obj uo join aaa_universe_class uc on uo.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id 
							 
	--wrzucam do tabeli tymczasowej wszystkie dane z Universe_filrt
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uf.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id),
		convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), @filtr_nki, uf.name, nullif(ltrim(rtrim(uf.description)), '') 
	from aaa_universe_filtr uf join aaa_universe_class uc on uf.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id 
							 

	--aktualizacja istniej�cych w�z��w w drzewie							
	update bik_node 
	set parent_node_id = null, node_kind_id = #tmpNodes.si_kind_id, name = #tmpNodes.si_name,
		is_deleted = 0, descr = #tmpNodes.si_description, tree_id = @tree_id
	from #tmpNodes 
	where bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id and bik_node.linked_node_id is null;
	
	--dorzucanie nowych w�z��w w drzewie
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select #tmpNodes.si_kind_id, #tmpNodes.si_name, @tree_id, #tmpNodes.si_id, #tmpNodes.si_description
	from #tmpNodes left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
		and bik_node.tree_id = @tree_id
	where bik_node.id is null;	
	
	--uaktualnianie parent�w w nodach
	declare @universe_kind_id int;
	select @universe_kind_id = id
	from bik_node_kind
	where code = 'Universe';
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id and bik_node.node_kind_id = pt.si_kind_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id 
		and  (
		 (pt.si_kind_id=@detail_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@dimension_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@measure_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@filtr_nki and bk.node_kind_id=@kind_id) or
		 (pt.si_kind_id=@kind_id and (bk.node_kind_id = @kind_id or bk.node_kind_id = @universe_kind_id)))
	where bik_node.linked_node_id is null;
		
	--usuwanie nod�w
	update bik_node
	set is_deleted = 1
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and bik_node.node_kind_id in (@kind_id, @detail_nki, @dimension_nki, @measure_nki,
	  @filtr_nki);
	
	drop table #tmpNodes;
	--uzupe�nianie danych w tabelach extra 
	--dla object
	create table #tmpObjExtra(text_of_select varchar(max),text_of_where varchar(max), type varchar(155), aggregate_function varchar(50), node_id int)
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select univ_obj.text_of_select, univ_obj.text_of_where, univ_obj.type, univ_obj.aggregate_function, bik_node.id
	from (
	select convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id) as obj_id, 
					uo.text_of_select, uo.text_of_where, uo.type, uo.aggregate_function
			from aaa_universe_obj uo join aaa_universe_class uc on uo.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id ) univ_obj 
			join bik_node on bik_node.obj_id = univ_obj.obj_id and bik_node.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)-- = @obj_node_kind_id;
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select null, univ_filtr.where_clause, univ_filtr.type, null, bik_node.id
	from (
		select convert(varchar, uf.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id) as obj_id,
			uf.where_clause, uf.type
		from aaa_universe_filtr uf join aaa_universe_class uc on uf.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id ) univ_filtr
				 join bik_node on bik_node.obj_id = univ_filtr.obj_id and bik_node.node_kind_id = @filtr_nki
			
	update bik_sapbo_universe_object
	set text_of_select = #tmpObjExtra.text_of_select, text_of_where = #tmpObjExtra.text_of_where, type = #tmpObjExtra.type,  
		           aggregate_function = #tmpObjExtra.aggregate_function 
	from #tmpObjExtra join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id;
	
	insert into bik_sapbo_universe_object(text_of_select, text_of_where, type, aggregate_function, node_id)
	select #tmpObjExtra.text_of_select,#tmpObjExtra.text_of_where, #tmpObjExtra.type, #tmpObjExtra.aggregate_function, #tmpObjExtra.node_id 
	from #tmpObjExtra left join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id
	where bik_sapbo_universe_object.id is null;

	drop table #tmpObjExtra;
	
	--dla tabeli
	
	update bik_sapbo_universe_table
	set name = ut.name, is_alias = ut.is_alias, is_derived = ut.is_derived, original_table = ut.original_table, sql_of_derived_table = ut.sql_of_derived_table, 
		sql_of_derived_table_with_alias = ut.sql_of_derived_table_with_alias, their_id = ut.their_id, universe_node_id = bik_node.id, type = ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connetion uc on u.universe_connetion_id = uc.id
		join aaa_universe_connetion_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where linked_node_id is null and bik_node.node_kind_id = @universe_kind_id
	
	
	insert into bik_sapbo_universe_table(name, is_alias, is_derived, original_table, sql_of_derived_table, sql_of_derived_table_with_alias, their_id, universe_node_id, type)
	select ut.name, ut.is_alias, ut.is_derived, ut.original_table, ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, ut.their_id, 
					bik_node.id as universe_node_id, ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connetion uc on u.universe_connetion_id = uc.id
		join aaa_universe_connetion_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where  bik_node.linked_node_id is null and bsut.name is null and bik_node.node_kind_id = @universe_kind_id

	--usupe�nianie name_for_teradata
	update bik_sapbo_universe_table
	set name_for_teradata = replace(name,'.', '|') + '|'
	where type = 'Teradata' and is_derived = 0 and is_alias = 0

	update bik_sapbo_universe_table
	set name_for_teradata = bsut.name_for_teradata
	from bik_sapbo_universe_table join bik_sapbo_universe_table bsut on bik_sapbo_universe_table.original_table = bsut.their_id and 
		bik_sapbo_universe_table.universe_node_id = bsut.universe_node_id
	where bik_sapbo_universe_table.type = 'Teradata' and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 1
	
	update bik_sapbo_universe_table
	set schema_name = left(name_for_teradata, charindex('|',name_for_teradata))
	where  type = 'Teradata' and is_derived = 0
	
	--dla kolumn
	update bik_sapbo_universe_column
	set name = uc.name, type = uc.type, table_id = bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and bik_node.node_kind_id = @universe_kind_id 
	
	insert into bik_sapbo_universe_column(name, type, table_id)
	select uc.name, uc.type, bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and bsuc.id is null and bik_node.node_kind_id = @universe_kind_id 
	
	delete 
	from bik_sapbo_universe_table
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and uc.id is null and bik_node.node_kind_id = @universe_kind_id
	
	delete 
	from bik_sapbo_universe_table
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where  bik_node.linked_node_id is null and ut.name is null and bik_node.node_kind_id = @universe_kind_id
	
	update bik_sapbo_universe_connection 
	set bik_sapbo_universe_connection.server = aaa_universe_connetion.server, bik_sapbo_universe_connection.user_name = aaa_universe_connetion.user_name,
		bik_sapbo_universe_connection.password = aaa_universe_connetion.password, bik_sapbo_universe_connection.database_source = aaa_universe_connetion.database_source, 
		bik_sapbo_universe_connection.connetion_networklayer_name = aaa_universe_connetion_networklayer.name, bik_sapbo_universe_connection.node_id = bik_node.id, 
		bik_sapbo_universe_connection.type = aaa_universe_connetion.type, bik_sapbo_universe_connection.database_enigme = aaa_universe_connetion.database_enigme,
		bik_sapbo_universe_connection.client_number = aaa_universe_connetion.client_number, bik_sapbo_universe_connection.language = aaa_universe_connetion.language, 
		bik_sapbo_universe_connection.system_number = aaa_universe_connetion.system_number, bik_sapbo_universe_connection.system_id = aaa_universe_connetion.system_id
	from aaa_universe_connetion join aaa_universe_connetion_networklayer on aaa_universe_connetion.connetion_networklayer_id = aaa_universe_connetion_networklayer.id
		join bik_node on aaa_universe_connetion.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		join bik_sapbo_universe_connection on  bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections'
	
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id, type, database_enigme, client_number, language, system_number, system_id)
	select aaa_universe_connetion.server, aaa_universe_connetion.user_name, aaa_universe_connetion.password, aaa_universe_connetion.database_source, 
		aaa_universe_connetion_networklayer.name, bik_node.id,
		aaa_universe_connetion.type, aaa_universe_connetion.database_enigme, aaa_universe_connetion.client_number, aaa_universe_connetion.language,
		aaa_universe_connetion.system_number, aaa_universe_connetion.system_id
	from aaa_universe_connetion join aaa_universe_connetion_networklayer on aaa_universe_connetion.connetion_networklayer_id = aaa_universe_connetion_networklayer.id
		join bik_node on aaa_universe_connetion.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		left join bik_sapbo_universe_connection on  bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections' and bik_sapbo_universe_connection.id is null;
		
	update bik_sapbo_extradata
	set statistic = u.statistic
	from bik_sapbo_extradata join bik_node bn on bn.id = bik_sapbo_extradata.node_id
		join bik_node_kind bnk on bn.node_kind_id = bnk.id
		join aaa_universe u on convert(varchar,u.global_props_si_id) = bn.obj_id
	where bnk.code = 'Universe'	
				
	exec sp_delete_linked_nodes_where_orignal_is_deleted;	
	exec sp_node_init_branch_id @tree_id, null;
end--end procedure
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_sapbo_extradata]
GO

create procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
as
begin
insert into bik_sapbo_extradata (node_id,author,owner,keyword,file_path,file_name,created,modified,cuid,files_value,size,has_children,guid,instance,owner_id,progid,obtype,flags,children,ruid,runnable_object,content_locale,is_schedulable,webi_doc_properties,read_only,last_successful_instance_id,last_run_time,timestamp,progid_machine,endtime,starttime,schedule_status,recurring,nextruntime,doc_sender,revisionnum,application_object,shortname,dataconnection_total,universe_total )
    select bik.id as node_id,
    nullif(ltrim(rtrim(p.SI_AUTHOR)), '') as author,
    nullif(ltrim(rtrim(p.SI_OWNER)), '') as owner,
    nullif(ltrim(rtrim(p.SI_KEYWORD)), '') as keyword,
    nullif(ltrim(rtrim(p.SI_FILES__SI_PATH)), '') as file_path,
    nullif(ltrim(rtrim(p.SI_FILES__SI_FILE1)), '') as file_name,
    nullif(ltrim(rtrim(p.SI_CREATION_TIME)), '') as created,
    nullif(ltrim(rtrim(p.SI_UPDATE_TS)), '') as modified,
    nullif(ltrim(rtrim(p.SI_CUID)), '') as cuid,
    nullif(ltrim(rtrim(p.SI_FILES__SI_VALUE1)), '') as files_value,
    nullif(ltrim(rtrim(p.SI_SIZE)), '') as size,
    p.SI_HAS_CHILDREN as has_children,
    nullif(ltrim(rtrim(p.SI_GUID)), '') as guid,
    p.SI_INSTANCE as instance,
    p.SI_OWNERID as owner_id,
    nullif(ltrim(rtrim(p.SI_PROGID)), '') as progid,
    p.SI_OBTYPE as obtype,
    p.SI_FLAGS as flags,
    p.SI_CHILDREN as children,
    nullif(ltrim(rtrim(p.SI_RUID)), '') as ruid,
    p.SI_RUNNABLE_OBJECT as runnable_object,
    nullif(ltrim(rtrim(p.SI_CONTENT_LOCALE)), '') as content_locale,
    p.SI_IS_SCHEDULABLE as is_schedulable,
    nullif(ltrim(rtrim(p.SI_WEBI_DOC_PROPERTIES)), '') as webi_doc_properties,
    p.SI_READ_ONLY as read_only,
    nullif(ltrim(rtrim(iw.SI_LAST_SUCCESSFUL_INSTANCE_ID)), '') as last_successful_instance_id,
    nullif(ltrim(rtrim(iw.SI_LAST_RUN_TIME)), '') as last_run_time,
    nullif(ltrim(rtrim(iw.SI_TIMESTAMP)), '')  as timestamp,
    nullif(ltrim(rtrim(iw.SI_PROGID_MACHINE)), '') as progid_machine,
    nullif(ltrim(rtrim(iw.SI_ENDTIME)), '') as endtime,
    nullif(ltrim(rtrim(iw.SI_STARTTIME)), '') as starttime,
    nullif(ltrim(rtrim(iw.SI_SCHEDULE_STATUS)), '') as schedule_status,
    nullif(ltrim(rtrim(iw.SI_RECURRING)), '') as recurring,
    nullif(ltrim(rtrim(iw.SI_NEXTRUNTIME)), '') as nextruntime,
    nullif(ltrim(rtrim(iw.SI_DOC_SENDER)), '') as doc_sender,
    au.SI_REVISIONNUM as revisionnum,
    au.SI_APPLICATION_OBJECT as application_object,
    nullif(ltrim(rtrim(au.SI_SHORTNAME)), '') as shortname,
    au.SI_DATACONNECTION__SI_TOTAL as dataconnection_total,
    iw.SI_UNIVERSE__SI_TOTAL as universe_total
    from aaa_global_props p
    left join INFO_WEBI iw on p.si_id = iw.si_id
    left join APP_UNIVERSE au on p.si_id = au.si_id
    inner join bik_node bik on convert(varchar,p.si_id) = bik.obj_id
    left join bik_sapbo_extradata spe on bik.id =spe.node_id
    where spe.node_id is null
    order by bik.id
end
go

--------------------------------------------------------------
exec sp_update_version '1.0.85.7', '1.0.86';
go