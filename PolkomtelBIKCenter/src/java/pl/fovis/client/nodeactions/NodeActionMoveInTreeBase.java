/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import com.google.gwt.user.client.Window;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.MoveNodeInTreeMode;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.treeandlist.ITreeGridForAction;
import simplelib.LameRuntimeException;

/**
 *
 * @author pmielanczuk
 */
public abstract class NodeActionMoveInTreeBase extends NodeActionUpdatingBase {

    protected int siblingCnt;
    protected int idxInSiblings;
    //protected int newIdxInSiblings;

    protected abstract MoveNodeInTreeMode getMoveMode();

    protected int calcNewIndexInSiblings() {
        int newIdxInSiblings;

        ITreeGridForAction<Integer> treeGrid = ctx.getTreeGrid();

        siblingCnt = treeGrid.getNodeChildCount(node.parentNodeId);
        idxInSiblings = treeGrid.getNodeIndexInSiblings(node.id);

        switch (getMoveMode()) {
            case MakeFirst:
                newIdxInSiblings = 0;
                break;
            case MoveUp:
                newIdxInSiblings = idxInSiblings - 1;
                break;
            case MoveDown:
                newIdxInSiblings = idxInSiblings + 2;
                break;
            case MakeLast:
                newIdxInSiblings = siblingCnt;
                break;
            default:
                throw new LameRuntimeException("unsupported mode" /* I18N: no */ + ": " + getMoveMode());
        }

        return newIdxInSiblings;
    }

    @Override
    public boolean isEnabledByDesign() {
        if (BIKClientSingletons.hideActionMoveInTree()
                && !(BIKConstants.TREE_KIND_META_BIKS.equals(node.treeKind) || BIKConstants.TREE_KIND_META_BIKS_MENU_CONF.equals(node.treeKind))) {
            return false;
        }
        if (isConfluenceObject()) {
            return node.isBuiltIn && isAdminOrExpertOrAuthorOrEditor();
        }
        if (BIKClientSingletons.isLoggedUserAuthorOfTree(node.treeId) || BIKClientSingletons.isLoggedUserCreatorOfTree(node.treeId)) {
            return true;
        }
        if (!isNodeNotLinkedUnderRootNorReadOnlyNode()) {
            return false;
        }

        int newIdxInSiblings = calcNewIndexInSiblings();

        return newIdxInSiblings >= 0 && newIdxInSiblings <= siblingCnt
                && newIdxInSiblings != idxInSiblings
                && newIdxInSiblings != idxInSiblings + 1;

    }

    @Override
    public void execute() {
        int newIdxInSiblings = calcNewIndexInSiblings();

        if (false) {
            Window.alert("execute: mode" /* I18N: no */ + "=" + getMoveMode() + ", oldIdx=" + idxInSiblings + ", " + "cnt" /* I18N: no */ + "=" + siblingCnt + ", newIdx="
                    + newIdxInSiblings);
            //return;
        }

        getService().setNodeVisualOrder(node.id, newIdxInSiblings,
                getAntiTreeRefreshingCallback(I18n.kolejnoscZmieniona.get() /* I18N:  */) //                new AntiNodeRefreshingCallback<Void>(node.id, "Kolejność zmieniona") {
        //
        //            @Override
        //            protected void refreshNode() {
        //                ctx.refreshAndRedisplayData();
        //               //ctx.getTreeGrid().deleteNode(nodeId);
        //               //ctx.addTreeGridNode(node);
        //            }
        //        }
        );

        ctx.getTreeGrid().setNodeIndexInSiblings(node.id, newIdxInSiblings);
        //ww: marker dla NodeActionResetVisualOrder, że węzły na tym poziomie
        // nie są uporządkowane alfabetycznie
        // metoda areChildrenCustomSorted w BIKClientSingletons sprawdza
        // czy wszystkie węzły mają 0 w visualOrder - to oznacza, że są
        // posortowane alfabetycznie
        node.visualOrder = -1;
    }
}
