﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node]
GO

create procedure sp_insert_into_bik_node(@tree_name varchar(255))
as
	declare @table_folders varchar(255);
	declare @si_id int, @si_parentid int, @si_kind varchar(max), @si_name varchar(max);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount off;
	
	select @tree_id =id 
	from bik_tree
	where name=@tree_name;

	create table #emptyFolders(IID int);
	create table #primaryIds(si_id int);
	create table #secondaryIds(si_id int);

	if(@tree_name!='Raporty')
	begin
		set @table_folders = ' APP_FOLDER where 1=1 ';
		
		if(@tree_name = 'Połączenia')
		begin
			set @kinds = '''DataConnection''';
			exec('insert into #primaryIds
					select SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');
			exec('insert into #secondaryIds 
					select SI_ID
					from APP_UNIVERSE
					where SI_DATACONNECTION__1 in (select * from #primaryIds)');			
		end;
		if(@tree_name = 'Światy obiektów')
		begin
			set @kinds = '''Universe''';
			exec('insert into #primaryIds
					select SI_ID  
					from aaa_global_props
					where SI_PARENTID in (select si_id from ' + @table_folders + ') and SI_KIND in (' + @kinds + ')');
		end;		
	end
	
	if(@tree_name='Raporty')
	begin
		set @table_folders = ' INFO_FOLDER where si_name != ''~Webintelligence''';
		set @kinds = '''Webi'', ''Flash'', ''CristalReports''';
		
	end;
	
	if(@tree_name!='Teradata')
	begin
		EXECUTE(	'DECLARE Folders CURSOR FOR		select si_id, case 
														when SI_PARENTID in(select si_id from ' + @table_folders +') 
															then SI_PARENTID
													else 0 end as SI_PARENTID, SI_KIND, SI_NAME 
													from ' + @table_folders);

	set nocount on;
	
	declare @howMany int = 0, @parent int;
	
	OPEN Folders

	FETCH NEXT FROM Folders
	into @si_id, @si_parentid, @si_kind, @si_name;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		 
		if(@si_id not in (select * from #emptyFolders))
		begin	
			select @parent = @si_id;
			declare @sql nvarchar(1000);
			set @sql = N'select @phowMany  = sum(howMany) 
						 from(	select  COUNT(*) as howMany 
								from' + @table_folders + ' and SI_PARENTID = ' + CONVERT(varchar,@si_id) + ' and si_id not in(select * from #emptyFolders)
					
								union all
		
								select COUNT(*) as howMany
								from aaa_global_props
								where SI_PARENTID = ' + Convert(varchar,@si_id) + 
									' and SI_KIND in (' + @kinds + ')';--) j';
			if(@tree_name = 'Połączenia')
				set @sql = @sql + N' union all 
				
									select COUNT(*) as howMany 
									from APP_UNIVERSE
									where SI_DATACONNECTION__1 = ' + Convert(varchar,@si_id);
			if(@tree_name = 'Światy obiektów' or @tree_name = 'Połączenia')
				set @sql = @sql + N' union all
				
									select COUNT(*) as howMany
									from INFO_WEBI
									where SI_UNIVERSE__1 = ' + convert(varchar,@si_id);
			set @sql = @sql + ') j';
									
			exec sp_executesql @sql, N'@phowMany int OUTPUT', @phowMany = @howMany OUTPUT;
				
		while(@howMany=0 and @parent not in(select * from #emptyFolders))
		begin
				declare @pom int;
				insert into #emptyFolders values(@parent)
				set @sql = 'select @ppom  = SI_PARENTID 
							from ' + @table_folders + ' and si_id = ' + convert(varchar,@parent);
				exec sp_executesql @sql, N'@ppom int OUTPUT', @ppom = @pom OUTPUT;
				
				select @parent = @pom;
			
				if(@parent != 0)
				begin
				
				set @sql = N'select @phowMany  = sum(howMany) 
							 from(	select  COUNT(*) as howMany from' + @table_folders + ' and SI_PARENTID = ' + CONVERT(varchar,@parent) + ' and si_id not in(select * from #emptyFolders)
					
									union all
		
									select COUNT(*) as howMany
									from aaa_global_props
									where SI_PARENTID = ' + Convert(varchar,@parent) + 
										' and SI_KIND in (' + @kinds + ')';
				if(@tree_name = 'Połączenia')
					set @sql = @sql + N'union all 
					
									select COUNT(*) as howMany 
									from APP_UNIVERSE
									where SI_DATACONNECTION__1 = ' + Convert(varchar,@parent);
									
				if(@tree_name = 'Światy obiektów' or @tree_name = 'Połączenia')
					set @sql = @sql + N' union all
					
									select COUNT(*) as howMany
									from INFO_WEBI
									where SI_UNIVERSE__1 = ' + convert(varchar,@parent);
									
				set @sql = @sql + ') j';
					
					
				exec sp_executesql @sql, N'@phowMany int OUTPUT', @phowMany = @howMany OUTPUT;
								
				end
				else
					set @howMany=1;
		end;
		end;

	FETCH NEXT FROM Folders
	into @si_id, @si_parentid, @si_kind, @si_name;

	END

CLOSE Folders;
DEALLOCATE Folders;
end
create table #pomTable (si_parentid int, id int, si_id int);
		
declare @node_kind_id int;
set @sql = N'';
if(@tree_name = 'Teradata')
	set @sql = N'DECLARE RepoEntity CURSOR FOR		select id as SI_ID, parent_id as SI_PARENTID, type as SI_KIND, name as SI_NAME
													from aaa_teradata_object
													where parent_id is not null or name like ''VD_US_%''';
else
begin
	
	set @sql = N'DECLARE RepoEntity CURSOR FOR		select si_id, case 
															when SI_PARENTID in(select si_id from ' + @table_folders +') 
																then SI_PARENTID
															else 0 end as SI_PARENTID, SI_KIND, SI_NAME 
													from ' + @table_folders + 
													
													'union all

													select SI_ID, SI_PARENTID, SI_KIND, SI_NAME
													from aaa_global_props
													where SI_PARENTID in (select si_id from ' + @table_folders + ') 
													and SI_KIND in ('+ @kinds +')';
		if(@tree_name = 'Połączenia')
			set @sql = @sql + N'union all		  

								select SI_ID, SI_DATACONNECTION__1 as SI_PARENTID, SI_KIND, SI_NAME 
								from APP_UNIVERSE
								where SI_DATACONNECTION__1 in (select * from #primaryIds)
	
								union all
								
								select SI_ID, SI_UNIVERSE__1 as SI_PARENTID, SI_KIND, SI_NAME
								from INFO_WEBI
								where SI_UNIVERSE__1 in (select * from #secondaryIds)';
							
		if(@tree_name = 'Światy obiektów')
			set @sql = @sql + N'union all
			
								select SI_ID, SI_UNIVERSE__1 as SI_PARENTID, SI_KIND,  SI_NAME
								from INFO_WEBI
								where SI_UNIVERSE__1 in (select * from #primaryIds)';
end;					
EXECUTE(@sql);
														
--otwieram kursor
set nocount on;
OPEN RepoEntity;

FETCH NEXT FROM RepoEntity
into @si_id, @si_parentid, @si_kind, @si_name;

WHILE @@FETCH_STATUS = 0
BEGIN
		if(@si_id not in(select * from #emptyFolders))
		begin
				if(isnull((	select id
					from bik_node_kind
					where code=@si_kind),'')!='')
				begin
					select @node_kind_id = id
					from bik_node_kind
					where code=@si_kind;
				end
				else
				begin
				--tutaj musze cos zmienic
					insert into bik_node_kind (code,caption) values(@si_kind,@si_kind);
					select @node_kind_id = SCOPE_IDENTITY();
				end
				--nie by�o wcze�niej tego si_Id
				if(@si_id not in (select obj_id from bik_node where tree_id = @tree_id and @si_id=obj_id))
				begin
					insert into bik_node (parent_node_id, node_kind_id, name, tree_id, obj_id)
					values (null, @node_kind_id, @si_name, @tree_id, @si_id);
				
					insert into #pomTable (si_parentid, id, si_id) select @si_parentid, SCOPE_IDENTITY(), @si_id;
				end
				--by�o to si_id
				if(@si_id in (select obj_id from bik_node where tree_id = @tree_id and @si_id=obj_id))
				begin
					update bik_node 
					set parent_node_id = null, node_kind_id = @node_kind_id, name = @si_name,
						 tree_id = @tree_id
					where obj_id = @si_id and @tree_id = tree_id;
						
					insert into #pomTable (si_parentid, id, si_id) values (@si_parentid,(select id from bik_node where obj_id=@si_id and tree_id = @tree_id), @si_id)
				end
		end;
   FETCH NEXT FROM RepoEntity
   into @si_id, @si_parentid, @si_kind, @si_name;

END

CLOSE RepoEntity;
DEALLOCATE RepoEntity;

set nocount on;

drop table #primaryIds;
drop table #secondaryIds;
drop table #emptyFolders;

update bik_node 
set parent_node_id=(select bk.id from bik_node bk where bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id)
from #pomTable as pt
where bik_node.id = pt.id and bik_node.tree_id = @tree_id;

delete from bik_node
where obj_id not in(select si_id from #pomTable) and tree_id = @tree_id;
				
drop table #pomTable;

end