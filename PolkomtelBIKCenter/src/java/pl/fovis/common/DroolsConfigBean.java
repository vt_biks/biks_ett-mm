/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author ctran
 */
public class DroolsConfigBean implements Serializable {

    public int isActive;
    public String drlFolder;
    public String frequency;
    public String startHour;
    public String startDate;
//    public String inspectedTreeIds;
    public String lastCheckedChangeId;
}
