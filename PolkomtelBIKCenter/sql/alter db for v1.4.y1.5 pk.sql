
exec sp_check_version '1.4.y1.5';
go
----------------------------------------------------
----------------------------------------------------
----------------------------------------------------
-- Poprawki dla Lisy:
-- 1. dostępność kategorii(-zacji) w wynikach 
-- 		wyszukiwania
-- 2. dodanie parametru określającego dostępność 
--		kolumn
----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

declare @tree_id int = dbo.fn_tree_id_by_code('DictionaryDWH')
declare @filterTxt varchar(max) = 'n.tree_id = ' + cast(@tree_id as varchar(20))
   exec sp_verticalize_node_attrs_inner @filterTxt
go

ALTER TABLE bik_lisa_dict_column
ADD access_level INTEGER DEFAULT 2;
go

UPDATE bik_lisa_dict_column
set access_level = (case when is_active = 1 then 2 else 0 end);
go

ALTER TABLE bik_lisa_dict_column
DROP column is_active ;
go   

create table bik_lisa_logs(
	id int identity(1,1) not null primary key,
	system_user_id int not null, 
	time_added datetime default GETDATE() not null,
	teradata_login varchar(64) not null,
	dictionary_name varchar(128) not null,
	info varchar(512)
	foreign key ( system_user_id ) references bik_system_user (id)
);
go
   
declare @treeOfTrees_id int = dbo.fn_tree_id_by_code('TreeOfTrees')   
update bik_node set name = 'NAS' where obj_id = 'admin:lisa' and tree_id = @treeOfTrees_id;
go


exec sp_update_version '1.4.y1.5', '1.4.y1.6';
go
