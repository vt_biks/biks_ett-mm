/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.LameUtils;
import commonlib.LameUtils.SqlFlavor;
import java.sql.Time;
import java.util.Date;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import simplelib.IValueFormatter;

/**
 *
 * @author wezyr
 */
public abstract class SqlValueFormatter implements IValueFormatter {

    // d is not null!
    protected abstract String formatDate(Date d);

    // t is not null!
    protected abstract String formatTime(Time t);

    // iter is not null!
    protected <T> String formatIterable(Iterable<T> iter) {
        return BeanConnectionUtils.mergeAsSqlStrings(this, iter);
    }

    // e is not null!
    protected String formatEnum(Enum e) {
        return "\'" + e.name() + "\'";
    }

    // b is not null!
    protected String formatBoolean(Boolean b) {
        return b ? "1" : "0";
    }

    protected String formatOther(Object o) {
        return LameUtils.toSqlString(o, getSqlFlavor());
    }

    protected String formatSingleValue(Object o) {
        if (o instanceof Time) {
            return formatTime((Time) o);
        } else if (o instanceof Date) {
            return formatDate((Date) o);
        } else if (o instanceof Enum) {
            return formatEnum((Enum) o);
        } else if (o instanceof Boolean) {
            return formatBoolean((Boolean) o);
        }
        return formatOther(o);
    }

    public String valToString(Object o) {
        Iterable<?> iter = LameUtils.makeIterable(o, null);
        if (iter != null) {
            return formatIterable(iter);
        }
        return formatSingleValue(o);
    }

    protected SqlFlavor getSqlFlavor() {
        return SqlFlavor.Other;
    }
}
