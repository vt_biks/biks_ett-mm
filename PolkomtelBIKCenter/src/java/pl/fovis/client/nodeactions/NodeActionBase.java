/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import pl.fovis.client.AntiNodeRefreshingCallback;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.BOXIServiceAsync;
import pl.fovis.client.ICheckEnabledByDesign;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.BlogAuthorBean;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.TreeBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public abstract class NodeActionBase implements INodeAction, ICheckEnabledByDesign {

//    public static final Set<String> multiLevelNodeKinds = BaseUtils.paramsAsSet(
//            "MetadataFolder",
//            "DocumentsFolder",
//            "TaxonomyEntity",
//            "GlossaryCategory",
//            "UsersGroup");
    protected INodeActionContext ctx;
    protected TreeNodeBean node;
    protected EntityDetailsDataBean data;
    protected INodeClipboard clipboard;
    protected TreeBean treeBean;
//    protected ITreeNodeLogic nodeLogic;
    protected NodeKindBean nodeKind;
    protected NodeKindBean treeNodeKind;

    protected final boolean isUserLoggedIn() {
        return BIKClientSingletons.isUserLoggedIn();
    }

    // builtin node or tree is readonly (metadata etc.)
    // null node is readonly
    // every node is readonly when user is not logged in
    protected final boolean isNodeReadOnlyAndBuiltIn() {
        if (!isAdminOrExpertOrAuthorOrEditor()) {
            return true;
        }

        return isNodeBuiltIn() || isTreeReadOnly();
    }

    // tree is readonly (metadata etc.)
    // null node is readonly
    // every node is readonly when user is not logged in
    protected final boolean isNodeReadOnly() {
        if (!isAdminOrExpertOrAuthorOrEditor()) {
            return true;
        }

        return isTreeReadOnly();
    }

    /*
     * wszystkie warunki muszą być spełnione: 1. dla niewbudowanych i 2.
     * (niepodlinkowanych lub podlinkowanych jako root) i 3. drzew
     * modyfikowalnych
     */
    protected final boolean isNodeNotLinkedUnderRootNorBuiltinNorReadOnlyNode() {
        return !(isNodeLinkedUnderRoot() || isNodeReadOnlyAndBuiltIn());
    }
    /*
     * wszystkie warunki muszą być spełnione: 1. (niepodlinkowanych lub podlinkowanych jako root)
     * i 2. drzew modyfikowalnych
     */

    protected final boolean isNodeNotLinkedUnderRootNorReadOnlyNode() {
        return !(isNodeLinkedUnderRoot() || isNodeReadOnly());
    }

    /*
     * wszystkie warunki muszą być spełnione: 1. dla niewbudowanych i 2.
     * niepodlinkowanych i 3. drzew modyfikowalnych
     */
    protected final boolean isNodeNotLinkedAnyhowNorBuiltinNorReadOnlyNode() {
        return !(isNodeLinkedAnyhow() || isNodeReadOnlyAndBuiltIn());
    }

    /*
     * wszystkie warunki muszą być spełnione: 1. dla zalogowanych i 2.
     * niepodlinkowanych
     */
    protected final boolean isNodeNotLinkedAnyhow() {
        return !(isNodeLinkedAnyhow() || !(isAdminOrExpertOrAuthorOrEditor() || isNodeAuthorOrEditor()));
    }

    /*
     * wszystkie warunki muszą być spełnione: 1. dla niepodlinkowanych i 2.
     * drzew modyfikowalnych i 3. węzeł musi być typu z tablicy nodeKinds
     */
    protected final boolean isNodeNotLinkedAnyhowNorReadOnlyNodeAndOneOfTheKind(String... nodeKindCodes) {
        if (isNodeLinkedAnyhow() || isNodeReadOnlyAndBuiltIn()) {
            return false;
        }
        return isNodeOneOfTheKind(false, nodeKindCodes);
    }

    protected final boolean isNodeNotLinkedAnyhowNorReadOnlyTreeAndOneOfTheKind(String... nodeKindCodes) {
        if (isNodeLinkedAnyhow() || isTreeReadOnly()) {
            return false;
        }

        return isNodeOneOfTheKind(false, nodeKindCodes);
    }

    protected final boolean isAuthorOfTreeAndOneOfTheKind(String... nodeKindCodes) {
        return isLoggedUserAuthorOfTree() && isNodeOneOfTheKind(false, nodeKindCodes);
    }

    protected final boolean isNodeAllowLinking() {
        //ostateczny tryb edycji wszystkiego, wszystko wszędzie możemy podlinkowywać
        if (isInUltimateEditMode()) {
            return true;
        }

        if (isNodeLinkedAnyhow() || isTreeReadOnly()) {
            return false;
        }

        if (BIKClientSingletons.allowLinkingInDocumentation() && node.treeKind.equals(BIKConstants.TREE_KIND_DOCUMENTS)) {
            return true;
        }

        return BIKClientSingletons.allowTreeKindLinking(node.treeKind) && isNodeBranch() && isNodeHasLink();
    }

    protected final boolean isNodeFromDynamicTree() {
        return BIKClientSingletons.isTreeKindDynamic(node.treeKind);
    }

    protected final boolean isNodeBranch() {
        return isNodeFromDynamicTree()
                && !BIKClientSingletons.getNodeKindsForTreeKind(node).isEmpty();
    }

    protected final boolean isNodeHasLink() {
        return isNodeFromDynamicTree()
                && !BIKClientSingletons.getNodeKindsForTreeKindHasLink(node.treeKind, node.nodeKindId).isEmpty();
    }

    protected final boolean isNodeLeaf() {
        return isNodeFromDynamicTree() && BIKClientSingletons.getTreeKindBeanByCode(node.treeKind).leafNodeKindId == node.nodeKindId;
    }

    protected final boolean isNodeFolder() {
        return nodeKind != null && nodeKind.isFolder;
    }

    /*
     * wszystkie warunki muszą być spełnione: 1. dla niepodlinkowanych i 2.
     * drzew modyfikowalnych i 3. węzeł musi być folderem
     */
    protected final boolean isNodeNotLinkedAnyhowNorReadOnlyTreeAndNodeIsFolder() {
//        boolean editor = isEditor();
//        boolean iseff = isEffectiveExpert();
//        boolean isauthor = isLoggedUserAuthorOfTree();
//        boolean istread = isTreeReadOnly();
        if (isNodeLinkedAnyhow() || isTreeReadOnly() || !(isEffectiveExpert() || isLoggedUserAuthorOfTree() || isEditor())) {
            return false;
        }

        return isNodeFolder();
    }

    /*
     * wszystkie warunki muszą być spełnione: 1. niepodlinkowanych i 2. drzew
     * modyfikowalnych i 3. w schowku jest jakiś węzeł 4. nie wolno wklejać pod
     * siebie (bezpośrednio i pośrednio) i 5. węzeł w którym wklejamy musi być
     * folderem
     */
    protected final boolean isNodeNotLinkedAnyhowNorReadOnlyTreeNorTheSameNorDescendantOfClipboarNodeAndNodeIsFolder() {
        return isNodeNotLinkedAnyhowNorReadOnlyTreeAndNodeIsFolder()
                && clipboard.getNode() != null && !isNodeTheSameOrDescendantOf(clipboard.getNode());
    }

    protected final boolean isLoggedUserIsProperAuthorOfBlogEntry() {
        return isLoggedUserInBlogAuthors(false) && data.blogEntry.userId == BIKClientSingletons.getLoggedUserId()//BIKClientSingletons.getLoggedUserBean().userId//BIKClientSingletons.getLoggedUserId()
                || isLoggedUserInBlogAuthors(true)
                //                || isAppAdmin()
                //ww->authors: przerabiam na eksperta
                || isEffectiveExpert()
                || isNodeAuthorOrEditor();
    }

    protected final boolean isBikUserLoggedIn() {
        return BIKClientSingletons.isBikUserLoggedIn();
    }

    protected final boolean isSystemUserBikUserLoggedIn() {
        return BIKClientSingletons.isSystemUserBikUserLoggedIn();
    }

    protected final boolean isSocialUserLoggedInWithInfo() {
        if (!BIKClientSingletons.mustHaveSocialUser()) {
            return true;
        } else {
            boolean isSocialLoggedIn = BIKClientSingletons.isSystemUserBikUserLoggedIn();

            if (!isSocialLoggedIn) {
                String detailMsg;

                //ww->authors: tu zostaje AppAdmin!
                if (isAppAdmin()) {
                    detailMsg = I18n.proszePrzypiszSobieUzutkownSlole.get() /* I18N:  */;
                } else {
                    detailMsg = I18n.proszeZwrocicSieDoAdministOPrzyp.get() /* I18N:  */;
                }

                Window.alert(I18n.uWAGAWZwiazkuZNowymiFunkcjonJaki.get() /* I18N:  */ + "\n"
                        + "\n\n"
                        + detailMsg);

            }
            return isSocialLoggedIn;
        }
    }

    /*
     * wszystkie warunki muszą być spełnione: 1. dla niepodlinkowanych i 2.
     * drzew modyfikowalnych i 3. węzeł musi być typu BlogEntity i 4. zalogowany
     * użytkownik musi być adminem bloga lub autorem wpisu i autorem bloga
     */
    protected final boolean isNodeNotLinkedAnyhowNorReadOnlyNodeAndBlogEntryNodeAndUserInProperAuthor() {
        return isNodeNotLinkedAnyhowNorReadOnlyNodeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_BLOG_ENTRY)
                && isLoggedUserIsProperAuthorOfBlogEntry()
                && isBikUserLoggedIn();
    }

    protected final boolean isNodeTheSameOrDescendantOf(TreeNodeBean ancestor) {
        return node != null && node.branchIds.startsWith(ancestor.branchIds);
    }

    protected final boolean isNodeOneOfTheKind(boolean okForNull, String... nodeKindCodes) {
        if (node == null) {
            return okForNull;
        }

        for (String okNodeKindCode : nodeKindCodes) {
            if (okNodeKindCode.equals(node.nodeKindCode)) {
                return true;
            }
        }

        return false;
    }

    protected final boolean isInBlogsTree() {
        return treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_BLOGS);
    }

    protected final boolean isInMenuConfigurationTree() {
        return treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_META_BIKS_MENU_CONF);
    }

    protected final boolean isInUserRolesTree() {
        return treeBean.code.equals(BIKGWTConstants.TREE_CODE_USER_ROLES);
    }

    /*
     * jesteśmy w drzewku blogi, akcja dla node==null
     */
    protected final boolean isInBlogsTreeAndNodeNull() {
        return node == null && isInBlogsTree();
    }

    /*
     * wszystkie warunki muszą być spełnione: 1. dla niepodlinkowanych i 2.
     * drzew modyfikowalnych i 3. węzeł musi być typu Blog i 4. zalogowany
     * użytkownik jest autorem bloga
     */
    protected final boolean isNodeNotLinkedAnyhowNorReadOnlyTreeAndBlogNodeAndUserInAuthors() {
        return isNodeNotLinkedAnyhowNorReadOnlyTreeAndOneOfTheKind(BIKGWTConstants.NODE_KIND_BLOG)
                && (isLoggedUserInBlogAuthors(false) || isNodeAuthorOrEditor())
                && isBikUserLoggedIn();
    }

    protected final boolean isLoggedUserInBlogAuthors(boolean requireAdmin) {
        if (data == null || data.blogAuthors == null || !isAdminOrExpertOrAuthorOrEditor()/*
                 * isUserLoggedIn()
                 */) {
            return false;
        }

        //ww->authors: to było poniżej fora (to słabe?), a generalnie tutaj
        // chyba dopuszczamy też eksperta?
//        if (isAppAdmin()) {
        if (isEffectiveExpert()) {
            return true;
        }

        for (BlogAuthorBean bab : data.blogAuthors) {
            if (bab.userId == BIKClientSingletons.getLoggedUserId()//BIKClientSingletons.getLoggedUserBean().userId
                    && (!requireAdmin || bab.isAdmin)) {
                return true;
            }
        }

        return false;
    }

    protected void doSetup() {
        // no-op here
    }

    // null node is builtin!
    // every node is builtin when user is not logged in
    protected boolean isNodeBuiltIn() {
        if (!isAdminOrExpertOrAuthorOrEditor()/*
                 * isUserLoggedIn()
                 */) {
            return true;
        }

        return node == null || node.isBuiltIn;
//        return node != null && (node.nodeKindCode.equals("DocumentsDefaultFolder")
//                || treeBean.treeKind.equals("Glossary")
//                && node.nodeKindCode.equals("GlossaryCategory")
//                && node.name.equals("Słowa kluczowe"));
    }

    protected final boolean isTreeReadOnlyInner() {
        return !BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(treeBean.treeKind)
                && !BIKConstants.TREE_KIND_META_BIKS_MENU_CONF.equalsIgnoreCase(treeBean.treeKind)
                && (treeBean.nodeKindId == null || treeBean.nodeKindId != null && treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_QUALITY_METADATA));
//                (treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_METADATA) ||
//                treeBean.treeKind.equals(BIKGWTConstants.TREE_KIND_DQCMETADATA)
//                );
    }

    // every tree is readonly when user is not logged in
    protected final boolean isTreeReadOnly() {
//        boolean adminOrall = isAdminOrExpertOrAuthorOrEditor();
//        boolean autoredi = isNodeAuthorOrEditor();
        if (!isAdminOrExpertOrAuthorOrEditor() && !isLoggedUserAuthorOfTree() && !isLoggedUserCreatorOfTree()
                && !isLoggedUserBranchAuthor() && !isLoggedUserBranchCreator() && !isLoggedUserIsNonPublicUserInTree() && !isLoggedUserIsNonPublicUserInBranch()) {
            return true;
        }

        return isTreeReadOnlyInner();
    }

    @Override
    public final void setup(INodeActionContext ctx) {
        this.ctx = ctx;
        this.node = ctx.getNode();
        this.clipboard = ctx.getClipboard();
        this.treeBean = BIKClientSingletons.getTreeBeanById(ctx.getTreeId());
//        this.nodeLogic = TreeNodeLogicFactory.getLogic(treeBean.treeKind);
        this.nodeKind = node == null ? null : BIKClientSingletons.getNodeKindById(node.nodeKindId);
        this.treeNodeKind = BIKClientSingletons.getNodeKindById(treeBean.nodeKindId);
        this.data = ctx.getNodeData();

        doSetup();

        BIKClientSingletons.registerOptNodeActionCodes(getOptNodeActionCodes());
    }

//    protected boolean isEnabledWhenLinkedAnyhow() {
//        return false;
//    }
//
//    protected boolean isEnabledWhenLinkedAsRoot() {
//        return false;
//    }
//
//    protected boolean isEnabledWhenLinkedUnderRoot() {
//        return false;
//    }
    protected final boolean isNodeLinkedAsRoot() {
        return node != null && (isNodeLinked() || node.id != getSelectedNodeId()) && node.treeId == getTreeId();
    }

    protected final boolean isNodeLinked() {
        return node.linkedNodeId != null;
    }

    protected final boolean isNodeLinkedUnderRoot() {
        return node != null && node.treeId != getTreeId();
    }

    protected final boolean isNodeLinkedAnyhow() {
        return isNodeLinkedAsRoot() || isNodeLinkedUnderRoot();
    }

    //ww->authors: ostateczny tryb edycji wszystkiego, wszystko można edytować!
    protected boolean isInUltimateEditMode() {
        return BIKClientSingletons.isInUltimateEditMode();
    }

//    protected abstract boolean isActionReadOnly();
    //@Override
    @Override
    public abstract boolean isEnabledByDesign();

    @Override
    public boolean isEnabled() {

        return BIKClientSingletons.isNodeActionEnabledByCustomRightRoles(getOptCurrentNodeActionCode(), getTreeId(), data, this);

//        return isEnabledByDesign();
    }

//    protected boolean isNodeEditableForLoggedUser() {
//        return false;
//    }
//
//    public boolean isEnabledByDesign() {
//
//        if (!isActionReadOnly() && (!BIKClientSingletons.isUserLoggedIn()
//                || !isNodeEditableForLoggedUser())) {
//            return false;
//        }
//
//        boolean isEnabledWhenLinkedAnyhow = isEnabledWhenLinkedAnyhow();
//
//        if (isNodeLinkedAsRoot()) {
//            return isEnabledWhenLinkedAnyhow || isEnabledWhenLinkedAsRoot();
//        }
//
//        if (isNodeLinkedUnderRoot()) {
//            return isEnabledWhenLinkedAnyhow || isEnabledWhenLinkedUnderRoot();
//        }
//
//        return isEnabledWhenNotLinked();
//    }
    public String getButtonCss() {
        return null;
    }

//    protected boolean isEnabledWhenNotLinked() {
//        return true;
//    }
    protected final void refreshRedisplayDataAndSelectRecord(Integer nodeId/*
     * , boolean expand
     */) {
        ctx.refreshRedisplayDataAndSelectRecord(nodeId /*
         * , expand
         */);
    }

    protected void refreshAndRedisplayData() {
        ctx.refreshAndRedisplayData();
    }

    protected void redisplayData() {
        ctx.redisplayData();
    }

    protected int getTreeId() {
        return ctx.getTreeId();
    }

    protected int getSelectedNodeId() {
        return ctx.getSelectedNodeId();
    }

//    @Override
//    public boolean canBeEnabledByStateForThisNode() {
//        return false;
//    }
    protected final void refreshAndRedisplayDataByTreeCode(List<String> results) {
        if (results != null) {
            for (String codeLinkedNode : results) {
                BIKClientSingletons.invalidateTabData(codeLinkedNode);
            }
        }
    }

    //ww->authors: przerabiam na AppAdmina
    protected boolean isAppAdmin() {
        return BIKClientSingletons.isAppAdminLoggedIn(BIKConstants.ACTION_ASSIGN_USER);
//        return (/*
//                 * data.node.treeKind.equals(BIKGWTConstants.TREE_KIND_FAQ) &&
//                 */isAdminOrExpert()/*
//                 * isUserLoggedIn()
//                 */ && BIKCenterUtils.userHasRightRoleWithCode(BIKClientSingletons.getLoggedUserBean(), "Administrator"));//BaseUtils.safeEquals(BIKClientSingletons.getLoggedUserBean().kindName,
//                BIKGWTConstants.USER_KIND_ADMIN));
    }

    protected boolean isEffectiveExpert() {
        return BIKClientSingletons.isEffectiveExpertLoggedIn();
    }

    protected boolean isAdminOrExpertOrAuthorOrEditor() {
        return BIKClientSingletons.isEffectiveExpertLoggedIn() || isNodeAuthorOrEditor();
    }

    protected BOXIServiceAsync getService() {
        return BIKClientSingletons.getService();
    }

    protected AsyncCallback<Void> getAntiTreeRefreshingCallback(String successMsg) {
        return new AntiNodeRefreshingCallback<Void>(node.id, successMsg) {
            @Override
            protected void refreshNode() {
                ctx.refreshAndRedisplayData();
                //ctx.getTreeGrid().deleteNode(nodeId);
                //ctx.addTreeGridNode(node);
            }
        };
    }

    protected boolean areSiblingsCustomSorted() {
        return BIKClientSingletons.areChildrenCustomSorted(ctx.getTreeStorage(), node.parentNodeId);
//        final ITreeRowAndBeanStorage<Integer, Map<String, Object>, TreeNodeBean> storage = ctx.getTreeStorage();
//
//        return BIKClientSingletons.areChildrenCustomSorted(storage, node.parentNodeId, new TreeNodeBean4TreeExtractor());
    }

    protected void sortSiblingsWithDefaultOrder() {
        BIKClientSingletons.sortChildrenWithDefaultOrder(ctx.getTreeStorage(), ctx.getTreeGrid(), node.parentNodeId);
//        final ITreeRowAndBeanStorage<Integer, Map<String, Object>, TreeNodeBean> storage = ctx.getTreeStorage();
//
//        ctx.getTreeGrid().sortChildNodes(node.parentNodeId, new Comparator<Map<String, Object>>() {
//            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
//
//                TreeNodeBean n1 = storage.getBeanById(storage.getTreeBroker().getNodeId(o1));
//                TreeNodeBean n2 = storage.getBeanById(storage.getTreeBroker().getNodeId(o2));
//
//                //ww: foldery przed niefolderami
//                int folderCmpRes = -1 * Boolean.valueOf(n1.isFolder).compareTo(n2.isFolder);
//
//                if (folderCmpRes != 0) {
//                    return folderCmpRes;
//                }
//
//                return String.CASE_INSENSITIVE_ORDER.compare(n1.name, n2.name);
////                int res = n1.visualOrder - n2.visualOrder;
////
////                return res != 0 ? res : String.CASE_INSENSITIVE_ORDER.compare(n1.name, n2.name);
//            }
//        });
    }

    protected void maybeSortSiblingsWithDefaultOrder() {
        BIKClientSingletons.maybeSortChildrenWithDefaultOrder(ctx.getTreeStorage(), ctx.getTreeGrid(), node.parentNodeId);
//        if (!areSiblingsCustomSorted()) {
//            sortSiblingsWithDefaultOrder();
//        }
    }

    protected boolean isNodeAuthorOrEditor() {
        return BIKClientSingletons.isNodeAuthorOrEditorLoggedIn(data);
    }

    protected boolean isEditor() {
        return BIKClientSingletons.isEditorLoggedIn(data);
    }

    protected boolean isLoggedUserAuthorOfTree() {
        return BIKClientSingletons.isLoggedUserAuthorOfTree(getTreeId());
    }

    protected boolean isLoggedUserCreatorOfTree() {
        return BIKClientSingletons.isLoggedUserCreatorOfTree(getTreeId());
    }

    protected boolean isLoggedUserIsNonPublicUserInTree() {
        return BIKClientSingletons.isLoggedUserIsNonPublicUserInTree(getTreeId());
    }

    protected boolean isLoggedUserBranchAuthor() {
        return BIKClientSingletons.isBranchAuthorLoggedIn(node.branchIds);
    }

    protected boolean isLoggedUserBranchCreator() {
        return BIKClientSingletons.isBranchCreatorLoggedIn(node.branchIds);
    }

    protected boolean isLoggedUserIsNonPublicUserInBranch() {
        return BIKClientSingletons.isBranchNonPublicUserLoggedIn(node.branchIds);
    }

//    protected String getLeafNameForDynamicTree() {
//        return getLeafNodeKindForDynamicTree().caption;
//    }
//    protected int getLeafNodeKindIdForDynamicTree() {
//        return getLeafNodeKindForDynamicTree().id;
//    }
//    protected NodeKindBean getLeafNodeKindForDynamicTree() {
//        TreeKindBean bean = BIKClientSingletons.getTreeKindBeanByCode(node.treeKind);
//        return BIKClientSingletons.getNodeKindById(bean.leafNodeKindId);
//    }
//    protected String getBranchNameForDynamicTree() {
//        TreeKindBean bean = BIKClientSingletons.getTreeKindBeanByCode(node.treeKind);
//        return BIKClientSingletons.getNodeKindById(bean.branchNodeKindId).caption;
//    }
//    protected boolean allowChangeBranchToLeaf() {
//        return node.hasNoChildren && node.parentNodeId != null && !isNodeLinkedAnyhow();
//    }
    protected boolean hasChildren() {
        return data.hasChildren;
    }

    protected boolean isConfluenceObject() {
        return isNodeOneOfTheKind(false, BIKConstants.NODE_KIND_CONFLUENCE_SPACE, BIKConstants.NODE_KIND_CONFLUENCE_PAGE, BIKConstants.NODE_KIND_CONFLUENCE_BLOG);
    }

    protected boolean isDQMTest() {
        return isNodeOneOfTheKind(false, BIKConstants.NODE_KIND_DQM_TEST_FAILED, BIKConstants.NODE_KIND_DQM_TEST_INACTIVE, BIKConstants.NODE_KIND_DQM_TEST_NOT_STARTED, BIKConstants.NODE_KIND_DQM_TEST_SUCCESS);
    }

    protected boolean isDQMTree() {
        return BIKConstants.TREE_KIND_QUALITY_METADATA.equals(treeBean.treeKind) && treeBean.nodeKindId != null;
    }

    protected boolean isInDQMAdminMode() {
        return isDQMTree() && isEffectiveExpert();
    }

    // obiekty bezpośrednio dodane z Confluencea, dające się usuwać
    protected boolean isAddedConfluenceObject() {
        return isConfluenceObject() && node.isBuiltIn;
    }

    protected boolean isCustomTreeMode() {
        return BIKClientSingletons.isCustomTreeModeEnable(node.treeKind);
    }

    protected boolean isMetaBiks() {
        return BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(treeBean.treeKind);
    }

    protected Collection<String> getOptAdditionalNodeActionCodes() {
        return null;
    }

    @Override
    public Collection<String> getOptNodeActionCodes() {
        String actCode = getOptCurrentNodeActionCode();
        Collection<String> additionalActCodes = getOptAdditionalNodeActionCodes();
        if (actCode == null && BaseUtils.isCollectionEmpty(additionalActCodes)) {
            return null;
        }

        Set<String> res = new LinkedHashSet<String>();
        if (actCode != null) {
            res.add(actCode);
        }
        if (additionalActCodes != null) {
            res.addAll(additionalActCodes);
        }
        return res;
    }

    @Override
    public String getOptCurrentNodeActionCode() {
        String className = BaseUtils.safeGetSimpleClassName(this);
        return BaseUtils.dropOptionalPrefix(className, "NodeAction");
//        return BaseUtils.dropPrefixOrGiveNull(className, "NodeAction");
    }

//    protected boolean canBeEnabledByStateForThisNodeAndUserLoggedInByDesign() {
//        return false;
//    }
//
//    @Override
//    public final boolean canBeEnabledByStateForThisNodeAndUserLoggedIn() {
//        if (!BIKClientSingletons.isNodeActionEnabledByCustomRightRoles(getOptCurrentNodeActionCode(), ctx.getTreeId(), null)) {
//            return false;
//        }
//
//        return canBeEnabledByStateForThisNodeAndUserLoggedInByDesign();
//    }
}
