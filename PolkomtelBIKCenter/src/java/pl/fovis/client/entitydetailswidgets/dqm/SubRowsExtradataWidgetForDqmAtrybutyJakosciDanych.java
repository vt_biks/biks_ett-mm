/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.entitydetailswidgets.AbstractSubRowsExtradataWidget;
import pl.fovis.client.entitydetailswidgets.ISubRowsDisplayBroker;

/**
 *
 * @author tflorczak
 */
public class SubRowsExtradataWidgetForDqmAtrybutyJakosciDanych extends AbstractSubRowsExtradataWidget {

    @Override
    protected void setupHeaderWidgetStyle(Widget header) {
        super.setupHeaderWidgetStyle(header);
        header.addStyleName("DqmAtrybutyJakosciDanychHeader");
    }

    @Override
    protected String getOptHeaderCaption() {
        return "Atrybuty jakości danych";
    }

    @Override
    protected ISubRowsDisplayBroker getSubRowsDisplayBroker() {
        return new SubRowsDisplayBrokerForDQMAtrybutJakosciDanych(getData().dqmGrupaDanychPotomkowie);
    }
}
