/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

/**
 *
 * @author pmielanczuk
 */
public interface IFoxySessionPrimitivesProvider {

    public String getRequestHeader(String name);

    public void setSessionAttribute(String name, Object val);

    public String getSessionId();

}
