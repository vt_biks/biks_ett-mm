REPLACE PROCEDURE "DB_SBX_NAS"."add_kateg" (
                IN "p_id_typ_kateg" SMALLINT,
                IN "p_nazwa_kateg" VARCHAR(80) CHARACTER SET LATIN,
                IN "p_opis_kateg" VARCHAR(255) CHARACTER SET LATIN,
                IN "p_nazwa_kateg_en" VARCHAR(80) CHARACTER SET LATIN,
                IN "p_opis_kateg_en" VARCHAR(255) CHARACTER SET LATIN,
                OUT "p_new_id_kateg" SMALLINT)
-- ID kategoryzcji w ramach ktorej dodawana jest nowa kategoria
        p_nazwa_kateg varchar(80),      -- Nazwa nowo dodawanej kategorii
        p_opis_kateg varchar(255),      -- Opis nowo dodanej kategorii
        p_nazwa_kateg_en varchar(80),   -- Nazwa nowo dodawanej kategorii
        p_opis_kateg_en varchar(255)    -- Opis nowo dodanej kategorii
        ,OUT p_new_id_kateg smallint    -- zwraca ID nowo utworzonej kategorii
        )
L1: BEGIN
        DECLARE new_id_kateg smallint;

        SELECT max(id_kateg)+1 INTO new_id_kateg
        FROM DB_SBX_NAS.Kateg
        WHERE id_typ_kateg = :p_id_typ_kateg;

        INSERT INTO DB_SBX_NAS.Kateg -- dodanie kategorii
        SELECT
                p_id_typ_kateg AS id_typ_kateg,
                new_id_kateg AS id_kateg,
                p_nazwa_kateg AS nazwa_kateg,
                p_opis_kateg AS opis_kateg,
                current_date AS data_utworzenia,
                current_date AS data_zmiany,
                p_nazwa_kateg_en AS nazwa_kateg_en,
                p_opis_kateg_en AS opis_kateg_en
                ;
                SET             p_new_id_kateg = new_id_kateg;

END L1;




----
----
----

REPLACE PROCEDURE "DB_SBX_NAS"."ch_kateg" (
                IN "p_kateg_rel_table" VARCHAR(255) CHARACTER SET LATIN,
                IN "p_id_typ_kateg" INTEGER,
                IN "p_id_ref" VARCHAR(255) CHARACTER SET LATIN,
                IN "p_id" VARCHAR(255) CHARACTER SET LATIN,
                IN "p_id_kateg" INTEGER,
                IN "p_data_zmiany" DATE,
                IN "p_data_od" DATE)
-- tabela kategoryzacyjna (kateg_%_rel, np. kateg_prod_rel)
        p_id_typ_kateg  int,            -- ID typu kategoryzacji
        p_id_ref varchar(255),                  -- nazwa kolumny identyfikujacej element
s?ownika np. 'id_prod'
        p_id varchar(255),                              -- ID modyfikowanego elementu s?ownika np.
warto?? id_prod 'P1234'
        p_id_kateg int,                                                 -- ID kategorii
        p_data_zmiany date,                     -- data od kt?rej ma obowiazywa? zmiana (czyli
np. dzisiejsza)
        p_data_od date                                          -- Warto?? data_od aktualnego rekordu z tabeli
kategoryzacyjnej (klucz do kateg_%_rel wraz z id_kateg),
        )
L1: BEGIN

        DECLARE st1, st2 varchar(2048);

        SET st1 = 'UPDATE DB_SBX_NAS.'  || p_kateg_rel_table || ' SET data_do
= ''' || cast( ((p_data_zmiany-1)(format 'YYYY-MM-DD')) as char(10))
|| ''' WHERE id_typ_kateg= ' || cast(p_id_typ_kateg as varchar(8))  ||
' AND ' || p_id_ref || ' = ''' || p_id  || ''' AND data_od = ''' ||
cast((p_data_od(format 'YYYY-MM-DD')) as char(10)) || '''';
        SET st2 = 'INSERT INTO DB_SBX_NAS.'  || p_kateg_rel_table || ' SELECT
' || cast(p_id_typ_kateg as varchar(8)) || ' AS id_typ_kateg, '  ||
cast(p_id_kateg as varchar(8))  ||  ' AS id_kateg, ''' || p_id || '''
AS ' || p_id_ref || ', ''' || cast((p_data_zmiany(format
'YYYY-MM-DD')) as char(10)) || ''' AS data_od, ''' ||
cast((p_data_zmiany(format 'YYYY-MM-DD')) as char(10)) || ''' AS
data_zmiany, '''  || cast((p_data_zmiany(format 'YYYY-MM-DD')) as
char(10)) || ''' AS data_utworzenia, ''3000-12-31'' AS data_do ';

        BEGIN REQUEST
                CALL DBC.SYSEXECSQL (:st1);
        END REQUEST;

        INSERT INTO DB_SBX_NAS.Log_sql
        VALUES (:st1);

        BEGIN REQUEST
                CALL DBC.SYSEXECSQL (:st2);
        END REQUEST;

        INSERT INTO DB_SBX_NAS.Log_sql
        VALUES (:st2);

END L1;




----
----
----
----


REPLACE PROCEDURE "DB_SBX_NAS"."spSample1" (
                INOUT "IOParam1" INTEGER,
                OUT "OParam2" INTEGER)

L1: BEGIN
        DECLARE K INTEGER DEFAULT 10;
                L2: BEGIN
                        DECLARE K INTEGER DEFAULT 20;
                        SET OParam2 = K;
                        SET IOParam1 = L1.K;
                END L2;
END L1;

----
----
----

REPLACE PROCEDURE "DB_SBX_NAS"."upd_dict" (
                IN "p_dict_table" VARCHAR(80) CHARACTER SET LATIN,
                IN "p_lista_upd" VARCHAR(1024) CHARACTER SET LATIN,
                IN "p_lista_where" VARCHAR(512) CHARACTER SET LATIN,
                IN "p_ver" INTEGER)
-- nazwa tabeli s?ownikowej np 'prod'
        p_lista_upd varchar(1024),      -- lista podstawie? kolumna=warto?? np,
nazwa_prod = 'xxx', il_miesiecy_plan_taryfowy = 12, dla s?ownik?w
wewrsjonowanych powinna to byc kompletna lista kolumn/wartosci tabeli
        p_lista_where varchar(512),     -- lista do klauzuli where jednoznacznie
identyfikuj?ca modyfikowany element, np id_prod = 'PP1_09B2D', dla
s?ownik?w wersjonowanych musi m.in. zawiera? warto?c data_od
        p_ver int  /*DEFAULT 0  */      -- 1 je?li utworzy? wersj?, 0 wpp
        )
L1: BEGIN
        DECLARE st1, st2 varchar(2048);

        IF p_ver = 0 THEN
                SET st1 = 'UPDATE DB_SBX_NAS.'  || p_dict_table || ' SET ' ||
p_lista_upd  || ' WHERE ' || p_lista_where;

                BEGIN REQUEST
                        CALL DBC.SYSEXECSQL (:st1);
                END REQUEST;

                INSERT INTO DB_SBX_NAS.Log_sql
                VALUES (:st1);

        ELSE
                        SET st1 = 'UPDATE DB_SBX_NAS.'  || p_dict_table || ' SET
data_do=''' || current_date  || ''' WHERE ' || p_lista_where;
                        SET st2 = 'INSERT INTO.' || p_dict_table || 'SELECT ' || p_lista_upd;

                        BEGIN REQUEST
                                CALL DBC.SYSEXECSQL (:st1);
                        END REQUEST;

                        INSERT INTO DB_SBX_NAS.Log_sql
                        VALUES (:st1);

                        BEGIN REQUEST
                                CALL DBC.SYSEXECSQL (:st2);
                        END REQUEST;

                        INSERT INTO DB_SBX_NAS.Log_sql
                        VALUES (:st2);

        END IF;

END L1;
