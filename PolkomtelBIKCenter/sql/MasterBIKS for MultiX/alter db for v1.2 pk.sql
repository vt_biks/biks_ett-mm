------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- 1. zamiana tabeli login na login_database (jeden login moze wystepowac w wielu bazach)
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_mltx_check_version '1.2'
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

create table mltx_login_database(
	id int IDENTITY(1,1) primary key NOT NULL,
	login varchar(255) not null,
	database_id int not null,
	foreign key (database_id) references mltx_database(id),
	unique (login, database_id)
);
go

insert into mltx_login_database
(login, database_id)
select login, database_id
from mltx_login;
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_mltx_update_version '1.2', '1.3'
go
