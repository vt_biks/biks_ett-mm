/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill;

import commonlib.ILameResourceProvider;
import commonlib.LameUtils;
import java.io.InputStream;
import java.util.List;
import pl.foxys.foxymill.tags.ITag;
import pl.foxys.foxymill.templates.FoxyMillParser;
import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.CachedGenericResourceProvider;

/**
 *
 * @author wezyr
 */
public class NamedTemplateByResProvider2 extends CachedGenericResourceProvider<List<ITag>>
        implements IFoxyMillNamedTagsProvider {

    private static final RenderEnvironment renderEnv = FoxyMillParser.defaultRenderEnv;
    private String pathPrefix;

    public NamedTemplateByResProvider2(final ILameResourceProvider resProvider, String pathPrefix) {
        super(BaseUtils.compoundProvider(resProvider, new Projector<InputStream, List<ITag>>() {

            public List<ITag> project(InputStream val) {
                String fileContents = LameUtils.loadAsString(
                        val, "utf-8");
                ParserTask pt = new ParserTask("?", fileContents);
                List<ITag> res = pt.read();
                FoxyMillUtils.prepareTags(res, renderEnv);

                return res;
            }
        }));
        this.pathPrefix = BaseUtils.ensureDirSepPostfix(pathPrefix);
    }

    @Override
    protected String resourceNameToInnerName(String resourceName) {
        return pathPrefix + resourceName + ".html";
    }

    public List<ITag> getParsedTemplate(String templateName) {
        return gainResource(templateName);
    }
}
