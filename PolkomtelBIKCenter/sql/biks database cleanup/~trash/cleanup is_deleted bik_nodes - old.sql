-----------------------------------------------------------------
-----------------------------------------------------------------
-- czyszczenie tabeli bik_node where is_deleted = 1
-----------------------------------------------------------------
-----------------------------------------------------------------

-- select * from bik_node where is_deleted = 1

set nocount on

if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#table_to_cleanup'))
drop table #table_to_cleanup
go

create table #table_to_cleanup (name sysname not null, 
fk_col_name sysname null,
red_table_name sysname null,
red_key_col_name sysname null,
lvl int not null default 0, unique (name, fk_col_name, red_table_name, red_key_col_name))

insert into #table_to_cleanup (name) values ('bik_node')

if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#cleanup_order'))
drop table #cleanup_order
go

create table #cleanup_order (name sysname not null primary key, cuo int not null)

declare @lvl int = 0

while 1 = 1
begin
  print 'starting pass #' + cast(@lvl as varchar(10))
  print 'tables to cleanup:'
  select * from #table_to_cleanup

  declare @rc int, @rc2 int

  insert into #cleanup_order (name, cuo)
  select ttc.name, @lvl
  from #table_to_cleanup ttc
  where not exists (select 1 from vw_ww_foreignkeys fk where ttc.name = fk.ReferencedTableName)
  or not exists 
    (select 1 from vw_ww_foreignkeys fk left join #cleanup_order cuo on fk.table_name = cuo.name
     where ttc.name = fk.ReferencedTableName and cuo.name is null)

  set @rc2 = @@rowcount

  print 'tables added to cleanup queue:'
  select * from #cleanup_order where cuo = @lvl

  delete from #table_to_cleanup where name in (select name from #cleanup_order where cuo = @lvl)

  print 'remaininig tables to cleanup:'
  select * from #table_to_cleanup

  if not exists (select 1 from #table_to_cleanup) break

  declare @clean_me_before table (name sysname not null primary key)
  delete from @clean_me_before
  
  insert into @clean_me_before (name)
  select distinct table_name
  --select * 
  from vw_ww_foreignkeys
  where referencedtablename <> table_name 
  and referencedtablename in (select name from #table_to_cleanup)
  and table_name not in (select name from #cleanup_order)
  and table_name not in (select name from #table_to_cleanup)

  set @rc = @@rowcount
  
  if @rc = 0 and @rc2 = 0 begin
    print 'no new tables to cleanup before, all tables already on cleanup stack, nothing added to cleanup order queue'
    select * from #table_to_cleanup order by name
    break
  end

  insert into #table_to_cleanup (name) select name from @clean_me_before

  set @lvl = @lvl + 1    
end

print 'cleanup order:'
select * from #cleanup_order
order by cuo
