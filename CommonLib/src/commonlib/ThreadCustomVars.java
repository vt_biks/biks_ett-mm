/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class ThreadCustomVars {

//    private static final ILameLogger logger = LameUtils.getMyLogger();
//    private static class ThreadLocalVars extends ThreadLocal<MapHelper<String>> {
//
////        @Override
////        protected MapHelper<String> initialValue() {
////            return MapHelper.makeEmpty();
////        }
//    }
//    private static final ThreadLocalVars vars = new ThreadLocalVars();
    private static final ThreadLocal<Map<String, Object>> vars = new ThreadLocal<Map<String, Object>>();

    public static boolean isInterrupted() {
        return Thread.currentThread().isInterrupted();
    }

//    private static MapHelper<String> getVars() {
//        if (isInterrupted()) {
//            System.out.println("ThreadCustomVars.getVars(): isInterrupted == true!!!");
//            return null;
//        }
//        if (vars == null) {
////            if (logger.isErrorEnabled()) {
////                logger.error("getVars: vars == null!");
////            }
//            System.out.println("ThreadCustomVars.getVars(): vars == null!!!");
//            throw new LameRuntimeException("getVars: vars == null?!!");
//        }
//        return vars.get();
//    }
    public static Object setVar(String varName, Object val) {
        if (isInterrupted()) {
            return null;
        }
        Map<String, Object> m = vars.get();
        if (m == null) {
            if (val == null) {
                vars.remove();
                return null;
            }

            m = new LinkedHashMap<String, Object>();
            vars.set(m);
        }

        if (val != null) {
            return m.put(varName, val);
        }

        // m != null && val == null
        Object oldVal = m.get(varName);

        m.remove(varName);

        if (m.isEmpty()) {
            vars.remove();
        }

        return oldVal;
    }

    public static Object getVar(String varName) {
        if (isInterrupted()) {
            return null;
        }

        Map<String, Object> m = vars.get();
        if (m == null) {
            vars.remove();
            return null;
        }

        return m.get(varName);
    }
}
