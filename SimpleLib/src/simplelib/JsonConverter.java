/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

/**
 *
 * @author wezyr
 */
public class JsonConverter {

    private static final String MAP_KEY_CONTEXT = "<key>";
    protected Stack<String> contextStack = new Stack<String>();
    private Class superClass;
    public boolean suppressDefVals = false;
    public boolean keysAsIdents = false;
    public boolean skipRootBrackets = false;
    private int lastNewLineAt = 0;

    public JsonConverter(Class superClass) {
        this.superClass = superClass;
    }

    protected String convertDate(Date date) {
        return SimpleDateUtils.dateTimeToJson(date);
//        return //"/*date=" + SimpleDateUtils.toISOString(date) + ", " + date + "*/ " +
//                "new Date(" +
//                SimpleDateUtils.getYear(date) + "," +
//                (SimpleDateUtils.getMonth(date) - 1) + "," +
//                SimpleDateUtils.getDay(date) + "," +
//                SimpleDateUtils.getHours(date) + "," +
//                SimpleDateUtils.getMinutes(date) + "," +
//                SimpleDateUtils.getSeconds(date) + "," +
//                SimpleDateUtils.getMillis(date) +
//                ")";
    }

    protected boolean isConvertForKey() {
        return MAP_KEY_CONTEXT.equals(contextStack.peek());
    }

    protected boolean keyAsIdent() {
        return keysAsIdents && isConvertForKey();
    }

    protected boolean isDefaultVal(Object o) {
        if (o == null) {
            return true;
        }
        if (o instanceof Boolean) {
            return !(Boolean) o;
        }
        if (o instanceof Integer) {
            return (Integer) o == 0;
        }
        if (o instanceof Double) {
            return (Double) o == 0;
        }
        if (o instanceof String) {
            return ((String) o).isEmpty();
        }
        return false;
    }

    protected boolean convertSimple(Object o, StringBuilder sb) {
        boolean convertable = isInstanceOfSimpleClass(o);

        if (!convertable) {
            return false;
        }

        String s;
        if (o instanceof RawString) {
            s = ((RawString) o).str;
        } else if (o instanceof String) {
            s = (String) o;
            if (!(keyAsIdent() && BaseUtils.isProperIdent(s))) {
                s = JavaScriptExprBuilder.escapeJavaScriptAndQuote(s);
            }
        } else if (o instanceof Date) {
            Date date = (Date) o;
            s = convertDate(date);
        } else if (o instanceof Enum) {
            s = BaseUtils.safeEnumName((Enum) o);
            if (!(keyAsIdent() && BaseUtils.isProperIdent(s))) {
                s = JavaScriptExprBuilder.escapeJavaScriptAndQuote(BaseUtils.safeEnumName((Enum) o));
            }
        } else {
            s = o.toString();
        }
        sb.append(s);

        return true;
    }

    protected boolean convertArray(Object o, StringBuilder sb) {
        boolean convertable = o instanceof Iterable;

        if (!convertable) {
            return false;
        }

        Iterable iterable = (Iterable) o;

        if (appendBrackets()) {
            sb.append("[");
        }

        boolean first = true;
        int i = 0;
        for (Object innerObj : iterable) {
            if (first) {
                first = false;
            } else {
                sb.append(",");
            }

            convertObject(innerObj, sb, "[" + i + "]");
            i++;
        }

        if (appendBrackets()) {
            sb.append("]");
        }

        return true;
    }

    protected boolean convertCustom(Object o, StringBuilder sb) {
        return false;
    }

    protected void convertMap(Map<?, ?> map, StringBuilder sb) {
        if (appendBrackets()) {
            sb.append("{");
        }

        boolean first = true;
        for (Entry e : map.entrySet()) {
            Object val = e.getValue();
            if (!(suppressDefVals && isDefaultVal(val))) {
                if (!first) {
                    sb.append(",");
                }

                Object key = e.getKey();
                convertObject(key, sb, MAP_KEY_CONTEXT);
                sb.append(":");
                convertObject(val, sb, key + ":<val>");
                first = false;
            }
        }

        if (appendBrackets()) {
            sb.append("}");
        }
    }

    protected boolean convertCompound(Object o, StringBuilder sb) {
        boolean convertable = o instanceof Map || o instanceof INamedPropsBean;

        if (!convertable) {
            return false;
        }

        Map m;
        if (o instanceof Map) {
            m = (Map) o;
        } else {
            m = BaseUtils.namedPropsBeanToMap((INamedPropsBean) o);
        }

        convertMap(m, sb);

        return true;
    }

    protected String getContextInfo() {
        return "context: " + BaseUtils.mergeWithSepEx(contextStack, ".");
    }

    protected boolean isRootObject() {
        return contextStack.size() == 1;
    }

    protected boolean appendBrackets() {
        return !skipRootBrackets || !isRootObject();
    }

    protected void convertObject(Object o, StringBuilder sb, String context) {
        contextStack.push(context);

        if (sb.length() - lastNewLineAt > 10000) {
            sb.append("\n");
            lastNewLineAt = sb.length();
        }

        try {
            if (o == null) {
                sb.append("null");
                return;
            }

            boolean converted = convertCustom(o, sb)
                    || convertSimple(o, sb)
                    || convertArray(o, sb)
                    || convertCompound(o, sb);

            if (!converted) {
                throw new LameRuntimeException("cannot convert object " + o.toString()
                        + " of class " + o.getClass().getName()
                        + ", " + getContextInfo());
            }
        } finally {
            contextStack.pop();
        }
    }

    public void setCompactMode(boolean compact) {
        keysAsIdents = compact;
        skipRootBrackets = compact;
        suppressDefVals = compact;
    }

    public static String convertObject(Object o, Class superClass, boolean compact) {
        JsonConverter converter = new JsonConverter(superClass);
        converter.setCompactMode(compact);
        StringBuilder sb = new StringBuilder();
        converter.convertObject(o, sb);
        return sb.toString();
    }

    public static String convertObject(Object o, Class superClass) {
        return convertObject(o, superClass, false);
    }

    public static String convertObject(Object o) {
        return convertObject(o, (Class) null);
    }

    protected Class getCurrentSuperClass() {
        if (isRootObject()) {
            return superClass;
        } else {
            return null;
        }
    }

    public void convertObject(Object o, StringBuilder sb) {
        convertObject(o, sb, "<root>");
    }

    public static String compactConvertObject(Object o) {
        return convertObject(o, (Class) null, true);
    }

    public static boolean isInstanceOfSimpleClass(Object o) {
        boolean res = o instanceof Integer
                || o instanceof Long
                || o instanceof Date
                || o instanceof String
                || o instanceof Boolean
                || o instanceof Double
                || o instanceof Enum
                || o instanceof RawString;
        return res;
    }
}
