/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikwidgets.PaginationWidgetsHelper;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanGridWidget;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 * @param <T>
 */
public abstract class LogsPageBase<T> extends BikPageMinimalBase {

    protected VerticalPanel vp;
    protected DockLayoutPanel main;
    protected HorizontalPanel hPanel;
    protected PaginationWidgetsHelper pwh;

    public LogsPageBase() {
        this(30);
    }

    public LogsPageBase(int pageSize) {
        super();
        pwh = new PaginationWidgetsHelper(pageSize) {

            @Override
            protected void pageNumChanged() {
                fetchData();
            }
        };
    }

    @Override
    public Widget buildWidgets() {
        vp = new VerticalPanel();
        main = new DockLayoutPanel(Style.Unit.PX);
        hPanel = new HorizontalPanel();
        hPanel.addStyleName("vPanelAdmin");
        hPanel.setSpacing(20);
        main.setHeight("100%");
        main.setWidth("100%");
        vp.setWidth("100%");

        HTML infoHTML;
        hPanel.add(infoHTML = new HTML("<B>" + getHeaderString() + ":</B>"));
        hPanel.setCellVerticalAlignment(infoHTML, HasVerticalAlignment.ALIGN_MIDDLE);
        hPanel.add(createRefreshButton());

        pwh.init();
        hPanel.add(pwh.paginationHp);
        hPanel.setCellHorizontalAlignment(pwh.paginationHp, HasHorizontalAlignment.ALIGN_RIGHT);
        hPanel.setCellVerticalAlignment(pwh.paginationHp, HasVerticalAlignment.ALIGN_MIDDLE);

        main.addNorth(hPanel, BIKClientSingletons.SCROLL_PANEL_SPACING);

        ScrollPanel scPanelFvsm = new ScrollPanel(vp);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(scPanelFvsm, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT - BIKClientSingletons.SCROLL_PANEL_SPACING);
        scPanelFvsm.setStyleName("EntityDetailsPane");

        main.add(scPanelFvsm);

//        show();
        return main;
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        pwh.pageNum = 0;
        fetchData();
    }

    protected PushButton createRefreshButton() {
        PushButton btnRefresh = new PushButton(I18n.odswiez.get() /* I18N:  */);
        NewLookUtils.makeCustomPushButton(btnRefresh);
        btnRefresh.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                fetchData();
            }
        });
        return btnRefresh;
    }

    protected void fetchData() {
        callServiceToGetData(new StandardAsyncCallback<Pair<Integer, List<T>>>() {
            @Override
            public void onSuccess(Pair<Integer, List<T>> result) {
                List<T> rows = result.v2;
                vp.clear();
                NodeAwareBeanGridWidget<T> gw = new NodeAwareBeanGridWidget<T>(true, getDataExtractor() /*new DataSourceStatusBeanExtractor()*/, new HashMap<Integer, String>());
                vp.add(gw.buildGrid(rows, rows, 0, rows.size()));

                pwh.setPageCntByItemCnt(result.v1);
                pwh.refresh();
            }
        });
    }

    protected String getHeaderString() {
        return I18n.logiZOstatnichZasilen.get() /* I18N:  */;
    }

    protected abstract void callServiceToGetData(StandardAsyncCallback<Pair<Integer, List<T>>> callback); // {
//        return BIKClientSingletons.getService().getBikDataSourceStatuses(callback);
    //}

    protected abstract NodeAwareBeanExtractorBase<T> getDataExtractor();
}
