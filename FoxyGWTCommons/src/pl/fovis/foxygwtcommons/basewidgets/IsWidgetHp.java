/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.basewidgets;

import com.google.gwt.user.client.ui.HorizontalPanel;

/**
 *
 * @author pmielanczuk
 */
public abstract class IsWidgetHp extends IsWidgetPanel<HorizontalPanel> {

    @Override
    protected HorizontalPanel createMainWidget() {
        return new HorizontalPanel();
    }
}
