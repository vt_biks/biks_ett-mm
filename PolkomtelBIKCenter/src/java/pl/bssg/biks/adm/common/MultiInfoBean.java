/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.common;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import simplelib.Pair;

/**
 *
 * @author pmielanczuk
 */
public class MultiInfoBean implements Serializable {

    public List<AppInfoBean> apps;
    public String msgsInstanceName;
    public List<DbauLogBean> msgs;
    public String uploadFilesDir;
    public Map<String, Pair<String, String>> dbLogins;
    public Map<String, Set<String>> availableScriptsForDbVers;
    public Map<String, String> checkedSqlFilesDirs;
}
