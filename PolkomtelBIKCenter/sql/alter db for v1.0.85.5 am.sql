﻿exec sp_check_version '1.0.85.5';
go
--------------------------------------------------------------
--------------------------------------------------------------

alter table aaa_universe_obj_tables
	add obj_type int;
go
/*
-> 1 obiekty
-> 2 filtry
*/
--------------------------------------------------------------
update aaa_universe_obj_tables
set obj_type = 1
go
--------------------------------------------------------------
alter table aaa_universe_obj_tables
	add constraint CHECK_obj_type check (obj_type in (1,2))
go
--------------------------------------------------------------

declare @nameFK varchar(255);
select @nameFK = 
fk.ForeignKey_Name
from sysobjects so inner join syscolumns sc on so.id = sc.id
 left join vw_ww_foreignkeys fk on fk.table_name = so.name and fk.name = sc.name
where  so.name like 'aaa_universe_obj_tables'
and fk.Name = 'universe_obj_id'

if(not @nameFK is null)
	exec ('alter table aaa_universe_obj_tables drop constraint ' + @nameFK)
go
--------------------------------------------------------------

alter table bik_sapbo_universe_table
	add name_for_teradata varchar(255)
go
--------------------------------------------------------------
alter table bik_sapbo_universe_table
	add schema_name varchar(255)
go
--------------------------------------------------------------
alter table aaa_universe
	add statistic varchar(500)
go
--------------------------------------------------------------
alter table bik_sapbo_extradata
	add statistic varchar(500)
go
--------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_objects_from_Designer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_into_bik_node_objects_from_Designer]
GO

create procedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]
as
	declare @tree_id int;
	declare @kind_id int;
	declare @dimension_nki int, @measure_nki int, @detail_nki int, @filtr_nki int;
begin--begin procedure

	----wybranie id odpowiedniego drzewa
	select @tree_id = id
	from bik_tree
	where code = 'ObjectUniverses';	
	
	----wybranie id odpowiedniego kind'a
	select @kind_id = id
	from bik_node_kind
	where code = 'UniverseClass';
		
	create table #tmpNodes (si_id varchar(1000) , si_parentid varchar(1000), si_kind_id int, si_name varchar(max), si_description varchar(max));

	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Class
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), 
		case
			when uc.parent_id is null then convert(varchar, u.global_props_si_id)
			else convert(varchar, uc.parent_id) + '|' + convert(varchar, u.global_props_si_id) end as parent_id, 
		@kind_id, uc.name, uc.description
	from aaa_universe_class uc join aaa_universe u on u.id = uc.universe_id 
	
	----wybranie id odpowiedniego kind'a
	--detail
	select @detail_nki = id
	from bik_node_kind
	where code = 'Detail';
	--demension
	select @dimension_nki = id
	from bik_node_kind
	where code = 'Dimension';
	--measure
	select @measure_nki = id
	from bik_node_kind
	where code = 'Measure';
	--filtr
	select @filtr_nki = id
	from bik_node_kind
	where code = 'Filter';
	
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Obj
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), 
			case
				when uo.parent_id is null then convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id)
				else convert(varchar, uo.parent_id)+ '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id)
			end as parent_id, 
			case
				when uo.qualification = 'dsDetailObject' then @detail_nki
				when uo.qualification = 'dsMeasureObject' then @measure_nki
				when uo.qualification = 'dsDimensionObject' then @dimension_nki
				end as node_kind_id,
		 uo.name, uo.description
	from aaa_universe_obj uo join aaa_universe_class uc on uo.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id 
							 
	--wrzucam do tabeli tymczasowej wszystkie dane z Universe_filrt
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar, uf.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id),
		convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id), @filtr_nki, uf.name, nullif(ltrim(rtrim(uf.description)), '') 
	from aaa_universe_filtr uf join aaa_universe_class uc on uf.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id 
							 

	--aktualizacja istniejących węzłów w drzewie							
	update bik_node 
	set parent_node_id = null, node_kind_id = #tmpNodes.si_kind_id, name = #tmpNodes.si_name,
		is_deleted = 0, descr = #tmpNodes.si_description, tree_id = @tree_id
	from #tmpNodes 
	where bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id and bik_node.linked_node_id is null;
	
	--dorzucanie nowych węzłów w drzewie
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select #tmpNodes.si_kind_id, #tmpNodes.si_name, @tree_id, #tmpNodes.si_id, #tmpNodes.si_description
	from #tmpNodes left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
		and bik_node.tree_id = @tree_id
	where bik_node.id is null;	
	
	--uaktualnianie parentów w nodach
	declare @universe_kind_id int;
	select @universe_kind_id = id
	from bik_node_kind
	where code = 'Universe';
	
	update bik_node 
	set parent_node_id= bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id and bik_node.node_kind_id = pt.si_kind_id
		inner join bik_node bk on bk.obj_id = pt.si_parentid and bk.tree_id = @tree_id 
		and  (
		 (pt.si_kind_id=@detail_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@dimension_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@measure_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @kind_id)) or
		 (pt.si_kind_id=@filtr_nki and bk.node_kind_id=@kind_id) or
		 (pt.si_kind_id=@kind_id and (bk.node_kind_id = @kind_id or bk.node_kind_id = @universe_kind_id)))
	where bik_node.linked_node_id is null;
		
	--usuwanie nodów
	update bik_node
	set is_deleted = 1
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and bik_node.node_kind_id in (@kind_id, @detail_nki, @dimension_nki, @measure_nki,
	  @filtr_nki);
	
	drop table #tmpNodes;
	--uzupełnianie danych w tabelach extra 
	--dla object
	create table #tmpObjExtra(text_of_select varchar(max),text_of_where varchar(max), type varchar(155), aggregate_function varchar(50), node_id int)
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select univ_obj.text_of_select, univ_obj.text_of_where, univ_obj.type, univ_obj.aggregate_function, bik_node.id
	from (
	select convert(varchar, uo.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id) as obj_id, 
					uo.text_of_select, uo.text_of_where, uo.type, uo.aggregate_function
			from aaa_universe_obj uo join aaa_universe_class uc on uo.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id ) univ_obj 
			join bik_node on bik_node.obj_id = univ_obj.obj_id and bik_node.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki)-- = @obj_node_kind_id;
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select null, univ_filtr.where_clause, univ_filtr.type, null, bik_node.id
	from (
		select convert(varchar, uf.si_id) + '|' + convert(varchar, uc.si_id) + '|' + convert(varchar, u.global_props_si_id) as obj_id,
			uf.where_clause, uf.type
		from aaa_universe_filtr uf join aaa_universe_class uc on uf.universe_class_id = uc.id
							 join aaa_universe u on u.id = uc.universe_id ) univ_filtr
				 join bik_node on bik_node.obj_id = univ_filtr.obj_id and bik_node.node_kind_id = @filtr_nki
			
	update bik_sapbo_universe_object
	set text_of_select = #tmpObjExtra.text_of_select, text_of_where = #tmpObjExtra.text_of_where, type = #tmpObjExtra.type,  
		           aggregate_function = #tmpObjExtra.aggregate_function 
	from #tmpObjExtra join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id;
	
	insert into bik_sapbo_universe_object(text_of_select, text_of_where, type, aggregate_function, node_id)
	select #tmpObjExtra.text_of_select,#tmpObjExtra.text_of_where, #tmpObjExtra.type, #tmpObjExtra.aggregate_function, #tmpObjExtra.node_id 
	from #tmpObjExtra left join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id
	where bik_sapbo_universe_object.id is null;

	drop table #tmpObjExtra;
	
	--dla tabeli
	
	update bik_sapbo_universe_table
	set name = ut.name, is_alias = ut.is_alias, is_derived = ut.is_derived, original_table = ut.original_table, sql_of_derived_table = ut.sql_of_derived_table, 
		sql_of_derived_table_with_alias = ut.sql_of_derived_table_with_alias, their_id = ut.their_id, universe_node_id = bik_node.id, type = ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connetion uc on u.universe_connetion_id = uc.id
		join aaa_universe_connetion_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where linked_node_id is null and bik_node.node_kind_id = @universe_kind_id
	
	
	insert into bik_sapbo_universe_table(name, is_alias, is_derived, original_table, sql_of_derived_table, sql_of_derived_table_with_alias, their_id, universe_node_id, type)
	select ut.name, ut.is_alias, ut.is_derived, ut.original_table, ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, ut.their_id, 
					bik_node.id as universe_node_id, ucn.name
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
		join aaa_universe_connetion uc on u.universe_connetion_id = uc.id
		join aaa_universe_connetion_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where  bik_node.linked_node_id is null and bsut.name is null and bik_node.node_kind_id = @universe_kind_id

	--usupełnianie name_for_teradata
	update bik_sapbo_universe_table
	set name_for_teradata = replace(name,'.', '|') + '|'
	where type = 'Teradata' and is_derived = 0 and is_alias = 0

	update bik_sapbo_universe_table
	set name_for_teradata = bsut.name_for_teradata
	from bik_sapbo_universe_table join bik_sapbo_universe_table bsut on bik_sapbo_universe_table.original_table = bsut.their_id and 
		bik_sapbo_universe_table.universe_node_id = bsut.universe_node_id
	where bik_sapbo_universe_table.type = 'Teradata' and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 1
	
	update bik_sapbo_universe_table
	set schema_name = left(name_for_teradata, charindex('|',name_for_teradata))
	where  type = 'Teradata' and is_derived = 0
	
	--dla kolumn
	update bik_sapbo_universe_column
	set name = uc.name, type = uc.type, table_id = bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and bik_node.node_kind_id = @universe_kind_id 
	
	insert into bik_sapbo_universe_column(name, type, table_id)
	select uc.name, uc.type, bsut.id
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and bsuc.id is null and bik_node.node_kind_id = @universe_kind_id 
	
	delete 
	from bik_sapbo_universe_table
	from aaa_universe_column uc join aaa_universe_table ut on uc.universe_table_id = ut.id
			join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			join bik_sapbo_universe_table bsut on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
			left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bsut.id
	where linked_node_id is null and uc.id is null and bik_node.node_kind_id = @universe_kind_id
	
	delete 
	from bik_sapbo_universe_table
	from aaa_universe_table ut join aaa_universe u on ut.universe_id = u.id 
			left join bik_node on bik_node.obj_id = convert(varchar, u.global_props_si_id) 
			left join bik_sapbo_universe_table bsut 
		on bsut.their_id = ut.their_id and bsut.universe_node_id = bik_node.id
	where  bik_node.linked_node_id is null and ut.name is null and bik_node.node_kind_id = @universe_kind_id
	
	update bik_sapbo_universe_connection 
	set bik_sapbo_universe_connection.server = aaa_universe_connetion.server, bik_sapbo_universe_connection.user_name = aaa_universe_connetion.user_name,
		bik_sapbo_universe_connection.password = aaa_universe_connetion.password, bik_sapbo_universe_connection.database_source = aaa_universe_connetion.database_source, 
		bik_sapbo_universe_connection.connetion_networklayer_name = aaa_universe_connetion_networklayer.name, bik_sapbo_universe_connection.node_id = bik_node.id
	from aaa_universe_connetion join aaa_universe_connetion_networklayer on aaa_universe_connetion.connetion_networklayer_id = aaa_universe_connetion_networklayer.id
		join bik_node on aaa_universe_connetion.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		join bik_sapbo_universe_connection on  bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections'
	
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id)
	select aaa_universe_connetion.server, aaa_universe_connetion.user_name, aaa_universe_connetion.password, aaa_universe_connetion.database_source, 
		aaa_universe_connetion_networklayer.name, bik_node.id
	from aaa_universe_connetion join aaa_universe_connetion_networklayer on aaa_universe_connetion.connetion_networklayer_id = aaa_universe_connetion_networklayer.id
		join bik_node on aaa_universe_connetion.connetion_name = bik_node.name
		join bik_tree on bik_node.tree_id = bik_tree.id
		left join bik_sapbo_universe_connection on  bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_tree.code = 'Connections' and bik_sapbo_universe_connection.id is null;
		
	update bik_sapbo_extradata
	set statistic = u.statistic
	from bik_sapbo_extradata join bik_node bn on bn.id = bik_sapbo_extradata.node_id
		join bik_node_kind bnk on bn.node_kind_id = bnk.id
		join aaa_universe u on convert(varchar,u.global_props_si_id) = bn.obj_id
	where bnk.code = 'Universe'	
				
	exec sp_delete_linked_nodes_where_orignal_is_deleted;	
	exec sp_node_init_branch_id @tree_id, null;
end--end procedure
GO
--------------------------------------------------------------


--------------------------------------------------------------
--------------------------------------------------------------
exec sp_update_version '1.0.85.5', '1.0.85.7';
go

