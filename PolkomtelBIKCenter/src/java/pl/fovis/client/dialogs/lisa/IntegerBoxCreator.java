/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import pl.fovis.client.entitydetailswidgets.lisa.WidgetWithValue;
import pl.fovis.common.lisa.IntegerLDV;
import pl.fovis.common.lisa.LisaDictionaryValue;
import simplelib.BaseUtils;
import simplelib.IContinuation;

/**
 *
 * @author tbzowka
 */
public class IntegerBoxCreator implements WidgetCreator {

    public WidgetWithValue createWidget() {
        return new WidgetWithValue() {
            TextBox tb = new TextBox();

            public Widget getWidget() {
//                tb.setWidth("100%");
                return tb;
            }

            public void setValue(Object value) {
                if (value instanceof Integer) {
                    tb.setValue(BaseUtils.safeToString(value));
                }
            }

            public LisaDictionaryValue getValue() {
                return new IntegerLDV(BaseUtils.tryParseInteger(tb.getValue()));
            }

            public void setEditable(boolean b) {
                tb.setReadOnly(!b);
            }

            public void addChangeHandler(final IContinuation con) {
                tb.addChangeHandler(new ChangeHandler() {
                    public void onChange(ChangeEvent event) {
                        con.doIt();
                    }
                });
            }

            public void setStyleName(String styleName) {
                tb.setStyleName(styleName);
            }            
            
        };
    }
;
}
