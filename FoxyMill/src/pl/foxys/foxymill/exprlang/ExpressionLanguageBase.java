/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.exprlang;

import simplelib.LameRuntimeException;
import simplelib.TextFilePosition;

/**
 *
 * @author wezyr
 */
public abstract class ExpressionLanguageBase implements IExpressionLanguage {

    protected abstract Class<? extends ParsedExpressionBase> getExprClass();

    public IParsedExpression parseExpression(String exprStr, String name, TextFilePosition filePos) {
        try {
            ParsedExpressionBase expr = getExprClass().newInstance();
            expr.parse(exprStr, name, filePos);
            return expr;
        } catch (Exception ex) {
            throw new LameRuntimeException("error parsing expression:\n" + exprStr
                    + "\n at " + filePos + ", name: " + name, ex);
        }
    }
}
