/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.searchservice;

import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;
import pl.bssg.searchengine.IDocCrawler;
import pl.bssg.searchengine.Indexer;
import pl.fovis.server.BIKCenterConfigObj;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import pl.trzy0.foxy.serverlogic.FoxyAppUtils;
import pl.trzy0.foxy.serverlogic.db.MssqlConnection;
import simplelib.BaseUtils;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;

/**
 *
 * @author ctran
 */
public class IndexTask implements Runnable {

    private static final AtomicBoolean isIndexing = new AtomicBoolean(false);
    private static final ILameLogger logger = LameLoggerFactory.getLogger(IndexTask.class);
    protected static final String LAST_INDEXED_DATE_PROP = "indexing.lastIndexedDate";
    protected static final String PUMP_TASK_RUNNING_PROP = "pumpTaskIsRunning";

    protected String indexDirectory;
    protected String dirForUpload;
    protected INamedSqlsDAO adhocDao;
//    protected Date lastIndexedDate;
    protected Indexer indexer;
    protected IDocCrawler docCrawler;

    public IndexTask(BIKCenterConfigObj cfg) {
        indexDirectory = BiksSearchConstant.getIndexDirectory(cfg);
        dirForUpload = BaseUtils.ensureDirSepPostfix(cfg.dirForUpload);
        adhocDao = FoxyAppUtils.createNamedSqlsDAOFromJar(new MssqlConnection(cfg.connConfig), BiksSearchConstant.ADHOC_SQLS_PATH);
        //        lastIndexedDate = getLastIndexedDate();
        indexer = new Indexer(indexDirectory, BiksSearchConstant.getAnalyzer(), docCrawler = new BiksDocCrawler(adhocDao, dirForUpload));
    }

    public Date getLastIndexedDate() {
        String lastIndexedAppProp = BeanConnectionUtils.execNamedQuerySingleVal(adhocDao, "getAppPropValue", true, "val", LAST_INDEXED_DATE_PROP);
        Long lastIndexedDateLong = BaseUtils.tryParseLong(lastIndexedAppProp, null);
        if (lastIndexedDateLong == null) {
            return BiksSearchConstant.START_OF_ERA;
        }
        return new Date(lastIndexedDateLong);
    }

    protected void saveLastIndexedDate(Date lastIndexedDate) {
//        this.lastIndexedDate = lastIndexedDate;
        adhocDao.execNamedCommand("setAppPropValue", LAST_INDEXED_DATE_PROP, lastIndexedDate.getTime());
    }

    protected boolean isPumpTaskRunning() {
//        this.lastIndexedDate = lastIndexedDate;
        String val = BeanConnectionUtils.execNamedQuerySingleVal(adhocDao, "getAppPropValue", true, "val", PUMP_TASK_RUNNING_PROP);
        Integer res = BaseUtils.tryParseInteger(val, 0);
        return res > 0;
    }

    @Override
    public void run() {
        if (isPumpTaskRunning()) {
            return;
        }
        try {
            isIndexing.set(true);
            long startTime = System.currentTimeMillis();
//            System.out.println("Indeksowanie rozpoczęte : " + new Date());
            boolean indexingAll = false;
            if (getLastIndexedDate() == BiksSearchConstant.START_OF_ERA) {
                System.out.println("UWAGA: Trwa indeksowanie całej bazy");
                indexingAll = true;
                adhocDao.execNamedCommand("recreateAllContent");
                indexer.setReindexAll(true);
            } else {
//                System.out.println("Trwa aktualizacja indeksu bazy...");                
                indexer.setReindexAll(false);
            }
            Date now = new Date();
            adhocDao.execNamedCommand("buildNodeList", indexingAll, getLastIndexedDate(), now);
            docCrawler.setLastIndexedDate(getLastIndexedDate());
            docCrawler.setActualDate(now);
            docCrawler.restartDataProviders();
            indexer.index();
            saveLastIndexedDate(now);
            adhocDao.finishWorkUnit(true);
            if (indexingAll) {
                System.out.println("Indeksowanie całej bazy zakończone");
            }
            long endTime = System.currentTimeMillis();
            if (logger.isInfoEnabled()) {
                logger.info("Indeksowanie: " + (endTime - startTime) / 1000.0);
            }
//            System.out.println("Indeksowanie zakończone: " + new Date());
        } catch (Throwable ex) {
            ex.printStackTrace();
            adhocDao.finishWorkUnit(false);
        } finally {
            isIndexing.set(false);
        }
    }

    public static boolean isIndexing() {
        return isIndexing.get();
    }
}
