/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import simplelib.BaseUtils;
import simplelib.BaseUtils.Projector;
import simplelib.IPredicate;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
// i18n-default: no
public class TreeAlgorithms {

    public static <DN, TN, ID> void buildTree(ITreeBuilder<DN, TN, ID> tb, Collection<DN> dns) {
//        Map<ID, DN> nodeMap = new HashMap<ID, DN>();
//
//        for (DN dn : dns) {
//            ID id = tb.getNodeId(dn);
//            nodeMap.put(id, dn);
//        }
//
//        Map<ID, Set<ID>> childNodeIdsMap = new HashMap<ID, Set<ID>>();
//
//        Set<ID> rootIds = new HashSet<ID>();
//
//        for (DN dn : dns) {
//            ID id = tb.getNodeId(dn);
//            ID parentId = tb.getParentId(dn);
//
//            DN parentNode = nodeMap.get(parentId);
//
//            if (parentNode == null) {
//                rootIds.add(id);
//            }
//
//            Set<ID> children = childNodeIdsMap.get(parentId);
//
//            if (children == null) {
//                children = new HashSet<ID>();
//                childNodeIdsMap.put(parentId, children);
//            }
//            children.add(id);
//        }
        //TreeAlgorithms alg = new TreeAlgorithms();
        /*alg.*/
//        createChildNodes(tb, null, rootIds, nodeMap, childNodeIdsMap/*, null*/);

        if (dns.isEmpty()) {
            return;
        }

        GenericTreeHelper<DN, ID> treeHelper = new GenericTreeHelper<DN, ID>(tb, dns);

        Collection<DN> rootNodes = treeHelper.getChildNodes(null);

        if (tb instanceof ITreeLevelBuilder) {
            @SuppressWarnings("unchecked")
            ITreeLevelBuilder<DN> levelBuilder = (ITreeLevelBuilder<DN>) tb;
            createLevelNodes(levelBuilder, treeHelper, null, rootNodes);
        } else {
            createChildNodes(tb, treeHelper, null, rootNodes);
        }
    }

    public static <DN, ID> void createLevelNodes(ITreeLevelBuilder<DN> tb,
            GenericTreeHelper<DN, ID> treeHelper, DN parentNode,
            Collection<DN> childDataNodes) {

        tb.createChildNodes(childDataNodes, parentNode);

        for (DN dn : childDataNodes) {
            Collection<DN> grandchildDataNodes = treeHelper.getChildNodes(dn);

            if (grandchildDataNodes != null) {
                createLevelNodes(tb, treeHelper, dn, grandchildDataNodes);
            }
        }
    }

    public static <DN, TN, ID> void createChildNodes(ITreeBuilder<DN, TN, ID> tb,
            GenericTreeHelper<DN, ID> treeHelper, TN parentNode,
            Collection<DN> childDataNodes) {

        for (DN dn : childDataNodes) {
            TN childNode = tb.createNode(dn, parentNode);

            Collection<DN> grandchildDataNodes = treeHelper.getChildNodes(dn);

            if (grandchildDataNodes != null) {
                createChildNodes(tb, treeHelper, childNode, grandchildDataNodes);
            }
        }
    }
//    public static <DN, TN, ID> void createChildNodes(ITreeBuilder<DN, TN, ID> tb,
//            TN parentNode,
//            Collection<ID> ids, Map<ID, DN> nodeMap, Map<ID, Set<ID>> childNodeIdsMap
//            /*,
//            List<Integer> list*/) {
//
//        //int i = 0;
//        for (ID id : ids) {
////            System.out.println("" + ids.size());
//            DN dn = nodeMap.get(id);
////            List<Integer> listTmp;
////            if (list == null) {
////                listTmp = new LinkedList<Integer>();
////            } else {
////                listTmp = new LinkedList<Integer>(list);
////            }
////
////            if (i == ids.size() - 1) {
////                listTmp.add(0);
////            } else {
////                listTmp.add(1);
////            }
//            //i++;
//            TN childNode = tb.createNode(dn, parentNode/*, listTmp*/);
//            Set<ID> childIds = childNodeIdsMap.get(id);
//            if (childIds != null) {
//                createChildNodes(tb, childNode, childIds, nodeMap, childNodeIdsMap/*, listTmp*/);
//            }
//        }
//    }
    private static final Pair<Boolean, Boolean> stopProcessing = new Pair<Boolean, Boolean>(false, false);
    private static final Pair<Boolean, Boolean> contProcessing = new Pair<Boolean, Boolean>(true, true);

    // returns <properResults, extendedResults>
    public static <DN> Pair<List<DN>, List<DN>> filter(GenericTreeHelper<DN, ?> treeHelper,
            Collection<DN> allNodes, IPredicate<DN> nodeTester,
            boolean extendWithDescendants) {
        List<DN> res = new ArrayList<DN>();
        for (DN node : allNodes) {
            if (nodeTester.project(node)) {
                res.add(node);
            }
        }

        final Set<DN> extRes = new HashSet<DN>();
        for (DN rn : res) {
            DN parentNode = rn;
            while (parentNode != null && !extRes.contains(parentNode)) {
                extRes.add(parentNode);
                parentNode = treeHelper.getParentNode(parentNode);
            }

            if (extendWithDescendants) {
                if (ClientDiagMsgsStub.isShowDiagEnabled("ww:filterTree")) {
                    ClientDiagMsgsStub.showDiagMsg("ww:filterTree", "will extendWithDescendants" /* I18N: no */);
                }

                int cnt = treeHelper.populateWithDescendants(extRes, rn, new Projector<DN, Pair<Boolean, Boolean>>() {

                    public Pair<Boolean, Boolean> project(DN val) {
                        boolean hasIt = extRes.contains(val);

                        if (ClientDiagMsgsStub.isShowDiagEnabled("ww:filterTree")) {
                            ClientDiagMsgsStub.showDiagMsg("ww:filterTree", "test for node" /* I18N: no */ + "=" + val + ", hasIt=" + hasIt);
                        }

                        return hasIt ? stopProcessing : contProcessing;
                    }
                });

                if (ClientDiagMsgsStub.isShowDiagEnabled("ww:filterTree")) {
                    ClientDiagMsgsStub.showDiagMsg("ww:filterTree", "extendWithDescendants done, added" /* I18N: no */ + ": " + cnt + " " + "descendant(s" /* I18N: no */ + ")");
                }
            }
        }

        return new Pair<List<DN>, List<DN>>(res, new ArrayList<DN>(extRes));
    }

    //ww: obsługuje nulla jako dataNode - wtedy buduje najwyższy poziom i zwraca go
    protected static <DN, TN, ID> List<TN> innerBuildTreeBottomUpChildren(DN dataNode, ITreeBottomUpBuilder<DN, TN, ID> builder,
            GenericTreeHelper<DN, ID> helper) {

        //System.out.println("dataNode.id=" + (dataNode == null ? null : builder.getNodeId(dataNode)) + ", start");
        List<DN> childNodes = helper.getChildNodes(dataNode);
        if (childNodes == null) {
            //System.out.println("dataNode.id=" + (dataNode == null ? null : builder.getNodeId(dataNode)) + ", no data children");
            return null;
        }
        //System.out.println("dataNode.id=" + (dataNode == null ? null : builder.getNodeId(dataNode)) + ", data children cnt="
        //        + childNodes.size());

        List<TN> childTreeNodes = new ArrayList<TN>();
        for (DN childNode : childNodes) {
            TN childTreeNode = innerBuildTreeNodeBottomUp(childNode, builder, helper);
            if (childTreeNode != null) {
                childTreeNodes.add(childTreeNode);
            }
        }
        //System.out.println("dataNode.id=" + (dataNode == null ? null : builder.getNodeId(dataNode)) + ", tree children cnt="
        //        + childTreeNodes.size());
        return childTreeNodes;
    }

    protected static <DN, TN, ID> TN innerBuildTreeNodeBottomUp(DN dataNode, ITreeBottomUpBuilder<DN, TN, ID> builder,
            GenericTreeHelper<DN, ID> helper) {
        return builder.createTreeNode(dataNode, innerBuildTreeBottomUpChildren(dataNode, builder, helper));
    }

    public static <DN, TN, ID> List<TN> buildTreeBottomUp(Collection<DN> dataNodes, ITreeBottomUpBuilder<DN, TN, ID> builder) {
        GenericTreeHelper<DN, ID> helper = new GenericTreeHelper<DN, ID>(builder, dataNodes);
        List<TN> res = innerBuildTreeBottomUpChildren(null, builder, helper);
        //System.out.println("buildTreeBottomUp: res.size=" + (res == null ? null : res.size()));
        return res;
    }

    // dataNode == null -> calculate whole tree (all roots)
    public static <DN, ID> Pair<Integer, Integer> calcMinAndMaxSubtreeLevels(ITreeChildBroker<DN> childBroker,
            DN startNode) {
        List<DN> childNodes = childBroker.getChildNodes(startNode);

        int valOfNode = startNode == null ? 0 : 1;

        int minLvl = 0;
        int maxLvl = 0;

        if (childNodes != null) {
            for (DN childNode : childNodes) {
                Pair<Integer, Integer> chldVal = calcMinAndMaxSubtreeLevels(childBroker, childNode);

                int candidateMinLvl = chldVal.v1;
                int candidateMaxLvl = chldVal.v2;

                if (minLvl == 0 || minLvl > candidateMinLvl) {
                    minLvl = candidateMinLvl;
                }
                if (maxLvl < candidateMaxLvl) {
                    maxLvl = candidateMaxLvl;
                }
            }
        }

        return new Pair<Integer, Integer>(valOfNode + minLvl, valOfNode + maxLvl);
    }

    public static <DN, T> void prettyPrintTreeLevelsDown(Collection<DN> levelNodes, ITreeChildBroker<DN> childBroker,
            int level, Projector<DN, T> captionExtractor) {
        if (levelNodes == null) {
            return;
        }

        int lvlPlus1 = level + 1;
        for (DN node : levelNodes) {

            List<DN> subNodes = childBroker.getChildNodes(node);

            System.out.println(BaseUtils.replicateStr("  ", level) + captionExtractor.project(node) + (subNodes == null ? "" : " (*)"));
            prettyPrintTreeLevelsDown(subNodes, childBroker, lvlPlus1, captionExtractor);
        }
    }

    public static <DN, T> void prettyPrintTree(ITreeChildBroker<DN> childBroker,
            int startLevel, Projector<DN, T> captionExtractor) {
        prettyPrintTreeLevelsDown(childBroker.getChildNodes(null), childBroker, startLevel, captionExtractor);
    }
}
