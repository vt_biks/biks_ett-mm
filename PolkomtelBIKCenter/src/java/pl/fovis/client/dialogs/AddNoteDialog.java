/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import pl.fovis.common.NoteBean;
import simplelib.IParametrizedContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author młydkowski
 */
public class AddNoteDialog extends BaseActionOrCancelDialogEx {

//    protected Label status;
//    protected TextBox tbTytul;
//    protected TextArea tbDescription;
    protected NoteBean note;
    private IParametrizedContinuation<NoteBean> saveCont;
//    private Label lblTitle;
//    private Label lblContent;

    @Override
    protected String getDialogCaption() {
        return I18n.dodajKomentarz.get() /* I18N:  */;
    }

//    @Override
//    protected String getActionButtonCaption() {
//        return "Zapisz";
//    }
//
//    @Override
//    protected void buildMainWidgets() {
//
//        status = new Label();
//        main.add(status);
//        Grid grid = new Grid(2, 2);
//        grid.setCellSpacing(4);
//
//        grid.setWidget(0, 0, lblTitle = new Label("Tytuł:"));
//        grid.setWidget(1, 0, lblContent = new Label("Treść:"));
//        grid.setWidget(0, 1, tbTytul = new TextBox());
//        grid.setWidget(1, 1, tbDescription = new TextArea());
//        lblTitle.setStyleName("addNoteLbl");
//        lblContent.setStyleName("addNoteLbl");
//        tbTytul.setStyleName("addNote");
//        tbDescription.setStyleName("addNote");
//        tbTytul.setWidth("500px");
//        tbDescription.setWidth("500px");
//        tbDescription.setHeight("200px");
//        tbTytul.setText(this.note.title);
//        tbDescription.setValue(this.note.body);
//        tbTytul.addKeyUpHandler(addKeyHandler());
//        tbDescription.addKeyUpHandler(addKeyHandler());
//
//        main.add(grid);
//    }

    @Override
    protected void doAction() {
        note.body = tbDescription.getValue();
        note.title = tbTytul.getText();
        saveCont.doIt(note);
    }

    public void buildAndShowDialog(NoteBean note1, IParametrizedContinuation<NoteBean> saveCont) {
        this.saveCont = saveCont;
        note = note1;
        super.buildAndShowDialog();
        if (tbTytul.getText().equals("") && tbDescription.getText().equals("")) {
            actionBtn.setEnabled(false);
        }
    }

//    public KeyUpHandler addKeyHandler() {
//        return new KeyUpHandler() {
//
//            public void onKeyUp(KeyUpEvent event) {
//                if (tbTytul.getText().equals("") || tbDescription.getText().equals("")) {
//                    actionBtn.setEnabled(false);
//                } else {
//                    actionBtn.setEnabled(true);
//                }
//            }
//        };
//    }

    @Override
    protected String getTitle() {
        return this.note.title;
    }

    @Override
    protected String getBody() {
        return this.note.body;
    }
}
