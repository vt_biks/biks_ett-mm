exec sp_check_version '1.0.10';
go

----------------------------------------------------------------------
----------------------------------------------------------------------


update bik_metapedia
  set node_id = n.id
from bik_metapedia m inner join bik_node n on rtrim(ltrim(m.subject)) = rtrim(ltrim(n.name))
  inner join bik_tree t on (n.tree_id = t.id)
where
  t.name = 'Tezaurus'
go


----------------------------------------------------------------------
----------------------------------------------------------------------

if exists(select * from sysobjects where name = 'vw_ww_foreignkeys' and type = 'v')
  drop view vw_ww_foreignkeys
go

create view vw_ww_foreignkeys as
select 
cstr.name as [ForeignKey_Name],
fk.constraint_column_id as [ID] ,
tbl.name as [Table_Name] ,
cfk.name as [Name] ,
reftab.name As ReferencedTableName,
crk.name as [ReferencedColumn]
from sys.tables as tbl
inner join sys.foreign_keys as cstr
on cstr.parent_object_id=tbl.object_id
inner join sys.foreign_key_columns as fk
on fk.constraint_object_id=cstr.object_id
inner join sys.columns as cfk
on fk.parent_column_id = cfk.column_id
and fk.parent_object_id = cfk.object_id
inner join sys.columns as crk
on fk.referenced_column_id = crk.column_id
and fk.referenced_object_id = crk.object_id
inner join sys.tables reftab on reftab.object_id = cstr.referenced_object_id
--order by [Table_Name] asc , ReferencedTableName, [ForeignKey_Name] asc, [ID] asc
go


----------------------------------------------------------------------
----------------------------------------------------------------------


/*
select so.name as table_name, sc.name as col_name, * 
from sysobjects so inner join syscolumns sc on so.id = sc.id
 left join vw_ww_foreignkeys fk on fk.table_name = so.name and fk.name = sc.name
where sc.name like '%node_id' and so.name like 'bik%'
and so.type = 'U'
and fk.ForeignKey_Name is null
order by so.name, sc.name
*/

--alter table bik_joined_objs add constraint FK_bik_joined_objs_dst_node_id foreign key (dst_node_id) references bik_node (id);
--alter table bik_joined_objs drop constraint FK_bik_joined_objs_dst_node_id;


/*
select 'alter table ' + so.name + ' add constraint FK_' + so.name + '_' + sc.name + ' foreign key (' + sc.name + ') references bik_node (id);
go'
--so.name as table_name, sc.name as col_name, * 
from sysobjects so inner join syscolumns sc on so.id = sc.id
 left join vw_ww_foreignkeys fk on fk.table_name = so.name and fk.name = sc.name
where sc.name like '%node_id' and so.name like 'bik%'
and so.type = 'U'
and fk.ForeignKey_Name is null
order by so.name, sc.name
*/


alter table bik_joined_objs add constraint FK_bik_joined_objs_dst_node_id foreign key (dst_node_id) references bik_node (id);
go
alter table bik_joined_objs add constraint FK_bik_joined_objs_src_node_id foreign key (src_node_id) references bik_node (id);
go
alter table bik_metapedia add constraint FK_bik_metapedia_node_id foreign key (node_id) references bik_node (id);
go


----------------------------------------------------------------------
----------------------------------------------------------------------

exec sp_update_version '1.0.10', '1.0.11';
go
