/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

/**
 *
 * @author ctran
 */
public class DriverNameConstants {

    public static final String TERADATA = "com.teradata.jdbc.TeraDriver";
    public static final String POSTGRESQL = "org.postgresql.Driver";
    public static final String SQLSERVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    public static final String DB2 = "com.ibm.db2.jcc.DB2Driver";
    public static final String MYSQL = "com.mysql.jdbc.Driver";
    public static final String ORACLE = "oracle.jdbc.driver.OracleDriver";
}
