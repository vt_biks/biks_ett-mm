/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import pl.bssg.biks.adm.common.AppInfoBean;
import pl.bssg.biks.adm.common.AuthenticationDataBean;
import pl.bssg.biks.adm.common.BIKSAdmRuntimeException;
import pl.bssg.biks.adm.common.BIKSAdmUtils;
import pl.bssg.biks.adm.common.BIKSAdmUtils.NeedToUpgrade;
import pl.bssg.biks.adm.common.DbauLogBean;
import pl.bssg.biks.adm.common.InstanceInfoBean;
import pl.bssg.biks.adm.common.MultiInfoBean;
import pl.fovis.foxygwtcommons.DoNothingAsyngCallback;
import pl.fovis.foxygwtcommons.FoxyClientSingletons;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.InlineFoxyNotificationHandlerExBase;
import pl.fovis.foxygwtcommons.ShowablePasswordTextBox;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.i18nsupport.EntryPointWithI18nSupport;
import simplelib.BaseUtils;
import simplelib.INamedTypedPropsBean;
import simplelib.INamedTypedPropsBeanBroker;
import simplelib.Pair;
import simplelib.SimpleDateUtils;

/**
 *
 * @author pmielanczuk
 */
// i18n-default: no
public class BIKSAdmEntryPoint extends EntryPointWithI18nSupport {

//    public static final String RETRIEVING_MSG = "...Retrieving...";
//    protected Label lblAppName = new Label(RETRIEVING_MSG);
//    protected Label lblAppVer = new Label(RETRIEVING_MSG);
//    protected Label lblDbVer = new Label(RETRIEVING_MSG);
    protected static final AppAndDBVerInfoServiceAsync service = GWT.create(AppAndDBVerInfoService.class);
    public final static IAdmBeanWrapper beanWrapper = GWT.create(IAdmBeanWrapper.class);
    public static final INamedTypedPropsBeanBroker<InstanceInfoBean> broker = beanWrapper.getBroker(InstanceInfoBean.class);
    protected Map<String, Label> lblMap = new HashMap<String, Label>();
//    protected Set<String> propNames = broker.getPropNames();
    protected TextBox tbLogin = new TextBox();
    protected ShowablePasswordTextBox tbPass = new ShowablePasswordTextBox();
    protected Label lblStatus = new Label("initializing...");
//    protected Button btnRunDbUpgrade;
    protected String properLogin;
    protected String properPass;
//    protected Map<String, InstanceInfoBean> lastInfos;
    protected MultiInfoBean lastInfos;
//    protected RadioButton rbApp;
//    protected RadioButton rbLoad;
    protected TextBox tbDbLogin = new TextBox();
    protected ShowablePasswordTextBox tbDbPass;
//    protected TextBox tbSqlFilesDir = new TextBox();
    protected Integer procId;
    protected FlexTable pnlLogMsgs = new FlexTable();
    protected Integer nextLogId;
    protected FlexTable propTable = new FlexTable();
    protected int propValsFirstRowNum;
    FlowPanel fpDbLogins = new FlowPanel();
    protected FlexTable infosTable = new FlexTable() {
        {
            setCellSpacing(4);
            setCellPadding(4);
            setBorderWidth(1);
        }
    };
    protected Map<String, String> infoTableCaptions = new LinkedHashMap<String, String>() {
        {
            put("appInfo.appCode", "App Code");
            put("appInfo.appVer", "App Ver");
            put("dbInstanceName", "DB Name");
            put("dbVer", "DB ver");
            put("lastProcessInfo.id", "Last AutoUpgrade process\\ID");
            put("lastProcessInfo.initDbVer", "Last AutoUpgrade process\\Init dbVer");
            put("lastProcessInfo.appVer", "Last AutoUpgrade process\\For App Ver");
            put("lastProcessInfo.spid", "Last AutoUpgrade process\\spid");
            put("lastProcessInfo.status", "Last AutoUpgrade process\\Status");
            put("lastProcessInfo.dateAdded", "Last AutoUpgrade process\\Start Date");
            put("lastProcessInfo.lastLogDate", "Last AutoUpgrade process\\Last Date");
            put("lastProcessInfo.finalDbVer", "Last AutoUpgrade process\\Final DB Ver");
        }
    };
    protected Integer appCodeFirstRow;
    protected List<String> appCodeToRowNum = new ArrayList<String>();
    protected Map<String, TextBox> appCodeToTbSqlDir = new HashMap<String, TextBox>();
    protected Map<String, Label> appCodeToSqlDirStatLbl = new HashMap<String, Label>();
//    Map<String, Set<String>> appCodeToDbInstances = new HashMap<String, Set<String>>();
    protected Map<String, Button> dbInstanceToBtnUpgrade = new HashMap<String, Button>();
    protected Map<String, AppInfoBean> appCodeToAppInfo = new HashMap<String, AppInfoBean>();
    protected Map<String, AppInfoBean> instanceToAppInfo = new HashMap<String, AppInfoBean>();
    protected Map<String, InstanceInfoBean> instanceToInstanceInfo = new LinkedHashMap<String, InstanceInfoBean>();
    protected Button btnUpgradeAll = new Button("Upgrade all", new ClickHandler() {
        public void onClick(ClickEvent event) {
            if (!isUpgradeInProgress) {
                Set<String> toUpgrade = allUpgradeable;
                startAutoUpgradeDbs(toUpgrade);
            }
        }
    });
    protected Map<String, Button> appCodeToUpgradeAllOfAppBtn = new HashMap<String, Button>();

    static {
        ServiceDefTarget endpoint = (ServiceDefTarget) service;
        endpoint.setServiceEntryPoint(/*"/" +*/FoxyClientSingletons.FILE_HANDLING_SERVLET_NAME);
    }

    @Override
    protected String getOptForcedLang() {
        return "en";
    }

    protected <T extends IsWidget> T addRow(HTMLTable ft, String caption, T widget) {
        int rowNum = ft.getRowCount();
        ft.setText(rowNum, 0, caption);
        ft.setWidget(rowNum, 1, widget);
        return widget;
    }

//    protected void addInfoLabelRow(HTMLTable ft, String propName, String caption) {
//        lblMap.put(propName, addRow(ft, caption, new Label("...")));
//    }
    protected void onModuleLoadProper() {

        Timer keepSessionAliveTimer = new Timer() {
            @Override
            public void run() {
                if (properLogin != null) {
                    performGetInfo();
                } else {
                    service.keepSessionAlive(new DoNothingAsyngCallback<Void>());
                }
            }
        };
        keepSessionAliveTimer.scheduleRepeating(1000);

        btnUpgradeAll.setEnabled(false);

        FoxyClientSingletons.addNotificationHandler(new InlineFoxyNotificationHandlerExBase() {
            @Override
            protected void showMsg(boolean isWarning, String msg) {
                lblStatus.setText((isWarning ? "WARN: " : "") + msg);
            }

            @Override
            public void showWarning(String err, Throwable ex) {
                if (ex instanceof BIKSAdmRuntimeException) {
                    showWarning(ex.getMessage());
                } else {
                    super.showWarning(err, ex);
                }
            }
        });

//        KeyUpHandler kuh = new KeyUpHandler() {
//            public void onKeyUp(KeyUpEvent event) {
//                updateBtnDbUpgradeEnabled();
//            }
//        };
//
//        tbLogin.addKeyUpHandler(kuh);
//        tbPass.setKeyUpHandler(kuh);
////        tbSqlFilesDir.addKeyUpHandler(kuh);
//        tbDbLogin.addKeyUpHandler(kuh);
        VerticalPanel vp = new VerticalPanel();
        RootPanel.get().add(vp);

//        int row = 0;
        addRow(propTable, "Status", lblStatus);
        addRow(propTable, "Login", tbLogin);
        addRow(propTable, "Pass", tbPass);

//        for (String propName : propNames) {
//            lblMap.put(propName, addRow(ft, propName, new Label("...")));
//        }

        /*
         public String appCode;
         public String appVer;
         public String dbVer;
         public String dbInstanceName;
         */
        int rowNum = propTable.getRowCount();
        propTable.setWidget(rowNum, 0, new Button("get info", new ClickHandler() {
            public void onClick(ClickEvent event) {
                performGetInfo();
            }
        }));

        HorizontalPanel hp = new HorizontalPanel();
        hp.add(btnUpgradeAll);
        hp.add(lblUpgradeStatus);
        propTable.setWidget(rowNum, 1, hp);

//        propTable.setWidget(rowNum, 1, btnRunDbUpgrade = new Button("run db upgrade", new ClickHandler() {
//            public void onClick(ClickEvent event) {
//                performDbUpgrade();
//            }
//        }));
//        rbfp.add(rbApp = getAccDbRadioButton("app"));
//        rbfp.add(rbLoad = getAccDbRadioButton("load"));
//        rbfp.add(getAccDbRadioButton("<custom>"));
        addRow(propTable, "db logins", fpDbLogins);
        addRow(propTable, "db login", tbDbLogin);
        addRow(propTable, "db pass", tbDbPass = new ShowablePasswordTextBox());

//        btnRunDbUpgrade.setEnabled(false);
        vp.add(propTable);
        vp.add(infosTable);
        vp.add(pnlLogMsgs);

        lblStatus.setText("enter login/pass");

//        Timer t = new Timer() {
//            @Override
//            public void run() {
//                if (properLogin != null) {
//                    performGetInfo();
//                }
////                if (procId != null) {
////                    service.getLogMsgs(getADB(), procId, nextLogId, new StandardAsyncCallback<List<DbauLogBean>>() {
////                        public void onSuccess(List<DbauLogBean> result) {
////                            addLogMsgs(result);
////                        }
////                    });
////                }
//            }
//        };
//
//        t.scheduleRepeating(1000);
    }

    protected void addLogMsgs(List<DbauLogBean> msgs) {
        if (msgs == null) {
            nextLogId = null;
            return;
        }

        int rowNum = pnlLogMsgs.getRowCount();
        for (DbauLogBean msg : msgs) {
            int currProcId = msg.procId;

            if (!BaseUtils.safeEquals(currProcId, procId)) {
                pnlLogMsgs.removeAllRows();
                procId = currProcId;
                rowNum = 0;
            }

            pnlLogMsgs.setText(rowNum, 0, SimpleDateUtils.toCanonicalStringWithMillis(msg.dateAdded));
            pnlLogMsgs.setText(rowNum, 1, msg.msg);
            rowNum++;
            nextLogId = msg.id + 1;
        }
    }

    protected void setSelectedInstanceForLogs(String instanceName) {
        if (!BaseUtils.safeEquals(selectedInstanceForLogs, instanceName)) {
            nextLogId = null;
            selectedInstanceForLogs = instanceName;
            pnlLogMsgs.removeAllRows();
        }
    }

//    protected RadioButton getAccDbRadioButton(String caption) {
//        final RadioButton rb = new RadioButton("dbLO", caption);
//        rb.addClickHandler(new ClickHandler() {
//            public void onClick(ClickEvent event) {
//                setAccDbLoginAndPass();
//            }
//        });
//        return rb;
//    }
//
//    protected void setAccDbLoginAndPass() {
////        if (rbApp.getValue()) {
////            tbDbLogin.setText(lastInfos.accAppLogin);
////            tbDbPass.setText(lastInfos.accAppPass);
////        } else if (rbLoad.getValue()) {
////            tbDbLogin.setText(lastInfos.load_user_login);
////            tbDbPass.setText(lastInfos.load_user_pwd);
////        }
//        updateBtnDbUpgradeEnabled();
//    }
    protected void performDbUpgrade(String dbInstanceName) {
        pendingForAutoUpgrade.remove(currentlyUpgrading);
        final String appCode = //lastInfos.get(dbInstanceName).appCode;
                instanceToAppInfo.get(dbInstanceName).appCode;
        final TextBox tbSqlFilesDir = appCodeToTbSqlDir.get(
                appCode);
        String sqlFilesDir
                = tbSqlFilesDir.getText();

        if (BaseUtils.isStrEmptyOrWhiteSpace(sqlFilesDir)) {
            tbSqlFilesDir.setFocus(true);
            Window.alert("no sqlFilesDir for [" + appCode + "]");
            return;
        }

        Button btn = dbInstanceToBtnUpgrade.get(dbInstanceName);
        btn.setEnabled(false);

        service.runDbUpgradeProcess(getADB(), dbInstanceName, tbDbLogin.getText(),
                tbDbPass.getText(), sqlFilesDir, new StandardAsyncCallback<Integer>() {
                    public void onSuccess(Integer result) {
                        procId = result;
                    }
                });
    }
    String selectedInstanceForLogs;

    protected void performGetInfo() {

        final AuthenticationDataBean adb = getADB();
        properLogin = null;

        Map<String, String> sqlFilesDirs;

        if (lastInfos != null) {
            sqlFilesDirs = new LinkedHashMap<String, String>();

            for (Entry<String, TextBox> e : appCodeToTbSqlDir.entrySet()) {
                sqlFilesDirs.put(e.getKey(), e.getValue().getText());
            }
        } else {
            sqlFilesDirs = null;
        }

        service.getInfo(adb, selectedInstanceForLogs, nextLogId, sqlFilesDirs,
                new StandardAsyncCallback<MultiInfoBean>() {
                    public void onSuccess(MultiInfoBean result) {
                        showInfo(adb, result);
                    }
                });
    }

    protected AuthenticationDataBean getADB() {
        return new AuthenticationDataBean(tbLogin.getText(), tbPass.getText());
    }
//    protected void updateBtnDbUpgradeEnabled() {
////        btnRunDbUpgrade.setEnabled(lastInfos != null
////                && !BaseUtils.safeEquals(lastInfos.appVer, lastInfos.dbVer)
////                && BaseUtils.safeEquals(properLogin, tbLogin.getText())
////                && BaseUtils.safeEquals(properPass, tbPass.getText())
////                && (lastInfos.lastProcessInfo == null || ((!BaseUtils.safeEquals(lastInfos.lastProcessInfo.finalDbVer, lastInfos.dbVer)
////                || lastInfos.lastProcessInfo.status > 0)
////                && lastInfos.lastProcessInfo.status != 0))
////                && !BaseUtils.isStrEmptyOrWhiteSpace(tbSqlFilesDir.getText())
////                && !BaseUtils.isStrEmptyOrWhiteSpace(tbDbLogin.getText()));
//    }
//    protected Pair<Set<String>, List<Map<String, Object>>> getInfosAsVals(List<InstanceInfoBean> infos) {
//        Set<String> propNames = new LinkedHashSet<String>();
//        List<Map<String, Object>> vals = new ArrayList<Map<String, Object>>(infos.size());
//
//        for (InstanceInfoBean info : infos) {
//            INamedTypedPropsBean<InstanceInfoBean> getter = beanWrapper.create(info);
//
//            Map<String, Object> pv = BaseUtils.beanPropsToMapDeep(getter, beanWrapper);
//
//            for (Entry<String, Object> e : pv.entrySet()) {
//                final Object value = e.getValue();
//                if (value instanceof Collection) {
//                    continue;
//                }
//                final String propName = e.getKey();
//                propNames.add(propName);
//            }
//            vals.add(pv);
//        }
//
//        return new Pair<Set<String>, List<Map<String, Object>>>(propNames, vals);
//    }

    protected void setDbLoginByName(final String loginName) {
        Pair<String, String> p = lastInfos.dbLogins.get(loginName);
        tbDbLogin.setText(p.v1);
        tbDbPass.setText(p.v2);
    }

    protected String calcSqlFilesDirStatusText(String appCode,
            Map<String, Set<String>> availableScriptsForDbVers,
            Map<String, String> checkedSqlFilesDirs) {

        String checkedDir = BaseUtils.safeMapGet(checkedSqlFilesDirs, appCode);
        String currDir = appCodeToTbSqlDir.get(appCode).getText();

        if (!BaseUtils.safeEquals(checkedDir, currDir)) {
            return "(unchecked)";
        }

        Set<String> okDbVers = availableScriptsForDbVers.get(appCode);
        if (okDbVers == null) {
            return "no such dir";
        }

        Set<String> notOK = new HashSet<String>();

        AppInfoBean app = appCodeToAppInfo.get(appCode);
        for (InstanceInfoBean instance : app.instances) {
            final String dbVer = instance.dbVer;
            if (!okDbVers.contains(dbVer)) {
                if (!BaseUtils.safeEquals(app.appVer, dbVer)) {
                    notOK.add(dbVer);
                }
            }
        }

        if (notOK.isEmpty()) {
            return "OK!";
        }

        return "not OK for dbVers: " + notOK;
    }

    protected void updateSqlFilesDirStatusLbl(Label lblStat, String appCode,
            Map<String, Set<String>> availableScriptsForDbVers,
            Map<String, String> checkedSqlFilesDirs) {
        String statTxt = calcSqlFilesDirStatusText(appCode, availableScriptsForDbVers, checkedSqlFilesDirs);
        lblStat.setText(statTxt);
    }

    protected void updateSqlFilesDirStatusLabels() {
        for (String appCode : appCodeToTbSqlDir.keySet()) {
            Label lbl = appCodeToSqlDirStatLbl.get(appCode);
            updateSqlFilesDirStatusLbl(lbl, appCode, lastInfos.availableScriptsForDbVers,
                    lastInfos.checkedSqlFilesDirs);
        }
    }
    Map<String, Set<String>> upgradeableByAppCode = new LinkedHashMap<String, Set<String>>();
    Set<String> allUpgradeable = new LinkedHashSet<String>();
    String currentlyUpgrading;
    Set<String> pendingForAutoUpgrade = new LinkedHashSet<String>();
    Set<String> finishedAutoUpgrade = new LinkedHashSet<String>();
    Set<String> failedAutoUpgrade = new LinkedHashSet<String>();
    boolean isUpgradeInProgress;

    protected void startAutoUpgradeDbs(Set<String> toUpgrade) {
        currentlyUpgrading = null;
        pendingForAutoUpgrade.clear();
        pendingForAutoUpgrade.addAll(toUpgrade);
        failedAutoUpgrade.clear();
        finishedAutoUpgrade.clear();

        currentlyUpgrading = BaseUtils.safeGetFirstItem(pendingForAutoUpgrade, null);

        setUpgradeStatus("Starting with DB: " + currentlyUpgrading);

        performDbUpgrade(currentlyUpgrading);
    }
    Label lblUpgradeStatus = new Label("");

    protected void setUpgradeStatus(String txt) {
        lblUpgradeStatus.setText(txt);
    }

    protected void advanceAutoUpgradeProcess(InstanceInfoBean info, NeedToUpgrade ntu) {
        String status;
        final String dbInstanceName = info.dbInstanceName;
        if (ntu == NeedToUpgrade.NeedsManualFixing) {
            status = "Failed for DB: " + dbInstanceName;
            failedAutoUpgrade.add(dbInstanceName);
            currentlyUpgrading = null;
            pendingForAutoUpgrade.clear();
        } else {
            status = "Finished with DB: " + dbInstanceName;
            currentlyUpgrading = null;
            finishedAutoUpgrade.add(dbInstanceName);

            for (String candidate : pendingForAutoUpgrade) {
                if (allUpgradeable.contains(candidate)) {
                    currentlyUpgrading = candidate;
                    break;
                }
            }
            if (currentlyUpgrading == null) {
                status += ". All upgradable DONE.";
                if (!pendingForAutoUpgrade.isEmpty()) {
                    status += " Cannot upgrade: " + pendingForAutoUpgrade;
                }
            } else {
                status += ". Starting with DB: " + currentlyUpgrading;
                performDbUpgrade(currentlyUpgrading);
            }
        }
        setUpgradeStatus(status);
    }

    protected NeedToUpgrade calcNeedToUpgrade(InstanceInfoBean info) {
        final String dbInstanceName = info.dbInstanceName;
        final AppInfoBean app = instanceToAppInfo.get(dbInstanceName);
        NeedToUpgrade ntu = BIKSAdmUtils.calcNeedToUpgrade(info, app,
                lastInfos.availableScriptsForDbVers);
        return ntu;
    }

    protected void showInfo(AuthenticationDataBean adb, MultiInfoBean infos) {
        boolean firstTimeLogins = lastInfos == null;
        lastInfos = infos;

        fpDbLogins.clear();

        for (String loginName : infos.dbLogins.keySet()) {
            final String loginNameFinal = loginName;
            Button btnSetLogin = new Button(loginName, new ClickHandler() {
                public void onClick(ClickEvent event) {
                    setDbLoginByName(loginNameFinal);
                }
            });
            fpDbLogins.add(btnSetLogin);
            if (firstTimeLogins) {
                setDbLoginByName(loginName);
                firstTimeLogins = false;
            }
        }

        Set<String> appCodes = new LinkedHashSet<String>();
//        appCodeToDbInstances.clear();

//        lastInfos = new LinkedHashMap<String, InstanceInfoBean>();
        instanceToInstanceInfo.clear();
        boolean hasSelectedInstanceForInfo = BaseUtils.safeEquals(infos.msgsInstanceName, selectedInstanceForLogs);
        if (hasSelectedInstanceForInfo) {
            addLogMsgs(infos.msgs);
        }
//        else {
//            setSelectedInstanceForLogs(null);
//        }

        upgradeableByAppCode.clear();

        for (AppInfoBean aib : infos.apps) {
            final String appCode = aib.appCode;
            upgradeableByAppCode.put(appCode, new LinkedHashSet<String>());
            appCodes.add(appCode);
            List<InstanceInfoBean> instances = aib.instances;
            appCodeToAppInfo.put(appCode, aib);

            for (InstanceInfoBean info : instances) {
                final String dbInstanceName = info.dbInstanceName;
                instanceToInstanceInfo.put(dbInstanceName, info);
                instanceToAppInfo.put(dbInstanceName, aib);

//            BaseUtils.addToCollectingMapWithLinkedSet(appCodeToDbInstances, appCode, dbInstanceName);
            }
        }

        if (appCodeFirstRow != null) {
            for (int appCodeRowNum = appCodeToRowNum.size() - 1; appCodeRowNum >= 0; appCodeRowNum--) {
                String appCode = appCodeToRowNum.get(appCodeRowNum);
                if (!appCodes.contains(appCode)) {
                    propTable.removeRow(appCodeFirstRow + appCodeRowNum);
                }
            }
        } else {
            appCodeFirstRow = propTable.getRowCount();
        }

        //ww: zachowaj tylko aktualne (usuń te, co wyleciały)
        appCodeToRowNum.retainAll(appCodes);
        int rowNumForNewAppCodes = appCodeFirstRow + appCodeToRowNum.size();
        //ww: usuń część wspólną
        appCodes.removeAll(appCodeToRowNum);
        //ww: appCodes to teraz tylko te, co doszły, dodajemy je na końcu
        appCodeToRowNum.addAll(appCodes);

        final String uploadDirFixed;

        if (BaseUtils.isStrEmptyOrWhiteSpace(lastInfos.uploadFilesDir)) {
            uploadDirFixed = null;
        } else {
            uploadDirFixed = BaseUtils.ensureDirSepPostfix4C(lastInfos.uploadFilesDir.replace("\\", "/"));
        }

        for (String appCode : appCodes) {
            final String appCodeFinal = appCode;
            propTable.setText(rowNumForNewAppCodes, 0, "[" + appCode + "] sql files dir");
            final TextBox tb = new TextBox();
            HorizontalPanel hp = new HorizontalPanel();
            hp.add(tb);
            final String dirFromSvr = uploadDirFixed + "dbau/" + appCodeFinal;
            tb.setText(dirFromSvr);
            Button btnGetFromServer = new Button("set from svr", new ClickHandler() {
                public void onClick(ClickEvent event) {
                    tb.setText(dirFromSvr);
                }
            });
            hp.add(btnGetFromServer);
            final Label lblStat = new Label();
//            final Label lblStat = new Label("(unchecked)");
//            Button btnCheckDir = new Button("check", new ClickHandler() {
//                public void onClick(ClickEvent event) {
//                    final Map<String, String> m = new HashMap<String, String>();
//                    m.put(appCodeFinal, tb.getText());
//                    service.checkSqlFilesDir(m, new StandardAsyncCallback<Map<String, Set<String>>>() {
//                        public void onSuccess(Map<String, Set<String>> result) {
//                            updateSqlFilesDirStatusLbl(lblStat, appCodeFinal, result, m);
//                        }
//                    });
//                }
//            });
//            hp.add(btnCheckDir);
            hp.add(lblStat);

            Button btnUpgradeByAppCode = new Button("", new ClickHandler() {
                public void onClick(ClickEvent event) {
                    if (!isUpgradeInProgress) {
                        Set<String> toUpgrade = upgradeableByAppCode.get(appCodeFinal);
                        startAutoUpgradeDbs(toUpgrade);
                    }
                }
            });

            hp.add(btnUpgradeByAppCode);
            appCodeToUpgradeAllOfAppBtn.put(appCode, btnUpgradeByAppCode);

            propTable.setWidget(rowNumForNewAppCodes, 1, hp);
            appCodeToTbSqlDir.put(appCode, tb);
            appCodeToSqlDirStatLbl.put(appCode, lblStat);
            rowNumForNewAppCodes++;
        }

        updateSqlFilesDirStatusLabels();

        infosTable.removeAllRows();

//        Pair<Set<String>, List<Map<String, Object>>> infosVals = getInfosAsVals(infos);
//
////        Set<String> propNames = infosVals.v1;
//        List<Map<String, Object>> vals = infosVals.v2;
        List<InstanceInfoBean> instances = new ArrayList<InstanceInfoBean>(instanceToInstanceInfo.values());
        List<Map<String, Object>> vals = new ArrayList<Map<String, Object>>(instances.size());

        for (InstanceInfoBean instance : instances) {
            @SuppressWarnings("unchecked")
            INamedTypedPropsBean<InstanceInfoBean> getter = beanWrapper.create(instance);
            @SuppressWarnings("unchecked")
            INamedTypedPropsBean<AppInfoBean> aibGetter = beanWrapper.create(instanceToAppInfo.get(instance.dbInstanceName));
            Map<String, Object> instVals = BaseUtils.beanPropsToMapDeep(getter, beanWrapper);
            BaseUtils.beanPropsToMapDeep(instVals, "appInfo.", aibGetter, beanWrapper);
            vals.add(instVals);
        }

        int valsSize = vals.size();

        boolean hasTwoLevelHeader
                = GWTUtils.setupTwoLevelTableHeader(infosTable, 0, 1, infoTableCaptions.values(), "\\");

        int rowFix = hasTwoLevelHeader ? 2 : 1;

        int colNum = 1;

        for (Entry<String, String> e : infoTableCaptions.entrySet()) {
            if (e.getValue() == null) {
                continue;
            }

            String propName = e.getKey();
            for (int i = 0; i < valsSize; i++) {
                Object o = vals.get(i).get(propName);
                infosTable.setText(i + rowFix, colNum, BaseUtils.safeToString(o));
            }
            colNum++;
        }

        allUpgradeable.clear();

        isUpgradeInProgress = currentlyUpgrading != null;

        if (!isUpgradeInProgress) {
            for (InstanceInfoBean instance : instances) {
                if (instance.lastProcessInfo != null && instance.lastProcessInfo.status == 0) {
                    isUpgradeInProgress = true;
                    break;
                }
            }
        }

        for (int i = 0; i < valsSize; i++) {
            final InstanceInfoBean info = instances.get(i);
            final String dbInstanceName = info.dbInstanceName;
            final AppInfoBean app = instanceToAppInfo.get(dbInstanceName);
//            NeedToUpgrade ntu = BIKSAdmUtils.calcNeedToUpgrade(info, app,
//                    lastInfos.availableScriptsForDbVers);
            NeedToUpgrade ntu = calcNeedToUpgrade(info);
            final int iRowFixed = i + rowFix;

            if (ntu != NeedToUpgrade.NeedsUpgrade) {
                infosTable.setText(iRowFixed, 0, ntu.toString());
            } else {
                Button btn = new Button("Upgrade -->", new ClickHandler() {
                    public void onClick(ClickEvent event) {
                        setSelectedInstanceForLogs(dbInstanceName);
//                        performDbUpgrade(dbInstanceName);
                        startAutoUpgradeDbs(BaseUtils.paramsAsSet(dbInstanceName));
                    }
                });
                btn.setEnabled(!isUpgradeInProgress);
                infosTable.setWidget(iRowFixed, 0, btn);
                dbInstanceToBtnUpgrade.put(dbInstanceName, btn);
                BaseUtils.addToCollectingMapWithLinkedSet(upgradeableByAppCode, app.appCode, dbInstanceName);
                allUpgradeable.add(dbInstanceName);
            }

            RadioButton rb = new RadioButton("showLogs", "");
            if (BaseUtils.safeEquals(dbInstanceName, selectedInstanceForLogs)) {
                rb.setValue(true);
            }
            rb.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    setSelectedInstanceForLogs(dbInstanceName);
                }
            });
            infosTable.setWidget(iRowFixed, colNum, rb);
        }

        if (currentlyUpgrading != null) {
            InstanceInfoBean cuInstance = instanceToInstanceInfo.get(currentlyUpgrading);
            NeedToUpgrade ntu = calcNeedToUpgrade(cuInstance);
            if (ntu != NeedToUpgrade.UpgradeInProgress) {
                advanceAutoUpgradeProcess(cuInstance, ntu);
            }
        }

        final boolean isAnyToUpgrade = !allUpgradeable.isEmpty();
        btnUpgradeAll.setEnabled(isAnyToUpgrade && !isUpgradeInProgress);
        btnUpgradeAll.setText(isAnyToUpgrade ? "Upgrade All (" + allUpgradeable.size() + ")" : "Nothing to upgrade");

        for (Entry<String, Set<String>> e : upgradeableByAppCode.entrySet()) {
            final String appCode = e.getKey();
            Button bu = appCodeToUpgradeAllOfAppBtn.get(appCode);
            Set<String> dbInsts = e.getValue();
            boolean isAnyToUpgrByApp = !dbInsts.isEmpty();
            bu.setEnabled(isAnyToUpgrByApp && !isUpgradeInProgress);
            bu.setText(isAnyToUpgrByApp ? "Upgrade all [" + appCode + "] (" + dbInsts.size() + ")" : "Nothing to upgrade");
        }

//        Window.alert("propNames:\n" + propNames);
        properLogin = adb.login;
        properPass = adb.pass;

        lblStatus.setText("Last checked: " + SimpleDateUtils.toCanonicalString(new Date()) //                + ", last msg: " + SimpleDateUtils.getShortElapsedTimeStr(null, null, properPass, properPass, properPass, properPass, procId)
        );

//        addLogMsgs(lastInfos.msgs);
//        btnRunDbUpgrade.setEnabled(!BaseUtils.safeEquals(info.appVer, info.dbVer));
//        updateBtnDbUpgradeEnabled();
//        lblAppName.setText(info.appCode);
//        lblAppVer.setText(info.appVer);
//        lblDbVer.setText(info.dbVer);
    }
}
