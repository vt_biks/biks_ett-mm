/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.widgets.sbavc;

import com.google.gwt.user.client.ui.Widget;
import pl.fovis.common.sbavc.ISearchByAttrValueCriteria;
import pl.fovis.common.sbavc.SearchByAttrValueCriteriaEquals;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class SearchByAttrValueCriteriaEqualsWidget extends SearchByAttrValueCriteriaWidgetBase {

    protected Widget equalWidget;

    @Override
    protected void createOptWidget() {
        row.setWidget(0, 4, equalWidget = createWidgetBox());
    }

    @Override
    public ISearchByAttrValueCriteria provideBean() {
        return (isValidInput() ? new SearchByAttrValueCriteriaEquals(getSelectedAttrId(), getSelectedAttrCaption(), getWidgetValueAsString(equalWidget), negationChbx.getValue()) : null);
    }

    @Override
    protected boolean isValidInput() {
        if (manager.isDisplayedAsNumber(getSelectedAttrId())) {
            String equalValue = getWidgetValueAsString(equalWidget).replace(",", ".");
            return BaseUtils.tryParseNumber(equalValue) != null;
        }
        return true;
    }
}
