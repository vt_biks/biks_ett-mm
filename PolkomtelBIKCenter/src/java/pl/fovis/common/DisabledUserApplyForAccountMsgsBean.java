/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author pmielanczuk
 */
public class DisabledUserApplyForAccountMsgsBean implements Serializable {

    public String headerMsg;
    public String availableOptions;
    public String footerMsg;
    public String mailTo;
    public String mailTitle;
    public String mailBody;

    public DisabledUserApplyForAccountMsgsBean() {
    }

    public DisabledUserApplyForAccountMsgsBean(String headerMsg, String availableOptions, String footerMsg, String mailTo, String mailTitle, String mailBody) {
        this.headerMsg = headerMsg;
        this.availableOptions = availableOptions;
        this.footerMsg = footerMsg;
        this.mailTo = mailTo;
        this.mailTitle = mailTitle;
        this.mailBody = mailBody;
    }
}
