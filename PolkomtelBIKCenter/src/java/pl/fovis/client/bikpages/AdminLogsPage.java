/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.DataSourceStatusBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class AdminLogsPage extends LogsPageBase<DataSourceStatusBean> {

    @Override
    public String getId() {
        return "admin:load:logs";
    }

    @Override
    protected void callServiceToGetData(StandardAsyncCallback<Pair<Integer, List<DataSourceStatusBean>>> callback) {
        BIKClientSingletons.getService().getBikDataSourceStatuses(pwh.pageNum, pwh.pageSize, callback);
    }

    @Override
    protected NodeAwareBeanExtractorBase<DataSourceStatusBean> getDataExtractor() {
        return new DataSourceStatusBeanExtractor();
    }
}
