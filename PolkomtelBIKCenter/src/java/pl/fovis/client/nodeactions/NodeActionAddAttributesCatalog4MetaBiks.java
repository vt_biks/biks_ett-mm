/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class NodeActionAddAttributesCatalog4MetaBiks extends NodeActionAddNodeBaseOnTreeKindBase {

    @Override
    public boolean isEnabledByDesign() {
        return isMetaBiks() && BaseUtils.ensureObjIdEndWith(BIKConstants.METABIKS_OBJ_ID_ATTRIBUTE_DEF, "|").equalsIgnoreCase(node.objId);
    }

    @Override
    public String getCaption() {
        return I18n.dodaj.get() + ": " + I18n.kategorieAtrybutow.get();
    }

    @Override
    public void execute() {
        NodeKindBean nk = BIKClientSingletons.getNodeKindBeanByCode(BIKConstants.NODE_KIND_META_ATTRIBUTES_CATALOG);

        nodeKindId = nk.id;
        nodeKindCode = nk.code;
        super.execute();
    }
}
