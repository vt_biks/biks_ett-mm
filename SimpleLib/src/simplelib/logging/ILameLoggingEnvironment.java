/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simplelib.logging;

import simplelib.IContinuationWithReturn;

/**
 *
 * @author wezyr
 */
public interface ILameLoggingEnvironment {

    public boolean getRunWithoutLoggingMode();

    public <T> T runWithoutLogging(boolean noLogging, IContinuationWithReturn<T> cont);

    public String getCurrentRuningContextInfo();
}
