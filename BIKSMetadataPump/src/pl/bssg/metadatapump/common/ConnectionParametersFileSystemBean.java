/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.common;

import simplelib.BeanWithIntIdAndNameBase;

/**
 *
 * @author tflorczak
 */
public class ConnectionParametersFileSystemBean extends BeanWithIntIdAndNameBase {

    public String path;
    public boolean isRemote;
    public String domain;
    public String user;
    public String password;
    public boolean isActive;
    public boolean downloadFiles; // default 0 - false
    public String treeCode;
    public Double maxFileSize;
    public String fileNamePatterns;
    public String fileSince;
}
