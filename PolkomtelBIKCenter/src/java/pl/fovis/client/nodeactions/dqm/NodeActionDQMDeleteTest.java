 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions.dqm;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.nodeactions.NodeActionDelete;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;

/**
 *
 * @author tflorczak
 */
public class NodeActionDQMDeleteTest extends NodeActionDelete {

    @Override
    public boolean isEnabledByDesign() {
        return node != null && isInDQMAdminMode() && isNodeOneOfTheKind(false, BIKGWTConstants.NODE_KIND_DQM_TEST_FAILED, BIKGWTConstants.NODE_KIND_DQM_TEST_INACTIVE, BIKGWTConstants.NODE_KIND_DQM_TEST_SUCCESS, BIKGWTConstants.NODE_KIND_DQM_TEST_NOT_STARTED) && !isNodeLinked();
    }

    @Override
    public String getCaption() {
        return I18n.usunTest.get() /* I18N:  */;
    }

    @Override
    protected void callServiceToDelete(TreeNodeBean node, AsyncCallback<List<String>> AsyncCallback) {
        BIKClientSingletons.getDQMService().deleteDQMTest(node, AsyncCallback);
    }

    @Override
    protected String getDeleteHTMLText() {
        return I18n.czyNaPewnoUsunacTest.get() + ": <div class='PUWelement'><img src='images/" /* I18N: no */ + BIKClientSingletons.getNodeKindIconNameByCode(node.nodeKindCode, node.treeCode) + ".gif'/> " + BaseUtils.encodeForHTMLTag(node.name) + "</div>";
    }
}
