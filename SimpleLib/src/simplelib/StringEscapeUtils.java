/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

/**
 *
 * @author wezyr
 */
public class StringEscapeUtils {

    private static String hex(char ch) {
        return Integer.toHexString(ch).toUpperCase();
    }

    public static String escapeJavaScriptAndQuote(String str) {
        return escapeJavaScriptAndQuote(str, true, true);
    }

    public static String escapeJavaScriptAndQuote(String str, boolean escapeSingleQuote, boolean escapeUnicode) {
        String res;
        if (str == null) {
            res = "null";
        } else {
            res = "\"" + StringEscapeUtils.escapeJavaStyleString(str, escapeSingleQuote, escapeUnicode) + "\"";
        }
        return res;
    }

    public static String escapeJavaScriptAndQuote(Object o) {
        return escapeJavaScriptAndQuote(BaseUtils.safeToString(o));
    }

    public static String escapeJavaScript(String str) {
        return escapeJavaStyleString(str, true);
    }

    private static String escapeJavaStyleString(String str, boolean escapeSingleQuotes) {
        return escapeJavaStyleString(str, escapeSingleQuotes, true);
    }

    private static String escapeJavaStyleString(String str, boolean escapeSingleQuotes, boolean escapeUnicode) {
        if (str == null) {
            return null;
        }
        StringBuilder out = new StringBuilder(str.length() * 2);
        escapeJavaStyleString(out, str, escapeSingleQuotes, escapeUnicode);
        return out.toString();
    }

    private static void escapeJavaStyleString(StringBuilder out, String str, boolean escapeSingleQuote) {
        escapeJavaStyleString(out, str, escapeSingleQuote, true);
    }

    private static void escapeJavaStyleString(StringBuilder out, String str, boolean escapeSingleQuote, boolean escapeUnicode) {
        if (out == null) {
            throw new IllegalArgumentException("The out must not be null");
        }
        if (str == null) {
            return;
        }
        int sz;
        sz = str.length();
        for (int i = 0; i < sz; i++) {
            char ch = str.charAt(i);

            if (escapeUnicode && ch > 0x7f) {
                // handle unicode
                if (ch > 0xfff) {
                    out.append("\\u").append(hex(ch));
                } else if (ch > 0xff) {
                    out.append("\\u0").append(hex(ch));
                } else if (ch > 0x7f) {
                    out.append("\\u00").append(hex(ch));
                }
            } else if (ch < 32) {
                switch (ch) {
                    case '\b':
                        out.append('\\');
                        out.append('b');
                        break;
                    case '\n':
                        out.append('\\');
                        out.append('n');
                        break;
                    case '\t':
                        out.append('\\');
                        out.append('t');
                        break;
                    case '\f':
                        out.append('\\');
                        out.append('f');
                        break;
                    case '\r':
                        out.append('\\');
                        out.append('r');
                        break;
                    default:
                        if (ch > 0xf) {
                            out.append("\\u00").append(hex(ch));
                        } else {
                            out.append("\\u000").append(hex(ch));
                        }
                        break;
                }
            } else {
                switch (ch) {
                    case '\'':
                        if (escapeSingleQuote) {
                            out.append('\\');
                        }
                        out.append('\'');
                        break;
                    case '"':
                        out.append('\\');
                        out.append('"');
                        break;
                    case '\\':
                        out.append('\\');
                        out.append('\\');
                        break;
                    default:
                        out.append(ch);
                        break;
                }
            }
        }
    }

    public static String unescapeJava(String str) {
        if (str == null) {
            return null;
        }
        StringBuilder out = new StringBuilder();
        int sz = str.length();
        StringBuffer unicode = new StringBuffer(4);
        boolean hadSlash = false;
        boolean inUnicode = false;
        for (int i = 0; i < sz; i++) {
            char ch = str.charAt(i);
            if (inUnicode) {
                // if in unicode, then we're reading unicode
                // values in somehow
                unicode.append(ch);
                if (unicode.length() == 4) {
                    // unicode now contains the four hex digits
                    // which represents our unicode character
                    try {
                        int value = Integer.parseInt(unicode.toString(), 16);
                        out.append((char) value);
                        unicode.setLength(0);
                        inUnicode = false;
                        hadSlash = false;
                    } catch (NumberFormatException nfe) {
                        throw new //NestableRuntimeException
                                LameRuntimeException("Unable to parse unicode value: " + unicode, nfe);
                    }
                }
                continue;
            }
            if (hadSlash) {
                // handle an escaped value
                hadSlash = false;
                switch (ch) {
                    case '\\':
                        out.append('\\');
                        break;
                    case '\'':
                        out.append('\'');
                        break;
                    case '\"':
                        out.append('"');
                        break;
                    case 'r':
                        out.append('\r');
                        break;
                    case 'f':
                        out.append('\f');
                        break;
                    case 't':
                        out.append('\t');
                        break;
                    case 'n':
                        out.append('\n');
                        break;
                    case 'b':
                        out.append('\b');
                        break;
                    case 'u': {
                        // uh-oh, we're in unicode country....
                        inUnicode = true;
                        break;
                    }
                    default:
                        out.append(ch);
                        break;
                }
                continue;
            } else if (ch == '\\') {
                hadSlash = true;
                continue;
            }
            out.append(ch);
        }
        if (hadSlash) {
            // then we're in the weird case of a \ at the end of the
            // string, let's output it anyway.
            out.append('\\');
        }
        return out.toString();
    }

    public static String unescapeQuotedJava(String str) {
        if (str == null) {
            return null;
        }
        if (str.length() < 2) {
            throw new LameRuntimeException("string is too short, str: \"" + str + "\"");
        }
        char c = str.charAt(0), c2 = str.charAt(str.length() - 1);
        if (c != c2) {
            throw new LameRuntimeException("quotes do not match, str=\"" + str + "\"");
        }
        return unescapeJava(str.substring(1, str.length() - 1));
    }
}
