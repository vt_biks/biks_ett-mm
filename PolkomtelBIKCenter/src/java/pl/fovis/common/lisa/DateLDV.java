/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.lisa;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;
import java.util.Date;
import pl.trzy0.foxy.commons.FoxyRuntimeException;

/**
 *
 * @author lbiegniewski
 */
public class DateLDV implements LisaDictionaryValue, Serializable, IsSerializable {

    Date value;

    public DateLDV() {
    }

    public DateLDV(Date value) {
        this.value = value;
    }

    public Date getValue() {
        return value;
    }

    public void setValue(Object o) {

        if (o instanceof java.util.Date) {
            this.value = (java.util.Date) o;
        } else {
            throw new FoxyRuntimeException("DateLDV: " + "argument is not of 'Date' type" /* i18n: no */);
        }
    }
}
