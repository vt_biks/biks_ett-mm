/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.genericimporter;

import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.bssg.metadatapump.AdhocPartialBeanCollector;
import pl.bssg.metadatapump.GenericNodeBean;
import pl.bssg.metadatapump.IPumpLogger;
import pl.bssg.metadatapump.common.PumpConstants;
import pl.trzy0.foxy.commons.BeanConnectionUtils;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public class CSVImporter {

    protected static final int PACKAGE_SIZE = 1000;
    protected IPumpLogger pumpLogger;
    protected InputStream sourceStream;
    protected String treeCode;
    protected INamedSqlsDAO<Object> adhocDaoForPump;
    protected AdhocPartialBeanCollector<GenericNodeBean> beanCollector;
    protected Set<String> allAttrNames = new HashSet<String>();
    protected boolean isIncremental;

    public CSVImporter(INamedSqlsDAO<Object> adhocDaoForPump, String treeCode) {
        this(adhocDaoForPump, treeCode, true);
    }

    public CSVImporter(INamedSqlsDAO<Object> adhocDaoForPump, String treeCode, boolean isIncremental) {
        this.adhocDaoForPump = adhocDaoForPump;
        this.treeCode = treeCode;
        this.isIncremental = isIncremental;
    }

//    public CSVImporter(INamedSqlsDAO<Object> adhocDaoForPump, InputStream is, String treeCode, boolean isForceDelete) {
//        this.adhocDaoForPump = adhocDaoForPump;
//        this.sourceStream = is;
//        this.treeCode = treeCode;
//        this.isForceDelete = isForceDelete;
//    }
//
//    public CSVImporter(INamedSqlsDAO<Object> adhocDaoForPump, InputStream is, String treeCode) {
//        this(adhocDaoForPump, is, treeCode, true);
//    }
    public Pair<Integer, String> pumpTree(List<Map<String, String>> nodeInfoList) {
        adhocDaoForPump.execNamedCommand("deleteFromAmgNodeBase");
        beanCollector = new AdhocPartialBeanCollector<GenericNodeBean>(PACKAGE_SIZE, adhocDaoForPump, "insertIntoGenericTable");
        // read from file
//        String csvContent = null;
//        List<Map<String, String>> nodeInfoList = null;
//        try {
//            pumpLog("Reading file content", 0);
//            csvContent = LameUtils.loadAsString(sourceStream, "*Cp1250,ISO-8859-2,UTF-8");
//            CSVReader reader = new CSVReader(csvContent, CSVReader.CSVParseMode.CONSIDER_QUOTES, ';', true);
//            pumpLog("Parsing info", 5);
//            nodeInfoList = reader.readFullTableAsMaps();
//        } catch (Exception e) {
//            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Error in reading file: " + e.getMessage());
//        }
        // save in database
//        adhocDaoForPump.execInAutoCommitMode(new IContinuation() {
//
//            @Override
//            public void doIt() {
        try {
            pumpLog("Updating tree nodes", 20);
            saveTreeToDatabase(nodeInfoList);
            pumpLog("Updating attributes", 50);
            updateNodeAttributes(nodeInfoList);
            adhocDaoForPump.execNamedCommand("addDefaultAttributes", treeCode);
            adhocDaoForPump.execNamedCommand("deleteEmptyAttributes4MetaBiks", treeCode);
        } catch (Exception e) {
            return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_ERROR, "Error" + e.getMessage());
        }
        return new Pair<Integer, String>(PumpConstants.LOG_LOAD_STATUS_DONE, PumpConstants.PUMP_DATA_LOAD_SUCCESS);
    }

    protected void updateNodeAttributes(List<Map<String, String>> nodeInfoList) {
//        BeanConnectionUtils.execNamedQuerySingleVal(adhocDaoForPump, "getOrCreateAttrCategory", false, "id", "Ogólne");
//        Integer generalCategoryId = adhocDaoForPump.execNamedCommand("getOrCreateAttrCategory", "Ogólne");
        Integer generalCategoryId = BeanConnectionUtils.execNamedQuerySingleVal(adhocDaoForPump, "getOrCreateAttrCategory", false, "id", "Ogólne");
        for (Map<String, String> nodeInfo : nodeInfoList) {
            String objId = BaseUtils.ensureObjIdEndWith(nodeInfo.get(PumpConstants.GENERIC_NODE_OBJID), "|");
            for (String attrName : allAttrNames) {
                String attrValue = nodeInfo.get(attrName);
                Integer attrDefId = BeanConnectionUtils.execNamedQuerySingleVal(adhocDaoForPump, "getOrCreateAttrDefInCategory", false, "id", attrName, generalCategoryId);
                Map<String, Object> m = adhocDaoForPump.execNamedQuerySingleRowCFN("getOrCreateAttrForNodeKind", true, null, attrDefId, nodeInfo.get(PumpConstants.GENERIC_NODE_NODE_KIND_CODE));
                if (m != null) {
                    Integer attrId = (Integer) m.get("attr_id");
//                    boolean isRequired = ((Integer) m.get("is_required")) == 0 ? false : true;
//                    String defaultValue = (String) m.get("default_value");
                    if (BaseUtils.isStrEmptyOrWhiteSpace(attrValue)) {
//                        if (!isRequired) {
                        continue;
//                        }
//                        attrValue = defaultValue;
                    }
                    if (attrId != null) {
                        adhocDaoForPump.execNamedCommand("addAttrToNode", treeCode, objId, attrId, attrValue);
                    }
                }
            }
        }
    }

    protected void saveTreeToDatabase(List<Map<String, String>> results) {
        for (Map<String, String> result : results) {
            allAttrNames.addAll(result.keySet());
            String objId = BaseUtils.ensureObjIdEndWith(result.get(PumpConstants.GENERIC_NODE_OBJID), "|");
            String parentObjId = BaseUtils.ensureObjIdEndWith(result.get(PumpConstants.GENERIC_NODE_PARENT_OBJID), "|");
            String nodeKindCode = result.get(PumpConstants.GENERIC_NODE_NODE_KIND_CODE);
            String nodeName = result.get(PumpConstants.GENERIC_NODE_NAME);
            String descr = result.get(PumpConstants.GENERIC_NODE_DESCR);
            Integer visualOrder = BaseUtils.tryParseInt(result.get(PumpConstants.GENERIC_NODE_VISUAL_ORDER));

            beanCollector.add(new GenericNodeBean(objId, (BaseUtils.isStrEmptyOrWhiteSpace(parentObjId) ? null : parentObjId), nodeKindCode, nodeName, descr, visualOrder));
        }
        for (String s : allAttrNames) {
            s = BaseUtils.trimAndCompactSpaces(s);
        }
        allAttrNames.remove(PumpConstants.GENERIC_NODE_OBJID);
        allAttrNames.remove(PumpConstants.GENERIC_NODE_PARENT_OBJID);
        allAttrNames.remove(PumpConstants.GENERIC_NODE_NODE_KIND_CODE);
        allAttrNames.remove(PumpConstants.GENERIC_NODE_NAME);
        allAttrNames.remove(PumpConstants.GENERIC_NODE_DESCR);
        allAttrNames.remove(PumpConstants.GENERIC_NODE_VISUAL_ORDER);
//            }
//        });
        beanCollector.flush();
        // move to bik_node
        adhocDaoForPump.execNamedCommand("insertGenericNodesIntoBikNodeWithForDeleteOption", treeCode, isIncremental ? 0 : 1);
        // indexing
//        adhocDaoForPump.execNamedCommand("verticalizeNodeAttrsForTree", treeCode);
    }

    public void setPumpLogger(IPumpLogger pumpLogger) {
        this.pumpLogger = pumpLogger;
    }

    private void pumpLog(String msg, int proc) {
        if (pumpLogger != null) {
            pumpLogger.pumpLog(msg, proc);
        }
    }
}
