/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.biks.adm.common;

import java.io.Serializable;

/**
 *
 * @author pmielanczuk
 */
public class AuthenticationDataBean implements Serializable {

    public String login;
    public String pass;

    public AuthenticationDataBean() {
    }

    public AuthenticationDataBean(String login, String pass) {
        this.login = login;
        this.pass = pass;
    }
}
