﻿exec sp_check_version '1.4.y1.11';
go

update bik_translation set txt = 'Business Area' 
where code = 'dtk_TaxonomyEntityLeaf' and lang = 'en' and kind = 'kind'
go

exec sp_update_version '1.4.y1.11', '1.4.y1.12';
go