/*
 * RelativeResolverProvider.java
 *
 * Created on 15 pa�dziernik 2007, 13:40
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package commonlib;

import simplelib.LameRuntimeException;
import simplelib.Pair;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author msmoktunowicz
 */
public class RelativeResolverProvider implements IResourceProvider {
    // mo�liwe resource
    static public enum ResourceType {None, Jar, War, File};
    // resource dla sciezek wzglednych
    private ResourceType resourceType;
    private String path;
    private Map<String, IResourceProvider> subProviders;
    
    public RelativeResolverProvider(ResourceType resourceType, String path,
            Map<String, IResourceProvider> subProviders) {
        this.resourceType = resourceType;
        this.path = path;
        this.subProviders = new HashMap<String, IResourceProvider>(subProviders);
        
        if ((resourceType == ResourceType.Jar || resourceType == ResourceType.War) &&
                path != null && !"".equals(path) && !path.startsWith("/"))  {
            throw new LameRuntimeException("Path must be absolute: [" + path + "]");
        } else {
            if (resourceType == ResourceType.File &&
                    path != null &&  !"".equals(path) && !(new File(path)).isAbsolute()) {
                throw new LameRuntimeException("Path must be absolute: [" + path + "]");
            }
        }
    }
    
    protected Pair<IResourceProvider, String> getSubProvider(String resourceName) {
        Pair<String, String> ss = LameUtils.splitString(resourceName, "://");
        String prefix, resNameForSubProvider;
        
        if (ss.v2 == null) {
            prefix = "";
            resNameForSubProvider = ss.v1;
        } else {
            prefix = ss.v1;
            resNameForSubProvider = ss.v2;
        }
        
        IResourceProvider subProvider = subProviders.get(prefix);
        
//        System.out.println("getSubProvider: resourceName: " + resourceName +
//                ", prefix: " + prefix +
//                ", resNameForSubProvider: " + resNameForSubProvider +
//                ", subProvider.class: " + LameUtils.safeGetClassName(subProvider));
        
        if (subProvider == null) {
            throw new LameRuntimeException(
                    "getSubProvider: cannot find subProvider for resource: " + resourceName + ", prefix: " + prefix + ", providers: " + subProviders.keySet());
        }
        
        return new Pair<IResourceProvider, String>(subProvider, resNameForSubProvider);
    }
    
    public void setProviderByPrefix(String prefix, IResourceProvider resourceProvider) {
        subProviders.put(prefix, resourceProvider);
    }
    
    public long getLastModified(String resourceName) {
        Pair<IResourceProvider, String> p = getSubProvider(resolveName(resourceName));
        return p.v1.getLastModified(p.v2);
    }
    
    public byte[] loadAsBytes(String resourceName) {
        Pair<IResourceProvider, String> p = getSubProvider(resolveName(resourceName));
        return p.v1.loadAsBytes(p.v2);
    }
    
    private boolean isAbsolutePath(ResourceType resourceType, String resourceName) {
        boolean res;
        switch(resourceType) {
            case Jar:
            case War: res = resourceName.startsWith("/"); break;
            case File: res = new File(resourceName).isAbsolute(); break;
            default: res = false;
        }
        return res;
    }
    
    private String resolveName(String resourceName) {
        String res;
        if (resourceName.indexOf("://") != -1) {
            res = resourceName;
        } else {
            // sciezka bez schematu
            // doklejamy sciezke, gdy sciezka wzgledna
            String filePath = isAbsolutePath(resourceType, resourceName) ? "" : path;
            // dodajemy schemat
            switch(resourceType) {
                case Jar: res = "jar://" + LameUtils.mergeResourcePaths(filePath, resourceName); break;
                case War: res = "web://" + LameUtils.mergeResourcePaths(filePath, resourceName); break;
                case File: res = "file://" + filePath + resourceName; break;
                default: res = resourceName;
            }
        }
        return res;
    }
}
