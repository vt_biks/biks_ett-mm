/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server;

import java.util.Map;
import javax.mail.MessagingException;
import pl.trzy0.foxy.commons.MailServerConfig;
import pl.trzy0.foxy.serverlogic.mailing.MailProps;
import pl.trzy0.foxy.serverlogic.mailing.SendMail;

/**
 *
 * @author bfechner
 */
public class SendMailWithLog extends SendMail {

    protected static BIKSServiceBaseForSingleBIKS service;

    public SendMailWithLog(MailServerConfig server) {
        super(server);
    }

    public static void sendMessageWithCC(MailServerConfig server, BIKSServiceBaseForSingleBIKS service,
            final String recipients[], final String[] cc, final String subject, final String message,
            final Map<String, String> embeddedFiles) throws MessagingException {
        SendMailWithLog.service = service;
        sendMessageWithCC(server, service, recipients, cc, subject, message, embeddedFiles, null);
    }

    public static void sendMessageWithCC(MailServerConfig server, BIKSServiceBaseForSingleBIKS service,
            final String recipients[], final String[] cc, final String subject, final String message,
            final Map<String, String> embeddedFiles, final Map<String, byte[]> embeddedByteArrays) throws MessagingException {

        SendMailWithLog sm = new SendMailWithLog(server);
        sm.send(new MailProps(recipients, cc, subject, message, embeddedFiles, embeddedByteArrays), server.ssl);
    }

    @Override
    protected void addLog(String msg) {
        if (service != null) {
            service.addMailLogMsg(msg);
        }
    }
}
