exec sp_check_version '1.4.y1.4'
go

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------
-- czystka w bik_tutorial, 
--   wypada tabela bik_tutorial_translation
--   do bik_tutorial dochodz� kolumny lang, 
--     visual_order
----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

alter table bik_tutorial add lang varchar(3) null
go

update bik_tutorial set lang = 'pl'
go

alter table bik_tutorial drop constraint UQ_bik_tutorial_code
go

-- alter table bik_tutorial drop constraint UQ_bik_tutorial_lang_code
alter table bik_tutorial alter column lang varchar(3) not null
go

alter table bik_tutorial add constraint UQ_bik_tutorial_lang_code unique (lang, code)
go

insert into bik_tutorial (code, title, text, movie, lang)
select code, title, txt, null, 'en' from bik_tutorial_translation where lang = 'en'
go

alter table bik_tutorial add visual_order int null
go

update bik_tutorial
set visual_order = (select idx from
dbo.fn_split_by_sep('Introduction,MyBIKS,Search,AppNavigation,UseCases,Analysis,AddNews,AddAttributes,UpdateContent,LinkElements,AddEditDefinitions,AddConnections', ',', 3)
where str = code
)
go

alter table bik_tutorial alter column visual_order int not null
go

drop table bik_tutorial_translation
go

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

exec sp_update_version '1.4.y1.4', '1.4.y1.5'
go
