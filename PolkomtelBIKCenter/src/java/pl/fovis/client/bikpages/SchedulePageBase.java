/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.bssg.metadatapump.common.SchedulePumpBean;
import pl.fovis.client.BIKClientSingletons;
import static pl.fovis.client.BIKGWTConstants.DIAG_MSG_KIND$SchedulePageBase;
import pl.fovis.client.bikwidgets.PaginationWidgetsHelper;
import pl.fovis.common.ScheduleWidgetBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.ClientDiagMsgs;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.Pair;

/**
 *
 * @author tflorczak
 */
public abstract class SchedulePageBase<T extends SchedulePumpBean, WID extends ScheduleWidgetBean> extends BikPageMinimalBase {

    protected Map<Integer, Pair<Integer, Integer>> startTimeMap;
//    protected Map<Integer, Integer> dayToIndexMap;
    protected Map<Integer, WID> widgets;
    protected VerticalPanel main;
//    protected FlexTable grid;
    protected Grid grid;
    protected PaginationWidgetsHelper pwh = new PaginationWidgetsHelper(15) {

        @Override
        protected void pageNumChanged() {
            fetchData();
        }
    };

    protected abstract void createHeaders();

    protected void populateRows(List<T> results) {

        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SchedulePageBase)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SchedulePageBase, "before populateRows" /* I18N: no */);
        }

        long startCtm = System.currentTimeMillis();

        for (int i = 0; i < results.size(); i++) {
            populateRow(results.get(i));
        }

        long endCtm = System.currentTimeMillis();

        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SchedulePageBase)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SchedulePageBase, "after populateRows, total time: " /* I18N: no */ + (endCtm - startCtm) + " ms");
        }
    }

    protected abstract void populateRow(T bean);

    protected abstract int getColumnCount();

    protected void createRows(List<T> results) {
        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SchedulePageBase)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SchedulePageBase, "createRows starting" /* I18N: no */);
        }
        grid.clear();
        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SchedulePageBase)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SchedulePageBase, "after grid.clear" /* I18N: no */);
        }
        grid.resize(results.size() + 1, getColumnCount());
        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SchedulePageBase)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SchedulePageBase, "after grid.resize" /* I18N: no */);
        }
        createHeaders();
        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SchedulePageBase)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SchedulePageBase, "after createHeaders" /* I18N: no */);
        }

        for (int i = 0; i < results.size(); i++) {

            long beforeCtm = System.currentTimeMillis();

            createRow(i + 1, results.get(i));

            long afterCtm = System.currentTimeMillis();

            if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SchedulePageBase)) {
                ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SchedulePageBase, "created row #" + i + " in: " + (afterCtm - beforeCtm) + " ms" /* I18N: no */);
            }
        }
        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SchedulePageBase)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SchedulePageBase, "createRows done" /* I18N: no */);
        }
    }

    protected abstract void createRow(int row, T bean);

    protected void setWidgetEnabled(WID widgetBean, boolean isEnabled, boolean checkEnabling) {
//        widgetBean.startTime.setEnabled(isEnabled);
//        widgetBean.getStartTime().setEnabled(isEnabled);
        widgetBean.setStartTimeEnabled(isEnabled);
        widgetBean.time.setEnabled(isEnabled);
        widgetBean.interval.setEnabled(isEnabled);
        setAddsWidgetEnabled(widgetBean, isEnabled, checkEnabling);
    }

    protected void setAddsWidgetEnabled(WID widgetBean, boolean isEnabled, boolean checkEnabling) {
        // no op
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // UWAGA: kolejnosc musi sie zgadzac z kolejnoscia wrzucania na mapkę: insertIntoDayToIndexMap()!!!//
    /////////////////////////////////////////////////////////////////////////////////////////////////////
//    protected ListBox createDayListBox() {
//        ListBox listbox = new ListBox();
//        listbox.addItem(I18n.codziennie.get() /* I18N:  */, "1");
//        listbox.addItem(I18n.co3Dni.get() /* I18N:  */, "3");
//        listbox.addItem(I18n.co5Dni.get() /* I18N:  */, "5");
//        listbox.addItem(I18n.co7Dni.get() /* I18N:  */, "7");
//        listbox.addItem(I18n.co14Dni.get() /* I18N:  */, "14");
//        listbox.addItem(I18n.co30Dni.get() /* I18N:  */, "30");
//        listbox.addItem(I18n.jednorazowo.get() /* I18N:  */, "0");
//        return listbox;
//    }
    //////////////////////////////////////////////////////////////////////////////////////////////
    // UWAGA: kolejnosc musi sie zgadzac z kolejnoscia wrzucania na listBoxa: createListBox()!!!//
    //////////////////////////////////////////////////////////////////////////////////////////////
//    protected void insertIntoDayToIndexMap() {
//        dayToIndexMap.put(1, 0);
//        dayToIndexMap.put(3, 1);
//        dayToIndexMap.put(5, 2);
//        dayToIndexMap.put(7, 3);
//        dayToIndexMap.put(14, 4);
//        dayToIndexMap.put(30, 5);
//        dayToIndexMap.put(0, 6);
//    }
    protected PushButton createButton(String text, ClickHandler ch) {
        PushButton btnSaveChanges = new PushButton(text);
        NewLookUtils.makeCustomPushButton(btnSaveChanges);
        btnSaveChanges.addClickHandler(ch);
        return btnSaveChanges;
    }

    protected boolean validateTime() {
        for (WID scheduleWidgetsBean : widgets.values()) {
            if (!validateTime(scheduleWidgetsBean.id, scheduleWidgetsBean.time.getText(), scheduleWidgetsBean.isSchedule.getValue())) {
                return false;
            }
        }
        return true;
    }

    protected boolean validateTime(Integer scheduleId, String textToFormat, Boolean isActive) {
//        if (isActive == false) {
//            startTimeMap.put(scheduleId, new Pair<Integer, Integer>(0, 0));
//            return true;
//        }
        try {
            int index = textToFormat.indexOf(":");
            if (index == -1 || (index + 1 >= textToFormat.length())) {
                return false;
            }
            int hour = BaseUtils.tryParseInteger(textToFormat.substring(0, index), -1);
            int minute = BaseUtils.tryParseInteger(textToFormat.substring(index + 1), -1);
            if (hour < 0 || hour >= 24 || minute < 0 || minute >= 60) {
                return false;
            }
            startTimeMap.put(scheduleId, new Pair<Integer, Integer>(hour, minute));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    protected abstract void getSchedules(StandardAsyncCallback<Pair<Integer, List<T>>> async);

    protected abstract void setSchedules(List<T> schedules, StandardAsyncCallback<Void> async);

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        pwh.pageNum = 0;
        fetchData();
    }

    protected void fetchData() {
        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SchedulePageBase)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SchedulePageBase, "starting" /* I18N: no */);
        }

        getSchedules(new StandardAsyncCallback<Pair<Integer, List<T>>>("Error in" /* I18N: no */ + " getSchedule") {
                    @Override
                    public void onSuccess(final Pair<Integer, List<T>> result) {

                        long startCtm = System.currentTimeMillis();

                        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SchedulePageBase)) {
                            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SchedulePageBase, "before createRows" /* I18N: no */);
                        }
                        createRows(result.v2);
                        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SchedulePageBase)) {
                            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SchedulePageBase, "after createRows" /* I18N: no */);
                        }

                        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                            @Override
                            public void execute() {
                                populateRows(result.v2);
                            }
                        });

                        long endCtm = System.currentTimeMillis();

                        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SchedulePageBase)) {
                            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SchedulePageBase, "populateRows deferred, total time: " /* I18N: no */ + (endCtm - startCtm) + " ms");
                        }
                        pwh.setPageCntByItemCnt(result.v1);
                        pwh.refresh();
                    }
                });

        if (ClientDiagMsgs.isShowDiagEnabled(DIAG_MSG_KIND$SchedulePageBase)) {
            ClientDiagMsgs.showDiagMsg(DIAG_MSG_KIND$SchedulePageBase, "done" /* I18N: no */);
        }
    }

//    private static final DateTimeFormat dateFormat = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_SHORT);
//    private static final DateBox.DefaultFormat DEFAULT_FORMAT = new DateBox.DefaultFormat(dateFormat);
//    private static final DatePicker dp = new DatePicker();
    protected void setWidgetsFromBean(final WID widgetBean, T bean) {
        widgetBean.id = bean.id;
        widgetBean.name = bean.source;
        widgetBean.instance = bean.instanceName;
        widgetBean.time = new TextBox();
        widgetBean.time.setWidth("100px");
//        widgetBean.startTime = new DateBox();
//        widgetBean.startTime = new DateBox(dp, null, DEFAULT_FORMAT);
////        widgetBean.startTime.setFormat(DEFAULT_FORMAT);
//        widgetBean.startTime.setWidth("100px");
        widgetBean.startTimeFake = new TextBox();
        widgetBean.startTimeFake.setWidth("100px");

//        widgetBean.interval = createDayListBox();
        widgetBean.interval = new TextBox();
        widgetBean.isSchedule = new CheckBox();
        widgetBean.isSchedule.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setWidgetEnabled(widgetBean, event.getValue(), false);
            }
        });
    }

    protected void populateWidgets(WID widgetBean, T bean) {
        widgetBean.time.setText((bean.hour >= 10 ? bean.hour : "0" + bean.hour) + ":" + (bean.minute >= 10 ? bean.minute : "0" + bean.minute));
//        widgetBean.startTime.setValue(bean.dateStarted);
//        widgetBean.getStartTime().setValue(bean.dateStarted);
        widgetBean.setStartTimeValue(bean.dateStarted);
//        widgetBean.interval.setSelectedIndex(dayToIndexMap.get(bean.interval));
        widgetBean.interval.setText(BaseUtils.safeToStringDef(bean.interval, "43200"));//co 30 dni
        widgetBean.isSchedule.setValue(bean.isSchedule);
        setWidgetEnabled(widgetBean, bean.isSchedule, true);
//        setAddsWidgetBean(widgetBean, bean);
    }

    @Override
    protected Widget buildWidgets() {
        main = new VerticalPanel();
        main.setSpacing(10);
        pwh.init();
        main.add(pwh.paginationHp);
        main.setCellHorizontalAlignment(pwh.paginationHp, HasHorizontalAlignment.ALIGN_RIGHT);
        main.setCellVerticalAlignment(pwh.paginationHp, HasVerticalAlignment.ALIGN_MIDDLE);
//        dayToIndexMap = new HashMap<Integer, Integer>();
        widgets = new HashMap<Integer, WID>();
        startTimeMap = new HashMap<Integer, Pair<Integer, Integer>>();
        grid = new Grid();
        grid.addStyleName("dqmScheduleGrid");
        grid.setWidth("100%");
        grid.setBorderWidth(0);
//        grid.setCellSpacing(15);
        grid.setCellSpacing(0);
//        grid.setCellPadding(8);
//        insertIntoDayToIndexMap();
        main.add(grid);
        main.add(createButton(I18n.zapiszZmiany.get() /* I18N:  */, new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        startTimeMap.clear();
                        if (validateTime()) {
                            List<T> beans = new ArrayList<T>();
                            for (WID scheduleBean : widgets.values()) {
                                T newBean = getScheduleBean();
                                newBean.id = scheduleBean.id;
                                newBean.source = scheduleBean.name;
                                newBean.instanceName = scheduleBean.instance;
                                Pair<Integer, Integer> timeToShedule = startTimeMap.get(scheduleBean.id);
                                newBean.hour = timeToShedule.v1;
                                newBean.minute = timeToShedule.v2;
//                                newBean.dateStarted = scheduleBean.startTime.getValue();
//                                newBean.dateStarted = scheduleBean.getStartTime().getValue();
                                newBean.dateStarted = scheduleBean.getStartTimeValue();
//                                newBean.interval = BaseUtils.tryParseInteger(scheduleBean.interval.getValue(scheduleBean.interval.getSelectedIndex()), 1);
                                newBean.interval = BaseUtils.tryParseInteger(scheduleBean.interval.getText(), 43200);//co 30 dni
                                newBean.isSchedule = scheduleBean.isSchedule.getValue();
                                setAddsBeanFields(scheduleBean, newBean);
                                beans.add(newBean);
                            }
                            doOptFix(beans);
                            setSchedules(beans, new StandardAsyncCallback<Void>("Error in" /* I18N: no */ + " setScheduleInfo") {
                                @Override
                                public void onSuccess(Void result) {
                                    BIKClientSingletons.showInfo(I18n.zmienionoHarmonogram.get() /* I18N:  */);
                                    doOptActionAfterSave();

                                }
                            });
                        } else {
                            Window.alert(I18n.wprowawaPoprawneGodzinyWFormacie.get() /* I18N:  */ + ". 20:00");
                        }
                    }
                }));
        main.setWidth("100%");
        createAddtionalWidgets(main);
        Widget res = createMainScrollPanel(main, null);

        res.setWidth("100%");

        return res;
//        return main;
    }

    protected abstract T getScheduleBean();

    protected void setAddsBeanFields(WID scheduleBean, T newBean) {
        // no op
    }

    protected void doOptActionAfterSave() {
        // no op
    }

    protected void doOptFix(List<T> beans) {
        // no op
    }

//    protected void setAddsWidgetBean(WID widgetBean, T bean) {
//        // no op
//    }
    protected void createAddtionalWidgets(VerticalPanel main) {
        //no-op
    }
}
