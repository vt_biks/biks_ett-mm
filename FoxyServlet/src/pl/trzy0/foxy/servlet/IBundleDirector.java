/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.servlet;

import simplelib.MapHelper;

/**
 *
 * @author wezyr
 */
public interface IBundleDirector {

//    public Object createPageModel(HttpServletRequest request, HttpServletResponse response,
//            Map<String, IRenderable> boxes, IFoxyService service,
//            String pageName, MapHelper<String> params, Map<String, Object> globals,
//            //Properties props,
//            MapHelper<String> sysProps,
//            Map<String, Properties> textsProps,
//            CacheStorage cacheStorage,
//            Map<String, INamedTemplateProvider> tmplProviders);
//
//    public Iterable<String> getSupportedLangs(MapHelper<String> sysProps);

    public IBundleDerivedLogic createLogic(MapHelper<String> sysProps
            /*FoxyMillBundleData bundle*/);
}
