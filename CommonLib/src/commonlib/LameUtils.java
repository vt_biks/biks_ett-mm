/*
 * LameUtils.java
 *
 * Created on 17 czerwiec 2006, 16:40
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package commonlib;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import commonlib.i18nsupport.JavaCurrentLocaleNameProvider;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.text.CharacterIterator;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import simplelib.BaseUtils;
import static simplelib.BaseUtils.appendBytesAsHex;
import static simplelib.BaseUtils.byteToHex;
import simplelib.IGenericResourceProvider;
import simplelib.ISimpleRandomGenerator;
import simplelib.LameRuntimeException;
import simplelib.Pair;
import simplelib.StackTraceUtil;
import simplelib.logging.ILameLogger;
import simplelib.logging.LameLoggerFactory;
import simplelib.logging.StdOutLameLoggerAppender;

/**
 *
 * @author wezyr
 */
public class LameUtils extends BaseUtils {

    protected static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    static ILameLogger logger = LameLoggerFactory.getLogger(LameUtils.class);
    //LameLogger.getClassLogger();
    static private boolean factoriesInited;

    synchronized public static void initFactories() {
        if (factoriesInited) {
            return;
        }
        factoriesInited = true;

        ThreadedLameLoggingEnvironment.initLameLoggingEnvironment();

        BaseUtils.setRegExpMatcherFactory(new JavaRegExpMatcherFactory());
        BaseUtils.setStringComparator(new JavaStringComparator());
        BaseUtils.setSimpleRandomGenerator(new ISimpleRandomGenerator() {
            private final Random r = new Random();

            @Override
            public int nextInt(int upperBound) {
                return r.nextInt(upperBound);
            }
        });

        StdOutLameLoggerAppender.addAppender();

//        I18nMessage.setCurrentLocaleNameProvider(new JavaCurrentLocaleNameProvider());
        JavaCurrentLocaleNameProvider.setFactory();
    }

    static {
        initFactories();
    }
    private static Random rndForInts;

    private static void ensureRndForIntsInited() {
        if (rndForInts == null) {
            rndForInts = new Random();
        }
    }

    public static Map<String, Object> readJsonAsMap(String s) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
        };
        return mapper.readValue(s, typeRef);
    }

    public static int randomBetween(int min, int afterMax) {
        if (min + 1 >= afterMax) {
            return min;
        }
        ensureRndForIntsInited();
        int res = rndForInts.nextInt(afterMax - min) + min;
        if (res < min || res >= afterMax) {
            throw new LameRuntimeException("randomBetween: " + min + " and " + afterMax + " is out of range: " + res);
        }
        return res;
    }

    static public int randomIntInRange(int minV, int maxV) {
        return (int) (Math.random() * (maxV - minV + 1)) + minV;
    }

    public static int randomInt(int afterMax) {
        ensureRndForIntsInited();
        return rndForInts.nextInt(afterMax);
    }

    public static String mergeCollWithSep(Collection<String> strs, String sep) {
        if (collectionSizeFix(strs) == 0) {
            return "";
        }

        Iterator<String> iter = strs.iterator();
        StringBuilder sb = new StringBuilder(iter.next());
        while (iter.hasNext()) {
            sb.append(sep).append(iter.next());
        }

        return sb.toString();
    }

    public static String mergeWithSep(Collection<String> strs, String sep) {
        return mergeCollWithSep(strs, sep);
    }

    public static String mergeWithSepRange(String[] strs, String sep, int fromIdx, int len) {
        if (strs == null) {
            return null;
        }
        if (strs.length == 0 || len <= 0 || fromIdx >= strs.length) {
            return "";
        }

        StringBuilder sb = new StringBuilder(strs[fromIdx]);
        for (int i = fromIdx + 1; i < fromIdx + len; i++) {
            sb.append(sep).append(strs[i]);
        }

        return sb.toString();
    }

    public static String mergeWithSep(String[] strs, String sep) {
        return mergeWithSepRange(strs, sep, 0, arrLengthFix(strs));
    }

    public static String formatMergeWithSep(String fmtStr, String[] strs, String sep) {
        if (strs.length == 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder(String.format(fmtStr, strs[0]));
        for (int i = 1; i < strs.length; i++) {
            sb.append(sep).append(String.format(fmtStr, strs[i]));
        }

        return sb.toString();
    }

    public static String formatMergeWithSepObj(String fmtStr, Object[] objs, String sep) {
        if (objs.length == 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder(String.format(fmtStr, objs[0]));
        for (int i = 1; i < objs.length; i++) {
            sb.append(sep).append(String.format(fmtStr, objs[i]));
        }

        return sb.toString();
    }
    public static boolean DumpMemUsageModeOn = true;

    public static void dumpMemoryUsage(String marker) {
        if (DumpMemUsageModeOn) {
            System.out.println(marker + ". " + LameUtils.getMemoryDiagInfoCompact(true, ", "));
        }
    }

    public static String write2TempFile(String dirForUpload, String fileExt, InputStream inputStream) throws IOException {
        File dir = new File(dirForUpload);
        File file = File.createTempFile("file_", fileExt, dir);
        copy2File(new FileOutputStream(file), inputStream);
        return file.getName();
    }

    public static void copy2File(OutputStream outputStream, InputStream inputStream) throws IOException {
        byte[] buf = new byte[16 * 1024 * 1024];
        int len;
        while ((len = inputStream.read(buf)) > 0) {
            outputStream.write(buf, 0, len);
        }
        inputStream.close();
        outputStream.close();
    }

    public static long getFileSize(String filePath) {
        return new File(filePath).length();
    }

    public static String sanitizeFileName(String fileName) {
        return fileName.replace(" ", "_")
                .replace("�", "a").replace("�", "A")
                .replace("�", "c").replace("�", "C")
                .replace("�", "e").replace("�", "E")
                .replace("�", "l").replace("�", "L")
                .replace("�", "n").replace("�", "N")
                .replace("�", "o").replace("�", "O")
                .replace("�", "s").replace("�", "S")
                .replace("�", "z").replace("�", "Z")
                .replace("�", "z").replace("�", "Z");
    }

    public static Date parseDate(String s) {
        try {
            return sdf.parse(s);
        } catch (ParseException ex) {
            return null;
        }
    }

    public static String writeListMapAsJson(List<Map<String, Object>> nodeList) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(nodeList);
    }

    public static class CurrentClassGetter extends SecurityManager {

        public Class getCurrClass(int nestingLevel) {
            return getClassContext()[2 + nestingLevel];
        }
    }

    public static Class getCurrentClass(int nestingLevel) {
        return new CurrentClassGetter().getCurrClass(nestingLevel);
    }

    public static Class getCurrentClass() {
        return getCurrentClass(1);
    }

    public static int[] intIdentityArray(int offset, int length) {
        return intIdentityArray(offset, length, null);
    }

    public static int arrLengthFix(Object arr) {
        return arr == null ? 0 : Array.getLength(arr);
    }

    public static int[] intIdentityArray(int offset, int length, int[] ignoreIdxes) {
        int[] res = new int[length - arrLengthFix(ignoreIdxes)];

        int v = offset;
        for (int i = 0; i < length; i++) {
            if (linearSearch(ignoreIdxes, v) < 0) {
                res[i] = v++;
            }
        }

        return res;
    }

    public static int linearSearch(int[] a, int key) {
        if (a != null) {
            for (int i = 0; i < a.length; i++) {
                if (a[i] == key) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static <T> int linearSearch(T[] a, T key) {
        if (a != null) {
            for (int i = 0; i < a.length; i++) {
                if (safeEquals(a[i], key)) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static <V, T> int linearSearchEx(V[] a, Projector<V, T> projector, T key) {
        if (a != null) {
            for (int i = 0; i < a.length; i++) {
                if (safeEquals(projector.project(a[i]), key)) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static <V, T> int linearSearchEx(Collection<V> coll, Projector<V, T> projector, T key) {
        if (coll != null) {
            int i = 0;
            for (V e : coll) {
                if (safeEquals(projector.project(e), key)) {
                    return i;
                }
                i++;
            }
        }
        return -1;
    }

    public static int mapSizeFix(Map m) {
        return m == null ? 0 : m.size();
    }

    public static <K, V> void mapPutNotNull(Map<K, V> m, K key, V val) {
        if (val == null) {
            return;
        }
        m.put(key, val);
    }

//    public static Object nullToDef(Object optionalObj, Object defaultObj) {
//        return optionalObj == null ? defaultObj : optionalObj;
//    }
    public static String safeToStringTxtFmt(Object o, String format) {
        return o == null ? "<null>" : String.format(format, o.toString());
    }

    public static <T extends Comparable<T>> int compareArrays(T[] a1, T[] a2) {
        int minLen = Math.min(a1.length, a2.length);

        for (int i = 0; i < minLen; i++) {
            T o2 = a2[i];
            int elemCmpRes = a1[i].compareTo(o2);
            if (elemCmpRes != 0) {
                return elemCmpRes;
            }
        }

        if (a1.length > minLen) {
            return 1;
        } else if (a2.length > minLen) {
            return -1;
        } else {
            return 0;
        }
    }

    public static String formatDate(Date date) {
        return sdf.format(date);
    }

    public static <T extends Comparable<T>> Comparator<T[]> makeArrayComparator() {
        return new Comparator<T[]>() {
            public int compare(T[] o1, T[] o2) {
                return compareArrays(o1, o2);
            }
        };
    }

    /*
     public static <T extends Comparable> Comparator makeArrayComparator(Comparator<T[]> nullCmpr) {
     return new Comparator() {
     public int compare(Object o1, Object o2) {
     return compareArrays((T[]) o1, (T[]) o2);
     }
     };
     }
     */
    public static String toDiagString(Object o) {
        String res;
        if (o instanceof Collection) {
            res = collectionToDiagString((Collection) o);
        } else if (o instanceof Map) {
            res = mapToDiagString((Map) o);
        } else {
            res = safeToStringTxt(o);
        }
        return res;
    }

    public static String mapToDiagString(Map<?, ?> m) {
        StringBuilder sb = new StringBuilder();

        sb.append("(Map: ").append(safeGetClassName(m)).append(" { ");

        for (Entry<?, ?> e : m.entrySet()) {
            sb.append(toDiagString(e.getKey())).append(":").append(toDiagString(e.getValue())).append(" ");
        }

        sb.append("} )");

        return sb.toString();
    }

    public static String collectionToDiagString(Collection c) {
        StringBuilder sb = new StringBuilder();

        int elemCnt = collectionSizeFix(c);

        sb.append("(Collection: ").append(safeGetClassName(c)).append(", elem.cnt.: ").append(elemCnt).append(" [");

        int elemCntFix = Math.min(5, elemCnt);

        Iterator iter = c.iterator();
        int i = 0;

        while (i < elemCntFix && iter.hasNext()) {
            sb.append(toDiagString(iter.next()));
            if (i < elemCnt - 1) {
                sb.append(", ");
            }
            i++;
        }

        if (elemCnt != elemCntFix) {
            sb.append("{...}");
        }

        sb.append("])");

        return sb.toString();
    }

    public static <T> String arrayToDiagString(T[] a) {
        StringBuilder sb = new StringBuilder();

        int elemCnt = arrLengthFix(a);

        sb.append("(").append(a == null ? "<null>" : a.getClass().getCanonicalName()).append(", elem.cnt.: ").append(elemCnt).append(" [");

        int elemCntFix = Math.min(5, elemCnt);

        for (int i = 0; i < elemCntFix; i++) {
            sb.append(toDiagString(a[i]));
            if (i < elemCnt - 1) {
                sb.append(", ");
            }
        }

        if (elemCnt != elemCntFix) {
            sb.append("{...}");
        }

        sb.append("])");

        return sb.toString();
    }

    public static int countCommonStrs(String[] a1, String[] a2) {
        int a1Idx = 0, a2Idx = 0;
        int res = 0;
        final int a1Len = arrLengthFix(a1), a2Len = arrLengthFix(a2);

        while (a1Idx < a1Len && a2Idx < a2Len) {
            // cmprRes > 0 -> a1[] > a2[]
            int cmprRes = a1[a1Idx].compareTo(a2[a2Idx]);
            if (cmprRes == 0) {
                res++;
            }
            if (cmprRes >= 0) {
                a2Idx++;
            }
            if (cmprRes <= 0) {
                a1Idx++;
            }
        }
        return res;
    }

    // works for sorted lists
    public static <T extends Comparable<T>> int countCommonItemsOfSortedLists(List<T> list1, List<T> list2) {
        int a1Idx = 0, a2Idx = 0;
        int res = 0;
        final int a1Len = collectionSizeFix(list1), a2Len = collectionSizeFix(list2);

        while (a1Idx < a1Len && a2Idx < a2Len) {
            // cmprRes > 0 -> a1[] > a2[]
            int cmprRes = list1.get(a1Idx).compareTo(list2.get(a2Idx));
            if (cmprRes == 0) {
                res++;
            }
            if (cmprRes >= 0) {
                a2Idx++;
            }
            if (cmprRes <= 0) {
                a1Idx++;
            }
        }
        return res;
    }

    public interface StrCountFactorWizard {

        double factorForStr(String s);

        double finalResult(double commonStrsSum, double firstStrsSum, double secondStrsSum, int len1, int len2);
    }

    public interface ItemCountFactorWizard<T extends Comparable> {

        double factorForItem(T item);

        double finalResult(double commonStrsSum, double firstStrsSum, double secondStrsSum, int len1, int len2);
    }

    public static <T extends Comparable<T>> Set<T> getCommonItemsEx(T[] a1, T[] a2) {
        Set<T> res = new HashSet<T>();

        int a1Idx = 0, a2Idx = 0;
        //double res = 0, sum1 = 0, sum2 = 0;
        final int a1Len = arrLengthFix(a1), a2Len = arrLengthFix(a2);

        while (a1Idx < a1Len && a2Idx < a2Len) {
            // cmprRes > 0 -> a1[] > a2[]
            int cmprRes = a1[a1Idx].compareTo(a2[a2Idx]);
            if (cmprRes == 0) {
                res.add(a1[a1Idx]);
            }
            if (cmprRes >= 0) {
                a2Idx++;
            }
            if (cmprRes <= 0) {
                a1Idx++;
            }
        }

        return res;
    }

    public static boolean isFoundAsPrefix(String prefix, Collection<String> items) {
        for (String item : items) {
            if (item.startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isFoundAsInfix(String infix, Collection<String> items, boolean matchWholeString) {
        for (String item : items) {
            if ((matchWholeString || infix.length() < item.length()) && item.contains(infix)) {
                return true;
            }
        }
        return false;
    }

    public static <T extends Comparable<T>> double countCommonItemsEx(T[] a1, T[] a2, ItemCountFactorWizard<T> factorWizard) {
        int a1Idx = 0, a2Idx = 0;
        double res = 0, sum1 = 0, sum2 = 0;
        final int a1Len = arrLengthFix(a1), a2Len = arrLengthFix(a2);

        double fac1 = 0, fac2 = 0;
        int oldIdx1 = -1, oldIdx2 = -1;

        while (a1Idx < a1Len && a2Idx < a2Len) {
            // cmprRes > 0 -> a1[] > a2[]
            if (oldIdx1 != a1Idx) {
                fac1 = factorWizard.factorForItem(a1[a1Idx]);
            }
            int cmprRes = a1[a1Idx].compareTo(a2[a2Idx]);
            if (cmprRes == 0) {
                fac2 = fac1;
                res += fac1;
            } else if (oldIdx2 != a2Idx) {
                fac2 = factorWizard.factorForItem(a2[a2Idx]);
            }
            oldIdx1 = a1Idx;
            oldIdx2 = a2Idx;
            if (cmprRes >= 0) {
                a2Idx++;
                sum2 += fac2;
            }
            if (cmprRes <= 0) {
                a1Idx++;
                sum1 += fac1;
            }
        }

        while (a1Idx < a1Len || a2Idx < a2Len) {
            if (a1Idx < a1Len) {
                sum1 += factorWizard.factorForItem(a1[a1Idx++]);
            } else if (a2Idx < a2Len) {
                sum2 += factorWizard.factorForItem(a2[a2Idx++]);
            }
        }

        return factorWizard.finalResult(res, sum1, sum2, a1Len, a2Len);
    }

    public static double countCommonStrsEx(String[] a1, String[] a2, StrCountFactorWizard factorWizard) {
        int a1Idx = 0, a2Idx = 0;
        double res = 0, sum1 = 0, sum2 = 0;
        final int a1Len = arrLengthFix(a1), a2Len = arrLengthFix(a2);

        double fac1 = 0, fac2 = 0;
        int oldIdx1 = -1, oldIdx2 = -1;

        while (a1Idx < a1Len && a2Idx < a2Len) {
            // cmprRes > 0 -> a1[] > a2[]
            if (oldIdx1 != a1Idx) {
                fac1 = factorWizard.factorForStr(a1[a1Idx]);
            }
            int cmprRes = a1[a1Idx].compareTo(a2[a2Idx]);
            if (cmprRes == 0) {
                fac2 = fac1;
                res += fac1;
            } else if (oldIdx2 != a2Idx) {
                fac2 = factorWizard.factorForStr(a2[a2Idx]);
            }
            oldIdx1 = a1Idx;
            oldIdx2 = a2Idx;
            if (cmprRes >= 0) {
                a2Idx++;
                sum2 += fac2;
            }
            if (cmprRes <= 0) {
                a1Idx++;
                sum1 += fac1;
            }
        }

        while (a1Idx < a1Len || a2Idx < a2Len) {
            if (a1Idx < a1Len) {
                sum1 += factorWizard.factorForStr(a1[a1Idx++]);
            } else if (a2Idx < a2Len) {
                sum2 += factorWizard.factorForStr(a2[a2Idx++]);
            }
        }

        return factorWizard.finalResult(res, sum1, sum2, a1Len, a2Len);
    }

    public static String[] buildWordArrayNoSort(String str) {
        if (str == null) {
            return null;
        }
        str = separatorsToSpace(str);

        str = str.toLowerCase();
        str = LameUtils.maleLiteryNaPolskawe(str);
        str = highLowAsciiToSpace(str).trim();

        if (str.equals("")) {
            return null;
        }

        String[] a = str.split("[ ]+");

        return a;
    }

    public static String normalizeByWordArray(String s) {
        String[] sA = buildWordArrayNoSort(s);
        String res = mergeWithSep(sA, " ");
        return res == null ? null : res.trim();
    }

    public static String[] buildWordArrayNoSortIntern(String str) {
        String[] strs = buildWordArrayNoSort(str);
        internStringArray(strs);
        return strs;
    }

    public static String[] buildWordArrayNoSortInternCond(boolean doIntern, String str) {
        String[] strs = buildWordArrayNoSort(str);
        if (doIntern) {
            internStringArray(strs);
        }
        return strs;
    }

    static public void internStringArray(String[] strs) {
        if (strs == null) {
            return;
        }

        for (int i = 0; i < strs.length; i++) {
            strs[i] = strs[i].intern();
        }
    }

    public static <T> T[] sortArray(T[] a) {
        if (a != null) {
            Arrays.sort(a);
        }
        return a;
    }

    public static <T> T[] sortArrayClone(T[] a) {
        return a == null ? null : sortArray(a.clone());
    }

    public static String[] buildWordArray(String str) {
        String[] a = buildWordArrayNoSort(str);
        if (a != null) {
            Arrays.sort(a);
        } else {
            a = new String[]{};
        }

        return a;
    }

    static public <T extends Comparable<T>> List<T> getCommonItems(List<T> list1, List<T> list2) {
        int len1 = collectionSizeFix(list1), len2 = collectionSizeFix(list2);
        List<T> res = new ArrayList<T>();
        if (len1 == 0 || len2 == 0) {
            return res;
        }

        Collections.sort(list1);
        Collections.sort(list2);
        int idx1 = 0, idx2 = 0;

        while (idx1 < len1 && idx2 < len2) {
            T o = list1.get(idx1);
            int cmpRes = o.compareTo(list2.get(idx2));

            if (cmpRes == 0) {
                res.add(o);
            }
            if (cmpRes <= 0) {
                idx1++;
            }
            if (cmpRes >= 0) {
                idx2++;
            }
        }
        return res;
    }

    static public String nullToDefPrefixed(String prefix, String obj, String val) {
        return obj == null ? val : prefix + obj;
    }

    public static boolean linearExists(Object[] a, Object key) {
        return linearSearch(a, key) != -1;
    }

    public static boolean linearExists(Object[] a, Object key[]) {
        if (key != null) {
            for (int i = 0; i < key.length; i++) {
                if (linearExists(a, key[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String getGetter(String s) {
        return "get" + s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static String getSetter(String s) {
        return "set" + s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static void byteArrayToFile(String fileName, byte[] arr) throws IOException {
        FileOutputStream fos = new FileOutputStream(new File(fileName));
        fos.write(arr);
        fos.close();
    }

    public static void byteArrayToLogFile(String fileName, byte[] arr) throws IOException {
        byteArrayToFile(//"/opt/tomcat/logs/" +
                fileName, arr);
    }

    public static void txtToFileWithEncoding(String fileName, String encodingName, String txt) throws IOException {
        FileOutputStream fos = new FileOutputStream(new File(fileName));
        OutputStreamWriter osw = new OutputStreamWriter(fos, encodingName);
        osw.write(txt);
        osw.close();
    }

    public static void strToFile(String fileName, String txt) throws IOException {
        FileOutputStream fos = new FileOutputStream(new File(fileName));
        fos.write(txt.getBytes());
        fos.close();
    }

    // plytkie klonowanie tablicy
    public static <T> T[] cloneArray(T[] a) {
        // po prostu...
        return a.clone();
    }

    public static byte[] inputStreamToByteArray(InputStream inpStrm) throws IOException {
        byte[] buff = new byte[1024];
        List<byte[]> lst = new ArrayList<byte[]>();
        int r;
        int totalSize = 0;

        try {
            while (true) {
                r = inpStrm.read(buff);
                if (r < 0) {
                    break;
                }

                byte[] hlpr = new byte[r];
                System.arraycopy(buff, 0, hlpr, 0, r);

                lst.add(hlpr);

                totalSize += r;
            }
        } finally {
            inpStrm.close();
        }

        byte[] res = new byte[totalSize];

        int currOffset = 0;

        for (int i = 0; i < lst.size(); i++) {
            System.arraycopy(lst.get(i), 0, res, currOffset, lst.get(i).length);
            currOffset += lst.get(i).length;
        }

        return res;
    }

    /**
     * @param b true -> 1, false -> 0
     */
    static public int boolToInt(boolean b) {
        return b ? 1 : 0;
    }

    static public int intSignum(int val) {
        if (val < 0) {
            return -1;
        }
        if (val > 0) {
            return 1;
        }
        return 0;
    }

    static public <T extends Comparable<T>> boolean isSortedArray(T[] a, int direction) {
        if (arrLengthFix(a) < 2) {
            return true;
        }

        int dirSign = intSignum(direction);
        T firstElem = a[0];
        for (int i = 1; i < a.length; i++) {
            int ecSign = firstElem.compareTo(a[i]);
            if (ecSign != dirSign) {
                return false;
            }
        }

        return true;
    }

    /* Deletes all files and subdirectories under dir.
     * Returns true if all deletions were successful.
     * If a deletion fails, the method stops attempting to delete and returns false.
     */
    public static boolean killDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                File chldFile = new File(dir, children[i]);
                boolean success;
                if (chldFile.isDirectory()) {
                    success = killDir(new File(dir, children[i]));
                } else {
                    success = chldFile.delete();
                }
                if (!success) {
                    return false;
                }
            }

            // The directory is now empty so delete it
            return dir.delete();
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    static public <T> T[] mergeArrays(T[] a1, T[] a2) {
        T[] res = (T[]) Array.newInstance(a1.getClass().getComponentType(),
                a1.length + a2.length);

        System.arraycopy(a1, 0, res, 0, a1.length);
        System.arraycopy(a2, 0, res, a1.length, a2.length);

        return res;
    }

    public static String normalizujStrPl(String str) {
        if (str == null) {
            return null;
        }

        str = str.toLowerCase();
        str = maleLiteryNaPolskawe(str);
        str = highLowAsciiToSpace(str);
        str = str.replaceAll("[ ]+", " ").trim();
        return str;
    }

    // litery tylko polskawe
    public static String maleLiteryCyfryISpacje(String str) {
        char[] chrs = maleLiteryNaPolskawe(str.toLowerCase()).toCharArray();

        for (int i = 0; i < str.length(); i++) {
            char c = chrs[i];
            if ((c < '0') || (c > '9' && c < 'a') || (c > 'z')) {
                chrs[i] = ' ';
            }
        }

        return new String(chrs).replaceAll("[ ]+", " ").trim();
    }

    // litery tylko polskawe
    public static String maleLiteryICyfry(String str) {
        char[] chrs = maleLiteryNaPolskawe(str.toLowerCase()).toCharArray();

        for (int i = 0; i < str.length(); i++) {
            char c = chrs[i];
            if ((c < '0') || (c > '9' && c < 'a') || (c > 'z')) {
                chrs[i] = ' ';
            }
        }

        return new String(chrs).replaceAll("[ ]+", "");
    }

    public static boolean isEmptyStr(String str) {
        return str == null || str.equals("");
    }

    public static Integer parseIntOrNull(String s) {
        return tryParseInteger(s);
    }

    //static public <T> List<T> arrayToList(T[] a) {
    //    return Arrays.asList(a);
    //}
    static public <T> List<T> arrayToArrayList(T[] a) {
        return new ArrayList<T>(arrayToList(a));
    }

    @SuppressWarnings("unchecked")
    static public <K> List<K> setToSortedList(Set<K> ks) {
        K[] a = (K[]) ks.toArray();
        return arrayToList(sortArray(a));
    }

    static public <K, V> List<K> mapKeysToSortedList(Map<K, V> m) {
        return setToSortedList(m.keySet());
    }

    static public <K, E> boolean mapOfSetContainsKeyElem(Map<K, Set<E>> m, K key, E elem) {
        Set<E> s = m.get(key);
        return (s != null) && s.contains(elem);
    }
    static private Runtime runtime = null;

    protected static void initRuntimeOnce() {
        if (runtime == null) {
            runtime = Runtime.getRuntime();
        }
    }

    static public long usedMemory() {
        initRuntimeOnce();

        return runtime.totalMemory() - runtime.freeMemory();
    }

    public static long getUsedMemoryAfterGc_Slow() {
        runGc(10, 100);
        return usedMemory();
    }

    public static Map<String, Long> getMemoryDiagVals(boolean useGcSlow) {
        initRuntimeOnce();

        if (useGcSlow) {
            runGc(10, 100);
        }

        Map<String, Long> m = new LinkedHashMap<String, Long>();

        long total = runtime.totalMemory();
        long free = runtime.freeMemory();
        m.put("usedHeap", total - free);
        m.put("freeHeap", free);
        m.put("totalHeap", total);
        m.put("maxHeap", runtime.maxMemory());

        try {
            MemoryMXBean mx = ManagementFactory.getMemoryMXBean();
            m.put("initHeap", mx.getHeapMemoryUsage().getInit());
            MemoryUsage mu = mx.getNonHeapMemoryUsage();
            long totalNH = mu.getCommitted();
            long usedNH = mu.getUsed();
            m.put("usedNonH", usedNH);
            m.put("freeNonH", totalNH - usedNH);
            m.put("totalNonH", totalNH);
            m.put("maxNonH", mu.getMax());
            m.put("initNonH", mu.getInit());
        } catch (Exception ex) {
            m.put("noHeapInfo", -1L);
        }

        return m;
    }

    public static String getMemoryDiagInfoCompact(boolean useGcSlow, String sep) {
        return BaseUtils.formatMapOfIntNumbers(getMemoryDiagVals(useGcSlow), ": ", sep, null, false, true);
    }

    static public String usedMemTxt() {
        return "[usedmem:" + usedMemory() + "]";
    }

    static public void runGc(int loopCount) {
        runGc(loopCount, 0);
    }

    static public void runGc(int loopCount, int sleepMillis) {
        long usedMem = usedMemory();
        if (logger.isInfoEnabled()) {
            logger.info("invoking gc " + loopCount + " time(s), used mem:" + usedMem);
        }
        for (int i = 0; i < loopCount; i++) {
            runtime.gc();
            if (sleepMillis > 0) {
                threadSleep(sleepMillis);
            }
        }
        long usedMem2 = usedMemory();
        if (logger.isInfoEnabled()) {
            logger.info("invoked gc, used mem:" + usedMem2 + ", gained: " + (usedMem - usedMem2));
        }
    }

    static public void runGc() {
        runGc(1);
    }

    @SuppressWarnings("unchecked")
    static public <T extends Comparable> List<T> sortSrcRemoveDuplicates(List<T> lst) {
        Collections.sort(lst);
        T prevElem = null;
        ArrayList<T> res = new ArrayList<T>(lst.size());
        for (T elem : lst) {
            if (prevElem == null || !prevElem.equals(elem)) {
                res.add(elem);
            }
            prevElem = elem;
        }
        res.trimToSize();
        return res;
    }

    static public String randomWord(String letters, int minLen, int maxLen) {
        int len = randomIntInRange(minLen, maxLen);
        char[] chrs = new char[len];
        for (int i = 0; i < len; i++) {
            chrs[i] = letters.charAt((int) (Math.random() * letters.length()));
        }
        return new String(chrs);
    }

    static public String[] randomWords(String letters, int minWordLen, int maxWordLen, int minArrLen, int maxArrLen) {
        int arrLen = randomIntInRange(minArrLen, maxArrLen);
        String[] res = new String[arrLen];
        for (int i = 0; i < arrLen; i++) {
            res[i] = randomWord(letters, minWordLen, maxWordLen);
        }
        return res;
    }

    static public int charCount(char c, String str) {
        return str.length() - str.replace(String.valueOf(c), "").length();
    }

    // poczatek
    // sciezka
    static public String getStartPath(String path, char sep, int poziom) {
        int ileuciac = charCount(sep, path) - poziom;
        if (ileuciac <= 0) {
            return path;
        }

        while (ileuciac >= 0) {
            path = path.substring(0, path.lastIndexOf(sep));
            //System.out.println( path );
            ileuciac = ileuciac - 1;
        }

        return path + "-";
    }

    // dodac do Lame Utils
    static public String getParentPath(String path, char sep) {
        if (charCount(sep, path) < 1) {
            return "";
        } else {
            return path.substring(0, path.lastIndexOf(String.valueOf(sep), path.length() - 2) + 1);
        }
    }

    //static public Collection<String> splitBySepIntoColl(String str, String sep, Collection<String> coll) {
    public static enum SqlFlavor {

        Postgres, Other
    };

    public static String toSqlString(Object v, SqlFlavor flavor) {
        String s;
        if (v == null) {
            s = "NULL";
        } else if (v instanceof String) {
            String inner = ((String) v).replace("'", "''");
            if (flavor == SqlFlavor.Postgres) {
                inner = inner.replace("\\", "\\\\");
            }
            s = "'" + inner + "'";
        } else {
            s = v.toString();
        }
        return s;
    }

//    static public void serCode() throws IOException, ClassNotFoundException {
//        ObjectInputStream ois = null;//new ObjectInputStream();
//        ois.readObject();
//    }
    public static int safeHashCode(Object o) {
        if (o == null) {
            return 0;
        }
        return o.hashCode();
    }

    public static <T> List<T> makeIterable(Iterator<T> iter) {
        List<T> list = new ArrayList<T>();
        while (iter.hasNext()) {
            list.add(iter.next());
        }
        return list;
    }

    public static <T, T1 extends T, T2 extends T> boolean collectionEqual(Collection<T1> c1, Collection<T2> c2) {
        return (c1.containsAll(c2) && (c1.size() == c2.size()));
    }

    public static String uniqueName(String name, Collection<String> names) {
        String result;
        if (names.contains(name)) {
            int i = 1;
            while (names.contains(name + i)) {
                i++;
            }
            result = name + i;
        } else {
            result = name;
        }
        return result;
    }

    public static String normalizeStr(String str) {
        if (str == null) {
            return null;
        }

        str = str.replaceAll("[ \t\n\r]+", " ").trim();
        return str;
    }

    public static <T> Set<T> setSubtraction(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<T>(set1);
        result.removeAll(set2);
        return result;
    }

    public static <T> Set<T> setIntersection(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<T>(set1);
        result.retainAll(set2);
        return result;
    }

    // n-ty wg.sortowania z mapy
    public static <T> T getFromSortedMap(Map<String, T> map, int index) {
        if (index < 0 || map.size() < index) {
            throw new IndexOutOfBoundsException();
        }

        List<String> pks = new ArrayList<String>(map.keySet());
        Collections.sort(pks);
        return map.get(pks.get(index));
    }

    public static boolean equalStrings(String str1, String str2, boolean caseInsensitive) {
        if (str1 == null) {
            return str2 == null;
        }
        if (caseInsensitive) {
            return str1.equalsIgnoreCase(str2);
        }
        return str1.equals(str2);
    }

    public static Field getField(Object obj, String propName, boolean caseInsensitive) throws NoSuchFieldException {
        return getField(obj.getClass(), propName, caseInsensitive);
    }

    public static Field getField(Class clazz, String propName, boolean caseInsensitive) throws NoSuchFieldException {
        Field fld = null;

        if (caseInsensitive) {
            for (Field fld2 : clazz.getFields()) {
                if (LameUtils.equalStrings(fld2.getName(), propName, caseInsensitive)) {
                    fld = fld2;
                    break;
                }
            }
        } else {
            fld = clazz.getField(propName);
        }
        return fld;
    }

    public static <K1, K2, V1, V2> boolean hasSubMap(Map<K1, V1> map, Map<K2, V2> subMap) {
        for (Entry<K2, V2> e : subMap.entrySet()) {
            Object vs = e.getValue();
            Object vm = map.get(e.getKey());
            if (!safeEquals(vs, vm)) {
                return false;
            }
        }
        return true;
    }

    public static <K1, K2, V1, V2> boolean hasSubMapAsStrings(Map<K1, V1> map, Map<K2, V2> subMap) {
        for (Entry<K2, V2> e : subMap.entrySet()) {
            Object vs = e.getValue();
            Object vm = map.get(e.getKey());
            if (vs != vm) {
                if (vs == null || vm == null || !vs.toString().equals(vm.toString())) {
                    return false;
                }
            }
        }
        return true;
    }

    static public Map<String, Object> filterOutMap(Map<String, Object> map, Collection<String> keys) {
        if (map == null) {
            throw new LameRuntimeException("map is null");
        }
        if (keys == null) {
            throw new LameRuntimeException("keys == null");
        }
        Map<String, Object> res = new LinkedHashMap<String, Object>();
        for (Entry<String, Object> e : map.entrySet()) {
            if (!keys.contains(e.getKey())) {
                res.put(e.getKey(), e.getValue());
            }
        }
        return res;
    }

    static public <K, V> String mapToString(Map<K, V> map, Collection<K> keys, String sep) {
        StringBuilder sb = new StringBuilder();
        boolean putSep = false;
        for (K key : keys) {
            if (putSep) {
                sb.append(sep);
            }
            putSep = true;
            sb.append(map.get(key));
        }
        return sb.toString();
    }

    @Deprecated
    static public String loadAsStringFail(InputStream is, String encoding) {
        try {
            int x = is.available();
            byte b[] = new byte[x];
            is.read(b);
            return new String(b, encoding);
        } catch (Exception ex) {
            throw new LameRuntimeException(/*"loadAsString: " + fileName,*/ex);
        }
    }

    public static String byteArrayToStringWithAutoDecodeInner(byte[] bb, Collection<String> encodings) throws UnsupportedEncodingException {

        int len = bb.length;

        if (len == 0) {
            return "";
        }

        // spr. potencjalne encodingi utf najpierw
        if (len >= 2) {
            int b = bb[0] & 0xff;
            int b1 = bb[1] & 0xff;

            if (logger.isDebugEnabled()) {
                logger.debug("byteArrayToStringWithAutoDecodeInner: first 2 bytes: " + byteToHex(b) + ":" + byteToHex(b1)
                        + ", is first byte 0xef=" + (b == 0xef));
            }

            if (b == 0xef) {
                // mo�e utf-8
//                System.out.println("utf-8 maybe");
                if (len >= 3) {
                    if (b1 == 0xbb && (bb[2] & 0xff) == 0xbf) {
//                        System.out.println("utf-8 for sure");
                        // jest bom dla utf-8
                        if (logger.isDebugEnabled()) {
                            logger.debug("byteArrayToStringWithAutoDecodeInner: detected utf-8 by BOM");
                        }
                        return new String(bb, 3, len - 3, "UTF8");
                    }
                }
            } else if (b == 0xfe && b1 == 0xff || b == 0xff && b1 == 0xfe) {
                // utf-16 BE lub utf-16 LE
//                System.out.println("utf-16 for sure");
                if (logger.isDebugEnabled()) {
                    logger.debug("byteArrayToStringWithAutoDecodeInner: detected utf-16 by BOM");
                }
                return new String(bb, "UTF16");
            }
        }

        // specjalny przypadek - UTF8 ale bez BOM
        try {
            CharsetDecoder cd = Charset.forName("utf8").newDecoder();
            String res = cd.decode(ByteBuffer.wrap(bb)).toString();
            if (logger.isDebugEnabled()) {
                logger.debug("byteArrayToStringWithAutoDecodeInner: detected utf-8 by decode without errors");
            }
            return res;
        } catch (Exception ex) {
            //ok, mo�e by� b��d, wtedy kontynuuj
        }

        // to nie by�y utfy, wi�c lecimy po kolei wg listy
        for (String enc : encodings) {
            try {
                CharsetDecoder cd = Charset.forName(enc).newDecoder();
                String res = cd.decode(ByteBuffer.wrap(bb)).toString();
                if (logger.isDebugEnabled()) {
                    logger.debug("byteArrayToStringWithAutoDecodeInner: detected encoding \"" + enc + "\" by decode without errors");
                }
                return res;
            } catch (Exception ex) {
                //ok, mo�e by� b��d, wtedy kontynuuj
            }
        }

        throw new LameRuntimeException("cannot properly decode string from given byte[]");
    }

    public static String byteArrayToStringWithAutoDecode(byte[] bb, Collection<String> encodings) {
        try {
            return byteArrayToStringWithAutoDecodeInner(bb, encodings);
        } catch (UnsupportedEncodingException ex) {
            throw new LameRuntimeException("cannot decode string", ex);
        }
    }

    static public String loadAsString(InputStream is, String encoding) {
        if (is == null) {
            throw new LameRuntimeException("input stream is null (no file)");
        }
        try {
            byte[] b = inputStreamToByteArray(is);

            if (logger.isDebugEnabled()) {
                logger.debug("first bytes as hex: " + appendBytesAsHex(new StringBuilder(8), b, 0, 4));
            }

            if (encoding.startsWith("*")) {
                List<String> encodings = BaseUtils.splitBySep(encoding.substring(1), ",", true);
                if (logger.isDebugEnabled()) {
                    logger.debug("autodectecting: " + encodings);
                }
                return byteArrayToStringWithAutoDecode(b, encodings);
            }

            String res = new String(b, encoding);
            // a mo�e tam jest BOM?
            if (!res.isEmpty()) {
                boolean hasBOM = res.charAt(0) == 0xfeff;
                if (hasBOM) {
                    res = res.substring(1);
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("loadAsString: input stream does" + (hasBOM ? "" : " not") + " have BOM");
                }
            }

//
//            System.out.println("file read, length: " + res.length() + ", byte array size: " + b.length + ", encoding: " + encoding);
//            System.out.println("---------------- file content start --------------------");
//            System.out.println(res);
//            System.out.println("---------------- file content end   --------------------");
            return res;
        } catch (Exception ex) {
            throw new LameRuntimeException(/*"loadAsString: " + fileName,*/ex);
        }
    }
    static private String defaultSystemEncoding = null;

    static public String getDefaultSystemEncoding() {
        if (defaultSystemEncoding == null) {
            defaultSystemEncoding
                    = Charset.defaultCharset().name();
            //new InputStreamReader(
            //new ByteArrayInputStream(new byte[0])).getEncoding();
        }
        return defaultSystemEncoding;
    }

    static public String loadAsString(InputStream is) {
        return loadAsString(is, getDefaultSystemEncoding());
//        try {
//            int x = is.available();
//            byte b[] = new byte[x];
//            is.read(b);
//            return new String(b);
//        } catch(Exception ex) {
//            throw new LameRuntimeException(/*"loadAsString: " + fileName,*/ ex);
//        }
    }

    static public String loadAsString(String fileName, String encoding) {
        try {
            return loadAsString(new FileInputStream(fileName), encoding);
        } catch (Exception ex) {
            throw new LameRuntimeException("loadAsString: " + fileName, ex);
        }
    }

    static public String loadAsString(String fileName) {
        return loadAsString(fileName, getDefaultSystemEncoding());
//        try {
//            return loadAsString(new FileInputStream(fileName));
//        } catch(Exception ex) {
//            throw new LameRuntimeException("loadAsString: " + fileName, ex);
//        }
    }

    static public long getLastModified(String fileName) {
        return new File(fileName).lastModified();
    }

    public static BigDecimal objectAsBigDecimal(Object obj) {
        BigDecimal result;
        if (obj == null) {
            result = null;
        } else if (obj instanceof Double) {
            result = new BigDecimal((Double) obj);
        } else if (obj instanceof Integer) {
            result = new BigDecimal((Integer) obj);
        } else if (obj instanceof BigDecimal) {
            result = (BigDecimal) obj;
        } else if (obj instanceof Long) {
            result = new BigDecimal(((Long) obj).longValue());
        } else if (obj instanceof String) {
            result = new BigDecimal((String) obj);
        } else {
            throw new LameRuntimeException("Nie da si� skonwertowa� na big decimal: " + obj.getClass());
        }
        return result;
    }

    public static Integer objectAsInteger(Object obj) {
        Integer result;
        if (obj == null) {
            result = null;
        } else if (obj instanceof Integer) {
            result = ((Integer) obj).intValue();
        } else if (obj instanceof BigDecimal) {
            result = ((BigDecimal) obj).intValue();
        } else if (obj instanceof Long) {
            result = ((Long) obj).intValue();
        } else if (obj instanceof String) {
            result = Integer.parseInt((String) obj);
        } else {
            throw new LameRuntimeException("Nie da si� skonwertowa� na integer: " + obj.getClass());
        }
        return result;
    }

    /*
     public static void logProfiling(Logger logger, String message, long nanoStart) {
     long now = System.nanoTime();
     if (logger.isInfoEnabled())
     logger.info(String.format("profiler: %s: %.3f second(s)", message, (now - nanoStart) / 1000000000f));
     }
     public static long logProfilingStart(Logger logger, String message) {
     if (logger.isDebugEnabled())
     logger.debug("profiler: " + message + ": starting...");
     return System.nanoTime();
     }
     public static void logFormat(Logger logger, Priority priority, String message, Object... args) {
     if (logger.isEnabledFor(priority))
     logger.log(priority, String.format(message, args));
     }
     */
    /*
     public static String decodeForURL(String s) throws UnsupportedEncodingException {
     return URLDecoder.decode(s, "utf-8");
     }
     */
    public static <K, V> List<Pair<K, V>> mapToPairList(Map<K, V> la) {
        List<Pair<K, V>> result = new ArrayList<Pair<K, V>>();
        for (Entry<K, V> entry : la.entrySet()) {
            result.add(new Pair<K, V>(entry.getKey(), entry.getValue()));
        }
        return result;
    }

    public static <T> void addIfNotExists(Collection<T> source, Collection<T> coll) {
        for (T elem : coll) {
            if (!source.contains(elem)) {
                source.add(elem);
            }
        }
    }

    public static String cutPrefix(String str, String sep) {
        int index = str.indexOf(sep);
        String result;
        if (index == -1) {
            result = str;
        } else {
            result = str.substring(index + sep.length());
        }
        return result;
    }

    public static String cutPostfix(String str, String sep) {
        int index = str.lastIndexOf(sep);
        String result;
        if (index == -1) {
            result = str;
        } else {
            result = str.substring(0, index);
        }
        return result;
    }

    public static String stringBetween(String str, String fromStr, String toStr) {
        int idx = str.indexOf(fromStr);
        if (idx == -1) {
            return "";
        }

        str = str.substring(idx + fromStr.length());

        int idx2 = str.indexOf(toStr);
        if (idx2 == -1) {
            return "";
        }

        return str.substring(0, idx2);
    }

    public static Pair<String, List<String>> parseParamStm(String str, String startStr, String endStr, String paramStr) {
        List<String> params = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();

        int idxStartStm = 0;
        int idxStart;
        int idxEnd = 0;

        while (str.indexOf(startStr, idxStartStm) != -1) {
            idxStart = str.indexOf(startStr, idxStartStm);

            idxEnd = str.indexOf(endStr, idxStart + startStr.length());
            //   System.out.println(idxStart + "," + idxEnd);
            if (idxEnd == -1) {
                throw new LameRuntimeException("Brak zako�czenia pola w wyrazeniu " + str);
            }

            params.add(str.substring(idxStart + startStr.length(), idxEnd));
            sb.append(str.substring(idxStartStm, idxStart) + paramStr);

            idxStartStm = idxEnd + endStr.length();
        }
        sb.append(str.substring(idxStartStm));
        return new Pair<String, List<String>>(sb.toString(), params);
    }

// jak getterSetterName nie jest nazw� metody getter/setter (wa�ny prefix), to daje null
    public static String getFieldForGetterSetter(String getterSetterName, String prefix) {
        if (getterSetterName.length() > prefix.length() && getterSetterName.startsWith(prefix)) {
            char c = getterSetterName.charAt(prefix.length());
            if (c >= 'A' && c <= 'Z' || c == '_') {
                return Character.toLowerCase(c) + getterSetterName.substring(prefix.length() + 1);
            } else {
                return null;
            }
        }
        return null;
    }

    public static String getFieldForGetter(String getterName) {
        return getFieldForGetterSetter(getterName, "get");
    }

    public static String getFieldForSetter(String setterName) {
        return getFieldForGetterSetter(setterName, "set");
    }

    public static <K, V> void removeByKeys(Map<K, V> map, Collection<K> coll) {
        map.keySet().removeAll(coll);
    }

    public static <V, T> Map<T, Integer> countFrequency(Collection<V> coll, Projector<V, T> projector) {
        Map<T, Integer> res = new LinkedHashMap<T, Integer>();
        for (V val : coll) {
            T name = projector.project(val);
            Integer cnt = res.get(name);
            if (cnt == null) {
                cnt = 0;
            }
            res.put(name, cnt + 1);
        }
        return res;
    }

    public static <V, T> Map<T, Integer> findDuplicates(Collection<V> coll, Projector<V, T> projector) {
        Map<T, Integer> freq = countFrequency(coll, projector);

        Map<T, Integer> res = new LinkedHashMap<T, Integer>();
        for (Entry<T, Integer> e : freq.entrySet()) {
            if (e.getValue() > 1) {
                res.put(e.getKey(), e.getValue());
            }
        }

        return res;
    }

    public static void logProfiling(ILameLogger logger, String message, long nanoStart) {
        long now = System.nanoTime();
        if (logger.isInfoEnabled()) {
            logger.info(String.format("profiler: %s: %.3f second(s)", message, (now - nanoStart) / 1000000000f));
        }
    }

    public static long logProfilingStart(ILameLogger logger, String message) {
        if (logger.isDebugEnabled()) {
            logger.debug("profiler: " + message + ": starting...");
        }
        return System.nanoTime();
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] sliceArray(T[] arr, int startIdx, int endIdx) {
        int len = endIdx - startIdx + 1;
        T[] res = (T[]) Array.newInstance(arr.getClass().getComponentType(), len);
        System.arraycopy(arr, startIdx, res, 0, len);
        return res;
    }

    public static boolean equalInts(Integer x, int y) {
        return x != null && x == y;
    }

    public static <K, V> V safeMapGet(Map<K, V> m, K key) {
        return m == null ? null : m.get(key);
    }

    public static int nthIndexOf(String s, String sep, int cnt) {
        int res = -1;
        while (cnt > 0) {
            res = s.indexOf(sep, res + 1);
            if (res == -1) {
                break;
            }
            cnt--;
        }
        return res;
    }

    public static Pair<String, String> splitStringCnt(String s, String sep, int cnt) {
        int index = nthIndexOf(s, sep, cnt);
        if (index == -1) {
            return new Pair<String, String>(s, null);
        } else {
            return new Pair<String, String>(s.substring(0, index), s.substring(index + sep.length(), s.length()));
        }
    }

    public static <T> List<T> makeIterable(Iterable<T> iterable) {
        return makeIterable(iterable.iterator());
    }

    static public <T, V> List<V> projectArrayToList(T[] a, Projector<T, V> projector) {
        if (a == null) {
            return null;
        }
        List<V> res = new ArrayList<V>(a.length);
        for (T e : a) {
            res.add(projector.project(e));
        }
        return res;
    }

    static public Map<String, Object> prefixKeys(Map<String, Object> map, String prefix) {
        Map<String, Object> res = new HashMap<String, Object>((int) (map.size() / 0.75f), 0.75f);
        for (Entry<String, Object> e : map.entrySet()) {
            res.put(prefix + e.getKey(), e.getValue());
        }
        return res;
    }

    static public String readerToString(Reader r) throws IOException {
        StringBuilder sb = new StringBuilder();

        char[] buff = new char[1024];
        int rd;
        int totalSize = 0;

        while (true) {
            rd = r.read(buff);
            if (rd < 0) {
                break;
            }

            sb.append(buff);

            totalSize += rd;
        }

        return sb.toString();
    }

    static public <K, V> Map<K, V> retainKeysInMap(Map<K, V> map, Collection<K> keys) {
        if (map == null) {
            throw new LameRuntimeException("map is null");
        }
        if (keys == null) {
            throw new LameRuntimeException("keys == null");
        }
        Map<K, V> res = new LinkedHashMap<K, V>();
        for (Entry<K, V> e : map.entrySet()) {
            if (keys.contains(e.getKey())) {
                res.put(e.getKey(), e.getValue());
            }
        }
        return res;
    }

    static public <K> boolean safeContains(Collection<K> coll, K obj) {
        return coll != null && coll.contains(obj);
    }

    static public String extractFileNameWithoutExt(String filePath) {
        File f = new File(filePath);
        String name = f.getName();
        int idx = name.lastIndexOf(".");
        if (idx == -1) {
            return name;
        }
        return name.substring(0, idx);
    }

    static public String extractFileExtNoDot(String filePath) {
        File f = new File(filePath);
        String name = f.getName();
        int idx = name.lastIndexOf(".");
        if (idx == -1) {
            return "";
        }
        return name.substring(idx + 1);
    }

    static public String extractFileDir(String filePath) {
        int idx;
        if (filePath == null) {
            return null;
        }
        idx = Math.max(filePath.lastIndexOf("\\"), filePath.lastIndexOf("/"));
        if (idx == -1) {
            return null;
        }
        return filePath.substring(0, idx + 1);
    }

    static public <T> Set<T> safeSumSets(Set<T> a, Set<T> b) {
        if (a == null) {
            return b;
        }
        if (b == null) {
            return a;
        }
        Set<T> res = new HashSet<T>(a);
        res.addAll(b);
        return res;
    }

    static public <T> boolean containsAny0(Collection<T> c4contains, Collection<T> c2iter) {
        if (collectionSizeFix(c4contains) > 0 && collectionSizeFix(c2iter) > 0) {
            for (T elem : c2iter) {
                if (c4contains.contains(elem)) {
                    return true;
                }
            }
        }
        return false;
    }

    static public <T> boolean containsAny(Collection<T> c1, Collection<T> c2) {
        return c1.size() < c2.size() ? containsAny0(c2, c1) : containsAny0(c1, c2);
    }
    static private Map<Class<Enum>, Map<Integer, Enum>> enumValsForClasses = new HashMap<Class<Enum>, Map<Integer, Enum>>();

    //static public <E extends Enum<E>> Map<Integer, E> enumValsToMap(Class<E> clazz) {
    static public Map<Integer, Enum> enumValsToMap(Class<Enum> clazz) {
        Map<Integer, Enum> res = new HashMap<Integer, Enum>();
        for (Enum e : clazz.getEnumConstants()) {
            res.put(e.ordinal(), e);
        }
        return res;
    }

    /**
     * @deprecated @see #intToEnum
     */
    @SuppressWarnings("unchecked")
    static public <E extends Enum> E intToEnum0(Class<E> clazz, int ordinal) {
        Map<Integer, Enum> map = enumValsForClasses.get(clazz);
        if (map == null) {
            map = (Map<Integer, Enum>) (enumValsToMap((Class<Enum>) clazz));
            enumValsForClasses.put((Class<Enum>) clazz, map);
        }
        return (E) (map.get(ordinal));
    }

    static public <E extends Enum> E intToEnum(Class<E> clazz, int ordinal) {
        return clazz.getEnumConstants()[ordinal];
    }

    public static boolean safeToBoolean(Object b) {
        return (new Integer(1)).equals(LameUtils.objectAsInteger(b));
    }

    /**
     * Dokleja znaki na pocz�tku
     */
    public static String addLeadingBlanks(String str, int len, char blank) {
        if (len <= str.length()) {
            return str;
        } else {
            return replicateStr(Character.toString(blank), len - str.length()) + str;
        }
    }

    public static String encodeForHTMLTagNoWordBreak(final String s, final boolean escapeBackSlash) {
        final StringBuffer result = new StringBuffer();

        if (s != null) {
            final StringCharacterIterator iterator = new StringCharacterIterator(s);
            char character = iterator.current();
            int wordLen = 0;
            while (character != CharacterIterator.DONE) {
                if (character == '<') {
                    result.append("&lt;");
                } else if (character == '>') {
                    result.append("&gt;");
                } else if (character == '\"') {
                    result.append("&quot;");
                } else if (character == '\'') {
                    result.append("&#039;");
                } else if (escapeBackSlash && character == '\\') {
                    result.append("&#092;");
                } else if (character == '&') {
                    result.append("&amp;");
                } else {
                    result.append(character);
                }

                character = iterator.next();
            }
        }

        return result.toString();
    }

    public static char intToChar(int i) {
        return (char) i;
    }

    public static String strSurrounding(String str, int pos, int beforeCnt, int afterCnt) {
        if (str == null) {
            return null;
        }

        int startPos = pos - beforeCnt;
        int endPos = pos + afterCnt;
        if (startPos < 0) {
            startPos = 0;
        }
        if (endPos > str.length()) {
            endPos = str.length();
        }

        return str.substring(startPos, endPos);
    }

    public static String getFirstTwoSentences(String str) {
        if (str == null) {
            return null;
        }

        int pos0 = -1;
        int pos1 = -1;
        int cnt = 2;

        String str2 = StringEscapeUtilsEx.unescapeHtml(str.replaceAll("\\<.*?\\>", ""));

        while (cnt > 0) {
            pos0 = pos1;
            pos1 = str2.indexOf(".", pos0 + 1);
            if (pos1 < 0) {
                return str2;
            }
            if (pos1 - pos0 >= 4) {
                cnt--;
            }
        }
        return str2.substring(0, pos1 + 1);
    }

    @SuppressWarnings("unchecked")
    public static <K, V, C extends Collection<V>> void addToCollectingMap(Map<K, C> map, K k, V v) {
//        C coll = map.get(k);
//        if (coll == null) {
//            coll = (C) new ArrayList<V>();
//            map.put(k, coll);
//        }
//        coll.add(v);
        addToCollectingMapEx(map, k, v, (Class) ArrayList.class);
    }

    public static <K, V, C extends Collection<V>, C2 extends C> void addToCollectingMapEx(Map<K, C> map, K k, V v, Class<C2> clz) {
        C coll = map.get(k);
        if (coll == null) {
            try {
                coll = //new ArrayList<V>();
                        clz.newInstance();
            } catch (Exception ex) {
                throw new LameRuntimeException("error on newInstance", ex);
            }
            map.put(k, coll);
        }
        coll.add(v);
    }

    @SuppressWarnings("unchecked")
    public static <K, IK, IV, C extends Collection<IV>, IM extends Map<IK, C>, M extends Map<K, IM>, C2 extends C> void addToCollectingMapOfMapEx(M map, K k, IK ik, IV iv,
            Class<C2> clz) {
        IM im = map.get(k);
        if (im == null) {
            im = (IM) new LinkedHashMap<IK, C>();
            map.put(k, im);
        }
        addToCollectingMapEx(im, ik, iv, clz);
    }

    @SuppressWarnings("unchecked")
    public static <K, IK, IV, C extends Collection<IV>, IM extends Map<IK, C>, M extends Map<K, IM>> void addToCollectingMapOfMap(M map, K k, IK ik, IV iv) {
        IM im = map.get(k);
        if (im == null) {
            im = (IM) new HashMap<IK, C>();
            map.put(k, im);
        }
        addToCollectingMap(im, ik, iv);
    }
    private static Pattern remHrefOpen = Pattern.compile("<[aA][ \t\n\r]+[^>]*[hH][rR][eE][fF][^>]*>");
    private static Pattern remHrefClose = Pattern.compile("<[ \t\n\r]*/[ \t\n\r]*[aA][ \t\n\r]*>");

    public static String removeHtmlLinks(String htmlTxt) {
        if (htmlTxt == null) {
            return null;
        }

        String res = remHrefOpen.matcher(htmlTxt).replaceAll("");
        res = remHrefClose.matcher(res).replaceAll("");
        return res;
    }

    public static <T, T2 extends T> void safeAddAll(Collection<T> c1, Collection<T2> c2) {
        if (c2 != null) {
            c1.addAll(c2);
        }
    }

    public static boolean fileExists(String path) {
        File file = new File(path);
        return file.exists();
    }

    public static boolean deleteFile(String path) {
        File file = new File(path);
        if (file.isFile()) {
            return file.delete();
        }
        return false;
    }

    /**
     * Ile razy dany prefiks wystepuje na poczatku ciagu
     */
    public static int prefixCount(String str, String prefix) {
        int cnt = 0;
        if (str != null && prefix != null) {
            while (str.startsWith(prefix)) {
                cnt = cnt + 1;
                str = str.substring(prefix.length());
            }
        }
        return cnt;
    }

    /**
     * Laczy sciezki Zalozenie absolutePath zaczyna sie i konczy na /
     */
    public static String mergeResourcePaths(String absolutePath, String relativePath) {
        String res;
        String upperLevelStr = "../";
        int cnt = prefixCount(relativePath, upperLevelStr);
        int pathLevel = charCount('/', absolutePath);

        if ("".equals(absolutePath) && cnt == 0) {
            res = relativePath;
        } else {

            if (!(absolutePath.startsWith("/") && absolutePath.endsWith("/"))) {
                throw new LameRuntimeException("Absolute path must starts and ends with /");
            }

            if (cnt >= pathLevel) {
                throw new LameRuntimeException("Malformed path: " + absolutePath + relativePath);
            } else {
                // skracamy
                relativePath = relativePath.substring(cnt * upperLevelStr.length());
                int index = nthIndexOf(absolutePath, "/", pathLevel - cnt);
                res = absolutePath.substring(0, index + 1) + relativePath;
            }
        }
        return res;
    }

    /*
     public static String mergeAsSqlStrings(Collection<String> strs, SqlFlavor flavor) {
     StringBuilder sb = new StringBuilder();

     boolean firstOne = true;
     for (String s : strs) {
     if (firstOne) {
     firstOne = false;
     } else {
     sb.append(",");
     }

     sb.append(toSqlString(s, flavor));
     }

     return sb.toString();
     }
     */
    @Deprecated
    public static List<String> getFilePaths(String fileOrDir, final boolean collectSubdirs) {
        List<String> res = new ArrayList<String>();
        File f = new File(fileOrDir);
        String mask = null;
        String parent = null;

        if (!f.exists()) {
            if (logger.isDebugEnabled()) {
                logger.debug("fileOrMask: \"" + fileOrDir + "\" does not exist as file or dir");
            }
            parent = f.getParent();
            mask = f.getName();
            f = new File(parent);
            if (logger.isDebugEnabled()) {
                logger.debug("mask=\"" + mask + "\", parent=\"" + parent + "\"");
            }
        }

        if (f.isDirectory()) {
            String maskRegexp = mask == null ? null : fileNameMaskToRegexp(mask);
            if (logger.isDebugEnabled()) {
                logger.debug("maskRegexp=\"" + maskRegexp + "\"");
            }
            final Pattern patt = mask == null ? null : Pattern.compile(maskRegexp);
            final File f2 = f;
            String[] children = f.list(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    //return maskRegexp == null || name.toLowerCase().matches(maskRegexp);

                    boolean isMatching = patt == null || patt.matcher(name.toLowerCase()).matches();
                    if (isMatching && !collectSubdirs) {
                        File fileInDir = new File(f2, name);
                        isMatching = !fileInDir.isDirectory();
                    }

                    return isMatching;
                }
            });

            for (String oneFileName : children) {
                res.add(new File(f, oneFileName).getPath());
            }
        } else {
            if (mask != null) {
                throw new LameRuntimeException(
                        "we have mask (" + mask + ") but parent (" + parent
                        + ") is not a dir for fileOrDir=\"" + fileOrDir + "\"");
            }
            res.add(f.getPath());
        }

        return res;
    }

    public static String fileNameMaskToRegexp(String mask) {
        StringBuilder buffer = new StringBuilder();

        boolean anyExt = mask.endsWith(".*");

        if (anyExt) {
            mask = mask.substring(0, mask.length() - 2);
        }

        char chars[] = mask.toCharArray();

        for (int i = 0; i < chars.length; ++i) {
            if (chars[i] == '*') {
                buffer.append(".*");
            } else if (chars[i] == '?') {
                buffer.append(".");
            } else if ("+()^$.{}[]|\\".indexOf(chars[i]) != -1) {
                buffer.append('\\').append(chars[i]);
            } else {
                buffer.append(chars[i]);
            }
        }

        if (anyExt) {
            buffer.append("(\\.[^.]*)?");
        }

        return buffer.toString();
    }

    public static String shortenStr(String str, int len) {
        return str.length() <= len ? str : str.substring(0, len);
    }

    public static <K, V> Map<K, V> zipToMap(Collection<K> keys, Collection<V> vals) {
        Iterator<K> kIter = keys.iterator();
        Iterator<V> vIter = vals.iterator();

        Map<K, V> res = new LinkedHashMap<K, V>();

        while (kIter.hasNext() && vIter.hasNext()) {
            res.put(kIter.next(), vIter.next());
        }

        if (kIter.hasNext()) {
            throw new LameRuntimeException("no more vals, but still have keys");
        }

        if (vIter.hasNext()) {
            throw new LameRuntimeException("no more keys, but still have vals");
        }

        return res;
    }

    public static void moveFileToDir(String fileName, String dir) {
        if (dir == null) {
            return;
        }
        File f = new File(fileName);
        if (!f.renameTo(new File(dir, f.getName()))) {
            throw new LameRuntimeException("Cannot move file: \"" + fileName + "\" to dir: \"" + dir + "\"");
        }
    }

    public static <T> List<T> makeList(T... elems) {
        return Arrays.asList(elems);
    }

    static public String extractPackageName(String qualifiedClassName) {
        int idx = qualifiedClassName.lastIndexOf(".");
        if (idx == -1) {
            throw new LameRuntimeException("inproper qualified class name (no '.'): " + qualifiedClassName);
        }
        return qualifiedClassName.substring(0, idx);
    }

    public static <T> String mergeAsSqlStrings(Collection<T> items,
            SqlFlavor flavor) {
        StringBuilder sb = new StringBuilder();

        boolean first = true;

        for (T item : items) {
            if (first) {
                first = false;
            } else {
                sb.append(",");
            }
            sb.append(toSqlString(item, flavor));
        }

        return sb.toString();
    }

    // optFormat == null jest tożsamy z "%s", ale nie wymaga wywoływania format i sprawdzania -> szybciej działa
    public static <T> String mergeWithSepEx(Iterable<T> elems, String sep, String optFormat) {
        boolean isSubseqElem = false;

        StringBuilder sb = new StringBuilder();

        for (T elem : elems) {
            if (isSubseqElem) {
                sb.append(sep);
            } else {
                isSubseqElem = true;
            }

            String s;
            if (optFormat == null) {
                s = elem.toString();
            } else {
                s = String.format(optFormat, elem);
            }

            sb.append(s);
        }

        return sb.toString();
    }

    public static String printExceptionToString(Throwable th) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(output);
        th.printStackTrace(ps);
        ps.flush();
        String res = output.toString();
        return res;
    }

    public static int getIdentityHashCode(Object o) {
        if (o == null) {
            return 0;
        }
        return System.identityHashCode(o);
    }

    public static void threadSleep(long millisToSleep) {
        try {
            if (millisToSleep > 0) {
                Thread.sleep(millisToSleep);
            }
        } catch (InterruptedException ex) {
            if (logger.isWarnEnabled()) {
                logger.warn(/*Thread.currentThread().getName() + ": " +*/"threadSleep#1: InterruptedException, isInterrupted=" + Thread.currentThread().isInterrupted());
            }

            Thread.currentThread().interrupt();

            if (logger.isWarnEnabled()) {
                logger.warn(/*Thread.currentThread().getName() + ": " +*/"threadSleep#2: InterruptedException, isInterrupted=" + Thread.currentThread().isInterrupted());
            }
        }
    }

    // val != null
    // wrapNonIterable == null -> non iterable yealds null
    public static Iterable makeIterable(Object val, Boolean wrapNonIterable) {
        if (val instanceof Iterable) {
            return (Iterable) val;
        } else if (val == null || !val.getClass().isArray()) {
            if (wrapNonIterable == null) {
                return null;
            }
            if (!wrapNonIterable) {
                throw new LameRuntimeException("val is of class " + BaseUtils.safeGetClassName(val)
                        + " which cannot be iterated");
            }
            val = new Object[]{val};
        }
        final Object finalVal = val;
        return new Iterable() {
            public Iterator iterator() {
                return new GenericArrayIterator(finalVal);
            }
        };
    }

    //c&p jest specjalnie, bo inaczej getCurrentClass(1); �le dzia�a!
    public static ILameLogger getMyLogger() {
        Class c = getCurrentClass(1);
        //System.out.println("current class = " + c);
        return LameLoggerFactory.getLogger(c);
    }
    private static final Pattern deAccentPattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");

    public static String deAccent(String str) {
        return deAccent(str, Form.NFKD);
    }

    public static String deAccentExAndLowerCase(String str) {
        return deAccent(str).toLowerCase().replace("�", "l");
    }

    public static String deAccent(String str, Normalizer.Form form) {
        String nfdNormalizedString = Normalizer.normalize(str, form);
        return deAccentPattern.matcher(nfdNormalizedString).replaceAll("");
    }

    public static void scale(InputStream input, int width, int height, OutputStream output,
            String outputFormat)
            throws IOException {
        BufferedImage bsrc = ImageIO.read(input); //read(new File(src));
        BufferedImage bdest
                = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bdest.createGraphics();
        AffineTransform at
                = AffineTransform.getScaleInstance((double) width / bsrc.getWidth(),
                        (double) height / bsrc.getHeight());
        g.drawRenderedImage(bsrc, at);
        ImageIO.write(bdest, outputFormat, output);
        //ImageIO.write(bdest, "JPG", new File(dest));
    }

    public static byte[] scaleToWH(byte[] fileContent, String fileName, int width, int height) {
        //return super.preprocessUploadedFile(fileContent);
        ByteArrayInputStream input = new ByteArrayInputStream(fileContent);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        String ext = BaseUtils.extractFileExtFromFullPath(fileName);
        try {
            scale(input, width, height, output, BaseUtils.dropOptionalPrefix(ext, "."));
            output.flush();
        } catch (IOException ex) {
            throw new LameRuntimeException("error while scaling image", ex);
        }
        return output.toByteArray();
    }

    @SuppressWarnings("unchecked")
    public static <V> Class<V> safeGetClassForName(String className) {
        try {
            return (Class<V>) Class.forName(className);
        } catch (ClassNotFoundException ex) {
            return null;
        }
    }

    public static String gainStringFromProvider(IGenericResourceProvider<InputStream> resProvider, String name,
            String encoding) {
        InputStream is = resProvider.gainResource(name);
        if (is == null) {
            return null;
        }
        return loadAsString(is, encoding);
    }

    public static String gainReqStringFromProvider(IGenericResourceProvider<InputStream> resProvider, String name,
            String encoding) {
        InputStream is = resProvider.gainResource(name);
        if (is == null) {
            throw new LameRuntimeException("no resource with name: " + name);
        }
        return loadAsString(is, encoding);
    }

    public static String getPackagePathOfClassNoExtraSeps(Class c) {
        return c.getPackage().getName().replace(".", "/");
    }

    public static String getPackagePathOfClass(Class c) {
        return "/" + getPackagePathOfClassNoExtraSeps(c) + "/";
    }

    public static String getPackagePathOfObject(Object o) {
        return getPackagePathOfClassNoExtraSeps(o.getClass());
    }

    public static String loadAsStringClassPathFile(Class classInPackage,
            String fileName, String encoding) {
        String fullFileName = getPackagePathOfClass(classInPackage) + fileName;
        InputStream is = classInPackage.getResourceAsStream(fullFileName);
        if (is == null) {
            throw new LameRuntimeException("no file: " + fullFileName);
        }
        return loadAsString(is, encoding);
    }

    public static boolean ensureDirExists(String dirPath) {
        File d = new File(dirPath);
        return d.mkdirs();
    }

    public static void saveStringNoExc(String fileName, String txt, String encoding,
            boolean createDirs, boolean addBOM) {
        try {
            saveString(fileName, txt, encoding, createDirs, addBOM);
        } catch (IOException ex) {
            throw new LameRuntimeException("error writing file: " + fileName, ex);
        }
    }

    public static void saveStringNoExc(String fileName, String txt, String encoding,
            boolean createDirs) {
        saveStringNoExc(fileName, txt, encoding, createDirs, false);
    }

    public static void saveString(String fileName, String txt, String encoding,
            boolean createDirs) throws IOException {
        saveString(fileName, txt, encoding, createDirs, false);
    }
    private static byte[] BOMbytes = {(byte) 0xEF, (byte) 0xBB, (byte) 0xBF};

    public static void saveString(String fileName, String txt, String encoding,
            boolean createDirs, boolean addBOM) throws IOException {

        if (createDirs) {
            String filePath = extractFilePath(fileName);
            ensureDirExists(filePath);
        }
        FileOutputStream fos = new FileOutputStream(new File(fileName));
        try {
            if (addBOM) {
                fos.write(BOMbytes);
            }
            fos.write(txt.getBytes(encoding));
        } finally {
            fos.close();
        }
    }

    // nonAlphaReplacement -> must be in regexpr compatible format (e.g. \\$ for $)
    public static String deAccentAndAlphaNumOnly(String txt, String nonAlphaReplacement) {
        if (txt == null) {
            return null;
        }
        txt = deAccentExAndLowerCase(txt);
        String nonAlphaNums = "[^a-zA-Z0-9]+";
        txt = txt.replaceAll(nonAlphaNums + "$", "");
        txt = txt.replaceAll("^" + nonAlphaNums, "");
        txt = txt.replaceAll(nonAlphaNums, nonAlphaReplacement);
        return txt;
    }

    public static Properties readPropsFileFromResProvider(ILameResourceProvider resProvider, String propsFilePath, boolean optional) {
        InputStream is = resProvider.gainResource(propsFilePath);
        return readPropsFileFromInputStream(is, propsFilePath, optional);
    }

    public static Properties readPropsFileFromInputStream(InputStream is, String propsFilePath, boolean optional) {
        Properties res = new Properties();

        if (is == null) {
            if (optional) {
                return null;
            } else {
                throw new LameRuntimeException("cannot find props file: " + propsFilePath);
            }
        }

        try {
            try {
                res.load(is);
            } finally {
                is.close();
            }
        } catch (Exception ex) {
            throw new LameRuntimeException("cannot read props file: " + propsFilePath, ex);
        }
        return res;
    }

    public static List<String> getClassNamesInJarFile(String filePath, String optRegExpFilter) {
        try {
            InputStream is = new FileInputStream(filePath);
            Pattern patt = optRegExpFilter != null ? Pattern.compile(optRegExpFilter) : null;

            ArrayList<String> res = new ArrayList<String>();

            JarInputStream jarFile = new JarInputStream(is);

            while (true) {
                JarEntry jarEntry = jarFile.getNextJarEntry();
                if (jarEntry == null) {
                    break;
                }

                String cn = jarEntry.getName();

                if (cn.endsWith(".class")) {
                    cn = cn.substring(0, cn.length() - 6).replace("/", ".");
                    if (patt == null || patt.matcher(cn).find()) {
                        res.add(cn);
                    }
                }
            }

            return res;
        } catch (Exception e) {
            throw new RuntimeException(
                    "getClassNamesInJarFile(" + filePath + ", " + optRegExpFilter + ")", e);
        }
    }

    // System.out.println("found jars: " + getClassNamesInJars("c:/Users/wezyr/Desktop/java/lib", false, "IXMLSerializable$", false));
    public static Map<String, List<String>> getClassNamesInJars(String dirWithJars, boolean recurseSubdirs,
            String optRegExpFilter, boolean appendNoResultJarNames) {
        Map<String, List<String>> res = new LinkedHashMap<String, List<String>>();

        Set<String> fileNames = BaseUtils.listResources(FileNamesProvider.getInstance(), dirWithJars,
                BaseUtils.paramsAsSet(".jar"), recurseSubdirs);

        for (String fileName : fileNames) {
            List<String> classNames = getClassNamesInJarFile(fileName, optRegExpFilter);
            if (appendNoResultJarNames || !classNames.isEmpty()) {
                res.put(fileName, classNames);
            }
        }

        return res;
    }

    public static void logExtendedDiagVals() {
        System.out.println("============ EXTENDED DIAG VALS - START ============");
        System.out.println("System environments: " + System.getenv());
        Properties properties = System.getProperties();
        System.out.println("System properties: ");
        Collection<Object> values = properties.keySet();
        for (Object key : values) {
            System.out.println("Property: " + key + ", value: " + properties.getProperty(key.toString()));
        }
        System.out.println("Memory diag vals: " + getMemoryDiagVals(false));
        System.out.println("============ EXTENDED DIAG VALS - END ============");
    }

    public static String getSysComputerName() {
        String envCompName = null;
        String netCompName = null;
        try {
            envCompName = System.getenv("COMPUTERNAME");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            envCompName = System.getenv("COMPUTERNAME");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return isEmptyStr(envCompName) ? netCompName : envCompName;
    }

    public static String getSysUserName() {
        return System.getProperty("user.name");
    }

    public static String removeNonCanonicSysChars(String sysName) {
        if (sysName == null) {
            return null;
        }
        sysName = LameUtils.deAccentExAndLowerCase(sysName);
        sysName = sysName.replaceAll("[^A-Za-z0-9\\-._]+", "");
        return sysName;
    }

    public static void tryReadAndApplyLameLoggersConfig(InputStream is) {
        Properties lameLoggersConfig = LameUtils.readPropsFileFromInputStream(is,
                "file name unimportant", true);
//        System.out.println("lameLoggersConfig=" + lameLoggersConfig);
        tryReadAndApplyLameLoggersConfig(lameLoggersConfig);

    }

    public static void tryReadAndApplyLameLoggersConfig(String classResourceName) {
//        System.out.println("classResourceName=" + classResourceName);
        InputStream is = LameUtils.class.getResourceAsStream(classResourceName);
        tryReadAndApplyLameLoggersConfig(is);
    }

    public static void tryReadAndApplyLameLoggersConfig(Properties lameLoggersConfig) {
        if (lameLoggersConfig != null) {
            @SuppressWarnings("unchecked")
            Map<String, String> cfgAsMap = (Map) lameLoggersConfig;

            //System.out.println("has config: " + lameLoggersConfig);
            LameLoggerFactory.applyLameLoggersConfig(cfgAsMap, false);
        } else {
            //System.out.println("no config!");
        }
    }

    public static String getCurrentStackTrace() {
        StringBuilder sb = new StringBuilder();

        StackTraceUtil.appendStackTrace(Thread.currentThread().getStackTrace(), sb, 2);

        return sb.toString();
    }

    @SuppressWarnings("unchecked")
    public static <T> Map<String, T> projectBeanCollToMapByName(Collection<T> beans) {
        return BaseUtils.projectToMap(beans, MuppetMerger.namePropProjector);
    }

    public static String printExceptionAsString(Exception ex) {
        CharArrayWriter cw = new CharArrayWriter();
        PrintWriter w = new PrintWriter(cw);
        ex.printStackTrace(w);
        w.close();
        return cw.toString();
    }

    public static List<File> getFileListInFolder(String folderPath, String... fileTypes) throws IOException {
        List<File> res = new ArrayList<File>();
        List<String> queue = new ArrayList<String>();
        queue.add(folderPath);
        for (int i = 0; i < queue.size(); i++) {
            File folder = new File(queue.get(i));
            File[] filesList = folder.listFiles();
            for (File file : filesList) {
                if (file.isDirectory()) {
                    queue.add(file.getCanonicalPath());
                } else if (file.isFile()) {
                    for (String type : fileTypes) {
                        if (file.getName().toLowerCase().endsWith(type)) {
                            res.add(file);
                            break;
                        }
                    }
                }
            }

        }
        return res;
    }

}
//    public static String deAccent2(String str, Normalizer.Form form) {
//        Collator cltr = Collator.getInstance();
//        CollationKey ck = cltr.getCollationKey(str);
//        ck.compareTo(ck)
//        String nfdNormalizedString = Normalizer.normalize(str, form);
//        return deAccentPattern.matcher(nfdNormalizedString).replaceAll("");
//    }

