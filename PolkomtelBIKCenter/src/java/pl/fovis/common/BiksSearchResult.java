/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ctran
 */
public class BiksSearchResult implements Serializable {

    public Map<Integer, Integer> treeCnt;
    public String query;
    public List<TreeNodeBean> items;
    public Integer searchId;
}
