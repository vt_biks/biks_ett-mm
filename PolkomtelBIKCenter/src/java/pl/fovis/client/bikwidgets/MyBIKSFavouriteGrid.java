/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import static pl.fovis.client.BIKClientSingletons.htmlEntitiesDecode;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.bikpages.ObjectInFvsBeanExtractor;
import pl.fovis.client.entitydetailswidgets.IGridBeanAction;
import pl.fovis.common.BIKCenterUtils;
import pl.fovis.common.FvsChangeExBean;
import pl.fovis.common.ObjectInFvsChangeExBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.BikCustomButton;
import pl.fovis.foxygwtcommons.PushButtonWithHideText;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author acichy
 * bf->pozbyc sie jeszcze extractora!
 */
public class MyBIKSFavouriteGrid extends MyBIKSGridDoubleRowBase<FvsChangeExBean> implements IPaginatedItemsDisplayer<FvsChangeExBean> {

    @Override
    protected String getIconUrl(FvsChangeExBean n) {
        return "images" /* I18N: no */ + "/" + BIKClientSingletons.getNodeKindIconNameByCode(n.code, n.tabId) + "." + "gif" /* I18N: no */;
    }

    public void createActionList(int row, final FvsChangeExBean n) {

        ObjectInFvsBeanExtractor extractor = new ObjectInFvsBeanExtractor() {
            @Override
            protected void execActView(final FvsChangeExBean b) {
                super.execActView(b);
                deleteChangedObjectInFvs(b.nodeId);
            }
        };
        extractor.setDataChangedHandler(new IParametrizedContinuation<Pair<Boolean, Boolean>>() {
            @Override
            public void doIt(Pair<Boolean, Boolean> param) {
                if (!param.v1) {
                    if (filteredItems != allItems) {
                        filteredItems.remove(n);
                        allItems.remove(n);
                    }
                }
                rebuildTab(param.v1, param.v2);
            }
        });
        List<IGridBeanAction<FvsChangeExBean>> l = extractor.createActionList();
        for (IGridBeanAction<FvsChangeExBean> act : l) {
            gp.setWidget(row, columnsCnt++, createActionBtn(act, act.getActionIconUrl(n), n));
        }
    }

    protected void deleteChangedObjectInFvs(int nodeId) {
        BIKClientSingletons.getService().deleteChangedObjectInFvs(nodeId, new StandardAsyncCallback<Void>() {
            @Override
            public void onSuccess(Void result) {
                rebuildTab(true, true);
            }
        });
    }

    Set<String> actionFvs = new HashSet<String>();

    @Override
    protected void addOrRefreshTitles(int row, FvsChangeExBean n) {
        String s = BaseUtils.encodeForHTMLTag(BIKClientSingletons.htmlEntitiesDecode(n.name), true, 1);
        String actions = BIKCenterUtils.getActionObjectInFvs(n);
        boolean isDartDownVisible = !BaseUtils.isStrEmptyOrWhiteSpace(actions);// isDartDownVisible(n);
        String str = isDartDownVisible ? "<b>" + s + " (" + actions + ")</b>" : s;

        String title = setAncestorPath(n.menuPath, n.namePath);
        if (!isDartDownVisible) {
            BikCustomButton bkb = BIKClientSingletons.createHtmlLabelLookupButton(str, n.tabId, n.nodeId, null, true, new IContinuation() {
                @Override
                public void doIt() {
                    rebuildTab(true, true);
                }
            });
            bkb.setStyleName("News" /* i18n: no:css-class */ + "-newsTitle");
            bkb.setTitle(title);
            titles.put(row, bkb);
        } else {
            HTML bkb = new HTML(str);
            bkb.setStyleName("News" /* i18n: no:css-class */ + "-newsTitle");
            bkb.setTitle(title);
            bkb.addClickHandler(createClickHandlerForRow(row, n));
            titles.put(row, bkb);
        }
    }

    protected String setAncestorPath(String menuPath, String namePath) {
        return BIKClientSingletons.htmlEntitiesDecode(menuPath) + BIKGWTConstants.RAQUO_STR_SPACED + 
                BIKClientSingletons.htmlEntitiesDecode(namePath);
    }

    @Override
    protected void createRowFromBean(int row, FvsChangeExBean n) {
        columnsCnt = 0;
        addWidgetsToRow(row, n);
        createActionList(row, n);
        gp.getRowFormatter().setStyleName(row, getBackgroundStyleForRowInGrid(row, 4));
    }

    @Override
    protected void setUpGridColumnsWidths(FlexTable gp) {
        gp.setWidth("100%");
        gp.getColumnFormatter().setWidth(0, "16px");
        gp.getColumnFormatter().setWidth(1, "16px");
        gp.getColumnFormatter().setWidth(3, "20px");
        gp.getColumnFormatter().setWidth(4, "26px");
        gp.getColumnFormatter().setWidth(5, "26px");
        gp.getColumnFormatter().setWidth(6, "26px");
    }

    @Override
    protected boolean isDartDownVisible(FvsChangeExBean n) {
        return getObjecChangeCnt(n) > 0;
    }

    @Override
    protected ClickHandler createClickHandlerForRow(final int row, final FvsChangeExBean n) {
        ClickHandler click = new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String s = buttons.get(row).getStyleName();
                buttons.get(row).setStyleName(s.equals(dartAtStyle()) ? dartDownStyle() : dartAtStyle());
                gp.getRowFormatter().setVisible(row * 2 + 1, !gp.getRowFormatter().isVisible(row * 2 + 1));
            }
        };
        return click;
    }

    @Override
    protected boolean isIgnoreImageVisible() {
        return true;
    }

    @Override
    protected PushButtonWithHideText addIgnoreImage(final FvsChangeExBean n) {
        PushButtonWithHideText btn = addPushButtonWithHideText(I18n.oznaczWszystkieElementyW.get() /* I18N:  */ + " " + n.name + " " + I18n.jakoPrzeczytane.get() /* I18N:  */);
        btn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                BIKClientSingletons.getService().deleteAllChangedObjectInFvsInFvs(n.nodeId, new StandardAsyncCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        rebuildTab(true, true);
                    }
                });
            }
        });
        return btn;
    }

    @Override
    protected Widget getWidgetToRowExpanded(final FvsChangeExBean n) {

        final FlexTable descendantGp = new FlexTable();
        BIKClientSingletons.getService().getChangedDescendantsObjectInFvsEx(n.nodeId, n.userId, new StandardAsyncCallback<List<ObjectInFvsChangeExBean>>() {
            public void onSuccess(List<ObjectInFvsChangeExBean> result) {
                int row = 0;

                for (final ObjectInFvsChangeExBean o : result) {
                    int col = 0;

                    String ic = "images" /* I18N: no */ + "/" + BIKClientSingletons.getNodeKindIconNameByCode(o.code, o.tabId) + "." + "gif" /* I18N: no */;

                    descendantGp.setWidget(row, col++, new Image(ic));
                    String nameWithActionsInFvs = BaseUtils.encodeForHTMLTag(o.name) + " <font color=red>(" + BIKCenterUtils.actionsDescendantsObjectInFvs(o) /* I18N:  */ + ")</font>";
                    BikCustomButton nameWithActionsInFvsBtn = BIKClientSingletons.createHtmlLabelLookupButton(nameWithActionsInFvs, o.tabId, o.nodeId, null, true, new IContinuation() {
                        public void doIt() {
                            deleteChangedObjectInFvs(o.nodeId);
//                            rebuildTab(true, true);

                        }
                    });
                    nameWithActionsInFvsBtn.setStyleName("News" /* i18n: no:css-class */ + "-newsTitle");
                    String title = setAncestorPath(o.menuPath, o.namePath);
                    nameWithActionsInFvsBtn.setTitle(title);
                    HTML nameWithActionsInFvsHtml = new HTML(nameWithActionsInFvs);
                    nameWithActionsInFvsHtml.setTitle(title);
                    descendantGp.setWidget(row, col++, o.nodeState != -1 ? nameWithActionsInFvsBtn : nameWithActionsInFvsHtml);
                    descendantGp.setWidget(row, col++, addIgnoreImageToDescendant(o.name, o.nodeId)/*i*/);
                    createDescendantActionList(row, o, col++, descendantGp);
                    row++;
                }
            }
        });
        if (getObjecChangeCnt(n) > 20) {
            descendantGp.setWidget(21, 0, new HTML("<i>" + I18n.pokazaneJest20Z.get() /* I18N:  */ + " " + getObjecChangeCnt(n) + " " + I18n.zmodyfikowanychElementow.get() /* I18N:  */ + " </i>"));
            descendantGp.getFlexCellFormatter().setColSpan(21, 0, 6);
        }
        descendantGp.setStyleName("gridDescendantJoinedObj");
        descendantGp.setWidth("100%");
        setDescendantGridColumnsWidths(descendantGp);
        return descendantGp;
    }

    protected PushButtonWithHideText addIgnoreImageToDescendant(String name, final int nodeId) {
        PushButtonWithHideText btn = addPushButtonWithHideText(I18n.oznaczElement.get() /* I18N:  */ + " " + name + " " + I18n.jakoPrzeczytany.get() /* I18N:  */);
        btn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                deleteChangedObjectInFvs(nodeId);
            }
        });
        return btn;
    }

    protected PushButtonWithHideText addPushButtonWithHideText(String title) {
        PushButtonWithHideText btn = new PushButtonWithHideText(new Image("images/ignore.gif" /* I18N: no */));
        btn.setTitle(title);
        btn.setStyleName("Ignore" /* I18N: no */);
        return btn;
    }

    public int getObjecChangeCnt(FvsChangeExBean n) {
        return n.cntDelete + n.cntNew + n.cntUpdate + n.cntJoinedObjChange + n.cntNewComments + n.cntVoteChange + n.cntDqcTestFails
                + n.isPasted + n.cutParentId;
    }

    protected void setDescendantGridColumnsWidths(FlexTable descendantGp) {
        descendantGp.setWidth("100%");
        descendantGp.getColumnFormatter().setWidth(0, "20px");
        descendantGp.getColumnFormatter().setWidth(2, "20px");
        descendantGp.getColumnFormatter().setWidth(3, "26px");
        descendantGp.getColumnFormatter().setWidth(4, "26px");
        descendantGp.getColumnFormatter().setWidth(5, "26px");
    }

    public void createDescendantActionList(int row, final ObjectInFvsChangeExBean n, int col, FlexTable descendantGp) {

        ObjectInFvsBeanExtractor extractor = new ObjectInFvsBeanExtractor() {
            @Override
            protected void execActView(final FvsChangeExBean b) {
                super.execActView(b);
                deleteChangedObjectInFvs(b.nodeId);
            }
        };
        List<IGridBeanAction<FvsChangeExBean>> l = extractor.createActionList2();
        for (final IGridBeanAction<FvsChangeExBean> act : l) {

            FvsChangeExBean fvs = new FvsChangeExBean();
            fvs.nodeId = n.nodeId;
            fvs.tabId = n.tabId;
            fvs.name = n.name;
            PushButton btn = createActionBtn(act, act.getActionIconUrl(fvs), fvs);
            descendantGp.setWidget(row, col++, btn);
            btn.setEnabled(n.nodeState != -1);
        }
        Label empty = new Label();
        empty.setWidth("32px");
        descendantGp.setWidget(row, col++, empty);
    }
}
