/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import pl.fovis.foxygwtcommons.BikCustomButton;
import simplelib.IContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
public class BikGridActionButton extends BikCustomButton {

    public BikGridActionButton(String text, final IContinuation click) {
        super(text, "RelatedObj-linkSmallBtn", click);
    }
}
