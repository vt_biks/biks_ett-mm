/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.sapbo.designersdk;

import pl.bssg.metadatapump.common.PumpConstants;

/**
 *
 * @author tflorczak
 */
public class Designer4Pump extends DesignerPump {

    @Override
    public String getSourceName() {
        return PumpConstants.SOURCE_NAME_SAPBO4;
    }

    @Override
    protected String getAdminStringPath() {
        return "sapbo4.path";
    }
}
