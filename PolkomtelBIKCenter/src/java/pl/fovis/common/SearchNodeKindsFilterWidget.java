/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.SearchFilterBase;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class SearchNodeKindsFilterWidget extends SearchFilterBase {

    public CheckBox allNodeKindCb;
    public Map<String, CheckBox> otherFilterCheckBoxes = new HashMap<String, CheckBox>();
    public Map<Integer, CheckBox> allNodeKindCheckBoxes = new HashMap<Integer, CheckBox>();
    public VerticalPanel nodeKindsFilterVp = new VerticalPanel();
    protected IContinuation ic;
    public Set<Integer> nodeKindIds;

    protected Pair<Set<Integer>, Boolean> nodeKindIdAndIsAnythingTreeSelected;

    public SearchNodeKindsFilterWidget(IContinuation ic, Set<Integer> nodeKindIds) {
        this.ic = ic;
        this.nodeKindIds = nodeKindIds;
    }

    @Override
    public VerticalPanel buildWidget() {
        nodeKindsFilterVp.add(new Label(I18n.obiekty.get()));
        ScrollPanel nodeKindsSP = new ScrollPanel();
        nodeKindsFilterVp.addStyleName("lblPadding Search");
        allNodeKindCb = new CheckBox(I18n.wszystkieObiekty.get());
        allNodeKindCb.setValue(true);
        allNodeKindCb.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                for (CheckBox cb : allNodeKindCheckBoxes.values()) {
                    if (cb.isVisible()) {
                        cb.setValue(allNodeKindCb.getValue());
                    }
                }
                ic.doIt();
            }
        });

        nodeKindsSP.add(addNodeKindsCbxsVp());
        nodeKindsSP.setHeight("170px");
        nodeKindsSP.setWidth("300px");
        nodeKindsFilterVp.add(filterPanel(filterTb = new TextBox()));
        nodeKindsFilterVp.add(getAllCheckboxFpWithStyle(allNodeKindCb));
        nodeKindsFilterVp.add(nodeKindsSP);

        return nodeKindsFilterVp;
    }

    protected VerticalPanel addNodeKindsCbxsVp() {
        VerticalPanel nodeKindsVp = new VerticalPanel();
        for (Integer nodeKindId : nodeKindIds) {
            NodeKindBean nk = BIKClientSingletons.getNodeKindById(nodeKindId);
            final CheckBox cb = new CheckBox("<" + "img style='vertical-align:text-top' border='0' src='images" /* I18N: no */ + "/"
                    + nk.iconName + "." + "gif' width='16' " + "height" /* I18N: no */ + "='16' />" //&nbsp;
                    + " <span>" + nk.nodeKindCaption + "</span>", true);
            cb.setTitle(nk.nodeKindCaption);
            cb.setValue(true);
            cb.setFormValue(nk.id + "");
            allNodeKindCheckBoxes.put(nk.id, cb);
            cb.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {

                    boolean all = true;
                    for (CheckBox x : allNodeKindCheckBoxes.values()) {
                        if (!x.getValue() && x.isVisible()) {
                            all = false;
                            break;
                        }
                    }
                    allNodeKindCb.setValue(all);
                    ic.doIt();
                }
            });
            nodeKindsVp.add(cb);

        }
        return nodeKindsVp;
    }

    public Set<Integer> getSelectedNodeKindIds() {
        Set<Integer> selectedNodeKindIds = new HashSet<Integer>();

        for (CheckBox cb : allNodeKindCheckBoxes.values()) {
            if (cb.getValue()) {
                selectedNodeKindIds.add(BaseUtils.tryParseInt(cb.getFormValue()));
            }
        }
        return selectedNodeKindIds;
    }

    public Pair<Set<String>, Boolean> getAttrNamesAndIsAnythingNodeKindSelected() {
        boolean isAnythingNodeKindSelected = false;
        Set<String> attrNames = new HashSet<String>();
        for (CheckBox cb : allNodeKindCheckBoxes.values()) {
            if (/*cb.getFormValue() != null*/cb.isVisible() && cb.getValue()) {
                isAnythingNodeKindSelected = true;
                List<AttributeBean> abs = BIKClientSingletons.getAllAttributesForKind(BaseUtils.tryParseInteger(cb.getFormValue()));
                if (!BaseUtils.isCollectionEmpty(abs)) {
                    for (AttributeBean ab : abs) {
                        attrNames.add(ab.atrName);
                    }
                }
            }
        }

        return new Pair<Set<String>, Boolean>(attrNames, isAnythingNodeKindSelected);
    }

    protected void filterAction() {
        if (nodeKindIdAndIsAnythingTreeSelected == null) {
            reselectedAndFilteredNodeKindsVp(new Pair<Set<Integer>, Boolean>(nodeKindIds, true));
        } else {
            reselectedAndFilteredNodeKindsVp(nodeKindIdAndIsAnythingTreeSelected);
        }
        ic.doIt();
    }

    public void selectOrUnselectAllFilter(boolean check) {
        allNodeKindCb.setValue(check);
        for (CheckBox cb : allNodeKindCheckBoxes.values()) {
            cb.setValue(check);
            cb.setVisible(check);
        }
        filterTb.setText("");
    }

    public void reselectedAndFilteredNodeKindsVp(Pair<Set<Integer>, Boolean> nodeKindIdAndIsAnythingTreeSelected) {
        this.nodeKindIdAndIsAnythingTreeSelected = nodeKindIdAndIsAnythingTreeSelected;
        String filtr = filterTb.getText();

        boolean isAnythingTreeSelected = nodeKindIdAndIsAnythingTreeSelected.v2;
        for (CheckBox cb : allNodeKindCheckBoxes.values()) {
            if (nodeKindIdAndIsAnythingTreeSelected.v1.contains(BaseUtils.tryParseInteger(cb.getFormValue()))) {
//                cb.setVisible(true);
                if (!cb.isVisible()) {
                    cb.setValue(true);
                    cb.setVisible(true);
                }
                cb.setVisible(true);
            } else {
                cb.setVisible(false);
                cb.setValue(false);
            }
        }
        boolean isAllNodeKindIdsSelected = true;
        for (Integer nodeKindId : nodeKindIdAndIsAnythingTreeSelected.v1) {

            NodeKindBean nk = BIKClientSingletons.getNodeKindById(nodeKindId);
            CheckBox cb = allNodeKindCheckBoxes.get(nk.id);
            if (BaseUtils.isStrEmptyOrWhiteSpace(filtr) || nk.nodeKindCaption.toLowerCase().contains(filtr.toLowerCase())) {
                if (!cb.isVisible()) {//zaznaczenie
                    cb.setValue(true);
                    cb.setVisible(true);
                }
                cb.setVisible(true);
                if (!cb.getValue()) {
                    isAllNodeKindIdsSelected = false;
                }
            } else {
                cb.setVisible(false);
                cb.setValue(false);
            }

        }
//poszukać!
        allNodeKindCb.setVisible(isAnythingTreeSelected);
        allNodeKindCb.setValue(isAnythingTreeSelected && isAllNodeKindIdsSelected);
    }

}
