/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author mgraczkowski
 */
public class DataSourceStatusBean implements Serializable {

    public Integer id;
    public String system;
    public String server;
    public Date startTime;
    public Date finishTime;
    public String status;
}
