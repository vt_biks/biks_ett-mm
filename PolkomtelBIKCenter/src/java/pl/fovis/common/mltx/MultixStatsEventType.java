/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.mltx;

import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author lbiegniewski
 */
public enum MultixStatsEventType {

    USER_LOGIN("userLogin", I18n.logowaloSie.get()),
    LAST_ACTIVITY("lastActivity", I18n.ostatniaAktywnosc.get()),
    TOTAL_INVITED("totalInvited", I18n.zaproszonych.get());
    public final String name;
    public final String displayName;

    MultixStatsEventType(String name, String displayName) {
        this.name = name;
        this.displayName = displayName;
    }

    public static String getDisplayNameByName(String name) {
        for (MultixStatsEventType eventType : MultixStatsEventType.values()) {
            if (name.equals(eventType.name)) {
                return eventType.displayName;
            }
        }
        return null;
    }
}
