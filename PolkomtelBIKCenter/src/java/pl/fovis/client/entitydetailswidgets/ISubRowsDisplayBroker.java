/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.Widget;
import simplelib.IParametrizedContinuationWithReturn;

/**
 *
 * @author pmielanczuk
 */
public interface ISubRowsDisplayBroker {

    public int getAttrCount();

    public int getAttrWidth(int attrIdx);

    public String getAttrCaption(int attrIdx);

    //---
    public int getRowCount();

    public Widget[] makeWidgetsForRow(int rowIdx);

    public IParametrizedContinuationWithReturn<Integer, String> getRowStyleCalc();
}
