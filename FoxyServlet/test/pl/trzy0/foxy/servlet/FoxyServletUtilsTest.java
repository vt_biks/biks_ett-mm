/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.trzy0.foxy.servlet;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wezyr
 */
public class FoxyServletUtilsTest {

    public FoxyServletUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     * Test of getMultiPartParamsAsMap method, of class FoxyServletUtils.
     */
    @Test
    public void testGetWebAppRealNameByRealPath() {
        assertEquals("NaqNaqApp", 
                FoxyServletUtils.getWebAppRealNameByRealPath("/usr/local/tomcat/./webapps/NaqNaqApp/"));
        assertEquals("web",
                FoxyServletUtils.getWebAppRealNameByRealPath("C:\\3zero\\QnaApp\\build\\web\\"));
    }
}