use master;
go

-- restore filelistonly from DISK = 'C:\projekty\mssql-backups\PB-BIKS-TEST upgraded to v1.7.z5.3.bak';
-- restore filelistonly from DISK = 'C:\projekty\mssql-backups\mssql-new-bak\biks_polkomtel_2012_10_22.bak';


if exists (select 1 from sys.databases where name = 'aa_biks_fix_ids')
  drop database aa_biks_fix_ids;
go


--restore DATABASE aa_biks_fix_ids from DISK = 'C:\projekty\mssql-backups\PB-BIKS-TEST upgraded to v1.7.z5.3.bak'
restore DATABASE aa_biks_fix_ids from DISK = 'C:\projekty\mssql-backups\mssql-new-bak\biks_polkomtel_2012_10_22.bak'
with 
  --move 'PB-BIKS-TEST' to 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\aa_biks_fix_ids.mdf',
  --move 'PB-BIKS-TEST_log' to 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\aa_biks_fix_ids.ldf';
  move 'boxi' to 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\aa_biks_fix_ids.mdf',
  move 'boxi_log' to 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\aa_biks_fix_ids.ldf';
go



-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

/*

-- restore filelistonly from DISK = 'C:\projekty\mssql-backups\Polkomtel i Cyfrowy 2015\BIKS_Polkomtel_2015_shrinked.bak';

restore DATABASE BIKS_Polkomtel_2015 from DISK = 'C:\projekty\mssql-backups\Polkomtel i Cyfrowy 2015\BIKS_Polkomtel_2015_shrinked.bak'
with 
  move 'boxi' to 'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS2014\MSSQL\DATA\BIKS_Polkomtel_2015.mdf',
  move 'boxi_log' to 'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS2014\MSSQL\DATA\BIKS_Polkomtel_2015.ldf';
go


C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS2014\MSSQL\DATA\BIKnowledgeSystem.mdf


select * from BIKS_Cyfrowy_2015.sys.database_files

*/
