/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.mail;

import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.LogsPageBase;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.MailLogBean;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class MailLogPage extends LogsPageBase<MailLogBean> {

    @Override
    protected void callServiceToGetData(StandardAsyncCallback<Pair<Integer, List<MailLogBean>>> callback) {
        BIKClientSingletons.getService().getMailLogs(pwh.pageNum, pwh.pageSize, callback);
    }

    @Override
    protected NodeAwareBeanExtractorBase<MailLogBean> getDataExtractor() {
        return new MailBeanExtractor();
    }

    @Override
    public String getId() {
        return "admin:email:logs";
    }
    
}
