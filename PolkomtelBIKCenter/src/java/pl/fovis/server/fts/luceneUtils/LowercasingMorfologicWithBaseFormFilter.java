/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts.luceneUtils;

import commonlib.LameUtils;
import java.io.IOException;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.morfologik.MorfologikFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import simplelib.logging.ILameLogger;

/**
 *
 * @author pmielanczuk
 */
public class LowercasingMorfologicWithBaseFormFilter extends TokenFilter {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
    protected OffsetAttribute offsAttr = addAttribute(OffsetAttribute.class);
    protected PositionIncrementAttribute posIncrAtt = addAttribute(PositionIncrementAttribute.class);
//    protected CharTermAttribute srcTermAtt;
    protected int prevStartPos = -1;
    protected char[] baseForm = null;
    protected int baseFormLen = -1;
    protected State savedState;
    protected State prevState;
    protected boolean hasNext;
    protected StandardFilterWithTerm src;

    public LowercasingMorfologicWithBaseFormFilter(MorfologikFilter mFilter, StandardFilterWithTerm src) {
        super(mFilter);
        this.src = src;
    }

    protected boolean maybeRestoreSavedState() {
        if (savedState != null) {
            restoreState(savedState);
            savedState = null;
            return true;
        } else {
            return false;
        }
    }

    @Override
    final public boolean incrementToken() throws IOException {

        if (!maybeRestoreSavedState()) {
            // nie było zapamiętanego stanu, więc:
            //   hasNext = bierzemy następny token ze strumienia Morfologika
            hasNext = input.incrementToken();

            // od razu go na małe litery zamieniamy
            if (hasNext) {
                lowerCaseTerm();
            }

            // 1. jeżeli hasNext i to jest inna forma poprzedniego tokenu, czyli bez
            //    zmiany pozycji, to tylko sprawdzamy, czy aktualny token to nie
            //    jest przypadkiem zapamiętana forma bazowa, jeżeli mamy zapamiętaną
            //    i ten token to jest forma bazowa, to czyścimy formę bazową, tzn.
            //    Morfologik sam ją wypchnął, mniej roboty dla nas potem
            if (hasNext && prevStartPos == offsAttr.startOffset()) {
                if (hasBaseForm()) {
                    if (baseFormEqualsCurrentTerm()) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("clear baseForm=" + baseFormToString());
                        }
                        clearBaseForm();
                    }
                }
                return true;
            }

            // 2. mamy: !hasNext lub jest to inny token (nie forma poprzeniego),
            //      czyli nie jest to ta sama pozycja którą zapamiętaliśmy
            //      poprzednim razem, więc:
            //    jeżeli forma bazowa poprzedniego słowa nie została
            //    wypchnięta przez Morfologika, to my ją teraz wypchniemy, ale
            //    najpierw zapamiętamy sobie aktualny stan (savedState, hasNext)
            if (hasBaseForm()) {
                if (logger.isDebugEnabled()) {
                    logger.debug("push out baseForm=" + baseFormToString());
                }
                savedState = captureState();
                restoreState(prevState);
                posIncrAtt.setPositionIncrement(0);
                termAtt.copyBuffer(baseForm, 0, baseFormLen);
                clearBaseForm();
                return true;
            }
        }

        // tu możemy dojść, gdy (zachodzi przynajmniej jedno z poniższych):
        // a) mieliśmy savedState (wtedy też mieliśmy hasNext zapamiętane)
        //    i ten stan został już odtworzony
        // b) hasNext == false
        // c) pozycje się nie zgadzały (nowy token), ale nie było baseForm do
        //    wypchnięcia
        if (hasNext) {
            // w strumieniu jeszcze coś jest
            prevState = captureState();
            prevStartPos = offsAttr.startOffset();
            saveBaseForm();
            if (logger.isDebugEnabled()) {
                logger.debug("baseForm=" + baseFormToString());
            }
            if (baseFormEqualsCurrentTerm()) {
                if (logger.isDebugEnabled()) {
                    logger.debug("immediate clear baseForm=" + baseFormToString());
                }
                clearBaseForm();
            }
        }

        return hasNext;
    }

    protected boolean baseFormEqualsCurrentTerm() {
        int termLen = termAtt.length();
        if (termLen != baseFormLen) {
            return false;
        }

        char[] buffer = termAtt.buffer();

        for (int i = 0; i < termLen; i++) {
            if (buffer[i] != baseForm[i]) {
                return false;
            }
        }

        return true;
    }

    protected String baseFormToString() {
        if (logger.isDebugEnabled()) {
            logger.debug("baseFormToString: baseFormLen=" + baseFormLen);
        }
        return new String(baseForm, 0, baseFormLen);
    }

    protected void lowerCaseTerm() {
        char[] chars = termAtt.buffer();
        StandardFilterWithTerm.toLowerCase(chars, termAtt.length(), chars);
    }

    @Override
    public void reset() throws IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("reset()");
        }
        hasNext = false;
        savedState = null;
        clearBaseForm();
        prevState = null;
        prevStartPos = -1;
        super.reset();
    }

    protected boolean hasBaseForm() {
        if (logger.isDebugEnabled()) {
            logger.debug("hasBaseForm: baseFormLen=" + baseFormLen);
        }
        return baseFormLen >= 0;
    }

    protected void clearBaseForm() {
        if (logger.isDebugEnabled()) {
            logger.debug("clearBaseForm()");
        }
        baseFormLen = -1;
    }

    protected void saveBaseForm() {
        baseFormLen = src.tokenLength;
        baseForm = StandardFilterWithTerm.adjustToNewLength(baseForm, baseFormLen);
        System.arraycopy(src.tokenChars, 0, baseForm, 0, baseFormLen);
        if (logger.isDebugEnabled()) {
            logger.debug("saveBaseForm: baseForm=" + baseFormToString());
        }
    }
}
