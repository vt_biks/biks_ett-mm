/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.AbstractBIKTreeWidget;
import pl.fovis.client.bikpages.BikTreeWidget;
import pl.fovis.client.bikwidgets.AttrAnsiText;
import pl.fovis.client.bikwidgets.AttrCheckbox;
import pl.fovis.client.bikwidgets.AttrCombobox;
import pl.fovis.client.bikwidgets.AttrDatePicker;
import pl.fovis.client.bikwidgets.AttrDateTime;
import pl.fovis.client.bikwidgets.AttrLongText;
import pl.fovis.client.bikwidgets.AttrSelectOneJoinedObject;
import pl.fovis.client.bikwidgets.AttrShortText;
import pl.fovis.client.bikwidgets.AttrWigetBase;
import pl.fovis.client.dialogs.AddOrEditAdminAttributeWithTypeDialog.AttributeType;
import pl.fovis.client.treeandlist.TreeNodeBeanAsMapStorage;
import pl.fovis.common.AttributeBean;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.JoinedObjBasicGenericInfoBean;
import pl.fovis.common.JoinedObjBasicInfoBean;
import pl.fovis.common.JoinedObjMode;
import pl.fovis.common.NodeKindBean;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.NewLookUtils;
import simplelib.BaseUtils;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author beata
 */
public class AddOrEditAttributeDialog extends TreeSelectorForLinkedNodesDialog {

//    protected Label status;
    protected TextBox tbTytul;
    protected TextBox tbDescription;
    protected AttributeBean attr;
    private IParametrizedContinuation<AttributeBean> saveCont;
    protected ListBox listBox;
    protected Map<String, AttributeBean>/*Set<String>*/ attrNamesAndType;
    protected Label lblTitle;
    protected Label lblContent;
    protected HTML errorMssg;
    //
    protected Map<String, String> attrNameToValue;
    protected String attrDescription;
    protected Grid gridIcons;
    protected String caption;
    protected boolean isNodeFromDynamicTree;
    protected NodeKindBean nodeKind;
    protected Grid gridAttrs;
    protected boolean readOnly = true;
    protected String branchIds;
//    protected PushButton refreshBtn;
    protected Map<String, List<TreeNodeBean>> /*Map<String, List<Integer>>*/ allItems;
//    Map<String, String> originalValue = new HashMap<String, String>();
    protected AttrWigetBase attributeWidget;

    protected Map<String, String> newToOriginalValue = new HashMap<String, String>();

    public AddOrEditAttributeDialog() {
    }

    public AddOrEditAttributeDialog(Integer nodeKindId, boolean isNodeFromDynamicTree) {
        this.nodeKindId = nodeKindId;
        nodeKind = BIKClientSingletons.getNodeKindById(nodeKindId);
        this.isNodeFromDynamicTree = isNodeFromDynamicTree;
    }

    public AddOrEditAttributeDialog(Integer nodeId, Integer nodeKindId, boolean isNodeFromDynamicTree) {
        this.nodeKindId = nodeKindId;
        this.nodeId = nodeId;
        nodeKind = BIKClientSingletons.getNodeKindById(nodeKindId);
        this.isNodeFromDynamicTree = isNodeFromDynamicTree;
    }

    @Override
    protected String getDialogCaption() {
//        return caption;
        return attr == null ? caption /*I18n.dodajAtrybut.get()*/ : I18n.edytujAtrybut.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }

    protected List<String> sortedList(List<String> attr) {

        Collections.sort(attr, new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                return String.CASE_INSENSITIVE_ORDER.compare(o1, o2);
            }
        });

        return attr;
    }

    @Override
    protected void buildMainWidgets() {
//        status = new Label();

//        main.add(status);
        main.add(errorMssg = new HTML());
        errorMssg.setStyleName("status" /* i18n: no:css-class */);
        gridAttrs = new Grid(2, 2);
        gridAttrs.setSize("100%", "100%");
        gridAttrs.setCellSpacing(4);

        gridAttrs.setWidget(0, 0, lblTitle = new Label(I18n.tytul.get() /* I18N:  */ + ":"));
        if (!caption.equals(I18n.usunAtrybut.get())) {
            gridAttrs.setWidget(1, 0, lblContent = new Label(I18n.wartosc.get() /* I18N:  */ + ":"));
            lblContent.setStyleName("addAttrLbl");
        }
        gridAttrs.getCellFormatter().setStyleName(1, 0, "textTop");
        gridAttrs.setWidget(0, 1, listBox = new ListBox());
        List<String> attrlist = new ArrayList<String>();
        Map<Integer, String> sortMap = new TreeMap<Integer, String>();
        for (String value : attrNamesAndType.keySet()) {
            attrlist.add(value);
        }
//        for (Entry<String, AttributeBean> a : attrNamesAndType.entrySet()) {
//            AttributeBean ab = a.getValue();
//
//            sortMap.put(ab.visualOrder, ab.atrName);
//        }
//
//        for (Entry<Integer, String> value : sortMap.entrySet()) {
//            attrlist.add(value.getValue());
//            Window.alert(value.getValue() + "");
//        }
        attrlist = sortedList(attrlist);
        for (String value : sortedList(attrlist)) {
//        for (String value : attrlist/*sortedList(attrlist)*/) {
            listBox.addItem(trimDot(value));
//            Window.alert(trimDot(value) + "");
        }
        if (attr != null) {
            listBox.setEnabled(false);
            listBox.setSelectedIndex(attrlist.indexOf(attr.atrName));
        }
        tbDescription = new TextBox();
        tbDescription.setStyleName("addAttr");
        tbDescription.setWidth(/*"800px"*/"100%");
        if (!caption.equals(I18n.usunAtrybut.get())) {
            setTypeAttributeWidget(gridAttrs);
        }
        lblTitle.setStyleName("addAttrLbl");

        listBox.setStyleName("addAttr");
        listBox.setWidth("210px");

        listBox.addChangeHandler(new ChangeHandler() {

            public void onChange(ChangeEvent event) {
                if (!caption.equals(I18n.usunAtrybut.get())) {
                    errorMssg.setText("");
                    attrDescription = "";

                    setTypeAttributeWidget(gridAttrs);
                }
                setVisibleAdditionalBtn();
                recenterDialog();

            }
        });
        //refresh button (for Piotr Nowak)
//        refreshBtn = new PushButton("Odśwież");
//        NewLookUtils.makeCustomPushButton(refreshBtn);
//
//        refreshBtn.addClickHandler(new ClickHandler() {
//            @Override
//            public void onClick(ClickEvent event) {
//                BIKClientSingletons.getTabSelector().invalidateTabData("58");
//            }
//        });
//        main.add(refreshBtn);
//        main.setCellWidth(refreshBtn, "20%");
//        main.setCellHorizontalAlignment(refreshBtn, HasHorizontalAlignment.ALIGN_RIGHT);
//
        ScrollPanel scPanel = new ScrollPanel(gridAttrs);
        // TF: zamiast poniższego robiony jest recenter dialog po każdej zmianie na listboxie
//        scPanel.setSize(Window.getClientWidth() - 60 + "px", Window.getClientHeight() - 110 + "px");
        if (readOnly) {
            actionBtn.setVisible(false);
            listBox.setEnabled(false);
        }
        main.add(scPanel);
    }

    protected String getOriginalSelectedText() {
        int selectedIdx = listBox.getSelectedIndex() >= 0 ? listBox.getSelectedIndex() : 0;
        String selectedText = listBox.getItemText(selectedIdx);
        for (Entry<String, String> s : newToOriginalValue.entrySet()) {
            if (s.getKey().contains(selectedText)) {
                return s.getValue();
            }
        }
        return null;
    }

    protected void setTypeAttributeWidget(Grid grid) {
        actionBtn.setEnabled(true);
//        int selectedIdx = listBox.getSelectedIndex() >= 0 ? listBox.getSelectedIndex() : 0;
        String selectedText = getOriginalSelectedText();// trimDot(listBox.getItemText(selectedIdx));
        AttributeBean ab = attrNamesAndType.get(selectedText);
        String attrType = ab.typeAttr;
        List<String> valueOptList = BaseUtils.splitBySep(ab.valueOpt, ",");

        //do edycji atrybutów
        boolean isEditAttr = attrNameToValue.containsKey(selectedText);
        String atrLinValue = isEditAttr ? attrNameToValue.get(selectedText) : BaseUtils.safeToStringNotNull(ab.defaultValue);

        if (readOnly) {
            if (AttributeType.hyperlinkInMulti.name().equals(attrType)
                    || AttributeType.hyperlinkInMono.name().equals(attrType)
                    || AttributeType.permissions.name().equals(attrType)) {
                grid.setWidget(1, 1, new Label(""));
            } else {
                grid.setWidget(1, 1, new Label(atrLinValue));
            }
            BIKClientSingletons.showWarning(I18n.cantEditAttr.get());
        } else if (AttributeType.shortText.name().equals(attrType)
                || AttributeType.hyperlinkOut.name().equals(attrType)) {
            attributeWidget = new AttrShortText(atrLinValue);
            grid.setWidget(1, 1, attributeWidget.getWidget());
            tbDescription.setText(atrLinValue);
        } else if (AttributeType.calculation.name().equals(attrType)) {
            // nie ustawiamy wartości, wartość się sama policzy
            grid.setWidget(1, 1, new Label(I18n.atrybutWiliczanyAutomatycznie.get()));
        } else if (AttributeType.javascript.name().equals(attrType)) {
            // nie ustawiamy wartości, wartość się sama policzy
            grid.setWidget(1, 1, new Label(I18n.javascript.get()));
        } else if (AttributeType.longText.name().equals(attrType) || AttributeType.sql.name().equals(attrType)) {
            attributeWidget = new AttrLongText(atrLinValue);
            grid.setWidget(1, 1, attributeWidget.getWidget());
//            grid.setWidget(1, 1, vp);
        } else if (AttributeType.hyperlinkInMulti.name().equals(attrType)
                || AttributeType.hyperlinkInMono.name().equals(attrType)
                || AttributeType.permissions.name().equals(attrType)) {
            VerticalPanel vp = new VerticalPanel();
//            if (AttributeType.hyperlinkInMulti.name().equals(attrType)) {
            directOverride.clear();
            parentNodeIdsDirectOverride.clear();
            if (allItems != null && !allItems.isEmpty()) {
                List<TreeNodeBean> tnbs = allItems.get(selectedText);
                if (!BaseUtils.isCollectionEmpty(tnbs)) {
                    for (TreeNodeBean tb : tnbs) {
                        directOverride.put(tb.id, JoinedObjMode.Single);
                        List<String> parentNodeIdsFromBranch = BaseUtils.splitBySep(tb.branchIds, "|");
                        if (BIKClientSingletons.expandSelectedNodesInHyperlinkAttribute()) {
                            if (tb.parentNodeId == null) {
                                treeIdWhenParentIdIsNull.add(-tb.treeId);
                            } else {
                                parentsDirectOverrideOrIdWhenParentNull.add(tb.parentNodeId);
                            }
                        }
                        for (String pNodeId : parentNodeIdsFromBranch) {
                            parentNodeIdsDirectOverride.add(BaseUtils.tryParseInt(pNodeId));
                        }
                        //tb.branchIds
                    }
                }
            }
//            }
            if (treeId == null) {
                treeId = 0;
            }
            buildTreeMainWidgets(vp);
            if (AttributeType.hyperlinkInMono.name().equals(attrType)) {
                bikTree.addSelectionChangedHandler(new IParametrizedContinuation<Map<String, Object>>() {
                    public void doIt(Map<String, Object> param) {
                        tbDescription.setText(bikTree.getCurrentNodeId() + "_" + bikTree.getCurrentNodeBean().treeCode + "," + bikTree.getCurrentNodeBean().name);
                    }
                });
            }
            vp.setStyleName("richTextArea");
            vp.setWidth("100%");
            grid.setWidget(1, 1, vp);
            fetchData();
        } else if (AttributeType.data.name().equals(attrType)) {
            attributeWidget = new AttrDatePicker(atrLinValue, ab.valueOpt, isEditAttr);
            grid.setWidget(1, 1, attributeWidget.getWidget());
        } else if (AttributeType.combobox.name().equals(attrType) || AttributeType.comboBooleanBox.name().equals(attrType)) {
            attributeWidget = new AttrCombobox(atrLinValue, valueOptList, ab.atrName, isEditAttr, ab.defaultValue);
            grid.setWidget(1, 1, attributeWidget.getWidget());
        } else if (AttributeType.selectOneJoinedObject.name().equals(attrType)) {
            attributeWidget = new AttrSelectOneJoinedObject(atrLinValue, ab.valueOpt, isEditAttr);
            grid.setWidget(1, 1, attributeWidget.getWidget());
        } else if (AttributeType.checkbox.name().equals(attrType)) {
            attributeWidget = new AttrCheckbox(atrLinValue, valueOptList, isEditAttr);
            grid.setWidget(1, 1, attributeWidget.getWidget());
        } else if (AttributeType.ansiiText.name().equals(attrType)) {
            attributeWidget = new AttrAnsiText(atrLinValue);
            grid.setWidget(1, 1, attributeWidget.getWidget());
        } else if (AttributeType.datetime.name().equals(attrType)) {
            attributeWidget = new AttrDateTime(atrLinValue, ab.valueOpt, isEditAttr);
            grid.setWidget(1, 1, attributeWidget.getWidget());

        }
    }

//trims a string before a dot and then trims trailing and leading whitespaces
    protected String trimDot(String key) {
        String value = key.substring(key.lastIndexOf(".") + 1);
        newToOriginalValue.put(value, key);
        if (key.contains(".")) {
// TODO: test trimmed value
//            String value = (key.substring(key.lastIndexOf(".") + 1)).trim();
//            originalValue.put(key, value);
            return value;
        }
//        else if (originalValue.containsValue(key)) {
//            for (String str : originalValue.keySet()) {
//                if (originalValue.get(str).equals(key)) {
//                    return str;
//                }
//            }
//        }
        return key;
    }

    @Override
    protected void doAction() {
        if (attr == null) {
            attr = new AttributeBean();
        }
        attr.atrName = getOriginalSelectedText();
//                trimDot(listBox.getValue(listBox.getSelectedIndex()));
        attr.atrLinValue = attrDescription;
        attr.isAddNodeInToTheSameTree = isAddNodeInToTheSameTree;
        saveCont.doIt(attr);
    }

    protected void onEnterPressed() {
        AttributeBean ab = attrNamesAndType.get(getOriginalSelectedText()
        );
        String attrType = ab.typeAttr;
        if (!AttributeType.ansiiText.name().equals(attrType)) {

            super.onEnterPressed();
        }

    }

    @Override
    protected boolean doActionBeforeClose() {

        if (caption.equals(I18n.usunAtrybut.get())) {
            return true;
        }

        //attrDescription = tbDescription.getText();
//        int selectedIdx = listBox.getSelectedIndex() >= 0 ? listBox.getSelectedIndex() : 0;
        AttributeBean ab = attrNamesAndType.get(getOriginalSelectedText()
        //                (listBox.getItemText(selectedIdx))
        );
        String attrType = ab.typeAttr;
        if (AttributeType.javascript.name().equals(attrType)) {
            return true;
        } else if (AttributeType.shortText.name().equals(attrType)
                || AttributeType.hyperlinkOut.name().equals(attrType)
                || AttributeType.longText.name().equals(attrType)
                || AttributeType.data.name().equals(attrType)
                || AttributeType.combobox.name().equals(attrType) || AttributeType.comboBooleanBox.name().equals(attrType)
                || AttributeType.selectOneJoinedObject.name().equals(attrType)
                || AttributeType.checkbox.name().equals(attrType)
                || AttributeType.ansiiText.name().equals(attrType)
                || AttributeType.sql.name().equals(attrType)
                || AttributeType.datetime.name().equals(attrType)) {

            attrDescription = attributeWidget.getValue();
        } else if (AttributeType.hyperlinkInMono.name().equals(attrType)) {
            attrDescription = tbDescription.getText();
        } else if (AttributeType.hyperlinkInMulti.name().equals(attrType)
                || AttributeType.permissions.name().equals(attrType) /* || AttributeType.hyperlinkInMono.name().equals(attrType)*//*&& isNodeFromDynamicTree*/) {

            String txt = "";
            for (Entry<Integer, JoinedObjMode> e : directOverride.entrySet()) {
                TreeNodeBean node = bikTree.getTreeStorage().getBeanById(e.getKey());
                //  if (node != null) {
                if (node != null) {
                    if (e.getValue().equals(JoinedObjMode.Single)) {
                        txt = txt + node.id + "_" + node.treeCode + "," + node.name.replace("|", "I");
                        if (!BIKConstants.METABIKS_ATTR_ORIGINAL.equals(ab.atrName)) {
                            txt = txt + "|";
                        }
                    }
                } else if (node == null) {
                    for (TreeNodeBean tb : allItems.get(ab.atrName)) {
                        if (tb != null && tb.id == e.getKey()) {
                            txt = txt + tb.id + "_" + tb.treeCode + "," + tb.name.replace("|", "I");
                            if (!BIKConstants.METABIKS_ATTR_ORIGINAL.equals(ab.atrName)) {
                                txt = txt + "|";
                            }
                        }
                    }
                }
            }
            attrDescription = txt;

        }
        if (AttributeType.hyperlinkOut.name().equals(attrType) && (attrDescription.equals("http" /* I18N: no */ + "://") || attrDescription.equals("https" /* I18N: no */ + "://"))) {
            errorMssg.setHTML(I18n.niepoprawnyAdresStrony.get() /* I18N:  */);
            return false;
        }

        if (ab.displayAsNumber) {
            attrDescription = clearString(attrDescription);
            Double res = BaseUtils.tryParseDouble(attrDescription);
            if (res == null) {
                errorMssg.setHTML(I18n.wprowadzonaWartoscNieJestLiczba.get() /* I18N:  */);
                return false;
            }
        }

        if (!BaseUtils.isHTMLTextEmpty(attrDescription) || attrType.equals(AttributeType.calculation.name())) {
            return true;
        }
        if (ab.atrName.startsWith(BIKConstants.METABIKS_META_ATTR_PREFIX)) {
            return true;
        }

        if (!ab.isEmpty) {
            errorMssg.setHTML(I18n.trescNieMozeBycPusta.get() /* I18N:  */ + ".");
        }
        return ab.isEmpty;
    }

    public void buildAndShowDialog(Map<String, AttributeBean> result, Map<String, String> attrNameToValue, AttributeBean attrb, Integer treeId,
            String caption, IParametrizedContinuation<AttributeBean> saveCont) {
        super.treeId = treeId;
        attr = attrb;
        this.saveCont = saveCont;
        this.attrNamesAndType = result;
        this.attrNameToValue = attrNameToValue;
        this.caption = caption;
        boolean treeUserRights = treeId != null && (BIKClientSingletons.isLoggedUserAuthorOfTree(treeId)
                || BIKClientSingletons.isLoggedUserCreatorOfTree(treeId));
        boolean branchUserRights = !BaseUtils.isStrEmptyOrWhiteSpace(branchIds) && (BIKClientSingletons.isBranchAuthorLoggedIn(branchIds)
                || BIKClientSingletons.isBranchCreatorLoggedIn(branchIds));

        if ((branchUserRights || treeUserRights || BIKClientSingletons.isEffectiveExpertLoggedIn())) {
            readOnly = false;
        }
        super.buildAndShowDialog();
    }

    public void buildAndShowDialog(Map<String, AttributeBean> result, Map<String, String> attrNameToValue, AttributeBean attrb, Integer treeId,
            String caption, TreeNodeBean node, IParametrizedContinuation<AttributeBean> saveCont) {
        super.treeId = treeId;
        attr = attrb;
        if (node != null) {
            this.branchIds = node.branchIds;
        }
        this.saveCont = saveCont;
        this.attrNamesAndType = result;
        this.attrNameToValue = attrNameToValue;
        this.caption = caption;
        //dla atrybutów zdarzenia mamy nodeKind.isRegistry = false, natomiast dla nagłówka nodeKind.isRegistry = true
        boolean treeUserRights = treeId != null && (BIKClientSingletons.isLoggedUserAuthorOfTree(treeId)
                || BIKClientSingletons.isLoggedUserCreatorOfTree(treeId));
        boolean branchUserRights = !BaseUtils.isStrEmptyOrWhiteSpace(branchIds) && (BIKClientSingletons.isBranchAuthorLoggedIn(branchIds)
                || BIKClientSingletons.isBranchCreatorLoggedIn(branchIds));
        boolean regHeaderAttrEditability = node != null && nodeKind != null && nodeKind.isRegistry && node.isRegistry;
        boolean regEventAttrEditability = node != null && nodeKind != null && !nodeKind.isRegistry && node.isRegistry;
        if ((((regHeaderAttrEditability) && (treeUserRights || branchUserRights))
                || (regEventAttrEditability && BIKClientSingletons.isSysAdminLoggedIn()))
                || ((branchUserRights && node != null && !node.isRegistry)
                || (treeUserRights && node != null && !node.isRegistry)
                || (BIKClientSingletons.isEffectiveExpertLoggedIn() && nodeKind != null && nodeKind.isRegistry)
                || (BIKClientSingletons.isEffectiveExpertLoggedIn() && node != null && !node.isRegistry)
                || ((treeUserRights || branchUserRights) && node == null)
                || BIKClientSingletons.isSysAdminLoggedIn())) {
            readOnly = false;
        }
        super.buildAndShowDialog();
    }

    public void buildAndShowDialog(Map<String, AttributeBean> result, Map<String, String> attrNameToValue, AttributeBean attrb, Integer treeId,
            String caption, Map<String, List<TreeNodeBean>> /*Map<String, List<Integer>>*/ nodes, String branchIds, TreeNodeBean node,
            IParametrizedContinuation<AttributeBean> saveCont) {
        this.branchIds = branchIds;
        this.allItems = nodes;
        buildAndShowDialog(result, attrNameToValue, attrb, treeId, caption, node, saveCont);
    }

    private String clearString(String textValue) {
        textValue = textValue.replaceAll(",", ".");
        return textValue.replaceAll(" ", "");
    }

//
//    protected String getCode(TreeNodeBean n) {
//        return n.nodeKindCode;
//    }
    @Override
    protected Integer getNodeKindId() {
        return isNodeFromDynamicTree || BIKConstants.TREE_KIND_META_BIKS.equalsIgnoreCase(BIKClientSingletons.getTreeBeanById(treeId).treeKind) ? nodeKindId : null;
    }

    @Override
    protected String getRelationType() {
        return BIKConstants.HYPERLINK;
    }

    @Override
    protected String getAttributeName() {
//        int selectedIdx = listBox.getSelectedIndex() >= 0 ? listBox.getSelectedIndex() : 0;
//        return trimDot(listBox.getItemText(selectedIdx));
        return getOriginalSelectedText();
    }

    @Override
    protected boolean isInMultiSelectMode() {
        if (treeId == null) {
            return true;
        }
        return AttributeType.hyperlinkInMulti.name().equals(attrNamesAndType.get(getAttributeName()).typeAttr)
                || AttributeType.permissions.name().equals(attrNamesAndType.get(getAttributeName()).typeAttr);
    }

    @Override
    protected boolean isGrouppingByMenuEnabled() {
        return false;
    }

    protected boolean isStandardJoinedObjMode() {
        return false;
    }

    @Override
    protected boolean isNodeKindIdInLinkedKind(TreeNodeBean tb) {

        return getNodeKindId() == null ? false : !tb.isFixed;
    }

    protected JoinedObjBasicGenericInfoBean getJoinedObjBasicInfo(Integer dstNodeId) {
        if (allItems == null || allItems.isEmpty()) {
            return null;
        }
        JoinedObjBasicInfoBean job = null;
        int selectedIdx = listBox.getSelectedIndex() >= 0 ? listBox.getSelectedIndex() : 0;
        String selectedText = getOriginalSelectedText();// trimDot(listBox.getItemText(selectedIdx));
        List<TreeNodeBean> tbs = allItems.get(selectedText);
        if (tbs != null && !tbs.isEmpty()) {
            for (TreeNodeBean tb : tbs) {
                if (tb.id == dstNodeId) {
                    job = new JoinedObjBasicInfoBean();
                    job.srcId = nodeId;
                    job.dstId = tb.id;
                    job.branchIds = tb.branchIds;
                    job.dstName = tb.name;
                    job.type = false;
                    job.inheritToDescendants = false;
                }
            }
        }

        return job;
    }

    protected void buildAlreadyJoinedObjIds() {
        if (allItems != null && !allItems.isEmpty()) {
            int selectedIdx = listBox.getSelectedIndex() >= 0 ? listBox.getSelectedIndex() : 0;
            String selectedText = getOriginalSelectedText();// trimDot(listBox.getItemText(selectedIdx));
            List<TreeNodeBean> tbs = allItems.get(selectedText);
            if (tbs != null && !tbs.isEmpty()) {
                for (TreeNodeBean tb : tbs) {
                    alreadyJoinedObjIds.add(tb.id);
                }

                for (Integer id : alreadyJoinedObjIds) {
                    JoinedObjBasicGenericInfoBean jobi = getJoinedObjBasicInfo(id);
                    splitBranchIdsIntoColl(jobi.branchIds, true, ancestorIdsOfJoinedObjs);
                }
            }
        }
    }

    @Override
    protected AbstractBIKTreeWidget<TreeNodeBean, Integer> getTreeWidget() {

        return new BikTreeWidget(mustHideLinkedNodes(),
                dataFetchBroker,
                calcHasNoChildren(), getRelationType(), getAttributeName(), getNodeKindId(), getTreeId(),
                TreeNodeBeanAsMapStorage.NAME_FIELD_NAME + "=300:Nazwa",
                TreeNodeBeanAsMapStorage.PRETTY_KIND_FIELD_NAME + "=100:Typ",
                TreeNodeBeanAsMapStorage.VIEW_NODE_FIELD_NAME + "=200:view") {
            @Override
            protected void optFixBeanPropsAfterLoad(TreeNodeBean tnb) {
                super.optFixBeanPropsAfterLoad(tnb);
            }

            @Override
            protected void afterInitWithNoData() {
                super.afterInitWithNoData();
                treeHasNoData();
            }
        };
    }
    protected PushButton addNodeBtn;
    protected PushButton clearSelectedNodeOnTreeBtn;

    @Override
    protected void addAdditionalButtons(HorizontalPanel hp) {
        clearSelectedNodeOnTreeBtn = createClearSelectedNodesOnTreeBtn();
        hp.add(clearSelectedNodeOnTreeBtn);

        addNodeBtn = addNodeBtn();
        hp.add(addNodeBtn);

        setVisibleAdditionalBtn();
    }

    protected PushButton createClearSelectedNodesOnTreeBtn() {
        PushButton clearSelectedNodeOnTreeBtn2 = NewLookUtils.newCustomPushButton(I18n.wyczyscZaznaczenia.get()/* I18N:  */, new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                AttributeBean ab = attrNamesAndType.get(getOriginalSelectedText());
                if (AttributeType.hyperlinkInMulti.name().equals(ab.typeAttr)
                        || AttributeType.permissions.name().equals(ab.typeAttr)) {

                    if (allItems != null && !allItems.isEmpty()) {
                        allItems.clear();
                    }
                    directOverride.clear();
                    fetchData();
                } else {
                    bikTree.selectEntityById(null);
                    tbDescription.setText("");
                }
                BIKClientSingletons.showInfo(I18n.usunietoWartoscAtrybutu.get() /* I18N:  */);
                actionBtn.setEnabled(ab.isEmpty);
            }
        });
        return clearSelectedNodeOnTreeBtn2;
    }

    /* czy dla tych co nie mogą być puste też pokazywać tylko wtedy nie aktywowac actionBtn?*/
 /* i jak linkami mono -> */
    protected void setVisibleAdditionalBtn() {
        AttributeBean ab = attrNamesAndType.get(getOriginalSelectedText());
        String attrType = ab.typeAttr;
        boolean isHyperlink = AttributeType.hyperlinkInMulti.name().equals(attrType) || AttributeType.hyperlinkInMono.name().equals(attrType)
                || AttributeType.permissions.name().equals(attrType);
        clearSelectedNodeOnTreeBtn.setVisible(isHyperlink && ab.isEmpty);
        addNodeBtn.setVisible(isHyperlink && BIKClientSingletons.isShowAddNodeButtonOnHyperlinkDialog());
    }

    protected Pair<String, String> getInnerJoinRelationType(Integer nodeKindId) {
        return new Pair<String, String>(nodeKindId != -1 ? BIKConstants.CHILD : null, BIKConstants.HYPERLINK);
    }
}
