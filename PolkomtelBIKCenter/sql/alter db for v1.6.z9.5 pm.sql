exec sp_check_version '1.6.z9.5';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

-- drop table bik_custom_right_role_rut_entry;

if not exists(select * from sys.objects where object_id = object_id(N'bik_custom_right_role_rut_entry') and type in (N'U'))
create table bik_custom_right_role_rut_entry (
  id int identity not null primary key,
  tree_id int not null references bik_tree (id),
  node_id int null references bik_node (id),
  is_deleted int not null,
  unique (tree_id, node_id)
);
go


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_recalculate_Custom_Right_Role_Ruts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].sp_recalculate_Custom_Right_Role_Ruts
GO

create procedure sp_recalculate_Custom_Right_Role_Ruts as
begin
  declare @regUserCrrId int = (select id from bik_custom_right_role where code = 'RegularUser');
  
  if @regUserCrrId is null return;
  
  merge into bik_custom_right_role_user_entry crrue
  using bik_custom_right_role_rut_entry crrre cross join bik_system_user su
  on crrue.role_id = @regUserCrrId and crrue.user_id = su.id 
    and crrre.tree_id = crrue.tree_id and (crrre.node_id is null and crrue.node_id is null or crrre.node_id = crrue.node_id)
  when matched and crrre.is_deleted <> 0 then
    delete
  when not matched by target and crrre.is_deleted = 0 then
    insert (user_id, tree_id, node_id, role_id) values (su.id, crrre.tree_id, crrre.node_id, @regUserCrrId)
  ;
  
  delete from bik_custom_right_role_rut_entry where is_deleted <> 0;
    
  exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
end;
go


declare @show_in_tree_node_action_id int = (select id from bik_node_action where code = 'ShowInTree');

if @show_in_tree_node_action_id is not null begin
  insert into bik_node_action_in_custom_right_role (action_id, role_id)
  select @show_in_tree_node_action_id, r.id
  from bik_custom_right_role r
  where not exists (select 1 from bik_node_action_in_custom_right_role where action_id = @show_in_tree_node_action_id and role_id = r.id);
end;
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

exec sp_update_version '1.6.z9.5', '1.6.z9.6';
go
