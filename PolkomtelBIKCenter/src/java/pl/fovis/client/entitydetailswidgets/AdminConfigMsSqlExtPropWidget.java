package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TextBox;
import pl.bssg.metadatapump.common.MsSqlExPropConfigBean;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.AdminConfigBaseWidget;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;

/**
 *
 * @author tflorczak
 */
public class AdminConfigMsSqlExtPropWidget extends AdminConfigBaseWidget<MsSqlExPropConfigBean> {

    protected final static String lineLength = "820px";
    protected TextBox database;
    protected TextBox table;
    protected TextBox view;
    protected TextBox procedure;
    protected TextBox scalarFunction;
    protected TextBox tableFunction;
    protected TextBox column;

    @Override
    public String getSourceName() {
        return I18n.wlasciwosciRozszerzone.get();
    }

    @Override
    public void populateWidgetWithData(MsSqlExPropConfigBean data) {
        database.setText(data.exPropDatabase);
        table.setText(data.exPropTable);
        view.setText(data.exPropView);
        procedure.setText(data.exPropProcedure);
        scalarFunction.setText(data.exPropScalarFunction);
        tableFunction.setText(data.exPropTableFunction);
        column.setText(data.exPropColumn);
    }

    @Override
    protected void buildInnerWidget() {
        createHeader();
        createPropertiesFields();
        createSaveButton();
    }

    protected void callServiceToSaveExProps(MsSqlExPropConfigBean bean, AsyncCallback<Void> asyncCallback) {
        BIKClientSingletons.getService().setMsSqlExPropParameters(bean, asyncCallback);
    }

    private void createHeader() {
        createLabel("<property_name>=<display_name>|<property_name>=<display_name>|...", main);
        createLabel("<property_name>=<lang>:<display_name>;<lang>:<display_name>|...", main);
        createLabel(I18n.naPrzyklad.get() + ": db=pl:Baza danych;en:Database", main);
        createLine(main, lineLength);
    }

    private void createPropertiesFields() {
        database = createPropertiesField(I18n.bazaDanych.get());
        table = createPropertiesField(I18n.tabela.get());
        view = createPropertiesField(I18n.widok.get());
        procedure = createPropertiesField(I18n.procedura.get());
        scalarFunction = createPropertiesField(I18n.funkcjaSkalarna.get());
        tableFunction = createPropertiesField(I18n.funkcjaTabelaryczna.get());
        column = createPropertiesField(I18n.kolumna.get());
        setPropertiesFieldsWidth("820px");
        createLine(main, lineLength);
    }

    private void createSaveButton() {
        createButton(I18n.zapisz.get(), new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                callServiceToSaveExProps(getExProps(), new StandardAsyncCallback<Void>("error in" /* I18N: no:err-fail-in */ + " setMsSqlExPropParameters") {
                            @Override
                            public void onSuccess(Void result) {
                                BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get());
                            }
                        });
            }
        }, main);
    }

    private TextBox createPropertiesField(String label) {
        createLabel(label, main);
        return createTextBox(main);
    }

    private void setPropertiesFieldsWidth(String width) {
        TextBox[] fields = {database, table, view, procedure, scalarFunction,
            tableFunction, column};
        for (TextBox f : fields) {
            f.setWidth(width);
        }
    }

    private MsSqlExPropConfigBean getExProps() {
        MsSqlExPropConfigBean bean = new MsSqlExPropConfigBean();
        bean.exPropColumn = column.getText();
        bean.exPropDatabase = database.getText();
        bean.exPropProcedure = procedure.getText();
        bean.exPropScalarFunction = scalarFunction.getText();
        bean.exPropTable = table.getText();
        bean.exPropTableFunction = tableFunction.getText();
        bean.exPropView = view.getText();
        return bean;
    }
}
