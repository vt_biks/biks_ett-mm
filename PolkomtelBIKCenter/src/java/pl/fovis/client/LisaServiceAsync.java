/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.fovis.common.ErwinDataModelProcessObjectsRel;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.common.lisa.ImportOverviewBean;
import pl.fovis.common.lisa.LisaAuthenticationBean;
import pl.fovis.common.lisa.LisaConstants.ImportMode;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryBean;
import pl.fovis.common.lisa.LisaDictionaryValue;
import pl.fovis.common.lisa.LisaGridParamBean;
import pl.fovis.common.lisa.LisaImportActionBean;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import pl.fovis.common.lisa.LisaLogBean;
import pl.fovis.common.lisa.TeradataException;
import simplelib.Pair;

/**
 *
 * @author tbzowka
 */
public interface LisaServiceAsync {

    public void getDictionaryColumns(String dictionaryName, AsyncCallback<List<LisaDictColumnBean>> callback);

    public void updateDictionaryColumns(LisaDictColumnBean bean, AsyncCallback<Void> callback);

    public void getDictionaryColumns(Integer categoryNodeId, AsyncCallback<List<LisaDictColumnBean>> callback);

    public void getDictionaryColumnsForDic(Integer dicNodeId, AsyncCallback<List<LisaDictColumnBean>> callback);

    public void saveNewCategory(Integer instanceId, String categoryName, Integer categoryTypeNodeId, AsyncCallback<TreeNodeBean> AsyncCallback);

    public void deleteCategory(Integer instanceId, TreeNodeBean nodeToDelete, AsyncCallback<List<String>> AsyncCallback) throws TeradataException;

    public void moveCategory(Integer instanceId, TreeNodeBean nodeTomove, Integer newCategId, AsyncCallback<List<String>> AsyncCallback) throws TeradataException;

    public void saveNewCategorization(Integer instanceId, final String categorizationName, Integer dictionaryParentNodeId, AsyncCallback<Pair<TreeNodeBean, TreeNodeBean>> AsyncCallback);

    public Request getDictionaryRecords(Integer instanceId, LisaGridParamBean params, String requestTicket, AsyncCallback<List<Map<String, LisaDictionaryValue>>> AsyncCallback);

    public void getCountDictionaryRecords(Integer lisaInstanceId, LisaGridParamBean params, String requestTicket, AsyncCallback<Integer> AsyncCallback);

    public void getDictionaries(Integer instanceId, AsyncCallback<List<LisaDictionaryBean>> callback);

    public void authorizeTeradataConnection(Integer instanceId, String username, String password, AsyncCallback<Void> callback);

//    public void getLisaConnectionData(AsyncCallback<LisaTeradataConnectionBean> callback);
//    public void updateLisaConnectionData(LisaTeradataConnectionBean bean, AsyncCallback<Void> callback);
    public void moveValuesToCategory(Integer instanceId, Set<Map<String, LisaDictionaryValue>> values, Integer oldCategoryNodeId, Integer newCategoryNodeId, AsyncCallback<Void> callback);
//    public void updateDictionaryContent(Map<String, LisaDictionaryValue> record, Integer categoryNodeId, boolean fromDictonary, AsyncCallback<Void> callback);

    public void updateDictionaryContents(Integer instanceId, List<LisaDictColumnBean> columnInfo, Set<Map<String, LisaDictionaryValue>> record, Integer categoryNodeId, boolean fromDictonary, AsyncCallback<Void> callback);

    public void updateDictionaryContents(Integer instanceId, Set<Map<String, LisaDictionaryValue>> record, Integer categoryNodeId, boolean fromDictonary, AsyncCallback<Void> callback);

    public void deleteDictionaryContent(Integer instanceId, Set<Map<String, LisaDictionaryValue>> record, Integer categoryNodeId, boolean fromDictonary, AsyncCallback<Void> callback);

    public void insertDictionaryContent(Integer instanceId, List<LisaDictColumnBean> columnInfo, Map<String, LisaDictionaryValue> record, Integer categoryNodeId, boolean fromDictonary, AsyncCallback<Void> callback);

    public void insertDictionaryContent(Integer instanceId, Map<String, LisaDictionaryValue> record, Integer categoryNodeId, boolean fromDictonary, AsyncCallback<Void> callback);

    public void getDictionaryColumnsByNodeId(Integer nodeId, AsyncCallback<List<LisaDictColumnBean>> callback);

    public void getLisaLogs(LisaGridParamBean params, AsyncCallback<List<LisaLogBean>> callback);

    public void addNewDictionary(Integer instanceId, LisaDictionaryBean b, AsyncCallback<String> callback);

    public void deleteDictionary(Integer instanceId, String dictionaryName, AsyncCallback<Void> callback);

//    public void checkLisaUnassignedCategory(AsyncCallback<Boolean> callback);
    public void deleteAllTeredataSessions(AsyncCallback<Boolean> callback);

    public void getDistinctColumnValues(Integer instanceId, LisaDictColumnBean bean, String value, int numberFetchItem, AsyncCallback<List<String>> callback);

//    public void checkIsUniqueColumn(String dictName, String colName, AsyncCallback<Integer> callback);
    public void getOriginalValueFromReference(Integer instanceId, LisaDictColumnBean columnBean, String value, AsyncCallback<String> asyncCallback);

    public void getValidationList(int columnIdInBIK, AsyncCallback<List<String>> asyncCallback);

    public void addValidationItem(int columnIdInBIK, String value, AsyncCallback<Void> asyncCallback);

    public void deleteValidationItem(int columnIdInBIK, String value, AsyncCallback<Void> asyncCallback);

    public void getDataModelProcessObjectsRel(int nodeId, String areaName, boolean dashboardObjects, AsyncCallback<List<ErwinDataModelProcessObjectsRel>> asyncCallback);

    public void getLisaAuthentication(int nodeId, AsyncCallback<LisaAuthenticationBean> callback);

    public void saveOrAddLisaAuthentication(LisaAuthenticationBean bean, AsyncCallback<Void> callback);

    public void tryLoginToTeradata(Integer instanceId, Integer nodeId, AsyncCallback<Boolean> callback) throws TeradataException;

    public void addLisaFolder(int parentNodeId, String folderName, AsyncCallback<Void> callback);

    public void exportDictionary(Integer instanceId, Integer nodeId, List<String> mappedNames, AsyncCallback<String> callback);

    public void extractColumnNamesFromFile(String serverFileName, AsyncCallback<List<String>> callback);

    public void importDictionary(Integer instanceId, int nodeId, Map<String, String> mappedNames, String serverFileName, ImportMode imporMode, AsyncCallback<Void> callback);

    public void analyseDictWithImportedFile(Integer instanceId, int nodeId, Map<String, String> mappedNames, String serverFilename, ImportMode importMode, AsyncCallback<ImportOverviewBean> callback);

    public void clearLisaImportCache(String serverFileName, AsyncCallback<Void> callback);

    public void getActionList(String serverFileName, String actionCode, int offset, int limit, AsyncCallback<List<LisaImportActionBean>> callback);

    public void getCategoryzationNodeId(Integer categoryId, AsyncCallback<Integer> callback);

    public void validateRecords(Integer instanceId, List<LisaDictColumnBean> columnInfo, Set<Map<String, LisaDictionaryValue>> records, Integer nodeId, boolean editFromDictonary, AsyncCallback<Boolean> callback);

    public void getLisaInstanceList(AsyncCallback<List<LisaInstanceInfoBean>> callback);

    public void updateLisaInstanceInfo(LisaInstanceInfoBean bean, AsyncCallback callback);

    public void addLisaInstanceInfo(LisaInstanceInfoBean bean, AsyncCallback callback);

    public void deleteLisaInstance(Integer selectedId, AsyncCallback<Void> callback) throws TeradataException;

//    public void getLisaInstanceInfoByNodeId(int id, AsyncCallback<LisaInstanceInfoBean> callback);
    public void getLisaInstanceByNodeId(int nodeId, AsyncCallback<LisaInstanceInfoBean> callback);
}
