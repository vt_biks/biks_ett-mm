/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaAuthenticationBean;

/**
 *
 * @author ctran
 */
public class AuthenticationModeDialog extends SerializableLisaDialogBase<LisaAuthenticationBean> {

    protected RadioButton askEveryTimeBtn = createRadioButton(AuthenticationMode.ASK_EVERY_TIME, I18n.pytajPrzyPolaczeniu.get());
    protected RadioButton useSavedLoginBtn = createRadioButton(AuthenticationMode.USE_SAVED_LOGIN, I18n.uzywajLogin.get());
    protected TextBox teradataLoginTb = new TextBox();
    protected PasswordTextBox teradataPwdTb = new PasswordTextBox();

    @Override
    protected String getDialogCaption() {
        return I18n.typUwierzytelnienia.get();
    }

    @Override
    protected void buildMainWidgets() {
        FlexTable table = new FlexTable();
        main.add(askEveryTimeBtn);
        main.add(useSavedLoginBtn);
        int row = 0;
        table.setWidget(row, 0, new Label(I18n.login.get()));
        table.setWidget(row++, 1, teradataLoginTb);
        table.setWidget(row, 0, new Label(I18n.haslo.get()));
        table.setWidget(row++, 1, teradataPwdTb);
        main.add(table);
    }

    protected void onAuthenticationModeChange(AuthenticationMode authenticationMode) {
        bean.loginMode = authenticationMode.mode;
        enableLoginTable(useSavedLoginBtn.getValue());
    }

    @Override
    protected void customFillControlsBeforeShow() {
        if (AuthenticationMode.USE_SAVED_LOGIN.mode == bean.loginMode) {
            useSavedLoginBtn.setValue(true);
            onAuthenticationModeChange(AuthenticationMode.USE_SAVED_LOGIN);
        } else {
            askEveryTimeBtn.setValue(true);
            onAuthenticationModeChange(AuthenticationMode.ASK_EVERY_TIME);
        }
        teradataLoginTb.setText(bean.teradataLogin);
        teradataPwdTb.setText(bean.teradataPwd);
    }

    @Override
    protected void customFillBeanBeforeAction() {
        bean.teradataLogin = teradataLoginTb.getText();
        bean.teradataPwd = teradataPwdTb.getText();
    }

    @Override
    protected LisaAuthenticationBean createBean() {
        return new LisaAuthenticationBean();
    }

    private RadioButton createRadioButton(final AuthenticationMode authenticationMode, String text) {
        RadioButton btn = new RadioButton("authenticationMode", text);
        btn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                onAuthenticationModeChange(authenticationMode);
            }
        });
        return btn;
    }

    private void enableLoginTable(boolean isEnabled) {
        teradataLoginTb.setEnabled(isEnabled);
        teradataPwdTb.setEnabled(isEnabled);
    }

    public static enum AuthenticationMode {

        ASK_EVERY_TIME(0),
        USE_SAVED_LOGIN(1);

        public int mode;

        private AuthenticationMode(int mode) {
            this.mode = mode;
        }
    }
}
