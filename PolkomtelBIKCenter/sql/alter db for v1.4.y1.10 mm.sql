﻿exec sp_check_version '1.4.y1.10';
go

update bik_translation set txt = 'Filter' where code = 'Filtr'
and lang = 'en' and kind = 'adef'
go

exec sp_update_version '1.4.y1.10', '1.4.y1.11';
go
