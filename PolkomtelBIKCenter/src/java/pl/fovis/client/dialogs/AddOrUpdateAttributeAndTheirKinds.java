/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RichTextArea;
import java.util.HashMap;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.client.bikwidgets.BikRichTextArea;
import pl.fovis.common.AttributeAndTheirKindsBean;
import pl.fovis.common.NodeKindBean;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;
import simplelib.IParametrizedContinuation;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author AMamcarz
 */
public class AddOrUpdateAttributeAndTheirKinds extends BaseActionOrCancelDialog {

    private IParametrizedContinuation<AttributeAndTheirKindsBean> saveCont;
    private AttributeAndTheirKindsBean record;
    protected Label status;
    protected RichTextArea tbAtrName;
    private Label lblQuestion;
    private Label lblAnswer;
    private ListBox listBox;
    private HashMap<String, NodeKindBean> listOfNodeKind;
    int index;

    @Override
    protected String getDialogCaption() {
        return I18n.atrybutDodatkowy.get() /* I18N:  */;
    }

    @Override
    protected String getActionButtonCaption() {
        return I18n.zapisz.get() /* I18N:  */;
    }

    @Override
    protected void buildMainWidgets() {
        status = new Label();
        main.add(status);
        Grid grid = new Grid(2, 2);
        grid.setCellSpacing(4);

        grid.setWidget(0, 0, lblQuestion = new Label("Node kind" /* I18N: no */+ ":"));
        grid.setWidget(1, 0, lblAnswer = new Label(I18n.nazwaAtrybutu.get() /* I18N:  */+ ":"));
        grid.setWidget(0, 1, listBox = new ListBox());
        grid.setWidget(1, 1, tbAtrName = new BikRichTextArea());
        lblQuestion.setStyleName("addNoteLbl");
        lblAnswer.setStyleName("addNoteLbl");
        tbAtrName.setStyleName("addNote");
        listBox.setStyleName("addAttr");
        listBox.setWidth("210px");

        BIKClientSingletons.getService().getNodeKinds(new StandardAsyncCallback<List<NodeKindBean>>("Failed in getting node kind's" /* I18N: no:err-fail-in */) {

            public void onSuccess(List<NodeKindBean> result) {
                listOfNodeKind = new HashMap<String, NodeKindBean>();
                boolean pom = true;
                for (NodeKindBean value : result) {
                    listBox.addItem(value.caption + "(" + value.code + ")");
                    listOfNodeKind.put(value.caption + "(" + value.code + ")", value);
                    if (record != null && pom && (value.caption + "(" + value.code + ")").equals(record.kindCaption + "(" + record.kindCode + ")")) {
                        index = listBox.getItemCount() - 1;
                        pom = false;
                    }
                }
                selectInListBox(index);
            }
        });
        tbAtrName.setWidth("240px");
        tbAtrName.setHTML((record != null) ? record.atrName : "");

        main.add(grid);
    }

    private void selectInListBox(int index) {
        if (record != null) {
            listBox.setSelectedIndex(index);
        }
    }

    @Override
    protected boolean doActionBeforeClose() {
        if (tbAtrName.getText().trim().equals("") || listBox.getValue(listBox.getSelectedIndex()).trim().equals("")) {
            BIKClientSingletons.showWarning(I18n.polaNazwaAtrybutuINodeKindNieMog.get() /* I18N:  */);
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void doAction() {
        AttributeAndTheirKindsBean attAndTKinds = new AttributeAndTheirKindsBean();
        attAndTKinds.id = record == null ? 0 : record.id;
        attAndTKinds.atrName = tbAtrName.getText();
        attAndTKinds.kindId = listOfNodeKind.get(listBox.getValue(listBox.getSelectedIndex())).id;
        this.saveCont.doIt(attAndTKinds);
    }

    public void buildAndShowDialog(AttributeAndTheirKindsBean attAndTKinds, IParametrizedContinuation<AttributeAndTheirKindsBean> saveCont) {
        this.saveCont = saveCont;
        this.record = attAndTKinds;
        super.buildAndShowDialog();
    }
}
