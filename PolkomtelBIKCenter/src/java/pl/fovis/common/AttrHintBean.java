/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author beata
 */
public class AttrHintBean implements Serializable {

    public int id;
    public String name;
    public String hint;
    public boolean isDeleted;
    public boolean isBuiltIn;
    public String typeAttr;
    
}
