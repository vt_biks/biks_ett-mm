/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib.stringeraser;

/**
 * Wymazywalny element składni
 *
 * @author pmielanczuk
 */
public interface IErasableSyntaxItem {

    // pos - od którego znaku szukać
    // searchPosLimit - do którego znaku jest sens szukać - wystąpienia pod tym
    // lub dalszym znakiem nie są potrzebne, równie dobrze może zwrócić -1 wtedy
    // gdy nie znajdzie tego ESI, zwraca null
    public EsiPos findStartPos(String content, int pos, int searchPosLimit);
}
