------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- 1. dekompozycja tabeli registration na action_token i registered user
-- 2. powiązanie bazy z rejestracją
--    - nowe pole registration_id w mltx_database
-- 3. nowe pola w mltx_registration
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_mltx_check_version '1.1'
go

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

create table mltx_action_type(
	action_type varchar(20) primary key not null
);
go

create table mltx_action_token(
	id int IDENTITY(1,1) primary key NOT NULL,
	token varchar(128) NOT NULL,
	email varchar(128) NOT NULL,
	is_valid int NOT NULL,
	created_at datetime NOT NULL,
	expires_at datetime NOT NULL,
	status int NOT NULL,
	action_type varchar(20) not null,
	foreign key (action_type) references mltx_action_type(action_type)
);
go

create table mltx_registered_user (
	id int IDENTITY(1,1) primary key NOT NULL,
	first_name varchar(200) not null,
	last_name varchar(200) not null,
	email varchar(128) not null,
	phone_num varchar(30) ,
	company varchar(200) ,
	job_title varchar(200) ,
	country varchar(200) ,
	city varchar(200)
);
go

create table mltx_invitation(
	id int IDENTITY(1,1) primary key NOT NULL,
	token_id int not null,
	foreign key (token_id) references mltx_action_token(id)
);
go

insert into mltx_action_type (action_type) values ('register'), ('invite'), ('reset_pass');
go

insert into mltx_action_token
(token,is_valid,created_at, expires_at, status, email, action_type)
 select token,is_valid,created_at, expires_at, status, email, 'register'
from mltx_registration;
go

insert into mltx_registered_user
(first_name, last_name, email, phone_num, company, job_title, country, city)
select coalesce(first_name,'John'), coalesce(last_name,'Doe'), email, phone_num, company, job_title, country, city
from mltx_registration;
go

alter table mltx_database
add creator_id int;
go

update d
set creator_id = ru.id
from mltx_database d
join mltx_registration r on d.registration_id = r.id
join mltx_registered_user ru on ru.email = r.email


--na wypadek gdy baza byla niespojna
update mltx_database
set creator_id = (select min(id) from mltx_registration)
where creator_id is null;


alter table mltx_database
add  constraint mltx_database_creator_fk FOREIGN KEY (creator_id)
      REFERENCES mltx_registered_user (id) ;
go

alter table mltx_database
alter column creator_id int not null;

alter table mltx_database
add template_id int;
go

update d
set template_id = r.template_id
from mltx_database d
join mltx_registration r on d.registration_id = r.id
go

alter table mltx_database
add  constraint mltx_database_template_fk FOREIGN KEY (template_id)
      REFERENCES mltx_database_template (id) ;
go

--na wypadek gdy baza byla niespojna
update mltx_database
set template_id = (select top 1 template_id from mltx_registration order by id asc)
where template_id is null;

alter table mltx_database
alter column template_id int not null;
go




------------------------------------------------------
------------------------------------------------------
------------------------------------------------------

exec sp_mltx_update_version '1.1', '1.2'
go
