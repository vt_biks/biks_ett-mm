﻿exec sp_check_version '1.6.z9.19';
go

--------------------------------------
--------------------------------------
--------------------------------------

if not exists(select * from bik_translation where code = 'Metoda próbkowania' and kind = 'adef' and lang = 'pl')
begin
	insert into bik_translation(code,txt,lang,kind)
	values ('Metoda próbkowania', 'Częstotliwość uruchamiania testu', 'pl', 'adef')
	
	insert into bik_translation(code,txt,lang,kind)
	values ('Informacje dodatkowe', 'Sposób uruchamiania testu', 'pl', 'adef')
	
	insert into bik_translation(code,txt,lang,kind)
	values ('Zapytanie testujące', 'Zapytanie sprawdzające czy występują błędy', 'pl', 'adef')
	
end;

--------------------------------------
--------------------------------------
--------------------------------------

exec sp_update_version '1.6.z9.19', '1.6.z9.20';
go
