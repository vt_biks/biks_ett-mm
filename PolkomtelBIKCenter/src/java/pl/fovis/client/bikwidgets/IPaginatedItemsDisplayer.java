/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikwidgets;

import com.google.gwt.user.client.ui.Widget;
import java.util.List;

/**
 *
 * @author mmastalerz
 */
public interface IPaginatedItemsDisplayer<T> {

    public void setPaginatedItemsProvider(IPaginatedItemsProvider<T> paginator);

    public Widget build(List<T> itemsToDisplay, int firstItemToDisplayIdx, List<T> filteredItems, List<T> allItems);
}
