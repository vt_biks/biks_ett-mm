/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db.datapump;

import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.templates.INamedTemplateProvider;

/**
 *
 * @author pmielanczuk
 */
public class MssqlDataPumpDestination extends JdbcDataPumpDestination {

    public MssqlDataPumpDestination(IBeanConnectionEx<Object> dstConn, INamedTemplateProvider templateProvider, String dstTableName) {
        super(dstConn, templateProvider, dstTableName);
    }

    @Override
    protected void dropDstTableIfExists() {
        dstConn.execCommand("IF OBJECT_ID (N'" + dstTableName + "', N'U') IS NOT NULL drop table " + dstTableName);
    }

    @Override
    protected String optAddAutoIncColumn() {
        return ", \n[" + "id_" + dstTableName + "] int identity(1,1) not null";
    }
}
