/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.basewidgets;

import com.google.gwt.user.client.ui.VerticalPanel;

/**
 *
 * @author pmielanczuk
 */
public abstract class IsWidgetVp extends IsWidgetPanel<VerticalPanel> {

    @Override
    protected VerticalPanel createMainWidget() {
        return new VerticalPanel();
    }
}
