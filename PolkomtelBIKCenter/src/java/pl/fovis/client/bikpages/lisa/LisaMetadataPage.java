/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.lisa;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.BikPageMinimalBase;
import pl.fovis.client.dialogs.FileUploadDialogSimple;
import pl.fovis.client.dialogs.lisa.AddLisaDictionaryDialog;
import pl.fovis.client.dialogs.lisa.AuthenticationModeDialog;
import pl.fovis.client.dialogs.lisa.LisaBeanDisplay;
import pl.fovis.client.dialogs.lisa.LisaDictionaryColumnEditionDialog;
import pl.fovis.client.dialogs.lisa.LisaExportDictionaryDialog;
import pl.fovis.client.dialogs.lisa.LisaImportDictionaryDialog;
import pl.fovis.client.entitydetailswidgets.lisa.LisaAuthorizedRequest;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.common.lisa.LisaAuthenticationBean;
import pl.fovis.common.lisa.LisaConstants;
import pl.fovis.common.lisa.LisaDictColumnBean;
import pl.fovis.common.lisa.LisaDictionaryBean;
import pl.fovis.common.lisa.LisaInstanceInfoBean;
import pl.fovis.foxygwtcommons.FoxyFileUpload;
import pl.fovis.foxygwtcommons.NewLookUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import pl.fovis.foxygwtcommons.dialogs.IAsyncDialogAction;
import pl.fovis.foxygwtcommons.dialogs.SimpleProgressInfoDialog;
import pl.fovis.foxygwtcommons.grid.BeanGrid;
import pl.fovis.foxygwtcommons.grid.BeanGridPager;
import pl.fovis.foxygwtcommons.grid.IBeanProvider;
import pl.fovis.foxygwtcommons.grid.StandardBeanDisplay;
import simplelib.BaseUtils;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author tbzowka
 */
public class LisaMetadataPage extends BikPageMinimalBase {

    protected BeanGrid<LisaDictColumnBean> dictColumnGrid;
    protected ListBox dictionariesLb, instancesLb;
    protected List<LisaDictionaryBean> allDictionaries;
    protected LisaDictionaryBean selectedDict;
    protected LisaInstanceInfoBean selectedInstance;
    protected List<LisaDictColumnBean> allColumnsOfSelectedDict;
    protected Map<Integer, LisaInstanceInfoBean> lisaInstanceById;
    protected VerticalPanel vp;
    protected PushButton deleteButton;
    protected PushButton importBtn, exportBtn;
    protected boolean pageWasCreated = false;
//    protected List<String> dictValidationList = new ArrayList<String>();

    @Override
    protected Widget buildWidgets() {
        vp = new VerticalPanel();

        Label dictionariesLbl = new Label(I18n.listaSlownikow.get() /* I18N:  */ + ":");
        dictionariesLb = new ListBox();
        instancesLb = new ListBox();
        selectedInstance = null;
        instancesLb.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Integer selectedInstanceId = BaseUtils.tryParseInteger(instancesLb.getSelectedValue());
                if (selectedInstanceId != null) {
                    selectedInstance = lisaInstanceById.get(selectedInstanceId);
                    onSelectedInstanceChanged();
                }
            }
        });
        dictionariesLb.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                onSelectedDictionaryChanged(dictionariesLb.getSelectedIndex());
            }
        });

        HorizontalPanel dictionariesPanel = new HorizontalPanel();
        dictionariesPanel.setSpacing(6);
        dictionariesPanel.add(dictionariesLbl);
        dictionariesPanel.add(instancesLb);
        dictionariesPanel.add(dictionariesLb);
        dictionariesPanel.add(createAddDictionaryButton());
        dictionariesPanel.add(createDeleteDictionaryButton());
        dictionariesPanel.add(createAuthenticationModeButton());
        dictionariesPanel.add(exportBtn = createExportButton());
        dictionariesPanel.add(importBtn = createImportButton());
        vp.add(dictionariesPanel);
        vp.add(dictColumnGrid = createDictColumnGrid());
        BeanGridPager<LisaDictColumnBean> bgp = new BeanGridPager<LisaDictColumnBean>(dictColumnGrid);
        vp.add(bgp);

//        refreshDictionariesList();
        populateInstacesList();

        ScrollPanel scPanel = new ScrollPanel(vp);
        BIKClientSingletons.registerResizeSensitiveWidgetByHeight(scPanel, BIKClientSingletons.ORIGINAL_TREE_AND_FILTER_HEIGHT);

        pageWasCreated = true;
        return scPanel;
    }

    private void onSelectedDictionaryChanged(int selectedIndex) {
        selectedDict = allDictionaries.get(selectedIndex);
        dictColumnGrid.setPageNum(0);
        changeButtonsState();
    }

    private void onSelectedInstanceChanged() {
        selectedDict = null;
        allDictionaries = null;
        allColumnsOfSelectedDict = null;
        dictColumnGrid.setPageNum(0);
        changeButtonsState();
        refreshDictionariesList();
    }

    protected void modifyColumn(final LisaDictColumnBean cb) {
        new LisaDictionaryColumnEditionDialog().buildAndShowDialog(cb, new IAsyncDialogAction<LisaDictColumnBean>() {
            @Override
            public void doIt(final LisaDictColumnBean bean, final IParametrizedContinuation<String> onCompleteCont) {
                dictColumnGrid.oneBeanChanged(bean);
                BIKClientSingletons.getLisaService().updateDictionaryColumns(bean, new StandardAsyncCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        onCompleteCont.doIt(null);
                    }
                });
            }
        });
    }

    public void refreshDictionariesList() {
        BIKClientSingletons.getLisaService().getDictionaries(selectedInstance.id, new StandardAsyncCallback<List<LisaDictionaryBean>>() {
            @Override
            public void onSuccess(List<LisaDictionaryBean> result) {
                dictionariesLb.clear();
                allDictionaries = result;
                selectedDict = null;
                for (LisaDictionaryBean bean : result) {
                    dictionariesLb.addItem(bean.displayName);
//                    dictValidationList.add(bean.dbName);
                }
                selectDictionary();
                changeButtonsState();
            }
        });
    }

    private void selectDictionary() {
        if (BaseUtils.isCollectionEmpty(allDictionaries)) { //nie jest pobrana lista slownikow lub nie ma jeszcze slownika
            selectedDict = null;
            return;
        }
        if (selectedDict == null) {
            selectedDict = allDictionaries.get(0);
        }
        for (int i = 0; i < allDictionaries.size(); i++) {
            if (allDictionaries.get(i).nodeId == selectedDict.nodeId) {
                onSelectedDictionaryChanged(i);
                return;
            }
        }
        onSelectedDictionaryChanged(0);
    }

    @Override
    public String getId() {
        return "admin:lisa:metadata";
    }

    @Override
    public void activatePage(boolean refreshData, Integer optIdToSelect, boolean refreshDetailData) {
        if (pageWasCreated) {
            populateInstacesList();
        }
    }

    private PushButton createAddDictionaryButton() {
        PushButton b = new PushButton(I18n.dodajNowy.get());
        NewLookUtils.makeCustomPushButton(b);
        b.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                new AddLisaDictionaryDialog().buildAndShowDialog(null, new IAsyncDialogAction<LisaDictionaryBean>() {
                    @Override
                    public void doIt(final LisaDictionaryBean bean, final IParametrizedContinuation<String> onCompleteCont) {
                        bean.lisaId = selectedInstance.id;
                        bean.lisaName = selectedInstance.name;
                        bean.dbName = bean.lisaId + "." + bean.dbName;
                        bean.dbNameReal = bean.lisaId + "." + bean.dbNameReal;

                        new LisaAuthorizedRequest<String>(selectedInstance.id, selectedInstance.name) {

                            @Override
                            protected void performServerAction(AsyncCallback<String> callback) {
                                BIKClientSingletons.getLisaService().addNewDictionary(bean.lisaId, bean, callback);
                            }

                            @Override
                            public void onActionSuccess(String result) {
                                onCompleteCont.doIt(result);
                                populateInstacesList();
                            }

                            @Override
                            public void onActionFailure(Throwable caught) {
                                onCompleteCont.doIt(caught.getMessage());
                            }
                        }.go();
                    }
                });
            }
        });
        return b;
    }

    private PushButton createDeleteDictionaryButton() {
        deleteButton = new PushButton(I18n.usunSlownik.get());
        NewLookUtils.makeCustomPushButton(deleteButton);
        deleteButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                HTML element = new HTML(I18n.czyNaPewnoUsunaSlownik.get() /* I18N:  */ + "? <div class='PUWelement'><img src='images/" /* I18N: no */ + BIKClientSingletons.getNodeKindIconNameByCode(BIKConstants.NODE_KIND_LISA_DICTIONARY, BIKConstants.TREE_CODE_DICTIONARIES_DWH) + "." + "gif" /* I18N: no */ + "'/> " + BaseUtils.encodeForHTMLTag(selectedDict.dbName) + "</div> ");
                element.setStyleName("komunikat" /* I18N: no:css-class */);
                new ConfirmDialog().buildAndShowDialog(element, I18n.usun.get() /* I18N:  */, new IContinuation() {
                            @Override
                            public void doIt() {

                                new LisaAuthorizedRequest<Void>(selectedInstance.id, selectedInstance.name) {
                                    @Override
                                    protected void performServerAction(AsyncCallback<Void> callback) {
                                        BIKClientSingletons.getLisaService().deleteDictionary(selectedDict.lisaId, selectedDict.dbName, callback);
                                    }

                                    @Override
                                    public void onActionSuccess(Void result) {
                                        BIKClientSingletons.showInfo(I18n.kasowaniaSlownikaSukces.get());
                                        dictionariesLb.removeItem(dictionariesLb.getSelectedIndex());
                                        refreshDictionariesList();
                                    }

                                    @Override
                                    public void onActionFailure(Throwable caught) {
                                        BIKClientSingletons.showWarning(I18n.kasowaniaSlownikaBlad.get());
                                    }
                                }.go();

                            }
                        });
            }
        });

        deleteButton.setEnabled(false);
        return deleteButton;
    }

    private void changeButtonsState() {
        if (deleteButton != null) {
            if (dictionariesLb != null && dictionariesLb.getItemCount() > 0) {
                deleteButton.setEnabled(true);
            }
        }
        if (importBtn != null) {
            importBtn.setEnabled((selectedDict == null || allColumnsOfSelectedDict == null) ? false : /*!LisaUtils.hasDateColumns(allColumnsOfSelectedDict)*/ true);
        }
        if (exportBtn != null) {
            exportBtn.setEnabled(selectedDict != null);
        }
    }

    private PushButton createExportButton() {
        PushButton btn = new PushButton(I18n.eksport.get());
        NewLookUtils.makeCustomPushButton(btn);
        btn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (selectedDict == null) {
                    BIKClientSingletons.showError(I18n.wybierzSlownikDoEksportu.get());
                    return;
                }
                final List<String> mappedNames = new ArrayList<String>();
                List<String> originalNames = new ArrayList<String>();
                for (LisaDictColumnBean column : allColumnsOfSelectedDict) {
                    mappedNames.add(column.titleToDisplay);
                    originalNames.add(column.nameInDb);
                }
                new LisaExportDictionaryDialog().buildAndShowDialog(selectedDict, allColumnsOfSelectedDict);
            }
        });
        return btn;
    }

    private PushButton createAuthenticationModeButton() {
        PushButton btn = new PushButton(I18n.typUwierzytelnienia.get());
        NewLookUtils.makeCustomPushButton(btn);
        btn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                BIKClientSingletons.getLisaService().getLisaAuthentication(selectedDict.nodeId, new StandardAsyncCallback<LisaAuthenticationBean>() {

                    @Override
                    public void onSuccess(LisaAuthenticationBean bean) {
                        new AuthenticationModeDialog().buildAndShowDialog(bean, new IAsyncDialogAction<LisaAuthenticationBean>() {

                            @Override
                            public void doIt(final LisaAuthenticationBean bean, final IParametrizedContinuation<String> onCompleteCont) {
                                BIKClientSingletons.getLisaService().saveOrAddLisaAuthentication(bean, new StandardAsyncCallback<Void>() {

                                    @Override
                                    public void onSuccess(Void result) {
                                        onCompleteCont.doIt(null);
                                        BIKClientSingletons.showInfo(I18n.zapisanoZmiany.get());
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
        return btn;
    }

    private BeanGrid<LisaDictColumnBean> createDictColumnGrid() {
        return new BeanGrid<LisaDictColumnBean>(new IBeanProvider<LisaDictColumnBean>() {
            @Override
            public void getBeans(final int offset, final int limit, Iterable<String> sortCols,
                    Iterable<String> descendingCols, final IParametrizedContinuation<Pair<? extends Iterable<LisaDictColumnBean>, Integer>> showBeansCont) {
                BIKClientSingletons.getLisaService().getDictionaryColumns(selectedDict == null ? "" : selectedDict.dbName, new StandardAsyncCallback<List<LisaDictColumnBean>>() {
                    @Override
                    public void onSuccess(List<LisaDictColumnBean> result) {
                        allColumnsOfSelectedDict = result;
                        int cnt = result.size();
                        int toIndex = offset + limit;
                        if (limit < 0 || toIndex > cnt) {
                            toIndex = cnt;
                        }
                        showBeansCont.doIt(new Pair<Iterable<LisaDictColumnBean>, Integer>(result.subList(offset, toIndex), cnt));
                    }
                });
            }
        }, new LisaBeanDisplay<LisaDictColumnBean>(LisaDictColumnBean.class, new IParametrizedContinuation<LisaDictColumnBean>() {
            @Override
            public void doIt(final LisaDictColumnBean param) {
                modifyColumn(param);
            }
        }, BIKClientSingletons.beanCreator), LisaConstants.GRID_PAGE_SIZE, LisaBeanDisplay.NAME_IN_DB,
                LisaBeanDisplay.TITLE_TO_DISPLAY,
                LisaBeanDisplay.DESCRIPTION,
                LisaBeanDisplay.DATA_TYPE,
                LisaBeanDisplay.IS_MAIN_KEY,
                LisaBeanDisplay.ACCESS_LEVEL,
                LisaBeanDisplay.DICT_VALIDATION,
                LisaBeanDisplay.COL_VALIDATION,
                LisaBeanDisplay.TEXT_DISPLAYED,
                StandardBeanDisplay.ACT_EDIT);
    }

    private PushButton createImportButton() {
        PushButton btn = new PushButton(I18n.importuj.get());
        NewLookUtils.makeCustomPushButton(btn);
        btn.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (selectedDict == null) {
                    BIKClientSingletons.showError(I18n.wybierzSlownikDoImportu.get());
                    return;
                }
                new FileUploadDialogSimple().buildAndShowDialog(new IParametrizedContinuation<FoxyFileUpload>() {
                    private SimpleProgressInfoDialog infoDialog;

                    @Override
                    public void doIt(final FoxyFileUpload ffu) {
                        final String serverFileName = ffu.getServerFileName();
                        infoDialog = new SimpleProgressInfoDialog();
                        infoDialog.buildAndShowDialog(I18n.pleaseWait.get(), I18n.trwaPrzygotowanieImportu.get(), true);
                        BIKClientSingletons.getLisaService().extractColumnNamesFromFile(serverFileName, new StandardAsyncCallback<List<String>>() {

                            @Override
                            public void onSuccess(final List<String> headerNames) {
                                infoDialog.hideDialog();
                                new LisaImportDictionaryDialog().buildAndShowDialog(selectedDict, allColumnsOfSelectedDict, headerNames, serverFileName);
                            }

                            @Override
                            public void onFailure(Throwable caught) {
                                infoDialog.hideDialog();
                                super.onFailure(caught); //To change body of generated methods, choose Tools | Templates.
                            }
                        });
                    }
                }, Arrays.asList(BIKClientSingletons.allowedFileExtensionsList()));
            }
        });

        return btn;
    }

    private void populateInstacesList() {
        BIKClientSingletons.getLisaService().getLisaInstanceList(new StandardAsyncCallback<List<LisaInstanceInfoBean>>() {

            @Override
            public void onSuccess(List<LisaInstanceInfoBean> result) {
                dictionariesLb.clear();
                instancesLb.clear();

                lisaInstanceById = new HashMap<Integer, LisaInstanceInfoBean>();
                for (LisaInstanceInfoBean instance : result) {
                    lisaInstanceById.put(instance.id, instance);
                    instancesLb.addItem(instance.name);
                    instancesLb.setValue(instancesLb.getItemCount() - 1, BaseUtils.safeToString(instance.id));
                }
                if (result.size() > 0) {
                    selectedInstance = result.get(0);
                    refreshDictionariesList();
                }
            }
        });
    }
}
