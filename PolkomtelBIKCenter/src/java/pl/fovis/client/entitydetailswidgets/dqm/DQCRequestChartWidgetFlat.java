/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets.dqm;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gchart.client.GChart;
import java.util.List;
import pl.fovis.client.BIKGWTConstants;
import pl.fovis.client.bikwidgets.BikRequestChart;
import pl.fovis.client.entitydetailswidgets.EntityDetailsTabWithItemsBase;
import pl.fovis.common.BIKConstants;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.dqm.DataQualityRequestBean;

/**
 *
 * @author tflorczak
 */
public class DQCRequestChartWidgetFlat extends EntityDetailsTabWithItemsBase<DataQualityRequestBean> {

    protected VerticalPanel mainVp;
    protected GChart chart;

    @Override
    protected List<DataQualityRequestBean> extractItemsFromFetchedData(EntityDetailsDataBean data) {
        return data.dataQualityTestRequests;
    }

    @Override
    public void displayData() {
        mainVp.clear();
        chart = new BikRequestChart(getData().dataQualityTestExtradata.errorThreshold, items);
        mainVp.add(chart);
        chart.update();
    }

    @Override
    public Widget buildWidgets() {
        mainVp = new VerticalPanel();
        return mainVp;
    }

    @Override
    public String getCaption() {
        return /*BIKGWTConstants.MENU_NAME_DQC + "<br>" + */ BIKGWTConstants.MENU_NAME_CHART;
    }

    @Override
    public String getTreeKindForSelected() {
        return BIKGWTConstants.TREE_KIND_QUALITY_METADATA;
    }

    @Override
    public boolean isEnabled() {
        String nodeKindCode = getNodeKindCode();
//            @EDDBProp4NodeKind({BIKConstants.NODE_KIND_DQM_TEST_SUCCESS, BIKConstants.NODE_KIND_DQM_TEST_NOT_STARTED, BIKConstants.NODE_KIND_DQM_TEST_INACTIVE, BIKConstants.NODE_KIND_DQM_TEST_FAILED, BIKConstants.NODE_KIND_DQC_TEST_SUCCESS, BIKConstants.NODE_KIND_DQC_TEST_FAILED, BIKConstants.NODE_KIND_DQC_TEST_INACTIVE})
        return BIKConstants.NODE_KIND_DQM_TEST_SUCCESS.equals(nodeKindCode) || BIKConstants.NODE_KIND_DQM_TEST_NOT_STARTED.equals(nodeKindCode) || BIKConstants.NODE_KIND_DQM_TEST_INACTIVE.equals(nodeKindCode) || BIKConstants.NODE_KIND_DQM_TEST_FAILED.equals(nodeKindCode) || BIKConstants.NODE_KIND_DQC_TEST_SUCCESS.equals(nodeKindCode) || BIKConstants.NODE_KIND_DQC_TEST_FAILED.equals(nodeKindCode) || BIKConstants.NODE_KIND_DQC_TEST_INACTIVE.equals(nodeKindCode);
    }
}
