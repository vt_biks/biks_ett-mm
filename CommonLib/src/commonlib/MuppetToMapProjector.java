/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonlib;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import simplelib.BaseUtils.Projector;

/**
 *
 * @author wezyr
 */
public class MuppetToMapProjector<T> implements Projector<T, Map<String, Object>> {

    private Collection<String> cols;

    public MuppetToMapProjector(String colNames) {
        this(LameUtils.splitBySep(colNames, ","));
    }

    public MuppetToMapProjector(Collection<String> cols) {
        this.cols = cols;
    }

    public MuppetToMapProjector(Class<T> clazz) {
        this(MuppetMerger.getMuppetProps(clazz, 3));
    }

    public static Map<String, Object> convertToMap(Object val, Collection<String> cols) {
        Map<String, Object> res = new LinkedHashMap<String, Object>();

        for (String colName : cols) {
            res.put(colName, MuppetMerger.getProperty(val, colName));
        }

        return res;
    }

    public static Map<String, Object> convertToMap(Object val, Class superClass) {
        Class c = superClass == null ? val.getClass() : superClass;
        return convertToMap(val, MuppetMerger.getMuppetProps(c, 3));
    }

    public static Map<String, Object> convertToMap(Object val) {
        return convertToMap(val, (Class)null);
    }

    public Map<String, Object> project(T val) {
        return convertToMap(val, cols);
    }
}
