/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.Map;
import simplelib.BaseUtils.Projector;

/**
 *
 * @author wezyr
 */
public class NamedPropsRenameStrategy implements INamedPropsRenameStrategy {

    public Iterable<String> getSrcPropNames(INamedPropsBean src) {
        return src.getPropNames();
    }

    public String getDestPropName(INamedPropsBean src, INamedPropsBeanWriter dst, String srcPropName) {
        return dst.getPropNames().contains(srcPropName) ? srcPropName : null;
    }

    public static void copyProps(INamedPropsBean src, INamedPropsBeanWriter dst,
            INamedPropsRenameStrategy renameStrategy) {

        if (src == null) {
            return;
        }

        Iterable<String> srcPropNames = renameStrategy == null
                ? src.getPropNames() : renameStrategy.getSrcPropNames(src);

        for (String srcPropName : srcPropNames) {
            String dstPropName = renameStrategy == null
                    ? srcPropName : renameStrategy.getDestPropName(src, dst, srcPropName);
            if (dstPropName == null) {
                continue;
            }
            if (!dst.getPropNames().contains(dstPropName)) {
                if (renameStrategy == null) {
                    continue;
                } else {
                    throw new LameRuntimeException("dst has no prop named: " + dstPropName + ", "
                            + "for source prop name: " + srcPropName);
                }
            }
            dst.setPropValue(dstPropName, src.getPropValue(srcPropName));
        }
    }

    public static <T1, T2, K> void extendWithExtraProps(Iterable<T1> items, Map<K, T2> extraProps,
            Projector<T1, K> keyProjector, Projector<T1, INamedPropsBeanWriter> writableProjector,
            Projector<T2, INamedPropsBean> readableProjector,
            INamedPropsRenameStrategy propsRenameStrategy) {
        if (propsRenameStrategy == null) {
            propsRenameStrategy = new NamedPropsRenameStrategy();
        }

        for (T1 item : items) {
            K key = keyProjector.project(item);
            T2 extraPropsBean = extraProps.get(key);
            copyProps(readableProjector.project(extraPropsBean), writableProjector.project(item),
                    propsRenameStrategy);
        }
    }
}
