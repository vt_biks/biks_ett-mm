/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.renderctx;

import pl.foxys.foxymill.tags.CacheTag;

/**
 *
 * @author wezyr
 */
public class StringOutputDevice implements IOutputDevice {

    private StringBuilder sb;

    public StringOutputDevice(StringBuilder sb) {
        this.sb = sb;
    }

    public StringOutputDevice() {
        this(new StringBuilder());
    }

    public void append(Object o) {
        if (o != null) {
            sb.append(o);
        }
    }

//    public int getLength() {
//        return sb.length();
//    }
//
//    public String substring(int start, int end) {
//        return sb.substring(start, end);
//    }
    public void appendCacheTag(CacheTag ct, IRenderCtx renderCtx) {
        ct.renderSubTags(renderCtx);
    }

    public String doneRendering() {
        String res = sb.toString();
        sb.setLength(0);
        return res;
    }
}
