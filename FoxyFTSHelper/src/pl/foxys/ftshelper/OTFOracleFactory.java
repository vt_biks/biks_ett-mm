/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.foxys.ftshelper;

import simplelib.NamedGodsFactory;
import pl.trzy0.foxy.commons.IOtherTermFormsOracle;
import simplelib.IObjectCreator;

/**
 *
 * @author wezyr
 */
public class OTFOracleFactory {
    private static final String defKeyForNgodsf = "_default_";
    
    private NamedGodsFactory<String, IOtherTermFormsOracle> ngodsf =
            new NamedGodsFactory<String, IOtherTermFormsOracle>(defKeyForNgodsf);

    public OTFOracleFactory() {
        ngodsf.addCreator("pl", new IObjectCreator<IOtherTermFormsOracle>() {

            public IOtherTermFormsOracle create() {
                return OtherTermFormsOraclePl.getInstance();
            }
        });

        ngodsf.addCreator(defKeyForNgodsf, new IObjectCreator<IOtherTermFormsOracle>() {

            public IOtherTermFormsOracle create() {
                return OtherTermFormsOracleBase.getInstance();
            }
        });
    }

    public static synchronized IOtherTermFormsOracle getOracle(String langName) {
        return getInstance().ngodsf.get(langName);
    }

    private static class SingletonHolder {

        private static final OTFOracleFactory INSTANCE = new OTFOracleFactory();
    }

    public static OTFOracleFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
