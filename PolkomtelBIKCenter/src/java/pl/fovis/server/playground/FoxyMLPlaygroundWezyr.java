/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.playground;

import simplelib.BaseUtils;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public class FoxyMLPlaygroundWezyr extends FoxyMLPlaygroundBase {

    public static void main(String[] args) {
        startPlaying();
    }

    @Override
    protected void letsPlay() {
        dumpSql("getBikFilteredNodes", "annapu" /* I18N:  */, BaseUtils.paramsAsSet(20, 30), false);
        dumpSql("z" /* I18N:  */);
        dumpSql("aqq" /* I18N:  */);
    }
}
