/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.erwin;

import java.util.HashMap;

/**
 *
 * @author pgajda
 */
public class ErwinRelationship extends ErwinXMLElementString{
    public ErwinRelationship() {
        propertiesMap = new HashMap<String, String>() {
            {
                put("Name", null);
                put("Long_Id", null);
                put("Type", null);
                put("Key_Group_Ref", null);
                put("Parent_Entity_Ref", null);
                put("Child_Entity_Ref", null);
                put("Comment", null);
            }
        };
    }    
}
