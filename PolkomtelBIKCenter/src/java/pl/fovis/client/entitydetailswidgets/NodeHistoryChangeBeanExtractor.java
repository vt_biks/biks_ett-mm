/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import java.util.HashMap;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.common.NodeHistoryChangeBean;
import pl.fovis.common.i18npoc.I18n;
import simplelib.BaseUtils;
import simplelib.SimpleDateUtils;

/**
 *
 * @author ctran
 */
public class NodeHistoryChangeBeanExtractor extends NodeAwareBeanExtractorBase<NodeHistoryChangeBean> {

    public NodeHistoryChangeBeanExtractor(boolean isAddJoinedButtonEnabled) {
        super(isAddJoinedButtonEnabled);
    }

    @Override
    public boolean hasIcon() {
        return false;
    }

    @Override
    protected String getNodeKindCode(NodeHistoryChangeBean b) {
        return null;
    }

    @Override
    protected String getName(NodeHistoryChangeBean b) {
        return null;
    }

    @Override
    public String getTreeCode(NodeHistoryChangeBean b) {
        return null;
    }

    @Override
    public Integer getNodeId(NodeHistoryChangeBean b) {
        return null;
    }

    @Override
    public String getBranchIds(NodeHistoryChangeBean b) {
        return null;
    }

    @Override
    public int getAddColCnt() {
        return 6;
    }

    @Override
    public int getActionCnt() {
        return 0;
    }

    @Override
    public boolean createGridColumnNames(FlexTable gp) {
        gp.setStyleName("gridJoinedObj");
        int colId = 0;
        gp.setWidget(0, colId, new HTML(I18n.data.get()));
        gp.getColumnFormatter().setWidth(colId++, "10%");

        gp.setWidget(0, colId, new HTML(I18n.uzytkownik.get()));
        gp.getColumnFormatter().setWidth(colId++, "10%");

        gp.setWidget(0, colId, new HTML(I18n.obiekt.get()));
        gp.getColumnFormatter().setWidth(colId++, "10%");

        gp.setWidget(0, colId, new HTML(I18n.atrybut.get()));
        gp.getColumnFormatter().setWidth(colId++, "10%");

        gp.setWidget(0, colId, new HTML(I18n.staraWartosc.get()));
        gp.getColumnFormatter().setWidth(colId++, "30%");

        gp.setWidget(0, colId, new HTML(I18n.nowaWartosc.get()));
        gp.getRowFormatter().setStyleName(0, "RelatedObj-oneObj-dqc");
        return true;
    }

    @Override
    public Object getAddColVal(NodeHistoryChangeBean b, int addColIdx) {
        Map<Integer, Object> m = new HashMap<Integer, Object>();
        int colId = 0;
        m.put(colId++, new HTML(SimpleDateUtils.toCanonicalString(b.dateAdded)));
        m.put(colId++, new HTML(b.changeSource));
        m.put(colId++, new HTML(b.nodeName));
        m.put(colId++, new HTML(trimToDot(b.attrName)));
//        '&amp;','&'),'&gt;','>' ), '&lt;','<'
        String oldValue = BIKClientSingletons.htmlEntitiesDecode(b.oldValue);
        String newValue = BIKClientSingletons.htmlEntitiesDecode(b.newValue);
        m.put(colId++, BaseUtils.isStrEmpty(oldValue) ? "" : BIKClientSingletons.createAttributeView(b.typeAttr, oldValue, false));
        m.put(colId++, BaseUtils.isStrEmpty(newValue) ? "" : BIKClientSingletons.createAttributeView(b.typeAttr, newValue, false));
        return m.get(addColIdx);
    }

    protected String trimToDot(String attrName) {
        return attrName.indexOf('.') < 0 ? attrName : attrName.split("\\.")[1];
    }
}
