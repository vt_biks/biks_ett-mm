﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- Poprawki do szablonów Confluensowych
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_check_version '1.6.y2.1';
go

declare @treeOfTreesId int = dbo.fn_tree_id_by_code('TreeOfTrees');
if exists(select 1 from bik_node where tree_id = @treeOfTreesId and obj_id = '$AtlassianConfluence' and is_deleted = 0)
begin
	update bik_node
	set obj_id = '@Confluence', name = 'Confluence'
	where tree_id = @treeOfTreesId
	and obj_id = '$AtlassianConfluence'
	and is_deleted = 0
	
	exec sp_add_menu_node 'confluence', '@', '$Confluence'
	exec sp_node_init_branch_id @treeOfTreesId, null
	
	update bik_tree set code = 'Confluence' where code = 'AtlassianConfluence'
end;

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_mssql_foreign_key') and name = 'fk_owner')
begin
	alter table bik_mssql_foreign_key add fk_owner sysname null;
	alter table bik_mssql_foreign_key add pk_owner sysname null;
	alter table bik_mssql_dependency add ref_name sysname null;
	alter table bik_mssql_dependency add ref_owner sysname null;
end;
go

exec sp_update_version '1.6.y2.1', '1.6.y2.2';
go