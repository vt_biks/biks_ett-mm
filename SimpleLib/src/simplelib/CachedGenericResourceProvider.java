/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simplelib;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author wezyr
 */
public class CachedGenericResourceProvider<T> implements IGenericResourceProvider<T> {

    private final static long TIMESTAMP_CHECK_INTERVAL_MILLIS = 1000;
    protected IGenericResourceProvider<T> resProvider;
    private Map<String, Long> timeStamps = new HashMap<String, Long>();
    private Map<String, T> resMap = new HashMap<String, T>();
    private Map<String, Long> lastTimeStampCheckMap = new HashMap<String, Long>();

    public CachedGenericResourceProvider(IGenericResourceProvider<T> resProvider) {
        this.resProvider = resProvider;
    }

    protected String resourceNameToInnerName(String resourceName) {
        return resourceName;
    }

    public T gainResource(String resourceName) {
        String innerResName = resourceNameToInnerName(resourceName);

        Long lastTimeStampCheck = lastTimeStampCheckMap.get(resourceName);
        Long currTimeStamp = null;
        long currSysMillis = System.currentTimeMillis();

        if (lastTimeStampCheck == null || lastTimeStampCheck + TIMESTAMP_CHECK_INTERVAL_MILLIS
                < currSysMillis) {
            currTimeStamp = resProvider.getLastModifiedOfResource(innerResName);
            lastTimeStampCheckMap.put(resourceName, currSysMillis);
            Long lastTimeStamp = timeStamps.get(resourceName);

            if (lastTimeStamp != null && lastTimeStamp == currTimeStamp) {
                currTimeStamp = null;
            }
        }

        T res;

        if (currTimeStamp != null) {
            res = resProvider.gainResource(innerResName);
            timeStamps.put(resourceName, currTimeStamp);
            resMap.put(resourceName, res);
        } else {
            res = resMap.get(resourceName);
        }
        return res;
    }

    public long getLastModifiedOfResource(String resourceName) {
        return resProvider.getLastModifiedOfResource(resourceNameToInnerName(resourceName));
    }
}
