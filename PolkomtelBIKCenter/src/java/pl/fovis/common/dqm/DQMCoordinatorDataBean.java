/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author ctran
 */
public class DQMCoordinatorDataBean implements Serializable {

    public Integer errorRegisterNodeId;
    public List<DQMEvaluationBean> reportData;
    public int numerOpenedDataErrorIssues;
    public int numerAllDataErrorIssues;
}
