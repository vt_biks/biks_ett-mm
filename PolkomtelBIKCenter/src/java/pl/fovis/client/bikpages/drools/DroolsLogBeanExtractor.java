/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.bikpages.drools;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import java.util.HashMap;
import java.util.Map;
import pl.fovis.client.entitydetailswidgets.NodeAwareBeanExtractorBase;
import pl.fovis.common.DroolsLogBean;
import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author ctran
 */
public class DroolsLogBeanExtractor extends NodeAwareBeanExtractorBase<DroolsLogBean> {

    private final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private final DateTimeFormat dtf = DateTimeFormat.getFormat(DATE_FORMAT);

    public DroolsLogBeanExtractor() {
        super(false);
    }

    @Override
    protected String getNodeKindCode(DroolsLogBean b) {
        return null;
    }

    @Override
    protected String getName(DroolsLogBean b) {
        return null;
    }

    @Override
    public String getTreeCode(DroolsLogBean b) {
        return null;
    }

    @Override
    public Integer getNodeId(DroolsLogBean b) {
        return null;
    }

    @Override
    public String getBranchIds(DroolsLogBean b) {
        return null;
    }

    @Override
    public int getAddColCnt() {
        return 2;
    }

    @Override
    public boolean hasIcon() {
        return false;
    }

    @Override
    public Object getAddColVal(DroolsLogBean b, int addColIdx) {
        Map<Integer, Object> m = new HashMap<Integer, Object>();
//        HTML jobCuid = new HTML(b.jobCuid);
//        jobCuid.setStyleName("Notes" /* i18n: no:css-class */ + "-labBody");
//        m.put(0, jobCuid);
        m.put(0, new Label(dtf.format(b.startTime)));
        Label status = new Label(b.msg);
        m.put(1, status);

        return m.get(addColIdx);
    }

    @Override
    public int getActionCnt() {
        return 0;
    }

//    public static void setTitleForServerName(Label widget, String serverName, String serverInstance, String fileName, String databaseName) {
//        if (serverName.equals("BIKS")) {
//            // NO OP
//        } else if (serverName.equals("FTP")) {
//            widget.setTitle(I18n.prefiksPliku.get() + ": " + fileName);
//        } else {
//            widget.setTitle((!BaseUtils.isStrEmptyOrWhiteSpace(serverInstance) ? I18n.instance.get() + " / SID: " + serverInstance + " \n" : "") + I18n.bazaDanych.get() + ": " + databaseName);
//        }
//    }
    @Override
    public boolean createGridColumnNames(FlexTable gp) {
        gp.setStyleName("gridJoinedObj");
//        gp.setWidget(0, 0, new HTML(I18n.cuidProcesu.get()));
        gp.setWidget(0, 0, new HTML(I18n.dataRozpoczecia.get() + "&nbsp;&nbsp;&nbsp;&nbsp;"));
        gp.setWidget(0, 1, new HTML(I18n.status.get() + "&nbsp;&nbsp;&nbsp;&nbsp;"));
//        gp.getColumnFormatter().setWidth(0, "20%");
        gp.getColumnFormatter().setWidth(0, "10%");
        gp.getRowFormatter().setStyleName(0, "RelatedObj-oneObj-dqc");
        return true;
    }
}
