﻿exec sp_check_version '1.0.98.4';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------
create table bik_service_request (
    id int not null IDENTITY(1,1) primary key,
    method_name varchar(1000) not null,
    parameters varchar(max) not null,
    start_time datetime not null,
    duration_millis int not null default -1,
    ip_addr varchar(1000) not null,
    session_id varchar(1000) not null,
    exception_msg varchar(max) null,
    extra_info varchar(max) null
);
go

insert into bik_attr_hint (name, hint)
values ('Podpowiedź do wyszukiwarki', '<b>Frazy w wyszukiwarce</b><br />Wyszukiwarka obsługuje wyszukiwanie słów oddzielonych spacjami, dodatkowo można szukać przedrostków słów, np. <b>annapu*</b>. Szczególny przypadek to wyszukiwanie wszystkich obiektów danego typu <b>*</b> - sama gwiazdka.<br />Ze względu na ilość danych obsługiwaną przez wyszukiwarkę, nalezy unikać szukania bardzo krótkich przedrostków lub bardzo częstych słów (np. samo "a").<br />')
go

exec sp_set_app_prop 'serviceRequestLogLevel', '0'
go
-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------
exec sp_update_version '1.0.98.4', '1.0.99';
go
