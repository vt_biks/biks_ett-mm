/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.datamodel;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import static pl.fovis.client.datamodel.DataProcessRef.STATUS_A_COLOR;
import static pl.fovis.client.datamodel.DataProcessRef.STATUS_B_COLOR;
import static pl.fovis.client.datamodel.DataProcessRef.STATUS_C_COLOR;
import static pl.fovis.client.datamodel.DataProcessRef.STATUS_DEFAULT_COLOR;
import static pl.fovis.client.datamodel.DataProcessRef.STATUS_D_COLOR;
import static pl.fovis.client.datamodel.DataProcessRef.STATUS_E_COLOR;
import static pl.fovis.client.datamodel.DataProcessRef.STATUS_F_COLOR;
import static pl.fovis.client.datamodel.DataProcessRef.STATUS_I_COLOR;
import static pl.fovis.client.datamodel.DataProcessRef.STATUS_J_COLOR;
import static pl.fovis.client.datamodel.DataProcessRef.STATUS_R_COLOR;
import static pl.fovis.client.datamodel.DataProcessRef.STATUS_W_COLOR;
import static pl.fovis.client.datamodel.DataProcessRef.STATUS_X_COLOR;
import static pl.fovis.client.datamodel.DataProcessRef.STATUS_Z_COLOR;
import static pl.fovis.client.datamodel.DataProcessRef.statusColorMap;
import pl.fovis.common.EntityDetailsDataBean;
import pl.fovis.common.ErwinDataModelProcessObjectsRel;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.GWTUtils;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class DataSummary {

    protected Map<String, Integer> tableName2NodeId;
    protected EntityDetailsDataBean eddb;
    protected HorizontalPanel controlPanel;
    protected String activeSubjectArea;
    protected HorizontalPanel panel;

    public Widget buildWidgets() {
        if (panel != null) {
            panel.clear();
        } else {
            panel = new HorizontalPanel();
        }

        return panel;
    }

    public void setEntityDetailsDataBean(EntityDetailsDataBean eddb) {
        this.eddb = eddb;
    }

    public Widget buildContlolWidgets() {
        if (controlPanel == null) {
            controlPanel = new HorizontalPanel();
        } else {
            controlPanel.clear();
        }

        Label label = new Label(I18n.obszarDanych.get() + " : ");
        controlPanel.add(label);
        controlPanel.setCellWidth(label, "110px");
        controlPanel.setCellHorizontalAlignment(label, HasHorizontalAlignment.ALIGN_RIGHT);
        controlPanel.add(getSubjectAreaListBox());

        controlPanel.setWidth("100%");

        return controlPanel;

    }

    protected Widget getSubjectAreaListBox() {
        final ListBox listSubjectArea = new ListBox();
        if (eddb == null) {
            return listSubjectArea;
        }

        if (eddb.erwinDataModelProcessAreas == null || eddb.erwinDataModelProcessAreas.isEmpty()) {
            listSubjectArea.addItem("<Model>", "000000");
        } else {
            activeSubjectArea = eddb.erwinDataModelProcessAreas.get(0);
            for (Iterator<String> iterStr = eddb.erwinDataModelProcessAreas.iterator(); iterStr.hasNext();) {
                listSubjectArea.addItem(iterStr.next());
            }
        }
        listSubjectArea.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                activeSubjectArea = listSubjectArea.getValue(listSubjectArea.getSelectedIndex());
                displayData();
            }
        });

        return listSubjectArea;
    }

    public void displayData() {
        downloadAllData();
    }

    public void downloadAllData() {
        BIKClientSingletons.showInfo(I18n.zadanieWToku.get() /*
         * i18n:
         */);
        panel.clear();
        BIKClientSingletons.getLisaService().getDataModelProcessObjectsRel(eddb.node.id, activeSubjectArea, true,
                new StandardAsyncCallback<List<ErwinDataModelProcessObjectsRel>>("error in" /*
                         * I18N: no:err-fail-in
                         */ + " getDataModelProcessObjectsRel()") {
            @Override
            public void onSuccess(List<ErwinDataModelProcessObjectsRel> result) {
                eddb.erwinDataModelProcessObjectsRel = result;
                refreshDisplay();
                BIKClientSingletons.showInfo(I18n.pobieranieDanychProcesowErwin.get());
            }

        });
    }

    private void refreshDisplay() {
        FlowPanel ft = new FlowPanel();
//        ft.setCellPadding(0);
//        ft.setCellSpacing(0);
        Map<String, List<ErwinDataModelProcessObjectsRel>> processesInTable = new HashMap<String, List<ErwinDataModelProcessObjectsRel>>();
        tableName2NodeId = new HashMap<String, Integer>();
        for (ErwinDataModelProcessObjectsRel process : eddb.erwinDataModelProcessObjectsRel) {
            if (!processesInTable.containsKey(process.objName)) {
                processesInTable.put(process.objName, new ArrayList<ErwinDataModelProcessObjectsRel>());
            }
            tableName2NodeId.put(process.objName, process.nodeId);
            processesInTable.get(process.objName).add(process);
        }
        int tableCnt = 0;
        for (String tableName : processesInTable.keySet()) {
            ft.add(/*
                     * tableCnt / N_TABLE_IN_LINE, tableCnt % N_TABLE_IN_LINE,
                     */buildWidget4Table(tableName, processesInTable.get(tableName)));
            tableCnt++;
        }
//        int maxWidth = -1;
//        for (int i = 0; i < tableCnt; i++) {
//            Widget cell = ft.getWidget(i / N_TABLE_IN_LINE, i % N_TABLE_IN_LINE);
//            int offsetWidth = cell.getOffsetWidth();
//            if (maxWidth < offsetWidth) {
//                maxWidth = offsetWidth;
//            }
//        }
//        for (int i = 0; i < tableCnt; i++) {
//            Widget cell = ft.getWidget(i / N_TABLE_IN_LINE, i % N_TABLE_IN_LINE);
//            cell.setWidth(maxWidth + "px");
//        }
        //        Window.alert("calculateBackgoundColorBaseOnProcessState=" + calculateBackgoundColorBaseOnProcessState());
        panel.add(ft);
    }

    private String calculateBackgoundColorAndBuildWidgetBaseOnProcessState(HorizontalPanel widget, List<ErwinDataModelProcessObjectsRel> processes) {
        int redCnt = 0;
        int greenCnt = 0;
        int yellowCnt = 0;
        int blueCnt = 0;
        int purpleCnt = 0;
        for (ErwinDataModelProcessObjectsRel process : processes) {
            String backgroundColor = null;
            if (process.procStatus != null && statusColorMap.containsKey(process.procStatus)) {
                backgroundColor = statusColorMap.get(process.procStatus);
            }

            if (backgroundColor == null) {
                backgroundColor = STATUS_DEFAULT_COLOR; //szary
            }
//            Window.alert(process.objName + " " + process.procStatus);
//            System.out.println(backgroundColor);

            if (STATUS_R_COLOR.equals(backgroundColor)
                    || STATUS_E_COLOR.equals(backgroundColor)
                    || STATUS_I_COLOR.equals(backgroundColor)
                    || STATUS_J_COLOR.equals(backgroundColor)) {
                redCnt++;//czerwony
            }
            if (STATUS_Z_COLOR.equals(backgroundColor)
                    || STATUS_D_COLOR.equals(backgroundColor)
                    || STATUS_B_COLOR.equals(backgroundColor)) {
                greenCnt++;
            }
            if (STATUS_A_COLOR.equals(backgroundColor)
                    || STATUS_C_COLOR.equals(backgroundColor)) {
                yellowCnt++;
            }
            if (STATUS_W_COLOR.equals(backgroundColor)) {
                blueCnt++;
            }
            if (STATUS_X_COLOR.equals(backgroundColor)
                    || STATUS_F_COLOR.equals(backgroundColor)) {
                purpleCnt++;
            }
        }
        if (redCnt > 0) {
            widget.add(createOneProcessTypeSummary(redCnt, STATUS_R_COLOR, "R", "E", "I", "J"));
        }
        if (greenCnt > 0) {
            widget.add(createOneProcessTypeSummary(greenCnt, STATUS_Z_COLOR, "Z", "D", "B"));
        }
        if (yellowCnt > 0) {
            widget.add(createOneProcessTypeSummary(yellowCnt, STATUS_A_COLOR, "A", "C"));
        }
        if (blueCnt > 0) {
            widget.add(createOneProcessTypeSummary(blueCnt, STATUS_W_COLOR, "W"));
        }
        if (purpleCnt > 0) {
            widget.add(createOneProcessTypeSummary(purpleCnt, STATUS_X_COLOR, "X", "F"));
        }
        if (redCnt > 0) {
            return STATUS_R_COLOR;
        }
        if (yellowCnt > 0) {
            return STATUS_A_COLOR;
        }
        if (blueCnt > 0) {
            return STATUS_W_COLOR;
        }
        if (purpleCnt > 0) {
            return STATUS_X_COLOR;
        }
        if (greenCnt == processes.size()) {
            return STATUS_Z_COLOR;
        }
        return STATUS_DEFAULT_COLOR;
    }

    private IsWidget createOneProcessTypeSummary(int processCnt, String backgroundColor, String... statusNames) {
        StringBuilder hint = new StringBuilder(I18n.status.get()).append(" ");
        for (String name : statusNames) {
            hint.append(name).append(",");
        }
        if (statusNames.length > 0) {
            hint.deleteCharAt(hint.length() - 1);
        }
        Label cell = new Label("" + processCnt);
        cell.setTitle(hint.toString());
        SimplePanel sp = new SimplePanel(cell);
        sp.setStyleName("erwinProcessTypeCell");
        GWTUtils.setStyleAttribute(sp.getElement(), "backgroundColor", backgroundColor);
        return sp;
    }

    private Widget buildWidget4Table(final String tableName, List<ErwinDataModelProcessObjectsRel> processes) {
        VerticalPanel child = new VerticalPanel();
        child.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        child.setWidth("280px");
        child.setHeight("90px");
        HTML tableNameWidget = new HTML(BaseUtils.encodeForHTMLTag(tableName, true, 10));
        tableNameWidget.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
//                                BIKClientSingletons.getTabSelector().submitNewTabSelection(eddb.node.treeCode, Process, true);
                BIKClientSingletons.getTabSelector().submitNewTabSelection(eddb.node.treeCode, tableName2NodeId.get(tableName), true);
            }

        });
        tableNameWidget.setStyleName("btnPointer");
        child.add(tableNameWidget);
        child.setStyleName("BizDef-keyowrdsMain superBorderForChuck testWidget");
        HorizontalPanel hp = new HorizontalPanel();
        GWTUtils.setStyleAttribute(child.getElement(), "backgroundColor", calculateBackgoundColorAndBuildWidgetBaseOnProcessState(hp, processes));
        child.add(hp);
        return child;
    }
}
