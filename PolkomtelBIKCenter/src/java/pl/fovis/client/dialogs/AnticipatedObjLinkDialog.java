/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import pl.fovis.client.HistoryManager;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.dialogs.BaseActionOrCancelDialog;

/**
 *
 * @author pmielanczuk
 */
public class AnticipatedObjLinkDialog extends BaseActionOrCancelDialog {

    protected TextBox tbObjName = new TextBox();
    protected TextBox tbGeneratedLink = new TextBox();
    protected CheckBox cbRestrictLocation = new CheckBox();
    protected Label lblCopyLinkToClipboard = new Label(I18n.generatedLinkForAnticipatedObj.get());
    protected String treeCode;
    protected String optNodeNamePath;
    protected Integer optNodeId;

    public AnticipatedObjLinkDialog(String treeCode, String optNodeNamePath, Integer optNodeId) {
        this.treeCode = treeCode;
        this.optNodeNamePath = optNodeNamePath;
        this.optNodeId = optNodeId;
    }

    @Override
    protected String getDialogCaption() {
        return I18n.anticipatedObjLinkAct.get();
    }

    @Override
    protected String getActionButtonCaption() {
        return null;
    }

    @Override
    protected void buildMainWidgets() {
        FlexTable g = new FlexTable();
        int rowNum = 0;
        g.setCellSpacing(6);
        final FlexCellFormatter flexCellFormatter = g.getFlexCellFormatter();

        g.setText(rowNum, 0, I18n.enterAnticipatedObjName.get());
        g.setWidget(rowNum, 1, tbObjName);
        flexCellFormatter.setHorizontalAlignment(rowNum, 1, HasHorizontalAlignment.ALIGN_RIGHT);
        tbObjName.setWidth("280px");
        rowNum++;

        if (optNodeNamePath != null) {
            cbRestrictLocation.setText(I18n.restrictLocationToSubobjects.get()
                    + " [" + optNodeNamePath + "]");
            g.setWidget(rowNum, 0, cbRestrictLocation);
            flexCellFormatter.setColSpan(rowNum, 0, 2);
//            g.setWidget(rowNum, 1, cbRestrictLocation);
//            rowNum++;
//            g.getFlexCellFormatter().setColSpan(rowNum, 0, 1);
//            g.setText(rowNum, 0, optNodeNamePath);
            rowNum++;
        }

        g.setWidget(rowNum, 0, lblCopyLinkToClipboard);
        flexCellFormatter.setColSpan(rowNum, 0, 2);
        rowNum++;
        flexCellFormatter.setColSpan(rowNum, 0, 2);
        g.setWidget(rowNum, 0, tbGeneratedLink);
        tbGeneratedLink.setWidth("600px");

        main.add(g);

        tbObjName.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                regenerateLink();
            }
        });

        tbObjName.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                regenerateLink();
            }
        });

        if (optNodeNamePath != null) {
            cbRestrictLocation.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    regenerateLink();
                }
            });

            cbRestrictLocation.setValue(true, false);
        }

        tbGeneratedLink.setReadOnly(true);
        tbGeneratedLink.addFocusHandler(new FocusHandler() {
            @Override
            public void onFocus(FocusEvent event) {
                regenerateLinkAndFocus();
            }
        });

        lblCopyLinkToClipboard.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                regenerateLinkAndFocus();
            }
        });

        regenerateLink();
    }

    protected void regenerateLinkAndFocus() {
        regenerateLink();
        tbGeneratedLink.selectAll();
    }

    protected void regenerateLink() {
        String objName = tbObjName.getText().trim();

        boolean isNameEmpty = objName.isEmpty();
        tbGeneratedLink.setEnabled(!isNameEmpty);

        String idPart
                = (optNodeId != null && cbRestrictLocation.getValue() ? optNodeId : "")
                + "|";

        String url = HistoryManager.getFullUrl(treeCode, idPart
                + //                URL.encodeQueryString(objName)
                objName
        );

        tbGeneratedLink.setText(url);
    }

    @Override
    protected void doAction() {
        // no-op - to się nie ma prawa wywołać
    }
}
