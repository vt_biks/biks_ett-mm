/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.foxys.foxymill.tags;

import java.util.Map;
import pl.foxys.foxymill.RenderEnvironment;
import pl.foxys.foxymill.renderctx.IRenderCtx;

/**
 *
 * @author wezyr
 */
public class CommentTag extends TagBase {

    @Override
    public void prepare(RenderEnvironment renderEnv) {
        // no-op
    }

    @Override
    public void internalRender(IRenderCtx renderCtx) {
        // no-op
    }

    @Override
    public void fixUp(Map<String, Object> globals) {
        // no-op
    }
}
