/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.treeandlist;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import simplelib.IParametrizedContinuation;

/**
 *
 * @author bfechner
 */
public interface IFoxyTreeGrid<IDT> extends IFoxyTreeOrListGrid<IDT>, ITreeGridForAction<IDT> {

    public void setFirstColStyleName(String styleName);

    public void defineTreeFields(String idFieldName, String parentIdFieldName, String nameFieldName,
            String prettyKindFieldName, String nodeIconFieldName, String additionalNodeIconFieldName);

    // ww: chyba zbędne, czystka
    //public void expandNodeById(IDT... ids);
//    public boolean wasSelectedBefor();
//    public int getNodeIdAfterDelete(int old); // zbędne
    // musi wywołać handler wybierania rekordu, niezależnie czy aktualnie
    // wybrany jest inny rekord czy już ten sam
    public void selectRecord(IDT id, boolean expand);

    public Set<IDT> getExpandedRowIds();

    public void expandNodesByIds(Collection<IDT> ids, IParametrizedContinuation<Integer> optExpandInProgressCallback);

    public void expandNodesByIds(final Collection<IDT> ids, final IParametrizedContinuation<Integer> optExpandInProgressCallback, final boolean allowDataLoading);

    public boolean expandNodesByIdsWithDataLoading(final Collection<IDT> ids, IDT optIdToSelect, final IParametrizedContinuation<IDT> optCont);

    public void setNodeName(IDT id, String name);

    public void addNode(Map<String, Object> row, Integer optIdxInSiblings);

    public void deleteNode(IDT nodeId);

    public void markAsCutNode(IDT nodeId);

    public void markAsUncutNode(IDT nodeId);

    public void pasteNode(IDT nodeId, IDT srcNodeId, boolean isCut);

    // adds many to single parent, marks parent as leaf if rows == null or rows.isEmpty
    public void addChildNodes(Map<String, Object> parent, Collection<Map<String, Object>> rows);

    //public void setLazyRowLoader(ILazyRowLoader<Map<String, Object>> lazyLoader);
    public void setData(//Collection<Map<String, Object>> rows,
            ITreeRowStorage<IDT, Map<String, Object>> storage,
            ILazyRowLoaderEx<Map<String, Object>, IDT> optLazyLoader);

    public boolean hasUnloadedChildren(IDT nodeId);

    public boolean isNodeExpanded(IDT nodeId);

    public void addHeaderRow(Object... headerRowVals);

    public void addFooterRow(Object... footerRowVals);

    public ITreeRowStorage<IDT, Map<String, Object>> getStorage();
}
