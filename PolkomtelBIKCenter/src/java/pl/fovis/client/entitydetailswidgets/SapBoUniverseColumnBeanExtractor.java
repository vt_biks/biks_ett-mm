/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.entitydetailswidgets;

import pl.fovis.common.SapBoUniverseColumnBean;
import simplelib.BaseUtils;

/**
 *
 * @author beata
 */
public class SapBoUniverseColumnBeanExtractor extends NodeAwareBeanExtractorBase<SapBoUniverseColumnBean> {

    public SapBoUniverseColumnBeanExtractor(boolean isAddJoinedButtonEnabled) {
        super(isAddJoinedButtonEnabled);
    }

    public Integer getNodeId(SapBoUniverseColumnBean b) {
        return null;
    }

    public String getTreeCode(SapBoUniverseColumnBean b) {
        return null;
    }

    protected String getName(SapBoUniverseColumnBean b) {
        return null;
    }

    protected String getNodeKindCode(SapBoUniverseColumnBean b) {
        return null;
    }

    public String getBranchIds(SapBoUniverseColumnBean b) {
        return null;
    }

    @Override
    public String getIconUrl(SapBoUniverseColumnBean b) {
        return "images/column.gif" /* I18N: no */;
    }

    @Override
    public int getAddColCnt() {
        return 2;
    }

    @Override
    public Object getAddColVal(SapBoUniverseColumnBean b, int addColIdx) {
        return addColIdx == 0 ? b.name : BaseUtils.dropOptionalSuffix((BaseUtils.dropOptionalPrefix(b.type, "ds" /* i18n: no */)), "Column" /* I18N: no */);
    }

    @Override
    public int getActionCnt() {
        return 0;
    }
}
