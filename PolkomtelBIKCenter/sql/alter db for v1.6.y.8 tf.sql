﻿------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
-- Konektor do MS SQL - pobieranie zależnośći
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
exec sp_check_version '1.6.y.8';
go

if not exists(select * from sys.objects where object_id = object_id(N'aaa_mssql_dependency') and type in (N'U'))
begin
	create table aaa_mssql_dependency (
		id int not null identity(1,1) primary key,
		server varchar(500) null,
		database_name sysname not null,
		ref_name sysname not null,
		ref_owner sysname not null,
		dep_name sysname not null,
		dep_owner sysname not null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_mssql_dependency') and type in (N'U'))
begin
	create table bik_mssql_dependency (
		id int not null identity(1,1) primary key,
		server varchar(500) not null,
		ref_node_id int not null references bik_node(id),
		dep_node_id int not null references bik_node(id),
		dep_name sysname not null,
		dep_owner sysname not null
	);
end;
go

if not exists (select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[bik_mssql_dependency]') and name = N'idx_bik_mssql_dependency_ref_node_id')
begin
	create index idx_bik_mssql_dependency_ref_node_id on [dbo].[bik_mssql_dependency](ref_node_id);
end;
go

exec sp_update_version '1.6.y.8', '1.6.y.9';
go