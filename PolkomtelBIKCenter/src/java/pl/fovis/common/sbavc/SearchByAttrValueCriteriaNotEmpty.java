/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.sbavc;

import pl.fovis.common.i18npoc.I18n;

/**
 *
 * @author pmielanczuk
 */
public class SearchByAttrValueCriteriaNotEmpty extends SearchByAttrValueCriteriaBase {

    public SearchByAttrValueCriteriaNotEmpty() {
    }

    public SearchByAttrValueCriteriaNotEmpty(int attrId, String attrName, boolean isNegation) {
        super(attrId, attrName, isNegation);
    }

    @Override
    public String getCriteriaMessage() {
        return "<b>" + attrName + "</b> " + (isNegation ? I18n.nie3.get() + " " : "") + I18n.noValueAttribute.get();
    }
}
