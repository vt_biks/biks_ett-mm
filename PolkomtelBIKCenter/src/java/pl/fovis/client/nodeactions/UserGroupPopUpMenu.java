/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.nodeactions;

import java.util.ArrayList;
import java.util.List;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.dialogs.AddSystemUserGroupDialog;
import pl.fovis.client.dialogs.AddUsersToGroupDialog;
import pl.fovis.common.SystemUserBean;
import pl.fovis.common.SystemUserGroupBean;
import pl.fovis.common.i18npoc.I18n;
import pl.fovis.foxygwtcommons.StandardAsyncCallback;
import pl.fovis.foxygwtcommons.dialogs.ConfirmDialog;
import pl.fovis.foxygwtcommons.popupmenu.PopupMenuDisplayer;
import simplelib.IContinuation;
import simplelib.IParametrizedContinuation;
import simplelib.Pair;

/**
 *
 * @author bfechner
 */
public class UserGroupPopUpMenu {

    protected PopupMenuDisplayer popMenu = new PopupMenuDisplayer();
    /* protected CTX contextObject;*/
    protected int clientX;
    protected int clientY;
    protected SystemUserGroupBean sub;
    protected IContinuation optDoAfterDataFetched;

    public UserGroupPopUpMenu(/*CTX contextObject,*/SystemUserGroupBean sub, int clientX, int clientY, IContinuation optDoAfterDataFetched) {
//        this.contextObject = contextObject;
        this.sub = sub;
        this.clientX = clientX;
        this.clientY = clientY;
        this.optDoAfterDataFetched = optDoAfterDataFetched;
    }

    public void createAndShowPopup() {
        addAction();
        popMenu.showAtPosition(clientX, clientY);
    }

//    @Override
    public void addAction() {
        popMenu.addMenuItem(I18n.zmienNazwe.get(), renameUserGroup());
        popMenu.addMenuItem(I18n.usun.get(), deleteUserGroup());
        popMenu.addMenuItem(I18n.dodajGrupe.get(), addUserGroup());
        popMenu.addMenuItem(I18n.kopiujUprawnienia.get(), copyPermissionFromUser());
    }

    public void addSeparator() {
        popMenu.addMenuSeparator();
    }

    protected IContinuation renameUserGroup() {
        return new IContinuation() {

            @Override
            public void doIt() {
                new AddSystemUserGroupDialog().buildAndShowDialog(sub.name, I18n.zmienNazwe.get(), new IParametrizedContinuation<String>() {

                    @Override
                    public void doIt(String param) {
                        sub.name = param;

                        BIKClientSingletons.getService().renameUserGroup(sub, new StandardAsyncCallback<Integer>() {

                            @Override
                            public void onSuccess(Integer result) {
                                BIKClientSingletons.showInfo(result == 1 ? I18n.nazwaZostalaZmieniona.get() : I18n.grupaIstnieje.get() /* I18N:  */);
                                optDoAfterDataFetched.doIt();
                            }
                        });

                    }
                });
            }
        };
    }

    protected IContinuation deleteUserGroup() {
        return new IContinuation() {

            @Override
            public void doIt() {

                StringBuilder sb = new StringBuilder(I18n.czyNaPewnoUsunac.get());
                sb.append(" ").append(sub.name).append(" ").append("?");
                new ConfirmDialog().buildAndShowDialog(sb.toString(), new IContinuation() {
                    @SuppressWarnings("unchecked")
                    public void doIt() {
                        BIKClientSingletons.getService().deleteUserGroup(sub.id, new StandardAsyncCallback() {

                            @Override
                            public void onSuccess(Object result) {
                                BIKClientSingletons.showInfo(I18n.grupaUsunieta.get());
                                optDoAfterDataFetched.doIt();
                            }
                        });
                    }
                });

            }
        };
    }

    protected IContinuation addUserGroup() {
        return new IContinuation() {

            @Override
            public void doIt() {
                new AddSystemUserGroupDialog().buildAndShowDialog(null, I18n.dodajGrupe.get(), new IParametrizedContinuation<String>() {

                    @Override
                    public void doIt(String param) {
                        sub.name = param;

                        BIKClientSingletons.getService().addSystemUserGroup(param, sub.id, new StandardAsyncCallback<Integer>() {

                            @Override
                            public void onSuccess(Integer result) {
                                BIKClientSingletons.showInfo(result == 1 ? I18n.grupaDodana.get() : I18n.grupaIstnieje.get() /* I18N:  */);
                                optDoAfterDataFetched.doIt();
                            }
                        });

                    }
                });
            }
        };
    }

    protected IContinuation copyPermissionFromUser() {
        return new IContinuation() {

            @Override
            public void doIt() {
                String ticket = BIKClientSingletons.getCancellableRequestTicket();
                BIKClientSingletons.getService().getAllBikSystemUserWithFilter(ticket,
                        "", false, null, null, null, null, true, new StandardAsyncCallback<List<SystemUserBean>>() {

                            @Override
                            public void onSuccess(List<SystemUserBean> result) {

                                new AddUsersToGroupDialog().buildAndShowDialog(new ArrayList<String>(), result,
                                        I18n.kopiujUprawnieniaUzytkownika.get(), false,
                                        new IParametrizedContinuation<Pair<List<Integer>, List<String>>>() {

                                            @Override
                                            public void doIt(final Pair<List<Integer>, List<String>> param) {

                                                BIKClientSingletons.getService().copyPermissionFromUser(param.v1.get(0), sub.id, new StandardAsyncCallback<Void>() {

                                                    @Override
                                                    public void onSuccess(Void result) {
                                                        BIKClientSingletons.showInfo(I18n.uprawnieniaSkopiowane.get());
                                                        optDoAfterDataFetched.doIt();
//                                                vp.clear();
//                                                vp.add(fp);
//                                                vp.add(getLabel(param.v2));
//                                                userGroup.usersName = param.v2;
                                                    }
                                                });
                                            }
                                        });
                            }
                        });

            }
        };
    }
}
