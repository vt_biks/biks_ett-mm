﻿exec sp_check_version '1.8.5.9';
go
/*
1. Dodanie indeksów
2. Dodanie funkcji zwracającej wartość atrybutu
3. Zmiana trigera zapisującego historię zmian obiektu (poprawka dla usuniętych o obiektów)
4. Dodanie dodac procedury wyswietlającej  historie obiektu i drzewa
*/

/*1. Dodanie indeksów*/
if not exists(select * from sys.indexes where name='Node_ID_Modified_Date')
create nonclustered index Node_ID_Modified_Date
on bik_node_history (node_id,modified_date)
include (descr,change_source)
go

if not exists(select * from sys.indexes where name='attribute_linked_id_modified_date')
create nonclustered index attribute_linked_id_modified_date
on bik_attribute_linked_history (attribute_linked_id,modified_date)
go

/*2. Dodanie funkcji zwracającej wartość atrybutu*/

IF OBJECT_ID (N'dbo.fn_get_attribute_value') IS NOT NULL
    DROP FUNCTION dbo.fn_get_attribute_value;
GO

create FUNCTION  dbo.fn_get_attribute_value (@attrName varchar(max), @nodeId int)
RETURNS varchar(max) 
AS
BEGIN
		declare @value varchar(max)
	select @value=value from bik_attribute_linked bal join
	bik_attribute ba on bal.attribute_id=ba.id
	join bik_attr_def bad on bad.id=ba.attr_def_id
	where bad.name=@attrName and node_id=@nodeId
	and bad.is_deleted=0 and ba.is_deleted=0 and bal.is_deleted=0
	
	return (@value);
end
go

/*3. Zmiana trigera zapisującego historię zmian obiektu (poprawka dla usuniętych o obiektów)*/
update bik_node_history set name='Obiekt usunięty' where 
name is null and node_id in (select id from bik_node where is_deleted=1)

if (object_id('trg_bik_node_history') is not null)
drop trigger trg_bik_node_history
go

create trigger trg_bik_node_history on bik_node 
for insert, delete, update
as
begin
	set nocount on

	declare @log_level int = 1

	declare @change_source varchar(max) = (select coalesce(coalesce(su.name, su.login_name), ds.description) from bik_spid_source ss left join bik_system_user su on ss.user_id = su.id left join bik_data_source_def ds on ss.data_source_def_id = ds.id where ss.spid = @@spid)
	declare @modified_date datetime = sysdatetime()

	declare @node_status table (
		node_id int,
		old_is_deleted int,
		new_is_deleted int
	)
	insert into @node_status(node_id, old_is_deleted, new_is_deleted)
	select i.id, d.is_deleted, i.is_deleted
	from inserted i join deleted d on i.id = d.id
 
	--fizycznie insert
	insert into bik_node_history(node_id, name, descr, change_source, modified_date)
	select id, name, descr, @change_source, @modified_date
	from inserted where id not in (select node_id from @node_status)
	
	--insert przez set is_deleted=0 a byl is_deleted=1
	insert into bik_node_history(node_id, name, descr, change_source, modified_date)
	select id, name, descr, @change_source, @modified_date
	from inserted where id in (select node_id from @node_status where old_is_deleted = 1 and new_is_deleted = 0)

	insert into bik_node_history(node_id, name, descr, change_source, modified_date) 
	select i.id, i.name, i.descr, @change_source, @modified_date
	from inserted i join deleted d on i.id = d.id
	where i.id in (select node_id from @node_status where old_is_deleted = 0 and new_is_deleted = 0)
	and (coalesce(i.name, '') <> coalesce(d.name, '') or coalesce(i.descr, '') <> coalesce(d.descr, ''))

	insert into bik_node_history(node_id, change_source, modified_date)
	select id, @change_source, @modified_date
	from deleted where id not in (select node_id from @node_status)

	insert into bik_node_history(node_id,name, change_source, modified_date) 
	select node_id,'Obiekt usunięty', @change_source, @modified_date from @node_status where old_is_deleted = 0 and new_is_deleted = 1
end
go
/*4. Dodanie dodac procedury wyswietlającej  historie obiektu i drzewa

execute sp_show_history_of_tree 'nazwa drzewa' 
execute sp_show_history_of_object nodeId 

*/

   if object_id('sp_show_history_of_object_or_tree') is not null
	drop procedure sp_show_history_of_object_or_tree
	go
   
	create procedure sp_show_history_of_object_or_tree(@node_id int,@tree_name varchar(max)) as
	begin
  
    declare @branchId varchar(max) = (select branch_ids from bik_node where id = @node_id )
    declare @aggLevel int = 1;
    declare @treeId int = (select id from bik_tree where name = @tree_name )
     
  
	------------------------nazwa  
    create table  #nameHistory (
    node_id int,
    name varchar(max),
    change_source varchar(max),
    date_added datetime
    )
   declare @sqltext varchar(max) = '';
   set @sqltext=' insert into #nameHistory(node_id, name, date_added)
    select nh.node_id, nh.name, min(nh.modified_date) from bik_node_history nh join bik_node bn on bn.id = nh.node_id
    where coalesce(nh.change_source, '''') <> '''''
    if @node_id  is not null begin
    set @sqltext=@sqltext+'and nh.node_id in (select id from bik_node where branch_ids like '''+cast(@branchId as varchar(max))+'%''
    and len(branch_ids) - len(replace(branch_ids,''|'','''')) <= 
    len('''+cast(@branchId as varchar(max))+''') - len(replace('''+cast(@branchId as varchar(max))+''',''|'','''')) + '+cast(@aggLevel as varchar(max))+')'
    end
    if @treeId is not null begin
    set @sqltext=@sqltext+'
    and bn.tree_id='+cast(@treeId as varchar(max))+''
    end
    set @sqltext=@sqltext+'
    group by nh.node_id, nh.name
    update n set change_source = h.change_source
    from #nameHistory n join bik_node_history h on h.node_id = n.node_id and h.name = n.name and h.modified_date=n.date_added'
--	print @sqltext
	exec(@sqltext)
	--------------------------------opis
	 create table  #descrHistory (
    node_id int,
    descr varchar(max),
    change_source varchar(max),
    date_added datetime
    )
    set @sqltext='insert into #descrHistory(node_id, descr, date_added)
    select distinct nh.node_id, nh.descr, min(modified_date) from bik_node_history nh join bik_node bn on bn.id = nh.node_id
    where coalesce(nh.change_source, '''') <> '''' '
    if @node_id  is not null begin
    set @sqltext=@sqltext+'and nh.node_id in (select id from bik_node where branch_ids like '''+cast(@branchId as varchar(max))+'%''
    and len(branch_ids) - len(replace(branch_ids,''|'','''')) <= len('''+cast(@branchId as varchar(max))+''') - len(replace('''+
    cast(@branchId as varchar(max))+''',''|'','''')) + '+cast(@aggLevel as varchar(max))+')'
    end
    if @treeId is not null begin
    set @sqltext=@sqltext+'
    and bn.tree_id='+cast(@treeId as varchar(max))+''
    end
    set @sqltext=@sqltext+'
    group by nh.node_id, nh.descr'
    update d set d.change_source=h.change_source
    from #descrHistory d join bik_node_history h on h.node_id = d.node_id and coalesce(h.descr, '') = coalesce(d.descr, '') and d.date_added = h.modified_date
--	print @sqltext
	exec(@sqltext)
------------------------------------
	--Atrybuty
	create table  #attributeHistory (
	--declare @attributeHistory table (
		attr_id int
		, attr_name varchar(max)
		, type_attr varchar(max)
		, change_source varchar(max)
		, date_added datetime
		, new_value  varchar(max)
		, old_value varchar(max)
		, node_name varchar(max)
		, node_id int
	)
	set @sqltext='insert into #attributeHistory (attr_id,attr_name,type_attr,change_source,date_added,new_value,old_value,node_name,node_id)
	select alh.attribute_linked_id as attr_id, ad.name as attr_name--, ad.type_attr,
	, coalesce(attr.type_attr, ad.type_attr)as type_attr
	, alh.change_source as change_source, alh.modified_date as date_added
	, alh.value as new_value,
	(select top 1 alhin.value from bik_attribute_linked_history alhin
	join bik_attribute_linked al on alhin.attribute_linked_id = al.id
	where alhin.attribute_linked_id = alh.attribute_linked_id
	and modified_date < alh.modified_date
	order by modified_date desc
	) as old_value
	, bn.name as node_name
	, bn.id as node_id
    from bik_attribute_linked_history alh
    join bik_attribute_linked al on alh.attribute_linked_id = al.id
    join bik_attribute attr on attr.id = al.attribute_id
    join bik_attr_def ad on ad.id = attr.attr_def_id
    join bik_node bn on bn.id = al.node_id
    where coalesce(alh.change_source, '''') <> '''' 
	'
	if @node_id  is not null begin
    set @sqltext=@sqltext+' and al.node_id in (select id from bik_node where branch_ids like '''+cast(@branchId as varchar(max))+'%''
     and len(branch_ids) - len(replace(branch_ids,''|'','''')) <= len('''+cast(@branchId as varchar(max))+''') - len(replace('''+cast(@branchId as varchar(max))+''',''|'','''')) + '+cast(@aggLevel as varchar(max))+')'
    end
    if @treeId is not null begin
    set @sqltext=@sqltext+' and bn.tree_id='+cast(@treeId as varchar(max))+''
    end
    set @sqltext=@sqltext+'
	order by attr_id'
--	print @sqltext
	exec(@sqltext)
update n set new_Value = ltrim(h.n_val),
old_value=LTRIM(h.o_val)
    from #attributeHistory n join  (SELECT 
    stuff((
            SELECT  ', '+SUBSTRING(str,CHARINDEX(',',str,0)+1,LEN(str))as value 
            FROM #attributeHistory  ST1
            cross apply dbo.fn_split_by_sep(new_Value, '|', 7)
            WHERE ST1.attr_id = ST2.attr_id
            and st1.new_Value=st2.new_Value
            FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')  n_val,
             stuff((
            SELECT  ', '+SUBSTRING(str,CHARINDEX(',',str,0)+1,LEN(str))as value 
            FROM #attributeHistory  ST1
            cross apply dbo.fn_split_by_sep(old_value, '|', 7)
            WHERE ST1.attr_id = ST2.attr_id
            and st1.old_value=st2.old_value
            FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')  o_val,
            st2.*
FROM #attributeHistory  ST2)h on h.attr_id=n.attr_id and h.new_Value=n.new_Value and h.old_value=n.old_value
and h.date_added=n.date_added 
where n.type_attr in ('hyperlinkInMono','hyperlinkInMulti','permissions')

---------------------------------------------------------------------------------------------------------
/*Wszystko  */

select   
	date_added as Data
	,change_source as Użytkownik
	, node_name as Obiekt
	, attr_name as Atrybut
	, old_value as 'Stara wartość'
	, new_value as 'Nowa wartość'
 from (
select * from #attributeHistory
union
select -1 as attr_id, 'Nazwa' as attr_name, 'shortText' as type_attr, n.change_source, n.date_added, n.name as new_value, 
(select top 1 name from #nameHistory where 
date_added< n.date_added and node_id=n.node_id
order by date_added desc)  as old_value,bn.name as node_name, bn.id as node_id
from #nameHistory n join bik_node bn on bn.id=n.node_id
union
select -2 as attr_id, 'Opis' as attr_name, 'longText' as type_attr, d.change_source, d.date_added, d.descr as new_value,(select top 1 descr from #descrHistory where 
date_added< d.date_added and node_id=d.node_id
order by date_added desc)  as old_value, bn.name as node_name, bn.id as node_id
from #descrHistory d join bik_node bn on bn.id=d.node_id)x
where (old_Value is not null and old_value!='') or (new_value is not null and new_value!='')
order by date_added desc, attr_id asc

drop table #nameHistory;
drop table #descrHistory ;
drop table  #attributeHistory 
end;
go
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

   if object_id('sp_show_history_of_tree') is not null
	drop procedure sp_show_history_of_tree
	go
   
	create procedure sp_show_history_of_tree(@tree_name varchar(max)) as
	begin
  
	exec sp_show_history_of_object_or_tree null, @tree_name;
   
	end;
	go



/* sp_show_history_of_object*/

   if object_id('sp_show_history_of_object') is not null
	drop procedure sp_show_history_of_object
	go
   
	create procedure sp_show_history_of_object(@node_id int) as
	begin
			exec sp_show_history_of_object_or_tree @node_id,null;
	end;
go
exec sp_update_version '1.8.5.9','1.8.5.10';
go

