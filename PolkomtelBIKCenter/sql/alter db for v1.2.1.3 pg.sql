exec sp_check_version '1.2.1.3';
go

	alter table bik_role_for_node
	add code varchar(255) null default null  
	go

	declare @node_kind_id int;
	declare @tree_id int;
	declare @add_id int;
	declare @brfn_id int;
	
	select @tree_id = id, @node_kind_id = node_kind_id from bik_tree where code = 'UserRoles'
	
	--tu czesc zmieniajaca skrypt "alter db for v1.2.1.1 pg.sql", jeszcze raz dodam te role ze wszystkimi zmiennymi
	update bik_node set is_deleted=1 where tree_id = @tree_id and name = 'Autor' and is_built_in = 1 and is_deleted=0;
	update bik_node set is_deleted=1 where tree_id = @tree_id and name = 'Redaktor' and is_built_in = 1 and is_deleted=0;

    insert into bik_node( name, node_kind_id, tree_id, is_built_in) values ('Autor', @node_kind_id, @tree_id, 1);
	select @add_id = scope_identity()
	insert into bik_role_for_node(caption, node_id, code) values('Autor', @add_id, 'Author');
	select @brfn_id = scope_identity()
	update bik_node set branch_ids = convert(varchar(20),@add_id)+'|', obj_id = @brfn_id where  id = @add_id;

    insert into bik_node( name, node_kind_id, tree_id, is_built_in) values ('Redaktor', @node_kind_id, @tree_id, 1);
	select @add_id = scope_identity()
	insert into bik_role_for_node(caption, node_id, code) values('Redaktor', @add_id, 'Editor');
	select @brfn_id = scope_identity()
	update bik_node set branch_ids = convert(varchar(20),@add_id)+'|', obj_id = @brfn_id where  id = @add_id;

exec sp_update_version '1.2.1.3', '1.2.1.4';
go
