/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bssg.metadatapump.filesystem;

/**
 *
 * @author ctran
 */
public class FileSystemInfoBean {

    public String targetFilePath;
    public String objId;
    public Long fileSize;
    public Long lastModified;
    public String downloadLink;
    public String fileName;

    public FileSystemInfoBean(String path, String objId, Long length, Long date, String downloadLink, String fileName) {
        this.targetFilePath = path;
        this.objId = objId;
        this.fileSize = length;
        this.lastModified = date;
        this.downloadLink = downloadLink;
        this.fileName = fileName;
    }

    public FileSystemInfoBean() {
    }
}
