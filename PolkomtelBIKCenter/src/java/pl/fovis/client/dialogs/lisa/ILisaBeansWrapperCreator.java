/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.dialogs.lisa;

import java.io.Serializable;
import pl.trzy0.foxy.commons.annotations.CreatorPackage;
import simplelib.INamedTypedBeanPropsBeanWrapperCreator;

/**
 *
 * @author tbzowka
 */

@CreatorPackage(value = "/pl/fovis/common/lisa")
public interface ILisaBeansWrapperCreator extends INamedTypedBeanPropsBeanWrapperCreator<Serializable>  {
    
}

