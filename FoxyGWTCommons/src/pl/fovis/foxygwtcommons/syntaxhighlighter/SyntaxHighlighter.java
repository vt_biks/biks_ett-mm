/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.syntaxhighlighter;

import com.google.gwt.core.client.JavaScriptObject;

/**
 *
 * @author tflorczak
 */
public class SyntaxHighlighter {

    public static native String highlight(String code, JavaScriptObject brush, boolean toolbar) /*-{
     var params = {};
     params['toolbar'] = toolbar;
     brush.init(params);
     return brush.getHtml(code);
     }-*/;
}
