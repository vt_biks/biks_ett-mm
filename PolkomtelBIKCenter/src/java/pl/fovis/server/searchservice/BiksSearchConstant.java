/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.searchservice;

import commonlib.LameUtils;
import java.util.Date;
import pl.bssg.searchengine.SearchConstant;
import pl.fovis.server.BIKCenterConfigObj;
import simplelib.BaseUtils;

/**
 *
 * @author ctran
 */
public class BiksSearchConstant extends SearchConstant {

    public static final Date START_OF_ERA = LameUtils.parseDate("1970-01-01 00:00:01.000");
    public static final String ADHOC_SQLS_PATH = LameUtils.getPackagePathOfClass(BiksSearchConstant.class) + "adhocSqlsSearchService.xml";

    public static final String BIK_NODE_NAME = "@name";
    public static final String BIK_NODE_DESC = "@desc";
//    public static final String BIK_ATTRIBUTE_LINKED = "bik_attribute_linked";
    public static final String BIK_JOINED_OBJ_ATTRIBUTE_LINKED = "@joined_obj";
    public static final String BIK_FILE_CONTENT = "@attachment";
    public static final String BIK_TREE_ID = "@tree_id";
    public static final String[] ALL_TEXT_FIELDS = new String[]{
        BiksSearchConstant.BIK_NODE_NAME,
        BiksSearchConstant.BIK_NODE_DESC,
        BiksSearchConstant.BIK_JOINED_OBJ_ATTRIBUTE_LINKED,
        BiksSearchConstant.BIK_FILE_CONTENT
    };

    public static String getIndexDirectory(BIKCenterConfigObj cfg) {
        return BaseUtils.ensureDirSepPostfix(cfg.dirForLucene) + cfg.connConfig.database;
    }

}
