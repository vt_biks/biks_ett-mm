/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import commonlib.connectionpool.ConnectionPoolManagerViaConnectionCreator;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import pl.trzy0.foxy.serverlogic.DriverNameConstants;
import pl.trzy0.foxy.serverlogic.IConnectionDialectHelper;
import simplelib.BaseUtils;
import simplelib.IContinuationWithReturn;
import simplelib.LameRuntimeException;

/**
 *
 * @author lb
 */
public class TeradataConnection extends PooledJdbcConnectionBase<TeradataConnectionConfig> {

    protected ConnectionPoolManagerViaConnectionCreatorForTeradataConnection cpmtc;
    protected String tmode = "";

    public TeradataConnection() {
    }

    public TeradataConnection(TeradataConnectionConfig config) {
        tmode = config.tmode;
        initConnectionFinal(config);
    }

    public String getTeradataMode() {
        return tmode;
    }

    @Override
    protected IConnectionDialectHelper createDialectHelper() {
        return new SqlValueFormatterCurlyTimeStamp();
    }

    @Override
    public void initConnection(final TeradataConnectionConfig config) {
        this.cpm = cpmtc = new ConnectionPoolManagerViaConnectionCreatorForTeradataConnection(config);
        //this.cpm = cpmtc;
    }

    public boolean isConnectionOpenAndValid() {
        return cpmtc.isConnectionOpenAndValid();
    }

    public void commit() {
        try {
            getConnection().commit();
        } catch (SQLException ex) {
            cpm.markConnectionAsInvalid();
            throw new LameRuntimeException("error when trying commit on Teradata connection", ex);
        }
    }

    public void rollback() {
        try {
            getConnection().rollback();
        } catch (SQLException ex) {
            cpm.markConnectionAsInvalid();
            throw new LameRuntimeException("error when trying rollback on Teradata connection", ex);
        }
    }

    class ConnectionPoolManagerViaConnectionCreatorForTeradataConnection extends ConnectionPoolManagerViaConnectionCreator {

        public ConnectionPoolManagerViaConnectionCreatorForTeradataConnection(final TeradataConnectionConfig config) {

            super(new IContinuationWithReturn<Connection>() {
                @Override
                public Connection doIt() {

                    String optParams = BaseUtils.mapToString(new LinkedHashMap<String, String>() {
                        {
                            put("tmode", config.tmode);
                            put("charset", config.charset);
                            put("database", config.database);
                            put("dbs_port", config.port);
                        }
                    }, "=", ",", BaseUtils.SuppressOptions.SUPPRESS_NULLS_AND_WHITESPACES, false);

                    return ConnectionUtils.getConnectionByConnectionString(DriverNameConstants.TERADATA, "jdbc:teradata://" + config.server, config.user, config.password, optParams);
                }
            }, config.poolSize, config.timeout);

            disableConnectionIsValidCheck();
        }

        public boolean isConnectionOpenAndValid() {
            boolean isValid = true;
            try {
                Connection conn = getConnection();
                isValid = (!conn.isClosed()) && isConnectionValid(conn);
            } catch (SQLException ex) {
                return false;
            }
            return isValid;
        }
    }
}
