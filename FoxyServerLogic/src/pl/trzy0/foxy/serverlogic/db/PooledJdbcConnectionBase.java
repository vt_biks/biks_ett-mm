/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic.db;

import commonlib.LameUtils;
import commonlib.connectionpool.IConnectionPoolManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import simplelib.LameRuntimeException;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public abstract class PooledJdbcConnectionBase<CFG> extends JdbcConnectionBase<Object> {

    private static final ILameLogger logger = LameUtils.getMyLogger();
    protected IConnectionPoolManager cpm;
    //protected IConnectionDialectHelper dialect;

    public abstract void initConnection(CFG cfg);

    protected final void initConnectionFinal(CFG cfg) {
        initConnection(cfg);
    }

    @Override
    public void destroy() {
        if (logger.isDebugEnabled()) {
            logger.debug("destroy starts");
        }
        cpm.dispose();
    }

    @Override
    public void finishWorkUnit(boolean success) {
        cpm.recycleThreadConnection(success);
    }

    protected Connection getConnection() {
        if (logger.isDebugEnabled()) {
            logger.debug("getConnection: start");
        }

        Connection res = cpm.getConnection();

        if (logger.isDebugEnabled()) {
            logger.debug("getConnection: before return (end)");
        }
        return res;
    }

    @Override
    protected Statement getStatement() {
        if (logger.isDebugEnabled()) {
            logger.debug("getStatement: start");
        }
        try {
            Connection ccc = getConnection();
            if (logger.isDebugEnabled()) {
                logger.debug("getStatement: after getConnection, before createStatement");
            }
            Statement sss = ccc.createStatement();

            if (logger.isDebugEnabled()) {
                logger.debug("getStatement: before return (end)");
            }

            return sss;
        } catch (SQLException ex) {
            cpm.markConnectionAsInvalid();
            throw new LameRuntimeException("getStatement failed", ex);
        }
    }

    @Override
    protected void releaseStatement(Statement st) {
        try {
            st.close();
        } catch (SQLException ex) {
            cpm.markConnectionAsInvalid();
            throw new LameRuntimeException("error closing statement", ex);
        }
    }
}
