/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common;

import java.io.Serializable;

/**
 *
 * @author wezyr
 */
public class AttachmentBean implements Serializable, IHasSimilarityScore {

    public int id;
    public String caption;
    public String href;
    public String dstCode;
    public boolean dstHasChildren;
    public int nodeId;
    public int srcNodeId;
    public String nodeKindCaption;
    public String branchIds;
    public boolean inheritToDescendants;
    public String treeCode;
    public String descrPlain;
    public Double similarityScore;
    public String attributes;
    public int joinedObjsId;

    @Override
    public Double getSimilarityScore() {
        return similarityScore;
    }
}
