/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import java.util.Map;

/**
 *
 * @author pmielanczuk
 */
public interface IFoxySessionDataProvider<SD> extends IFoxySessionPrimitivesProvider {

    public SD getSessionData();

    public Map<String, Object> getRequestTempStorage();

//    public void setServiceTimeStampForResponse(long timeStamp);
    public void setServiceExtraResponseData(String data);

    public String getFoxyAppInstanceId();

    public String getClientLoggedUserName();

    public long getServiceMethodInvokeCurrentMillis();
}
