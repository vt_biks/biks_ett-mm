/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.trzy0.foxy.serverlogic;

import commonlib.ILameResourceProvider;
import commonlib.LameFileResourceProvider;
import commonlib.LameJarResourceProvider;
import commonlib.LameUtils;
import commonlib.MuppetMaker;
import commonlib.StringFromInputStreamResourceProvider;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import pl.trzy0.foxy.commons.IBeanConnectionEx;
import pl.trzy0.foxy.commons.INamedSqlsDAO;
import simplelib.BaseUtils;
import simplelib.IGenericResourceProvider;
import simplelib.IResourceFromProvider;
import simplelib.JsonReader;
import simplelib.LameRuntimeException;
import simplelib.ResourceFromProvider;
import simplelib.StackTraceUtil;
import simplelib.logging.ILameLogger;

/**
 *
 * @author wezyr
 */
public class FoxyAppUtils {

    public static final String CONFIG_FILE_ENCODING = "Cp1250";
    private static final ILameLogger logger = LameUtils.getMyLogger();

    protected static InputStream tryGetResourceAsStream(String foxyConfigsDir, List<String> paths) {
        if (logger.isDebugEnabled()) {
            logger.debug("tryGetResourceAsStream: will try load config from paths: " + paths);
            logger.debug("tryGetResourceAsStream: env var foxyConfigsDir=" + foxyConfigsDir);
        }

        final String fixedDir = BaseUtils.ensureDirSepPostfix(foxyConfigsDir);
        int idx = 0;
        for (String path : paths) {
            InputStream is = null;

            if (!BaseUtils.isStrEmptyOrWhiteSpace(foxyConfigsDir)) {
                FileInputStream fis;

                final String fullPath = fixedDir + path;
                try {
                    fis = new FileInputStream(fullPath);
                    if (logger.isDebugEnabled()) {
                        logger.debug("tryGetResourceAsStream: ok for fullPath=" + fullPath);
                    }
                } catch (IOException ex) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("tryGetResourceAsStream: exception for fullPath=" + fullPath + "\n" + StackTraceUtil.getCustomStackTrace(ex));
                    }
                    fis = null;
                }
                is = fis;
            }

            boolean fromFile = is != null;

            if (is == null) {
                is = FoxyAppUtils.class.getResourceAsStream(BaseUtils.ensureDirSepPrefix(path));
            }

            if (is != null) {
                if (logger.isInfoEnabled()) {
                    logger.info("tryGetResourceAsStream: trying paths: " + paths);
                    logger.info("tryGetResourceAsStream: env var foxyConfigsDir=" + foxyConfigsDir);
                    logger.info("tryGetResourceAsStream: will read from path " + (fromFile ? "(via foxyConfigsDir) " : "") + "#" + idx + ": " + path);
                }

                return is;
            }

            idx++;
        }

        throw new LameRuntimeException("no file with paths: " + BaseUtils.mergeWithSepEx(paths, ", "));
    }

    protected static InputStream tryGetResourceAsStream(List<String> paths) {
        String foxyConfigsDir = System.getenv("foxyConfigsDir");
        return tryGetResourceAsStream(foxyConfigsDir, paths);
    }

    public static List<String> getConfigPaths(String appNameWithExt, String optAppNameVariant) {
        List<String> paths = new ArrayList<String>();
        paths.add(appNameWithExt);
        String compName = LameUtils.removeNonCanonicSysChars(LameUtils.getSysComputerName());
        String userName = LameUtils.removeNonCanonicSysChars(LameUtils.getSysUserName());
        if (!BaseUtils.isStrEmptyOrWhiteSpace(optAppNameVariant)) {
            if (!BaseUtils.isStrEmptyOrWhiteSpace(compName)
                    && !BaseUtils.isStrEmptyOrWhiteSpace(userName)) {
                paths.add(compName + "-" + userName + "-" + optAppNameVariant + "-" + appNameWithExt);
            }
            if (!BaseUtils.isStrEmptyOrWhiteSpace(userName)) {
                paths.add(userName + "-" + optAppNameVariant + "-" + appNameWithExt);
            }
            if (!BaseUtils.isStrEmptyOrWhiteSpace(compName)) {
                paths.add(compName + "-" + optAppNameVariant + "-" + appNameWithExt);
            }
        }
        if (!BaseUtils.isStrEmptyOrWhiteSpace(compName)
                && !BaseUtils.isStrEmptyOrWhiteSpace(userName)) {
            paths.add(compName + "-" + userName + "-" + appNameWithExt);
        }
        if (!BaseUtils.isStrEmptyOrWhiteSpace(userName)) {
            paths.add(userName + "-" + appNameWithExt);
        }
        if (!BaseUtils.isStrEmptyOrWhiteSpace(compName)) {
            paths.add(compName + "-" + appNameWithExt);
        }

        if (!BaseUtils.isStrEmptyOrWhiteSpace(optAppNameVariant)) {
            paths.add(optAppNameVariant + "-" + appNameWithExt);
        }

        return paths;
    }

    public static <T> T readConfigObj(String appName, Class<T> c) {
        return readConfigObj(appName, c, null);
    }

    public static <T> T readConfigObj(InputStream is, Class<T> c) {
        String confStr = LameUtils.loadAsString(is, CONFIG_FILE_ENCODING);

        Map<String, Object> map = JsonReader.readMap(confStr);
        return MuppetMaker.newMuppet(c, map);
    }

    public static <T> T readConfigObjFromFile(String fileName, Class<T> c) {
        return readConfigObjFromFile(fileName, c, false);
    }

    public static <T> T readConfigObjFromFile(String fileName, Class<T> c, boolean isOptional) {
        InputStream is;
        try {
            is = new FileInputStream(fileName);
        } catch (Exception ex) {
            throw new LameRuntimeException("error reading file: " + fileName, ex);
        }
        if (is == null) {
            if (isOptional) {
                return null;
            } else {
                throw new LameRuntimeException("no such file to read: " + fileName);
            }
        }

        return readConfigObj(is, c);
    }

    public static <T> T readConfigObj(String foxyConfigsDir, String appName, Class<T> c, String optAppNameVariant) {
        List<String> paths = getConfigPaths(appName + ".cfg", optAppNameVariant);

        InputStream is = tryGetResourceAsStream(foxyConfigsDir, paths);

        String confStr = LameUtils.loadAsString(is, CONFIG_FILE_ENCODING);

//        System.out.println("^^^ config file read ^^^\n  fullPath=" + fullConfigFileName
//                + "\n  basePath=" + configFileNameBase
//                + "\n  isFromFull=" + isReadFromFull
//                + "\n  webAppRealName=" + webAppRealName
//                + "\n--- contents:\n" + confStr);
        //System.out.println("after LameUtils.loadAsString confStr=" + confStr);
        Map<String, Object> map = JsonReader.readMap(confStr);

        //System.out.println("CFG MAP=" + map);
        return MuppetMaker.newMuppet(c, map);

    }

    public static <T> T readConfigObj(String appName, Class<T> c, String optAppNameVariant) {
        String foxyConfigsDir = System.getenv("foxyConfigsDir");
        return readConfigObj(foxyConfigsDir, appName, c, optAppNameVariant);
    }

    public static INamedSqlsDAO<Object> createNamedSqlsDAOFromResource(IBeanConnectionEx<Object> connection,
            IResourceFromProvider<String> resource) {

        INamedSqlsDAO<Object> res = new BeanConnectionDAOWithFoxyMill(
                connection, resource, null);
        return res;
    }

    public static INamedSqlsDAO<Object> createNamedSqlsDAOFromResources(IBeanConnectionEx<Object> connection,
            IGenericResourceProvider<String> resProvider, Collection<String> resNames,
            IDaoAwareNamedSqlsModel<Object> optNamedSqlsModel,
            IServerSupportedLocaleNameRetriever optServerSupportedLocaleNameRetriever) {

        INamedSqlsDAO<Object> res = new BeanConnectionDAOWithFoxyMill(connection, resProvider,
                resNames, optNamedSqlsModel, optServerSupportedLocaleNameRetriever);
        return res;
    }

    public static INamedSqlsDAO<Object> createNamedSqlsDAO(IBeanConnectionEx<Object> connection,
            ILameResourceProvider resProvider, String resourceName) {
        IGenericResourceProvider<String> stringResProvider = new StringFromInputStreamResourceProvider(resProvider, "UTF-8");
        return createNamedSqlsDAOFromResource(connection, new ResourceFromProvider<String>(stringResProvider, resourceName));
    }

    public static INamedSqlsDAO<Object> createNamedSqlsDAOFromJar(IBeanConnectionEx<Object> connection,
            String resourceName) {
        return createNamedSqlsDAO(connection, new LameJarResourceProvider(), resourceName);
    }

    public static INamedSqlsDAO<Object> createNamedSqlsDAOFromFile(IBeanConnectionEx<Object> connection,
            String resourceName) {
        return createNamedSqlsDAO(connection, new LameFileResourceProvider(), resourceName);
    }

    public static void main(String[] args) {
//        String compName = LameUtils.removeNonCanonicSysChars("mońa-pc_ex2?");// LameUtils.getComputerName();
//        compName = LameUtils.deAccentExAndLowerCase(compName);
//        compName = compName.replaceAll("[^A-Za-z0-9\\-._]+", "");
//        System.out.println("compName=" + compName);
//        System.out.println("userName=" + LameUtils.removeNonCanonicSysChars(LameUtils.getSysUserName()));
        System.out.println("paths: " + getConfigPaths("nazwa-aplikacji", "nazwa-web-app"));
        System.out.println("paths: " + getConfigPaths("nazwa-aplikacji", null));
        System.out.println("paths: " + getConfigPaths("bikcenter", null));
    }
}
