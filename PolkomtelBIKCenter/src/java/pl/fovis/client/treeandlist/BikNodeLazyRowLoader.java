/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.client.treeandlist;

import com.google.gwt.http.client.Request;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import pl.fovis.client.BIKClientSingletons;
import pl.fovis.client.bikpages.IPageDataFetchBroker;
import pl.fovis.common.BikNodeTreeOptMode;
import pl.fovis.common.TreeNodeBean;
import pl.fovis.foxygwtcommons.treeandlist.IFoxyTreeGrid;
import pl.fovis.foxygwtcommons.treeandlist.ILazyRowLoaderMultiExpand;
import pl.fovis.foxygwtcommons.treeandlist.ITreeRowAndBeanStorage;
import simplelib.BaseUtils;

/**
 *
 * @author wezyr
 */
public class BikNodeLazyRowLoader extends AbstractLazyRowLoader<TreeNodeBean, Integer>
        implements ILazyRowLoaderMultiExpand<Integer> {

    protected boolean noLinkedNodes;
    protected Integer nodeKindId;
    protected String relationType;
    protected String attributeName;
    protected Integer outerTreeId;

    public BikNodeLazyRowLoader(IFoxyTreeGrid<Integer> treeGrid,
            ITreeRowAndBeanStorage<Integer, Map<String, Object>, TreeNodeBean> storage,
            boolean noLinkedNodes, BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode,
            IPageDataFetchBroker dataFetchBroker, boolean calcHasNoChildren, Integer nodeKindId, String relationType,String attributeName,Integer outerTreeId) {
        super(treeGrid, storage, optExtraMode, optFilteringNodeActionCode, dataFetchBroker, calcHasNoChildren);
        this.noLinkedNodes = noLinkedNodes;
        this.nodeKindId = nodeKindId;
        this.relationType = relationType;
        this.attributeName=attributeName;
        this.outerTreeId=outerTreeId;
    }

    @Override
    public Request getOneSubtreeLevelOfTree(/*int treeId, Integer parentNodeId, boolean withLinkedNodes,*/TreeNodeBean bean,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, boolean calcHasNoChildren, AsyncCallback<List<TreeNodeBean>> asyncCallback) {
        return BIKClientSingletons.getService().getOneSubtreeLevelOfTree(bean.treeId, BaseUtils.nullToDef(bean.linkedNodeId, bean.id), noLinkedNodes, optExtraMode, optFilteringNodeActionCode, calcHasNoChildren, nodeKindId, relationType,attributeName,outerTreeId, asyncCallback);
    }

    @Override
    public Request getBikNodeAncestorIds(Integer nodeId, AsyncCallback<List<Integer>> asyncCallback) {
        return BIKClientSingletons.getService().getBikNodeAncestorIds(nodeId, asyncCallback);
    }

    @Override
    public Request getSubtreeLevelsByParentIds(Collection<Integer> parentNodeIds,
            BikNodeTreeOptMode optExtraMode, String optFilteringNodeActionCode, AsyncCallback<List<TreeNodeBean>> asyncCallback) {
        return BIKClientSingletons.getService().getSubtreeLevelsByParentIds(parentNodeIds, optExtraMode, optFilteringNodeActionCode,
                relationType,attributeName,nodeKindId,asyncCallback);
    }

    @Override
    public void optFixBeanPropsAfterLoad(TreeNodeBean tb, TreeNodeBean ancestorBean) {
        if (tb.linkedNodeId != null) {
            tb.parentNodeId = ancestorBean.id;
        }
        tb.isFromLinkedSubtree = ancestorBean.linkedNodeId != null;
    }

    @Override
    public IBean4TreeExtractor<TreeNodeBean, Integer> createBeanExtractor() {
        return new TreeBean4TreeExtractor();
    }

    @Override
    public Request getBikNodeAncestorIdsMulti(Collection<Integer> nodeIds, AsyncCallback<Map<Integer, List<Integer>>> asyncCallback) {
        return BIKClientSingletons.getService().getBikNodeAncestorIdsMulti(nodeIds, asyncCallback);
    }
}
