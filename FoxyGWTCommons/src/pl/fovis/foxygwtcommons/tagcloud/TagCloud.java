/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons.tagcloud;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import simplelib.IContinuation;
import pl.fovis.foxygwtcommons.i18n.I18n;

/**
 *
 * @author wezyr
 */
// i18n-default: no
public class TagCloud extends AbsolutePanel {

    public static final double MAX_SPEED = Math.PI * 2;
    private List<Widget> tags;
    private TagCloudAnimation animation;
    private TagSphereCoords coords;
    //private double speedX;
    //private double speedZ;
    private boolean hasMouse;
    private Label lblStatus;
    private Label lblAnimationStatus;
    private Label debugLabel;
    private int radius;

    public static TagCloud createCloud(Collection<String> tagTxts, int radius) {
        Collection<Widget> labels = new ArrayList<Widget>(tagTxts.size());
        for (String txt : tagTxts) {
            Label l = new Label(txt);
            l.setStyleName("");
            l.getElement().getStyle().setProperty("fontWeight", "bolder" /* I18N: no */);
            l.getElement().getStyle().setProperty("font" /* I18N: no */, "Arial" /* I18N: no */);
            labels.add(l);
        }
        return new TagCloud(labels, radius);
    }

    public TagCloud(Collection<Widget> tags, int radius) {
        //Window.alert("constructor TagCloud(...) called");
        this.tags = new ArrayList<Widget>(tags);
        this.radius = radius;

        for (Widget w : this.tags) {
            add(w);
        }
        //lblStatus = new Label("status");
        //add(lblStatus);
        //setWidgetPosition(lblStatus, -1, -1);
        //debugLabel = new Label("coords");
        //add(debugLabel);
        //setWidgetPosition(debugLabel, -1, -1);
        //lblAnimationStatus = new Label("?");
        //add(lblAnimationStatus);
        //setWidgetPosition(lblAnimationStatus, -1, -1);
        sinkEvents(Event.MOUSEEVENTS);        
    }

    private static native int getEventPageX(Event evt) /*-{
    return evt.pageX || 0;
    }-*/;

    private static native int getEventPageY(Event evt) /*-{
    return evt.pageY || 0;
    }-*/;

    @Override
    public void onBrowserEvent(Event event) {
        int typeInt = event.getTypeInt();
        if ((typeInt & Event.MOUSEEVENTS) != 0) {
            int clientX = getEventPageX(event); // event.getScreenX();
            int clientY = getEventPageY(event); //event.getScreenY();
            hasMouse = typeInt != Event.ONMOUSEOUT;
            int radiusX = getOffsetWidth() / 2;
            int radiusY = getOffsetHeight() / 2;
            int centeredX = clientX - getAbsoluteLeft() - radiusX;
            int centeredY = clientY - getAbsoluteTop() - radiusY;
            double speedX = 0;
            double speedZ = 0;
            if (hasMouse) {
                speedX = -1 * MAX_SPEED * centeredX / radiusX;
                speedZ = MAX_SPEED * centeredY / radiusY;
            } else {
                if (animation != null) {
                    speedX = animation.speedX;
                    speedZ = animation.speedZ;
                }
            }
            if (animation != null) {
                animation.changeSpeed(speedX, speedZ, hasMouse);
            }
            if (lblStatus != null) {
                lblStatus.setText("clientX=" + clientX + ", clientY=" + clientY +
                        ", radiusX = " + radiusX + ", radiusY=" + radiusY +
                        ", getAbsoluteLeft = " + getAbsoluteLeft() + ", getAbsoluteTop=" + getAbsoluteTop() +
                        ", centeredX = " + centeredX + ", centeredY=" + centeredY +
                        ", hasMouse=" + hasMouse + ", speedX=" + speedX + ", speedZ=" + speedZ);
            }
        }
    }

    private void createAndRunAnimation() {
        double offsetX = animation == null ? 0 : animation.currentX;
        double offsetZ = animation == null ? 0 : animation.currentZ;
        double speedX = animation == null ? 0 : animation.speedX;
        double speedZ = animation == null ? 0 : animation.speedZ;

        if (coords == null) {
            coords = new TagSphereCoords(tags.size(), radius, true, debugLabel);
        }

        animation = new TagCloudAnimation(tags,
                radius,
                offsetX, offsetZ,
                speedX, speedZ,
                coords,
                this,
                new IContinuation() {

                    public void doIt() {
                        //Window.alert("new animation");
                        if (animation != null) {
                            createAndRunAnimation();
                        }
                    }
                }, lblAnimationStatus,
                hasMouse);
        animation.run(5000);
    //animation.changeSpeed(speedX, speedZ, true);
    }

    @Override
    protected void onLoad() {
        //Window.alert("onLoad called");
        super.onLoad();
        createAndRunAnimation();
    }

    @Override
    protected void onDetach() {
        //Window.alert("detach!");
        super.onDetach();
        if (animation != null) {
            Animation anim = animation;
            animation = null;
            anim.cancel();
        }
    }
}
