exec sp_check_version '1.0.30';
go

update bik_node
set
  node_kind_id = (select id from bik_node_kind where code = 'Folder')
where
  tree_id = (select id from bik_tree where code = 'Documents')
  and node_kind_id = (select id from bik_node_kind where code = 'DefaultFolder')
  and id != (select MIN(id) from bik_node where tree_id = (select id from bik_tree where code = 'Documents')
  and node_kind_id = (select id from bik_node_kind where code = 'DefaultFolder'))
go

delete from bik_attachment where id not in 
(select a.id as id
from bik_node n inner join bik_joined_objs j on n.id = j.src_node_id
inner join bik_node n2 on j.dst_node_id = n2.id inner join bik_attachment a on n.id = a.node_id
where n.tree_id = (select id from bik_tree where code = 'Documents')
and n2.linked_node_id is null
and n2.tree_id != (select id from bik_tree where code = 'Documents'))
go


delete from bik_joined_objs where
src_node_id in (select id from bik_node where
tree_id = (select id from bik_tree where code = 'Documents')
and node_kind_id = (select id from bik_node_kind where code = 'Document')
and id not in (select node_id from bik_attachment))
or dst_node_id in (select id from bik_node where
tree_id = (select id from bik_tree where code = 'Documents')
and node_kind_id = (select id from bik_node_kind where code = 'Document')
and id not in (select node_id from bik_attachment))
go

delete from bik_node where
tree_id = (select id from bik_tree where code = 'Documents')
and node_kind_id = (select id from bik_node_kind where code = 'Document')
and id not in (select node_id from bik_attachment)
go

exec sp_update_version '1.0.30', '1.0.31';
go


