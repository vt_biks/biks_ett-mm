/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.foxygwtcommons;

import pl.fovis.foxygwtcommons.i18nsupport.GWTCurrentLocaleNameProvider;

/**
 *
 * @author pmielanczuk
 */
//ww: klasa jest final, żeby nie było można robić podklas, bo wtedy duplikuje
// się zmienna factoriesInited i lecą dwie inicjalizacje fabryk, z czego druga
// kończy się niepowodzeniem
public final class InitFactories4GWT {
    
    private static boolean factoriesInited = false;

    public static synchronized void initFactories() {
        if (factoriesInited) {
            return;
        }

        factoriesInited = true;
        JavaScriptRegExpMatcherFactory.setFactory();
        JavaScriptStringComparatorFactory.setFactory();
        JavaScriptSimpleRandomGenerator.setFactory();
        GWTCurrentLocaleNameProvider.setFactory();
    }

    static {
        initFactories();
    }
}
