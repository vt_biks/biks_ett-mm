﻿exec sp_check_version '1.1.7.1';
go

create table bik_active_directory(
	s_a_m_account_name varchar(250) not null, -- login name
	distinguished_name varchar(max) null,
	user_principal_name varchar(max) null,
	w_w_w_home_page varchar(max) null,
	url varchar(max) null,
	title varchar(max) null,
	telephone_number varchar(max) null,
	street_address varchar(max) null,
	st varchar(max) null,
	sn varchar(max) null,
	post_office_box varchar(max) null,
	postal_code varchar(max) null,
	physical_delivery_office_name varchar(max) null,
	pager varchar(max) null,
	other_telephone varchar(max) null,
	other_pager varchar(max) null,
	other_mobile varchar(max) null,
	other_ip_phone varchar(max) null,
	other_facsimile_telephone_number varchar(max) null,
	mobile varchar(max) null,
	manager varchar(max) null,
	--managed_objects varchar(max) null, -- jesli bedzie to pozniej potrzebne to stworzyc dodatkowa tabelke z listami ról
	l varchar(max) null,
	ip_phone varchar(max) null,
	initials varchar(max) null,
	home_phone varchar(max) null,
	given_name varchar(max) null,
	facsimile_telephone_number varchar(max) null,
	direct_reports varchar(max) null,
	department varchar(max) null,
	display_name varchar(max) null,
	description varchar(max) null,
	notes varchar(max) null,
	co varchar(max) null,
	c varchar(max) null
);
go

alter table bik_user
add login_name_for_ad varchar(250) null;
go

insert into bik_admin(param,value)
values('ad.isActive','0')

insert into bik_admin(param,value)
values('ad.avatarField','url')

insert into bik_data_source_def(description) 
values('Active Directory')

insert into bik_attr_category(name, is_deleted, is_built_in)
values('Użytkownicy', 0, 1)
go

--------------------------------------------------------------
--------------------------------------------------------------

insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Zdjęcie', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Account Name', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Distinguished Name', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Nazwa logowania', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Strona domowa', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Strona wwww', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Stanowisko', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Numer telefonu', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Ulica', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Województwo', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Nazwisko', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Skrytka pocztowa', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Kod pocztowy', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Firma', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Telefon pager', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Inny telefon', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Inny pager', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Inny komórkowy', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Inny telefon IP', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Inny fax', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Telefon komórkowy', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Manager', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Zarządza', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Miasto', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Telefon IP', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Inicjały', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Telefon domowy', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Imię', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Telefon fax', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Podwładny', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Pion', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Wyświetlana nazwa', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Opis', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Uwagi', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Kraj/region', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Kraj ISO', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('E-mail', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)
insert into bik_attr_def(name,attr_category_id,hint,is_built_in,is_deleted)
values ('Telefon', (select id from bik_attr_category where name='Użytkownicy'), '', 1, 0)

-- node kindy
declare @User int;
select @User=id from bik_node_kind where code='User'

--atrybuty
declare @Picture int;
select @Picture=id from bik_attr_def where name='Zdjęcie'
declare @AccountName int;
select @AccountName=id from bik_attr_def where name='Account Name'
declare @DistinguishedName int;
select @DistinguishedName=id from bik_attr_def where name='Distinguished Name'
declare @Nazwalogowania int;
select @Nazwalogowania=id from bik_attr_def where name='Nazwa logowania'
declare @Stronadomowa int;
select @Stronadomowa=id from bik_attr_def where name='Strona domowa'
declare @Stronawwww int;
select @Stronawwww=id from bik_attr_def where name='Strona wwww'
declare @Stanowisko int;
select @Stanowisko=id from bik_attr_def where name='Stanowisko'
declare @Numertelefonu int;
select @Numertelefonu=id from bik_attr_def where name='Numer telefonu'
declare @Ulica int;
select @Ulica=id from bik_attr_def where name='Ulica'
declare @Wojew int;
select @Wojew=id from bik_attr_def where name='Województwo'
declare @Nazwisko int;
select @Nazwisko=id from bik_attr_def where name='Nazwisko'
declare @Skrytkapocztowa int;
select @Skrytkapocztowa=id from bik_attr_def where name='Skrytka pocztowa'
declare @Kodpocztowy int;
select @Kodpocztowy=id from bik_attr_def where name='Kod pocztowy'
declare @Firma int;
select @Firma=id from bik_attr_def where name='Firma'
declare @Telefonpager int;
select @Telefonpager=id from bik_attr_def where name='Telefon pager'
declare @Innytelefon int;
select @Innytelefon=id from bik_attr_def where name='Inny telefon'
declare @Innypager int;
select @Innypager=id from bik_attr_def where name='Inny pager'
declare @InnyKom int;
select @InnyKom=id from bik_attr_def where name='Inny komórkowy'
declare @InnyIp int;
select @InnyIp=id from bik_attr_def where name='Inny telefon IP'
declare @Innyfax int;
select @Innyfax=id from bik_attr_def where name='Inny fax'
declare @TelKom int;
select @TelKom=id from bik_attr_def where name='Telefon komórkowy'
declare @Manager int;
select @Manager=id from bik_attr_def where name='Manager'
declare @Zarzadza int;
select @Zarzadza=id from bik_attr_def where name='Zarządza'
declare @Miasto int;
select @Miasto=id from bik_attr_def where name='Miasto'
declare @TelefonIP int;
select @TelefonIP=id from bik_attr_def where name='Telefon IP'
declare @Initials int;
select @Initials=id from bik_attr_def where name='Inicjały'
declare @Telhome int;
select @Telhome=id from bik_attr_def where name='Telefon domowy'
declare @Name int;
select @Name=id from bik_attr_def where name='Imię'
declare @Fax int;
select @Fax=id from bik_attr_def where name='Telefon fax'
declare @Podwladny int;
select @Podwladny=id from bik_attr_def where name='Podwładny'
declare @Pion int;
select @Pion=id from bik_attr_def where name='Pion'
declare @DisplayName int;
select @DisplayName=id from bik_attr_def where name='Wyświetlana nazwa'
declare @Opis int;
select @Opis=id from bik_attr_def where name='Opis'
declare @Uwagi int;
select @Uwagi=id from bik_attr_def where name='Uwagi'
declare @Krajregion int;
select @Krajregion=id from bik_attr_def where name='Kraj/region'
declare @CountryISO int;
select @CountryISO=id from bik_attr_def where name='Kraj ISO'
declare @Mail int;
select @Mail=id from bik_attr_def where name='E-mail'
declare @Telefon int;
select @Telefon=id from bik_attr_def where name='Telefon'
       

-- dopasowanie

insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Picture, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @AccountName, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @DistinguishedName, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Nazwalogowania, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Stronadomowa, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Stronawwww, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Stanowisko, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Numertelefonu, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Ulica, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Wojew, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Nazwisko, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Skrytkapocztowa, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Kodpocztowy, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Firma, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Telefonpager, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Innytelefon, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Innypager, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @InnyKom, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @InnyIp, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Innyfax, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @TelKom, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Manager, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Zarzadza, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Miasto, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @TelefonIP, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Initials, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Telhome, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Name, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Fax, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Podwladny, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Pion, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @DisplayName, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Opis, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Uwagi, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Krajregion, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @CountryISO, 0)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Mail, 1)
insert into bik_attr_system(node_kind_id,attr_id,is_visible)
values (@User, @Telefon, 1)
go


-- poprzednia wersja w "alter db for v1.1.0.6 pm.sql"
if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_verticalize_node_attrs_add_attrs') and type in (N'P', N'PC'))
drop procedure dbo.sp_verticalize_node_attrs_add_attrs
go

create procedure dbo.sp_verticalize_node_attrs_add_attrs(@optNodeFilter varchar(max))
as
begin
  set nocount on

  declare @diags_level int = 0 -- 0 oznacza brak logowania

  declare @attr_names table (attr_id int not null unique, name varchar(255) not null unique, is_deleted int not null)
  
  declare @attr_names_sql varchar(max) = 'select ad.id, ad.name, ad.is_deleted from bik_attr_def ad where is_built_in=0 /*where ad.is_deleted = 0*/'
  
  /*
  if (@optNodeFilter is not null) begin
    set @attr_names_sql = @attr_names_sql + ' and ad.id in (select a.attr_def_id from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id
    inner join bik_node n on al.node_id = n.id where ' + @optNodeFilter + ')'
  end
  */
  
  insert into @attr_names (attr_id, name, is_deleted)
  exec(@attr_names_sql)
  
  --select * from @attr_names
  
  declare attr_names_cur cursor for select attr_id, name, is_deleted from @attr_names
  
  declare @attr_id int, @name varchar(255), @is_deleted int
  
  open attr_names_cur
  
  fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  
  --return
  
  while @@fetch_status = 0 begin
    declare @attr_source_sql varchar(max) = '(select node_id, value from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id 
    where ' + case when @is_deleted = 0 then '' else '1 = 0 and ' end + 'al.is_deleted = 0 and a.is_deleted = 0 and a.attr_def_id = ' + cast(@attr_id as varchar(20)) + ')'
    
    --if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': before verticalize one, @name=' + @name + ', source_sql=' + @attr_source_sql

    exec sp_verticalize_node_attrs_one_table @attr_source_sql, 'node_id', 'value', @name, @optNodeFilter    
  
    fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  end
  
  close attr_names_cur
  
  deallocate attr_names_cur
end
go

exec sp_update_version '1.1.7.1', '1.1.7.2';
go