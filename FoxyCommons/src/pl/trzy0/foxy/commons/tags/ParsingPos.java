package pl.trzy0.foxy.commons.tags;

/**
 *
 * @author wezyr
 */
public class ParsingPos {

    private int currPos;
    private int colNum;
    private int rowNum;
    private String content;
    private int contentLen;
    private String sourceFileName;

    public ParsingPos(String content, String sourceFileName) {
        this.content = content;
        this.contentLen = this.content.length();
        this.currPos = 0;
        this.rowNum = 1;
        this.colNum = 1;
        this.sourceFileName = sourceFileName;
    }

    public ParsingPos(String content, int currPos, int rowNum, int colNum, String sourceFileName) {
        this.content = content;
        this.contentLen = this.content.length();
        this.currPos = currPos;
        this.rowNum = rowNum;
        this.colNum = colNum;
        this.sourceFileName = sourceFileName;
    }

    public boolean isProperCurrPos() {
        return currPos < contentLen && currPos >= 0;
    }

    public void throwAtCurrPos(String msg) {
        throw new FoxyParsingRuntimeException(msg, rowNum, colNum, sourceFileName);
    }

    private void checkProperCurrPos() {
        if (!isProperCurrPos()) {
            throwAtCurrPos("currPos out of bounds: " + currPos + " of " + contentLen);
        }
    }

    public void movePos(int delta) {
        while (delta > 0) {
            checkProperCurrPos();
            if (content.charAt(currPos++) == 10) {
                rowNum++;
                colNum = 1;
            } else {
                colNum++;
            }
            delta--;
        }
    }

    public void moveToPos(int newPos) {
        movePos(newPos - currPos);
    }

    public void moveBehind(String substr) {
        int newPos = content.indexOf(substr, currPos);
        if (newPos == 0) {
            throwAtCurrPos("missing \"" + substr + "\" from current pos");
        }
        moveToPos(newPos + substr.length());
    }

    public boolean compareAtCurrPos(String substr) {
        return content.startsWith(substr, currPos);
    }

    public boolean consumeChars(String chars) {
        boolean hasChars = compareAtCurrPos(chars);
        if (hasChars) {
            currPos += chars.length();
        }
        return hasChars;
    }

    public ParsingPos copy() {
        return new ParsingPos(content, currPos, rowNum, colNum, sourceFileName);
    }

    public char currChar() {
        return content.charAt(currPos);
    }

    public int pos() {
        return currPos;
    }

    public int getRowNum() {
        return rowNum;
    }

    public int getColNum() {
        return colNum;
    }
}
