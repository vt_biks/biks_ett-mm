/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.server.fts;

import java.util.List;
import java.util.Map;
import pl.fovis.common.SearchWizardHitResult;
import pl.fovis.server.fts.luceneUtils.ISearchWizardBucketManager;

/**
 *
 * @author pmielanczuk
 */
public interface ISimilarNodesWizard {

    public static final String TREE_ID_ATTR_NAME = "_tree_id_";
    public static final String NODE_KIND_ID_ATTR_NAME = "_node_kind_id_";
    public static final String TREE_KIND_ATTR_NAME = "_tree_kind_";
    public static final String FILE_CONTENTS_ATTR_NAME = "_file_contents_";

    public void setUp(INodesDataFetcher fetcher, boolean forceRecreate);

    public void setUpReadOnly();

    public void nodeDataChanged();

    public void waitForFinalizeIndexingIfInProgress();

    public boolean isIndexingInProgress();

    public boolean hasIndex();

    public <T> Map<Integer, Float> findSimilarNodeIds(int nodeId, double minScore, int limit,
            ISearchWizardBucketManager<T> bucketManager);

    public List<SearchWizardHitResult> findNodeIds(String searchText, double minScore, int limit,
            String optAdvancedQueryText, boolean advancedQueryResultsMustOccur);

    public void destroy();
}
