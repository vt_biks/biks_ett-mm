/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.fovis.common.dqm;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pmielanczuk
 */
public class DqmGrupaDanychPotomkowieBean implements Serializable {

    // id atrybutu (nodeId węzła atrybutu) -> nazwa
    public Map<Integer, String> atrybutyJakosciDanych;

    // id atrybutu (nodeId węzła atrybutu) -> lista info o testach
    public Map<Integer, List<DataQualityGroupBean>> testyDlaAtrybutow;
}
